import sys
import re
import javalang
from pathlib import Path
import os
from concurrent.futures import ThreadPoolExecutor


def rewrite(sample, target, verbose=False):
    java = sample.read_text()
    print(len(java))

    java = re.sub(r'^( +)\? ', r'\1Object ', java, flags=re.MULTILINE)

    lines = java.split('\n')

    remove = []
    for i, line in enumerate(lines):
        if '/* access modifiers changed from' in line:
            correct = line.split(': ')[1].split(' ')[0]
            correct = " " if correct == "0000" else (" "+correct+" ")
            n = i+1
            lines[n] = lines[n].replace(" public ", correct)
            remove.append(i)
        if '    kotlin.jvm.internal.Intrinsics.' in line:
            line = line.replace('kotlin.jvm.internal.Intrinsics.', '// kotlin.jvm.internal.Intrinsics.')
            lines[i] = line
            
        if '@kotlin.Metadata' in line:
            line = line.replace('@kotlin.Metadata', '// @kotlin.Metadata')
            lines[i] = line

    for i in reversed(remove):
        lines.pop(i)

    java = "\n".join(lines)
    
    java = re.sub(r"( +)(static \{\n)((.*\n)+?)(\1})", r"\1/*\n\1\2\3\5\n\1*/", java)

    tree = javalang.parse.parse(java)
#     print(tree)
    classes = [node for path, node in tree.filter(javalang.tree.ClassDeclaration)]
    fields = [node for path, node in tree.filter(javalang.tree.FieldDeclaration)]
    methods = [node for path, node in tree.filter(javalang.tree.MethodDeclaration)]
    inits = [node for path, node in tree.filter(javalang.tree.ConstructorDeclaration)]
    enums = [node for path, node in tree.filter(javalang.tree.EnumDeclaration)]
    iface = [node for path, node in tree.filter(javalang.tree.InterfaceDeclaration)]

    details = sorted(classes + fields + methods + inits + enums + iface, key=lambda x:x.position)

    lines = java.split('\n')

    interface = []
    
    if isinstance(details[0], javalang.tree.ClassDeclaration):
        header = "\n"\
        "import lanchon.dexpatcher.annotation.DexEdit;\n"\
        "import lanchon.dexpatcher.annotation.DexIgnore;\n"\
        "import lanchon.dexpatcher.annotation.DexAction;\n"\
        "\n"\
        "@DexIgnore"
        # "@DexEdit(defaultAction = DexAction.IGNORE)"
    
    elif isinstance(details[0], (javalang.tree.InterfaceDeclaration, javalang.tree.EnumDeclaration)):
        header = "\n"\
        "import lanchon.dexpatcher.annotation.DexEdit;\n"\
        "import lanchon.dexpatcher.annotation.DexIgnore;\n"\
        "import lanchon.dexpatcher.annotation.DexAction;\n"\
        "\n"\
        "@DexIgnore"
        
    else:
        raise ValueError(sample)
        
    for i in iface:
#         print(dir(i))
#         print(i.children)
#         print(i.fields)
        interface.extend([n for n in i.fields if isinstance(n, javalang.tree.FieldDeclaration)])
        
#     print(interface)
        
    pos = details[0].position.line-1
    while lines[pos-1].strip().startswith('@') or \
          lines[pos-1].strip().startswith('/* ') or \
          lines[pos-1].strip().startswith('// '):
        pos -= 1

    lines.insert(pos, header)

    for node in reversed(details[1:]):

        line = lines[node.position.line]

        if isinstance(node, javalang.tree.FieldDeclaration) and node not in interface:
            line = line.replace(' final ', ' /* final */ ')
            line = line.replace(' = ', '; // = ')
            lines[node.position.line] = line
            
#             print(dir(node))
#             print(node)

        indent = len(line) - len(line.lstrip())

        if isinstance(node, javalang.tree.ClassDeclaration):
            prev = lines[node.position.line-1]
            if "/* renamed from" in prev:
                clash_target = prev.strip().split(' ')[3]
                annotation = '@DexEdit(defaultAction = DexAction.IGNORE, target = "%s")' % clash_target
            else:
                annotation = "@DexEdit(defaultAction = DexAction.IGNORE)"
        else:
            annotation = "@DexIgnore"

        pos = node.position.line
        while lines[pos-1].strip().startswith('@') or \
              lines[pos-1].strip().startswith('// ') or \
              lines[pos-1].strip().startswith('/* '):
            pos -= 1

        lines.insert(pos, (" "*indent + annotation))


    print("Writing", target)
    target.parent.mkdir(parents=True, exist_ok=True)
    target.write_text("\n".join(lines))
    # os.system('git add %s' % target)


conversions = (
  (Path(r".\src\src\main\java\com\fossil"), Path(r"..\src\main\java_decomp\com\fossil")), 
  (Path(r".\src\src\main\java\com\misfit"), Path(r"..\src\main\java_decomp\com\misfit")), 
  (Path(r".\src\src\main\java\com\portfolio"), Path(r"..\src\main\java_decomp\com\portfolio")), 
)

import os
import sys
from concurrent.futures import ThreadPoolExecutor


jadx_command = r'jadx-1.0.0-b1165-8ba3e935\bin\jadx -d src -e --show-bad-code --no-inline-anonymous --escape-unicode --rename-flags valid Fossil_Smartwatches_v4.1.3.apk'
jadx_command_verbose = r'jadx-1.0.0-b1165-8ba3e935\bin\jadx -d src -e --no-imports --show-bad-code --no-inline-anonymous --escape-unicode --rename-flags none --single-class %s Fossil_Smartwatches_v4.1.3.apk'


verbose = False
if len(sys.argv) > 1:
    # print(sys.argv)
    jadx = Path(sys.argv[1]).resolve()
    try:
        tail = jadx.relative_to(Path(r".\src\src\main\java").resolve())
    except ValueError:
        dest = jadx
        tail = jadx.relative_to(Path(r"..\src\main\java_decomp").resolve())
        jadx = Path(r".\src\src\main\java") / tail
    else:
        dest = Path(r"..\src\main\java_decomp") / tail

    # print(jadx, dest)
    # verbose = True
    # conversions = ((jadx, dest),)

    print("Verbose processing", jadx)

    class_name = str(tail)[:-5].replace('\\', '.').replace('/', '.')
    os.system(jadx_command_verbose % class_name)    

    try:
        rewrite(jadx, dest, True)
    except:
        print("Failed, copying instead for inspection:", dest)
        dest.parent.mkdir(parents=True, exist_ok=True)
        dest.write_text(jadx.read_text())
    sys.exit()


os.system(jadx_command)

with ThreadPoolExecutor(max_workers=30) as executor:

  for jadx, dest in conversions:

    for root, dirs, files in os.walk(jadx):
        for f in files:
            # if 'com/fossil/blesdk/obfuscated' in root.replace('\\', '/'):
            #     continue

            sample = Path(os.path.join(root, f))

            rel = os.path.relpath(root, jadx)
            target = dest / rel / f

            if target.exists():
                
                # print("Not replacing", target)
                pass
            else:
                try:
                    rewrite(sample, target)
                except:
                    print(sample)
                    # raise
#                 executor.submit(os.system, "git add %s" % target)

