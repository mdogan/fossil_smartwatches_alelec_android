# Fossil Smartwatches alelec

This project provides a patched version of the Fossil Smartwatches android phone app to support Fossil's hybrid smart watch range.

Currently based on the v4.1.3 release of the Fossil App, this project adds:
* Support for notifications from all apps on phone.
* Android "Do Not Disturb" is honored.

Other than this the app should look and behave exactly the same as the official one.

The notifications for each app can be turned on and off the same way as the original app settings page.
The correct app icon should come through to your watch, however a generic icon often shows for the very first
notification for each app before the correct one is transferred to your watch for subsequent notifications.

#Installation

As mentioned, this is completely unofficial, and slightly annoying to install.
My patched app cannot be installed at the same time as the official one, so you'll need to uninstall it first (which wipes all your settings).

If you have Titanium Backup, make a backup of the official one first, then uninstall it.

My app is available to download as an apk from https://gitlab.com/alelec/fossil_smartwatches_alelec_android/-/releases

You should be able to install the apk directly from the download, though your phone will probably
warn you about installing from an unofficial source, which you'll need to allow to install the app.

If you're on android 10 and / or using chrome it'll generally work against you when it comes to
downloading an apk, this might help: https://www.reddit.com/r/android_beta/comments/d0wvi3/android_10can_install_apk_that_are_not_from/

Sometimes it also helps to hold down on the link to get the popup menu, then select "Download link".

Now if you made a titanium backup beforehand, you can now restore data only of the fossil app and
it'll continue to work with all your previous settings in place.

This has all been possible thanks to the ability to decompile android/java and the magic of
[DexPatcher](https://github.com/DexPatcher/dexpatcher-gradle) which allows me to make these kind of
patches relatively quickly and, more importantly, reliably!