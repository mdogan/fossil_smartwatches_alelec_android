package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p90 {
    @DexIgnore
    public static final String a(String str) {
        wd4.b(str, "$this$checkAndAddTerminateCharacter");
        if ((str.length() > 0) && eg4.e(str) == ((char) 0)) {
            return str;
        }
        return str + 0;
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, int i, Charset charset, CodingErrorAction codingErrorAction, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            charset = nf4.a;
        }
        if ((i2 & 4) != 0) {
            codingErrorAction = CodingErrorAction.IGNORE;
            wd4.a((Object) codingErrorAction, "CodingErrorAction.IGNORE");
        }
        return a(str, i, charset, codingErrorAction);
    }

    @DexIgnore
    public static final String a(String str, int i, Charset charset, CodingErrorAction codingErrorAction) {
        wd4.b(str, "$this$truncate");
        wd4.b(charset, "charset");
        wd4.b(codingErrorAction, "codingErrorAction");
        CharsetDecoder newDecoder = charset.newDecoder();
        wd4.a((Object) newDecoder, "charset.newDecoder()");
        byte[] bytes = str.getBytes(charset);
        wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        int min = Math.min(i, bytes.length);
        ByteBuffer wrap = ByteBuffer.wrap(bytes, 0, min);
        wd4.a((Object) wrap, "ByteBuffer.wrap(stringByteArray, 0, lengthInByte)");
        CharBuffer allocate = CharBuffer.allocate(min);
        wd4.a((Object) allocate, "CharBuffer.allocate(lengthInByte)");
        newDecoder.onMalformedInput(codingErrorAction);
        newDecoder.decode(wrap, allocate, true);
        newDecoder.flush(allocate);
        char[] array = allocate.array();
        wd4.a((Object) array, "charBuffer.array()");
        return new String(array, 0, allocate.position());
    }
}
