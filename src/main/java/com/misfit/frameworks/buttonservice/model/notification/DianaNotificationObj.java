package com.misfit.frameworks.buttonservice.model.notification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.text.TextUtils;

import com.fossil.blesdk.device.data.notification.AppNotification;
import com.fossil.blesdk.device.data.notification.NotificationFlag;
import com.fossil.blesdk.device.data.notification.NotificationType;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.WeiboAppManager;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public final class DianaNotificationObj extends NotificationBaseObj {

    // @DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.IGNORE)
    @DexReplace
    public static class AApplicationName extends DynaEnum{
        public final static AApplicationName PHONE_INCOMING_CALL;
        public final static AApplicationName PHONE_MISSED_CALL;
        public final static AApplicationName MESSAGES;
        public final static AApplicationName GOOGLE_CALENDAR;
        public final static AApplicationName GMAIL;
        public final static AApplicationName HANGOUTS;
        public final static AApplicationName MESSENGER;
        public final static AApplicationName WHATSAPP;
        public final static AApplicationName FACEBOOK;
        public final static AApplicationName TWITTER;
        public final static AApplicationName INSTAGRAM;
        public final static AApplicationName SNAPCHAT;
        public final static AApplicationName WECHAT;
        public final static AApplicationName WEIBO;
        public final static AApplicationName LINE;
        public final static AApplicationName GOOGLE_FIT;
        public final static AApplicationName FOSSIL;

        public static final Companion Companion;
        public final String appName;
        public final long bundleCrc;
        public final byte groupId;
        public final String iconFwPath;
        public final int iconResourceId;
        public final NotificationBaseObj.ANotificationType notificationType;
        public final String packageName;

        public static AApplicationName get(String appName) {
            return AApplicationName.get(AApplicationName.class, appName);
        }

        public static AApplicationName[] values() {
            return values(AApplicationName.class);
        }


        @DexReplace
        public static final class Companion {
            public Companion() {
            }

            public final boolean isPackageNameSupportedBySDK(String str) {
                wd4.b(str, "packageName");
                for (AApplicationName packageName : AApplicationName.values()) {
                    if (wd4.a((Object) str, (Object) packageName.getPackageName())) {
                        return true;
                    }
                }
                return false;
            }

            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        static {
            PHONE_INCOMING_CALL = new AApplicationName("Phone", 3076063360L, (byte) 1, "incoming call", R.drawable.icon_phone, "icIncomingCall.icon", NotificationBaseObj.ANotificationType.INCOMING_CALL, 0);
            PHONE_MISSED_CALL = new AApplicationName("Phone", 3076063360L, (byte) 1, "miss call", R.drawable.icon_phone, "icMissedCall.icon", NotificationBaseObj.ANotificationType.MISSED_CALL, 1);
            MESSAGES = new AApplicationName("Messages", 758293805, (byte) 2, "text message", R.drawable.icon_messages, "icMessage.icon", NotificationBaseObj.ANotificationType.TEXT, 2);
            GOOGLE_CALENDAR = new AApplicationName("Google Calendar", 1373531549, (byte) 5, "com.google.android.calendar", R.drawable.icon_google_calendar, "icCalendar.icon", NotificationBaseObj.ANotificationType.CALENDAR, 3);
            GMAIL = new AApplicationName("Gmail", 4233104465L, (byte) 6, "com.google.android.gm", R.drawable.icon_gmail, "icEmail.icon", NotificationBaseObj.ANotificationType.EMAIL, 4);
            HANGOUTS = new AApplicationName("Hangouts", 3756985074L, (byte) 3, "com.google.android.talk", R.drawable.icon_hangouts, "icHangouts.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 5);
            MESSENGER = new AApplicationName("Messenger", 1427889641, (byte) 2, "com.facebook.orca", R.drawable.icon_messenger, "icMessenger.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 6);
            WHATSAPP = new AApplicationName("WhatsApp", 3672127513L, (byte) 2, "com.whatsapp", R.drawable.icon_whatsapp, "icWhatsapp.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 7);
            FACEBOOK = new AApplicationName("Facebook", 2178452653L, (byte) 2, "com.facebook.katana", R.drawable.icon_facebook, "icFacebook.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 8);
            TWITTER = new AApplicationName("Twitter", 2290640688L, (byte) 4, "com.twitter.android", R.drawable.icon_twitter, "icTwitter.icon", NotificationBaseObj.ANotificationType.NOTIFICATION,9);
            INSTAGRAM = new AApplicationName("Instagram", 351310857, (byte) 3, "com.instagram.android", R.drawable.icon_instagram, "icInstagram.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 10);
            SNAPCHAT = new AApplicationName("Snapchat", 1569315745, (byte) 2, "com.snapchat.android", R.drawable.icon_snapchat, "icSnapchat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 11);
            WECHAT = new AApplicationName("WeChat", 2332507162L, (byte) 2, "com.tencent.mm", R.drawable.icon_wechat, "icWeChat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 12);
            WEIBO = new AApplicationName("Weibo", 331027221, (byte) 7, WeiboAppManager.WEIBO_PACKAGENAME, R.drawable.icon_weibo, "icWeibo.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 13);
            LINE = new AApplicationName("Line", 3857604782L, (byte) 2, "jp.naver.line.android", R.drawable.icon_line, "icLine.icon", NotificationBaseObj.ANotificationType.NOTIFICATION, 14);
            GOOGLE_FIT = new AApplicationName("Google Fit", 1727023459, (byte) 2, "com.google.android.apps.fitness", R.drawable.icon_google_fit, "", NotificationBaseObj.ANotificationType.NOTIFICATION, 15);
            FOSSIL = new AApplicationName("Fossil Smartwatches", 2875037489L, (byte) 2, "com.fossil.wearables.fossil", R.drawable.icon_messages, "", NotificationBaseObj.ANotificationType.NOTIFICATION, 16);

            Companion = new Companion((rd4) null);

            getAllApplications();
        }

        static void getAllApplications() {
            Context context = PortfolioApp.W.c().getApplicationContext();
            PackageManager pm = context.getPackageManager();
            @SuppressLint("WrongConstant")
            List<ApplicationInfo> appList = removeAppsThatWeDontWant(context, pm, pm.getInstalledApplications(128));
            try {
                Collections.sort(appList, (lhs, rhs) -> {
                    if (lhs == rhs) {
                        return 0;
                    }
                    if (lhs == null) {
                        return -1;
                    }
                    if (rhs == null) {
                        return 1;
                    }
                    try {
                        if ("/system/priv-app/Keyguard.apk".equals(lhs.sourceDir) || "/system/priv-app/Keyguard.apk".equals(rhs.sourceDir)) {
                            return lhs.packageName.compareTo(rhs.packageName);
                        }
                        CharSequence obj1 = lhs.loadLabel(pm);
                        CharSequence obj2 = rhs.loadLabel(pm);
                        if (obj1 == obj2) {
                            return 0;
                        }
                        if (obj1 == null) {
                            return -1;
                        }
                        if (obj2 == null) {
                            return 1;
                        }
                        return obj1.toString().compareToIgnoreCase(obj2.toString());
                    } catch (Exception e) {
                        return lhs.packageName.compareTo(rhs.packageName);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


            for (ApplicationInfo appInfo: appList) {
                if (appInfo.name == null) {
                    continue;
                }
                if (AApplicationName.Companion.isPackageNameSupportedBySDK(appInfo.packageName)) {
                    continue;
                }

                Charset charset = nf4.a;
                byte[] bytes = appInfo.packageName.getBytes(charset);
                long crc = Crc32Calculator.a.a(bytes, Crc32Calculator.CrcType.CRC32);
                String iconFwPath = /*(notifIcon == null) ? "icHangouts.icon" : */appInfo.packageName + ".icon";
                new AApplicationName(appInfo.name, crc, (byte) 6, appInfo.packageName, R.drawable.icon_messages, iconFwPath, NotificationBaseObj.ANotificationType.NOTIFICATION, -1);
            }
        }

        static boolean isEmptyAfterTrim(CharSequence var0) {
            return var0 == null || var0.toString().trim().length() <= 0;
        }

        static boolean equalsOrBothEmptyAfterTrim(CharSequence var0, CharSequence var1) {

            return (isEmptyAfterTrim(var0) && isEmptyAfterTrim(var1)) ||
                    (!isEmptyAfterTrim(var0) && !isEmptyAfterTrim(var1) &&
                            TextUtils.equals(var0.toString().trim(), var1.toString().trim()));
        }

        static List<ApplicationInfo> removeAppsThatWeDontWant(Context context, PackageManager pm, List<ApplicationInfo> list) {
            ArrayList<ApplicationInfo> appList = new ArrayList<>();
            for (ApplicationInfo info : list) {
                try {
                    if (pm.getLaunchIntentForPackage(info.packageName) != null && !equalsOrBothEmptyAfterTrim(context.getPackageName(), info.packageName)) {
                        appList.add(info);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return appList;
        }


        AApplicationName(String name, long bundleCrc, byte groupId, String packageName, int iconResourceId, String iconFwPath, NotificationBaseObj.ANotificationType aNotificationType, int ordinal) {
            super(packageName, ordinal);
            this.appName = name;
            this.bundleCrc = bundleCrc;
            this.groupId = groupId;
            this.packageName = packageName;
            this.iconResourceId = iconResourceId;
            this.iconFwPath = iconFwPath;
            this.notificationType = aNotificationType;
        }

        public final String getAppName() {
            return this.appName;
        }

        public final long getBundleCrc() {
            return this.bundleCrc;
        }

        public final byte getGroupId() {
            return this.groupId;
        }

        public final String getIconFwPath() {
            return this.iconFwPath;
        }

        public final int getIconResourceId() {
            return this.iconResourceId;
        }

        public final NotificationBaseObj.ANotificationType getNotificationType() {
            return this.notificationType;
        }

        public final String getPackageName() {
            return this.packageName;
        }
    }

    @DexIgnore
    public AApplicationName notificationApp;

    @DexIgnore
    public DianaNotificationObj(AApplicationName aApplicationName) {
        wd4.b(aApplicationName, "appName");
        this.notificationApp = aApplicationName;
    }

    @DexIgnore
    public final AApplicationName getNotificationApp() {
        return this.notificationApp;
    }

    @DexIgnore
    public final void setNotificationApp(AApplicationName aApplicationName) {
        wd4.b(aApplicationName, "<set-?>");
        this.notificationApp = aApplicationName;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return "UID=" + getUid() + ", notificationApp=" + this.notificationApp + ", flag=" + getNotificationFlags();
    }

    @DexIgnore
    public AppNotification toSDKNotification() {
        NotificationType sDKNotificationType = getNotificationType().toSDKNotificationType();
        int uid = getUid();
        long bundleCrc = this.notificationApp.getBundleCrc();
        String title = getTitle();
        String sender = getSender();
        String message = getMessage();
        Object[] array = toSDKNotificationFlags(getNotificationFlags()).toArray(new NotificationFlag[0]);
        if (array != null) {
            return new AppNotification(sDKNotificationType, uid, bundleCrc, title, sender, message, (NotificationFlag[]) array);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexReplace
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(getUid());
        parcel.writeInt(getNotificationType().ordinal());
        parcel.writeInt(this.notificationApp.ordinal());
        parcel.writeString(getTitle());
        parcel.writeString(getSender());
        parcel.writeString(getMessage());
        ArrayList arrayList = new ArrayList();
        for (NotificationBaseObj.ANotificationFlag ordinal : getNotificationFlags()) {
            arrayList.add(Integer.valueOf(ordinal.ordinal()));
        }
        parcel.writeList(arrayList);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DianaNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, AApplicationName aApplicationName, String str, String str2, String str3, List<NotificationBaseObj.ANotificationFlag> list) {
        this(aApplicationName);
        wd4.b(aNotificationType, "notificationType");
        wd4.b(aApplicationName, "appName");
        wd4.b(str, "title");
        wd4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        wd4.b(str3, "message");
        wd4.b(list, "notificationFlags");
        setUid(i);
        setNotificationType(aNotificationType);
        setTitle(str);
        setSender(str2);
        setMessage(str3);
        setNotificationFlags(list);
    }

    @DexIgnore
    public DianaNotificationObj(Parcel parcel) {
        super(parcel);
        setUid(parcel.readInt());
        setNotificationType(NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        this.notificationApp = AApplicationName.values()[parcel.readInt()];
        String readString = parcel.readString();
        setTitle(readString == null ? "" : readString);
        String readString2 = parcel.readString();
        setSender(readString2 == null ? "" : readString2);
        String readString3 = parcel.readString();
        setMessage(readString3 == null ? "" : readString3);
        setNotificationFlags(new ArrayList());
        ArrayList<Number> arrayList = new ArrayList<>();
        parcel.readList(arrayList, (ClassLoader) null);
        for (Number intValue : arrayList) {
            getNotificationFlags().add(NotificationBaseObj.ANotificationFlag.values()[intValue.intValue()]);
        }
    }
}
