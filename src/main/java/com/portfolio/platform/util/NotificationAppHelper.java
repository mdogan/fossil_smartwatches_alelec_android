package com.portfolio.platform.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import com.fossil.blesdk.device.data.notification.NotificationVibePattern;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.ms3;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.ol2;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.CRC32;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public final class NotificationAppHelper {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ NotificationAppHelper b; // = new NotificationAppHelper();

    /*
    static {
        String simpleName = NotificationAppHelper.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationAppHelper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final List<AppFilter> a(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? en2.p.a().a().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter a(String str, MFDeviceFamily mFDeviceFamily) {
        wd4.b(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return en2.p.a().a().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final boolean a(String str) {
        wd4.b(str, "type");
        return a(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final Object a(wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, fn2 fn2, kc4<? super List<AppNotificationFilter>> kc4) {
        throw null;
        // return kg4.a(zh4.a(), new NotificationAppHelper$buildNotificationAppFilters$Anon2(fn2, notificationSettingsDatabase, qx2, wy2, (kc4) null), kc4);
    }

    @DexReplace
    // @DexIgnore
    public final AppNotificationFilterSettings a(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        ArrayList<AppNotificationFilter> arrayList = new ArrayList<>();
        if (sparseArray != null) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            wd4.a((Object) clone, "it.clone()");
            int size = clone.size();
            short movingHand_s = -1;
            for (int i = 0; i < size; i++) {
                List<BaseFeatureModel> valueAt = clone.valueAt(i);
                if (valueAt != null) {
                    for (BaseFeatureModel baseFeatureModel : valueAt) {
                        if (baseFeatureModel.isEnabled()) {
                            short movingHand_c = (short) ol2.c(baseFeatureModel.getHour());
                            int i2 = 10000;
                            if (baseFeatureModel instanceof ContactGroup) {
                                ContactGroup contactGroup = (ContactGroup) baseFeatureModel;
                                if (contactGroup.getContacts() != null) {
                                    for (Contact next : contactGroup.getContacts()) {
                                        wd4.a((Object) next, "contact");
                                        if (next.isUseCall()) {
                                            if (z) {
                                                movingHand_s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(movingHand_c, movingHand_c, movingHand_s, i2);
                                            DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                            // FNotification fNotification = r13;
                                            FNotification fNotification2 = new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), -1, "", aApplicationName.getNotificationType());
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification2);
                                            appNotificationFilter.setSender(next.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                                            appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (next.isUseSms()) {
                                            if (z) {
                                                movingHand_s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(movingHand_c, movingHand_c, movingHand_s, i2);
                                            DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.MESSAGES;
                                            // FNotification fNotification3 = r12;
                                            FNotification fNotification4 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification4);
                                            appNotificationFilter2.setSender(next.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                                            appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                        i2 = 10000;
                                    }
                                }
                            } else if (baseFeatureModel instanceof AppFilter) {
                                AppFilter appFilter = (AppFilter) baseFeatureModel;
                                String type = appFilter.getType();
                                if (type == null || cg4.a((CharSequence)type)) {
                                    continue;
                                }
                                CRC32 crc32 = new CRC32();
                                String type2 = appFilter.getType();
                                wd4.a((Object) type2, "item.type");
                                Charset charset = nf4.a;
                                if (type2 != null) {
                                    byte[] bytes = type2.getBytes(charset);
                                    wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                                    crc32.update(bytes);
                                    long value = crc32.getValue();
                                    String name = appFilter.getName() == null ? "" : appFilter.getName();
                                    if (z) {
                                        movingHand_s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                    }
                                    NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(movingHand_c, movingHand_c, movingHand_s, 10000);
                                    wd4.a((Object) name, "appName");
                                    String type3 = appFilter.getType();
                                    wd4.a((Object) type3, "item.type");
                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(name, value, (byte) 2, type3, -1, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                    appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
                                    appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                                    arrayList.add(appNotificationFilter3);
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    continue;
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "buildAppNotificationFilterSettings size = " + arrayList.size());
        return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final boolean a(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku specialSku;
        wd4.b(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            specialSku = companion.fromType(sku);
        } else {
            specialSku = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return specialSku == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(Context context, NotificationSettingsDatabase notificationSettingsDatabase, fn2 fn2) {
        int i;
        wd4.b(context, "context");
        wd4.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        wd4.b(fn2, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean A = fn2.A();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        int i2 = 0;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + A);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> a2 = a(i, i2);
        List<AppWrapper> a3 = a(context);
        arrayList.addAll(a2);
        arrayList.addAll(ms3.a(a3, A));
        return arrayList;
    }

    // @DexIgnore
    @DexReplace
    public final List<AppNotificationFilter> a(int group, int i2) {
        ArrayList<AppNotificationFilter> arrayList = new ArrayList<>();
        List<ContactGroup> allContactGroups = en2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (group == 0) {
            DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
            // FNotification fNotification = r9;
            FNotification fNotification = new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
            arrayList.add(new AppNotificationFilter(fNotification));
        } else if (group == 1) {
            int size = allContactGroups.size();
            for (int i5 = 0; i5 < size; i5++) {
                ContactGroup contactGroup = allContactGroups.get(i5);
                DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                FNotification fNotification3 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                wd4.a((Object) contactGroup, "item");
                List<Contact> contacts = contactGroup.getContacts();
                wd4.a((Object) contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification3);
                    Contact contact = contactGroup.getContacts().get(0);
                    wd4.a((Object) contact, "item.contacts[0]");
                    appNotificationFilter.setSender(contact.getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }

        if (i2 == 0) {
            DianaNotificationObj.AApplicationName aApplicationName3 = DianaNotificationObj.AApplicationName.MESSAGES;
            FNotification fNotification = new FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
            arrayList.add(new AppNotificationFilter(fNotification));
        } else if (i2 == 1) {
            int size2 = allContactGroups.size();
            for (int i6 = 0; i6 < size2; i6++) {
                ContactGroup contactGroup2 = allContactGroups.get(i6);
                DianaNotificationObj.AApplicationName aApplicationName4 = DianaNotificationObj.AApplicationName.MESSAGES;
                FNotification fNotification4 = new FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                wd4.a((Object) contactGroup2, "item");
                List<Contact> contacts2 = contactGroup2.getContacts();
                wd4.a((Object) contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification4);
                    Contact contact2 = contactGroup2.getContacts().get(0);
                    wd4.a((Object) contact2, "item.contacts[0]");
                    appNotificationFilter2.setSender(contact2.getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<AppWrapper> a(Context context) {
        List<AppFilter> allAppFilters = en2.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.a> it = AppHelper.f.b(context).iterator();
        while (it.hasNext()) {
            AppHelper.a next = it.next();
            if (TextUtils.isEmpty(next.b()) || !cg4.b(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    wd4.a((Object) next2, "appFilter");
                    if (wd4.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        return linkedList;
    }
}
