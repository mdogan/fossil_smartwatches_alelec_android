package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.a62;
import com.fossil.blesdk.obfuscated.ar2;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.e62;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fc;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.g62;
import com.fossil.blesdk.obfuscated.gd;
import com.fossil.blesdk.obfuscated.hd;
import com.fossil.blesdk.obfuscated.hr3;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.jx3;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.kj;
import com.fossil.blesdk.obfuscated.ko2;
import com.fossil.blesdk.obfuscated.lu3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.nj2;
import com.fossil.blesdk.obfuscated.nl2;
import com.fossil.blesdk.obfuscated.o42;
import com.fossil.blesdk.obfuscated.o7;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.ol2;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qk2;
import com.fossil.blesdk.obfuscated.qs3;
import com.fossil.blesdk.obfuscated.qx3;
import com.fossil.blesdk.obfuscated.r44;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rj2;
import com.fossil.blesdk.obfuscated.sc;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tl2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.blesdk.obfuscated.wl2;
import com.fossil.blesdk.obfuscated.y52;
import com.fossil.blesdk.obfuscated.yi;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.yr3;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.blesdk.obfuscated.zw;
import com.fossil.wearables.fossil.R;
import com.google.common.primitives.Ints;
import com.google.firebase.iid.FirebaseInstanceId;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.enums.FossilBrand;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.localization.LocalizationManager;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import java.io.File;
import java.io.IOException;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.Regex;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

// @DexEdit
@DexIgnore
public final class PortfolioApp extends hd implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String P;
    @DexIgnore
    public static boolean Q;
    @DexIgnore
    public static PortfolioApp R;
    @DexIgnore
    public static boolean S;
    @DexIgnore
    public static jx3 T;
    @DexIgnore
    public static IButtonConnectivity U;
    @DexIgnore
    public static MFDeviceService.b V;
    @DexIgnore
    public static /* final */ a W; // = new a((rd4) null);
    @DexIgnore
    public MutableLiveData<String> A; // = new MutableLiveData<>();
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C; // = true;
    @DexIgnore
    public /* final */ Handler D; // = new Handler();
    @DexIgnore
    public Runnable E;
    @DexIgnore
    public m42 F;
    @DexIgnore
    public Thread.UncaughtExceptionHandler G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public ServerError I;
    @DexIgnore
    public vl2 J;
    @DexIgnore
    public wl2 K;
    @DexIgnore
    public NetworkChangedReceiver L;
    @DexIgnore
    public /* final */ po2 M; // = new po2();
    @DexIgnore
    public ko2 N;
    @DexIgnore
    public SmsMmsReceiver O;
    @DexIgnore
    public LocalizationManager e;
    @DexIgnore
    public fn2 f;
    @DexIgnore
    public UserRepository g;
    @DexIgnore
    public SummariesRepository h;
    @DexIgnore
    public SleepSummariesRepository i;
    @DexIgnore
    public AlarmHelper j;
    @DexIgnore
    public GuestApiService k;
    @DexIgnore
    public i42 l;
    @DexIgnore
    public ApiServiceV2 m;
    @DexIgnore
    public yr3 n;
    @DexIgnore
    public k62 o;
    @DexIgnore
    public DeleteLogoutUserUseCase p;
    @DexIgnore
    public AnalyticsHelper q;
    @DexIgnore
    public ApplicationEventListener r;
    @DexIgnore
    public DeviceRepository s;
    @DexIgnore
    public yk2 t;
    @DexIgnore
    public ShakeFeedbackService u;
    @DexIgnore
    public DianaPresetRepository v;
    @DexIgnore
    public lu3 w;
    @DexIgnore
    public WatchLocalizationRepository x;
    @DexIgnore
    public hr3 y;
    @DexIgnore
    public WatchFaceRepository z;

    @DexIgnore
    public enum LIFECIRCLE {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(boolean z) {
            PortfolioApp.d(z);
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.U;
        }

        @DexIgnore
        public final PortfolioApp c() {
            PortfolioApp W = PortfolioApp.R;
            if (W != null) {
                return W;
            }
            wd4.d("instance");
            throw null;
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.P;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.S;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.Q;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final jx3 a() {
            return PortfolioApp.T;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0033 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0056 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0064 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x006d A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x008e A[Catch:{ Exception -> 0x0093 }] */
        public final void b(IButtonConnectivity iButtonConnectivity) {
            String str;
            String str2;
            IButtonConnectivity b;
            String str3;
            String str4;
            wd4.b(iButtonConnectivity, Constants.SERVICE);
            a(iButtonConnectivity);
            try {
                Access a = new SoLibraryLoader().a(c());
                String f = g62.x.f();
                String c = g62.x.c();
                if (a != null) {
                    str = a.getI();
                    if (str != null) {
                        if (a != null) {
                            str2 = a.getK();
                            if (str2 != null) {
                                CloudLogConfig cloudLogConfig = new CloudLogConfig(f, c, str, str2);
                                b = b();
                                if (b != null) {
                                    String q = g62.x.q();
                                    if (a != null) {
                                        str3 = a.getL();
                                        if (str3 != null) {
                                            if (a != null) {
                                                str4 = a.getM();
                                                if (str4 != null) {
                                                    b.init(q, str3, str4, (wd4.a((Object) "release", (Object) "release") ? c().l() : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN).getPrefix(), AppHelper.f.a().b(), AppHelper.f.a().a(), cloudLogConfig);
                                                    return;
                                                }
                                            }
                                            str4 = "";
                                            b.init(q, str3, str4, (wd4.a((Object) "release", (Object) "release") ? c().l() : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN).getPrefix(), AppHelper.f.a().b(), AppHelper.f.a().a(), cloudLogConfig);
                                            return;
                                        }
                                    }
                                    str3 = "";
                                    if (a != null) {
                                    }
                                    str4 = "";
                                    b.init(q, str3, str4, (wd4.a((Object) "release", (Object) "release") ? c().l() : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN).getPrefix(), AppHelper.f.a().b(), AppHelper.f.a().a(), cloudLogConfig);
                                    return;
                                }
                                wd4.a();
                                throw null;
                            }
                        }
                        str2 = "";
                        CloudLogConfig cloudLogConfig2 = new CloudLogConfig(f, c, str, str2);
                        b = b();
                        if (b != null) {
                        }
                    }
                }
                str = "";
                if (a != null) {
                }
                str2 = "";
                CloudLogConfig cloudLogConfig22 = new CloudLogConfig(f, c, str, str2);
                b = b();
                if (b != null) {
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = d();
                local.e(d, ".updateButtonService(), ex=" + e);
            }
        }

        @DexIgnore
        public final void c(Object obj) {
            wd4.b(obj, "o");
            try {
                jx3 a = a();
                if (a != null) {
                    a.c(obj);
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e) {
                String d = d();
                zw.a(0, d, "Exception when unregister bus events for object=" + obj + ",exception=" + e);
            }
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.U = iButtonConnectivity;
        }

        @DexIgnore
        public final void a(MFDeviceService.b bVar) {
            PortfolioApp.V = bVar;
        }

        @DexIgnore
        public final void a(Object obj) {
            wd4.b(obj, Constants.EVENT);
            jx3 a = a();
            if (a != null) {
                a.a(obj);
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final void b(MFDeviceService.b bVar) {
            wd4.b(bVar, Constants.SERVICE);
            a(bVar);
        }

        @DexIgnore
        public final void b(Object obj) {
            wd4.b(obj, "o");
            try {
                jx3 a = a();
                if (a != null) {
                    a.b(obj);
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e) {
                String d = d();
                zw.a(0, d, "Exception when register bus events for object=" + obj + ",exception=" + e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Thread {
        @DexIgnore
        public void run() {
            try {
                FirebaseInstanceId.m().a();
            } catch (IOException e) {
                FLogger.INSTANCE.getLocal().e(PortfolioApp.W.d(), e.toString());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<DeleteLogoutUserUseCase.d, DeleteLogoutUserUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;
        @DexIgnore
        public /* final */ /* synthetic */ ServerError b;

        @DexIgnore
        public c(PortfolioApp portfolioApp, ServerError serverError) {
            this.a = portfolioApp;
            this.b = serverError;
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.W.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.a, WelcomeActivity.class);
            intent.addFlags(268468224);
            ServerError serverError = this.b;
            if (serverError != null) {
                Integer code = serverError.getCode();
                if (code != null) {
                    int intValue = code.intValue();
                }
            }
            this.a.startActivity(intent);
            this.a.b((ServerError) null);
            this.a.b(false);
            en2.p.a().o();
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.W.d(), "logout unsuccessfully");
            this.a.b((ServerError) null);
            this.a.b(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp e;

        @DexIgnore
        public d(PortfolioApp portfolioApp) {
            this.e = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.e.z() || !this.e.C) {
                PortfolioApp.W.a(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.W.d(), "still foreground");
                return;
            }
            this.e.a(false);
            PortfolioApp.W.a(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.W.d(), "went background");
            this.e.E();
            vl2 a = this.e.J;
            if (a != null) {
                a.a("");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public e(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.W.d();
            local.e(d, "uncaughtException - ex=" + th);
            th.printStackTrace();
            wd4.a((Object) th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        wd4.a(stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        wd4.a((Object) className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        wd4.a((Object) simpleName, "ButtonService::class.java.simpleName");
                        if (StringsKt__StringsKt.a((CharSequence) className, (CharSequence) simpleName, false/*, 2, (Object) null*/)) {
                            FLogger.INSTANCE.getLocal().e(PortfolioApp.W.d(), "uncaughtException - stopLogService");
                            this.a.a(FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
                            break;
                        }
                        i++;
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.a.u().c(currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.W.d();
            local2.d(d2, "Inside .uncaughtException - currentTime = " + currentTimeMillis);
            Thread.UncaughtExceptionHandler b = this.a.G;
            if (b != null) {
                b.uncaughtException(thread, th);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        wd4.a((Object) simpleName, "PortfolioApp::class.java.simpleName");
        P = simpleName;
    }
    */

    @DexIgnore
    public static final IButtonConnectivity a0() {
        return U;
    }

    @DexIgnore
    public static final /* synthetic */ void d(boolean z2) {
    }

    @DexIgnore
    public final boolean A() {
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                int[] listActiveCommunicator = iButtonConnectivity.getListActiveCommunicator();
                List<Integer> a2 = Ints.a(Arrays.copyOf(listActiveCommunicator, listActiveCommunicator.length));
                return a2 != null && !a2.isEmpty() && a2.contains(Integer.valueOf(CommunicateMode.OTA.getValue()));
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final boolean B() {
        return wd4.a((Object) getPackageName(), (Object) q());
    }

    @DexIgnore
    public final boolean C() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + ZendeskConfig.SLASH + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(P, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(P, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        wd4.a((Object) string, "enabledNotificationListeners");
        return StringsKt__StringsKt.a((CharSequence) string, (CharSequence) str, false/*, 2, (Object) null*/);
    }

    @DexIgnore
    public final boolean D() {
        return cg4.b("release", "release", true);
    }

    @DexIgnore
    public final void E() {
        vl2 c2 = AnalyticsHelper.f.c("ota_session");
        vl2 c3 = AnalyticsHelper.f.c("sync_session");
        vl2 c4 = AnalyticsHelper.f.c("setup_device_session");
        if (c2 != null && c2.b()) {
            c2.a(AnalyticsHelper.f.a("ota_session_go_to_background"));
        } else if (c3 != null && c3.b()) {
            c3.a(AnalyticsHelper.f.a("sync_session_go_to_background"));
        } else if (c4 != null && c4.b()) {
            c4.a(AnalyticsHelper.f.a("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void F() {
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final ri4 G() {
        throw null;
        // return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new PortfolioApp$onActiveDeviceStealed$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void H() {
        Q();
        P();
        synchronized (PortfolioApp.class) {
            o42 o42 = new o42(this);
            y52.i c0 = y52.c0();
            c0.a(o42);
            m42 a2 = c0.a();
            wd4.a(a2, "DaggerApplicationCompone\u2026                 .build()");
            this.F = a2;
            m42 m42 = this.F;
            if (m42 != null) {
                m42.a(this);
                cb4 cb4 = com.fossil.blesdk.obfuscated.cb4.a;
            } else {
                wd4.d("applicationComponent");
                throw null;
            }
        }
        MFDeviceService.b bVar = V;
        if (bVar != null) {
            if (bVar != null) {
                bVar.a().w();
            } else {
                wd4.a();
                throw null;
            }
        }
        L();
        J();
        WeatherManager.n.a().f();
        WatchAppCommuteTimeManager.s.a().c();
        MusicControlComponent musicControlComponent = MusicControlComponent.o.a(this);
        DianaPresetRepository dianaPresetRepository = this.v;
        if (dianaPresetRepository != null) {
            musicControlComponent.a(dianaPresetRepository);
            cn2 cn2 = com.fossil.blesdk.obfuscated.cn2.d;
            fn2 fn2 = this.f;
            if (fn2 != null) {
                cn2.a(fn2);
            } else {
                wd4.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            wd4.d("mDianaPresetRepository");
            throw null;
        }
    }

    @DexIgnore
    public final long I() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(e());
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void J() {
        UserRepository userRepository = this.g;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = P;
                local.d(str, "registerContactObserver currentUser=" + currentUser);
                boolean e2 = os3.a.e(this);
                if (e2) {
                    yr3 yr3 = this.n;
                    if (yr3 != null) {
                        yr3.b();
                        ContentResolver contentResolver = getContentResolver();
                        Uri uri = ContactsContract.Contacts.CONTENT_URI;
                        yr3 yr32 = this.n;
                        if (yr32 != null) {
                            contentResolver.registerContentObserver(uri, true, yr32);
                            FLogger.INSTANCE.getLocal().d(P, "registerContactObserver success");
                            fn2 fn2 = this.f;
                            if (fn2 != null) {
                                fn2.n(true);
                            } else {
                                wd4.d("sharedPreferencesManager");
                                throw null;
                            }
                        } else {
                            wd4.d("mContactObserver");
                            throw null;
                        }
                    } else {
                        wd4.d("mContactObserver");
                        throw null;
                    }
                } else {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = P;
                    local2.d(str2, "registerContactObserver fail due to enough Permission =" + e2);
                }
            }
        } else {
            wd4.d("mUserRepository");
            throw null;
        }
    }

    @DexIgnore
    public final void K() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        SmsMmsReceiver smsMmsReceiver = this.O;
        if (smsMmsReceiver != null) {
            registerReceiver(smsMmsReceiver, intentFilter);
        } else {
            wd4.d("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    public final void L() {
        try {
            ko2 ko2 = this.N;
            if (ko2 != null) {
                registerReceiver(ko2, new IntentFilter("android.intent.action.PHONE_STATE"));
                K();
                return;
            }
            wd4.d("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final void M() {
        AlarmHelper alarmHelper = this.j;
        if (alarmHelper != null) {
            alarmHelper.a(this);
            AlarmHelper alarmHelper2 = this.j;
            if (alarmHelper2 != null) {
                alarmHelper2.c(this);
            } else {
                wd4.d("mAlarmHelper");
                throw null;
            }
        } else {
            wd4.d("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void N() {
        String e2 = e();
        try {
            FLogger.INSTANCE.getLocal().d(P, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(e2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.e(str, "Error inside " + P + ".resetDeviceSettingToDefault - e=" + e3);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023 A[SYNTHETIC, Splitter:B:10:0x0023] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043 A[Catch:{ Exception -> 0x00b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a9 A[Catch:{ Exception -> 0x00b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ad A[SYNTHETIC, Splitter:B:30:0x00ad] */
    public final void O() {
        String str;
        fn2 fn2;
        String str2;
        fn2 fn22;
        try {
            String n2 = n();
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                MisfitDeviceProfile deviceProfile = iButtonConnectivity.getDeviceProfile(this.A.a());
                if (deviceProfile != null) {
                    str = deviceProfile.getLocale();
                    fn2 = this.f;
                    if (fn2 == null) {
                        String h2 = fn2.h(n2);
                        IButtonConnectivity iButtonConnectivity2 = U;
                        if (iButtonConnectivity2 != null) {
                            MisfitDeviceProfile deviceProfile2 = iButtonConnectivity2.getDeviceProfile(this.A.a());
                            if (deviceProfile2 != null) {
                                str2 = deviceProfile2.getLocaleVersion();
                                fn22 = this.f;
                                if (fn22 == null) {
                                    String p2 = fn22.p();
                                    FLogger.INSTANCE.getLocal().d(P, "phoneLocale " + n2 + " - watchLocale: " + str + " - standardLocale: " + h2 + "\nwatchLocaleVersion: " + str2 + " - standardLocaleVersion: " + p2);
                                    if (!wd4.a((Object) h2, (Object) str)) {
                                        c(n2);
                                        return;
                                    } else if (!wd4.a((Object) str2, (Object) p2)) {
                                        c(n2);
                                        return;
                                    } else {
                                        FLogger.INSTANCE.getLocal().d(P, "don't need to set to watch");
                                        return;
                                    }
                                } else {
                                    wd4.d("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        }
                        str2 = null;
                        fn22 = this.f;
                        if (fn22 == null) {
                        }
                    } else {
                        wd4.d("sharedPreferencesManager");
                        throw null;
                    }
                }
            }
            str = null;
            fn2 = this.f;
            if (fn2 == null) {
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void P() {
        FLogger.INSTANCE.getLocal().d(P, "unregisterContactObserver");
        try {
            yr3 yr3 = this.n;
            if (yr3 != null) {
                yr3.c();
                ContentResolver contentResolver = getContentResolver();
                yr3 yr32 = this.n;
                if (yr32 != null) {
                    contentResolver.unregisterContentObserver(yr32);
                    fn2 fn2 = this.f;
                    if (fn2 != null) {
                        fn2.n(false);
                    } else {
                        wd4.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    wd4.d("mContactObserver");
                    throw null;
                }
            } else {
                wd4.d("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    public final void Q() {
        try {
            ko2 ko2 = this.N;
            if (ko2 != null) {
                unregisterReceiver(ko2);
                SmsMmsReceiver smsMmsReceiver = this.O;
                if (smsMmsReceiver != null) {
                    unregisterReceiver(smsMmsReceiver);
                } else {
                    wd4.d("mMessageReceiver");
                    throw null;
                }
            } else {
                wd4.d("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final ri4 R() {
        throw null;
        // return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new PortfolioApp$updateActiveDeviceInfoLog$Anon1((kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void S() {
        AppLogInfo b2 = AppHelper.f.a().b();
        FLogger.INSTANCE.updateAppLogInfo(b2);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateAppLogInfo(b2);
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.e(str, ".updateAppLogInfo(), error=" + e2);
        }
    }

    @DexIgnore
    public final void T() {
        UserRepository userRepository = this.g;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = P;
                local.d(str, "updateFabricUserInformation currentUser=" + currentUser);
                zw.w().k.c(currentUser.getUserId());
                zw.w().k.a("UserId", currentUser.getUserId());
                if (S && !TextUtils.isEmpty(currentUser.getEmail())) {
                    zw.w().k.b(currentUser.getEmail());
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void attachBaseContext(Context context) {
        wd4.b(context, "base");
        super.attachBaseContext(context);
        throw null;
        // gd.d(this);
    }

    @DexIgnore
    public final String e() {
        fn2 fn2 = this.f;
        if (fn2 != null) {
            String b2 = fn2.b();
            return b2 != null ? b2 : "";
        }
        wd4.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> f() {
        fn2 fn2 = this.f;
        if (fn2 != null) {
            String b2 = fn2.b();
            if (!wd4.a((Object) b2, (Object) this.A.a())) {
                throw null;
                // this.A.a(b2);
            }
            return this.A;
        }
        wd4.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final m42 g() {
        m42 m42 = this.F;
        if (m42 != null) {
            return m42;
        }
        wd4.d("applicationComponent");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c  */
    public final String h() {
        List<Object> list = null;
        Object[] array;
        boolean z2;
        if (!S) {
            return "4.1.3";
        }
        List<String> split = new Regex(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.1.3", 0);
        if (!split.isEmpty()) {
            ListIterator<String> listIterator = split.listIterator(split.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    break;
                }
                if (listIterator.previous().length() == 0) {
                    z2 = true;
                    // continue;
                } else {
                    z2 = false;
                    // continue;
                }
                if (!z2) {
                    list = wb4.c(split, listIterator.nextIndex() + 1);
                    break;
                }
            }
            array = list.toArray(new String[0]);
            if (array == null) {
                String[] strArr = (String[]) array;
                if (!(strArr.length == 0)) {
                    return strArr[0];
                }
                return "4.1.3";
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        list = ob4.a();
        array = list.toArray(new String[0]);
        if (array == null) {
        }
        throw null;
    }

    @DexIgnore
    public final FossilBrand i() {
        FossilBrand fromInt = FossilBrand.fromInt(Integer.parseInt(g62.x.b()));
        wd4.a(fromInt, "FossilBrand.fromInt(Inte\u2026PortfolioConfig.brandId))");
        return fromInt;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007e  */
    public final UserProfile j() {
        int i2 = 0;
        int i3;
        double d2;
        int i4;
        int i5;
        int i6;
        int i7;
        UserBiometricData.BiometricWearingPosition biometricWearingPosition;
        int i8;
        int i9;
        List<Object> list = null;
        Object[] array;
        boolean z2;
        UserRepository userRepository = this.g;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser == null) {
                return null;
            }
            String birthday = currentUser.getBirthday();
            int i10 = 0;
            if (TextUtils.isEmpty(birthday)) {
                i2 = 0;
            } else if (birthday != null) {
                List<String> split = new Regex(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(birthday, 0);
                if (!split.isEmpty()) {
                    ListIterator<String> listIterator = split.listIterator(split.size());
                    while (true) {
                        if (!listIterator.hasPrevious()) {
                            break;
                        }
                        if (listIterator.previous().length() == 0) {
                            z2 = true;
                            // continue;
                        } else {
                            z2 = false;
                            // continue;
                        }
                        if (!z2) {
                            list = wb4.c(split, listIterator.nextIndex() + 1);
                            break;
                        }
                    }
                    array = list.toArray(new String[0]);
                    if (array == null) {
                        @SuppressLint("WrongConstant") int i11 = Calendar.getInstance().get(1);
                        Integer valueOf = Integer.valueOf(((String[]) array)[0]);
                        wd4.a(valueOf, "Integer.valueOf(s[0])");
                        i2 = i11 - valueOf.intValue();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                list = ob4.a();
                array = list.toArray(new String[0]);
                if (array == null) {
                }
            } else {
                wd4.a();
                throw null;
            }
            float e2 = qk2.e((float) (currentUser.getWeightInGrams() > 0 ? currentUser.getWeightInGrams() : 68039));
            float d3 = qk2.d((float) (currentUser.getHeightInCentimeters() > 0 ? currentUser.getHeightInCentimeters() : 170));
            SummariesRepository summariesRepository = this.h;
            if (summariesRepository != null) {
                ActivitySettings currentActivitySettings = summariesRepository.getCurrentActivitySettings();
                int component1 = currentActivitySettings.component1();
                int component2 = currentActivitySettings.component2();
                int component3 = currentActivitySettings.component3();
                SummariesRepository summariesRepository2 = this.h;
                if (summariesRepository2 != null) {
                    Calendar instance = Calendar.getInstance();
                    wd4.a(instance, "Calendar.getInstance()");
                    ActivitySummary summary = summariesRepository2.getSummary(instance);
                    SleepSummariesRepository sleepSummariesRepository = this.i;
                    if (sleepSummariesRepository != null) {
                        Calendar instance2 = Calendar.getInstance();
                        wd4.a(instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        wd4.a(time, "Calendar.getInstance().time");
                        MFSleepDay sleepSummaryFromDb = sleepSummariesRepository.getSleepSummaryFromDb(time);
                        yk2 yk2 = this.t;
                        if (yk2 != null) {
                            float a2 = yk2.a(new Date(), true);
                            double d4 = 0.0d;
                            if (summary != null) {
                                i3 = summary.getActiveTime();
                                d2 = summary.getCalories();
                                d4 = ((double) 100) * summary.getDistance();
                            } else {
                                d2 = 0.0d;
                                i3 = 0;
                            }
                            if (sleepSummaryFromDb != null) {
                                int sleepMinutes = sleepSummaryFromDb.getSleepMinutes() + 0;
                                SleepDistribution sleepStateDistInMinute = sleepSummaryFromDb.getSleepStateDistInMinute();
                                if (sleepStateDistInMinute != null) {
                                    int component12 = sleepStateDistInMinute.component1();
                                    i8 = sleepStateDistInMinute.component2() + 0;
                                    i9 = 0 + sleepStateDistInMinute.component3();
                                    i10 = component12 + 0;
                                } else {
                                    i9 = 0;
                                    i8 = 0;
                                }
                                i7 = sleepMinutes;
                                i6 = i10;
                                i4 = i9;
                                i5 = i8;
                            } else {
                                i7 = 0;
                                i6 = 0;
                                i5 = 0;
                                i4 = 0;
                            }
                            UserBiometricData.BiometricGender biometricGender = UserBiometricData.BiometricGender.UNSPECIFIED;
                            if (currentUser.getGender() == Gender.MALE) {
                                biometricGender = UserBiometricData.BiometricGender.MALE;
                            } else if (currentUser.getGender() == Gender.FEMALE) {
                                biometricGender = UserBiometricData.BiometricGender.FEMALE;
                            }
                            fn2 fn2 = this.f;
                            if (fn2 != null) {
                                String k2 = fn2.k();
                                if (k2 != null) {
                                    int hashCode = k2.hashCode();
                                    if (hashCode != -1867896629) {
                                        if (hashCode != 647131558) {
                                            if (hashCode == 660843126 && k2.equals("left wrist")) {
                                                biometricWearingPosition = UserBiometricData.BiometricWearingPosition.LEFT_WRIST;
                                                return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, rj2.a(currentUser), i7, i6, i5, i4, null, false, -1, -1, System.currentTimeMillis());
                                            }
                                        } else if (k2.equals("unspecified wrist")) {
                                            biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED_WRIST;
                                            return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, rj2.a(currentUser), i7, i6, i5, i4, null, false, -1, -1, System.currentTimeMillis());
                                        }
                                    } else if (k2.equals("right wrist")) {
                                        biometricWearingPosition = UserBiometricData.BiometricWearingPosition.RIGHT_WRIST;
                                        return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, rj2.a(currentUser), i7, i6, i5, i4, null, false, -1, -1, System.currentTimeMillis());
                                    }
                                }
                                biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED;
                                return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, rj2.a(currentUser), i7, i6, i5, i4, null, false, -1, -1, System.currentTimeMillis());
                            }
                            wd4.d("sharedPreferencesManager");
                            throw null;
                        }
                        wd4.d("mFitnessHelper");
                        throw null;
                    }
                    wd4.d("mSleepSummariesRepository");
                    throw null;
                }
                wd4.d("mSummariesRepository");
                throw null;
            }
            wd4.d("mSummariesRepository");
            throw null;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final Date k() {
        MFUser b2 = en2.p.a().n().b();
        if (b2 != null) {
            String createdAt = b2.getCreatedAt();
            if (!TextUtils.isEmpty(createdAt)) {
                Date a2 = nl2.a(sk2.d(createdAt));
                wd4.a(a2, "TimeHelper.getStartOfDay(registeredDate)");
                return a2;
            }
        }
        FLogger.INSTANCE.getLocal().d(P, "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)");
        return new Date(1, 1, 1);
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND l() {
        throw null;
        // switch (e62.a[i().ordinal()]) {
        //     case 1:
        //         return FossilDeviceSerialPatternUtil.BRAND.CHAPS;
        //     case 2:
        //         return FossilDeviceSerialPatternUtil.BRAND.DIESEL;
        //     case 3:
        //         return FossilDeviceSerialPatternUtil.BRAND.EA;
        //     case 4:
        //         return FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE;
        //     case 5:
        //         return FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS;
        //     case 6:
        //         return FossilDeviceSerialPatternUtil.BRAND.SKAGEN;
        //     case 7:
        //         return FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE;
        //     case 8:
        //         return FossilDeviceSerialPatternUtil.BRAND.RELIC;
        //     case 9:
        //         return FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS;
        //     case 10:
        //         return FossilDeviceSerialPatternUtil.BRAND.FOSSIL;
        //     case 11:
        //         return FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL;
        //     case 12:
        //         return FossilDeviceSerialPatternUtil.BRAND.CITIZEN;
        //     default:
        //         return FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
        // }
    }

    @DexIgnore
    public final String m() {
        Locale locale = Locale.getDefault();
        wd4.a(locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (wd4.a((Object) language, (Object) "iw")) {
            language = "he";
        }
        if (wd4.a((Object) language, (Object) "in")) {
            language = "id";
        }
        if (wd4.a((Object) language, (Object) "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            be4 be4 = com.fossil.blesdk.obfuscated.be4.a;
            Locale locale2 = Locale.US;
            wd4.a(locale2, "Locale.US");
            Object[] objArr = {language, country};
            language = String.format(locale2, "%s-%s", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) language, "java.lang.String.format(locale, format, *args)");
        }
        wd4.a((Object) language, "localeString");
        return language;
    }

    @DexIgnore
    public final String n() {
        Resources resources = getResources();
        wd4.a(resources, "resources");
        String languageTag = o7.a(resources.getConfiguration()).a(0).toLanguageTag();
        wd4.a((Object) languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final ApiServiceV2 o() {
        ApiServiceV2 apiServiceV2 = this.m;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wd4.d("mApiService");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        wd4.b(activity, Constants.ACTIVITY);
        LIFECIRCLE lifecircle = LIFECIRCLE.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        wd4.b(activity, Constants.ACTIVITY);
        LIFECIRCLE lifecircle = LIFECIRCLE.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        wd4.b(activity, Constants.ACTIVITY);
        LIFECIRCLE lifecircle = LIFECIRCLE.PAUSE;
        if (S) {
            ShakeFeedbackService shakeFeedbackService = this.u;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.e();
            } else {
                wd4.d("mShakeFeedbackService");
                throw null;
            }
        }
        this.C = true;
        Runnable runnable = this.E;
        if (runnable != null) {
            this.D.removeCallbacks(runnable);
        }
        this.D.postDelayed(new d(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        wd4.b(activity, Constants.ACTIVITY);
        if (S) {
            ShakeFeedbackService shakeFeedbackService = this.u;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.a(activity);
            } else {
                wd4.d("mShakeFeedbackService");
                throw null;
            }
        }
        LIFECIRCLE lifecircle = LIFECIRCLE.RESUME;
        this.C = false;
        boolean z2 = !this.B;
        this.B = true;
        Runnable runnable = this.E;
        if (runnable != null) {
            this.D.removeCallbacks(runnable);
        }
        if (z2) {
            Q = true;
            FLogger.INSTANCE.getLocal().d(P, "from background");
            vl2 vl2 = this.J;
            if (vl2 != null) {
                vl2.d();
            }
            fn2 fn2 = this.f;
            if (fn2 != null) {
                fn2.q(true);
            } else {
                wd4.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            Q = false;
            FLogger.INSTANCE.getLocal().d(P, "still foreground");
        }
        if (this.H) {
            a(this.I);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        wd4.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        wd4.b(activity, Constants.ACTIVITY);
        LIFECIRCLE lifecircle = LIFECIRCLE.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        wd4.b(activity, Constants.ACTIVITY);
        LIFECIRCLE lifecircle = LIFECIRCLE.STOP;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        R = this;
        S = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (B()) {
            o42 o42 = new o42(this);
            y52.i c0 = y52.c0();
            c0.a(o42);
            m42 a2 = c0.a();
            wd4.a(a2, "DaggerApplicationCompone\u2026\n                .build()");
            this.F = a2;
            m42 m42 = this.F;
            if (m42 != null) {
                m42.a(this);
                System.loadLibrary("FitnessAlgorithm");
                LifecycleOwner g2 = fc.g();
                wd4.a(g2, "ProcessLifecycleOwner.get()");
                Lifecycle lifecycle = g2.getLifecycle();
                ApplicationEventListener applicationEventListener = this.r;
                if (applicationEventListener != null) {
                    lifecycle.a(applicationEventListener);
                    MicroAppEventLogger.initialize(this);
                    x();
                    throw null;
                    // ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new PortfolioApp$onCreate$Anon1(this, (kc4) null), 3, (Object) null);
                    // FLogger.INSTANCE.getLocal().d(P, "On app create");
                    // T = new nj2(qx3.a);
                    // MutableLiveData<String> mutableLiveData = this.A;
                    // fn2 fn2 = this.f;
                    // if (fn2 != null) {
                    //     mutableLiveData.b(fn2.b());
                    //     ViewPump.b bVar = ViewPump.g;
                    //     ViewPump.a a3 = bVar.a();
                    //     a3.a(new CalligraphyInterceptor(new CalligraphyConfig.Builder().setFontAttrId(com.fossil.wearables.fossil.R.attr.fontPath).build()));
                    //     bVar.a(a3.a());
                    //     registerActivityLifecycleCallbacks(this);
                    //     MusicControlComponent musicControlComponent = MusicControlComponent.o.a(this);
                    //     DianaPresetRepository dianaPresetRepository = this.v;
                    //     if (dianaPresetRepository != null) {
                    //         musicControlComponent.a(dianaPresetRepository);
                    //         String str = "4.1.3";
                    //         if (S) {
                    //             Object[] array = new Regex(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(str, 2).toArray(new String[0]);
                    //             if (array != null) {
                    //                 str = ((String[]) array)[0];
                    //             } else {
                    //                 throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    //             }
                    //         }
                    //         GuestApiService guestApiService = this.k;
                    //         if (guestApiService != null) {
                    //             this.e = new LocalizationManager(this, str, guestApiService);
                    //             a62.a();
                    //             registerReceiver(this.e, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                    //             LocalizationManager localizationManager = this.e;
                    //             if (localizationManager != null) {
                    //                 registerActivityLifecycleCallbacks(localizationManager.a());
                    //                 LocalizationManager localizationManager2 = this.e;
                    //                 if (localizationManager2 != null) {
                    //                     String simpleName = SplashScreenActivity.class.getSimpleName();
                    //                     wd4.a((Object) simpleName, "SplashScreenActivity::class.java.simpleName");
                    //                     localizationManager2.a(simpleName);
                    //                     LocalizationManager localizationManager3 = this.e;
                    //                     if (localizationManager3 != null) {
                    //                         localizationManager3.i();
                    //                         ResolutionHelper.INSTANCE.initDeviceDensity(this);
                    //                         cn2 cn2 = com.fossil.blesdk.obfuscated.cn2.d;
                    //                         fn2 fn22 = this.f;
                    //                         if (fn22 != null) {
                    //                             cn2.a(fn22);
                    //                             if (S) {
                    //                                 StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().detectCustomSlowCalls().penaltyLog().build());
                    //                                 StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects().detectActivityLeaks().detectLeakedRegistrationObjects().penaltyLog().build());
                    //                             }
                    //                             try {
                    //                                 if (Build.VERSION.SDK_INT >= 24) {
                    //                                     this.L = new NetworkChangedReceiver();
                    //                                     registerReceiver(this.L, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                    //                                 }
                    //                             } catch (Exception e2) {
                    //                                 ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    //                                 String str2 = P;
                    //                                 local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                    //                             }
                    //                             J();
                    //                             IntentFilter intentFilter = new IntentFilter();
                    //                             intentFilter.addAction("android.intent.action.TIME_TICK");
                    //                             intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                    //                             registerReceiver(this.M, intentFilter);
                    //                             L();
                    //                             this.G = Thread.getDefaultUncaughtExceptionHandler();
                    //                             Thread.setDefaultUncaughtExceptionHandler(new e(this));
                    //                             StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    //                             StrictMode.setVmPolicy(builder.build());
                    //                             builder.detectFileUriExposure();
                    //                             AlarmHelper alarmHelper = this.j;
                    //                             if (alarmHelper != null) {
                    //                                 alarmHelper.d();
                    //                                 AlarmHelper alarmHelper2 = this.j;
                    //                                 if (alarmHelper2 != null) {
                    //                                     alarmHelper2.c(this);
                    //                                     this.J = AnalyticsHelper.f.b("app_appearance");
                    //                                     AnalyticsHelper analyticsHelper = this.q;
                    //                                     if (analyticsHelper != null) {
                    //                                         analyticsHelper.b("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                    //                                         throw null;
                    //                                         // qs3.a.a(qs3.a, this, MFDeviceService.class, (String) null, 4, (Object) null);
                    //                                         // qs3.a.a(qs3.a, this, ButtonService.class, (String) null, 4, (Object) null);
                    //                                         b();
                    //                                         return;
                    //                                     }
                    //                                     wd4.d("mAnalyticsHelper");
                    //                                     throw null;
                    //                                 }
                    //                                 wd4.d("mAlarmHelper");
                    //                                 throw null;
                    //                             }
                    //                             wd4.d("mAlarmHelper");
                    //                             throw null;
                    //                         }
                    //                         wd4.d("sharedPreferencesManager");
                    //                         throw null;
                    //                     }
                    //                     wd4.a();
                    //                     throw null;
                    //                 }
                    //                 wd4.a();
                    //                 throw null;
                    //             }
                    //             wd4.a();
                    //             throw null;
                    //         }
                    //         wd4.d("mGuestApiService");
                    //         throw null;
                    //     }
                    //     wd4.d("mDianaPresetRepository");
                    //     throw null;
                    // }
                    // wd4.d("sharedPreferencesManager");
                    // throw null;
                }
                wd4.d("mAppEventManager");
                throw null;
            }
            wd4.d("applicationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onLowMemory() {
        super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "Inside " + P + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    public void onTerminate() {
        super.onTerminate();
        FLogger.INSTANCE.getLocal().d(P, "---Inside .onTerminate of Application");
        Q();
        unregisterReceiver(this.e);
        P();
        LocalizationManager localizationManager = this.e;
        unregisterActivityLifecycleCallbacks(localizationManager != null ? localizationManager.a() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(P, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.L);
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "Inside " + P + ".onTrimMemory");
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final LocalizationManager p() {
        return this.e;
    }

    @DexIgnore
    public final String q() {
        int myPid = Process.myPid();
        Object systemService = getSystemService(Context.ACTIVITY_SERVICE);
        if (systemService != null) {
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) systemService).getRunningAppProcesses()) {
                if (next.pid == myPid) {
                    return next.processName;
                }
            }
            return null;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.ActivityManager");
    }

    @DexIgnore
    public final hr3 r() {
        hr3 hr3 = this.y;
        if (hr3 != null) {
            return hr3;
        }
        wd4.d("mUaRestfulApi");
        throw null;
    }

    @DexIgnore
    public final UserRepository s() {
        UserRepository userRepository = this.g;
        if (userRepository != null) {
            return userRepository;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository t() {
        WatchLocalizationRepository watchLocalizationRepository = this.x;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        wd4.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final fn2 u() {
        fn2 fn2 = this.f;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final WatchFaceRepository v() {
        WatchFaceRepository watchFaceRepository = this.z;
        if (watchFaceRepository != null) {
            return watchFaceRepository;
        }
        wd4.d("watchFaceRepository");
        throw null;
    }

    @DexIgnore
    public final boolean w() {
        Object systemService = getSystemService(Context.CONNECTIVITY_SERVICE);
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new TypeCastException("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    @DexIgnore
    public final void x() {
        FLogger.INSTANCE.getLocal().d(P, "initFabric");
        r44.c cVar = new r44.c(this);
        cVar.a(new zw());
        cVar.a(true);
        r44.d(cVar.a());
        zw.w().k.a("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        zw.w().k.a("Locale", Locale.getDefault().toString());
        T();
    }

    @DexIgnore
    public final void y() {
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(e());
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean z() {
        return this.B;
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final void c(String str, String str2) {
        wd4.b(str, "serial");
        if (TextUtils.isEmpty(str) || DeviceHelper.o.e(str)) {
            String e2 = e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, "Inside " + P + ".setActiveDeviceSerial - current=" + e2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            fn2 fn2 = this.f;
            if (fn2 != null) {
                fn2.o(str);
                if (e2 != str) {
                    throw null;
                    // this.A.a(str);
                }
                R();
                try {
                    IButtonConnectivity iButtonConnectivity = U;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        FossilNotificationBar.c.a(this);
                        return;
                    }
                    wd4.a();
                    throw null;
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else {
                wd4.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    public final String d() {
        String str;
        String e2 = e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + e2);
        if (TextUtils.isEmpty(e2)) {
            PortfolioApp portfolioApp = R;
            if (portfolioApp != null) {
                String a2 = tm2.a(portfolioApp, (int) com.fossil.wearables.fossil.R.string.Onboarding_WithoutDevice_Dashboard_CTA__PairWatch);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return a2;
            }
            wd4.d("instance");
            throw null;
        }
        String a3 = tm2.a(this, (int) com.fossil.wearables.fossil.R.string.Profile_MyWatch_DianaProfile_Text__Disconnected);
        DeviceRepository deviceRepository = this.s;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(e2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = P;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(e2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = U;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(e2) == 2) {
                            PortfolioApp portfolioApp2 = R;
                            if (portfolioApp2 == null) {
                                wd4.d("instance");
                                throw null;
                            } else if (portfolioApp2.h(e2)) {
                                PortfolioApp portfolioApp3 = R;
                                if (portfolioApp3 != null) {
                                    a3 = tm2.a(portfolioApp3, (int) com.fossil.wearables.fossil.R.string.DesignPatterns_AndroidQuickAccessPanel_SyncInProgressExpanded_Text__SyncInProgress);
                                } else {
                                    wd4.d("instance");
                                    throw null;
                                }
                            } else {
                                fn2 fn2 = this.f;
                                if (fn2 != null) {
                                    long g2 = fn2.g(e2);
                                    if (g2 == 0) {
                                        str = "";
                                    } else if (System.currentTimeMillis() - g2 < 60000) {
                                        be4 be4 = com.fossil.blesdk.obfuscated.be4.a;
                                        PortfolioApp portfolioApp4 = R;
                                        if (portfolioApp4 != null) {
                                            String a4 = tm2.a(portfolioApp4, (int) com.fossil.wearables.fossil.R.string.Profile_MyWatch_WatchSettings_Label__NumbermAgo);
                                            wd4.a((Object) a4, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                                            Object[] objArr = {1};
                                            str = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                                            wd4.a((Object) str, "java.lang.String.format(format, *args)");
                                        } else {
                                            wd4.d("instance");
                                            throw null;
                                        }
                                    } else {
                                        str = sk2.a(g2);
                                    }
                                    be4 be42 = be4.a;
                                    PortfolioApp portfolioApp5 = R;
                                    if (portfolioApp5 != null) {
                                        String a5 = tm2.a(portfolioApp5, (int) com.fossil.wearables.fossil.R.string.Profile_MyWatch_HybridProfile_Text__LastSyncedDayTime);
                                        wd4.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
                                        Object[] objArr2 = {str};
                                        String format = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                                        wd4.a((Object) format, "java.lang.String.format(format, *args)");
                                        a3 = format;
                                    } else {
                                        wd4.d("instance");
                                        throw null;
                                    }
                                } else {
                                    wd4.d("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        }
                        return deviceNameBySerial + " : " + a3;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = P;
                local3.d(str4, "generateDeviceNotificationContent e=" + e3);
            }
        } else {
            wd4.d("mDeviceRepository");
            throw null;
        }
        throw null;
    }

    @DexIgnore
    public final boolean e(String str) {
        wd4.b(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                j(str);
                return forceSwitchDeviceWithoutErase;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final int g(String str) {
        wd4.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception unused) {
            return 0;
        }
    }

    @DexIgnore
    public final void i(String str) {
        wd4.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexIgnore
    public final void n(String str) {
        String str2;
        int i2;
        if (str == null) {
            i2 = 1024;
            str2 = "";
        } else {
            try {
                i2 = ol2.a(str);
                str2 = str;
            } catch (Exception e2) {
                e2.printStackTrace();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = P;
                local.e(str3, "Inside " + P + ".setAutoSecondTimezone - ex=" + e2);
                return;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = P;
        local2.d(str4, "Inside " + P + ".setAutoSecondTimezone - offsetMinutes=" + i2 + ", mSecondTimezoneId=" + str2);
        IButtonConnectivity iButtonConnectivity = U;
        if (iButtonConnectivity != null) {
            if (str == null) {
                str = "";
            }
            try {
                iButtonConnectivity.deviceSetAutoSecondTimezone(str);
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void o(String str) {
        wd4.b(str, "serial");
        UserProfile j2 = j();
        if (j2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "setImplicitDeviceConfig - currentUserProfile=" + j2);
            try {
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.setImplicitDeviceConfig(j2, str);
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = P;
                local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
                e2.printStackTrace();
            }
        } else {
            FLogger.INSTANCE.getLocal().e(P, "setImplicitDeviceConfig - currentUserProfile is NULL");
        }
    }

    @DexIgnore
    public final void p(String str) {
        wd4.b(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception unused) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final void r(String str) {
        wd4.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (cg4.b(str, e(), true)) {
                    fn2 fn2 = this.f;
                    if (fn2 != null) {
                        fn2.o("");
                        throw null;
                        // this.A.a("");
                    } else {
                        wd4.d("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    fn2 fn22 = this.f;
                    if (fn22 != null) {
                        fn22.a(str, 0, false);
                    } else {
                        wd4.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = P;
                local.e(str2, "Inside " + P + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final void b(ServerError serverError) {
        this.I = serverError;
    }

    @DexIgnore
    public final long b(String str, boolean z2) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.B = z2;
    }

    @DexIgnore
    public final void a(wl2 wl2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "Active view tracer old: " + this.K + " \n new: " + wl2);
        if (wl2 != null && wd4.a((Object) wl2.a(), (Object) "view_appearance")) {
            wl2 wl22 = this.K;
            if (!(wl22 == null || wl22 == null || wl22.a(wl2))) {
                vl2 vl2 = this.J;
                if (vl2 != null) {
                    tl2 a2 = AnalyticsHelper.f.a("app_appearance_view_navigate");
                    wl2 wl23 = this.K;
                    if (wl23 != null) {
                        String e2 = wl23.e();
                        if (e2 != null) {
                            a2.a("prev_view", e2);
                            String e3 = wl2.e();
                            if (e3 != null) {
                                a2.a("next_view", e3);
                                vl2.a(a2);
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
            this.K = wl2;
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        wd4.b(str2, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + str);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final int f(String str) {
        wd4.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            wd4.a();
            throw null;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2, int i2) {
        wd4.b(str, "serial");
        wd4.b(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final long k(String str) {
        wd4.b(str, "serial");
        return a(str, 1, 1, true);
    }

    @DexIgnore
    public final boolean q(String str) {
        wd4.b(str, "newActiveDeviceSerial");
        UserProfile j2 = j();
        if (j2 == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, "Error inside " + P + ".switchActiveDevice - user is null");
            return false;
        }
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                boolean switchActiveDevice = iButtonConnectivity.switchActiveDevice(str, j2);
                if (!switchActiveDevice) {
                    return switchActiveDevice;
                }
                if (!(str.length() == 0)) {
                    return switchActiveDevice;
                }
                j(str);
                return switchActiveDevice;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        wd4.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void m(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final void c(String str) {
        String str2;
        String str3 = "localization_" + str + '.';
        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
        int length = listFiles.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                str2 = "";
                break;
            }
            File file = listFiles[i2];
            wd4.a(file, "file");
            String path = file.getPath();
            wd4.a((Object) path, "file.path");
            if (StringsKt__StringsKt.a((CharSequence) path, (CharSequence) str3, false/*, 2, (Object) null*/)) {
                str2 = file.getPath();
                wd4.a((Object) str2, "file.path");
                break;
            }
            i2++;
        }
        FLogger.INSTANCE.getLocal().d(P, "setLocalization - neededLocalizationFilePath: " + str2);
        if (TextUtils.isEmpty(str2)) {
            throw null;
            // ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new PortfolioApp$executeSetLocalization$Anon1(this, (kc4) null), 3, (Object) null);
            // return;
        }
        IButtonConnectivity iButtonConnectivity = U;
        if (iButtonConnectivity != null) {
            try {
                iButtonConnectivity.setLocalizationData(new LocalizationData(str2, (String) null/*, 2, (rd4) null*/));
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }
    }

    @DexIgnore
    public final void b(String str, List<? extends BLEMapping> list) {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, (List<BLEMapping>) list);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean h(String str) {
        wd4.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            wd4.a();
            throw null;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public final void l(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = P;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = P;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    // @DexReplace
    @DexIgnore
    public final long b(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(wj2 wj2, boolean z2, int i2) {
        wd4.b(wj2, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", e());
        sc.a(this).a(intent);
        String e2 = e();
        if (h(e2)) {
            FLogger.INSTANCE.getLocal().d(P, "Device is syncing, Skip this sync.");
        } else {
            wj2.b(e2).a(new ar2(i2, e2, z2), (CoroutineUseCase.e) null);
        }
    }

    @DexIgnore
    public final void c(boolean z2) {
        try {
            String e2 = e();
            if (!z2) {
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.updatePercentageGoalProgress(e2, false, j());
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                IButtonConnectivity iButtonConnectivity2 = U;
                if (iButtonConnectivity2 != null) {
                    iButtonConnectivity2.updatePercentageGoalProgress(e2, true, j());
                } else {
                    wd4.a();
                    throw null;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    @DexIgnore
    public final void b() {
        yi.a aVar = new yi.a();
        lu3 lu3 = this.w;
        if (lu3 != null) {
            aVar.a(lu3);
            yi a2 = aVar.a();
            wd4.a(a2, "Configuration.Builder()\n\u2026\n                .build()");
            kj.a(this, a2);
            PushPendingDataWorker.y.a();
            return;
        }
        wd4.d("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public final void d(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = P;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final long c(String str, List<? extends MicroAppMapping> list) {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    for (String append : microAppMapping.getDeclarationFiles()) {
                        sb.append("||");
                        sb.append(append);
                    }
                    FLogger.INSTANCE.getLocal().d(P, sb.toString());
                    FLogger.INSTANCE.getLocal().d(P, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping((List<MicroAppMapping>) list));
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void d(String str) {
        wd4.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "Inside " + P + ".forceReconnectDevice");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceForceReconnect(str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "Error inside " + P + ".deviceReconnect - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z2, int i2) {
        wd4.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final Object a(kc4<? super cb4> kc4) {
        MFUser b2 = en2.p.a().n().b();
        if (b2 == null) {
            return cb4.a;
        }
        Installation installation = new Installation();
        installation.setAppMarketingVersion("4.1.3");
        installation.setAppBuildNumber(24121);
        installation.setModel(Build.MODEL);
        installation.setOsVersion(Build.VERSION.RELEASE);
        AppHelper.Companion companion = AppHelper.f;
        String userId = b2.getUserId();
        wd4.a((Object) userId, "currentUser.userId");
        installation.setInstallationId(companion.a(userId));
        installation.setAppPermissions(os3.a.a());
        fn2 fn2 = this.f;
        if (fn2 != null) {
            installation.setDeviceToken(fn2.f());
            TimeZone timeZone = TimeZone.getDefault();
            wd4.a(timeZone, "TimeZone.getDefault()");
            String id = timeZone.getID();
            wd4.a((Object) id, "zone");
            if (StringsKt__StringsKt.a((CharSequence) id, '/', 0, false/*, 6, (Object) null*/) > 0) {
                installation.setTimeZone(id);
            }
            try {
                String packageName = getPackageName();
                PackageManager packageManager = getPackageManager();
                String str = packageManager.getPackageInfo(packageName, 0).versionName;
                String obj = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
                if (!TextUtils.isEmpty(obj)) {
                    installation.setAppName(obj);
                }
                if (!TextUtils.isEmpty(str)) {
                    installation.setAppVersion(str);
                }
            } catch (Exception unused) {
                FLogger.INSTANCE.getLocal().e(P, "Cannot load package info; will not be saved to installation");
            }
            installation.setDeviceType("android");
            a(installation);
            throw null;
            // return kg4.a(zh4.b(), new PortfolioApp$saveInstallation$Anon2(this, installation, (kc4) null), kc4);
        }
        wd4.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void c() {
        new b().start();
    }

    @DexIgnore
    public final void j(String str) {
        wd4.b(str, "newActiveDeviceSerial");
        String e2 = e();
        fn2 fn2 = this.f;
        if (fn2 != null) {
            fn2.o(str);
            throw null;
            // this.A.a(str);
            // FossilNotificationBar.c.a(this);
            // ILocalFLogger local = FLogger.INSTANCE.getLocal();
            // String str2 = P;
            // local.d(str2, "onSwitchActiveDeviceSuccess - current=" + e2 + ", new=" + str);
            // return;
        }
        wd4.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void a(Installation installation) {
        installation.setLocaleIdentifier(m());
    }

    @DexIgnore
    public final long a(String str, int i2) {
        wd4.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "Inside " + P + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = U;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateGoalStep(str, i2);
                }
                wd4.a();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "Error Inside " + P + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local3.e(str4, "Error Inside " + P + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "Inside " + P + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "Error inside " + P + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(String str, NotificationBaseObj notificationBaseObj) {
        wd4.b(str, "serial");
        wd4.b(notificationBaseObj, "newNotification");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, List<? extends Alarm> list) {
        wd4.b(str, "serial");
        wd4.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, (List<Alarm>) list);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(List<? extends Alarm> list) {
        wd4.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm((List<Alarm>) list);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    public final void a() {
        fn2 fn2 = this.f;
        if (fn2 != null) {
            fn2.a(53);
            File cacheDir = getCacheDir();
            wd4.a(cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                for (String str : file.list()) {
                    if (!wd4.a((Object) str, (Object) "lib")) {
                        a(new File(file, str));
                    }
                }
            }
            @SuppressLint("WrongConstant") PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), 268435456);
            Object systemService = getSystemService(Context.ALARM_SERVICE);
            if (systemService != null) {
                ((AlarmManager) systemService).set(1, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Context.ACTIVITY_SERVICE);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            wd4.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = a(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, String str2) {
        wd4.b(communicateMode, "communicateMode");
        wd4.b(str, "serial");
        wd4.b(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = P;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        wd4.b(communicateMode, "curCommunicateMode");
        wd4.b(str, "curSerial");
        wd4.b(communicateMode2, "newCommunicateMode");
        wd4.b(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = P;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        String e2 = e();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = P;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(e2, min, min2, min3, min4);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local2.e(str2, "Error inside " + P + ".simulateDisconnection - e=" + e3);
        }
    }

    @DexIgnore
    public final long a(String str, CustomRequest customRequest) {
        wd4.b(str, "serial");
        wd4.b(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005a, code lost:
        if (kotlin.text.StringsKt__StringsKt.a((java.lang.CharSequence) r6, (java.lang.CharSequence) "image", false, 2, (java.lang.Object) null) == false) goto L_0x0061;
     */
    @DexIgnore
    public final boolean a(Intent intent, Uri uri) {
        wd4.b(uri, "fileUri");
        String a2 = a(uri);
        String type = intent != null ? intent.getType() : "";
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "isImageFile intentMimeType=" + type + ", uriMimeType=" + a2);
        String path = uri.getPath();
        if (path != null) {
            wd4.a((Object) path, "fileUri.path!!");
            if (!StringsKt__StringsKt.a((CharSequence) path, (CharSequence) "pickerImage", false/*, 2, (Object) null*/)) {
                if (!TextUtils.isEmpty(type)) {
                    if (type == null) {
                        wd4.a();
                        throw null;
                    }
                }
                if (TextUtils.isEmpty(a2)) {
                    return false;
                }
                if (a2 == null) {
                    wd4.a();
                    throw null;
                } else return StringsKt__StringsKt.a((CharSequence) a2, (CharSequence) "image", false/*, 2, (Object) null*/);
            }
            return true;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final String a(Uri uri) {
        if (wd4.a((Object) uri.getScheme(), (Object) "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, "Inside " + P + ".pairDevice");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDevice(str, str2, j());
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, "Error inside " + P + ".pairDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, PairingResponse pairingResponse) {
        wd4.b(str, "serial");
        wd4.b(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        wd4.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = P;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                cb4 cb4 = com.fossil.blesdk.obfuscated.cb4.a;
                return;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            cb4 cb42 = cb4.a;
        }
    }

    @DexIgnore
    public final void a(String str, VibrationStrengthObj vibrationStrengthObj) {
        wd4.b(str, "serial");
        wd4.b(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "Inside " + P + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "Error inside " + P + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, FirmwareData firmwareData, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(firmwareData, "firmwareData");
        wd4.b(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.d(str2, "Inside " + P + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, "Error inside " + P + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        wd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setComplicationApps - complicationAppsSettings=" + complicationAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoComplicationAppSettings(complicationAppMappingSettings, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(DeviceAppResponse deviceAppResponse, String str) {
        wd4.b(deviceAppResponse, "deviceAppResponse");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(MusicResponse musicResponse, String str) {
        wd4.b(musicResponse, "musicAppResponse");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long a(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setPresetApps - watchAppMappingSettings=" + watchAppMappingSettings + ", complicationAppsSettings=" + complicationAppMappingSettings + ", backgroundConfig=" + backgroundConfig);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(WatchAppMappingSettings watchAppMappingSettings, String str) {
        wd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(BackgroundConfig backgroundConfig, String str) {
        wd4.b(backgroundConfig, "backgroundConfig");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setAutoBackgroundImageConfig - backgroundConfig=" + backgroundConfig);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoBackgroundImageConfig(backgroundConfig, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setAutoNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserDisplayUnit userDisplayUnit, String str) {
        wd4.b(userDisplayUnit, "userDisplayUnit");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = P;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserProfile userProfile) {
        wd4.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(String str, DeviceAppResponse deviceAppResponse) {
        wd4.b(str, "serial");
        wd4.b(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, "Error inside " + P + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, InactiveNudgeData inactiveNudgeData) {
        wd4.b(str, "serial");
        wd4.b(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = U;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = P;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(P, "forceLogout");
        if (this.B) {
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.p;
            if (deleteLogoutUserUseCase != null) {
                deleteLogoutUserUseCase.a(new DeleteLogoutUserUseCase.b(2, null), new c(this, serverError));
            } else {
                wd4.d("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.I = serverError;
            this.H = true;
        }
    }
}
