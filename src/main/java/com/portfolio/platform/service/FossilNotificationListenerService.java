package com.portfolio.platform.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.AppLinkData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.ep2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.up4;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

// @DexEdit
@DexIgnore
@SuppressLint({"OverrideAbstract"})
public final class FossilNotificationListenerService extends NotificationListenerService {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static boolean q;
    @DexIgnore
    public static /* final */ String[] r; // = {"android.title", "android.title.big", "android.text", "android.subText", "android.infoText", "android.summaryText", "android.bigText"};
    @DexIgnore
    public static /* final */ a s; // = new a((rd4) null);
    @DexIgnore
    public /* final */ StringBuilder e; // = new StringBuilder();
    @DexIgnore
    public /* final */ SparseArray<b> f; // = new SparseArray<>();
    @DexIgnore
    public /* final */ HashSet<String> g; // = new HashSet<>();
    @DexIgnore
    public /* final */ Handler h; // = new Handler();
    @DexIgnore
    public /* final */ id4<cb4> i; // = new FossilNotificationListenerService$mRunnable$Anon1(this);
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<StatusBarNotification> j; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public MusicControlComponent k; // = ((MusicControlComponent) MusicControlComponent.o.a(this));
    @DexIgnore
    public DianaNotificationComponent l;
    @DexIgnore
    public HybridMessageNotificationComponent m;
    @DexIgnore
    public i42 n;
    @DexIgnore
    public fn2 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return FossilNotificationListenerService.p;
        }

        @DexIgnore
        public final boolean b() {
            return FossilNotificationListenerService.q;
        }

        @DexIgnore
        public final void c() {
            FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService()");
            try {
                ComponentName componentName = new ComponentName(PortfolioApp.W.c(), FossilNotificationListenerService.class);
                a(componentName);
                if (Build.VERSION.SDK_INT >= 24) {
                    FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService() - requestRebind");
                    NotificationListenerService.requestRebind(componentName);
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = a();
                local.d(a, "tryReconnectNotificationService() - ex = " + e);
            }
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        public final void a(ComponentName componentName) {
            FLogger.INSTANCE.getLocal().d(a(), "toggleNotificationListenerService()");
            PackageManager packageManager = PortfolioApp.W.c().getPackageManager();
            packageManager.setComponentEnabledSetting(componentName, 2, 1);
            packageManager.setComponentEnabledSetting(componentName, 1, 1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a; // = -1;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ long e;

        @DexIgnore
        public b(String str, String str2, int i, long j) {
            wd4.b(str, "packageName");
            wd4.b(str2, "text");
            this.b = str;
            this.c = str2;
            this.d = i;
            this.e = j;
        }

        @DexIgnore
        public final long a() {
            return this.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                return wd4.a((Object) this.b, (Object) bVar.b) && wd4.a((Object) this.c, (Object) bVar.c) && this.d == bVar.d;
            }
            throw null;
        }

        @DexIgnore
        public int hashCode() {
            if (this.a == -1) {
                this.a = (this.b + this.d).hashCode();
            }
            return this.a;
        }

        @DexIgnore
        public String toString() {
            return this.d + " | " + this.b + " | " + this.c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification e;

        @DexIgnore
        public c(StatusBarNotification statusBarNotification) {
            this.e = statusBarNotification;
        }

        // @DexReplace
        @DexIgnore
        public final void run() {
            NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
            String packageName = this.e.getPackageName();
            wd4.a((Object) packageName, "statusBarNotification.packageName");
            if (notificationAppHelper.a(packageName)) {
                String str = us3.b() ? "DND" : "Silence";
                MFUser b = en2.p.a().n().b();
                HashMap hashMap = new HashMap();
                if (b != null) {
                    String userId = b.getUserId();
                    wd4.a((Object) userId, "user.userId");
                    hashMap.put("user_id", userId);
                }
                String packageName2 = this.e.getPackageName();
                wd4.a((Object) packageName2, "statusBarNotification.packageName");
                hashMap.put("app_id", packageName2);
                hashMap.put("post_time", String.valueOf(this.e.getPostTime()));
                hashMap.put("blocked_by", str);
                AnalyticsHelper.f.c().a("notification_blocked", (Map<String, ? extends Object>) hashMap);
            }
        }
    }

    /*
    static {
        String simpleName = FossilNotificationListenerService.class.getSimpleName();
        wd4.a((Object) simpleName, "FossilNotificationListen\u2026ce::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public final boolean c() {
        fn2 fn2 = this.o;
        if (fn2 != null) {
            boolean D = fn2.D();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = p;
            local.d(str, "isBlockedByDND() = " + D);
            return D;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    // @DexReplace
    @DexIgnore
    public final void f(StatusBarNotification statusBarNotification) {
        i42 i42 = this.n;
        if (i42 != null) {
            i42.a().execute(new c(statusBarNotification));
        } else {
            wd4.d("mAppExecutors");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.W.c().g().a(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onCreate() - Notification Listener Service permission " + PortfolioApp.W.c().C());
        throw null;
        // ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new FossilNotificationListenerService$onCreate$Anon1(this, (kc4) null), 3, (Object) null);
        // FLogger.INSTANCE.getLocal().d(p, "onCreate() - Notification Listener Service Created");
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.k.g();
        FLogger.INSTANCE.getLocal().d(p, "onDestroy() - Notification Listener Service Destroyed");
    }

    @DexIgnore
    public void onListenerConnected() {
        super.onListenerConnected();
        FLogger.INSTANCE.getLocal().d(p, "onListenerConnected()");
        q = true;
    }

    @DexIgnore
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        FLogger.INSTANCE.getLocal().d(p, "onListenerDisconnected()");
        q = false;
    }

    @DexIgnore
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        wd4.b(statusBarNotification, "sbn");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onNotificationPosted() - Receive notification with permission enable is " + os3.a.e());
        String e2 = PortfolioApp.W.c().e();
        if (!DeviceHelper.o.d(e2)) {
            FLogger.INSTANCE.getLocal().d(p, "onNotificationPosted() - Device is not supported");
        } else if (FossilDeviceSerialPatternUtil.isDianaDevice(e2)) {
            d(statusBarNotification);
            c(statusBarNotification);
            DianaNotificationComponent dianaNotificationComponent = this.l;
            if (dianaNotificationComponent != null) {
                dianaNotificationComponent.b(statusBarNotification);
            } else {
                wd4.d("mDianaNotificationComponent");
                throw null;
            }
        } else {
            fn2 fn2 = this.o;
            if (fn2 != null) {
                if (fn2.M() && !c()) {
                    HybridMessageNotificationComponent hybridMessageNotificationComponent = this.m;
                    if (hybridMessageNotificationComponent != null) {
                        hybridMessageNotificationComponent.a(statusBarNotification);
                    } else {
                        wd4.d("mHybridMessageNotificationComponent");
                        throw null;
                    }
                }
                a(statusBarNotification);
                return;
            }
            wd4.d("mSharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(p, "onNotificationRemoved()");
        if (statusBarNotification != null) {
            try {
                super.onNotificationRemoved(statusBarNotification);
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                    d(statusBarNotification);
                    c(statusBarNotification);
                    DianaNotificationComponent dianaNotificationComponent = this.l;
                    if (dianaNotificationComponent != null) {
                        dianaNotificationComponent.a(statusBarNotification);
                    } else {
                        wd4.d("mDianaNotificationComponent");
                        throw null;
                    }
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = p;
                local.d(str, "onNotificationRemoved(): package = " + statusBarNotification.getPackageName() + " - notification = " + statusBarNotification.getNotification() + ", exception = " + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        wd4.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(p, "onStartCommand() - Notification Listener Service Started");
        return super.onStartCommand(intent, i2, i3);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.fossil.blesdk.obfuscated.ep2] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.fossil.blesdk.obfuscated.ep2] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(p, "addNotificationToQueue()");
        if (statusBarNotification.getNotification().priority < -1) {
            FLogger.INSTANCE.getLocal().d(p, "addNotificationToQueue() - Ignoring Min priority notification");
            if (!PortfolioApp.W.c().D()) {
                c(statusBarNotification);
                return;
            }
            return;
        }
        this.j.add(statusBarNotification);
        Handler handler = this.h;
        id4<cb4> id4 = this.i;
        if (id4 != null) {
            throw null;
            // id4 = new ep2(id4);
        }
        handler.removeCallbacks((Runnable) id4);
        Handler handler2 = this.h;
        id4<cb4> id42 = this.i;
        if (id42 != null)
            throw null;{
            // id42 = new ep2(id42);
        }
        handler2.postDelayed((Runnable) id42, 500);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (wd4.a((Object) statusBarNotification.getPackageName(), (Object) ((StatusBarNotification) list.get(i2)).getPackageName())) {
                FLogger.INSTANCE.getLocal().d(p, "isDuplicated() - Notification is duplicated");
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void d(StatusBarNotification statusBarNotification) {
        Notification notification = statusBarNotification.getNotification();
        FLogger.INSTANCE.getLocal().d(p, "............");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "  Notification Posted: " + statusBarNotification.getPackageName());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local2.d(str2, "  Id: " + statusBarNotification.getId());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = p;
        local3.d(str3, "  Post Time: " + statusBarNotification.getPostTime());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = p;
        local4.d(str4, "  Tag: " + statusBarNotification.getTag());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = p;
        local5.d(str5, "  Content: " + notification.extras);
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = p;
        local6.d(str6, "  Notification Priority: " + notification.priority);
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = p;
        local7.d(str7, "  Notification Flags: " + Integer.toBinaryString(notification.flags));
        if (Build.VERSION.SDK_INT >= 21) {
            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
            String str8 = p;
            local8.d(str8, "  Group Key: " + statusBarNotification.getGroupKey());
            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
            String str9 = p;
            local9.d(str9, "  Key: " + statusBarNotification.getKey());
            ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
            String str10 = p;
            StringBuilder sb = new StringBuilder();
            sb.append("  Notification Group: ");
            wd4.a((Object) notification, "notification");
            sb.append(notification.getGroup());
            local10.d(str10, sb.toString());
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            String str11 = p;
            local11.d(str11, "  Notification Sort Key: " + notification.getSortKey());
        }
        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
        String str12 = p;
        local12.d(str12, "  isSummary = " + b(statusBarNotification));
        FLogger.INSTANCE.getLocal().d(p, "............");
    }

    @DexIgnore
    public final boolean e(StatusBarNotification statusBarNotification) {
        String str;
        if (statusBarNotification == null) {
            FLogger.INSTANCE.getLocal().d(p, "processNotification() - sbn is null ");
            return false;
        }
        Notification notification = statusBarNotification.getNotification();
        if (notification == null) {
            FLogger.INSTANCE.getLocal().d(p, "processNotification() - sbn.getNotification() is null ");
            return false;
        } else if (statusBarNotification.getNotification().extras == null || statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0) {
            FLogger.INSTANCE.getLocal().d(p, "processNotification() - Ignoring Progress notification");
            return false;
        } else {
            c(statusBarNotification);
            String obj = a(notification).getCharSequence("android.title", "").toString();
            String sb = this.e.toString();
            wd4.a((Object) sb, "mStringBuilder.toString()");
            int length = sb.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean z2 = sb.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            String obj2 = sb.subSequence(i2, length + 1).toString();
            long currentTimeMillis = System.currentTimeMillis();
            String packageName = statusBarNotification.getPackageName();
            wd4.a((Object) packageName, "sbn.packageName");
            b bVar = new b(packageName, obj2, statusBarNotification.getId(), currentTimeMillis);
            if (a(bVar)) {
                return false;
            }
            this.f.put(bVar.hashCode(), bVar);
            if (c()) {
                f(statusBarNotification);
                return false;
            }
            PackageManager packageManager = getPackageManager();
            try {
                @SuppressLint("WrongConstant")
                CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(statusBarNotification.getPackageName(), 128));
                if (applicationLabel != null) {
                    str = (String) applicationLabel;
                    FLogger.INSTANCE.getLocal().d(p, "processNotification() - We are going to analyze this notification from: " + str + ", string = " + obj2);
                    LightAndHapticsManager.i.a().a(new NotificationInfo(NotificationSource.OS, obj, obj2, statusBarNotification.getPackageName(), str));
                    a();
                    return true;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(p, "processNotification() - Could not find app name - ex = " + e2);
                str = "";
            }
        }
        throw null;
    }

    @DexIgnore
    public final void c(StatusBarNotification statusBarNotification) {
        String packageName = statusBarNotification.getPackageName();
        Notification notification = statusBarNotification.getNotification();
        Bundle bundle = notification.extras;
        String obj = !TextUtils.isEmpty(notification.tickerText) ? notification.tickerText.toString() : "";
        CharSequence charSequence = bundle.getCharSequence("android.title");
        CharSequence charSequence2 = bundle.getCharSequence("android.text");
        CharSequence charSequence3 = bundle.getCharSequence("android.subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "printDebugInfo() - Notification info:\n  package name: " + packageName + "\n" + "  ticker: " + obj + "\n" + "  title: " + charSequence + "\n" + "  subtext: " + charSequence3 + "\n" + "  text: " + charSequence2);
    }

    @DexIgnore
    public final void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "filterQueue() - queue size = " + this.j.size() + " items");
        ArrayList arrayList = new ArrayList(this.j.size());
        arrayList.addAll(this.j);
        ArrayList arrayList2 = new ArrayList();
        this.j.clear();
        HashSet hashSet = new HashSet();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = arrayList.get(i2);
            wd4.a(obj, "notifications[i]");
            StatusBarNotification statusBarNotification = (StatusBarNotification) obj;
            FLogger.INSTANCE.getLocal().d(p, "filterQueue() - Looking at:");
            if (PortfolioApp.W.e()) {
                d(statusBarNotification);
            }
            boolean a2 = a(statusBarNotification, arrayList2);
            if (b(statusBarNotification, arrayList2)) {
                a2 = false;
            }
            if (hashSet.contains(statusBarNotification.getPackageName())) {
                FLogger.INSTANCE.getLocal().d(p, "filterQueue() - This notification is processing");
                a2 = false;
            }
            if (a2) {
                FLogger.INSTANCE.getLocal().d(p, "filterQueue() - Allowing notification to process...");
                arrayList2.add(statusBarNotification);
                if (e(statusBarNotification)) {
                    hashSet.add(statusBarNotification.getPackageName());
                }
            } else {
                FLogger.INSTANCE.getLocal().d(p, "filterQueue() - Blocking notification from processing");
            }
        }
    }

    @DexIgnore
    public final boolean a(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        boolean z = true;
        if (b(statusBarNotification) && Build.VERSION.SDK_INT >= 21) {
            int size = list.size();
            boolean z2 = true;
            for (int i2 = 0; i2 < size; i2++) {
                String groupKey = statusBarNotification.getGroupKey();
                String groupKey2 = ((StatusBarNotification) list.get(i2)).getGroupKey();
                Notification notification = statusBarNotification.getNotification();
                wd4.a((Object) notification, "notification.notification");
                String group = notification.getGroup();
                Notification notification2 = ((StatusBarNotification) list.get(i2)).getNotification();
                wd4.a((Object) notification2, "filterList[j].notification");
                String group2 = notification2.getGroup();
                if (!(groupKey == null || groupKey2 == null || !cg4.b(groupKey, groupKey2, true))) {
                    z2 = false;
                }
                if (!(group == null || group2 == null || !cg4.b(group, group2, true))) {
                    z2 = false;
                }
            }
            z = z2;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "handleSummaryNotification() - isAlone = " + z);
        return z;
    }

    @DexIgnore
    public final Bundle a(Notification notification) {
        FLogger.INSTANCE.getLocal().d(p, "collectTextInfo()");
        Bundle bundle = notification.extras;
        this.e.setLength(0);
        this.g.clear();
        for (String str : r) {
            Object obj = bundle.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = p;
            StringBuilder sb = new StringBuilder();
            sb.append("  Looking at ");
            sb.append(str);
            sb.append(" : ");
            sb.append(obj);
            sb.append(" : class ");
            sb.append(obj != null ? obj.getClass().getSimpleName() : "null");
            local.d(str2, sb.toString());
            if ((obj instanceof CharSequence) && !TextUtils.isEmpty((CharSequence) obj)) {
                FLogger.INSTANCE.getLocal().d(p, "  Adding " + str + " with value of " + obj);
                a(obj.toString());
            }
        }
        FLogger.INSTANCE.getLocal().d(p, "  Ticker Text = " + notification.tickerText);
        if (!TextUtils.isEmpty(notification.tickerText)) {
            a(notification.tickerText.toString());
        }
        wd4.a((Object) bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        return bundle;
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification) {
        return (statusBarNotification.getNotification().flags & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 512;
    }

    @DexIgnore
    public final boolean a(b bVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "isBlockedByIntervalTime() - thisNotification.hashCode() = " + bVar.hashCode());
        b bVar2 = this.f.get(bVar.hashCode());
        if (bVar2 == null) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(p, "isBlockedByIntervalTime() - Have an old notification with the same hash");
        if (bVar.a() - bVar2.a() > ButtonService.CONNECT_TIMEOUT) {
            FLogger.INSTANCE.getLocal().d(p, "isBlockedByIntervalTime() - Older than 10000 milliseconds. Allow update to this notification");
            return false;
        }
        long abs = Math.abs(ButtonService.CONNECT_TIMEOUT - (bVar.a() - bVar2.a()));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local2.d(str2, "isBlockedByIntervalTime() - Block duplicate in " + up4.a(abs) + ", notification: " + bVar);
        return true;
    }

    @DexIgnore
    public final void a() {
        long currentTimeMillis = System.currentTimeMillis();
        int size = this.f.size();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "cleanPastNotificationMap() - Size = " + size);
        LinkedList linkedList = new LinkedList();
        for (int i2 = 0; i2 < size; i2++) {
            int keyAt = this.f.keyAt(i2);
            if (this.f.get(keyAt) != null && currentTimeMillis - this.f.get(keyAt).a() > ButtonService.CONNECT_TIMEOUT) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = p;
                local2.d(str2, "cleanPastNotificationMap() - Adding key to remove - key = " + keyAt);
                linkedList.add(Integer.valueOf(keyAt));
            }
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            FLogger.INSTANCE.getLocal().d(p, "cleanPastNotificationMap() - Dumping old notification");
            SparseArray<b> sparseArray = this.f;
            wd4.a((Object) num, "keyInt");
            sparseArray.remove(num.intValue());
        }
    }

    @DexIgnore
    public final void a(String str) {
        if (!this.g.contains(str)) {
            Iterator<String> it = this.g.iterator();
            while (it.hasNext()) {
                String next = it.next();
                wd4.a((Object) next, "oldString");
                throw null;
                // if (StringsKt__StringsKt.a((CharSequence) next, (CharSequence) str, false, 2, (Object) null)) {
                //     return;
                // }
            }
            this.e.append(' ');
            this.e.append(str);
            this.g.add(str);
        }
    }
}
