package com.portfolio.platform;

import com.portfolio.platform.workers.PushPendingDataWorker;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.portfolio.platform.helper.DeviceHelper;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.blesdk.obfuscated.cg4;
import com.portfolio.platform.data.AppType;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.blesdk.obfuscated.kd4;
import kotlinx.coroutines.CoroutineStart;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.kc4;
import kotlin.coroutines.CoroutineContext;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.portfolio.platform.data.legacy.threedotzero.DeviceModel;
import com.portfolio.platform.data.model.Device;
import com.fossil.blesdk.obfuscated.or3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.zw;
import com.portfolio.platform.data.model.MFUser;
import android.text.TextUtils;
import android.content.Context;
import com.fossil.blesdk.obfuscated.ks3;
import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import java.util.HashMap;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import java.util.List;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import java.util.Iterator;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Date;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import com.fossil.blesdk.obfuscated.sj2;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.fossil.blesdk.obfuscated.d62;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.fossil.blesdk.obfuscated.fl2;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.fossil.blesdk.obfuscated.sk2;
import java.util.Calendar;
import java.util.ArrayList;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.MappingSet;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.fossil.blesdk.obfuscated.wj2;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.fossil.blesdk.obfuscated.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

// Needs a minimal amount of overridding to accept the DynaEnums
@DexEdit
public final class MigrationManager
{
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ MigrationManager.a n;
    @DexIgnore
    public /* final */ fn2 a;
    @DexIgnore
    public /* final */ UserRepository b;
    @DexIgnore
    public /* final */ NotificationsRepository c;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase d;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ GoalTrackingRepository f;
    @DexIgnore
    public /* final */ GoalTrackingDatabase g;
    @DexIgnore
    public /* final */ DeviceDao h;
    @DexIgnore
    public /* final */ HybridCustomizeDatabase i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;
    @DexIgnore
    public /* final */ wj2 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    
    /*
    static {
        n = new a(null);
        final String simpleName = MigrationManager.class.getSimpleName();
        wd4.a((Object)simpleName, "MigrationManager::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final class a
    {
        @DexIgnore
        public a() {
            super();
        }

        @DexIgnore
        public a(final rd4 rd4) {
            this();
        }

        @DexIgnore
        public final String a() {
            return MigrationManager.e();
        }
    }


    @DexIgnore
    public static final class MigrationManager$b implements Runnable {
        @DexIgnore
        public final /* synthetic */ MigrationManager e;
        @DexIgnore
        public final /* synthetic */ ArrayList f;
        @DexIgnore
        public final /* synthetic */ List g;

        @DexIgnore
        public MigrationManager$b(final MigrationManager e, final ArrayList f, final List g) {
            super();
            this.e = e;
            this.f = f;
            this.g = g;
        }

        @Override
        @DexIgnore
        public final void run() {
            MigrationManager.e(this.e).microAppDao().upsertListMicroApp(this.f);
            MigrationManager.e(this.e).presetDao().upsertPresetList(this.g);
        }
    }


    @DexIgnore
    public MigrationManager(final fn2 a, final UserRepository b, final NotificationsRepository c, final GetHybridDeviceSettingUseCase d, final PortfolioApp e, final GoalTrackingRepository f, final GoalTrackingDatabase g, final DeviceDao h, final HybridCustomizeDatabase i, final MicroAppLastSettingRepository j, final wj2 k, final AlarmsRepository l) {
        super();
        wd4.b(a, "mSharedPrefs");
        wd4.b(b, "mUserRepository");
        wd4.b(c, "mNotificationRepository");
        wd4.b(d, "mGetHybridDeviceSettingUseCase");
        wd4.b(e, "mApp");
        wd4.b(f, "mGoalTrackingRepository");
        wd4.b(g, "mGoalTrackingDatabase");
        wd4.b(h, "mDeviceDao");
        wd4.b(i, "mHybridCustomizeDatabase");
        wd4.b(j, "mMicroAppLastSettingRepository");
        wd4.b(k, "mDeviceSettingFactory");
        wd4.b(l, "alarmsRepository");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
    }
    
    @DexIgnore
    public static final /* synthetic */ PortfolioApp a(final MigrationManager migrationManager) {
        return migrationManager.e;
    }
    
    @DexIgnore
    public static final /* synthetic */ GetHybridDeviceSettingUseCase b(final MigrationManager migrationManager) {
        return migrationManager.d;
    }
    
    @DexIgnore
    public static final /* synthetic */ GoalTrackingDatabase c(final MigrationManager migrationManager) {
        return migrationManager.g;
    }
    
    @DexIgnore
    public static final /* synthetic */ GoalTrackingRepository d(final MigrationManager migrationManager) {
        return migrationManager.f;
    }
    
    @DexIgnore
    public static final /* synthetic */ HybridCustomizeDatabase e(final MigrationManager migrationManager) {
        return migrationManager.i;
    }
    
    @DexIgnore
    public static final /* synthetic */ String e() {
        return MigrationManager.m;
    }
    
    @DexIgnore
    public final HybridPreset a(final String s, final MappingSet set, final DeviceProvider deviceProvider, final LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        throw null;
    //     final ILocalFLogger local = FLogger.INSTANCE.getLocal();
    //     final String m = MigrationManager.m;
    //     final StringBuilder sb = new StringBuilder();
    //     sb.append("convertSavedMappingSetToHybridPreset ");
    //     sb.append(set.getName());
    //     local.d(m, sb.toString());
    //     final ArrayList<Object> list = new ArrayList<Object>();
    //     final Calendar instance = Calendar.getInstance();
    //     wd4.a((Object)instance, "Calendar.getInstance()");
    //     final String t = sk2.t(instance.getTime());
    // Label_0096:
    //     for (final Mapping mapping : set.getMappingList()) {
    //         final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
    //         final String i = MigrationManager.m;
    //         final StringBuilder sb2 = new StringBuilder();
    //         sb2.append("convertSavedMappingSetToHybridPreset action ");
    //         wd4.a((Object)mapping, "mapping");
    //         sb2.append(mapping.getAction());
    //         sb2.append(" extraInfo ");
    //         sb2.append(mapping.getExtraInfo());
    //         local2.d(i, sb2.toString());
    //         final MicroAppInstruction.MicroAppID a = fl2.a.a(mapping.getAction());
    //         final String a2 = fl2.a.a(mapping.getGesture());
    //         if (a != null && a != MicroAppInstruction.MicroAppID.UAPP_ALARM_ID) {
    //             final int n = d62.a[a.ordinal()];
    //             final String s2 = "";
    //             String settings;
    //             if (n != 1) {
    //                 if (n != 2) {
    //                     settings = null;
    //                 }
    //                 else {
    //                     final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
    //                     final String j = MigrationManager.m;
    //                     final StringBuilder sb3 = new StringBuilder();
    //                     sb3.append("convertSavedMappingSetToHybridPreset 2ndTz setting ");
    //                     sb3.append(mapping.getExtraInfo());
    //                     local3.d(j, sb3.toString());
    //                     settings = s2;
    //                     if (legacySecondTimezoneSetting != null) {
    //                         final String timezoneCityName = legacySecondTimezoneSetting.getTimezoneCityName();
    //                         wd4.a((Object)timezoneCityName, "it.timezoneCityName");
    //                         final String timezoneId = legacySecondTimezoneSetting.getTimezoneId();
    //                         wd4.a((Object)timezoneId, "it.timezoneId");
    //                         final int n2 = (int)legacySecondTimezoneSetting.getTimezoneOffset();
    //                         final String timezoneId2 = legacySecondTimezoneSetting.getTimezoneId();
    //                         wd4.a((Object)timezoneId2, "it.timezoneId");
    //                         final String a3 = sj2.<SecondTimezoneSetting>a(new SecondTimezoneSetting(timezoneCityName, timezoneId, n2, timezoneId2));
    //                         settings = s2;
    //                         if (a3 != null) {
    //                             settings = a3;
    //                         }
    //                     }
    //                 }
    //             }
    //             else {
    //                 final ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
    //                 final String k = MigrationManager.m;
    //                 final StringBuilder sb4 = new StringBuilder();
    //                 sb4.append("convertSavedMappingSetToHybridPreset ringphone setting ");
    //                 sb4.append(mapping.getExtraInfo());
    //                 local4.d(k, sb4.toString());
    //                 final String extraInfo = mapping.getExtraInfo();
    //                 settings = s2;
    //                 if (extraInfo != null) {
    //                     final String a4 = sj2.<Ringtone>a(new Ringtone(extraInfo, ""));
    //                     settings = s2;
    //                     if (a4 != null) {
    //                         settings = a4;
    //                     }
    //                 }
    //             }
    //             final String value = a.getValue();
    //             wd4.a((Object)t, "localUpdatedAt");
    //             final HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(a2, value, t);
    //             if (settings != null) {
    //                 hybridPresetAppSetting.setSettings(settings);
    //             }
    //             while (true) {
    //                 for (final HybridPresetAppSetting next : list) {
    //                     if (wd4.a((Object)next.getPosition(), (Object)hybridPresetAppSetting.getPosition())) {
    //                         if (next == null) {
    //                             list.add(hybridPresetAppSetting);
    //                             continue Label_0096;
    //                         }
    //                         continue Label_0096;
    //                     }
    //                 }
    //                 HybridPresetAppSetting next = null;
    //                 continue;
    //             }
    //         }
    //         final ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
    //         final String l = MigrationManager.m;
    //         final StringBuilder sb5 = new StringBuilder();
    //         sb5.append("convertSavedMappingSetToHybridPreset ");
    //         sb5.append(a);
    //         sb5.append(" not supported, revert  to default preset");
    //         local5.d(l, sb5.toString());
    //         return this.a(s, deviceProvider.getDefaultPreset(s));
    //     }
    //     final StringBuilder sb6 = new StringBuilder();
    //     sb6.append(s);
    //     sb6.append(':');
    //     sb6.append(set.getId());
    //     final HybridPreset hybridPreset = new HybridPreset(sb6.toString(), set.getName(), s, (ArrayList<HybridPresetAppSetting>)list, false);
    //     hybridPreset.setPinType(0);
    //     final long create = set.getCreateAt();
    //     final long n3 = 1000;
    //     hybridPreset.setCreatedAt(sk2.t(new Date(create * n3)));
    //     hybridPreset.setUpdatedAt(sk2.t(new Date(set.getUpdateAt() * n3)));
    //     return hybridPreset;
    }
    
    @DexReplace
    public final HybridPreset a(final String s, final RecommendedPreset recommendedPreset) {
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String m = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("convertRecommendPresetToHybridPreset ");
        String name;
        if (recommendedPreset != null) {
            name = recommendedPreset.getName();
        }
        else {
            name = null;
        }
        sb.append(name);
        local.d(m, sb.toString());
        if (recommendedPreset != null) {
            final ArrayList<HybridPresetAppSetting> list = new ArrayList<HybridPresetAppSetting>();
            final Calendar instance = Calendar.getInstance();
            wd4.a((Object)instance, "Calendar.getInstance()");
            final String t = sk2.t(instance.getTime());
            final List<ButtonMapping> buttonMappingList = recommendedPreset.getButtonMappingList();
            wd4.a((Object)buttonMappingList, "recommendedPreset.buttonMappingList");
            for (final ButtonMapping buttonMapping : buttonMappingList) {
                final String component1 = buttonMapping.component1();
                final String component2 = buttonMapping.component2();
                wd4.a((Object)t, "localUpdatedAt");
                list.add(new HybridPresetAppSetting(component1, component2, t));
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(':');
            sb2.append(recommendedPreset.getId());
            final HybridPreset hybridPreset = new HybridPreset(sb2.toString(), recommendedPreset.getName(), s, new ArrayList<HybridPresetAppSetting>(), false);
            hybridPreset.setPinType(0);
            final long create = recommendedPreset.getCreateAt();
            final long n = 1000;
            hybridPreset.setCreatedAt(sk2.t(new Date(create * n)));
            hybridPreset.setUpdatedAt(sk2.t(new Date(recommendedPreset.getUpdateAt() * n)));
            return hybridPreset;
        }
        return null;
    }
    
    @DexIgnore
    public final HybridPreset a(final String s, final String s2, final MappingSet set, final LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        throw null;
    //     final ILocalFLogger local = FLogger.INSTANCE.getLocal();
    //     final String m = MigrationManager.m;
    //     final StringBuilder sb = new StringBuilder();
    //     sb.append("convertActiveMappingSetToHybridPreset ");
    //     sb.append(set.getName());
    //     local.d(m, sb.toString());
    //     final ArrayList<Object> list = new ArrayList<Object>();
    //     final Calendar instance = Calendar.getInstance();
    //     wd4.a((Object)instance, "Calendar.getInstance()");
    //     final String t = sk2.t(instance.getTime());
    // Label_0097:
    //     for (final Mapping mapping : set.getMappingList()) {
    //         final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
    //         final String i = MigrationManager.m;
    //         final StringBuilder sb2 = new StringBuilder();
    //         sb2.append("convertActiveMappingSetToHybridPreset action ");
    //         wd4.a((Object)mapping, "mapping");
    //         sb2.append(mapping.getAction());
    //         sb2.append(" extraInfo ");
    //         sb2.append(mapping.getExtraInfo());
    //         local2.d(i, sb2.toString());
    //         final MicroAppInstruction.MicroAppID a = fl2.a.a(mapping.getAction());
    //         final String a2 = fl2.a.a(mapping.getGesture());
    //         if (a != null && a != MicroAppInstruction.MicroAppID.UAPP_ALARM_ID) {
    //             final int n = d62.b[a.ordinal()];
    //             String settings = null;
    //             Label_0403: {
    //                 if (n != 1) {
    //                     if (n != 2) {
    //                         settings = null;
    //                         break Label_0403;
    //                     }
    //                     if (legacySecondTimezoneSetting != null) {
    //                         final String timezoneCityName = legacySecondTimezoneSetting.getTimezoneCityName();
    //                         wd4.a((Object)timezoneCityName, "it.timezoneCityName");
    //                         final String timezoneId = legacySecondTimezoneSetting.getTimezoneId();
    //                         wd4.a((Object)timezoneId, "it.timezoneId");
    //                         final int n2 = (int)legacySecondTimezoneSetting.getTimezoneOffset();
    //                         final String timezoneId2 = legacySecondTimezoneSetting.getTimezoneId();
    //                         wd4.a((Object)timezoneId2, "it.timezoneId");
    //                         settings = sj2.<SecondTimezoneSetting>a(new SecondTimezoneSetting(timezoneCityName, timezoneId, n2, timezoneId2));
    //                         if (settings != null) {
    //                             break Label_0403;
    //                         }
    //                     }
    //                 }
    //                 else {
    //                     final String extraInfo = mapping.getExtraInfo();
    //                     if (extraInfo != null) {
    //                         settings = sj2.<Ringtone>a(new Ringtone(extraInfo, ""));
    //                         if (settings != null) {
    //                             break Label_0403;
    //                         }
    //                     }
    //                 }
    //                 settings = "";
    //             }
    //             final String value = a.getValue();
    //             wd4.a((Object)t, "localUpdatedAt");
    //             final HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(a2, value, t);
    //             if (settings != null) {
    //                 hybridPresetAppSetting.setSettings(settings);
    //             }
    //             while (true) {
    //                 for (final HybridPresetAppSetting next : list) {
    //                     if (wd4.a((Object)next.getPosition(), (Object)hybridPresetAppSetting.getPosition())) {
    //                         if (next == null) {
    //                             list.add(hybridPresetAppSetting);
    //                             continue Label_0097;
    //                         }
    //                         continue Label_0097;
    //                     }
    //                 }
    //                 HybridPresetAppSetting next = null;
    //                 continue;
    //             }
    //         }
    //         final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
    //         final String j = MigrationManager.m;
    //         final StringBuilder sb3 = new StringBuilder();
    //         sb3.append("convertActiveMappingSetToHybridPreset ");
    //         sb3.append(a);
    //         sb3.append(" not supported");
    //         local3.d(j, sb3.toString());
    //         return null;
    //     }
    //     final StringBuilder sb4 = new StringBuilder();
    //     sb4.append(s);
    //     sb4.append(':');
    //     sb4.append(s2);
    //     final HybridPreset hybridPreset = new HybridPreset(sb4.toString(), "Active Preset", s2, (ArrayList<HybridPresetAppSetting>)list, true);
    //     hybridPreset.setPinType(0);
    //     final long create = set.getCreateAt();
    //     final long n3 = 1000;
    //     hybridPreset.setCreatedAt(sk2.t(new Date(create * n3)));
    //     hybridPreset.setUpdatedAt(sk2.t(new Date(set.getUpdateAt() * n3)));
    //     return hybridPreset;
    }
    
    @DexIgnore
    public final HybridPreset a(final List<? extends MicroApp> list, String m, final SavedPreset savedPreset, final HashMap<String, String> hashMap) {
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String i = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("convert ");
        sb.append(savedPreset);
        sb.append(" microAppList ");
        sb.append(list.size());
        local.d(i, sb.toString());
        final ArrayList<HybridPresetAppSetting> list2 = new ArrayList<HybridPresetAppSetting>();
        final Calendar instance = Calendar.getInstance();
        wd4.a((Object)instance, "Calendar.getInstance()");
        final String t = sk2.t(instance.getTime());
    Label_0115:
        for (final ButtonMapping buttonMapping : savedPreset.getButtonMappingList()) {
            final String component1 = buttonMapping.component1();
            final String component2 = buttonMapping.component2();
            while (true) {
                for (final MicroApp next : list) {
                    if (wd4.a((Object)next.getAppId(), (Object)component2)) {
                        if (next == null) {
                            final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            final String j = MigrationManager.m;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("this preset is not support for ");
                            sb2.append(m);
                            sb2.append(" due to ");
                            sb2.append(component2);
                            local2.d(j, sb2.toString());
                            return null;
                        }
                        final String settings = hashMap.get(component2);
                        wd4.a((Object)t, "localUpdatedAt");
                        final HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, t);
                        if (settings != null) {
                            hybridPresetAppSetting.setSettings(settings);
                        }
                        list2.add(hybridPresetAppSetting);
                        continue Label_0115;
                    }
                }
                MicroApp next = null;
                continue;
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(m);
        sb3.append(':');
        sb3.append(savedPreset.getId());
        final HybridPreset hybridPreset = new HybridPreset(sb3.toString(), savedPreset.getName(), m, list2, false);
        final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        m = MigrationManager.m;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("legacy createdAt ");
        sb4.append(savedPreset.getCreateAt());
        sb4.append(" updatedAt ");
        sb4.append(savedPreset.getUpdateAt());
        local3.d(m, sb4.toString());
        final long create = savedPreset.getCreateAt();
        final long n = 1000;
        hybridPreset.setCreatedAt(sk2.t(new Date(create * n)));
        hybridPreset.setUpdatedAt(sk2.t(new Date(savedPreset.getUpdateAt() * n)));
        final ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        final String k = MigrationManager.m;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("preset createdAt ");
        sb5.append(hybridPreset.getCreatedAt());
        sb5.append(" updatedAt ");
        sb5.append(hybridPreset.getUpdatedAt());
        local4.d(k, sb5.toString());
        hybridPreset.setPinType(savedPreset.getPinType());
        return hybridPreset;
    }
    
    @DexIgnore
    public final HybridPreset a(final List<? extends MicroApp> list, String s, String m, final ActivePreset activePreset, final HashMap<String, String> hashMap) {
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String i = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("convert ");
        sb.append(activePreset);
        local.d(i, sb.toString());
        final ArrayList<HybridPresetAppSetting> list2 = new ArrayList<HybridPresetAppSetting>();
        final Calendar instance = Calendar.getInstance();
        wd4.a((Object)instance, "Calendar.getInstance()");
        final String t = sk2.t(instance.getTime());
    Label_0096:
        for (final ButtonMapping buttonMapping : activePreset.getButtonMappingList()) {
            final String component1 = buttonMapping.component1();
            final String component2 = buttonMapping.component2();
            while (true) {
                for (final MicroApp next : list) {
                    if (wd4.a((Object)next.getAppId(), (Object)component2)) {
                        if (next == null) {
                            final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            s = MigrationManager.m;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("this preset is not support for ");
                            sb2.append(m);
                            sb2.append(" due to ");
                            sb2.append(component2);
                            local2.d(s, sb2.toString());
                            return null;
                        }
                        final String settings = hashMap.get(component2);
                        wd4.a((Object)t, "localUpdatedAt");
                        final HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, t);
                        if (settings != null) {
                            hybridPresetAppSetting.setSettings(settings);
                        }
                        list2.add(hybridPresetAppSetting);
                        continue Label_0096;
                    }
                }
                MicroApp next = null;
                continue;
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(':');
        sb3.append(m);
        final HybridPreset hybridPreset = new HybridPreset(sb3.toString(), "Active Preset", m, list2, true);
        final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        m = MigrationManager.m;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("legacy active createdAt ");
        sb4.append(activePreset.getCreateAt());
        sb4.append(" updatedAt ");
        sb4.append(activePreset.getUpdateAt());
        local3.d(m, sb4.toString());
        hybridPreset.setPinType(activePreset.getPinType());
        final long create = activePreset.getCreateAt();
        final long n = 1000;
        hybridPreset.setCreatedAt(sk2.t(new Date(create * n)));
        hybridPreset.setUpdatedAt(sk2.t(new Date(activePreset.getUpdateAt() * n)));
        final ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        s = MigrationManager.m;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("active preset createdAt ");
        sb5.append(hybridPreset.getCreatedAt());
        sb5.append(" updatedAt ");
        sb5.append(hybridPreset.getUpdatedAt());
        local4.d(s, sb5.toString());
        return hybridPreset;
    }
    
    @DexReplace
    public final void a() {
        final MFUser currentUser = this.b.getCurrentUser();
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String m = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("migrateFor2Dot0 currentUser ");
        sb.append(currentUser);
        local.d(m, sb.toString());
        if (currentUser == null && !TextUtils.isEmpty((CharSequence)ks3.a().f((Context)this.e))) {
            this.a.w(ks3.a().f((Context)this.e));
            final MFUser mfUser = new MFUser();
            mfUser.setUserAccessToken(ks3.a().f((Context)this.e));
            mfUser.setUserId(ks3.a().m((Context)this.e));
            mfUser.setRegisterDate(ks3.a().d((Context)this.e));
            mfUser.setFirstName(ks3.a().j((Context)this.e));
            mfUser.setLastName(ks3.a().n((Context)this.e));
            mfUser.setCreatedAt(ks3.a().a((Context)this.e));
            mfUser.setUpdatedAt(ks3.a().e((Context)this.e));
            mfUser.setEmail(ks3.a().i((Context)this.e));
            mfUser.setAuthType(ks3.a().g((Context)this.e));
            try {
                if (!TextUtils.isEmpty((CharSequence)ks3.a().s((Context)this.e))) {
                    String s = ks3.a().s((Context)this.e);
                    if (s == null) {
                        s = "0";
                    }
                    final Integer value = Integer.valueOf(s);
                    wd4.a((Object)value, "Integer.valueOf(MFProfil…                  ?: \"0\")");
                    mfUser.setWeightInGrams(value);
                }
                else {
                    mfUser.setWeightInGrams(0);
                }
                if (!TextUtils.isEmpty((CharSequence)ks3.a().l((Context)this.e))) {
                    String l = ks3.a().l((Context)this.e);
                    if (l == null) {
                        l = "0";
                    }
                    final Integer value2 = Integer.valueOf(l);
                    wd4.a((Object)value2, "Integer.valueOf(MFProfil…                  ?: \"0\")");
                    mfUser.setHeightInCentimeters(value2);
                }
                else {
                    mfUser.setHeightInCentimeters(0);
                }
                if (!TextUtils.isEmpty((CharSequence)ks3.a().c((Context)this.e))) {
                    final Boolean value3 = Boolean.valueOf(ks3.a().c((Context)this.e));
                    wd4.a((Object)value3, "java.lang.Boolean.valueO…gistrationComplete(mApp))");
                    mfUser.setRegistrationComplete(value3);
                }
                else {
                    mfUser.setRegistrationComplete(true);
                }
            }
            catch (Exception ex) {
                final String i = MigrationManager.m;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Exception when parse weight, height... e=");
                sb2.append(ex);
                sb2.append(", keep going to migrate other data");
                zw.a(0, i, sb2.toString());
            }
            mfUser.setDistanceUnit(ks3.a().p((Context)this.e));
            mfUser.setHeightUnit(ks3.a().q((Context)this.e));
            mfUser.setWeightUnit(ks3.a().r((Context)this.e));
            mfUser.setBirthday(ks3.a().h((Context)this.e));
            mfUser.setGender(ks3.a().k((Context)this.e).toString());
            mfUser.setProfilePicture(ks3.a().o((Context)this.e));
            mfUser.setOnboardingComplete(ks3.a().t((Context)this.e));
            mfUser.setIntegrations(ks3.a().b((Context)this.e));
            mfUser.setEmailOptIn(ks3.a().v((Context)this.e));
            mfUser.setDiagnosticEnabled(ks3.a().u((Context)this.e));
            mfUser.setActiveDeviceId(this.a.t());
            en2.p.a().n().b(mfUser);
            new or3().a();
        }
        if (!TextUtils.isEmpty((CharSequence)this.a.t())) {
            final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            final String j = MigrationManager.m;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("migrateFor2Dot0 legacySerial=");
            final String t = this.a.t();
            if (t == null) {
                wd4.a();
                throw null;
            }
            sb3.append(t);
            local2.d(j, sb3.toString());
            final fn2 a = this.a;
            a.o(a.t());
        }
        final String e = this.e.e();
        final String k = ks3.a().m((Context)this.e);
        if (k != null) {
            wd4.a((Object)k, "MFProfileUtils.getInstance().getUserId(mApp)!!");
            final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            final String m2 = MigrationManager.m;
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("migrateFor2Dot0 userID=");
            sb4.append(k);
            sb4.append(" activeSerial ");
            sb4.append(e);
            local3.d(m2, sb4.toString());
            if (!TextUtils.isEmpty((CharSequence)k)) {
                final en2 a2 = en2.p.a();
                final String m3 = ks3.a().m((Context)this.e);
                if (m3 == null) {
                    wd4.a();
                    throw null;
                }
                wd4.a((Object)m3, "MFProfileUtils.getInstance().getUserId(mApp)!!");
                a2.a(m3).getDeviceById(this.a.t());
            }
            en2.p.a().b().getAllContactGroups();
            en2.p.a().a().getAllAppFilters();
            final fn2 a3 = this.a;
            a3.f(a3.r());
            final DeviceProvider d = en2.p.a().d();
            final List<DeviceModel> allDevice = d.getAllDevice();
            final ArrayList<Device> list = new ArrayList<Device>();
            wd4.a((Object)allDevice, "deviceList");
            for (final DeviceModel deviceModel : allDevice) {
                final String deviceId = deviceModel.getDeviceId();
                if (deviceId == null) {
                    wd4.a();
                    throw null;
                }
                // throw null;
                Device device = new Device(
                        deviceId,
                        deviceModel.getMacAddress(),
                        deviceModel.getSku(),
                        deviceModel.getFirmwareRevision(),
                        deviceModel.getBatteryLevel(),
                        50,
                        false,
                        64, null);
                device.setCreatedAt(deviceModel.getCreatedAt());
                device.setUpdatedAt(deviceModel.getUpdateAt());
                device.setMajor(deviceModel.getMajor());
                device.setMinor(deviceModel.getMinor());
                device.setVibrationStrength(deviceModel.getVibrationStrength());
                device.setActive(wd4.a((Object)deviceModel.getDeviceId(), (Object)e));
                final ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                final String m4 = MigrationManager.m;
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("migrateFor2Dot0 migrate success device ");
                sb5.append(device);
                local4.d(m4, sb5.toString());
                list.add(device);
            }
            if (list.isEmpty() ^ true) {
                this.h.addAllDevice(list);
            }
            final List<MappingSet> mappingSetByType = d.getMappingSetByType(MappingSet.MappingSetType.USER_SAVED);
            final ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            final String m5 = MigrationManager.m;
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("migrateFor2Dot0 userSavedMappingSet ");
            Integer value4;
            if (mappingSetByType != null) {
                value4 = mappingSetByType.size();
            }
            else {
                value4 = null;
            }
            sb6.append(value4);
            local5.d(m5, sb6.toString());
            if (allDevice.isEmpty() ^ true) {
                final LegacySecondTimezoneSetting activeSecondTimezone = en2.p.a().k().getActiveSecondTimezone();
                final MFUser currentUser2 = this.b.getCurrentUser();
                final ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                final String m6 = MigrationManager.m;
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("migrateFor2Dot0 2ndTzSetting ");
                String timezoneCityName;
                if (activeSecondTimezone != null) {
                    timezoneCityName = activeSecondTimezone.getTimezoneCityName();
                }
                else {
                    timezoneCityName = null;
                }
                sb7.append(timezoneCityName);
                local6.d(m6, sb7.toString());
                for (final DeviceModel deviceModel2 : allDevice) {
                    final MFDeviceFamily deviceFamily = MFDeviceFamily.getDeviceFamily(deviceModel2.getDeviceId());
                    final MappingSet activeMappingSetByDeviceFamily = d.getActiveMappingSetByDeviceFamily(deviceFamily);
                    final ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                    final String m7 = MigrationManager.m;
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append("migrateFor2Dot0 activeMappingSet of ");
                    sb8.append(deviceModel2.getDeviceId());
                    sb8.append(" deviceFamily ");
                    sb8.append(deviceFamily);
                    sb8.append(" name ");
                    wd4.a((Object)activeMappingSetByDeviceFamily, "mappingSet");
                    sb8.append(activeMappingSetByDeviceFamily.getName());
                    local7.d(m7, sb8.toString());
                    final List<Mapping> mappingList = activeMappingSetByDeviceFamily.getMappingList();
                    wd4.a((Object)mappingList, "mappingSet.mappingList");
                    final ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                    final String m8 = MigrationManager.m;
                    final StringBuilder sb9 = new StringBuilder();
                    sb9.append("migrateFor2Dot0 mappingList=");
                    sb9.append(mappingList);
                    local8.d(m8, sb9.toString());
                    String userId = null;
                    Label_1657: {
                        if (currentUser2 != null) {
                            userId = currentUser2.getUserId();
                            if (userId != null) {
                                break Label_1657;
                            }
                        }
                        userId = "";
                    }
                    final String deviceId2 = deviceModel2.getDeviceId();
                    if (deviceId2 == null) {
                        wd4.a();
                        throw null;
                    }
                    final HybridPreset a4 = this.a(userId, deviceId2, activeMappingSetByDeviceFamily, activeSecondTimezone);
                    if (a4 != null) {
                        final ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                        final String m9 = MigrationManager.m;
                        final StringBuilder sb10 = new StringBuilder();
                        sb10.append("migrateFor2Dot0 activeHybridPreset ");
                        sb10.append(a4);
                        local9.d(m9, sb10.toString());
                        this.i.presetDao().upsertPreset(a4);
                    }
                    final ArrayList<HybridPreset> list2 = new ArrayList<HybridPreset>();
                    wd4.a((Object)mappingSetByType, "savedMappingSetList");
                    for (final MappingSet set : mappingSetByType) {
                        wd4.a((Object)set, "it");
                        if (set.getDeviceFamily() == deviceFamily) {
                            final ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                            final String m10 = MigrationManager.m;
                            final StringBuilder sb11 = new StringBuilder();
                            sb11.append("migrateFor2Dot0 convert userMappingSet ");
                            sb11.append(set.getName());
                            sb11.append(" to hybridPreset");
                            local10.d(m10, sb11.toString());
                            final String deviceId3 = deviceModel2.getDeviceId();
                            if (deviceId3 == null) {
                                wd4.a();
                                throw null;
                            }
                            final HybridPreset a5 = this.a(deviceId3, set, d, activeSecondTimezone);
                            if (a5 == null) {
                                continue;
                            }
                            list2.add(a5);
                        }
                    }
                    if (list2.isEmpty() ^ true) {
                        this.i.presetDao().upsertPresetList(list2);
                    }
                    if (!wd4.a((Object)deviceModel2.getDeviceId(), (Object)e)) {
                        continue;
                    }
                    final ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                    final String m11 = MigrationManager.m;
                    final StringBuilder sb12 = new StringBuilder();
                    sb12.append("migrateFor2Dot0 ");
                    sb12.append(deviceModel2.getDeviceId());
                    sb12.append(" is active, download device setting if possible");
                    local11.d(m11, sb12.toString());
                    throw null;
                    // kg4.b(mh4.a(zh4.b()), null, null, (kd4)new MigrationManager$migrateFor2Dot0.MigrationManager$migrateFor2Dot0$Anon3(this, (kc4)null), 3, null);
                }
            }
            final List<AppFilter> allAppFilters = en2.p.a().a().getAllAppFilters();
            final ArrayList<AppFilter> list3 = new ArrayList<AppFilter>();
            if (allAppFilters != null && (allAppFilters.isEmpty() ^ true)) {
                for (final AppFilter appFilter : allAppFilters) {
                    final String name = AppType.ALL_CALLS.name();
                    wd4.a((Object)appFilter, "appFilter");
                    if (cg4.b(name, appFilter.getType(), true) || cg4.b(AppType.ALL_SMS.name(), appFilter.getType(), true)) {
                        final ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                        final String m12 = MigrationManager.m;
                        final StringBuilder sb13 = new StringBuilder();
                        sb13.append("migrateFor2Dot0 Add app filter=");
                        sb13.append(appFilter.getName());
                        local12.d(m12, sb13.toString());
                        list3.add(appFilter);
                        this.c.removeAppFilter(appFilter);
                    }
                }
            }
            if (list3.isEmpty() ^ true) {
                for (final AppFilter appFilter2 : list3) {
                    final ContactGroup contactGroup = new ContactGroup();
                    contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                    wd4.a((Object)appFilter2, "appFilter");
                    contactGroup.setHour(appFilter2.getHour());
                    this.c.saveContactGroup(contactGroup);
                    final ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                    final String m13 = MigrationManager.m;
                    final StringBuilder sb14 = new StringBuilder();
                    sb14.append("migrateFor2Dot0 Save contact group=");
                    sb14.append(contactGroup);
                    local13.d(m13, sb14.toString());
                    final Contact contact = new Contact();
                    final PhoneNumber phoneNumber = new PhoneNumber();
                    if (cg4.b(AppType.ALL_CALLS.packageName, appFilter2.getName(), true)) {
                        contact.setContactId(-100);
                        contact.setFirstName(AppType.ALL_CALLS.packageName);
                        contact.setUseCall(true);
                        phoneNumber.setNumber("-1234");
                    }
                    if (cg4.b(AppType.ALL_SMS.packageName, appFilter2.getName(), true)) {
                        contact.setContactId(-200);
                        contact.setFirstName(AppType.ALL_SMS.packageName);
                        contact.setUseSms(true);
                        phoneNumber.setNumber("-5678");
                    }
                    final ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
                    final String m14 = MigrationManager.m;
                    final StringBuilder sb15 = new StringBuilder();
                    sb15.append("migrateFor2Dot0 Save contact=");
                    sb15.append(contact);
                    local14.d(m14, sb15.toString());
                    final ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
                    final String m15 = MigrationManager.m;
                    final StringBuilder sb16 = new StringBuilder();
                    sb16.append("migrateFor2Dot0 Save phone number=");
                    sb16.append(phoneNumber);
                    local15.d(m15, sb16.toString());
                    contact.setContactGroup(contactGroup);
                    this.c.saveContact(contact);
                    phoneNumber.setContact(contact);
                    this.c.savePhoneNumber(phoneNumber);
                }
            }
            final fn2 a6 = this.a;
            a6.d(a6.u());
            final fn2 a7 = this.a;
            a7.u(a7.s());
            this.c();
            return;
        }
        wd4.a();
        throw null;
    }
    
    @DexIgnore
    public final void a(final String s) {
        this.l.getAllAlarmIgnoreDeletePinType();
    }
    
    @DexIgnore
    public final void b() {
        this.a.c(System.currentTimeMillis());
        this.a.b(0);
        this.a.r(true);
    }
    
    @DexIgnore
    public final void b(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   com/google/gson/Gson.<init>:()V
        //     7: astore_2       
        //     8: aload_0        
        //     9: getfield        com/portfolio/platform/MigrationManager.e:Lcom/portfolio/platform/PortfolioApp;
        //    12: invokevirtual   com/portfolio/platform/PortfolioApp.e:()Ljava/lang/String;
        //    15: astore_3       
        //    16: getstatic       com/fossil/blesdk/obfuscated/en2.p:Lcom/fossil/blesdk/obfuscated/en2$a;
        //    19: invokevirtual   com/fossil/blesdk/obfuscated/en2$a.a:()Lcom/fossil/blesdk/obfuscated/en2;
        //    22: invokevirtual   com/fossil/blesdk/obfuscated/en2.d:()Lcom/portfolio/platform/data/legacy/threedotzero/DeviceProvider;
        //    25: astore          4
        //    27: aload           4
        //    29: invokeinterface com/portfolio/platform/data/legacy/threedotzero/DeviceProvider.getAllDevice:()Ljava/util/List;
        //    34: astore          5
        //    36: iconst_1       
        //    37: anewarray       Ljava/lang/String;
        //    40: dup            
        //    41: iconst_0       
        //    42: ldc_w           "general"
        //    45: aastore        
        //    46: invokestatic    com/fossil/blesdk/obfuscated/ob4.a:([Ljava/lang/Object;)Ljava/util/ArrayList;
        //    49: astore          6
        //    51: new             Ljava/util/ArrayList;
        //    54: dup            
        //    55: invokespecial   java/util/ArrayList.<init>:()V
        //    58: astore          7
        //    60: new             Ljava/util/ArrayList;
        //    63: dup            
        //    64: invokespecial   java/util/ArrayList.<init>:()V
        //    67: astore          8
        //    69: getstatic       com/fossil/blesdk/obfuscated/en2.p:Lcom/fossil/blesdk/obfuscated/en2$a;
        //    72: invokevirtual   com/fossil/blesdk/obfuscated/en2$a.a:()Lcom/fossil/blesdk/obfuscated/en2;
        //    75: invokevirtual   com/fossil/blesdk/obfuscated/en2.h:()Lcom/fossil/blesdk/obfuscated/xn2;
        //    78: invokeinterface com/fossil/blesdk/obfuscated/xn2.d:()Ljava/util/List;
        //    83: astore          9
        //    85: new             Ljava/util/HashMap;
        //    88: dup            
        //    89: invokespecial   java/util/HashMap.<init>:()V
        //    92: astore          10
        //    94: aload_0        
        //    95: aload_1        
        //    96: invokevirtual   com/portfolio/platform/MigrationManager.a:(Ljava/lang/String;)V
        //    99: aload           5
        //   101: ldc_w           "allDevices"
        //   104: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   107: aload           8
        //   109: astore          11
        //   111: aload           7
        //   113: astore          12
        //   115: aload           5
        //   117: astore          13
        //   119: aload           5
        //   121: invokeinterface java/util/Collection.isEmpty:()Z
        //   126: iconst_1       
        //   127: ixor           
        //   128: ifeq            1757
        //   131: aload           9
        //   133: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   138: astore          13
        //   140: aload           13
        //   142: invokeinterface java/util/Iterator.hasNext:()Z
        //   147: ifeq            897
        //   150: aload           13
        //   152: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   157: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/MicroAppSetting;
        //   160: astore          11
        //   162: aload           11
        //   164: ldc_w           "microAppSetting"
        //   167: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   170: aload           11
        //   172: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getMicroAppId:()Ljava/lang/String;
        //   175: astore          12
        //   177: aload           12
        //   179: getstatic       com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.UAPP_GOAL_TRACKING_ID:Lcom/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID;
        //   182: invokevirtual   com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.getValue:()Ljava/lang/String;
        //   185: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   188: ifeq            250
        //   191: aload_2        
        //   192: aload           11
        //   194: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getSetting:()Ljava/lang/String;
        //   197: ldc_w           Lcom/portfolio/platform/data/legacy/threedotzero/LegacyGoalTrackingSettings;.class
        //   200: invokevirtual   com/google/gson/Gson.a:(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
        //   203: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/LegacyGoalTrackingSettings;
        //   206: astore          9
        //   208: aload           9
        //   210: ifnull          247
        //   213: invokestatic    com/fossil/blesdk/obfuscated/zh4.b:()Lcom/fossil/blesdk/obfuscated/gh4;
        //   216: invokestatic    com/fossil/blesdk/obfuscated/mh4.a:(Lkotlin/coroutines/CoroutineContext;)Lcom/fossil/blesdk/obfuscated/lh4;
        //   219: astore          14
        //   221: new             Lcom/portfolio/platform/MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1;
        //   224: astore          12
        //   226: aload           12
        //   228: aload           9
        //   230: aconst_null    
        //   231: aload_0        
        //   232: invokespecial   com/portfolio/platform/MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1.<init>:(Lcom/portfolio/platform/data/legacy/threedotzero/LegacyGoalTrackingSettings;Lcom/fossil/blesdk/obfuscated/kc4;Lcom/portfolio/platform/MigrationManager;)V
        //   235: aload           14
        //   237: aconst_null    
        //   238: aconst_null    
        //   239: aload           12
        //   241: iconst_3       
        //   242: aconst_null    
        //   243: invokestatic    com/fossil/blesdk/obfuscated/kg4.b:(Lcom/fossil/blesdk/obfuscated/lh4;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lcom/fossil/blesdk/obfuscated/kd4;ILjava/lang/Object;)Lcom/fossil/blesdk/obfuscated/ri4;
        //   246: pop            
        //   247: goto            894
        //   250: aload           12
        //   252: getstatic       com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.UAPP_RING_PHONE:Lcom/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID;
        //   255: invokevirtual   com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.getValue:()Ljava/lang/String;
        //   258: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   261: ifeq            583
        //   264: aload_2        
        //   265: aload           11
        //   267: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getSetting:()Ljava/lang/String;
        //   270: ldc_w           Lcom/portfolio/platform/data/legacy/threedotzero/LegacyRingtoneSetting;.class
        //   273: invokevirtual   com/google/gson/Gson.a:(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
        //   276: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/LegacyRingtoneSetting;
        //   279: astore          12
        //   281: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //   284: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //   287: astore          15
        //   289: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //   292: astore          9
        //   294: new             Ljava/lang/StringBuilder;
        //   297: astore          14
        //   299: aload           14
        //   301: invokespecial   java/lang/StringBuilder.<init>:()V
        //   304: aload           14
        //   306: ldc_w           "legacyRingtoneSeting "
        //   309: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   312: pop            
        //   313: aload           14
        //   315: aload           12
        //   317: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   320: pop            
        //   321: aload           15
        //   323: aload           9
        //   325: aload           14
        //   327: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   330: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   335: aload           12
        //   337: ifnull          405
        //   340: new             Lcom/portfolio/platform/data/model/Ringtone;
        //   343: astore          9
        //   345: aload           12
        //   347: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacyRingtoneSetting.getName:()Ljava/lang/String;
        //   350: astore          14
        //   352: aload           14
        //   354: ldc_w           "legacyRingtoneSetting.name"
        //   357: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   360: aload           9
        //   362: aload           14
        //   364: aload           12
        //   366: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacyRingtoneSetting.getId:()Ljava/lang/String;
        //   369: invokespecial   com/portfolio/platform/data/model/Ringtone.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   372: aload           11
        //   374: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getMicroAppId:()Ljava/lang/String;
        //   377: astore          12
        //   379: aload           12
        //   381: ldc_w           "microAppSetting.microAppId"
        //   384: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   387: aload           10
        //   389: aload           12
        //   391: aload           9
        //   393: invokestatic    com/fossil/blesdk/obfuscated/sj2.a:(Ljava/lang/Object;)Ljava/lang/String;
        //   396: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   401: pop            
        //   402: goto            247
        //   405: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //   408: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //   411: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //   414: ldc_w           "No legacy ringtone setting, set default instead"
        //   417: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   422: getstatic       com/portfolio/platform/helper/AppHelper.f:Lcom/portfolio/platform/helper/AppHelper$Companion;
        //   425: invokevirtual   com/portfolio/platform/helper/AppHelper$Companion.c:()Ljava/util/List;
        //   428: astore          12
        //   430: aload           12
        //   432: ifnull          247
        //   435: aload           12
        //   437: invokeinterface java/util/Collection.isEmpty:()Z
        //   442: iconst_1       
        //   443: ixor           
        //   444: ifeq            247
        //   447: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //   450: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //   453: astore          9
        //   455: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //   458: astore          14
        //   460: new             Ljava/lang/StringBuilder;
        //   463: dup            
        //   464: invokespecial   java/lang/StringBuilder.<init>:()V
        //   467: astore          15
        //   469: aload           15
        //   471: ldc_w           "defaultRingtone "
        //   474: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   477: pop            
        //   478: aload           15
        //   480: aload           12
        //   482: iconst_0       
        //   483: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   488: checkcast       Lcom/portfolio/platform/data/model/Ringtone;
        //   491: invokevirtual   com/portfolio/platform/data/model/Ringtone.getRingtoneName:()Ljava/lang/String;
        //   494: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   497: pop            
        //   498: aload           9
        //   500: aload           14
        //   502: aload           15
        //   504: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   507: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   512: new             Lcom/portfolio/platform/data/model/Ringtone;
        //   515: astore          9
        //   517: aload           9
        //   519: aload           12
        //   521: iconst_0       
        //   522: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   527: checkcast       Lcom/portfolio/platform/data/model/Ringtone;
        //   530: invokevirtual   com/portfolio/platform/data/model/Ringtone.getRingtoneName:()Ljava/lang/String;
        //   533: aload           12
        //   535: iconst_0       
        //   536: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   541: checkcast       Lcom/portfolio/platform/data/model/Ringtone;
        //   544: invokevirtual   com/portfolio/platform/data/model/Ringtone.getRingtoneId:()Ljava/lang/String;
        //   547: invokespecial   com/portfolio/platform/data/model/Ringtone.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   550: aload           11
        //   552: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getMicroAppId:()Ljava/lang/String;
        //   555: astore          12
        //   557: aload           12
        //   559: ldc_w           "microAppSetting.microAppId"
        //   562: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   565: aload           10
        //   567: aload           12
        //   569: aload           9
        //   571: invokestatic    com/fossil/blesdk/obfuscated/sj2.a:(Ljava/lang/Object;)Ljava/lang/String;
        //   574: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   579: pop            
        //   580: goto            894
        //   583: aload           12
        //   585: getstatic       com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.UAPP_TIME2_ID:Lcom/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID;
        //   588: invokevirtual   com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.getValue:()Ljava/lang/String;
        //   591: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   594: ifeq            713
        //   597: aload_2        
        //   598: aload           11
        //   600: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getSetting:()Ljava/lang/String;
        //   603: ldc_w           Lcom/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting;.class
        //   606: invokevirtual   com/google/gson/Gson.a:(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
        //   609: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting;
        //   612: astore          15
        //   614: aload           15
        //   616: ifnull          894
        //   619: new             Lcom/portfolio/platform/data/model/setting/SecondTimezoneSetting;
        //   622: astore          14
        //   624: aload           15
        //   626: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting.getTimezoneCityName:()Ljava/lang/String;
        //   629: astore          9
        //   631: aload           9
        //   633: ldc_w           "it.timezoneCityName"
        //   636: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   639: aload           15
        //   641: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting.getTimezoneId:()Ljava/lang/String;
        //   644: astore          12
        //   646: aload           12
        //   648: ldc_w           "it.timezoneId"
        //   651: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   654: aload           15
        //   656: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting.getTimezoneOffset:()J
        //   659: l2i            
        //   660: istore          16
        //   662: aload           15
        //   664: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacySecondTimezoneSetting.getTimezoneId:()Ljava/lang/String;
        //   667: astore          15
        //   669: aload           15
        //   671: ldc_w           "it.timezoneId"
        //   674: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   677: aload           14
        //   679: aload           9
        //   681: aload           12
        //   683: iload           16
        //   685: aload           15
        //   687: invokespecial   com/portfolio/platform/data/model/setting/SecondTimezoneSetting.<init>:(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
        //   690: aload           10
        //   692: aload           11
        //   694: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getMicroAppId:()Ljava/lang/String;
        //   697: aload           14
        //   699: invokestatic    com/fossil/blesdk/obfuscated/sj2.a:(Ljava/lang/Object;)Ljava/lang/String;
        //   702: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   705: checkcast       Ljava/lang/String;
        //   708: astore          12
        //   710: goto            894
        //   713: aload           12
        //   715: getstatic       com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.UAPP_COMMUTE_TIME:Lcom/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID;
        //   718: invokevirtual   com/misfit/frameworks/buttonservice/model/microapp/MicroAppInstruction$MicroAppID.getValue:()Ljava/lang/String;
        //   721: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   724: ifeq            894
        //   727: aload_2        
        //   728: aload           11
        //   730: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getSetting:()Ljava/lang/String;
        //   733: ldc_w           Lcom/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings;.class
        //   736: invokevirtual   com/google/gson/Gson.a:(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
        //   739: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings;
        //   742: astore          14
        //   744: aload           14
        //   746: ifnull          894
        //   749: new             Lcom/portfolio/platform/data/model/setting/CommuteTimeSetting;
        //   752: astore          12
        //   754: aload           14
        //   756: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings.getDestination:()Ljava/lang/String;
        //   759: astore          9
        //   761: aload           9
        //   763: ldc_w           "it.destination"
        //   766: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   769: aload           12
        //   771: aload           9
        //   773: aload           14
        //   775: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings.getTimeFormat:()Lcom/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings$TIME_FORMAT;
        //   778: invokevirtual   java/lang/Enum.toString:()Ljava/lang/String;
        //   781: aload           14
        //   783: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/LegacyCommuteTimeSettings.isIsAvoidTolls:()Z
        //   786: aconst_null    
        //   787: bipush          8
        //   789: aconst_null    
        //   790: invokespecial   com/portfolio/platform/data/model/setting/CommuteTimeSetting.<init>:(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/fossil/blesdk/obfuscated/rd4;)V
        //   793: aload           10
        //   795: aload           11
        //   797: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getMicroAppId:()Ljava/lang/String;
        //   800: aload           12
        //   802: invokestatic    com/fossil/blesdk/obfuscated/sj2.a:(Ljava/lang/Object;)Ljava/lang/String;
        //   805: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   808: checkcast       Ljava/lang/String;
        //   811: astore          12
        //   813: goto            894
        //   816: astore          12
        //   818: goto            823
        //   821: astore          12
        //   823: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //   826: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //   829: astore          9
        //   831: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //   834: astore          14
        //   836: new             Ljava/lang/StringBuilder;
        //   839: dup            
        //   840: invokespecial   java/lang/StringBuilder.<init>:()V
        //   843: astore          12
        //   845: aload           12
        //   847: ldc_w           "Exception when parsing micro app setting "
        //   850: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   853: pop            
        //   854: aload           11
        //   856: ifnull          869
        //   859: aload           11
        //   861: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroAppSetting.getSetting:()Ljava/lang/String;
        //   864: astore          11
        //   866: goto            872
        //   869: aconst_null    
        //   870: astore          11
        //   872: aload           12
        //   874: aload           11
        //   876: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   879: pop            
        //   880: aload           9
        //   882: aload           14
        //   884: aload           12
        //   886: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   889: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   894: goto            140
        //   897: new             Ljava/util/ArrayList;
        //   900: dup            
        //   901: invokespecial   java/util/ArrayList.<init>:()V
        //   904: astore          13
        //   906: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   909: astore          11
        //   911: aload           11
        //   913: ldc             "Calendar.getInstance()"
        //   915: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   918: aload           11
        //   920: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   923: invokestatic    com/fossil/blesdk/obfuscated/sk2.t:(Ljava/util/Date;)Ljava/lang/String;
        //   926: astore          12
        //   928: aload           10
        //   930: invokeinterface java/util/Map.entrySet:()Ljava/util/Set;
        //   935: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //   940: astore_2       
        //   941: aload_2        
        //   942: invokeinterface java/util/Iterator.hasNext:()Z
        //   947: ifeq            1017
        //   950: aload_2        
        //   951: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   956: checkcast       Ljava/util/Map$Entry;
        //   959: astore          9
        //   961: aload           9
        //   963: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //   968: checkcast       Ljava/lang/String;
        //   971: astore          11
        //   973: aload           9
        //   975: invokeinterface java/util/Map$Entry.getValue:()Ljava/lang/Object;
        //   980: checkcast       Ljava/lang/String;
        //   983: astore          9
        //   985: aload           12
        //   987: ldc_w           "updatedAt"
        //   990: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //   993: aload           13
        //   995: new             Lcom/portfolio/platform/data/model/microapp/MicroAppLastSetting;
        //   998: dup            
        //   999: aload           11
        //  1001: aload           12
        //  1003: aload           9
        //  1005: invokespecial   com/portfolio/platform/data/model/microapp/MicroAppLastSetting.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //  1008: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1013: pop            
        //  1014: goto            941
        //  1017: aload_0        
        //  1018: getfield        com/portfolio/platform/MigrationManager.j:Lcom/portfolio/platform/data/source/MicroAppLastSettingRepository;
        //  1021: aload           13
        //  1023: invokevirtual   com/portfolio/platform/data/source/MicroAppLastSettingRepository.upsertMicroAppLastSettingList:(Ljava/util/List;)V
        //  1026: aload           5
        //  1028: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //  1033: astore_2       
        //  1034: aload           8
        //  1036: astore          11
        //  1038: aload           7
        //  1040: astore          12
        //  1042: aload           5
        //  1044: astore          13
        //  1046: aload_2        
        //  1047: invokeinterface java/util/Iterator.hasNext:()Z
        //  1052: ifeq            1757
        //  1055: aload_2        
        //  1056: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1061: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/DeviceModel;
        //  1064: astore          12
        //  1066: aload           5
        //  1068: astore          13
        //  1070: aload           4
        //  1072: aload           12
        //  1074: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1077: invokeinterface com/portfolio/platform/data/legacy/threedotzero/DeviceProvider.getListMicroApp:(Ljava/lang/String;)Ljava/util/List;
        //  1082: astore          9
        //  1084: aload           5
        //  1086: astore          13
        //  1088: aload           9
        //  1090: ldc_w           "legacyMicroApps"
        //  1093: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1096: aload           5
        //  1098: astore          13
        //  1100: aload           9
        //  1102: invokeinterface java/lang/Iterable.iterator:()Ljava/util/Iterator;
        //  1107: astore          11
        //  1109: aload           5
        //  1111: astore          13
        //  1113: aload           11
        //  1115: invokeinterface java/util/Iterator.hasNext:()Z
        //  1120: istore          17
        //  1122: iload           17
        //  1124: ifeq            1260
        //  1127: aload           11
        //  1129: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1134: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/MicroApp;
        //  1137: astore          18
        //  1139: new             Lcom/portfolio/platform/data/model/room/microapp/MicroApp;
        //  1142: astore          14
        //  1144: aload           18
        //  1146: ldc_w           "it"
        //  1149: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1152: aload           18
        //  1154: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroApp.getAppId:()Ljava/lang/String;
        //  1157: astore          13
        //  1159: aload           13
        //  1161: ldc_w           "it.appId"
        //  1164: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1167: aload           18
        //  1169: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroApp.getName:()Ljava/lang/String;
        //  1172: astore          15
        //  1174: aload           15
        //  1176: ldc_w           "it.name"
        //  1179: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1182: aload           12
        //  1184: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1187: astore          19
        //  1189: aload           19
        //  1191: ifnull          1250
        //  1194: aload           18
        //  1196: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroApp.getDescription:()Ljava/lang/String;
        //  1199: astore          20
        //  1201: aload           20
        //  1203: ldc_w           "it.description"
        //  1206: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1209: aload           18
        //  1211: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/MicroApp.getIconUrl:()Ljava/lang/String;
        //  1214: astore          18
        //  1216: aload           14
        //  1218: aload           13
        //  1220: aload           15
        //  1222: ldc_w           ""
        //  1225: aload           19
        //  1227: aload           6
        //  1229: aload           20
        //  1231: ldc_w           ""
        //  1234: aload           18
        //  1236: invokespecial   com/portfolio/platform/data/model/room/microapp/MicroApp.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //  1239: aload           7
        //  1241: aload           14
        //  1243: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  1246: pop            
        //  1247: goto            1109
        //  1250: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:()V
        //  1253: aconst_null    
        //  1254: athrow         
        //  1255: astore          11
        //  1257: goto            1692
        //  1260: aload           5
        //  1262: astore          13
        //  1264: aload           4
        //  1266: aload           12
        //  1268: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1271: invokeinterface com/portfolio/platform/data/legacy/threedotzero/DeviceProvider.getActivePreset:(Ljava/lang/String;)Lcom/portfolio/platform/data/legacy/threedotzero/ActivePreset;
        //  1276: astore          11
        //  1278: aload           4
        //  1280: invokeinterface com/portfolio/platform/data/legacy/threedotzero/DeviceProvider.getAllSavedPresets:()Ljava/util/List;
        //  1285: astore          14
        //  1287: aload           11
        //  1289: ifnull          1396
        //  1292: aload           12
        //  1294: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1297: astore          15
        //  1299: aload           15
        //  1301: ifnull          1391
        //  1304: aload_0        
        //  1305: aload           9
        //  1307: aload_1        
        //  1308: aload           15
        //  1310: aload           11
        //  1312: aload           10
        //  1314: invokevirtual   com/portfolio/platform/MigrationManager.a:(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/portfolio/platform/data/legacy/threedotzero/ActivePreset;Ljava/util/HashMap;)Lcom/portfolio/platform/data/model/room/microapp/HybridPreset;
        //  1317: astore          5
        //  1319: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  1322: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  1325: astore          19
        //  1327: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  1330: astore          20
        //  1332: new             Ljava/lang/StringBuilder;
        //  1335: astore          15
        //  1337: aload           15
        //  1339: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1342: aload           15
        //  1344: ldc_w           "convert active preset success "
        //  1347: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1350: pop            
        //  1351: aload           15
        //  1353: aload           5
        //  1355: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //  1358: pop            
        //  1359: aload           19
        //  1361: aload           20
        //  1363: aload           15
        //  1365: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1368: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  1373: aload           5
        //  1375: ifnull          1396
        //  1378: aload           8
        //  1380: aload           5
        //  1382: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1387: pop            
        //  1388: goto            1396
        //  1391: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:()V
        //  1394: aconst_null    
        //  1395: athrow         
        //  1396: aload           14
        //  1398: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //  1403: astore          5
        //  1405: aload           5
        //  1407: invokeinterface java/util/Iterator.hasNext:()Z
        //  1412: ifeq            1569
        //  1415: aload           5
        //  1417: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1422: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/SavedPreset;
        //  1425: astore          14
        //  1427: aload           14
        //  1429: ldc_w           "preset"
        //  1432: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1435: aload           14
        //  1437: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/SavedPreset.getId:()Ljava/lang/String;
        //  1440: astore          15
        //  1442: aload           11
        //  1444: ldc_w           "legacyActivePreset"
        //  1447: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  1450: aload           15
        //  1452: aload           11
        //  1454: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/ActivePreset.getOriginalId:()Ljava/lang/String;
        //  1457: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //  1460: ifeq            1466
        //  1463: goto            1405
        //  1466: aload           12
        //  1468: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1471: astore          15
        //  1473: aload           15
        //  1475: ifnull          1564
        //  1478: aload_0        
        //  1479: aload           9
        //  1481: aload           15
        //  1483: aload           14
        //  1485: aload           10
        //  1487: invokevirtual   com/portfolio/platform/MigrationManager.a:(Ljava/util/List;Ljava/lang/String;Lcom/portfolio/platform/data/legacy/threedotzero/SavedPreset;Ljava/util/HashMap;)Lcom/portfolio/platform/data/model/room/microapp/HybridPreset;
        //  1490: astore          15
        //  1492: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  1495: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  1498: astore          19
        //  1500: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  1503: astore          14
        //  1505: new             Ljava/lang/StringBuilder;
        //  1508: astore          20
        //  1510: aload           20
        //  1512: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1515: aload           20
        //  1517: ldc_w           "into "
        //  1520: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1523: pop            
        //  1524: aload           20
        //  1526: aload           15
        //  1528: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //  1531: pop            
        //  1532: aload           19
        //  1534: aload           14
        //  1536: aload           20
        //  1538: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1541: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  1546: aload           15
        //  1548: ifnull          1405
        //  1551: aload           8
        //  1553: aload           15
        //  1555: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //  1560: pop            
        //  1561: goto            1405
        //  1564: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:()V
        //  1567: aconst_null    
        //  1568: athrow         
        //  1569: aload           13
        //  1571: astore          5
        //  1573: aload           12
        //  1575: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1578: aload_3        
        //  1579: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //  1582: ifeq            1754
        //  1585: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  1588: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  1591: astore          5
        //  1593: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  1596: astore          11
        //  1598: new             Ljava/lang/StringBuilder;
        //  1601: astore          9
        //  1603: aload           9
        //  1605: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1608: aload           9
        //  1610: aload           12
        //  1612: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1615: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1618: pop            
        //  1619: aload           9
        //  1621: ldc_w           " is active, download device setting if possible"
        //  1624: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1627: pop            
        //  1628: aload           5
        //  1630: aload           11
        //  1632: aload           9
        //  1634: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1637: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  1642: invokestatic    com/fossil/blesdk/obfuscated/zh4.b:()Lcom/fossil/blesdk/obfuscated/gh4;
        //  1645: invokestatic    com/fossil/blesdk/obfuscated/mh4.a:(Lkotlin/coroutines/CoroutineContext;)Lcom/fossil/blesdk/obfuscated/lh4;
        //  1648: astore          5
        //  1650: new             Lcom/portfolio/platform/MigrationManager$migrateFor4Dot0$Anon7;
        //  1653: astore          11
        //  1655: aload           11
        //  1657: aload_0        
        //  1658: aconst_null    
        //  1659: invokespecial   com/portfolio/platform/MigrationManager$migrateFor4Dot0$Anon7.<init>:(Lcom/portfolio/platform/MigrationManager;Lcom/fossil/blesdk/obfuscated/kc4;)V
        //  1662: aload           5
        //  1664: aconst_null    
        //  1665: aconst_null    
        //  1666: aload           11
        //  1668: iconst_3       
        //  1669: aconst_null    
        //  1670: invokestatic    com/fossil/blesdk/obfuscated/kg4.b:(Lcom/fossil/blesdk/obfuscated/lh4;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lcom/fossil/blesdk/obfuscated/kd4;ILjava/lang/Object;)Lcom/fossil/blesdk/obfuscated/ri4;
        //  1673: pop            
        //  1674: aload           13
        //  1676: astore          5
        //  1678: goto            1754
        //  1681: astore          11
        //  1683: aload           13
        //  1685: astore          5
        //  1687: goto            1701
        //  1690: astore          11
        //  1692: goto            1701
        //  1695: astore          11
        //  1697: aload           13
        //  1699: astore          5
        //  1701: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  1704: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  1707: astore          13
        //  1709: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  1712: astore          9
        //  1714: new             Ljava/lang/StringBuilder;
        //  1717: dup            
        //  1718: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1721: astore          12
        //  1723: aload           12
        //  1725: ldc_w           "exception when migrate customize "
        //  1728: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1731: pop            
        //  1732: aload           12
        //  1734: aload           11
        //  1736: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //  1739: pop            
        //  1740: aload           13
        //  1742: aload           9
        //  1744: aload           12
        //  1746: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1749: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  1754: goto            1034
        //  1757: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  1760: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  1763: astore          7
        //  1765: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  1768: astore          5
        //  1770: new             Ljava/lang/StringBuilder;
        //  1773: dup            
        //  1774: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1777: astore_1       
        //  1778: aload_1        
        //  1779: ldc_w           "microAppList "
        //  1782: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1785: pop            
        //  1786: aload_1        
        //  1787: aload           12
        //  1789: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //  1792: pop            
        //  1793: aload_1        
        //  1794: ldc_w           " preset "
        //  1797: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1800: pop            
        //  1801: aload_1        
        //  1802: aload           11
        //  1804: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //  1807: pop            
        //  1808: aload           7
        //  1810: aload           5
        //  1812: aload_1        
        //  1813: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1816: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  1821: aload_0        
        //  1822: getfield        com/portfolio/platform/MigrationManager.i:Lcom/portfolio/platform/data/source/local/hybrid/microapp/HybridCustomizeDatabase;
        //  1825: new             Lcom/portfolio/platform/MigrationManager$b;
        //  1828: dup            
        //  1829: aload_0        
        //  1830: aload           12
        //  1832: aload           11
        //  1834: invokespecial   com/portfolio/platform/MigrationManager$b.<init>:(Lcom/portfolio/platform/MigrationManager;Ljava/util/ArrayList;Ljava/util/List;)V
        //  1837: invokevirtual   androidx/room/RoomDatabase.runInTransaction:(Ljava/lang/Runnable;)V
        //  1840: new             Ljava/util/ArrayList;
        //  1843: dup            
        //  1844: invokespecial   java/util/ArrayList.<init>:()V
        //  1847: astore_1       
        //  1848: aload           13
        //  1850: invokeinterface java/lang/Iterable.iterator:()Ljava/util/Iterator;
        //  1855: astore          5
        //  1857: aload           5
        //  1859: invokeinterface java/util/Iterator.hasNext:()Z
        //  1864: ifeq            2016
        //  1867: aload           5
        //  1869: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1874: checkcast       Lcom/portfolio/platform/data/legacy/threedotzero/DeviceModel;
        //  1877: astore          7
        //  1879: aload           7
        //  1881: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1884: astore          8
        //  1886: aload           8
        //  1888: ifnull          2011
        //  1891: new             Lcom/portfolio/platform/data/model/Device;
        //  1894: dup            
        //  1895: aload           8
        //  1897: aload           7
        //  1899: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getMacAddress:()Ljava/lang/String;
        //  1902: aload           7
        //  1904: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getSku:()Ljava/lang/String;
        //  1907: aload           7
        //  1909: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getFirmwareRevision:()Ljava/lang/String;
        //  1912: aload           7
        //  1914: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getBatteryLevel:()I
        //  1917: aconst_null    
        //  1918: iconst_0       
        //  1919: bipush          96
        //  1921: aconst_null    
        //  1922: invokespecial   com/portfolio/platform/data/model/Device.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;ZILcom/fossil/blesdk/obfuscated/rd4;)V
        //  1925: astore          8
        //  1927: aload           8
        //  1929: aload           7
        //  1931: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getCreatedAt:()Ljava/lang/String;
        //  1934: invokevirtual   com/portfolio/platform/data/model/Device.setCreatedAt:(Ljava/lang/String;)V
        //  1937: aload           8
        //  1939: aload           7
        //  1941: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getUpdateAt:()Ljava/lang/String;
        //  1944: invokevirtual   com/portfolio/platform/data/model/Device.setUpdatedAt:(Ljava/lang/String;)V
        //  1947: aload           8
        //  1949: aload           7
        //  1951: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getMajor:()I
        //  1954: invokevirtual   com/portfolio/platform/data/model/Device.setMajor:(I)V
        //  1957: aload           8
        //  1959: aload           7
        //  1961: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getMinor:()I
        //  1964: invokevirtual   com/portfolio/platform/data/model/Device.setMinor:(I)V
        //  1967: aload           8
        //  1969: aload_0        
        //  1970: getfield        com/portfolio/platform/MigrationManager.a:Lcom/fossil/blesdk/obfuscated/fn2;
        //  1973: aload           7
        //  1975: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1978: invokevirtual   com/fossil/blesdk/obfuscated/fn2.j:(Ljava/lang/String;)I
        //  1981: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1984: invokevirtual   com/portfolio/platform/data/model/Device.setVibrationStrength:(Ljava/lang/Integer;)V
        //  1987: aload           8
        //  1989: aload           7
        //  1991: invokevirtual   com/portfolio/platform/data/legacy/threedotzero/DeviceModel.getDeviceId:()Ljava/lang/String;
        //  1994: aload_3        
        //  1995: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //  1998: invokevirtual   com/portfolio/platform/data/model/Device.setActive:(Z)V
        //  2001: aload_1        
        //  2002: aload           8
        //  2004: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //  2007: pop            
        //  2008: goto            1857
        //  2011: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:()V
        //  2014: aconst_null    
        //  2015: athrow         
        //  2016: aload_1        
        //  2017: invokeinterface java/util/Collection.isEmpty:()Z
        //  2022: iconst_1       
        //  2023: ixor           
        //  2024: ifeq            2037
        //  2027: aload_0        
        //  2028: getfield        com/portfolio/platform/MigrationManager.h:Lcom/portfolio/platform/data/source/DeviceDao;
        //  2031: aload_1        
        //  2032: invokeinterface com/portfolio/platform/data/source/DeviceDao.addAllDevice:(Ljava/util/List;)V
        //  2037: getstatic       com/fossil/blesdk/obfuscated/en2.p:Lcom/fossil/blesdk/obfuscated/en2$a;
        //  2040: invokevirtual   com/fossil/blesdk/obfuscated/en2$a.b:()Lcom/fossil/blesdk/obfuscated/en2;
        //  2043: astore_1       
        //  2044: aload_1        
        //  2045: ifnull          2067
        //  2048: aload_1        
        //  2049: invokevirtual   com/fossil/blesdk/obfuscated/en2.b:()Lcom/fossil/wearables/fsl/contact/ContactProvider;
        //  2052: astore_1       
        //  2053: aload_1        
        //  2054: ifnull          2067
        //  2057: aload_1        
        //  2058: invokeinterface com/fossil/wearables/fsl/contact/ContactProvider.getAllContactGroups:()Ljava/util/List;
        //  2063: astore_1       
        //  2064: goto            2069
        //  2067: aconst_null    
        //  2068: astore_1       
        //  2069: aload_1        
        //  2070: ifnull          2333
        //  2073: aload_1        
        //  2074: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //  2079: astore_1       
        //  2080: aload_1        
        //  2081: invokeinterface java/util/Iterator.hasNext:()Z
        //  2086: ifeq            2329
        //  2089: aload_1        
        //  2090: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2095: checkcast       Lcom/fossil/wearables/fsl/contact/ContactGroup;
        //  2098: astore          5
        //  2100: aload           5
        //  2102: ldc_w           "group"
        //  2105: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  2108: aload           5
        //  2110: invokevirtual   com/fossil/wearables/fsl/contact/ContactGroup.getContacts:()Ljava/util/List;
        //  2113: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //  2118: astore          5
        //  2120: aload           5
        //  2122: invokeinterface java/util/Iterator.hasNext:()Z
        //  2127: ifeq            2080
        //  2130: aload           5
        //  2132: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2137: checkcast       Lcom/fossil/wearables/fsl/contact/Contact;
        //  2140: astore          7
        //  2142: aload           7
        //  2144: ldc_w           "contact"
        //  2147: invokestatic    com/fossil/blesdk/obfuscated/wd4.a:(Ljava/lang/Object;Ljava/lang/String;)V
        //  2150: aload           7
        //  2152: invokevirtual   com/fossil/wearables/fsl/contact/Contact.getContactId:()I
        //  2155: sipush          -200
        //  2158: if_icmpne       2240
        //  2161: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  2164: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  2167: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  2170: ldc_w           "Migrate Text from everyone"
        //  2173: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  2178: new             Lcom/fossil/wearables/fsl/contact/PhoneNumber;
        //  2181: dup            
        //  2182: invokespecial   com/fossil/wearables/fsl/contact/PhoneNumber.<init>:()V
        //  2185: astore          8
        //  2187: aload           7
        //  2189: getstatic       com/portfolio/platform/data/AppType.ALL_SMS:Lcom/portfolio/platform/data/AppType;
        //  2192: getfield        com/portfolio/platform/data/AppType.packageName:Ljava/lang/String;
        //  2195: invokevirtual   com/fossil/wearables/fsl/contact/Contact.setFirstName:(Ljava/lang/String;)V
        //  2198: aload           7
        //  2200: iconst_1       
        //  2201: invokevirtual   com/fossil/wearables/fsl/contact/Contact.setUseSms:(Z)V
        //  2204: aload           8
        //  2206: ldc_w           "-5678"
        //  2209: invokevirtual   com/fossil/wearables/fsl/contact/PhoneNumber.setNumber:(Ljava/lang/String;)V
        //  2212: aload_0        
        //  2213: getfield        com/portfolio/platform/MigrationManager.c:Lcom/portfolio/platform/data/source/NotificationsRepository;
        //  2216: aload           7
        //  2218: invokevirtual   com/portfolio/platform/data/source/NotificationsRepository.saveContact:(Lcom/fossil/wearables/fsl/contact/Contact;)V
        //  2221: aload           8
        //  2223: aload           7
        //  2225: invokevirtual   com/fossil/wearables/fsl/contact/PhoneNumber.setContact:(Lcom/fossil/wearables/fsl/contact/Contact;)V
        //  2228: aload_0        
        //  2229: getfield        com/portfolio/platform/MigrationManager.c:Lcom/portfolio/platform/data/source/NotificationsRepository;
        //  2232: aload           8
        //  2234: invokevirtual   com/portfolio/platform/data/source/NotificationsRepository.savePhoneNumber:(Lcom/fossil/wearables/fsl/contact/PhoneNumber;)V
        //  2237: goto            2120
        //  2240: aload           7
        //  2242: invokevirtual   com/fossil/wearables/fsl/contact/Contact.getContactId:()I
        //  2245: bipush          -100
        //  2247: if_icmpne       2120
        //  2250: getstatic       com/misfit/frameworks/buttonservice/log/FLogger.INSTANCE:Lcom/misfit/frameworks/buttonservice/log/FLogger;
        //  2253: invokevirtual   com/misfit/frameworks/buttonservice/log/FLogger.getLocal:()Lcom/misfit/frameworks/buttonservice/log/ILocalFLogger;
        //  2256: getstatic       com/portfolio/platform/MigrationManager.m:Ljava/lang/String;
        //  2259: ldc_w           "Migrate Call from everyone"
        //  2262: invokeinterface com/misfit/frameworks/buttonservice/log/ILocalFLogger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //  2267: new             Lcom/fossil/wearables/fsl/contact/PhoneNumber;
        //  2270: dup            
        //  2271: invokespecial   com/fossil/wearables/fsl/contact/PhoneNumber.<init>:()V
        //  2274: astore          8
        //  2276: aload           7
        //  2278: getstatic       com/portfolio/platform/data/AppType.ALL_CALLS:Lcom/portfolio/platform/data/AppType;
        //  2281: getfield        com/portfolio/platform/data/AppType.packageName:Ljava/lang/String;
        //  2284: invokevirtual   com/fossil/wearables/fsl/contact/Contact.setFirstName:(Ljava/lang/String;)V
        //  2287: aload           7
        //  2289: iconst_1       
        //  2290: invokevirtual   com/fossil/wearables/fsl/contact/Contact.setUseCall:(Z)V
        //  2293: aload           8
        //  2295: ldc_w           "-1234"
        //  2298: invokevirtual   com/fossil/wearables/fsl/contact/PhoneNumber.setNumber:(Ljava/lang/String;)V
        //  2301: aload_0        
        //  2302: getfield        com/portfolio/platform/MigrationManager.c:Lcom/portfolio/platform/data/source/NotificationsRepository;
        //  2305: aload           7
        //  2307: invokevirtual   com/portfolio/platform/data/source/NotificationsRepository.saveContact:(Lcom/fossil/wearables/fsl/contact/Contact;)V
        //  2310: aload           8
        //  2312: aload           7
        //  2314: invokevirtual   com/fossil/wearables/fsl/contact/PhoneNumber.setContact:(Lcom/fossil/wearables/fsl/contact/Contact;)V
        //  2317: aload_0        
        //  2318: getfield        com/portfolio/platform/MigrationManager.c:Lcom/portfolio/platform/data/source/NotificationsRepository;
        //  2321: aload           8
        //  2323: invokevirtual   com/portfolio/platform/data/source/NotificationsRepository.savePhoneNumber:(Lcom/fossil/wearables/fsl/contact/PhoneNumber;)V
        //  2326: goto            2120
        //  2329: getstatic       com/fossil/blesdk/obfuscated/cb4.a:Lcom/fossil/blesdk/obfuscated/cb4;
        //  2332: astore_1       
        //  2333: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  162    208    821    823    Ljava/lang/Exception;
        //  213    247    821    823    Ljava/lang/Exception;
        //  250    335    821    823    Ljava/lang/Exception;
        //  340    402    821    823    Ljava/lang/Exception;
        //  405    430    821    823    Ljava/lang/Exception;
        //  435    469    821    823    Ljava/lang/Exception;
        //  469    580    816    821    Ljava/lang/Exception;
        //  583    614    816    821    Ljava/lang/Exception;
        //  619    710    816    821    Ljava/lang/Exception;
        //  713    744    816    821    Ljava/lang/Exception;
        //  749    813    816    821    Ljava/lang/Exception;
        //  1070   1084   1695   1701   Ljava/lang/Exception;
        //  1088   1096   1695   1701   Ljava/lang/Exception;
        //  1100   1109   1695   1701   Ljava/lang/Exception;
        //  1113   1122   1695   1701   Ljava/lang/Exception;
        //  1127   1189   1255   1260   Ljava/lang/Exception;
        //  1194   1216   1255   1260   Ljava/lang/Exception;
        //  1216   1247   1690   1692   Ljava/lang/Exception;
        //  1250   1253   1690   1692   Ljava/lang/Exception;
        //  1264   1287   1690   1692   Ljava/lang/Exception;
        //  1292   1299   1690   1692   Ljava/lang/Exception;
        //  1304   1373   1681   1690   Ljava/lang/Exception;
        //  1378   1388   1681   1690   Ljava/lang/Exception;
        //  1391   1394   1681   1690   Ljava/lang/Exception;
        //  1396   1405   1681   1690   Ljava/lang/Exception;
        //  1405   1463   1681   1690   Ljava/lang/Exception;
        //  1466   1473   1681   1690   Ljava/lang/Exception;
        //  1478   1546   1681   1690   Ljava/lang/Exception;
        //  1551   1561   1681   1690   Ljava/lang/Exception;
        //  1564   1567   1681   1690   Ljava/lang/Exception;
        //  1573   1674   1681   1690   Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_1250:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.DecompilerLinkProvider.generateContent(DecompilerLinkProvider.java:97)
        //     at us.deathmarine.luyten.OpenFile.decompileWithNavigationLinks(OpenFile.java:494)
        //     at us.deathmarine.luyten.OpenFile.decompile(OpenFile.java:467)
        //     at us.deathmarine.luyten.Model.extractClassToTextPane(Model.java:420)
        //     at us.deathmarine.luyten.Model.openEntryByTreePath(Model.java:339)
        //     at us.deathmarine.luyten.Model$1.keyPressed(Model.java:134)
        //     at java.awt.AWTEventMulticaster.keyPressed(Unknown Source)
        //     at java.awt.Component.processKeyEvent(Unknown Source)
        //     at javax.swing.JComponent.processKeyEvent(Unknown Source)
        //     at java.awt.Component.processEvent(Unknown Source)
        //     at java.awt.Container.processEvent(Unknown Source)
        //     at java.awt.Component.dispatchEventImpl(Unknown Source)
        //     at java.awt.Container.dispatchEventImpl(Unknown Source)
        //     at java.awt.Component.dispatchEvent(Unknown Source)
        //     at java.awt.KeyboardFocusManager.redispatchEvent(Unknown Source)
        //     at java.awt.DefaultKeyboardFocusManager.dispatchKeyEvent(Unknown Source)
        //     at java.awt.DefaultKeyboardFocusManager.preDispatchKeyEvent(Unknown Source)
        //     at java.awt.DefaultKeyboardFocusManager.typeAheadAssertions(Unknown Source)
        //     at java.awt.DefaultKeyboardFocusManager.dispatchEvent(Unknown Source)
        //     at java.awt.Component.dispatchEventImpl(Unknown Source)
        //     at java.awt.Container.dispatchEventImpl(Unknown Source)
        //     at java.awt.Window.dispatchEventImpl(Unknown Source)
        //     at java.awt.Component.dispatchEvent(Unknown Source)
        //     at java.awt.EventQueue.dispatchEventImpl(Unknown Source)
        //     at java.awt.EventQueue.access$500(Unknown Source)
        //     at java.awt.EventQueue$3.run(Unknown Source)
        //     at java.awt.EventQueue$3.run(Unknown Source)
        //     at java.security.AccessController.doPrivileged(Native Method)
        //     at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown Source)
        //     at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown Source)
        //     at java.awt.EventQueue$4.run(Unknown Source)
        //     at java.awt.EventQueue$4.run(Unknown Source)
        //     at java.security.AccessController.doPrivileged(Native Method)
        //     at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(Unknown Source)
        //     at java.awt.EventQueue.dispatchEvent(Unknown Source)
        //     at java.awt.EventDispatchThread.pumpOneEventForFilters(Unknown Source)
        //     at java.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source)
        //     at java.awt.EventDispatchThread.pumpEventsForHierarchy(Unknown Source)
        //     at java.awt.EventDispatchThread.pumpEvents(Unknown Source)
        //     at java.awt.EventDispatchThread.pumpEvents(Unknown Source)
        //     at java.awt.EventDispatchThread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @DexIgnore
    public final void c() {
        final List<DeviceModel> allDevice = en2.p.a().d().getAllDevice();
        final String e = this.e.e();
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String m = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("migrationDeviceData - allDevices=");
        sb.append(allDevice);
        sb.append(", activeSerial=");
        sb.append(e);
        local.d(m, sb.toString());
        try {
            final boolean b = !TextUtils.isEmpty((CharSequence)e) && !DeviceHelper.o.e(e);
            if (b) {
                this.a.o("");
                this.a.l(true);
            }
            if (allDevice == null || allDevice.isEmpty()) {
                return;
            }
            for (final DeviceModel deviceModel : allDevice) {
                if (deviceModel != null) {
                    if (TextUtils.isEmpty((CharSequence)deviceModel.getDeviceId())) {
                        continue;
                    }
                    final String deviceId = deviceModel.getDeviceId();
                    final String macAddress = deviceModel.getMacAddress();
                    final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    final String i = MigrationManager.m;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("migrationDeviceData - step1: deviceId=");
                    sb2.append(deviceId);
                    sb2.append(", deviceAddress=");
                    sb2.append(macAddress);
                    local2.d(i, sb2.toString());
                    final DeviceHelper.a o = DeviceHelper.o;
                    final String deviceId2 = deviceModel.getDeviceId();
                    if (deviceId2 == null) {
                        wd4.a();
                        throw null;
                    }
                    if (o.e(deviceId2)) {
                        continue;
                    }
                    en2.p.a().d().removeDevice(deviceId);
                }
            }
            if (b) {
                try {
                    final IButtonConnectivity b2 = PortfolioApp.W.b();
                    if (b2 == null) {
                        wd4.a();
                        throw null;
                    }
                    b2.removeActiveSerial(e);
                }
                catch (Exception ex) {
                    final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    final String j = MigrationManager.m;
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Exception when remove legacy device in button service ,keep going e=");
                    sb3.append(ex);
                    local3.d(j, sb3.toString());
                }
            }
            for (final DeviceModel deviceModel2 : allDevice) {
                if (deviceModel2 != null) {
                    if (TextUtils.isEmpty((CharSequence)deviceModel2.getDeviceId())) {
                        continue;
                    }
                    final String deviceId3 = deviceModel2.getDeviceId();
                    final String macAddress2 = deviceModel2.getMacAddress();
                    final ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    final String k = MigrationManager.m;
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("migrationDeviceData - step2: - deviceId=");
                    sb4.append(deviceId3);
                    sb4.append(", deviceAddress=");
                    sb4.append(macAddress2);
                    local4.d(k, sb4.toString());
                    final DeviceHelper.a o2 = DeviceHelper.o;
                    final String deviceId4 = deviceModel2.getDeviceId();
                    if (deviceId4 == null) {
                        wd4.a();
                        throw null;
                    }
                    if (o2.e(deviceId4)) {
                        if (cg4.b(deviceModel2.getDeviceId(), e, true)) {
                            final PortfolioApp e2 = this.e;
                            final String deviceId5 = deviceModel2.getDeviceId();
                            if (deviceId5 == null) {
                                wd4.a();
                                throw null;
                            }
                            e2.c(deviceId5, deviceModel2.getMacAddress());
                        }
                        final IButtonConnectivity b3 = PortfolioApp.W.b();
                        if (b3 == null) {
                            wd4.a();
                            throw null;
                        }
                        b3.setPairedSerial(deviceModel2.getDeviceId(), deviceModel2.getMacAddress());
                    }
                    else {
                        final IButtonConnectivity b4 = PortfolioApp.W.b();
                        if (b4 == null) {
                            wd4.a();
                            throw null;
                        }
                        b4.removePairedSerial(deviceModel2.getDeviceId());
                    }
                }
            }
            this.a.m(false);
        }
        catch (Exception ex2) {
            final ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            final String l = MigrationManager.m;
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("migrationDeviceData - Exception when upgrade to 1.14.0 version e=");
            sb5.append(ex2);
            local5.e(l, sb5.toString());
            ex2.printStackTrace();
            this.a.m(true);
        }
    }
    
    @DexIgnore
    public final void c(String s) {
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String m = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("processMigration from ");
        sb.append(s);
        sb.append(" value ");
        sb.append(zk2.a(s, "2.0.0"));
        local.d(m, sb.toString());
        if (zk2.a(s, "2.0.0") == -1) {
            FLogger.INSTANCE.getLocal().d(MigrationManager.m, "processMigration for 1.5");
            this.a();
            this.a.q("2.0.0");
        }
        else if (zk2.a(s, "4.0.0") == -1) {
            final MFUser currentUser = this.b.getCurrentUser();
            final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            final String i = MigrationManager.m;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("processMigration for 4.0.0 userId ");
            if (currentUser != null) {
                s = currentUser.getUserId();
            }
            else {
                s = null;
            }
            sb2.append(s);
            local2.d(i, sb2.toString());
            if (currentUser != null) {
                s = currentUser.getUserId();
                wd4.a((Object)s, "it.userId");
                this.b(s);
                this.a.a(this.e.e(), 0L, false);
            }
            this.a.q("4.0.0");
            PushPendingDataWorker.y.a();
        }
    }
    
    @DexIgnore
    public final boolean d() {
        final String m = this.a.m();
        final String h = this.e.h();
        final ILocalFLogger local = FLogger.INSTANCE.getLocal();
        final String i = MigrationManager.m;
        final StringBuilder sb = new StringBuilder();
        sb.append("onUpgrade for appVersion ");
        sb.append(h);
        sb.append(" lastVersion ");
        sb.append(m);
        local.d(i, sb.toString());
        try {
            if (TextUtils.isEmpty((CharSequence)m)) {
                final MFUser b = en2.p.a().n().b();
                final String f = ks3.a().f((Context)this.e);
                final ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                final String j = MigrationManager.m;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("userToken ");
                String userAccessToken;
                if (b != null) {
                    userAccessToken = b.getUserAccessToken();
                }
                else {
                    userAccessToken = null;
                }
                sb2.append(userAccessToken);
                sb2.append(" legacyToken ");
                sb2.append(f);
                local2.d(j, sb2.toString());
                String userAccessToken2;
                if (b != null) {
                    userAccessToken2 = b.getUserAccessToken();
                }
                else {
                    userAccessToken2 = null;
                }
                if (TextUtils.isEmpty((CharSequence)userAccessToken2) && !TextUtils.isEmpty((CharSequence)f)) {
                    this.c("1.14.2");
                }
            }
            else if (wd4.a((Object)m, (Object)h) ^ true) {
                if (m == null) {
                    wd4.a();
                    throw null;
                }
                this.c(m);
            }
        }
        catch (Exception ex) {
            final ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            final String k = MigrationManager.m;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Exception when start upgrade from version ");
            sb3.append(m);
            sb3.append(" to version=");
            sb3.append(h);
            sb3.append(", exception=");
            sb3.append(ex);
            local3.e(k, sb3.toString());
            final String l = MigrationManager.m;
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Exception when migration e=");
            sb4.append(ex);
            zw.a(0, l, sb4.toString());
        }
        this.b();
        this.a.q(h);
        this.a.a(true, h);
        throw null;
        // kg4.a(null, (kd4)new MigrationManager$upgrade.MigrationManager$upgrade$Anon1((kc4)null), 1, null);
        // FLogger.INSTANCE.getLocal().d(MigrationManager.m, "trigger full sync after upgrade");
        // this.e.a(this.k, false, 13);
        // return true;
    }
}
