package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.model.notification.DynaEnum;
import com.sina.weibo.sdk.WeiboAppManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class AppType extends DynaEnum {
    public static AppType BADOO = new AppType(-1, "com.badoo.mobile");
    public static AppType CLOCK = new AppType(R.string.app_clock, "");
    public static AppType ESPN = new AppType(-1, "com.espn.score_center");
    public static AppType FACEBOOK = new AppType(-1, "com.facebook.katana");
    public static AppType FACEBOOK_MESSENGER = new AppType(-1, "com.facebook.orca");
    public static AppType FLICKR = new AppType(-1, "com.yahoo.mobile.client.android.flickr");
    public static AppType FOURSQUARE = new AppType(-1, "com.joelapenna.foursquared");
    public static AppType GMAIL = new AppType(-1, "com.google.android.gm");
    public static AppType GOOGLE_PLUS = new AppType(-1, "com.google.android.apps.plus");
    public static AppType GOOGLE_INBOX = new AppType(-1, "com.google.android.apps.inbox");
    public static AppType HANGOUTS = new AppType(-1, "com.google.android.talk");
    public static AppType HIPCHAT = new AppType(-1, "com.hipchat");
    public static AppType INSTAGRAM = new AppType(-1, "com.instagram.android");
    public static AppType KAKAO = new AppType(-1, "com.kakao.talk");
    public static AppType LINE = new AppType(-1, "jp.naver.line.android");
    public static AppType LYNC = new AppType(-1, "com.microsoft.office.lync15");
    public static AppType MESSAGING = new AppType(-1, "com.android.mms");
    public static AppType PANDORA = new AppType(-1, "com.pandora.android");
    public static AppType PINTEREST = new AppType(-1, "com.pinterest");
    public static AppType SHAZAM = new AppType(-1, "com.shazam.android");
    public static AppType SINA_WEIBO = new AppType(-1, WeiboAppManager.WEIBO_PACKAGENAME);
    public static AppType SKYPE = new AppType(-1, "com.skype.raider");
    public static AppType SLACK = new AppType(-1, "com.Slack");
    public static AppType SNAPCHAT = new AppType(-1, "com.snapchat.android");
    public static AppType SPOTIFY = new AppType(-1, "com.spotify.music");
    public static AppType SWARM = new AppType(-1, "com.foursquare.robin");
    public static AppType TUMBLR = new AppType(-1, "com.tumblr");
    public static AppType TWITTER = new AppType(-1, "com.twitter.android");
    public static AppType UBER = new AppType(-1, "com.ubercab");
    public static AppType VIADEO = new AppType(-1, "com.viadeo.android");
    public static AppType WECHAT = new AppType(-1, "com.tencent.mm");
    public static AppType WHATSAPP = new AppType(-1, "com.whatsapp");
    public static AppType ALL_SMS = new AppType(-1, "All Texts");
    public static AppType ALL_EMAIL = new AppType(R.string.email_from_everyone, "");
    public static AppType ALL_CALLS = new AppType(-1, "All Calls");
    
    public final String packageName;
    public final int titleResId;

    public static AppType get(String appName) {
        return AppType.get(AppType.class, appName);
    }

    public AppType(int i, String str) {
        super(str, -1);
        this.titleResId = i;
        this.packageName = str;
    }

    public static boolean isInstalled(Context context, String str) {
        return context.getPackageManager().getLaunchIntentForPackage(str) != null;
    }

    public int getAppResId() {
        return this.titleResId;
    }

    public String getPackageName() {
        return this.packageName;
    }
}
