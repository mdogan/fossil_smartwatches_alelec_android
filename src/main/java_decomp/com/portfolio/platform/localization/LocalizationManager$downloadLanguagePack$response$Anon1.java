package com.portfolio.platform.localization;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.localization.LocalizationManager$downloadLanguagePack$response$Anon1", f = "LocalizationManager.kt", l = {191}, m = "invokeSuspend")
public final class LocalizationManager$downloadLanguagePack$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<yz1>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ LocalizationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocalizationManager$downloadLanguagePack$response$Anon1(LocalizationManager localizationManager, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = localizationManager;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new LocalizationManager$downloadLanguagePack$response$Anon1(this.this$Anon0, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((LocalizationManager$downloadLanguagePack$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            GuestApiService b = this.this$Anon0.g;
            String a2 = this.this$Anon0.f;
            this.label = 1;
            obj = b.getLocalizations(a2, "android", this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
