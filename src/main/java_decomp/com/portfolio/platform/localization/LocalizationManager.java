package com.portfolio.platform.localization;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.Utility;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.um2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wm2;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.response.ResponseKt;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationManager extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public WeakReference<Activity> b;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public /* final */ Application.ActivityLifecycleCallbacks e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ GuestApiService g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LocalizationManager.h;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0080  */
        public final boolean a(String str, String str2) {
            wd4.b(str, "filePath");
            if (str2 == null) {
                return true;
            }
            FileInputStream fileInputStream = null;
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                byte[] bArr = new byte[2014];
                FileInputStream fileInputStream2 = new FileInputStream(str);
                while (true) {
                    try {
                        int read = fileInputStream2.read(bArr);
                        if (read == -1) {
                            byte[] digest = instance.digest();
                            wd4.a((Object) digest, "digest.digest()");
                            String a = a(digest);
                            String lowerCase = str2.toLowerCase();
                            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                            boolean a2 = wd4.a((Object) lowerCase, (Object) a);
                            fileInputStream2.close();
                            return a2;
                        } else if (read > 0) {
                            instance.update(bArr, 0, read);
                        }
                    } catch (Exception e) {
                        e = e;
                        fileInputStream = fileInputStream2;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = fileInputStream2;
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = a();
                    local.e(a3, "Error inside " + a() + ".verifyDownloadFile - e=" + e);
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            }
        }

        @DexIgnore
        public final String a(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & FileType.MASKED_INDEX) + 256, 16);
                wd4.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    wd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            wd4.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public c(LocalizationManager localizationManager, String str, boolean z) {
            wd4.b(str, "mFilePath");
            this.c = localizationManager;
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            wd4.b(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            boolean z = true;
            try {
                if (wd4.a((Object) tm2.b(), (Object) "en_US")) {
                    wm2.a(this.a, this.b);
                    wm2.a(this.c.d() + "/strings.json", true);
                } else {
                    wm2.a(this.a, this.b);
                }
                wm2.b();
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "load cache failed e=" + e);
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    /*
    static {
        String simpleName = LocalizationManager.class.getSimpleName();
        wd4.a((Object) simpleName, "LocalizationManager::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public LocalizationManager(Application application, String str, GuestApiService guestApiService) {
        wd4.b(application, "app");
        wd4.b(str, "mAppVersion");
        wd4.b(guestApiService, "mGuestApiService");
        this.f = str;
        this.g = guestApiService;
        wd4.a((Object) application.getSharedPreferences(application.getPackageName() + ".language", 0), "app.getSharedPreferences\u2026e\", Context.MODE_PRIVATE)");
        this.a = application;
        this.e = new LocalizationManager$activityCallbacks$Anon1(this);
    }

    @DexIgnore
    public final String c() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wd4.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(ZendeskConfig.SLASH);
        sb.append(e());
        return sb.toString();
    }

    @DexIgnore
    public final String d() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wd4.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(ZendeskConfig.SLASH);
        sb.append("language");
        sb.append("/values");
        return sb.toString();
    }

    @DexIgnore
    public final String e() {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wd4.a((Object) filesDir, "mContext.filesDir");
        sb2.append(filesDir.getAbsolutePath());
        sb2.append(ZendeskConfig.SLASH);
        sb2.append("language");
        File file = new File(sb2.toString());
        if (file.exists()) {
            String[] list = file.list();
            wd4.a((Object) list, "file.list()");
            Locale a2 = tm2.a();
            int length = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = list[i2];
                wd4.a((Object) a2, "l");
                String language = a2.getLanguage();
                wd4.a((Object) language, "l.language");
                if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) language, false, 2, (Object) null)) {
                    String sb3 = sb.toString();
                    wd4.a((Object) sb3, "path.toString()");
                    String language2 = a2.getLanguage();
                    wd4.a((Object) language2, "l.language");
                    if (!StringsKt__StringsKt.a((CharSequence) sb3, (CharSequence) language2, false, 2, (Object) null)) {
                        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb.append(a2.getLanguage());
                    }
                    String country = a2.getCountry();
                    wd4.a((Object) country, "l.country");
                    if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) country, false, 2, (Object) null)) {
                        sb.append("-r");
                        sb.append(a2.getCountry());
                        break;
                    }
                }
                i2++;
            }
        }
        sb.insert(0, "language/values");
        sb.append("/strings.json");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "path=" + sb.toString());
        String sb4 = sb.toString();
        wd4.a((Object) sb4, "path.toString()");
        return sb4;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final Context g() {
        return this.a;
    }

    @DexIgnore
    public final WeakReference<Activity> h() {
        return this.b;
    }

    @DexIgnore
    public final void i() {
        wm2.a();
        String c2 = c();
        if (new File(c2).exists()) {
            a(c2, true);
        } else {
            a(e(), false);
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && wd4.a((Object) intent.getAction(), (Object) "android.intent.action.LOCALE_CHANGED")) {
            PortfolioApp.W.c().O();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "onReceive locale=" + tm2.b());
            WeakReference<Activity> weakReference = this.b;
            if (weakReference != null) {
                Activity activity = (Activity) weakReference.get();
                if (activity != null) {
                    wd4.a((Object) activity, "it");
                    if (!activity.isFinishing()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = h;
                        local2.d(str2, "onReceive finish activity=" + activity.getLocalClassName());
                        activity.setResult(0);
                        activity.finishAffinity();
                    }
                }
            }
            PortfolioApp.W.c().y();
            i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            wd4.b(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            String str = strArr[0];
            LocalizationManager localizationManager = LocalizationManager.this;
            return Boolean.valueOf(localizationManager.a(localizationManager.g()));
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    LocalizationManager.this.a(new File(LocalizationManager.this.g().getFilesDir().toString() + ""));
                    LocalizationManager localizationManager = LocalizationManager.this;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = LocalizationManager.this.g().getFilesDir();
                    wd4.a((Object) filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append(ZendeskConfig.SLASH);
                    sb.append(LocalizationManager.this.e());
                    localizationManager.a(sb.toString(), true);
                }
                d b = LocalizationManager.this.b();
                if (b != null) {
                    b.a(bool.booleanValue());
                    return;
                }
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        this.b = weakReference;
    }

    @DexIgnore
    public final d b() {
        return this.d;
    }

    @DexIgnore
    public final Application.ActivityLifecycleCallbacks a() {
        return this.e;
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        wd4.b(str, "path");
        new c(this, str, z).execute(new String[0]);
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "splashScreen");
        this.c = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(kc4<? super cb4> kc4) {
        LocalizationManager$downloadLanguagePack$Anon1 localizationManager$downloadLanguagePack$Anon1;
        int i2;
        LocalizationManager localizationManager;
        ro2 ro2;
        if (kc4 instanceof LocalizationManager$downloadLanguagePack$Anon1) {
            localizationManager$downloadLanguagePack$Anon1 = (LocalizationManager$downloadLanguagePack$Anon1) kc4;
            int i3 = localizationManager$downloadLanguagePack$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                localizationManager$downloadLanguagePack$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = localizationManager$downloadLanguagePack$Anon1.result;
                Object a2 = oc4.a();
                i2 = localizationManager$downloadLanguagePack$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(h, "downloadLanguagePack() called");
                    LocalizationManager$downloadLanguagePack$response$Anon1 localizationManager$downloadLanguagePack$response$Anon1 = new LocalizationManager$downloadLanguagePack$response$Anon1(this, (kc4) null);
                    localizationManager$downloadLanguagePack$Anon1.L$Anon0 = this;
                    localizationManager$downloadLanguagePack$Anon1.label = 1;
                    obj = ResponseKt.a(localizationManager$downloadLanguagePack$response$Anon1, localizationManager$downloadLanguagePack$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                    localizationManager = this;
                } else if (i2 == 1) {
                    localizationManager = (LocalizationManager) localizationManager$downloadLanguagePack$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = h;
                    StringBuilder sb = new StringBuilder();
                    sb.append("download language success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local.d(str, sb.toString());
                    if (!so2.b()) {
                        ApiResponse apiResponse = (ApiResponse) so2.a();
                        if (apiResponse != null) {
                            List list = apiResponse.get_items();
                            if (list != null && (!list.isEmpty())) {
                                um2 um2 = new um2();
                                um2.a((yz1) list.get(0));
                                localizationManager.a(um2);
                            }
                        }
                    }
                    d dVar = localizationManager.d;
                    if (dVar != null) {
                        dVar.a(false);
                    }
                } else if (ro2 instanceof qo2) {
                    d dVar2 = localizationManager.d;
                    if (dVar2 != null) {
                        dVar2.a(false);
                    }
                }
                return cb4.a;
            }
        }
        localizationManager$downloadLanguagePack$Anon1 = new LocalizationManager$downloadLanguagePack$Anon1(this, kc4);
        Object obj2 = localizationManager$downloadLanguagePack$Anon1.result;
        Object a22 = oc4.a();
        i2 = localizationManager$downloadLanguagePack$Anon1.label;
        if (i2 != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final void a(um2 um2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "downloadFile response=" + um2);
        new b(this, this.a, um2).execute(new String[0]);
    }

    @DexIgnore
    public final void a(File file) {
        wd4.b(file, "folder");
        if (file.exists() && file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                wd4.a((Object) file2, "f");
                if (file2.isDirectory()) {
                    a(file2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ um2 b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public b(LocalizationManager localizationManager, Context context, um2 um2) {
            wd4.b(context, "context");
            wd4.b(um2, Firmware.COLUMN_DOWNLOAD_URL);
            this.c = localizationManager;
            this.a = context;
            this.b = um2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r2.flush();
            r2.close();
         */
        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            BufferedInputStream bufferedInputStream;
            FileOutputStream fileOutputStream;
            wd4.b(strArr, "links");
            if (!TextUtils.isEmpty(this.b.b())) {
                try {
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.a.getFilesDir();
                    wd4.a((Object) filesDir, "context.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append(ZendeskConfig.SLASH);
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    if (new File(sb2).exists() && LocalizationManager.i.a(sb2, this.b.a())) {
                        return true;
                    }
                    URLConnection openConnection = new URL(this.b.b()).openConnection();
                    openConnection.connect();
                    bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                    fileOutputStream = new FileOutputStream(sb2);
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            } else if (isCancelled()) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                bufferedInputStream.close();
                                return false;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                    bufferedInputStream.close();
                    if (TextUtils.isEmpty(this.b.a())) {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "Download complete with risk cause by empty checksum");
                        return true;
                    } else if (LocalizationManager.i.a(sb2, this.b.a())) {
                        return true;
                    } else {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "Inconsistent checksum, retry download?");
                        return true;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = LocalizationManager.i.a();
                    local.e(a2, "Error inside " + LocalizationManager.i.a() + ".onHandleIntent - e=" + e2);
                } catch (Throwable th) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    bufferedInputStream.close();
                    throw th;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    new e().execute(new String[]{this.b.c()});
                }
                d b2 = this.c.b();
                if (b2 != null) {
                    b2.a(false);
                    return;
                }
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e5 A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ea A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f5 A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fa A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[Catch:{ Exception -> 0x00fe }, RETURN, SYNTHETIC] */
    public final boolean a(Context context) {
        wd4.b(context, "ctx");
        FLogger.INSTANCE.getLocal().d(h, "unzipFile");
        byte[] bArr = new byte[2048];
        try {
            StringBuilder sb = new StringBuilder();
            File filesDir = context.getFilesDir();
            wd4.a((Object) filesDir, "ctx.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(ZendeskConfig.SLASH);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            File filesDir2 = context.getFilesDir();
            wd4.a((Object) filesDir2, "ctx.filesDir");
            sb3.append(filesDir2.getAbsolutePath());
            sb3.append(ZendeskConfig.SLASH);
            sb3.append("language.zip");
            String sb4 = sb3.toString();
            FLogger.INSTANCE.getLocal().d(h, "filePath=" + sb4);
            if (!new File(sb4).exists()) {
                return false;
            }
            FileInputStream fileInputStream = new FileInputStream(sb4);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            FileOutputStream fileOutputStream = null;
            ZipEntry zipEntry = null;
            while (true) {
                try {
                    zipEntry = zipInputStream.getNextEntry();
                    if (zipEntry == null) {
                        break;
                    } else if (zipEntry.isDirectory()) {
                        String name = zipEntry.getName();
                        wd4.a((Object) name, "ze.name");
                        a(sb2, name);
                    } else {
                        FileOutputStream fileOutputStream2 = new FileOutputStream(sb2 + zipEntry.getName());
                        while (true) {
                            try {
                                int read = zipInputStream.read(bArr);
                                if (read <= 0) {
                                    break;
                                }
                                fileOutputStream2.write(bArr, 0, read);
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = fileOutputStream2;
                                try {
                                    e.printStackTrace();
                                    zipInputStream.close();
                                    fileInputStream.close();
                                    if (zipEntry != null) {
                                        zipInputStream.closeEntry();
                                    }
                                    if (fileOutputStream != null) {
                                        return true;
                                    }
                                    fileOutputStream.close();
                                    return true;
                                } catch (Throwable th) {
                                    th = th;
                                    zipInputStream.close();
                                    fileInputStream.close();
                                    if (zipEntry != null) {
                                        zipInputStream.closeEntry();
                                    }
                                    if (fileOutputStream != null) {
                                        fileOutputStream.close();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = fileOutputStream2;
                                zipInputStream.close();
                                fileInputStream.close();
                                if (zipEntry != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                throw th;
                            }
                        }
                        fileOutputStream = fileOutputStream2;
                    }
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    zipInputStream.close();
                    fileInputStream.close();
                    if (zipEntry != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                }
            }
            zipInputStream.close();
            fileInputStream.close();
            if (fileOutputStream == null) {
                return true;
            }
            fileOutputStream.close();
            return true;
        } catch (Exception e4) {
            FLogger.INSTANCE.getLocal().e(h, "Unzipping failed ex=" + e4);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        File file = new File(str + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    @DexIgnore
    public final LocalizationManager a(d dVar) {
        this.d = dVar;
        return this;
    }
}
