package com.portfolio.platform.localization;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocalizationManager$activityCallbacks$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1(kc4 kc4, LocalizationManager$activityCallbacks$Anon1 localizationManager$activityCallbacks$Anon1) {
        super(2, kc4);
        this.this$Anon0 = localizationManager$activityCallbacks$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 = new LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1(kc4, this.this$Anon0);
        localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            StringBuilder sb = new StringBuilder();
            File filesDir = this.this$Anon0.e.g().getFilesDir();
            wd4.a((Object) filesDir, "mContext.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(ZendeskConfig.SLASH);
            sb.append("language.zip");
            String sb2 = sb.toString();
            File file = new File(sb2);
            if (!file.exists()) {
                LocalizationManager localizationManager = this.this$Anon0.e;
                this.L$Anon0 = lh4;
                this.L$Anon1 = sb2;
                this.L$Anon2 = file;
                this.label = 1;
                if (localizationManager.a((kc4<? super cb4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            File file2 = (File) this.L$Anon2;
            String str = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
