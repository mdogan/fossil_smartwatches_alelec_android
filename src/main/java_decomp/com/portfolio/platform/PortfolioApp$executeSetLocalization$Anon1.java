package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$Anon1", f = "PortfolioApp.kt", l = {1209}, m = "invokeSuspend")
public final class PortfolioApp$executeSetLocalization$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$executeSetLocalization$Anon1(PortfolioApp portfolioApp, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = portfolioApp;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PortfolioApp$executeSetLocalization$Anon1 portfolioApp$executeSetLocalization$Anon1 = new PortfolioApp$executeSetLocalization$Anon1(this.this$Anon0, kc4);
        portfolioApp$executeSetLocalization$Anon1.p$ = (lh4) obj;
        return portfolioApp$executeSetLocalization$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$executeSetLocalization$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            WatchLocalizationRepository t = this.this$Anon0.t();
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = t.getWatchLocalizationFromServer(true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String str = (String) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String d = PortfolioApp.W.d();
        local.d(d, "path of localization file: " + str);
        if (str != null) {
            IButtonConnectivity b = PortfolioApp.W.b();
            if (b != null) {
                b.setLocalizationData(new LocalizationData(str, (String) null, 2, (rd4) null));
            }
        }
        return cb4.a;
    }
}
