package com.portfolio.platform.provider;

import android.content.Context;
import com.fossil.blesdk.obfuscated.eo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.DatabaseHelper;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserProviderImp extends BaseDbProvider implements eo2 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserProviderImp(Context context, String str) {
        super(context, str);
        wd4.b(context, "context");
        wd4.b(str, "dbPath");
    }

    @DexIgnore
    public void c(MFUser mFUser) {
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            wd4.a((Object) str, "TAG");
            local.e(str, "Inside .deleteUser user=" + mFUser);
            try {
                g().delete(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wd4.a((Object) str2, "TAG");
                local2.e(str2, "Inside .deleteUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wd4.a((Object) str3, "TAG");
            local3.e(str3, "Inside .deleteUser error user is null");
        }
    }

    @DexIgnore
    public void f() {
        try {
            TableUtils.clearTable(g().getConnectionSource(), MFUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public final Dao<MFUser, Integer> g() throws SQLException {
        Dao<MFUser, Integer> dao = this.databaseHelper.getDao(MFUser.class);
        wd4.a((Object) dao, "databaseHelper.getDao(MFUser::class.java)");
        return dao;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{MFUser.class};
    }

    @DexIgnore
    public String getDbPath() {
        DatabaseHelper databaseHelper = this.databaseHelper;
        wd4.a((Object) databaseHelper, "databaseHelper");
        String dbPath = databaseHelper.getDbPath();
        wd4.a((Object) dbPath, "databaseHelper.dbPath");
        return dbPath;
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new UserProviderImp$getDbUpgrades$Anon1(this);
    }

    @DexIgnore
    public int getDbVersion() {
        return 4;
    }

    @DexIgnore
    public void a(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        wd4.a((Object) str, "TAG");
        local.d(str, "Inside .updateUser " + mFUser);
        if (mFUser != null) {
            try {
                g().update(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wd4.a((Object) str2, "TAG");
                local2.d(str2, "Inside .updateUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wd4.a((Object) str3, "TAG");
            local3.d(str3, "Inside .updateUser error user is null");
        }
    }

    @DexIgnore
    public void b(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        wd4.a((Object) str, "TAG");
        local.d(str, "Inside .insertUser " + mFUser);
        if (mFUser != null) {
            try {
                g().createIfNotExists(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wd4.a((Object) str2, "TAG");
                local2.d(str2, "Inside .insertUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wd4.a((Object) str3, "TAG");
            local3.d(str3, "Inside .insertUser error user is null");
        }
    }

    @DexIgnore
    public MFUser b() {
        try {
            List<MFUser> queryForAll = g().queryForAll();
            if (queryForAll == null || !(!queryForAll.isEmpty())) {
                return null;
            }
            return queryForAll.get(0);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            wd4.a((Object) str, "TAG");
            local.e(str, "Inside .getUser exception=" + e);
            return null;
        }
    }
}
