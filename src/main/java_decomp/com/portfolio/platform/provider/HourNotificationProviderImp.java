package com.portfolio.platform.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.tn2;
import com.fossil.blesdk.obfuscated.un2;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class HourNotificationProviderImp extends BaseDbProvider implements un2 {
    @DexIgnore
    public static /* final */ String a; // = "HourNotificationProviderImp";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements UpgradeCommand {
            @DexIgnore
            public a(Anon1 anon1) {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                new a(sQLiteDatabase).execute(new Void[0]);
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new a(this));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public /* final */ SQLiteDatabase a;

        @DexIgnore
        public a(SQLiteDatabase sQLiteDatabase) {
            this.a = sQLiteDatabase;
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            try {
                FLogger.INSTANCE.getLocal().d(HourNotificationProviderImp.a, "Inside upgrade db from 1 to 2");
                Cursor query = this.a.query(true, "hourNotification", new String[]{"extraId", "createdAt", AppFilter.COLUMN_HOUR, AppFilter.COLUMN_IS_VIBRATION_ONLY}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                List<tn2> arrayList = new ArrayList<>();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex("extraId"));
                        String string2 = query.getString(query.getColumnIndex("createdAt"));
                        int i = query.getInt(query.getColumnIndex(AppFilter.COLUMN_HOUR));
                        int i2 = query.getInt(query.getColumnIndex(AppFilter.COLUMN_IS_VIBRATION_ONLY));
                        tn2 tn2 = new tn2();
                        tn2.b(string);
                        tn2.a(Long.valueOf(string2).longValue());
                        tn2.a(i);
                        boolean z = true;
                        if (i2 != 1) {
                            z = false;
                        }
                        tn2.a(z);
                        arrayList.add(tn2);
                        query.moveToNext();
                    }
                    query.close();
                }
                FLogger.INSTANCE.getLocal().d(HourNotificationProviderImp.a, "Inside upgrade db from 1 to 2, creating hour notification copy table");
                this.a.execSQL("CREATE TABLE hour_notification_copy (id VARCHAR PRIMARY KEY, extraId VARCHAR, hour INTEGER, createdAt VARCHAR, isVibrationOnly INTEGER, deviceFamily VARCHAR);");
                if (!arrayList.isEmpty()) {
                    arrayList = HourNotificationProviderImp.b(arrayList);
                }
                if (!arrayList.isEmpty()) {
                    for (tn2 tn22 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("extraId", tn22.c());
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(tn22.d()));
                        contentValues.put("createdAt", Long.valueOf(tn22.a()));
                        contentValues.put("deviceFamily", tn22.b());
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(tn22.f()));
                        contentValues.put("id", tn22.e());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = HourNotificationProviderImp.a;
                        local.d(str, "Insert new values " + contentValues + " into copy table");
                        this.a.insert("hour_notification_copy", (String) null, contentValues);
                    }
                }
                this.a.execSQL("DROP TABLE hourNotification;");
                this.a.execSQL("ALTER TABLE hour_notification_copy RENAME TO hourNotification;");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = HourNotificationProviderImp.a;
                local2.e(str2, "Error inside " + HourNotificationProviderImp.a + ".upgrade - e=" + e);
            }
            return null;
        }
    }

    @DexIgnore
    public HourNotificationProviderImp(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static tn2 a(String str, List<tn2> list) {
        for (tn2 next : list) {
            if (next.c().equalsIgnoreCase(str)) {
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public static List<tn2> b(List<tn2> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && !list.isEmpty()) {
            for (MFDeviceFamily mFDeviceFamily : DeviceHelper.o.a()) {
                if (mFDeviceFamily != MFDeviceFamily.DEVICE_FAMILY_Q_MOTION) {
                    List<AppFilter> a2 = NotificationAppHelper.b.a(mFDeviceFamily);
                    List<ContactGroup> allContactGroups = en2.p.a().b().getAllContactGroups(mFDeviceFamily.ordinal());
                    if (a2 != null && !a2.isEmpty()) {
                        for (AppFilter next : a2) {
                            tn2 a3 = a(next.getType(), list);
                            if (a3 != null) {
                                tn2 tn2 = new tn2(a3.d(), a3.f(), a3.c(), a3.b());
                                FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + " Found hands setting of app filter=" + next.getType());
                                tn2.a(mFDeviceFamily.name());
                                StringBuilder sb = new StringBuilder();
                                sb.append(next.getType());
                                sb.append(mFDeviceFamily);
                                tn2.c(sb.toString());
                                arrayList.add(tn2);
                            }
                        }
                    }
                    if (allContactGroups != null && !allContactGroups.isEmpty()) {
                        for (ContactGroup next2 : allContactGroups) {
                            if (next2.getContacts() != null && !next2.getContacts().isEmpty()) {
                                tn2 a4 = a(String.valueOf(next2.getContacts().get(0).getContactId()), list);
                                if (a4 != null) {
                                    FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + "Found hands setting of contactId=" + next2.getContacts().get(0).getContactId());
                                    tn2 tn22 = new tn2(a4.d(), a4.f(), a4.c(), a4.b());
                                    tn22.a(mFDeviceFamily.name());
                                    tn22.c(next2.getContacts().get(0).getContactId() + mFDeviceFamily.name());
                                    arrayList.add(tn22);
                                }
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final tn2 d(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "getHourNotificationByExtraIdOnly() id = " + str);
        try {
            QueryBuilder<tn2, Integer> queryBuilder = g().queryBuilder();
            queryBuilder.where().eq("extraId", str);
            tn2 queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst;
            }
            FLogger.INSTANCE.getLocal().d(a, "getHourNotificationByExtraIdOnly() - notification is null - return default notification for this action");
            return new tn2(1, false, str, DeviceHelper.o.a(PortfolioApp.R.e()).name());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.e(str3, "Error inside " + a + ".getHourNotificationByExtraIdOnly - e=" + e);
            return new tn2(1, false, str, DeviceHelper.o.a(PortfolioApp.R.e()).name());
        }
    }

    @DexIgnore
    public final Dao<tn2, Integer> g() throws SQLException {
        return this.databaseHelper.getDao(tn2.class);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{tn2.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public tn2 a(String str, String str2) {
        try {
            QueryBuilder<tn2, Integer> queryBuilder = g().queryBuilder();
            queryBuilder.where().eq("extraId", str).and().eq("deviceFamily", str2);
            tn2 queryForFirst = queryBuilder.queryForFirst();
            return queryForFirst == null ? new tn2(1, false, str, DeviceHelper.o.a(PortfolioApp.R.e()).name()) : queryForFirst;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.e(str3, "Error inside " + a + ".getHourNotificationByExtraId - e=" + e);
            return d(str);
        }
    }
}
