package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.tb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ApplicationEventListener_LifecycleAdapter implements tb {
    @DexIgnore
    public /* final */ ApplicationEventListener a;

    @DexIgnore
    public ApplicationEventListener_LifecycleAdapter(ApplicationEventListener applicationEventListener) {
        this.a = applicationEventListener;
    }

    @DexIgnore
    public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event, boolean z, cc ccVar) {
        boolean z2 = ccVar != null;
        if (!z) {
            if (event == Lifecycle.Event.ON_START) {
                if (!z2 || ccVar.a("onAppEnterForeground", 1)) {
                    this.a.onAppEnterForeground();
                }
            } else if (event != Lifecycle.Event.ON_STOP) {
            } else {
                if (!z2 || ccVar.a("onAppEnterBackground", 1)) {
                    this.a.onAppEnterBackground();
                }
            }
        }
    }
}
