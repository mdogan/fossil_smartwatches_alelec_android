package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$Anon1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
public final class CoroutineUseCase$executeUseCase$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.e $callBack;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.b $requestValues;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$executeUseCase$Anon1(CoroutineUseCase coroutineUseCase, CoroutineUseCase.e eVar, CoroutineUseCase.b bVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = coroutineUseCase;
        this.$callBack = eVar;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CoroutineUseCase$executeUseCase$Anon1 coroutineUseCase$executeUseCase$Anon1 = new CoroutineUseCase$executeUseCase$Anon1(this.this$Anon0, this.$callBack, this.$requestValues, kc4);
        coroutineUseCase$executeUseCase$Anon1.p$ = (lh4) obj;
        return coroutineUseCase$executeUseCase$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CoroutineUseCase$executeUseCase$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            this.this$Anon0.a = this.$callBack;
            CoroutineUseCase coroutineUseCase = this.this$Anon0;
            coroutineUseCase.b = coroutineUseCase.c();
            FLogger.INSTANCE.getLocal().d(this.this$Anon0.b, "Start UseCase");
            CoroutineUseCase coroutineUseCase2 = this.this$Anon0;
            CoroutineUseCase.b bVar = this.$requestValues;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = coroutineUseCase2.a(bVar, (kc4<Object>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj instanceof CoroutineUseCase.d) {
            this.this$Anon0.a((CoroutineUseCase.d) obj);
        } else if (obj instanceof CoroutineUseCase.a) {
            this.this$Anon0.a((CoroutineUseCase.a) obj);
        }
        return cb4.a;
    }
}
