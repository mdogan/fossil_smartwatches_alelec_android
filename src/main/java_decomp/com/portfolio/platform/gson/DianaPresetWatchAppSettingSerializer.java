package com.portfolio.platform.gson;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.c02;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.zz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetWatchAppSettingSerializer implements c02<List<? extends DianaPresetWatchAppSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<DianaPresetWatchAppSetting> list, Type type, b02 b02) {
        wd4.b(list, "src");
        uz1 uz1 = new uz1();
        zz1 zz1 = new zz1();
        for (DianaPresetWatchAppSetting next : list) {
            String component1 = next.component1();
            String component2 = next.component2();
            String component3 = next.component3();
            String component4 = next.component4();
            yz1 yz1 = new yz1();
            yz1.a("buttonPosition", component1);
            yz1.a("appId", component2);
            yz1.a("localUpdatedAt", component3);
            if (!TextUtils.isEmpty(component4)) {
                try {
                    JsonElement a = zz1.a(component4);
                    if (a != null) {
                        yz1.a(Constants.USER_SETTING, (JsonElement) (yz1) a);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception unused) {
                    yz1.a(Constants.USER_SETTING, (JsonElement) new xz1());
                }
            } else {
                yz1.a(Constants.USER_SETTING, (JsonElement) new xz1());
            }
            uz1.a((JsonElement) yz1);
        }
        return uz1;
    }
}
