package com.portfolio.platform.gson;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.fossil.blesdk.obfuscated.yz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetDeserializer implements wz1<DianaPreset> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DianaPreset deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        boolean z;
        String str8;
        String str9;
        String str10;
        Iterator<JsonElement> it;
        String str11;
        String str12;
        String str13;
        String str14;
        String str15;
        String str16;
        Iterator<JsonElement> it2;
        String str17;
        String str18;
        String str19;
        if (jsonElement != null) {
            yz1 d = jsonElement.d();
            JsonElement a2 = d.a("id");
            wd4.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_ID)");
            String f = a2.f();
            String str20 = "name";
            JsonElement a3 = d.a(str20);
            wd4.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f2 = a3.f();
            JsonElement a4 = d.a("updatedAt");
            wd4.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a4.f();
            JsonElement a5 = d.a("createdAt");
            wd4.a((Object) a5, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a5.f();
            JsonElement a6 = d.a("isActive");
            wd4.a((Object) a6, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a7 = a6.a();
            String str21 = "serialNumber";
            JsonElement a8 = d.a(str21);
            wd4.a((Object) a8, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
            String f5 = a8.f();
            String str22 = "watchFaceId";
            JsonElement a9 = d.a(str22);
            wd4.a((Object) a9, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f6 = a9.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (vz1 != null) {
                Iterator<JsonElement> it3 = d.b("buttons").iterator();
                while (true) {
                    str2 = f3;
                    str = f4;
                    z = a7;
                    str8 = "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)";
                    str7 = str22;
                    str9 = "itemJsonObject.get(Constants.JSON_KEY_APP_ID)";
                    str6 = f6;
                    str5 = str20;
                    str4 = f2;
                    str3 = str21;
                    str10 = "";
                    if (!it3.hasNext()) {
                        break;
                    }
                    JsonElement next = it3.next();
                    wd4.a((Object) next, "item");
                    yz1 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a10 = d2.a("buttonPosition");
                        it2 = it3;
                        wd4.a((Object) a10, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str17 = a10.f();
                    } else {
                        it2 = it3;
                        str17 = str10;
                    }
                    if (d2.d("appId")) {
                        JsonElement a11 = d2.a("appId");
                        wd4.a((Object) a11, str9);
                        str18 = a11.f();
                    } else {
                        str18 = str10;
                    }
                    if (d2.d("localUpdatedAt")) {
                        JsonElement a12 = d2.a("localUpdatedAt");
                        wd4.a((Object) a12, str8);
                        str19 = a12.f();
                    } else {
                        str19 = str10;
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a13 = d2.a(Constants.USER_SETTING);
                            wd4.a((Object) a13, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str10 = sj2.a(a13.d());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    wd4.a((Object) str17, "position");
                    wd4.a((Object) str18, "appId");
                    wd4.a((Object) str19, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str17, str18, str19, str10));
                    f3 = str2;
                    f4 = str;
                    it3 = it2;
                    a7 = z;
                    str22 = str7;
                    f6 = str6;
                    str20 = str5;
                    f2 = str4;
                    str21 = str3;
                }
                Iterator<JsonElement> it4 = d.b("complications").iterator();
                while (it4.hasNext()) {
                    JsonElement next2 = it4.next();
                    wd4.a((Object) next2, "item");
                    yz1 d3 = next2.d();
                    if (d3.d("complicationPosition")) {
                        JsonElement a14 = d3.a("complicationPosition");
                        it = it4;
                        wd4.a((Object) a14, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str11 = a14.f();
                    } else {
                        it = it4;
                        str11 = str10;
                    }
                    if (d3.d("appId")) {
                        JsonElement a15 = d3.a("appId");
                        wd4.a((Object) a15, str9);
                        str12 = a15.f();
                    } else {
                        str12 = str10;
                    }
                    if (d3.d("localUpdatedAt")) {
                        str14 = str9;
                        JsonElement a16 = d3.a("localUpdatedAt");
                        wd4.a((Object) a16, str8);
                        str15 = a16.f();
                        str13 = str8;
                    } else {
                        str14 = str9;
                        Calendar instance = Calendar.getInstance();
                        str13 = str8;
                        wd4.a((Object) instance, "Calendar.getInstance()");
                        str15 = sk2.t(instance.getTime());
                    }
                    if (d3.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a17 = d3.a(Constants.USER_SETTING);
                            wd4.a((Object) a17, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str16 = sj2.a(a17.d());
                        } catch (Exception unused2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        wd4.a((Object) str11, "position");
                        wd4.a((Object) str12, "appId");
                        wd4.a((Object) str15, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str11, str12, str15, str16));
                        it4 = it;
                        str9 = str14;
                        str8 = str13;
                    }
                    str16 = str10;
                    wd4.a((Object) str11, "position");
                    wd4.a((Object) str12, "appId");
                    wd4.a((Object) str15, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str11, str12, str15, str16));
                    it4 = it;
                    str9 = str14;
                    str8 = str13;
                }
            } else {
                str5 = str20;
                str2 = f3;
                str3 = str21;
                str4 = f2;
                z = a7;
                str7 = str22;
                str6 = f6;
                str = f4;
            }
            wd4.a((Object) f, "id");
            wd4.a((Object) f5, str3);
            String str23 = str4;
            wd4.a((Object) str23, str5);
            String str24 = str6;
            wd4.a((Object) str24, str7);
            DianaPreset dianaPreset = new DianaPreset(f, f5, str23, z, arrayList2, arrayList, str24);
            dianaPreset.setCreatedAt(str);
            dianaPreset.setUpdatedAt(str2);
            return dianaPreset;
        }
        wd4.a();
        throw null;
    }
}
