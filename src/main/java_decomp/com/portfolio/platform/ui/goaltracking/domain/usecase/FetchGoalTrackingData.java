package com.portfolio.platform.ui.goaltracking.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchGoalTrackingData extends CoroutineUseCase<b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ GoalTrackingRepository d;
    @DexIgnore
    public /* final */ UserRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = FetchGoalTrackingData.class.getSimpleName();
        wd4.a((Object) simpleName, "FetchGoalTrackingData::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public FetchGoalTrackingData(GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(userRepository, "mUserRepository");
        this.d = goalTrackingRepository;
        this.e = userRepository;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(b bVar, kc4<? super cb4> kc4) {
        FetchGoalTrackingData$run$Anon1 fetchGoalTrackingData$run$Anon1;
        int i;
        if (kc4 instanceof FetchGoalTrackingData$run$Anon1) {
            fetchGoalTrackingData$run$Anon1 = (FetchGoalTrackingData$run$Anon1) kc4;
            int i2 = fetchGoalTrackingData$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchGoalTrackingData$run$Anon1.label = i2 - Integer.MIN_VALUE;
                FetchGoalTrackingData$run$Anon1 fetchGoalTrackingData$run$Anon12 = fetchGoalTrackingData$run$Anon1;
                Object obj = fetchGoalTrackingData$run$Anon12.result;
                Object a2 = oc4.a();
                i = fetchGoalTrackingData$run$Anon12.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return cb4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = f;
                    local.d(str, "executeUseCase - date=" + gt3.a(a3));
                    MFUser currentUser = this.e.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = f;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cb4.a;
                    }
                    Date d2 = sk2.d(currentUser.getCreatedAt());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    wd4.a((Object) d2, "createdDate");
                    sb.append(gt3.a(d2));
                    local3.d(str3, sb.toString());
                    if (sk2.b(d2, a3) || sk2.b(a3, new Date())) {
                        return cb4.a;
                    }
                    Calendar p = sk2.p(a3);
                    wd4.a((Object) p, "DateHelper.getStartOfWeek(date)");
                    Date time = p.getTime();
                    if (sk2.c(d2, time)) {
                        time = d2;
                    }
                    GoalTrackingRepository goalTrackingRepository = this.d;
                    wd4.a((Object) time, GoalPhase.COLUMN_START_DATE);
                    fetchGoalTrackingData$run$Anon12.L$Anon0 = this;
                    fetchGoalTrackingData$run$Anon12.L$Anon1 = bVar;
                    fetchGoalTrackingData$run$Anon12.L$Anon2 = a3;
                    fetchGoalTrackingData$run$Anon12.L$Anon3 = currentUser;
                    fetchGoalTrackingData$run$Anon12.L$Anon4 = d2;
                    fetchGoalTrackingData$run$Anon12.L$Anon5 = time;
                    fetchGoalTrackingData$run$Anon12.label = 1;
                    if (GoalTrackingRepository.loadGoalTrackingDataList$default(goalTrackingRepository, time, a3, 0, 0, fetchGoalTrackingData$run$Anon12, 12, (Object) null) == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    Date date = (Date) fetchGoalTrackingData$run$Anon12.L$Anon5;
                    Date date2 = (Date) fetchGoalTrackingData$run$Anon12.L$Anon4;
                    MFUser mFUser = (MFUser) fetchGoalTrackingData$run$Anon12.L$Anon3;
                    Date date3 = (Date) fetchGoalTrackingData$run$Anon12.L$Anon2;
                    b bVar2 = (b) fetchGoalTrackingData$run$Anon12.L$Anon1;
                    FetchGoalTrackingData fetchGoalTrackingData = (FetchGoalTrackingData) fetchGoalTrackingData$run$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        fetchGoalTrackingData$run$Anon1 = new FetchGoalTrackingData$run$Anon1(this, kc4);
        FetchGoalTrackingData$run$Anon1 fetchGoalTrackingData$run$Anon122 = fetchGoalTrackingData$run$Anon1;
        Object obj2 = fetchGoalTrackingData$run$Anon122.result;
        Object a22 = oc4.a();
        i = fetchGoalTrackingData$run$Anon122.label;
        if (i != 0) {
        }
        return cb4.a;
    }
}
