package com.portfolio.platform.ui;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BaseActivity$mButtonServiceConnection$Anon1 implements ServiceConnection {
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity a;

    @DexIgnore
    public BaseActivity$mButtonServiceConnection$Anon1(BaseActivity baseActivity) {
        this.a = baseActivity;
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        wd4.b(componentName, "name");
        wd4.b(iBinder, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service connected");
        BaseActivity.A.a(IButtonConnectivity.Stub.asInterface(iBinder));
        this.a.b(true);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
        wd4.b(componentName, "name");
        FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service disconnected");
        this.a.b(false);
        BaseActivity.A.a((IButtonConnectivity) null);
    }
}
