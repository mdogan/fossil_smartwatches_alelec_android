package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$Anon2", f = "SwitchActiveDeviceUseCase.kt", l = {232, 239}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$linkServer$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends Boolean, ? extends Integer>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$linkServer$Anon2(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, MisfitDeviceProfile misfitDeviceProfile, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = switchActiveDeviceUseCase;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SwitchActiveDeviceUseCase$linkServer$Anon2 switchActiveDeviceUseCase$linkServer$Anon2 = new SwitchActiveDeviceUseCase$linkServer$Anon2(this.this$Anon0, this.$currentDeviceProfile, this.$serial, kc4);
        switchActiveDeviceUseCase$linkServer$Anon2.p$ = (lh4) obj;
        return switchActiveDeviceUseCase$linkServer$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$linkServer$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bd, code lost:
        if (r22 == null) goto L_0x00c0;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object obj3;
        lh4 lh4;
        Device device;
        Object a = oc4.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0;
            MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                int batteryLevel = misfitDeviceProfile.getBatteryLevel();
                if (batteryLevel < 0 || 100 < batteryLevel) {
                    batteryLevel = batteryLevel < 0 ? 0 : 100;
                }
                int b = zk2.b(misfitDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
                Device deviceBySerial = this.this$Anon0.i.getDeviceBySerial(this.$serial);
                if (deviceBySerial != null) {
                    deviceBySerial.setBatteryLevel(batteryLevel);
                    deviceBySerial.setFirmwareRevision(misfitDeviceProfile.getFirmwareVersion());
                    deviceBySerial.setMacAddress(misfitDeviceProfile.getAddress());
                    deviceBySerial.setMajor(misfitDeviceProfile.getMicroAppMajorVersion());
                    deviceBySerial.setMinor(misfitDeviceProfile.getMicroAppMinorVersion());
                    if (deviceBySerial != null) {
                        device = deviceBySerial;
                    }
                }
                device = new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), batteryLevel, pc4.a(b), false, 64, (rd4) null);
                cb4 cb4 = cb4.a;
            }
            device = this.this$Anon0.i.getDeviceBySerial(this.$serial);
            switchActiveDeviceUseCase.a(device);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "doSwitchDevice device " + this.this$Anon0.e());
            if (this.this$Anon0.e() == null) {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, ob4.a((T[]) new Integer[]{pc4.a(-1)}), "No device data"));
                return new Pair(pc4.a(false), pc4.a(-1));
            }
            DeviceRepository b2 = this.this$Anon0.i;
            Device e = this.this$Anon0.e();
            if (e != null) {
                this.L$Anon0 = lh4;
                this.label = 1;
                obj3 = b2.forceLinkDevice(e, this);
                if (obj3 == a) {
                    return a;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj3 = obj;
        } else if (i == 2) {
            MFUser mFUser = (MFUser) this.L$Anon3;
            MFUser mFUser2 = (MFUser) this.L$Anon2;
            ro2 ro2 = (ro2) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj2 = obj;
            ro2 ro22 = (ro2) obj2;
            return new Pair(pc4.a(true), pc4.a(-1));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro23 = (ro2) obj3;
        if (ro23 instanceof so2) {
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "doSwitchDevice success");
            MFUser currentUser = this.this$Anon0.h.getCurrentUser();
            if (currentUser != null) {
                Device e2 = this.this$Anon0.e();
                if (e2 != null) {
                    currentUser.setActiveDeviceId(e2.getDeviceId());
                    UserRepository d = this.this$Anon0.h;
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = ro23;
                    this.L$Anon2 = currentUser;
                    this.L$Anon3 = currentUser;
                    this.label = 2;
                    obj2 = d.updateUser(currentUser, false, this);
                    if (obj2 == a) {
                        return a;
                    }
                    ro2 ro222 = (ro2) obj2;
                } else {
                    wd4.a();
                    throw null;
                }
            }
            return new Pair(pc4.a(true), pc4.a(-1));
        } else if (!(ro23 instanceof qo2)) {
            return new Pair(pc4.a(false), pc4.a(-1));
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = SwitchActiveDeviceUseCase.n.a();
            StringBuilder sb = new StringBuilder();
            sb.append("doSwitchDevice fail ");
            qo2 qo2 = (qo2) ro23;
            sb.append(qo2.a());
            local2.d(a3, sb.toString());
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$Anon0;
            ArrayList a4 = ob4.a((T[]) new Integer[]{pc4.a(qo2.a())});
            ServerError c = qo2.c();
            if (c != null) {
                str = c.getMessage();
            }
            switchActiveDeviceUseCase2.a(new SwitchActiveDeviceUseCase.c(114, a4, str));
            PortfolioApp.W.c().a(this.$serial, false, qo2.a());
            return new Pair(pc4.a(false), pc4.a(qo2.a()));
        }
    }
}
