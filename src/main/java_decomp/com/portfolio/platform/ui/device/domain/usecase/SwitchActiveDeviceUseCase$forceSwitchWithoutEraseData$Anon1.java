package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1", f = "SwitchActiveDeviceUseCase.kt", l = {180}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $newActiveDeviceSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = switchActiveDeviceUseCase;
        this.$newActiveDeviceSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 = new SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(this.this$Anon0, this.$newActiveDeviceSerial, kc4);
        switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1.p$ = (lh4) obj;
        return switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
            String e = this.this$Anon0.k.e();
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0;
            switchActiveDeviceUseCase.a(switchActiveDeviceUseCase.i.getDeviceBySerial(this.$newActiveDeviceSerial));
            boolean e2 = PortfolioApp.W.c().e(this.$newActiveDeviceSerial);
            if (e2) {
                SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$Anon0;
                String str2 = this.$newActiveDeviceSerial;
                this.L$Anon0 = lh4;
                this.L$Anon1 = e;
                this.Z$Anon0 = e2;
                this.label = 1;
                obj = switchActiveDeviceUseCase2.a(str2, (MisfitDeviceProfile) null, this);
                if (obj == a) {
                    return a;
                }
                str = e;
            } else {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
                return cb4.a;
            }
        } else if (i == 1) {
            str = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((Boolean) ((Pair) obj).component1()).booleanValue()) {
            this.this$Anon0.b(this.$newActiveDeviceSerial);
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase3 = this.this$Anon0;
            switchActiveDeviceUseCase3.a(new SwitchActiveDeviceUseCase.d(switchActiveDeviceUseCase3.e()));
        } else {
            PortfolioApp.W.c().e(str);
            SwitchActiveDeviceUseCase.c g = this.this$Anon0.g();
            if (g == null || this.this$Anon0.a(g) == null) {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
            }
        }
        return cb4.a;
    }
}
