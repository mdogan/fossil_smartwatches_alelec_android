package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ar2;
import com.fossil.blesdk.obfuscated.br2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.uj2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zq2;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.SyncResponseCode;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncUseCase extends CoroutineUseCase<ar2, br2, zq2> {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ DianaPresetRepository e;
    @DexIgnore
    public /* final */ fn2 f;
    @DexIgnore
    public /* final */ wy2 g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase j;
    @DexIgnore
    public /* final */ qx2 k;
    @DexIgnore
    public /* final */ VerifySecretKeyUseCase l;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase m;
    @DexIgnore
    public /* final */ AnalyticsHelper n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ AlarmsRepository p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DianaSyncUseCase.q;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase a;

        @DexIgnore
        public b(DianaSyncUseCase dianaSyncUseCase) {
            this.a = dianaSyncUseCase;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && cg4.b(stringExtra, this.a.h.e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.r.a(), "sync success, remove device now");
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    this.a.a(new br2());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = DianaSyncUseCase.r.a();
                    local.d(a2, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.a.a(new zq2(SyncResponseCode.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, (ArrayList<Integer>) null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a(new zq2(SyncResponseCode.FAIL_DUE_TO_SYNC_FAIL, (ArrayList<Integer>) null));
                            return;
                        }
                    }
                    this.a.a(new zq2(SyncResponseCode.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.r.a(), "sync pending due to workout");
                    this.a.a(new zq2(SyncResponseCode.FAIL_DUE_TO_PENDING_WORKOUT, (ArrayList<Integer>) null));
                }
            }
        }
    }

    /*
    static {
        String simpleName = DianaSyncUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "DianaSyncUseCase::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public DianaSyncUseCase(DianaPresetRepository dianaPresetRepository, fn2 fn2, wy2 wy2, PortfolioApp portfolioApp, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, qx2 qx2, VerifySecretKeyUseCase verifySecretKeyUseCase, UpdateFirmwareUsecase updateFirmwareUsecase, AnalyticsHelper analyticsHelper, WatchFaceRepository watchFaceRepository, AlarmsRepository alarmsRepository) {
        wd4.b(dianaPresetRepository, "mDianaPresetRepository");
        wd4.b(fn2, "mSharedPrefs");
        wd4.b(wy2, "mGetApps");
        wd4.b(portfolioApp, "mApp");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wd4.b(qx2, "mGetAllContactGroup");
        wd4.b(verifySecretKeyUseCase, "mVerifySecretKeyUseCase");
        wd4.b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        wd4.b(analyticsHelper, "mAnalyticsHelper");
        wd4.b(watchFaceRepository, "watchFaceRepository");
        wd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = dianaPresetRepository;
        this.f = fn2;
        this.g = wy2;
        this.h = portfolioApp;
        this.i = deviceRepository;
        this.j = notificationSettingsDatabase;
        this.k = qx2;
        this.l = verifySecretKeyUseCase;
        this.m = updateFirmwareUsecase;
        this.n = analyticsHelper;
        this.o = watchFaceRepository;
        this.p = alarmsRepository;
    }

    @DexIgnore
    public final void b(int i2, boolean z, UserProfile userProfile, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.d(str2, "verifySecretKey " + str);
        a(userProfile.getOriginalSyncMode(), str, 0);
        this.l.a(new VerifySecretKeyUseCase.b(str), new DianaSyncUseCase$verifySecretKey$Anon1(this, str, i2, z, userProfile));
    }

    @DexIgnore
    public String c() {
        return q;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0222 A[Catch:{ Exception -> 0x02b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x025e A[Catch:{ Exception -> 0x02b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(ar2 ar2, kc4<Object> kc4) {
        DianaSyncUseCase$run$Anon1 dianaSyncUseCase$run$Anon1;
        int i2;
        UserProfile userProfile;
        int i3;
        DianaSyncUseCase dianaSyncUseCase;
        List activeAlarms;
        DianaPreset activePresetBySerial;
        if (kc4 instanceof DianaSyncUseCase$run$Anon1) {
            dianaSyncUseCase$run$Anon1 = (DianaSyncUseCase$run$Anon1) kc4;
            int i4 = dianaSyncUseCase$run$Anon1.label;
            if ((i4 & Integer.MIN_VALUE) != 0) {
                dianaSyncUseCase$run$Anon1.label = i4 - Integer.MIN_VALUE;
                Object obj = dianaSyncUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i2 = dianaSyncUseCase$run$Anon1.label;
                BackgroundConfig backgroundConfig = null;
                if (i2 != 0) {
                    za4.a(obj);
                    if (ar2 == null) {
                        return new zq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    }
                    int b2 = ar2.b();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String c = c();
                    StringBuilder sb = new StringBuilder();
                    sb.append("start on thread=");
                    Thread currentThread = Thread.currentThread();
                    wd4.a((Object) currentThread, "Thread.currentThread()");
                    sb.append(currentThread.getName());
                    local.d(c, sb.toString());
                    userProfile = PortfolioApp.W.c().j();
                    if (userProfile == null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String c2 = c();
                        local2.e(c2, "Error inside " + c() + ".startDeviceSync - user is null");
                        a(b2, ar2.a(), 2);
                        return new zq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    } else if (TextUtils.isEmpty(ar2.a())) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String c3 = c();
                        local3.e(c3, "Error inside " + c() + ".startDeviceSync - serial is null");
                        a(b2, ar2.a(), 2);
                        return new zq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
                    } else {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String c4 = c();
                        local4.d(c4, "Inside " + c() + ".startDeviceSync - serial=" + ar2.a() + "," + " weightInKg=" + userProfile.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + userProfile.getUserBiometricData().getHeightInMeter() + ", goal=" + userProfile.getGoalSteps() + ", isNewDevice=" + ar2.c() + ", SyncMode=" + b2);
                        userProfile.setOriginalSyncMode(b2);
                        boolean k2 = this.f.k(ar2.a());
                        if (!k2 || b2 == 13) {
                            b2 = 13;
                        }
                        if (b2 == 13 || !PortfolioApp.W.c().h(ar2.a())) {
                            FLogger.INSTANCE.getLocal().e(q, "start set localization");
                            PortfolioApp.W.c().O();
                            if (b2 == 13) {
                                try {
                                    this.f.a(ar2.a(), System.currentTimeMillis(), false);
                                    dianaSyncUseCase$run$Anon1.L$Anon0 = this;
                                    dianaSyncUseCase$run$Anon1.L$Anon1 = ar2;
                                    dianaSyncUseCase$run$Anon1.I$Anon0 = b2;
                                    dianaSyncUseCase$run$Anon1.L$Anon2 = userProfile;
                                    dianaSyncUseCase$run$Anon1.Z$Anon0 = k2;
                                    dianaSyncUseCase$run$Anon1.label = 1;
                                    if (a((kc4<? super cb4>) dianaSyncUseCase$run$Anon1) == a2) {
                                        return a2;
                                    }
                                    dianaSyncUseCase = this;
                                    i3 = b2;
                                } catch (Exception e2) {
                                    i3 = b2;
                                    e = e2;
                                    dianaSyncUseCase = this;
                                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                                    FLogger.Component component = FLogger.Component.APP;
                                    FLogger.Session session = FLogger.Session.OTHER;
                                    String a3 = ar2.a();
                                    String str = q;
                                    remote.i(component, session, a3, str, "[Sync] Exception when prepare settings " + e);
                                    dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
                                    return new Object();
                                }
                            } else {
                                dianaSyncUseCase = this;
                                i3 = b2;
                                dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
                                return new Object();
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().e(c(), "Device is syncing, returning...");
                            return new zq2(SyncResponseCode.SYNC_IS_IN_PROGRESS, (ArrayList<Integer>) null);
                        }
                    }
                } else if (i2 == 1) {
                    boolean z = dianaSyncUseCase$run$Anon1.Z$Anon0;
                    UserProfile userProfile2 = (UserProfile) dianaSyncUseCase$run$Anon1.L$Anon2;
                    i3 = dianaSyncUseCase$run$Anon1.I$Anon0;
                    ar2 ar22 = (ar2) dianaSyncUseCase$run$Anon1.L$Anon1;
                    dianaSyncUseCase = (DianaSyncUseCase) dianaSyncUseCase$run$Anon1.L$Anon0;
                    try {
                        za4.a(obj);
                        ar2 ar23 = ar22;
                        userProfile = userProfile2;
                        ar2 = ar23;
                    } catch (Exception e3) {
                        e = e3;
                        ar2 ar24 = ar22;
                        userProfile = userProfile2;
                        ar2 = ar24;
                        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                        FLogger.Component component2 = FLogger.Component.APP;
                        FLogger.Session session2 = FLogger.Session.OTHER;
                        String a32 = ar2.a();
                        String str2 = q;
                        remote2.i(component2, session2, a32, str2, "[Sync] Exception when prepare settings " + e);
                        dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
                        return new Object();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String c5 = dianaSyncUseCase.c();
                local5.d(c5, "Inside " + dianaSyncUseCase.c() + ".startDeviceSync - Start full-sync");
                activeAlarms = dianaSyncUseCase.p.getActiveAlarms();
                if (activeAlarms == null) {
                    activeAlarms = new ArrayList();
                }
                PortfolioApp.W.c().a((List<? extends Alarm>) oj2.a(activeAlarms));
                activePresetBySerial = dianaSyncUseCase.e.getActivePresetBySerial(ar2.a());
                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                String c6 = dianaSyncUseCase.c();
                local6.d(c6, "startDeviceSync activePreset=" + activePresetBySerial);
                if (activePresetBySerial != null) {
                    PortfolioApp.W.c().a(tj2.a(activePresetBySerial.getComplications(), new Gson()), ar2.a());
                    PortfolioApp.W.c().a(tj2.b(activePresetBySerial.getWatchapps(), new Gson()), ar2.a());
                    WatchFace watchFaceWithId = dianaSyncUseCase.o.getWatchFaceWithId(activePresetBySerial.getWatchFaceId());
                    if (watchFaceWithId != null) {
                        backgroundConfig = uj2.a(watchFaceWithId, (List<DianaPresetComplicationSetting>) activePresetBySerial.getComplications());
                    }
                    if (backgroundConfig != null) {
                        PortfolioApp.W.c().a(backgroundConfig, ar2.a());
                    }
                }
                dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
                return new Object();
            }
        }
        dianaSyncUseCase$run$Anon1 = new DianaSyncUseCase$run$Anon1(this, kc4);
        Object obj2 = dianaSyncUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i2 = dianaSyncUseCase$run$Anon1.label;
        BackgroundConfig backgroundConfig2 = null;
        if (i2 != 0) {
        }
        try {
            ILocalFLogger local52 = FLogger.INSTANCE.getLocal();
            String c52 = dianaSyncUseCase.c();
            local52.d(c52, "Inside " + dianaSyncUseCase.c() + ".startDeviceSync - Start full-sync");
            activeAlarms = dianaSyncUseCase.p.getActiveAlarms();
            if (activeAlarms == null) {
            }
            PortfolioApp.W.c().a((List<? extends Alarm>) oj2.a(activeAlarms));
            activePresetBySerial = dianaSyncUseCase.e.getActivePresetBySerial(ar2.a());
            ILocalFLogger local62 = FLogger.INSTANCE.getLocal();
            String c62 = dianaSyncUseCase.c();
            local62.d(c62, "startDeviceSync activePreset=" + activePresetBySerial);
            if (activePresetBySerial != null) {
            }
        } catch (Exception e4) {
            e = e4;
            IRemoteFLogger remote22 = FLogger.INSTANCE.getRemote();
            FLogger.Component component22 = FLogger.Component.APP;
            FLogger.Session session22 = FLogger.Session.OTHER;
            String a322 = ar2.a();
            String str22 = q;
            remote22.i(component22, session22, a322, str22, "[Sync] Exception when prepare settings " + e);
            dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
            return new Object();
        }
        dianaSyncUseCase.b(i3, ar2.c(), userProfile, ar2.a());
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object a(kc4<? super cb4> kc4) {
        DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1 dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1;
        int i2;
        if (kc4 instanceof DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1) {
            dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1 = (DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1) kc4;
            int i3 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1.label = i3 - Integer.MIN_VALUE;
                DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1 dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1;
                Object obj = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12.result;
                Object a2 = oc4.a();
                i2 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12.label;
                if (i2 != 0) {
                    za4.a(obj);
                    NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                    wy2 wy2 = this.g;
                    qx2 qx2 = this.k;
                    NotificationSettingsDatabase notificationSettingsDatabase = this.j;
                    fn2 fn2 = this.f;
                    dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12.L$Anon0 = this;
                    dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12.label = 1;
                    obj = notificationAppHelper.a(wy2, qx2, notificationSettingsDatabase, fn2, dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    DianaSyncUseCase dianaSyncUseCase = (DianaSyncUseCase) dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                PortfolioApp.W.c().a(new AppNotificationFilterSettings((List) obj, System.currentTimeMillis()), PortfolioApp.W.c().e());
                return cb4.a;
            }
        }
        dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1 = new DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1(this, kc4);
        DianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1 dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon122 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon1;
        Object obj2 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon122.result;
        Object a22 = oc4.a();
        i2 = dianaSyncUseCase$setRuleNotificationFilterToDevice$Anon122.label;
        if (i2 != 0) {
        }
        PortfolioApp.W.c().a(new AppNotificationFilterSettings((List) obj2, System.currentTimeMillis()), PortfolioApp.W.c().e());
        return cb4.a;
    }

    @DexIgnore
    public final void a(int i2, String str, int i3) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c = c();
        local.d(c, "broadcastSyncStatus serial=" + str + ' ' + i3);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), i2);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.a(communicateMode, new BleCommandResultManager.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        wd4.b(userProfile, "userProfile");
        wd4.b(str, "serial");
        if (a() != null) {
            BleCommandResultManager.d.a((BleCommandResultManager.b) this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String c = c();
        local.d(c, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.U()) {
            SKUModel skuModelBySerialPrefix = this.i.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
            this.f.a((Boolean) true);
            this.n.a(i2, skuModelBySerialPrefix);
            vl2 b2 = AnalyticsHelper.f.b("sync_session");
            AnalyticsHelper.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.W.c().a(str, userProfile)) {
            a(userProfile.getOriginalSyncMode(), str, 2);
        }
    }
}
