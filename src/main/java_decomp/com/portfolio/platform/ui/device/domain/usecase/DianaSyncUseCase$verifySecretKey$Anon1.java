package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncUseCase$verifySecretKey$Anon1 implements CoroutineUseCase.e<VerifySecretKeyUseCase.d, VerifySecretKeyUseCase.c> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncUseCase a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ UserProfile e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<UpdateFirmwareUsecase.d, UpdateFirmwareUsecase.c> {
        @DexIgnore
        public void a(UpdateFirmwareUsecase.c cVar) {
            wd4.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateFirmwareUsecase.d dVar) {
            wd4.b(dVar, "responseValue");
        }
    }

    @DexIgnore
    public DianaSyncUseCase$verifySecretKey$Anon1(DianaSyncUseCase dianaSyncUseCase, String str, int i, boolean z, UserProfile userProfile) {
        this.a = dianaSyncUseCase;
        this.b = str;
        this.c = i;
        this.d = z;
        this.e = userProfile;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(VerifySecretKeyUseCase.d dVar) {
        wd4.b(dVar, "responseValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = DianaSyncUseCase.r.a();
        local.d(a2, "secret key " + dVar.a() + ", start sync");
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b, DianaSyncUseCase.r.a(), "[Sync] Verify secret key success, start sync");
        PortfolioApp.W.c().e(dVar.a(), this.b);
        ri4 unused = mg4.b(this.a.b(), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(VerifySecretKeyUseCase.c cVar) {
        wd4.b(cVar, "errorValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = DianaSyncUseCase.r.a();
        local.d(a2, "verify secret key fail, stop sync " + cVar.a());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str = this.b;
        String a3 = DianaSyncUseCase.r.a();
        remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + cVar.a());
        if (cVar.a() == FeatureErrorCode.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
            this.a.m.a(new UpdateFirmwareUsecase.b(this.b, true), new a());
        }
        this.a.a(this.e.getOriginalSyncMode(), this.b, 2);
    }
}
