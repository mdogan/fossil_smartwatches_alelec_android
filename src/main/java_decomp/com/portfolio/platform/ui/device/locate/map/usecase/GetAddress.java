package com.portfolio.platform.ui.device.locate.map.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.response.ResponseKt;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetAddress extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ GoogleApiService d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public b(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wd4.b(str, "address");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GetAddress(GoogleApiService googleApiService) {
        wd4.b(googleApiService, "mGoogleApiService");
        this.d = googleApiService;
    }

    @DexIgnore
    public String c() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(b bVar, kc4<Object> kc4) {
        GetAddress$run$Anon1 getAddress$run$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof GetAddress$run$Anon1) {
            getAddress$run$Anon1 = (GetAddress$run$Anon1) kc4;
            int i2 = getAddress$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                getAddress$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = getAddress$run$Anon1.result;
                Object a2 = oc4.a();
                i = getAddress$run$Anon1.label;
                JsonElement jsonElement = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d("GetAddress", "executeUseCase");
                    GetAddress$run$response$Anon1 getAddress$run$response$Anon1 = new GetAddress$run$response$Anon1(this, bVar, (kc4) null);
                    getAddress$run$Anon1.L$Anon0 = this;
                    getAddress$run$Anon1.L$Anon1 = bVar;
                    getAddress$run$Anon1.label = 1;
                    obj = ResponseKt.a(getAddress$run$response$Anon1, getAddress$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) getAddress$run$Anon1.L$Anon1;
                    GetAddress getAddress = (GetAddress) getAddress$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    yz1 yz1 = (yz1) ((so2) ro2).a();
                    uz1 b2 = yz1 != null ? yz1.b("results") : null;
                    if (b2 == null || b2.size() <= 0) {
                        return new c();
                    }
                    yz1 yz12 = (yz1) b2.get(0);
                    if (yz12 != null) {
                        jsonElement = yz12.a("formatted_address");
                    }
                    if (jsonElement == null) {
                        return new c();
                    }
                    String f = jsonElement.f();
                    wd4.a((Object) f, "value.asString");
                    return new d(f);
                } else if (ro2 instanceof qo2) {
                    return new c();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        getAddress$run$Anon1 = new GetAddress$run$Anon1(this, kc4);
        Object obj2 = getAddress$run$Anon1.result;
        Object a22 = oc4.a();
        i = getAddress$run$Anon1.label;
        JsonElement jsonElement2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
