package com.portfolio.platform.ui.device.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.util.DeviceUtils;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DownloadFirmwareByDeviceModelUsecase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GuestApiService e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "deviceModel");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wd4.b(str, "latestFwVersion");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DownloadFirmwareByDeviceModelUsecase(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        wd4.b(portfolioApp, "mApp");
        wd4.b(guestApiService, "mGuestApiService");
        this.d = portfolioApp;
        this.e = guestApiService;
    }

    @DexIgnore
    public String c() {
        return "DownloadFirmwareByDeviceModelUsecase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public Object a(b bVar, kc4<Object> kc4) {
        DownloadFirmwareByDeviceModelUsecase$run$Anon1 downloadFirmwareByDeviceModelUsecase$run$Anon1;
        int i;
        Firmware firmware;
        DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase;
        b bVar2;
        String str;
        ro2 ro2;
        b bVar3 = bVar;
        kc4<Object> kc42 = kc4;
        if (kc42 instanceof DownloadFirmwareByDeviceModelUsecase$run$Anon1) {
            downloadFirmwareByDeviceModelUsecase$run$Anon1 = (DownloadFirmwareByDeviceModelUsecase$run$Anon1) kc42;
            int i2 = downloadFirmwareByDeviceModelUsecase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                downloadFirmwareByDeviceModelUsecase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = downloadFirmwareByDeviceModelUsecase$run$Anon1.result;
                Object a2 = oc4.a();
                i = downloadFirmwareByDeviceModelUsecase$run$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d("DownloadFirmwareByDeviceModelUsecase", "run");
                    if (bVar3 == null) {
                        FLogger.INSTANCE.getLocal().e("DownloadFirmwareByDeviceModelUsecase", "run - requestValues is NULL!!!");
                        return new c();
                    }
                    FLogger.INSTANCE.getLocal().d("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + bVar.a());
                    String a3 = bVar.a();
                    DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1 downloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1 = new DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1(this, a3, (kc4) null);
                    downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon0 = this;
                    downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon1 = bVar3;
                    downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon2 = a3;
                    downloadFirmwareByDeviceModelUsecase$run$Anon1.label = 1;
                    Object a4 = ResponseKt.a(downloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1, downloadFirmwareByDeviceModelUsecase$run$Anon1);
                    if (a4 == a2) {
                        return a2;
                    }
                    downloadFirmwareByDeviceModelUsecase = this;
                    Object obj2 = a4;
                    bVar2 = bVar3;
                    str = a3;
                    obj = obj2;
                } else if (i == 1) {
                    str = (String) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon2;
                    bVar2 = (b) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon1;
                    downloadFirmwareByDeviceModelUsecase = (DownloadFirmwareByDeviceModelUsecase) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else if (i == 2) {
                    List list = (List) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon4;
                    ro2 ro22 = (ro2) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon3;
                    String str3 = (String) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon2;
                    b bVar4 = (b) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon1;
                    DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase2 = (DownloadFirmwareByDeviceModelUsecase) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon0;
                    za4.a(obj);
                    firmware = (Firmware) downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon5;
                    if (((Boolean) obj).booleanValue()) {
                        return new c();
                    }
                    String versionNumber = firmware.getVersionNumber();
                    wd4.a((Object) versionNumber, "latestFirmware.versionNumber");
                    return new d(versionNumber);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ApiResponse apiResponse = (ApiResponse) ((so2) ro2).a();
                    List<Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
                    if (list2 != null) {
                        firmware = null;
                        for (Firmware firmware2 : list2) {
                            if (!TextUtils.isEmpty(firmware2.getDeviceModel())) {
                                String deviceModel = firmware2.getDeviceModel();
                                wd4.a((Object) deviceModel, "firmware.deviceModel");
                                if (StringsKt__StringsKt.a((CharSequence) deviceModel, (CharSequence) str, false, 2, (Object) null)) {
                                    firmware = firmware2;
                                }
                            }
                        }
                    } else {
                        firmware = null;
                    }
                    if (firmware != null) {
                        DeviceUtils a5 = DeviceUtils.g.a();
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon0 = downloadFirmwareByDeviceModelUsecase;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon1 = bVar2;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon2 = str;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon3 = ro2;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon4 = list2;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.L$Anon5 = firmware;
                        downloadFirmwareByDeviceModelUsecase$run$Anon1.label = 2;
                        obj = a5.a(firmware, (kc4<? super Boolean>) downloadFirmwareByDeviceModelUsecase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        if (((Boolean) obj).booleanValue()) {
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().e("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + str + " not found on server");
                        return new c();
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("run - getFirmwares of deviceModel=");
                    sb.append(str);
                    sb.append(" not found on server code=");
                    qo2 qo2 = (qo2) ro2;
                    ServerError c2 = qo2.c();
                    sb.append(c2 != null ? c2.getCode() : null);
                    sb.append(", message=");
                    ServerError c3 = qo2.c();
                    if (c3 != null) {
                        str2 = c3.getMessage();
                    }
                    sb.append(str2);
                    local.e("DownloadFirmwareByDeviceModelUsecase", sb.toString());
                    return new c();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        downloadFirmwareByDeviceModelUsecase$run$Anon1 = new DownloadFirmwareByDeviceModelUsecase$run$Anon1(this, kc42);
        Object obj3 = downloadFirmwareByDeviceModelUsecase$run$Anon1.result;
        Object a22 = oc4.a();
        i = downloadFirmwareByDeviceModelUsecase$run$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj3;
        if (!(ro2 instanceof so2)) {
        }
    }
}
