package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.util.DeviceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwareUsecase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a((rd4) null);
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ fn2 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareUsecase.f;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final FirmwareData a(fn2 fn2, String str) {
            wd4.b(fn2, "sharedPreferencesManager");
            wd4.b(str, "deviceSKU");
            if (cg4.b("release", "release", true) || !fn2.C()) {
                Firmware a = en2.p.a().e().a(str);
                if (a != null) {
                    return FirmwareFactory.getInstance().createFirmwareData(a.getVersionNumber(), a.getDeviceModel(), a.getChecksum());
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a();
                local.e(a2, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            Firmware a3 = fn2.a(str);
            if (a3 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a3.getVersionNumber(), a3.getDeviceModel(), a3.getChecksum());
            }
            Firmware a4 = en2.p.a().e().a(str);
            if (a4 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a4.getVersionNumber(), a4.getDeviceModel(), a4.getChecksum());
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a5 = a();
            local2.e(a5, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(String str, boolean z) {
            wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, boolean z, int i, rd4 rd4) {
            this(str, (i & 2) != 0 ? false : z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        wd4.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwareUsecase(DeviceRepository deviceRepository, fn2 fn2) {
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = fn2;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f0 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f1 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f8 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x016d A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(b bVar, kc4<Object> kc4) {
        UpdateFirmwareUsecase$run$Anon1 updateFirmwareUsecase$run$Anon1;
        int i;
        String str;
        Device device;
        UpdateFirmwareUsecase updateFirmwareUsecase;
        String sku;
        FirmwareData a2;
        if (kc4 instanceof UpdateFirmwareUsecase$run$Anon1) {
            updateFirmwareUsecase$run$Anon1 = (UpdateFirmwareUsecase$run$Anon1) kc4;
            int i2 = updateFirmwareUsecase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                updateFirmwareUsecase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = updateFirmwareUsecase$run$Anon1.result;
                Object a3 = oc4.a();
                i = updateFirmwareUsecase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(f, "running UseCase");
                    if (bVar == null) {
                        try {
                            FLogger.INSTANCE.getLocal().e(f, "Error when update firmware, requestValues is NULL");
                            return new c();
                        } catch (Exception e2) {
                            e = e2;
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.OTHER;
                            if (bVar != null) {
                            }
                            str = "";
                            String str2 = f;
                            remote.i(component, session, str, str2, "[OTA Start] Exception " + e);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str3 = f;
                            local.d(str3, "Inside .run failed with exception=" + e);
                            e.printStackTrace();
                            return new c();
                        }
                    } else {
                        device = this.d.getDeviceBySerial(bVar.a());
                        if (device == null) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = f;
                            local2.e(str4, "Error when update firmware, can't find latest device on db serial=" + bVar.a());
                            return new c();
                        }
                        if (bVar.b()) {
                            FLogger.INSTANCE.getLocal().d(f, "force download latest fw before udpate fw");
                            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, PortfolioApp.W.c().e(), ApplicationEventListener.p.a(), "[OTA Start] Download FW");
                            DeviceUtils a4 = DeviceUtils.g.a();
                            updateFirmwareUsecase$run$Anon1.L$Anon0 = this;
                            updateFirmwareUsecase$run$Anon1.L$Anon1 = bVar;
                            updateFirmwareUsecase$run$Anon1.L$Anon2 = device;
                            updateFirmwareUsecase$run$Anon1.label = 1;
                            if (a4.a((kc4<? super cb4>) updateFirmwareUsecase$run$Anon1) == a3) {
                                return a3;
                            }
                        }
                        updateFirmwareUsecase = this;
                    }
                } else if (i == 1) {
                    Device device2 = (Device) updateFirmwareUsecase$run$Anon1.L$Anon2;
                    b bVar2 = (b) updateFirmwareUsecase$run$Anon1.L$Anon1;
                    updateFirmwareUsecase = (UpdateFirmwareUsecase) updateFirmwareUsecase$run$Anon1.L$Anon0;
                    try {
                        za4.a(obj);
                        device = device2;
                        bVar = bVar2;
                    } catch (Exception e3) {
                        e = e3;
                        bVar = bVar2;
                        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                        FLogger.Component component2 = FLogger.Component.APP;
                        FLogger.Session session2 = FLogger.Session.OTHER;
                        if (bVar != null) {
                            String a5 = bVar.a();
                            if (a5 != null) {
                                str = a5;
                                String str22 = f;
                                remote2.i(component2, session2, str, str22, "[OTA Start] Exception " + e);
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str32 = f;
                                local3.d(str32, "Inside .run failed with exception=" + e);
                                e.printStackTrace();
                                return new c();
                            }
                        }
                        str = "";
                        String str222 = f;
                        remote2.i(component2, session2, str, str222, "[OTA Start] Exception " + e);
                        ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                        String str322 = f;
                        local32.d(str322, "Inside .run failed with exception=" + e);
                        e.printStackTrace();
                        return new c();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                a aVar = g;
                fn2 fn2 = updateFirmwareUsecase.e;
                sku = device.getSku();
                if (sku != null) {
                    sku = "";
                }
                a2 = aVar.a(fn2, sku);
                if (a2 != null) {
                    return new c();
                }
                updateFirmwareUsecase.e.a(device.getDeviceId(), device.getFirmwareRevision());
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = f;
                local4.d(str5, "Start update firmware with version=" + a2.getFirmwareVersion() + ", currentVersion=" + device.getFirmwareRevision());
                UserProfile j = PortfolioApp.W.c().j();
                if (j != null) {
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, bVar.a(), f, "[OTA Start] Calling OTA from SDK");
                    PortfolioApp.W.c().a(bVar.a(), a2, j);
                    cb4 cb4 = cb4.a;
                } else {
                    new c();
                }
                return new d();
            }
        }
        updateFirmwareUsecase$run$Anon1 = new UpdateFirmwareUsecase$run$Anon1(this, kc4);
        Object obj2 = updateFirmwareUsecase$run$Anon1.result;
        Object a32 = oc4.a();
        i = updateFirmwareUsecase$run$Anon1.label;
        if (i != 0) {
        }
        a aVar2 = g;
        fn2 fn22 = updateFirmwareUsecase.e;
        sku = device.getSku();
        if (sku != null) {
        }
        a2 = aVar2.a(fn22, sku);
        if (a2 != null) {
        }
    }
}
