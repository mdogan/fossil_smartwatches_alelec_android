package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.qn3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActiveDeviceUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public b d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public c f;
    @DexIgnore
    public /* final */ BleCommandResultManager.b g; // = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ wj2 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ fn2 l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SwitchActiveDeviceUseCase.m;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            wd4.b(str, "newActiveSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(int i, ArrayList<Integer> arrayList, String str) {
            this.a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public d(Device device) {
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<qn3, on3> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qn3 qn3) {
            wd4.b(qn3, "responseValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting success");
        }

        @DexIgnore
        public void a(on3 on3) {
            wd4.b(on3, "errorValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting fail");
        }
    }

    /*
    static {
        String simpleName = SwitchActiveDeviceUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public SwitchActiveDeviceUseCase(UserRepository userRepository, DeviceRepository deviceRepository, wj2 wj2, PortfolioApp portfolioApp, fn2 fn2) {
        wd4.b(userRepository, "mUserRepository");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(wj2, "mDeviceSettingFactory");
        wd4.b(portfolioApp, "mApp");
        wd4.b(fn2, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = wj2;
        this.k = portfolioApp;
        this.l = fn2;
    }

    @DexIgnore
    public final Device e() {
        return this.e;
    }

    @DexIgnore
    public final b f() {
        return this.d;
    }

    @DexIgnore
    public final c g() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
        BleCommandResultManager.d.a(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).a(new pn3(str), new e());
    }

    @DexIgnore
    public String c() {
        return m;
    }

    @DexIgnore
    public final ri4 d() {
        return mg4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$doSwitchDevice$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void a(c cVar) {
        this.f = cVar;
    }

    @DexIgnore
    public Object a(b bVar, kc4<Object> kc4) {
        if (bVar == null) {
            return new c(600, (ArrayList<Integer>) null, "");
        }
        this.d = bVar;
        String e2 = this.k.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bVar.a() + " currentActive " + e2);
        if (bVar.a() != 4) {
            d();
        } else {
            a(bVar.b());
        }
        return new Object();
    }

    @DexIgnore
    public final ri4 a(String str) {
        return mg4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, MisfitDeviceProfile misfitDeviceProfile, kc4<? super Pair<Boolean, Integer>> kc4) {
        return kg4.a(zh4.b(), new SwitchActiveDeviceUseCase$linkServer$Anon2(this, misfitDeviceProfile, str, (kc4) null), kc4);
    }
}
