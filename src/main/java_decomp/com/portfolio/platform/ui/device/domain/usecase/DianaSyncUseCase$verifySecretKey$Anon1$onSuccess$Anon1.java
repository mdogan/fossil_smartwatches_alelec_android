package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
public final class DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncUseCase$verifySecretKey$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1(DianaSyncUseCase$verifySecretKey$Anon1 dianaSyncUseCase$verifySecretKey$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dianaSyncUseCase$verifySecretKey$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 = new DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1(this.this$Anon0, kc4);
        dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            DianaSyncUseCase$verifySecretKey$Anon1 dianaSyncUseCase$verifySecretKey$Anon1 = this.this$Anon0;
            dianaSyncUseCase$verifySecretKey$Anon1.a.a(dianaSyncUseCase$verifySecretKey$Anon1.c, dianaSyncUseCase$verifySecretKey$Anon1.d, dianaSyncUseCase$verifySecretKey$Anon1.e, dianaSyncUseCase$verifySecretKey$Anon1.b);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
