package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$Anon1", f = "UnlinkDeviceUseCase.kt", l = {57}, m = "invokeSuspend")
public final class UnlinkDeviceUseCase$doRemoveDevice$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UnlinkDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkDeviceUseCase$doRemoveDevice$Anon1(UnlinkDeviceUseCase unlinkDeviceUseCase, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = unlinkDeviceUseCase;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UnlinkDeviceUseCase$doRemoveDevice$Anon1 unlinkDeviceUseCase$doRemoveDevice$Anon1 = new UnlinkDeviceUseCase$doRemoveDevice$Anon1(this.this$Anon0, kc4);
        unlinkDeviceUseCase$doRemoveDevice$Anon1.p$ = (lh4) obj;
        return unlinkDeviceUseCase$doRemoveDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UnlinkDeviceUseCase$doRemoveDevice$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
        if (r12 != null) goto L_0x00d7;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Device device;
        String str;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            DeviceRepository a2 = this.this$Anon0.e;
            UnlinkDeviceUseCase.b b = this.this$Anon0.d;
            String str2 = null;
            String a3 = b != null ? b.a() : null;
            if (a3 != null) {
                Device deviceBySerial = a2.getDeviceBySerial(a3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = UnlinkDeviceUseCase.i.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doRemoveDevice ");
                if (deviceBySerial != null) {
                    str2 = deviceBySerial.getDeviceId();
                }
                sb.append(str2);
                local.d(a4, sb.toString());
                if (deviceBySerial != null) {
                    DeviceRepository a5 = this.this$Anon0.e;
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = deviceBySerial;
                    this.label = 1;
                    obj = a5.removeDevice(deviceBySerial, this);
                    if (obj == a) {
                        return a;
                    }
                    device = deviceBySerial;
                } else {
                    this.this$Anon0.a(new UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", ob4.a((T[]) new Integer[]{pc4.a(600)}), ""));
                    return cb4.a;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            device = (Device) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            String deviceId = device.getDeviceId();
            PortfolioApp.W.c().r(deviceId);
            if (DeviceHelper.o.f(deviceId)) {
                this.this$Anon0.g.deleteWatchFacesWithSerial(deviceId);
            }
            this.this$Anon0.a(new UnlinkDeviceUseCase.d());
        } else if (ro2 instanceof qo2) {
            UnlinkDeviceUseCase unlinkDeviceUseCase = this.this$Anon0;
            qo2 qo2 = (qo2) ro2;
            ArrayList a6 = ob4.a((T[]) new Integer[]{pc4.a(qo2.a())});
            ServerError c = qo2.c();
            if (c != null) {
                str = c.getUserMessage();
            }
            str = "";
            unlinkDeviceUseCase.a(new UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", a6, str));
        }
        return cb4.a;
    }
}
