package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1(LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1 linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1, DownloadFirmwareByDeviceModelUsecase.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1 = new LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, kc4);
        linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return linkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            String a = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.q.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a);
            FirmwareData a3 = UpdateFirmwareUsecase.g.a(this.this$Anon0.a.n, this.this$Anon0.a.f());
            if (a3 == null) {
                a3 = new EmptyFirmwareData();
            }
            PortfolioApp c = PortfolioApp.W.c();
            String h = this.this$Anon0.a.h();
            if (h != null) {
                c.a(h, (PairingResponse) PairingResponse.CREATOR.buildPairingUpdateFWResponse(a3));
                return cb4.a;
            }
            wd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
