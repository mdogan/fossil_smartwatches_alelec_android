package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1", f = "SwitchActiveDeviceUseCase.kt", l = {74}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1(SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1, String str, MisfitDeviceProfile misfitDeviceProfile, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1;
        this.$serial = str;
        this.$currentDeviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1 = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1(this.this$Anon0, this.$serial, this.$currentDeviceProfile, kc4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1.p$ = (lh4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0.a;
            String str = this.$serial;
            wd4.a((Object) str, "serial");
            MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = switchActiveDeviceUseCase.a(str, misfitDeviceProfile, this);
                if (obj == a) {
                    return a;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        boolean booleanValue = ((Boolean) pair.component1()).booleanValue();
        int intValue = ((Number) pair.component2()).intValue();
        PortfolioApp c = PortfolioApp.W.c();
        String str2 = this.$serial;
        wd4.a((Object) str2, "serial");
        c.a(str2, booleanValue, intValue);
        return cb4.a;
    }
}
