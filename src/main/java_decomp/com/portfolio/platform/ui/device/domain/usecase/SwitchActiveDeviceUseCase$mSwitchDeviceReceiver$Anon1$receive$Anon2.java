package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2", f = "SwitchActiveDeviceUseCase.kt", l = {95}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2(SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1, MisfitDeviceProfile misfitDeviceProfile, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2(this.this$Anon0, this.$currentDeviceProfile, this.$serial, kc4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2.p$ = (lh4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            int b = zk2.b(this.$currentDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
            DeviceRepository b2 = this.this$Anon0.a.i;
            String str = this.$serial;
            wd4.a((Object) str, "serial");
            Device deviceBySerial = b2.getDeviceBySerial(str);
            if (deviceBySerial != null) {
                Integer vibrationStrength = deviceBySerial.getVibrationStrength();
                if (vibrationStrength == null || vibrationStrength.intValue() != b) {
                    deviceBySerial.setVibrationStrength(pc4.a(b));
                    DeviceRepository b3 = this.this$Anon0.a.i;
                    this.L$Anon0 = lh4;
                    this.I$Anon0 = b;
                    this.L$Anon1 = deviceBySerial;
                    this.label = 1;
                    if (b3.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            }
        } else if (i == 1) {
            Device device = (Device) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
