package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zk2;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetVibrationStrengthUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((rd4) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ SetVibrationStrengthBroadcastReceiver g; // = new SetVibrationStrengthBroadcastReceiver();
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetVibrationStrengthBroadcastReceiver implements BleCommandResultManager.b {
        @DexIgnore
        public SetVibrationStrengthBroadcastReceiver() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SetVibrationStrengthUseCase.j.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + SetVibrationStrengthUseCase.this.f() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.SET_VIBRATION_STRENGTH && SetVibrationStrengthUseCase.this.f()) {
                boolean z = false;
                SetVibrationStrengthUseCase.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    ri4 unused = mg4.b(SetVibrationStrengthUseCase.this.b(), (CoroutineContext) null, (CoroutineStart) null, new SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1(this, (kc4) null), 3, (Object) null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                SetVibrationStrengthUseCase.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SetVibrationStrengthUseCase.i;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            wd4.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        String simpleName = SetVibrationStrengthUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetVibrationStrengthUseCase(DeviceRepository deviceRepository, fn2 fn2) {
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.h = deviceRepository;
    }

    @DexIgnore
    public String c() {
        return i;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.f;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public final void h() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(b bVar, kc4<Object> kc4) {
        try {
            FLogger.INSTANCE.getLocal().d(i, "running UseCase");
            this.d = true;
            Integer a2 = bVar != null ? pc4.a(bVar.b()) : null;
            if (a2 != null) {
                this.e = a2.intValue();
                this.f = bVar.a();
                PortfolioApp.W.c().a(bVar.a(), new VibrationStrengthObj(zk2.a(bVar.b()), false, 2, (rd4) null));
                return new Object();
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.e(str, "Error inside " + i + ".connectDevice - e=" + e2);
            return new c(600, -1, new ArrayList());
        }
    }
}
