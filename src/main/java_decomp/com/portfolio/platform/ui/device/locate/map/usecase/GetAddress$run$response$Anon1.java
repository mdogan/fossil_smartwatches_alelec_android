package com.portfolio.platform.ui.device.locate.map.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$Anon1", f = "GetAddress.kt", l = {19}, m = "invokeSuspend")
public final class GetAddress$run$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<yz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GetAddress.b $requestValues;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GetAddress this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetAddress$run$response$Anon1(GetAddress getAddress, GetAddress.b bVar, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = getAddress;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new GetAddress$run$response$Anon1(this.this$Anon0, this.$requestValues, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((GetAddress$run$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            GoogleApiService a2 = this.this$Anon0.d;
            StringBuilder sb = new StringBuilder();
            GetAddress.b bVar = this.$requestValues;
            Double d = null;
            sb.append(bVar != null ? pc4.a(bVar.a()) : null);
            sb.append(',');
            GetAddress.b bVar2 = this.$requestValues;
            if (bVar2 != null) {
                d = pc4.a(bVar2.b());
            }
            sb.append(d);
            String sb2 = sb.toString();
            this.label = 1;
            obj = a2.getAddress(sb2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
