package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.jl2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OverviewSleepWeekChart extends OverviewWeekChart {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public OverviewSleepWeekChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public float a(float f) {
        return ((f - (getMBarWidth() * ((float) getMNumberBar()))) - ((float) 20)) / ((float) (getMNumberBar() - 1));
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public String a(int i) {
        be4 be4 = be4.a;
        String a2 = tm2.a(getContext(), (int) R.string.DashboardDiana_Main_Sleep7days_Label__NumberHr);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026eep7days_Label__NumberHr)");
        Object[] objArr = {jl2.a(((float) i) / ((float) 60), 1).toString()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
