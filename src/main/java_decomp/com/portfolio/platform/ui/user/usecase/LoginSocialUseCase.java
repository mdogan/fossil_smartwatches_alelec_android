package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginSocialUseCase extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public b(int i, String str) {
            wd4.b(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, String str2, String str3) {
            wd4.b(str, Constants.SERVICE);
            wd4.b(str2, "token");
            wd4.b(str3, "clientId");
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public d(Auth auth) {
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = LoginSocialUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "LoginSocialUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public LoginSocialUseCase(UserRepository userRepository) {
        wd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ba, code lost:
        if (r10 != null) goto L_0x00be;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(c cVar, kc4<Object> kc4) {
        LoginSocialUseCase$run$Anon1 loginSocialUseCase$run$Anon1;
        int i;
        ro2 ro2;
        String str;
        if (kc4 instanceof LoginSocialUseCase$run$Anon1) {
            loginSocialUseCase$run$Anon1 = (LoginSocialUseCase$run$Anon1) kc4;
            int i2 = loginSocialUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                loginSocialUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = loginSocialUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i = loginSocialUseCase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (cVar == null) {
                        return new b(600, "");
                    }
                    UserRepository userRepository = this.d;
                    String b2 = cVar.b();
                    String c2 = cVar.c();
                    String a3 = cVar.a();
                    loginSocialUseCase$run$Anon1.L$Anon0 = this;
                    loginSocialUseCase$run$Anon1.L$Anon1 = cVar;
                    loginSocialUseCase$run$Anon1.label = 1;
                    obj = userRepository.loginWithSocial(b2, c2, a3, loginSocialUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    c cVar2 = (c) loginSocialUseCase$run$Anon1.L$Anon1;
                    LoginSocialUseCase loginSocialUseCase = (LoginSocialUseCase) loginSocialUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new d((Auth) ((so2) ro2).a());
                }
                if (!(ro2 instanceof qo2)) {
                    return new b(600, "");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .run failed with http code=");
                qo2 qo2 = (qo2) ro2;
                sb.append(qo2.a());
                local.d(str2, sb.toString());
                int a4 = qo2.a();
                ServerError c3 = qo2.c();
                if (c3 != null) {
                    str = c3.getMessage();
                }
                str = "";
                return new b(a4, str);
            }
        }
        loginSocialUseCase$run$Anon1 = new LoginSocialUseCase$run$Anon1(this, kc4);
        Object obj2 = loginSocialUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i = loginSocialUseCase$run$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
