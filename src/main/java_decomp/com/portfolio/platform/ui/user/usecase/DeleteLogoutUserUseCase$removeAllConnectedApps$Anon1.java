package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {187}, m = "removeAllConnectedApps")
public final class DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1(DeleteLogoutUserUseCase deleteLogoutUserUseCase, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((kc4<? super cb4>) this);
    }
}
