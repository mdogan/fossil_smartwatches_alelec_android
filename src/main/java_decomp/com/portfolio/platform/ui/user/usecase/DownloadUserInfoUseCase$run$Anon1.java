package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
public final class DownloadUserInfoUseCase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DownloadUserInfoUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DownloadUserInfoUseCase$run$Anon1(DownloadUserInfoUseCase downloadUserInfoUseCase, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = downloadUserInfoUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((DownloadUserInfoUseCase.c) null, (kc4<Object>) this);
    }
}
