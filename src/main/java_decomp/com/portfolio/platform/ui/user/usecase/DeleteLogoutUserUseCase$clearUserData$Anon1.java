package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.bp2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.js3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$Anon1", f = "DeleteLogoutUserUseCase.kt", l = {148}, m = "invokeSuspend")
public final class DeleteLogoutUserUseCase$clearUserData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$clearUserData$Anon1(DeleteLogoutUserUseCase deleteLogoutUserUseCase, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeleteLogoutUserUseCase$clearUserData$Anon1 deleteLogoutUserUseCase$clearUserData$Anon1 = new DeleteLogoutUserUseCase$clearUserData$Anon1(this.this$Anon0, kc4);
        deleteLogoutUserUseCase$clearUserData$Anon1.p$ = (lh4) obj;
        return deleteLogoutUserUseCase$clearUserData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteLogoutUserUseCase$clearUserData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            FLogger.INSTANCE.getRemote().flush();
            try {
                js3.b.f(ButtonService.DEVICE_SECRET_KEY);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DeleteLogoutUserUseCase.I.a();
                local.e(a2, "exception when remove alias in keystore " + e);
            }
            this.this$Anon0.k.clearAllNotificationSetting();
            this.this$Anon0.m.cleanUp();
            this.this$Anon0.q.cleanUp();
            this.this$Anon0.A.cleanUp();
            this.this$Anon0.f.V();
            this.this$Anon0.d.clearAllUser();
            PortfolioApp.W.c().S();
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.this$Anon0;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (deleteLogoutUserUseCase.a((kc4<? super cb4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.x.cleanUp();
        this.this$Anon0.y.cleanUp();
        this.this$Anon0.z.cleanUp();
        this.this$Anon0.g.cleanUp();
        this.this$Anon0.w.cleanUp();
        this.this$Anon0.h.cleanUp();
        this.this$Anon0.i.cleanUp();
        this.this$Anon0.n.cleanUp();
        this.this$Anon0.r.cleanUp();
        this.this$Anon0.s.cleanUp();
        this.this$Anon0.t.cleanUp();
        this.this$Anon0.j.clearData();
        this.this$Anon0.e.cleanUp();
        en2.p.a().o();
        bp2.g.a();
        PortfolioApp.W.c().c();
        PortfolioApp.W.c().F();
        this.this$Anon0.u.getNotificationSettingsDao().delete();
        this.this$Anon0.v.getDNDScheduledTimeDao().delete();
        this.this$Anon0.E.getInactivityNudgeTimeDao().delete();
        this.this$Anon0.E.getRemindTimeDao().delete();
        this.this$Anon0.B.cleanUp();
        this.this$Anon0.C.cleanUp();
        this.this$Anon0.D.cleanUp();
        ThirdPartyDatabase w = this.this$Anon0.F;
        w.getGFitSampleDao().clearAll();
        w.getGFitActiveTimeDao().clearAll();
        w.getGFitWorkoutSessionDao().clearAll();
        w.getUASampleDao().clearAll();
        this.this$Anon0.a(new DeleteLogoutUserUseCase.d());
        this.this$Anon0.l.cleanUp();
        return cb4.a;
    }
}
