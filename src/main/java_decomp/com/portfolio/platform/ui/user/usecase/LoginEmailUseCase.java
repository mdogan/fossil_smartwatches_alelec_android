package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginEmailUseCase extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            wd4.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            wd4.b(str, "email");
            wd4.b(str2, "password");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public d(Auth auth) {
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = LoginEmailUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "LoginEmailUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public LoginEmailUseCase(UserRepository userRepository) {
        wd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c7, code lost:
        if (r9 != null) goto L_0x00cb;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(c cVar, kc4<Object> kc4) {
        LoginEmailUseCase$run$Anon1 loginEmailUseCase$run$Anon1;
        int i;
        ro2 ro2;
        String str;
        if (kc4 instanceof LoginEmailUseCase$run$Anon1) {
            loginEmailUseCase$run$Anon1 = (LoginEmailUseCase$run$Anon1) kc4;
            int i2 = loginEmailUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                loginEmailUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = loginEmailUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i = loginEmailUseCase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (cVar == null) {
                        return new b(600, "");
                    }
                    String a3 = cVar.a();
                    if (a3 != null) {
                        String lowerCase = a3.toLowerCase();
                        wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        UserRepository userRepository = this.d;
                        String b2 = cVar.b();
                        loginEmailUseCase$run$Anon1.L$Anon0 = this;
                        loginEmailUseCase$run$Anon1.L$Anon1 = cVar;
                        loginEmailUseCase$run$Anon1.L$Anon2 = lowerCase;
                        loginEmailUseCase$run$Anon1.label = 1;
                        obj = userRepository.loginEmail(lowerCase, b2, loginEmailUseCase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else if (i == 1) {
                    String str2 = (String) loginEmailUseCase$run$Anon1.L$Anon2;
                    c cVar2 = (c) loginEmailUseCase$run$Anon1.L$Anon1;
                    LoginEmailUseCase loginEmailUseCase = (LoginEmailUseCase) loginEmailUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new d((Auth) ((so2) ro2).a());
                }
                if (!(ro2 instanceof qo2)) {
                    return new b(600, "");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .run failed with http code=");
                qo2 qo2 = (qo2) ro2;
                sb.append(qo2.a());
                local.d(str3, sb.toString());
                int a4 = qo2.a();
                ServerError c2 = qo2.c();
                if (c2 != null) {
                    str = c2.getMessage();
                }
                str = "";
                return new b(a4, str);
            }
        }
        loginEmailUseCase$run$Anon1 = new LoginEmailUseCase$run$Anon1(this, kc4);
        Object obj2 = loginEmailUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i = loginEmailUseCase$run$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
