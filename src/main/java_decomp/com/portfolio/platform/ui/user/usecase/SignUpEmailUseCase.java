package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpEmailUseCase extends CoroutineUseCase<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ fn2 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ SignUpEmailAuth a;

        @DexIgnore
        public a(SignUpEmailAuth signUpEmailAuth) {
            wd4.b(signUpEmailAuth, "emailAuth");
            this.a = signUpEmailAuth;
        }

        @DexIgnore
        public final SignUpEmailAuth a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            wd4.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.d {
    }

    @DexIgnore
    public SignUpEmailUseCase(UserRepository userRepository, fn2 fn2) {
        wd4.b(userRepository, "mUserRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = fn2;
    }

    @DexIgnore
    public String c() {
        return "SignUpEmailUseCase";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        if (r9 != null) goto L_0x00e2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public Object a(a aVar, kc4<Object> kc4) {
        SignUpEmailUseCase$run$Anon1 signUpEmailUseCase$run$Anon1;
        int i;
        SignUpEmailUseCase signUpEmailUseCase;
        ro2 ro2;
        String str;
        if (kc4 instanceof SignUpEmailUseCase$run$Anon1) {
            signUpEmailUseCase$run$Anon1 = (SignUpEmailUseCase$run$Anon1) kc4;
            int i2 = signUpEmailUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                signUpEmailUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = signUpEmailUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i = signUpEmailUseCase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "running UseCase");
                    if (aVar == null) {
                        return new b(600, "");
                    }
                    if (this.d.getCurrentUser() != null) {
                        return new c();
                    }
                    UserRepository userRepository = this.d;
                    SignUpEmailAuth a3 = aVar.a();
                    signUpEmailUseCase$run$Anon1.L$Anon0 = this;
                    signUpEmailUseCase$run$Anon1.L$Anon1 = aVar;
                    signUpEmailUseCase$run$Anon1.label = 1;
                    obj = userRepository.signUpEmail(a3, signUpEmailUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                    signUpEmailUseCase = this;
                } else if (i == 1) {
                    a aVar2 = (a) signUpEmailUseCase$run$Anon1.L$Anon1;
                    signUpEmailUseCase = (SignUpEmailUseCase) signUpEmailUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                String str2 = null;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "signUpEmail success");
                    fn2 fn2 = signUpEmailUseCase.e;
                    Auth auth = (Auth) ((so2) ro2).a();
                    if (auth != null) {
                        str2 = auth.getAccessToken();
                    }
                    fn2.w(str2);
                    return new c();
                } else if (!(ro2 instanceof qo2)) {
                    return new b(600, "");
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("signUpEmail failed code=");
                    qo2 qo2 = (qo2) ro2;
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local.d("SignUpEmailUseCase", sb.toString());
                    int a4 = qo2.a();
                    ServerError c3 = qo2.c();
                    if (c3 != null) {
                        str = c3.getMessage();
                    }
                    str = "";
                    return new b(a4, str);
                }
            }
        }
        signUpEmailUseCase$run$Anon1 = new SignUpEmailUseCase$run$Anon1(this, kc4);
        Object obj2 = signUpEmailUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i = signUpEmailUseCase$run$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        String str22 = null;
        if (!(ro2 instanceof so2)) {
        }
    }
}
