package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.response.ResponseKt;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ResetPasswordUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "email");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = ResetPasswordUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ResetPasswordUseCase(AuthApiGuestService authApiGuestService) {
        wd4.b(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ec, code lost:
        if (r10 != null) goto L_0x00f0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(b bVar, kc4<Object> kc4) {
        ResetPasswordUseCase$run$Anon1 resetPasswordUseCase$run$Anon1;
        int i;
        ro2 ro2;
        int i2;
        ServerError c2;
        String str;
        if (kc4 instanceof ResetPasswordUseCase$run$Anon1) {
            resetPasswordUseCase$run$Anon1 = (ResetPasswordUseCase$run$Anon1) kc4;
            int i3 = resetPasswordUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                resetPasswordUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = resetPasswordUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i = resetPasswordUseCase$run$Anon1.label;
                Integer num = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("running UseCase with email=");
                    sb.append(bVar != null ? bVar.a() : null);
                    local.d(str2, sb.toString());
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    yz1 yz1 = new yz1();
                    yz1.a("email", bVar.a());
                    ResetPasswordUseCase$run$response$Anon1 resetPasswordUseCase$run$response$Anon1 = new ResetPasswordUseCase$run$response$Anon1(this, yz1, (kc4) null);
                    resetPasswordUseCase$run$Anon1.L$Anon0 = this;
                    resetPasswordUseCase$run$Anon1.L$Anon1 = bVar;
                    resetPasswordUseCase$run$Anon1.L$Anon2 = yz1;
                    resetPasswordUseCase$run$Anon1.label = 1;
                    obj = ResponseKt.a(resetPasswordUseCase$run$response$Anon1, resetPasswordUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) resetPasswordUseCase$run$Anon1.L$Anon2;
                    b bVar2 = (b) resetPasswordUseCase$run$Anon1.L$Anon1;
                    ResetPasswordUseCase resetPasswordUseCase = (ResetPasswordUseCase) resetPasswordUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new d();
                }
                if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = e;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Inside .run failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    ServerError c3 = qo2.c();
                    if (c3 != null) {
                        num = c3.getCode();
                    }
                    sb2.append(num);
                    local2.d(str3, sb2.toString());
                    ServerError c4 = qo2.c();
                    if (c4 != null) {
                        Integer code = c4.getCode();
                        if (code != null) {
                            i2 = code.intValue();
                            c2 = qo2.c();
                            if (c2 != null) {
                                str = c2.getMessage();
                            }
                            str = "";
                            return new c(i2, str);
                        }
                    }
                    i2 = qo2.a();
                    c2 = qo2.c();
                    if (c2 != null) {
                    }
                    str = "";
                    return new c(i2, str);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        resetPasswordUseCase$run$Anon1 = new ResetPasswordUseCase$run$Anon1(this, kc4);
        Object obj2 = resetPasswordUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i = resetPasswordUseCase$run$Anon1.label;
        Integer num2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
