package com.portfolio.platform.ui;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1", f = "BaseActivity.kt", l = {}, m = "invokeSuspend")
public final class BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity$mButtonServiceConnection$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1(BaseActivity$mButtonServiceConnection$Anon1 baseActivity$mButtonServiceConnection$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = baseActivity$mButtonServiceConnection$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1 = new BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1(this.this$Anon0, kc4);
        baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1.p$ = (lh4) obj;
        return baseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BaseActivity$mButtonServiceConnection$Anon1$onServiceConnected$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            PortfolioApp.a aVar = PortfolioApp.W;
            IButtonConnectivity a = BaseActivity.A.a();
            if (a != null) {
                aVar.b(a);
                try {
                    MFUser currentUser = this.this$Anon0.a.d().getCurrentUser();
                    if (!(currentUser == null || BaseActivity.A.a() == null)) {
                        IButtonConnectivity a2 = BaseActivity.A.a();
                        if (a2 != null) {
                            a2.updateUserId(currentUser.getUserId());
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    this.this$Anon0.a.n();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String f = this.this$Anon0.a.f();
                    local.e(f, ".onServiceConnected(), ex=" + e);
                    e.printStackTrace();
                }
                return cb4.a;
            }
            wd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
