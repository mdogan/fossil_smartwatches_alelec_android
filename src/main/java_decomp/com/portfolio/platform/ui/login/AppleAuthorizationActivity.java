package com.portfolio.platform.ui.login;

import android.content.Intent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.data.AppleAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppleAuthorizationActivity extends BaseWebViewActivity {
    @DexIgnore
    public static /* final */ a F; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str) {
            wd4.b(fragmentActivity, Constants.ACTIVITY);
            wd4.b(str, "authorizationUrl");
            Intent intent = new Intent(fragmentActivity, AppleAuthorizationActivity.class);
            intent.putExtra("urlToLoad", str);
            fragmentActivity.startActivityForResult(intent, 3535);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ AppleAuthorizationActivity a;

        @DexIgnore
        public b(AppleAuthorizationActivity appleAuthorizationActivity) {
            this.a = appleAuthorizationActivity;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[Catch:{ Exception -> 0x00c7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6 A[Catch:{ Exception -> 0x00c7 }] */
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            boolean z;
            boolean z2 = false;
            if (webResourceRequest != null) {
                try {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = this.a.f();
                    local.d(a2, "request = " + webResourceRequest.getUrl());
                    String queryParameter = webResourceRequest.getUrl().getQueryParameter("id_token");
                    if (queryParameter != null) {
                        if (!cg4.a(queryParameter)) {
                            z = false;
                            if (z) {
                                if (this.a.getIntent() == null) {
                                    this.a.setIntent(new Intent());
                                }
                                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                                signUpSocialAuth.setService("apple");
                                signUpSocialAuth.setToken(queryParameter);
                                String queryParameter2 = webResourceRequest.getUrl().getQueryParameter("user");
                                if (queryParameter2 == null || cg4.a(queryParameter2)) {
                                    z2 = true;
                                }
                                if (!z2) {
                                    AppleAuth appleAuth = (AppleAuth) new Gson().a(queryParameter2, AppleAuth.class);
                                    signUpSocialAuth.setEmail(appleAuth.getEmail());
                                    signUpSocialAuth.setLastName(appleAuth.getName().getLastName());
                                    signUpSocialAuth.setFirstName(appleAuth.getName().getFirstName());
                                }
                                this.a.getIntent().putExtra("USER_INFO_EXTRA", signUpSocialAuth);
                                this.a.setResult(-1, this.a.getIntent());
                                z2 = true;
                            } else {
                                z2 = wd4.a((Object) webResourceRequest.getUrl().getQueryParameter("error"), (Object) "user_cancelled_authorize");
                            }
                        }
                    }
                    z = true;
                    if (z) {
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = this.a.f();
                    local2.d(a3, "Get authorization info with error: " + e.getMessage());
                }
            }
            if (!z2) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            this.a.finish();
            return true;
        }
    }

    @DexIgnore
    public WebViewClient s() {
        return new b(this);
    }
}
