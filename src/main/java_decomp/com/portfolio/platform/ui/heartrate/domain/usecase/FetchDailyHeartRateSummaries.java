package com.portfolio.platform.ui.heartrate.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchDailyHeartRateSummaries extends CoroutineUseCase<b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = FetchDailyHeartRateSummaries.class.getSimpleName();
        wd4.a((Object) simpleName, "FetchDailyHeartRateSumma\u2026es::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchDailyHeartRateSummaries(HeartRateSummaryRepository heartRateSummaryRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        wd4.b(heartRateSummaryRepository, "mHeartRateSummariesRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        this.d = heartRateSummaryRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(b bVar, kc4<? super cb4> kc4) {
        FetchDailyHeartRateSummaries$run$Anon1 fetchDailyHeartRateSummaries$run$Anon1;
        int i;
        Date date;
        Date date2;
        if (kc4 instanceof FetchDailyHeartRateSummaries$run$Anon1) {
            fetchDailyHeartRateSummaries$run$Anon1 = (FetchDailyHeartRateSummaries$run$Anon1) kc4;
            int i2 = fetchDailyHeartRateSummaries$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchDailyHeartRateSummaries$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fetchDailyHeartRateSummaries$run$Anon1.result;
                Object a2 = oc4.a();
                i = fetchDailyHeartRateSummaries$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return cb4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = g;
                    local.d(str, "executeUseCase - date=" + gt3.a(a3));
                    MFUser currentUser = this.e.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cb4.a;
                    }
                    Date d2 = sk2.d(currentUser.getCreatedAt());
                    Calendar instance = Calendar.getInstance();
                    wd4.a((Object) instance, "calendar");
                    instance.setTime(a3);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    wd4.a((Object) d2, "createdDate");
                    if (sk2.a(timeInMillis, d2.getTime())) {
                        date = d2;
                    } else {
                        Calendar e2 = sk2.e(instance);
                        wd4.a((Object) e2, "DateHelper.getStartOfMonth(calendar)");
                        date = e2.getTime();
                        wd4.a((Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (sk2.b(d2, date)) {
                            return cb4.a;
                        }
                    }
                    Boolean r = sk2.r(date);
                    wd4.a((Object) r, "DateHelper.isThisMonth(startDate)");
                    if (r.booleanValue()) {
                        date2 = new Date();
                    } else {
                        Calendar j = sk2.j(date);
                        wd4.a((Object) j, "DateHelper.getEndOfMonth(startDate)");
                        date2 = j.getTime();
                        wd4.a((Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    List<FitnessDataWrapper> fitnessData = this.f.getFitnessData(date, date2);
                    if (fitnessData.isEmpty()) {
                        HeartRateSummaryRepository heartRateSummaryRepository = this.d;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon0 = this;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon1 = bVar;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon2 = a3;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon3 = currentUser;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon4 = d2;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon5 = instance;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon6 = date2;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon7 = date;
                        fetchDailyHeartRateSummaries$run$Anon1.L$Anon8 = fitnessData;
                        fetchDailyHeartRateSummaries$run$Anon1.label = 1;
                        if (heartRateSummaryRepository.loadSummaries(date, date2, fetchDailyHeartRateSummaries$run$Anon1) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) fetchDailyHeartRateSummaries$run$Anon1.L$Anon8;
                    Date date3 = (Date) fetchDailyHeartRateSummaries$run$Anon1.L$Anon7;
                    Date date4 = (Date) fetchDailyHeartRateSummaries$run$Anon1.L$Anon6;
                    Calendar calendar = (Calendar) fetchDailyHeartRateSummaries$run$Anon1.L$Anon5;
                    Date date5 = (Date) fetchDailyHeartRateSummaries$run$Anon1.L$Anon4;
                    MFUser mFUser = (MFUser) fetchDailyHeartRateSummaries$run$Anon1.L$Anon3;
                    Date date6 = (Date) fetchDailyHeartRateSummaries$run$Anon1.L$Anon2;
                    b bVar2 = (b) fetchDailyHeartRateSummaries$run$Anon1.L$Anon1;
                    FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = (FetchDailyHeartRateSummaries) fetchDailyHeartRateSummaries$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        fetchDailyHeartRateSummaries$run$Anon1 = new FetchDailyHeartRateSummaries$run$Anon1(this, kc4);
        Object obj2 = fetchDailyHeartRateSummaries$run$Anon1.result;
        Object a22 = oc4.a();
        i = fetchDailyHeartRateSummaries$run$Anon1.label;
        if (i != 0) {
        }
        return cb4.a;
    }
}
