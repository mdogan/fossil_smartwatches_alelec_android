package com.portfolio.platform.ui.heartrate.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchHeartRateSamples extends CoroutineUseCase<b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSampleRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = FetchHeartRateSamples.class.getSimpleName();
        wd4.a((Object) simpleName, "FetchHeartRateSamples::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchHeartRateSamples(HeartRateSampleRepository heartRateSampleRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository) {
        wd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(userRepository, "mUserRepository");
        this.d = heartRateSampleRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(b bVar, kc4<? super cb4> kc4) {
        FetchHeartRateSamples$run$Anon1 fetchHeartRateSamples$run$Anon1;
        int i;
        if (kc4 instanceof FetchHeartRateSamples$run$Anon1) {
            fetchHeartRateSamples$run$Anon1 = (FetchHeartRateSamples$run$Anon1) kc4;
            int i2 = fetchHeartRateSamples$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchHeartRateSamples$run$Anon1.label = i2 - Integer.MIN_VALUE;
                FetchHeartRateSamples$run$Anon1 fetchHeartRateSamples$run$Anon12 = fetchHeartRateSamples$run$Anon1;
                Object obj = fetchHeartRateSamples$run$Anon12.result;
                Object a2 = oc4.a();
                i = fetchHeartRateSamples$run$Anon12.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return cb4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = g;
                    local.d(str, "executeUseCase - date=" + gt3.a(a3));
                    MFUser currentUser = this.f.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cb4.a;
                    }
                    Date d2 = sk2.d(currentUser.getCreatedAt());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = g;
                    StringBuilder sb = new StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    wd4.a((Object) d2, "createdDate");
                    sb.append(gt3.a(d2));
                    local3.d(str3, sb.toString());
                    if (sk2.b(d2, a3) || sk2.b(a3, new Date())) {
                        return cb4.a;
                    }
                    Calendar p = sk2.p(a3);
                    wd4.a((Object) p, "DateHelper.getStartOfWeek(date)");
                    Date time = p.getTime();
                    if (sk2.c(d2, time)) {
                        time = d2;
                    }
                    FitnessDataRepository fitnessDataRepository = this.e;
                    wd4.a((Object) time, GoalPhase.COLUMN_START_DATE);
                    List<FitnessDataWrapper> fitnessData = fitnessDataRepository.getFitnessData(time, a3);
                    if (fitnessData.isEmpty()) {
                        HeartRateSampleRepository heartRateSampleRepository = this.d;
                        fetchHeartRateSamples$run$Anon12.L$Anon0 = this;
                        fetchHeartRateSamples$run$Anon12.L$Anon1 = bVar;
                        fetchHeartRateSamples$run$Anon12.L$Anon2 = a3;
                        fetchHeartRateSamples$run$Anon12.L$Anon3 = currentUser;
                        fetchHeartRateSamples$run$Anon12.L$Anon4 = d2;
                        fetchHeartRateSamples$run$Anon12.L$Anon5 = time;
                        fetchHeartRateSamples$run$Anon12.L$Anon6 = fitnessData;
                        fetchHeartRateSamples$run$Anon12.label = 1;
                        if (HeartRateSampleRepository.fetchHeartRateSamples$default(heartRateSampleRepository, time, a3, 0, 0, fetchHeartRateSamples$run$Anon12, 12, (Object) null) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) fetchHeartRateSamples$run$Anon12.L$Anon6;
                    Date date = (Date) fetchHeartRateSamples$run$Anon12.L$Anon5;
                    Date date2 = (Date) fetchHeartRateSamples$run$Anon12.L$Anon4;
                    MFUser mFUser = (MFUser) fetchHeartRateSamples$run$Anon12.L$Anon3;
                    Date date3 = (Date) fetchHeartRateSamples$run$Anon12.L$Anon2;
                    b bVar2 = (b) fetchHeartRateSamples$run$Anon12.L$Anon1;
                    FetchHeartRateSamples fetchHeartRateSamples = (FetchHeartRateSamples) fetchHeartRateSamples$run$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        fetchHeartRateSamples$run$Anon1 = new FetchHeartRateSamples$run$Anon1(this, kc4);
        FetchHeartRateSamples$run$Anon1 fetchHeartRateSamples$run$Anon122 = fetchHeartRateSamples$run$Anon1;
        Object obj2 = fetchHeartRateSamples$run$Anon122.result;
        Object a22 = oc4.a();
        i = fetchHeartRateSamples$run$Anon122.label;
        if (i != 0) {
        }
        return cb4.a;
    }
}
