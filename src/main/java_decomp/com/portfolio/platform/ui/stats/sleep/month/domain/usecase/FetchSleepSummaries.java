package com.portfolio.platform.ui.stats.sleep.month.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchSleepSummaries extends CoroutineUseCase<b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SleepSummariesRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;
    @DexIgnore
    public /* final */ FitnessDataRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = FetchSleepSummaries.class.getSimpleName();
        wd4.a((Object) simpleName, "FetchSleepSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public FetchSleepSummaries(SleepSummariesRepository sleepSummariesRepository, UserRepository userRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository) {
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        this.d = sleepSummariesRepository;
        this.e = userRepository;
        this.f = sleepSessionsRepository;
        this.g = fitnessDataRepository;
    }

    @DexIgnore
    public String c() {
        return h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(b bVar, kc4<? super cb4> kc4) {
        FetchSleepSummaries$run$Anon1 fetchSleepSummaries$run$Anon1;
        int i;
        Date date;
        Date date2;
        if (kc4 instanceof FetchSleepSummaries$run$Anon1) {
            fetchSleepSummaries$run$Anon1 = (FetchSleepSummaries$run$Anon1) kc4;
            int i2 = fetchSleepSummaries$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchSleepSummaries$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fetchSleepSummaries$run$Anon1.result;
                Object a2 = oc4.a();
                i = fetchSleepSummaries$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return cb4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = h;
                    local.d(str, "executeUseCase - date=" + gt3.a(a3));
                    MFUser currentUser = this.e.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = h;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cb4.a;
                    }
                    Date d2 = sk2.d(currentUser.getCreatedAt());
                    Calendar instance = Calendar.getInstance();
                    wd4.a((Object) instance, "calendar");
                    instance.setTime(a3);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    wd4.a((Object) d2, "createdDate");
                    if (sk2.a(timeInMillis, d2.getTime())) {
                        date = d2;
                    } else {
                        Calendar e2 = sk2.e(instance);
                        wd4.a((Object) e2, "DateHelper.getStartOfMonth(calendar)");
                        date = e2.getTime();
                        wd4.a((Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (sk2.b(d2, date)) {
                            return cb4.a;
                        }
                    }
                    Boolean r = sk2.r(date);
                    wd4.a((Object) r, "DateHelper.isThisMonth(startDate)");
                    if (r.booleanValue()) {
                        date2 = new Date();
                    } else {
                        Calendar j = sk2.j(date);
                        wd4.a((Object) j, "DateHelper.getEndOfMonth(startDate)");
                        date2 = j.getTime();
                        wd4.a((Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    List<MFSleepSession> pendingSleepSessions = this.f.getPendingSleepSessions(date, date2);
                    List<FitnessDataWrapper> fitnessData = this.g.getFitnessData(date, date2);
                    if (pendingSleepSessions.isEmpty() && fitnessData.isEmpty()) {
                        SleepSummariesRepository sleepSummariesRepository = this.d;
                        fetchSleepSummaries$run$Anon1.L$Anon0 = this;
                        fetchSleepSummaries$run$Anon1.L$Anon1 = bVar;
                        fetchSleepSummaries$run$Anon1.L$Anon2 = a3;
                        fetchSleepSummaries$run$Anon1.L$Anon3 = currentUser;
                        fetchSleepSummaries$run$Anon1.L$Anon4 = d2;
                        fetchSleepSummaries$run$Anon1.L$Anon5 = instance;
                        fetchSleepSummaries$run$Anon1.L$Anon6 = date2;
                        fetchSleepSummaries$run$Anon1.L$Anon7 = date;
                        fetchSleepSummaries$run$Anon1.L$Anon8 = pendingSleepSessions;
                        fetchSleepSummaries$run$Anon1.L$Anon9 = fitnessData;
                        fetchSleepSummaries$run$Anon1.label = 1;
                        if (sleepSummariesRepository.fetchSleepSummaries(date, date2, fetchSleepSummaries$run$Anon1) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) fetchSleepSummaries$run$Anon1.L$Anon9;
                    List list2 = (List) fetchSleepSummaries$run$Anon1.L$Anon8;
                    Date date3 = (Date) fetchSleepSummaries$run$Anon1.L$Anon7;
                    Date date4 = (Date) fetchSleepSummaries$run$Anon1.L$Anon6;
                    Calendar calendar = (Calendar) fetchSleepSummaries$run$Anon1.L$Anon5;
                    Date date5 = (Date) fetchSleepSummaries$run$Anon1.L$Anon4;
                    MFUser mFUser = (MFUser) fetchSleepSummaries$run$Anon1.L$Anon3;
                    Date date6 = (Date) fetchSleepSummaries$run$Anon1.L$Anon2;
                    b bVar2 = (b) fetchSleepSummaries$run$Anon1.L$Anon1;
                    FetchSleepSummaries fetchSleepSummaries = (FetchSleepSummaries) fetchSleepSummaries$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        fetchSleepSummaries$run$Anon1 = new FetchSleepSummaries$run$Anon1(this, kc4);
        Object obj2 = fetchSleepSummaries$run$Anon1.result;
        Object a22 = oc4.a();
        i = fetchSleepSummaries$run$Anon1.label;
        if (i != 0) {
        }
        return cb4.a;
    }
}
