package com.portfolio.platform.ui.stats.sleep.day.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchSleepSessions extends CoroutineUseCase<b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ SleepSessionsRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = FetchSleepSessions.class.getSimpleName();
        wd4.a((Object) simpleName, "FetchSleepSessions::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchSleepSessions(SleepSessionsRepository sleepSessionsRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        wd4.b(sleepSessionsRepository, "mRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        this.d = sleepSessionsRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(b bVar, kc4<? super cb4> kc4) {
        FetchSleepSessions$run$Anon1 fetchSleepSessions$run$Anon1;
        int i;
        if (kc4 instanceof FetchSleepSessions$run$Anon1) {
            fetchSleepSessions$run$Anon1 = (FetchSleepSessions$run$Anon1) kc4;
            int i2 = fetchSleepSessions$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchSleepSessions$run$Anon1.label = i2 - Integer.MIN_VALUE;
                FetchSleepSessions$run$Anon1 fetchSleepSessions$run$Anon12 = fetchSleepSessions$run$Anon1;
                Object obj = fetchSleepSessions$run$Anon12.result;
                Object a2 = oc4.a();
                i = fetchSleepSessions$run$Anon12.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return cb4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = g;
                    local.d(str, "executeUseCase - date=" + gt3.a(a3));
                    MFUser currentUser = this.e.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cb4.a;
                    }
                    Date d2 = sk2.d(currentUser.getCreatedAt());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = g;
                    StringBuilder sb = new StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    wd4.a((Object) d2, "createdDate");
                    sb.append(gt3.a(d2));
                    local3.d(str3, sb.toString());
                    if (sk2.b(d2, a3) || sk2.b(a3, new Date())) {
                        return cb4.a;
                    }
                    Calendar p = sk2.p(a3);
                    wd4.a((Object) p, "DateHelper.getStartOfWeek(date)");
                    Date time = p.getTime();
                    if (sk2.c(d2, time)) {
                        time = d2;
                    }
                    FitnessDataRepository fitnessDataRepository = this.f;
                    wd4.a((Object) time, GoalPhase.COLUMN_START_DATE);
                    List<FitnessDataWrapper> fitnessData = fitnessDataRepository.getFitnessData(time, a3);
                    if (fitnessData.isEmpty()) {
                        SleepSessionsRepository sleepSessionsRepository = this.d;
                        fetchSleepSessions$run$Anon12.L$Anon0 = this;
                        fetchSleepSessions$run$Anon12.L$Anon1 = bVar;
                        fetchSleepSessions$run$Anon12.L$Anon2 = a3;
                        fetchSleepSessions$run$Anon12.L$Anon3 = currentUser;
                        fetchSleepSessions$run$Anon12.L$Anon4 = d2;
                        fetchSleepSessions$run$Anon12.L$Anon5 = time;
                        fetchSleepSessions$run$Anon12.L$Anon6 = fitnessData;
                        fetchSleepSessions$run$Anon12.label = 1;
                        if (SleepSessionsRepository.fetchSleepSessions$default(sleepSessionsRepository, time, a3, 0, 0, fetchSleepSessions$run$Anon12, 12, (Object) null) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) fetchSleepSessions$run$Anon12.L$Anon6;
                    Date date = (Date) fetchSleepSessions$run$Anon12.L$Anon5;
                    Date date2 = (Date) fetchSleepSessions$run$Anon12.L$Anon4;
                    MFUser mFUser = (MFUser) fetchSleepSessions$run$Anon12.L$Anon3;
                    Date date3 = (Date) fetchSleepSessions$run$Anon12.L$Anon2;
                    b bVar2 = (b) fetchSleepSessions$run$Anon12.L$Anon1;
                    FetchSleepSessions fetchSleepSessions = (FetchSleepSessions) fetchSleepSessions$run$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        fetchSleepSessions$run$Anon1 = new FetchSleepSessions$run$Anon1(this, kc4);
        FetchSleepSessions$run$Anon1 fetchSleepSessions$run$Anon122 = fetchSleepSessions$run$Anon1;
        Object obj2 = fetchSleepSessions$run$Anon122.result;
        Object a22 = oc4.a();
        i = fetchSleepSessions$run$Anon122.label;
        if (i != 0) {
        }
        return cb4.a;
    }
}
