package com.portfolio.platform.ui.stats.activity.day.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities", f = "FetchActivities.kt", l = {45}, m = "run")
public final class FetchActivities$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ FetchActivities this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchActivities$run$Anon1(FetchActivities fetchActivities, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = fetchActivities;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((FetchActivities.b) null, (kc4<? super cb4>) this);
    }
}
