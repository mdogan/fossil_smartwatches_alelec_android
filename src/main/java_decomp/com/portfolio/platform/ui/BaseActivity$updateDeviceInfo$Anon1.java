package com.portfolio.platform.ui;

import android.widget.TextView;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$Anon1", f = "BaseActivity.kt", l = {698}, m = "invokeSuspend")
public final class BaseActivity$updateDeviceInfo$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$updateDeviceInfo$Anon1(BaseActivity baseActivity, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = baseActivity;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BaseActivity$updateDeviceInfo$Anon1 baseActivity$updateDeviceInfo$Anon1 = new BaseActivity$updateDeviceInfo$Anon1(this.this$Anon0, kc4);
        baseActivity$updateDeviceInfo$Anon1.p$ = (lh4) obj;
        return baseActivity$updateDeviceInfo$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BaseActivity$updateDeviceInfo$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            DeviceRepository b = this.this$Anon0.b();
            String a2 = this.this$Anon0.u;
            if (a2 != null) {
                Device deviceBySerial = b.getDeviceBySerial(a2);
                if (deviceBySerial != null) {
                    bj4 c = zh4.c();
                    BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((kc4) null, this, deviceBySerial);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = deviceBySerial;
                    this.L$Anon2 = deviceBySerial;
                    this.label = 1;
                    if (kg4.a(c, baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                        return a;
                    }
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            Device device = (Device) this.L$Anon2;
            Device device2 = (Device) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser b2 = en2.p.a().n().b();
        String accessTokenExpiresAt = b2 != null ? b2.getAccessTokenExpiresAt() : null;
        TextView c2 = this.this$Anon0.n;
        if (c2 != null) {
            if (accessTokenExpiresAt == null) {
                accessTokenExpiresAt = "";
            }
            c2.setText(accessTokenExpiresAt);
            return cb4.a;
        }
        wd4.a();
        throw null;
    }
}
