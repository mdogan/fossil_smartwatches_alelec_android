package com.portfolio.platform.ui.debug;

import android.content.DialogInterface;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.io.File;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 implements DialogInterface.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ Firmware e;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity f;
    @DexIgnore
    public /* final */ /* synthetic */ DebugFirmwareData g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0129Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super File>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0129Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0129Anon1 anon1 = new C0129Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0129Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    FirmwareFileRepository x = this.this$Anon0.this$Anon0.f.x();
                    String versionNumber = this.this$Anon0.this$Anon0.e.getVersionNumber();
                    wd4.a((Object) versionNumber, "firmware.versionNumber");
                    String downloadUrl = this.this$Anon0.this$Anon0.e.getDownloadUrl();
                    wd4.a((Object) downloadUrl, "firmware.downloadUrl");
                    String checksum = this.this$Anon0.this$Anon0.e.getChecksum();
                    wd4.a((Object) checksum, "firmware.checksum");
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = x.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            DebugFirmwareData debugFirmwareData;
            int i;
            Object a = oc4.a();
            int i2 = this.label;
            if (i2 == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 a2 = zh4.a();
                C0129Anon1 anon1 = new C0129Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i2 == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((File) obj) != null) {
                debugFirmwareData = this.this$Anon0.g;
                i = 2;
            } else {
                debugFirmwareData = this.this$Anon0.g;
                i = 0;
            }
            debugFirmwareData.setState(i);
            DebugActivity.b(this.this$Anon0.f).f();
            return cb4.a;
        }
    }

    @DexIgnore
    public DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1(Firmware firmware, DebugActivity debugActivity, DebugFirmwareData debugFirmwareData) {
        this.e = firmware;
        this.f = debugActivity;
        this.g = debugFirmwareData;
    }

    @DexIgnore
    public final void onClick(DialogInterface dialogInterface, int i) {
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        this.g.setState(1);
        DebugActivity.b(this.f).f();
    }
}
