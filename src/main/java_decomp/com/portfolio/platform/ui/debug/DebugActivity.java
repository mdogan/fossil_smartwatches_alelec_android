package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.c0;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ce4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.dr2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.hj2;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kq2;
import com.fossil.blesdk.obfuscated.mc;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mq2;
import com.fossil.blesdk.obfuscated.nq2;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oq2;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.pq2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.qq2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.DebugForceBackgroundRequestData;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.viewmodel.FirmwareDebugViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugActivity extends BaseActivity implements kq2.b {
    @DexIgnore
    public static /* final */ a P; // = new a((rd4) null);
    @DexIgnore
    public fn2 B;
    @DexIgnore
    public dr2 C;
    @DexIgnore
    public FirmwareFileRepository D;
    @DexIgnore
    public GuestApiService E;
    @DexIgnore
    public DianaPresetRepository F;
    @DexIgnore
    public ShakeFeedbackService G;
    @DexIgnore
    public wj2 H;
    @DexIgnore
    public /* final */ kq2 I; // = new kq2();
    @DexIgnore
    public /* final */ ArrayList<qq2> J; // = new ArrayList<>();
    @DexIgnore
    public FirmwareDebugViewModel K;
    @DexIgnore
    public String L; // = "";
    @DexIgnore
    public HeartRateMode M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public /* final */ c O; // = new c(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            context.startActivity(new Intent(context, DebugActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements InputFilter {
        @DexIgnore
        public double e;
        @DexIgnore
        public double f;

        @DexIgnore
        public b(DebugActivity debugActivity, double d, double d2) {
            this.e = d;
            this.f = d2;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0018 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        public final boolean a(double d, double d2, double d3) {
            if (d2 > d) {
                return d3 >= d && d3 <= d2;
            }
            if (d3 >= d2 && d3 <= d) {
                return true;
            }
        }

        @DexIgnore
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            wd4.b(charSequence, "source");
            wd4.b(spanned, "dest");
            try {
                StringBuilder sb = new StringBuilder();
                String obj = spanned.toString();
                if (obj != null) {
                    String substring = obj.substring(0, i3);
                    wd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    sb.append(substring);
                    String obj2 = spanned.toString();
                    int length = spanned.toString().length();
                    if (obj2 != null) {
                        String substring2 = obj2.substring(i4, length);
                        wd4.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        sb.append(substring2);
                        String sb2 = sb.toString();
                        StringBuilder sb3 = new StringBuilder();
                        if (sb2 != null) {
                            String substring3 = sb2.substring(0, i3);
                            wd4.a((Object) substring3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            sb3.append(substring3);
                            sb3.append(charSequence.toString());
                            int length2 = sb2.length();
                            if (sb2 != null) {
                                String substring4 = sb2.substring(i3, length2);
                                wd4.a((Object) substring4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                                sb3.append(substring4);
                                if (a(this.e, this.f, Double.parseDouble(sb3.toString()))) {
                                    return null;
                                }
                                return "";
                            }
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            } catch (Exception e2) {
                e2.printStackTrace();
                return "";
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public c(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            T t;
            T t2;
            T t3;
            T t4;
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            mq2 mq2 = null;
            if (communicateMode == CommunicateMode.SET_HEART_RATE_MODE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.C().a(this.a.z());
                    Iterator<T> it = this.a.u().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t3 = null;
                            break;
                        }
                        t3 = it.next();
                        if (wd4.a((Object) ((qq2) t3).b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    qq2 qq2 = (qq2) t3;
                    if (qq2 != null) {
                        ArrayList<mq2> a2 = qq2.a();
                        if (a2 != null) {
                            Iterator<T> it2 = a2.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    t4 = null;
                                    break;
                                }
                                t4 = it2.next();
                                if (wd4.a((Object) ((mq2) t4).a(), (Object) "SWITCH HEART RATE MODE")) {
                                    break;
                                }
                            }
                            mq2 = (mq2) t4;
                        }
                    }
                    pq2 pq2 = (pq2) mq2;
                    if (pq2 != null) {
                        pq2.b(this.a.z().name());
                    }
                    this.a.s().notifyDataSetChanged();
                } else {
                    DebugActivity debugActivity = this.a;
                    HeartRateMode d = debugActivity.C().d(PortfolioApp.W.c().e());
                    wd4.a((Object) d, "mSharedPreferencesManage\u2026tance.activeDeviceSerial)");
                    debugActivity.a(d);
                }
                this.a.h();
            } else if (communicateMode == CommunicateMode.SET_FRONT_LIGHT_ENABLE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.C().g(this.a.A());
                } else {
                    DebugActivity debugActivity2 = this.a;
                    debugActivity2.d(debugActivity2.C().l(PortfolioApp.W.c().e()));
                    Iterator<T> it3 = this.a.u().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it3.next();
                        if (wd4.a((Object) ((qq2) t).b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    qq2 qq22 = (qq2) t;
                    if (qq22 != null) {
                        ArrayList<mq2> a3 = qq22.a();
                        if (a3 != null) {
                            Iterator<T> it4 = a3.iterator();
                            while (true) {
                                if (!it4.hasNext()) {
                                    t2 = null;
                                    break;
                                }
                                t2 = it4.next();
                                if (wd4.a((Object) ((mq2) t2).a(), (Object) "FRONT LIGHT ENABLE")) {
                                    break;
                                }
                            }
                            mq2 = (mq2) t2;
                        }
                    }
                    oq2 oq2 = (oq2) mq2;
                    if (oq2 != null) {
                        oq2.a(this.a.A());
                    }
                    this.a.s().notifyDataSetChanged();
                }
                this.a.h();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements dc<Firmware> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public d(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public final void a(Firmware firmware) {
            mq2 mq2;
            T t;
            T t2;
            if (firmware != null) {
                Iterator<T> it = this.a.u().iterator();
                while (true) {
                    mq2 = null;
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wd4.a((Object) ((qq2) t).b(), (Object) "OTHER")) {
                        break;
                    }
                }
                qq2 qq2 = (qq2) t;
                if (qq2 != null) {
                    ArrayList<mq2> a2 = qq2.a();
                    if (a2 != null) {
                        Iterator<T> it2 = a2.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it2.next();
                            if (wd4.a((Object) ((mq2) t2).a(), (Object) "CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                                break;
                            }
                        }
                        mq2 = (mq2) t2;
                    }
                }
                if (mq2 != null) {
                    mq2.a("CONSIDER AS LATEST BUNDLE FIRMWARE: " + firmware.getVersionNumber());
                }
                this.a.s().notifyDataSetChanged();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements dc<List<? extends DebugFirmwareData>> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public e(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public final void a(List<DebugFirmwareData> list) {
            T t;
            if (list != null && (!list.isEmpty())) {
                Iterator<T> it = this.a.u().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wd4.a((Object) ((qq2) t).b(), (Object) "FIRMWARE")) {
                        break;
                    }
                }
                qq2 qq2 = (qq2) t;
                if (qq2 != null) {
                    ArrayList<mq2> a2 = qq2.a();
                    if (a2 != null) {
                        a2.clear();
                    }
                }
                for (DebugFirmwareData debugFirmwareData : list) {
                    Firmware firmware = debugFirmwareData.getFirmware();
                    int state = debugFirmwareData.getState();
                    String str = state != 1 ? state != 2 ? "" : "Downloaded" : "Downloading";
                    String versionNumber = firmware.getVersionNumber();
                    wd4.a((Object) versionNumber, "firmware.versionNumber");
                    pq2 pq2 = new pq2("FIRMWARE", versionNumber, str);
                    pq2.a(debugFirmwareData);
                    if (qq2 != null) {
                        ArrayList<mq2> a3 = qq2.a();
                        if (a3 != null) {
                            a3.add(pq2);
                        }
                    }
                }
                this.a.s().notifyDataSetChanged();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity e;
        @DexIgnore
        public /* final */ /* synthetic */ EditText f;
        @DexIgnore
        public /* final */ /* synthetic */ EditText g;
        @DexIgnore
        public /* final */ /* synthetic */ EditText h;
        @DexIgnore
        public /* final */ /* synthetic */ EditText i;
        @DexIgnore
        public /* final */ /* synthetic */ c0 j;

        @DexIgnore
        public f(DebugActivity debugActivity, EditText editText, EditText editText2, EditText editText3, EditText editText4, c0 c0Var) {
            this.e = debugActivity;
            this.f = editText;
            this.g = editText2;
            this.h = editText3;
            this.i = editText4;
            this.j = c0Var;
        }

        @DexIgnore
        public final void onClick(View view) {
            int i2;
            int i3;
            int i4;
            int i5;
            EditText editText = this.f;
            wd4.a((Object) editText, "etDelay");
            String obj = editText.getText().toString();
            EditText editText2 = this.g;
            wd4.a((Object) editText2, "etDuration");
            String obj2 = editText2.getText().toString();
            EditText editText3 = this.h;
            wd4.a((Object) editText3, "etRepeat");
            String obj3 = editText3.getText().toString();
            EditText editText4 = this.i;
            wd4.a((Object) editText4, "etDelayEachTime");
            String obj4 = editText4.getText().toString();
            try {
                i2 = Integer.parseInt(obj);
            } catch (Exception unused) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(obj2);
            } catch (Exception unused2) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(obj3);
            } catch (Exception unused3) {
                i4 = 0;
            }
            try {
                i5 = Integer.parseInt(obj4);
            } catch (Exception unused4) {
                i5 = 0;
            }
            if (i2 > 65535 || i3 > 65535 || i4 > 65535 || i5 > 65535) {
                Toast.makeText(this.e, "Value should be in range [0, 65535]", 0).show();
                return;
            }
            PortfolioApp.W.c().a(i2, i3, i4, i5);
            this.j.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity e;
        @DexIgnore
        public /* final */ /* synthetic */ EditText f;

        @DexIgnore
        public g(DebugActivity debugActivity, EditText editText) {
            this.e = debugActivity;
            this.f = editText;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            String obj = this.f.getText().toString();
            if (obj != null) {
                Integer valueOf = Integer.valueOf(StringsKt__StringsKt.d(obj).toString());
                wd4.a((Object) valueOf, "Integer.valueOf(input.text.toString().trim())");
                int intValue = valueOf.intValue();
                this.e.C().d(intValue);
                for (qq2 qq2 : this.e.u()) {
                    if (wd4.a((Object) qq2.b(), (Object) "SIMULATION")) {
                        for (T next : qq2.a()) {
                            if (wd4.a((Object) ((mq2) next).c(), (Object) "TRIGGER LOW BATTERY EVENT")) {
                                if (next != null) {
                                    be4 be4 = be4.a;
                                    Object[] objArr = {Integer.valueOf(intValue)};
                                    String format = String.format("Battery level: %d", Arrays.copyOf(objArr, objArr.length));
                                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                                    ((pq2) next).b(format);
                                    this.e.s().notifyDataSetChanged();
                                    return;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ c0 e;

        @DexIgnore
        public h(c0 c0Var) {
            this.e = c0Var;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            if (com.fossil.blesdk.obfuscated.wd4.a(java.lang.Integer.valueOf(kotlin.text.StringsKt__StringsKt.d(r6).toString()).intValue(), 100) <= 0) goto L_0x006f;
         */
        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wd4.b(editable, "s");
            boolean z = true;
            if (editable.length() == 0) {
                Button b = this.e.b(-1);
                wd4.a((Object) b, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                b.setEnabled(false);
                return;
            }
            Button b2 = this.e.b(-1);
            wd4.a((Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
            String obj = editable.toString();
            if (obj != null) {
                if (wd4.a(Integer.valueOf(StringsKt__StringsKt.d(obj).toString()).intValue(), 1) >= 0) {
                    String obj2 = editable.toString();
                    if (obj2 == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                z = false;
                b2.setEnabled(z);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ i e; // = new i();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity e;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware f;

        @DexIgnore
        public j(DebugActivity debugActivity, Firmware firmware) {
            this.e = debugActivity;
            this.f = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.e.a(this.f);
            dialogInterface.dismiss();
            this.e.finish();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ k e; // = new k();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity e;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware f;

        @DexIgnore
        public l(DebugActivity debugActivity, Firmware firmware) {
            this.e = debugActivity;
            this.f = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.e.C().a(this.f, this.e.L);
            DebugActivity.b(this.e).a(this.f);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ m e; // = new m();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static final /* synthetic */ FirmwareDebugViewModel b(DebugActivity debugActivity) {
        FirmwareDebugViewModel firmwareDebugViewModel = debugActivity.K;
        if (firmwareDebugViewModel != null) {
            return firmwareDebugViewModel;
        }
        wd4.d("mFirmwareViewModel");
        throw null;
    }

    @DexIgnore
    public final boolean A() {
        return this.N;
    }

    @DexIgnore
    public final ShakeFeedbackService B() {
        ShakeFeedbackService shakeFeedbackService = this.G;
        if (shakeFeedbackService != null) {
            return shakeFeedbackService;
        }
        wd4.d("mShakeFeedbackService");
        throw null;
    }

    @DexIgnore
    public final fn2 C() {
        fn2 fn2 = this.B;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void c(Firmware firmware) {
        c0.a aVar = new c0.a(this);
        aVar.b((CharSequence) "Confirm Latest Firmware");
        aVar.a((CharSequence) "Are you sure you want to use firmware " + firmware.getVersionNumber() + " as the bundle latest firmware?");
        aVar.b("Confirm", new l(this, firmware));
        aVar.a((CharSequence) "Cancel", (DialogInterface.OnClickListener) m.e);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public final void d(boolean z) {
        this.N = z;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_debug);
        PortfolioApp.W.c().g().a(this);
        String e2 = PortfolioApp.W.c().e();
        fn2 fn2 = this.B;
        if (fn2 != null) {
            HeartRateMode d2 = fn2.d(e2);
            wd4.a((Object) d2, "mSharedPreferencesManage\u2026eMode(activeDeviceSerial)");
            this.M = d2;
            fn2 fn22 = this.B;
            if (fn22 != null) {
                this.N = fn22.l(e2);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new mq2("VIEW LOG", "VIEW LOG"));
                arrayList.add(new mq2("SEND LOG", "SEND LOG"));
                arrayList.add(new mq2("RESET UAPP LOG FILES", "RESET UAPP LOG FILES"));
                this.J.add(new qq2("LOG", "LOG", arrayList, false, 8, (rd4) null));
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(new mq2("CLEAR DATA", "CLEAR DATA"));
                arrayList2.add(new mq2("GENERATE PRESET DATA", "GENERATE PRESET DATA"));
                this.J.add(new qq2("DATA", "DATA", arrayList2, false, 8, (rd4) null));
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new mq2("BLUETOOTH SETTING", "BLUETOOTH SETTING"));
                this.J.add(new qq2("ANDROID SETTINGS", "ANDROID SETTINGS", arrayList3, false, 8, (rd4) null));
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(new mq2("START MINDFULNESS PRACTICE", "START MINDFULNESS PRACTICE"));
                arrayList4.add(new mq2("WATCH APP MUSIC CONTROL", "WATCH APP MUSIC CONTROL"));
                arrayList4.add(new mq2("SIMULATE DISCONNECTION", "SIMULATE DISCONNECTION"));
                be4 be4 = be4.a;
                Object[] objArr = new Object[1];
                fn2 fn23 = this.B;
                if (fn23 != null) {
                    objArr[0] = Integer.valueOf(fn23.w());
                    String format = String.format("Battery level: %d", Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    arrayList4.add(new pq2("TRIGGER LOW BATTERY EVENT", "TRIGGER LOW BATTERY EVENT", format));
                    this.J.add(new qq2("SIMULATION", "SIMULATION", arrayList4, false, 8, (rd4) null));
                    ArrayList arrayList5 = new ArrayList();
                    arrayList5.add(new mq2("FIRMWARE_LOADING", "FIRMWARE_LOADING"));
                    this.J.add(new qq2("FIRMWARE", "FIRMWARE", arrayList5, false, 8, (rd4) null));
                    ArrayList arrayList6 = new ArrayList();
                    arrayList6.add(new mq2("SYNC", "SYNC"));
                    List<DebugForceBackgroundRequestData> d3 = ob4.d(new DebugForceBackgroundRequestData("Notification Filter Background", ForceBackgroundRequest.BackgroundRequestType.SET_NOTIFICATION_FILTER), new DebugForceBackgroundRequestData("Alarms Background", ForceBackgroundRequest.BackgroundRequestType.SET_MULTI_ALARM), new DebugForceBackgroundRequestData("Device Configuration Background", ForceBackgroundRequest.BackgroundRequestType.SET_CONFIG_FILE));
                    ArrayList arrayList7 = new ArrayList(pb4.a(d3, 10));
                    for (DebugForceBackgroundRequestData visibleName : d3) {
                        arrayList7.add(visibleName.getVisibleName());
                    }
                    nq2 nq2 = new nq2("FORCE_BACKGROUND_REQUEST", "FORCE_BACKGROUND_REQUEST", wb4.d(arrayList7), "Send");
                    nq2.a(d3);
                    cb4 cb4 = cb4.a;
                    arrayList6.add(nq2);
                    arrayList6.add(new mq2("WEATHER_WATCH_APP_TAP", "WEATHER_WATCH_APP_TAP"));
                    arrayList6.add(new mq2("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT", "RESET DEVICE SETTING IN FIRMWARE TO DEFAULT"));
                    arrayList6.add(new mq2("RESET_DELAY_OTA_TIMESTAMP", "RESET_DELAY_OTA_TIMESTAMP"));
                    fn2 fn24 = this.B;
                    if (fn24 != null) {
                        arrayList6.add(new oq2("SKIP OTA", "SKIP OTA", fn24.T()));
                        fn2 fn25 = this.B;
                        if (fn25 != null) {
                            arrayList6.add(new oq2("SHOW ALL DEVICES", "SHOW ALL DEVICES", fn25.S()));
                            fn2 fn26 = this.B;
                            if (fn26 != null) {
                                arrayList6.add(new oq2("DISABLE HW_LOG SYNC", "DISABLE HW_LOG SYNC", !fn26.I()));
                                fn2 fn27 = this.B;
                                if (fn27 != null) {
                                    arrayList6.add(new oq2("DISABLE AUTO SYNC", "DISABLE AUTO SYNC", !fn27.B()));
                                    fn2 fn28 = this.B;
                                    if (fn28 != null) {
                                        arrayList6.add(new oq2("SHOW DISPLAY DEVICE INFO", "SHOW DISPLAY DEVICE INFO", fn28.J()));
                                        fn2 fn29 = this.B;
                                        if (fn29 != null) {
                                            arrayList6.add(new oq2("CONSIDER AS LATEST BUNDLE FIRMWARE", "CONSIDER AS LATEST BUNDLE FIRMWARE:No Firmware", fn29.C()));
                                            fn2 fn210 = this.B;
                                            if (fn210 != null) {
                                                arrayList6.add(new oq2("APPLY NEW NOTIFICATION FILTER", "APPLY NEW NOTIFICATION FILTER", fn210.M()));
                                                if (FossilDeviceSerialPatternUtil.isDianaDevice(e2)) {
                                                    arrayList6.add(new oq2("FRONT LIGHT ENABLE", "FRONT LIGHT ENABLE", this.N));
                                                    HeartRateMode heartRateMode = this.M;
                                                    if (heartRateMode != null) {
                                                        arrayList6.add(new pq2("SWITCH HEART RATE MODE", "SWITCH HEART RATE MODE", heartRateMode.name()));
                                                    } else {
                                                        wd4.d("mHeartRateMode");
                                                        throw null;
                                                    }
                                                }
                                                this.J.add(new qq2("OTHER", "OTHER", arrayList6, true));
                                                kq2 kq2 = this.I;
                                                kq2.a(this.J);
                                                kq2.a((kq2.b) this);
                                                cb4 cb42 = cb4.a;
                                                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_debug_parent);
                                                recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                                                recyclerView.setAdapter(this.I);
                                                RecyclerView.g adapter = recyclerView.getAdapter();
                                                if (adapter != null) {
                                                    adapter.notifyDataSetChanged();
                                                    cb4 cb43 = cb4.a;
                                                    jc a2 = mc.a((FragmentActivity) this).a(FirmwareDebugViewModel.class);
                                                    wd4.a((Object) a2, "ViewModelProviders.of(th\u2026bugViewModel::class.java)");
                                                    this.K = (FirmwareDebugViewModel) a2;
                                                    e eVar = new e(this);
                                                    FirmwareDebugViewModel firmwareDebugViewModel = this.K;
                                                    if (firmwareDebugViewModel != null) {
                                                        firmwareDebugViewModel.c().a(this, eVar);
                                                        d dVar = new d(this);
                                                        FirmwareDebugViewModel firmwareDebugViewModel2 = this.K;
                                                        if (firmwareDebugViewModel2 != null) {
                                                            firmwareDebugViewModel2.d().a(this, dVar);
                                                            if (!TextUtils.isEmpty(e2)) {
                                                                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DebugActivity$onCreate$Anon4(this, e2, (kc4) null), 3, (Object) null);
                                                                return;
                                                            }
                                                            return;
                                                        }
                                                        wd4.d("mFirmwareViewModel");
                                                        throw null;
                                                    }
                                                    wd4.d("mFirmwareViewModel");
                                                    throw null;
                                                }
                                                wd4.a();
                                                throw null;
                                            }
                                            wd4.d("mSharedPreferencesManager");
                                            throw null;
                                        }
                                        wd4.d("mSharedPreferencesManager");
                                        throw null;
                                    }
                                    wd4.d("mSharedPreferencesManager");
                                    throw null;
                                }
                                wd4.d("mSharedPreferencesManager");
                                throw null;
                            }
                            wd4.d("mSharedPreferencesManager");
                            throw null;
                        }
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                    wd4.d("mSharedPreferencesManager");
                    throw null;
                }
                wd4.d("mSharedPreferencesManager");
                throw null;
            }
            wd4.d("mSharedPreferencesManager");
            throw null;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.O, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.O, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
        BleCommandResultManager.d.a(CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public final kq2 s() {
        return this.I;
    }

    @DexIgnore
    public final LinkedList<Pair<String, Long>> t() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Pair<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 31; i2++) {
            wd4.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wd4.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(5, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListDay - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final ArrayList<qq2> u() {
        return this.J;
    }

    @DexIgnore
    public final LinkedList<Pair<String, Long>> v() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Pair<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 12; i2++) {
            wd4.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wd4.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(2, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListMonth - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final LinkedList<Pair<String, Long>> w() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Pair<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 52; i2++) {
            wd4.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wd4.a((Object) time, "calendar.time");
            linkedList.add(b(time));
            instance.add(5, -7);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListWeek - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final FirmwareFileRepository x() {
        FirmwareFileRepository firmwareFileRepository = this.D;
        if (firmwareFileRepository != null) {
            return firmwareFileRepository;
        }
        wd4.d("mFirmwareFileRepository");
        throw null;
    }

    @DexIgnore
    public final GuestApiService y() {
        GuestApiService guestApiService = this.E;
        if (guestApiService != null) {
            return guestApiService;
        }
        wd4.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final HeartRateMode z() {
        HeartRateMode heartRateMode = this.M;
        if (heartRateMode != null) {
            return heartRateMode;
        }
        wd4.d("mHeartRateMode");
        throw null;
    }

    @DexIgnore
    public final Pair<String, Long> b(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        wd4.a((Object) instance, "calendar");
        instance.setTime(date);
        instance.set(7, instance.getFirstDayOfWeek());
        int i2 = instance.get(5);
        instance.set(7, instance.getFirstDayOfWeek() + 6);
        int i3 = instance.get(5);
        StringBuilder sb = new StringBuilder();
        sb.append(i2);
        sb.append('-');
        sb.append(i3);
        return new Pair<>(sb.toString(), Long.valueOf(date.getTime()));
    }

    @DexIgnore
    public final void a(HeartRateMode heartRateMode) {
        wd4.b(heartRateMode, "<set-?>");
        this.M = heartRateMode;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(String str, kc4<? super List<DebugFirmwareData>> kc4) {
        DebugActivity$loadFirmware$Anon1 debugActivity$loadFirmware$Anon1;
        int i2;
        ArrayList arrayList;
        Object obj;
        DebugActivity debugActivity;
        ro2 ro2;
        if (kc4 instanceof DebugActivity$loadFirmware$Anon1) {
            debugActivity$loadFirmware$Anon1 = (DebugActivity$loadFirmware$Anon1) kc4;
            int i3 = debugActivity$loadFirmware$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                debugActivity$loadFirmware$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj2 = debugActivity$loadFirmware$Anon1.result;
                Object a2 = oc4.a();
                i2 = debugActivity$loadFirmware$Anon1.label;
                String str2 = null;
                if (i2 != 0) {
                    za4.a(obj2);
                    if (str.length() > 0) {
                        arrayList = new ArrayList();
                        DebugActivity$loadFirmware$repoResponse$Anon1 debugActivity$loadFirmware$repoResponse$Anon1 = new DebugActivity$loadFirmware$repoResponse$Anon1(this, str, (kc4) null);
                        debugActivity$loadFirmware$Anon1.L$Anon0 = this;
                        debugActivity$loadFirmware$Anon1.L$Anon1 = str;
                        debugActivity$loadFirmware$Anon1.L$Anon2 = arrayList;
                        debugActivity$loadFirmware$Anon1.label = 1;
                        obj = ResponseKt.a(debugActivity$loadFirmware$repoResponse$Anon1, debugActivity$loadFirmware$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        debugActivity = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(f(), ".loadFirmware(), device model is empty. Return empty list");
                        return new ArrayList();
                    }
                } else if (i2 == 1) {
                    String str3 = (String) debugActivity$loadFirmware$Anon1.L$Anon1;
                    debugActivity = (DebugActivity) debugActivity$loadFirmware$Anon1.L$Anon0;
                    za4.a(obj2);
                    Object obj3 = obj2;
                    arrayList = (ArrayList) debugActivity$loadFirmware$Anon1.L$Anon2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f2 = debugActivity.f();
                local.d(f2, ".loadFirmware(), response" + ro2);
                if (!(ro2 instanceof so2)) {
                    ApiResponse apiResponse = (ApiResponse) ((so2) ro2).a();
                    List<Firmware> list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list != null) {
                        for (Firmware firmware : list) {
                            FirmwareFileRepository firmwareFileRepository = debugActivity.D;
                            if (firmwareFileRepository != null) {
                                String versionNumber = firmware.getVersionNumber();
                                wd4.a((Object) versionNumber, "it.versionNumber");
                                String checksum = firmware.getChecksum();
                                wd4.a((Object) checksum, "it.checksum");
                                arrayList.add(new DebugFirmwareData(firmware, firmwareFileRepository.isDownloaded(versionNumber, checksum) ? 2 : 0));
                            } else {
                                wd4.d("mFirmwareFileRepository");
                                throw null;
                            }
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String f3 = debugActivity.f();
                    StringBuilder sb = new StringBuilder();
                    sb.append(".loadFirmware(), fail with code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(", message=");
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local2.d(f3, sb.toString());
                }
                return arrayList;
            }
        }
        debugActivity$loadFirmware$Anon1 = new DebugActivity$loadFirmware$Anon1(this, kc4);
        Object obj22 = debugActivity$loadFirmware$Anon1.result;
        Object a22 = oc4.a();
        i2 = debugActivity$loadFirmware$Anon1.label;
        String str22 = null;
        if (i2 != 0) {
        }
        ro2 = (ro2) obj;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String f22 = debugActivity.f();
        local3.d(f22, ".loadFirmware(), response" + ro2);
        if (!(ro2 instanceof so2)) {
        }
        return arrayList;
    }

    @DexIgnore
    public void b(String str, int i2, int i3, Object obj, Bundle bundle) {
        wd4.b(str, "tagName");
        if (str.hashCode() == 227289531 && str.equals("FIRMWARE")) {
            DebugFirmwareData debugFirmwareData = (DebugFirmwareData) obj;
            if (debugFirmwareData != null) {
                FirmwareFileRepository firmwareFileRepository = this.D;
                if (firmwareFileRepository != null) {
                    String versionNumber = debugFirmwareData.getFirmware().getVersionNumber();
                    wd4.a((Object) versionNumber, "it.firmware.versionNumber");
                    String checksum = debugFirmwareData.getFirmware().getChecksum();
                    wd4.a((Object) checksum, "it.firmware.checksum");
                    if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                        c(debugFirmwareData.getFirmware());
                    } else {
                        FLogger.INSTANCE.getLocal().d(f(), "Firmware hasn't been downloaded. Could not be set as latest firmware.");
                    }
                } else {
                    wd4.d("mFirmwareFileRepository");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void b(Firmware firmware) {
        c0.a aVar = new c0.a(this);
        aVar.b((CharSequence) "Confirm OTA");
        aVar.a((CharSequence) "Are you sure you want OTA to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new j(this, firmware));
        aVar.a((CharSequence) "Cancel", (DialogInterface.OnClickListener) k.e);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public void a(String str, int i2, int i3, Object obj, Bundle bundle) {
        String str2 = str;
        Bundle bundle2 = bundle;
        wd4.b(str2, "tagName");
        switch (str.hashCode()) {
            case -2017027426:
                if (str2.equals("BLUETOOTH SETTING")) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.BLUETOOTH_SETTINGS");
                    startActivity(intent);
                    cb4 cb4 = cb4.a;
                    return;
                }
                return;
            case -1935069302:
                if (str2.equals("WEATHER_WATCH_APP_TAP")) {
                    PortfolioApp.a aVar = PortfolioApp.W;
                    aVar.a((Object) new hj2(aVar.c().e(), DeviceEventId.WEATHER_WATCH_APP.ordinal(), new Bundle()));
                    return;
                }
                return;
            case -1822588397:
                if (str2.equals("TRIGGER LOW BATTERY EVENT")) {
                    c0.a aVar2 = new c0.a(this);
                    EditText editText = new EditText(this);
                    editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    editText.setInputType(2);
                    aVar2.b((View) editText);
                    aVar2.b((CharSequence) "Custom dialog");
                    aVar2.a((CharSequence) "Enter battery level between 1-100");
                    aVar2.b("Done", new g(this, editText));
                    c0 a2 = aVar2.a();
                    a2.show();
                    Button b2 = a2.b(-1);
                    wd4.a((Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                    b2.setEnabled(false);
                    editText.addTextChangedListener(new h(a2));
                    return;
                }
                return;
            case -1367489317:
                if (str2.equals("SKIP OTA")) {
                    fn2 fn2 = this.B;
                    if (fn2 != null) {
                        Boolean valueOf = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf != null) {
                            fn2.y(valueOf.booleanValue());
                            return;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1337201641:
                if (str2.equals("SHOW DISPLAY DEVICE INFO")) {
                    fn2 fn22 = this.B;
                    if (fn22 != null) {
                        Boolean valueOf2 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf2 != null) {
                            fn22.p(valueOf2.booleanValue());
                            fn2 fn23 = this.B;
                            if (fn23 == null) {
                                wd4.d("mSharedPreferencesManager");
                                throw null;
                            } else if (fn23.J()) {
                                a();
                                return;
                            } else {
                                o();
                                return;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1276486892:
                if (str2.equals("DISABLE AUTO SYNC")) {
                    fn2 fn24 = this.B;
                    if (fn24 != null) {
                        Boolean valueOf3 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf3 != null) {
                            fn24.b(!valueOf3.booleanValue());
                            return;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -907588345:
                if (str2.equals("CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                    fn2 fn25 = this.B;
                    if (fn25 != null) {
                        Boolean valueOf4 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf4 != null) {
                            fn25.c(valueOf4.booleanValue());
                            return;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -833549689:
                if (str2.equals("RESET_DELAY_OTA_TIMESTAMP")) {
                    fn2 fn26 = this.B;
                    if (fn26 != null) {
                        fn26.a(PortfolioApp.W.c().e(), -1);
                        return;
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -558521180:
                if (str2.equals("FRONT LIGHT ENABLE") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_IS_CHECKED")) {
                    boolean z = bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false);
                    p();
                    if (PortfolioApp.W.c().b(PortfolioApp.W.c().e(), z) == ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD()) {
                        h();
                        return;
                    }
                    return;
                }
                return;
            case -276828739:
                if (str2.equals("CLEAR DATA")) {
                    xs3.f fVar = new xs3.f(R.layout.dialog_confirm);
                    fVar.a((int) R.id.ftv_confirm_title, "ARE YOU SURE?");
                    fVar.a((int) R.id.ftv_confirm_desc, "** This will NOT happen on production **");
                    fVar.a((int) R.id.fb_ok, "OK");
                    fVar.a((int) R.id.fb_ok);
                    xs3 a3 = fVar.a("CONFIRM_CLEAR_DATA");
                    wd4.a((Object) a3, "AlertDialogFragment.Buil\u2026Utils.CONFIRM_CLEAR_DATA)");
                    a3.setCancelable(true);
                    a3.setStyle(0, R.style.DialogNotFullScreen);
                    a3.show(getSupportFragmentManager(), "CONFIRM_CLEAR_DATA");
                    return;
                }
                return;
            case -75588064:
                if (str2.equals("GENERATE PRESET DATA")) {
                    t();
                    w();
                    v();
                    return;
                }
                return;
            case -62411653:
                if (str2.equals("SHOW ALL DEVICES")) {
                    fn2 fn27 = this.B;
                    if (fn27 != null) {
                        Boolean valueOf5 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf5 != null) {
                            fn27.x(valueOf5.booleanValue());
                            DeviceHelper.o.e().a();
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.d("mSharedPreferencesManager");
                    throw null;
                }
                return;
            case 2560667:
                if (str2.equals("SYNC")) {
                    PortfolioApp c2 = PortfolioApp.W.c();
                    wj2 wj2 = this.H;
                    if (wj2 != null) {
                        c2.a(wj2, false, 12);
                        return;
                    } else {
                        wd4.d("mDeviceSettingFactory");
                        throw null;
                    }
                } else {
                    return;
                }
            case 227289531:
                if (str2.equals("FIRMWARE")) {
                    DebugFirmwareData debugFirmwareData = (DebugFirmwareData) obj;
                    if (debugFirmwareData != null) {
                        if (debugFirmwareData.getState() == 2) {
                            b(debugFirmwareData.getFirmware());
                        } else if (debugFirmwareData.getState() == 0) {
                            a(debugFirmwareData);
                        }
                        cb4 cb42 = cb4.a;
                        return;
                    }
                    return;
                }
                return;
            case 334768719:
                if (str2.equals("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT")) {
                    PortfolioApp.W.c().N();
                    return;
                }
                return;
            case 437897810:
                if (str2.equals("FORCE_BACKGROUND_REQUEST") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_SPINNER_SELECTED_POS")) {
                    int i4 = bundle2.getInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS");
                    List c3 = ce4.c(obj);
                    Object obj2 = c3 != null ? c3.get(i4) : null;
                    if (!(obj2 instanceof DebugForceBackgroundRequestData)) {
                        obj2 = null;
                    }
                    DebugForceBackgroundRequestData debugForceBackgroundRequestData = (DebugForceBackgroundRequestData) obj2;
                    if (debugForceBackgroundRequestData != null) {
                        Long.valueOf(PortfolioApp.W.c().a(PortfolioApp.W.c().e(), (CustomRequest) new ForceBackgroundRequest(debugForceBackgroundRequestData.getBackgroundRequestType())));
                        return;
                    }
                    return;
                }
                return;
            case 596314607:
                if (str2.equals("DISABLE HW_LOG SYNC")) {
                    fn2 fn28 = this.B;
                    if (fn28 != null) {
                        Boolean valueOf6 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf6 != null) {
                            fn28.i(!valueOf6.booleanValue());
                            return;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 967694544:
                if (str2.equals("SIMULATE DISCONNECTION")) {
                    c0.a aVar3 = new c0.a(this);
                    Object systemService = getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(R.layout.simulate_disconnection_input, (ViewGroup) null);
                        aVar3.b(inflate);
                        c0 a4 = aVar3.a();
                        a4.show();
                        EditText editText2 = (EditText) inflate.findViewById(R.id.et_delay);
                        EditText editText3 = (EditText) inflate.findViewById(R.id.et_duration);
                        EditText editText4 = (EditText) inflate.findViewById(R.id.et_repeat);
                        EditText editText5 = (EditText) inflate.findViewById(R.id.et_delay_each_time);
                        wd4.a((Object) editText2, "etDelay");
                        double d2 = (double) 0;
                        double d3 = d2;
                        double d4 = d2;
                        double d5 = (double) 65535;
                        editText2.setFilters(new InputFilter[]{new b(this, d3, d5)});
                        wd4.a((Object) editText3, "etDuration");
                        double d6 = d4;
                        editText3.setFilters(new InputFilter[]{new b(this, d6, d5)});
                        wd4.a((Object) editText4, "etRepeat");
                        editText4.setFilters(new InputFilter[]{new b(this, d6, d5)});
                        wd4.a((Object) editText5, "etDelayEachTime");
                        editText5.setFilters(new InputFilter[]{new b(this, d6, (double) 4294967)});
                        ((FlexibleButton) inflate.findViewById(R.id.btn_done)).setOnClickListener(new f(this, editText2, editText3, editText4, editText5, a4));
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                }
                return;
            case 999402770:
                if (str2.equals("WATCH APP MUSIC CONTROL")) {
                    finish();
                    return;
                }
                return;
            case 1053818587:
                if (str2.equals("APPLY NEW NOTIFICATION FILTER")) {
                    fn2 fn29 = this.B;
                    if (fn29 != null) {
                        Boolean valueOf7 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf7 != null) {
                            fn29.s(valueOf7.booleanValue());
                            return;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1484036056:
                if (str2.equals("RESET UAPP LOG FILES")) {
                    MicroAppEventLogger.resetLogFiles();
                    return;
                }
                return;
            case 1642625733:
                if (str2.equals("GENERATE HEART RATE DATA")) {
                    finish();
                    return;
                }
                return;
            case 1977879337:
                if (str2.equals("VIEW LOG")) {
                    LogcatActivity.a((Context) this);
                    return;
                }
                return;
            case 2029483660:
                if (str2.equals("SEND LOG")) {
                    ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DebugActivity$onItemClicked$Anon1(this, (kc4) null), 3, (Object) null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final Pair<String, Long> a(Date date) {
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendar");
        instance.setTime(date);
        long timeInMillis = instance.getTimeInMillis();
        switch (instance.get(7)) {
            case 1:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S), Long.valueOf(timeInMillis));
            case 2:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__M), Long.valueOf(timeInMillis));
            case 3:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T), Long.valueOf(timeInMillis));
            case 4:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__W), Long.valueOf(timeInMillis));
            case 5:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1), Long.valueOf(timeInMillis));
            case 6:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__F), Long.valueOf(timeInMillis));
            case 7:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1), Long.valueOf(timeInMillis));
            default:
                return new Pair<>(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S), Long.valueOf(timeInMillis));
        }
    }

    @DexIgnore
    public final void a(DebugFirmwareData debugFirmwareData) {
        c0.a aVar = new c0.a(this);
        Firmware firmware = debugFirmwareData.getFirmware();
        aVar.b((CharSequence) "Confirm Download");
        aVar.a((CharSequence) "Are you sure you want download to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1(firmware, this, debugFirmwareData));
        aVar.a((CharSequence) "Cancel", (DialogInterface.OnClickListener) i.e);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        String e2 = PortfolioApp.W.c().e();
        FirmwareData createFirmwareData = FirmwareFactory.getInstance().createFirmwareData(firmware.getVersionNumber(), firmware.getDeviceModel(), firmware.getChecksum());
        wd4.a((Object) createFirmwareData, "FirmwareFactory.getInsta\u2026Model, firmware.checksum)");
        dr2.b bVar = new dr2.b(e2, createFirmwareData);
        dr2 dr2 = this.C;
        if (dr2 != null) {
            dr2.a(bVar, (CoroutineUseCase.e) null);
        } else {
            wd4.d("mOTAToSpecificFirmwareUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wd4.b(str, "tag");
        if (wd4.a((Object) str, (Object) "CONFIRM_CLEAR_DATA") && i2 == R.id.fb_ok) {
            DebugClearDataWarningActivity.a(this);
        } else if (wd4.a((Object) str, (Object) "SWITCH HEART RATE MODE") && i2 == R.id.fb_ok && intent != null && intent.hasExtra("EXTRA_RADIO_GROUPS_RESULTS")) {
            HashMap hashMap = (HashMap) intent.getSerializableExtra("EXTRA_RADIO_GROUPS_RESULTS");
        }
    }
}
