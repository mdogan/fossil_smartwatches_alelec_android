package com.portfolio.platform.ui.debug;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$Anon4", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
public final class DebugActivity$onCreate$Anon4 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onCreate$Anon4(DebugActivity debugActivity, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = debugActivity;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DebugActivity$onCreate$Anon4 debugActivity$onCreate$Anon4 = new DebugActivity$onCreate$Anon4(this.this$Anon0, this.$activeDeviceSerial, kc4);
        debugActivity$onCreate$Anon4.p$ = (lh4) obj;
        return debugActivity$onCreate$Anon4;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DebugActivity$onCreate$Anon4) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            Device deviceBySerial = this.this$Anon0.b().getDeviceBySerial(this.$activeDeviceSerial);
            if (deviceBySerial != null) {
                DebugActivity debugActivity = this.this$Anon0;
                String sku = deviceBySerial.getSku();
                if (sku == null) {
                    sku = "";
                }
                debugActivity.L = sku;
                Firmware a = this.this$Anon0.C().a(this.this$Anon0.L);
                if (a != null) {
                    DebugActivity.b(this.this$Anon0).a(a);
                }
                if (!DebugActivity.b(this.this$Anon0).e()) {
                    DebugActivity.b(this.this$Anon0).a((jd4<? super kc4<? super List<DebugFirmwareData>>, ? extends Object>) new DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1((kc4) null, this));
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
