package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.widget.Toast;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.uh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.service.ShakeFeedbackService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$Anon1", f = "DebugActivity.kt", l = {294, 295, 296}, m = "invokeSuspend")
public final class DebugActivity$onItemClicked$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$Anon1$Anon1", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity$onItemClicked$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DebugActivity$onItemClicked$Anon1 debugActivity$onItemClicked$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = debugActivity$onItemClicked$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                Toast.makeText(this.this$Anon0.this$Anon0, "Log will be sent after 1 second", 1).show();
                this.this$Anon0.this$Anon0.finish();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onItemClicked$Anon1(DebugActivity debugActivity, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = debugActivity;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DebugActivity$onItemClicked$Anon1 debugActivity$onItemClicked$Anon1 = new DebugActivity$onItemClicked$Anon1(this.this$Anon0, kc4);
        debugActivity$onItemClicked$Anon1.p$ = (lh4) obj;
        return debugActivity$onItemClicked$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DebugActivity$onItemClicked$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        bj4 c;
        Anon1 anon1;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            ShakeFeedbackService B = this.this$Anon0.B();
            DebugActivity debugActivity = this.this$Anon0;
            this.L$Anon0 = lh42;
            this.label = 1;
            if (B.a((Context) debugActivity, (kc4<? super cb4>) this) == a) {
                return a;
            }
            lh4 = lh42;
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            c = zh4.c();
            anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 3;
            if (kg4.a(c, anon1, this) == a) {
                return a;
            }
            return cb4.a;
        } else if (i == 3) {
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.L$Anon0 = lh4;
        this.label = 2;
        if (uh4.a(1000, this) == a) {
            return a;
        }
        c = zh4.c();
        anon1 = new Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.label = 3;
        if (kg4.a(c, anon1, this) == a) {
        }
        return cb4.a;
    }
}
