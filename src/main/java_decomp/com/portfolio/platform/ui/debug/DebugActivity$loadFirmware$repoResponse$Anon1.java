package com.portfolio.platform.ui.debug;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.debug.DebugActivity$loadFirmware$repoResponse$Anon1", f = "DebugActivity.kt", l = {263}, m = "invokeSuspend")
public final class DebugActivity$loadFirmware$repoResponse$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<Firmware>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $model;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$loadFirmware$repoResponse$Anon1(DebugActivity debugActivity, String str, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = debugActivity;
        this.$model = str;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new DebugActivity$loadFirmware$repoResponse$Anon1(this.this$Anon0, this.$model, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DebugActivity$loadFirmware$repoResponse$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            GuestApiService y = this.this$Anon0.y();
            String h = PortfolioApp.W.c().h();
            String str = this.$model;
            this.label = 1;
            obj = y.getFirmwares(h, str, "android", this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
