package com.portfolio.platform.ui.debug;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements jd4<kc4<? super List<? extends DebugFirmwareData>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity$onCreate$Anon4 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1(kc4 kc4, DebugActivity$onCreate$Anon4 debugActivity$onCreate$Anon4) {
        super(1, kc4);
        this.this$Anon0 = debugActivity$onCreate$Anon4;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1(kc4, this.this$Anon0);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            DebugActivity debugActivity = this.this$Anon0.this$Anon0;
            String a2 = debugActivity.L;
            this.label = 1;
            obj = debugActivity.a(a2, (kc4<? super List<DebugFirmwareData>>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
