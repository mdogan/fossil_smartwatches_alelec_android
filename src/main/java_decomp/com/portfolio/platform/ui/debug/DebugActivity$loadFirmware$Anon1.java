package com.portfolio.platform.ui.debug;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import java.util.List;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ui.debug.DebugActivity", f = "DebugActivity.kt", l = {263}, m = "loadFirmware")
public final class DebugActivity$loadFirmware$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$loadFirmware$Anon1(DebugActivity debugActivity, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = debugActivity;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((String) null, (kc4<? super List<DebugFirmwareData>>) this);
    }
}
