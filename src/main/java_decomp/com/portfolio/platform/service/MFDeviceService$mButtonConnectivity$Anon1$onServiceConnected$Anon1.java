package com.portfolio.platform.service;

import android.os.IBinder;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ IBinder $service;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(IBinder iBinder, kc4 kc4) {
        super(2, kc4);
        this.$service = iBinder;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 = new MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(this.$service, kc4);
        mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1.p$ = (lh4) obj;
        return mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            PortfolioApp.a aVar = PortfolioApp.W;
            IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
            wd4.a((Object) asInterface, "IButtonConnectivity.Stub.asInterface(service)");
            aVar.b(asInterface);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
