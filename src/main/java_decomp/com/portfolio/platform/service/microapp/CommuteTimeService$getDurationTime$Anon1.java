package com.portfolio.platform.service.microapp;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mr2;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.maps.model.TravelMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$Anon1", f = "CommuteTimeService.kt", l = {280}, m = "invokeSuspend")
public final class CommuteTimeService$getDurationTime$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Location $location;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeService$getDurationTime$Anon1(CommuteTimeService commuteTimeService, Location location, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeService;
        this.$location = location;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeService$getDurationTime$Anon1 commuteTimeService$getDurationTime$Anon1 = new CommuteTimeService$getDurationTime$Anon1(this.this$Anon0, this.$location, kc4);
        commuteTimeService$getDurationTime$Anon1.p$ = (lh4) obj;
        return commuteTimeService$getDurationTime$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeService$getDurationTime$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.m != null) {
                CommuteTimeSetting a2 = this.this$Anon0.m;
                if (a2 != null) {
                    if (!(a2.getAddress().length() == 0)) {
                        mr2 h = this.this$Anon0.h();
                        if (h != null) {
                            CommuteTimeSetting a3 = this.this$Anon0.m;
                            if (a3 != null) {
                                String address = a3.getAddress();
                                TravelMode travelMode = TravelMode.DRIVING;
                                CommuteTimeSetting a4 = this.this$Anon0.m;
                                if (a4 != null) {
                                    boolean avoidTolls = a4.getAvoidTolls();
                                    double latitude = this.$location.getLatitude();
                                    double longitude = this.$location.getLongitude();
                                    this.L$Anon0 = lh4;
                                    this.label = 1;
                                    obj = h.a(address, travelMode, avoidTolls, latitude, longitude, this);
                                    if (obj == a) {
                                        return a;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            return cb4.a;
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        long longValue = ((Number) obj).longValue();
        if (longValue != -1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = CommuteTimeService.A.a();
            local.d(a5, "getDurationTime duration " + longValue);
            this.this$Anon0.l = longValue;
            if (this.this$Anon0.l < ((long) 60)) {
                this.this$Anon0.l = 60;
            }
            this.this$Anon0.k();
        } else {
            this.this$Anon0.a();
        }
        return cb4.a;
    }
}
