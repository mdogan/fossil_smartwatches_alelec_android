package com.portfolio.platform.service;

import android.graphics.Bitmap;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.ShakeFeedbackService$sendLog$Anon2", f = "ShakeFeedbackService.kt", l = {82}, m = "invokeSuspend")
public final class ShakeFeedbackService$sendLog$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Bitmap $bitmap;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShakeFeedbackService$sendLog$Anon2(ShakeFeedbackService shakeFeedbackService, Bitmap bitmap, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = shakeFeedbackService;
        this.$bitmap = bitmap;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ShakeFeedbackService$sendLog$Anon2 shakeFeedbackService$sendLog$Anon2 = new ShakeFeedbackService$sendLog$Anon2(this.this$Anon0, this.$bitmap, kc4);
        shakeFeedbackService$sendLog$Anon2.p$ = (lh4) obj;
        return shakeFeedbackService$sendLog$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ShakeFeedbackService$sendLog$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ShakeFeedbackService shakeFeedbackService = this.this$Anon0;
            Bitmap bitmap = this.$bitmap;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (shakeFeedbackService.a(bitmap, (kc4<? super cb4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
