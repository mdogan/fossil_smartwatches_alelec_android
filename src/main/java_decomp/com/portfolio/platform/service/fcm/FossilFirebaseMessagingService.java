package com.portfolio.platform.service.fcm;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.bk3;
import com.fossil.blesdk.obfuscated.kz1;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public bk3 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void a(kz1 kz1) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("From: ");
        if (kz1 != null) {
            String I = kz1.I();
            if (I != null) {
                sb.append(I);
                local.d("FossilFirebaseMessagingService", sb.toString());
                if (kz1.H() != null) {
                    Map<String, String> H = kz1.H();
                    wd4.a((Object) H, "remoteMessage.data");
                    if (!H.isEmpty()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("FossilFirebaseMessagingService", "Message data payload: " + kz1.H());
                    }
                }
                if (kz1.J() != null) {
                    kz1.a J = kz1.J();
                    if (J != null) {
                        wd4.a((Object) J, "remoteMessage.notification!!");
                        if (!TextUtils.isEmpty(J.b())) {
                            kz1.a J2 = kz1.J();
                            if (J2 != null) {
                                wd4.a((Object) J2, "remoteMessage.notification!!");
                                str = J2.b();
                                if (str == null) {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            str = "";
                        }
                        wd4.a((Object) str, "if (!TextUtils.isEmpty(r\u2026ication!!.title!! else \"\"");
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Message Notification title: ");
                        sb2.append(str);
                        sb2.append("\n body: ");
                        kz1.a J3 = kz1.J();
                        if (J3 != null) {
                            wd4.a((Object) J3, "remoteMessage.notification!!");
                            String a2 = J3.a();
                            if (a2 != null) {
                                sb2.append(a2);
                                local3.d("FossilFirebaseMessagingService", sb2.toString());
                                bk3 bk3 = this.k;
                                if (bk3 != null) {
                                    kz1.a J4 = kz1.J();
                                    if (J4 != null) {
                                        wd4.a((Object) J4, "remoteMessage.notification!!");
                                        String a3 = J4.a();
                                        if (a3 != null) {
                                            wd4.a((Object) a3, "remoteMessage.notification!!.body!!");
                                            bk3.a(str, a3);
                                            return;
                                        }
                                        wd4.a();
                                        throw null;
                                    }
                                    wd4.a();
                                    throw null;
                                }
                                wd4.d("mInAppNotificationManager");
                                throw null;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.W.c().g().a(this);
    }
}
