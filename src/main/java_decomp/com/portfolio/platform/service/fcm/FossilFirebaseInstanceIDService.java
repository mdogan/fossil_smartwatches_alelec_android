package com.portfolio.platform.service.fcm;

import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @DexIgnore
    public fn2 j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void a() {
        FirebaseInstanceId m = FirebaseInstanceId.m();
        wd4.a((Object) m, "FirebaseInstanceId.getInstance()");
        String c = m.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FossilFirebaseInstanceIDService", "Refreshed token: " + c);
        fn2 fn2 = this.j;
        if (fn2 != null) {
            fn2.r(c);
            a(c);
            return;
        }
        wd4.d("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d("FossilFirebaseInstanceIDService", "onCreate()");
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final ri4 a(String str) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1((kc4) null), 3, (Object) null);
    }
}
