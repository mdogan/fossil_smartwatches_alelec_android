package com.portfolio.platform.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.core.content.FileProvider;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ip2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tr1;
import com.fossil.blesdk.obfuscated.vk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ya0;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HwLogProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AppHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public WeakReference<Context> a;
    @DexIgnore
    public ip2 b;
    @DexIgnore
    public tr1 c;
    @DexIgnore
    public tr1 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ UserRepository h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = ShakeFeedbackService.class.getSimpleName();
        wd4.a((Object) simpleName, "ShakeFeedbackService::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public ShakeFeedbackService(UserRepository userRepository) {
        String str;
        wd4.b(userRepository, "mUserRepository");
        this.h = userRepository;
        if (b()) {
            str = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            wd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            str = applicationContext.getFilesDir().toString();
        }
        this.e = str;
        this.e = wd4.a(this.e, (Object) "/com.fossil.wearables.fossil/");
    }

    @DexIgnore
    public final boolean c() {
        tr1 tr1 = this.c;
        if (tr1 != null) {
            if (tr1 == null) {
                wd4.a();
                throw null;
            } else if (tr1.isShowing()) {
                tr1 tr12 = this.d;
                if (tr12 != null) {
                    if (tr12 == null) {
                        wd4.a();
                        throw null;
                    } else if (tr12.isShowing()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            wd4.a();
            throw null;
        } else if (vk2.a((Context) weakReference.get()) && !c()) {
            tr1 tr1 = this.d;
            if (tr1 != null) {
                if (tr1 != null) {
                    tr1.dismiss();
                } else {
                    wd4.a();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Object obj = weakReference2.get();
                if (obj != null) {
                    Object systemService = ((Context) obj).getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(R.layout.debug_question_sheet, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Object obj2 = weakReference3.get();
                            if (obj2 != null) {
                                this.d = new tr1((Context) obj2);
                                tr1 tr12 = this.d;
                                if (tr12 != null) {
                                    tr12.setContentView(inflate);
                                    View findViewById = inflate.findViewById(R.id.tv_version_name);
                                    if (findViewById != null) {
                                        ((TextView) findViewById).setText("4.1.3");
                                        inflate.findViewById(R.id.bt_kill_app_feedback).setOnClickListener(new ShakeFeedbackService$showQuestionDialog$Anon1(this));
                                        inflate.findViewById(R.id.bt_on_off_bluetooth_feedback).setOnClickListener(new ShakeFeedbackService$showQuestionDialog$Anon2(this));
                                        tr1 tr13 = this.d;
                                        if (tr13 != null) {
                                            tr13.show();
                                        } else {
                                            wd4.a();
                                            throw null;
                                        }
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final synchronized void e() {
        ip2 ip2 = this.b;
        if (ip2 != null) {
            ip2.a();
        }
        if (this.c != null) {
            tr1 tr1 = this.c;
            if (tr1 != null) {
                tr1.dismiss();
                this.c = null;
            } else {
                wd4.a();
                throw null;
            }
        }
        if (this.d != null) {
            tr1 tr12 = this.d;
            if (tr12 != null) {
                tr12.dismiss();
                this.d = null;
            } else {
                wd4.a();
                throw null;
            }
        }
        this.a = null;
    }

    @DexIgnore
    public final boolean b() {
        return wd4.a((Object) "mounted", (Object) Environment.getExternalStorageState());
    }

    @DexIgnore
    public final Object a(Context context, kc4<? super cb4> kc4) {
        if (context != null) {
            Window window = ((Activity) context).getWindow();
            wd4.a((Object) window, "(context as Activity).window");
            View decorView = window.getDecorView();
            wd4.a((Object) decorView, "(context as Activity).window.decorView");
            View rootView = decorView.getRootView();
            wd4.a((Object) rootView, "v1");
            rootView.setDrawingCacheEnabled(true);
            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            wd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
            return kg4.a(zh4.b(), new ShakeFeedbackService$sendLog$Anon2(this, createBitmap, (kc4) null), kc4);
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final synchronized void a(Context context) {
        wd4.b(context, Constants.ACTIVITY);
        this.a = new WeakReference<>(context);
        Object systemService = context.getSystemService("sensor");
        if (systemService != null) {
            SensorManager sensorManager = (SensorManager) systemService;
            this.b = new ip2(new ShakeFeedbackService$initShakeFeedbackService$Anon1(this));
            ip2 ip2 = this.b;
            if (ip2 != null) {
                ip2.a(sensorManager);
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type android.hardware.SensorManager");
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(kc4<? super cb4> kc4) {
        WeakReference<Context> weakReference = this.a;
        if (weakReference != null) {
            HwLogProvider instance = HwLogProvider.getInstance((Context) weakReference.get());
            wd4.a((Object) instance, "HwLogProvider.getInstanc\u2026extWeakReference!!.get())");
            String a2 = new Gson().a((Object) instance.getAllHardwareLogs());
            String str = this.e;
            if (str != null) {
                try {
                    new File(str).mkdir();
                    String str2 = this.e;
                    be4 be4 = be4.a;
                    Object[] objArr = {pc4.a(System.currentTimeMillis())};
                    String format = String.format("hwlog_%s.txt", Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    File file = new File(str2, format);
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    wd4.a((Object) a2, "jsonLog");
                    Charset charset = nf4.a;
                    if (a2 != null) {
                        byte[] bytes = a2.getBytes(charset);
                        wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        fileOutputStream.write(bytes);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        rk2 rk2 = rk2.a;
                        WeakReference<Context> weakReference2 = this.a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj != null) {
                                wd4.a(obj, "contextWeakReference!!.get()!!");
                                Context context = (Context) obj;
                                String str3 = this.e;
                                if (str3 == null) {
                                    str3 = "";
                                }
                                String a3 = rk2.a(context, str3);
                                ArrayList arrayList = new ArrayList();
                                if (a3 != null) {
                                    arrayList.add(a3);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String str4 = i;
                                    local.d(str4, "HwLogFile=" + file.getAbsolutePath());
                                    ArrayList arrayList2 = new ArrayList();
                                    arrayList2.add(file);
                                    a((String) null, arrayList, arrayList2);
                                    WeakReference<Context> weakReference3 = this.a;
                                    if (weakReference3 != null) {
                                        HwLogProvider.getInstance((Context) weakReference3.get()).setHardwareLogRead();
                                        return cb4.a;
                                    }
                                    wd4.a();
                                    throw null;
                                }
                                wd4.a();
                                throw null;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                } catch (Exception e2) {
                    e2.printStackTrace();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = i;
                    local2.e(str5, ".sendHardwareLog - ex=" + e2);
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str, List<String> list, List<? extends File> list2) {
        String str2 = str;
        Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
        intent.setType("vnd.android.cursor.dir/email");
        if (!PortfolioApp.W.e()) {
            intent.putExtra("android.intent.extra.EMAIL", new String[]{"help@misfit.com"});
        } else if (this.g != -1) {
            intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
        } else {
            int i2 = this.f;
            if (i2 == 0) {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
            } else if (i2 == 2) {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "dungdna@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com", "fossiluat@fossil.com"});
            } else {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "dungdna@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
            }
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        long j = 0;
        if ((!list.isEmpty()) && PortfolioApp.W.e()) {
            for (String next : list) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local.e(str3, ".sendFeedbackEmail - databaseFile=" + next);
                if (next != null) {
                    Uri a2 = a(next);
                    if (a2 != null) {
                        arrayList.add(a2);
                        arrayList2.add(next);
                        String path = a2.getPath();
                        if (path != null) {
                            j += new File(path).length();
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Context applicationContext = PortfolioApp.W.c().getApplicationContext();
        wd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
        File filesDir = applicationContext.getFilesDir();
        wd4.a((Object) filesDir, "PortfolioApp.instance.applicationContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(File.separator);
        sb.append(ya0.c.b());
        File a3 = a("NewSDKLog", new File(sb.toString()));
        if (a3 != null) {
            String absolutePath = a3.getAbsolutePath();
            wd4.a((Object) absolutePath, "newSdkZipFile.absolutePath");
            Uri a4 = a(absolutePath);
            if (a4 != null) {
                arrayList.add(a4);
                arrayList2.add(a3.getAbsolutePath());
                j += a3.length();
            }
        }
        if (!TextUtils.isEmpty(str)) {
            if (str2 != null) {
                Uri a5 = a(str);
                if (a5 != null) {
                    arrayList.add(a5);
                    arrayList2.add(str2);
                    String path2 = a5.getPath();
                    if (path2 != null) {
                        j += new File(path2).length();
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        for (File file : list2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = i;
            local2.d(str4, "sendFeedbackEmail - logFile=" + file.getName() + ", length=" + file.length());
            String absolutePath2 = file.getAbsolutePath();
            wd4.a((Object) absolutePath2, "log.absolutePath");
            Uri a6 = a(absolutePath2);
            if (a6 != null) {
                j += file.length();
                arrayList.add(a6);
                arrayList2.add(file.getAbsolutePath());
            }
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str5 = i;
        local3.d(str5, ".sendFeedbackEmail - dataFeedbackSize=" + j);
        if (j > 9437184) {
            File a7 = a((ArrayList<String>) arrayList2);
            arrayList.clear();
            String absolutePath3 = a7.getAbsolutePath();
            wd4.a((Object) absolutePath3, "file.absolutePath");
            Uri a8 = a(absolutePath3);
            if (a8 != null) {
                arrayList.add(a8);
            }
        }
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
        int i3 = this.g;
        if (i3 == -1) {
            String str6 = "Staging] - ";
            if (this.f == 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("[BETA - ");
                sb2.append(PortfolioApp.W.c().i().getName());
                sb2.append(" ");
                if (!PortfolioApp.W.e()) {
                    str6 = "] - ";
                }
                sb2.append(str6);
                sb2.append("[Android - Feedback]");
                intent.putExtra("android.intent.extra.SUBJECT", sb2.toString());
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("[");
                sb3.append(PortfolioApp.W.c().i().getName());
                sb3.append(" ");
                if (!PortfolioApp.W.e()) {
                    str6 = "] - ";
                }
                sb3.append(str6);
                sb3.append("[Android Feedback] -");
                be4 be4 = be4.a;
                Object[] objArr = {Long.valueOf(System.currentTimeMillis() / ((long) 1000)), sk2.c(new Date())};
                String format = String.format(" on %s (%s)", Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                sb3.append(format);
                intent.putExtra("android.intent.extra.SUBJECT", sb3.toString());
            }
        } else if (i3 == 0) {
            intent.putExtra("android.intent.extra.SUBJECT", "GATT CONNECTION");
        } else if (i3 == 1) {
            intent.putExtra("android.intent.extra.SUBJECT", "HID CONNECTION");
        }
        try {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("[");
            sb4.append(PortfolioApp.W.c().i().getName());
            sb4.append(" ");
            sb4.append(PortfolioApp.W.e() ? "Staging" : "");
            sb4.append("]\n");
            AppHelper a9 = AppHelper.f.a();
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    wd4.a(obj, "contextWeakReference!!.get()!!");
                    sb4.append(a9.a((Context) obj, this.f, this.g));
                    intent.putExtra("android.intent.extra.TEXT", sb4.toString());
                    WeakReference<Context> weakReference2 = this.a;
                    if (weakReference2 == null) {
                        return;
                    }
                    if (weakReference2 == null) {
                        wd4.a();
                        throw null;
                    } else if (weakReference2.get() != null) {
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Object obj2 = weakReference3.get();
                            if (obj2 != null) {
                                Context context = (Context) obj2;
                                WeakReference<Context> weakReference4 = this.a;
                                if (weakReference4 != null) {
                                    Object obj3 = weakReference4.get();
                                    if (obj3 != null) {
                                        context.startActivity(Intent.createChooser(intent, ((Context) obj3).getString(R.string.rate_app_send_email)));
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str7 = i;
            local4.d(str7, ".sendFeedbackEmail - ex=" + e2);
        }
    }

    @DexIgnore
    public final File a(String str, File file) {
        File file2 = null;
        if (file == null) {
            return null;
        }
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            File file3 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file3));
                for (File file4 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file4);
                    wd4.a((Object) file4, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file4.getName()));
                    Ref$IntRef ref$IntRef = new Ref$IntRef();
                    while (true) {
                        ref$IntRef.element = fileInputStream.read(bArr);
                        if (!(ref$IntRef.element > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, ref$IntRef.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file3;
            } catch (IOException e2) {
                e = e2;
                file2 = file3;
                FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
                return file2;
            }
        } catch (IOException e3) {
            e = e3;
            FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
            return file2;
        }
    }

    @DexIgnore
    public final List<File> a() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            wd4.a((Object) file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                wd4.a((Object) channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                wd4.a((Object) channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = i;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00af, code lost:
        if (r8 != null) goto L_0x00b3;
     */
    @DexIgnore
    public final /* synthetic */ Object a(Bitmap bitmap, kc4<? super cb4> kc4) {
        String str;
        String str2 = this.e;
        if (str2 != null) {
            try {
                new File(str2).mkdir();
                File file = new File(this.e, "screenshot.jpg");
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local.e(str3, ".ScreenShootTask - doInBackground ex=" + e2);
            }
            be4 be4 = be4.a;
            Object[] objArr = {pc4.a(System.currentTimeMillis() / ((long) 1000))};
            String format = String.format("logCat_%s.txt", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            File file2 = new File(this.e, format);
            try {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("logcat -v time -d -f " + file2.getAbsolutePath());
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            MFUser currentUser = this.h.getCurrentUser();
            String str4 = "";
            if (currentUser != null) {
                str = currentUser.getUserId();
            }
            str = str4;
            rk2 rk2 = rk2.a;
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    wd4.a(obj, "contextWeakReference!!.get()!!");
                    Context context = (Context) obj;
                    String str5 = this.e;
                    if (str5 != null) {
                        str4 = str5;
                    }
                    List<String> a2 = rk2.a(context, str, str4);
                    StringBuilder sb = new StringBuilder();
                    String str6 = this.e;
                    if (str6 != null) {
                        sb.append(str6);
                        sb.append("screenshot.jpg");
                        String sb2 = sb.toString();
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(file2);
                        List<File> a3 = a();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str7 = i;
                        local2.d(str7, "Number of app logs=" + a3.size());
                        arrayList.addAll(a3);
                        a(sb2, a2, arrayList);
                        return cb4.a;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final File a(ArrayList<String> arrayList) {
        File file = new File(Environment.getExternalStorageDirectory(), "DebugDataLog.zip");
        try {
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file));
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                byte[] bArr = new byte[1024];
                File file2 = new File(it.next());
                FileInputStream fileInputStream = new FileInputStream(file2);
                try {
                    zipOutputStream.putNextEntry(new ZipEntry(file2.getName()));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = i;
                    local.d(str, "zipDataFile - Processing file=" + file2.getName() + ", length=" + file2.length());
                    Ref$IntRef ref$IntRef = new Ref$IntRef();
                    while (true) {
                        ref$IntRef.element = fileInputStream.read(bArr);
                        if (!(ref$IntRef.element > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, ref$IntRef.element);
                    }
                    fileInputStream.close();
                } catch (ZipException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = i;
                    local2.e(str2, ".zipDataFile - ZipException=" + e2);
                }
            }
            zipOutputStream.close();
        } catch (IOException e3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = i;
            local3.e(str3, ".zipDataFile - ex=" + e3);
        }
        return file;
    }

    @DexIgnore
    public final Uri a(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    return FileProvider.getUriForFile((Context) obj, "com.fossil.wearables.fossil.provider", new File(str));
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
    }
}
