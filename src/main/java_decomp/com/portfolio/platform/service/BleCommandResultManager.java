package com.portfolio.platform.service;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleCommandResultManager {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static c<CommunicateMode, a> b; // = new c<>();
    @DexIgnore
    public static /* final */ HashMap<CommunicateMode, d<b>> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ BleCommandResultManager d; // = new BleCommandResultManager();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Intent a;

        @DexIgnore
        public a(CommunicateMode communicateMode, String str, Intent intent) {
            wd4.b(communicateMode, "mode");
            wd4.b(intent, "intent");
            this.a = intent;
        }

        @DexIgnore
        public final Intent a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CommunicateMode communicateMode, Intent intent);
    }

    /*
    static {
        String simpleName = BleCommandResultManager.class.getSimpleName();
        wd4.a((Object) simpleName, "BleCommandResultManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final synchronized void a(CommunicateMode communicateMode, a aVar) {
        wd4.b(communicateMode, "mode");
        wd4.b(aVar, "bleResult");
        b.b(communicateMode, aVar);
        a(communicateMode);
    }

    @DexIgnore
    public final void b(b bVar, CommunicateMode... communicateModeArr) {
        wd4.b(bVar, "observerReceiver");
        wd4.b(communicateModeArr, "communicateModes");
        b(bVar, (List<? extends CommunicateMode>) ob4.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> {
        @DexIgnore
        public /* final */ List<T> a; // = new ArrayList();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final List<T> a() {
            ArrayList arrayList;
            synchronized (this.b) {
                arrayList = new ArrayList();
                arrayList.addAll(this.a);
            }
            return arrayList;
        }

        @DexIgnore
        public final int b() {
            return this.a.size();
        }
    }

    @DexIgnore
    public final synchronized void b(b bVar, List<? extends CommunicateMode> list) {
        wd4.b(bVar, "observerReceiver");
        wd4.b(list, "communicateModes");
        for (CommunicateMode communicateMode : list) {
            d dVar = c.get(communicateMode);
            if (dVar != null) {
                dVar.b(bVar);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T, V> {
        @DexIgnore
        public /* final */ HashMap<T, V> a; // = new HashMap<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final boolean a(T t, V v) {
            boolean z;
            synchronized (this.b) {
                if (wd4.a((Object) this.a.get(t), (Object) v)) {
                    this.a.remove(t);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        @DexIgnore
        public final void b(T t, V v) {
            synchronized (this.b) {
                this.a.put(t, v);
                cb4 cb4 = cb4.a;
            }
        }

        @DexIgnore
        public String toString() {
            String hashMap = this.a.toString();
            wd4.a((Object) hashMap, "mHashMap.toString()");
            return hashMap;
        }

        @DexIgnore
        public final V a(T t) {
            return this.a.get(t);
        }
    }

    @DexIgnore
    public final void a(b bVar, CommunicateMode... communicateModeArr) {
        wd4.b(bVar, "observerReceiver");
        wd4.b(communicateModeArr, "communicateModes");
        a(bVar, (List<? extends CommunicateMode>) ob4.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void a(CommunicateMode... communicateModeArr) {
        wd4.b(communicateModeArr, "communicateModes");
        a((List<? extends CommunicateMode>) ob4.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final synchronized void a(List<? extends CommunicateMode> list) {
        wd4.b(list, "communicateModes");
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BleCommandResultManager$retrieveBleResult$Anon1(list, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final synchronized void a(b bVar, List<? extends CommunicateMode> list) {
        wd4.b(bVar, "observerReceiver");
        wd4.b(list, "communicateModes");
        for (CommunicateMode communicateMode : list) {
            d dVar = c.get(communicateMode);
            if (dVar != null) {
                dVar.a(bVar);
            } else {
                HashMap<CommunicateMode, d<b>> hashMap = c;
                d dVar2 = new d();
                dVar2.a(bVar);
                hashMap.put(communicateMode, dVar2);
            }
        }
    }
}
