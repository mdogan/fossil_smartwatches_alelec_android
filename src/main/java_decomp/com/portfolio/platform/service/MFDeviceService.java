package com.portfolio.platform.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.hj2;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ij2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mj2;
import com.fossil.blesdk.obfuscated.nr3;
import com.fossil.blesdk.obfuscated.on2;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qs3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.fitness.FitnessData;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.enums.FossilBrand;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.receiver.RestartServiceReceiver;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService extends Service {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ String T;
    @DexIgnore
    public static /* final */ a U; // = new a((rd4) null);
    @DexIgnore
    public yk2 A;
    @DexIgnore
    public WatchParamHelper B;
    @DexIgnore
    public MisfitDeviceProfile C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public /* final */ on2 E; // = new on2();
    @DexIgnore
    public /* final */ Gson F; // = new Gson();
    @DexIgnore
    public /* final */ MFDeviceService$mButtonConnectivity$Anon1 G; // = new MFDeviceService$mButtonConnectivity$Anon1(this);
    @DexIgnore
    public boolean H;
    @DexIgnore
    public Handler I;
    @DexIgnore
    public /* final */ Runnable J; // = new g(this);
    @DexIgnore
    public /* final */ MFDeviceService$bleActionServiceReceiver$Anon1 K; // = new MFDeviceService$bleActionServiceReceiver$Anon1(this);
    @DexIgnore
    public /* final */ MFDeviceService$connectionStateChangeReceiver$Anon1 L; // = new MFDeviceService$connectionStateChangeReceiver$Anon1(this);
    @DexIgnore
    public /* final */ i M; // = new i();
    @DexIgnore
    public /* final */ f N; // = new f();
    @DexIgnore
    public /* final */ d O; // = new d();
    @DexIgnore
    public /* final */ e P; // = new e();
    @DexIgnore
    public /* final */ h Q; // = new h();
    @DexIgnore
    public /* final */ c R; // = new c(this);
    @DexIgnore
    public /* final */ b e; // = new b();
    @DexIgnore
    public fn2 f;
    @DexIgnore
    public DeviceRepository g;
    @DexIgnore
    public ActivitiesRepository h;
    @DexIgnore
    public SummariesRepository i;
    @DexIgnore
    public SleepSessionsRepository j;
    @DexIgnore
    public SleepSummariesRepository k;
    @DexIgnore
    public UserRepository l;
    @DexIgnore
    public HybridPresetRepository m;
    @DexIgnore
    public MicroAppRepository n;
    @DexIgnore
    public HeartRateSampleRepository o;
    @DexIgnore
    public HeartRateSummaryRepository p;
    @DexIgnore
    public WorkoutSessionRepository q;
    @DexIgnore
    public FitnessDataRepository r;
    @DexIgnore
    public GoalTrackingRepository s;
    @DexIgnore
    public i42 t;
    @DexIgnore
    public AnalyticsHelper u;
    @DexIgnore
    public PortfolioApp v;
    @DexIgnore
    public LinkStreamingManager w;
    @DexIgnore
    public ThirdPartyRepository x;
    @DexIgnore
    public nr3 y;
    @DexIgnore
    public VerifySecretKeyUseCase z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFDeviceService.T;
        }

        @DexIgnore
        public final String b() {
            return MFDeviceService.S;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Binder {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final MFDeviceService a() {
            return MFDeviceService.this;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        public c(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            if (intent != null) {
                String stringExtra = intent.getStringExtra("message");
                MFDeviceService mFDeviceService = this.a;
                wd4.a((Object) stringExtra, "message");
                mFDeviceService.a(stringExtra);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Device app event receive");
            if (intent != null) {
                PortfolioApp.W.a((Object) new hj2(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getExtras()));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            if (intent != null) {
                int intExtra = intent.getIntExtra(Constants.DAILY_STEPS, 0);
                int intExtra2 = intent.getIntExtra(Constants.DAILY_POINTS, 0);
                Calendar instance = Calendar.getInstance();
                wd4.a((Object) instance, "Calendar.getInstance()");
                long longExtra = intent.getLongExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
                FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), ".saveSyncResult - Update heartbeat step by heartbeat receiver");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.U.b();
                local.i(b, "Heartbeat data received, dailySteps=" + intExtra + ", dailyPoints=" + intExtra2 + ", receivedTime=" + longExtra);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Micro app cancel event receive");
            if (intent != null) {
                PortfolioApp.W.a((Object) new ij2(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getIntExtra(Constants.VARIANT_ID, -1), intent.getIntExtra("gesture", -1)));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService e;

        @DexIgnore
        public g(MFDeviceService mFDeviceService) {
            this.e = mFDeviceService;
        }

        @DexIgnore
        public final void run() {
            if (cg4.b("release", "debug", true) && this.e.c().i() == FossilBrand.PORTFOLIO) {
                this.e.c().I();
                this.e.x();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            wd4.b(intent, "intent");
            intent.setAction("SCAN_DEVICE_FOUND");
            sc.a(context).a(intent);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Streaming event receive");
            if (intent != null) {
                int intExtra = intent.getIntExtra("gesture", -1);
                PortfolioApp.W.a((Object) new mj2(intent.getStringExtra(Constants.SERIAL_NUMBER), intExtra));
            }
        }
    }

    /*
    static {
        String simpleName = MFDeviceService.class.getSimpleName();
        wd4.a((Object) simpleName, "MFDeviceService::class.java.simpleName");
        S = simpleName;
        StringBuilder sb = new StringBuilder();
        Package packageR = MFDeviceService.class.getPackage();
        if (packageR != null) {
            wd4.a((Object) packageR, "MFDeviceService::class.java.`package`!!");
            sb.append(packageR.getName());
            sb.append(".location_updated");
            T = sb.toString();
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public final ri4 A() {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$updatePairedDeviceToButtonService$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ActivitiesRepository b() {
        ActivitiesRepository activitiesRepository = this.h;
        if (activitiesRepository != null) {
            return activitiesRepository;
        }
        wd4.d("mActivitiesRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp c() {
        PortfolioApp portfolioApp = this.v;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wd4.d("mApp");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository d() {
        DeviceRepository deviceRepository = this.g;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final nr3 e() {
        nr3 nr3 = this.y;
        if (nr3 != null) {
            return nr3;
        }
        wd4.d("mEncryptUseCase");
        throw null;
    }

    @DexIgnore
    public final FitnessDataRepository f() {
        FitnessDataRepository fitnessDataRepository = this.r;
        if (fitnessDataRepository != null) {
            return fitnessDataRepository;
        }
        wd4.d("mFitnessDataRepository");
        throw null;
    }

    @DexIgnore
    public final yk2 g() {
        yk2 yk2 = this.A;
        if (yk2 != null) {
            return yk2;
        }
        wd4.d("mFitnessHelper");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository h() {
        GoalTrackingRepository goalTrackingRepository = this.s;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        wd4.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSampleRepository i() {
        HeartRateSampleRepository heartRateSampleRepository = this.o;
        if (heartRateSampleRepository != null) {
            return heartRateSampleRepository;
        }
        wd4.d("mHeartRateSampleRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryRepository j() {
        HeartRateSummaryRepository heartRateSummaryRepository = this.p;
        if (heartRateSummaryRepository != null) {
            return heartRateSummaryRepository;
        }
        wd4.d("mHeartRateSummaryRepository");
        throw null;
    }

    @DexIgnore
    public final MicroAppRepository k() {
        MicroAppRepository microAppRepository = this.n;
        if (microAppRepository != null) {
            return microAppRepository;
        }
        wd4.d("mMicroAppRepository");
        throw null;
    }

    @DexIgnore
    public final HybridPresetRepository l() {
        HybridPresetRepository hybridPresetRepository = this.m;
        if (hybridPresetRepository != null) {
            return hybridPresetRepository;
        }
        wd4.d("mPresetRepository");
        throw null;
    }

    @DexIgnore
    public final fn2 m() {
        fn2 fn2 = this.f;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSessionsRepository n() {
        SleepSessionsRepository sleepSessionsRepository = this.j;
        if (sleepSessionsRepository != null) {
            return sleepSessionsRepository;
        }
        wd4.d("mSleepSessionsRepository");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository o() {
        SleepSummariesRepository sleepSummariesRepository = this.k;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        wd4.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        FLogger.INSTANCE.getLocal().i(S, "on misfit service bind");
        return this.e;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.W.c().g().a(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "Inside " + S + ".onCreate");
        v();
    }

    @DexIgnore
    public void onDestroy() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "Inside " + S + ".onDestroy");
        super.onDestroy();
        if (this.H) {
            qs3.a.a((Context) this, (ServiceConnection) this.G);
        }
        unregisterReceiver(this.K);
        unregisterReceiver(this.R);
        unregisterReceiver(this.M);
        unregisterReceiver(this.L);
        unregisterReceiver(this.Q);
        unregisterReceiver(this.O);
        unregisterReceiver(this.N);
        unregisterReceiver(this.P);
        unregisterReceiver(this.E);
        this.D = false;
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.i(str, "onStartCommand() - isButtonServiceBound=" + this.H);
        if (cg4.b(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, (Object) null)) {
            FLogger.INSTANCE.getLocal().d(S, "onStartCommand() - Received Start Foreground Intent");
            FossilNotificationBar.c.a((Context) this, (Service) this, false);
        } else {
            FLogger.INSTANCE.getLocal().d(S, "onStartCommand() - Received Stop Foreground Intent");
            FossilNotificationBar.c.a((Context) this, (Service) this, true);
        }
        if (!this.H) {
            qs3.a.a((Context) this, ButtonService.class, (ServiceConnection) this.G, 1);
        }
        return 1;
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(S, "onTaskRemoved()");
        stopSelf();
        sendBroadcast(new Intent(this, RestartServiceReceiver.class));
    }

    @DexIgnore
    public final SummariesRepository p() {
        SummariesRepository summariesRepository = this.i;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        wd4.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final ThirdPartyRepository q() {
        ThirdPartyRepository thirdPartyRepository = this.x;
        if (thirdPartyRepository != null) {
            return thirdPartyRepository;
        }
        wd4.d("mThirdPartyRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository r() {
        UserRepository userRepository = this.l;
        if (userRepository != null) {
            return userRepository;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final VerifySecretKeyUseCase s() {
        VerifySecretKeyUseCase verifySecretKeyUseCase = this.z;
        if (verifySecretKeyUseCase != null) {
            return verifySecretKeyUseCase;
        }
        wd4.d("mVerifySecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final WatchParamHelper t() {
        WatchParamHelper watchParamHelper = this.B;
        if (watchParamHelper != null) {
            return watchParamHelper;
        }
        wd4.d("mWatchParamHelper");
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionRepository u() {
        WorkoutSessionRepository workoutSessionRepository = this.q;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        wd4.d("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final void v() {
        if (this.D) {
            FLogger.INSTANCE.getLocal().e(S, "Return from device service register receiver");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.i(str, "Inside " + S + ".registerReceiver " + b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE()));
        registerReceiver(this.K, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE())));
        registerReceiver(this.R, new IntentFilter(b(ButtonService.Companion.getACTION_ANALYTIC_EVENT())));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local2.i(str2, "Inside " + S + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT());
        registerReceiver(this.M, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT())));
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = S;
        local3.i(str3, "Inside " + S + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT());
        registerReceiver(this.N, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT())));
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = S;
        local4.i(str4, "Inside " + S + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT());
        registerReceiver(this.O, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT())));
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = S;
        local5.i(str5, "Inside " + S + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA());
        registerReceiver(this.P, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA())));
        registerReceiver(this.L, new IntentFilter(b(ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE())));
        registerReceiver(this.Q, new IntentFilter(b(ButtonService.Companion.getACTION_SCAN_DEVICE_FOUND())));
        registerReceiver(this.E, new IntentFilter("android.intent.action.TIME_TICK"));
        this.D = true;
    }

    @DexIgnore
    public final void w() {
        LinkStreamingManager linkStreamingManager = this.w;
        if (linkStreamingManager != null) {
            linkStreamingManager.a();
            PortfolioApp.W.c().g().a(this);
            return;
        }
        wd4.d("mLinkStreamingManager");
        throw null;
    }

    @DexIgnore
    public final void x() {
        y();
        FLogger.INSTANCE.getLocal().d(S, "Start reading realtime step timeoutHandler 30 mins");
        this.I = new Handler(getMainLooper());
        Handler handler = this.I;
        if (handler != null) {
            handler.postDelayed(this.J, 1800000);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void y() {
        if (this.I != null) {
            FLogger.INSTANCE.getLocal().d(S, "Stop reading realtime step timeoutHandler");
            Handler handler = this.I;
            if (handler != null) {
                handler.removeCallbacks(this.J);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void z() {
        boolean b2 = FossilNotificationListenerService.s.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "triggerReconnectNotificationService() - isConnectedNotificationManager = " + b2);
        if (!b2 && os3.a.e()) {
            FossilNotificationListenerService.s.c();
        }
    }

    @DexIgnore
    public final String b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local.d(str2, "Key=" + getPackageName() + str);
        return getPackageName() + str;
    }

    @DexIgnore
    public final void c(String str) {
        wd4.b(str, "serial");
        LocationProvider g2 = en2.p.a().g();
        DeviceLocation deviceLocation = g2.getDeviceLocation(str);
        double d2 = 0.0d;
        double latitude = deviceLocation != null ? deviceLocation.getLatitude() : 0.0d;
        if (deviceLocation != null) {
            d2 = deviceLocation.getLongitude();
        }
        g2.saveDeviceLocation(new DeviceLocation(str, latitude, d2, System.currentTimeMillis()));
    }

    @DexIgnore
    public final MisfitDeviceProfile a() {
        return this.C;
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile) {
        this.C = misfitDeviceProfile;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        int i2 = bundle.getInt(ButtonService.WATCH_PARAMS_MAJOR);
        float f2 = bundle.getFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$executeGetWatchParamsFlow$Anon1(this, str, i2, f2, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final List<FitnessDataWrapper> a(List<FitnessData> list, String str, DateTime dateTime) {
        wd4.b(list, "syncData");
        wd4.b(str, "serial");
        wd4.b(dateTime, "syncTime");
        ArrayList arrayList = new ArrayList();
        for (FitnessData fitnessData : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = S;
            local.d(str2, "saveFitnessData=" + fitnessData);
            arrayList.add(new FitnessDataWrapper(fitnessData, str, dateTime));
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ void a(MFDeviceService mFDeviceService, String str, int i2, int i3, ArrayList arrayList, Integer num, int i4, Object obj) {
        if ((i4 & 8) != 0) {
            arrayList = new ArrayList(i3);
        }
        ArrayList arrayList2 = arrayList;
        if ((i4 & 16) != 0) {
            num = 10;
        }
        mFDeviceService.a(str, i2, i3, arrayList2, num);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, ArrayList<Integer> arrayList, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local.i(str2, "Broadcast sync status=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra("LAST_ERROR_CODE", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), num);
        intent.putIntegerArrayListExtra("LIST_ERROR_CODE", arrayList);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.a(communicateMode, new BleCommandResultManager.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile, String str) {
        wd4.b(str, "serial");
        if (misfitDeviceProfile != null) {
            boolean isDefaultValue = misfitDeviceProfile.getVibrationStrength().isDefaultValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = S;
            local.d(str2, "updateVibrationStrengthLevel - isDefaultValue: " + isDefaultValue);
            if (!isDefaultValue) {
                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1(misfitDeviceProfile, (kc4) null, this, str), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public final ri4 a(MisfitDeviceProfile misfitDeviceProfile, String str, boolean z2) {
        wd4.b(str, "serial");
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$updateDeviceData$Anon1(this, str, z2, misfitDeviceProfile, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(String str, Location location, int i2) {
        wd4.b(str, "serial");
        if (i2 >= 0) {
            if (location != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = S;
                local.d(str2, "Inside " + S + ".onLocationUpdated - location=[lat:" + location.getLatitude() + ", lon:" + location.getLongitude() + ", accuracy:" + location.getAccuracy() + "]");
                if (location.getAccuracy() <= 500.0f) {
                    try {
                        DeviceLocation deviceLocation = new DeviceLocation(str, location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                        a(str, deviceLocation);
                        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$onLocationUpdated$Anon1(deviceLocation, (kc4) null), 3, (Object) null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = S;
                        local2.e(str3, "Error inside " + S + ".onLocationUpdated - e=" + e2);
                    }
                }
            }
        } else if (i2 != -1) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = S;
            local3.e(str4, "Error inside " + S + ".onLocationUpdated - code=" + i2);
        }
    }

    @DexIgnore
    public final void a(String str, DeviceLocation deviceLocation) {
        Intent intent = new Intent();
        intent.putExtra("SERIAL", str);
        intent.putExtra("device_location", deviceLocation);
        intent.setAction(T);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void a(SKUModel sKUModel, int i2, int i3) {
        if (sKUModel != null) {
            if (i3 == 1) {
                AnalyticsHelper analyticsHelper = this.u;
                if (analyticsHelper != null) {
                    String sku = sKUModel.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    String deviceName = sKUModel.getDeviceName();
                    if (deviceName == null) {
                        deviceName = "";
                    }
                    analyticsHelper.c(sku, deviceName, i2);
                } else {
                    wd4.d("mAnalyticsHelper");
                    throw null;
                }
            } else if (i3 == 2) {
                AnalyticsHelper analyticsHelper2 = this.u;
                if (analyticsHelper2 != null) {
                    String sku2 = sKUModel.getSku();
                    if (sku2 == null) {
                        sku2 = "";
                    }
                    String deviceName2 = sKUModel.getDeviceName();
                    if (deviceName2 == null) {
                        deviceName2 = "";
                    }
                    analyticsHelper2.b(sku2, deviceName2, i2);
                } else {
                    wd4.d("mAnalyticsHelper");
                    throw null;
                }
            }
            fn2 fn2 = this.f;
            if (fn2 != null) {
                fn2.a((Boolean) false);
            } else {
                wd4.d("mSharedPreferencesManager");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, Constants.EVENT);
        AnalyticsHelper.f.c().a(str);
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        wd4.b(str, Constants.EVENT);
        AnalyticsHelper.f.c().a(str, (Map<String, ? extends Object>) map);
    }
}
