package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$Anon1", f = "BleCommandResultManager.kt", l = {69}, m = "invokeSuspend")
public final class BleCommandResultManager$retrieveBleResult$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $communicateModes;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommandResultManager$retrieveBleResult$Anon1(List list, kc4 kc4) {
        super(2, kc4);
        this.$communicateModes = list;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BleCommandResultManager$retrieveBleResult$Anon1 bleCommandResultManager$retrieveBleResult$Anon1 = new BleCommandResultManager$retrieveBleResult$Anon1(this.$communicateModes, kc4);
        bleCommandResultManager$retrieveBleResult$Anon1.p$ = (lh4) obj;
        return bleCommandResultManager$retrieveBleResult$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleCommandResultManager$retrieveBleResult$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Iterable iterable;
        Iterator it;
        Object obj2;
        BleCommandResultManager$retrieveBleResult$Anon1 bleCommandResultManager$retrieveBleResult$Anon1;
        CommunicateMode communicateMode;
        Object obj3;
        BleCommandResultManager.a aVar;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            List list = this.$communicateModes;
            it = list.iterator();
            lh4 = lh42;
            iterable = list;
            obj2 = a;
            bleCommandResultManager$retrieveBleResult$Anon1 = this;
        } else if (i == 1) {
            BleCommandResultManager.d dVar = (BleCommandResultManager.d) this.L$Anon6;
            aVar = (BleCommandResultManager.a) this.L$Anon5;
            it = (Iterator) this.L$Anon2;
            iterable = (Iterable) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj3 = a;
            communicateMode = (CommunicateMode) this.L$Anon4;
            bleCommandResultManager$retrieveBleResult$Anon1 = this;
            BleCommandResultManager.b.a(communicateMode, aVar);
            obj2 = obj3;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (it.hasNext()) {
            Object next = it.next();
            communicateMode = (CommunicateMode) next;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = BleCommandResultManager.a;
            local.d(c, "retrieve mode=" + communicateMode);
            BleCommandResultManager.a aVar2 = (BleCommandResultManager.a) BleCommandResultManager.b.a(communicateMode);
            if (aVar2 != null) {
                BleCommandResultManager.d dVar2 = (BleCommandResultManager.d) BleCommandResultManager.c.get(communicateMode);
                if (dVar2 != null && dVar2.b() > 0) {
                    if (communicateMode == CommunicateMode.SYNC) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String c2 = BleCommandResultManager.a;
                        local2.d(c2, "SyncMode=" + aVar2.a().getIntExtra("sync_result", 3));
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String c3 = BleCommandResultManager.a;
                    local3.d(c3, "retrieve mode success=" + dVar2.b());
                    bj4 c4 = zh4.c();
                    BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 = new BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1(dVar2, communicateMode, aVar2, (kc4) null);
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon0 = lh4;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon1 = iterable;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon2 = it;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon3 = next;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon4 = communicateMode;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon5 = aVar2;
                    bleCommandResultManager$retrieveBleResult$Anon1.L$Anon6 = dVar2;
                    bleCommandResultManager$retrieveBleResult$Anon1.label = 1;
                    if (kg4.a(c4, bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1, bleCommandResultManager$retrieveBleResult$Anon1) == obj2) {
                        return obj2;
                    }
                    obj3 = obj2;
                    aVar = aVar2;
                    BleCommandResultManager.b.a(communicateMode, aVar);
                    obj2 = obj3;
                    if (it.hasNext()) {
                    }
                    return obj2;
                }
            }
            if (it.hasNext()) {
            }
        }
        return cb4.a;
    }
}
