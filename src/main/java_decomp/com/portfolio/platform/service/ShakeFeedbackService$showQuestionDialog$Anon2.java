package com.portfolio.platform.service;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.tr1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService$showQuestionDialog$Anon2 implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1", f = "ShakeFeedbackService.kt", l = {262, 263}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$showQuestionDialog$Anon2 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0125Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0125Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0125Anon1 anon1 = new C0125Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0125Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    tr1 c = this.this$Anon0.this$Anon0.e.d;
                    if (c != null) {
                        c.dismiss();
                        return cb4.a;
                    }
                    wd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ShakeFeedbackService$showQuestionDialog$Anon2 shakeFeedbackService$showQuestionDialog$Anon2, Bitmap bitmap, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = shakeFeedbackService$showQuestionDialog$Anon2;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            lh4 lh4;
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e;
                Bitmap bitmap = this.$bitmap;
                this.L$Anon0 = lh4;
                this.label = 1;
                if (shakeFeedbackService.a(bitmap, (kc4<? super cb4>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else if (i == 2) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                return cb4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            bj4 c = zh4.c();
            C0125Anon1 anon1 = new C0125Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 2;
            if (kg4.a(c, anon1, this) == a) {
                return a;
            }
            return cb4.a;
        }
    }

    @DexIgnore
    public ShakeFeedbackService$showQuestionDialog$Anon2(ShakeFeedbackService shakeFeedbackService) {
        this.e = shakeFeedbackService;
    }

    @DexIgnore
    public final void onClick(View view) {
        os3.a aVar = os3.a;
        WeakReference b = this.e.a;
        if (b != null) {
            Object obj = b.get();
            if (obj == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
            } else if (aVar.c((Activity) obj, 123)) {
                WeakReference b2 = this.e.a;
                if (b2 != null) {
                    Object obj2 = b2.get();
                    if (obj2 != null) {
                        Window window = ((Activity) obj2).getWindow();
                        wd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                        View decorView = window.getDecorView();
                        wd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                        View rootView = decorView.getRootView();
                        wd4.a((Object) rootView, "v1");
                        rootView.setDrawingCacheEnabled(true);
                        Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                        wd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                        rootView.setDrawingCacheEnabled(false);
                        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (kc4) null), 3, (Object) null);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                }
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
