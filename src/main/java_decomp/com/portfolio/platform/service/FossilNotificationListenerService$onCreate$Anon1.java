package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.uh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.FossilNotificationListenerService$onCreate$Anon1", f = "FossilNotificationListenerService.kt", l = {83, 86}, m = "invokeSuspend")
public final class FossilNotificationListenerService$onCreate$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FossilNotificationListenerService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationListenerService$onCreate$Anon1(FossilNotificationListenerService fossilNotificationListenerService, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = fossilNotificationListenerService;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        FossilNotificationListenerService$onCreate$Anon1 fossilNotificationListenerService$onCreate$Anon1 = new FossilNotificationListenerService$onCreate$Anon1(this.this$Anon0, kc4);
        fossilNotificationListenerService$onCreate$Anon1.p$ = (lh4) obj;
        return fossilNotificationListenerService$onCreate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FossilNotificationListenerService$onCreate$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (uh4.a(5000, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (PortfolioApp.W.c().C()) {
            FLogger.INSTANCE.getLocal().d(FossilNotificationListenerService.s.a(), "onCreate() - enable Music Control Component.");
            MusicControlComponent b = this.this$Anon0.k;
            FossilNotificationListenerService fossilNotificationListenerService = this.this$Anon0;
            this.L$Anon0 = lh4;
            this.label = 2;
            if (b.a(fossilNotificationListenerService, (kc4<? super cb4>) this) == a) {
                return a;
            }
        }
        return cb4.a;
    }
}
