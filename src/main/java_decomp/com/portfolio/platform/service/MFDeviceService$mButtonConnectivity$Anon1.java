package com.portfolio.platform.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$mButtonConnectivity$Anon1 implements ServiceConnection {
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService a;

    @DexIgnore
    public MFDeviceService$mButtonConnectivity$Anon1(MFDeviceService mFDeviceService) {
        this.a = mFDeviceService;
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        wd4.b(componentName, "name");
        wd4.b(iBinder, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Connection to the BLE has been established");
        this.a.a(true);
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(iBinder, (kc4) null), 3, (Object) null);
        this.a.A();
        this.a.z();
        FossilNotificationBar.c.a(this.a);
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
        wd4.b(componentName, "name");
        this.a.a(false);
    }
}
