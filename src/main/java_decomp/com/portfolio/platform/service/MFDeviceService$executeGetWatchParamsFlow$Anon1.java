package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.WatchParamHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$Anon1", f = "MFDeviceService.kt", l = {668, 670, 672}, m = "invokeSuspend")
public final class MFDeviceService$executeGetWatchParamsFlow$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ float $currentWPVersion;
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$executeGetWatchParamsFlow$Anon1(MFDeviceService mFDeviceService, String str, int i, float f, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = mFDeviceService;
        this.$serial = str;
        this.$majorVersion = i;
        this.$currentWPVersion = f;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$executeGetWatchParamsFlow$Anon1 mFDeviceService$executeGetWatchParamsFlow$Anon1 = new MFDeviceService$executeGetWatchParamsFlow$Anon1(this.this$Anon0, this.$serial, this.$majorVersion, this.$currentWPVersion, kc4);
        mFDeviceService$executeGetWatchParamsFlow$Anon1.p$ = (lh4) obj;
        return mFDeviceService$executeGetWatchParamsFlow$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$executeGetWatchParamsFlow$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            DeviceRepository d = this.this$Anon0.d();
            String str = this.$serial;
            int i2 = this.$majorVersion;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = d.getLatestWatchParamFromServer(str, i2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2 || i == 3) {
            WatchParameterResponse watchParameterResponse = (WatchParameterResponse) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) obj;
        if (watchParameterResponse2 != null) {
            WatchParamHelper t = this.this$Anon0.t();
            String str2 = this.$serial;
            float f = this.$currentWPVersion;
            this.L$Anon0 = lh4;
            this.L$Anon1 = watchParameterResponse2;
            this.label = 2;
            if (t.a(str2, f, watchParameterResponse2, this) == a) {
                return a;
            }
        } else {
            WatchParamHelper t2 = this.this$Anon0.t();
            String str3 = this.$serial;
            float f2 = this.$currentWPVersion;
            this.L$Anon0 = lh4;
            this.L$Anon1 = watchParameterResponse2;
            this.label = 3;
            if (t2.a(str3, f2, this) == a) {
                return a;
            }
        }
        return cb4.a;
    }
}
