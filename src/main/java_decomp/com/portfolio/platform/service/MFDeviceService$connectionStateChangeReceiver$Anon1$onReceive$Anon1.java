package com.portfolio.platform.service;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g42;
import com.fossil.blesdk.obfuscated.jj2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService$connectionStateChangeReceiver$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements g42.b {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1, String str) {
            this.a = mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1;
            this.b = str;
        }

        @DexIgnore
        public void a(Location location, int i) {
            MFDeviceService mFDeviceService = this.a.this$Anon0.a;
            String str = this.b;
            wd4.a((Object) str, "serial");
            mFDeviceService.a(str, location, i);
            if (location != null) {
                g42.a((Context) this.a.this$Anon0.a.c()).b((g42.b) this);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1(MFDeviceService$connectionStateChangeReceiver$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1, Intent intent, Context context, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = mFDeviceService$connectionStateChangeReceiver$Anon1;
        this.$intent = intent;
        this.$context = context;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1 = new MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1(this.this$Anon0, this.$intent, this.$context, kc4);
        mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1.p$ = (lh4) obj;
        return mFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$connectionStateChangeReceiver$Anon1$onReceive$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            String stringExtra = this.$intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = this.$intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = MFDeviceService.U.b();
            local.d(b, "---Inside .connectionStateChangeReceiver " + stringExtra + " status " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_ON.ordinal()) {
                DeviceHelper e = DeviceHelper.o.e();
                wd4.a((Object) stringExtra, "serial");
                MisfitDeviceProfile a2 = e.a(stringExtra);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String b2 = MFDeviceService.U.b();
                local2.d(b2, "---Inside .connectionStateChangeReceiver " + stringExtra + " deviceProfile=" + a2);
                this.this$Anon0.a.a(a2, stringExtra, true);
                if (!TextUtils.isEmpty(stringExtra) && wd4.a((Object) stringExtra, (Object) PortfolioApp.W.c().e())) {
                    this.this$Anon0.a.x();
                }
                this.this$Anon0.a.z();
                if (((MusicControlComponent) MusicControlComponent.o.a(this.$context)).b(stringExtra)) {
                    ((MusicControlComponent) MusicControlComponent.o.a(this.$context)).b();
                }
            } else if (intExtra == ConnectionStateChange.GATT_OFF.ordinal() && !TextUtils.isEmpty(stringExtra) && wd4.a((Object) stringExtra, (Object) PortfolioApp.W.c().e())) {
                this.this$Anon0.a.y();
            }
            PortfolioApp.W.a((Object) new jj2(stringExtra, intExtra));
            MFDeviceService mFDeviceService = this.this$Anon0.a;
            wd4.a((Object) stringExtra, "serial");
            mFDeviceService.c(stringExtra);
            if (this.this$Anon0.a.m().F()) {
                FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Collect location data only user enable locate feature");
                g42.a((Context) this.this$Anon0.a).a((g42.b) new a(this, stringExtra));
            }
            FossilNotificationBar.c.a(this.this$Anon0.a);
            sc.a((Context) this.this$Anon0.a.c()).a(this.$intent);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
