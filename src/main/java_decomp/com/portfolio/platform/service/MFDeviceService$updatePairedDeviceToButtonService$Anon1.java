package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$updatePairedDeviceToButtonService$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updatePairedDeviceToButtonService$Anon1(MFDeviceService mFDeviceService, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = mFDeviceService;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$updatePairedDeviceToButtonService$Anon1 mFDeviceService$updatePairedDeviceToButtonService$Anon1 = new MFDeviceService$updatePairedDeviceToButtonService$Anon1(this.this$Anon0, kc4);
        mFDeviceService$updatePairedDeviceToButtonService$Anon1.p$ = (lh4) obj;
        return mFDeviceService$updatePairedDeviceToButtonService$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$updatePairedDeviceToButtonService$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            List<Device> allDevice = this.this$Anon0.d().getAllDevice();
            if (!allDevice.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.U.b();
                local.d(b, "Get all device success devicesList=" + allDevice);
                if (this.this$Anon0.r().getCurrentUser() != null) {
                    for (Device next : allDevice) {
                        this.this$Anon0.c().d(next.component1(), next.component2());
                    }
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
