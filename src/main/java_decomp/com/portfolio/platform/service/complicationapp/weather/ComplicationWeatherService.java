package com.portfolio.platform.service.complicationapp.weather;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kp2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mp2;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tl2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.enums.SyncErrorCode;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.enums.WeatherCondition;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.usecase.GetWeather;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationWeatherService extends kp2 {
    @DexIgnore
    public /* final */ int k; // = 1800000;
    @DexIgnore
    public GetWeather l;
    @DexIgnore
    public k62 m;
    @DexIgnore
    public fn2 n;
    @DexIgnore
    public PortfolioApp o;
    @DexIgnore
    public CustomizeRealDataRepository p;
    @DexIgnore
    public UserRepository q;
    @DexIgnore
    public String r;
    @DexIgnore
    public WeatherSettings s;
    @DexIgnore
    public Weather t;
    @DexIgnore
    public a<String> u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.d<GetWeather.d, GetWeather.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService a;

        @DexIgnore
        public b(ComplicationWeatherService complicationWeatherService) {
            this.a = complicationWeatherService;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetWeather.d dVar) {
            wd4.b(dVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(kp2.j.a(), "getWeather - onSuccess");
            this.a.a(dVar.a());
            if (this.a.j() != null) {
                this.a.i().s(new Gson().a((Object) this.a.k(), (Type) WeatherSettings.class));
                ComplicationWeatherService complicationWeatherService = this.a;
                WeatherSettings k = complicationWeatherService.k();
                if (k != null) {
                    ComplicationWeatherService.a(complicationWeatherService, k, false, 2, (Object) null);
                    this.a.a();
                    return;
                }
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public void a(GetWeather.b bVar) {
            wd4.b(bVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(kp2.j.a(), "getWeather - onError");
            this.a.a(bVar.a());
            this.a.a();
        }
    }

    @DexIgnore
    public final void c(LocationSource.ErrorState errorState) {
        a<String> aVar = this.u;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (wd4.a((Object) b2, (Object) "TEMPERATURE")) {
                vl2 c = AnalyticsHelper.f.c("weather");
                if (c != null) {
                    be4 be4 = be4.a;
                    Object[] objArr = {"weather"};
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    tl2 a2 = AnalyticsHelper.f.a(format);
                    a2.a("error_code", b(errorState));
                    c.a(a2);
                    return;
                }
                return;
            }
            vl2 c2 = AnalyticsHelper.f.c("chance-of-rain");
            if (c2 != null) {
                be4 be42 = be4.a;
                Object[] objArr2 = {AnalyticsHelper.f.d("chance-of-rain")};
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(objArr2, objArr2.length));
                wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                tl2 a3 = AnalyticsHelper.f.a(format2);
                a3.a("error_code", b(errorState));
                c2.a(a3);
                return;
            }
            return;
        }
        wd4.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository g() {
        CustomizeRealDataRepository customizeRealDataRepository = this.p;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        wd4.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp h() {
        PortfolioApp portfolioApp = this.o;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wd4.d("mPortfolioApp");
        throw null;
    }

    @DexIgnore
    public final fn2 i() {
        fn2 fn2 = this.n;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final Weather j() {
        return this.t;
    }

    @DexIgnore
    public final WeatherSettings k() {
        return this.s;
    }

    @DexIgnore
    public final void l() {
        k62 k62 = this.m;
        if (k62 != null) {
            GetWeather getWeather = this.l;
            if (getWeather != null) {
                WeatherSettings weatherSettings = this.s;
                if (weatherSettings != null) {
                    LatLng latLng = weatherSettings.getLatLng();
                    WeatherSettings weatherSettings2 = this.s;
                    if (weatherSettings2 != null) {
                        k62.a(getWeather, new GetWeather.c(latLng, weatherSettings2.getTempUnit()), new b(this));
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mGetWeather");
                throw null;
            }
        } else {
            wd4.d("mUseCaseHandler");
            throw null;
        }
    }

    @DexIgnore
    public final ri4 m() {
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new ComplicationWeatherService$getWeatherBaseOnLocation$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        wd4.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(kp2.j.a(), "onCreate");
        PortfolioApp.W.c().g().a(this);
        this.r = PortfolioApp.W.c().e();
        this.u = new a<>();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(kp2.j.a(), "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        wd4.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(kp2.j.a(), "onStartCommand");
        this.e = 3001;
        super.d();
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return 2;
        }
        String string = extras.getString("action");
        if (string == null) {
            return 2;
        }
        a<String> aVar = this.u;
        if (aVar == null) {
            wd4.d("mWeatherTasks");
            throw null;
        } else if (aVar.a()) {
            a<String> aVar2 = this.u;
            if (aVar2 != null) {
                aVar2.a(string);
                fn2 fn2 = this.n;
                if (fn2 != null) {
                    String l2 = fn2.l();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = kp2.j.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Inside SteamingAction .run - WEATHER_ACTION json ");
                    if (l2 != null) {
                        sb.append(l2);
                        local.d(a2, sb.toString());
                        this.s = (WeatherSettings) new Gson().a(l2, WeatherSettings.class);
                        m();
                        return 2;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mSharedPreferencesManager");
                throw null;
            }
            wd4.d("mWeatherTasks");
            throw null;
        } else {
            a<String> aVar3 = this.u;
            if (aVar3 != null) {
                aVar3.a(string);
                return 2;
            }
            wd4.d("mWeatherTasks");
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ HashSet<T> a; // = new HashSet<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final boolean a() {
            boolean isEmpty;
            synchronized (this.b) {
                isEmpty = this.a.isEmpty();
            }
            return isEmpty;
        }

        @DexIgnore
        public final T b() {
            T next;
            synchronized (this.b) {
                Iterator<T> it = this.a.iterator();
                wd4.a((Object) it, "hashSet.iterator()");
                next = it.hasNext() ? it.next() : null;
            }
            return next;
        }
    }

    @DexIgnore
    public final void a(Weather weather) {
        this.t = weather;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0123  */
    public final void b(WeatherSettings weatherSettings, boolean z) {
        Pair pair;
        int i;
        WeatherComplicationAppInfo.TemperatureUnit temperatureUnit;
        String str;
        UserRepository userRepository = this.q;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                Unit temperatureUnit2 = currentUser.getTemperatureUnit();
                if (temperatureUnit2 != null) {
                    int i2 = mp2.a[temperatureUnit2.ordinal()];
                    if (i2 == 1) {
                        WeatherSettings.TEMP_UNIT temp_unit = WeatherSettings.TEMP_UNIT.CELSIUS;
                        pair = new Pair(temp_unit, Float.valueOf(weatherSettings.getTemperatureIn(temp_unit)));
                    } else if (i2 == 2) {
                        WeatherSettings.TEMP_UNIT temp_unit2 = WeatherSettings.TEMP_UNIT.FAHRENHEIT;
                        pair = new Pair(temp_unit2, Float.valueOf(weatherSettings.getTemperatureIn(temp_unit2)));
                    }
                    WeatherSettings.TEMP_UNIT temp_unit3 = (WeatherSettings.TEMP_UNIT) pair.component1();
                    float floatValue = ((Number) pair.component2()).floatValue();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = kp2.j.a();
                    local.d(a2, "showTemperature - forecast=" + weatherSettings.getForecast() + ", temperature=" + floatValue + ", temperatureUnit=" + temp_unit3);
                    WeatherComplicationAppInfo.WeatherCondition a3 = WeatherCondition.Companion.a(weatherSettings.getForecast());
                    i = mp2.b[temp_unit3.ordinal()];
                    if (i != 1) {
                        temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.C;
                    } else if (i == 2) {
                        temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.F;
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    WeatherComplicationAppInfo weatherComplicationAppInfo = new WeatherComplicationAppInfo(floatValue, temperatureUnit, a3, 3600);
                    PortfolioApp c = PortfolioApp.W.c();
                    str = this.r;
                    if (str == null) {
                        c.a((DeviceAppResponse) weatherComplicationAppInfo, str);
                        vl2 c2 = AnalyticsHelper.f.c("weather");
                        if (c2 != null) {
                            c2.a(z, "");
                        }
                        AnalyticsHelper.f.e("weather");
                        CustomizeRealData customizeRealData = new CustomizeRealData("temperature", String.valueOf(weatherSettings.getTemperatureIn(WeatherSettings.TEMP_UNIT.CELSIUS)));
                        CustomizeRealDataRepository customizeRealDataRepository = this.p;
                        if (customizeRealDataRepository != null) {
                            customizeRealDataRepository.upsertCustomizeRealData(customizeRealData);
                            return;
                        } else {
                            wd4.d("mCustomizeRealDataRepository");
                            throw null;
                        }
                    } else {
                        wd4.d("mSerial");
                        throw null;
                    }
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a4 = kp2.j.a();
                local2.e(a4, "showTemperature - temperature unit is not found, " + currentUser.getTemperatureUnit());
                pair = new Pair(weatherSettings.getTempUnit(), Float.valueOf(weatherSettings.getTemperature()));
                WeatherSettings.TEMP_UNIT temp_unit32 = (WeatherSettings.TEMP_UNIT) pair.component1();
                float floatValue2 = ((Number) pair.component2()).floatValue();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a22 = kp2.j.a();
                local3.d(a22, "showTemperature - forecast=" + weatherSettings.getForecast() + ", temperature=" + floatValue2 + ", temperatureUnit=" + temp_unit32);
                WeatherComplicationAppInfo.WeatherCondition a32 = WeatherCondition.Companion.a(weatherSettings.getForecast());
                i = mp2.b[temp_unit32.ordinal()];
                if (i != 1) {
                }
                WeatherComplicationAppInfo weatherComplicationAppInfo2 = new WeatherComplicationAppInfo(floatValue2, temperatureUnit, a32, 3600);
                PortfolioApp c3 = PortfolioApp.W.c();
                str = this.r;
                if (str == null) {
                }
            } else {
                FLogger.INSTANCE.getLocal().e(kp2.j.a(), "showTemperature - do not send temperature, no user info");
            }
        } else {
            wd4.d("mUserRepository");
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(ComplicationWeatherService complicationWeatherService, WeatherSettings weatherSettings, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        complicationWeatherService.a(weatherSettings, z);
    }

    @DexIgnore
    public final synchronized void a(WeatherSettings weatherSettings, boolean z) {
        wd4.b(weatherSettings, "weatherSettings");
        while (true) {
            a<String> aVar = this.u;
            if (aVar == null) {
                wd4.d("mWeatherTasks");
                throw null;
            } else if (!aVar.a()) {
                a<String> aVar2 = this.u;
                if (aVar2 != null) {
                    String b2 = aVar2.b();
                    if (b2 != null) {
                        if (wd4.a((Object) b2, (Object) "TEMPERATURE")) {
                            b(weatherSettings, z);
                        } else if (wd4.a((Object) b2, (Object) "RAIN_CHANCE")) {
                            a(weatherSettings.getRainProbability(), z);
                        }
                        a<String> aVar3 = this.u;
                        if (aVar3 != null) {
                            aVar3.b(b2);
                        } else {
                            wd4.d("mWeatherTasks");
                            throw null;
                        }
                    }
                } else {
                    wd4.d("mWeatherTasks");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(float f, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = kp2.j.a();
        local.d(a2, "showChanceOfRain - probability=" + f);
        int i = (int) (f * ((float) 100));
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = new ChanceOfRainComplicationAppInfo(i, 3600);
        PortfolioApp c = PortfolioApp.W.c();
        String str = this.r;
        if (str != null) {
            c.a((DeviceAppResponse) chanceOfRainComplicationAppInfo, str);
            vl2 c2 = AnalyticsHelper.f.c("chance-of-rain");
            if (c2 != null) {
                c2.a(z, "");
            }
            AnalyticsHelper.f.e("chance-of-rain");
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ComplicationWeatherService$showChanceOfRain$Anon1(this, i, (kc4) null), 3, (Object) null);
            return;
        }
        wd4.d("mSerial");
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "errorMessage");
        a<String> aVar = this.u;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (wd4.a((Object) b2, (Object) "TEMPERATURE")) {
                vl2 c = AnalyticsHelper.f.c("weather");
                if (c != null) {
                    be4 be4 = be4.a;
                    Object[] objArr = {"weather"};
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    tl2 a2 = AnalyticsHelper.f.a(format);
                    a2.a("error_code", str);
                    c.a(a2);
                    return;
                }
                return;
            }
            vl2 c2 = AnalyticsHelper.f.c("chance-of-rain");
            if (c2 != null) {
                be4 be42 = be4.a;
                Object[] objArr2 = {AnalyticsHelper.f.d("chance-of-rain")};
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(objArr2, objArr2.length));
                wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                tl2 a3 = AnalyticsHelper.f.a(format2);
                a3.a("error_code", str);
                c2.a(a3);
                return;
            }
            return;
        }
        wd4.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final String b(LocationSource.ErrorState errorState) {
        int i = mp2.c[errorState.ordinal()];
        if (i == 1) {
            return SyncErrorCode.LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i == 2) {
            return SyncErrorCode.BACKGROUND_LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i != 3) {
            return SyncErrorCode.UNKNOWN.getCode();
        }
        return SyncErrorCode.LOCATION_SERVICE_DISABLED.getCode();
    }

    @DexIgnore
    public void b() {
        FLogger.INSTANCE.getLocal().d(kp2.j.a(), "forceStop");
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(kp2.j.a(), VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }
}
