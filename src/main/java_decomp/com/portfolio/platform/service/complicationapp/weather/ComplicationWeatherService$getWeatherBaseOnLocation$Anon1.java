package com.portfolio.platform.service.complicationapp.weather;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kp2;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import java.util.Calendar;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$Anon1", f = "ComplicationWeatherService.kt", l = {118}, m = "invokeSuspend")
public final class ComplicationWeatherService$getWeatherBaseOnLocation$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationWeatherService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationWeatherService$getWeatherBaseOnLocation$Anon1(ComplicationWeatherService complicationWeatherService, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = complicationWeatherService;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ComplicationWeatherService$getWeatherBaseOnLocation$Anon1 complicationWeatherService$getWeatherBaseOnLocation$Anon1 = new ComplicationWeatherService$getWeatherBaseOnLocation$Anon1(this.this$Anon0, kc4);
        complicationWeatherService$getWeatherBaseOnLocation$Anon1.p$ = (lh4) obj;
        return complicationWeatherService$getWeatherBaseOnLocation$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationWeatherService$getWeatherBaseOnLocation$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.k() != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = kp2.j.a();
                StringBuilder sb = new StringBuilder();
                sb.append("getWeatherBaseOnLocation - location=");
                WeatherSettings k = this.this$Anon0.k();
                if (k != null) {
                    sb.append(k.getLocation());
                    local.d(a2, sb.toString());
                    WeatherSettings k2 = this.this$Anon0.k();
                    if (k2 == null) {
                        wd4.a();
                        throw null;
                    } else if (!k2.isUseCurrentLocation()) {
                        this.this$Anon0.l();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        wd4.a((Object) instance, "Calendar.getInstance()");
                        long timeInMillis = instance.getTimeInMillis();
                        WeatherSettings k3 = this.this$Anon0.k();
                        if (k3 == null) {
                            wd4.a();
                            throw null;
                        } else if (timeInMillis - k3.getUpdatedAt() < ((long) this.this$Anon0.k)) {
                            ComplicationWeatherService complicationWeatherService = this.this$Anon0;
                            WeatherSettings k4 = complicationWeatherService.k();
                            if (k4 != null) {
                                complicationWeatherService.a(k4, true);
                                this.this$Anon0.a();
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            LocationSource e = this.this$Anon0.e();
                            PortfolioApp h = this.this$Anon0.h();
                            this.L$Anon0 = lh4;
                            this.label = 1;
                            obj = e.getLocation(h, false, this);
                            if (obj == a) {
                                return a;
                            }
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            return cb4.a;
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        LocationSource.Result result = (LocationSource.Result) obj;
        if (result.getErrorState() == LocationSource.ErrorState.SUCCESS) {
            Location location = result.getLocation();
            WeatherSettings k5 = this.this$Anon0.k();
            if (k5 == null) {
                wd4.a();
                throw null;
            } else if (location != null) {
                k5.setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                this.this$Anon0.l();
            } else {
                wd4.a();
                throw null;
            }
        } else {
            this.this$Anon0.a(result.getErrorState());
            this.this$Anon0.c(result.getErrorState());
            FLogger.INSTANCE.getLocal().d(kp2.j.a(), "getWeatherBaseOnLocation - using current location but location is null, stop service.");
            this.this$Anon0.a();
        }
        return cb4.a;
    }
}
