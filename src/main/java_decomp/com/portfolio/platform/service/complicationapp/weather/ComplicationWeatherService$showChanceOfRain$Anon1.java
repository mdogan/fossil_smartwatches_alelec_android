package com.portfolio.platform.service.complicationapp.weather;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.CustomizeRealData;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$Anon1", f = "ComplicationWeatherService.kt", l = {}, m = "invokeSuspend")
public final class ComplicationWeatherService$showChanceOfRain$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $rainProbabilityPercent;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationWeatherService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationWeatherService$showChanceOfRain$Anon1(ComplicationWeatherService complicationWeatherService, int i, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = complicationWeatherService;
        this.$rainProbabilityPercent = i;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ComplicationWeatherService$showChanceOfRain$Anon1 complicationWeatherService$showChanceOfRain$Anon1 = new ComplicationWeatherService$showChanceOfRain$Anon1(this.this$Anon0, this.$rainProbabilityPercent, kc4);
        complicationWeatherService$showChanceOfRain$Anon1.p$ = (lh4) obj;
        return complicationWeatherService$showChanceOfRain$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationWeatherService$showChanceOfRain$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.g().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
