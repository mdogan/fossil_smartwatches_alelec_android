package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$Anon1", f = "MusicControlComponent.kt", l = {872, 558, 565, 569}, m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.OldVersionMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1(MusicControlComponent.OldVersionMusicController oldVersionMusicController, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = oldVersionMusicController;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1 = new MusicControlComponent$OldVersionMusicController$connect$Anon1(this.this$Anon0, kc4);
        musicControlComponent$OldVersionMusicController$connect$Anon1.p$ = (lh4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e1 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0120 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0141 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0148 A[Catch:{ all -> 0x0065 }] */
    public final Object invokeSuspend(Object obj) {
        pl4 pl4;
        pl4 pl42;
        lh4 lh4;
        sh4 sh4;
        MediaBrowserCompat mediaBrowserCompat;
        MusicControlComponent.OldVersionMusicController oldVersionMusicController;
        MusicControlComponent.OldVersionMusicController oldVersionMusicController2;
        MediaBrowserCompat i;
        lh4 lh42;
        pl4 pl43;
        Object a = oc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            za4.a(obj);
            lh42 = this.p$;
            pl43 = ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.W.c())).f();
            this.L$Anon0 = lh42;
            this.L$Anon1 = pl43;
            this.label = 1;
            if (pl43.a((Object) null, this) == a) {
                return a;
            }
        } else if (i2 == 1) {
            pl43 = (pl4) this.L$Anon1;
            za4.a(obj);
            lh42 = (lh4) this.L$Anon0;
        } else if (i2 == 2) {
            oldVersionMusicController2 = (MusicControlComponent.OldVersionMusicController) this.L$Anon3;
            sh4 = (sh4) this.L$Anon2;
            pl4 = (pl4) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            oldVersionMusicController2.a((MediaBrowserCompat) obj);
            i = this.this$Anon0.i();
            if (i != null) {
                this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                pl42 = pl4;
                cb4 cb4 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            } else if (!i.d()) {
                this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                cb4 cb42 = cb4.a;
                pl4.a((Object) null);
                return cb42;
            } else {
                oldVersionMusicController = this.this$Anon0;
                bj4 c = zh4.c();
                MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2(i, (kc4) null, this, lh4);
                this.L$Anon0 = lh4;
                this.L$Anon1 = pl4;
                this.L$Anon2 = sh4;
                this.L$Anon3 = i;
                this.L$Anon4 = oldVersionMusicController;
                this.label = 3;
                Object a2 = kg4.a(c, musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2, this);
                if (a2 == a) {
                    return a;
                }
                Object obj2 = a2;
                mediaBrowserCompat = i;
                obj = obj2;
                oldVersionMusicController.a((MediaControllerCompat) obj);
                if (this.this$Anon0.j() == null) {
                }
                pl42 = pl4;
                cb4 cb43 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            }
        } else if (i2 == 3) {
            oldVersionMusicController = (MusicControlComponent.OldVersionMusicController) this.L$Anon4;
            mediaBrowserCompat = (MediaBrowserCompat) this.L$Anon3;
            sh4 = (sh4) this.L$Anon2;
            pl4 = (pl4) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                oldVersionMusicController.a((MediaControllerCompat) obj);
                if (this.this$Anon0.j() == null) {
                    this.this$Anon0.a(this.this$Anon0);
                    bj4 c2 = zh4.c();
                    MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3((kc4) null, this, lh4);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = pl4;
                    this.L$Anon2 = sh4;
                    this.L$Anon3 = mediaBrowserCompat;
                    this.label = 4;
                    if (kg4.a(c2, musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3, this) == a) {
                        return a;
                    }
                } else {
                    this.this$Anon0.a((MusicControlComponent.OldVersionMusicController) null);
                }
                pl42 = pl4;
                cb4 cb432 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 4) {
            MediaBrowserCompat mediaBrowserCompat2 = (MediaBrowserCompat) this.L$Anon3;
            sh4 sh42 = (sh4) this.L$Anon2;
            pl42 = (pl4) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                pl4 = pl42;
                pl42 = pl4;
                cb4 cb4322 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            } catch (Throwable th2) {
                th = th2;
                pl4 = pl42;
                pl4.a((Object) null);
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "connecting");
            sh4 a3 = mg4.a(lh42, zh4.c(), (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((kc4) null, this, lh42), 2, (Object) null);
            MusicControlComponent.OldVersionMusicController oldVersionMusicController3 = this.this$Anon0;
            this.L$Anon0 = lh42;
            this.L$Anon1 = pl43;
            this.L$Anon2 = a3;
            this.L$Anon3 = oldVersionMusicController3;
            this.label = 2;
            Object a4 = a3.a(this);
            if (a4 == a) {
                return a;
            }
            MusicControlComponent.OldVersionMusicController oldVersionMusicController4 = oldVersionMusicController3;
            lh4 = lh42;
            obj = a4;
            sh4 = a3;
            pl4 = pl43;
            oldVersionMusicController2 = oldVersionMusicController4;
            oldVersionMusicController2.a((MediaBrowserCompat) obj);
            i = this.this$Anon0.i();
            if (i != null) {
            }
        } catch (Throwable th3) {
            th = th3;
            pl4 = pl43;
            pl4.a((Object) null);
            throw th;
        }
    }
}
