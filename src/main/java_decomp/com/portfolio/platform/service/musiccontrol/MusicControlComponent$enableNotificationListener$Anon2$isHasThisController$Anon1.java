package com.portfolio.platform.service.musiccontrol;

import android.media.session.MediaController;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$enableNotificationListener$Anon2$isHasThisController$Anon1 extends Lambda implements jd4<MusicControlComponent.a, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ MediaController $activeMediaController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$Anon2$isHasThisController$Anon1(MediaController mediaController) {
        super(1);
        this.$activeMediaController = mediaController;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((MusicControlComponent.a) obj));
    }

    @DexIgnore
    public final boolean invoke(MusicControlComponent.a aVar) {
        wd4.b(aVar, "musicController");
        String c = aVar.c();
        MediaController mediaController = this.$activeMediaController;
        wd4.a((Object) mediaController, "activeMediaController");
        return wd4.a((Object) c, (Object) mediaController.getPackageName());
    }
}
