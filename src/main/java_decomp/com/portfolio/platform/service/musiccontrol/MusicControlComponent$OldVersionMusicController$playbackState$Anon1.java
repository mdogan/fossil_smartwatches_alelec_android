package com.portfolio.platform.service.musiccontrol;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$Anon1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$playbackState$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $currentState;
    @DexIgnore
    public /* final */ /* synthetic */ int $oldState;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.OldVersionMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$playbackState$Anon1(MusicControlComponent.OldVersionMusicController oldVersionMusicController, int i, int i2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = oldVersionMusicController;
        this.$oldState = i;
        this.$currentState = i2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$OldVersionMusicController$playbackState$Anon1 musicControlComponent$OldVersionMusicController$playbackState$Anon1 = new MusicControlComponent$OldVersionMusicController$playbackState$Anon1(this.this$Anon0, this.$oldState, this.$currentState, kc4);
        musicControlComponent$OldVersionMusicController$playbackState$Anon1.p$ = (lh4) obj;
        return musicControlComponent$OldVersionMusicController$playbackState$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$playbackState$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.this$Anon0;
            oldVersionMusicController.a(this.$oldState, this.$currentState, (MusicControlComponent.a) oldVersionMusicController);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
