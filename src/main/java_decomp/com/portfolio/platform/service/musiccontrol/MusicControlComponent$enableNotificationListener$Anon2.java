package com.portfolio.platform.service.musiccontrol;

import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$enableNotificationListener$Anon2 implements MediaSessionManager.OnActiveSessionsChangedListener {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent a;

    @DexIgnore
    public MusicControlComponent$enableNotificationListener$Anon2(MusicControlComponent musicControlComponent) {
        this.a = musicControlComponent;
    }

    @DexIgnore
    public final void onActiveSessionsChanged(List<MediaController> list) {
        if (list != null && (!list.isEmpty())) {
            for (MediaController next : list) {
                if (!(this.a.e().a(new MusicControlComponent$enableNotificationListener$Anon2$isHasThisController$Anon1(next)) != null)) {
                    wd4.a((Object) next, "activeMediaController");
                    MusicControlComponent musicControlComponent = this.a;
                    String packageName = next.getPackageName();
                    wd4.a((Object) packageName, "activeMediaController.packageName");
                    a aVar = new a(this, next, next, musicControlComponent.a(packageName));
                    if (this.a.e().a(aVar)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = MusicControlComponent.o.a();
                        local.d(a2, ".NewNotificationMusicController is added to list controller, packageName=" + aVar.c());
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends MusicControlComponent.NewNotificationMusicController {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent$enableNotificationListener$Anon2 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(MusicControlComponent$enableNotificationListener$Anon2 musicControlComponent$enableNotificationListener$Anon2, MediaController mediaController, MediaController mediaController2, String str) {
            super(mediaController2, str);
            this.g = musicControlComponent$enableNotificationListener$Anon2;
        }

        @DexIgnore
        public void a(MusicControlComponent.b bVar, MusicControlComponent.b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            super.a(bVar, bVar2);
            this.g.a.a(b());
        }

        @DexIgnore
        public void a(int i, int i2, MusicControlComponent.a aVar) {
            wd4.b(aVar, "controller");
            super.a(i, i2, aVar);
            if (i2 == 3) {
                this.g.a.a(aVar);
            } else if (this.g.a.d() == null) {
                this.g.a.a(aVar);
            }
            this.g.a.a(i2);
        }

        @DexIgnore
        public void a(MusicControlComponent.a aVar) {
            wd4.b(aVar, "controller");
            super.a(aVar);
            this.g.a.e().b(aVar);
            if (wd4.a((Object) aVar, (Object) this.g.a.d())) {
                this.g.a.a((MusicControlComponent.a) null);
            }
        }
    }
}
