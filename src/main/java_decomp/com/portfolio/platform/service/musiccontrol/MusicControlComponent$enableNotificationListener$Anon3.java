package com.portfolio.platform.service.musiccontrol;

import android.content.ComponentName;
import android.media.session.MediaSessionManager;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.service.FossilNotificationListenerService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$Anon3", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$enableNotificationListener$Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ FossilNotificationListenerService $fossilNotificationListenerService;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$Anon3(MusicControlComponent musicControlComponent, FossilNotificationListenerService fossilNotificationListenerService, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = musicControlComponent;
        this.$fossilNotificationListenerService = fossilNotificationListenerService;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$enableNotificationListener$Anon3 musicControlComponent$enableNotificationListener$Anon3 = new MusicControlComponent$enableNotificationListener$Anon3(this.this$Anon0, this.$fossilNotificationListenerService, kc4);
        musicControlComponent$enableNotificationListener$Anon3.p$ = (lh4) obj;
        return musicControlComponent$enableNotificationListener$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$enableNotificationListener$Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            MediaSessionManager.OnActiveSessionsChangedListener b = this.this$Anon0.f;
            if (b == null) {
                return null;
            }
            MediaSessionManager c = this.this$Anon0.a;
            if (c != null) {
                c.addOnActiveSessionsChangedListener(b, new ComponentName(this.$fossilNotificationListenerService, FossilNotificationListenerService.class));
                return cb4.a;
            }
            wd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
