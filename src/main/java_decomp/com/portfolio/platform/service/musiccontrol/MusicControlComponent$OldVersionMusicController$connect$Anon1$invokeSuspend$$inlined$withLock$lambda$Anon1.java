package com.portfolio.platform.service.musiccontrol;

import android.content.Context;
import android.support.v4.media.MediaBrowserCompat;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MediaBrowserCompat>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ lh4 $this_launch$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent$OldVersionMusicController$connect$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(kc4 kc4, MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1, lh4 lh4) {
        super(2, kc4);
        this.this$Anon0 = musicControlComponent$OldVersionMusicController$connect$Anon1;
        this.$this_launch$inlined = lh4;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(kc4, this.this$Anon0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (lh4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.this$Anon0.this$Anon0;
            Context b = oldVersionMusicController.h;
            MediaAppDetails c = this.this$Anon0.this$Anon0.i;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = oldVersionMusicController.a(b, c, (kc4<? super MediaBrowserCompat>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
