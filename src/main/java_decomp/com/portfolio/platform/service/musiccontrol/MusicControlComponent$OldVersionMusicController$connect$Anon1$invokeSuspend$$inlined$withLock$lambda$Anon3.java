package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.session.MediaControllerCompat;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ lh4 $this_launch$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent$OldVersionMusicController$connect$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3(kc4 kc4, MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1, lh4 lh4) {
        super(2, kc4);
        this.this$Anon0 = musicControlComponent$OldVersionMusicController$connect$Anon1;
        this.$this_launch$inlined = lh4;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3(kc4, this.this$Anon0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3.p$ = (lh4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            MediaControllerCompat j = this.this$Anon0.this$Anon0.j();
            if (j != null) {
                j.a(this.this$Anon0.this$Anon0.f());
                return cb4.a;
            }
            wd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
