package com.portfolio.platform.service.musiccontrol;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.RemoteController;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.KeyEvent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.obfuscated.bc;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.qg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rj2;
import com.fossil.blesdk.obfuscated.rl4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.tp2;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.uc4;
import com.fossil.blesdk.obfuscated.up2;
import com.fossil.blesdk.obfuscated.ve4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponseFactory;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ List<String> n; // = ob4.d("org.videolan.vlc");
    @DexIgnore
    public static /* final */ Companion o; // = new Companion((rd4) null);
    @DexIgnore
    public MediaSessionManager a;
    @DexIgnore
    public a b;
    @DexIgnore
    public /* final */ pl4 c; // = rl4.a(false, 1, (Object) null);
    @DexIgnore
    public d<a> d; // = new d<>();
    @DexIgnore
    public /* final */ AudioManager e;
    @DexIgnore
    public MediaSessionManager.OnActiveSessionsChangedListener f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ MutableLiveData<DianaPresetRepository> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ LiveData<DianaPreset> j;
    @DexIgnore
    public /* final */ dc<DianaPreset> k;
    @DexIgnore
    public /* final */ Context l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$Anon1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(MusicControlComponent musicControlComponent, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = musicControlComponent;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                MusicControlComponent musicControlComponent = this.this$Anon0;
                PackageManager packageManager = musicControlComponent.c().getPackageManager();
                wd4.a((Object) packageManager, "this@MusicControlComponent.context.packageManager");
                Resources resources = this.this$Anon0.c().getResources();
                wd4.a((Object) resources, "this@MusicControlComponent.context.resources");
                ArrayList unused = musicControlComponent.a(packageManager, resources);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion extends up2<MusicControlComponent, Context> {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class Anon1 extends FunctionReference implements jd4<Context, MusicControlComponent> {
            @DexIgnore
            public static /* final */ Anon1 INSTANCE; // = new Anon1();

            @DexIgnore
            public Anon1() {
                super(1);
            }

            @DexIgnore
            public final String getName() {
                return "<init>";
            }

            @DexIgnore
            public final ve4 getOwner() {
                return yd4.a(MusicControlComponent.class);
            }

            @DexIgnore
            public final String getSignature() {
                return "<init>(Landroid/content/Context;)V";
            }

            @DexIgnore
            public final MusicControlComponent invoke(Context context) {
                wd4.b(context, "p1");
                return new MusicControlComponent(context);
            }
        }

        @DexIgnore
        public Companion() {
            super(Anon1.INSTANCE);
        }

        @DexIgnore
        public final String a() {
            return MusicControlComponent.m;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class NewNotificationMusicController extends a {
        @DexIgnore
        public /* final */ MediaController.Callback c;
        @DexIgnore
        public int d;
        @DexIgnore
        public b e;
        @DexIgnore
        public /* final */ MediaController f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends MediaController.Callback {
            @DexIgnore
            public /* final */ /* synthetic */ NewNotificationMusicController a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;

            @DexIgnore
            public a(NewNotificationMusicController newNotificationMusicController, String str) {
                this.a = newNotificationMusicController;
                this.b = str;
            }

            @DexIgnore
            public void onMetadataChanged(MediaMetadata mediaMetadata) {
                super.onMetadataChanged(mediaMetadata);
                if (mediaMetadata != null) {
                    String a2 = rj2.a(mediaMetadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    String a3 = rj2.a(mediaMetadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    String a4 = rj2.a(mediaMetadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a5 = MusicControlComponent.o.a();
                    local.d(a5, ".onMetadataChanged callback, title=" + a2 + ", artist=" + a3);
                    b bVar = new b(this.a.c(), this.b, a2, a3, a4);
                    if (!wd4.a((Object) bVar, (Object) this.a.e())) {
                        b e = this.a.e();
                        this.a.a(bVar);
                        this.a.a(e, bVar);
                    }
                }
            }

            @DexIgnore
            public void onPlaybackStateChanged(PlaybackState playbackState) {
                super.onPlaybackStateChanged(playbackState);
                int state = playbackState != null ? playbackState.getState() : 0;
                if (state != this.a.f()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = MusicControlComponent.o.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append(".onPlaybackStateChanged, PlaybackState = ");
                    sb.append(playbackState != null ? Integer.valueOf(playbackState.getState()) : null);
                    local.d(a2, sb.toString());
                    int f = this.a.f();
                    this.a.a(state);
                    NewNotificationMusicController newNotificationMusicController = this.a;
                    newNotificationMusicController.a(f, state, newNotificationMusicController);
                }
            }

            @DexIgnore
            public void onSessionDestroyed() {
                super.onSessionDestroyed();
                NewNotificationMusicController newNotificationMusicController = this.a;
                newNotificationMusicController.a((a) newNotificationMusicController);
            }

            @DexIgnore
            public void onSessionEvent(String str, Bundle bundle) {
                wd4.b(str, Constants.EVENT);
                super.onSessionEvent(str, bundle);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = MusicControlComponent.o.a();
                local.d(a2, ".onSessionEvent callback, event=" + str + ", extras=" + bundle);
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public NewNotificationMusicController(MediaController mediaController, String str) {
            super(str, r0);
            wd4.b(mediaController, "mMediaController");
            wd4.b(str, "appName");
            String packageName = mediaController.getPackageName();
            wd4.a((Object) packageName, "mMediaController.packageName");
            this.f = mediaController;
            this.c = new a(this, str);
            this.e = new b(c(), str, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            this.f.registerCallback(this.c);
        }

        @DexIgnore
        public final void a(int i) {
            this.d = i;
        }

        @DexIgnore
        public b b() {
            String str;
            String str2;
            String str3;
            MediaMetadata metadata = this.f.getMetadata();
            if (metadata != null) {
                String a2 = rj2.a(metadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a3 = rj2.a(metadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                str = rj2.a(metadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                str3 = a2;
                str2 = a3;
            } else {
                str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                str2 = str3;
                str = str2;
            }
            b bVar = new b(c(), a(), str3, str2, str);
            if (!wd4.a((Object) bVar, (Object) this.e)) {
                ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$NewNotificationMusicController$metadata$Anon2(this, this.e, bVar, (kc4) null), 3, (Object) null);
                this.e = bVar;
            }
            return this.e;
        }

        @DexIgnore
        public int d() {
            PlaybackState playbackState = this.f.getPlaybackState();
            Integer valueOf = playbackState != null ? Integer.valueOf(playbackState.getState()) : null;
            int i = this.d;
            if ((valueOf == null || valueOf.intValue() != i) && valueOf != null) {
                int i2 = this.d;
                this.d = valueOf.intValue();
                ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$NewNotificationMusicController$playbackState$Anon1(this, i2, valueOf, (kc4) null), 3, (Object) null);
            }
            return this.d;
        }

        @DexIgnore
        public final b e() {
            return this.e;
        }

        @DexIgnore
        public final int f() {
            return this.d;
        }

        @DexIgnore
        public final void a(b bVar) {
            wd4.b(bVar, "<set-?>");
            this.e = bVar;
        }

        @DexIgnore
        public void a(int i, int i2, a aVar) {
            wd4.b(aVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i + ", newState=" + i2);
        }

        @DexIgnore
        public void a(b bVar, b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onMetadataChanged(), oldMetadata=" + bVar + ", newMetadata=" + bVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wd4.b(keyEvent, "keyEvent");
            return this.f.dispatchMediaButtonEvent(keyEvent);
        }

        @DexIgnore
        public void a(a aVar) {
            wd4.b(aVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, ".onSessionDestroyed, packageName=" + c());
            this.f.unregisterCallback(this.c);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class OldVersionMusicController extends a {
        @DexIgnore
        public /* final */ MediaControllerCompat.a c;
        @DexIgnore
        public MediaBrowserCompat d;
        @DexIgnore
        public MediaControllerCompat e;
        @DexIgnore
        public int f;
        @DexIgnore
        public b g; // = new b("", "", AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
        @DexIgnore
        public /* final */ Context h;
        @DexIgnore
        public /* final */ MediaAppDetails i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends MediaBrowserCompat.b {
            @DexIgnore
            public /* final */ /* synthetic */ pg4 c;
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef d;

            @DexIgnore
            public a(pg4 pg4, Ref$ObjectRef ref$ObjectRef) {
                this.c = pg4;
                this.d = ref$ObjectRef;
            }

            @DexIgnore
            public void a() {
                super.a();
                if (this.c.isActive()) {
                    pg4 pg4 = this.c;
                    Result.a aVar = Result.Companion;
                    pg4.resumeWith(Result.m3constructorimpl((MediaBrowserCompat) this.d.element));
                }
            }

            @DexIgnore
            public void b() {
                super.b();
                if (this.c.isActive()) {
                    pg4 pg4 = this.c;
                    Result.a aVar = Result.Companion;
                    pg4.resumeWith(Result.m3constructorimpl((Object) null));
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public OldVersionMusicController(Context context, MediaAppDetails mediaAppDetails, String str) {
            super(str, r0);
            wd4.b(context, "context");
            wd4.b(mediaAppDetails, "mMediaAppDetails");
            wd4.b(str, "appName");
            String str2 = mediaAppDetails.packageName;
            wd4.a((Object) str2, "mMediaAppDetails.packageName");
            this.h = context;
            this.i = mediaAppDetails;
            this.c = new MusicControlComponent$OldVersionMusicController$callback$Anon1(this, str);
            e();
        }

        @DexIgnore
        public abstract void a(OldVersionMusicController oldVersionMusicController);

        @DexIgnore
        public int d() {
            MediaControllerCompat mediaControllerCompat = this.e;
            int a2 = a(mediaControllerCompat != null ? mediaControllerCompat.b() : null);
            int i2 = this.f;
            if (a2 != i2) {
                this.f = a2;
                ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$playbackState$Anon1(this, i2, a2, (kc4) null), 3, (Object) null);
            }
            return this.f;
        }

        @DexIgnore
        public final ri4 e() {
            return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$connect$Anon1(this, (kc4) null), 3, (Object) null);
        }

        @DexIgnore
        public final MediaControllerCompat.a f() {
            return this.c;
        }

        @DexIgnore
        public final b g() {
            return this.g;
        }

        @DexIgnore
        public final int h() {
            return this.f;
        }

        @DexIgnore
        public final MediaBrowserCompat i() {
            return this.d;
        }

        @DexIgnore
        public final MediaControllerCompat j() {
            return this.e;
        }

        @DexIgnore
        public final void a(MediaBrowserCompat mediaBrowserCompat) {
            this.d = mediaBrowserCompat;
        }

        @DexIgnore
        public b b() {
            String str;
            String str2;
            String str3;
            MediaControllerCompat mediaControllerCompat = this.e;
            MediaMetadataCompat a2 = mediaControllerCompat != null ? mediaControllerCompat.a() : null;
            if (a2 != null) {
                String a3 = rj2.a(a2.a("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a4 = rj2.a(a2.a("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                str = rj2.a(a2.a("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                str3 = a3;
                str2 = a4;
            } else {
                str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                str2 = str3;
                str = str2;
            }
            b bVar = new b(c(), a(), str3, str2, str);
            if (!wd4.a((Object) bVar, (Object) this.g)) {
                ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MusicControlComponent$OldVersionMusicController$metadata$Anon2(this, this.g, bVar, (kc4) null), 3, (Object) null);
                this.g = bVar;
            }
            return this.g;
        }

        @DexIgnore
        public final void a(MediaControllerCompat mediaControllerCompat) {
            this.e = mediaControllerCompat;
        }

        @DexIgnore
        public final void a(int i2) {
            this.f = i2;
        }

        @DexIgnore
        public final void a(b bVar) {
            wd4.b(bVar, "<set-?>");
            this.g = bVar;
        }

        @DexIgnore
        public final int a(PlaybackStateCompat playbackStateCompat) {
            if (playbackStateCompat == null) {
                return 0;
            }
            switch (playbackStateCompat.getState()) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                case 6:
                    return 6;
                case 7:
                    return 7;
                case 8:
                    return 8;
                case 9:
                    return 9;
                case 10:
                    return 10;
                case 11:
                    return 11;
                default:
                    return 0;
            }
        }

        @DexIgnore
        public void a(int i2, int i3, a aVar) {
            wd4.b(aVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i2 + ", newState=" + i3);
        }

        @DexIgnore
        public void a(b bVar, b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onMetadataChanged(), oldMetadata=" + bVar + ", newMetadata=" + bVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wd4.b(keyEvent, "keyEvent");
            MediaControllerCompat mediaControllerCompat = this.e;
            if (mediaControllerCompat != null) {
                return mediaControllerCompat.a(keyEvent);
            }
            return false;
        }

        @DexIgnore
        public void a(a aVar) {
            wd4.b(aVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, ".onSessionDestroyed(), packageName=" + c());
        }

        @DexIgnore
        public final /* synthetic */ Object a(Context context, MediaAppDetails mediaAppDetails, kc4<? super MediaBrowserCompat> kc4) {
            qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            ref$ObjectRef.element = new MediaBrowserCompat(context, mediaAppDetails.componentName, new a(qg4, ref$ObjectRef), (Bundle) null);
            ((MediaBrowserCompat) ref$ObjectRef.element).a();
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), Constants.IF_VERIFY_ENABLE_CONNECTED);
            Object e2 = qg4.e();
            if (e2 == oc4.a()) {
                uc4.c(kc4);
            }
            return e2;
        }

        @DexIgnore
        public final MediaControllerCompat b(MediaBrowserCompat mediaBrowserCompat) {
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "setupMediaController");
            if (!mediaBrowserCompat.d()) {
                return null;
            }
            try {
                MediaSessionCompat.Token c2 = mediaBrowserCompat.c();
                wd4.a((Object) c2, "browser.sessionToken");
                MediaControllerCompat mediaControllerCompat = new MediaControllerCompat(this.h, c2);
                FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "MediaControllerCompat created");
                return mediaControllerCompat;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = MusicControlComponent.o.a();
                local.e(a2, "Failed to create MediaController from session token " + e2);
                return null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$a$a")
        /* renamed from: com.portfolio.platform.service.musiccontrol.MusicControlComponent$a$a  reason: collision with other inner class name */
        public static final class C0126a {
            @DexIgnore
            public C0126a() {
            }

            @DexIgnore
            public /* synthetic */ C0126a(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            new C0126a((rd4) null);
        }
        */

        @DexIgnore
        public a(String str, String str2) {
            wd4.b(str, "appName");
            wd4.b(str2, "packageName");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public abstract boolean a(KeyEvent keyEvent);

        @DexIgnore
        public abstract b b();

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public abstract int d();

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof a)) {
                return false;
            }
            return wd4.a((Object) this.b, (Object) ((a) obj).b);
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;

        @DexIgnore
        public b(String str, String str2, String str3, String str4, String str5) {
            wd4.b(str, "packageName");
            wd4.b(str2, "appName");
            wd4.b(str3, "title");
            wd4.b(str4, "artist");
            wd4.b(str5, "album");
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = str5;
        }

        @DexIgnore
        public final String a() {
            return this.e;
        }

        @DexIgnore
        public final String b() {
            return this.d;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final String d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wd4.a((Object) this.a, (Object) bVar.a) && wd4.a((Object) this.b, (Object) bVar.b) && wd4.a((Object) this.c, (Object) bVar.c) && wd4.a((Object) this.d, (Object) bVar.d) && wd4.a((Object) this.e, (Object) bVar.e);
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.d;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.e;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode4 + i;
        }

        @DexIgnore
        public String toString() {
            return "MusicMetadata[packageName=" + this.a + ", appName=" + this.b + ", title=" + this.c + ", artist=" + this.d + ", album=" + this.e + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c extends a {
        @DexIgnore
        public /* final */ RemoteController.OnClientUpdateListener c;
        @DexIgnore
        public /* final */ RemoteController d;
        @DexIgnore
        public int e;
        @DexIgnore
        public b f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Context context, String str) {
            super(str, "All apps");
            wd4.b(context, "context");
            wd4.b(str, "appName");
            this.c = new a(this, str);
            this.f = new b(c(), str, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            this.d = new RemoteController(context, this.c);
        }

        @DexIgnore
        public final int a(int i) {
            switch (i) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                case 6:
                    return 10;
                case 7:
                    return 9;
                case 8:
                    return 6;
                case 9:
                    return 7;
                default:
                    return 0;
            }
        }

        @DexIgnore
        public final void a(b bVar) {
            wd4.b(bVar, "<set-?>");
            this.f = bVar;
        }

        @DexIgnore
        public final void b(int i) {
            this.e = i;
        }

        @DexIgnore
        public int d() {
            return this.e;
        }

        @DexIgnore
        public final b e() {
            return this.f;
        }

        @DexIgnore
        public final int f() {
            return this.e;
        }

        @DexIgnore
        public void a(int i, int i2, a aVar) {
            wd4.b(aVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i + ", newState=" + i2);
        }

        @DexIgnore
        public b b() {
            return this.f;
        }

        @DexIgnore
        public void a(b bVar, b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "onMetadataChanged(), oldMetadata=" + bVar + ", newMetadata=" + bVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wd4.b(keyEvent, "keyEvent");
            return this.d.sendMediaKeyEvent(keyEvent);
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements RemoteController.OnClientUpdateListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;

            @DexIgnore
            public a(c cVar, String str) {
                this.a = cVar;
                this.b = str;
            }

            @DexIgnore
            public void onClientChange(boolean z) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = MusicControlComponent.o.a();
                local.d(a2, "onClientChange(), clearing=" + z);
            }

            @DexIgnore
            public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
                if (metadataEditor != null) {
                    String a2 = rj2.a(metadataEditor.getString(7, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    String a3 = rj2.a(metadataEditor.getString(2, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    String a4 = rj2.a(metadataEditor.getString(1, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a5 = MusicControlComponent.o.a();
                    local.d(a5, ".onMetadataChanged callback, title=" + a2 + ", artist=" + a3 + ", album=" + a4);
                    b bVar = new b(this.a.c(), this.b, a2, a3, a4);
                    if (true ^ wd4.a((Object) bVar, (Object) this.a.e())) {
                        b e = this.a.e();
                        this.a.a(bVar);
                        this.a.a(e, bVar);
                    }
                }
            }

            @DexIgnore
            public void onClientPlaybackStateUpdate(int i) {
                int a2 = this.a.a(i);
                if (a2 != this.a.f()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = MusicControlComponent.o.a();
                    local.d(a3, "onClientPlaybackStateUpdate(), state=" + i);
                    int f = this.a.f();
                    this.a.b(a2);
                    c cVar = this.a;
                    cVar.a(f, a2, cVar);
                }
            }

            @DexIgnore
            public void onClientTransportControlUpdate(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = MusicControlComponent.o.a();
                local.d(a2, "onClientTransportControlUpdate(), transportControlFlags=" + i);
            }

            @DexIgnore
            public void onClientPlaybackStateUpdate(int i, long j, long j2, float f) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = MusicControlComponent.o.a();
                local.d(a2, "onClientPlaybackStateUpdate(), state=" + i + ", stateChangeTimeMs=" + j + ", currentPosMs=" + j2 + ", speed=" + f);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<X, Y> extends bc<Pair<? extends X, ? extends Y>> {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements dc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ e a;
            @DexIgnore
            public /* final */ /* synthetic */ LiveData b;

            @DexIgnore
            public a(e eVar, LiveData liveData) {
                this.a = eVar;
                this.b = liveData;
            }

            @DexIgnore
            public final void a(X x) {
                this.a.b(new Pair(x, this.b.a()));
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements dc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ e a;
            @DexIgnore
            public /* final */ /* synthetic */ LiveData b;

            @DexIgnore
            public b(e eVar, LiveData liveData) {
                this.a = eVar;
                this.b = liveData;
            }

            @DexIgnore
            public final void a(Y y) {
                this.a.b(new Pair(this.b.a(), y));
            }
        }

        @DexIgnore
        public e(LiveData<X> liveData, LiveData<Y> liveData2) {
            wd4.b(liveData, "trigger1");
            wd4.b(liveData2, "trigger2");
            a(liveData, new a(this, liveData2));
            a(liveData2, new b(this, liveData));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements dc<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent a;

        @DexIgnore
        public h(MusicControlComponent musicControlComponent) {
            this.a = musicControlComponent;
        }

        @DexIgnore
        public final void a(DianaPreset dianaPreset) {
            this.a.i = false;
            if (dianaPreset != null) {
                ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
                if (watchapps != null) {
                    for (DianaPresetWatchAppSetting id : watchapps) {
                        if (wd4.a((Object) id.getId(), (Object) Constants.MUSIC)) {
                            this.a.i = true;
                        }
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "mCurrentPresetObserver: mIsControlMusicAssigned=" + this.a.i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ i a; // = new i();

        @DexIgnore
        /* renamed from: a */
        public final LiveData<DianaPreset> apply(Pair<DianaPresetRepository, String> pair) {
            DianaPresetRepository component1 = pair.component1();
            String component2 = pair.component2();
            if (component2 == null) {
                component2 = "";
            }
            if (component1 != null) {
                LiveData<DianaPreset> activePresetBySerialLiveData = component1.getActivePresetBySerialLiveData(component2);
                if (activePresetBySerialLiveData != null) {
                    return activePresetBySerialLiveData;
                }
            }
            return tr3.k.a();
        }
    }

    /*
    static {
        String simpleName = MusicControlComponent.class.getSimpleName();
        wd4.a((Object) simpleName, "MusicControlComponent::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public MusicControlComponent(Context context) {
        wd4.b(context, "context");
        this.l = context;
        LiveData<DianaPreset> b2 = ic.b(new e(this.h, PortfolioApp.W.c().f()), i.a);
        wd4.a((Object) b2, "Transformations.switchMa\u2026ntLiveData.create()\n    }");
        this.j = b2;
        this.k = new h(this);
        FLogger.INSTANCE.getLocal().d(m, "init");
        Object systemService = this.l.getSystemService("audio");
        this.e = (AudioManager) (!(systemService instanceof AudioManager) ? null : systemService);
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        this.j.a(this.k);
    }

    @DexIgnore
    public final a d() {
        return this.b;
    }

    @DexIgnore
    public final d<a> e() {
        return this.d;
    }

    @DexIgnore
    public final pl4 f() {
        return this.c;
    }

    @DexIgnore
    public final void g() {
        this.g = false;
        MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener = this.f;
        if (onActiveSessionsChangedListener != null) {
            MediaSessionManager mediaSessionManager = this.a;
            if (mediaSessionManager != null) {
                mediaSessionManager.removeOnActiveSessionsChangedListener(onActiveSessionsChangedListener);
            }
        }
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus h() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            audioManager.adjustStreamVolume(3, -1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus i() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            audioManager.adjustStreamVolume(3, 1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final void b() {
        a aVar = this.b;
        if (aVar != null) {
            int d2 = aVar.d();
            b b2 = aVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, ".forcePushEventAndMetadataChanged active controller=" + aVar.c() + ", state=" + d2 + ", metadata=" + b2);
            a(aVar.d());
            a(aVar.b());
            return;
        }
        FLogger.INSTANCE.getLocal().d(m, ".forcePushEventAndMetadataChanged, no active controller now.");
    }

    @DexIgnore
    public final Context c() {
        return this.l;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> {
        @DexIgnore
        public /* final */ Set<T> a; // = new LinkedHashSet();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final boolean a(T t) {
            boolean add;
            synchronized (this.b) {
                add = !this.a.contains(t) ? this.a.add(t) : false;
            }
            return add;
        }

        @DexIgnore
        public final boolean b(T t) {
            boolean remove;
            synchronized (this.b) {
                remove = this.a.remove(t);
            }
            return remove;
        }

        @DexIgnore
        public final T a(jd4<? super T, Boolean> jd4) {
            T t;
            wd4.b(jd4, "predicate");
            synchronized (this.b) {
                Iterator<T> it = this.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (jd4.invoke(t).booleanValue()) {
                        break;
                    }
                }
            }
            return t;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends c {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(MusicControlComponent musicControlComponent, Context context, String str) {
            super(context, str);
            this.g = musicControlComponent;
        }

        @DexIgnore
        public void a(int i, int i2, a aVar) {
            wd4.b(aVar, "controller");
            super.a(i, i2, aVar);
            if (i2 == 3) {
                this.g.a(aVar);
            } else if (this.g.d() == null) {
                this.g.a(aVar);
            }
            this.g.a(i2);
        }

        @DexIgnore
        public void a(b bVar, b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            super.a(bVar, bVar2);
            this.g.a(b());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends OldVersionMusicController {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent j;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(MediaAppDetails mediaAppDetails, Context context, MediaAppDetails mediaAppDetails2, String str, MusicControlComponent musicControlComponent) {
            super(context, mediaAppDetails2, str);
            this.j = musicControlComponent;
        }

        @DexIgnore
        public void a(OldVersionMusicController oldVersionMusicController) {
            if (oldVersionMusicController != null && this.j.e().a(oldVersionMusicController)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = MusicControlComponent.o.a();
                local.d(a, ".OldVersionMusicController is added to list controller, packageName=" + oldVersionMusicController.c());
                if (oldVersionMusicController.d() == 3) {
                    this.j.a(oldVersionMusicController.d());
                    this.j.a(oldVersionMusicController.b());
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, a aVar) {
            wd4.b(aVar, "controller");
            super.a(i, i2, aVar);
            if (i2 == 3) {
                this.j.a(aVar);
            } else if (this.j.d() == null) {
                this.j.a(aVar);
            }
            this.j.a(i2);
        }

        @DexIgnore
        public void a(b bVar, b bVar2) {
            wd4.b(bVar, "oldMetadata");
            wd4.b(bVar2, "newMetadata");
            super.a(bVar, bVar2);
            this.j.a(b());
        }
    }

    @DexIgnore
    public final void a(a aVar) {
        boolean z = !wd4.a((Object) this.b, (Object) aVar);
        this.b = aVar;
        if (z && aVar != null) {
            a(aVar.b());
        }
    }

    @DexIgnore
    public final void a(DianaPresetRepository dianaPresetRepository) {
        this.h.a(dianaPresetRepository);
    }

    @DexIgnore
    public final ArrayList<MediaAppDetails> a(PackageManager packageManager, Resources resources) {
        ArrayList arrayList = new ArrayList();
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(new Intent("android.media.browse.MediaBrowserService"), 64);
        if (queryIntentServices != null && (!queryIntentServices.isEmpty())) {
            for (ResolveInfo next : queryIntentServices) {
                if (!n.contains(next.serviceInfo.packageName)) {
                    arrayList.add(new MediaAppDetails(next.serviceInfo, packageManager, resources));
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = m;
                    local.d(str, ".findMediaBrowserAppsTask(), " + next.serviceInfo.packageName + " is in black list of using Media Browser Compat.");
                }
            }
        }
        ArrayList<MediaAppDetails> arrayList2 = new ArrayList<>();
        for (MediaAppDetails mediaAppDetails : wb4.b(arrayList)) {
            Context context = this.l;
            String str2 = mediaAppDetails.packageName;
            wd4.a((Object) str2, "it.packageName");
            new g(mediaAppDetails, context, mediaAppDetails, a(str2), this);
        }
        return arrayList2;
    }

    @DexIgnore
    public final boolean b(String str) {
        wd4.b(str, "serial");
        return FossilDeviceSerialPatternUtil.isDianaDevice(str);
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus b(int i2) {
        a aVar = this.b;
        if (aVar != null) {
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = (!aVar.a(new KeyEvent(0, i2)) || !aVar.a(new KeyEvent(1, i2))) ? NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER : NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.NO_MUSIC_PLAYER;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(FossilNotificationListenerService fossilNotificationListenerService, kc4<? super cb4> kc4) {
        MusicControlComponent$enableNotificationListener$Anon1 musicControlComponent$enableNotificationListener$Anon1;
        int i2;
        MusicControlComponent musicControlComponent;
        if (kc4 instanceof MusicControlComponent$enableNotificationListener$Anon1) {
            musicControlComponent$enableNotificationListener$Anon1 = (MusicControlComponent$enableNotificationListener$Anon1) kc4;
            int i3 = musicControlComponent$enableNotificationListener$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                musicControlComponent$enableNotificationListener$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = musicControlComponent$enableNotificationListener$Anon1.result;
                Object a2 = oc4.a();
                i2 = musicControlComponent$enableNotificationListener$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(m, ".enableNotificationListener()");
                    if (!this.g) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            Object systemService = this.l.getSystemService("media_session");
                            if (systemService != null) {
                                this.a = (MediaSessionManager) systemService;
                                this.f = new MusicControlComponent$enableNotificationListener$Anon2(this);
                                bj4 c2 = zh4.c();
                                MusicControlComponent$enableNotificationListener$Anon3 musicControlComponent$enableNotificationListener$Anon3 = new MusicControlComponent$enableNotificationListener$Anon3(this, fossilNotificationListenerService, (kc4) null);
                                musicControlComponent$enableNotificationListener$Anon1.L$Anon0 = this;
                                musicControlComponent$enableNotificationListener$Anon1.L$Anon1 = fossilNotificationListenerService;
                                musicControlComponent$enableNotificationListener$Anon1.label = 1;
                                if (kg4.a(c2, musicControlComponent$enableNotificationListener$Anon3, musicControlComponent$enableNotificationListener$Anon1) == a2) {
                                    return a2;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type android.media.session.MediaSessionManager");
                            }
                        } else {
                            f fVar = new f(this, this.l, "All Apps");
                            if (this.d.a(fVar)) {
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str = m;
                                local.d(str, ".OldNotificationMusicController is added to list controller, packageName=" + fVar.c());
                            }
                        }
                        musicControlComponent = this;
                    }
                    return cb4.a;
                } else if (i2 == 1) {
                    FossilNotificationListenerService fossilNotificationListenerService2 = (FossilNotificationListenerService) musicControlComponent$enableNotificationListener$Anon1.L$Anon1;
                    musicControlComponent = (MusicControlComponent) musicControlComponent$enableNotificationListener$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                musicControlComponent.g = true;
                return cb4.a;
            }
        }
        musicControlComponent$enableNotificationListener$Anon1 = new MusicControlComponent$enableNotificationListener$Anon1(this, kc4);
        Object obj2 = musicControlComponent$enableNotificationListener$Anon1.result;
        Object a22 = oc4.a();
        i2 = musicControlComponent$enableNotificationListener$Anon1.label;
        if (i2 != 0) {
        }
        musicControlComponent.g = true;
        return cb4.a;
    }

    @DexIgnore
    public final String a(String str) {
        ApplicationInfo applicationInfo;
        PackageManager packageManager = PortfolioApp.W.c().getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(str, 0);
        } catch (Exception unused) {
            applicationInfo = null;
        }
        if (applicationInfo != null) {
            String obj = packageManager.getApplicationLabel(applicationInfo).toString();
            if (obj != null) {
                return obj;
            }
        }
        return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
    }

    @DexIgnore
    public final void a(NotifyMusicEventResponse.MusicMediaAction musicMediaAction) {
        NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus;
        wd4.b(musicMediaAction, "action");
        switch (tp2.a[musicMediaAction.ordinal()]) {
            case 1:
                musicMediaStatus = b(126);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 2:
                musicMediaStatus = b(127);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 3:
                musicMediaStatus = b(85);
                break;
            case 4:
                musicMediaStatus = b(87);
                break;
            case 5:
                musicMediaStatus = b(88);
                break;
            case 6:
                musicMediaStatus = i();
                break;
            case 7:
                musicMediaStatus = h();
                break;
            default:
                musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
                break;
        }
        if (this.i) {
            PortfolioApp.W.c().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, musicMediaStatus), PortfolioApp.W.c().e());
        }
    }

    @DexIgnore
    public final int a() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            return audioManager.getStreamVolume(3);
        }
        return 100;
    }

    @DexIgnore
    public final synchronized void a(int i2) {
        NotifyMusicEventResponse.MusicMediaAction musicMediaAction;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".notifyMusicAction(), state=" + i2);
        if (i2 == 2) {
            musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PAUSE;
        } else if (i2 != 3) {
            musicMediaAction = null;
        } else {
            musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PLAY;
        }
        if (musicMediaAction != null && this.i) {
            PortfolioApp.W.c().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus.SUCCESS), PortfolioApp.W.c().e());
        }
    }

    @DexIgnore
    public final synchronized void a(b bVar) {
        wd4.b(bVar, "metadata");
        String c2 = bVar.c();
        a aVar = this.b;
        if (wd4.a((Object) c2, (Object) aVar != null ? aVar.c() : null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, ".notifyMusicTrackInfo(), metadata=" + bVar);
            if (this.i) {
                PortfolioApp.W.c().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicTrackInfoResponse("", (byte) a(), bVar.d(), bVar.b(), bVar.a()), PortfolioApp.W.c().e());
            }
        }
    }
}
