package com.portfolio.platform.service.musiccontrol;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$Anon2", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$NewNotificationMusicController$metadata$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.b $currentMetadata;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.b $oldMetadata;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.NewNotificationMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$NewNotificationMusicController$metadata$Anon2(MusicControlComponent.NewNotificationMusicController newNotificationMusicController, MusicControlComponent.b bVar, MusicControlComponent.b bVar2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = newNotificationMusicController;
        this.$oldMetadata = bVar;
        this.$currentMetadata = bVar2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MusicControlComponent$NewNotificationMusicController$metadata$Anon2 musicControlComponent$NewNotificationMusicController$metadata$Anon2 = new MusicControlComponent$NewNotificationMusicController$metadata$Anon2(this.this$Anon0, this.$oldMetadata, this.$currentMetadata, kc4);
        musicControlComponent$NewNotificationMusicController$metadata$Anon2.p$ = (lh4) obj;
        return musicControlComponent$NewNotificationMusicController$metadata$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$NewNotificationMusicController$metadata$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.a(this.$oldMetadata, this.$currentMetadata);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
