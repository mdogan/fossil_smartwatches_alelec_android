package com.portfolio.platform.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$bleActionServiceReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService a;

    @DexIgnore
    public MFDeviceService$bleActionServiceReceiver$Anon1(MFDeviceService mFDeviceService) {
        this.a = mFDeviceService;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        wd4.b(intent, "intent");
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1(this, intent, (kc4) null), 3, (Object) null);
    }
}
