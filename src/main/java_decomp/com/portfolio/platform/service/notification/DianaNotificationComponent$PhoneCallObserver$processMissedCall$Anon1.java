package com.portfolio.platform.service.notification;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent.PhoneCallObserver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(DianaNotificationComponent.PhoneCallObserver phoneCallObserver, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = phoneCallObserver;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1 = new DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(this.this$Anon0, kc4);
        dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1.p$ = (lh4) obj;
        return dianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            DianaNotificationComponent.PhoneCallObserver.a b = this.this$Anon0.a();
            if (b != null) {
                DianaNotificationComponent.PhoneCallObserver.a a = this.this$Anon0.a;
                if (a != null) {
                    if (a.a() < b.a()) {
                        if (b.c() == 3) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String b2 = DianaNotificationComponent.this.b();
                            local.d(b2, "processMissedCall - number = " + b.b());
                            DianaNotificationComponent.this.a(b.b(), new Date(), DianaNotificationComponent.PhoneStateEnum.MISSED);
                        } else if (b.c() == 5 || b.c() == 6 || b.c() == 1) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String b3 = DianaNotificationComponent.this.b();
                            local2.d(b3, "processHangUpCall - number = " + b.b());
                            DianaNotificationComponent.this.a(b.b(), new Date(), DianaNotificationComponent.PhoneStateEnum.PICKED);
                        }
                    }
                    this.this$Anon0.a = b;
                } else {
                    wd4.a();
                    throw null;
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
