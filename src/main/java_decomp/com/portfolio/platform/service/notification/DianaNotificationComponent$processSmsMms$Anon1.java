package com.portfolio.platform.service.notification;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$processSmsMms$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $content;
    @DexIgnore
    public /* final */ /* synthetic */ String $phoneNumber;
    @DexIgnore
    public /* final */ /* synthetic */ long $receivedTime;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processSmsMms$Anon1(DianaNotificationComponent dianaNotificationComponent, String str, String str2, long j, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dianaNotificationComponent;
        this.$phoneNumber = str;
        this.$content = str2;
        this.$receivedTime = j;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaNotificationComponent$processSmsMms$Anon1 dianaNotificationComponent$processSmsMms$Anon1 = new DianaNotificationComponent$processSmsMms$Anon1(this.this$Anon0, this.$phoneNumber, this.$content, this.$receivedTime, kc4);
        dianaNotificationComponent$processSmsMms$Anon1.p$ = (lh4) obj;
        return dianaNotificationComponent$processSmsMms$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaNotificationComponent$processSmsMms$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            if (!this.this$Anon0.g.M()) {
                FLogger.INSTANCE.getLocal().d(this.this$Anon0.b(), "Process SMS/MMS by old solution");
                if (!this.this$Anon0.a(this.$phoneNumber, false)) {
                    return cb4.a;
                }
                this.this$Anon0.c((DianaNotificationComponent.d) new DianaNotificationComponent.f(this.this$Anon0.d(this.$phoneNumber), this.$content, this.$receivedTime));
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
