package com.portfolio.platform.service.notification;

import android.content.Context;
import android.os.Build;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.vp2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Calendar;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$processPhoneCall$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $phoneNumber;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startTime;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent.PhoneStateEnum $state;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processPhoneCall$Anon1(DianaNotificationComponent dianaNotificationComponent, DianaNotificationComponent.PhoneStateEnum phoneStateEnum, String str, Date date, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dianaNotificationComponent;
        this.$state = phoneStateEnum;
        this.$phoneNumber = str;
        this.$startTime = date;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaNotificationComponent$processPhoneCall$Anon1 dianaNotificationComponent$processPhoneCall$Anon1 = new DianaNotificationComponent$processPhoneCall$Anon1(this.this$Anon0, this.$state, this.$phoneNumber, this.$startTime, kc4);
        dianaNotificationComponent$processPhoneCall$Anon1.p$ = (lh4) obj;
        return dianaNotificationComponent$processPhoneCall$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaNotificationComponent$processPhoneCall$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            int i = vp2.a[this.$state.ordinal()];
            if (i != 1) {
                if (i == 2) {
                    DianaNotificationComponent.e a = this.this$Anon0.e;
                    if (a != null && wd4.a((Object) a.l(), (Object) this.$phoneNumber)) {
                        DianaNotificationComponent.e eVar = new DianaNotificationComponent.e(DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        eVar.d(a.d());
                        eVar.a(a.a());
                        this.this$Anon0.d((DianaNotificationComponent.d) eVar);
                        this.this$Anon0.e = null;
                    }
                    if (Build.VERSION.SDK_INT >= 28) {
                        DianaNotificationComponent dianaNotificationComponent = this.this$Anon0;
                        Context applicationContext = PortfolioApp.W.c().getApplicationContext();
                        wd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        dianaNotificationComponent.b(applicationContext);
                    }
                } else if (i == 3) {
                    if (!this.this$Anon0.a(this.$phoneNumber, true)) {
                        return cb4.a;
                    }
                    DianaNotificationComponent.e a2 = this.this$Anon0.e;
                    if (a2 != null && wd4.a((Object) a2.l(), (Object) this.$phoneNumber)) {
                        DianaNotificationComponent.e eVar2 = new DianaNotificationComponent.e(DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        eVar2.a(a2.a());
                        eVar2.d(a2.d());
                        this.this$Anon0.d((DianaNotificationComponent.d) eVar2);
                        this.this$Anon0.e = null;
                    }
                    String a3 = this.this$Anon0.d(this.$phoneNumber);
                    DianaNotificationComponent dianaNotificationComponent2 = this.this$Anon0;
                    String packageName = DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL.getPackageName();
                    String str = this.$phoneNumber;
                    Calendar instance = Calendar.getInstance();
                    wd4.a((Object) instance, "Calendar.getInstance()");
                    Date time = instance.getTime();
                    wd4.a((Object) time, "Calendar.getInstance().time");
                    dianaNotificationComponent2.b((DianaNotificationComponent.d) new DianaNotificationComponent.e(packageName, a3, str, "Missed Call", time));
                    if (Build.VERSION.SDK_INT >= 28) {
                        DianaNotificationComponent dianaNotificationComponent3 = this.this$Anon0;
                        Context applicationContext2 = PortfolioApp.W.c().getApplicationContext();
                        wd4.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                        dianaNotificationComponent3.b(applicationContext2);
                    }
                }
            } else if (!this.this$Anon0.a(this.$phoneNumber, true)) {
                return cb4.a;
            } else {
                if (Build.VERSION.SDK_INT >= 28) {
                    DianaNotificationComponent dianaNotificationComponent4 = this.this$Anon0;
                    Context applicationContext3 = PortfolioApp.W.c().getApplicationContext();
                    wd4.a((Object) applicationContext3, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent4.b(applicationContext3);
                    DianaNotificationComponent dianaNotificationComponent5 = this.this$Anon0;
                    Context applicationContext4 = PortfolioApp.W.c().getApplicationContext();
                    wd4.a((Object) applicationContext4, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent5.a(applicationContext4);
                }
                String a4 = this.this$Anon0.d(this.$phoneNumber);
                DianaNotificationComponent.e a5 = this.this$Anon0.e;
                if (a5 != null) {
                    this.this$Anon0.d((DianaNotificationComponent.d) a5);
                }
                DianaNotificationComponent.e eVar3 = new DianaNotificationComponent.e(DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getPackageName(), a4, this.$phoneNumber, "Incoming Call", this.$startTime);
                this.this$Anon0.e = eVar3.clone();
                this.this$Anon0.b((DianaNotificationComponent.d) eVar3);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
