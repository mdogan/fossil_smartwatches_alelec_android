package com.portfolio.platform.service.notification;

import android.service.notification.StatusBarNotification;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$Anon1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
public final class DianaNotificationComponent$processAppNotification$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ StatusBarNotification $sbn;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaNotificationComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processAppNotification$Anon1(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dianaNotificationComponent;
        this.$sbn = statusBarNotification;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaNotificationComponent$processAppNotification$Anon1 dianaNotificationComponent$processAppNotification$Anon1 = new DianaNotificationComponent$processAppNotification$Anon1(this.this$Anon0, this.$sbn, kc4);
        dianaNotificationComponent$processAppNotification$Anon1.p$ = (lh4) obj;
        return dianaNotificationComponent$processAppNotification$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaNotificationComponent$processAppNotification$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.c(new DianaNotificationComponent.d(this.$sbn));
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
