package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $batteryLevel$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Device $device$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(kc4 kc4, MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1, int i, Device device) {
        super(2, kc4);
        this.this$Anon0 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
        this.$batteryLevel$inlined = i;
        this.$device$inlined = device;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(kc4, this.this$Anon0, this.$batteryLevel$inlined, this.$device$inlined);
        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            DeviceRepository d = this.this$Anon0.this$Anon0.a.d();
            Device device = this.$device$inlined;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = d.updateDevice(device, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
