package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fe4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.usecase.HybridSyncDataProcessing;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon2", f = "HybridSyncDataProcessing.kt", l = {148}, m = "invokeSuspend")
public final class HybridSyncDataProcessing$saveSyncResult$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super ri4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridSyncDataProcessing.a $finalResult;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$Anon2$Anon1", f = "HybridSyncDataProcessing.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ri4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $listGFitSample;
        @DexIgnore
        public /* final */ /* synthetic */ List $listSleepSession;
        @DexIgnore
        public /* final */ /* synthetic */ List $listUASample;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing$saveSyncResult$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HybridSyncDataProcessing$saveSyncResult$Anon2 hybridSyncDataProcessing$saveSyncResult$Anon2, List list, List list2, List list3, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = hybridSyncDataProcessing$saveSyncResult$Anon2;
            this.$listGFitSample = list;
            this.$listUASample = list2;
            this.$listSleepSession = list3;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$listGFitSample, this.$listUASample, this.$listSleepSession, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (GFitActiveTime) null, ob4.a(), ob4.a(), this.$listSleepSession);
                return ThirdPartyRepository.uploadData$default(this.this$Anon0.$thirdPartyRepository, (ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (Object) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$saveSyncResult$Anon2(HybridSyncDataProcessing.a aVar, ThirdPartyRepository thirdPartyRepository, kc4 kc4) {
        super(2, kc4);
        this.$finalResult = aVar;
        this.$thirdPartyRepository = thirdPartyRepository;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HybridSyncDataProcessing$saveSyncResult$Anon2 hybridSyncDataProcessing$saveSyncResult$Anon2 = new HybridSyncDataProcessing$saveSyncResult$Anon2(this.$finalResult, this.$thirdPartyRepository, kc4);
        hybridSyncDataProcessing$saveSyncResult$Anon2.p$ = (lh4) obj;
        return hybridSyncDataProcessing$saveSyncResult$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HybridSyncDataProcessing$saveSyncResult$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            List<GFitSample> a2 = this.$finalResult.a();
            ArrayList arrayList = new ArrayList();
            for (ActivitySample next : this.$finalResult.c()) {
                DateTime component3 = next.component3();
                next.component4();
                double component5 = next.component5();
                arrayList.add(new UASample(fe4.a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000)));
            }
            List<MFSleepSession> d = this.$finalResult.d();
            gh4 b = zh4.b();
            Anon1 anon1 = new Anon1(this, a2, arrayList, d, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = a2;
            this.L$Anon2 = arrayList;
            this.L$Anon3 = d;
            this.label = 1;
            Object a3 = kg4.a(b, anon1, this);
            return a3 == a ? a : a3;
        } else if (i == 1) {
            List list = (List) this.L$Anon3;
            List list2 = (List) this.L$Anon2;
            List list3 = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
