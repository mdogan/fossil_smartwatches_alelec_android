package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fe4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.usecase.DianaSyncDataProcessing;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon2", f = "DianaSyncDataProcessing.kt", l = {186}, m = "invokeSuspend")
public final class DianaSyncDataProcessing$saveSyncResult$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super ri4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncDataProcessing.a $finalResult;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon2$Anon1", f = "DianaSyncDataProcessing.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ri4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $gFitActiveTime;
        @DexIgnore
        public /* final */ /* synthetic */ List $listGFitHeartRate;
        @DexIgnore
        public /* final */ /* synthetic */ List $listGFitSample;
        @DexIgnore
        public /* final */ /* synthetic */ List $listGFitWorkoutSession;
        @DexIgnore
        public /* final */ /* synthetic */ List $listMFSleepSession;
        @DexIgnore
        public /* final */ /* synthetic */ List $listUASample;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncDataProcessing$saveSyncResult$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaSyncDataProcessing$saveSyncResult$Anon2 dianaSyncDataProcessing$saveSyncResult$Anon2, List list, List list2, Ref$ObjectRef ref$ObjectRef, List list3, List list4, List list5, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = dianaSyncDataProcessing$saveSyncResult$Anon2;
            this.$listGFitSample = list;
            this.$listUASample = list2;
            this.$gFitActiveTime = ref$ObjectRef;
            this.$listGFitHeartRate = list3;
            this.$listGFitWorkoutSession = list4;
            this.$listMFSleepSession = list5;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$listGFitSample, this.$listUASample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (GFitActiveTime) this.$gFitActiveTime.element, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession);
                return ThirdPartyRepository.uploadData$default(this.this$Anon0.$thirdPartyRepository, (ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (Object) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncDataProcessing$saveSyncResult$Anon2(DianaSyncDataProcessing.a aVar, ThirdPartyRepository thirdPartyRepository, kc4 kc4) {
        super(2, kc4);
        this.$finalResult = aVar;
        this.$thirdPartyRepository = thirdPartyRepository;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaSyncDataProcessing$saveSyncResult$Anon2 dianaSyncDataProcessing$saveSyncResult$Anon2 = new DianaSyncDataProcessing$saveSyncResult$Anon2(this.$finalResult, this.$thirdPartyRepository, kc4);
        dianaSyncDataProcessing$saveSyncResult$Anon2.p$ = (lh4) obj;
        return dianaSyncDataProcessing$saveSyncResult$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaSyncDataProcessing$saveSyncResult$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            List<GFitSample> c = this.$finalResult.c();
            ArrayList arrayList = new ArrayList();
            for (ActivitySample next : this.$finalResult.h()) {
                DateTime component3 = next.component3();
                next.component4();
                double component5 = next.component5();
                arrayList.add(new UASample(fe4.a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000)));
            }
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            List<Pair<Long, Long>> a2 = this.$finalResult.a();
            ArrayList arrayList2 = new ArrayList(pb4.a(a2, 10));
            for (Pair pair : a2) {
                arrayList2.add(ob4.c((Long) pair.getFirst(), (Long) pair.getSecond()));
            }
            List k = wb4.k(pb4.a(arrayList2));
            if (!k.isEmpty()) {
                ref$ObjectRef.element = new GFitActiveTime(k);
            }
            List<MFSleepSession> i2 = this.$finalResult.i();
            List<GFitHeartRate> b = this.$finalResult.b();
            List<GFitWorkoutSession> d = this.$finalResult.d();
            Anon1 anon1 = r0;
            gh4 b2 = zh4.b();
            List<GFitWorkoutSession> list = d;
            Anon1 anon12 = new Anon1(this, c, arrayList, ref$ObjectRef, b, list, i2, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = c;
            this.L$Anon2 = arrayList;
            this.L$Anon3 = ref$ObjectRef;
            this.L$Anon4 = k;
            this.L$Anon5 = i2;
            this.L$Anon6 = b;
            this.L$Anon7 = list;
            this.label = 1;
            Object a3 = kg4.a(b2, anon1, this);
            Object obj2 = a;
            return a3 == obj2 ? obj2 : a3;
        } else if (i == 1) {
            List list2 = (List) this.L$Anon7;
            List list3 = (List) this.L$Anon6;
            List list4 = (List) this.L$Anon5;
            List list5 = (List) this.L$Anon4;
            Ref$ObjectRef ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon3;
            List list6 = (List) this.L$Anon2;
            List list7 = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
