package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.List;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {322}, m = "processGoalTrackingData")
public final class HybridSyncDataProcessing$processGoalTrackingData$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ HybridSyncDataProcessing this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$processGoalTrackingData$Anon1(HybridSyncDataProcessing hybridSyncDataProcessing, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = hybridSyncDataProcessing;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((List<? extends WrapperTapEventSummary>) null, (String) null, (HybridPresetRepository) null, (GoalTrackingRepository) null, (UserRepository) null, this);
    }
}
