package com.portfolio.platform.service.usecase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.blesdk.obfuscated.zk2;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.util.SyncDataExtensionsKt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ DianaSyncDataProcessing b; // = new DianaSyncDataProcessing();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<ActivitySample> b;
        @DexIgnore
        public /* final */ List<ActivitySummary> c;
        @DexIgnore
        public /* final */ List<MFSleepSession> d;
        @DexIgnore
        public /* final */ List<HeartRateSample> e;
        @DexIgnore
        public /* final */ List<DailyHeartRateSummary> f;
        @DexIgnore
        public /* final */ List<Pair<Long, Long>> g;
        @DexIgnore
        public /* final */ List<WorkoutSession> h;
        @DexIgnore
        public /* final */ List<GFitSample> i;
        @DexIgnore
        public /* final */ List<GFitHeartRate> j;
        @DexIgnore
        public /* final */ List<GFitWorkoutSession> k;

        @DexIgnore
        public a(long j2, List<ActivitySample> list, List<ActivitySummary> list2, List<MFSleepSession> list3, List<HeartRateSample> list4, List<DailyHeartRateSummary> list5, List<Pair<Long, Long>> list6, List<WorkoutSession> list7, List<GFitSample> list8, List<GFitHeartRate> list9, List<GFitWorkoutSession> list10) {
            wd4.b(list, "sampleRawList");
            wd4.b(list2, "summaryList");
            wd4.b(list3, "sleepSessionList");
            wd4.b(list4, "heartRateDataList");
            wd4.b(list5, "heartRateSummaryList");
            wd4.b(list6, "activeTimeList");
            wd4.b(list7, "workoutSessionList");
            wd4.b(list8, "gFitSampleList");
            wd4.b(list9, "gFitHeartRateList");
            wd4.b(list10, "gFitWorkoutSessionList");
            this.a = j2;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
            this.g = list6;
            this.h = list7;
            this.i = list8;
            this.j = list9;
            this.k = list10;
        }

        @DexIgnore
        public final List<Pair<Long, Long>> a() {
            return this.g;
        }

        @DexIgnore
        public final List<GFitHeartRate> b() {
            return this.j;
        }

        @DexIgnore
        public final List<GFitSample> c() {
            return this.i;
        }

        @DexIgnore
        public final List<GFitWorkoutSession> d() {
            return this.k;
        }

        @DexIgnore
        public final List<HeartRateSample> e() {
            return this.e;
        }

        @DexIgnore
        public final List<DailyHeartRateSummary> f() {
            return this.f;
        }

        @DexIgnore
        public final long g() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> h() {
            return this.b;
        }

        @DexIgnore
        public final List<MFSleepSession> i() {
            return this.d;
        }

        @DexIgnore
        public final List<ActivitySummary> j() {
            return this.c;
        }

        @DexIgnore
        public final List<WorkoutSession> k() {
            return this.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ic4.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ic4.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    /*
    static {
        String name = DianaSyncDataProcessing.class.getName();
        wd4.a((Object) name, "DianaSyncDataProcessing::class.java.name");
        a = name;
    }
    */

    @DexIgnore
    public final a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        wd4.b(str2, "serial");
        wd4.b(list2, "syncData");
        wd4.b(mFUser, "user");
        wd4.b(userProfile, "userProfile");
        wd4.b(portfolioApp, "portfolioApp");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a;
        local.d(str3, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list2);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (list.isEmpty()) {
            a(str2, "Sync data is empty");
            return aVar;
        }
        return b(str, list, mFUser, userProfile, j, j2, portfolioApp);
    }

    @DexIgnore
    public final a b(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        portfolioApp.a(CommunicateMode.SYNC, str2, "Calculating sleep and activity...");
        String userId = mFUser.getUserId();
        wd4.a((Object) userId, ButtonService.USER_ID);
        Triple<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = SyncDataExtensionsKt.a(list2, str2, userId, 1000 * j);
        List<ActivitySample> first = a2.getFirst();
        List second = a2.getSecond();
        List third = a2.getThird();
        List<MFSleepSession> a3 = SyncDataExtensionsKt.a(list2, str2);
        Pair<List<HeartRateSample>, List<DailyHeartRateSummary>> a4 = a(list2, userId);
        List first2 = a4.getFirst();
        List second2 = a4.getSecond();
        List<WorkoutSession> a5 = a(list2, str2, userId);
        List<GFitHeartRate> a6 = a(list2);
        List<GFitWorkoutSession> b2 = b(list2);
        int size = a3.size();
        double d = 0.0d;
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        int i = 0;
        int i2 = 0;
        for (ActivitySample activitySample : first) {
            d3 += activitySample.getCalories();
            d4 += activitySample.getDistance();
            d += activitySample.getSteps();
            i += activitySample.getActiveTime();
            Boolean s = sk2.s(activitySample.getDate());
            wd4.a((Object) s, "DateHelper.isToday(it.date)");
            if (s.booleanValue()) {
                d2 += activitySample.getSteps();
                i2 += activitySample.getActiveTime();
            }
            List<FitnessDataWrapper> list3 = list;
        }
        be4 be4 = be4.a;
        Object[] objArr = {String.valueOf(size)};
        String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        a(str2, format);
        be4 be42 = be4.a;
        Object[] objArr2 = {Double.valueOf(d), Double.valueOf(d2), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d3), Double.valueOf(d4), Integer.valueOf(i), Integer.valueOf(i2)};
        String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s, Calories=%s, DistanceWrapper=%s, ActiveTime=%s, TodayActiveTime=%s", Arrays.copyOf(objArr2, objArr2.length));
        wd4.a((Object) format2, "java.lang.String.format(format, *args)");
        a(str2, format2);
        a(str2, "HeartRateWrapper data size: " + first2.size());
        FLogger.INSTANCE.getLocal().d(a, "Release=" + zk2.a());
        if (!zk2.a()) {
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Sleep sessions details: " + new Gson().a((Object) a3));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - SampleRaw list details: " + new Gson().a((Object) first));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate sample list details: " + first2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate summary list details: " + second2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Workout list details: " + new Gson().a((Object) a5));
        }
        return new a(j2, first, second, a3, first2, second2, SyncDataExtensionsKt.a(list), a5, third, a6, b2);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.W.c().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b2  */
    public final Object a(a aVar, String str, SleepSessionsRepository sleepSessionsRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, ThirdPartyRepository thirdPartyRepository, PortfolioApp portfolioApp, yk2 yk2, kc4<? super cb4> kc4) {
        List<HeartRateSample> e;
        List<WorkoutSession> k;
        String str2 = str;
        String str3 = "Save sync result - size of sleepSessions=" + aVar.i().size() + ", size of sampleRaws=" + aVar.h().size() + ", realTimeSteps=" + aVar.g();
        FLogger.INSTANCE.getLocal().i(a, str3);
        a(str2, str3);
        if (cg4.a(PortfolioApp.W.c().e()) || !cg4.b(PortfolioApp.W.c().e(), str2, true)) {
            FLogger.INSTANCE.getLocal().e(a, "Error inside " + a + ".saveSyncResult - Sync data does not match any user's device");
            return cb4.a;
        }
        a(str2, "Saving sleep data");
        try {
            try {
                sleepSessionsRepository.insertFromDevice(aVar.i());
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
            a(str2, "Saving sleep data. error=" + e.getMessage());
            a(str2, "Saving sleep data. OK");
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
            a(str2, "Saving activity data");
            sh4 unused = mg4.a(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon2(aVar, thirdPartyRepository, (kc4) null), 3, (Object) null);
            yk2.a(new Date(), aVar.g());
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
            try {
                activitiesRepository.insertFromDevice(aVar.h());
                a(str2, "Saving activity data. OK");
            } catch (Exception e4) {
                e = e4;
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
            summariesRepository.insertFromDevice(aVar.j());
            a(str2, "Saving activity summaries data. OK");
            e = aVar.e();
            if (!(!e.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            try {
                heartRateSummaryRepository.insertFromDevice(aVar.f());
                a(str2, "Saving heartrate summaries data. OK");
            } catch (Exception e5) {
                e = e5;
            }
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            sh4 unused2 = mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (kc4) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cb4.a;
        }
        a(str2, "Saving sleep data. OK");
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
        a(str2, "Saving activity data");
        sh4 unused3 = mg4.a(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon2(aVar, thirdPartyRepository, (kc4) null), 3, (Object) null);
        yk2.a(new Date(), aVar.g());
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
        try {
            activitiesRepository.insertFromDevice(aVar.h());
            a(str2, "Saving activity data. OK");
        } catch (Exception e6) {
            e = e6;
            ActivitiesRepository activitiesRepository2 = activitiesRepository;
            a(str2, "Saving activity data. error=" + e.getMessage());
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
            summariesRepository.insertFromDevice(aVar.j());
            a(str2, "Saving activity summaries data. OK");
            e = aVar.e();
            if (!(!e.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            sh4 unused4 = mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (kc4) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cb4.a;
        }
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
        try {
            try {
                summariesRepository.insertFromDevice(aVar.j());
                a(str2, "Saving activity summaries data. OK");
            } catch (Exception e7) {
                e = e7;
            }
        } catch (Exception e8) {
            e = e8;
            SummariesRepository summariesRepository2 = summariesRepository;
            a(str2, "Saving activity summaries data. error=" + e.getMessage());
            e = aVar.e();
            if (!(!e.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            sh4 unused5 = mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (kc4) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cb4.a;
        }
        e = aVar.e();
        if (!(!e.isEmpty())) {
            heartRateSampleRepository.insertFromDevice(e);
            a(str2, "Saving heart rate data. OK");
        } else {
            HeartRateSampleRepository heartRateSampleRepository2 = heartRateSampleRepository;
            a(str2, "No heart rate data.");
        }
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
        try {
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
        } catch (Exception e9) {
            e = e9;
            HeartRateSummaryRepository heartRateSummaryRepository2 = heartRateSummaryRepository;
            a(str2, "Saving heartrate summaries data. error=" + e.getMessage());
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            sh4 unused6 = mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (kc4) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cb4.a;
        }
        k = aVar.k();
        if (true ^ k.isEmpty()) {
            workoutSessionRepository.insertFromDevice(k);
            a(str2, "Saving workout sessions. OK");
        } else {
            WorkoutSessionRepository workoutSessionRepository2 = workoutSessionRepository;
            a(str2, "No workout sessions data.");
        }
        sh4 unused7 = mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new DianaSyncDataProcessing$saveSyncResult$Anon3(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (kc4) null), 3, (Object) null);
        FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
        portfolioApp.b(str2);
        return cb4.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0271  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0281  */
    public final List<GFitWorkoutSession> b(List<FitnessDataWrapper> list) {
        ArrayList arrayList;
        Object obj;
        int i;
        Iterator it;
        Pair pair;
        long j;
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it2 = list.iterator();
        while (it2.hasNext()) {
            Iterator<T> it3 = ((FitnessDataWrapper) it2.next()).getWorkouts().iterator();
            while (it3.hasNext()) {
                WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) it3.next();
                DateTime component2 = workoutSessionWrapper.component2();
                DateTime component3 = workoutSessionWrapper.component3();
                int component6 = workoutSessionWrapper.component6();
                StepWrapper component7 = workoutSessionWrapper.component7();
                CalorieWrapper component8 = workoutSessionWrapper.component8();
                DistanceWrapper component9 = workoutSessionWrapper.component9();
                HeartRateWrapper component10 = workoutSessionWrapper.component10();
                long millis = component2.getMillis();
                long millis2 = component3.getMillis();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                ArrayList arrayList5 = new ArrayList();
                ArrayList arrayList6 = new ArrayList();
                Iterator<T> it4 = it2;
                ArrayList arrayList7 = new ArrayList();
                Iterator<T> it5 = it3;
                int size = component7.getValues().size();
                long j2 = millis2;
                int i2 = 0;
                while (i2 < size) {
                    i2++;
                    arrayList4.add(new GFitWOStep(component7.getValues().get(i2).shortValue(), millis + (((long) (component7.getResolutionInSecond() * i2)) * 1000), millis + (((long) (component7.getResolutionInSecond() * i2)) * 1000)));
                    arrayList2 = arrayList2;
                    arrayList7 = arrayList7;
                }
                ArrayList arrayList8 = arrayList2;
                ArrayList arrayList9 = arrayList7;
                arrayList3.add(new Pair(Integer.valueOf(size), Long.valueOf(((long) component7.getResolutionInSecond()) * 1000)));
                int size2 = component8.getValues().size();
                int i3 = 0;
                while (i3 < size2) {
                    i3++;
                    arrayList5.add(new GFitWOCalorie(component8.getValues().get(i3).floatValue(), millis + (((long) (component8.getResolutionInSecond() * i3)) * 1000), millis + (((long) (component8.getResolutionInSecond() * i3)) * 1000)));
                }
                arrayList3.add(new Pair(Integer.valueOf(size2), Long.valueOf(((long) component8.getResolutionInSecond()) * 1000)));
                int size3 = component9.getValues().size();
                int i4 = 0;
                while (i4 < size3) {
                    i4++;
                    arrayList6.add(new GFitWODistance((float) component9.getValues().get(i4).doubleValue(), millis + (((long) (component9.getResolutionInSecond() * i4)) * 1000), millis + (((long) (component9.getResolutionInSecond() * i4)) * 1000)));
                }
                arrayList3.add(new Pair(Integer.valueOf(size3), Long.valueOf(((long) component9.getResolutionInSecond()) * 1000)));
                if (component10 != null) {
                    int size4 = component10.getValues().size();
                    int i5 = 0;
                    while (i5 < size4) {
                        i5++;
                        arrayList9.add(new GFitWOHeartRate((float) component10.getValues().get(i5).shortValue(), millis + (((long) (component10.getResolutionInSecond() * i5)) * 1000), millis + (((long) (component10.getResolutionInSecond() * i5)) * 1000)));
                    }
                    arrayList = arrayList9;
                    arrayList3.add(new Pair(Integer.valueOf(size4), Long.valueOf(((long) component10.getResolutionInSecond()) * 1000)));
                } else {
                    arrayList = arrayList9;
                }
                Iterator it6 = arrayList3.iterator();
                Object obj2 = null;
                if (!it6.hasNext()) {
                    obj = null;
                } else {
                    obj = it6.next();
                    if (it6.hasNext()) {
                        int intValue = ((Number) ((Pair) obj).getFirst()).intValue();
                        do {
                            Object next = it6.next();
                            int intValue2 = ((Number) ((Pair) next).getFirst()).intValue();
                            if (intValue < intValue2) {
                                obj = next;
                                intValue = intValue2;
                            }
                        } while (it6.hasNext());
                    }
                }
                Pair pair2 = (Pair) obj;
                if (pair2 != null) {
                    Integer num = (Integer) pair2.getFirst();
                    if (num != null) {
                        i = num.intValue();
                        it = arrayList3.iterator();
                        if (it.hasNext()) {
                            obj2 = it.next();
                            if (it.hasNext()) {
                                long longValue = ((Number) ((Pair) obj2).getSecond()).longValue();
                                do {
                                    Object next2 = it.next();
                                    long longValue2 = ((Number) ((Pair) next2).getSecond()).longValue();
                                    if (longValue < longValue2) {
                                        obj2 = next2;
                                        longValue = longValue2;
                                    }
                                } while (it.hasNext());
                            }
                        }
                        pair = (Pair) obj2;
                        if (pair != null) {
                            Long l = (Long) pair.getSecond();
                            if (l != null) {
                                j = l.longValue();
                                ArrayList arrayList10 = arrayList8;
                                arrayList10.add(new GFitWorkoutSession(millis, (i > 0 || j <= 0) ? j2 : (((long) (i + 1)) * j) + millis, component6, arrayList4, arrayList5, arrayList6, arrayList));
                                it2 = it4;
                                arrayList2 = arrayList10;
                                it3 = it5;
                            }
                        }
                        j = 0;
                        ArrayList arrayList102 = arrayList8;
                        arrayList102.add(new GFitWorkoutSession(millis, (i > 0 || j <= 0) ? j2 : (((long) (i + 1)) * j) + millis, component6, arrayList4, arrayList5, arrayList6, arrayList));
                        it2 = it4;
                        arrayList2 = arrayList102;
                        it3 = it5;
                    }
                }
                i = 0;
                it = arrayList3.iterator();
                if (it.hasNext()) {
                }
                pair = (Pair) obj2;
                if (pair != null) {
                }
                j = 0;
                ArrayList arrayList1022 = arrayList8;
                arrayList1022.add(new GFitWorkoutSession(millis, (i > 0 || j <= 0) ? j2 : (((long) (i + 1)) * j) + millis, component6, arrayList4, arrayList5, arrayList6, arrayList));
                it2 = it4;
                arrayList2 = arrayList1022;
                it3 = it5;
            }
        }
        return arrayList2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v7, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v8, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v9, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v11, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Pair<List<HeartRateSample>, List<DailyHeartRateSummary>> a(List<FitnessDataWrapper> list, String str) {
        Object obj;
        Iterator<T> it;
        int i;
        long j;
        int i2;
        DailyHeartRateSummary dailyHeartRateSummary;
        DailyHeartRateSummary dailyHeartRateSummary2;
        int i3;
        List<FitnessDataWrapper> list2 = list;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list.isEmpty()) {
            return new Pair<>(arrayList, arrayList2);
        }
        int i4 = 0;
        wb4.a(list2, ic4.a(DianaSyncDataProcessing$getHeartRateData$Anon1.INSTANCE, DianaSyncDataProcessing$getHeartRateData$Anon2.INSTANCE));
        Calendar c2 = sk2.c(list2.get(0).getStartLongTime());
        c2.set(13, 0);
        c2.set(14, 0);
        wd4.a((Object) c2, SampleRaw.COLUMN_START_TIME);
        c2.setTimeZone(sk2.a(list2.get(0).getTimezoneOffsetInSecond() * 1000));
        long startLongTime = list2.get(0).getStartLongTime();
        HeartRateWrapper heartRate = list2.get(0).getHeartRate();
        long j2 = 1000;
        Calendar c3 = sk2.c(startLongTime + (((long) (heartRate != null ? heartRate.getResolutionInSecond() : 0)) * 1000));
        int i5 = c2.get(10);
        int i6 = c2.get(12);
        Date time = c2.getTime();
        wd4.a((Object) time, "startTime.time");
        long currentTimeMillis = System.currentTimeMillis();
        long currentTimeMillis2 = System.currentTimeMillis();
        DateTime dateTime = r10;
        wd4.a((Object) c3, SampleRaw.COLUMN_END_TIME);
        DateTime dateTime2 = new DateTime(c3.getTimeInMillis());
        DateTime dateTime3 = r8;
        DateTime dateTime4 = new DateTime(c2.getTimeInMillis());
        HeartRateSample heartRateSample = new HeartRateSample((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time, currentTimeMillis, currentTimeMillis2, dateTime, dateTime3, list2.get(0).getTimezoneOffsetInSecond(), Integer.MAX_VALUE, Integer.MIN_VALUE, str, 0, (Resting) null, 2048, (rd4) null);
        DailyHeartRateSummary dailyHeartRateSummary3 = new DailyHeartRateSummary(heartRateSample.getAverage(), new Date(heartRateSample.getStartTimeId().getMillis()), System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample.getMin(), heartRateSample.getMax(), heartRateSample.getMinuteCount(), heartRateSample.getResting());
        TimeZone timeZone = TimeZone.getDefault();
        wd4.a((Object) timeZone, "TimeZone.getDefault()");
        int rawOffset = timeZone.getRawOffset() / 1000;
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        Iterator<T> it2 = list.iterator();
        int i7 = i6;
        int i8 = 0;
        int i9 = 0;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i10 = i5;
        DailyHeartRateSummary dailyHeartRateSummary4 = dailyHeartRateSummary3;
        HeartRateSample heartRateSample2 = heartRateSample;
        while (it2.hasNext()) {
            T next = it2.next();
            int i11 = i8 + 1;
            if (i8 >= 0) {
                FitnessDataWrapper fitnessDataWrapper = (FitnessDataWrapper) next;
                HeartRateWrapper heartRate2 = fitnessDataWrapper.getHeartRate();
                if (heartRate2 != null) {
                    int component1 = heartRate2.component1();
                    List<Short> component3 = heartRate2.component3();
                    int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
                    it = it2;
                    long millis = fitnessDataWrapper.getResting().isEmpty() ^ true ? fitnessDataWrapper.getResting().get(i4).getStartTime().getMillis() : 0;
                    ref$ObjectRef.element = fitnessDataWrapper.getResting();
                    int i12 = i10;
                    FLogger.INSTANCE.getLocal().d(a, "Resting: restingStartTimeInSecond=" + millis + ", restingValues=" + sj2.a(ref$ObjectRef.element));
                    String[] availableIDs = TimeZone.getAvailableIDs(rawOffset * 1000);
                    Iterator<T> it3 = component3.iterator();
                    int i13 = i12;
                    DailyHeartRateSummary dailyHeartRateSummary5 = dailyHeartRateSummary4;
                    int i14 = 0;
                    while (it3.hasNext()) {
                        T next2 = it3.next();
                        int i15 = i14 + 1;
                        if (i14 >= 0) {
                            int shortValue = ((Number) next2).shortValue();
                            DailyHeartRateSummary dailyHeartRateSummary6 = dailyHeartRateSummary5;
                            Calendar c4 = sk2.c(fitnessDataWrapper.getStartLongTime() + (((long) (i14 * component1)) * 1000));
                            Iterator<T> it4 = it3;
                            c4.set(13, 0);
                            c4.set(14, 0);
                            wd4.a((Object) c4, "currentStartTime");
                            c4.setTimeZone(TimeZone.getTimeZone(availableIDs[0]));
                            Object clone = c4.clone();
                            if (clone != null) {
                                Calendar calendar = (Calendar) clone;
                                calendar.add(13, component1);
                                cb4 cb4 = cb4.a;
                                String[] strArr = availableIDs;
                                int i16 = c4.get(10);
                                int i17 = c4.get(12);
                                if (i16 == i13 && i17 / 5 == i7 / 5 && timezoneOffsetInSecond == heartRateSample2.getTimezoneOffsetInSecond()) {
                                    if (shortValue > 0) {
                                        i3 = i9 + 1;
                                        heartRateSample2.setMinuteCount(i3);
                                    } else {
                                        i3 = i9;
                                    }
                                    f += (float) shortValue;
                                    if (shortValue > 0) {
                                        if (heartRateSample2.getMin() == 0) {
                                            heartRateSample2.setMin(shortValue);
                                        } else if (shortValue < heartRateSample2.getMin()) {
                                            heartRateSample2.setMin(shortValue);
                                        }
                                    }
                                    if (shortValue > 0 && shortValue > heartRateSample2.getMax()) {
                                        heartRateSample2.setMax(shortValue);
                                    }
                                    heartRateSample2.setEndTime(new DateTime((Object) calendar.getTime()));
                                    if (i3 > 0) {
                                        heartRateSample2.setAverage(f / ((float) i3));
                                    }
                                    for (RestingWrapper restingWrapper : ref$ObjectRef.element) {
                                        if (restingWrapper.getTimezoneOffsetInSecond() == heartRateSample2.getTimezoneOffsetInSecond() && heartRateSample2.getStartTime().getMillis() <= restingWrapper.getStartTime().getMillis() && restingWrapper.getStartTime().getMillis() <= heartRateSample2.getEndTime().getMillis()) {
                                            heartRateSample2.setResting(new Resting(heartRateSample2.getEndTime(), restingWrapper.getValue()));
                                        }
                                    }
                                    i9 = i3;
                                    dailyHeartRateSummary5 = dailyHeartRateSummary6;
                                    i2 = rawOffset;
                                } else {
                                    if (heartRateSample2.getAverage() > ((float) 0)) {
                                        DateTime minusMinutes = heartRateSample2.getStartTimeId().minusMinutes(heartRateSample2.getStartTimeId().getMinuteOfHour() % 5);
                                        wd4.a((Object) minusMinutes, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
                                        heartRateSample2.setStartTimeId(minusMinutes);
                                        arrayList.add(heartRateSample2);
                                        if (sk2.d(dailyHeartRateSummary6.getDate(), heartRateSample2.getStartTimeId().toLocalDateTime().toDate())) {
                                            dailyHeartRateSummary = b.a(dailyHeartRateSummary6, heartRateSample2);
                                        } else {
                                            DailyHeartRateSummary dailyHeartRateSummary7 = dailyHeartRateSummary6;
                                            Iterator it5 = arrayList2.iterator();
                                            while (true) {
                                                if (!it5.hasNext()) {
                                                    dailyHeartRateSummary2 = null;
                                                    break;
                                                }
                                                Object next3 = it5.next();
                                                Iterator it6 = it5;
                                                dailyHeartRateSummary2 = next3;
                                                if (sk2.d(next3.getDate(), heartRateSample2.getDate())) {
                                                    break;
                                                }
                                                it5 = it6;
                                            }
                                            DailyHeartRateSummary dailyHeartRateSummary8 = dailyHeartRateSummary2;
                                            if (dailyHeartRateSummary8 != null) {
                                                arrayList2.remove(dailyHeartRateSummary8);
                                                dailyHeartRateSummary = b.a(dailyHeartRateSummary8, heartRateSample2);
                                            } else {
                                                arrayList2.add(dailyHeartRateSummary7);
                                                float average = heartRateSample2.getAverage();
                                                Date date = heartRateSample2.getStartTimeId().toLocalDateTime().toDate();
                                                wd4.a((Object) date, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                                                dailyHeartRateSummary = new DailyHeartRateSummary(average, date, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample2.getMin(), heartRateSample2.getMax(), heartRateSample2.getMinuteCount(), heartRateSample2.getResting());
                                            }
                                        }
                                    } else {
                                        dailyHeartRateSummary = dailyHeartRateSummary6;
                                    }
                                    int i18 = c4.get(10);
                                    i7 = c4.get(12);
                                    float f2 = (float) shortValue;
                                    i9 = shortValue > 0 ? 1 : 0;
                                    Date time2 = c4.getTime();
                                    DailyHeartRateSummary dailyHeartRateSummary9 = dailyHeartRateSummary;
                                    wd4.a((Object) time2, "currentStartTime.time");
                                    long currentTimeMillis3 = System.currentTimeMillis();
                                    long currentTimeMillis4 = System.currentTimeMillis();
                                    int i19 = i18;
                                    int i20 = timezoneOffsetInSecond / 3600;
                                    DateTime withZone = new DateTime(calendar.getTimeInMillis()).withZone(DateTimeZone.forOffsetHours(i20));
                                    DateTime dateTime5 = withZone;
                                    wd4.a((Object) withZone, "DateTime(currentEndTime.\u2026neOffsetInSecond / 3600))");
                                    i2 = rawOffset;
                                    DateTime withZone2 = new DateTime(c4.getTimeInMillis()).withZone(DateTimeZone.forOffsetHours(i20));
                                    wd4.a((Object) withZone2, "DateTime(currentStartTim\u2026neOffsetInSecond / 3600))");
                                    HeartRateSample heartRateSample3 = new HeartRateSample(f2, time2, currentTimeMillis3, currentTimeMillis4, dateTime5, withZone2, timezoneOffsetInSecond, shortValue, shortValue, str, i9, (Resting) null, 2048, (rd4) null);
                                    FLogger.INSTANCE.getLocal().d(a, "getHeartRateData startTime=" + c4.getTime() + " endTime=" + calendar.getTime());
                                    dailyHeartRateSummary5 = dailyHeartRateSummary9;
                                    i13 = i19;
                                    f = f2;
                                    heartRateSample2 = heartRateSample3;
                                }
                                cb4 cb42 = cb4.a;
                                i14 = i15;
                                it3 = it4;
                                rawOffset = i2;
                                availableIDs = strArr;
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.util.Calendar");
                            }
                        } else {
                            ob4.c();
                            throw null;
                        }
                    }
                    i = rawOffset;
                    j = 1000;
                    cb4 cb43 = cb4.a;
                    dailyHeartRateSummary4 = dailyHeartRateSummary5;
                    i10 = i13;
                } else {
                    it = it2;
                    i = rawOffset;
                    DailyHeartRateSummary dailyHeartRateSummary10 = dailyHeartRateSummary4;
                    j = j2;
                    int i21 = i10;
                }
                it2 = it;
                i8 = i11;
                j2 = j;
                rawOffset = i;
                i4 = 0;
            } else {
                ob4.c();
                throw null;
            }
        }
        DailyHeartRateSummary dailyHeartRateSummary11 = dailyHeartRateSummary4;
        float f3 = (float) 0;
        if (heartRateSample2.getAverage() > f3) {
            DateTime minusMinutes2 = heartRateSample2.getStartTimeId().minusMinutes(heartRateSample2.getStartTimeId().getMinuteOfHour() % 5);
            wd4.a((Object) minusMinutes2, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
            heartRateSample2.setStartTimeId(minusMinutes2);
            arrayList.add(heartRateSample2);
            if (sk2.d(dailyHeartRateSummary11.getDate(), heartRateSample2.getStartTimeId().toLocalDateTime().toDate())) {
                arrayList2.add(a(dailyHeartRateSummary11, heartRateSample2));
            } else {
                DailyHeartRateSummary dailyHeartRateSummary12 = dailyHeartRateSummary11;
                Iterator it7 = arrayList2.iterator();
                while (true) {
                    if (!it7.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it7.next();
                    if (sk2.d(((DailyHeartRateSummary) obj).getDate(), heartRateSample2.getDate())) {
                        break;
                    }
                }
                DailyHeartRateSummary dailyHeartRateSummary13 = (DailyHeartRateSummary) obj;
                if (dailyHeartRateSummary13 != null) {
                    arrayList2.remove(dailyHeartRateSummary13);
                    arrayList2.add(a(dailyHeartRateSummary13, heartRateSample2));
                } else {
                    arrayList2.add(dailyHeartRateSummary12);
                    float average2 = heartRateSample2.getAverage();
                    Date date2 = heartRateSample2.getStartTimeId().toLocalDateTime().toDate();
                    wd4.a((Object) date2, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                    arrayList2.add(new DailyHeartRateSummary(average2, date2, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample2.getMin(), heartRateSample2.getMax(), heartRateSample2.getMinuteCount(), heartRateSample2.getResting()));
                }
            }
        } else {
            DailyHeartRateSummary dailyHeartRateSummary14 = dailyHeartRateSummary11;
            if (dailyHeartRateSummary14.getAverage() > f3) {
                arrayList2.add(dailyHeartRateSummary14);
            }
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "heartrate " + arrayList + " \n summary " + arrayList2);
        return new Pair<>(arrayList, arrayList2);
    }

    @DexIgnore
    public final List<GFitHeartRate> a(List<FitnessDataWrapper> list) {
        FLogger.INSTANCE.getLocal().d(a, "getGFitHeartRates");
        ArrayList arrayList = new ArrayList();
        wb4.a(list, new b());
        for (FitnessDataWrapper fitnessDataWrapper : list) {
            int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
            HeartRateWrapper heartRate = fitnessDataWrapper.getHeartRate();
            if (heartRate != null) {
                int component1 = heartRate.component1();
                int i = 0;
                for (T next : heartRate.component3()) {
                    int i2 = i + 1;
                    if (i >= 0) {
                        short shortValue = ((Number) next).shortValue();
                        Calendar calendar = new DateTime(fitnessDataWrapper.getStartLongTime() + (((long) (i * component1)) * 1000), DateTimeZone.forOffsetMillis(timezoneOffsetInSecond * 1000)).toCalendar(Locale.US);
                        calendar.set(13, 0);
                        calendar.set(14, 0);
                        Object clone = calendar.clone();
                        if (clone != null) {
                            Calendar calendar2 = (Calendar) clone;
                            calendar2.add(13, component1);
                            wd4.a((Object) calendar, "currentStartTime");
                            arrayList.add(new GFitHeartRate((float) shortValue, calendar.getTimeInMillis(), calendar2.getTimeInMillis()));
                            i = i2;
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.util.Calendar");
                        }
                    } else {
                        ob4.c();
                        throw null;
                    }
                }
                continue;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final DailyHeartRateSummary a(DailyHeartRateSummary dailyHeartRateSummary, HeartRateSample heartRateSample) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "calculateHeartRateSummary - summary=" + dailyHeartRateSummary + ", sample=" + heartRateSample);
        int minuteCount = dailyHeartRateSummary.getMinuteCount() + heartRateSample.getMinuteCount();
        return new DailyHeartRateSummary(((dailyHeartRateSummary.getAverage() * ((float) dailyHeartRateSummary.getMinuteCount())) + (heartRateSample.getAverage() * ((float) heartRateSample.getMinuteCount()))) / ((float) minuteCount), dailyHeartRateSummary.getDate(), dailyHeartRateSummary.getCreatedAt(), System.currentTimeMillis(), Math.min(dailyHeartRateSummary.getMin(), heartRateSample.getMin()), Math.max(dailyHeartRateSummary.getMax(), heartRateSample.getMax()), minuteCount, heartRateSample.getResting() != null ? heartRateSample.getResting() : dailyHeartRateSummary.getResting());
    }

    @DexIgnore
    public final List<WorkoutSession> a(List<FitnessDataWrapper> list, String str, String str2) {
        FLogger.INSTANCE.getLocal().d(a, "getWorkoutData");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        wb4.a(list, new c());
        for (FitnessDataWrapper workouts : list) {
            for (WorkoutSessionWrapper workoutSessionWrapper : workouts.getWorkouts()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local.d(str3, "getWorkoutData - value=" + workoutSessionWrapper);
                arrayList.add(new WorkoutSession(workoutSessionWrapper, str, str2));
            }
        }
        return arrayList;
    }
}
