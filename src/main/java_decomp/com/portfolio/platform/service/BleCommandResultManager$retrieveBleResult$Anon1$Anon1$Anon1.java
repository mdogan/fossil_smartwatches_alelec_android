package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.portfolio.platform.service.BleCommandResultManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
public final class BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.a $bleResult;
    @DexIgnore
    public /* final */ /* synthetic */ CommunicateMode $communicateMode;
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.d $observerList;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1(BleCommandResultManager.d dVar, CommunicateMode communicateMode, BleCommandResultManager.a aVar, kc4 kc4) {
        super(2, kc4);
        this.$observerList = dVar;
        this.$communicateMode = communicateMode;
        this.$bleResult = aVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 = new BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1(this.$observerList, this.$communicateMode, this.$bleResult, kc4);
        bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1.p$ = (lh4) obj;
        return bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            for (BleCommandResultManager.b a : this.$observerList.a()) {
                a.a(this.$communicateMode, this.$bleResult.a());
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
