package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $this_run;
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1(MisfitDeviceProfile misfitDeviceProfile, kc4 kc4, MFDeviceService mFDeviceService, String str) {
        super(2, kc4);
        this.$this_run = misfitDeviceProfile;
        this.this$Anon0 = mFDeviceService;
        this.$serial$inlined = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1 mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1 = new MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1(this.$this_run, kc4, this.this$Anon0, this.$serial$inlined);
        mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1.p$ = (lh4) obj;
        return mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            Device deviceBySerial = this.this$Anon0.d().getDeviceBySerial(this.$serial$inlined);
            int b = zk2.b(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = MFDeviceService.U.b();
            local.d(b2, "newVibrationLvl: " + b + " - device: " + deviceBySerial);
            if (deviceBySerial != null) {
                Integer vibrationStrength = deviceBySerial.getVibrationStrength();
                if (vibrationStrength == null || vibrationStrength.intValue() != b) {
                    deviceBySerial.setVibrationStrength(pc4.a(b));
                    DeviceRepository d = this.this$Anon0.d();
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = deviceBySerial;
                    this.I$Anon0 = b;
                    this.label = 1;
                    if (d.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            }
        } else if (i == 1) {
            Device device = (Device) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
