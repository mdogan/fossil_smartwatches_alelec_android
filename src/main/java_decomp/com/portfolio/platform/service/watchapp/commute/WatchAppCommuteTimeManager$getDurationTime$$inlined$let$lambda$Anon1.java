package com.portfolio.platform.service.watchapp.commute;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<TrafficResponse>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kc4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Location $location$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1(TrafficRequest trafficRequest, kc4 kc4, WatchAppCommuteTimeManager watchAppCommuteTimeManager, Location location, kc4 kc42) {
        super(1, kc4);
        this.$trafficRequest = trafficRequest;
        this.this$Anon0 = watchAppCommuteTimeManager;
        this.$location$inlined = location;
        this.$continuation$inlined = kc42;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1(this.$trafficRequest, kc4, this.this$Anon0, this.$location$inlined, this.$continuation$inlined);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchAppCommuteTimeManager$getDurationTime$$inlined$let$lambda$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 a2 = this.this$Anon0.a();
            TrafficRequest trafficRequest = this.$trafficRequest;
            this.label = 1;
            obj = a2.getTrafficStatus(trafficRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
