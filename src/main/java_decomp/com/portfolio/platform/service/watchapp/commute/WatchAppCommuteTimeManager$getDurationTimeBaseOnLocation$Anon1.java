package com.portfolio.platform.service.watchapp.commute;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {153, 171}, m = "getDurationTimeBaseOnLocation")
public final class WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$Anon1(WatchAppCommuteTimeManager watchAppCommuteTimeManager, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((kc4<? super cb4>) this);
    }
}
