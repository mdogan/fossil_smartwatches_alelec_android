package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.CoroutineUseCase$onError$Anon1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
public final class CoroutineUseCase$onError$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.a $errorValue;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$onError$Anon1(CoroutineUseCase coroutineUseCase, CoroutineUseCase.a aVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = coroutineUseCase;
        this.$errorValue = aVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CoroutineUseCase$onError$Anon1 coroutineUseCase$onError$Anon1 = new CoroutineUseCase$onError$Anon1(this.this$Anon0, this.$errorValue, kc4);
        coroutineUseCase$onError$Anon1.p$ = (lh4) obj;
        return coroutineUseCase$onError$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CoroutineUseCase$onError$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            FLogger.INSTANCE.getLocal().d(this.this$Anon0.b, "Trigger UseCase callback failed");
            CoroutineUseCase.e a = this.this$Anon0.a;
            if (a != null) {
                a.a(this.$errorValue);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
