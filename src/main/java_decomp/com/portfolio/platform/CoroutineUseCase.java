package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase.a;
import com.portfolio.platform.CoroutineUseCase.b;
import com.portfolio.platform.CoroutineUseCase.d;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class CoroutineUseCase<Q extends b, R extends d, E extends a> {
    @DexIgnore
    public e<? super R, ? super E> a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ lh4 c; // = mh4.a(zh4.a());

    @DexIgnore
    public interface a extends c {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d extends c {
    }

    @DexIgnore
    public interface e<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore
    public abstract Object a(Q q, kc4<Object> kc4);

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final lh4 b() {
        return this.c;
    }

    @DexIgnore
    public final ri4 a(Q q, e<? super R, ? super E> eVar) {
        return mg4.b(this.c, (CoroutineContext) null, (CoroutineStart) null, new CoroutineUseCase$executeUseCase$Anon1(this, eVar, q, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final e<R, E> a() {
        return this.a;
    }

    @DexIgnore
    public final void a(e<? super R, ? super E> eVar) {
        wd4.b(eVar, Constants.CALLBACK);
        this.a = eVar;
    }

    @DexIgnore
    public final ri4 a(R r) {
        wd4.b(r, "response");
        return mg4.b(this.c, zh4.c(), (CoroutineStart) null, new CoroutineUseCase$onSuccess$Anon1(this, r, (kc4) null), 2, (Object) null);
    }

    @DexIgnore
    public final ri4 a(E e2) {
        wd4.b(e2, "errorValue");
        return mg4.b(this.c, zh4.c(), (CoroutineStart) null, new CoroutineUseCase$onError$Anon1(this, e2, (kc4) null), 2, (Object) null);
    }
}
