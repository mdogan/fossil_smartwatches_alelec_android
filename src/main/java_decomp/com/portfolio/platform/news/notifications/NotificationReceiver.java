package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationReceiver extends BroadcastReceiver {
    @DexIgnore
    public fn2 a;
    @DexIgnore
    public wj2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        if (en2.p.a().n().b() == null) {
            FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
            return;
        }
        int intExtra = intent != null ? intent.getIntExtra("ACTION_EVENT", 0) : 0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("NotificationReceiver", "onReceive - actionEvent=" + intExtra);
        if (intExtra == 1) {
            PortfolioApp c = PortfolioApp.W.c();
            wj2 wj2 = this.b;
            if (wj2 != null) {
                c.a(wj2, false, 10);
            } else {
                wd4.d("mDeviceSettingFactory");
                throw null;
            }
        }
    }
}
