package com.portfolio.platform.news.notifications;

import android.content.Context;
import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$Anon1", f = "FossilNotificationBar.kt", l = {38}, m = "invokeSuspend")
public final class FossilNotificationBar$Companion$updateData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$Anon1$Anon1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationBar $fossilNotificationBar;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationBar$Companion$updateData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(FossilNotificationBar$Companion$updateData$Anon1 fossilNotificationBar$Companion$updateData$Anon1, FossilNotificationBar fossilNotificationBar, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = fossilNotificationBar$Companion$updateData$Anon1;
            this.$fossilNotificationBar = fossilNotificationBar;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$fossilNotificationBar, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                FossilNotificationBar.Companion.a(FossilNotificationBar.c, this.this$Anon0.$context, this.$fossilNotificationBar, false, 4, (Object) null);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationBar$Companion$updateData$Anon1(Context context, kc4 kc4) {
        super(2, kc4);
        this.$context = context;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        FossilNotificationBar$Companion$updateData$Anon1 fossilNotificationBar$Companion$updateData$Anon1 = new FossilNotificationBar$Companion$updateData$Anon1(this.$context, kc4);
        fossilNotificationBar$Companion$updateData$Anon1.p$ = (lh4) obj;
        return fossilNotificationBar$Companion$updateData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FossilNotificationBar$Companion$updateData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            String d = PortfolioApp.W.c().d();
            FossilNotificationBar fossilNotificationBar = new FossilNotificationBar(d, (String) null, 2, (rd4) null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "content " + d);
            bj4 c = zh4.c();
            Anon1 anon1 = new Anon1(this, fossilNotificationBar, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = d;
            this.L$Anon2 = fossilNotificationBar;
            this.label = 1;
            if (kg4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            FossilNotificationBar fossilNotificationBar2 = (FossilNotificationBar) this.L$Anon2;
            String str = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
