package com.portfolio.platform.glide;

import android.content.Context;
import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.ak2;
import com.fossil.blesdk.obfuscated.av;
import com.fossil.blesdk.obfuscated.bk2;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.hk2;
import com.fossil.blesdk.obfuscated.ik2;
import com.fossil.blesdk.obfuscated.sn;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zj2;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioGlideModule extends av {
    @DexIgnore
    public void a(Context context, sn snVar, Registry registry) {
        wd4.b(context, "context");
        wd4.b(snVar, "glide");
        wd4.b(registry, "registry");
        super.a(context, snVar, registry);
        registry.a(ak2.class, InputStream.class, new zj2.b());
        registry.a(ck2.class, InputStream.class, new bk2.b());
        registry.a(ik2.class, InputStream.class, new hk2.b());
    }

    @DexIgnore
    public boolean a() {
        return false;
    }
}
