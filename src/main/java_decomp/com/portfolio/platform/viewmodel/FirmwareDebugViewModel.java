package com.portfolio.platform.viewmodel;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FirmwareDebugViewModel extends jc {
    @DexIgnore
    public /* final */ MutableLiveData<List<DebugFirmwareData>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> d; // = new MutableLiveData<>();

    @DexIgnore
    public final ri4 a(jd4<? super kc4<? super List<DebugFirmwareData>>, ? extends Object> jd4) {
        wd4.b(jd4, "block");
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new FirmwareDebugViewModel$loadFirmware$Anon1(this, jd4, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> c() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> d() {
        return this.d;
    }

    @DexIgnore
    public final boolean e() {
        if (this.c.a() != null) {
            List<DebugFirmwareData> a = this.c.a();
            if (a != null) {
                wd4.a((Object) a, "firmwares.value!!");
                if (!a.isEmpty()) {
                    return true;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final void f() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.c;
        mutableLiveData.a(mutableLiveData.a());
        MutableLiveData<Firmware> mutableLiveData2 = this.d;
        mutableLiveData2.a(mutableLiveData2.a());
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        wd4.b(firmware, "firmware");
        this.d.a(firmware);
    }
}
