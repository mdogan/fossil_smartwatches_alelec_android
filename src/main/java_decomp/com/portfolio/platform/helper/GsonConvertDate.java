package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonConvertDate implements wz1<Date> {
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057 A[SYNTHETIC, Splitter:B:18:0x0057] */
    public Date deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        wd4.b(jsonElement, "json");
        wd4.b(type, "typeOfT");
        wd4.b(vz1, "context");
        String f = jsonElement.f();
        wd4.a((Object) f, "dateAsString");
        if (f.length() == 0) {
            return new Date(0);
        }
        Date date = sk2.a(DateTimeZone.getDefault(), f).toDate();
        wd4.a((Object) date, "DateHelper.getServerDate\u2026), dateAsString).toDate()");
        try {
            SimpleDateFormat simpleDateFormat = sk2.a.get();
            if (simpleDateFormat == null) {
                Date parse = simpleDateFormat.parse(jsonElement.f());
                if (parse != null) {
                    return parse;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GsonConvertDate", "deserialize - json=" + jsonElement.f() + ", e=" + e);
            e.printStackTrace();
            return new Date(0);
        }
    }
}
