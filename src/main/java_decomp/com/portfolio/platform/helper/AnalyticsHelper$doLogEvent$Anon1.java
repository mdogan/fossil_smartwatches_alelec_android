package com.portfolio.platform.helper;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$Anon1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
public final class AnalyticsHelper$doLogEvent$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle $data;
    @DexIgnore
    public /* final */ /* synthetic */ String $event;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnalyticsHelper$doLogEvent$Anon1(String str, Bundle bundle, kc4 kc4) {
        super(2, kc4);
        this.$event = str;
        this.$data = bundle;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        AnalyticsHelper$doLogEvent$Anon1 analyticsHelper$doLogEvent$Anon1 = new AnalyticsHelper$doLogEvent$Anon1(this.$event, this.$data, kc4);
        analyticsHelper$doLogEvent$Anon1.p$ = (lh4) obj;
        return analyticsHelper$doLogEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AnalyticsHelper$doLogEvent$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            FirebaseAnalytics d = AnalyticsHelper.c;
            if (d != null) {
                d.a(this.$event, this.$data);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
