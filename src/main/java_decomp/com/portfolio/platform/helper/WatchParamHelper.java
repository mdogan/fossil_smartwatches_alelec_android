package com.portfolio.platform.helper;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParamHelper {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public /* final */ DeviceRepository a;
    @DexIgnore
    public /* final */ PortfolioApp b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = WatchParamHelper.class.getSimpleName();
        wd4.a((Object) simpleName, "WatchParamHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public WatchParamHelper(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(portfolioApp, "mApp");
        this.a = deviceRepository;
        this.b = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(String str, float f, WatchParameterResponse watchParameterResponse, kc4<? super cb4> kc4) {
        WatchParamHelper$handleSuccessResponseWatchParam$Anon1 watchParamHelper$handleSuccessResponseWatchParam$Anon1;
        int i;
        String str2;
        WatchParam watchParam;
        WatchParamHelper watchParamHelper;
        String versionMajor;
        if (kc4 instanceof WatchParamHelper$handleSuccessResponseWatchParam$Anon1) {
            watchParamHelper$handleSuccessResponseWatchParam$Anon1 = (WatchParamHelper$handleSuccessResponseWatchParam$Anon1) kc4;
            int i2 = watchParamHelper$handleSuccessResponseWatchParam$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchParamHelper$handleSuccessResponseWatchParam$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchParamHelper$handleSuccessResponseWatchParam$Anon1.result;
                Object a2 = oc4.a();
                i = watchParamHelper$handleSuccessResponseWatchParam$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    local.d(str3, "handleSuccessResponseWatchParam, response=" + watchParameterResponse + ", currentWPVersion=" + f);
                    WatchParam watchParamModel = watchParameterResponse.toWatchParamModel(str);
                    DeviceRepository deviceRepository = this.a;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon0 = this;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon1 = str;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.F$Anon0 = f;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon2 = watchParameterResponse;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon3 = watchParamModel;
                    watchParamHelper$handleSuccessResponseWatchParam$Anon1.label = 1;
                    if (deviceRepository.saveWatchParamModel(watchParamModel, watchParamHelper$handleSuccessResponseWatchParam$Anon1) == a2) {
                        return a2;
                    }
                    watchParamHelper = this;
                    str2 = str;
                    watchParam = watchParamModel;
                } else if (i == 1) {
                    watchParam = (WatchParam) watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon3;
                    WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon2;
                    f = watchParamHelper$handleSuccessResponseWatchParam$Anon1.F$Anon0;
                    str2 = (String) watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon1;
                    watchParamHelper = (WatchParamHelper) watchParamHelper$handleSuccessResponseWatchParam$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                versionMajor = watchParam.getVersionMajor();
                if (versionMajor == null) {
                    int parseInt = Integer.parseInt(versionMajor);
                    String versionMinor = watchParam.getVersionMinor();
                    if (versionMinor != null) {
                        if (f < watchParamHelper.a(parseInt, Integer.parseInt(versionMinor))) {
                            FLogger.INSTANCE.getLocal().d(c, "Need to update newer WP version from response");
                            String data = watchParam.getData();
                            PortfolioApp portfolioApp = watchParamHelper.b;
                            if (data != null) {
                                portfolioApp.a(str2, true, new WatchParamsFileMapping(data));
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().d(c, "No need to update WP version in device, it's the latest one");
                            watchParamHelper.b.a(str2, true, (WatchParamsFileMapping) null);
                        }
                        return cb4.a;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
        }
        watchParamHelper$handleSuccessResponseWatchParam$Anon1 = new WatchParamHelper$handleSuccessResponseWatchParam$Anon1(this, kc4);
        Object obj2 = watchParamHelper$handleSuccessResponseWatchParam$Anon1.result;
        Object a22 = oc4.a();
        i = watchParamHelper$handleSuccessResponseWatchParam$Anon1.label;
        if (i != 0) {
        }
        versionMajor = watchParam.getVersionMajor();
        if (versionMajor == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(String str, float f, kc4<? super cb4> kc4) {
        WatchParamHelper$handleFailureResponseWatchParam$Anon1 watchParamHelper$handleFailureResponseWatchParam$Anon1;
        int i;
        WatchParamHelper watchParamHelper;
        WatchParam watchParam;
        if (kc4 instanceof WatchParamHelper$handleFailureResponseWatchParam$Anon1) {
            watchParamHelper$handleFailureResponseWatchParam$Anon1 = (WatchParamHelper$handleFailureResponseWatchParam$Anon1) kc4;
            int i2 = watchParamHelper$handleFailureResponseWatchParam$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchParamHelper$handleFailureResponseWatchParam$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchParamHelper$handleFailureResponseWatchParam$Anon1.result;
                Object a2 = oc4.a();
                i = watchParamHelper$handleFailureResponseWatchParam$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = c;
                    local.d(str2, "handleFailureResponseWatchParam, currentWPVersion=" + f);
                    DeviceRepository deviceRepository = this.a;
                    watchParamHelper$handleFailureResponseWatchParam$Anon1.L$Anon0 = this;
                    watchParamHelper$handleFailureResponseWatchParam$Anon1.L$Anon1 = str;
                    watchParamHelper$handleFailureResponseWatchParam$Anon1.F$Anon0 = f;
                    watchParamHelper$handleFailureResponseWatchParam$Anon1.label = 1;
                    obj = deviceRepository.getWatchParamBySerialId(str, watchParamHelper$handleFailureResponseWatchParam$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                    watchParamHelper = this;
                } else if (i == 1) {
                    f = watchParamHelper$handleFailureResponseWatchParam$Anon1.F$Anon0;
                    str = (String) watchParamHelper$handleFailureResponseWatchParam$Anon1.L$Anon1;
                    watchParamHelper = (WatchParamHelper) watchParamHelper$handleFailureResponseWatchParam$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                watchParam = (WatchParam) obj;
                if (watchParam == null) {
                    String versionMajor = watchParam.getVersionMajor();
                    if (versionMajor != null) {
                        int parseInt = Integer.parseInt(versionMajor);
                        String versionMinor = watchParam.getVersionMinor();
                        if (versionMinor == null) {
                            wd4.a();
                            throw null;
                        } else if (f < watchParamHelper.a(parseInt, Integer.parseInt(versionMinor))) {
                            FLogger.INSTANCE.getLocal().d(c, "Newer version is available in database, set to device");
                            String data = watchParam.getData();
                            PortfolioApp portfolioApp = watchParamHelper.b;
                            if (data != null) {
                                portfolioApp.a(str, true, new WatchParamsFileMapping(data));
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().d(c, "The saved version in database is older than the current one, skip it");
                            watchParamHelper.b.a(str, true, (WatchParamsFileMapping) null);
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database, but keep going because the current version is not empty");
                    watchParamHelper.b.a(str, true, (WatchParamsFileMapping) null);
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database and the current version is empty -> notify failed");
                    watchParamHelper.b.a(str, false, (WatchParamsFileMapping) null);
                }
                return cb4.a;
            }
        }
        watchParamHelper$handleFailureResponseWatchParam$Anon1 = new WatchParamHelper$handleFailureResponseWatchParam$Anon1(this, kc4);
        Object obj2 = watchParamHelper$handleFailureResponseWatchParam$Anon1.result;
        Object a22 = oc4.a();
        i = watchParamHelper$handleFailureResponseWatchParam$Anon1.label;
        if (i != 0) {
        }
        watchParam = (WatchParam) obj2;
        if (watchParam == null) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final float a(int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append('.');
        sb.append(i2);
        return Float.parseFloat(sb.toString());
    }
}
