package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GsonConverterShortDateTime implements wz1<DateTime> {
    @DexIgnore
    public DateTime deserialize(JsonElement jsonElement, Type type, vz1 vz1) throws JsonParseException {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return sk2.b(f);
    }
}
