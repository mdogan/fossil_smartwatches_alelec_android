package com.portfolio.platform.helper;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.cl2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.g62;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.SecondTimezoneRaw;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.FossilBrand;
import com.zendesk.sdk.network.Constants;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppHelper {
    @DexIgnore
    public static AppHelper d;
    @DexIgnore
    public static /* final */ ArrayList<Ringtone> e; // = new ArrayList<>();
    @DexIgnore
    public static /* final */ Companion f; // = new Companion((rd4) null);
    @DexIgnore
    public fn2 a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public UserRepository c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final void a(AppHelper appHelper) {
            AppHelper.d = appHelper;
        }

        @DexIgnore
        public final ArrayList<a> b(Context context) {
            wd4.b(context, "context");
            return a(context);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0058, code lost:
            if (r0 != null) goto L_0x005a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x005a, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x007b, code lost:
            if (r0 == null) goto L_0x007e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0082, code lost:
            return b();
         */
        @DexIgnore
        public final List<Ringtone> c() {
            FLogger.INSTANCE.getLocal().d("AppHelper", "getListRingtoneFromRingtonePhone");
            if (!b().isEmpty()) {
                return b();
            }
            Cursor cursor = null;
            try {
                RingtoneManager ringtoneManager = new RingtoneManager(PortfolioApp.W.c().getApplicationContext());
                ringtoneManager.setType(1);
                cursor = ringtoneManager.getCursor();
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        String string = cursor.getString(1);
                        String string2 = cursor.getString(0);
                        ArrayList<Ringtone> b = b();
                        wd4.a((Object) string, "title");
                        b.add(new Ringtone(string, string2));
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("AppHelper", "getListRingtoneFromRingtonePhone - Exception=" + e);
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        @DexIgnore
        public final AppHelper d() {
            return AppHelper.d;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:29:0x009b, code lost:
            if (r3 != null) goto L_0x0078;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0098  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00a8  */
        public final ArrayList<SecondTimezoneSetting> e() {
            BufferedReader bufferedReader;
            InputStream inputStream;
            ArrayList<SecondTimezoneSetting> arrayList = new ArrayList<>();
            InputStream inputStream2 = null;
            try {
                inputStream = PortfolioApp.W.c().getResources().openRawResource(R.raw.timezone_cities_code);
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                } catch (Exception unused) {
                    bufferedReader = null;
                    inputStream2 = inputStream;
                    try {
                        FLogger.INSTANCE.getLocal().d("AppHelper", "exception when close stream.");
                        if (inputStream2 != null) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        inputStream = inputStream2;
                        if (inputStream != null) {
                        }
                        if (bufferedReader != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = null;
                    if (inputStream != null) {
                    }
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
                try {
                    List<SecondTimezoneRaw> list = (List) new Gson().a(new JsonReader(bufferedReader), new AppHelper$Companion$getTimezoneCompAppRawDataByRawData$rawSecondTimezoneList$Anon1().getType());
                    wd4.a((Object) list, "rawSecondTimezoneList");
                    ArrayList arrayList2 = new ArrayList(pb4.a(list, 10));
                    for (SecondTimezoneRaw secondTimezoneRaw : list) {
                        arrayList2.add(new SecondTimezoneSetting(secondTimezoneRaw.getCityName(), secondTimezoneRaw.getTimeZoneId(), 0, secondTimezoneRaw.getCityCode()));
                    }
                    arrayList.addAll(arrayList2);
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception unused2) {
                    inputStream2 = inputStream;
                    FLogger.INSTANCE.getLocal().d("AppHelper", "exception when close stream.");
                    if (inputStream2 != null) {
                        inputStream2.close();
                    }
                } catch (Throwable th3) {
                    th = th3;
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    throw th;
                }
            } catch (Exception unused3) {
                bufferedReader = null;
                FLogger.INSTANCE.getLocal().d("AppHelper", "exception when close stream.");
                if (inputStream2 != null) {
                }
            } catch (Throwable th4) {
                th = th4;
                inputStream = null;
                bufferedReader = null;
                if (inputStream != null) {
                }
                if (bufferedReader != null) {
                }
                throw th;
            }
            bufferedReader.close();
            return arrayList;
        }

        @DexIgnore
        public final String f() {
            String sb = new StringBuilder("4.1.3").delete(StringsKt__StringsKt.a((CharSequence) "4.1.3", '-', 0, false, 6, (Object) null), 5).toString();
            wd4.a((Object) sb, "stringBuilder.delete(pos\u2026N_NAME.length).toString()");
            return sb;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final synchronized AppHelper a() {
            AppHelper d;
            if (AppHelper.f.d() == null) {
                AppHelper.f.a(new AppHelper((rd4) null));
            }
            d = AppHelper.f.d();
            if (d == null) {
                wd4.a();
                throw null;
            }
            return d;
        }

        @DexIgnore
        public final ArrayList<Ringtone> b() {
            return AppHelper.e;
        }

        @DexIgnore
        public final void b(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "packageName");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + str));
            try {
                intent.setFlags(268435456);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("AppHelper", "openMarketApp - ex=" + e);
            }
        }

        @DexIgnore
        public final ArrayList<a> a(Context context) {
            ArrayList<a> arrayList = new ArrayList<>();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
            PackageManager packageManager = context.getPackageManager();
            for (ResolveInfo next : queryIntentActivities) {
                if (!cg4.b(next.loadLabel(packageManager).toString(), "Contacts", true) && !cg4.b(next.loadLabel(packageManager).toString(), "Phone", true)) {
                    a aVar = new a(next.loadLabel(context.getPackageManager()).toString());
                    String str = next.activityInfo.packageName;
                    wd4.a((Object) str, "item.activityInfo.packageName");
                    aVar.a(str);
                    try {
                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(aVar.b(), 0);
                        wd4.a((Object) applicationInfo, "packageManager.getApplic\u2026ionInfo(newInfo.pname, 0)");
                        if (applicationInfo.icon != 0) {
                            aVar.a(Uri.parse("android.resource://" + aVar.b() + ZendeskConfig.SLASH + applicationInfo.icon));
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    Iterator<a> it = arrayList.iterator();
                    boolean z = false;
                    while (it.hasNext()) {
                        if (wd4.a((Object) it.next().b(), (Object) aVar.b())) {
                            z = true;
                        }
                    }
                    if (!z) {
                        arrayList.add(aVar);
                    }
                }
            }
            return arrayList;
        }

        @DexIgnore
        @SuppressLint({"HardwareIds"})
        public final String a(String str) {
            wd4.b(str, ButtonService.USER_ID);
            if (TextUtils.isEmpty(str)) {
                String str2 = Build.SERIAL;
                wd4.a((Object) str2, "Build.SERIAL");
                return str2;
            }
            return Build.SERIAL + ":" + str;
        }

        @DexIgnore
        public final boolean a(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "packageName");
            try {
                context.getPackageManager().getPackageInfo(str, 0);
                return true;
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public String a; // = "";
        @DexIgnore
        public Uri b;
        @DexIgnore
        public String c;

        @DexIgnore
        public a(String str) {
            wd4.b(str, "appname");
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final Uri c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && wd4.a((Object) this.c, (Object) ((a) obj).c);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.c;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PInfo(appname=" + this.c + ")";
        }

        @DexIgnore
        public final void a(String str) {
            wd4.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void a(Uri uri) {
            this.b = uri;
        }
    }

    @DexIgnore
    public AppHelper() {
        PortfolioApp.W.c().g().a(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r0 != null) goto L_0x0013;
     */
    @DexIgnore
    public final AppLogInfo b() {
        String str;
        String str2;
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                str = currentUser.getUserId();
            }
            str = "";
            String str3 = str;
            if (wd4.a((Object) "release", (Object) "release")) {
                str2 = "4.1.3";
            } else {
                str2 = f.f();
            }
            String str4 = str2;
            String str5 = cl2.c.a((Context) PortfolioApp.W.c()) + ':' + Build.MODEL;
            String str6 = Build.MODEL;
            String str7 = Build.VERSION.RELEASE;
            wd4.a((Object) str7, "Build.VERSION.RELEASE");
            String sDKVersion = ButtonService.Companion.getSDKVersion();
            wd4.a((Object) str6, "phoneModel");
            return new AppLogInfo(str3, str4, "android", str7, str5, sDKVersion, str6);
        }
        wd4.d("mUserRepository");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (r3 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r1 != null) goto L_0x0028;
     */
    @DexIgnore
    public final ActiveDeviceInfo a() {
        String str;
        String str2;
        String e2 = PortfolioApp.W.c().e();
        DeviceRepository deviceRepository = this.b;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(e2);
            if (deviceBySerial != null) {
                str = deviceBySerial.getSku();
            }
            str = "";
            if (deviceBySerial != null) {
                str2 = deviceBySerial.getFirmwareRevision();
            }
            str2 = "";
            return new ActiveDeviceInfo(e2, str, str2);
        }
        wd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ AppHelper(rd4 rd4) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:61:0x03e0  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0555  */
    public final String a(Context context, int i, int i2) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        int i3 = i2;
        wd4.b(context, "context");
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            DeviceRepository deviceRepository = this.b;
            if (deviceRepository != null) {
                Device deviceBySerial = deviceRepository.getDeviceBySerial(PortfolioApp.W.c().e());
                DeviceRepository deviceRepository2 = this.b;
                if (deviceRepository2 != null) {
                    List<Device> allDevice = deviceRepository2.getAllDevice();
                    TimeZone timeZone = TimeZone.getDefault();
                    String str10 = "";
                    if (deviceBySerial != null) {
                        String sku = deviceBySerial.getSku();
                        String firmwareRevision = deviceBySerial.getFirmwareRevision();
                        int batteryLevel = deviceBySerial.getBatteryLevel();
                        DeviceRepository deviceRepository3 = this.b;
                        if (deviceRepository3 != null) {
                            Device deviceBySerial2 = deviceRepository3.getDeviceBySerial(deviceBySerial.getDeviceId());
                            if (deviceBySerial2 != null) {
                                if (TextUtils.isEmpty(sku)) {
                                    sku = deviceBySerial2.getSku();
                                }
                                if (TextUtils.isEmpty(firmwareRevision)) {
                                    firmwareRevision = deviceBySerial2.getFirmwareRevision();
                                }
                                if (batteryLevel <= 0) {
                                    batteryLevel = deviceBySerial2.getBatteryLevel();
                                }
                            }
                            String str11 = sku;
                            String str12 = firmwareRevision;
                            int i4 = batteryLevel;
                            fn2 fn2 = this.a;
                            if (fn2 != null) {
                                str = str10;
                                long g = fn2.g(deviceBySerial.getDeviceId());
                                StringBuilder sb = new StringBuilder();
                                sb.append("Serial: ");
                                sb.append(deviceBySerial.getDeviceId());
                                sb.append("\n");
                                sb.append("Model: ");
                                sb.append(str11);
                                sb.append("\n");
                                sb.append("FW: ");
                                sb.append(str12);
                                sb.append("\n");
                                sb.append("SDKVersion: ");
                                sb.append("5.8.5-production-release");
                                sb.append("\n");
                                sb.append("Latest Firmware: ");
                                sb.append(deviceBySerial.getFirmwareRevision());
                                sb.append("\n");
                                sb.append("BatteryLevel: ");
                                sb.append(i4);
                                sb.append("\n");
                                sb.append("Last successful sync: ");
                                be4 be4 = be4.a;
                                Object[] objArr = {Long.valueOf(g / ((long) 1000)), sk2.c(new Date(g))};
                                String format = String.format(" %s (%s)", Arrays.copyOf(objArr, objArr.length));
                                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                                sb.append(format);
                                sb.append("\n");
                                str10 = sb.toString();
                            } else {
                                wd4.d("mSharedPreferencesManager");
                                throw null;
                            }
                        } else {
                            wd4.d("mDeviceRepository");
                            throw null;
                        }
                    } else {
                        str = str10;
                    }
                    String str13 = str10 + "List of paired device:";
                    for (Device component1 : allDevice) {
                        str13 = str13 + ' ' + component1.component1();
                    }
                    if (currentUser != null) {
                        str2 = currentUser.getEmail();
                        wd4.a((Object) str2, "user.email");
                        str3 = currentUser.getUserId();
                        wd4.a((Object) str3, "user.userId");
                    } else {
                        str3 = str;
                        str2 = str3;
                    }
                    String str14 = "Tags: ";
                    if (PortfolioApp.W.c().i() == FossilBrand.PORTFOLIO) {
                        str14 = str14 + "diana, diana-bug, bug, uat";
                    }
                    String str15 = "Brand: " + PortfolioApp.W.c().i().getName();
                    if (i3 != -1) {
                        str7 = str13;
                        str4 = str;
                        if (i3 == 0) {
                            str5 = str7;
                            str8 = str14;
                            str9 = str4 + "Hardware/FW\nDid the device have any form of feedback such as hands-movement, vibe, led-flashing when you press/tap on it?\n*Note: if there's none, pls replace the battery. If the issue persist, report it as FW/HW issue.\nWhat's the connection status in phone's Bluetooth settings?\n\nDisconnected \u2192 Was you able to find the device with another phone, using either nRF or ShineSample?\n\nNo \u2192 move away from everyone and try sync again.\nIt works \u2192 it's probably someone else was connected to your watch.\nNo \u2192 feedback as HW/FW issue.\nYes \u2192 go on.\nConnected \u2192 Go on.\nSW and OS\nPlease provide the followings info:\nWhen was the last successful sync (? mins/hours/days ago) and what was the battery level?\n\nHow many apps which use bluetooth in background installed on your phone?\n\n*Note:\nShineSample, nRF and TestShine are NOT counted as these apps don't automatically do any Bluetooth thing in background.\nMisfit, Whitelabel, Fossil Q and portfolio apps each have a background service, which continuously manage bluetooth connection.\nFor QA, it is recommended to have only one of these on your phone during the test.\nHow many bluetooth devices are currently connected to your phone? Please provide a screenshot of Settings > Bluetooth.\n*Note:\nThere're unconfirmed reports that things would go wrong if people have multiple Bluetooth-enabled device connected to the phone at the same time.\nWas ShineSample, nRF or TestShine on your phone able to connect?\nWas you able to connect with another device using ShineSample or nRF?\nLet's try the following tricks. If the connection recover in any of the following steps, you may stop there and report the result.\nDid killing the app then relaunch help?\n\nDid turn off/on Bluetooth help?\n\nDid turn on/off Airplane mode help?\n\nDid remove/forget device in Settings > Bluetooth then kill app and relaunch help?\n\nDid reboot phone help?\n\nDid clearBluetoothCache help?\n\nHardware\nTurn off Bluetooth on your phone \u2192 Try with another phone and see if it works.\"\nHID connection issue: https://misfit.jira.com/wiki/display/SDS/Troubleshoot%3A+HID+Connection\n\n\n";
                        } else if (i3 == 1) {
                            str5 = str7;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str4);
                            str8 = str14;
                            sb2.append("Hardware/FW\nDid the device have any form of feedback such as hands-movement, vibe, led-flashing when you press/tap on it?\n*Note: if there's none, pls replace the battery. If the issue persist, report it as FW/HW issue.\n\nWhat's the connection status in phone's Bluetooth settings?\nDisconnected \u2192 Was you able to find the device with another phone, using either nRF or ShineSample?\nNo \u2192 move away from everyone and try sync again.\nIt works \u2192 it's probably someone else was connected to your watch.\nNo \u2192 feedback as HW/FW issue.\nYes \u2192 go on.\nConnected \u2192 Go on.\nSW and OS\nPlease provide the followings info:\nWhen was the last successful sync (?mins/hours/days) and what was the battery level?\n\nHow many apps which use bluetooth in background installed on your phone?\n\n*Note:\nShineSample, nRF and TestShine are NOT counted as these apps don't automatically do any Bluetooth thing in background.\nMisfit, Whitelabel, Fossil Q and portfolio apps each have a background service, which continuously manage bluetooth connection.\nFor QA, it is recommended to have only one of these on your phone during the test.\nHow many bluetooth devices are currently connected to your phone? Please provide a screenshot of Settings > Bluetooth.\n*Note:\nThere're unconfirmed reports that things would go wrong if people have multiple Bluetooth-enabled device connected to the phone at the same time.\nWas ShineSample, nRF or TestShine on your phone able to connect?\n\nWas you able to connect with another device using ShineSample or nRF?\n\nLet's try the following tricks. If the connection recover in any of the following steps, you may stop there and report the result.\nDid killing the app then relaunch help?\n\nDid turn off/on Bluetooth help?\n\nDid turn on/off Airplane mode help?\n\nDid remove/forget device in Settings > Bluetooth then kill app and relaunch help?\n\nDid reboot phone help?\n\nDid clearBluetoothCache help?\n\nHardware\nTurn off Bluetooth on your phone \u2192 Try with another phone and see if it works.\n\n\n");
                            str9 = sb2.toString();
                        } else if (i3 != 2) {
                            str6 = str4;
                        } else {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(str4);
                            sb3.append("\n\n\n__________\nAppName: ");
                            sb3.append(context.getResources().getString(R.string.app_name));
                            sb3.append("\n");
                            sb3.append("AppVersion: ");
                            sb3.append("4.1.3");
                            sb3.append("\n");
                            sb3.append("Build number: ");
                            sb3.append("24121-2019-11-15");
                            sb3.append("\n");
                            sb3.append("UserID: ");
                            if (TextUtils.isEmpty(str3)) {
                                str3 = "NULL";
                            }
                            sb3.append(str3);
                            sb3.append("\n");
                            sb3.append("Email: ");
                            sb3.append(str2);
                            sb3.append("\n");
                            sb3.append("Phone Info: ");
                            sb3.append(Build.DEVICE);
                            sb3.append(" - ");
                            sb3.append(Build.MODEL);
                            sb3.append("\n");
                            sb3.append("System version: ");
                            sb3.append(Build.VERSION.RELEASE);
                            sb3.append("\n");
                            sb3.append("Local timezone: (");
                            wd4.a((Object) timeZone, "timeZone");
                            sb3.append(timeZone.getID());
                            sb3.append(" (");
                            sb3.append(timeZone.getDisplayName(false, 0));
                            sb3.append(")");
                            sb3.append("offset ");
                            Calendar instance = Calendar.getInstance();
                            wd4.a((Object) instance, "Calendar.getInstance()");
                            sb3.append(timeZone.getOffset(instance.getTimeInMillis()));
                            sb3.append("\n");
                            sb3.append("System timezone: (");
                            sb3.append(timeZone.getID());
                            sb3.append(" (");
                            sb3.append(timeZone.getDisplayName(false, 0));
                            sb3.append(")");
                            sb3.append("offset ");
                            Calendar instance2 = Calendar.getInstance();
                            wd4.a((Object) instance2, "Calendar.getInstance()");
                            sb3.append(timeZone.getOffset(instance2.getTimeInMillis()));
                            sb3.append("\n");
                            sb3.append("Date time: ");
                            be4 be42 = be4.a;
                            Object[] objArr2 = {Long.valueOf(System.currentTimeMillis() / ((long) 1000)), sk2.c(new Date())};
                            String format2 = String.format(" %s (%s)", Arrays.copyOf(objArr2, objArr2.length));
                            wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                            sb3.append(format2);
                            sb3.append("\n");
                            sb3.append("SDK Version V1: ");
                            sb3.append(ButtonService.Companion.getSdkVersionV2());
                            sb3.append("\n");
                            sb3.append("SDK Version V2 (Diana): ");
                            sb3.append(ButtonService.Companion.getSdkVersionV2());
                            sb3.append("\n");
                            sb3.append("Model: ");
                            sb3.append(Build.MODEL);
                            sb3.append("\n");
                            sb3.append("App code: android_");
                            sb3.append(g62.x.a());
                            sb3.append("\n");
                            sb3.append("Language code: ");
                            Locale locale = Locale.getDefault();
                            wd4.a((Object) locale, "Locale.getDefault()");
                            sb3.append(locale.getLanguage());
                            sb3.append("_t");
                            sb3.append("\n");
                            sb3.append("Tags: ");
                            sb3.append(str14);
                            sb3.append("\n");
                            sb3.append("Brand: ");
                            sb3.append(str15);
                            sb3.append("\n");
                            sb3.append("\n__________\n");
                            sb3.append(str7);
                            return sb3.toString();
                        }
                        str14 = str8;
                        if (TextUtils.isEmpty(str2)) {
                        }
                    } else if (i == 0) {
                        StringBuilder sb4 = new StringBuilder();
                        str4 = str;
                        sb4.append(str4);
                        sb4.append("\nPlease give a detailed Description of the issue you are experiencing.\n\n\nHow often is the issue happening?\n\n\nIf you are able, provide the steps you took that caused the bug. (example: opened app > synced > tapped new preset > exited app > watch did not adapt to new functions assigned)\n\nPlease attach any additional pictures, screenshots, or video that may help describe the issue.\n\n\n");
                        str14 = str14 + "uat";
                        String sb5 = sb4.toString();
                        str5 = str13;
                        str6 = sb5;
                        if (TextUtils.isEmpty(str2)) {
                            String str16 = str4;
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append(str6);
                            sb6.append("\n\n\n__________\nAppName: ");
                            String str17 = str15;
                            sb6.append(context.getResources().getString(R.string.app_name));
                            sb6.append(" ");
                            sb6.append(PortfolioApp.W.e() ? str16 : Constants.ENVIRONMENT_PRODUCTION);
                            sb6.append("\n");
                            sb6.append("Email: ");
                            sb6.append(str2);
                            sb6.append("\n");
                            sb6.append("UserID:");
                            if (TextUtils.isEmpty(str3)) {
                                str3 = "NULL";
                            }
                            sb6.append(str3);
                            sb6.append("\n");
                            sb6.append("Device name: ");
                            sb6.append(Build.DEVICE);
                            sb6.append("\n");
                            sb6.append("System name: ");
                            sb6.append("Android OS");
                            sb6.append("\n");
                            sb6.append("System version: ");
                            sb6.append(Build.VERSION.RELEASE);
                            sb6.append("\n");
                            sb6.append("Model: ");
                            sb6.append(Build.MODEL);
                            sb6.append("\n");
                            sb6.append("App code: android_");
                            sb6.append(g62.x.a());
                            sb6.append("\n");
                            sb6.append("Language code: ");
                            Locale locale2 = Locale.getDefault();
                            wd4.a((Object) locale2, "Locale.getDefault()");
                            sb6.append(locale2.getLanguage());
                            sb6.append("_t");
                            sb6.append("\n");
                            sb6.append("Server type: ");
                            sb6.append("release");
                            sb6.append("\n");
                            sb6.append("Local timezone: ");
                            sb6.append("Local Time Zone (");
                            wd4.a((Object) timeZone, "timeZone");
                            sb6.append(timeZone.getID());
                            sb6.append(" (");
                            sb6.append(timeZone.getDisplayName(false, 0));
                            sb6.append(")");
                            sb6.append("offset ");
                            Calendar instance3 = Calendar.getInstance();
                            wd4.a((Object) instance3, "Calendar.getInstance()");
                            sb6.append(timeZone.getOffset(instance3.getTimeInMillis()));
                            sb6.append("\n");
                            sb6.append("Date time: ");
                            be4 be43 = be4.a;
                            Object[] objArr3 = {Long.valueOf(System.currentTimeMillis() / ((long) 1000)), sk2.c(new Date())};
                            String format3 = String.format(" %s (%s)", Arrays.copyOf(objArr3, objArr3.length));
                            wd4.a((Object) format3, "java.lang.String.format(format, *args)");
                            sb6.append(format3);
                            sb6.append("\n");
                            sb6.append("Ver. ");
                            sb6.append(f.f());
                            sb6.append("\n");
                            sb6.append("Build number: ");
                            sb6.append("24121-2019-11-15");
                            sb6.append("\n");
                            sb6.append(str5);
                            sb6.append("\n");
                            sb6.append(str14);
                            sb6.append("\n");
                            sb6.append(str17);
                            sb6.append("\n");
                            sb6.append("\n");
                            sb6.append("Data Log:");
                            return sb6.toString();
                        }
                        String str18 = str4;
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append(str6);
                        sb7.append("\n\n\n__________\nAppName: ");
                        String str19 = str15;
                        sb7.append(context.getResources().getString(R.string.app_name));
                        sb7.append(" ");
                        sb7.append(PortfolioApp.W.e() ? str18 : Constants.ENVIRONMENT_PRODUCTION);
                        sb7.append("\n");
                        sb7.append("Email: ");
                        sb7.append(str2);
                        sb7.append("\n");
                        sb7.append("UserID:");
                        if (TextUtils.isEmpty(str3)) {
                            str3 = "NULL";
                        }
                        sb7.append(str3);
                        sb7.append("\n");
                        sb7.append("Device name: ");
                        sb7.append(Build.DEVICE);
                        sb7.append("\n");
                        sb7.append("System name: ");
                        sb7.append("Android OS");
                        sb7.append("\n");
                        sb7.append("System version: ");
                        sb7.append(Build.VERSION.RELEASE);
                        sb7.append("\n");
                        sb7.append("Model: ");
                        sb7.append(Build.MODEL);
                        sb7.append("\n");
                        sb7.append("App code: android_");
                        sb7.append(g62.x.a());
                        sb7.append("\n");
                        sb7.append("Language code: ");
                        Locale locale3 = Locale.getDefault();
                        wd4.a((Object) locale3, "Locale.getDefault()");
                        sb7.append(locale3.getLanguage());
                        sb7.append("_t");
                        sb7.append("\n");
                        sb7.append("Server type: ");
                        sb7.append("release");
                        sb7.append("\n");
                        sb7.append("Local timezone: ");
                        sb7.append("Local Time Zone (");
                        wd4.a((Object) timeZone, "timeZone");
                        sb7.append(timeZone.getID());
                        sb7.append(" (");
                        sb7.append(timeZone.getDisplayName(false, 0));
                        sb7.append(")");
                        sb7.append("offset ");
                        Calendar instance4 = Calendar.getInstance();
                        wd4.a((Object) instance4, "Calendar.getInstance()");
                        sb7.append(timeZone.getOffset(instance4.getTimeInMillis()));
                        sb7.append("\n");
                        sb7.append("Date time: ");
                        be4 be44 = be4.a;
                        Object[] objArr4 = {Long.valueOf(System.currentTimeMillis() / ((long) 1000)), sk2.c(new Date())};
                        String format4 = String.format(" %s (%s)", Arrays.copyOf(objArr4, objArr4.length));
                        wd4.a((Object) format4, "java.lang.String.format(format, *args)");
                        sb7.append(format4);
                        sb7.append("\n");
                        sb7.append("Ver. ");
                        sb7.append(f.f());
                        sb7.append("\n");
                        sb7.append("Build number: ");
                        sb7.append("24121-2019-11-15");
                        sb7.append("\n");
                        sb7.append("\n__________\n");
                        sb7.append(str5);
                        sb7.append("\n");
                        sb7.append(str14);
                        sb7.append("\n");
                        sb7.append(str19);
                        sb7.append("\n");
                        sb7.append("\n");
                        sb7.append("Data Log:");
                        return sb7.toString();
                    } else {
                        str4 = str;
                        StringBuilder sb8 = new StringBuilder();
                        sb8.append(str4);
                        str7 = str13;
                        sb8.append("\n1. What did you do?\n\n2. What did you actually get?\n\n3. What did you expect?\n\n\n");
                        str6 = sb8.toString();
                    }
                    str5 = str7;
                    if (TextUtils.isEmpty(str2)) {
                    }
                } else {
                    wd4.d("mDeviceRepository");
                    throw null;
                }
            } else {
                wd4.d("mDeviceRepository");
                throw null;
            }
        } else {
            wd4.d("mUserRepository");
            throw null;
        }
    }
}
