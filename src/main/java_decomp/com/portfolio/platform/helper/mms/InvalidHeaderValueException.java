package com.portfolio.platform.helper.mms;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class InvalidHeaderValueException extends MmsException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -2053384496042052262L;

    @DexIgnore
    public InvalidHeaderValueException() {
    }

    @DexIgnore
    public InvalidHeaderValueException(String str) {
        super(str);
    }
}
