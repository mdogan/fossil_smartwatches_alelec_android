package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.c02;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonConvertDateTime implements wz1<DateTime>, c02<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(DateTime dateTime, Type type, b02 b02) {
        String str;
        wd4.b(type, "typeOfSrc");
        wd4.b(b02, "context");
        if (dateTime == null) {
            str = "";
        } else {
            str = sk2.a(DateTimeZone.UTC, dateTime);
            wd4.a((Object) str, "DateHelper.printServerDa\u2026at(DateTimeZone.UTC, src)");
        }
        return new a02(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0039 */
    public DateTime deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        wd4.b(jsonElement, "json");
        wd4.b(type, "typeOfT");
        wd4.b(vz1, "context");
        String f = jsonElement.f();
        wd4.a((Object) f, "dateAsString");
        if (f.length() == 0) {
            return new DateTime(0);
        }
        DateTime a = sk2.a(DateTimeZone.getDefault(), f);
        wd4.a((Object) a, "DateHelper.getServerDate\u2026tDefault(), dateAsString)");
        try {
            DateTime b = sk2.b(jsonElement.f());
            wd4.a((Object) b, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
            return b;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GsonConvertDateTime", "deserialize - json=" + jsonElement.f() + ", e=" + e);
            e.printStackTrace();
            return new DateTime(0);
        }
    }
}
