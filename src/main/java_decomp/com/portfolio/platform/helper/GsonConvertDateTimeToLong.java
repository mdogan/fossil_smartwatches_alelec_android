package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonConvertDateTimeToLong implements wz1<Long> {
    /* JADX WARNING: Can't wrap try/catch for region: R(3:10|11|14) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r6 = com.fossil.blesdk.obfuscated.sk2.b(r5.f());
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r6, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
        r0 = r6.getMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0041, code lost:
        r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r2 = new java.lang.StringBuilder();
        r2.append("deserialize - json=");
        r2.append(r5.f());
        r2.append(", e=");
        r6.printStackTrace();
        r2.append(com.fossil.blesdk.obfuscated.cb4.a);
        r7.e("GsonConvertDateTimeToLong", r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006e, code lost:
        r5 = r0;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002e */
    public Long deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        wd4.b(jsonElement, "json");
        String f = jsonElement.f();
        wd4.a((Object) f, "dateAsString");
        long j = 0;
        if (f.length() == 0) {
            return 0L;
        }
        DateTime c = sk2.c(f);
        wd4.a((Object) c, "DateHelper.getServerDate\u2026ffsetParsed(dateAsString)");
        long j2 = c.getMillis();
        return Long.valueOf(j2);
    }
}
