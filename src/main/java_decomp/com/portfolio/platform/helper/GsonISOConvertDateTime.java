package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.c02;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonISOConvertDateTime implements wz1<Date>, c02<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, b02 b02) {
        String str;
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = sk2.l.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                wd4.a((Object) str, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                wd4.a();
                throw null;
            }
        }
        return new a02(str);
    }

    @DexIgnore
    public Date deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        if (jsonElement != null) {
            String f = jsonElement.f();
            if (f != null) {
                try {
                    SimpleDateFormat simpleDateFormat = sk2.l.get();
                    if (simpleDateFormat != null) {
                        Date parse = simpleDateFormat.parse(f);
                        SimpleDateFormat simpleDateFormat2 = sk2.j.get();
                        if (simpleDateFormat2 != null) {
                            SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                            SimpleDateFormat simpleDateFormat4 = sk2.j.get();
                            if (simpleDateFormat4 != null) {
                                Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                                wd4.a((Object) parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                                return parse2;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                } catch (ParseException e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("deserialize - json=");
                    sb.append(jsonElement.f());
                    sb.append(", e=");
                    e.printStackTrace();
                    sb.append(cb4.a);
                    local.e("GsonISOConvertDateTime", sb.toString());
                }
            }
        }
        return new Date(0);
    }
}
