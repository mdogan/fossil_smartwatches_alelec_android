package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.c02;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonConverterShortDate implements wz1<Date>, c02<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, b02 b02) {
        String str;
        wd4.b(type, "typeOfSrc");
        wd4.b(b02, "context");
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = sk2.a.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                wd4.a((Object) str, "DateHelper.SHORT_DATE_FO\u2026ATTER.get()!!.format(src)");
            } else {
                wd4.a();
                throw null;
            }
        }
        return new a02(str);
    }

    @DexIgnore
    public Date deserialize(JsonElement jsonElement, Type type, vz1 vz1) {
        wd4.b(jsonElement, "json");
        wd4.b(type, "typeOfT");
        wd4.b(vz1, "context");
        String f = jsonElement.f();
        if (f != null) {
            try {
                SimpleDateFormat simpleDateFormat = sk2.a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    if (parse != null) {
                        return parse;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(f);
                sb.append(", e=");
                e.printStackTrace();
                sb.append(cb4.a);
                local.e("GsonConverterShortDate", sb.toString());
            }
        }
        return new Date(0);
    }
}
