package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$Anon1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
public final class PortfolioApp$onActiveDeviceStealed$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$onActiveDeviceStealed$Anon1(PortfolioApp portfolioApp, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = portfolioApp;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PortfolioApp$onActiveDeviceStealed$Anon1 portfolioApp$onActiveDeviceStealed$Anon1 = new PortfolioApp$onActiveDeviceStealed$Anon1(this.this$Anon0, kc4);
        portfolioApp$onActiveDeviceStealed$Anon1.p$ = (lh4) obj;
        return portfolioApp$onActiveDeviceStealed$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$onActiveDeviceStealed$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.W.d();
            local.d(d, "activeDevice=" + this.this$Anon0.e() + ", was stealed remove it!!!");
            if (DeviceHelper.o.f(this.this$Anon0.e())) {
                this.this$Anon0.v().deleteWatchFacesWithSerial(this.this$Anon0.e());
            }
            PortfolioApp portfolioApp = this.this$Anon0;
            portfolioApp.r(portfolioApp.e());
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
