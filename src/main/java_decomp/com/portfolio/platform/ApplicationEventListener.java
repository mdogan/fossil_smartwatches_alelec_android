package com.portfolio.platform;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityManager;
import androidx.lifecycle.Lifecycle;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.ec;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.blesdk.obfuscated.xb;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ApplicationEventListener implements xb {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public /* final */ PortfolioApp a;
    @DexIgnore
    public /* final */ fn2 b;
    @DexIgnore
    public /* final */ HybridPresetRepository c;
    @DexIgnore
    public /* final */ CategoryRepository d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ MicroAppRepository g;
    @DexIgnore
    public /* final */ DianaPresetRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ wj2 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ WatchFaceRepository m;
    @DexIgnore
    public /* final */ WatchLocalizationRepository n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ApplicationEventListener.o;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ApplicationEventListener$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        wd4.a((Object) simpleName, "ApplicationEventListener\u2026lass.javaClass.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public ApplicationEventListener(PortfolioApp portfolioApp, fn2 fn2, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, wj2 wj2, AlarmsRepository alarmsRepository, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository) {
        wd4.b(portfolioApp, "mApp");
        wd4.b(fn2, "mSharedPrefs");
        wd4.b(hybridPresetRepository, "mHybridPresetRepository");
        wd4.b(categoryRepository, "mCategoryRepository");
        wd4.b(watchAppRepository, "mWatchAppRepository");
        wd4.b(complicationRepository, "mComplicationRepository");
        wd4.b(microAppRepository, "mMicroAppRepository");
        wd4.b(dianaPresetRepository, "mDianaPresetRepository");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(wj2, "mDeviceSettingFactory");
        wd4.b(alarmsRepository, "mAlarmRepository");
        wd4.b(watchFaceRepository, "mWatchFaceRepository");
        wd4.b(watchLocalizationRepository, "watchLocalization");
        this.a = portfolioApp;
        this.b = fn2;
        this.c = hybridPresetRepository;
        this.d = categoryRepository;
        this.e = watchAppRepository;
        this.f = complicationRepository;
        this.g = microAppRepository;
        this.h = dianaPresetRepository;
        this.i = deviceRepository;
        this.j = userRepository;
        this.k = wj2;
        this.l = alarmsRepository;
        this.m = watchFaceRepository;
        this.n = watchLocalizationRepository;
    }

    @DexIgnore
    @ec(Lifecycle.Event.ON_STOP)
    public final void onAppEnterBackground() {
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.e(), o, "[App Close] User put app in background");
    }

    @DexIgnore
    @ec(Lifecycle.Event.ON_START)
    public final void onAppEnterForeground() {
        boolean m2 = this.b.m(this.a.h());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = o;
        local.d(str, "onAppEnterForeground isMigrationComplete " + m2);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ApplicationEventListener$onAppEnterForeground$Anon1(this, (kc4) null), 3, (Object) null);
        String e2 = this.a.e();
        if (!TextUtils.isEmpty(e2)) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String e3 = this.a.e();
            String str2 = o;
            remote.i(component, session, e3, str2, "[App Open] Is migrate complete " + m2 + " \n Is Notification Listener Enabled " + PortfolioApp.W.c().C());
        }
        if (m2) {
            ri4 unused2 = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ApplicationEventListener$onAppEnterForeground$Anon2(this, e2, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(o, "Inside .autoSync");
        String e2 = this.a.e();
        if (TextUtils.isEmpty(e2)) {
            FLogger.INSTANCE.getLocal().d(o, "User has no active device, skip auto sync");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = o;
        local.d(str, "Inside .autoSync lastSyncSuccess=" + this.b.f(this.a.e()));
        long currentTimeMillis = System.currentTimeMillis() - this.b.g(this.a.e());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = o;
        remote.i(component, session, e2, str2, "[App Open] Last sync OK interval " + currentTimeMillis);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = o;
        local2.d(str3, "Inside .autoSync, last sync interval " + currentTimeMillis);
        if (currentTimeMillis >= ((long) CommuteTimeService.y) || currentTimeMillis < 0) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp.f(portfolioApp.e()) == CommunicateMode.OTA.getValue()) {
                FLogger.INSTANCE.getLocal().d(o, "Inside .autoSync, device is ota, wait for the next time");
            } else if (this.a.D() || this.b.B()) {
                FLogger.INSTANCE.getLocal().d(o, "Inside .autoSync, start auto-sync.");
                boolean a2 = cn2.d.a(this.a, "SYNC_DEVICE", false);
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String str4 = o;
                remote2.i(component2, session2, e2, str4, "[App Open] [Sync Start] AUTO SYNC isBlueToothEnabled " + BluetoothUtils.isBluetoothEnable() + ' ');
                if (a2) {
                    this.a.a(this.k, false, 10);
                } else {
                    FLogger.INSTANCE.getLocal().d(o, "autoSync fail due to lack of permission");
                }
            } else {
                FLogger.INSTANCE.getLocal().d(o, "Inside .autoSync, doesn't start auto-sync.");
            }
        }
    }

    @DexIgnore
    public final void b() {
        Object systemService = this.a.getSystemService("accessibility");
        if (systemService != null) {
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = ((AccessibilityManager) systemService).getEnabledAccessibilityServiceList(-1);
            StringBuilder sb = new StringBuilder();
            for (AccessibilityServiceInfo next : enabledAccessibilityServiceList) {
                wd4.a((Object) next, "accessibility");
                String id = next.getId();
                wd4.a((Object) id, "accessibility.id");
                sb.append((String) wb4.f(StringsKt__StringsKt.a((CharSequence) id, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null)));
                sb.append(" ");
            }
            String sb2 = sb.toString();
            wd4.a((Object) sb2, "enabledAccessibilityStringBuilder.toString()");
            if (sb2 != null) {
                String obj = StringsKt__StringsKt.d(sb2).toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = o;
                local.d(str, "Enabled Accessibility: " + obj);
                AnalyticsHelper.f.c().a("accessibility_config_on_launch", obj);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.accessibility.AccessibilityManager");
    }
}
