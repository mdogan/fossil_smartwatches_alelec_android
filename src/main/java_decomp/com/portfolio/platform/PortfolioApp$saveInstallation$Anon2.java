package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.response.ResponseKt;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.PortfolioApp$saveInstallation$Anon2", f = "PortfolioApp.kt", l = {1047}, m = "invokeSuspend")
public final class PortfolioApp$saveInstallation$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Installation $installation;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$saveInstallation$Anon2(PortfolioApp portfolioApp, Installation installation, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = portfolioApp;
        this.$installation = installation;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PortfolioApp$saveInstallation$Anon2 portfolioApp$saveInstallation$Anon2 = new PortfolioApp$saveInstallation$Anon2(this.this$Anon0, this.$installation, kc4);
        portfolioApp$saveInstallation$Anon2.p$ = (lh4) obj;
        return portfolioApp$saveInstallation$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$saveInstallation$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            PortfolioApp$saveInstallation$Anon2$response$Anon1 portfolioApp$saveInstallation$Anon2$response$Anon1 = new PortfolioApp$saveInstallation$Anon2$response$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = ResponseKt.a(portfolioApp$saveInstallation$Anon2$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            so2 so2 = (so2) ro2;
            if (so2.a() != null) {
                this.this$Anon0.u().p(((Installation) so2.a()).getInstallationId());
            }
        }
        return cb4.a;
    }
}
