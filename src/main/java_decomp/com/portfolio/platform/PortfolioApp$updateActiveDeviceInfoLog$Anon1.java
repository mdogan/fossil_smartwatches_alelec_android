package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.portfolio.platform.helper.AppHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$Anon1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
public final class PortfolioApp$updateActiveDeviceInfoLog$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    public PortfolioApp$updateActiveDeviceInfoLog$Anon1(kc4 kc4) {
        super(2, kc4);
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PortfolioApp$updateActiveDeviceInfoLog$Anon1 portfolioApp$updateActiveDeviceInfoLog$Anon1 = new PortfolioApp$updateActiveDeviceInfoLog$Anon1(kc4);
        portfolioApp$updateActiveDeviceInfoLog$Anon1.p$ = (lh4) obj;
        return portfolioApp$updateActiveDeviceInfoLog$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$updateActiveDeviceInfoLog$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ActiveDeviceInfo a = AppHelper.f.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.W.d();
            local.d(d, ".updateActiveDeviceInfoLog(), " + new Gson().a((Object) a));
            FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
            try {
                IButtonConnectivity b = PortfolioApp.W.b();
                if (b != null) {
                    b.updateActiveDeviceInfoLog(a);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.W.d();
                local2.e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
