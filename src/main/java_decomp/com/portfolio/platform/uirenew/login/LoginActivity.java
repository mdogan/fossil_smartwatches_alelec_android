package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.gk3;
import com.fossil.blesdk.obfuscated.hk3;
import com.fossil.blesdk.obfuscated.jn2;
import com.fossil.blesdk.obfuscated.kn2;
import com.fossil.blesdk.obfuscated.ln2;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a G; // = new a((rd4) null);
    @DexIgnore
    public jn2 B;
    @DexIgnore
    public kn2 C;
    @DexIgnore
    public ln2 D;
    @DexIgnore
    public MFLoginWechatManager E;
    @DexIgnore
    public LoginPresenter F;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            context.startActivity(new Intent(context, LoginActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            jn2 jn2 = this.B;
            if (jn2 != null) {
                jn2.a(i, i2, intent);
                kn2 kn2 = this.C;
                if (kn2 != null) {
                    kn2.a(i, i2, intent);
                    ln2 ln2 = this.D;
                    if (ln2 != null) {
                        ln2.a(i, i2, intent);
                        Fragment a2 = getSupportFragmentManager().a((int) R.id.content);
                        if (a2 != null) {
                            a2.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    wd4.d("mLoginWeiboManager");
                    throw null;
                }
                wd4.d("mLoginGoogleManager");
                throw null;
            }
            wd4.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        gk3 gk3 = (gk3) getSupportFragmentManager().a((int) R.id.content);
        if (gk3 == null) {
            gk3 = gk3.n.b();
            a((Fragment) gk3, gk3.n.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (gk3 != null) {
            g.a(new hk3(this, gk3)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.login.LoginContract.View");
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        wd4.b(intent, "intent");
        super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.E;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            wd4.a((Object) intent2, "getIntent()");
            mFLoginWechatManager.a(intent2);
            return;
        }
        wd4.d("mLoginWechatManager");
        throw null;
    }
}
