package com.portfolio.platform.uirenew.login;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1", f = "LoginPresenter.kt", l = {377}, m = "invokeSuspend")
public final class LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<ApiResponse<Device>>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1(LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1 loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1 loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1 = new LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1(this.this$Anon0, kc4);
        loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1.p$ = (lh4) obj;
        return loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$response$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            DeviceRepository r = this.this$Anon0.this$Anon0.a.r();
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = r.downloadDeviceList(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
