package com.portfolio.platform.uirenew.login;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$Anon1", f = "LoginPresenter.kt", l = {512}, m = "invokeSuspend")
public final class LoginPresenter$checkOnboarding$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$checkOnboarding$Anon1(LoginPresenter loginPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = loginPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LoginPresenter$checkOnboarding$Anon1 loginPresenter$checkOnboarding$Anon1 = new LoginPresenter$checkOnboarding$Anon1(this.this$Anon0, kc4);
        loginPresenter$checkOnboarding$Anon1.p$ = (lh4) obj;
        return loginPresenter$checkOnboarding$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LoginPresenter$checkOnboarding$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            LoginPresenter$checkOnboarding$Anon1$currentUser$Anon1 loginPresenter$checkOnboarding$Anon1$currentUser$Anon1 = new LoginPresenter$checkOnboarding$Anon1$currentUser$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, loginPresenter$checkOnboarding$Anon1$currentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = LoginPresenter.O.a();
        local.d(a2, "checkOnboarding currentUser=" + mFUser);
        if (mFUser != null) {
            this.this$Anon0.L.h();
        }
        return cb4.a;
    }
}
