package com.portfolio.platform.uirenew.login;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1", f = "LoginPresenter.kt", l = {414}, m = "invokeSuspend")
public final class LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ on3 $errorValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1.Anon4 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1$Anon1", f = "LoginPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1 loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.a.this$Anon0.a.y().clearAllUser();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1(LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1.Anon4 anon4, on3 on3, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon4;
        this.$errorValue = on3;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1 loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1 = new LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1(this.this$Anon0, this.$errorValue, kc4);
        loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1.p$ = (lh4) obj;
        return loginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "onLoginSuccess download device setting fail " + this.$errorValue.a());
            gh4 b = this.this$Anon0.a.this$Anon0.a.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.a.this$Anon0.a.L.i();
        this.this$Anon0.a.this$Anon0.a.a(this.$errorValue.a(), this.$errorValue.b());
        return cb4.a;
    }
}
