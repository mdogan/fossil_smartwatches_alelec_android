package com.portfolio.platform.uirenew.splash;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.cp3;
import com.fossil.blesdk.obfuscated.sq3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SplashScreenActivity extends BaseActivity {
    @DexIgnore
    public SplashPresenter B;

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        sq3 sq3 = (sq3) getSupportFragmentManager().a((int) R.id.content);
        if (sq3 == null) {
            sq3 = sq3.l.a();
            a((Fragment) sq3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new cp3(sq3)).a(this);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        a((Class<? extends T>[]) new Class[]{MFDeviceService.class});
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        b((Class<? extends T>[]) new Class[]{MFDeviceService.class});
    }
}
