package com.portfolio.platform.uirenew.splash;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.ap3;
import com.fossil.blesdk.obfuscated.bp3;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SplashPresenter extends ap3 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Handler f; // = new Handler();
    @DexIgnore
    public /* final */ Runnable g; // = new b(this);
    @DexIgnore
    public /* final */ bp3 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ MigrationHelper j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SplashPresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter e;

        @DexIgnore
        public b(SplashPresenter splashPresenter) {
            this.e = splashPresenter;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(SplashPresenter.l.a(), "runnable");
            this.e.h();
        }
    }

    /*
    static {
        String simpleName = SplashPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "SplashPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public SplashPresenter(bp3 bp3, UserRepository userRepository, MigrationHelper migrationHelper, fn2 fn2) {
        wd4.b(bp3, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(migrationHelper, "mMigrationHelper");
        wd4.b(fn2, "mSharePrefs");
        this.h = bp3;
        this.i = userRepository;
        this.j = migrationHelper;
    }

    @DexIgnore
    public final void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SplashPresenter$checkToGoToNextStep$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        String h2 = PortfolioApp.W.c().h();
        boolean a2 = this.j.a(h2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "currentAppVersion " + h2 + " complete " + a2);
        if (!a2) {
            this.j.b(h2);
        }
        this.f.postDelayed(this.g, 1000);
    }

    @DexIgnore
    public void g() {
        this.f.removeCallbacksAndMessages((Object) null);
    }
}
