package com.portfolio.platform.uirenew.splash;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$Anon1", f = "SplashPresenter.kt", l = {63}, m = "invokeSuspend")
public final class SplashPresenter$checkToGoToNextStep$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SplashPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SplashPresenter$checkToGoToNextStep$Anon1(SplashPresenter splashPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = splashPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SplashPresenter$checkToGoToNextStep$Anon1 splashPresenter$checkToGoToNextStep$Anon1 = new SplashPresenter$checkToGoToNextStep$Anon1(this.this$Anon0, kc4);
        splashPresenter$checkToGoToNextStep$Anon1.p$ = (lh4) obj;
        return splashPresenter$checkToGoToNextStep$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SplashPresenter$checkToGoToNextStep$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            String h = PortfolioApp.W.c().h();
            boolean a2 = this.this$Anon0.j.a(h);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = SplashPresenter.l.a();
            local.d(a3, "checkToGoToNextStep isMigrationComplete " + a2);
            if (!a2) {
                this.this$Anon0.f.postDelayed(this.this$Anon0.g, 500);
                return cb4.a;
            }
            gh4 b = this.this$Anon0.b();
            SplashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1 splashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1 = new SplashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = h;
            this.Z$Anon0 = a2;
            this.label = 1;
            obj = kg4.a(b, splashPresenter$checkToGoToNextStep$Anon1$mCurrentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            String str = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser == null) {
            this.this$Anon0.h.p0();
        } else {
            String email = mFUser.getEmail();
            boolean z = false;
            if (!(email == null || cg4.a(email))) {
                String birthday = mFUser.getBirthday();
                if (birthday == null || cg4.a(birthday)) {
                    z = true;
                }
                if (!z) {
                    String firstName = mFUser.getFirstName();
                    wd4.a((Object) firstName, "mCurrentUser.firstName");
                    if (!cg4.a(firstName)) {
                        String lastName = mFUser.getLastName();
                        wd4.a((Object) lastName, "mCurrentUser.lastName");
                        if (!cg4.a(lastName)) {
                            this.this$Anon0.h.h();
                        }
                    }
                }
            }
            this.this$Anon0.h.d0();
        }
        return cb4.a;
    }
}
