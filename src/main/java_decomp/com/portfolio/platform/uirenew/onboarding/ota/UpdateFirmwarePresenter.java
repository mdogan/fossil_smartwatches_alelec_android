package com.portfolio.platform.uirenew.onboarding.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ml3;
import com.fossil.blesdk.obfuscated.nl3;
import com.fossil.blesdk.obfuscated.ql3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter extends ml3 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((rd4) null);
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public boolean g;
    @DexIgnore
    public Device h;
    @DexIgnore
    public vl2 i;
    @DexIgnore
    public /* final */ c j; // = new c(this);
    @DexIgnore
    public /* final */ b k; // = new b(this);
    @DexIgnore
    public /* final */ nl3 l;
    @DexIgnore
    public /* final */ DeviceRepository m;
    @DexIgnore
    public /* final */ wj2 n;
    @DexIgnore
    public /* final */ fn2 o;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase p;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwarePresenter.r;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public b(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.a.o().b(booleanExtra);
            this.a.o.n(PortfolioApp.W.c().e());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.f, UpdateFirmwarePresenter.s.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.W.c().a(this.a.n, false, 13);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public c(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            wd4.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            this.a.f = otaEvent.getSerial();
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && cg4.b(otaEvent.getSerial(), PortfolioApp.W.c().e(), true)) {
                this.a.o().g((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwarePresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwarePresenter(nl3 nl3, DeviceRepository deviceRepository, UserRepository userRepository, wj2 wj2, fn2 fn2, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        wd4.b(nl3, "mView");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(wj2, "mDeviceSettingFactory");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        wd4.b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        this.l = nl3;
        this.m = deviceRepository;
        this.n = wj2;
        this.o = fn2;
        this.p = updateFirmwareUsecase;
        this.q = downloadFirmwareByDeviceModelUsecase;
    }

    @DexIgnore
    public boolean j() {
        return this.g;
    }

    @DexIgnore
    public void k() {
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
        if (deviceBySerial != null && ql3.a[deviceBySerial.ordinal()] == 1) {
            this.l.t();
        } else {
            this.l.h();
        }
    }

    @DexIgnore
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, "tryOtaAgain");
        Device device = this.h;
        if (device != null) {
            c(device);
        }
        this.l.n0();
    }

    @DexIgnore
    public final void m() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.f);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "serial=" + this.f + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__NeverMissAStepAndKeep));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_diana_two);
            explore3.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__GlanceableContentReadyOnTheWrist));
            explore4.setBackground(R.drawable.update_fw_diana_four);
        } else {
            explore.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__TrackActivitySleepAndPersonalGoals));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_hybrid_two);
            explore3.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__NoChargingNeededYourWatchIs));
            explore4.setBackground(R.drawable.update_fw_hybrid_four);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.l.i(arrayList);
    }

    @DexIgnore
    public final DeviceRepository n() {
        return this.m;
    }

    @DexIgnore
    public final nl3 o() {
        return this.l;
    }

    @DexIgnore
    public void p() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "start - mIsOnBoardingFlow=" + this.g);
        if (!this.g) {
            this.l.X();
        }
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
        PortfolioApp c2 = PortfolioApp.W.c();
        c cVar = this.j;
        c2.registerReceiver(cVar, new IntentFilter(PortfolioApp.W.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.l.g();
        BleCommandResultManager.d.a(CommunicateMode.OTA);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            PortfolioApp.W.c().unregisterReceiver(this.j);
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.e(str, "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        BleCommandResultManager.d.a(CommunicateMode.OTA);
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(r, "checkFwAgain");
        b(this.h);
    }

    @DexIgnore
    public final void b(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(r, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String i2 = this.o.i(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + i2);
            if (TextUtils.isEmpty(i2)) {
                a(device);
                return;
            }
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(deviceId, i2, (kc4) null, this, device), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void c(Device device) {
        wd4.b(device, "Device");
        String deviceId = device.getDeviceId();
        boolean A = PortfolioApp.W.c().A();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + A);
        if (!A) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$updateFirmware$Anon1(this, deviceId, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void a(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.W.c().D() || !this.o.T()) {
            DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase = this.q;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            downloadFirmwareByDeviceModelUsecase.a(new DownloadFirmwareByDeviceModelUsecase.b(sku), new UpdateFirmwarePresenter$checkFirmware$Anon1(this, deviceId, device));
            return;
        }
        this.l.v0();
    }

    @DexIgnore
    public void a(boolean z) {
        this.g = z;
    }
}
