package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.util.DeviceUtils;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$Anon1", f = "UpdateFirmwarePresenter.kt", l = {247}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$updateFirmware$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$Anon1$Anon1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends String, ? extends String>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter$updateFirmware$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(UpdateFirmwarePresenter$updateFirmware$Anon1 updateFirmwarePresenter$updateFirmware$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = updateFirmwarePresenter$updateFirmware$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return DeviceUtils.g.a().a(this.this$Anon0.$activeSerial, (Device) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<UpdateFirmwareUsecase.d, UpdateFirmwareUsecase.c> {
        @DexIgnore
        public void a(UpdateFirmwareUsecase.c cVar) {
            wd4.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateFirmwareUsecase.d dVar) {
            wd4.b(dVar, "responseValue");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$updateFirmware$Anon1(UpdateFirmwarePresenter updateFirmwarePresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = updateFirmwarePresenter;
        this.$activeSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UpdateFirmwarePresenter$updateFirmware$Anon1 updateFirmwarePresenter$updateFirmware$Anon1 = new UpdateFirmwarePresenter$updateFirmware$Anon1(this.this$Anon0, this.$activeSerial, kc4);
        updateFirmwarePresenter$updateFirmware$Anon1.p$ = (lh4) obj;
        return updateFirmwarePresenter$updateFirmware$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$updateFirmware$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        String str = (String) pair.component1();
        String str2 = (String) pair.component2();
        if (!(str == null || str2 == null)) {
            vl2 c = this.this$Anon0.i;
            if (c != null) {
                c.a("old_firmware", str);
                if (c != null) {
                    c.a("new_firmware", str2);
                    if (c != null) {
                        c.d();
                    }
                }
            }
        }
        this.this$Anon0.p.a(new UpdateFirmwareUsecase.b(this.$activeSerial, false, 2, (rd4) null), new a());
        return cb4.a;
    }
}
