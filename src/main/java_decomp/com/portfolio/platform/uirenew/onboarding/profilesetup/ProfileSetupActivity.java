package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.is2;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xl3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileSetupActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public ProfileSetupPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            wd4.b(context, "context");
            wd4.b(signUpEmailAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "start with auth=" + signUpEmailAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpSocialAuth signUpSocialAuth) {
            wd4.b(context, "context");
            wd4.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "startSetUpSocial with " + signUpSocialAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("SOCIAL_AUTH", signUpSocialAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            FLogger.INSTANCE.getLocal().d("ProfileSetupActivity", "startUpdateProfile");
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("IS_UPDATE_PROFILE_PROCESS", true);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        is2 is2 = (is2) getSupportFragmentManager().a((int) R.id.content);
        if (is2 == null) {
            is2 = is2.p.a();
            a((Fragment) is2, "ProfileSetupFragment", (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (is2 != null) {
            g.a(new xl3(is2)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent socialAuth");
                    ProfileSetupPresenter profileSetupPresenter = this.B;
                    if (profileSetupPresenter != null) {
                        Parcelable parcelableExtra = intent.getParcelableExtra("SOCIAL_AUTH");
                        wd4.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ms.Constants.SOCIAL_AUTH)");
                        profileSetupPresenter.a((SignUpSocialAuth) parcelableExtra);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent emailAuth");
                    ProfileSetupPresenter profileSetupPresenter2 = this.B;
                    if (profileSetupPresenter2 != null) {
                        Parcelable parcelableExtra2 = intent.getParcelableExtra("EMAIL_AUTH");
                        wd4.a((Object) parcelableExtra2, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                        profileSetupPresenter2.a((SignUpEmailAuth) parcelableExtra2);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean booleanExtra = intent.getBooleanExtra("IS_UPDATE_PROFILE_PROCESS", false);
                    ProfileSetupPresenter profileSetupPresenter3 = this.B;
                    if (profileSetupPresenter3 != null) {
                        profileSetupPresenter3.c(booleanExtra);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
            if (bundle != null) {
                if (bundle.containsKey("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from savedInstanceState socialAuth");
                    SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) bundle.getParcelable("SOCIAL_AUTH");
                    if (signUpSocialAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter4 = this.B;
                        if (profileSetupPresenter4 != null) {
                            profileSetupPresenter4.a(signUpSocialAuth);
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(f(), "Retrieve from savedInstanceState emailAuth");
                    SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) bundle.getParcelable("EMAIL_AUTH");
                    if (signUpEmailAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter5 = this.B;
                        if (profileSetupPresenter5 != null) {
                            profileSetupPresenter5.a(signUpEmailAuth);
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean z = bundle.getBoolean("IS_UPDATE_PROFILE_PROCESS");
                    ProfileSetupPresenter profileSetupPresenter6 = this.B;
                    if (profileSetupPresenter6 != null) {
                        profileSetupPresenter6.c(z);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            ProfileSetupPresenter profileSetupPresenter = this.B;
            if (profileSetupPresenter != null) {
                SignUpEmailAuth l = profileSetupPresenter.l();
                if (l != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String f = f();
                    local.d(f, "onSaveInstanceState put emailAuth " + l);
                    bundle.putParcelable("EMAIL_AUTH", l);
                }
                ProfileSetupPresenter profileSetupPresenter2 = this.B;
                if (profileSetupPresenter2 != null) {
                    SignUpSocialAuth n = profileSetupPresenter2.n();
                    if (n != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String f2 = f();
                        local2.d(f2, "onSaveInstanceState put socialAuth " + n);
                        bundle.putParcelable("SOCIAL_AUTH", n);
                    }
                    ProfileSetupPresenter profileSetupPresenter3 = this.B;
                    if (profileSetupPresenter3 != null) {
                        bundle.putBoolean("IS_UPDATE_PROFILE_PROCESS", profileSetupPresenter3.t());
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle);
    }
}
