package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.gl3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {31}, m = "invokeSuspend")
public final class OnboardingHeightWeightPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$Anon1$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(OnboardingHeightWeightPresenter$start$Anon1 onboardingHeightWeightPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = onboardingHeightWeightPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.h.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OnboardingHeightWeightPresenter$start$Anon1(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = onboardingHeightWeightPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        OnboardingHeightWeightPresenter$start$Anon1 onboardingHeightWeightPresenter$start$Anon1 = new OnboardingHeightWeightPresenter$start$Anon1(this.this$Anon0, kc4);
        onboardingHeightWeightPresenter$start$Anon1.p$ = (lh4) obj;
        return onboardingHeightWeightPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((OnboardingHeightWeightPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
    public final Object invokeSuspend(Object obj) {
        MFUser b;
        OnboardingHeightWeightPresenter onboardingHeightWeightPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.f == null) {
                OnboardingHeightWeightPresenter onboardingHeightWeightPresenter2 = this.this$Anon0;
                gh4 a2 = onboardingHeightWeightPresenter2.b();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = onboardingHeightWeightPresenter2;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                onboardingHeightWeightPresenter = onboardingHeightWeightPresenter2;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = OnboardingHeightWeightPresenter.k.a();
            local.d(a3, "start with currentUser=" + this.this$Anon0.f);
            b = this.this$Anon0.f;
            if (b != null) {
                if (b.getHeightInCentimeters() == 0 || b.getWeightInGrams() == 0) {
                    nk2 nk2 = nk2.a;
                    Gender gender = b.getGender();
                    wd4.a((Object) gender, "it.gender");
                    Pair<Integer, Integer> a4 = nk2.a(gender, MFUser.getAge(b.getBirthday()));
                    if (b.getHeightInCentimeters() == 0) {
                        b.setHeightInCentimeters(a4.getFirst().intValue());
                    }
                    if (b.getWeightInGrams() == 0) {
                        b.setWeightInGrams(a4.getSecond().intValue() * 1000);
                    }
                }
                if (b.getHeightInCentimeters() > 0) {
                    gl3 d = this.this$Anon0.g;
                    int heightInCentimeters = b.getHeightInCentimeters();
                    Unit heightUnit = b.getHeightUnit();
                    wd4.a((Object) heightUnit, "it.heightUnit");
                    d.a(heightInCentimeters, heightUnit);
                }
                if (b.getWeightInGrams() > 0) {
                    gl3 d2 = this.this$Anon0.g;
                    int weightInGrams = b.getWeightInGrams();
                    Unit weightUnit = b.getWeightUnit();
                    wd4.a((Object) weightUnit, "it.weightUnit");
                    d2.b(weightInGrams, weightUnit);
                }
            }
            this.this$Anon0.g.g();
            return cb4.a;
        } else if (i == 1) {
            onboardingHeightWeightPresenter = (OnboardingHeightWeightPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        onboardingHeightWeightPresenter.f = (MFUser) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a32 = OnboardingHeightWeightPresenter.k.a();
        local2.d(a32, "start with currentUser=" + this.this$Anon0.f);
        b = this.this$Anon0.f;
        if (b != null) {
        }
        this.this$Anon0.g.g();
        return cb4.a;
    }
}
