package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.blesdk.obfuscated.fl3;
import com.fossil.blesdk.obfuscated.gl3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OnboardingHeightWeightPresenter extends fl3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((rd4) null);
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ gl3 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return OnboardingHeightWeightPresenter.j;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<GetRecommendedGoalUseCase.d, GetRecommendedGoalUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser a;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(MFUser mFUser, OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, boolean z) {
            this.a = mFUser;
            this.b = onboardingHeightWeightPresenter;
            this.c = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetRecommendedGoalUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            this.b.a(this.a, this.c);
        }

        @DexIgnore
        public void a(GetRecommendedGoalUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.b.a(this.a, this.c);
        }
    }

    /*
    static {
        String simpleName = OnboardingHeightWeightPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "OnboardingHeightWeightPr\u2026er::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public OnboardingHeightWeightPresenter(gl3 gl3, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        wd4.b(gl3, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        this.g = gl3;
        this.h = userRepository;
        this.i = getRecommendedGoalUseCase;
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new OnboardingHeightWeightPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.g.a(this);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onWeightChanged weightInGram=" + i2);
        MFUser mFUser = this.f;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onHeightChanged heightInCentimeters=" + i2);
        MFUser mFUser = this.f;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
    }

    @DexIgnore
    public void b(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onUnitWeightChanged unit=" + unit);
        MFUser mFUser = this.f;
        if (mFUser != null) {
            mFUser.setWeightUnit(unit.getValue());
            gl3 gl3 = this.g;
            int weightInGrams = mFUser.getWeightInGrams();
            Unit weightUnit = mFUser.getWeightUnit();
            wd4.a((Object) weightUnit, "it.weightUnit");
            gl3.b(weightInGrams, weightUnit);
        }
    }

    @DexIgnore
    public void a(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onUnitHeightChanged unit=" + unit);
        MFUser mFUser = this.f;
        if (mFUser != null) {
            mFUser.setHeightUnit(unit.getValue());
            gl3 gl3 = this.g;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            Unit heightUnit = mFUser.getHeightUnit();
            wd4.a((Object) heightUnit, "it.heightUnit");
            gl3.a(heightInCentimeters, heightUnit);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "completeOnboarding currentUser=" + this.f);
        this.g.j0();
        MFUser mFUser = this.f;
        if (mFUser != null) {
            int age = MFUser.getAge(mFUser.getBirthday());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "completeOnboarding update heightUnit=" + mFUser.getHeightUnit() + ", weightUnit=" + mFUser.getWeightUnit() + ',' + " height=" + mFUser.getHeightInCentimeters() + ", weight=" + mFUser.getWeightInGrams());
            GetRecommendedGoalUseCase getRecommendedGoalUseCase = this.i;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            int weightInGrams = mFUser.getWeightInGrams();
            Gender gender = mFUser.getGender();
            wd4.a((Object) gender, "it.gender");
            getRecommendedGoalUseCase.a(new GetRecommendedGoalUseCase.c(age, heightInCentimeters, weightInGrams, gender), new b(mFUser, this, z));
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser, boolean z) {
        wd4.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d(j, "onSetUpHeightWeightComplete");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1(this, mFUser, z, (kc4) null), 3, (Object) null);
    }
}
