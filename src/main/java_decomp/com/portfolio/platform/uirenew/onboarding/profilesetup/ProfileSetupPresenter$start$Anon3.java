package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$Anon3", f = "ProfileSetupPresenter.kt", l = {131}, m = "invokeSuspend")
public final class ProfileSetupPresenter$start$Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$Anon3$Anon1", f = "ProfileSetupPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter$start$Anon3 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ProfileSetupPresenter$start$Anon3 profileSetupPresenter$start$Anon3, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = profileSetupPresenter$start$Anon3;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.y.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$start$Anon3(ProfileSetupPresenter profileSetupPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileSetupPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileSetupPresenter$start$Anon3 profileSetupPresenter$start$Anon3 = new ProfileSetupPresenter$start$Anon3(this.this$Anon0, kc4);
        profileSetupPresenter$start$Anon3.p$ = (lh4) obj;
        return profileSetupPresenter$start$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$start$Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ProfileSetupPresenter profileSetupPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ProfileSetupPresenter profileSetupPresenter2 = this.this$Anon0;
            gh4 a2 = profileSetupPresenter2.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = profileSetupPresenter2;
            this.label = 1;
            obj = kg4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            profileSetupPresenter = profileSetupPresenter2;
        } else if (i == 1) {
            profileSetupPresenter = (ProfileSetupPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        profileSetupPresenter.k = (MFUser) obj;
        MFUser c = this.this$Anon0.k;
        if (c != null) {
            this.this$Anon0.w.b(c);
        }
        return cb4.a;
    }
}
