package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
public final class OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<MFUser>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                UserRepository c = this.this$Anon0.this$Anon0.h;
                MFUser mFUser = this.this$Anon0.$user;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = c.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, MFUser mFUser, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = onboardingHeightWeightPresenter;
        this.$user = mFUser;
        this.$isDefaultValuesUsed = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 = new OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1(this.this$Anon0, this.$user, this.$isDefaultValuesUsed, kc4);
        onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1.p$ = (lh4) obj;
        return onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.$user.isUseDefaultBiometric()) {
                this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
            }
            gh4 a2 = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
