package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$Anon1", f = "ProfileSetupPresenter.kt", l = {251, 258}, m = "invokeSuspend")
public final class ProfileSetupPresenter$downloadRecommendedGoals$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $service;
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<GetRecommendedGoalUseCase.d, GetRecommendedGoalUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter$downloadRecommendedGoals$Anon1 a;

        @DexIgnore
        public a(ProfileSetupPresenter$downloadRecommendedGoals$Anon1 profileSetupPresenter$downloadRecommendedGoals$Anon1) {
            this.a = profileSetupPresenter$downloadRecommendedGoals$Anon1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetRecommendedGoalUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            ProfileSetupPresenter$downloadRecommendedGoals$Anon1 profileSetupPresenter$downloadRecommendedGoals$Anon1 = this.a;
            profileSetupPresenter$downloadRecommendedGoals$Anon1.this$Anon0.e(profileSetupPresenter$downloadRecommendedGoals$Anon1.$service);
        }

        @DexIgnore
        public void a(GetRecommendedGoalUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            ProfileSetupPresenter$downloadRecommendedGoals$Anon1 profileSetupPresenter$downloadRecommendedGoals$Anon1 = this.a;
            profileSetupPresenter$downloadRecommendedGoals$Anon1.this$Anon0.e(profileSetupPresenter$downloadRecommendedGoals$Anon1.$service);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$downloadRecommendedGoals$Anon1(ProfileSetupPresenter profileSetupPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileSetupPresenter;
        this.$service = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileSetupPresenter$downloadRecommendedGoals$Anon1 profileSetupPresenter$downloadRecommendedGoals$Anon1 = new ProfileSetupPresenter$downloadRecommendedGoals$Anon1(this.this$Anon0, this.$service, kc4);
        profileSetupPresenter$downloadRecommendedGoals$Anon1.p$ = (lh4) obj;
        return profileSetupPresenter$downloadRecommendedGoals$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$downloadRecommendedGoals$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        int i;
        lh4 lh4;
        Object a2 = oc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 a3 = this.this$Anon0.b();
            ProfileSetupPresenter$downloadRecommendedGoals$Anon1$currentUser$Anon1 profileSetupPresenter$downloadRecommendedGoals$Anon1$currentUser$Anon1 = new ProfileSetupPresenter$downloadRecommendedGoals$Anon1$currentUser$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a3, profileSetupPresenter$downloadRecommendedGoals$Anon1$currentUser$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i2 == 2) {
            i = this.I$Anon0;
            MFUser mFUser2 = (MFUser) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            mFUser = (MFUser) this.L$Anon2;
            GetRecommendedGoalUseCase b = this.this$Anon0.x;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            int weightInGrams = mFUser.getWeightInGrams();
            Gender gender = mFUser.getGender();
            wd4.a((Object) gender, "it.gender");
            b.a(new GetRecommendedGoalUseCase.c(i, heightInCentimeters, weightInGrams, gender), new a(this));
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        mFUser = (MFUser) obj;
        if (mFUser != null) {
            int age = MFUser.getAge(mFUser.getBirthday());
            this.this$Anon0.m().b(mFUser.getUserId());
            PortfolioApp c = PortfolioApp.W.c();
            String userId = mFUser.getUserId();
            wd4.a((Object) userId, "it.userId");
            c.p(userId);
            PortfolioApp c2 = PortfolioApp.W.c();
            this.L$Anon0 = lh4;
            this.L$Anon1 = mFUser;
            this.L$Anon2 = mFUser;
            this.I$Anon0 = age;
            this.label = 2;
            if (c2.a((kc4<? super cb4>) this) == a2) {
                return a2;
            }
            i = age;
            GetRecommendedGoalUseCase b2 = this.this$Anon0.x;
            int heightInCentimeters2 = mFUser.getHeightInCentimeters();
            int weightInGrams2 = mFUser.getWeightInGrams();
            Gender gender2 = mFUser.getGender();
            wd4.a((Object) gender2, "it.gender");
            b2.a(new GetRecommendedGoalUseCase.c(i, heightInCentimeters2, weightInGrams2, gender2), new a(this));
        }
        return cb4.a;
    }
}
