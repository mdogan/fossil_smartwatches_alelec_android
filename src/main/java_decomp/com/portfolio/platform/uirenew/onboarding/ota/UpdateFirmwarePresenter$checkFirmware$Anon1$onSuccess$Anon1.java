package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1", f = "UpdateFirmwarePresenter.kt", l = {213, 216}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkFirmware$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1(UpdateFirmwarePresenter$checkFirmware$Anon1 updateFirmwarePresenter$checkFirmware$Anon1, DownloadFirmwareByDeviceModelUsecase.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = updateFirmwarePresenter$checkFirmware$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, kc4);
        updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b0  */
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        String str;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            str = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str);
            gh4 b = this.this$Anon0.a.b();
            UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = str;
            this.label = 1;
            Object a3 = kg4.a(b, updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1, this);
            if (a3 == a) {
                return a;
            }
            Object obj2 = a3;
            lh4 = lh42;
            obj = obj2;
        } else if (i == 1) {
            str = (String) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            String str2 = (String) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            if (!((Boolean) obj).booleanValue()) {
                this.this$Anon0.a.o().v0();
            } else {
                UpdateFirmwarePresenter$checkFirmware$Anon1 updateFirmwarePresenter$checkFirmware$Anon1 = this.this$Anon0;
                updateFirmwarePresenter$checkFirmware$Anon1.a.c(updateFirmwarePresenter$checkFirmware$Anon1.c);
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        if (!booleanValue) {
            gh4 b2 = this.this$Anon0.a.b();
            UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = str;
            this.Z$Anon0 = booleanValue;
            this.label = 2;
            obj = kg4.a(b2, updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isLatestFw$Anon1, this);
            if (obj == a) {
                return a;
            }
            if (!((Boolean) obj).booleanValue()) {
            }
            return cb4.a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a4 = UpdateFirmwarePresenter.s.a();
        local2.e(a4, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str + " but device is DianaEV1!!!");
        return cb4.a;
    }
}
