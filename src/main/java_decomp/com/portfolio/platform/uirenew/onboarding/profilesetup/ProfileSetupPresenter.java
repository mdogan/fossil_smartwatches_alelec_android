package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.ls3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.or2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tl2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.vl3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wl3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import com.portfolio.platform.util.URLHelper;
import java.util.Calendar;
import java.util.Date;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileSetupPresenter extends vl3 {
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public SignUpEmailUseCase f;
    @DexIgnore
    public SignUpSocialUseCase g;
    @DexIgnore
    public or2 h;
    @DexIgnore
    public AnalyticsHelper i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public MFUser k;
    @DexIgnore
    public String l; // = "";
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public Calendar s; // = Calendar.getInstance();
    @DexIgnore
    public SignUpSocialAuth t;
    @DexIgnore
    public SignUpEmailAuth u;
    @DexIgnore
    public /* final */ int v; // = 16;
    @DexIgnore
    public /* final */ wl3 w;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase x;
    @DexIgnore
    public /* final */ UserRepository y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SignUpSocialUseCase.c, SignUpSocialUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth a;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter b;

        @DexIgnore
        public b(SignUpSocialAuth signUpSocialAuth, ProfileSetupPresenter profileSetupPresenter) {
            this.a = signUpSocialAuth;
            this.b = profileSetupPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SignUpSocialUseCase.c cVar) {
            wd4.b(cVar, "responseValue");
            PortfolioApp.W.c().g().a(this.b);
            this.b.d(this.a.getService());
        }

        @DexIgnore
        public void a(SignUpSocialUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.b.w.i();
            this.b.w.g(bVar.a(), bVar.b());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<SignUpEmailUseCase.c, SignUpEmailUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter a;

        @DexIgnore
        public c(ProfileSetupPresenter profileSetupPresenter) {
            this.a = profileSetupPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SignUpEmailUseCase.c cVar) {
            wd4.b(cVar, "responseValue");
            PortfolioApp.W.c().g().a(this.a);
            ProfileSetupPresenter profileSetupPresenter = this.a;
            String lowerCase = "Email".toLowerCase();
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            profileSetupPresenter.d(lowerCase);
        }

        @DexIgnore
        public void a(SignUpEmailUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.a.w.i();
            this.a.w.g(bVar.a(), bVar.b());
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = ProfileSetupPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "ProfileSetupPresenter::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    public ProfileSetupPresenter(wl3 wl3, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository) {
        wd4.b(wl3, "mView");
        wd4.b(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        wd4.b(userRepository, "mUserRepository");
        this.w = wl3;
        this.x = getRecommendedGoalUseCase;
        this.y = userRepository;
    }

    @DexIgnore
    public void f() {
        wl3 wl3 = this.w;
        Spanned fromHtml = Html.fromHtml(o());
        wd4.a((Object) fromHtml, "Html.fromHtml(getTermsOfUseString())");
        wl3.a(fromHtml);
        this.w.g();
        if (!this.j) {
            SignUpSocialAuth signUpSocialAuth = this.t;
            if (signUpSocialAuth != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = z;
                local.d(str, "start - mSocialAuth=" + signUpSocialAuth);
                this.w.a(signUpSocialAuth);
            }
            SignUpEmailAuth signUpEmailAuth = this.u;
            if (signUpEmailAuth != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = z;
                local2.d(str2, "start - mEmailAuth=" + signUpEmailAuth);
                this.w.c(signUpEmailAuth);
            }
        } else if (this.k == null) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileSetupPresenter$start$Anon3(this, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        if (this.j) {
            MFUser mFUser = this.k;
            if (mFUser != null) {
                mFUser.setEmail(this.l);
                String str = this.m;
                if (str != null) {
                    mFUser.setFirstName(str);
                    String str2 = this.n;
                    if (str2 != null) {
                        mFUser.setLastName(str2);
                        mFUser.setBirthday(this.p);
                        mFUser.setDiagnosticEnabled(this.r);
                        String str3 = this.o;
                        if (str3 == null) {
                            str3 = Gender.OTHER.toString();
                        }
                        mFUser.setGender(str3);
                        a(mFUser);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = z;
        local.d(str4, "create account socialAuth=" + this.t + " emailAuth=" + this.u);
        this.w.k();
        SignUpSocialAuth signUpSocialAuth = this.t;
        if (signUpSocialAuth != null) {
            signUpSocialAuth.setEmail(this.l);
            String str5 = this.o;
            if (str5 == null) {
                str5 = Gender.OTHER.toString();
            }
            signUpSocialAuth.setGender(str5);
            String str6 = this.p;
            if (str6 != null) {
                signUpSocialAuth.setBirthday(str6);
                signUpSocialAuth.setClientId(AppHelper.f.a(""));
                String str7 = this.m;
                if (str7 != null) {
                    signUpSocialAuth.setFirstName(str7);
                    String str8 = this.n;
                    if (str8 != null) {
                        signUpSocialAuth.setLastName(str8);
                        signUpSocialAuth.setDiagnosticEnabled(this.r);
                        SignUpSocialUseCase signUpSocialUseCase = this.g;
                        if (signUpSocialUseCase != null) {
                            signUpSocialUseCase.a(new SignUpSocialUseCase.a(signUpSocialAuth), new b(signUpSocialAuth, this));
                        } else {
                            wd4.d("mSignUpSocialUseCase");
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        SignUpEmailAuth signUpEmailAuth = this.u;
        if (signUpEmailAuth != null) {
            String str9 = this.o;
            if (str9 == null) {
                str9 = Gender.OTHER.toString();
            }
            signUpEmailAuth.setGender(str9);
            String str10 = this.p;
            if (str10 != null) {
                signUpEmailAuth.setBirthday(str10);
                signUpEmailAuth.setClientId(AppHelper.f.a(""));
                String str11 = this.m;
                if (str11 != null) {
                    signUpEmailAuth.setFirstName(str11);
                    String str12 = this.n;
                    if (str12 != null) {
                        signUpEmailAuth.setLastName(str12);
                        signUpEmailAuth.setDiagnosticEnabled(this.r);
                        SignUpEmailUseCase signUpEmailUseCase = this.f;
                        if (signUpEmailUseCase != null) {
                            signUpEmailUseCase.a(new SignUpEmailUseCase.a(signUpEmailAuth), new c(this));
                        } else {
                            wd4.d("mSignUpEmailUseCase");
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public Calendar i() {
        Calendar calendar = this.s;
        wd4.a((Object) calendar, "mBirthdayCalendar");
        return calendar;
    }

    @DexIgnore
    public void j() {
        Bundle bundle = new Bundle();
        bundle.putInt("DAY", this.s.get(5));
        bundle.putInt("MONTH", this.s.get(2) + 1);
        bundle.putInt("YEAR", this.s.get(1));
        this.w.a(bundle);
    }

    @DexIgnore
    public void k() {
        Bundle bundle = new Bundle();
        Calendar instance = Calendar.getInstance();
        bundle.putInt("DAY", instance.get(5));
        bundle.putInt("MONTH", instance.get(2) + 1);
        bundle.putInt("YEAR", instance.get(1) - 32);
        this.w.a(bundle);
    }

    @DexIgnore
    public SignUpEmailAuth l() {
        return this.u;
    }

    @DexIgnore
    public final AnalyticsHelper m() {
        AnalyticsHelper analyticsHelper = this.i;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wd4.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public SignUpSocialAuth n() {
        return this.t;
    }

    @DexIgnore
    public final String o() {
        String a2 = URLHelper.a(URLHelper.StaticPage.TERMS, (URLHelper.Feature) null);
        String a3 = URLHelper.a(URLHelper.StaticPage.PRIVACY, (URLHelper.Feature) null);
        String str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_ProfileSetup_Text__IAgreeToTheAHreftermofuseurlterms).toString();
        wd4.a((Object) a2, "termOfUseUrl");
        String a4 = cg4.a(str, "term_of_use_url", a2, false, 4, (Object) null);
        wd4.a((Object) a3, "privacyPolicyUrl");
        return cg4.a(a4, "privacy_policy", a3, false, 4, (Object) null);
    }

    @DexIgnore
    public final boolean p() {
        Calendar calendar = this.s;
        if (calendar == null) {
            return false;
        }
        if (calendar != null) {
            Years yearsBetween = Years.yearsBetween((ReadablePartial) LocalDate.fromCalendarFields(calendar), (ReadablePartial) LocalDate.now());
            wd4.a((Object) yearsBetween, "age");
            if (yearsBetween.getYears() >= this.v) {
                return true;
            }
            return false;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean q() {
        if (this.l.length() == 0) {
            this.w.a(false, false, "");
            return false;
        } else if (!ls3.a(this.l)) {
            wl3 wl3 = this.w;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_SignUp_InputError_Text__InvalidEmailAddress);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            wl3.a(false, true, a2);
            return false;
        } else {
            this.w.a(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean r() {
        String str = this.m;
        if (!(str == null || cg4.a(str))) {
            this.w.B(true);
            return true;
        }
        this.w.B(false);
        return false;
    }

    @DexIgnore
    public final boolean s() {
        String str = this.n;
        if (!(str == null || cg4.a(str))) {
            this.w.A(true);
            return true;
        }
        this.w.A(false);
        return false;
    }

    @DexIgnore
    public boolean t() {
        return this.j;
    }

    @DexIgnore
    public void u() {
        this.w.a(this);
    }

    @DexIgnore
    public final void v() {
        if (!q()) {
            this.w.H0();
        } else if (!r()) {
            this.w.H0();
        } else if (!s()) {
            this.w.H0();
        } else if (!p()) {
            this.w.H0();
        } else if (!this.q) {
            this.w.H0();
        } else {
            this.w.t0();
        }
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, "firstName");
        this.m = str;
        r();
        v();
    }

    @DexIgnore
    public void c(String str) {
        wd4.b(str, "lastName");
        this.n = str;
        s();
        v();
    }

    @DexIgnore
    public final ri4 d(String str) {
        wd4.b(str, Constants.SERVICE);
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileSetupPresenter$downloadRecommendedGoals$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void e(String str) {
        wd4.b(str, Constants.SERVICE);
        tl2 a2 = AnalyticsHelper.f.a("user_signup");
        String lowerCase = "Source".toLowerCase();
        wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        a2.a(lowerCase, str);
        a2.a();
        this.w.i();
        this.w.g0();
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "email");
        this.l = str;
        q();
        v();
    }

    @DexIgnore
    public void b(boolean z2) {
        this.q = z2;
        v();
    }

    @DexIgnore
    public void c(boolean z2) {
        this.j = z2;
    }

    @DexIgnore
    public void a(Date date, Calendar calendar) {
        wd4.b(date, "data");
        wd4.b(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d(z, "onBirthDayChanged");
        this.p = sk2.e(date);
        this.s = calendar;
        wl3 wl3 = this.w;
        String d = sk2.d(date);
        wd4.a((Object) d, "DateHelper.formatLocalDateMonth(data)");
        wl3.K(d);
        if (!p()) {
            wl3 wl32 = this.w;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_DateofBirthError_Text__YouCantUseTheAppIf);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026Text__YouCantUseTheAppIf)");
            wl32.b(false, a2);
        } else {
            this.w.b(true, "");
        }
        v();
    }

    @DexIgnore
    public void a(Gender gender) {
        wd4.b(gender, "gender");
        this.o = gender.toString();
        this.w.a(gender);
    }

    @DexIgnore
    public void a(boolean z2) {
        this.r = z2;
        v();
    }

    @DexIgnore
    public void a(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "auth");
        this.u = signUpEmailAuth;
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "auth");
        this.t = signUpSocialAuth;
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "updateAccount - userId = " + mFUser.getUserId());
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileSetupPresenter$updateAccount$Anon1(this, mFUser, (kc4) null), 3, (Object) null);
    }
}
