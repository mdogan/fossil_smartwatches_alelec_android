package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$Anon1", f = "ExploreWatchPresenter.kt", l = {40}, m = "invokeSuspend")
public final class ExploreWatchPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ExploreWatchPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExploreWatchPresenter$start$Anon1(ExploreWatchPresenter exploreWatchPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = exploreWatchPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ExploreWatchPresenter$start$Anon1 exploreWatchPresenter$start$Anon1 = new ExploreWatchPresenter$start$Anon1(this.this$Anon0, kc4);
        exploreWatchPresenter$start$Anon1.p$ = (lh4) obj;
        return exploreWatchPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ExploreWatchPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            ExploreWatchPresenter$start$Anon1$data$Anon1 exploreWatchPresenter$start$Anon1$data$Anon1 = new ExploreWatchPresenter$start$Anon1$data$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, exploreWatchPresenter$start$Anon1$data$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.j().u((List) obj);
        this.this$Anon0.j().g();
        return cb4.a;
    }
}
