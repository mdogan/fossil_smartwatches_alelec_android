package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$Anon1$response$Anon1", f = "ProfileSetupPresenter.kt", l = {374}, m = "invokeSuspend")
public final class ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<MFUser>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter$updateAccount$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(ProfileSetupPresenter$updateAccount$Anon1 profileSetupPresenter$updateAccount$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileSetupPresenter$updateAccount$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 profileSetupPresenter$updateAccount$Anon1$response$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(this.this$Anon0, kc4);
        profileSetupPresenter$updateAccount$Anon1$response$Anon1.p$ = (lh4) obj;
        return profileSetupPresenter$updateAccount$Anon1$response$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$updateAccount$Anon1$response$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            UserRepository d = this.this$Anon0.this$Anon0.y;
            MFUser mFUser = this.this$Anon0.$user;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = d.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
