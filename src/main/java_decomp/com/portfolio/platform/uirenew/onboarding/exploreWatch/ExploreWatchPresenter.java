package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ok3;
import com.fossil.blesdk.obfuscated.pk3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchPresenter extends ok3 {
    @DexIgnore
    public /* final */ pk3 f;
    @DexIgnore
    public /* final */ wj2 g;
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public ExploreWatchPresenter(pk3 pk3, wj2 wj2, DeviceRepository deviceRepository) {
        wd4.b(pk3, "mView");
        wd4.b(wj2, "mDeviceSettingFactory");
        wd4.b(deviceRepository, "mDeviceRepository");
        this.f = pk3;
        this.g = wj2;
        this.h = deviceRepository;
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ExploreWatchPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        PortfolioApp c = PortfolioApp.W.c();
        ri4 unused = mg4.b(mh4.a(c()), (CoroutineContext) null, (CoroutineStart) null, new ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(c, (kc4) null, this), 3, (Object) null);
        c.a(this.g, false, 13);
        this.f.s0();
    }

    @DexIgnore
    public final List<Explore> i() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        explore.setTitle(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__FlickTheWrist));
        explore.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__QuicklyTurnYourWristAwayTo));
        explore.setExploreType(Explore.ExploreType.WRIST_FLICK);
        Explore explore2 = new Explore();
        explore2.setTitle(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__DoubleTap));
        explore2.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__BrightenYourWatchFaceForEasy));
        explore2.setExploreType(Explore.ExploreType.DOUBLE_TAP);
        Explore explore3 = new Explore();
        explore3.setTitle(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__PressHoldMiddleButton));
        explore3.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__GoBackToYourMainWatch));
        explore3.setExploreType(Explore.ExploreType.PRESS_AND_HOLD);
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        return arrayList;
    }

    @DexIgnore
    public final pk3 j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }
}
