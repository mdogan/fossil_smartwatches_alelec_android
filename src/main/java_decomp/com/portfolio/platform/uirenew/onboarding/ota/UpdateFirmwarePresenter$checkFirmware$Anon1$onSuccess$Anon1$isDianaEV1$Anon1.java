package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.util.DeviceUtils;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Boolean>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1 = new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1(this.this$Anon0, kc4);
        updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1.p$ = (lh4) obj;
        return updateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1$isDianaEV1$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return pc4.a(DeviceUtils.g.a().b(this.this$Anon0.this$Anon0.b));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
