package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.al3;
import com.fossil.blesdk.obfuscated.fs2;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ForgotPasswordActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public al3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "email");
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        fs2 fs2 = (fs2) getSupportFragmentManager().a((int) R.id.content);
        if (fs2 == null) {
            fs2 = fs2.n.b();
            a((Fragment) fs2, fs2.n.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (fs2 != null) {
            g.a(new yk3(fs2)).a(this);
            if (bundle == null) {
                al3 al3 = this.B;
                if (al3 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    al3.c(stringExtra);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                al3 al32 = this.B;
                if (al32 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string == null) {
                        string = "";
                    }
                    al32.c(string);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            al3 al3 = this.B;
            if (al3 != null) {
                al3.a("EMAIL", bundle);
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle, persistableBundle);
    }
}
