package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wl3;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.enums.AuthType;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$Anon1", f = "ProfileSetupPresenter.kt", l = {374}, m = "invokeSuspend")
public final class ProfileSetupPresenter$updateAccount$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$updateAccount$Anon1(ProfileSetupPresenter profileSetupPresenter, MFUser mFUser, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileSetupPresenter;
        this.$user = mFUser;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileSetupPresenter$updateAccount$Anon1 profileSetupPresenter$updateAccount$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1(this.this$Anon0, this.$user, kc4);
        profileSetupPresenter$updateAccount$Anon1.p$ = (lh4) obj;
        return profileSetupPresenter$updateAccount$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$updateAccount$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b0, code lost:
        if (r6 != null) goto L_0x00b5;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            this.this$Anon0.w.H0();
            this.this$Anon0.w.k();
            gh4 a2 = this.this$Anon0.b();
            ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 profileSetupPresenter$updateAccount$Anon1$response$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, profileSetupPresenter$updateAccount$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.z, "updateAccount successfully");
            PortfolioApp.W.c().g().a(this.this$Anon0);
            ProfileSetupPresenter profileSetupPresenter = this.this$Anon0;
            AuthType authType = this.$user.getAuthType();
            wd4.a((Object) authType, "user.authType");
            String value = authType.getValue();
            wd4.a((Object) value, "user.authType.value");
            profileSetupPresenter.d(value);
        } else if (ro2 instanceof qo2) {
            FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.z, "updateAccount failed");
            wl3 e = this.this$Anon0.w;
            qo2 qo2 = (qo2) ro2;
            int a3 = qo2.a();
            ServerError c = qo2.c();
            if (c != null) {
                str = c.getMessage();
            }
            str = "";
            e.g(a3, str);
            this.this$Anon0.w.i();
            this.this$Anon0.w.t0();
        }
        return cb4.a;
    }
}
