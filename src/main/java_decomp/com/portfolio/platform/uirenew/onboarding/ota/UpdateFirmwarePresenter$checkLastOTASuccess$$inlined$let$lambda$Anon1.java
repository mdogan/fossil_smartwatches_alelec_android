package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public /* final */ /* synthetic */ String $lastFwTemp;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.n().getDeviceBySerial(this.this$Anon0.$deviceId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(String str, String str2, kc4 kc4, UpdateFirmwarePresenter updateFirmwarePresenter, Device device) {
        super(2, kc4);
        this.$deviceId = str;
        this.$lastFwTemp = str2;
        this.this$Anon0 = updateFirmwarePresenter;
        this.$device$inlined = device;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 = new UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(this.$deviceId, this.$lastFwTemp, kc4, this.this$Anon0, this.$device$inlined);
        updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device = (Device) obj;
        FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.s.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
        if (device != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
            if (!cg4.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.s.a(), "Handle OTA complete on check last OTA success");
                this.this$Anon0.o.n(this.$deviceId);
                this.this$Anon0.o().v0();
            } else {
                this.this$Anon0.a(this.$device$inlined);
            }
        }
        return cb4.a;
    }
}
