package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp $portfolioApp;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ExploreWatchPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(PortfolioApp portfolioApp, kc4 kc4, ExploreWatchPresenter exploreWatchPresenter) {
        super(2, kc4);
        this.$portfolioApp = portfolioApp;
        this.this$Anon0 = exploreWatchPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 = new ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(this.$portfolioApp, kc4, this.this$Anon0);
        exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002a, code lost:
        if (r0 != null) goto L_0x002f;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            String e = this.$portfolioApp.e();
            String deviceNameBySerial = this.this$Anon0.h.getDeviceNameBySerial(e);
            MisfitDeviceProfile a = DeviceHelper.o.e().a(e);
            if (a != null) {
                str = a.getFirmwareVersion();
            }
            str = "";
            AnalyticsHelper.f.c().a(DeviceHelper.o.b(e), deviceNameBySerial, str);
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, e, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
