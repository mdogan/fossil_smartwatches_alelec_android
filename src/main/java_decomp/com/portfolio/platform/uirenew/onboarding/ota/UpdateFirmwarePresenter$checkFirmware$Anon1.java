package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter$checkFirmware$Anon1 implements CoroutineUseCase.e<DownloadFirmwareByDeviceModelUsecase.d, DownloadFirmwareByDeviceModelUsecase.c> {
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Device c;

    @DexIgnore
    public UpdateFirmwarePresenter$checkFirmware$Anon1(UpdateFirmwarePresenter updateFirmwarePresenter, String str, Device device) {
        this.a = updateFirmwarePresenter;
        this.b = str;
        this.c = device;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(DownloadFirmwareByDeviceModelUsecase.d dVar) {
        wd4.b(dVar, "responseValue");
        ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$checkFirmware$Anon1$onSuccess$Anon1(this, dVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(DownloadFirmwareByDeviceModelUsecase.c cVar) {
        wd4.b(cVar, "errorValue");
        FLogger.INSTANCE.getLocal().e(UpdateFirmwarePresenter.s.a(), "checkFirmware - downloadFw FAILED!!!");
        this.a.o().f0();
    }
}
