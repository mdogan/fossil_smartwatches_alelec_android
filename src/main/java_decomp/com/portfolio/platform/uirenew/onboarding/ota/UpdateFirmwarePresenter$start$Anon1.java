package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.helper.AnalyticsHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$Anon1", f = "UpdateFirmwarePresenter.kt", l = {105}, m = "invokeSuspend")
public final class UpdateFirmwarePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$Anon1$Anon1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Device>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(UpdateFirmwarePresenter$start$Anon1 updateFirmwarePresenter$start$Anon1, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = updateFirmwarePresenter$start$Anon1;
            this.$activeSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$activeSerial, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.n().getDeviceBySerial(this.$activeSerial);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$start$Anon1(UpdateFirmwarePresenter updateFirmwarePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = updateFirmwarePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        UpdateFirmwarePresenter$start$Anon1 updateFirmwarePresenter$start$Anon1 = new UpdateFirmwarePresenter$start$Anon1(this.this$Anon0, kc4);
        updateFirmwarePresenter$start$Anon1.p$ = (lh4) obj;
        return updateFirmwarePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        UpdateFirmwarePresenter updateFirmwarePresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            str = PortfolioApp.W.c().e();
            UpdateFirmwarePresenter updateFirmwarePresenter2 = this.this$Anon0;
            gh4 b = updateFirmwarePresenter2.b();
            Anon1 anon1 = new Anon1(this, str, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = str;
            this.L$Anon2 = updateFirmwarePresenter2;
            this.label = 1;
            obj = kg4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
            updateFirmwarePresenter = updateFirmwarePresenter2;
        } else if (i == 1) {
            updateFirmwarePresenter = (UpdateFirmwarePresenter) this.L$Anon2;
            str = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        updateFirmwarePresenter.h = (Device) obj;
        boolean A = PortfolioApp.W.c().A();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = UpdateFirmwarePresenter.s.a();
        local.d(a2, "start - activeSerial=" + str + ", isDeviceOtaing=" + A);
        if (!A) {
            UpdateFirmwarePresenter updateFirmwarePresenter3 = this.this$Anon0;
            updateFirmwarePresenter3.b(updateFirmwarePresenter3.h);
        }
        this.this$Anon0.m();
        vl2 b2 = AnalyticsHelper.f.b("ota_session");
        this.this$Anon0.i = b2;
        AnalyticsHelper.f.a("ota_session", b2);
        return cb4.a;
    }
}
