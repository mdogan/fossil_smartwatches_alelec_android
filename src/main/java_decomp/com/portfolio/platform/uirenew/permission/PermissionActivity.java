package com.portfolio.platform.uirenew.permission;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.co3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.tn3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yn3;
import com.fossil.blesdk.obfuscated.zn3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PermissionActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public co3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, ArrayList<Integer> arrayList) {
            wd4.b(context, "context");
            wd4.b(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        tn3 tn3;
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(1);
        }
        ArrayList<Integer> integerArrayListExtra = getIntent().getIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS");
        if (integerArrayListExtra.contains(10)) {
            tn3 tn32 = (tn3) getSupportFragmentManager().a((int) R.id.content);
            tn3 tn33 = tn32;
            if (tn32 == null) {
                tn3 a2 = tn3.m.a();
                a((Fragment) a2, "AllowNotificationServiceFragment", (int) R.id.content);
                tn33 = a2;
            }
            wd4.a((Object) integerArrayListExtra, "listPermissions");
            tb4.a(integerArrayListExtra, PermissionActivity$onCreate$currentFragment$Anon1.INSTANCE);
            tn3 = tn33;
        } else {
            yn3 yn3 = (yn3) getSupportFragmentManager().a((int) R.id.content);
            tn3 = yn3;
            if (yn3 == null) {
                yn3 b = yn3.o.b();
                a((Fragment) b, yn3.o.a(), (int) R.id.content);
                tn3 = b;
            }
        }
        m42 g = PortfolioApp.W.c().g();
        if (tn3 != null) {
            g.a(new zn3(tn3, integerArrayListExtra)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionContract.View");
    }
}
