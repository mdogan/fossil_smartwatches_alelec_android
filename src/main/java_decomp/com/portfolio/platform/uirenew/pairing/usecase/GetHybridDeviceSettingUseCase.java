package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.bs3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.qn3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetHybridDeviceSettingUseCase extends CoroutineUseCase<pn3, qn3, on3> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = GetHybridDeviceSettingUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public GetHybridDeviceSettingUseCase(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        wd4.b(hybridPresetRepository, "mPresetRepository");
        wd4.b(microAppRepository, "mMicroAppRepository");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(notificationsRepository, "mNotificationsRepository");
        wd4.b(categoryRepository, "mCategoryRepository");
        wd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return k;
    }

    @DexIgnore
    public final ri4 d() {
        return mg4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new GetHybridDeviceSettingUseCase$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public Object a(pn3 pn3, kc4<? super cb4> kc4) {
        if (pn3 == null) {
            a(new on3(600, ""));
            return cb4.a;
        }
        this.d = pn3.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!bs3.b(PortfolioApp.W.c())) {
                a(new on3(601, ""));
                return cb4.a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                DeviceHelper.a aVar = DeviceHelper.o;
                String str3 = this.d;
                if (str3 == null) {
                    wd4.a();
                    throw null;
                } else if (aVar.e(str3)) {
                    d();
                    return cb4.a;
                }
            }
            a(new on3(600, ""));
            return cb4.a;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final ri4 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1(sparseArray, z, str, (kc4) null), 3, (Object) null);
    }
}
