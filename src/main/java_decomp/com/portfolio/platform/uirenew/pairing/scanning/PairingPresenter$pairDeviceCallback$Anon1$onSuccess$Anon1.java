package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.HashMap;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1", f = "PairingPresenter.kt", l = {254, 260}, m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $devicePairingSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$pairDeviceCallback$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1$Anon1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ri4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0030, code lost:
            if (r0 != null) goto L_0x0035;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            String str;
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                String deviceNameBySerial = this.this$Anon0.this$Anon0.a.u.getDeviceNameBySerial(this.this$Anon0.$devicePairingSerial);
                MisfitDeviceProfile a = DeviceHelper.o.e().a(this.this$Anon0.$devicePairingSerial);
                if (a != null) {
                    str = a.getFirmwareVersion();
                }
                str = "";
                AnalyticsHelper.f.c().a(DeviceHelper.o.b(this.this$Anon0.$devicePairingSerial), deviceNameBySerial, str);
                return this.this$Anon0.this$Anon0.a.w.a(new SetNotificationUseCase.b(this.this$Anon0.$devicePairingSerial), (CoroutineUseCase.e) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1(PairingPresenter$pairDeviceCallback$Anon1 pairingPresenter$pairDeviceCallback$Anon1, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pairingPresenter$pairDeviceCallback$Anon1;
        this.$devicePairingSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1(this.this$Anon0, this.$devicePairingSerial, kc4);
        pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    public final Object invokeSuspend(Object obj) {
        vl2 o;
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 a2 = this.this$Anon0.a.b();
            PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1$params$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1$params$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1$params$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, pairingPresenter$pairDeviceCallback$Anon1$onSuccess$Anon1$params$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            HashMap hashMap = (HashMap) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            o = this.this$Anon0.a.o();
            if (o != null) {
                o.a("");
            }
            AnalyticsHelper.f.e("setup_device_session");
            this.this$Anon0.a.v();
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HashMap hashMap2 = (HashMap) obj;
        if (hashMap2.containsKey("Style_Number") && hashMap2.containsKey("Device_Name")) {
            this.this$Anon0.a.a("pair_success", (Map<String, String>) hashMap2);
            this.this$Anon0.a.a((Map<String, String>) hashMap2);
        }
        gh4 b = this.this$Anon0.a.c();
        Anon1 anon1 = new Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = hashMap2;
        this.label = 2;
        if (kg4.a(b, anon1, this) == a) {
            return a;
        }
        o = this.this$Anon0.a.o();
        if (o != null) {
        }
        AnalyticsHelper.f.e("setup_device_session");
        this.this$Anon0.a.v();
        return cb4.a;
    }
}
