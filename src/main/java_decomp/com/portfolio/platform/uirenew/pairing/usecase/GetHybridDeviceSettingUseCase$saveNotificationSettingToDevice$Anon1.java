package com.portfolio.platform.uirenew.pairing.usecase;

import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1", f = "GetHybridDeviceSettingUseCase.kt", l = {}, m = "invokeSuspend")
public final class GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SparseArray $data;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isMovemberModel;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1(SparseArray sparseArray, boolean z, String str, kc4 kc4) {
        super(2, kc4);
        this.$data = sparseArray;
        this.$isMovemberModel = z;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1 getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1 = new GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1(this.$data, this.$isMovemberModel, this.$serialNumber, kc4);
        getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1.p$ = (lh4) obj;
        return getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            AppNotificationFilterSettings a = NotificationAppHelper.b.a((SparseArray<List<BaseFeatureModel>>) this.$data, this.$isMovemberModel);
            PortfolioApp.W.c().a(a, this.$serialNumber);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = GetHybridDeviceSettingUseCase.k;
            local.d(e, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
