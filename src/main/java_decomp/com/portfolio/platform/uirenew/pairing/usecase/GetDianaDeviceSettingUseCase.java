package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.bs3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.qn3;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetDianaDeviceSettingUseCase extends CoroutineUseCase<pn3, qn3, on3> {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ CategoryRepository h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;
    @DexIgnore
    public /* final */ wy2 j;
    @DexIgnore
    public /* final */ qx2 k;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase l;
    @DexIgnore
    public /* final */ fn2 m;
    @DexIgnore
    public /* final */ WatchLocalizationRepository n;
    @DexIgnore
    public /* final */ AlarmsRepository o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = GetDianaDeviceSettingUseCase.class.getSimpleName();
        wd4.a((Object) simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public GetDianaDeviceSettingUseCase(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, CategoryRepository categoryRepository, WatchFaceRepository watchFaceRepository, wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, fn2 fn2, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository) {
        wd4.b(watchAppRepository, "mWatchAppRepository");
        wd4.b(complicationRepository, "mComplicationRepository");
        wd4.b(dianaPresetRepository, "mDianaPresetRepository");
        wd4.b(categoryRepository, "mCategoryRepository");
        wd4.b(watchFaceRepository, "mWatchFaceRepository");
        wd4.b(wy2, "mGetApp");
        wd4.b(qx2, "mGetAllContactGroups");
        wd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wd4.b(fn2, "mSharePref");
        wd4.b(watchLocalizationRepository, "mWatchLocalizationRepository");
        wd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = watchAppRepository;
        this.f = complicationRepository;
        this.g = dianaPresetRepository;
        this.h = categoryRepository;
        this.i = watchFaceRepository;
        this.j = wy2;
        this.k = qx2;
        this.l = notificationSettingsDatabase;
        this.m = fn2;
        this.n = watchLocalizationRepository;
        this.o = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return p;
    }

    @DexIgnore
    public final ri4 d() {
        return mg4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new GetDianaDeviceSettingUseCase$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public Object a(pn3 pn3, kc4<? super cb4> kc4) {
        if (pn3 == null) {
            a(new on3(600, ""));
            return cb4.a;
        }
        this.d = pn3.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "download device setting of " + this.d);
        if (!bs3.b(PortfolioApp.W.c())) {
            a(new on3(601, ""));
            return cb4.a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            DeviceHelper.a aVar = DeviceHelper.o;
            String str2 = this.d;
            if (str2 == null) {
                wd4.a();
                throw null;
            } else if (aVar.e(str2)) {
                d();
                return cb4.a;
            }
        }
        a(new on3(600, ""));
        return cb4.a;
    }
}
