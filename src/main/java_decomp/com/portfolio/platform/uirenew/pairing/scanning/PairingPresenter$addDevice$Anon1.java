package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hn3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import java.util.Iterator;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$Anon1", f = "PairingPresenter.kt", l = {370}, m = "invokeSuspend")
public final class PairingPresenter$addDevice$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ShineDevice $shineDevice;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$addDevice$Anon1(PairingPresenter pairingPresenter, ShineDevice shineDevice, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pairingPresenter;
        this.$shineDevice = shineDevice;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PairingPresenter$addDevice$Anon1 pairingPresenter$addDevice$Anon1 = new PairingPresenter$addDevice$Anon1(this.this$Anon0, this.$shineDevice, kc4);
        pairingPresenter$addDevice$Anon1.p$ = (lh4) obj;
        return pairingPresenter$addDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$addDevice$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        T t;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            PairingPresenter$addDevice$Anon1$serial$Anon1 pairingPresenter$addDevice$Anon1$serial$Anon1 = new PairingPresenter$addDevice$Anon1$serial$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, pairingPresenter$addDevice$Anon1$serial$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String str = (String) obj;
        Iterator<T> it = this.this$Anon0.r().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (pc4.a(wd4.a((Object) ((ShineDevice) ((Pair) t).getFirst()).getSerial(), (Object) this.$shineDevice.getSerial())).booleanValue()) {
                break;
            }
        }
        Pair pair = (Pair) t;
        if (pair == null) {
            this.this$Anon0.r().add(new Pair(this.$shineDevice, str));
        } else {
            ((ShineDevice) pair.getFirst()).updateRssi(this.$shineDevice.getRssi());
        }
        hn3 g = this.this$Anon0.s;
        PairingPresenter pairingPresenter = this.this$Anon0;
        g.h(pairingPresenter.a(pairingPresenter.r()));
        return cb4.a;
    }
}
