package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$Anon1$serial$Anon1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
public final class PairingPresenter$addDevice$Anon1$serial$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$addDevice$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$addDevice$Anon1$serial$Anon1(PairingPresenter$addDevice$Anon1 pairingPresenter$addDevice$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pairingPresenter$addDevice$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PairingPresenter$addDevice$Anon1$serial$Anon1 pairingPresenter$addDevice$Anon1$serial$Anon1 = new PairingPresenter$addDevice$Anon1$serial$Anon1(this.this$Anon0, kc4);
        pairingPresenter$addDevice$Anon1$serial$Anon1.p$ = (lh4) obj;
        return pairingPresenter$addDevice$Anon1$serial$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$addDevice$Anon1$serial$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            DeviceRepository c = this.this$Anon0.this$Anon0.u;
            String serial = this.this$Anon0.$shineDevice.getSerial();
            wd4.a((Object) serial, "shineDevice.serial");
            return c.getDeviceNameBySerial(serial);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
