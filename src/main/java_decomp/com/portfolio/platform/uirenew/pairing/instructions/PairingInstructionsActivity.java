package com.portfolio.platform.uirenew.pairing.instructions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.nm3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.um3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wm3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingInstructionsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public wm3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            aVar.a(context, z);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            wd4.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("", "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingInstructionsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        wd4.a((Object) PairingInstructionsActivity.class.getSimpleName(), "PairingInstructionsActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        nm3 nm3 = (nm3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (nm3 == null) {
            nm3 = nm3.o.a(z);
            a((Fragment) nm3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new um3(nm3)).a(this);
    }
}
