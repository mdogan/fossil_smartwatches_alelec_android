package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.in3;
import com.fossil.blesdk.obfuscated.mm3;
import com.fossil.blesdk.obfuscated.qs3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a((rd4) null);
    @DexIgnore
    public PairingPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PairingActivity.C;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            wd4.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = PairingActivity.class.getSimpleName();
        wd4.a((Object) simpleName, "PairingActivity::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        mm3 mm3 = (mm3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (mm3 == null) {
            mm3 = mm3.n.a(z);
            a((Fragment) mm3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new in3(mm3)).a(this);
        if (bundle != null) {
            PairingPresenter pairingPresenter = this.B;
            if (pairingPresenter != null) {
                pairingPresenter.d(bundle.getBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP"));
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            PairingPresenter pairingPresenter = this.B;
            if (pairingPresenter != null) {
                bundle.putBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP", pairingPresenter.t());
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        s();
    }

    @DexIgnore
    public final void s() {
        qs3.a.a(this, MFDeviceService.class, Constants.START_FOREGROUND_ACTION);
        qs3.a.a(this, ButtonService.class, Constants.START_FOREGROUND_ACTION);
    }
}
