package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hn3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.tl2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import java.util.HashMap;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1", f = "PairingPresenter.kt", l = {281}, m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase.h $errorValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$pairDeviceCallback$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1(PairingPresenter$pairDeviceCallback$Anon1 pairingPresenter$pairDeviceCallback$Anon1, LinkDeviceUseCase.h hVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pairingPresenter$pairDeviceCallback$Anon1;
        this.$errorValue = hVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1(this.this$Anon0, this.$errorValue, kc4);
        pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1.p$ = (lh4) obj;
        return pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
            gh4 a3 = this.this$Anon0.a.b();
            PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a3, pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HashMap hashMap = (HashMap) obj;
        if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
            this.this$Anon0.a.a("pair_fail", (Map<String, String>) hashMap);
        }
        this.this$Anon0.a.s.a();
        LinkDeviceUseCase.h hVar = this.$errorValue;
        if (hVar instanceof LinkDeviceUseCase.c) {
            hn3 g = this.this$Anon0.a.s;
            int b = this.$errorValue.b();
            ShineDevice f = this.this$Anon0.a.f;
            if (f != null) {
                str = f.getSerial();
            }
            g.c(b, str);
        } else if (hVar instanceof LinkDeviceUseCase.j) {
            this.this$Anon0.a.s.a(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
        } else if (hVar instanceof LinkDeviceUseCase.d) {
            this.this$Anon0.a.s.y();
        }
        String valueOf = String.valueOf(this.$errorValue.b());
        tl2 a4 = AnalyticsHelper.f.a("setup_device_session_optional_error");
        a4.a("error_code", valueOf);
        vl2 o = this.this$Anon0.a.o();
        if (o != null) {
            o.a(a4);
        }
        vl2 o2 = this.this$Anon0.a.o();
        if (o2 != null) {
            o2.a(valueOf);
        }
        AnalyticsHelper.f.e("setup_device_session");
        return cb4.a;
    }
}
