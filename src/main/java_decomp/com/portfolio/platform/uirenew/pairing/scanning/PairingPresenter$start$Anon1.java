package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hn3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mm3;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$Anon1", f = "PairingPresenter.kt", l = {100, 102}, m = "invokeSuspend")
public final class PairingPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$Anon1$Anon1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends Device>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(PairingPresenter$start$Anon1 pairingPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = pairingPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.u.getAllDevice();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$Anon1$Anon2", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends SKUModel>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(PairingPresenter$start$Anon1 pairingPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = pairingPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.u.getSupportedSku();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$start$Anon1(PairingPresenter pairingPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pairingPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PairingPresenter$start$Anon1 pairingPresenter$start$Anon1 = new PairingPresenter$start$Anon1(this.this$Anon0, kc4);
        pairingPresenter$start$Anon1.p$ = (lh4) obj;
        return pairingPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0112  */
    public final Object invokeSuspend(Object obj) {
        ArrayList<SKUModel> arrayList;
        hn3 g;
        lh4 lh4;
        ArrayList<Device> arrayList2;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            this.this$Anon0.s().clear();
            arrayList2 = this.this$Anon0.s();
            gh4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = arrayList2;
            this.label = 1;
            obj = kg4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            arrayList2 = (ArrayList) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            arrayList = (ArrayList) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            arrayList.addAll((Collection) obj);
            sc.a((Context) PortfolioApp.W.c()).a(this.this$Anon0.r, new IntentFilter("SCAN_DEVICE_FOUND"));
            this.this$Anon0.t.k();
            this.this$Anon0.i = new Handler(Looper.getMainLooper());
            if (this.this$Anon0.f != null) {
                LinkDeviceUseCase e = this.this$Anon0.t;
                ShineDevice f = this.this$Anon0.f;
                if (f != null) {
                    e.a(f, (CoroutineUseCase.e<? super LinkDeviceUseCase.i, ? super LinkDeviceUseCase.h>) this.this$Anon0.q);
                    BleCommandResultManager.d.a(CommunicateMode.LINK);
                } else {
                    wd4.a();
                    throw null;
                }
            }
            cn2 cn2 = cn2.d;
            g = this.this$Anon0.s;
            if (g == null) {
                if (cn2.a(cn2, ((mm3) g).getContext(), "PAIR_DEVICE", false, 4, (Object) null)) {
                    this.this$Anon0.u();
                }
                return cb4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList2.addAll((Collection) obj);
        this.this$Anon0.n().clear();
        ArrayList<SKUModel> n = this.this$Anon0.n();
        gh4 b2 = this.this$Anon0.c();
        Anon2 anon2 = new Anon2(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = n;
        this.label = 2;
        Object a2 = kg4.a(b2, anon2, this);
        if (a2 == a) {
            return a;
        }
        arrayList = n;
        obj = a2;
        arrayList.addAll((Collection) obj);
        sc.a((Context) PortfolioApp.W.c()).a(this.this$Anon0.r, new IntentFilter("SCAN_DEVICE_FOUND"));
        this.this$Anon0.t.k();
        this.this$Anon0.i = new Handler(Looper.getMainLooper());
        if (this.this$Anon0.f != null) {
        }
        cn2 cn22 = cn2.d;
        g = this.this$Anon0.s;
        if (g == null) {
        }
    }
}
