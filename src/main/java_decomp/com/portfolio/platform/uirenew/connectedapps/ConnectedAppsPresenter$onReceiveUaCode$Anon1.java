package com.portfolio.platform.uirenew.connectedapps;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hr3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1", f = "ConnectedAppsPresenter.kt", l = {120}, m = "invokeSuspend")
public final class ConnectedAppsPresenter$onReceiveUaCode$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $code;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ConnectedAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1", f = "ConnectedAppsPresenter.kt", l = {121}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter$onReceiveUaCode$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0131Anon1 implements hr3.b {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public C0131Anon1(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            public void a() {
                ri4 unused = mg4.b(this.a.this$Anon0.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1$onLoginSuccess$Anon1(this, (kc4) null), 3, (Object) null);
            }

            @DexIgnore
            public void a(String str) {
                FLogger.INSTANCE.getLocal().d(ConnectedAppsPresenter.m.a(), str);
                this.a.this$Anon0.this$Anon0.h.i();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ConnectedAppsPresenter$onReceiveUaCode$Anon1 connectedAppsPresenter$onReceiveUaCode$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = connectedAppsPresenter$onReceiveUaCode$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                hr3 r = this.this$Anon0.this$Anon0.j.r();
                String str = this.this$Anon0.$code;
                C0131Anon1 anon1 = new C0131Anon1(this);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (r.a(str, (hr3.b) anon1, (kc4<? super cb4>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cb4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$onReceiveUaCode$Anon1(ConnectedAppsPresenter connectedAppsPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = connectedAppsPresenter;
        this.$code = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ConnectedAppsPresenter$onReceiveUaCode$Anon1 connectedAppsPresenter$onReceiveUaCode$Anon1 = new ConnectedAppsPresenter$onReceiveUaCode$Anon1(this.this$Anon0, this.$code, kc4);
        connectedAppsPresenter$onReceiveUaCode$Anon1.p$ = (lh4) obj;
        return connectedAppsPresenter$onReceiveUaCode$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ConnectedAppsPresenter$onReceiveUaCode$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
