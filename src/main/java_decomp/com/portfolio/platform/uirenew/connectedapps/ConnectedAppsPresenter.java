package com.portfolio.platform.uirenew.connectedapps;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.al2;
import com.fossil.blesdk.obfuscated.ie0;
import com.fossil.blesdk.obfuscated.ju2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.ku2;
import com.fossil.blesdk.obfuscated.le0;
import com.fossil.blesdk.obfuscated.mg3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oe0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.vd0;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.Integration;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public final class ConnectedAppsPresenter extends ju2 implements al2.d {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public /* final */ ku2 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ PortfolioApp j;
    @DexIgnore
    public /* final */ al2 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ConnectedAppsPresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends le0<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ConnectedAppsPresenter connectedAppsPresenter, Activity activity, int i) {
            super(activity, i);
            this.c = connectedAppsPresenter;
        }

        @DexReplace
        public void b(Status status) {
            wd4.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = ConnectedAppsPresenter.m.a();
            local.d(a, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.L() + ", statusMessage = " + status.J() + ", statusCode = " + status.I());
            if (status.I() == 17 || status.I() == 5010) {
                this.c.k();
                return;
            }
            this.c.h.y(true);
            this.c.h.o(status.I());
        }

        // @DexIgnore
        /* renamed from: c */
        // public void a(Status status) {
        //     wd4.b(status, "status");
        //     ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //     String a = ConnectedAppsPresenter.m.a();
        //     local.d(a, "onGFLogout.onSuccess(): isSuccess = " + status.L() + ", statusMessage = " + status.J());
        //     if (status.L()) {
        //         this.c.k();
        //         return;
        //     }
        //     this.c.h.y(true);
        //     this.c.h.o(status.I());
        // }
    }

    /*
    static {
        String simpleName = ConnectedAppsPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public ConnectedAppsPresenter(ku2 ku2, UserRepository userRepository, PortfolioApp portfolioApp, al2 al2) {
        wd4.b(ku2, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(portfolioApp, "mApp");
        wd4.b(al2, "mGoogleFitHelper");
        this.h = ku2;
        this.i = userRepository;
        this.j = portfolioApp;
        this.k = al2;
    }

    @DexIgnore
    public void i() {
        this.k.a();
    }

    @DexIgnore
    public void j() {
        throw null;
        // if (this.j.r().b()) {
        //     FLogger.INSTANCE.getLocal().d(l, "setUpUnderAmourRecord - Logging out of UA");
        //     ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onUASelected$Anon1(this, (kc4) null), 3, (Object) null);
        //     return;
        // }
        // ri4 unused2 = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onUASelected$Anon2(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void k() {
        this.k.b();
        String name = Integration.GoogleFit.getName();
        wd4.a((Object) name, "Integration.GoogleFit.getName()");
        c(name);
        this.h.y(false);
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "connectedAppName");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            wd4.a((Object) integrations, "mUser!!.integrations");
            if (!integrations.contains(str)) {
                integrations.add(str);
            }
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(String str) {
        wd4.b(str, "connectedAppName");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            wd4.a((Object) integrations, "mUser!!.integrations");
            integrations.remove(str);
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        throw null;
        // ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        Log.d(l, "stop");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            throw null;
            // ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$stop$$inlined$let$lambda$Anon1(mFUser, (kc4) null, this), 3, (Object) null);
        }
        this.k.a((al2.d) null);
    }

    @DexIgnore
    public void h() {
        if (!PortfolioApp.W.c().w()) {
            this.h.o(601);
            this.h.y(this.k.e());
        } else if (this.k.e()) {
            this.k.g();
        } else if (this.k.d()) {
            this.k.a();
        } else {
            al2 al2 = this.k;
            ku2 ku2 = this.h;
            if (ku2 != null) {
                al2.a((Fragment) (mg3) ku2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "code");
        this.h.k();
        throw null;
        // ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onReceiveUaCode$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexReplace
    public void a() {
        FLogger.INSTANCE.getLocal().d(l, "onGFConnectionSuccess");
        String name = Integration.GoogleFit.getName();
        wd4.a((Object) name, "Integration.GoogleFit.getName()");
        b(name);
        this.h.y(true);
    }

    @DexIgnore
    public void a(vd0 vd0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "onGFConnectionFailed - Cause: " + vd0);
        if (vd0 == null) {
            return;
        }
        if (vd0.K()) {
            this.g = false;
            this.h.a(vd0);
            return;
        }
        this.h.o(vd0.H());
    }

    @DexIgnore
    public void a(ie0<Status> ie0) {
        if (ie0 != null) {
            ku2 ku2 = this.h;
            if (ku2 != null) {
                FragmentActivity activity = ((mg3) ku2).getActivity();
                if (activity != null) {
                    throw null;
                    // ie0.a((oe0<? super Status>) new b(this, activity, 3));
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }
}
