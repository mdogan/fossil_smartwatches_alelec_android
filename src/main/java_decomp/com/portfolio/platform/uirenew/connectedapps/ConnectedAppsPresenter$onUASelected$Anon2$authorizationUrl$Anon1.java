package com.portfolio.platform.uirenew.connectedapps;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.hr3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.underamour.UAWebViewActivity;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1", f = "ConnectedAppsPresenter.kt", l = {110}, m = "invokeSuspend")
public final class ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    public ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1(kc4 kc4) {
        super(2, kc4);
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 = new ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1(kc4);
        connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1.p$ = (lh4) obj;
        return connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            hr3 r = PortfolioApp.W.c().r();
            String a2 = UAWebViewActivity.H.a();
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = r.a(a2, (kc4<? super String>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
