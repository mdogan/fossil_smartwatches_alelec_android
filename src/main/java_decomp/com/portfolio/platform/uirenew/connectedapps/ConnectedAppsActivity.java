package com.portfolio.platform.uirenew.connectedapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.lu2;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.mg3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectedAppsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((rd4) null);
    @DexIgnore
    public mg3 B;
    @DexIgnore
    public ConnectedAppsPresenter C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            Intent intent = new Intent(context, ConnectedAppsActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        mg3 mg3 = this.B;
        if (mg3 != null) {
            mg3.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        this.B = (mg3) getSupportFragmentManager().a((int) R.id.content);
        if (this.B == null) {
            this.B = mg3.n.b();
            mg3 mg3 = this.B;
            if (mg3 != null) {
                a((Fragment) mg3, mg3.n.a(), (int) R.id.content);
            } else {
                wd4.a();
                throw null;
            }
        }
        m42 g = PortfolioApp.W.c().g();
        mg3 mg32 = this.B;
        if (mg32 != null) {
            g.a(new lu2(mg32)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.connectedapps.ConnectedAppsContract.View");
    }
}
