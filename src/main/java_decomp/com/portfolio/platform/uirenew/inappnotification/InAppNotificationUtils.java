package com.portfolio.platform.uirenew.inappnotification;

import android.app.Activity;
import android.widget.Toast;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wa4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xe4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yd4;
import com.portfolio.platform.data.InAppNotification;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationUtils {
    @DexIgnore
    public static /* final */ wa4 a; // = ya4.a(InAppNotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static /* final */ a b; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ xe4[] a;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(yd4.a(a.class), "instance", "getInstance()Lcom/portfolio/platform/uirenew/inappnotification/InAppNotificationUtils;");
            yd4.a((PropertyReference1) propertyReference1Impl);
            a = new xe4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final InAppNotificationUtils a() {
            wa4 a2 = InAppNotificationUtils.a;
            a aVar = InAppNotificationUtils.b;
            xe4 xe4 = a[0];
            return (InAppNotificationUtils) a2.getValue();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ InAppNotificationUtils a; // = new InAppNotificationUtils();
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public final InAppNotificationUtils a() {
            return a;
        }
    }

    /*
    static {
        wd4.a((Object) InAppNotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName(), "InAppNotificationUtils::\u2026lass.javaClass.simpleName");
    }
    */

    @DexIgnore
    public final void a(Activity activity, InAppNotification inAppNotification) {
        wd4.b(inAppNotification, "inAppNotification");
        if (activity != null) {
            Toast.makeText(activity, "Title: " + inAppNotification.getTitle() + " - Message Body: " + inAppNotification.getContent(), 1).show();
        }
    }
}
