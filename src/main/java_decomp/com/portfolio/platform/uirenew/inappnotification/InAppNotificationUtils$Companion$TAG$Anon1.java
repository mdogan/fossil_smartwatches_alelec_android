package com.portfolio.platform.uirenew.inappnotification;

import com.fossil.blesdk.obfuscated.hd4;
import com.fossil.blesdk.obfuscated.ve4;
import com.fossil.blesdk.obfuscated.yd4;
import com.fossil.blesdk.obfuscated.ye4;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class InAppNotificationUtils$Companion$TAG$Anon1 extends PropertyReference1 {
    @DexIgnore
    public static /* final */ ye4 INSTANCE; // = new InAppNotificationUtils$Companion$TAG$Anon1();

    @DexIgnore
    public Object get(Object obj) {
        return hd4.a((InAppNotificationUtils) obj);
    }

    @DexIgnore
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    public ve4 getOwner() {
        return yd4.a(hd4.class, "app_fossilRelease");
    }

    @DexIgnore
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
