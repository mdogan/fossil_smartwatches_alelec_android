package com.portfolio.platform.uirenew.inappnotification;

import com.fossil.blesdk.obfuscated.id4;
import com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationUtils$Companion$instance$Anon2 extends Lambda implements id4<InAppNotificationUtils> {
    @DexIgnore
    public static /* final */ InAppNotificationUtils$Companion$instance$Anon2 INSTANCE; // = new InAppNotificationUtils$Companion$instance$Anon2();

    @DexIgnore
    public InAppNotificationUtils$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final InAppNotificationUtils invoke() {
        return InAppNotificationUtils.b.b.a();
    }
}
