package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rp4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {129}, m = "invokeSuspend")
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ u03.d $responseValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1, u03.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 = new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, kc4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetAllHybridContactGroups onSuccess");
            ArrayList arrayList = new ArrayList();
            sh4 a2 = mg4.a(lh4, this.this$Anon0.a.b(), (CoroutineStart) null, new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1(this, arrayList, (kc4) null), 2, (Object) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = arrayList;
            this.L$Anon2 = a2;
            this.label = 1;
            if (a2.a(this) == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            sh4 sh4 = (sh4) this.L$Anon2;
            list = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.a.o().addAll(list);
        this.this$Anon0.a.q().addAll(list);
        List<Object> p = this.this$Anon0.a.p();
        Object[] array = list.toArray(new ContactWrapper[0]);
        if (array != null) {
            Serializable a3 = rp4.a((Serializable) array);
            wd4.a((Object) a3, "SerializationUtils.clone\u2026apperList.toTypedArray())");
            tb4.a(p, (T[]) (Object[]) a3);
            this.this$Anon0.a.t();
            return cb4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
