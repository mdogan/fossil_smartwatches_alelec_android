package com.portfolio.platform.uirenew.home.details.heartrate;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.of3;
import com.fossil.blesdk.obfuscated.pf3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.enums.Unit;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$Anon1", f = "HeartRateDetailPresenter.kt", l = {110}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements dc<ps3<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$start$Anon1 a;

        @DexIgnore
        public a(HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1) {
            this.a = heartRateDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<DailyHeartRateSummary>> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- status=");
            Status status = null;
            sb.append(ps3 != null ? ps3.f() : null);
            sb.append(", resource=");
            sb.append(ps3 != null ? (List) ps3.d() : null);
            sb.append(", status=");
            if (ps3 != null) {
                status = ps3.f();
            }
            sb.append(status);
            local.d("HeartRateDetailPresenter", sb.toString());
            if (ps3 != null && ps3.f() != Status.DATABASE_LOADING) {
                this.a.this$Anon0.i = (List) ps3.d();
                ri4 unused = this.a.this$Anon0.l();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<ps3<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$start$Anon1 a;

        @DexIgnore
        public b(HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1) {
            this.a = heartRateDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<HeartRateSample>> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sampleTransformations -- status=");
            Status status = null;
            sb.append(ps3 != null ? ps3.f() : null);
            sb.append(", resource=");
            sb.append(ps3 != null ? (List) ps3.d() : null);
            sb.append(", status=");
            if (ps3 != null) {
                status = ps3.f();
            }
            sb.append(status);
            local.d("HeartRateDetailPresenter", sb.toString());
            if (ps3 != null && ps3.f() != Status.DATABASE_LOADING) {
                this.a.this$Anon0.j = (List) ps3.d();
                ri4 unused = this.a.this$Anon0.m();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$start$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1 = new HeartRateDetailPresenter$start$Anon1(this.this$Anon0, kc4);
        heartRateDetailPresenter$start$Anon1.p$ = (lh4) obj;
        return heartRateDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        if (r6 != null) goto L_0x0046;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Unit unit;
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a3 = this.this$Anon0.b();
            HeartRateDetailPresenter$start$Anon1$currentUser$Anon1 heartRateDetailPresenter$start$Anon1$currentUser$Anon1 = new HeartRateDetailPresenter$start$Anon1$currentUser$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a3, heartRateDetailPresenter$start$Anon1$currentUser$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        HeartRateDetailPresenter heartRateDetailPresenter = this.this$Anon0;
        if (mFUser != null) {
            unit = mFUser.getDistanceUnit();
        }
        unit = Unit.METRIC;
        heartRateDetailPresenter.m = unit;
        LiveData o = this.this$Anon0.n;
        of3 m = this.this$Anon0.q;
        if (m != null) {
            o.a((pf3) m, new a(this));
            this.this$Anon0.o.a((LifecycleOwner) this.this$Anon0.q, new b(this));
            return cb4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }
}
