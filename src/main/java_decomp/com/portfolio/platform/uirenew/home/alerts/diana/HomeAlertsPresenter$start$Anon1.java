package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sb4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.tv2;
import com.fossil.blesdk.obfuscated.uv2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsPresenter$start$Anon1<T> implements dc<String> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1", f = "HomeAlertsPresenter.kt", l = {83}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon3<T> implements dc<List<? extends DNDScheduledTimeModel>> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1")
            @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1", f = "HomeAlertsPresenter.kt", l = {167}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1  reason: collision with other inner class name */
            public static final class C0133Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public int label;
                @DexIgnore
                public lh4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon3 this$Anon0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1")
                @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1  reason: collision with other inner class name */
                public static final class C0134Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public lh4 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0133Anon1 this$Anon0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0134Anon1(C0133Anon1 anon1, kc4 kc4) {
                        super(2, kc4);
                        this.this$Anon0 = anon1;
                    }

                    @DexIgnore
                    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                        wd4.b(kc4, "completion");
                        C0134Anon1 anon1 = new C0134Anon1(this.this$Anon0, kc4);
                        anon1.p$ = (lh4) obj;
                        return anon1;
                    }

                    @DexIgnore
                    public final Object invoke(Object obj, Object obj2) {
                        return ((C0134Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
                    }

                    @DexIgnore
                    public final Object invokeSuspend(Object obj) {
                        oc4.a();
                        if (this.label == 0) {
                            za4.a(obj);
                            this.this$Anon0.this$Anon0.a.this$Anon0.a.v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$Anon0.$lDndScheduledTimeModel);
                            return cb4.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0133Anon1(Anon3 anon3, List list, kc4 kc4) {
                    super(2, kc4);
                    this.this$Anon0 = anon3;
                    this.$lDndScheduledTimeModel = list;
                }

                @DexIgnore
                public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                    wd4.b(kc4, "completion");
                    C0133Anon1 anon1 = new C0133Anon1(this.this$Anon0, this.$lDndScheduledTimeModel, kc4);
                    anon1.p$ = (lh4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0133Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = oc4.a();
                    int i = this.label;
                    if (i == 0) {
                        za4.a(obj);
                        lh4 lh4 = this.p$;
                        gh4 c = this.this$Anon0.a.this$Anon0.a.c();
                        C0134Anon1 anon1 = new C0134Anon1(this, (kc4) null);
                        this.L$Anon0 = lh4;
                        this.label = 1;
                        if (kg4.a(c, anon1, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        lh4 lh42 = (lh4) this.L$Anon0;
                        za4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return cb4.a;
                }
            }

            @DexIgnore
            public Anon3(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            public final void a(List<DNDScheduledTimeModel> list) {
                if (list == null || list.isEmpty()) {
                    ArrayList arrayList = new ArrayList();
                    DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                    DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                    arrayList.add(dNDScheduledTimeModel);
                    arrayList.add(dNDScheduledTimeModel2);
                    ri4 unused = mg4.b(this.a.this$Anon0.a.e(), (CoroutineContext) null, (CoroutineStart) null, new C0133Anon1(this, arrayList, (kc4) null), 3, (Object) null);
                    return;
                }
                for (DNDScheduledTimeModel dNDScheduledTimeModel3 : list) {
                    if (dNDScheduledTimeModel3.getScheduledTimeType() == 0) {
                        this.a.this$Anon0.a.l.b(this.a.this$Anon0.a.b(dNDScheduledTimeModel3.getMinutes()));
                    } else {
                        this.a.this$Anon0.a.l.a(this.a.this$Anon0.a.b(dNDScheduledTimeModel3.getMinutes()));
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CoroutineUseCase.e<wy2.a, CoroutineUseCase.a> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public a(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(wy2.a aVar) {
                String str;
                wd4.b(aVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onSuccess");
                ArrayList<AppWrapper> arrayList = new ArrayList<>();
                for (AppWrapper next : aVar.a()) {
                    DianaNotificationObj.AApplicationName.Companion companion = DianaNotificationObj.AApplicationName.Companion;
                    InstalledApp installedApp = next.getInstalledApp();
                    if (installedApp != null) {
                        String identifier = installedApp.getIdentifier();
                        wd4.a((Object) identifier, "app.installedApp!!.identifier");
                        if (companion.isPackageNameSupportedBySDK(identifier)) {
                            arrayList.add(next);
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                if (this.a.this$Anon0.a.u.A()) {
                    ArrayList arrayList2 = new ArrayList();
                    for (AppWrapper appWrapper : arrayList) {
                        InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected == null) {
                            wd4.a();
                            throw null;
                        } else if (!isSelected.booleanValue()) {
                            arrayList2.add(appWrapper);
                        }
                    }
                    this.a.this$Anon0.a.i().clear();
                    this.a.this$Anon0.a.i().addAll(arrayList);
                    if (!arrayList2.isEmpty()) {
                        this.a.this$Anon0.a.a((List<AppWrapper>) arrayList2);
                        return;
                    }
                    String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Label__All);
                    tv2 m = this.a.this$Anon0.a.l;
                    wd4.a((Object) a2, "notificationAppOverView");
                    m.h(a2);
                    if (cn2.a(cn2.d, ((uv2) this.a.this$Anon0.a.l).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null)) {
                        HomeAlertsPresenter homeAlertsPresenter = this.a.this$Anon0.a;
                        if (homeAlertsPresenter.a(homeAlertsPresenter.i(), (List<AppWrapper>) arrayList)) {
                            this.a.this$Anon0.a.k();
                            return;
                        }
                        return;
                    }
                    return;
                }
                this.a.this$Anon0.a.i().clear();
                int i = 0;
                for (AppWrapper appWrapper2 : arrayList) {
                    InstalledApp installedApp3 = appWrapper2.getInstalledApp();
                    Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                    if (isSelected2 == null) {
                        wd4.a();
                        throw null;
                    } else if (isSelected2.booleanValue()) {
                        this.a.this$Anon0.a.i().add(appWrapper2);
                        i++;
                    }
                }
                if (i == aVar.a().size()) {
                    str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Label__All);
                } else if (i == 0) {
                    str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Sections_without_Device_Profile_Without_Watch_None);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(i);
                    sb.append('/');
                    sb.append(arrayList.size());
                    str = sb.toString();
                }
                tv2 m2 = this.a.this$Anon0.a.l;
                wd4.a((Object) str, "notificationAppOverView");
                m2.h(str);
                if (i > 0) {
                    cn2.a(cn2.d, ((uv2) this.a.this$Anon0.a.l).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null);
                }
            }

            @DexIgnore
            public void a(CoroutineUseCase.a aVar) {
                wd4.b(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onError");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ic4.a(Integer.valueOf(((Alarm) t).getTotalMinutes()), Integer.valueOf(((Alarm) t2).getTotalMinutes()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeAlertsPresenter$start$Anon1 homeAlertsPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeAlertsPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 c = this.this$Anon0.a.c();
                HomeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1 homeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1 = new HomeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj2 = kg4.a(c, homeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1, this);
                if (obj2 == a2) {
                    return a2;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<Alarm> list = (List) obj2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomeAlertsPresenter.x.a();
            local.d(a3, "GetAlarms onSuccess: size = " + list.size());
            this.this$Anon0.a.h.clear();
            for (Alarm copy$default : list) {
                this.this$Anon0.a.h.add(Alarm.copy$default(copy$default, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null));
            }
            ArrayList e = this.this$Anon0.a.h;
            if (e.size() > 1) {
                sb4.a(e, new b());
            }
            this.this$Anon0.a.l.d(this.this$Anon0.a.h);
            this.this$Anon0.a.o.a(null, new a(this));
            HomeAlertsPresenter homeAlertsPresenter = this.this$Anon0.a;
            homeAlertsPresenter.i = homeAlertsPresenter.u.E();
            this.this$Anon0.a.l.m(this.this$Anon0.a.i);
            this.this$Anon0.a.g.a((LifecycleOwner) this.this$Anon0.a.l, new Anon3(this));
            return cb4.a;
        }
    }

    @DexIgnore
    public HomeAlertsPresenter$start$Anon1(HomeAlertsPresenter homeAlertsPresenter) {
        this.a = homeAlertsPresenter;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.length() == 0) {
            this.a.l.a(true);
        } else {
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        }
    }
}
