package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
public final class MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String[]>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    public MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1(kc4 kc4) {
        super(2, kc4);
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1 = new MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1(kc4);
        microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1.p$ = (lh4) obj;
        return microAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$Anon1$Anon1$grantedPermissionList$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return os3.a.a();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
