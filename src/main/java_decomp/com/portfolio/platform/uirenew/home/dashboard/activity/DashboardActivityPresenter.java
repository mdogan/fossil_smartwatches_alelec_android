package com.portfolio.platform.uirenew.home.dashboard.activity;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.a93;
import com.fossil.blesdk.obfuscated.b93;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.z83;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardActivityPresenter extends z83 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> g;
    @DexIgnore
    public Unit h;
    @DexIgnore
    public /* final */ a93 i;
    @DexIgnore
    public /* final */ SummariesRepository j;
    @DexIgnore
    public /* final */ FitnessDataRepository k;
    @DexIgnore
    public /* final */ ActivitySummaryDao l;
    @DexIgnore
    public /* final */ FitnessDatabase m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ i42 o;
    @DexIgnore
    public /* final */ yk2 p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DashboardActivityPresenter(a93 a93, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42, yk2 yk2) {
        wd4.b(a93, "mView");
        wd4.b(summariesRepository, "mSummariesRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(activitySummaryDao, "mActivitySummaryDao");
        wd4.b(fitnessDatabase, "mFitnessDatabase");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(i42, "mAppExecutors");
        wd4.b(yk2, "mFitnessHelper");
        this.i = a93;
        this.j = summariesRepository;
        this.k = fitnessDataRepository;
        this.l = activitySummaryDao;
        this.m = fitnessDatabase;
        this.n = userRepository;
        this.o = i42;
        this.p = yk2;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardActivityPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardActivityPresenter$initDataSource$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                LiveData<rd<ActivitySummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    a93 a93 = this.i;
                    if (a93 != null) {
                        pagedList.a((LifecycleOwner) (b93) a93);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                    }
                }
            }
            this.j.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("DashboardActivityPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.g;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivityPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.i.f();
        }
    }
}
