package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fg3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(kc4 kc4, GoalTrackingDetailPresenter$showDetailChart$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(kc4, this.this$Anon0);
        goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (lh4) obj;
        return goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return fg3.a.a(this.this$Anon0.this$Anon0.g, this.this$Anon0.this$Anon0.n);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
