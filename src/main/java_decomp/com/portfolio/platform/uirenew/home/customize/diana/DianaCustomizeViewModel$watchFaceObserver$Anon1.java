package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.uj2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$watchFaceObserver$Anon1<T> implements dc<List<? extends WatchFace>> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {505}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $it;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel$watchFaceObserver$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0137Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Boolean>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $list;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0137Anon1(Anon1 anon1, List list, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
                this.$list = list;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0137Anon1 anon1 = new C0137Anon1(this.this$Anon0, this.$list, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0137Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    this.this$Anon0.this$Anon0.a.h.clear();
                    return pc4.a(this.this$Anon0.this$Anon0.a.h.addAll(this.$list));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaCustomizeViewModel$watchFaceObserver$Anon1 dianaCustomizeViewModel$watchFaceObserver$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = dianaCustomizeViewModel$watchFaceObserver$Anon1;
            this.$it = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$it, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                if (!wd4.a((Object) this.this$Anon0.a.q, (Object) this.$it)) {
                    this.this$Anon0.a.q = this.$it;
                    List list = this.$it;
                    wd4.a((Object) list, "it");
                    DianaPreset dianaPreset = (DianaPreset) this.this$Anon0.a.d.a();
                    List<WatchFaceWrapper> a2 = uj2.a((List<WatchFace>) list, (List<DianaPresetComplicationSetting>) dianaPreset != null ? dianaPreset.getComplications() : null);
                    bj4 c = zh4.c();
                    C0137Anon1 anon1 = new C0137Anon1(this, a2, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = a2;
                    this.label = 1;
                    if (kg4.a(c, anon1, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                List list2 = (List) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cb4.a;
        }
    }

    @DexIgnore
    public DianaCustomizeViewModel$watchFaceObserver$Anon1(DianaCustomizeViewModel dianaCustomizeViewModel) {
        this.a = dianaCustomizeViewModel;
    }

    @DexIgnore
    public final void a(List<WatchFace> list) {
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (kc4) null), 3, (Object) null);
    }
}
