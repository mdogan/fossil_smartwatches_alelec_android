package com.portfolio.platform.uirenew.home.customize.diana;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kc4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1(kc4 kc4, DianaCustomizeViewModel dianaCustomizeViewModel, DianaPreset dianaPreset, kc4 kc42) {
        super(2, kc4);
        this.this$Anon0 = dianaCustomizeViewModel;
        this.$currentPreset$inlined = dianaPreset;
        this.$continuation$inlined = kc42;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1 = new DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1(kc4, this.this$Anon0, this.$currentPreset$inlined, this.$continuation$inlined);
        dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            LiveData m = this.this$Anon0.p;
            if (m == null) {
                return null;
            }
            m.a(this.this$Anon0.y);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
