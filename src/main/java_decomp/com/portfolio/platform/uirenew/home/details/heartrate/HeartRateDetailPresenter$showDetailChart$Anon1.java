package com.portfolio.platform.uirenew.home.details.heartrate;

import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.nl2;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sb4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.st3;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$Anon1", f = "HeartRateDetailPresenter.kt", l = {205, 209, 218, 225}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$Anon1$Anon1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$showDetailChart$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ic4.a(Long.valueOf(((HeartRateSample) t).getStartTimeId().getMillis()), Long.valueOf(((HeartRateSample) t2).getStartTimeId().getMillis()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateDetailPresenter$showDetailChart$Anon1 heartRateDetailPresenter$showDetailChart$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = heartRateDetailPresenter$showDetailChart$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                List f = this.this$Anon0.this$Anon0.l;
                if (f == null) {
                    return null;
                }
                if (f.size() > 1) {
                    sb4.a(f, new a());
                }
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$Anon1$Anon2", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $listTimeZoneChange;
        @DexIgnore
        public /* final */ /* synthetic */ List $listTodayHeartRateModel;
        @DexIgnore
        public /* final */ /* synthetic */ int $maxHR;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef $previousTimeZoneOffset;
        @DexIgnore
        public /* final */ /* synthetic */ long $startOfDay;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$showDetailChart$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(HeartRateDetailPresenter$showDetailChart$Anon1 heartRateDetailPresenter$showDetailChart$Anon1, long j, Ref$IntRef ref$IntRef, List list, List list2, int i, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = heartRateDetailPresenter$showDetailChart$Anon1;
            this.$startOfDay = j;
            this.$previousTimeZoneOffset = ref$IntRef;
            this.$listTimeZoneChange = list;
            this.$listTodayHeartRateModel = list2;
            this.$maxHR = i;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Iterator it;
            char c;
            StringBuilder sb;
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                List f = this.this$Anon0.this$Anon0.l;
                if (f == null) {
                    return null;
                }
                for (Iterator it2 = f.iterator(); it2.hasNext(); it2 = it) {
                    HeartRateSample heartRateSample = (HeartRateSample) it2.next();
                    long j = (long) 60000;
                    long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                    long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                    long j2 = (millis2 + millis) / ((long) 2);
                    if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                        int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                        String a = nl2.a(hourOfDay);
                        be4 be4 = be4.a;
                        Object[] objArr = {pc4.a(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), pc4.a(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(format, *args)");
                        List list = this.$listTimeZoneChange;
                        Integer a2 = pc4.a((int) j2);
                        it = it2;
                        Pair pair = new Pair(pc4.a(hourOfDay), pc4.a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(a);
                        if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                            sb = new StringBuilder();
                            c = '+';
                        } else {
                            sb = new StringBuilder();
                            c = '-';
                        }
                        sb.append(c);
                        sb.append(format);
                        sb2.append(sb.toString());
                        list.add(new Triple(a2, pair, sb2.toString()));
                        this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                    } else {
                        it = it2;
                    }
                    if (!this.$listTodayHeartRateModel.isEmpty()) {
                        st3 st3 = (st3) wb4.f(this.$listTodayHeartRateModel);
                        if (millis - ((long) st3.a()) > 1) {
                            this.$listTodayHeartRateModel.add(new st3(0, 0, 0, st3.a(), (int) millis, (int) j2));
                        }
                    }
                    int average = (int) heartRateSample.getAverage();
                    if (heartRateSample.getMax() == this.$maxHR) {
                        average = heartRateSample.getMax();
                    }
                    this.$listTodayHeartRateModel.add(new st3(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2));
                }
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDetailChart$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateDetailPresenter$showDetailChart$Anon1 heartRateDetailPresenter$showDetailChart$Anon1 = new HeartRateDetailPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        heartRateDetailPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return heartRateDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x013d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x019c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01c3  */
    public final Object invokeSuspend(Object obj) {
        ArrayList arrayList;
        List list;
        int i;
        int i2;
        long j;
        lh4 lh4;
        List list2;
        Object obj2;
        Anon2 anon2;
        gh4 a;
        lh4 lh42;
        List list3;
        long j2;
        Object obj3;
        lh4 lh43;
        Object a2 = oc4.a();
        int i3 = this.label;
        if (i3 == 0) {
            za4.a(obj);
            lh43 = this.p$;
            FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "showDetailChart");
            gh4 a3 = this.this$Anon0.b();
            HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1 heartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1 = new HeartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1(this, (kc4) null);
            this.L$Anon0 = lh43;
            this.label = 1;
            obj3 = kg4.a(a3, heartRateDetailPresenter$showDetailChart$Anon1$summary$Anon1, this);
            if (obj3 == a2) {
                return a2;
            }
        } else if (i3 == 1) {
            lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj3 = obj;
        } else if (i3 == 2) {
            list3 = (List) this.L$Anon1;
            za4.a(obj);
            lh42 = (lh4) this.L$Anon0;
            ArrayList arrayList2 = new ArrayList();
            if (this.this$Anon0.l != null) {
                List f = this.this$Anon0.l;
                if (f == null) {
                    wd4.a();
                    throw null;
                } else if (!f.isEmpty()) {
                    List f2 = this.this$Anon0.l;
                    if (f2 != null) {
                        DateTime withTimeAtStartOfDay = ((HeartRateSample) f2.get(0)).getStartTimeId().withTimeAtStartOfDay();
                        wd4.a((Object) withTimeAtStartOfDay, "mHeartRateSamplesOfDate!\u2026().withTimeAtStartOfDay()");
                        j2 = withTimeAtStartOfDay.getMillis();
                        gh4 a4 = this.this$Anon0.b();
                        HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1 heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1 = new HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1(this, (kc4) null);
                        this.L$Anon0 = lh42;
                        this.L$Anon1 = list3;
                        this.L$Anon2 = arrayList2;
                        this.J$Anon0 = j2;
                        this.label = 3;
                        obj2 = kg4.a(a4, heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1, this);
                        if (obj2 == a2) {
                            return a2;
                        }
                        arrayList = arrayList2;
                        j = j2;
                        list2 = list3;
                        lh4 = lh42;
                        int intValue = ((Number) obj2).intValue();
                        Ref$IntRef ref$IntRef = new Ref$IntRef();
                        ref$IntRef.element = -1;
                        ArrayList arrayList3 = new ArrayList();
                        anon2 = r0;
                        a = this.this$Anon0.b();
                        Ref$IntRef ref$IntRef2 = ref$IntRef;
                        ArrayList arrayList4 = arrayList3;
                        int i4 = intValue;
                        Anon2 anon22 = new Anon2(this, j, ref$IntRef2, arrayList4, arrayList, i4, (kc4) null);
                        this.L$Anon0 = lh4;
                        this.L$Anon1 = list2;
                        this.L$Anon2 = arrayList;
                        this.J$Anon0 = j;
                        this.I$Anon0 = i4;
                        this.L$Anon3 = ref$IntRef2;
                        list = arrayList4;
                        this.L$Anon4 = list;
                        this.label = 4;
                        if (kg4.a(a, anon2, this) == a2) {
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("HeartRateDetailPresenter", "listTimeZoneChange=" + sj2.a(list));
                        if (!list.isEmpty()) {
                        }
                        list.clear();
                        i = 0;
                        int i5 = i + DateTimeConstants.MINUTES_PER_DAY;
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("HeartRateDetailPresenter", "minutesOfDay=" + i5);
                        this.this$Anon0.q.a(i5, (List<st3>) arrayList, (List<Triple<Integer, Pair<Integer, Float>, String>>) list);
                        return cb4.a;
                    }
                    wd4.a();
                    throw null;
                }
            }
            Date n = sk2.n(this.this$Anon0.g);
            wd4.a((Object) n, "DateHelper.getStartOfDay(mDate)");
            j2 = n.getTime();
            gh4 a42 = this.this$Anon0.b();
            HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1 heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon12 = new HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = list3;
            this.L$Anon2 = arrayList2;
            this.J$Anon0 = j2;
            this.label = 3;
            obj2 = kg4.a(a42, heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon12, this);
            if (obj2 == a2) {
            }
        } else if (i3 == 3) {
            long j3 = this.J$Anon0;
            za4.a(obj);
            j = j3;
            arrayList = (List) this.L$Anon2;
            list2 = (List) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            obj2 = obj;
            int intValue2 = ((Number) obj2).intValue();
            Ref$IntRef ref$IntRef3 = new Ref$IntRef();
            ref$IntRef3.element = -1;
            ArrayList arrayList32 = new ArrayList();
            anon2 = anon22;
            a = this.this$Anon0.b();
            Ref$IntRef ref$IntRef22 = ref$IntRef3;
            ArrayList arrayList42 = arrayList32;
            int i42 = intValue2;
            Anon2 anon222 = new Anon2(this, j, ref$IntRef22, arrayList42, arrayList, i42, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = list2;
            this.L$Anon2 = arrayList;
            this.J$Anon0 = j;
            this.I$Anon0 = i42;
            this.L$Anon3 = ref$IntRef22;
            list = arrayList42;
            this.L$Anon4 = list;
            this.label = 4;
            if (kg4.a(a, anon2, this) == a2) {
                return a2;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateDetailPresenter", "listTimeZoneChange=" + sj2.a(list));
            if (!list.isEmpty()) {
            }
            list.clear();
            i = 0;
            int i52 = i + DateTimeConstants.MINUTES_PER_DAY;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateDetailPresenter", "minutesOfDay=" + i52);
            this.this$Anon0.q.a(i52, (List<st3>) arrayList, (List<Triple<Integer, Pair<Integer, Float>, String>>) list);
            return cb4.a;
        } else if (i3 == 4) {
            list = (List) this.L$Anon4;
            Ref$IntRef ref$IntRef4 = (Ref$IntRef) this.L$Anon3;
            List list4 = (List) this.L$Anon1;
            lh4 lh44 = (lh4) this.L$Anon0;
            za4.a(obj);
            arrayList = (List) this.L$Anon2;
            ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
            local32.d("HeartRateDetailPresenter", "listTimeZoneChange=" + sj2.a(list));
            if ((!list.isEmpty()) || list.size() <= 1) {
                list.clear();
                i = 0;
            } else {
                float floatValue = ((Number) ((Pair) ((Triple) list.get(0)).getSecond()).getSecond()).floatValue();
                float floatValue2 = ((Number) ((Pair) ((Triple) list.get(ob4.a(list))).getSecond()).getSecond()).floatValue();
                float f3 = (float) 0;
                if ((floatValue >= f3 || floatValue2 >= f3) && (floatValue < f3 || floatValue2 < f3)) {
                    i2 = Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                    if (floatValue2 < f3) {
                        i2 = -i2;
                    }
                } else {
                    i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                }
                i = i2;
                ArrayList<Triple> arrayList5 = new ArrayList<>();
                for (Object next : list) {
                    if (pc4.a(((Number) ((Pair) ((Triple) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                        arrayList5.add(next);
                    }
                }
                ArrayList<Pair> arrayList6 = new ArrayList<>(pb4.a(arrayList5, 10));
                for (Triple second : arrayList5) {
                    arrayList6.add((Pair) second.getSecond());
                }
                ArrayList arrayList7 = new ArrayList(pb4.a(arrayList6, 10));
                for (Pair first : arrayList6) {
                    arrayList7.add(pc4.a(((Number) first.getFirst()).intValue()));
                }
                if (!arrayList7.contains(pc4.a(0))) {
                    String a5 = this.this$Anon0.a(pc4.a(floatValue));
                    Integer a6 = pc4.a(0);
                    Pair pair = new Pair(pc4.a(0), pc4.a(floatValue));
                    list.add(0, new Triple(a6, pair, "12a" + a5));
                }
                ArrayList<Triple> arrayList8 = new ArrayList<>();
                for (Object next2 : list) {
                    if (pc4.a(((Number) ((Pair) ((Triple) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                        arrayList8.add(next2);
                    }
                }
                ArrayList<Pair> arrayList9 = new ArrayList<>(pb4.a(arrayList8, 10));
                for (Triple second2 : arrayList8) {
                    arrayList9.add((Pair) second2.getSecond());
                }
                ArrayList arrayList10 = new ArrayList(pb4.a(arrayList9, 10));
                for (Pair first2 : arrayList9) {
                    arrayList10.add(pc4.a(((Number) first2.getFirst()).intValue()));
                }
                if (!arrayList10.contains(pc4.a(24))) {
                    String a7 = this.this$Anon0.a(pc4.a(floatValue2));
                    Integer a8 = pc4.a(i + DateTimeConstants.MINUTES_PER_DAY);
                    Pair pair2 = new Pair(pc4.a(24), pc4.a(floatValue2));
                    list.add(new Triple(a8, pair2, "12a" + a7));
                }
            }
            int i522 = i + DateTimeConstants.MINUTES_PER_DAY;
            ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
            local222.d("HeartRateDetailPresenter", "minutesOfDay=" + i522);
            this.this$Anon0.q.a(i522, (List<st3>) arrayList, (List<Triple<Integer, Pair<Integer, Float>, String>>) list);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list5 = (List) obj3;
        if (this.this$Anon0.l == null || (!wd4.a((Object) this.this$Anon0.l, (Object) list5))) {
            this.this$Anon0.l = list5;
            gh4 a9 = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh43;
            this.L$Anon1 = list5;
            this.label = 2;
            if (kg4.a(a9, anon1, this) == a2) {
                return a2;
            }
            List list6 = list5;
            lh42 = lh43;
            list3 = list6;
            ArrayList arrayList22 = new ArrayList();
            if (this.this$Anon0.l != null) {
            }
            Date n2 = sk2.n(this.this$Anon0.g);
            wd4.a((Object) n2, "DateHelper.getStartOfDay(mDate)");
            j2 = n2.getTime();
            gh4 a422 = this.this$Anon0.b();
            HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1 heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon122 = new HeartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = list3;
            this.L$Anon2 = arrayList22;
            this.J$Anon0 = j2;
            this.label = 3;
            obj2 = kg4.a(a422, heartRateDetailPresenter$showDetailChart$Anon1$maxHR$Anon122, this);
            if (obj2 == a2) {
            }
        }
        return cb4.a;
    }
}
