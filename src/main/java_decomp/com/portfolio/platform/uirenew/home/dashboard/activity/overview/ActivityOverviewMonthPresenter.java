package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.p93;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.q93;
import com.fossil.blesdk.obfuscated.r93;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityOverviewMonthPresenter extends p93 {
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public LiveData<ps3<List<ActivitySummary>>> k; // = new MutableLiveData();
    @DexIgnore
    public List<ActivitySummary> l; // = new ArrayList();
    @DexIgnore
    public LiveData<ps3<List<ActivitySummary>>> m;
    @DexIgnore
    public TreeMap<Long, Float> n;
    @DexIgnore
    public /* final */ q93 o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ SummariesRepository q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthPresenter a;

        @DexIgnore
        public b(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            this.a = activityOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<ActivitySummary>>> apply(Date date) {
            ActivityOverviewMonthPresenter activityOverviewMonthPresenter = this.a;
            wd4.a((Object) date, "it");
            if (activityOverviewMonthPresenter.b(date)) {
                ActivityOverviewMonthPresenter activityOverviewMonthPresenter2 = this.a;
                activityOverviewMonthPresenter2.k = activityOverviewMonthPresenter2.q.getSummaries(ActivityOverviewMonthPresenter.j(this.a), ActivityOverviewMonthPresenter.i(this.a), true);
            }
            return this.a.k;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public ActivityOverviewMonthPresenter(q93 q93, UserRepository userRepository, SummariesRepository summariesRepository) {
        wd4.b(q93, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(summariesRepository, "mSummariesRepository");
        this.o = q93;
        this.p = userRepository;
        this.q = summariesRepository;
        LiveData<ps3<List<ActivitySummary>>> b2 = ic.b(this.f, new b(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026 mActivitySummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date g(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
        Date date = activityOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date i(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
        Date date = activityOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        wd4.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date j(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
        Date date = activityOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<List<ActivitySummary>>> liveData = this.m;
        q93 q93 = this.o;
        if (q93 != null) {
            liveData.a((r93) q93, new ActivityOverviewMonthPresenter$start$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "stop");
        try {
            LiveData<ps3<List<ActivitySummary>>> liveData = this.m;
            q93 q93 = this.o;
            if (q93 != null) {
                liveData.a((LifecycleOwner) (r93) q93);
                this.k.a((LifecycleOwner) this.o);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthPresenter", "stop - e=" + e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001e, code lost:
        if (com.fossil.blesdk.obfuscated.sk2.s(r0).booleanValue() == false) goto L_0x0025;
     */
    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "loadData");
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                wd4.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityOverviewMonthPresenter$loadData$Anon2(this, (kc4) null), 3, (Object) null);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("loadData - mDate=");
        Date date2 = this.g;
        if (date2 != null) {
            sb.append(date2);
            local.d("ActivityOverviewMonthPresenter", sb.toString());
            return;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.o.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!sk2.a(date4.getTime(), date.getTime())) {
                Calendar o2 = sk2.o(date);
                wd4.a((Object) o2, "DateHelper.getStartOfMonth(date)");
                Date time = o2.getTime();
                wd4.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean r = sk2.r(date);
            wd4.a((Object) r, "DateHelper.isThisMonth(date)");
            if (r.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar j2 = sk2.j(date);
                wd4.a((Object) j2, "DateHelper.getEndOfMonth(date)");
                date2 = j2.getTime();
                wd4.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wd4.d("mStartDate");
                throw null;
            }
            wd4.d("mEndDate");
            throw null;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        if (this.f.a() == null || !sk2.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary next : list) {
                instance.set(next.getYear(), next.getMonth() - 1, next.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (next.getStepGoal() > 0) {
                    wd4.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) next.getSteps()) / ((float) next.getStepGoal())));
                } else {
                    wd4.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
