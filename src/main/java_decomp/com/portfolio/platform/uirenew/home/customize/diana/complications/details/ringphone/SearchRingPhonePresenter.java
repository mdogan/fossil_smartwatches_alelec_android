package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.blesdk.obfuscated.gn2;
import com.fossil.blesdk.obfuscated.k33;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.l33;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchRingPhonePresenter extends k33 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> f; // = new ArrayList<>();
    @DexIgnore
    public Ringtone g;
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ l33 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public SearchRingPhonePresenter(l33 l33) {
        wd4.b(l33, "mView");
        this.i = l33;
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchRingPhonePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public Ringtone h() {
        Ringtone ringtone = this.g;
        if (ringtone != null) {
            return ringtone;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void i() {
        gn2.o.a().b();
    }

    @DexIgnore
    public final l33 j() {
        return this.i;
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    public void b(Ringtone ringtone) {
        wd4.b(ringtone, Constants.RINGTONE);
        this.g = ringtone;
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        wd4.b(ringtone, Constants.RINGTONE);
        gn2.o.a().a(ringtone);
        this.g = ringtone;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        try {
            this.g = (Ringtone) this.h.a(str, Ringtone.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e);
        }
    }
}
