package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
public final class DoNotDisturbScheduledTimePresenter$save$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$Anon1$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter$save$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ DNDScheduledTimeModel e;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 f;

            @DexIgnore
            public a(DNDScheduledTimeModel dNDScheduledTimeModel, Anon1 anon1) {
                this.e = dNDScheduledTimeModel;
                this.f = anon1;
            }

            @DexIgnore
            public final void run() {
                this.f.this$Anon0.this$Anon0.k.getDNDScheduledTimeDao().insertDNDScheduledTime(this.e);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ DNDScheduledTimeModel e;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 f;

            @DexIgnore
            public b(DNDScheduledTimeModel dNDScheduledTimeModel, Anon1 anon1) {
                this.e = dNDScheduledTimeModel;
                this.f = anon1;
            }

            @DexIgnore
            public final void run() {
                this.f.this$Anon0.this$Anon0.k.getDNDScheduledTimeDao().insertDNDScheduledTime(this.e);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DoNotDisturbScheduledTimePresenter$save$Anon1 doNotDisturbScheduledTimePresenter$save$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = doNotDisturbScheduledTimePresenter$save$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                if (this.this$Anon0.this$Anon0.g == 0) {
                    DNDScheduledTimeModel c = this.this$Anon0.this$Anon0.h;
                    if (c == null) {
                        return null;
                    }
                    c.setMinutes(this.this$Anon0.this$Anon0.f);
                    this.this$Anon0.this$Anon0.k.runInTransaction((Runnable) new a(c, this));
                    return cb4.a;
                }
                DNDScheduledTimeModel b2 = this.this$Anon0.this$Anon0.i;
                if (b2 == null) {
                    return null;
                }
                b2.setMinutes(this.this$Anon0.this$Anon0.f);
                this.this$Anon0.this$Anon0.k.runInTransaction((Runnable) new b(b2, this));
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DoNotDisturbScheduledTimePresenter$save$Anon1(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = doNotDisturbScheduledTimePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DoNotDisturbScheduledTimePresenter$save$Anon1 doNotDisturbScheduledTimePresenter$save$Anon1 = new DoNotDisturbScheduledTimePresenter$save$Anon1(this.this$Anon0, kc4);
        doNotDisturbScheduledTimePresenter$save$Anon1.p$ = (lh4) obj;
        return doNotDisturbScheduledTimePresenter$save$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DoNotDisturbScheduledTimePresenter$save$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.j.close();
        return cb4.a;
    }
}
