package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.content.Intent;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.a63;
import com.fossil.blesdk.obfuscated.ab4;
import com.fossil.blesdk.obfuscated.av2;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.h13;
import com.fossil.blesdk.obfuscated.hl2;
import com.fossil.blesdk.obfuscated.i13;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.z53;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeHybridCustomizePresenter extends z53 {
    @DexIgnore
    public LiveData<List<HybridPreset>> f; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<HybridPreset> h; // = new ArrayList<>();
    @DexIgnore
    public HybridPreset i;
    @DexIgnore
    public MutableLiveData<String> j; // = this.n.f();
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public int l; // = 1;
    @DexIgnore
    public Pair<Boolean, ? extends List<HybridPreset>> m; // = ab4.a(false, null);
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ a63 o;
    @DexIgnore
    public /* final */ MicroAppRepository p;
    @DexIgnore
    public /* final */ HybridPresetRepository q;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase r;
    @DexIgnore
    public /* final */ fn2 s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SetHybridPresetToWatchUseCase.d, SetHybridPresetToWatchUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter a;

        @DexIgnore
        public b(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            this.a = homeHybridCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.o.m();
            this.a.o.e(0);
        }

        @DexIgnore
        public void a(SetHybridPresetToWatchUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.a.o.m();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
                wd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                a63 l = this.a.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
                if (array != null) {
                    PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                    l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.o.j();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HomeHybridCustomizePresenter(PortfolioApp portfolioApp, a63 a63, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, fn2 fn2) {
        wd4.b(portfolioApp, "mApp");
        wd4.b(a63, "mView");
        wd4.b(microAppRepository, "mMicroAppRepository");
        wd4.b(hybridPresetRepository, "mHybridPresetRepository");
        wd4.b(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.n = portfolioApp;
        this.o = a63;
        this.p = microAppRepository;
        this.q = hybridPresetRepository;
        this.r = setHybridPresetToWatchUseCase;
        this.s = fn2;
    }

    @DexIgnore
    public final boolean b(HybridPreset hybridPreset) {
        if (hybridPreset == null) {
            return true;
        }
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        for (T next : buttons) {
            if (hl2.c.c(((HybridPresetAppSetting) next).getAppId())) {
                arrayList.add(next);
            }
        }
        HybridPresetAppSetting hybridPresetAppSetting = null;
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) it.next();
                if (!hl2.c.e(hybridPresetAppSetting2.getAppId())) {
                    hybridPresetAppSetting = hybridPresetAppSetting2;
                    break;
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting);
        if (hybridPresetAppSetting == null) {
            return true;
        }
        this.o.q(hybridPresetAppSetting.getAppId());
        return false;
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeHybridCustomizePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            LiveData<List<HybridPreset>> liveData = this.f;
            a63 a63 = this.o;
            if (a63 != null) {
                liveData.a((LifecycleOwner) (av2) a63);
                this.j.a((LifecycleOwner) this.o);
                this.r.g();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "Exception when remove observer.");
        }
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeHybridCustomizePresenter$createNewPreset$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.i);
        Set<Integer> a2 = cn2.d.a(this.i);
        cn2 cn2 = cn2.d;
        a63 a63 = this.o;
        if (a63 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (cn2.a(((av2) a63).getContext(), a2)) {
            HybridPreset hybridPreset = this.i;
            if (hybridPreset != null) {
                this.s.q(true);
                if (b(hybridPreset)) {
                    this.o.l();
                    this.r.a(new SetHybridPresetToWatchUseCase.c(hybridPreset), new b(this));
                }
            }
        }
    }

    @DexIgnore
    public final int j() {
        return this.k;
    }

    @DexIgnore
    public void k() {
        this.o.a(this);
    }

    @DexIgnore
    public final void l() {
        ArrayList<HybridPreset> arrayList = this.h;
        ArrayList arrayList2 = new ArrayList(pb4.a(arrayList, 10));
        for (HybridPreset a2 : arrayList) {
            arrayList2.add(a(a2));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.h.size() + " uiData " + arrayList2);
        this.o.c((List<h13>) arrayList2);
    }

    @DexIgnore
    public final h13 a(HybridPreset hybridPreset) {
        String str;
        T t;
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        Iterator<HybridPresetAppSetting> it = buttons.iterator();
        while (true) {
            str = "";
            if (!it.hasNext()) {
                break;
            }
            HybridPresetAppSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.g.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                t = it2.next();
                if (wd4.a((Object) ((MicroApp) t).getId(), (Object) component2)) {
                    break;
                }
            }
            MicroApp microApp = (MicroApp) t;
            if (microApp != null) {
                String id = microApp.getId();
                String icon = microApp.getIcon();
                if (icon != null) {
                    str = icon;
                }
                arrayList.add(new i13(id, str, tm2.a(PortfolioApp.W.c(), microApp.getNameKey(), microApp.getName()), component1, (String) null, 16, (rd4) null));
            }
        }
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "convertPresetToHybridPresetConfigWrapper");
        String id2 = hybridPreset.getId();
        String name = hybridPreset.getName();
        if (name != null) {
            str = name;
        }
        return new h13(id2, str, arrayList, hybridPreset.isActive());
    }

    @DexIgnore
    public final void b(String str) {
        T t;
        T t2;
        cn2 cn2 = cn2.d;
        a63 a63 = this.o;
        if (a63 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (cn2.a(cn2, ((av2) a63).getContext(), "SET_MICRO_APP", false, 4, (Object) null)) {
            List a2 = this.f.a();
            if (a2 != null) {
                Boolean.valueOf(!a2.isEmpty());
            }
            Iterator<T> it = this.h.iterator();
            while (true) {
                t = null;
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (wd4.a((Object) ((HybridPreset) t2).getId(), (Object) str)) {
                    break;
                }
            }
            HybridPreset hybridPreset = (HybridPreset) t2;
            if (hybridPreset != null) {
                Iterator<T> it2 = this.h.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (((HybridPreset) next).isActive()) {
                        t = next;
                        break;
                    }
                }
                HybridPreset hybridPreset2 = (HybridPreset) t;
                this.o.l();
                FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + hybridPreset2 + " set preset " + hybridPreset + " as active first");
                this.r.a(new SetHybridPresetToWatchUseCase.c(hybridPreset), new HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(hybridPreset2, hybridPreset, this, str));
            }
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.h.size() > i2) {
            this.k = i2;
            this.i = this.h.get(this.k);
            HybridPreset hybridPreset = this.i;
            if (hybridPreset != null) {
                this.o.d(hybridPreset.isActive());
                return;
            }
            return;
        }
        this.o.d(false);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.l = i2;
        if (this.l == 1 && this.m.getFirst().booleanValue()) {
            List list = (List) this.m.getSecond();
            if (list != null) {
                a((List<HybridPreset>) list);
            }
            this.m = ab4.a(false, null);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        wd4.b(str, "name");
        wd4.b(str2, "presetId");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeHybridCustomizePresenter$renameCurrentPreset$Anon1(this, str2, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "nextActivePresetId");
        HybridPreset hybridPreset = this.i;
        if (hybridPreset == null) {
            return;
        }
        if (hybridPreset.isActive()) {
            b(str);
        } else {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$Anon1(hybridPreset, (kc4) null, this, str), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.o.d(0);
        }
    }

    @DexIgnore
    public final void a(List<HybridPreset> list) {
        T t;
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "doShowingPreset");
        this.h.clear();
        this.h.addAll(list);
        l();
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((HybridPreset) t).isActive()) {
                break;
            }
        }
        b((HybridPreset) t);
    }
}
