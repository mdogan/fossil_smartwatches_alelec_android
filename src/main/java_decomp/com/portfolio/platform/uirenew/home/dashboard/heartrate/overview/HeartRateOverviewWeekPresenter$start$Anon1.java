package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.vc3;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wc3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1", f = "HeartRateOverviewWeekPresenter.kt", l = {48}, m = "invokeSuspend")
public final class HeartRateOverviewWeekPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements dc<ps3<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1", f = "HeartRateOverviewWeekPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, List list, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon2;
                this.$data = list;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    gh4 a2 = this.this$Anon0.a.this$Anon0.b();
                    HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 = new HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = kg4.a(a2, heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (list != null) {
                    this.this$Anon0.a.this$Anon0.i.a(wb4.d(list), this.this$Anon0.a.this$Anon0.h);
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon2(HeartRateOverviewWeekPresenter$start$Anon1 heartRateOverviewWeekPresenter$start$Anon1) {
            this.a = heartRateOverviewWeekPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<DailyHeartRateSummary>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSummaries -- heartRateSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("HeartRateOverviewWeekPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewWeekPresenter$start$Anon1(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateOverviewWeekPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateOverviewWeekPresenter$start$Anon1 heartRateOverviewWeekPresenter$start$Anon1 = new HeartRateOverviewWeekPresenter$start$Anon1(this.this$Anon0, kc4);
        heartRateOverviewWeekPresenter$start$Anon1.p$ = (lh4) obj;
        return heartRateOverviewWeekPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewWeekPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b2  */
    public final Object invokeSuspend(Object obj) {
        LiveData d;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.f == null || !sk2.s(HeartRateOverviewWeekPresenter.c(this.this$Anon0)).booleanValue()) {
                this.this$Anon0.f = new Date();
                gh4 a2 = this.this$Anon0.b();
                HeartRateOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 heartRateOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 = new HeartRateOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, heartRateOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            this.this$Anon0.h();
            d = this.this$Anon0.g;
            if (d != null) {
                vc3 g = this.this$Anon0.i;
                if (g != null) {
                    d.a((wc3) g, new Anon2(this));
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
                }
            }
            return cb4.a;
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewWeekPresenter", "start - startDate=" + ((Date) pair.getFirst()) + ", endDate=" + ((Date) pair.getSecond()));
        HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter = this.this$Anon0;
        heartRateOverviewWeekPresenter.g = heartRateOverviewWeekPresenter.k.getHeartRateSummaries((Date) pair.getFirst(), (Date) pair.getSecond(), false);
        this.this$Anon0.h();
        d = this.this$Anon0.g;
        if (d != null) {
        }
        return cb4.a;
    }
}
