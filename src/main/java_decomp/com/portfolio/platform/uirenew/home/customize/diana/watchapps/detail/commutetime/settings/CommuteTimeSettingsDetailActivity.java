package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x43;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle, int i) {
            wd4.b(fragment, "fragment");
            wd4.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDetailActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_ADDRESS", bundle);
            fragment.startActivityForResult(intent, i);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        if (((x43) getSupportFragmentManager().a((int) R.id.content)) == null) {
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_ADDRESS");
            a((Fragment) x43.p.a((AddressWrapper) bundleExtra.getParcelable("KEY_SELECTED_ADDRESS"), bundleExtra.getStringArrayList("KEY_LIST_ADDRESS")), x43.p.a(), (int) R.id.content);
        }
    }
}
