package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.ha3;
import com.fossil.blesdk.obfuscated.la3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.ra3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.v92;
import com.fossil.blesdk.obfuscated.wa3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CaloriesOverviewFragment extends as2 {
    @DexIgnore
    public ur3<v92> j;
    @DexIgnore
    public CaloriesOverviewDayPresenter k;
    @DexIgnore
    public CaloriesOverviewWeekPresenter l;
    @DexIgnore
    public CaloriesOverviewMonthPresenter m;
    @DexIgnore
    public ha3 n;
    @DexIgnore
    public wa3 o;
    @DexIgnore
    public ra3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public b(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            ur3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(7, a != null ? (v92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public c(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            ur3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(4, a != null ? (v92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewFragment e;

        @DexIgnore
        public d(CaloriesOverviewFragment caloriesOverviewFragment) {
            this.e = caloriesOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CaloriesOverviewFragment caloriesOverviewFragment = this.e;
            ur3 a = caloriesOverviewFragment.j;
            caloriesOverviewFragment.a(2, a != null ? (v92) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "onCreateView");
        v92 v92 = (v92) ra.a(layoutInflater, R.layout.fragment_calories_overview, viewGroup, false, O0());
        g9.c((View) v92.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) v92, "binding");
        a(v92);
        this.j = new ur3<>(this, v92);
        ur3<v92> ur3 = this.j;
        if (ur3 != null) {
            v92 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(v92 v92) {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewFragment", "initUI");
        this.n = (ha3) getChildFragmentManager().a("CaloriesOverviewDayFragment");
        this.o = (wa3) getChildFragmentManager().a("CaloriesOverviewWeekFragment");
        this.p = (ra3) getChildFragmentManager().a("CaloriesOverviewMonthFragment");
        if (this.n == null) {
            this.n = new ha3();
        }
        if (this.o == null) {
            this.o = new wa3();
        }
        if (this.p == null) {
            this.p = new ra3();
        }
        ArrayList arrayList = new ArrayList();
        ha3 ha3 = this.n;
        if (ha3 != null) {
            arrayList.add(ha3);
            wa3 wa3 = this.o;
            if (wa3 != null) {
                arrayList.add(wa3);
                ra3 ra3 = this.p;
                if (ra3 != null) {
                    arrayList.add(ra3);
                    RecyclerView recyclerView = v92.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new CaloriesOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, v92);
                    m42 g = PortfolioApp.W.c().g();
                    ha3 ha32 = this.n;
                    if (ha32 != null) {
                        wa3 wa32 = this.o;
                        if (wa32 != null) {
                            ra3 ra32 = this.p;
                            if (ra32 != null) {
                                g.a(new la3(ha32, wa32, ra32)).a(this);
                                v92.s.setOnClickListener(new b(this));
                                v92.q.setOnClickListener(new c(this));
                                v92.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, v92 v92) {
        if (v92 != null) {
            FlexibleTextView flexibleTextView = v92.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = v92.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = v92.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = v92.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = v92.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = v92.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = v92.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = v92.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = v92.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<v92> ur3 = this.j;
                if (ur3 != null) {
                    v92 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = v92.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = v92.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = v92.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<v92> ur32 = this.j;
                if (ur32 != null) {
                    v92 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = v92.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = v92.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = v92.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<v92> ur33 = this.j;
                if (ur33 != null) {
                    v92 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = v92.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = v92.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = v92.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<v92> ur34 = this.j;
                if (ur34 != null) {
                    v92 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
