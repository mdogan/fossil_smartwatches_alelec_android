package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {204}, m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$addRecord$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$Anon1$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {205}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingData $goalTrackingData;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter$addRecord$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDetailPresenter$addRecord$Anon1 goalTrackingDetailPresenter$addRecord$Anon1, GoalTrackingData goalTrackingData, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = goalTrackingDetailPresenter$addRecord$Anon1;
            this.$goalTrackingData = goalTrackingData;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$goalTrackingData, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                GoalTrackingRepository d = this.this$Anon0.this$Anon0.r;
                List d2 = ob4.d(this.$goalTrackingData);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (d.insertFromDevice(d2, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cb4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$addRecord$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingDetailPresenter$addRecord$Anon1 goalTrackingDetailPresenter$addRecord$Anon1 = new GoalTrackingDetailPresenter$addRecord$Anon1(this.this$Anon0, this.$date, kc4);
        goalTrackingDetailPresenter$addRecord$Anon1.p$ = (lh4) obj;
        return goalTrackingDetailPresenter$addRecord$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$addRecord$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            Date date = this.$date;
            TimeZone timeZone = TimeZone.getDefault();
            wd4.a((Object) timeZone, "TimeZone.getDefault()");
            DateTime a2 = sk2.a(date, timeZone.getRawOffset() / 1000);
            wd4.a((Object) a2, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
            TimeZone timeZone2 = TimeZone.getDefault();
            wd4.a((Object) timeZone2, "TimeZone.getDefault()");
            GoalTrackingData goalTrackingData = new GoalTrackingData(uuid, a2, timeZone2.getRawOffset() / 1000, this.$date, new Date().getTime(), new Date().getTime());
            gh4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, goalTrackingData, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = goalTrackingData;
            this.label = 1;
            if (kg4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            GoalTrackingData goalTrackingData2 = (GoalTrackingData) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
