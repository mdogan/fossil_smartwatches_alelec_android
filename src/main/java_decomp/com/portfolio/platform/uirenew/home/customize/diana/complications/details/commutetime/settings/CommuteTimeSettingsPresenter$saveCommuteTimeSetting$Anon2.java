package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2", f = "CommuteTimeSettingsPresenter.kt", l = {161, 168}, m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $address;
    @DexIgnore
    public /* final */ /* synthetic */ String $displayInfo;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$Anon1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $searchedRecent;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2, Ref$ObjectRef ref$ObjectRef, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2;
            this.$searchedRecent = ref$ObjectRef;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$searchedRecent, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.m.a((List<String>) (List) this.$searchedRecent.element);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, String str, String str2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeSettingsPresenter;
        this.$address = str;
        this.$displayInfo = str2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 = new CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2(this.this$Anon0, this.$address, this.$displayInfo, kc4);
        commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2.p$ = (lh4) obj;
        return commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(T t) {
        lh4 lh4;
        Ref$ObjectRef ref$ObjectRef;
        Ref$ObjectRef ref$ObjectRef2;
        T a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(t);
            lh4 = this.p$;
            ref$ObjectRef2 = new Ref$ObjectRef();
            gh4 a2 = this.this$Anon0.c();
            CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1 = new CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = ref$ObjectRef2;
            this.L$Anon2 = ref$ObjectRef2;
            this.label = 1;
            t = kg4.a(a2, commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1, this);
            if (t == a) {
                return a;
            }
            ref$ObjectRef = ref$ObjectRef2;
        } else if (i == 1) {
            ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon2;
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(t);
        } else if (i == 2) {
            Ref$ObjectRef ref$ObjectRef3 = (Ref$ObjectRef) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(t);
            this.this$Anon0.l.a();
            this.this$Anon0.l.r(true);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        wd4.a((Object) t, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        ref$ObjectRef2.element = (List) t;
        if (!((List) ref$ObjectRef.element).contains(this.$address)) {
            if (this.$displayInfo.length() > 0) {
                if (this.$address.length() > 0) {
                    ((List) ref$ObjectRef.element).add(0, this.$address);
                    if (((List) ref$ObjectRef.element).size() > 5) {
                        ref$ObjectRef.element = ((List) ref$ObjectRef.element).subList(0, 5);
                    }
                    gh4 a3 = this.this$Anon0.c();
                    Anon1 anon1 = new Anon1(this, ref$ObjectRef, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = ref$ObjectRef;
                    this.label = 2;
                    if (kg4.a(a3, anon1, this) == a) {
                        return a;
                    }
                }
            }
        }
        this.this$Anon0.l.a();
        this.this$Anon0.l.r(true);
        return cb4.a;
    }
}
