package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pc3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qc3;
import com.fossil.blesdk.obfuscated.rc3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewMonthPresenter extends pc3 {
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public LiveData<ps3<List<DailyHeartRateSummary>>> k; // = new MutableLiveData();
    @DexIgnore
    public List<DailyHeartRateSummary> l; // = new ArrayList();
    @DexIgnore
    public LiveData<ps3<List<DailyHeartRateSummary>>> m;
    @DexIgnore
    public /* final */ qc3 n;
    @DexIgnore
    public /* final */ UserRepository o;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter a;

        @DexIgnore
        public b(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            this.a = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<DailyHeartRateSummary>>> apply(Date date) {
            HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter = this.a;
            wd4.a((Object) date, "it");
            if (heartRateOverviewMonthPresenter.b(date)) {
                HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter2 = this.a;
                heartRateOverviewMonthPresenter2.k = heartRateOverviewMonthPresenter2.p.getHeartRateSummaries(HeartRateOverviewMonthPresenter.j(this.a), HeartRateOverviewMonthPresenter.g(this.a), true);
            }
            return this.a.k;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewMonthPresenter(qc3 qc3, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        wd4.b(qc3, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.n = qc3;
        this.o = userRepository;
        this.p = heartRateSummaryRepository;
        LiveData<ps3<List<DailyHeartRateSummary>>> b2 = ic.b(this.f, new b(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026mHeartRateSummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date g(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        wd4.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date j(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<List<DailyHeartRateSummary>>> liveData = this.m;
        qc3 qc3 = this.n;
        if (qc3 != null) {
            liveData.a((rc3) qc3, new HeartRateOverviewMonthPresenter$start$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        MFLogger.d("HeartRateOverviewMonthPresenter", "stop");
        LiveData<ps3<List<DailyHeartRateSummary>>> liveData = this.m;
        qc3 qc3 = this.n;
        if (qc3 != null) {
            liveData.a((LifecycleOwner) (rc3) qc3);
            this.k.a((LifecycleOwner) this.n);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    public void h() {
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                wd4.d("mCurrentDate");
                throw null;
            } else if (sk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.g;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("HeartRateOverviewMonthPresenter", sb.toString());
                    return;
                }
                wd4.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.g;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("HeartRateOverviewMonthPresenter", sb2.toString());
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateOverviewMonthPresenter$loadData$Anon2(this, (kc4) null), 3, (Object) null);
            return;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.n.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!sk2.a(date4.getTime(), date.getTime())) {
                Calendar o2 = sk2.o(date);
                wd4.a((Object) o2, "DateHelper.getStartOfMonth(date)");
                Date time = o2.getTime();
                wd4.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean r = sk2.r(date);
            wd4.a((Object) r, "DateHelper.isThisMonth(date)");
            if (r.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar j2 = sk2.j(date);
                wd4.a((Object) j2, "DateHelper.getEndOfMonth(date)");
                date2 = j2.getTime();
                wd4.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wd4.d("mStartDate");
                throw null;
            }
            wd4.d("mEndDate");
            throw null;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        if (this.f.a() == null || !sk2.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }
}
