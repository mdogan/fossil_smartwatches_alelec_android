package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fg3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1", f = "GoalTrackingOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(GoalTrackingOverviewDayPresenter$showDetailChart$Anon1 goalTrackingOverviewDayPresenter$showDetailChart$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 goalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 = new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(this.this$Anon0, kc4);
        goalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1.p$ = (lh4) obj;
        return goalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            fg3 fg3 = fg3.a;
            Date b = GoalTrackingOverviewDayPresenter.b(this.this$Anon0.this$Anon0);
            ps3 ps3 = (ps3) this.this$Anon0.this$Anon0.j.a();
            return fg3.a(b, ps3 != null ? (List) ps3.d() : null);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
