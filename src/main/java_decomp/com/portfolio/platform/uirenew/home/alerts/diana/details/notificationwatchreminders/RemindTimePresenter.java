package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.jy2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.ky2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemindTimePresenter extends jy2 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public int f;
    @DexIgnore
    public RemindTimeModel g;
    @DexIgnore
    public /* final */ ky2 h;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = RemindTimePresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "RemindTimePresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public RemindTimePresenter(ky2 ky2, RemindersSettingsDatabase remindersSettingsDatabase) {
        wd4.b(ky2, "mView");
        wd4.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.h = ky2;
        this.i = remindersSettingsDatabase;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new RemindTimePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "stop");
    }

    @DexIgnore
    public void h() {
        RemindTimeModel remindTimeModel = this.g;
        if (remindTimeModel != null) {
            remindTimeModel.setMinutes(this.f);
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new RemindTimePresenter$save$Anon1(this, (kc4) null), 3, (Object) null);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(int i2) {
        this.f = i2 * 20;
    }
}
