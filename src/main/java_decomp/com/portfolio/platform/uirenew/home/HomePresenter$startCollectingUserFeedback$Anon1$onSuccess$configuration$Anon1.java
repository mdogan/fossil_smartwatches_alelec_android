package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.lr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomePresenter$startCollectingUserFeedback$Anon1$onSuccess$configuration$Anon1 extends BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ lr2.d $responseValue;

    @DexIgnore
    public HomePresenter$startCollectingUserFeedback$Anon1$onSuccess$configuration$Anon1(lr2.d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public String getAdditionalInfo() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a = HomePresenter.y.a();
        local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
        return this.$responseValue.a();
    }

    @DexIgnore
    public String getRequestSubject() {
        return this.$responseValue.d();
    }

    @DexIgnore
    public List<String> getTags() {
        return this.$responseValue.e();
    }
}
