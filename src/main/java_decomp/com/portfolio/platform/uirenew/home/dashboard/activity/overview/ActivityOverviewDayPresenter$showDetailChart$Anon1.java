package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$Anon1", f = "ActivityOverviewDayPresenter.kt", l = {114, 116, 117}, m = "invokeSuspend")
public final class ActivityOverviewDayPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityOverviewDayPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewDayPresenter$showDetailChart$Anon1(ActivityOverviewDayPresenter activityOverviewDayPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activityOverviewDayPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivityOverviewDayPresenter$showDetailChart$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        activityOverviewDayPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return activityOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityOverviewDayPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a7 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
    public final Object invokeSuspend(Object obj) {
        Pair pair;
        ArrayList arrayList;
        Integer num;
        lh4 lh4;
        Pair pair2;
        Object a;
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            gh4 a3 = this.this$Anon0.b();
            ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.label = 1;
            Object a4 = kg4.a(a3, activityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1, this);
            if (a4 == a2) {
                return a2;
            }
            lh4 = lh42;
            obj = a4;
        } else if (i == 1) {
            za4.a(obj);
            lh4 = (lh4) this.L$Anon0;
        } else if (i == 2) {
            arrayList = (ArrayList) this.L$Anon2;
            pair2 = (Pair) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            Integer num2 = (Integer) obj;
            gh4 a5 = this.this$Anon0.b();
            ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = pair2;
            this.L$Anon2 = arrayList;
            this.L$Anon3 = num2;
            this.label = 3;
            a = kg4.a(a5, activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1, this);
            if (a != a2) {
                return a2;
            }
            num = num2;
            obj = a;
            pair = pair2;
            int a6 = yk2.d.a((ActivitySummary) obj, GoalType.TOTAL_STEPS);
            this.this$Anon0.l.b(new BarChart.c(Math.max(num == null ? num.intValue() : 0, a6 / 16), a6, arrayList), (ArrayList) pair.getSecond());
            return cb4.a;
        } else if (i == 3) {
            num = (Integer) this.L$Anon3;
            arrayList = (ArrayList) this.L$Anon2;
            pair = (Pair) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            int a62 = yk2.d.a((ActivitySummary) obj, GoalType.TOTAL_STEPS);
            this.this$Anon0.l.b(new BarChart.c(Math.max(num == null ? num.intValue() : 0, a62 / 16), a62, arrayList), (ArrayList) pair.getSecond());
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair3 = (Pair) obj;
        arrayList = (ArrayList) pair3.getFirst();
        gh4 a7 = this.this$Anon0.b();
        ActivityOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1(arrayList, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = pair3;
        this.L$Anon2 = arrayList;
        this.label = 2;
        Object a8 = kg4.a(a7, activityOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1, this);
        if (a8 == a2) {
            return a2;
        }
        Object obj2 = a8;
        pair2 = pair3;
        obj = obj2;
        Integer num22 = (Integer) obj;
        gh4 a52 = this.this$Anon0.b();
        ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon12 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = pair2;
        this.L$Anon2 = arrayList;
        this.L$Anon3 = num22;
        this.label = 3;
        a = kg4.a(a52, activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon12, this);
        if (a != a2) {
        }
    }
}
