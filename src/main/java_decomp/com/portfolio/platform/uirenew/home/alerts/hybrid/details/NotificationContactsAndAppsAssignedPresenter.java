package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.gz2;
import com.fossil.blesdk.obfuscated.hz2;
import com.fossil.blesdk.obfuscated.i03;
import com.fossil.blesdk.obfuscated.iz2;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.jq4;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.rp4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wq2;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedPresenter extends gz2 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((rd4) null);
    @DexIgnore
    public /* final */ List<Object> g; // = new ArrayList();
    @DexIgnore
    public List<Object> h; // = new ArrayList();
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public /* final */ List<ContactWrapper> j; // = new ArrayList();
    @DexIgnore
    public /* final */ List<AppWrapper> k; // = new ArrayList();
    @DexIgnore
    public /* final */ List<ContactWrapper> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> m; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> n; // = new ArrayList();
    @DexIgnore
    public /* final */ LoaderManager o;
    @DexIgnore
    public /* final */ hz2 p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ k62 r;
    @DexIgnore
    public /* final */ u03 s;
    @DexIgnore
    public /* final */ i03 t;
    @DexIgnore
    public /* final */ f13 u;
    @DexIgnore
    public /* final */ wq2 v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsAndAppsAssignedPresenter.w;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.d<i03.b, j62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

        @DexIgnore
        public b(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            this.a = notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(i03.b bVar) {
            wd4.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetApps onSuccess");
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : bVar.a()) {
                InstalledApp installedApp = next.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    wd4.a();
                    throw null;
                } else if (isSelected.booleanValue() && next.getCurrentHandGroup() == this.a.q) {
                    arrayList.add(next);
                }
            }
            this.a.o().addAll(arrayList);
            this.a.n().addAll(arrayList);
            List<Object> p = this.a.p();
            Object[] array = arrayList.toArray(new AppWrapper[0]);
            if (array != null) {
                Serializable a2 = rp4.a((Serializable) array);
                wd4.a((Object) a2, "SerializationUtils.clone\u2026sSelected.toTypedArray())");
                tb4.a(p, (T[]) (Object[]) a2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = NotificationContactsAndAppsAssignedPresenter.x.a();
                local.d(a3, "mContactAndAppDataFirstLoad.size=" + this.a.p().size());
                this.a.o.a(1, new Bundle(), this.a);
                this.a.p.f(this.a.o());
                this.a.p.d();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void a(j62.a aVar) {
            wd4.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetApps onError");
            this.a.p.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.d<i03.b, j62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public c(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, ArrayList arrayList, List list) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(i03.b bVar) {
            wd4.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetHybridApp onSuccess");
            ArrayList arrayList = new ArrayList();
            for (String str : this.b) {
                Iterator<T> it = bVar.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        AppWrapper appWrapper = (AppWrapper) it.next();
                        if (wd4.a((Object) String.valueOf(appWrapper.getUri()), (Object) str)) {
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            if (installedApp != null) {
                                installedApp.setSelected(true);
                            }
                            appWrapper.setCurrentHandGroup(this.a.q);
                            arrayList.add(appWrapper);
                            List list = this.c;
                            Uri uri = appWrapper.getUri();
                            if (uri != null) {
                                list.add(uri);
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationContactsAndAppsAssignedPresenter.x.a();
            local.d(a2, "queryAppsString: appsSelected=" + arrayList);
            this.a.o().addAll(arrayList);
            NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter = this.a;
            notificationContactsAndAppsAssignedPresenter.a(notificationContactsAndAppsAssignedPresenter.s());
            this.a.p.p(this.a.i());
            int size = this.a.n().size();
            for (int i = 0; i < size; i++) {
                if (!wb4.a(this.c, this.a.n().get(i).getUri())) {
                    AppWrapper appWrapper2 = this.a.n().get(i);
                    InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                    if (installedApp2 != null) {
                        installedApp2.setSelected(false);
                        this.a.o().add(appWrapper2);
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
            this.a.p.f(this.a.o());
        }

        @DexIgnore
        public void a(j62.a aVar) {
            wd4.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetApps onError");
            this.a.p.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<wq2.d, wq2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements j62.d<f13.c, j62.a> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(f13.c cVar) {
                wd4.b(cVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), ".Inside mSaveAllNotification onSuccess");
                this.a.a.p.d();
                d dVar = this.a;
                if (dVar.d) {
                    dVar.a.p.close();
                }
            }

            @DexIgnore
            public void a(j62.a aVar) {
                wd4.b(aVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), ".Inside mSaveAllNotification onError");
                this.a.a.p.d();
            }
        }

        @DexIgnore
        public d(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, List list, List list2, boolean z) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = list;
            this.c = list2;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(wq2.d dVar) {
            wd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "saveNotification success!!");
            this.a.r.a(this.a.u, new f13.b(this.b, this.c), new a(this));
        }

        @DexIgnore
        public void a(wq2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationContactsAndAppsAssignedPresenter.x.a();
            local.d(a2, "saveNotification fail!! errorValue=" + cVar.a());
            this.a.p.d();
            if (cVar.c() != 1101) {
                this.a.p.j();
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "Bluetooth is disabled");
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(cVar.b());
            wd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            hz2 f = this.a.p;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                f.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = NotificationContactsAndAppsAssignedPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationContactsAndA\u2026er::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter(LoaderManager loaderManager, hz2 hz2, int i2, k62 k62, u03 u03, i03 i03, f13 f13, wq2 wq2) {
        wd4.b(loaderManager, "mLoaderManager");
        wd4.b(hz2, "mView");
        wd4.b(k62, "mUseCaseHandler");
        wd4.b(u03, "mGetAllHybridContactGroups");
        wd4.b(i03, "mGetHybridApp");
        wd4.b(f13, "mSaveAllHybridNotification");
        wd4.b(wq2, "mSetNotificationFiltersUserCase");
        this.o = loaderManager;
        this.p = hz2;
        this.q = i2;
        this.r = k62;
        this.s = u03;
        this.t = i03;
        this.u = f13;
        this.v = wq2;
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar) {
        wd4.b(rcVar, "loader");
    }

    @DexIgnore
    public void j() {
        ArrayList arrayList = new ArrayList();
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Object obj = this.g.get(size);
                if (obj instanceof AppWrapper) {
                    AppWrapper appWrapper = (AppWrapper) obj;
                    InstalledApp installedApp = appWrapper.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        wd4.a();
                        throw null;
                    } else if (isSelected.booleanValue()) {
                        Uri uri = appWrapper.getUri();
                        if (uri != null) {
                            arrayList.add(uri.toString());
                        }
                    } else {
                        this.g.remove(size);
                    }
                }
            }
        }
        this.p.b(this.q, arrayList);
    }

    @DexIgnore
    public void k() {
        ArrayList arrayList = new ArrayList();
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Object obj = this.g.get(size);
                if (obj instanceof ContactWrapper) {
                    ContactWrapper contactWrapper = (ContactWrapper) obj;
                    Contact contact = contactWrapper.getContact();
                    if (contact != null && contact.getContactId() == -100) {
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null && contact2.getContactId() == -200) {
                        }
                    }
                    if (contactWrapper.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.g.remove(size);
                    }
                }
            }
        }
        this.p.c(this.q, arrayList);
    }

    @DexIgnore
    public void l() {
        ArrayList arrayList = new ArrayList();
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Object obj = this.g.get(size);
                if (obj instanceof ContactWrapper) {
                    ContactWrapper contactWrapper = (ContactWrapper) obj;
                    Contact contact = contactWrapper.getContact();
                    if (contact == null || contact.getContactId() != -100) {
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            if (contact2.getContactId() != -200) {
                            }
                        }
                    }
                    if (contactWrapper.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.g.remove(size);
                    }
                }
            }
        }
        this.p.a(this.q, arrayList);
    }

    @DexIgnore
    public void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "mContactAndAppData.size=" + this.g.size() + " mContactAndAppDataFirstLoad.size=" + this.h.size());
        if (i()) {
            this.p.e();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (T next : this.g) {
                if (next instanceof ContactWrapper) {
                    arrayList.add(next);
                } else if (next != null) {
                    arrayList2.add((AppWrapper) next);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                }
            }
            a((List<ContactWrapper>) arrayList, (List<AppWrapper>) arrayList2, true);
        }
    }

    @DexIgnore
    public final List<AppWrapper> n() {
        return this.k;
    }

    @DexIgnore
    public final List<Object> o() {
        return this.g;
    }

    @DexIgnore
    public final List<Object> p() {
        return this.h;
    }

    @DexIgnore
    public final List<ContactWrapper> q() {
        return this.j;
    }

    @DexIgnore
    public final List<ContactWrapper> r() {
        return this.l;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0071 A[EDGE_INSN: B:128:0x0071->B:30:0x0071 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x012e A[EDGE_INSN: B:131:0x012e->B:86:0x012e ?: BREAK  , SYNTHETIC] */
    public final boolean s() {
        List<Object> list;
        List<Object> list2;
        T t2;
        boolean z;
        T t3;
        boolean z2;
        if (this.h.size() >= this.g.size()) {
            list2 = this.h;
            list = this.g;
        } else {
            list2 = this.g;
            list = this.h;
        }
        for (T next : list2) {
            if (next instanceof ContactWrapper) {
                ContactWrapper contactWrapper = (ContactWrapper) next;
                if (contactWrapper.getContact() != null) {
                    Contact contact = contactWrapper.getContact();
                    if (contact != null) {
                        int contactId = contact.getContactId();
                        Iterator<T> it = list.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t3 = null;
                                break;
                            }
                            t3 = it.next();
                            if (t3 instanceof ContactWrapper) {
                                ContactWrapper contactWrapper2 = (ContactWrapper) t3;
                                if (contactWrapper2.getContact() != null) {
                                    Contact contact2 = contactWrapper2.getContact();
                                    if (contact2 != null) {
                                        if (contact2.getContactId() == contactId) {
                                            z2 = true;
                                            continue;
                                            if (z2) {
                                                break;
                                            }
                                        }
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                }
                            }
                            z2 = false;
                            continue;
                            if (z2) {
                            }
                        }
                        if (t3 == null) {
                            return true;
                        }
                        ContactWrapper contactWrapper3 = (ContactWrapper) t3;
                        if (contactWrapper.isAdded() == contactWrapper3.isAdded()) {
                            Contact contact3 = contactWrapper.getContact();
                            if (contact3 != null) {
                                boolean isUseSms = contact3.isUseSms();
                                Contact contact4 = contactWrapper3.getContact();
                                if (contact4 == null) {
                                    wd4.a();
                                    throw null;
                                } else if (isUseSms == contact4.isUseSms()) {
                                    Contact contact5 = contactWrapper.getContact();
                                    if (contact5 != null) {
                                        boolean isUseCall = contact5.isUseCall();
                                        Contact contact6 = contactWrapper3.getContact();
                                        if (contact6 == null) {
                                            wd4.a();
                                            throw null;
                                        } else if (isUseCall == contact6.isUseCall()) {
                                            Contact contact7 = contactWrapper.getContact();
                                            if (contact7 != null) {
                                                boolean isUseEmail = contact7.isUseEmail();
                                                Contact contact8 = contactWrapper3.getContact();
                                                if (contact8 == null) {
                                                    wd4.a();
                                                    throw null;
                                                } else if (isUseEmail == contact8.isUseEmail() && contactWrapper.getCurrentHandGroup() == contactWrapper3.getCurrentHandGroup()) {
                                                }
                                            } else {
                                                wd4.a();
                                                throw null;
                                            }
                                        }
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                        return true;
                    }
                    wd4.a();
                    throw null;
                }
            }
            if (next instanceof AppWrapper) {
                AppWrapper appWrapper = (AppWrapper) next;
                if (appWrapper.getInstalledApp() != null) {
                    Iterator<T> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it2.next();
                        if (t2 instanceof AppWrapper) {
                            AppWrapper appWrapper2 = (AppWrapper) t2;
                            if (wd4.a((Object) String.valueOf(appWrapper2.getUri()), (Object) String.valueOf(appWrapper.getUri())) && appWrapper2.getInstalledApp() != null) {
                                z = true;
                                continue;
                                if (z) {
                                    break;
                                }
                            }
                        }
                        z = false;
                        continue;
                        if (z) {
                        }
                    }
                    if (t2 == null) {
                        return true;
                    }
                    AppWrapper appWrapper3 = (AppWrapper) t2;
                    if (appWrapper3.getCurrentHandGroup() == appWrapper.getCurrentHandGroup()) {
                        InstalledApp installedApp = appWrapper3.getInstalledApp();
                        if (installedApp != null) {
                            Boolean isSelected = installedApp.isSelected();
                            InstalledApp installedApp2 = appWrapper.getInstalledApp();
                            if (installedApp2 == null) {
                                wd4.a();
                                throw null;
                            } else if (!wd4.a((Object) isSelected, (Object) installedApp2.isSelected())) {
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    return true;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    public final void t() {
        this.r.a(this.t, null, new b(this));
    }

    @DexIgnore
    public final void u() {
        this.p.e();
        this.r.a(this.s, null, new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1(this));
    }

    @DexIgnore
    public final void v() {
        if (!this.n.isEmpty()) {
            Collection<T> a2 = jq4.a(this.n, this.m);
            wd4.a((Object) a2, "CollectionUtils.subtract\u2026actIds, mPhoneContactIds)");
            List<T> k2 = wb4.k(a2);
            if (!k2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                int size = this.g.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    Object obj = this.g.get(size);
                    if (obj instanceof ContactWrapper) {
                        ContactWrapper contactWrapper = (ContactWrapper) obj;
                        Contact contact = contactWrapper.getContact();
                        Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                        int size2 = k2.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                break;
                            } else if (wd4.a((Object) (Integer) k2.get(i2), (Object) valueOf)) {
                                contactWrapper.setAdded(false);
                                arrayList.add(obj);
                                this.g.remove(size);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.p.e();
                    a((List<ContactWrapper>) arrayList, (List<AppWrapper>) new ArrayList(), false);
                }
            }
        }
    }

    @DexIgnore
    public void w() {
        this.p.a(this);
    }

    @DexIgnore
    public void b(ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Object obj = this.g.get(size);
                if (obj instanceof ContactWrapper) {
                    ContactWrapper contactWrapper = (ContactWrapper) obj;
                    Contact contact = contactWrapper.getContact();
                    if (contact == null || contact.getContactId() != -100) {
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            if (contact2.getContactId() != -200) {
                            }
                        }
                    }
                    this.g.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (ContactWrapper contactWrapper2 : arrayList) {
            this.g.add(contactWrapper2);
            Contact contact3 = contactWrapper2.getContact();
            if (contact3 != null) {
                arrayList2.add(Integer.valueOf(contact3.getContactId()));
            } else {
                wd4.a();
                throw null;
            }
        }
        a(s());
        this.p.p(i());
        int size2 = this.l.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact4 = this.l.get(i2).getContact();
            if (!wb4.a(arrayList2, contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                ContactWrapper contactWrapper3 = this.l.get(i2);
                contactWrapper3.setAdded(false);
                this.g.add(contactWrapper3);
            }
        }
        this.p.f(this.g);
    }

    @DexIgnore
    public void c(ArrayList<String> arrayList) {
        wd4.b(arrayList, "stringAppsSelected");
        FLogger.INSTANCE.getLocal().d(w, "queryAppsString = " + arrayList);
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                if (this.g.get(size) instanceof AppWrapper) {
                    this.g.remove(size);
                }
            }
        }
        this.r.a(this.t, null, new c(this, arrayList, new ArrayList()));
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.v.e();
        BleCommandResultManager.d.a(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
        hz2 hz2 = this.p;
        if (hz2 != null) {
            Context context = ((iz2) hz2).getContext();
            if (context != null && cn2.a(cn2.d, context, "NOTIFICATION_CONTACTS_ASSIGNMENT", false, 4, (Object) null)) {
                this.p.h(this.q);
                if (!this.g.isEmpty() || !this.i) {
                    wd4.a((Object) this.o.a(1, new Bundle(), this), "mLoaderManager.initLoade\u2026AndAppsAssignedPresenter)");
                    return;
                }
                this.i = false;
                u();
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.v.e();
        this.o.a(1);
    }

    @DexIgnore
    public void h() {
        a(s());
        this.p.p(i());
    }

    @DexIgnore
    public void a(int i2, boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "updateContactWrapper: contactId=" + i2 + ", useCall=" + z + ", useText=" + z2);
        for (T next : this.g) {
            if (next instanceof ContactWrapper) {
                ContactWrapper contactWrapper = (ContactWrapper) next;
                Contact contact = contactWrapper.getContact();
                if (contact != null && contact.getContactId() == i2) {
                    Contact contact2 = contactWrapper.getContact();
                    if (contact2 != null) {
                        contact2.setUseCall(z);
                    }
                    Contact contact3 = contactWrapper.getContact();
                    if (contact3 != null) {
                        contact3.setUseSms(z2);
                    }
                }
            }
        }
        a(s());
        this.p.p(i());
        this.p.f(this.g);
    }

    @DexIgnore
    public void a(ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        FLogger.INSTANCE.getLocal().d(w, "addContactWrapperList: contactWrappersSelected = " + arrayList);
        if (!this.g.isEmpty()) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                if (this.g.get(size) instanceof ContactWrapper) {
                    this.g.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (ContactWrapper contactWrapper : arrayList) {
            this.g.add(contactWrapper);
            Contact contact = contactWrapper.getContact();
            if (contact != null) {
                arrayList2.add(Integer.valueOf(contact.getContactId()));
            } else {
                wd4.a();
                throw null;
            }
        }
        a(s());
        this.p.p(i());
        int size2 = this.j.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact2 = this.j.get(i2).getContact();
            if (!wb4.a(arrayList2, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                ContactWrapper contactWrapper2 = this.j.get(i2);
                contactWrapper2.setAdded(false);
                this.g.add(contactWrapper2);
            }
        }
        this.p.f(this.g);
    }

    @DexIgnore
    public rc<Cursor> a(int i2, Bundle bundle) {
        boolean z;
        this.n.clear();
        this.m.clear();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("has_phone_number != 0 AND mimetype =? ");
        ArrayList arrayList = new ArrayList();
        arrayList.add("vnd.android.cursor.item/phone_v2");
        if (!this.g.isEmpty()) {
            z = false;
            for (T next : this.g) {
                if (next instanceof ContactWrapper) {
                    ContactWrapper contactWrapper = (ContactWrapper) next;
                    if (contactWrapper.getContact() != null) {
                        Contact contact = contactWrapper.getContact();
                        Integer num = null;
                        if (contact == null) {
                            wd4.a();
                            throw null;
                        } else if (contact.getContactId() >= 0) {
                            if (!z) {
                                sb2.append("AND contact_id IN (");
                                z = true;
                            }
                            sb2.append("?, ");
                            Contact contact2 = contactWrapper.getContact();
                            if (contact2 != null) {
                                num = Integer.valueOf(contact2.getContactId());
                            }
                            if (num != null) {
                                this.n.add(num);
                            }
                            arrayList.add(String.valueOf(num));
                        }
                    } else {
                        continue;
                    }
                }
            }
        } else {
            z = false;
        }
        if (z) {
            sb.append(new StringBuilder(sb2.substring(0, sb2.length() - 2)));
            sb.append(")");
        } else {
            sb.append(sb2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, ".Inside onCreateLoader, selectionQuery = " + sb);
        String[] strArr = {"contact_id", "display_name", "photo_thumb_uri"};
        PortfolioApp c2 = PortfolioApp.W.c();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String sb3 = sb.toString();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new qc(c2, uri, strArr, sb3, (String[]) array, (String) null);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar, Cursor cursor) {
        wd4.b(rcVar, "loader");
        if (cursor != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onLoadFinished, loader id = " + rcVar.getId() + "cursor is closed = " + cursor.isClosed());
            if (cursor.isClosed()) {
                this.o.b(rcVar.getId(), new Bundle(), this);
                return;
            }
            if (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex("display_name"));
                    String string2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                    int i2 = cursor.getInt(cursor.getColumnIndex("contact_id"));
                    this.m.add(Integer.valueOf(i2));
                    a(i2, string, string2);
                } while (cursor.moveToNext());
            }
            v();
            this.p.f(this.g);
            cursor.close();
        }
    }

    @DexIgnore
    public final void a(int i2, String str, String str2) {
        for (T next : this.g) {
            if (next instanceof ContactWrapper) {
                ContactWrapper contactWrapper = (ContactWrapper) next;
                Contact contact = contactWrapper.getContact();
                if (contact != null && contact.getContactId() == i2) {
                    Contact contact2 = contactWrapper.getContact();
                    if (contact2 != null) {
                        contact2.setFirstName(str);
                    }
                    Contact contact3 = contactWrapper.getContact();
                    if (contact3 != null) {
                        contact3.setPhotoThumbUri(str2);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a(List<ContactWrapper> list, List<AppWrapper> list2, boolean z) {
        this.v.a(new wq2.b(list, list2, this.q), new d(this, list, list2, z));
    }
}
