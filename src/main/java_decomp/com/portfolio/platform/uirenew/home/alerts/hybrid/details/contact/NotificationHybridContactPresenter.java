package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.l03;
import com.fossil.blesdk.obfuscated.m03;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.n03;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridContactPresenter extends l03 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public /* final */ List<ContactWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ m03 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> i;
    @DexIgnore
    public /* final */ LoaderManager j;
    @DexIgnore
    public /* final */ k62 k;
    @DexIgnore
    public /* final */ u03 l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridContactPresenter.m;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = NotificationHybridContactPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridContactPresenter(m03 m03, int i2, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager, k62 k62, u03 u03) {
        wd4.b(m03, "mView");
        wd4.b(arrayList, "mContactWrappersSelected");
        wd4.b(loaderManager, "mLoaderManager");
        wd4.b(k62, "mUseCaseHandler");
        wd4.b(u03, "mGetAllHybridContactGroups");
        this.g = m03;
        this.h = i2;
        this.i = arrayList;
        this.j = loaderManager;
        this.k = k62;
        this.l = u03;
    }

    @DexIgnore
    public void i() {
        ArrayList arrayList = new ArrayList();
        for (ContactWrapper contactWrapper : this.f) {
            if (contactWrapper.isAdded() && contactWrapper.getCurrentHandGroup() == this.h) {
                arrayList.add(contactWrapper);
            }
        }
        this.g.a((ArrayList<ContactWrapper>) arrayList);
    }

    @DexIgnore
    public final List<ContactWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(m, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        cn2 cn2 = cn2.d;
        m03 m03 = this.g;
        if (m03 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactFragment");
        } else if (cn2.a(cn2, ((n03) m03).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationHybridContactPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(m, "stop");
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public void a(ContactWrapper contactWrapper) {
        T t;
        wd4.b(contactWrapper, "contactWrapper");
        FLogger.INSTANCE.getLocal().d(m, "reassignContact: contactWrapper=" + contactWrapper);
        Iterator<T> it = this.f.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            Contact contact = ((ContactWrapper) t).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (wd4.a((Object) valueOf, (Object) num)) {
                break;
            }
        }
        ContactWrapper contactWrapper2 = (ContactWrapper) t;
        if (contactWrapper2 != null) {
            contactWrapper2.setAdded(true);
            contactWrapper2.setCurrentHandGroup(this.h);
            this.g.x0();
        }
    }

    @DexIgnore
    public rc<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new qc(PortfolioApp.W.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar, Cursor cursor) {
        wd4.b(rcVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.a(cursor);
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar) {
        wd4.b(rcVar, "loader");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onLoaderReset");
        this.g.H();
    }
}
