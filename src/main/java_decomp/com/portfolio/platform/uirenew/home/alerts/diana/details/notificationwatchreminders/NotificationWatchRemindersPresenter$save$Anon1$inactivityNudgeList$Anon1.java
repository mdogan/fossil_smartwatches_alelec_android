package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
public final class NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends InactivityNudgeTimeModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationWatchRemindersPresenter$save$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1(NotificationWatchRemindersPresenter$save$Anon1 notificationWatchRemindersPresenter$save$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationWatchRemindersPresenter$save$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1 notificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1 = new NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1(this.this$Anon0, kc4);
        notificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1.p$ = (lh4) obj;
        return notificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return this.this$Anon0.this$Anon0.r.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
