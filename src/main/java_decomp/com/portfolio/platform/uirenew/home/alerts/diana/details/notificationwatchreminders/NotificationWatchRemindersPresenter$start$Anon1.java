package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.SpannableString;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cy2;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ol2;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationWatchRemindersPresenter$start$Anon1<T> implements dc<List<? extends InactivityNudgeTimeModel>> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $lInActivityNudgeTimeModel;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0135Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0135Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0135Anon1 anon1 = new C0135Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0135Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    this.this$Anon0.this$Anon0.a.r.getInactivityNudgeTimeDao().upsertListInactivityNudgeTime(this.this$Anon0.$lInActivityNudgeTimeModel);
                    return cb4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(NotificationWatchRemindersPresenter$start$Anon1 notificationWatchRemindersPresenter$start$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = notificationWatchRemindersPresenter$start$Anon1;
            this.$lInActivityNudgeTimeModel = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$lInActivityNudgeTimeModel, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 a2 = this.this$Anon0.a.c();
                C0135Anon1 anon1 = new C0135Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (kg4.a(a2, anon1, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cb4.a;
        }
    }

    @DexIgnore
    public NotificationWatchRemindersPresenter$start$Anon1(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
        this.a = notificationWatchRemindersPresenter;
    }

    @DexIgnore
    public final void a(List<InactivityNudgeTimeModel> list) {
        if (list == null || list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
            InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
            arrayList.add(inactivityNudgeTimeModel);
            arrayList.add(inactivityNudgeTimeModel2);
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, arrayList, (kc4) null), 3, (Object) null);
            return;
        }
        for (InactivityNudgeTimeModel inactivityNudgeTimeModel3 : list) {
            if (inactivityNudgeTimeModel3.getNudgeTimeType() == 0) {
                cy2 h = this.a.p;
                SpannableString e = ol2.e(inactivityNudgeTimeModel3.getMinutes());
                wd4.a((Object) e, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                h.c(e);
            } else {
                cy2 h2 = this.a.p;
                SpannableString e2 = ol2.e(inactivityNudgeTimeModel3.getMinutes());
                wd4.a((Object) e2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                h2.d(e2);
            }
        }
    }
}
