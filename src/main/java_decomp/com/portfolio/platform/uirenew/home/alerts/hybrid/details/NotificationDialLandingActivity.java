package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sz2;
import com.fossil.blesdk.obfuscated.tz2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationDialLandingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public NotificationDialLandingPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            wd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationDialLandingActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        wd4.a((Object) NotificationDialLandingActivity.class.getSimpleName(), "NotificationDialLandingA\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        sz2 sz2 = (sz2) getSupportFragmentManager().a((int) R.id.content);
        if (sz2 == null) {
            sz2 = sz2.n.b();
            a((Fragment) sz2, sz2.n.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (sz2 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            wd4.a((Object) supportLoaderManager, "supportLoaderManager");
            g.a(new tz2(sz2, supportLoaderManager)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingContract.View");
    }
}
