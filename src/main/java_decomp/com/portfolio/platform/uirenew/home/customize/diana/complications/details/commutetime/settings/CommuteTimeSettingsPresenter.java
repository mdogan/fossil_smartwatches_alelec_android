package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.u23;
import com.fossil.blesdk.obfuscated.v23;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.DirectionBy;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsPresenter extends u23 {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public PlacesClient f;
    @DexIgnore
    public Gson g; // = new Gson();
    @DexIgnore
    public CommuteTimeSetting h;
    @DexIgnore
    public ArrayList<String> i; // = new ArrayList<>();
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ v23 l;
    @DexIgnore
    public /* final */ fn2 m;
    @DexIgnore
    public /* final */ UserRepository n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = CommuteTimeSettingsPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeSettingsPrese\u2026er::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsPresenter(v23 v23, fn2 fn2, UserRepository userRepository) {
        wd4.b(v23, "mView");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(userRepository, "mUserRepository");
        this.l = v23;
        this.m = fn2;
        this.n = userRepository;
    }

    @DexIgnore
    public void h() {
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setAddress("");
        }
    }

    @DexIgnore
    public CommuteTimeSetting i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        MFUser mFUser = this.j;
        String str = "";
        if (TextUtils.isEmpty(mFUser != null ? mFUser.getHome() : null)) {
            this.k = false;
            this.l.d("Home", str);
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.j;
            if (mFUser2 != null) {
                String home = mFUser2.getHome();
                if (home != null) {
                    str = home;
                }
            }
            commuteTimeSetting.setAddress(str);
        }
        this.l.r(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 != null) goto L_0x0016;
     */
    @DexIgnore
    public void k() {
        String str;
        if (this.h != null) {
            this.k = true;
            v23 v23 = this.l;
            MFUser mFUser = this.j;
            if (mFUser != null) {
                str = mFUser.getHome();
            }
            str = "";
            v23.d("Home", str);
        }
    }

    @DexIgnore
    public void l() {
        MFUser mFUser = this.j;
        if (TextUtils.isEmpty(mFUser != null ? mFUser.getWork() : null)) {
            this.k = false;
            this.l.d("Other", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.j;
            if (mFUser2 != null) {
                String work = mFUser2.getWork();
                wd4.a((Object) work, "mUser!!.work");
                commuteTimeSetting.setAddress(work);
            } else {
                wd4.a();
                throw null;
            }
        }
        this.l.r(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 != null) goto L_0x0016;
     */
    @DexIgnore
    public void m() {
        String str;
        if (this.h != null) {
            this.k = true;
            v23 v23 = this.l;
            MFUser mFUser = this.j;
            if (mFUser != null) {
                str = mFUser.getWork();
            }
            str = "";
            v23.d("Other", str);
        }
    }

    @DexIgnore
    public void n() {
        this.l.a(this);
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = o;
        local.d(str2, "updateFormatType, format = " + str);
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setFormat(str);
        }
        this.l.a(this.h);
    }

    @DexIgnore
    public void c(String str) {
        CommuteTimeSetting commuteTimeSetting;
        wd4.b(str, MicroAppSetting.SETTING);
        try {
            commuteTimeSetting = (CommuteTimeSetting) this.g.a(str, CommuteTimeSetting.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = o;
            local.d(str2, "exception when parse commute time setting " + e);
            commuteTimeSetting = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (rd4) null);
        }
        this.h = commuteTimeSetting;
        if (this.h == null) {
            this.h = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (rd4) null);
        }
    }

    @DexIgnore
    public void f() {
        this.f = Places.createClient(PortfolioApp.W.c());
        this.l.a(this.f);
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                this.l.x(commuteTimeSetting.getAddress());
            }
            this.l.L(commuteTimeSetting.getAvoidTolls());
        }
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsPresenter$start$Anon2(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        this.f = null;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "address");
        if (this.k) {
            this.k = false;
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null && !TextUtils.isEmpty(str)) {
            commuteTimeSetting.setAddress(str);
            this.l.x(str);
        }
    }

    @DexIgnore
    public void a(String str, String str2, DirectionBy directionBy, boolean z, String str3) {
        wd4.b(str, "displayInfo");
        wd4.b(str2, "address");
        wd4.b(directionBy, "directionBy");
        wd4.b(str3, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = o;
        local.d(str4, "saveCommuteTimeSetting - displayInfo=" + str + ", address=" + str2 + ", directionBy=" + directionBy.getType() + ", avoidTolls=" + z + ", format=" + str3);
        this.l.b();
        CommuteTimeSetting commuteTimeSetting = this.h;
        if (commuteTimeSetting != null) {
            boolean z2 = true;
            if (str.length() == 0) {
                if (str2.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    commuteTimeSetting.setAddress(str);
                }
            } else {
                commuteTimeSetting.setAddress(str2);
            }
            commuteTimeSetting.setAvoidTolls(z);
            commuteTimeSetting.setFormat(str3);
        }
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2(this, str2, str, (kc4) null), 3, (Object) null);
    }
}
