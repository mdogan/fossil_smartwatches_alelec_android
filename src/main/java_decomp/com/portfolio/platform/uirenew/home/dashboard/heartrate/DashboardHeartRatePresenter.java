package com.portfolio.platform.uirenew.home.dashboard.heartrate;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ac3;
import com.fossil.blesdk.obfuscated.bc3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zb3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardHeartRatePresenter extends zb3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<DailyHeartRateSummary> g;
    @DexIgnore
    public /* final */ ac3 h;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao k;
    @DexIgnore
    public /* final */ FitnessDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ i42 n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DashboardHeartRatePresenter(ac3 ac3, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42) {
        wd4.b(ac3, "mView");
        wd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(heartRateDailySummaryDao, "mHeartRateDailySummaryDao");
        wd4.b(fitnessDatabase, "mFitnessDatabase");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(i42, "mAppExecutors");
        this.h = ac3;
        this.i = heartRateSummaryRepository;
        this.j = fitnessDataRepository;
        this.k = heartRateDailySummaryDao;
        this.l = fitnessDatabase;
        this.m = userRepository;
        this.n = i42;
    }

    @DexIgnore
    public void j() {
        MFLogger.d("DashboardHeartRatePresenter", "retry all failed request");
        Listing<DailyHeartRateSummary> listing = this.g;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardHeartRatePresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!sk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<DailyHeartRateSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardHeartRatePresenter", "stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardHeartRatePresenter$initDataSource$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<DailyHeartRateSummary> listing = this.g;
            if (listing != null) {
                LiveData<rd<DailyHeartRateSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    ac3 ac3 = this.h;
                    if (ac3 != null) {
                        pagedList.a((LifecycleOwner) (bc3) ac3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("DashboardHeartRatePresenter", sb.toString());
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        MFLogger.d("DashboardHeartRatePresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
