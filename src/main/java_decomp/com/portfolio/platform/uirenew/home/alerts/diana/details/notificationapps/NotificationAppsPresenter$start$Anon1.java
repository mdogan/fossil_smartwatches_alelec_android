package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1", f = "NotificationAppsPresenter.kt", l = {130, 136, 190, 198, 207, 216}, m = "invokeSuspend")
public final class NotificationAppsPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$Anon1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon1(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                PortfolioApp.W.c().J();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$Anon6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon6 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $notificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon6 e;

            @DexIgnore
            public a(Anon6 anon6) {
                this.e = anon6;
            }

            @DexIgnore
            public final void run() {
                this.e.this$Anon0.this$Anon0.s.getNotificationSettingsDao().insertListNotificationSettings(this.e.$notificationSettings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon6(NotificationAppsPresenter$start$Anon1 notificationAppsPresenter$start$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = notificationAppsPresenter$start$Anon1;
            this.$notificationSettings = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon6 anon6 = new Anon6(this.this$Anon0, this.$notificationSettings, kc4);
            anon6.p$ = (lh4) obj;
            return anon6;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon6) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.s.runInTransaction((Runnable) new a(this));
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$Anon7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon7 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $listNotificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon7(NotificationAppsPresenter$start$Anon1 notificationAppsPresenter$start$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = notificationAppsPresenter$start$Anon1;
            this.$listNotificationSettings = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon7 anon7 = new Anon7(this.this$Anon0, this.$listNotificationSettings, kc4);
            anon7.p$ = (lh4) obj;
            return anon7;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon7) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                Iterator it = this.$listNotificationSettings.iterator();
                while (it.hasNext()) {
                    NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) it.next();
                    int component2 = notificationSettingsModel.component2();
                    int i = 0;
                    if (notificationSettingsModel.component3()) {
                        String a = this.this$Anon0.this$Anon0.a(component2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = NotificationAppsPresenter.u.a();
                        local.d(a2, "CALL settingsTypeName=" + a);
                        if (component2 == 0) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = NotificationAppsPresenter.u.a();
                            local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL);
                            DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                            this.this$Anon0.this$Anon0.j.add(new AppNotificationFilter(new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType())));
                        } else if (component2 == 1) {
                            int size = this.this$Anon0.this$Anon0.k.size();
                            int i2 = 0;
                            while (i2 < size) {
                                ContactGroup contactGroup = (ContactGroup) this.this$Anon0.this$Anon0.k.get(i2);
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = NotificationAppsPresenter.u.a();
                                StringBuilder sb = new StringBuilder();
                                sb.append("mListAppNotificationFilter add PHONE item - ");
                                sb.append(i2);
                                sb.append(" name = ");
                                Contact contact = contactGroup.getContacts().get(i);
                                wd4.a((Object) contact, "item.contacts[0]");
                                sb.append(contact.getDisplayName());
                                local3.d(a4, sb.toString());
                                DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                FNotification fNotification = r9;
                                FNotification fNotification2 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                                AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                Contact contact2 = contactGroup.getContacts().get(0);
                                wd4.a((Object) contact2, "item.contacts[0]");
                                appNotificationFilter.setSender(contact2.getDisplayName());
                                this.this$Anon0.this$Anon0.j.add(appNotificationFilter);
                                i2++;
                                i = 0;
                            }
                        }
                    } else {
                        String a5 = this.this$Anon0.this$Anon0.a(component2);
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String a6 = NotificationAppsPresenter.u.a();
                        local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                        if (component2 == 0) {
                            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                            String a7 = NotificationAppsPresenter.u.a();
                            local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.MESSAGES);
                            DianaNotificationObj.AApplicationName aApplicationName3 = DianaNotificationObj.AApplicationName.MESSAGES;
                            this.this$Anon0.this$Anon0.j.add(new AppNotificationFilter(new FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType())));
                        } else if (component2 == 1) {
                            int size2 = this.this$Anon0.this$Anon0.k.size();
                            int i3 = 0;
                            while (i3 < size2) {
                                ContactGroup contactGroup2 = (ContactGroup) this.this$Anon0.this$Anon0.k.get(i3);
                                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                String a8 = NotificationAppsPresenter.u.a();
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                sb2.append(i3);
                                sb2.append(" name = ");
                                Contact contact3 = contactGroup2.getContacts().get(0);
                                wd4.a((Object) contact3, "item.contacts[0]");
                                sb2.append(contact3.getDisplayName());
                                local6.d(a8, sb2.toString());
                                DianaNotificationObj.AApplicationName aApplicationName4 = DianaNotificationObj.AApplicationName.MESSAGES;
                                Iterator it2 = it;
                                FNotification fNotification3 = r8;
                                FNotification fNotification4 = new FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                                AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification3);
                                Contact contact4 = contactGroup2.getContacts().get(0);
                                wd4.a((Object) contact4, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact4.getDisplayName());
                                this.this$Anon0.this$Anon0.j.add(appNotificationFilter2);
                                i3++;
                                it = it2;
                            }
                        }
                    }
                    it = it;
                }
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppsPresenter$start$Anon1(NotificationAppsPresenter notificationAppsPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationAppsPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationAppsPresenter$start$Anon1 notificationAppsPresenter$start$Anon1 = new NotificationAppsPresenter$start$Anon1(this.this$Anon0, kc4);
        notificationAppsPresenter$start$Anon1.p$ = (lh4) obj;
        return notificationAppsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppsPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0293, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0294, code lost:
        r1 = (com.portfolio.platform.CoroutineUseCase.c) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0299, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.qx2.d) == false) goto L_0x037f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x029b, code lost:
        r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r6 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.u.a();
        r7 = new java.lang.StringBuilder();
        r7.append("GetAllContactGroup onSuccess, size = ");
        r8 = (com.fossil.blesdk.obfuscated.qx2.d) r1;
        r7.append(r8.a().size());
        r13.d(r6, r7.toString());
        r12.this$Anon0.k.clear();
        r12.this$Anon0.k = com.fossil.blesdk.obfuscated.wb4.d(r8.a());
        r13 = r12.this$Anon0.c();
        r6 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$listNotificationSettings$Anon1(r12, (com.fossil.blesdk.obfuscated.kc4) null);
        r12.L$Anon0 = r5;
        r12.L$Anon1 = r1;
        r12.label = 4;
        r13 = com.fossil.blesdk.obfuscated.kg4.a(r13, r6, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02f2, code lost:
        if (r13 != r0) goto L_0x02f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02f5, code lost:
        r13 = (java.util.List) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02fb, code lost:
        if (r13.isEmpty() == false) goto L_0x0337;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02fd, code lost:
        r6 = new java.util.ArrayList();
        r7 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true);
        r2 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false);
        r6.add(r7);
        r6.add(r2);
        r3 = r12.this$Anon0.c();
        r8 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1.Anon6(r12, r6, (com.fossil.blesdk.obfuscated.kc4) null);
        r12.L$Anon0 = r5;
        r12.L$Anon1 = r1;
        r12.L$Anon2 = r13;
        r12.L$Anon3 = r6;
        r12.L$Anon4 = r7;
        r12.L$Anon5 = r2;
        r12.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0334, code lost:
        if (com.fossil.blesdk.obfuscated.kg4.a(r3, r8, r12) != r0) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0336, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0337, code lost:
        r12.this$Anon0.j.clear();
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.u.a(), "listNotificationSettings.size = " + r13.size());
        r2 = r12.this$Anon0.b();
        r3 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1.Anon7(r12, r13, (com.fossil.blesdk.obfuscated.kc4) null);
        r12.L$Anon0 = r5;
        r12.L$Anon1 = r1;
        r12.L$Anon2 = r13;
        r12.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x037c, code lost:
        if (com.fossil.blesdk.obfuscated.kg4.a(r2, r3, r12) != r0) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x037e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0381, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.qx2.b) == false) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0383, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.u.a(), "GetAllContactGroup onError");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0394, code lost:
        r12.this$Anon0.m.h(r12.this$Anon0.k());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x03a5, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008d, code lost:
        if (r12.this$Anon0.l().isEmpty() == false) goto L_0x0274;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x008f, code lost:
        r13 = r12.this$Anon0.b();
        r5 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$getAppResponse$Anon1(r12, (com.fossil.blesdk.obfuscated.kc4) null);
        r12.L$Anon0 = r1;
        r12.label = 2;
        r13 = com.fossil.blesdk.obfuscated.kg4.a(r13, r5, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a3, code lost:
        if (r13 != r0) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a5, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a6, code lost:
        r13 = (com.portfolio.platform.CoroutineUseCase.c) r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00aa, code lost:
        if ((r13 instanceof com.fossil.blesdk.obfuscated.wy2.a) == false) goto L_0x0255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ac, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.u.a(), "GetApps onSuccess");
        r12.this$Anon0.m.a();
        r13 = ((com.fossil.blesdk.obfuscated.wy2.a) r13).a().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d4, code lost:
        if (r13.hasNext() == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d6, code lost:
        r5 = r13.next();
        r6 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion;
        r7 = r5.getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e2, code lost:
        if (r7 == null) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e4, code lost:
        r7 = r7.getIdentifier();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r7, "app.installedApp!!.identifier");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f1, code lost:
        if (r6.isPackageNameSupportedBySDK(r7) == false) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f3, code lost:
        r12.this$Anon0.l().add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00fd, code lost:
        com.fossil.blesdk.obfuscated.wd4.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0100, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0101, code lost:
        r13 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.values();
        r5 = r13.length;
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0107, code lost:
        if (r6 >= r5) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0109, code lost:
        r7 = r13[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010d, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0111, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0115, code lost:
        if (r7 == com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0126, code lost:
        if ((!com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r7.getPackageName(), (java.lang.Object) com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.FOSSIL.getPackageName())) == false) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0128, code lost:
        r8 = r12.this$Anon0.l().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0136, code lost:
        if (r8.hasNext() == false) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0138, code lost:
        r9 = r8.next();
        r10 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r9).getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0143, code lost:
        if (r10 == null) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0145, code lost:
        r10 = r10.getIdentifier();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x014a, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x015b, code lost:
        if (com.fossil.blesdk.obfuscated.pc4.a(com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r10, (java.lang.Object) r7.getPackageName())).booleanValue() == false) goto L_0x0132;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x015e, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0161, code lost:
        if (((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r9) != null) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0163, code lost:
        r8 = new com.portfolio.platform.data.model.InstalledApp(r7.getPackageName(), r7.getAppName(), com.fossil.blesdk.obfuscated.pc4.a(false));
        r9 = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper();
        r9.setInstalledApp(r8);
        r9.setUri((android.net.Uri) null);
        r9.setIconResourceId(r7.getIconResourceId());
        r9.setCurrentHandGroup(0);
        r12.this$Anon0.l().add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0192, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0196, code lost:
        r13 = r12.this$Anon0.l().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01a4, code lost:
        if (r13.hasNext() == false) goto L_0x01cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        r0 = (java.util.List) r12.L$Anon2;
        r0 = (com.portfolio.platform.CoroutineUseCase.c) r12.L$Anon1;
        r0 = (com.fossil.blesdk.obfuscated.lh4) r12.L$Anon0;
        com.fossil.blesdk.obfuscated.za4.a(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01a6, code lost:
        r5 = r13.next().getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01b0, code lost:
        if (r5 == null) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01b2, code lost:
        r6 = r5.isSelected();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r6, "it.isSelected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01bf, code lost:
        if (r6.booleanValue() == false) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01c1, code lost:
        r12.this$Anon0.h.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01d1, code lost:
        if (r12.this$Anon0.k() == false) goto L_0x01d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01d3, code lost:
        r12.this$Anon0.c(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01de, code lost:
        if (r12.this$Anon0.k() != false) goto L_0x021f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01e0, code lost:
        r13 = r12.this$Anon0.l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01e8, code lost:
        if ((r13 instanceof java.util.Collection) == false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01ee, code lost:
        if (r13.isEmpty() == false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01f0, code lost:
        r13 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01f2, code lost:
        r13 = r13.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01fa, code lost:
        if (r13.hasNext() == false) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01fc, code lost:
        r5 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) r13.next()).getInstalledApp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0206, code lost:
        if (r5 == null) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0208, code lost:
        r5 = r5.isSelected();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r5, "it.installedApp!!.isSelected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0215, code lost:
        if (r5.booleanValue() == false) goto L_0x01f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0217, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0219, code lost:
        com.fossil.blesdk.obfuscated.wd4.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x021c, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x021d, code lost:
        if (r13 == false) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x021f, code lost:
        r13 = r12.this$Anon0.m;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0225, code lost:
        if (r13 == null) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0227, code lost:
        r6 = ((com.fossil.blesdk.obfuscated.ew2) r13).getContext();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x022d, code lost:
        if (r6 == null) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0045, code lost:
        r5 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x022f, code lost:
        com.fossil.blesdk.obfuscated.pc4.a(com.fossil.blesdk.obfuscated.cn2.a(com.fossil.blesdk.obfuscated.cn2.d, r6, "NOTIFICATION_APPS", false, 4, (java.lang.Object) null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x023d, code lost:
        r12.this$Anon0.m.g(r12.this$Anon0.l());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0254, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0257, code lost:
        if ((r13 instanceof com.portfolio.platform.CoroutineUseCase.a) == false) goto L_0x027d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0259, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.u.a(), "GetApps onError");
        r12.this$Anon0.m.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0274, code lost:
        r12.this$Anon0.m.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x027d, code lost:
        r13 = r12.this$Anon0.b();
        r5 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1(r12, (com.fossil.blesdk.obfuscated.kc4) null);
        r12.L$Anon0 = r1;
        r12.label = 3;
        r13 = com.fossil.blesdk.obfuscated.kg4.a(r13, r5, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0291, code lost:
        if (r13 != r0) goto L_0x0045;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        switch (this.label) {
            case 0:
                za4.a(obj);
                lh4 = this.p$;
                if (!PortfolioApp.W.c().u().N()) {
                    gh4 a2 = this.this$Anon0.b();
                    Anon1 anon1 = new Anon1((kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    if (kg4.a(a2, anon1, this) == a) {
                        return a;
                    }
                }
                break;
            case 1:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 2:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 3:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 4:
                CoroutineUseCase.c cVar = (CoroutineUseCase.c) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 5:
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) this.L$Anon5;
                NotificationSettingsModel notificationSettingsModel2 = (NotificationSettingsModel) this.L$Anon4;
                List list = (List) this.L$Anon3;
                break;
            case 6:
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
