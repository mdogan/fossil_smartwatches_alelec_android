package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter$sendFeedback$Anon1 implements CoroutineUseCase.e<lr2.d, lr2.b> {
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAccountPresenter a;

    @DexIgnore
    public DeleteAccountPresenter$sendFeedback$Anon1(DeleteAccountPresenter deleteAccountPresenter) {
        this.a = deleteAccountPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(lr2.d dVar) {
        wd4.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "sendFeedback onSuccess");
        ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
        ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
        this.a.f.b(new DeleteAccountPresenter$sendFeedback$Anon1$onSuccess$configuration$Anon1(dVar));
    }

    @DexIgnore
    public void a(lr2.b bVar) {
        wd4.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "sendFeedback onError");
    }
}
