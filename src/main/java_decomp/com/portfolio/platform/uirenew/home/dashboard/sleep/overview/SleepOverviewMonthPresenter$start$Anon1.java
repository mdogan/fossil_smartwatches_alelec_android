package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qd3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewMonthPresenter$start$Anon1<T> implements dc<ps3<? extends List<MFSleepDay>>> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewMonthPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$Anon1$Anon1", f = "SleepOverviewMonthPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$Anon1$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$Anon1$Anon1$Anon1", f = "SleepOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0156Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super TreeMap<Long, Float>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0156Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0156Anon1 anon1 = new C0156Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0156Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    SleepOverviewMonthPresenter sleepOverviewMonthPresenter = this.this$Anon0.this$Anon0.a;
                    Object a = sleepOverviewMonthPresenter.f.a();
                    if (a != null) {
                        wd4.a(a, "mDate.value!!");
                        return sleepOverviewMonthPresenter.a((Date) a, (List<MFSleepDay>) this.this$Anon0.$data);
                    }
                    wd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepOverviewMonthPresenter$start$Anon1 sleepOverviewMonthPresenter$start$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = sleepOverviewMonthPresenter$start$Anon1;
            this.$data = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            SleepOverviewMonthPresenter sleepOverviewMonthPresenter;
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                this.this$Anon0.a.l = this.$data;
                SleepOverviewMonthPresenter sleepOverviewMonthPresenter2 = this.this$Anon0.a;
                gh4 a2 = sleepOverviewMonthPresenter2.b();
                C0156Anon1 anon1 = new C0156Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = sleepOverviewMonthPresenter2;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                sleepOverviewMonthPresenter = sleepOverviewMonthPresenter2;
            } else if (i == 1) {
                sleepOverviewMonthPresenter = (SleepOverviewMonthPresenter) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            sleepOverviewMonthPresenter.n = (TreeMap) obj;
            qd3 m = this.this$Anon0.a.o;
            TreeMap d = this.this$Anon0.a.n;
            if (d == null) {
                d = new TreeMap();
            }
            m.a(d);
            return cb4.a;
        }
    }

    @DexIgnore
    public SleepOverviewMonthPresenter$start$Anon1(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        this.a = sleepOverviewMonthPresenter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public final void a(ps3<? extends List<MFSleepDay>> ps3) {
        Integer num;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("mDateTransformations - status=");
        sb.append(ps3 != null ? ps3.f() : null);
        sb.append(" -- data.size=");
        if (ps3 != null) {
            List list = (List) ps3.d();
            if (list != null) {
                num = Integer.valueOf(list.size());
                sb.append(num);
                local.d("SleepOverviewMonthPresenter", sb.toString());
                if ((ps3 == null ? ps3.f() : null) == Status.DATABASE_LOADING) {
                    List list2 = ps3 != null ? (List) ps3.d() : null;
                    if (list2 != null && (!wd4.a((Object) this.a.l, (Object) list2))) {
                        ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list2, (kc4) null), 3, (Object) null);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        num = null;
        sb.append(num);
        local.d("SleepOverviewMonthPresenter", sb.toString());
        if ((ps3 == null ? ps3.f() : null) == Status.DATABASE_LOADING) {
        }
    }
}
