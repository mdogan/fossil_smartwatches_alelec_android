package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$Anon1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchSecondTimezonePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$Anon1(SearchSecondTimezonePresenter searchSecondTimezonePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = searchSecondTimezonePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SearchSecondTimezonePresenter$start$Anon1 searchSecondTimezonePresenter$start$Anon1 = new SearchSecondTimezonePresenter$start$Anon1(this.this$Anon0, kc4);
        searchSecondTimezonePresenter$start$Anon1.p$ = (lh4) obj;
        return searchSecondTimezonePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchSecondTimezonePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            SearchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1((kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, searchSecondTimezonePresenter$start$Anon1$rawDataList$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            ArrayList arrayList = (ArrayList) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            this.this$Anon0.g.addAll((ArrayList) obj);
            this.this$Anon0.i.q(this.this$Anon0.g);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList2 = (ArrayList) obj;
        gh4 a3 = this.this$Anon0.b();
        SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(arrayList2, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = arrayList2;
        this.label = 2;
        obj = kg4.a(a3, searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1, this);
        if (obj == a) {
            return a;
        }
        this.this$Anon0.g.addAll((ArrayList) obj);
        this.this$Anon0.i.q(this.this$Anon0.g);
        return cb4.a;
    }
}
