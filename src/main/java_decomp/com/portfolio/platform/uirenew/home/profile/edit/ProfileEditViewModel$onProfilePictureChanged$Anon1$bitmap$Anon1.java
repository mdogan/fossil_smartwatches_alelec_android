package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dk2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mv;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.po;
import com.fossil.blesdk.obfuscated.qp;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sv;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
public final class ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Bitmap>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Uri $imageUri;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(Uri uri, kc4 kc4) {
        super(2, kc4);
        this.$imageUri = uri;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(this.$imageUri, kc4);
        profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1.p$ = (lh4) obj;
        return profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return dk2.a((Context) PortfolioApp.W.c()).e().a(this.$imageUri).a((mv<?>) ((sv) ((sv) new sv().a(qp.a)).a(true)).a((po<Bitmap>) new ok2())).c(200, 200).get();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
