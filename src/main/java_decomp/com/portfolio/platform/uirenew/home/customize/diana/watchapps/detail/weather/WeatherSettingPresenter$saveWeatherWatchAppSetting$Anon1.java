package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1", f = "WeatherSettingPresenter.kt", l = {126}, m = "invokeSuspend")
public final class WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1(WeatherSettingPresenter weatherSettingPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = weatherSettingPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1(this.this$Anon0, kc4);
        weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1.p$ = (lh4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            WeatherWatchAppSetting d = this.this$Anon0.g;
            if (d != null) {
                gh4 a2 = this.this$Anon0.b();
                WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = d;
                this.label = 1;
                obj = kg4.a(a2, weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this);
                if (obj == a) {
                    return a;
                }
                weatherWatchAppSetting = d;
            }
            this.this$Anon0.i.a();
            this.this$Anon0.i.r(true);
            return cb4.a;
        } else if (i == 1) {
            weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        weatherWatchAppSetting.setLocations(wb4.d((List) obj));
        this.this$Anon0.i.a();
        this.this$Anon0.i.r(true);
        return cb4.a;
    }
}
