package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x03;
import com.fossil.blesdk.obfuscated.y03;
import com.fossil.blesdk.obfuscated.z03;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridEveryonePresenter extends x03 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public /* final */ List<ContactWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ y03 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> i;
    @DexIgnore
    public /* final */ k62 j;
    @DexIgnore
    public /* final */ u03 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridEveryonePresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = NotificationHybridEveryonePresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridEveryo\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridEveryonePresenter(y03 y03, int i2, ArrayList<ContactWrapper> arrayList, k62 k62, u03 u03) {
        wd4.b(y03, "mView");
        wd4.b(arrayList, "mContactWrappersSelected");
        wd4.b(k62, "mUseCaseHandler");
        wd4.b(u03, "mGetAllHybridContactGroups");
        this.g = y03;
        this.h = i2;
        this.i = arrayList;
        this.j = k62;
        this.k = u03;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public void i() {
        if (!this.f.isEmpty()) {
            boolean z = false;
            int size = this.f.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Contact contact = this.f.get(size).getContact();
                if (contact == null || contact.getContactId() != -100) {
                    size--;
                } else {
                    if (this.f.get(size).getCurrentHandGroup() != this.h) {
                        this.g.a(this.f.get(size));
                    } else {
                        this.f.remove(size);
                    }
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_CALLS.packageName;
                wd4.a((Object) str, "AppType.ALL_CALLS.packageName");
                this.f.add(a(-100, str));
            }
        } else {
            String str2 = AppType.ALL_CALLS.packageName;
            wd4.a((Object) str2, "AppType.ALL_CALLS.packageName");
            this.f.add(a(-100, str2));
        }
        this.g.b(this.f, this.h);
    }

    @DexIgnore
    public void j() {
        if (!this.f.isEmpty()) {
            boolean z = false;
            int size = this.f.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Contact contact = this.f.get(size).getContact();
                if (contact == null || contact.getContactId() != -200) {
                    size--;
                } else {
                    if (this.f.get(size).getCurrentHandGroup() != this.h) {
                        this.g.a(this.f.get(size));
                    } else {
                        this.f.remove(size);
                    }
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_SMS.packageName;
                wd4.a((Object) str, "AppType.ALL_SMS.packageName");
                this.f.add(a(-200, str));
            }
        } else {
            String str2 = AppType.ALL_SMS.packageName;
            wd4.a((Object) str2, "AppType.ALL_SMS.packageName");
            this.f.add(a(-200, str2));
        }
        this.g.b(this.f, this.h);
    }

    @DexIgnore
    public void k() {
        ArrayList arrayList = new ArrayList();
        for (ContactWrapper contactWrapper : this.f) {
            if (contactWrapper.isAdded() && contactWrapper.getCurrentHandGroup() == this.h) {
                arrayList.add(contactWrapper);
            }
        }
        this.g.a((ArrayList<ContactWrapper>) arrayList);
    }

    @DexIgnore
    public void l() {
        if (!PortfolioApp.W.c().u().N()) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationHybridEveryonePresenter$registerContactObserver$Anon1(this, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final List<ContactWrapper> m() {
        return this.f;
    }

    @DexIgnore
    public void n() {
        this.g.a(this);
    }

    @DexIgnore
    public void a(ContactWrapper contactWrapper) {
        wd4.b(contactWrapper, "contactWrapper");
        for (ContactWrapper contactWrapper2 : this.f) {
            Contact contact = contactWrapper2.getContact();
            Integer num = null;
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (wd4.a((Object) valueOf, (Object) num)) {
                contactWrapper2.setCurrentHandGroup(this.h);
                contactWrapper2.setAdded(true);
            }
        }
        this.g.b(this.f, this.h);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        cn2 cn2 = cn2.d;
        y03 y03 = this.g;
        if (y03 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneFragment");
        } else if (cn2.a(cn2, ((z03) y03).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationHybridEveryonePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final ContactWrapper a(int i2, String str) {
        Contact contact = new Contact();
        contact.setContactId(i2);
        contact.setFirstName(str);
        ContactWrapper contactWrapper = new ContactWrapper(contact, (String) null, 2, (rd4) null);
        contactWrapper.setHasPhoneNumber(true);
        contactWrapper.setAdded(true);
        contactWrapper.setCurrentHandGroup(this.h);
        if (i2 == -100) {
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                contact2.setUseCall(true);
            }
            Contact contact3 = contactWrapper.getContact();
            if (contact3 != null) {
                contact3.setUseSms(false);
            }
        } else {
            Contact contact4 = contactWrapper.getContact();
            if (contact4 != null) {
                contact4.setUseCall(false);
            }
            Contact contact5 = contactWrapper.getContact();
            if (contact5 != null) {
                contact5.setUseSms(true);
            }
        }
        return contactWrapper;
    }
}
