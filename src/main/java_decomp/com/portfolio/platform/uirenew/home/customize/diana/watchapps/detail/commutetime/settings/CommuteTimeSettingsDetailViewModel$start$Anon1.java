package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.android.libraries.places.api.Places;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$Anon1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
public final class CommuteTimeSettingsDetailViewModel$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDetailViewModel$start$Anon1(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeSettingsDetailViewModel;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeSettingsDetailViewModel$start$Anon1 commuteTimeSettingsDetailViewModel$start$Anon1 = new CommuteTimeSettingsDetailViewModel$start$Anon1(this.this$Anon0, kc4);
        commuteTimeSettingsDetailViewModel$start$Anon1.p$ = (lh4) obj;
        return commuteTimeSettingsDetailViewModel$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDetailViewModel$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.c = Places.createClient(PortfolioApp.W.c());
            AddressWrapper a = this.this$Anon0.d;
            Boolean bool = null;
            boolean z = (a != null ? a.getType() : null) == AddressWrapper.AddressType.OTHER;
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.this$Anon0;
            AddressWrapper a2 = commuteTimeSettingsDetailViewModel.d;
            String name = a2 != null ? a2.getName() : null;
            Boolean a3 = pc4.a(z);
            AddressWrapper a4 = this.this$Anon0.d;
            String address = a4 != null ? a4.getAddress() : null;
            AddressWrapper a5 = this.this$Anon0.d;
            if (a5 != null) {
                bool = pc4.a(a5.getAvoidTolls());
            }
            CommuteTimeSettingsDetailViewModel.a(commuteTimeSettingsDetailViewModel, name, address, bool, a3, (Boolean) null, this.this$Anon0.c, (Boolean) null, 80, (Object) null);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
