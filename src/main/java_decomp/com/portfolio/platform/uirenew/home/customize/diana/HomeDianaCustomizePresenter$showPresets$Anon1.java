package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g13;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {333}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$Anon1(HomeDianaCustomizePresenter homeDianaCustomizePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDianaCustomizePresenter$showPresets$Anon1 homeDianaCustomizePresenter$showPresets$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1(this.this$Anon0, kc4);
        homeDianaCustomizePresenter$showPresets$Anon1.p$ = (lh4) obj;
        return homeDianaCustomizePresenter$showPresets$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$showPresets$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 c = this.this$Anon0.b();
            HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(c, homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.t.c((List<g13>) (List) obj);
        return cb4.a;
    }
}
