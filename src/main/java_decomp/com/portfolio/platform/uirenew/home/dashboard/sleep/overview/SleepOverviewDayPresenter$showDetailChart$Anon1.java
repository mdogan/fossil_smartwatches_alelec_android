package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$Anon1", f = "SleepOverviewDayPresenter.kt", l = {159}, m = "invokeSuspend")
public final class SleepOverviewDayPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewDayPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepOverviewDayPresenter$showDetailChart$Anon1(SleepOverviewDayPresenter sleepOverviewDayPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepOverviewDayPresenter$showDetailChart$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1 = new SleepOverviewDayPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        sleepOverviewDayPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return sleepOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepOverviewDayPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ps3 ps3 = (ps3) this.this$Anon0.j.a();
            if (ps3 != null) {
                List list = (List) ps3.d();
                if (list != null) {
                    gh4 a2 = this.this$Anon0.b();
                    SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(list, (kc4) null, this);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = list;
                    this.label = 1;
                    obj = kg4.a(a2, sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                }
            }
            this.this$Anon0.k.s(new ArrayList());
            return cb4.a;
        } else if (i == 1) {
            List list2 = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list3 = (List) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewDayPresenter", "showDetailChart - data=" + list3);
        this.this$Anon0.k.s(list3);
        return cb4.a;
    }
}
