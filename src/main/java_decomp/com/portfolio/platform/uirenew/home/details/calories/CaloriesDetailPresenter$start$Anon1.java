package com.portfolio.platform.uirenew.home.details.calories;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.we3;
import com.fossil.blesdk.obfuscated.xe3;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.enums.Unit;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1", f = "CaloriesDetailPresenter.kt", l = {116}, m = "invokeSuspend")
public final class CaloriesDetailPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Unit>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                MFUser currentUser = this.this$Anon0.this$Anon0.v.getCurrentUser();
                if (currentUser != null) {
                    Unit distanceUnit = currentUser.getDistanceUnit();
                    if (distanceUnit != null) {
                        return distanceUnit;
                    }
                }
                return Unit.METRIC;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements dc<ps3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon2$Anon1", f = "CaloriesDetailPresenter.kt", l = {130}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon2;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("XXX", "find todaySummary of " + this.this$Anon0.a.this$Anon0.k);
                    gh4 a2 = this.this$Anon0.a.this$Anon0.b();
                    CaloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 caloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 = new CaloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = kg4.a(a2, caloriesDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ActivitySummary activitySummary = (ActivitySummary) obj;
                if (this.this$Anon0.a.this$Anon0.m == null || (!wd4.a((Object) this.this$Anon0.a.this$Anon0.m, (Object) activitySummary))) {
                    this.this$Anon0.a.this$Anon0.m = activitySummary;
                    this.this$Anon0.a.this$Anon0.s.a(this.this$Anon0.a.this$Anon0.o, this.this$Anon0.a.this$Anon0.m);
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        ri4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon2(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1) {
            this.a = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<ActivitySummary>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.k = list;
                this.a.this$Anon0.i = true;
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3<T> implements dc<ps3<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon3$Anon1", f = "CaloriesDetailPresenter.kt", l = {152}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon3 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon3 anon3, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon3;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    gh4 a2 = this.this$Anon0.a.this$Anon0.b();
                    CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 = new CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = kg4.a(a2, caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (this.this$Anon0.a.this$Anon0.n == null || (!wd4.a((Object) this.this$Anon0.a.this$Anon0.n, (Object) list))) {
                    this.this$Anon0.a.this$Anon0.n = list;
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        ri4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon3(CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1) {
            this.a = caloriesDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<ActivitySample>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sampleTransformations -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.l = list;
                this.a.this$Anon0.j = true;
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$start$Anon1(CaloriesDetailPresenter caloriesDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = caloriesDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CaloriesDetailPresenter$start$Anon1 caloriesDetailPresenter$start$Anon1 = new CaloriesDetailPresenter$start$Anon1(this.this$Anon0, kc4);
        caloriesDetailPresenter$start$Anon1.p$ = (lh4) obj;
        return caloriesDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesDetailPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        CaloriesDetailPresenter caloriesDetailPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            CaloriesDetailPresenter caloriesDetailPresenter2 = this.this$Anon0;
            gh4 a2 = caloriesDetailPresenter2.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = caloriesDetailPresenter2;
            this.label = 1;
            obj = kg4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            caloriesDetailPresenter = caloriesDetailPresenter2;
        } else if (i == 1) {
            caloriesDetailPresenter = (CaloriesDetailPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        caloriesDetailPresenter.o = (Unit) obj;
        LiveData q = this.this$Anon0.p;
        we3 o = this.this$Anon0.s;
        if (o != null) {
            q.a((xe3) o, new Anon2(this));
            this.this$Anon0.q.a((LifecycleOwner) this.this$Anon0.s, new Anon3(this));
            return cb4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
    }
}
