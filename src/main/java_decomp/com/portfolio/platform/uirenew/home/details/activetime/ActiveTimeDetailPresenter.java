package com.portfolio.platform.uirenew.home.details.activetime;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ce3;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.de3;
import com.fossil.blesdk.obfuscated.df4;
import com.fossil.blesdk.obfuscated.ee3;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeDetailPresenter extends ce3 implements PagingRequestHelper.a {
    @DexIgnore
    public /* final */ i42 A;
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public List<ActivitySummary> k; // = new ArrayList();
    @DexIgnore
    public List<ActivitySample> l; // = new ArrayList();
    @DexIgnore
    public ActivitySummary m;
    @DexIgnore
    public List<ActivitySample> n;
    @DexIgnore
    public Unit o; // = Unit.METRIC;
    @DexIgnore
    public LiveData<ps3<List<ActivitySummary>>> p;
    @DexIgnore
    public LiveData<ps3<List<ActivitySample>>> q;
    @DexIgnore
    public Listing<WorkoutSession> r;
    @DexIgnore
    public /* final */ de3 s;
    @DexIgnore
    public /* final */ SummariesRepository t;
    @DexIgnore
    public /* final */ ActivitiesRepository u;
    @DexIgnore
    public /* final */ UserRepository v;
    @DexIgnore
    public /* final */ WorkoutSessionRepository w;
    @DexIgnore
    public /* final */ FitnessDataDao x;
    @DexIgnore
    public /* final */ WorkoutDao y;
    @DexIgnore
    public /* final */ FitnessDatabase z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<rd<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeDetailPresenter a;

        @DexIgnore
        public b(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            this.a = activeTimeDetailPresenter;
        }

        @DexIgnore
        public final void a(rd<WorkoutSession> rdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeDetailPresenter", "getWorkoutSessionsPaging observed size = " + rdVar.size());
            if (DeviceHelper.o.g(PortfolioApp.W.c().e())) {
                wd4.a((Object) rdVar, "pageList");
                if (wb4.d(rdVar).isEmpty()) {
                    this.a.s.a(false, this.a.o, rdVar);
                    return;
                }
            }
            de3 o = this.a.s;
            Unit h = this.a.o;
            wd4.a((Object) rdVar, "pageList");
            o.a(true, h, rdVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeDetailPresenter a;

        @DexIgnore
        public c(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            this.a = activeTimeDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<ActivitySample>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.u.getActivityList((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeDetailPresenter a;

        @DexIgnore
        public d(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            this.a = activeTimeDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<ActivitySummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.t.getSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public ActiveTimeDetailPresenter(de3 de3, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, i42 i42) {
        wd4.b(de3, "mView");
        wd4.b(summariesRepository, "mSummariesRepository");
        wd4.b(activitiesRepository, "mActivitiesRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        wd4.b(workoutDao, "mWorkoutDao");
        wd4.b(fitnessDatabase, "mWorkoutDatabase");
        wd4.b(i42, "appExecutors");
        this.s = de3;
        this.t = summariesRepository;
        this.u = activitiesRepository;
        this.v = userRepository;
        this.w = workoutSessionRepository;
        this.x = fitnessDataDao;
        this.y = workoutDao;
        this.z = fitnessDatabase;
        this.A = i42;
        LiveData<ps3<List<ActivitySummary>>> b2 = ic.b(this.h, new d(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.p = b2;
        LiveData<ps3<List<ActivitySample>>> b3 = ic.b(this.h, new c(this));
        wd4.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.q = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ActiveTimeDetailPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        LiveData<ps3<List<ActivitySummary>>> liveData = this.p;
        de3 de3 = this.s;
        if (de3 != null) {
            liveData.a((ee3) de3, new ActiveTimeDetailPresenter$start$Anon2(this));
            this.q.a((LifecycleOwner) this.s, new ActiveTimeDetailPresenter$start$Anon3(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeDetailPresenter", "stop");
        LiveData<ps3<List<ActivitySummary>>> liveData = this.p;
        de3 de3 = this.s;
        if (de3 != null) {
            liveData.a((LifecycleOwner) (ee3) de3);
            this.q.a((LifecycleOwner) this.s);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<WorkoutSession> listing = this.r;
            if (listing != null) {
                LiveData<rd<WorkoutSession>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    de3 de3 = this.s;
                    if (de3 != null) {
                        pagedList.a((LifecycleOwner) (ee3) de3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailFragment");
                    }
                }
            }
            this.w.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("ActiveTimeDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        Date l2 = sk2.l(this.g);
        wd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        b(l2);
    }

    @DexIgnore
    public void j() {
        Date m2 = sk2.m(this.g);
        wd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void k() {
        this.s.a(this);
    }

    @DexIgnore
    public final ri4 l() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActiveTimeDetailPresenter$showDetailChart$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        h();
        WorkoutSessionRepository workoutSessionRepository = this.w;
        this.r = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.x, this.y, this.z, this.A, this);
        Listing<WorkoutSession> listing = this.r;
        if (listing != null) {
            LiveData<rd<WorkoutSession>> pagedList = listing.getPagedList();
            de3 de3 = this.s;
            if (de3 != null) {
                pagedList.a((ee3) de3, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailFragment");
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActiveTimeDetailPresenter$setDate$Anon1(this, date, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ActivitySummary b(Date date, List<ActivitySummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (sk2.d(((ActivitySummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (ActivitySummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<ActivitySample> a(Date date, List<ActivitySample> list) {
        if (list != null) {
            df4<T> b2 = wb4.b(list);
            if (b2 != null) {
                df4<T> a2 = SequencesKt___SequencesKt.a(b2, new ActiveTimeDetailPresenter$findActivitySamples$Anon1(date));
                if (a2 != null) {
                    return SequencesKt___SequencesKt.g(a2);
                }
            }
        }
        return null;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        FLogger.INSTANCE.getLocal().d("ActiveTimeDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.r;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }
}
