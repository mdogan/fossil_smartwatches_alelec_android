package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.format.DateFormat;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.vx2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$Anon1", f = "InactivityNudgeTimePresenter.kt", l = {35}, m = "invokeSuspend")
public final class InactivityNudgeTimePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ InactivityNudgeTimePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InactivityNudgeTimePresenter$start$Anon1(InactivityNudgeTimePresenter inactivityNudgeTimePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        InactivityNudgeTimePresenter$start$Anon1 inactivityNudgeTimePresenter$start$Anon1 = new InactivityNudgeTimePresenter$start$Anon1(this.this$Anon0, kc4);
        inactivityNudgeTimePresenter$start$Anon1.p$ = (lh4) obj;
        return inactivityNudgeTimePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((InactivityNudgeTimePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            InactivityNudgeTimePresenter$start$Anon1$listInactivityNudgeTimeModel$Anon1 inactivityNudgeTimePresenter$start$Anon1$listInactivityNudgeTimeModel$Anon1 = new InactivityNudgeTimePresenter$start$Anon1$listInactivityNudgeTimeModel$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, inactivityNudgeTimePresenter$start$Anon1$listInactivityNudgeTimeModel$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        for (InactivityNudgeTimeModel inactivityNudgeTimeModel : (List) obj) {
            if (inactivityNudgeTimeModel.getNudgeTimeType() == this.this$Anon0.g) {
                boolean is24HourFormat = DateFormat.is24HourFormat(PortfolioApp.W.c());
                this.this$Anon0.j.J(is24HourFormat);
                vx2 f = this.this$Anon0.j;
                InactivityNudgeTimePresenter inactivityNudgeTimePresenter = this.this$Anon0;
                f.d(inactivityNudgeTimePresenter.b(inactivityNudgeTimePresenter.g));
                this.this$Anon0.j.a(inactivityNudgeTimeModel.getMinutes(), is24HourFormat);
            }
            if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                this.this$Anon0.h = inactivityNudgeTimeModel;
            } else {
                this.this$Anon0.i = inactivityNudgeTimeModel;
            }
        }
        return cb4.a;
    }
}
