package com.portfolio.platform.uirenew.home.customize.diana.theme;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.j43;
import com.fossil.blesdk.obfuscated.k43;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemePresenter extends j43 {
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public LiveData<List<WatchFace>> g;
    @DexIgnore
    public List<WatchFace> h; // = new ArrayList();
    @DexIgnore
    public /* final */ k43 i;
    @DexIgnore
    public /* final */ WatchFaceRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter a;

        @DexIgnore
        public b(CustomizeThemePresenter customizeThemePresenter) {
            this.a = customizeThemePresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                this.a.i.n(CustomizeThemePresenter.b(this.a).k().indexOf(CustomizeThemePresenter.b(this.a).f(str)));
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public CustomizeThemePresenter(k43 k43, WatchFaceRepository watchFaceRepository) {
        wd4.b(k43, "mView");
        wd4.b(watchFaceRepository, "watchFaceRepository");
        this.i = k43;
        this.j = watchFaceRepository;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel b(CustomizeThemePresenter customizeThemePresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = customizeThemePresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> h2 = dianaCustomizeViewModel.h();
            k43 k43 = this.i;
            if (k43 != null) {
                h2.a((LifecycleOwner) (CustomizeThemeFragment) k43);
                LiveData<List<WatchFace>> liveData = this.g;
                if (liveData != null) {
                    liveData.a((LifecycleOwner) this.i);
                    return;
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "onStart");
        ri4 unused = mg4.b(e(), zh4.b(), (CoroutineStart) null, new CustomizeThemePresenter$start$Anon1(this, (kc4) null), 2, (Object) null);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> h2 = dianaCustomizeViewModel.h();
            k43 k43 = this.i;
            if (k43 != null) {
                h2.a((CustomizeThemeFragment) k43, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        wd4.b(watchFaceWrapper, "watchFaceWrapper");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                a2.setWatchFaceId(watchFaceWrapper.getId());
                wd4.a((Object) a2, "currentPreset");
                a(a2);
                return;
            }
            return;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            wd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        wd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }
}
