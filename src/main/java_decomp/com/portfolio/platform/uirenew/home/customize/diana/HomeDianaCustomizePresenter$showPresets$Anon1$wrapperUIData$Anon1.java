package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g13;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {394}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends g13>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter$showPresets$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(HomeDianaCustomizePresenter$showPresets$Anon1 homeDianaCustomizePresenter$showPresets$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDianaCustomizePresenter$showPresets$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(this.this$Anon0, kc4);
        homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1.p$ = (lh4) obj;
        return homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        pl4 pl4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            pl4 l = this.this$Anon0.this$Anon0.q;
            this.L$Anon0 = lh4;
            this.L$Anon1 = l;
            this.label = 1;
            if (l.a((Object) null, this) == a) {
                return a;
            }
            pl4 = l;
        } else if (i == 1) {
            pl4 = (pl4) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            CopyOnWriteArrayList<DianaPreset> m = this.this$Anon0.this$Anon0.j;
            ArrayList arrayList = new ArrayList(pb4.a(m, 10));
            for (DianaPreset dianaPreset : m) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0.this$Anon0;
                wd4.a((Object) dianaPreset, "it");
                arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
            }
            return arrayList;
        } finally {
            pl4.a((Object) null);
        }
    }
}
