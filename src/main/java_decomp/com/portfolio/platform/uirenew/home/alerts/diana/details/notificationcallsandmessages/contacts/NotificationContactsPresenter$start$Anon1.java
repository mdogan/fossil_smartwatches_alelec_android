package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
public final class NotificationContactsPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1$Anon1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon1(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                PortfolioApp.W.c().J();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 implements CoroutineUseCase.e<qx2.d, qx2.b> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter$start$Anon1 a;

        @DexIgnore
        public Anon2(NotificationContactsPresenter$start$Anon1 notificationContactsPresenter$start$Anon1) {
            this.a = notificationContactsPresenter$start$Anon1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qx2.d dVar) {
            wd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), "GetAllContactGroup onSuccess");
            ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1(this, dVar, (kc4) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(qx2.b bVar) {
            wd4.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), "GetAllContactGroup onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsPresenter$start$Anon1(NotificationContactsPresenter notificationContactsPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationContactsPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationContactsPresenter$start$Anon1 notificationContactsPresenter$start$Anon1 = new NotificationContactsPresenter$start$Anon1(this.this$Anon0, kc4);
        notificationContactsPresenter$start$Anon1.p$ = (lh4) obj;
        return notificationContactsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (!PortfolioApp.W.c().u().N()) {
                gh4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (kg4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$Anon0.j().isEmpty()) {
            this.this$Anon0.j.a(null, new Anon2(this));
        }
        return cb4.a;
    }
}
