package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.graphics.RectF;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.ub3;
import com.fossil.blesdk.obfuscated.vb3;
import com.fossil.blesdk.obfuscated.wb3;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewWeekPresenter extends ub3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<ps3<List<GoalTrackingSummary>>> g; // = new MutableLiveData();
    @DexIgnore
    public BarChart.c h;
    @DexIgnore
    public /* final */ vb3 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ fn2 k;
    @DexIgnore
    public /* final */ GoalTrackingRepository l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewWeekPresenter(vb3 vb3, UserRepository userRepository, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        wd4.b(vb3, "mView");
        wd4.b(userRepository, "userRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.i = vb3;
        this.j = userRepository;
        this.k = fn2;
        this.l = goalTrackingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
        Date date = goalTrackingOverviewWeekPresenter.f;
        if (date != null) {
            return date;
        }
        wd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingOverviewWeekPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "stop");
        try {
            LiveData<ps3<List<GoalTrackingSummary>>> liveData = this.g;
            vb3 vb3 = this.i;
            if (vb3 != null) {
                liveData.a((LifecycleOwner) (wb3) vb3);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewWeekPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<GoalTrackingSummary> list) {
        int i2;
        T t;
        Date date2 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date2);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (rd4) null);
        Calendar instance = Calendar.getInstance(Locale.US);
        wd4.a((Object) instance, "calendar");
        instance.setTime(date2);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            wd4.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (sk2.d(((GoalTrackingSummary) t).getDate(), instance.getTime())) {
                        break;
                    }
                }
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) t;
                if (goalTrackingSummary != null) {
                    int goalTarget = goalTrackingSummary.getGoalTarget();
                    int totalTracked = goalTrackingSummary.getTotalTracked();
                    i4 = Math.max(Math.max(goalTarget, totalTracked), i4);
                    cVar.a().add(new BarChart.a(goalTarget, ob4.a((T[]) new ArrayList[]{ob4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, totalTracked, (RectF) null, 23, (rd4) null)})}), time2, i3 == 6));
                    i5 = goalTarget;
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, ob4.a((T[]) new ArrayList[]{ob4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (rd4) null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, ob4.a((T[]) new ArrayList[]{ob4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (rd4) null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 > 0) {
            i5 = i4;
        }
        cVar.b(i5);
        return cVar;
    }

    @DexIgnore
    public final Pair<Date, Date> a(Date date) {
        Date b = sk2.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = sk2.d(currentUser.getCreatedAt());
            if (!sk2.b(b, d)) {
                b = d;
            }
        }
        return new Pair<>(b, date);
    }
}
