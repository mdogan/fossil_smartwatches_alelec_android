package com.portfolio.platform.uirenew.home.details.heartrate;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.df4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.nf3;
import com.fossil.blesdk.obfuscated.of3;
import com.fossil.blesdk.obfuscated.pf3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDetailPresenter extends nf3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public List<DailyHeartRateSummary> i; // = new ArrayList();
    @DexIgnore
    public List<HeartRateSample> j; // = new ArrayList();
    @DexIgnore
    public DailyHeartRateSummary k;
    @DexIgnore
    public List<HeartRateSample> l;
    @DexIgnore
    public Unit m; // = Unit.METRIC;
    @DexIgnore
    public LiveData<ps3<List<DailyHeartRateSummary>>> n;
    @DexIgnore
    public LiveData<ps3<List<HeartRateSample>>> o;
    @DexIgnore
    public Listing<WorkoutSession> p;
    @DexIgnore
    public /* final */ of3 q;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository r;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ UserRepository t;
    @DexIgnore
    public /* final */ WorkoutSessionRepository u;
    @DexIgnore
    public /* final */ FitnessDataDao v;
    @DexIgnore
    public /* final */ WorkoutDao w;
    @DexIgnore
    public /* final */ FitnessDatabase x;
    @DexIgnore
    public /* final */ i42 y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<rd<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public b(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        public final void a(rd<WorkoutSession> rdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + rdVar.size());
            if (DeviceHelper.o.g(PortfolioApp.W.c().e())) {
                wd4.a((Object) rdVar, "pageList");
                if (wb4.d(rdVar).isEmpty()) {
                    this.a.q.a(false, this.a.m, rdVar);
                    return;
                }
            }
            of3 m = this.a.q;
            Unit c = this.a.m;
            wd4.a((Object) rdVar, "pageList");
            m.a(true, c, rdVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public c(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<HeartRateSample>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.s.getHeartRateSamples((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public d(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<DailyHeartRateSummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.r.getHeartRateSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HeartRateDetailPresenter(of3 of3, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, i42 i42) {
        wd4.b(of3, "mView");
        wd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        wd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        wd4.b(workoutDao, "mWorkoutDao");
        wd4.b(fitnessDatabase, "mWorkoutDatabase");
        wd4.b(i42, "appExecutors");
        this.q = of3;
        this.r = heartRateSummaryRepository;
        this.s = heartRateSampleRepository;
        this.t = userRepository;
        this.u = workoutSessionRepository;
        this.v = fitnessDataDao;
        this.w = workoutDao;
        this.x = fitnessDatabase;
        this.y = i42;
        LiveData<ps3<List<DailyHeartRateSummary>>> b2 = ic.b(this.h, new d(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.n = b2;
        LiveData<ps3<List<HeartRateSample>>> b3 = ic.b(this.h, new c(this));
        wd4.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.o = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<ps3<List<HeartRateSample>>> liveData = this.o;
        of3 of3 = this.q;
        if (of3 != null) {
            liveData.a((LifecycleOwner) (pf3) of3);
            this.n.a((LifecycleOwner) this.q);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<WorkoutSession> listing = this.p;
            if (listing != null) {
                LiveData<rd<WorkoutSession>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    of3 of3 = this.q;
                    if (of3 != null) {
                        pagedList.a((LifecycleOwner) (pf3) of3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                    }
                }
            }
            this.u.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        Date l2 = sk2.l(this.g);
        wd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        b(l2);
    }

    @DexIgnore
    public void j() {
        Date m2 = sk2.m(this.g);
        wd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void k() {
        this.q.a(this);
    }

    @DexIgnore
    public final ri4 l() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$showDayDetail$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ri4 m() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$showDetailChart$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        h();
        WorkoutSessionRepository workoutSessionRepository = this.u;
        this.p = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.v, this.w, this.x, this.y, this);
        Listing<WorkoutSession> listing = this.p;
        if (listing != null) {
            LiveData<rd<WorkoutSession>> pagedList = listing.getPagedList();
            of3 of3 = this.q;
            if (of3 != null) {
                pagedList.a((pf3) of3, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$setDate$Anon1(this, date, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final DailyHeartRateSummary b(Date date, List<DailyHeartRateSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (sk2.d(((DailyHeartRateSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (DailyHeartRateSummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<HeartRateSample> a(Date date, List<HeartRateSample> list) {
        if (list != null) {
            df4<T> b2 = wb4.b(list);
            if (b2 != null) {
                df4<T> a2 = SequencesKt___SequencesKt.a(b2, new HeartRateDetailPresenter$findHeartRateSamples$Anon1(date));
                if (a2 != null) {
                    return SequencesKt___SequencesKt.g(a2);
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        be4 be4 = be4.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.p;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }
}
