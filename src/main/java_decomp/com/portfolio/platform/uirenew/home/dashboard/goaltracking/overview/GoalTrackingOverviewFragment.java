package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.hb3;
import com.fossil.blesdk.obfuscated.lb3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.rb3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.wb3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.blesdk.obfuscated.zb2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewFragment extends as2 {
    @DexIgnore
    public ur3<zb2> j;
    @DexIgnore
    public GoalTrackingOverviewDayPresenter k;
    @DexIgnore
    public GoalTrackingOverviewWeekPresenter l;
    @DexIgnore
    public GoalTrackingOverviewMonthPresenter m;
    @DexIgnore
    public hb3 n;
    @DexIgnore
    public wb3 o;
    @DexIgnore
    public rb3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            ur3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(7, a != null ? (zb2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public c(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            ur3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(4, a != null ? (zb2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public d(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            ur3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(2, a != null ? (zb2) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onCreateView");
        zb2 zb2 = (zb2) ra.a(layoutInflater, R.layout.fragment_goal_tracking_overview, viewGroup, false, O0());
        g9.c((View) zb2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) zb2, "binding");
        a(zb2);
        this.j = new ur3<>(this, zb2);
        ur3<zb2> ur3 = this.j;
        if (ur3 != null) {
            zb2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(zb2 zb2) {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "initUI");
        this.n = (hb3) getChildFragmentManager().a("GoalTrackingOverviewDayFragment");
        this.o = (wb3) getChildFragmentManager().a("GoalTrackingOverviewWeekFragment");
        this.p = (rb3) getChildFragmentManager().a("GoalTrackingOverviewMonthFragment");
        if (this.n == null) {
            this.n = new hb3();
        }
        if (this.o == null) {
            this.o = new wb3();
        }
        if (this.p == null) {
            this.p = new rb3();
        }
        ArrayList arrayList = new ArrayList();
        hb3 hb3 = this.n;
        if (hb3 != null) {
            arrayList.add(hb3);
            wb3 wb3 = this.o;
            if (wb3 != null) {
                arrayList.add(wb3);
                rb3 rb3 = this.p;
                if (rb3 != null) {
                    arrayList.add(rb3);
                    RecyclerView recyclerView = zb2.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new GoalTrackingOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, zb2);
                    m42 g = PortfolioApp.W.c().g();
                    hb3 hb32 = this.n;
                    if (hb32 != null) {
                        wb3 wb32 = this.o;
                        if (wb32 != null) {
                            rb3 rb32 = this.p;
                            if (rb32 != null) {
                                g.a(new lb3(hb32, wb32, rb32)).a(this);
                                zb2.s.setOnClickListener(new b(this));
                                zb2.q.setOnClickListener(new c(this));
                                zb2.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, zb2 zb2) {
        if (zb2 != null) {
            FlexibleTextView flexibleTextView = zb2.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = zb2.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = zb2.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = zb2.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = zb2.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = zb2.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = zb2.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = zb2.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = zb2.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<zb2> ur3 = this.j;
                if (ur3 != null) {
                    zb2 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = zb2.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = zb2.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = zb2.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<zb2> ur32 = this.j;
                if (ur32 != null) {
                    zb2 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = zb2.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = zb2.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = zb2.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<zb2> ur33 = this.j;
                if (ur33 != null) {
                    zb2 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = zb2.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = zb2.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = zb2.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<zb2> ur34 = this.j;
                if (ur34 != null) {
                    zb2 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
