package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fg3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1", f = "ActivityOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityOverviewDayPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(ActivityOverviewDayPresenter$showDetailChart$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activityOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(this.this$Anon0, kc4);
        activityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1.p$ = (lh4) obj;
        return activityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            fg3 fg3 = fg3.a;
            Date d = ActivityOverviewDayPresenter.d(this.this$Anon0.this$Anon0);
            ps3 ps3 = (ps3) this.this$Anon0.this$Anon0.j.a();
            return fg3.a(d, (List<ActivitySample>) ps3 != null ? (List) ps3.d() : null, 0);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
