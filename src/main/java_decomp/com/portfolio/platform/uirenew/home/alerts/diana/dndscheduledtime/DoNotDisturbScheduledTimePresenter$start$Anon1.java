package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.ty2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
public final class DoNotDisturbScheduledTimePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DoNotDisturbScheduledTimePresenter$start$Anon1(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = doNotDisturbScheduledTimePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DoNotDisturbScheduledTimePresenter$start$Anon1 doNotDisturbScheduledTimePresenter$start$Anon1 = new DoNotDisturbScheduledTimePresenter$start$Anon1(this.this$Anon0, kc4);
        doNotDisturbScheduledTimePresenter$start$Anon1.p$ = (lh4) obj;
        return doNotDisturbScheduledTimePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DoNotDisturbScheduledTimePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            DoNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1 doNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1 = new DoNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, doNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) obj) {
            if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$Anon0.g) {
                ty2 g = this.this$Anon0.j;
                DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter = this.this$Anon0;
                g.d(doNotDisturbScheduledTimePresenter.b(doNotDisturbScheduledTimePresenter.g));
                this.this$Anon0.j.a(dNDScheduledTimeModel.getMinutes());
            }
            if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                this.this$Anon0.h = dNDScheduledTimeModel;
            } else {
                this.this$Anon0.i = dNDScheduledTimeModel;
            }
        }
        return cb4.a;
    }
}
