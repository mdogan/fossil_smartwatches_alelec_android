package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$Anon1", f = "MicroAppPresenter.kt", l = {232}, m = "invokeSuspend")
public final class MicroAppPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppPresenter$start$Anon1(MicroAppPresenter microAppPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = microAppPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MicroAppPresenter$start$Anon1 microAppPresenter$start$Anon1 = new MicroAppPresenter$start$Anon1(this.this$Anon0, kc4);
        microAppPresenter$start$Anon1.p$ = (lh4) obj;
        return microAppPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MicroAppPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            MicroAppPresenter$start$Anon1$allCategory$Anon1 microAppPresenter$start$Anon1$allCategory$Anon1 = new MicroAppPresenter$start$Anon1$allCategory$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, microAppPresenter$start$Anon1$allCategory$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList = new ArrayList();
        for (Category category : (List) obj) {
            if (!MicroAppPresenter.f(this.this$Anon0).b(category.getId()).isEmpty()) {
                arrayList.add(category);
            }
        }
        this.this$Anon0.g.addAll(arrayList);
        this.this$Anon0.t.a(this.this$Anon0.g);
        return cb4.a;
    }
}
