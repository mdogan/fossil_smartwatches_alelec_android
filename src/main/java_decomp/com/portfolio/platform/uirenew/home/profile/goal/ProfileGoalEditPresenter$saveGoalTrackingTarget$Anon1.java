package com.portfolio.platform.uirenew.home.profile.goal;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wh3;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1", f = "ProfileGoalEditPresenter.kt", l = {58}, m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1$Anon1", f = "ProfileGoalEditPresenter.kt", l = {59}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.UpdateGoalSettingCallback {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public a(Anon1 anon1) {
                this.a = anon1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x001d, code lost:
                if (r3 != null) goto L_0x0022;
             */
            @DexIgnore
            public void onFail(qo2<GoalSetting> qo2) {
                String str;
                wd4.b(qo2, "error");
                wh3 m = this.a.this$Anon0.this$Anon0.m();
                int a2 = qo2.a();
                ServerError c = qo2.c();
                if (c != null) {
                    str = c.getMessage();
                }
                str = "";
                m.d(a2, str);
                this.a.this$Anon0.this$Anon0.o();
            }

            @DexIgnore
            public void onSuccess(so2<GoalSetting> so2) {
                wd4.b(so2, "success");
                this.a.this$Anon0.this$Anon0.q();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 profileGoalEditPresenter$saveGoalTrackingTarget$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = profileGoalEditPresenter$saveGoalTrackingTarget$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                GoalTrackingRepository c = this.this$Anon0.this$Anon0.s;
                GoalSetting goalSetting = new GoalSetting(this.this$Anon0.this$Anon0.l);
                a aVar = new a(this);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (c.updateGoalSetting(goalSetting, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cb4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1(ProfileGoalEditPresenter profileGoalEditPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1 profileGoalEditPresenter$saveGoalTrackingTarget$Anon1 = new ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1(this.this$Anon0, kc4);
        profileGoalEditPresenter$saveGoalTrackingTarget$Anon1.p$ = (lh4) obj;
        return profileGoalEditPresenter$saveGoalTrackingTarget$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
