package com.portfolio.platform.uirenew.home.details.heartrate;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$Anon1", f = "HeartRateDetailPresenter.kt", l = {192}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDayDetail$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDayDetail$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateDetailPresenter$showDayDetail$Anon1 heartRateDetailPresenter$showDayDetail$Anon1 = new HeartRateDetailPresenter$showDayDetail$Anon1(this.this$Anon0, kc4);
        heartRateDetailPresenter$showDayDetail$Anon1.p$ = (lh4) obj;
        return heartRateDetailPresenter$showDayDetail$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$showDayDetail$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
    public final Object invokeSuspend(Object obj) {
        int i;
        DailyHeartRateSummary h;
        Object a = oc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            HeartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1 heartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1 = new HeartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, heartRateDetailPresenter$showDayDetail$Anon1$summary$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
        if (this.this$Anon0.k == null || (!wd4.a((Object) this.this$Anon0.k, (Object) dailyHeartRateSummary))) {
            this.this$Anon0.k = dailyHeartRateSummary;
            DailyHeartRateSummary h2 = this.this$Anon0.k;
            int i3 = 0;
            if (h2 != null) {
                Resting resting = h2.getResting();
                if (resting != null) {
                    Integer a3 = pc4.a(resting.getValue());
                    if (a3 != null) {
                        i = a3.intValue();
                        h = this.this$Anon0.k;
                        if (h != null) {
                            Integer a4 = pc4.a(h.getMax());
                            if (a4 != null) {
                                i3 = a4.intValue();
                            }
                        }
                        this.this$Anon0.q.c(i, i3);
                    }
                }
            }
            i = 0;
            h = this.this$Anon0.k;
            if (h != null) {
            }
            this.this$Anon0.q.c(i, i3);
        }
        return cb4.a;
    }
}
