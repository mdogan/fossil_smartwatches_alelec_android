package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ei3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.qi3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter extends qi3 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ri3 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ AnalyticsHelper h;
    @DexIgnore
    public /* final */ lr2 i;
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteAccountPresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<DeleteLogoutUserUseCase.d, DeleteLogoutUserUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore
        public b(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "deleteUser - onSuccess");
            this.a.f.d();
            if (((ei3) this.a.f).isActive()) {
                this.a.f.G0();
            }
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "deleteUser - onError");
            this.a.f.d();
            this.a.f.a(cVar.a(), "");
        }
    }

    /*
    static {
        String simpleName = DeleteAccountPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "DeleteAccountPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public DeleteAccountPresenter(ri3 ri3, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, lr2 lr2, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        wd4.b(ri3, "mView");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(analyticsHelper, "mAnalyticsHelper");
        wd4.b(lr2, "mGetZendeskInformation");
        wd4.b(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.f = ri3;
        this.g = deviceRepository;
        this.h = analyticsHelper;
        this.i = lr2;
        this.j = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "presenter stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DeleteAccountPresenter$logSendOpenFeedbackEvent$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(MFUser mFUser) {
        wd4.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d(k, "deleteUser");
        this.f.e();
        DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.j;
        ri3 ri3 = this.f;
        if (ri3 != null) {
            FragmentActivity activity = ((ei3) ri3).getActivity();
            if (activity != null) {
                deleteLogoutUserUseCase.a(new DeleteLogoutUserUseCase.b(0, new WeakReference(activity)), new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "subject");
        this.i.a(new lr2.c(str), new DeleteAccountPresenter$sendFeedback$Anon1(this));
    }
}
