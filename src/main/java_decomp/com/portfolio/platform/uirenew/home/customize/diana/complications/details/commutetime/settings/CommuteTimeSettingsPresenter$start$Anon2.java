package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.v23;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$Anon2", f = "CommuteTimeSettingsPresenter.kt", l = {73, 74}, m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$start$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$Anon2$Anon1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter$start$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsPresenter$start$Anon2 commuteTimeSettingsPresenter$start$Anon2, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = commuteTimeSettingsPresenter$start$Anon2;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.n.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$start$Anon2(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeSettingsPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeSettingsPresenter$start$Anon2 commuteTimeSettingsPresenter$start$Anon2 = new CommuteTimeSettingsPresenter$start$Anon2(this.this$Anon0, kc4);
        commuteTimeSettingsPresenter$start$Anon2.p$ = (lh4) obj;
        return commuteTimeSettingsPresenter$start$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsPresenter$start$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
    public final Object invokeSuspend(Object obj) {
        MFUser e;
        lh4 lh4;
        CommuteTimeSettingsPresenter commuteTimeSettingsPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            commuteTimeSettingsPresenter = this.this$Anon0;
            gh4 a2 = commuteTimeSettingsPresenter.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = commuteTimeSettingsPresenter;
            this.label = 1;
            Object a3 = kg4.a(a2, anon1, this);
            if (a3 == a) {
                return a;
            }
            Object obj2 = a3;
            lh4 = lh42;
            obj = obj2;
        } else if (i == 1) {
            commuteTimeSettingsPresenter = (CommuteTimeSettingsPresenter) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            wd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$Anon0.i.clear();
            this.this$Anon0.i.addAll((List) obj);
            e = this.this$Anon0.j;
            if (e != null) {
                v23 g = this.this$Anon0.l;
                String home = e.getHome();
                if (home == null) {
                    home = "";
                }
                g.w(home);
                v23 g2 = this.this$Anon0.l;
                String work = e.getWork();
                if (work == null) {
                    work = "";
                }
                g2.D(work);
            }
            this.this$Anon0.l.j(this.this$Anon0.i);
            this.this$Anon0.l.a(this.this$Anon0.h);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        commuteTimeSettingsPresenter.j = (MFUser) obj;
        gh4 a4 = this.this$Anon0.c();
        CommuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1 commuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1 = new CommuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.label = 2;
        obj = kg4.a(a4, commuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1, this);
        if (obj == a) {
            return a;
        }
        wd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$Anon0.i.clear();
        this.this$Anon0.i.addAll((List) obj);
        e = this.this$Anon0.j;
        if (e != null) {
        }
        this.this$Anon0.l.j(this.this$Anon0.i);
        this.this$Anon0.l.a(this.this$Anon0.h);
        return cb4.a;
    }
}
