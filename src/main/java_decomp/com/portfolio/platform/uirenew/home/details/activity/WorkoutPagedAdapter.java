package com.portfolio.platform.uirenew.home.details.activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.jl2;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sd;
import com.fossil.blesdk.obfuscated.se3;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.ti2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Arrays;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutPagedAdapter extends sd<WorkoutSession, b> {
    @DexIgnore
    public WorkoutItem c;
    @DexIgnore
    public Unit d;

    @DexIgnore
    public enum WorkoutItem {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        HEART_RATE
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public WorkoutSession a;
        @DexIgnore
        public /* final */ ti2 b;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutPagedAdapter c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutPagedAdapter workoutPagedAdapter, ti2 ti2) {
            super(ti2.d());
            wd4.b(ti2, "mBinding");
            this.c = workoutPagedAdapter;
            this.b = ti2;
        }

        @DexIgnore
        public final void a(WorkoutSession workoutSession) {
            wd4.b(workoutSession, "value");
            this.a = workoutSession;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutAdapter", "build - value=" + workoutSession);
            WorkoutPagedAdapter workoutPagedAdapter = this.c;
            View d = this.b.d();
            wd4.a((Object) d, "mBinding.root");
            Context context = d.getContext();
            wd4.a((Object) context, "mBinding.root.context");
            WorkoutSession workoutSession2 = this.a;
            if (workoutSession2 != null) {
                c a2 = workoutPagedAdapter.a(context, workoutSession2);
                this.b.D.setImageResource(a2.f());
                FlexibleTextView flexibleTextView = this.b.x;
                wd4.a((Object) flexibleTextView, "mBinding.ftvWorkoutTitle");
                flexibleTextView.setText(a2.i());
                FlexibleTextView flexibleTextView2 = this.b.w;
                wd4.a((Object) flexibleTextView2, "mBinding.ftvWorkoutTime");
                flexibleTextView2.setText(a2.h());
                FlexibleTextView flexibleTextView3 = this.b.q;
                wd4.a((Object) flexibleTextView3, "mBinding.ftvActiveTime");
                flexibleTextView3.setText(a2.a());
                FlexibleTextView flexibleTextView4 = this.b.s;
                wd4.a((Object) flexibleTextView4, "mBinding.ftvDistance");
                flexibleTextView4.setText(a2.c());
                FlexibleTextView flexibleTextView5 = this.b.v;
                wd4.a((Object) flexibleTextView5, "mBinding.ftvSteps");
                flexibleTextView5.setText(a2.g());
                FlexibleTextView flexibleTextView6 = this.b.r;
                wd4.a((Object) flexibleTextView6, "mBinding.ftvCalories");
                flexibleTextView6.setText(a2.b());
                FlexibleTextView flexibleTextView7 = this.b.t;
                wd4.a((Object) flexibleTextView7, "mBinding.ftvHeartRateAvg");
                flexibleTextView7.setText(a2.d());
                FlexibleTextView flexibleTextView8 = this.b.u;
                wd4.a((Object) flexibleTextView8, "mBinding.ftvHeartRateMax");
                flexibleTextView8.setText(a2.e());
                View d2 = this.b.d();
                wd4.a((Object) d2, "mBinding.root");
                Context context2 = d2.getContext();
                ColorStateList valueOf = ColorStateList.valueOf(k6.a(context2, (int) R.color.dianaStepsTab));
                wd4.a((Object) valueOf, "ColorStateList.valueOf(color)");
                int i = se3.a[this.c.c.ordinal()];
                if (i == 1) {
                    int a3 = k6.a(context2, (int) R.color.dianaActiveMinutesTab);
                    valueOf = ColorStateList.valueOf(a3);
                    wd4.a((Object) valueOf, "ColorStateList.valueOf(color)");
                    AppCompatImageView appCompatImageView = this.b.y;
                    wd4.a((Object) appCompatImageView, "mBinding.ivActiveTime");
                    appCompatImageView.setImageTintList(valueOf);
                    this.b.q.setTextColor(a3);
                } else if (i == 2) {
                    int a4 = k6.a(context2, (int) R.color.dianaStepsTab);
                    valueOf = ColorStateList.valueOf(a4);
                    wd4.a((Object) valueOf, "ColorStateList.valueOf(color)");
                    AppCompatImageView appCompatImageView2 = this.b.C;
                    wd4.a((Object) appCompatImageView2, "mBinding.ivSteps");
                    appCompatImageView2.setImageTintList(valueOf);
                    this.b.v.setTextColor(a4);
                } else if (i == 3) {
                    int a5 = k6.a(context2, (int) R.color.dianaActiveCaloriesTab);
                    valueOf = ColorStateList.valueOf(a5);
                    wd4.a((Object) valueOf, "ColorStateList.valueOf(color)");
                    AppCompatImageView appCompatImageView3 = this.b.z;
                    wd4.a((Object) appCompatImageView3, "mBinding.ivCalories");
                    appCompatImageView3.setImageTintList(valueOf);
                    this.b.r.setTextColor(a5);
                } else if (i == 4) {
                    int a6 = k6.a(context2, (int) R.color.dianaHeartRateTab);
                    valueOf = ColorStateList.valueOf(a6);
                    wd4.a((Object) valueOf, "ColorStateList.valueOf(color)");
                    AppCompatImageView appCompatImageView4 = this.b.A;
                    wd4.a((Object) appCompatImageView4, "mBinding.ivHeartRateAvg");
                    appCompatImageView4.setImageTintList(valueOf);
                    AppCompatImageView appCompatImageView5 = this.b.B;
                    wd4.a((Object) appCompatImageView5, "mBinding.ivHeartRateMax");
                    appCompatImageView5.setImageTintList(valueOf);
                    this.b.t.setTextColor(a6);
                    this.b.u.setTextColor(a6);
                }
                AppCompatImageView appCompatImageView6 = this.b.D;
                wd4.a((Object) appCompatImageView6, "mBinding.ivWorkout");
                appCompatImageView6.setImageTintList(valueOf);
                return;
            }
            wd4.d("mValue");
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;

        @DexIgnore
        public c(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
            wd4.b(str, "mTitle");
            wd4.b(str2, "mTime");
            wd4.b(str3, "mActiveMinute");
            wd4.b(str4, "mDistance");
            wd4.b(str5, "mSteps");
            wd4.b(str6, "mCalories");
            wd4.b(str7, "mHeartRateAvg");
            wd4.b(str8, "mHeartRateMax");
            this.a = i2;
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = str5;
            this.g = str6;
            this.h = str7;
            this.i = str8;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final String c() {
            return this.e;
        }

        @DexIgnore
        public final String d() {
            return this.h;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (!(this.a == cVar.a) || !wd4.a((Object) this.b, (Object) cVar.b) || !wd4.a((Object) this.c, (Object) cVar.c) || !wd4.a((Object) this.d, (Object) cVar.d) || !wd4.a((Object) this.e, (Object) cVar.e) || !wd4.a((Object) this.f, (Object) cVar.f) || !wd4.a((Object) this.g, (Object) cVar.g) || !wd4.a((Object) this.h, (Object) cVar.h) || !wd4.a((Object) this.i, (Object) cVar.i)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final int f() {
            return this.a;
        }

        @DexIgnore
        public final String g() {
            return this.f;
        }

        @DexIgnore
        public final String h() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = this.a * 31;
            String str = this.b;
            int i3 = 0;
            int hashCode = (i2 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.d;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.e;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.f;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.g;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            String str7 = this.h;
            int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
            String str8 = this.i;
            if (str8 != null) {
                i3 = str8.hashCode();
            }
            return hashCode7 + i3;
        }

        @DexIgnore
        public final String i() {
            return this.b;
        }

        @DexIgnore
        public String toString() {
            return "WorkoutModel(mResIconId=" + this.a + ", mTitle=" + this.b + ", mTime=" + this.c + ", mActiveMinute=" + this.d + ", mDistance=" + this.e + ", mSteps=" + this.f + ", mCalories=" + this.g + ", mHeartRateAvg=" + this.h + ", mHeartRateMax=" + this.i + ")";
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutPagedAdapter(WorkoutItem workoutItem, Unit unit, WorkoutSessionDifference workoutSessionDifference) {
        super(workoutSessionDifference);
        wd4.b(workoutItem, "mWorkoutItem");
        wd4.b(unit, "mDistanceUnit");
        wd4.b(workoutSessionDifference, "workoutSessionDiff");
        this.c = workoutItem;
        this.d = unit;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        ti2 a2 = ti2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wd4.a((Object) a2, "ItemWorkoutBinding.infla\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(Unit unit, rd<WorkoutSession> rdVar) {
        wd4.b(unit, MFUser.DISTANCE_UNIT);
        wd4.b(rdVar, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutAdapter", "setData - distanceUnit=" + unit + ", data=" + rdVar.size());
        this.d = unit;
        b(rdVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wd4.b(bVar, "holder");
        WorkoutSession workoutSession = (WorkoutSession) a(i);
        if (workoutSession != null) {
            bVar.a(workoutSession);
        }
    }

    @DexIgnore
    public final c a(Context context, WorkoutSession workoutSession) {
        String format;
        String str;
        String str2;
        String a2 = sk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond());
        Triple<Integer, Integer, Integer> c2 = sk2.c(workoutSession.getDuration());
        wd4.a((Object) c2, "DateHelper.getTimeValues(workoutSession.duration)");
        if (wd4.a(c2.getFirst().intValue(), 0) > 0) {
            be4 be4 = be4.a;
            String a3 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberHrsNumberMinsNumberSecs);
            wd4.a((Object) a3, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
            Object[] objArr = {c2.getFirst(), c2.getSecond(), c2.getThird()};
            format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
        } else if (wd4.a(c2.getSecond().intValue(), 0) > 0) {
            be4 be42 = be4.a;
            String a4 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberMinsNumberSecs);
            wd4.a((Object) a4, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
            Object[] objArr2 = {c2.getSecond(), c2.getThird()};
            format = String.format(a4, Arrays.copyOf(objArr2, objArr2.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
        } else {
            be4 be43 = be4.a;
            String a5 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberSecs);
            wd4.a((Object) a5, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
            Object[] objArr3 = {c2.getThird()};
            format = String.format(a5, Arrays.copyOf(objArr3, objArr3.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
        }
        String str3 = format;
        if (workoutSession.getWorkoutType() != WorkoutType.RUNNING) {
            str = tm2.a(context, (int) R.string.character_dash);
        } else {
            pl2 pl2 = pl2.a;
            Double totalDistance = workoutSession.getTotalDistance();
            str = pl2.a(totalDistance != null ? Float.valueOf((float) totalDistance.doubleValue()) : null, this.d);
        }
        if (this.d == Unit.IMPERIAL) {
            str2 = str + " " + tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Label__Miles);
        } else {
            str2 = str + " " + tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Label__Km);
        }
        be4 be44 = be4.a;
        String a6 = tm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Text__NumberSteps);
        wd4.a((Object) a6, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
        Object[] objArr4 = {pl2.a.b(workoutSession.getTotalSteps())};
        String format2 = String.format(a6, Arrays.copyOf(objArr4, objArr4.length));
        wd4.a((Object) format2, "java.lang.String.format(format, *args)");
        String str4 = pl2.a.a(workoutSession.getTotalCalorie()) + " " + tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Label__ActiveCals);
        StringBuilder sb = new StringBuilder();
        Float averageHeartRate = workoutSession.getAverageHeartRate();
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        sb.append(jl2.b(averageHeartRate != null ? averageHeartRate.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0));
        sb.append(" ");
        sb.append(tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Label__BpmAvg));
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        Integer maxHeartRate = workoutSession.getMaxHeartRate();
        if (maxHeartRate != null) {
            f = (float) maxHeartRate.intValue();
        }
        sb3.append(jl2.b(f, 0));
        sb3.append(" ");
        sb3.append(tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Label__BpmMax));
        String sb4 = sb3.toString();
        Pair<Integer, Integer> a7 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
        String a8 = tm2.a(context, a7.getSecond().intValue());
        int intValue = a7.getFirst().intValue();
        wd4.a((Object) a8, "title");
        wd4.a((Object) a2, LogBuilder.KEY_TIME);
        return new c(intValue, a8, a2, str3, str2, format2, str4, sb2, sb4);
    }
}
