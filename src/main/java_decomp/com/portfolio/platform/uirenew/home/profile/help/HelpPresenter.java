package com.portfolio.platform.uirenew.home.profile.help;

import com.fossil.blesdk.obfuscated.hi3;
import com.fossil.blesdk.obfuscated.ii3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HelpPresenter extends hi3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public /* final */ ii3 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ lr2 h;
    @DexIgnore
    public /* final */ AnalyticsHelper i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = HelpPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "HelpPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public HelpPresenter(ii3 ii3, DeviceRepository deviceRepository, lr2 lr2, AnalyticsHelper analyticsHelper) {
        wd4.b(ii3, "mView");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(lr2, "mGetZendeskInformation");
        wd4.b(analyticsHelper, "mAnalyticsHelper");
        this.f = ii3;
        this.g = deviceRepository;
        this.h = lr2;
        this.i = analyticsHelper;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HelpPresenter$logSendOpenFeedbackEvent$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "subject");
        this.h.a(new lr2.c(str), new HelpPresenter$sendFeedback$Anon1(this));
    }
}
