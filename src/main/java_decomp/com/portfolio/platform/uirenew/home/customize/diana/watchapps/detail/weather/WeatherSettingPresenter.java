package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.k53;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.l53;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.un1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xn1;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter extends k53 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public PlacesClient f;
    @DexIgnore
    public WeatherWatchAppSetting g;
    @DexIgnore
    public List<WeatherLocationWrapper> h; // = new ArrayList();
    @DexIgnore
    public /* final */ l53 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tn1 {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter a;

        @DexIgnore
        public b(WeatherSettingPresenter weatherSettingPresenter) {
            this.a = weatherSettingPresenter;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            wd4.b(exc, "exception");
            this.a.i.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = WeatherSettingPresenter.j;
            local.e(k, "FetchPlaceRequest - exception=" + exc);
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = WeatherSettingPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "WeatherSettingPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public WeatherSettingPresenter(l53 l53, GoogleApiService googleApiService) {
        wd4.b(l53, "mView");
        wd4.b(googleApiService, "mGoogleApiService");
        this.i = l53;
    }

    @DexIgnore
    public void f() {
        this.f = Places.createClient(PortfolioApp.W.c());
        this.i.o(this.h);
        this.i.a(this.f);
    }

    @DexIgnore
    public void g() {
        this.f = null;
    }

    @DexIgnore
    public WeatherWatchAppSetting h() {
        return this.g;
    }

    @DexIgnore
    public void i() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "saveWeatherWatchAppSetting - mLocationWrappers=" + this.h);
        this.i.b();
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.i.a(this);
    }

    @DexIgnore
    public void a(String str) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        wd4.b(str, MicroAppSetting.SETTING);
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) new Gson().a(str, WeatherWatchAppSetting.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local.d(str2, "exception when parse weather setting " + e);
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        this.g = weatherWatchAppSetting;
        if (this.g == null) {
            this.g = new WeatherWatchAppSetting();
        }
        WeatherWatchAppSetting weatherWatchAppSetting2 = this.g;
        if (weatherWatchAppSetting2 != null) {
            List<WeatherLocationWrapper> locations = weatherWatchAppSetting2.getLocations();
            ArrayList arrayList = new ArrayList();
            for (T next : locations) {
                if (!TextUtils.isEmpty(((WeatherLocationWrapper) next).getId())) {
                    arrayList.add(next);
                }
            }
            if (arrayList.isEmpty()) {
                this.g = new WeatherWatchAppSetting();
            }
            this.h.clear();
            WeatherWatchAppSetting weatherWatchAppSetting3 = this.g;
            if (weatherWatchAppSetting3 != null) {
                for (WeatherLocationWrapper add : weatherWatchAppSetting3.getLocations()) {
                    this.h.add(add);
                }
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(int i2, boolean z) {
        List<WeatherLocationWrapper> list = this.h;
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((WeatherLocationWrapper) next).isEnableLocation()) {
                arrayList.add(next);
            }
        }
        if (z && arrayList.size() > 1) {
            this.i.e0();
        } else if (i2 < this.h.size()) {
            this.h.get(i2).setEnableLocation(z);
            this.i.o(this.h);
        }
    }

    @DexIgnore
    public void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        wd4.b(str, "address");
        wd4.b(str2, "placeId");
        List<WeatherLocationWrapper> list = this.h;
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((WeatherLocationWrapper) next).isEnableLocation()) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() > 2) {
            this.i.e0();
            return;
        }
        this.i.b();
        if (this.f != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Place.Field.ADDRESS);
            arrayList2.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str2, arrayList2);
            wd4.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.f;
            if (placesClient != null) {
                xn1<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                wd4.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a((un1<? super FetchPlaceResponse>) new WeatherSettingPresenter$addLocation$Anon1(this, str, str2));
                fetchPlace.a((tn1) new b(this));
                return;
            }
            wd4.a();
            throw null;
        }
    }
}
