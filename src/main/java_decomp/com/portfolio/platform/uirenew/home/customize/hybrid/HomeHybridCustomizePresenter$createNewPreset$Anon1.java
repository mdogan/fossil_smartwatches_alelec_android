package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$createNewPreset$Anon1", f = "HomeHybridCustomizePresenter.kt", l = {286}, m = "invokeSuspend")
public final class HomeHybridCustomizePresenter$createNewPreset$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$createNewPreset$Anon1(HomeHybridCustomizePresenter homeHybridCustomizePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeHybridCustomizePresenter$createNewPreset$Anon1 homeHybridCustomizePresenter$createNewPreset$Anon1 = new HomeHybridCustomizePresenter$createNewPreset$Anon1(this.this$Anon0, kc4);
        homeHybridCustomizePresenter$createNewPreset$Anon1.p$ = (lh4) obj;
        return homeHybridCustomizePresenter$createNewPreset$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeHybridCustomizePresenter$createNewPreset$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            Iterator it = this.this$Anon0.h.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (pc4.a(((HybridPreset) obj2).isActive()).booleanValue()) {
                    break;
                }
            }
            HybridPreset hybridPreset = (HybridPreset) obj2;
            if (hybridPreset != null) {
                HybridPreset cloneFrom = HybridPreset.Companion.cloneFrom(hybridPreset);
                gh4 b = this.this$Anon0.c();
                HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new HomeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(cloneFrom, (kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = hybridPreset;
                this.L$Anon2 = hybridPreset;
                this.L$Anon3 = cloneFrom;
                this.label = 1;
                if (kg4.a(b, homeHybridCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                    return a;
                }
            }
            return cb4.a;
        } else if (i == 1) {
            HybridPreset hybridPreset2 = (HybridPreset) this.L$Anon3;
            HybridPreset hybridPreset3 = (HybridPreset) this.L$Anon2;
            HybridPreset hybridPreset4 = (HybridPreset) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.o.v();
        return cb4.a;
    }
}
