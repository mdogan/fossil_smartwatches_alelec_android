package com.portfolio.platform.uirenew.home.profile;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fv2;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$Anon1", f = "HomeProfilePresenter.kt", l = {355}, m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$Anon1(HomeProfilePresenter homeProfilePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeProfilePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeProfilePresenter$openCameraIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1(this.this$Anon0, kc4);
        homeProfilePresenter$openCameraIntent$Anon1.p$ = (lh4) obj;
        return homeProfilePresenter$openCameraIntent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$openCameraIntent$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            pg3 l = this.this$Anon0.l();
            if (l != null) {
                FragmentActivity activity = ((fv2) l).getActivity();
                if (activity != null) {
                    gh4 a2 = this.this$Anon0.b();
                    HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(activity, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = activity;
                    this.L$Anon2 = activity;
                    this.label = 1;
                    obj = kg4.a(a2, homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                }
                return cb4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        } else if (i == 1) {
            FragmentActivity fragmentActivity = (FragmentActivity) this.L$Anon2;
            FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Intent intent = (Intent) obj;
        if (intent != null) {
            ((fv2) this.this$Anon0.l()).startActivityForResult(intent, 1234);
        }
        return cb4.a;
    }
}
