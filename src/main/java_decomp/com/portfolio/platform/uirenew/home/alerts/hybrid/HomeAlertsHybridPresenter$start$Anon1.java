package com.portfolio.platform.uirenew.home.alerts.hybrid;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sb4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsHybridPresenter$start$Anon1<T> implements dc<String> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$Anon1$Anon1", f = "HomeAlertsHybridPresenter.kt", l = {58}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public boolean Z$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ic4.a(Integer.valueOf(((Alarm) t).getTotalMinutes()), Integer.valueOf(((Alarm) t2).getTotalMinutes()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeAlertsHybridPresenter$start$Anon1 homeAlertsHybridPresenter$start$Anon1, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeAlertsHybridPresenter$start$Anon1;
            this.$deviceId = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$deviceId, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                boolean b = DeviceHelper.o.e().b(this.$deviceId);
                this.this$Anon0.a.i.q(b);
                if (b) {
                    gh4 a3 = this.this$Anon0.a.c();
                    HomeAlertsHybridPresenter$start$Anon1$Anon1$allAlarms$Anon1 homeAlertsHybridPresenter$start$Anon1$Anon1$allAlarms$Anon1 = new HomeAlertsHybridPresenter$start$Anon1$Anon1$allAlarms$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.Z$Anon0 = b;
                    this.label = 1;
                    obj2 = kg4.a(a3, homeAlertsHybridPresenter$start$Anon1$Anon1$allAlarms$Anon1, this);
                    if (obj2 == a2) {
                        return a2;
                    }
                }
                HomeAlertsHybridPresenter homeAlertsHybridPresenter = this.this$Anon0.a;
                homeAlertsHybridPresenter.h = homeAlertsHybridPresenter.m.D();
                this.this$Anon0.a.i.i(this.this$Anon0.a.h);
                this.this$Anon0.a.i.n(this.this$Anon0.a.h);
                return cb4.a;
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<Alarm> list = (List) obj2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a4 = HomeAlertsHybridPresenter.o.a();
            local.d(a4, "GetAlarms onSuccess: size = " + list.size());
            this.this$Anon0.a.g.clear();
            for (Alarm copy$default : list) {
                this.this$Anon0.a.g.add(Alarm.copy$default(copy$default, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null));
            }
            ArrayList c = this.this$Anon0.a.g;
            if (c.size() > 1) {
                sb4.a(c, new a());
            }
            this.this$Anon0.a.i.d(this.this$Anon0.a.g);
            HomeAlertsHybridPresenter homeAlertsHybridPresenter2 = this.this$Anon0.a;
            homeAlertsHybridPresenter2.h = homeAlertsHybridPresenter2.m.D();
            this.this$Anon0.a.i.i(this.this$Anon0.a.h);
            this.this$Anon0.a.i.n(this.this$Anon0.a.h);
            return cb4.a;
        }
    }

    @DexIgnore
    public HomeAlertsHybridPresenter$start$Anon1(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        this.a = homeAlertsHybridPresenter;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.length() == 0) {
            this.a.i.a(true);
        } else {
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, str, (kc4) null), 3, (Object) null);
        }
    }
}
