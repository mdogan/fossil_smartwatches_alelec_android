package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $it;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $permissionErrorCodes$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1(DianaPreset dianaPreset, kc4 kc4, SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver setPresetBroadcastReceiver, int i, ArrayList arrayList) {
        super(2, kc4);
        this.$it = dianaPreset;
        this.this$Anon0 = setPresetBroadcastReceiver;
        this.$errorCode$inlined = i;
        this.$permissionErrorCodes$inlined = arrayList;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1 = new SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0, this.$errorCode$inlined, this.$permissionErrorCodes$inlined);
        setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase = SetDianaPresetToWatchUseCase.this;
            DianaPreset dianaPreset = this.$it;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (setDianaPresetToWatchUseCase.a(dianaPreset, (kc4<? super cb4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SetDianaPresetToWatchUseCase.this.a(new SetDianaPresetToWatchUseCase.b(this.$errorCode$inlined, this.$permissionErrorCodes$inlined));
        return cb4.a;
    }
}
