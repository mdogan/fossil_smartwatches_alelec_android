package com.portfolio.platform.uirenew.home.details.activity;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.me3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ne3;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.enums.Unit;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$Anon1", f = "ActivityDetailPresenter.kt", l = {114}, m = "invokeSuspend")
public final class ActivityDetailPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$Anon1$Anon1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Unit>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivityDetailPresenter$start$Anon1 activityDetailPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = activityDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                MFUser currentUser = this.this$Anon0.this$Anon0.v.getCurrentUser();
                if (currentUser != null) {
                    return currentUser.getDistanceUnit();
                }
                wd4.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements dc<ps3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$Anon1$Anon2$Anon1", f = "ActivityDetailPresenter.kt", l = {125}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon2;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    gh4 a2 = this.this$Anon0.a.this$Anon0.b();
                    ActivityDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 activityDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1 = new ActivityDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = kg4.a(a2, activityDetailPresenter$start$Anon1$Anon2$Anon1$summary$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ActivitySummary activitySummary = (ActivitySummary) obj;
                if (this.this$Anon0.a.this$Anon0.m == null || (!wd4.a((Object) this.this$Anon0.a.this$Anon0.m, (Object) activitySummary))) {
                    this.this$Anon0.a.this$Anon0.m = activitySummary;
                    this.this$Anon0.a.this$Anon0.s.a(this.this$Anon0.a.this$Anon0.o, this.this$Anon0.a.this$Anon0.m);
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        ri4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon2(ActivityDetailPresenter$start$Anon1 activityDetailPresenter$start$Anon1) {
            this.a = activityDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<ActivitySummary>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActivityDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.k = list;
                this.a.this$Anon0.i = true;
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3<T> implements dc<ps3<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$Anon1$Anon3$Anon1", f = "ActivityDetailPresenter.kt", l = {148}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon3 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon3 anon3, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon3;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    gh4 a2 = this.this$Anon0.a.this$Anon0.b();
                    ActivityDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 activityDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 = new ActivityDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = kg4.a(a2, activityDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (this.this$Anon0.a.this$Anon0.n == null || (!wd4.a((Object) this.this$Anon0.a.this$Anon0.n, (Object) list))) {
                    this.this$Anon0.a.this$Anon0.n = list;
                    if (this.this$Anon0.a.this$Anon0.i && this.this$Anon0.a.this$Anon0.j) {
                        ri4 unused = this.this$Anon0.a.this$Anon0.l();
                    }
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon3(ActivityDetailPresenter$start$Anon1 activityDetailPresenter$start$Anon1) {
            this.a = activityDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<ActivitySample>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sampleTransformations -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActivityDetailPresenter", sb.toString());
            if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
                this.a.this$Anon0.l = list;
                this.a.this$Anon0.j = true;
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDetailPresenter$start$Anon1(ActivityDetailPresenter activityDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activityDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivityDetailPresenter$start$Anon1 activityDetailPresenter$start$Anon1 = new ActivityDetailPresenter$start$Anon1(this.this$Anon0, kc4);
        activityDetailPresenter$start$Anon1.p$ = (lh4) obj;
        return activityDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityDetailPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ActivityDetailPresenter activityDetailPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ActivityDetailPresenter activityDetailPresenter2 = this.this$Anon0;
            gh4 a2 = activityDetailPresenter2.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = activityDetailPresenter2;
            this.label = 1;
            obj = kg4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            activityDetailPresenter = activityDetailPresenter2;
        } else if (i == 1) {
            activityDetailPresenter = (ActivityDetailPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        wd4.a(obj, "withContext(commonPool) \u2026ntUser()!!.distanceUnit }");
        activityDetailPresenter.o = (Unit) obj;
        LiveData q = this.this$Anon0.p;
        me3 o = this.this$Anon0.s;
        if (o != null) {
            q.a((ne3) o, new Anon2(this));
            this.this$Anon0.q.a((LifecycleOwner) this.this$Anon0.s, new Anon3(this));
            return cb4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }
}
