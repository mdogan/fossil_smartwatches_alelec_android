package com.portfolio.platform.uirenew.home.alerts.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$Anon1", f = "HomeAlertsPresenter.kt", l = {432, 434}, m = "invokeSuspend")
public final class HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$setNotificationFilterToDevice$Anon1(HomeAlertsPresenter homeAlertsPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeAlertsPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1 = new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1(this.this$Anon0, kc4);
        homeAlertsPresenter$setNotificationFilterToDevice$Anon1.p$ = (lh4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeAlertsPresenter$setNotificationFilterToDevice$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0106  */
    public final Object invokeSuspend(Object obj) {
        long j;
        List list;
        Object obj2;
        List list2;
        sh4 sh4;
        lh4 lh4;
        sh4 sh42;
        List list3;
        Object obj3;
        List list4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            list3 = new ArrayList();
            j = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "filter notification, time start=" + j);
            list3.clear();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = HomeAlertsPresenter.x.a();
            local2.d(a3, "mListAppWrapper.size = " + this.this$Anon0.i().size());
            sh4 a4 = mg4.a(lh42, this.this$Anon0.b(), (CoroutineStart) null, new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$otherAppDeffer$Anon1(this, (kc4) null), 2, (Object) null);
            sh42 = mg4.a(lh42, this.this$Anon0.b(), (CoroutineStart) null, new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1(this, (kc4) null), 2, (Object) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = list3;
            this.J$Anon0 = j;
            this.L$Anon2 = a4;
            this.L$Anon3 = sh42;
            this.L$Anon4 = list3;
            this.label = 1;
            obj3 = a4.a(this);
            if (obj3 == a) {
                return a;
            }
            sh4 = a4;
            lh4 = lh42;
            list4 = list3;
        } else if (i == 1) {
            list4 = (List) this.L$Anon4;
            j = this.J$Anon0;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            sh4 = (sh4) this.L$Anon2;
            list3 = (List) this.L$Anon1;
            sh42 = (sh4) this.L$Anon3;
            obj3 = obj;
        } else if (i == 2) {
            sh4 sh43 = (sh4) this.L$Anon3;
            sh4 sh44 = (sh4) this.L$Anon2;
            long j2 = this.J$Anon0;
            list = (List) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            j = j2;
            obj2 = obj;
            list2 = (List) obj2;
            if (list2 != null) {
                list.addAll(list2);
                long b = PortfolioApp.W.c().b(new AppNotificationFilterSettings(list, System.currentTimeMillis()), PortfolioApp.W.c().e());
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a5 = HomeAlertsPresenter.x.a();
                local3.d(a5, "filter notification, time end= " + b);
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a6 = HomeAlertsPresenter.x.a();
                local4.d(a6, "delayTime =" + (b - j) + " milliseconds");
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list4.addAll((Collection) obj3);
        this.L$Anon0 = lh4;
        this.L$Anon1 = list3;
        this.J$Anon0 = j;
        this.L$Anon2 = sh4;
        this.L$Anon3 = sh42;
        this.label = 2;
        obj2 = sh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list = list3;
        list2 = (List) obj2;
        if (list2 != null) {
        }
        return cb4.a;
    }
}
