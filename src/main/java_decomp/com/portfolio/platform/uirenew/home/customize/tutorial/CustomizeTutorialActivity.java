package com.portfolio.platform.uirenew.home.customize.tutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.p13;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeTutorialActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "watchAppId");
            Intent intent = new Intent(context, CustomizeTutorialActivity.class);
            intent.putExtra("EXTRA_WATCHAPP_ID", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        if (((p13) getSupportFragmentManager().a((int) R.id.content)) == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_WATCHAPP_ID");
            p13.a aVar = p13.n;
            wd4.a((Object) stringExtra, "watchAppId");
            a((Fragment) aVar.a(stringExtra), "CustomizeTutorialFragment", (int) R.id.content);
        }
    }
}
