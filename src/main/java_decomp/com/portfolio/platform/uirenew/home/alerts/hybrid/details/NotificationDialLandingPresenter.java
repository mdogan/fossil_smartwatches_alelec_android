package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.qz2;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rz2;
import com.fossil.blesdk.obfuscated.sz2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationDialLandingPresenter extends qz2 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ rz2 f;
    @DexIgnore
    public /* final */ NotificationsLoader g;
    @DexIgnore
    public /* final */ LoaderManager h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = NotificationDialLandingPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public NotificationDialLandingPresenter(rz2 rz2, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        wd4.b(rz2, "mView");
        wd4.b(notificationsLoader, "mNotificationLoader");
        wd4.b(loaderManager, "mLoaderManager");
        this.f = rz2;
        this.g = notificationsLoader;
        this.h = loaderManager;
    }

    @DexIgnore
    public void a(rc<SparseArray<List<BaseFeatureModel>>> rcVar) {
        wd4.b(rcVar, "loader");
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        cn2 cn2 = cn2.d;
        rz2 rz2 = this.f;
        if (rz2 != null) {
            if (cn2.a(cn2, ((sz2) rz2).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null) && cn2.a(cn2.d, ((sz2) this.f).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null) && !PortfolioApp.W.c().u().N()) {
                ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationDialLandingPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
            }
            this.h.a(3, (Bundle) null, this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingFragment");
    }

    @DexIgnore
    public void g() {
        this.h.a(3);
        FLogger.INSTANCE.getLocal().d(i, "stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    public rc<SparseArray<List<BaseFeatureModel>>> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onCreateLoader id=" + i2);
        return this.g;
    }

    @DexIgnore
    public void a(rc<SparseArray<List<BaseFeatureModel>>> rcVar, SparseArray<List<BaseFeatureModel>> sparseArray) {
        wd4.b(rcVar, "loader");
        if (sparseArray != null) {
            this.f.a(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(i, "onLoadFinished, data=null");
        }
    }
}
