package com.portfolio.platform.uirenew.home.customize.diana.theme;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.uh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $selectedPos$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ RecyclerView $this_with;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CustomizeThemeFragment this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(RecyclerView recyclerView, kc4 kc4, CustomizeThemeFragment customizeThemeFragment, int i) {
        super(2, kc4);
        this.$this_with = recyclerView;
        this.this$Anon0 = customizeThemeFragment;
        this.$selectedPos$inlined = i;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1 = new CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(this.$this_with, kc4, this.this$Anon0, this.$selectedPos$inlined);
        customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            long a2 = this.this$Anon0.m;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (uh4.a(a2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.m = 0;
        this.$this_with.j(this.$selectedPos$inlined);
        return cb4.a;
    }
}
