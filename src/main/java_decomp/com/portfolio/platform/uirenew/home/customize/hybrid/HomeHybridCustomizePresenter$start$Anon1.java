package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.a63;
import com.fossil.blesdk.obfuscated.ab4;
import com.fossil.blesdk.obfuscated.av2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1", f = "HomeHybridCustomizePresenter.kt", l = {60}, m = "invokeSuspend")
public final class HomeHybridCustomizePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$Anon1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends MicroApp>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeHybridCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.p.getAllMicroApp(this.this$Anon0.this$Anon0.n.e());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements dc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a  reason: collision with other inner class name */
        public static final class C0144a<T> implements dc<List<? extends HybridPreset>> {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a$a")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$Anon1$a$a$a  reason: collision with other inner class name */
            public static final class C0145a<T> implements Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return ic4.a(Boolean.valueOf(((HybridPreset) t2).isActive()), Boolean.valueOf(((HybridPreset) t).isActive()));
                }
            }

            @DexIgnore
            public C0144a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void a(List<HybridPreset> list) {
                if (list != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                    List<T> a2 = wb4.a(list, new C0145a());
                    boolean a3 = wd4.a((Object) a2, (Object) this.a.a.this$Anon0.h) ^ true;
                    int itemCount = this.a.a.this$Anon0.o.getItemCount();
                    if (a3 || a2.size() != itemCount - 1) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("HomeHybridCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                        if (this.a.a.this$Anon0.l == 1) {
                            this.a.a.this$Anon0.a((List<HybridPreset>) a2);
                        } else {
                            this.a.a.this$Anon0.m = ab4.a(true, a2);
                        }
                    }
                }
            }
        }

        @DexIgnore
        public a(HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1) {
            this.a = homeHybridCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$Anon0.l);
            if (!TextUtils.isEmpty(str)) {
                HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.a.this$Anon0;
                HybridPresetRepository e = homeHybridCustomizePresenter.q;
                if (str != null) {
                    homeHybridCustomizePresenter.f = e.getPresetListAsLiveData(str);
                    this.a.this$Anon0.f.a((LifecycleOwner) this.a.this$Anon0.o, new C0144a(this));
                    this.a.this$Anon0.o.a(false);
                    return;
                }
                wd4.a();
                throw null;
            }
            this.a.this$Anon0.o.a(true);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$start$Anon1(HomeHybridCustomizePresenter homeHybridCustomizePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeHybridCustomizePresenter$start$Anon1 homeHybridCustomizePresenter$start$Anon1 = new HomeHybridCustomizePresenter$start$Anon1(this.this$Anon0, kc4);
        homeHybridCustomizePresenter$start$Anon1.p$ = (lh4) obj;
        return homeHybridCustomizePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeHybridCustomizePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
    public final Object invokeSuspend(Object obj) {
        a63 l;
        ArrayList arrayList;
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.g.isEmpty()) {
                ArrayList a3 = this.this$Anon0.g;
                gh4 b = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = a3;
                this.label = 1;
                obj = kg4.a(b, anon1, this);
                if (obj == a2) {
                    return a2;
                }
                arrayList = a3;
            }
            MutableLiveData i2 = this.this$Anon0.j;
            l = this.this$Anon0.o;
            if (l == null) {
                i2.a((av2) l, new a(this));
                this.this$Anon0.r.e();
                BleCommandResultManager.d.a(CommunicateMode.SET_LINK_MAPPING);
                return cb4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (i == 1) {
            arrayList = (ArrayList) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList.addAll((Collection) obj);
        MutableLiveData i22 = this.this$Anon0.j;
        l = this.this$Anon0.o;
        if (l == null) {
        }
    }
}
