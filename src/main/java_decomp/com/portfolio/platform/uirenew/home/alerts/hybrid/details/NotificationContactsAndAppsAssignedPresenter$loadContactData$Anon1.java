package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 implements j62.d<u03.d, u03.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
        this.a = notificationContactsAndAppsAssignedPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(u03.d dVar) {
        wd4.b(dVar, "responseValue");
        ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(this, dVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(u03.b bVar) {
        wd4.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetAllHybridContactGroups onError");
        this.a.p.d();
    }
}
