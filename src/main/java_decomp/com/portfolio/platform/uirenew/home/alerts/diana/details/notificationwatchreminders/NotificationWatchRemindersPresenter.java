package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.by2;
import com.fossil.blesdk.obfuscated.cy2;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.dy2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationWatchRemindersPresenter extends by2 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public boolean f; // = this.q.P();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j; // = this.f;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> n; // = this.r.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> o; // = this.r.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ cy2 p;
    @DexIgnore
    public /* final */ fn2 q;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public b(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(List<InactivityNudgeTimeModel> list) {
            cy2 unused = this.a.p;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements dc<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public c(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(RemindTimeModel remindTimeModel) {
            cy2 unused = this.a.p;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = NotificationWatchRemindersPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public NotificationWatchRemindersPresenter(cy2 cy2, fn2 fn2, RemindersSettingsDatabase remindersSettingsDatabase) {
        wd4.b(cy2, "mView");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.p = cy2;
        this.q = fn2;
        this.r = remindersSettingsDatabase;
    }

    @DexIgnore
    public void j() {
        this.k = !this.k;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void k() {
        this.l = !this.l;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void l() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationWatchRemindersPresenter$save$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean m() {
        return (this.j == this.f && this.k == this.g && this.l == this.h && this.m == this.i) ? false : true;
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(s, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.n;
        cy2 cy2 = this.p;
        if (cy2 != null) {
            liveData.a((dy2) cy2, new NotificationWatchRemindersPresenter$start$Anon1(this));
            this.o.a((LifecycleOwner) this.p, new NotificationWatchRemindersPresenter$start$Anon2(this));
            this.g = this.q.Q();
            this.k = this.g;
            this.h = this.q.R();
            this.l = this.h;
            this.i = this.q.O();
            this.m = this.i;
            this.p.D(this.j);
            this.p.w(this.k);
            this.p.C(this.l);
            this.p.u(this.m);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(s, "stop");
        this.n.b(new b(this));
        this.o.b(new c(this));
    }

    @DexIgnore
    public void h() {
        this.m = !this.m;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void i() {
        this.j = !this.j;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
        this.p.D(this.j);
    }
}
