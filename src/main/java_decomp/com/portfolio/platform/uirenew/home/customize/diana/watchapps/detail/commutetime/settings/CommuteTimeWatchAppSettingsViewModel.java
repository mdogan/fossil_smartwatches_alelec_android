package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppSettingsViewModel extends jc {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public Gson c; // = new Gson();
    @DexIgnore
    public CommuteTimeWatchAppSetting d;
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public MutableLiveData<b> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public List<AddressWrapper> a;

        @DexIgnore
        public b(List<AddressWrapper> list) {
            this.a = list;
        }

        @DexIgnore
        public final List<AddressWrapper> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && wd4.a((Object) this.a, (Object) ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<AddressWrapper> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(addresses=" + this.a + ")";
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = CommuteTimeWatchAppSettingsViewModel.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeWatchAppSetti\u2026el::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel(fn2 fn2, UserRepository userRepository) {
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(userRepository, "mUserRepository");
        this.g = userRepository;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting c() {
        return this.d;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting d() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List<AddressWrapper>) new ArrayList());
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.d;
        if (commuteTimeWatchAppSetting2 != null) {
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting2.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T next : addresses) {
                if (!TextUtils.isEmpty(((AddressWrapper) next).getAddress())) {
                    arrayList.add(next);
                }
            }
            commuteTimeWatchAppSetting.setAddresses(wb4.d(arrayList));
        }
        return commuteTimeWatchAppSetting;
    }

    @DexIgnore
    public final MutableLiveData<b> e() {
        return this.f;
    }

    @DexIgnore
    public final boolean f() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.d;
        if (commuteTimeWatchAppSetting == null || commuteTimeWatchAppSetting.getAddresses().size() >= 10) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void g() {
        ri4 unused = mg4.b(kc.a(this), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeWatchAppSettingsViewModel$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.c.a(str, CommuteTimeWatchAppSetting.class);
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = addresses.iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (((AddressWrapper) next).getType() == AddressWrapper.AddressType.HOME) {
                    z = true;
                }
                if (z) {
                    arrayList.add(next);
                }
            }
            List<AddressWrapper> addresses2 = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList2 = new ArrayList();
            for (T next2 : addresses2) {
                if (((AddressWrapper) next2).getType() == AddressWrapper.AddressType.WORK) {
                    arrayList2.add(next2);
                }
            }
            if (arrayList.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(0, new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME));
            }
            if (arrayList2.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(1, new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = h;
            local.d(str2, "exception when parse commute time setting " + e2);
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (rd4) null);
        }
        this.d = commuteTimeWatchAppSetting;
        if (this.d == null) {
            this.d = new CommuteTimeWatchAppSetting((List) null, 1, (rd4) null);
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.d;
        if (commuteTimeWatchAppSetting != null && addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (commuteTimeWatchAppSetting != null) {
                int i = -1;
                int i2 = 0;
                for (T next : commuteTimeWatchAppSetting.getAddresses()) {
                    int i3 = i2 + 1;
                    if (i2 >= 0) {
                        if (wd4.a((Object) addressWrapper.getId(), (Object) ((AddressWrapper) next).getId())) {
                            i = i2;
                        }
                        i2 = i3;
                    } else {
                        ob4.c();
                        throw null;
                    }
                }
                if (i != -1) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.d;
                    if (commuteTimeWatchAppSetting2 != null) {
                        commuteTimeWatchAppSetting2.getAddresses().remove(i);
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.d;
                        if (commuteTimeWatchAppSetting3 != null) {
                            commuteTimeWatchAppSetting3.getAddresses().add(i, addressWrapper);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting4 = this.d;
                    if (commuteTimeWatchAppSetting4 != null) {
                        commuteTimeWatchAppSetting4.getAddresses().add(addressWrapper);
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting5 = this.d;
                if (commuteTimeWatchAppSetting5 != null) {
                    list = commuteTimeWatchAppSetting5.getAddresses();
                }
                a(list);
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        this.f.a(new b(list));
    }

    @DexIgnore
    public final void b(AddressWrapper addressWrapper) {
        if (addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (addressWrapper.getType() == AddressWrapper.AddressType.OTHER) {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.d;
                if (commuteTimeWatchAppSetting != null) {
                    commuteTimeWatchAppSetting.getAddresses().remove(addressWrapper);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.d;
                if (commuteTimeWatchAppSetting2 != null) {
                    commuteTimeWatchAppSetting2.getAddresses();
                    if (wd4.a((Object) addressWrapper.getId(), (Object) addressWrapper.getId())) {
                        addressWrapper.setAddress("");
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.d;
            if (commuteTimeWatchAppSetting3 != null) {
                list = commuteTimeWatchAppSetting3.getAddresses();
            }
            a(list);
        }
    }
}
