package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.hd3;
import com.fossil.blesdk.obfuscated.ld3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.rd3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tf2;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.wd3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewFragment extends as2 {
    @DexIgnore
    public ur3<tf2> j;
    @DexIgnore
    public SleepOverviewDayPresenter k;
    @DexIgnore
    public SleepOverviewWeekPresenter l;
    @DexIgnore
    public SleepOverviewMonthPresenter m;
    @DexIgnore
    public hd3 n;
    @DexIgnore
    public wd3 o;
    @DexIgnore
    public rd3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public b(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            ur3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(7, a != null ? (tf2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public c(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            ur3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(4, a != null ? (tf2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public d(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            ur3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(2, a != null ? (tf2) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onCreateView");
        tf2 tf2 = (tf2) ra.a(layoutInflater, R.layout.fragment_sleep_overview, viewGroup, false, O0());
        g9.c((View) tf2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) tf2, "binding");
        a(tf2);
        this.j = new ur3<>(this, tf2);
        ur3<tf2> ur3 = this.j;
        if (ur3 != null) {
            tf2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(tf2 tf2) {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "initUI");
        this.n = (hd3) getChildFragmentManager().a("SleepOverviewDayFragment");
        this.o = (wd3) getChildFragmentManager().a("SleepOverviewWeekFragment");
        this.p = (rd3) getChildFragmentManager().a("SleepOverviewMonthFragment");
        if (this.n == null) {
            this.n = new hd3();
        }
        if (this.o == null) {
            this.o = new wd3();
        }
        if (this.p == null) {
            this.p = new rd3();
        }
        ArrayList arrayList = new ArrayList();
        hd3 hd3 = this.n;
        if (hd3 != null) {
            arrayList.add(hd3);
            wd3 wd3 = this.o;
            if (wd3 != null) {
                arrayList.add(wd3);
                rd3 rd3 = this.p;
                if (rd3 != null) {
                    arrayList.add(rd3);
                    RecyclerView recyclerView = tf2.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new SleepOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, tf2);
                    m42 g = PortfolioApp.W.c().g();
                    hd3 hd32 = this.n;
                    if (hd32 != null) {
                        wd3 wd32 = this.o;
                        if (wd32 != null) {
                            rd3 rd32 = this.p;
                            if (rd32 != null) {
                                g.a(new ld3(hd32, wd32, rd32)).a(this);
                                tf2.s.setOnClickListener(new b(this));
                                tf2.q.setOnClickListener(new c(this));
                                tf2.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, tf2 tf2) {
        if (tf2 != null) {
            FlexibleTextView flexibleTextView = tf2.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = tf2.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = tf2.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = tf2.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = tf2.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = tf2.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = tf2.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = tf2.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = tf2.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<tf2> ur3 = this.j;
                if (ur3 != null) {
                    tf2 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = tf2.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = tf2.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = tf2.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<tf2> ur32 = this.j;
                if (ur32 != null) {
                    tf2 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = tf2.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = tf2.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = tf2.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<tf2> ur33 = this.j;
                if (ur33 != null) {
                    tf2 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = tf2.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = tf2.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = tf2.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<tf2> ur34 = this.j;
                if (ur34 != null) {
                    tf2 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
