package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1", f = "HeartRateOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
public final class HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends Integer>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter$start$Anon1.Anon2.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1(HeartRateOverviewWeekPresenter$start$Anon1.Anon2.Anon1 anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 = new HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1(this.this$Anon0, kc4);
        heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1.p$ = (lh4) obj;
        return heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            List<DailyHeartRateSummary> list = this.this$Anon0.$data;
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(pb4.a(list, 10));
            for (DailyHeartRateSummary resting : list) {
                Resting resting2 = resting.getResting();
                arrayList.add(resting2 != null ? pc4.a(resting2.getValue()) : null);
            }
            return arrayList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
