package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.DianaPresetRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $newPreset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter$createNewPreset$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(DianaPreset dianaPreset, kc4 kc4, HomeDianaCustomizePresenter$createNewPreset$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1) {
        super(2, kc4);
        this.$newPreset = dianaPreset;
        this.this$Anon0 = homeDianaCustomizePresenter$createNewPreset$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$newPreset, kc4, this.this$Anon0);
        homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            DianaPresetRepository j = this.this$Anon0.this$Anon0.w;
            DianaPreset dianaPreset = this.$newPreset;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (j.upsertPreset(dianaPreset, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
