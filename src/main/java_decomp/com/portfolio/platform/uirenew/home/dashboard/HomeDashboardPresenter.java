package com.portfolio.platform.uirenew.home.dashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tu2;
import com.fossil.blesdk.obfuscated.v73;
import com.fossil.blesdk.obfuscated.w73;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.enums.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDashboardPresenter extends v73 {
    @DexIgnore
    public /* final */ wj2 A;
    @DexIgnore
    public /* final */ SummariesRepository B;
    @DexIgnore
    public /* final */ GoalTrackingRepository C;
    @DexIgnore
    public /* final */ SleepSummariesRepository D;
    @DexIgnore
    public /* final */ HeartRateSampleRepository E;
    @DexIgnore
    public Date f;
    @DexIgnore
    public int g;
    @DexIgnore
    public String h; // = this.y.e();
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE i; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.h);
    @DexIgnore
    public MFSleepDay j;
    @DexIgnore
    public ActivitySummary k;
    @DexIgnore
    public GoalTrackingSummary l;
    @DexIgnore
    public Integer m;
    @DexIgnore
    public List<HeartRateSample> n;
    @DexIgnore
    public HashMap<Integer, Boolean> o; // = new HashMap<>();
    @DexIgnore
    public boolean p;
    @DexIgnore
    public volatile boolean q;
    @DexIgnore
    public /* final */ b r; // = new b();
    @DexIgnore
    public LiveData<ps3<ActivitySummary>> s; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<MFSleepDay>> t; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<GoalTrackingSummary>> u; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<Integer>> v; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<List<HeartRateSample>>> w; // = new MutableLiveData();
    @DexIgnore
    public /* final */ w73 x;
    @DexIgnore
    public /* final */ PortfolioApp y;
    @DexIgnore
    public /* final */ DeviceRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            wd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements dc<ps3<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public c(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(ps3<ActivitySummary> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSummaryLiveData -- summary=" + ps3 + ", mFirstDBLoad=" + ((Boolean) this.a.o.get(0)));
            ActivitySummary activitySummary = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    activitySummary = ps3.d();
                }
                if ((!wd4.a((Object) (Boolean) this.a.o.get(0), (Object) true)) || (!wd4.a((Object) this.a.k, (Object) activitySummary))) {
                    this.a.k = activitySummary;
                    this.a.o();
                    this.a.o.put(0, true);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements dc<ps3<? extends MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public d(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(ps3<MFSleepDay> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSleepSummaryLiveData -- summary=" + ps3 + ", mFirstDBLoad=" + ((Boolean) this.a.o.get(5)));
            MFSleepDay mFSleepDay = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    mFSleepDay = ps3.d();
                }
                if ((!wd4.a((Object) (Boolean) this.a.o.get(5), (Object) true)) || (!wd4.a((Object) this.a.j, (Object) mFSleepDay))) {
                    this.a.j = mFSleepDay;
                    this.a.o();
                    this.a.o.put(5, true);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements dc<ps3<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public e(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(ps3<GoalTrackingSummary> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalTrackingSummaryLiveData -- summary=" + ps3 + ", mFirstDBLoad=" + ((Boolean) this.a.o.get(4)));
            GoalTrackingSummary goalTrackingSummary = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    goalTrackingSummary = ps3.d();
                }
                if ((!wd4.a((Object) (Boolean) this.a.o.get(4), (Object) true)) || (!wd4.a((Object) this.a.l, (Object) goalTrackingSummary))) {
                    this.a.l = goalTrackingSummary;
                    this.a.o();
                    this.a.o.put(4, true);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements dc<ps3<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public f(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(ps3<Integer> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- resource=" + ps3);
            Integer num = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    num = ps3.d();
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- goal=" + num);
                if (!wd4.a((Object) this.a.m, (Object) num)) {
                    this.a.m = num;
                    this.a.o();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements dc<ps3<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public g(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(ps3<? extends List<HeartRateSample>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSampleLiveData -- resource.status=");
            sb.append(a2);
            sb.append(", ");
            sb.append("data.size=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", mFirstDBLoad=");
            sb.append((Boolean) this.a.o.get(3));
            local.d("HomeDashboardPresenter", sb.toString());
            if (a2 == Status.DATABASE_LOADING) {
                return;
            }
            if ((!wd4.a((Object) (Boolean) this.a.o.get(3), (Object) true)) || (!wd4.a((Object) this.a.n, (Object) list))) {
                this.a.n = list;
                this.a.o();
                this.a.o.put(3, true);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HomeDashboardPresenter(w73 w73, PortfolioApp portfolioApp, DeviceRepository deviceRepository, wj2 wj2, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository) {
        wd4.b(w73, "mView");
        wd4.b(portfolioApp, "mApp");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(wj2, "mDeviceSettingFactory");
        wd4.b(summariesRepository, "mSummaryRepository");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(sleepSummariesRepository, "mSleepSummaryRepository");
        wd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        this.x = w73;
        this.y = portfolioApp;
        this.z = deviceRepository;
        this.A = wj2;
        this.B = summariesRepository;
        this.C = goalTrackingRepository;
        this.D = sleepSummariesRepository;
        this.E = heartRateSampleRepository;
    }

    @DexIgnore
    public final void o() {
        Integer num;
        HeartRateSample heartRateSample;
        int i2;
        boolean z2;
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "updateHomeInfo");
        this.x.a(this.k, this.j, this.l);
        List<HeartRateSample> list = this.n;
        if (list != null) {
            ListIterator<HeartRateSample> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    heartRateSample = null;
                    break;
                }
                heartRateSample = listIterator.previous();
                if (heartRateSample.getResting() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            HeartRateSample heartRateSample2 = heartRateSample;
            if (heartRateSample2 != null) {
                Resting resting = heartRateSample2.getResting();
                if (resting != null) {
                    i2 = resting.getValue();
                    num = Integer.valueOf(i2);
                }
            }
            i2 = 0;
            num = Integer.valueOf(i2);
        } else {
            num = null;
        }
        this.x.a(this.k, this.j, this.l, this.m, num, (this.h.length() == 0) && this.p);
        w73 w73 = this.x;
        Date date = this.f;
        if (date != null) {
            w73.a(date);
        } else {
            wd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        this.x.b(z2);
    }

    @DexIgnore
    public final void c(int i2) {
        this.x.b(i2);
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "start - this=" + hashCode());
        l();
        PortfolioApp portfolioApp = this.y;
        b bVar = this.r;
        portfolioApp.registerReceiver(bVar, new IntentFilter(this.y.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        k();
        LiveData<ps3<ActivitySummary>> liveData = this.s;
        w73 w73 = this.x;
        if (w73 != null) {
            liveData.a((tu2) w73, new c(this));
            this.t.a((LifecycleOwner) this.x, new d(this));
            this.u.a((LifecycleOwner) this.x, new e(this));
            this.v.a((LifecycleOwner) this.x, new f(this));
            this.w.a((LifecycleOwner) this.x, new g(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "stop");
        try {
            this.y.unregisterReceiver(this.r);
            w73 w73 = this.x;
            if (w73 != null) {
                tu2 tu2 = (tu2) w73;
                this.s.a((LifecycleOwner) tu2);
                this.t.a((LifecycleOwner) tu2);
                this.u.a((LifecycleOwner) tu2);
                this.v.a((LifecycleOwner) tu2);
                this.w.a((LifecycleOwner) tu2);
                PortfolioApp.W.c().f().a((LifecycleOwner) tu2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeDashboardPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.i;
        wd4.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public int i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        String e2 = this.y.e();
        int f2 = this.y.f(e2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "Inside .onRefresh currentDeviceSession=" + f2);
        if (TextUtils.isEmpty(e2) || f2 == CommunicateMode.OTA.getValue()) {
            this.x.j(false);
            return;
        }
        this.q = true;
        this.x.V();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.y.e(), "HomeDashboardPresenter", "[Sync Start] PULL TO SYNC");
        this.y.a(this.A, false, 12);
    }

    @DexIgnore
    public final ri4 k() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDashboardPresenter$checkDeviceStatus$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "loadData");
        this.f = new Date();
        this.o = new HashMap<>();
        SummariesRepository summariesRepository = this.B;
        Date date = this.f;
        if (date != null) {
            this.s = summariesRepository.getSummary(date);
            SleepSummariesRepository sleepSummariesRepository = this.D;
            Date date2 = this.f;
            if (date2 != null) {
                this.t = sleepSummariesRepository.getSleepSummary(date2);
                GoalTrackingRepository goalTrackingRepository = this.C;
                Date date3 = this.f;
                if (date3 != null) {
                    this.u = goalTrackingRepository.getSummary(date3);
                    this.v = this.C.getLastGoalSetting();
                    HeartRateSampleRepository heartRateSampleRepository = this.E;
                    Date date4 = this.f;
                    if (date4 == null) {
                        wd4.d("mDate");
                        throw null;
                    } else if (date4 != null) {
                        this.w = heartRateSampleRepository.getHeartRateSamples(date4, date4, true);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("loadData - mDate=");
                        Date date5 = this.f;
                        if (date5 != null) {
                            sb.append(date5);
                            local.d("HomeDashboardPresenter", sb.toString());
                            return;
                        }
                        wd4.d("mDate");
                        throw null;
                    } else {
                        wd4.d("mDate");
                        throw null;
                    }
                } else {
                    wd4.d("mDate");
                    throw null;
                }
            } else {
                wd4.d("mDate");
                throw null;
            }
        } else {
            wd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.x.S();
    }

    @DexIgnore
    public void n() {
        this.x.a(this);
    }

    @DexIgnore
    public void b(int i2) {
        if (i2 == this.g) {
            this.x.z();
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "buildMode");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDashboardPresenter$showLayoutBattery$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str, boolean z2) {
        wd4.b(str, "activeId");
        this.y.a(str, z2);
    }

    @DexIgnore
    public void a(Intent intent) {
        wd4.b(intent, "intent");
        if (this.x.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && cg4.b(stringExtra, this.h, true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardPresenter", "mSyncReceiver - syncStatus: " + intExtra + " - mIntendingSync: " + this.q);
                if (intExtra != 0) {
                    if (intExtra == 1) {
                        this.q = false;
                        this.x.j(true);
                    } else if (intExtra == 2 || intExtra == 4) {
                        this.q = false;
                        this.x.j(false);
                        if (intExtra2 == 12) {
                            int intExtra3 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                            if (integerArrayListExtra == null) {
                                integerArrayListExtra = new ArrayList<>();
                            }
                            if (intExtra3 != 1101) {
                                if (intExtra3 == 1603) {
                                    this.x.j(false);
                                    return;
                                } else if (!(intExtra3 == 1112 || intExtra3 == 1113)) {
                                    this.x.U();
                                    return;
                                }
                            }
                            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(integerArrayListExtra);
                            wd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                            w73 w73 = this.x;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
                            if (array != null) {
                                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                                w73.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    } else if (intExtra == 5) {
                        this.x.C();
                    }
                } else if (!this.q) {
                    this.q = true;
                    this.x.V();
                }
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        this.q = z2;
    }
}
