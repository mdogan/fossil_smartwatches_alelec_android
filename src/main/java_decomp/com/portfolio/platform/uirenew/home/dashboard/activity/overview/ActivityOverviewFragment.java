package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.d92;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.h93;
import com.fossil.blesdk.obfuscated.l93;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.r93;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.w93;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityOverviewFragment extends as2 {
    @DexIgnore
    public ur3<d92> j;
    @DexIgnore
    public ActivityOverviewDayPresenter k;
    @DexIgnore
    public ActivityOverviewWeekPresenter l;
    @DexIgnore
    public ActivityOverviewMonthPresenter m;
    @DexIgnore
    public h93 n;
    @DexIgnore
    public w93 o;
    @DexIgnore
    public r93 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public b(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            ur3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(7, a != null ? (d92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public c(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            ur3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(4, a != null ? (d92) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewFragment e;

        @DexIgnore
        public d(ActivityOverviewFragment activityOverviewFragment) {
            this.e = activityOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActivityOverviewFragment activityOverviewFragment = this.e;
            ur3 a = activityOverviewFragment.j;
            activityOverviewFragment.a(2, a != null ? (d92) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "onCreateView");
        d92 d92 = (d92) ra.a(layoutInflater, R.layout.fragment_activity_overview, viewGroup, false, O0());
        g9.c((View) d92.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) d92, "binding");
        a(d92);
        this.j = new ur3<>(this, d92);
        ur3<d92> ur3 = this.j;
        if (ur3 != null) {
            d92 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(d92 d92) {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewFragment", "initUI");
        this.n = (h93) getChildFragmentManager().a("ActivityOverviewDayFragment");
        this.o = (w93) getChildFragmentManager().a("ActivityOverviewWeekFragment");
        this.p = (r93) getChildFragmentManager().a("ActivityOverviewMonthFragment");
        if (this.n == null) {
            this.n = new h93();
        }
        if (this.o == null) {
            this.o = new w93();
        }
        if (this.p == null) {
            this.p = new r93();
        }
        ArrayList arrayList = new ArrayList();
        h93 h93 = this.n;
        if (h93 != null) {
            arrayList.add(h93);
            w93 w93 = this.o;
            if (w93 != null) {
                arrayList.add(w93);
                r93 r93 = this.p;
                if (r93 != null) {
                    arrayList.add(r93);
                    RecyclerView recyclerView = d92.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActivityOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, d92);
                    m42 g = PortfolioApp.W.c().g();
                    h93 h932 = this.n;
                    if (h932 != null) {
                        w93 w932 = this.o;
                        if (w932 != null) {
                            r93 r932 = this.p;
                            if (r932 != null) {
                                g.a(new l93(h932, w932, r932)).a(this);
                                d92.s.setOnClickListener(new b(this));
                                d92.q.setOnClickListener(new c(this));
                                d92.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, d92 d92) {
        if (d92 != null) {
            FlexibleTextView flexibleTextView = d92.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = d92.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = d92.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = d92.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = d92.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = d92.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = d92.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = d92.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = d92.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<d92> ur3 = this.j;
                if (ur3 != null) {
                    d92 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = d92.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = d92.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = d92.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<d92> ur32 = this.j;
                if (ur32 != null) {
                    d92 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = d92.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = d92.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = d92.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<d92> ur33 = this.j;
                if (ur33 != null) {
                    d92 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = d92.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = d92.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = d92.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<d92> ur34 = this.j;
                if (ur34 != null) {
                    d92 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
