package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.fb3;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.gb3;
import com.fossil.blesdk.obfuscated.hb3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewDayPresenter extends fb3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<ps3<GoalTrackingSummary>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<List<GoalTrackingData>>> j; // = new MutableLiveData();
    @DexIgnore
    public /* final */ gb3 k;
    @DexIgnore
    public /* final */ fn2 l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<ps3<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(ps3<GoalTrackingSummary> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "start - mGoalTrackingSummary -- summary=" + ps3);
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    ri4 unused = this.a.j();
                }
                this.a.k.c(true ^ this.a.l.H());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements dc<ps3<? extends List<GoalTrackingData>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public c(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(ps3<? extends List<GoalTrackingData>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingData -- goalTrackingSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    ri4 unused = this.a.j();
                }
                this.a.k.c(true ^ this.a.l.H());
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewDayPresenter(gb3 gb3, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        wd4.b(gb3, "mView");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.k = gb3;
        this.l = fn2;
        this.m = goalTrackingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
        Date date = goalTrackingOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        wd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public final ri4 j() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<GoalTrackingSummary>> liveData = this.i;
        gb3 gb3 = this.k;
        if (gb3 != null) {
            liveData.a((hb3) gb3, new b(this));
            this.j.a((LifecycleOwner) this.k, new c(this));
            this.k.c(!this.l.H());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "stop");
        try {
            LiveData<ps3<List<GoalTrackingData>>> liveData = this.j;
            gb3 gb3 = this.k;
            if (gb3 != null) {
                liveData.a((LifecycleOwner) (hb3) gb3);
                this.i.a((LifecycleOwner) this.k);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wd4.d("mDate");
                throw null;
            } else if (sk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("GoalTrackingOverviewDayPresenter", sb.toString());
                    return;
                }
                wd4.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("GoalTrackingOverviewDayPresenter", sb2.toString());
            GoalTrackingRepository goalTrackingRepository = this.m;
            Date date4 = this.f;
            if (date4 != null) {
                this.i = goalTrackingRepository.getSummary(date4);
                GoalTrackingRepository goalTrackingRepository2 = this.m;
                Date date5 = this.f;
                if (date5 == null) {
                    wd4.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.j = goalTrackingRepository2.getGoalTrackingDataList(date5, date5, true);
                } else {
                    wd4.d("mDate");
                    throw null;
                }
            } else {
                wd4.d("mDate");
                throw null;
            }
        } else {
            wd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.k.a(this);
    }
}
