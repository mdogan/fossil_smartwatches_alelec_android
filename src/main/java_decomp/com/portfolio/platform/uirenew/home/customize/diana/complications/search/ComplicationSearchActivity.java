package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.d43;
import com.fossil.blesdk.obfuscated.e43;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationSearchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public ComplicationSearchPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3, String str4) {
            wd4.b(fragment, "fragment");
            wd4.b(str, "topComplication");
            wd4.b(str2, "bottomComplication");
            wd4.b(str3, "leftComplication");
            wd4.b(str4, "rightComplication");
            Intent intent = new Intent(fragment.getContext(), ComplicationSearchActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("bottom", str2);
            intent.putExtra(ViewHierarchy.DIMENSION_LEFT_KEY, str3);
            intent.putExtra("right", str4);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        d43 d43 = (d43) getSupportFragmentManager().a((int) R.id.content);
        if (d43 == null) {
            d43 = d43.o.b();
            a((Fragment) d43, d43.o.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (d43 != null) {
            g.a(new e43(d43)).a(this);
            Intent intent = getIntent();
            wd4.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                ComplicationSearchPresenter complicationSearchPresenter = this.B;
                if (complicationSearchPresenter != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("bottom");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("right");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    String string4 = extras.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    complicationSearchPresenter.a(string, string2, string3, string4);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                ComplicationSearchPresenter complicationSearchPresenter2 = this.B;
                if (complicationSearchPresenter2 != null) {
                    String string5 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    String string7 = bundle.getString("right");
                    if (string7 == null) {
                        string7 = "empty";
                    }
                    String string8 = bundle.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string8 == null) {
                        string8 = "empty";
                    }
                    complicationSearchPresenter2.a(string5, string6, string7, string8);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchContract.View");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        ComplicationSearchPresenter complicationSearchPresenter = this.B;
        if (complicationSearchPresenter != null) {
            complicationSearchPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }
}
