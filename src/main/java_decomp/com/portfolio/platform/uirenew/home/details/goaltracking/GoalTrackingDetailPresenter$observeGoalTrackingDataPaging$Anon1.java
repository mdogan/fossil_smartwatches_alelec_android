package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.ri4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1<T> implements dc<rd<GoalTrackingData>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

    @DexIgnore
    public GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        this.a = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final void a(rd<GoalTrackingData> rdVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getGoalTrackingDataPaging observer size=");
        sb.append(rdVar != null ? Integer.valueOf(rdVar.size()) : null);
        local.d("GoalTrackingDetailPresenter", sb.toString());
        if (rdVar != null) {
            this.a.j = true;
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1(rdVar, (kc4) null, this), 3, (Object) null);
        }
    }
}
