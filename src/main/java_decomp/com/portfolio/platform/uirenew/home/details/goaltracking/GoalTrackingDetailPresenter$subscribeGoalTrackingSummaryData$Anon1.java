package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.enums.Status;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1<T> implements dc<ps3<? extends List<GoalTrackingSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {120}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1 goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 a2 = this.this$Anon0.a.b();
                GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1 goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1 = new GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) obj;
            if (this.this$Anon0.a.m == null || (!wd4.a((Object) this.this$Anon0.a.m, (Object) goalTrackingSummary))) {
                this.this$Anon0.a.m = goalTrackingSummary;
                this.this$Anon0.a.q.a(this.this$Anon0.a.m);
                if (this.this$Anon0.a.i && this.this$Anon0.a.j) {
                    ri4 unused = this.this$Anon0.a.m();
                }
            }
            return cb4.a;
        }
    }

    @DexIgnore
    public GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        this.a = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final void a(ps3<? extends List<GoalTrackingSummary>> ps3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + ps3);
        if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
            this.a.l = ps3 != null ? (List) ps3.d() : null;
            this.a.i = true;
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        }
    }
}
