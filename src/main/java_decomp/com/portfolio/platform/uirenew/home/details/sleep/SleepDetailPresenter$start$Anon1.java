package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.enums.Status;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDetailPresenter$start$Anon1<T> implements dc<ps3<? extends List<MFSleepDay>>> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$Anon1$Anon1", f = "SleepDetailPresenter.kt", l = {82}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepDetailPresenter$start$Anon1 sleepDetailPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = sleepDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 a2 = this.this$Anon0.a.b();
                SleepDetailPresenter$start$Anon1$Anon1$summary$Anon1 sleepDetailPresenter$start$Anon1$Anon1$summary$Anon1 = new SleepDetailPresenter$start$Anon1$Anon1$summary$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, sleepDetailPresenter$start$Anon1$Anon1$summary$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFSleepDay mFSleepDay = (MFSleepDay) obj;
            if (this.this$Anon0.a.n == null || (!wd4.a((Object) this.this$Anon0.a.n, (Object) mFSleepDay))) {
                this.this$Anon0.a.n = mFSleepDay;
                this.this$Anon0.a.r.a(mFSleepDay);
                if (this.this$Anon0.a.j && this.this$Anon0.a.k) {
                    ri4 unused = this.this$Anon0.a.k();
                    ri4 unused2 = this.this$Anon0.a.l();
                }
            }
            return cb4.a;
        }
    }

    @DexIgnore
    public SleepDetailPresenter$start$Anon1(SleepDetailPresenter sleepDetailPresenter) {
        this.a = sleepDetailPresenter;
    }

    @DexIgnore
    public final void a(ps3<? extends List<MFSleepDay>> ps3) {
        Status a2 = ps3.a();
        List list = (List) ps3.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - summaryTransformations -- sleepSummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a2);
        local.d("SleepDetailPresenter", sb.toString());
        if (a2 == Status.NETWORK_LOADING || a2 == Status.SUCCESS) {
            this.a.l = list;
            this.a.j = true;
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        }
    }
}
