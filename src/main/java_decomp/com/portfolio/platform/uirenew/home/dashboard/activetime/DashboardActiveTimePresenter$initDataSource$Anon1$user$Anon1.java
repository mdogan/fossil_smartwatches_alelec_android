package com.portfolio.platform.uirenew.home.dashboard.activetime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1", f = "DashboardActiveTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardActiveTimePresenter$initDataSource$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1(DashboardActiveTimePresenter$initDataSource$Anon1 dashboardActiveTimePresenter$initDataSource$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dashboardActiveTimePresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1 dashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1 = new DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1(this.this$Anon0, kc4);
        dashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1.p$ = (lh4) obj;
        return dashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardActiveTimePresenter$initDataSource$Anon1$user$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return this.this$Anon0.this$Anon0.m.getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
