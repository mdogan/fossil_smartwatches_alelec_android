package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.uh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$Anon1", f = "HomePresenter.kt", l = {315}, m = "invokeSuspend")
public final class HomePresenter$checkIfHappyUser$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $hasServerSettings;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$Anon1$Anon1", f = "HomePresenter.kt", l = {335, 336}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int I$Anon0;
        @DexIgnore
        public int I$Anon1;
        @DexIgnore
        public long J$Anon0;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public Object L$Anon3;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter$checkIfHappyUser$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$Anon1$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$Anon1$Anon1$Anon1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0132Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0132Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0132Anon1 anon1 = new C0132Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0132Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    this.this$Anon0.this$Anon0.this$Anon0.l.M();
                    return cb4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomePresenter$checkIfHappyUser$Anon1 homePresenter$checkIfHappyUser$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homePresenter$checkIfHappyUser$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00dd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x00f3  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0166 A[RETURN] */
        public final Object invokeSuspend(Object obj) {
            long j;
            lh4 lh4;
            ServerSetting serverSetting;
            ServerSetting serverSetting2;
            ServerSetting serverSetting3;
            int i;
            int i2;
            String value;
            String value2;
            Object a = oc4.a();
            int i3 = this.label;
            if (i3 == 0) {
                za4.a(obj);
                lh4 = this.p$;
                ServerSetting generateSetting = this.this$Anon0.this$Anon0.u.generateSetting("crash-threshold", String.valueOf(3));
                ServerSetting generateSetting2 = this.this$Anon0.this$Anon0.u.generateSetting("successful-sync-threshold", String.valueOf(4));
                serverSetting3 = this.this$Anon0.this$Anon0.u.generateSetting("post-sync-threshold", String.valueOf(5));
                HomePresenter$checkIfHappyUser$Anon1 homePresenter$checkIfHappyUser$Anon1 = this.this$Anon0;
                if (homePresenter$checkIfHappyUser$Anon1.$hasServerSettings) {
                    ServerSetting serverSettingByKey = homePresenter$checkIfHappyUser$Anon1.this$Anon0.u.getServerSettingByKey("crash-threshold");
                    if (serverSettingByKey != null) {
                        generateSetting = serverSettingByKey;
                    }
                    ServerSetting serverSettingByKey2 = this.this$Anon0.this$Anon0.u.getServerSettingByKey("successful-sync-threshold");
                    if (serverSettingByKey2 != null) {
                        generateSetting2 = serverSettingByKey2;
                    }
                    ServerSetting serverSettingByKey3 = this.this$Anon0.this$Anon0.u.getServerSettingByKey("post-sync-threshold");
                    if (serverSettingByKey3 == null) {
                        serverSettingByKey3 = serverSetting3;
                    }
                    serverSetting = generateSetting;
                    serverSetting2 = generateSetting2;
                    serverSetting3 = serverSettingByKey3;
                } else {
                    serverSetting = generateSetting;
                    serverSetting2 = generateSetting2;
                }
                String value3 = serverSetting.getValue();
                if (value3 != null) {
                    Integer a2 = pc4.a(Integer.parseInt(value3));
                    if (a2 != null) {
                        i2 = a2.intValue();
                        value = serverSetting2.getValue();
                        if (value != null) {
                            Integer a3 = pc4.a(Integer.parseInt(value));
                            if (a3 != null) {
                                i = a3.intValue();
                                value2 = serverSetting3.getValue();
                                if (value2 != null) {
                                    Long a4 = pc4.a(Long.parseLong(value2));
                                    if (a4 != null) {
                                        j = a4.longValue();
                                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                        String a5 = HomePresenter.y.a();
                                        local.d(a5, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                                        if (this.this$Anon0.this$Anon0.t.a() && this.this$Anon0.this$Anon0.t.a(i2, i)) {
                                            this.L$Anon0 = lh4;
                                            this.L$Anon1 = serverSetting;
                                            this.L$Anon2 = serverSetting2;
                                            this.L$Anon3 = serverSetting3;
                                            this.I$Anon0 = i2;
                                            this.I$Anon1 = i;
                                            this.J$Anon0 = j;
                                            this.label = 1;
                                            if (uh4.a(j, this) == a) {
                                                return a;
                                            }
                                        }
                                        return cb4.a;
                                    }
                                }
                                j = 0;
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String a52 = HomePresenter.y.a();
                                local2.d(a52, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                                this.L$Anon0 = lh4;
                                this.L$Anon1 = serverSetting;
                                this.L$Anon2 = serverSetting2;
                                this.L$Anon3 = serverSetting3;
                                this.I$Anon0 = i2;
                                this.I$Anon1 = i;
                                this.J$Anon0 = j;
                                this.label = 1;
                                if (uh4.a(j, this) == a) {
                                }
                            }
                        }
                        i = 0;
                        value2 = serverSetting3.getValue();
                        if (value2 != null) {
                        }
                        j = 0;
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        String a522 = HomePresenter.y.a();
                        local22.d(a522, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                        this.L$Anon0 = lh4;
                        this.L$Anon1 = serverSetting;
                        this.L$Anon2 = serverSetting2;
                        this.L$Anon3 = serverSetting3;
                        this.I$Anon0 = i2;
                        this.I$Anon1 = i;
                        this.J$Anon0 = j;
                        this.label = 1;
                        if (uh4.a(j, this) == a) {
                        }
                    }
                }
                i2 = 0;
                value = serverSetting2.getValue();
                if (value != null) {
                }
                i = 0;
                value2 = serverSetting3.getValue();
                if (value2 != null) {
                }
                j = 0;
                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                String a5222 = HomePresenter.y.a();
                local222.d(a5222, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                this.L$Anon0 = lh4;
                this.L$Anon1 = serverSetting;
                this.L$Anon2 = serverSetting2;
                this.L$Anon3 = serverSetting3;
                this.I$Anon0 = i2;
                this.I$Anon1 = i;
                this.J$Anon0 = j;
                this.label = 1;
                if (uh4.a(j, this) == a) {
                }
            } else if (i3 == 1) {
                long j2 = this.J$Anon0;
                int i4 = this.I$Anon1;
                int i5 = this.I$Anon0;
                serverSetting3 = (ServerSetting) this.L$Anon3;
                serverSetting2 = (ServerSetting) this.L$Anon2;
                serverSetting = (ServerSetting) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                j = j2;
                i = i4;
                i2 = i5;
            } else if (i3 == 2) {
                ServerSetting serverSetting4 = (ServerSetting) this.L$Anon3;
                ServerSetting serverSetting5 = (ServerSetting) this.L$Anon2;
                ServerSetting serverSetting6 = (ServerSetting) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                this.this$Anon0.this$Anon0.m.r(false);
                return cb4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            bj4 j3 = this.this$Anon0.this$Anon0.d();
            C0132Anon1 anon1 = new C0132Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = serverSetting;
            this.L$Anon2 = serverSetting2;
            this.L$Anon3 = serverSetting3;
            this.I$Anon0 = i2;
            this.I$Anon1 = i;
            this.J$Anon0 = j;
            this.label = 2;
            if (kg4.a(j3, anon1, this) == a) {
                return a;
            }
            this.this$Anon0.this$Anon0.m.r(false);
            return cb4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkIfHappyUser$Anon1(HomePresenter homePresenter, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homePresenter;
        this.$hasServerSettings = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomePresenter$checkIfHappyUser$Anon1 homePresenter$checkIfHappyUser$Anon1 = new HomePresenter$checkIfHappyUser$Anon1(this.this$Anon0, this.$hasServerSettings, kc4);
        homePresenter$checkIfHappyUser$Anon1.p$ = (lh4) obj;
        return homePresenter$checkIfHappyUser$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomePresenter$checkIfHappyUser$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
