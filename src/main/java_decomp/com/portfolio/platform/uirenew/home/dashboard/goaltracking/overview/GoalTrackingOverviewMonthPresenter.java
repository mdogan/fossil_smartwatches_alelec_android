package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pb3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qb3;
import com.fossil.blesdk.obfuscated.rb3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewMonthPresenter extends pb3 {
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public List<GoalTrackingSummary> k; // = new ArrayList();
    @DexIgnore
    public LiveData<ps3<List<GoalTrackingSummary>>> l; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<List<GoalTrackingSummary>>> m;
    @DexIgnore
    public TreeMap<Long, Float> n;
    @DexIgnore
    public /* final */ qb3 o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ fn2 q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter a;

        @DexIgnore
        public b(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            this.a = goalTrackingOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<GoalTrackingSummary>>> apply(Date date) {
            GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter = this.a;
            wd4.a((Object) date, "it");
            if (goalTrackingOverviewMonthPresenter.b(date)) {
                GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter2 = this.a;
                goalTrackingOverviewMonthPresenter2.l = goalTrackingOverviewMonthPresenter2.r.getSummaries(GoalTrackingOverviewMonthPresenter.k(this.a), GoalTrackingOverviewMonthPresenter.g(this.a), true);
            }
            return this.a.l;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewMonthPresenter(qb3 qb3, UserRepository userRepository, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        wd4.b(qb3, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.o = qb3;
        this.p = userRepository;
        this.q = fn2;
        this.r = goalTrackingRepository;
        LiveData<ps3<List<GoalTrackingSummary>>> b2 = ic.b(this.f, new b(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026alTrackingSummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date g(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        wd4.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date k(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        Date date = goalTrackingOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<List<GoalTrackingSummary>>> liveData = this.m;
        qb3 qb3 = this.o;
        if (qb3 != null) {
            liveData.a((rb3) qb3, new GoalTrackingOverviewMonthPresenter$start$Anon1(this));
            this.o.c(!this.q.H());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "stop");
        try {
            LiveData<ps3<List<GoalTrackingSummary>>> liveData = this.m;
            qb3 qb3 = this.o;
            if (qb3 != null) {
                liveData.a((LifecycleOwner) (rb3) qb3);
                this.l.a((LifecycleOwner) this.o);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewMonthPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                wd4.d("mCurrentDate");
                throw null;
            } else if (sk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.g;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("GoalTrackingOverviewMonthPresenter", sb.toString());
                    return;
                }
                wd4.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.g;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("GoalTrackingOverviewMonthPresenter", sb2.toString());
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingOverviewMonthPresenter$loadData$Anon2(this, (kc4) null), 3, (Object) null);
            return;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.o.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!sk2.a(date4.getTime(), date.getTime())) {
                Calendar o2 = sk2.o(date);
                wd4.a((Object) o2, "DateHelper.getStartOfMonth(date)");
                Date time = o2.getTime();
                wd4.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean r2 = sk2.r(date);
            wd4.a((Object) r2, "DateHelper.isThisMonth(date)");
            if (r2.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar j2 = sk2.j(date);
                wd4.a((Object) j2, "DateHelper.getEndOfMonth(date)");
                date2 = j2.getTime();
                wd4.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wd4.d("mStartDate");
                throw null;
            }
            wd4.d("mEndDate");
            throw null;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        if (this.f.a() == null || !sk2.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<GoalTrackingSummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        if (list != null) {
            for (GoalTrackingSummary next : list) {
                Date component1 = next.component1();
                int component2 = next.component2();
                int component3 = next.component3();
                if (component3 > 0) {
                    Date n2 = sk2.n(component1);
                    wd4.a((Object) n2, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(n2.getTime()), Float.valueOf(((float) component2) / ((float) component3)));
                } else {
                    Date n3 = sk2.n(component1);
                    wd4.a((Object) n3, "DateHelper.getStartOfDay(date1)");
                    treeMap.put(Long.valueOf(n3.getTime()), Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
