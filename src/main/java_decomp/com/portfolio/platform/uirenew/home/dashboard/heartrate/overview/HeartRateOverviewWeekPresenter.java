package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.uc3;
import com.fossil.blesdk.obfuscated.vc3;
import com.fossil.blesdk.obfuscated.wc3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewWeekPresenter extends uc3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<ps3<List<DailyHeartRateSummary>>> g;
    @DexIgnore
    public List<String> h; // = new ArrayList();
    @DexIgnore
    public /* final */ vc3 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewWeekPresenter(vc3 vc3, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        wd4.b(vc3, "mView");
        wd4.b(userRepository, "userRepository");
        wd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.i = vc3;
        this.j = userRepository;
        this.k = heartRateSummaryRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date c(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
        Date date = heartRateOverviewWeekPresenter.f;
        if (date != null) {
            return date;
        }
        wd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateOverviewWeekPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        MFLogger.d("HeartRateOverviewWeekPresenter", "stop");
        LiveData<ps3<List<DailyHeartRateSummary>>> liveData = this.g;
        if (liveData != null) {
            vc3 vc3 = this.i;
            if (vc3 != null) {
                liveData.a((LifecycleOwner) (wc3) vc3);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
        }
    }

    @DexIgnore
    public final void h() {
        this.h.clear();
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendar");
        Date date = this.f;
        if (date != null) {
            instance.setTime(date);
            instance.add(5, -6);
            for (int i2 = 1; i2 <= 7; i2++) {
                Boolean s = sk2.s(instance.getTime());
                wd4.a((Object) s, "DateHelper.isToday(calendar.time)");
                if (!s.booleanValue()) {
                    switch (instance.get(7)) {
                        case 1:
                            List<String> list = this.h;
                            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S);
                            wd4.a((Object) a2, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                            list.add(a2);
                            break;
                        case 2:
                            List<String> list2 = this.h;
                            String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__M);
                            wd4.a((Object) a3, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                            list2.add(a3);
                            break;
                        case 3:
                            List<String> list3 = this.h;
                            String a4 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T);
                            wd4.a((Object) a4, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                            list3.add(a4);
                            break;
                        case 4:
                            List<String> list4 = this.h;
                            String a5 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__W);
                            wd4.a((Object) a5, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                            list4.add(a5);
                            break;
                        case 5:
                            List<String> list5 = this.h;
                            String a6 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1);
                            wd4.a((Object) a6, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                            list5.add(a6);
                            break;
                        case 6:
                            List<String> list6 = this.h;
                            String a7 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__F);
                            wd4.a((Object) a7, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                            list6.add(a7);
                            break;
                        case 7:
                            List<String> list7 = this.h;
                            String a8 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1);
                            wd4.a((Object) a8, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                            list7.add(a8);
                            break;
                    }
                } else {
                    List<String> list8 = this.h;
                    String a9 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_Sleep7days_Label__Today);
                    wd4.a((Object) a9, "LanguageHelper.getString\u2026_Sleep7days_Label__Today)");
                    list8.add(a9);
                }
                instance.add(5, 1);
            }
            return;
        }
        wd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public final Pair<Date, Date> a(Date date) {
        Date b = sk2.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = sk2.d(currentUser.getCreatedAt());
            if (!sk2.b(b, d)) {
                b = d;
            }
        }
        return new Pair<>(b, date);
    }
}
