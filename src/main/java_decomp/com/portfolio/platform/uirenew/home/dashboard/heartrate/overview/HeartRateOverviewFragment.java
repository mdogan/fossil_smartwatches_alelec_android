package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.hc3;
import com.fossil.blesdk.obfuscated.jc2;
import com.fossil.blesdk.obfuscated.lc3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.rc3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.wc3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewFragment extends as2 {
    @DexIgnore
    public ur3<jc2> j;
    @DexIgnore
    public HeartRateOverviewDayPresenter k;
    @DexIgnore
    public HeartRateOverviewWeekPresenter l;
    @DexIgnore
    public HeartRateOverviewMonthPresenter m;
    @DexIgnore
    public hc3 n;
    @DexIgnore
    public wc3 o;
    @DexIgnore
    public rc3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public b(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            ur3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(7, a != null ? (jc2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public c(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            ur3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(4, a != null ? (jc2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public d(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            ur3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(2, a != null ? (jc2) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        MFLogger.d("HeartRateOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewFragment", "onCreateView");
        jc2 jc2 = (jc2) ra.a(layoutInflater, R.layout.fragment_heartrate_overview, viewGroup, false, O0());
        g9.c((View) jc2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) jc2, "binding");
        a(jc2);
        this.j = new ur3<>(this, jc2);
        ur3<jc2> ur3 = this.j;
        if (ur3 != null) {
            jc2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(jc2 jc2) {
        MFLogger.d("HeartRateOverviewFragment", "initUI");
        this.n = (hc3) getChildFragmentManager().a("HeartRateOverviewDayFragment");
        this.o = (wc3) getChildFragmentManager().a("HeartRateOverviewWeekFragment");
        this.p = (rc3) getChildFragmentManager().a("HeartRateOverviewMonthFragment");
        if (this.n == null) {
            this.n = new hc3();
        }
        if (this.o == null) {
            this.o = new wc3();
        }
        if (this.p == null) {
            this.p = new rc3();
        }
        ArrayList arrayList = new ArrayList();
        hc3 hc3 = this.n;
        if (hc3 != null) {
            arrayList.add(hc3);
            wc3 wc3 = this.o;
            if (wc3 != null) {
                arrayList.add(wc3);
                rc3 rc3 = this.p;
                if (rc3 != null) {
                    arrayList.add(rc3);
                    RecyclerView recyclerView = jc2.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new HeartRateOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, jc2);
                    m42 g = PortfolioApp.W.c().g();
                    hc3 hc32 = this.n;
                    if (hc32 != null) {
                        wc3 wc32 = this.o;
                        if (wc32 != null) {
                            rc3 rc32 = this.p;
                            if (rc32 != null) {
                                g.a(new lc3(hc32, wc32, rc32)).a(this);
                                jc2.s.setOnClickListener(new b(this));
                                jc2.q.setOnClickListener(new c(this));
                                jc2.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, jc2 jc2) {
        if (jc2 != null) {
            FlexibleTextView flexibleTextView = jc2.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = jc2.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = jc2.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = jc2.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = jc2.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = jc2.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = jc2.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = jc2.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = jc2.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<jc2> ur3 = this.j;
                if (ur3 != null) {
                    jc2 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = jc2.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = jc2.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = jc2.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<jc2> ur32 = this.j;
                if (ur32 != null) {
                    jc2 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = jc2.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = jc2.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = jc2.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<jc2> ur33 = this.j;
                if (ur33 != null) {
                    jc2 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = jc2.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = jc2.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = jc2.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<jc2> ur34 = this.j;
                if (ur34 != null) {
                    jc2 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
