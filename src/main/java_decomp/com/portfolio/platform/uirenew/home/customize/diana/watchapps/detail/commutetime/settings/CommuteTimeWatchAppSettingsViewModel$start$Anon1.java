package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$Anon1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {69}, m = "invokeSuspend")
public final class CommuteTimeWatchAppSettingsViewModel$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$Anon1$Anon1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeWatchAppSettingsViewModel$start$Anon1 commuteTimeWatchAppSettingsViewModel$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = commuteTimeWatchAppSettingsViewModel$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.g.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppSettingsViewModel$start$Anon1(CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeWatchAppSettingsViewModel;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeWatchAppSettingsViewModel$start$Anon1 commuteTimeWatchAppSettingsViewModel$start$Anon1 = new CommuteTimeWatchAppSettingsViewModel$start$Anon1(this.this$Anon0, kc4);
        commuteTimeWatchAppSettingsViewModel$start$Anon1.p$ = (lh4) obj;
        return commuteTimeWatchAppSettingsViewModel$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeWatchAppSettingsViewModel$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel;
        Object a = oc4.a();
        int i = this.label;
        List<AddressWrapper> list = null;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel2 = this.this$Anon0;
            gh4 b = zh4.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = commuteTimeWatchAppSettingsViewModel2;
            this.label = 1;
            obj = kg4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
            commuteTimeWatchAppSettingsViewModel = commuteTimeWatchAppSettingsViewModel2;
        } else if (i == 1) {
            commuteTimeWatchAppSettingsViewModel = (CommuteTimeWatchAppSettingsViewModel) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        commuteTimeWatchAppSettingsViewModel.e = (MFUser) obj;
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel3 = this.this$Anon0;
        CommuteTimeWatchAppSetting a2 = commuteTimeWatchAppSettingsViewModel3.d;
        if (a2 != null) {
            list = a2.getAddresses();
        }
        commuteTimeWatchAppSettingsViewModel3.a(list);
        return cb4.a;
    }
}
