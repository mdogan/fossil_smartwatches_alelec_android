package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pd3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qd3;
import com.fossil.blesdk.obfuscated.rd3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewMonthPresenter extends pd3 {
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public LiveData<ps3<List<MFSleepDay>>> k; // = new MutableLiveData();
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public LiveData<ps3<List<MFSleepDay>>> m;
    @DexIgnore
    public TreeMap<Long, Float> n;
    @DexIgnore
    public /* final */ qd3 o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthPresenter a;

        @DexIgnore
        public b(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            this.a = sleepOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<MFSleepDay>>> apply(Date date) {
            SleepOverviewMonthPresenter sleepOverviewMonthPresenter = this.a;
            wd4.a((Object) date, "it");
            if (sleepOverviewMonthPresenter.b(date)) {
                SleepOverviewMonthPresenter sleepOverviewMonthPresenter2 = this.a;
                sleepOverviewMonthPresenter2.k = sleepOverviewMonthPresenter2.q.getSleepSummaries(SleepOverviewMonthPresenter.j(this.a), SleepOverviewMonthPresenter.h(this.a), true);
            }
            return this.a.k;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public SleepOverviewMonthPresenter(qd3 qd3, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository) {
        wd4.b(qd3, "mView");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(sleepSummariesRepository, "mSummariesRepository");
        this.o = qd3;
        this.p = userRepository;
        this.q = sleepSummariesRepository;
        LiveData<ps3<List<MFSleepDay>>> b2 = ic.b(this.f, new b(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026    mSleepSummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date e(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date h(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        wd4.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date j(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<List<MFSleepDay>>> liveData = this.m;
        qd3 qd3 = this.o;
        if (qd3 != null) {
            liveData.a((rd3) qd3, new SleepOverviewMonthPresenter$start$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", "stop");
        try {
            LiveData<ps3<List<MFSleepDay>>> liveData = this.m;
            qd3 qd3 = this.o;
            if (qd3 != null) {
                liveData.a((LifecycleOwner) (rd3) qd3);
                this.k.a((LifecycleOwner) this.o);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewMonthPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", "loadData");
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                wd4.d("mCurrentDate");
                throw null;
            } else if (sk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.g;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("SleepOverviewMonthPresenter", sb.toString());
                    return;
                }
                wd4.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.g;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("SleepOverviewMonthPresenter", sb2.toString());
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SleepOverviewMonthPresenter$loadData$Anon2(this, (kc4) null), 3, (Object) null);
            return;
        }
        wd4.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.o.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!sk2.a(date4.getTime(), date.getTime())) {
                Calendar o2 = sk2.o(date);
                wd4.a((Object) o2, "DateHelper.getStartOfMonth(date)");
                Date time = o2.getTime();
                wd4.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean r = sk2.r(date);
            wd4.a((Object) r, "DateHelper.isThisMonth(date)");
            if (r.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar j2 = sk2.j(date);
                wd4.a((Object) j2, "DateHelper.getEndOfMonth(date)");
                date2 = j2.getTime();
                wd4.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wd4.d("mStartDate");
                throw null;
            }
            wd4.d("mEndDate");
            throw null;
        }
        wd4.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        if (this.f.a() == null || !sk2.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<MFSleepDay> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (MFSleepDay next : list) {
                Date component1 = next.component1();
                int component2 = next.component2();
                int component3 = next.component3();
                wd4.a((Object) instance, "calendar");
                instance.setTime(component1);
                if (component2 > 0) {
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) component3) / ((float) component2)));
                } else {
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
