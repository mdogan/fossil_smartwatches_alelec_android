package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kk2;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.q43;
import com.fossil.blesdk.obfuscated.ql2;
import com.fossil.blesdk.obfuscated.r43;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.s43;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppsPresenter extends q43 {
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Pair<String, Boolean>>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Triple<String, Boolean, Parcelable>> k; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> m;
    @DexIgnore
    public /* final */ dc<String> n;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> o;
    @DexIgnore
    public /* final */ dc<List<WatchApp>> p;
    @DexIgnore
    public /* final */ LiveData<List<Pair<String, Boolean>>> q;
    @DexIgnore
    public /* final */ dc<List<Pair<String, Boolean>>> r;
    @DexIgnore
    public /* final */ dc<Triple<String, Boolean, Parcelable>> s;
    @DexIgnore
    public /* final */ r43 t;
    @DexIgnore
    public /* final */ CategoryRepository u;
    @DexIgnore
    public /* final */ fn2 v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public b(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.t.e(str);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public c(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(WatchApp watchApp) {
            if (watchApp != null) {
                String str = (String) this.a.h.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "transform from selected WatchApp to category" + " currentCategory=" + str + " watchAppCategories=" + watchApp.getCategories());
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.a(categories.get(0));
                } else {
                    this.a.h.a(str);
                }
            }
            return this.a.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements dc<List<? extends Pair<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public d(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0076, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0095, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0097, code lost:
            r0 = com.fossil.blesdk.obfuscated.be4.a;
            r0 = com.fossil.blesdk.obfuscated.tm2.a((android.content.Context) com.portfolio.platform.PortfolioApp.W.c(), (int) com.fossil.wearables.fossil.R.string.___Text__TurnOnLocationServicesToAllow);
            com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r0, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
            r3 = new java.lang.Object[]{com.portfolio.platform.PortfolioApp.W.c().i()};
            r0 = java.lang.String.format(r0, java.util.Arrays.copyOf(r3, r3.length));
            com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r0, "java.lang.String.format(format, *args)");
         */
        @DexIgnore
        public final void a(List<Pair<String, Boolean>> list) {
            T t;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (!((Boolean) ((Pair) t).getSecond()).booleanValue()) {
                        break;
                    }
                }
                Pair pair = (Pair) t;
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged permissionsRequired " + pair + ' ');
                String str = "";
                int i = 0;
                if (pair != null) {
                    String str2 = (String) pair.getFirst();
                    switch (str2.hashCode()) {
                        case 385352715:
                            break;
                        case 564039755:
                            if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                                str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.background_location_service_general_explain);
                                break;
                            }
                            break;
                        case 766697727:
                            break;
                        case 2009556792:
                            if (str2.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                                str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.watch_app_notification_access_required);
                                break;
                            }
                            break;
                    }
                    r43 j = this.a.t;
                    if (!(list instanceof Collection) || !list.isEmpty()) {
                        for (Pair second : list) {
                            if (((Boolean) second.getSecond()).booleanValue()) {
                                i++;
                                if (i < 0) {
                                    ob4.b();
                                    throw null;
                                }
                            }
                        }
                    }
                    int size = list.size();
                    String a2 = os3.a.a((String) pair.getFirst());
                    wd4.a((Object) str, "content");
                    j.a(i, size, a2, str);
                    return;
                }
                this.a.t.a(0, 0, str, str);
                WatchApp a3 = WatchAppsPresenter.f(this.a).i().a();
                if (a3 != null) {
                    r43 j2 = this.a.t;
                    String a4 = tm2.a(PortfolioApp.W.c(), a3.getDescriptionKey(), a3.getDescription());
                    wd4.a((Object) a4, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j2.E(a4);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements dc<Triple<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public e(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(Triple<String, Boolean, ? extends Parcelable> triple) {
            String str;
            String str2;
            String str3;
            if (triple != null) {
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onLiveDataChanged setting of " + triple.getFirst() + " isSettingRequired " + triple.getSecond().booleanValue() + " setting " + ((Parcelable) triple.getThird()) + ' ');
                String str4 = "";
                if (triple.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) triple.getThird();
                    if (parcelable == null) {
                        str = str4;
                        str4 = ql2.d.a(triple.getFirst());
                    } else {
                        try {
                            String first = triple.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    str2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Title__SavedDestinations);
                                    wd4.a((Object) str2, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = str4;
                            } else if (hashCode != 1223440372) {
                                str2 = str4;
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    str = str4;
                                    for (WeatherLocationWrapper next : locations) {
                                        try {
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < ob4.a(locations)) {
                                                str3 = next.getName() + "; ";
                                            } else {
                                                str3 = next.getName();
                                            }
                                            sb.append(str3);
                                            str = sb.toString();
                                        } catch (Exception e) {
                                            e = e;
                                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.a.t.a(true, triple.getFirst(), str4, str);
                                            return;
                                        }
                                    }
                                }
                                str = str4;
                            }
                            str = str2;
                        } catch (Exception e2) {
                            e = e2;
                            str = str4;
                            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.a.t.a(true, triple.getFirst(), str4, str);
                            return;
                        }
                    }
                    this.a.t.a(true, triple.getFirst(), str4, str);
                    return;
                }
                this.a.t.a(false, triple.getFirst(), str4, (String) null);
                WatchApp a2 = WatchAppsPresenter.f(this.a).i().a();
                if (a2 != null) {
                    r43 j = this.a.t;
                    String a3 = tm2.a(PortfolioApp.W.c(), a2.getDescriptionKey(), a2.getDescription());
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    j.E(a3);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements dc<List<? extends WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public f(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.a.t.v(list);
                WatchApp a2 = WatchAppsPresenter.f(this.a).i().a();
                if (a2 != null) {
                    this.a.t.b(a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public g(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<WatchApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            DianaCustomizeViewModel f = WatchAppsPresenter.f(this.a);
            wd4.a((Object) str, "category");
            List<WatchApp> d = f.d(str);
            ArrayList arrayList = new ArrayList();
            DianaPreset a2 = WatchAppsPresenter.f(this.a).c().a();
            WatchApp a3 = WatchAppsPresenter.f(this.a).i().a();
            if (a3 != null) {
                String watchappId = a3.getWatchappId();
                if (a2 != null) {
                    for (WatchApp next : d) {
                        Iterator<T> it = a2.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                            boolean z = true;
                            if (!wd4.a((Object) dianaPresetWatchAppSetting.getId(), (Object) next.getWatchappId()) || !(!wd4.a((Object) dianaPresetWatchAppSetting.getId(), (Object) watchappId))) {
                                z = false;
                                continue;
                            }
                            if (z) {
                                break;
                            }
                        }
                        if (((DianaPresetWatchAppSetting) t) == null || wd4.a((Object) next.getWatchappId(), (Object) "empty")) {
                            arrayList.add(next);
                        }
                    }
                }
                this.a.i.a(arrayList);
                return this.a.i;
            }
            wd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements dc<WatchApp> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public h(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            if (watchApp != null) {
                this.a.g.a(watchApp);
                this.a.t.t(ql2.d.e(watchApp.getWatchappId()));
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public WatchAppsPresenter(r43 r43, CategoryRepository categoryRepository, fn2 fn2) {
        wd4.b(r43, "mView");
        wd4.b(categoryRepository, "mCategoryRepository");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.t = r43;
        this.u = categoryRepository;
        this.v = fn2;
        new Gson();
        LiveData<String> b2 = ic.b(this.g, new c(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.m = b2;
        this.n = new b(this);
        LiveData<List<WatchApp>> b3 = ic.b(this.m, new g(this));
        wd4.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.o = b3;
        this.p = new f(this);
        LiveData<List<Pair<String, Boolean>>> b4 = ic.b(this.g, new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1(this));
        wd4.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.q = b4;
        this.r = new d(this);
        this.s = new e(this);
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel f(WatchAppsPresenter watchAppsPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = watchAppsPresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final ri4 b(String str) {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        if (this.l.isEmpty()) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppsPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        } else {
            this.t.a(this.l);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> i2 = dianaCustomizeViewModel.i();
            r43 r43 = this.t;
            if (r43 != null) {
                i2.a((s43) r43, new h(this));
                this.m.a(this.n);
                this.o.a(this.p);
                this.q.a(this.r);
                this.k.a(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<WatchApp> i2 = dianaCustomizeViewModel.i();
            r43 r43 = this.t;
            if (r43 != null) {
                i2.a((LifecycleOwner) (s43) r43);
                this.i.b(this.p);
                this.h.b(this.n);
                this.j.b(this.r);
                this.k.b(this.s);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r3 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r4 != null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009b, code lost:
        if (r0 != null) goto L_0x009f;
     */
    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        String str3;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t4 = null;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                Iterator<T> it = a2.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wd4.a((Object) ((DianaPresetWatchAppSetting) t2).getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                if (dianaPresetWatchAppSetting != null) {
                    str = dianaPresetWatchAppSetting.getId();
                }
                str = "empty";
                Iterator<T> it2 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (wd4.a((Object) ((DianaPresetWatchAppSetting) t3).getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t3;
                if (dianaPresetWatchAppSetting2 != null) {
                    str2 = dianaPresetWatchAppSetting2.getId();
                }
                str2 = "empty";
                Iterator<T> it3 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (wd4.a((Object) ((DianaPresetWatchAppSetting) next).getPosition(), (Object) "bottom")) {
                        t4 = next;
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) t4;
                if (dianaPresetWatchAppSetting3 != null) {
                    str3 = dianaPresetWatchAppSetting3.getId();
                }
                str3 = "empty";
                this.t.a(str, str2, str3);
                return;
            }
            this.t.a("empty", "empty", "empty");
            return;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t2 = null;
        if (dianaCustomizeViewModel != null) {
            WatchApp a2 = dianaCustomizeViewModel.i().a();
            if (a2 != null) {
                String component1 = a2.component1();
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a3 = dianaCustomizeViewModel2.c().a();
                    if (a3 != null) {
                        ArrayList<DianaPresetWatchAppSetting> watchapps = a3.getWatchapps();
                        if (watchapps != null) {
                            Iterator<T> it = watchapps.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                T next = it.next();
                                if (wd4.a((Object) component1, (Object) ((DianaPresetWatchAppSetting) next).getId())) {
                                    t2 = next;
                                    break;
                                }
                            }
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                            if (dianaPresetWatchAppSetting != null) {
                                str = dianaPresetWatchAppSetting.getSettings();
                            }
                        }
                    }
                    str = "";
                    String watchappId = a2.getWatchappId();
                    int hashCode = watchappId.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 1223440372 && watchappId.equals("weather")) {
                            this.t.M(str);
                        }
                    } else if (watchappId.equals("commute-time")) {
                        this.t.b(str);
                    }
                } else {
                    wd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            WatchApp a2 = dianaCustomizeViewModel.i().a();
            if (a2 != null) {
                String component1 = a2.component1();
                if (ql2.d.e(component1)) {
                    this.t.c(component1);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    public void k() {
        Object obj;
        boolean a2;
        List a3 = this.j.a();
        if (a3 != null) {
            Iterator it = a3.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((Pair) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            Pair pair = (Pair) obj;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "processRequiredPermission " + pair);
            if (pair != null) {
                String str = (String) pair.component1();
                boolean z = false;
                int hashCode = str.hashCode();
                if (hashCode != 564039755) {
                    if (hashCode == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        a2 = os3.a.e();
                    }
                    if (z) {
                        this.t.A((String) pair.getFirst());
                        return;
                    } else {
                        this.t.f((String) pair.getFirst());
                        return;
                    }
                } else {
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        a2 = os3.a.a((Context) PortfolioApp.W.c());
                    }
                    if (z) {
                    }
                }
                z = !a2;
                if (z) {
                }
            }
        }
    }

    @DexIgnore
    public void l() {
        this.t.a(this);
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        wd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void a(Category category) {
        wd4.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.h.a(category.getId());
    }

    @DexIgnore
    public void a(String str) {
        T t2;
        wd4.b(str, "watchappId");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            WatchApp e2 = dianaCustomizeViewModel.e(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + e2);
            if (e2 != null) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a2 = dianaCustomizeViewModel2.c().a();
                    if (a2 != null) {
                        DianaPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                        if (dianaCustomizeViewModel3 != null) {
                            String a3 = dianaCustomizeViewModel3.j().a();
                            if (a3 != null) {
                                wd4.a((Object) a3, "mDianaCustomizeViewModel\u2026ctedWatchAppPos().value!!");
                                String str2 = a3;
                                DianaCustomizeViewModel dianaCustomizeViewModel4 = this.f;
                                if (dianaCustomizeViewModel4 != null) {
                                    if (!dianaCustomizeViewModel4.j(str)) {
                                        Iterator<DianaPresetWatchAppSetting> it = clone.getWatchapps().iterator();
                                        while (it.hasNext()) {
                                            DianaPresetWatchAppSetting next = it.next();
                                            if (wd4.a((Object) next.getPosition(), (Object) str2)) {
                                                arrayList.add(new DianaPresetWatchAppSetting(str2, str, next.getLocalUpdateAt()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        clone.getWatchapps().clear();
                                        clone.getWatchapps().addAll(arrayList);
                                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Update current preset=" + clone);
                                        DianaCustomizeViewModel dianaCustomizeViewModel5 = this.f;
                                        if (dianaCustomizeViewModel5 != null) {
                                            dianaCustomizeViewModel5.a(clone);
                                        } else {
                                            wd4.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it2 = a2.getWatchapps().iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                t2 = null;
                                                break;
                                            }
                                            t2 = it2.next();
                                            if (wd4.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                                                break;
                                            }
                                        }
                                        DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                                        if (dianaPresetWatchAppSetting != null) {
                                            DianaCustomizeViewModel dianaCustomizeViewModel6 = this.f;
                                            if (dianaCustomizeViewModel6 != null) {
                                                dianaCustomizeViewModel6.l(dianaPresetWatchAppSetting.getPosition());
                                            } else {
                                                wd4.d("mDianaCustomizeViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = e2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.v.h()) {
                                        this.v.o(true);
                                        this.t.c("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                wd4.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    return;
                }
                wd4.d("mDianaCustomizeViewModel");
                throw null;
            }
            return;
        }
        wd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void a(String str, Parcelable parcelable) {
        T t2;
        wd4.b(str, "watchAppId");
        wd4.b(parcelable, MicroAppSetting.SETTING);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it = clone.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wd4.a((Object) ((DianaPresetWatchAppSetting) t2).getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                sz1 sz1 = new sz1();
                sz1.b(new kk2());
                Gson a3 = sz1.a();
                if (dianaPresetWatchAppSetting != null) {
                    dianaPresetWatchAppSetting.setSettings(a3.a((Object) parcelable));
                }
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "update current preset with new setting " + parcelable + " of " + str);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(clone);
                } else {
                    wd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            wd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
