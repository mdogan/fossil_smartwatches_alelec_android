package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.c03;
import com.fossil.blesdk.obfuscated.d03;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public NotificationHybridAppPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<String> arrayList) {
            wd4.b(fragment, "fragment");
            wd4.b(arrayList, "stringAppsSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridAppActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putStringArrayListExtra("LIST_APPS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 4567);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        wd4.a((Object) NotificationHybridAppActivity.class.getSimpleName(), "NotificationHybridAppAct\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        c03 c03 = (c03) getSupportFragmentManager().a((int) R.id.content);
        if (c03 == null) {
            c03 = c03.n.b();
            a((Fragment) c03, c03.n.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (c03 != null) {
            g.a(new d03(c03, getIntent().getIntExtra("HAND_NUMBER", 0), getIntent().getStringArrayListExtra("LIST_APPS_SELECTED"))).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppContract.View");
    }
}
