package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends WeatherLocationWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(kc4 kc4, WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1) {
        super(2, kc4);
        this.this$Anon0 = weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(kc4, this.this$Anon0);
        weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            List b = this.this$Anon0.this$Anon0.h;
            ArrayList arrayList = new ArrayList();
            for (Object next : b) {
                if (pc4.a(((WeatherLocationWrapper) next).isEnableLocation()).booleanValue()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
