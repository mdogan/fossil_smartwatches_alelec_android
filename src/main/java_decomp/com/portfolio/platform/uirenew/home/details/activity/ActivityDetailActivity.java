package com.portfolio.platform.uirenew.home.details.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.ne3;
import com.fossil.blesdk.obfuscated.oe3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((rd4) null);
    @DexIgnore
    public ActivityDetailPresenter B;
    @DexIgnore
    public Date C; // = new Date();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            wd4.b(date, "date");
            wd4.b(context, "context");
            Intent intent = new Intent(context, ActivityDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        ne3 ne3 = (ne3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        if (intent != null) {
            this.C = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (ne3 == null) {
            ne3 = ne3.o.a(this.C);
            a((Fragment) ne3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new oe3(ne3)).a(this);
    }
}
