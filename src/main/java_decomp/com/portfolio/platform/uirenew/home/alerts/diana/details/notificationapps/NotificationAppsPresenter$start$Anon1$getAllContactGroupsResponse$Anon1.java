package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x52;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1", f = "NotificationAppsPresenter.kt", l = {192}, m = "invokeSuspend")
public final class NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super CoroutineUseCase.c>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1(NotificationAppsPresenter$start$Anon1 notificationAppsPresenter$start$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationAppsPresenter$start$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1 = new NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1(this.this$Anon0, kc4);
        notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1.p$ = (lh4) obj;
        return notificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppsPresenter$start$Anon1$getAllContactGroupsResponse$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            qx2 c = this.this$Anon0.this$Anon0.p;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = x52.a(c, null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
