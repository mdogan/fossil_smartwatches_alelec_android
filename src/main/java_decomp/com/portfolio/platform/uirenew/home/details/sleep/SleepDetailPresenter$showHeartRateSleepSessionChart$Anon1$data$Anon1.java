package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ArrayList<dg3.a>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1(SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1 sleepDetailPresenter$showHeartRateSleepSessionChart$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepDetailPresenter$showHeartRateSleepSessionChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1 sleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1 = new SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1(this.this$Anon0, kc4);
        sleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1.p$ = (lh4) obj;
        return sleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepDetailPresenter$showHeartRateSleepSessionChart$Anon1$data$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            SleepDetailPresenter sleepDetailPresenter = this.this$Anon0.this$Anon0;
            return sleepDetailPresenter.a((List<MFSleepSession>) sleepDetailPresenter.o);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
