package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {371, 373}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationCallsAndMessagesPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 = new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1(this.this$Anon0, kc4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1.p$ = (lh4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01f2  */
    public final Object invokeSuspend(Object obj) {
        long j;
        Object obj2;
        List list;
        sh4 sh4;
        lh4 lh4;
        List list2;
        sh4 sh42;
        Object obj3;
        List list3;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            this.this$Anon0.t.b();
            this.this$Anon0.t.T();
            list2 = new ArrayList();
            if (this.this$Anon0.t.E() != this.this$Anon0.k()) {
                list2.add(new NotificationSettingsModel("AllowCallsFrom", this.this$Anon0.t.E(), true));
            }
            if (this.this$Anon0.t.J() != this.this$Anon0.j()) {
                list2.add(new NotificationSettingsModel("AllowMessagesFrom", this.this$Anon0.t.J(), false));
            }
            if (!list2.isEmpty()) {
                this.this$Anon0.b((List<NotificationSettingsModel>) list2);
            }
            j = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            local.d(a2, "filter notification, time start=" + j);
            this.this$Anon0.f.clear();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationCallsAndMessagesPresenter.C.a();
            local2.d(a3, "mListAppWrapper.size = " + this.this$Anon0.m().size());
            sh4 a4 = mg4.a(lh42, this.this$Anon0.b(), (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$otherAppDeffer$Anon1(this, (kc4) null), 2, (Object) null);
            sh42 = mg4.a(lh42, this.this$Anon0.b(), (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1(this, (kc4) null), 2, (Object) null);
            List h = this.this$Anon0.f;
            this.L$Anon0 = lh42;
            this.L$Anon1 = list2;
            this.J$Anon0 = j;
            this.L$Anon2 = a4;
            this.L$Anon3 = sh42;
            this.L$Anon4 = h;
            this.label = 1;
            obj3 = a4.a(this);
            if (obj3 == a) {
                return a;
            }
            sh4 sh43 = a4;
            lh4 = lh42;
            list3 = h;
            sh4 = sh43;
        } else if (i == 1) {
            list3 = (List) this.L$Anon4;
            long j2 = this.J$Anon0;
            list2 = (List) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            sh4 = (sh4) this.L$Anon2;
            j = j2;
            sh42 = (sh4) this.L$Anon3;
            obj3 = obj;
        } else if (i == 2) {
            sh4 sh44 = (sh4) this.L$Anon3;
            sh4 sh45 = (sh4) this.L$Anon2;
            long j3 = this.J$Anon0;
            List list4 = (List) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            j = j3;
            obj2 = obj;
            list = (List) obj2;
            if (list == null) {
                this.this$Anon0.f.addAll(list);
                long b = PortfolioApp.W.c().b(new AppNotificationFilterSettings(this.this$Anon0.f, System.currentTimeMillis()), PortfolioApp.W.c().e());
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a5 = NotificationCallsAndMessagesPresenter.C.a();
                local3.d(a5, "filter notification, time end= " + b);
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a6 = NotificationCallsAndMessagesPresenter.C.a();
                local4.d(a6, "delayTime =" + (b - j) + " mili seconds");
            } else {
                NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter = this.this$Anon0;
                notificationCallsAndMessagesPresenter.a(notificationCallsAndMessagesPresenter.n());
                this.this$Anon0.t.a();
                this.this$Anon0.t.N();
                this.this$Anon0.t.close();
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((Collection) obj3);
        this.L$Anon0 = lh4;
        this.L$Anon1 = list2;
        this.J$Anon0 = j;
        this.L$Anon2 = sh4;
        this.L$Anon3 = sh42;
        this.label = 2;
        obj2 = sh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list = (List) obj2;
        if (list == null) {
        }
        return cb4.a;
    }
}
