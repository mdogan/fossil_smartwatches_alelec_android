package com.portfolio.platform.uirenew.home.profile.help;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.data.model.SKUModel;
import java.util.HashMap;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1", f = "HelpPresenter.kt", l = {78}, m = "invokeSuspend")
public final class HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HelpPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1(HelpPresenter helpPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = helpPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1 helpPresenter$logSendFeedbackSuccessfullyEvent$Anon1 = new HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this.this$Anon0, kc4);
        helpPresenter$logSendFeedbackSuccessfullyEvent$Anon1.p$ = (lh4) obj;
        return helpPresenter$logSendFeedbackSuccessfullyEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = zh4.b();
            HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 helpPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 = new HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, helpPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SKUModel sKUModel = (SKUModel) obj;
        if (sKUModel != null) {
            HashMap hashMap = new HashMap();
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            hashMap.put("Style_Number", sku);
            String deviceName = sKUModel.getDeviceName();
            if (deviceName == null) {
                deviceName = "";
            }
            hashMap.put("Device_Name", deviceName);
            this.this$Anon0.i.a("feedback_submit", (Map<String, ? extends Object>) hashMap);
        }
        return cb4.a;
    }
}
