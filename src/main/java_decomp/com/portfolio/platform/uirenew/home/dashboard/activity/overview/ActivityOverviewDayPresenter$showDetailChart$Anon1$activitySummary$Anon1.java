package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1", f = "ActivityOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ActivitySummary>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityOverviewDayPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(ActivityOverviewDayPresenter$showDetailChart$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activityOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 = new ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(this.this$Anon0, kc4);
        activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1.p$ = (lh4) obj;
        return activityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ps3 ps3 = (ps3) this.this$Anon0.this$Anon0.i.a();
            ActivitySummary activitySummary = null;
            if (ps3 == null) {
                return null;
            }
            List list = (List) ps3.d();
            if (list == null) {
                return null;
            }
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (pc4.a(sk2.d(next.getDate(), ActivityOverviewDayPresenter.d(this.this$Anon0.this$Anon0))).booleanValue()) {
                    activitySummary = next;
                    break;
                }
            }
            return activitySummary;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
