package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetHybridPresetToWatchUseCase extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public c e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ HybridPresetRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            wd4.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ HybridPreset a;

        @DexIgnore
        public c(HybridPreset hybridPreset) {
            wd4.b(hybridPreset, "mPreset");
            this.a = hybridPreset;
        }

        @DexIgnore
        public final HybridPreset a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetHybridPresetToWatchUseCase.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_LINK_MAPPING) {
                    SetHybridPresetToWatchUseCase.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - success");
                        SetHybridPresetToWatchUseCase.this.f();
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - failed");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    SetHybridPresetToWatchUseCase.this.a(new b(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public SetHybridPresetToWatchUseCase(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository) {
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(hybridPresetRepository, "mHybridPresetRepository");
        wd4.b(microAppRepository, "mMicroAppRepository");
        wd4.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        this.g = deviceRepository;
        this.h = hybridPresetRepository;
        this.i = microAppRepository;
        this.j = microAppLastSettingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ c c(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase) {
        c cVar = setHybridPresetToWatchUseCase.e;
        if (cVar != null) {
            return cVar;
        }
        wd4.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public String c() {
        return "SetHybridPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    public final ri4 f() {
        return mg4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new SetHybridPresetToWatchUseCase$setPresetToDb$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x007b, code lost:
        if (com.fossil.blesdk.obfuscated.pc4.a(com.portfolio.platform.PortfolioApp.W.c().c(r6, (java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping>) r7)) != null) goto L_0x0094;
     */
    @DexIgnore
    public Object a(c cVar, kc4<Object> kc4) {
        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "executeUseCase");
        if (cVar != null) {
            this.e = cVar;
            this.d = true;
            String e2 = PortfolioApp.W.c().e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("execute with preset ");
            c cVar2 = this.e;
            if (cVar2 != null) {
                sb.append(cVar2.a());
                local.d("SetHybridPresetToWatchUseCase", sb.toString());
                c cVar3 = this.e;
                if (cVar3 != null) {
                    List<MicroAppMapping> a2 = tj2.a(cVar3.a(), e2, this.g, this.i);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetHybridPresetToWatchUseCase", "set preset to watch with bleMappings " + a2);
                } else {
                    wd4.d("mRequestValues");
                    throw null;
                }
            } else {
                wd4.d("mRequestValues");
                throw null;
            }
        }
        a(new b(-1, new ArrayList()));
        return new Object();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        try {
            if (wd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                PortfolioApp.W.c().n(((SecondTimezoneSetting) new Gson().a(str2, SecondTimezoneSetting.class)).getTimeZoneId());
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SetHybridPresetToWatchUseCase", "setAutoSettingToWatch exception " + e2);
        }
    }
}
