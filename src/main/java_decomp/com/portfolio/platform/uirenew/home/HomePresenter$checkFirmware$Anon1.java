package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$Anon1", f = "HomePresenter.kt", l = {265}, m = "invokeSuspend")
public final class HomePresenter$checkFirmware$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkFirmware$Anon1(HomePresenter homePresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homePresenter;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomePresenter$checkFirmware$Anon1 homePresenter$checkFirmware$Anon1 = new HomePresenter$checkFirmware$Anon1(this.this$Anon0, this.$activeDeviceSerial, kc4);
        homePresenter$checkFirmware$Anon1.p$ = (lh4) obj;
        return homePresenter$checkFirmware$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomePresenter$checkFirmware$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() enter checkFirmware");
            gh4 a2 = this.this$Anon0.b();
            HomePresenter$checkFirmware$Anon1$isLatestFw$Anon1 homePresenter$checkFirmware$Anon1$isLatestFw$Anon1 = new HomePresenter$checkFirmware$Anon1$isLatestFw$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, homePresenter$checkFirmware$Anon1$isLatestFw$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str = this.$activeDeviceSerial;
        String a3 = HomePresenter.y.a();
        remote.i(component, session, str, a3, "[Sync OK] Is device has latest FW " + booleanValue);
        if (booleanValue) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() fw is latest, skip OTA after sync success");
        } else {
            this.this$Anon0.p();
        }
        FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() exit checkFirmware");
        return cb4.a;
    }
}
