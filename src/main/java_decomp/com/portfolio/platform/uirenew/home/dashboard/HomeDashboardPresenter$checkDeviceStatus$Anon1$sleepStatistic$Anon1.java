package com.portfolio.platform.uirenew.home.dashboard;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1", f = "HomeDashboardPresenter.kt", l = {103}, m = "invokeSuspend")
public final class HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super SleepStatistic>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter$checkDeviceStatus$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1(HomeDashboardPresenter$checkDeviceStatus$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDashboardPresenter$checkDeviceStatus$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1(this.this$Anon0, kc4);
        homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1.p$ = (lh4) obj;
        return homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            SleepSummariesRepository k = this.this$Anon0.this$Anon0.D;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = k.getSleepStatisticAwait(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
