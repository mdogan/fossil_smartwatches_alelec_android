package com.portfolio.platform.uirenew.home.dashboard;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$Anon1", f = "HomeDashboardPresenter.kt", l = {136}, m = "invokeSuspend")
public final class HomeDashboardPresenter$showLayoutBattery$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $buildMode;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$showLayoutBattery$Anon1(HomeDashboardPresenter homeDashboardPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDashboardPresenter;
        this.$buildMode = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDashboardPresenter$showLayoutBattery$Anon1 homeDashboardPresenter$showLayoutBattery$Anon1 = new HomeDashboardPresenter$showLayoutBattery$Anon1(this.this$Anon0, this.$buildMode, kc4);
        homeDashboardPresenter$showLayoutBattery$Anon1.p$ = (lh4) obj;
        return homeDashboardPresenter$showLayoutBattery$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDashboardPresenter$showLayoutBattery$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            HomeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1 homeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1 = new HomeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, homeDashboardPresenter$showLayoutBattery$Anon1$activeDevice$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device = (Device) obj;
        if (device != null) {
            if (wd4.a((Object) this.$buildMode, (Object) "debug") || wd4.a((Object) this.$buildMode, (Object) "staging")) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.W.c().u().w() || device.getBatteryLevel() <= 0) {
                    this.this$Anon0.x.o(false);
                } else {
                    this.this$Anon0.x.o(true);
                }
            } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                this.this$Anon0.x.o(false);
            } else {
                this.this$Anon0.x.o(true);
            }
        }
        return cb4.a;
    }
}
