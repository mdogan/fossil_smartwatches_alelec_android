package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.dq2;
import com.fossil.blesdk.obfuscated.g8;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.k42;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.mc;
import com.fossil.blesdk.obfuscated.n63;
import com.fossil.blesdk.obfuscated.p63;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.v5;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x5;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a E; // = new a((rd4) null);
    @DexIgnore
    public HybridCustomizeEditPresenter B;
    @DexIgnore
    public k42 C;
    @DexIgnore
    public HybridCustomizeViewModel D;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            wd4.b(context, "context");
            wd4.b(str, "presetId");
            wd4.b(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<g8<View, String>> arrayList, List<? extends g8<CustomizeWidget, String>> list, String str2) {
            wd4.b(fragmentActivity, "context");
            wd4.b(str, "presetId");
            wd4.b(arrayList, "views");
            wd4.b(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            for (g8 g8Var : list) {
                arrayList.add(new g8(g8Var.a, g8Var.b));
                Bundle bundle = new Bundle();
                dq2.a aVar = dq2.f;
                F f = g8Var.a;
                if (f != null) {
                    wd4.a((Object) f, "wcPair.first!!");
                    aVar.a((CustomizeWidget) f, bundle);
                    F f2 = g8Var.a;
                    if (f2 != null) {
                        wd4.a((Object) f2, "wcPair.first!!");
                        intent.putExtra(((CustomizeWidget) f2).getTransitionName(), bundle);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new g8[0]);
            if (array != null) {
                g8[] g8VarArr = (g8[]) array;
                x5 a = x5.a(fragmentActivity, (g8[]) Arrays.copyOf(g8VarArr, g8VarArr.length));
                wd4.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                v5.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x012e  */
    public void onCreate(Bundle bundle) {
        HybridPreset hybridPreset;
        HybridPreset hybridPreset2;
        HybridCustomizeViewModel hybridCustomizeViewModel;
        Class cls = HybridPreset.class;
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        String stringExtra = getIntent().getStringExtra("KEY_PRESET_ID");
        String stringExtra2 = getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
        n63 n63 = (n63) getSupportFragmentManager().a((int) R.id.content);
        if (n63 == null) {
            n63 = new n63();
            a((Fragment) n63, "DianaCustomizeEditFragment", (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        wd4.a((Object) stringExtra, "presetId");
        wd4.a((Object) stringExtra2, "microAppPos");
        g.a(new p63(n63, stringExtra, stringExtra2)).a(this);
        k42 k42 = this.C;
        if (k42 != null) {
            jc a2 = mc.a((FragmentActivity) this, (lc.b) k42).a(HybridCustomizeViewModel.class);
            wd4.a((Object) a2, "ViewModelProviders.of(th\u2026izeViewModel::class.java)");
            this.D = (HybridCustomizeViewModel) a2;
            if (bundle == null) {
                FLogger.INSTANCE.getLocal().d(f(), "init from initialize state");
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.D;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.a(stringExtra, stringExtra2);
                } else {
                    wd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(f(), "init from savedInstanceState");
                sz1 sz1 = new sz1();
                sz1.a(cls, new HybridPresetDeserializer());
                Gson a3 = sz1.a();
                try {
                    if (bundle.containsKey("KEY_CURRENT_PRESET")) {
                        FLogger.INSTANCE.getLocal().d(f(), "parse gson " + bundle.getString("KEY_CURRENT_PRESET"));
                        hybridPreset = (HybridPreset) a3.a(bundle.getString("KEY_CURRENT_PRESET"), cls);
                    } else {
                        hybridPreset = null;
                    }
                    try {
                        if (bundle.containsKey("KEY_ORIGINAL_PRESET")) {
                            hybridPreset2 = (HybridPreset) a3.a(bundle.getString("KEY_ORIGINAL_PRESET"), cls);
                            if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                                stringExtra2 = bundle.getString("KEY_PRESET_WATCH_APP_POS_SELECTED");
                            }
                            hybridCustomizeViewModel = this.D;
                            if (hybridCustomizeViewModel == null) {
                                hybridCustomizeViewModel.a(stringExtra, hybridPreset2, hybridPreset, stringExtra2);
                                return;
                            } else {
                                wd4.d("mHybridCustomizeViewModel");
                                throw null;
                            }
                        }
                    } catch (Exception e) {
                        e = e;
                        FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                        hybridPreset2 = null;
                        if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                        }
                        hybridCustomizeViewModel = this.D;
                        if (hybridCustomizeViewModel == null) {
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    hybridPreset = null;
                    FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                    hybridPreset2 = null;
                    if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                    }
                    hybridCustomizeViewModel = this.D;
                    if (hybridCustomizeViewModel == null) {
                    }
                }
                hybridPreset2 = null;
                if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                }
                hybridCustomizeViewModel = this.D;
                if (hybridCustomizeViewModel == null) {
                }
            }
        } else {
            wd4.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        HybridCustomizeEditPresenter hybridCustomizeEditPresenter = this.B;
        if (hybridCustomizeEditPresenter != null) {
            hybridCustomizeEditPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }
}
