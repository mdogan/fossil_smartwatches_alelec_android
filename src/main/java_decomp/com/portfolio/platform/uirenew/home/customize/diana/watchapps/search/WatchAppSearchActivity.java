package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u53;
import com.fossil.blesdk.obfuscated.v53;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppSearchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public WatchAppSearchPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            wd4.b(fragment, "fragment");
            wd4.b(str, "watchAppTop");
            wd4.b(str2, "watchAppMiddle");
            wd4.b(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), WatchAppSearchActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            fragment.startActivityForResult(intent, 101, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        u53 u53 = (u53) getSupportFragmentManager().a((int) R.id.content);
        if (u53 == null) {
            u53 = u53.o.b();
            a((Fragment) u53, u53.o.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (u53 != null) {
            g.a(new v53(u53)).a(this);
            Intent intent = getIntent();
            wd4.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                WatchAppSearchPresenter watchAppSearchPresenter = this.B;
                if (watchAppSearchPresenter != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    watchAppSearchPresenter.a(string, string2, string3);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                WatchAppSearchPresenter watchAppSearchPresenter2 = this.B;
                if (watchAppSearchPresenter2 != null) {
                    String string4 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    watchAppSearchPresenter2.a(string4, string5, string6);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchContract.View");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        WatchAppSearchPresenter watchAppSearchPresenter = this.B;
        if (watchAppSearchPresenter != null) {
            watchAppSearchPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }
}
