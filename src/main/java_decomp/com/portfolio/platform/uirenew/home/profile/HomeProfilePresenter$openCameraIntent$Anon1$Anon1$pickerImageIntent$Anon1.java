package com.portfolio.platform.uirenew.home.profile;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.bl2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Intent>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ FragmentActivity $it;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(FragmentActivity fragmentActivity, kc4 kc4) {
        super(2, kc4);
        this.$it = fragmentActivity;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(this.$it, kc4);
        homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1.p$ = (lh4) obj;
        return homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return bl2.b(this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
