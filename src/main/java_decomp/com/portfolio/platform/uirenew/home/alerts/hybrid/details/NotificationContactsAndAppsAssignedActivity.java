package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.blesdk.obfuscated.iz2;
import com.fossil.blesdk.obfuscated.jz2;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i) {
            wd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsAndAppsAssignedActivity.class);
            intent.putExtra("EXTRA_HAND_NUMBER", i);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        iz2 iz2 = (iz2) getSupportFragmentManager().a((int) R.id.content);
        if (iz2 == null) {
            iz2 = iz2.q.b();
            a((Fragment) iz2, iz2.q.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (iz2 != null) {
            int intExtra = getIntent().getIntExtra("EXTRA_HAND_NUMBER", 0);
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            wd4.a((Object) supportLoaderManager, "supportLoaderManager");
            g.a(new jz2(iz2, intExtra, supportLoaderManager)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedContract.View");
    }
}
