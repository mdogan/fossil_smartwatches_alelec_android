package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.bv2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xu2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public HomePresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        HomePresenter homePresenter = this.B;
        if (homePresenter != null) {
            homePresenter.a(i, i2, intent);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        xu2 xu2 = (xu2) getSupportFragmentManager().a((int) R.id.content);
        if (xu2 == null) {
            xu2 = xu2.y.a();
            a((Fragment) xu2, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new bv2(xu2)).a(this);
        if (bundle != null) {
            HomePresenter homePresenter = this.B;
            if (homePresenter != null) {
                homePresenter.a(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null) {
                Intent intent2 = getIntent();
                if (intent2 != null) {
                    Bundle extras = intent2.getExtras();
                    if (extras == null) {
                        wd4.a();
                        throw null;
                    } else if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                        HomePresenter homePresenter2 = this.B;
                        if (homePresenter2 != null) {
                            Intent intent3 = getIntent();
                            if (intent3 != null) {
                                Bundle extras2 = intent3.getExtras();
                                if (extras2 != null) {
                                    homePresenter2.a(extras2.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            HomePresenter homePresenter = this.B;
            if (homePresenter != null) {
                bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", homePresenter.h());
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        a((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        b((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }
}
