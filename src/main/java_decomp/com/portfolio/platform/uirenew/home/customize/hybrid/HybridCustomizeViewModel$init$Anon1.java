package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$Anon1", f = "HybridCustomizeViewModel.kt", l = {106, 111}, m = "invokeSuspend")
public final class HybridCustomizeViewModel$init$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $microAppPos;
    @DexIgnore
    public /* final */ /* synthetic */ String $presetId;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridCustomizeViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$Anon1$Anon1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel$init$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HybridCustomizeViewModel$init$Anon1 hybridCustomizeViewModel$init$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = hybridCustomizeViewModel$init$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.d.a(this.this$Anon0.this$Anon0.n);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridCustomizeViewModel$init$Anon1(HybridCustomizeViewModel hybridCustomizeViewModel, String str, String str2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = hybridCustomizeViewModel;
        this.$presetId = str;
        this.$microAppPos = str2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HybridCustomizeViewModel$init$Anon1 hybridCustomizeViewModel$init$Anon1 = new HybridCustomizeViewModel$init$Anon1(this.this$Anon0, this.$presetId, this.$microAppPos, kc4);
        hybridCustomizeViewModel$init$Anon1.p$ = (lh4) obj;
        return hybridCustomizeViewModel$init$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HybridCustomizeViewModel$init$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.s.a();
            local.d(a2, "init presetId=" + this.$presetId + " originalPreset=" + this.this$Anon0.c + " microAppPos=" + this.$microAppPos + ' ');
            if (this.this$Anon0.c == null) {
                HybridCustomizeViewModel hybridCustomizeViewModel = this.this$Anon0;
                String str = this.$presetId;
                this.L$Anon0 = lh4;
                this.label = 1;
                if (hybridCustomizeViewModel.a(str, (kc4<? super cb4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.g.a(this.$microAppPos);
        bj4 c = zh4.c();
        Anon1 anon1 = new Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.label = 2;
        if (kg4.a(c, anon1, this) == a) {
            return a;
        }
        return cb4.a;
    }
}
