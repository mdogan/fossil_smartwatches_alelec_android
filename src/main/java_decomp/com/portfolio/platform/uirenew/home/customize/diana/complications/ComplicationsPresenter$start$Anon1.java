package com.portfolio.platform.uirenew.home.customize.diana.complications;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$Anon1", f = "ComplicationsPresenter.kt", l = {212}, m = "invokeSuspend")
public final class ComplicationsPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationsPresenter$start$Anon1(ComplicationsPresenter complicationsPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = complicationsPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ComplicationsPresenter$start$Anon1 complicationsPresenter$start$Anon1 = new ComplicationsPresenter$start$Anon1(this.this$Anon0, kc4);
        complicationsPresenter$start$Anon1.p$ = (lh4) obj;
        return complicationsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationsPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            ComplicationsPresenter$start$Anon1$allCategory$Anon1 complicationsPresenter$start$Anon1$allCategory$Anon1 = new ComplicationsPresenter$start$Anon1$allCategory$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, complicationsPresenter$start$Anon1$allCategory$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList = new ArrayList();
        for (Category category : (List) obj) {
            if (!ComplicationsPresenter.g(this.this$Anon0).b(category.getId()).isEmpty()) {
                arrayList.add(category);
            }
        }
        this.this$Anon0.m.addAll(arrayList);
        this.this$Anon0.u.a(this.this$Anon0.m);
        return cb4.a;
    }
}
