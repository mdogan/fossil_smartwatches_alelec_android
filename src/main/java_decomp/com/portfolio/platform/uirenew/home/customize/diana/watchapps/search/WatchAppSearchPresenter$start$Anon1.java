package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$Anon1", f = "WatchAppSearchPresenter.kt", l = {40, 43}, m = "invokeSuspend")
public final class WatchAppSearchPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppSearchPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppSearchPresenter$start$Anon1(WatchAppSearchPresenter watchAppSearchPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = watchAppSearchPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WatchAppSearchPresenter$start$Anon1 watchAppSearchPresenter$start$Anon1 = new WatchAppSearchPresenter$start$Anon1(this.this$Anon0, kc4);
        watchAppSearchPresenter$start$Anon1.p$ = (lh4) obj;
        return watchAppSearchPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppSearchPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            WatchAppSearchPresenter$start$Anon1$allWatchApps$Anon1 watchAppSearchPresenter$start$Anon1$allWatchApps$Anon1 = new WatchAppSearchPresenter$start$Anon1$allWatchApps$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, watchAppSearchPresenter$start$Anon1$allWatchApps$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            this.this$Anon0.k.clear();
            this.this$Anon0.k.addAll((List) obj);
            this.this$Anon0.i();
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list2 = (List) obj;
        this.this$Anon0.j.clear();
        this.this$Anon0.j.addAll(list2);
        gh4 a3 = this.this$Anon0.c();
        WatchAppSearchPresenter$start$Anon1$allSearchedWatchApps$Anon1 watchAppSearchPresenter$start$Anon1$allSearchedWatchApps$Anon1 = new WatchAppSearchPresenter$start$Anon1$allSearchedWatchApps$Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = list2;
        this.label = 2;
        obj = kg4.a(a3, watchAppSearchPresenter$start$Anon1$allSearchedWatchApps$Anon1, this);
        if (obj == a) {
            return a;
        }
        this.this$Anon0.k.clear();
        this.this$Anon0.k.addAll((List) obj);
        this.this$Anon0.i();
        return cb4.a;
    }
}
