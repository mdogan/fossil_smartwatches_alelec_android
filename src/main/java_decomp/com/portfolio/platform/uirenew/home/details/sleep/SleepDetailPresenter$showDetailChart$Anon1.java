package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$Anon1", f = "SleepDetailPresenter.kt", l = {324}, m = "invokeSuspend")
public final class SleepDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$showDetailChart$Anon1(SleepDetailPresenter sleepDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepDetailPresenter$showDetailChart$Anon1 sleepDetailPresenter$showDetailChart$Anon1 = new SleepDetailPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        sleepDetailPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return sleepDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepDetailPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            SleepDetailPresenter$showDetailChart$Anon1$data$Anon1 sleepDetailPresenter$showDetailChart$Anon1$data$Anon1 = new SleepDetailPresenter$showDetailChart$Anon1$data$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, sleepDetailPresenter$showDetailChart$Anon1$data$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list = (List) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailPresenter", "showDetailChart - data=" + list);
        if (!list.isEmpty()) {
            this.this$Anon0.r.p(list);
        }
        return cb4.a;
    }
}
