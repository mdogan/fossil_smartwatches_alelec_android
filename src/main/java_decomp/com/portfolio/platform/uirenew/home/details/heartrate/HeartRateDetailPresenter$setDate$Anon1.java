package com.portfolio.platform.uirenew.home.details.heartrate;

import android.util.Pair;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.of3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$Anon1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$setDate$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$Anon1$Anon1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Date>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon1(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return PortfolioApp.W.c().k();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$setDate$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, Date date, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateDetailPresenter$setDate$Anon1 heartRateDetailPresenter$setDate$Anon1 = new HeartRateDetailPresenter$setDate$Anon1(this.this$Anon0, this.$date, kc4);
        heartRateDetailPresenter$setDate$Anon1.p$ = (lh4) obj;
        return heartRateDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$setDate$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Pair<Date, Date> a;
        kotlin.Pair pair;
        HeartRateDetailPresenter heartRateDetailPresenter;
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.f == null) {
                HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$Anon0;
                gh4 a3 = heartRateDetailPresenter2.b();
                Anon1 anon1 = new Anon1((kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = heartRateDetailPresenter2;
                this.label = 1;
                obj = kg4.a(a3, anon1, this);
                if (obj == a2) {
                    return a2;
                }
                heartRateDetailPresenter = heartRateDetailPresenter2;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
            this.this$Anon0.g = this.$date;
            Boolean s = sk2.s(this.$date);
            of3 m = this.this$Anon0.q;
            Date date = this.$date;
            wd4.a((Object) s, "isToday");
            m.a(date, sk2.c(this.this$Anon0.f, this.$date), s.booleanValue(), !sk2.c(new Date(), this.$date));
            a = sk2.a(this.$date, this.this$Anon0.f);
            wd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (kotlin.Pair) this.this$Anon0.h.a();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
            if (pair != null || !sk2.d((Date) pair.getFirst(), (Date) a.first) || !sk2.d((Date) pair.getSecond(), (Date) a.second)) {
                this.this$Anon0.h.a(new kotlin.Pair(a.first, a.second));
            } else {
                ri4 unused = this.this$Anon0.l();
                ri4 unused2 = this.this$Anon0.m();
                HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$Anon0;
                heartRateDetailPresenter3.c(heartRateDetailPresenter3.g);
            }
            return cb4.a;
        } else if (i == 1) {
            heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        heartRateDetailPresenter.f = (Date) obj;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
        this.this$Anon0.g = this.$date;
        Boolean s2 = sk2.s(this.$date);
        of3 m2 = this.this$Anon0.q;
        Date date2 = this.$date;
        wd4.a((Object) s2, "isToday");
        m2.a(date2, sk2.c(this.this$Anon0.f, this.$date), s2.booleanValue(), !sk2.c(new Date(), this.$date));
        a = sk2.a(this.$date, this.this$Anon0.f);
        wd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (kotlin.Pair) this.this$Anon0.h.a();
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$Anon0.h.a(new kotlin.Pair(a.first, a.second));
        return cb4.a;
    }
}
