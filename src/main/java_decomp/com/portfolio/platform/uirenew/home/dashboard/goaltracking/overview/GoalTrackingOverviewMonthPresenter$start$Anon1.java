package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qb3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewMonthPresenter$start$Anon1<T> implements dc<ps3<? extends List<GoalTrackingSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0152Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super TreeMap<Long, Float>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0152Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0152Anon1 anon1 = new C0152Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0152Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter = this.this$Anon0.this$Anon0.a;
                    Object a = goalTrackingOverviewMonthPresenter.f.a();
                    if (a != null) {
                        wd4.a(a, "mDate.value!!");
                        return goalTrackingOverviewMonthPresenter.a((Date) a, (List<GoalTrackingSummary>) this.this$Anon0.$data);
                    }
                    wd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingOverviewMonthPresenter$start$Anon1 goalTrackingOverviewMonthPresenter$start$Anon1, List list, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = goalTrackingOverviewMonthPresenter$start$Anon1;
            this.$data = list;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter;
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter2 = this.this$Anon0.a;
                gh4 a2 = goalTrackingOverviewMonthPresenter2.b();
                C0152Anon1 anon1 = new C0152Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = goalTrackingOverviewMonthPresenter2;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                goalTrackingOverviewMonthPresenter = goalTrackingOverviewMonthPresenter2;
            } else if (i == 1) {
                goalTrackingOverviewMonthPresenter = (GoalTrackingOverviewMonthPresenter) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            goalTrackingOverviewMonthPresenter.n = (TreeMap) obj;
            qb3 m = this.this$Anon0.a.o;
            TreeMap c = this.this$Anon0.a.n;
            if (c == null) {
                c = new TreeMap();
            }
            m.a(c);
            return cb4.a;
        }
    }

    @DexIgnore
    public GoalTrackingOverviewMonthPresenter$start$Anon1(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        this.a = goalTrackingOverviewMonthPresenter;
    }

    @DexIgnore
    public final void a(ps3<? extends List<GoalTrackingSummary>> ps3) {
        Status a2 = ps3.a();
        List list = (List) ps3.b();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "mDateTransformations observer");
        if (a2 != Status.DATABASE_LOADING) {
            if (list != null && (!wd4.a((Object) this.a.k, (Object) list))) {
                this.a.k = list;
                ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (kc4) null), 3, (Object) null);
            }
            this.a.o.c(!this.a.q.H());
        }
    }
}
