package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.j43;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.k42;
import com.fossil.blesdk.obfuscated.k43;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.m43;
import com.fossil.blesdk.obfuscated.mc;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.ra2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.us2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemeFragment extends as2 implements k43 {
    @DexIgnore
    public ur3<ra2> j;
    @DexIgnore
    public j43 k;
    @DexIgnore
    public us2 l;
    @DexIgnore
    public long m; // = 500;
    @DexIgnore
    public k42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements us2.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public b(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public void a(WatchFaceWrapper watchFaceWrapper) {
            wd4.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked backgroundWrapper=" + watchFaceWrapper);
            CustomizeThemeFragment.b(this.a).a(watchFaceWrapper);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ j43 b(CustomizeThemeFragment customizeThemeFragment) {
        j43 j43 = customizeThemeFragment.k;
        if (j43 != null) {
            return j43;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void n(List<WatchFaceWrapper> list) {
        wd4.b(list, "watchFaceWrappers");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "showWatchFaces watchFaceWrappers=" + list);
        us2 us2 = this.l;
        if (us2 != null) {
            us2.a(list);
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            k42 k42 = this.n;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) dianaCustomizeEditActivity, (lc.b) k42).a(DianaCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                j43 j43 = this.k;
                if (j43 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        j43.a(dianaCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ra2 ra2 = (ra2) ra.a(layoutInflater, R.layout.fragment_customize_theme, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new m43(this)).a(this);
        this.j = new ur3<>(this, ra2);
        wd4.a((Object) ra2, "binding");
        return ra2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        j43 j43 = this.k;
        if (j43 != null) {
            j43.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        j43 j43 = this.k;
        if (j43 != null) {
            j43.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        us2 us2 = new us2((ArrayList) null, (us2.c) null, 3, (rd4) null);
        us2.a((us2.c) new b(this));
        this.l = us2;
        ur3<ra2> ur3 = this.j;
        if (ur3 != null) {
            ra2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator((RecyclerView.j) null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.l);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n(int i) {
        if (i >= 0) {
            us2 us2 = this.l;
            if (us2 != null) {
                us2.a(i);
            }
            ur3<ra2> ur3 = this.j;
            if (ur3 != null) {
                ra2 a2 = ur3.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.q;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager != null) {
                        int I = ((LinearLayoutManager) layoutManager).I();
                        RecyclerView.m layoutManager2 = recyclerView.getLayoutManager();
                        if (layoutManager2 == null) {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        } else if (i < ((LinearLayoutManager) layoutManager2).G() || i > I) {
                            ri4 unused = mg4.b(mh4.a(zh4.c()), (CoroutineContext) null, (CoroutineStart) null, new CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(recyclerView, (kc4) null, this, i), 3, (Object) null);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(j43 j43) {
        wd4.b(j43, "presenter");
        this.k = j43;
    }
}
