package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.s53;
import com.fossil.blesdk.obfuscated.t53;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppSearchPresenter extends s53 {
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ t53 l;
    @DexIgnore
    public /* final */ WatchAppRepository m;
    @DexIgnore
    public /* final */ fn2 n;

    @DexIgnore
    public WatchAppSearchPresenter(t53 t53, WatchAppRepository watchAppRepository, fn2 fn2) {
        wd4.b(t53, "mView");
        wd4.b(watchAppRepository, "mWatchAppRepository");
        wd4.b(fn2, "sharedPreferencesManager");
        this.l = t53;
        this.m = watchAppRepository;
        this.n = fn2;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.i = "";
        this.l.u();
        i();
    }

    @DexIgnore
    public final void i() {
        if (this.k.isEmpty()) {
            this.l.b(a((List<WatchApp>) wb4.d(this.j)));
        } else {
            this.l.e(a((List<WatchApp>) wb4.d(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            t53 t53 = this.l;
            String str = this.i;
            if (str != null) {
                t53.a(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppSearchPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wd4.b(str, "watchAppTop");
        wd4.b(str2, "watchAppMiddle");
        wd4.b(str3, "watchAppBottom");
        this.f = str;
        this.h = str3;
        this.g = str2;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new WatchAppSearchPresenter$search$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(WatchApp watchApp) {
        wd4.b(watchApp, "selectedWatchApp");
        List<String> y = this.n.y();
        wd4.a((Object) y, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!y.contains(watchApp.getWatchappId())) {
            y.add(0, watchApp.getWatchappId());
            if (y.size() > 5) {
                y = y.subList(0, 5);
            }
            this.n.d(y);
        }
        this.l.a(watchApp);
    }

    @DexIgnore
    public final List<Pair<WatchApp, String>> a(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : list) {
            if (!wd4.a((Object) next.getWatchappId(), (Object) "empty")) {
                String watchappId = next.getWatchappId();
                if (wd4.a((Object) watchappId, (Object) this.f)) {
                    arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_TOP_KEY));
                } else if (wd4.a((Object) watchappId, (Object) this.g)) {
                    arrayList.add(new Pair(next, "middle"));
                } else if (wd4.a((Object) watchappId, (Object) this.h)) {
                    arrayList.add(new Pair(next, "bottom"));
                } else {
                    arrayList.add(new Pair(next, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.f);
            bundle.putString("middle", this.g);
            bundle.putString("bottom", this.h);
        }
        return bundle;
    }
}
