package com.portfolio.platform.uirenew.home.profile.unit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.rj3;
import com.fossil.blesdk.obfuscated.tj3;
import com.fossil.blesdk.obfuscated.vj3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PreferredUnitActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public vj3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            Intent intent = new Intent(context, PreferredUnitActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        rj3 rj3 = (rj3) getSupportFragmentManager().a((int) R.id.content);
        if (rj3 == null) {
            rj3 = rj3.n.b();
            a((Fragment) rj3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new tj3(rj3)).a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
