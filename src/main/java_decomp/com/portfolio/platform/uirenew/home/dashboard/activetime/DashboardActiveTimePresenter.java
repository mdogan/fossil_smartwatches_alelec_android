package com.portfolio.platform.uirenew.home.dashboard.activetime;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.a83;
import com.fossil.blesdk.obfuscated.b83;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.z73;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardActiveTimePresenter extends z73 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> g;
    @DexIgnore
    public /* final */ a83 h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ ActivitySummaryDao k;
    @DexIgnore
    public /* final */ FitnessDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ i42 n;
    @DexIgnore
    public /* final */ yk2 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DashboardActiveTimePresenter(a83 a83, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42, yk2 yk2) {
        wd4.b(a83, "mView");
        wd4.b(summariesRepository, "mSummariesRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(activitySummaryDao, "mActivitySummaryDao");
        wd4.b(fitnessDatabase, "mFitnessDatabase");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(i42, "mAppExecutors");
        wd4.b(yk2, "mFitnessHelper");
        this.h = a83;
        this.i = summariesRepository;
        this.j = fitnessDataRepository;
        this.k = activitySummaryDao;
        this.l = fitnessDatabase;
        this.m = userRepository;
        this.n = i42;
        this.o = yk2;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!sk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardActiveTimePresenter$initDataSource$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                LiveData<rd<ActivitySummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    a83 a83 = this.h;
                    if (a83 != null) {
                        pagedList.a((LifecycleOwner) (b83) a83);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("DashboardActiveTimePresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.g;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimePresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
