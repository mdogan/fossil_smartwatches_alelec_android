package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hl2;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pj2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeViewModel extends jc {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((rd4) null);
    @DexIgnore
    public HybridPreset c;
    @DexIgnore
    public MutableLiveData<HybridPreset> d; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson j; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ LiveData<MicroApp> l;
    @DexIgnore
    public /* final */ LiveData<Boolean> m;
    @DexIgnore
    public /* final */ dc<HybridPreset> n;
    @DexIgnore
    public /* final */ HybridPresetRepository o;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository p;
    @DexIgnore
    public /* final */ MicroAppRepository q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HybridCustomizeViewModel.r;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public b(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final void a(HybridPreset hybridPreset) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.s.a();
            local.d(a2, "current preset change=" + hybridPreset);
            if (hybridPreset != null) {
                String str = (String) this.a.g.a();
                String str2 = (String) this.a.h.a();
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    boolean z = true;
                    if (!wd4.a((Object) hybridPresetAppSetting.getPosition(), (Object) str) || cg4.b(hybridPresetAppSetting.getAppId(), str2, true)) {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t;
                if (hybridPresetAppSetting2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = HybridCustomizeViewModel.s.a();
                    local2.d(a3, "Update new microapp id=" + hybridPresetAppSetting2.getAppId() + " at position=" + str);
                    this.a.h.a(hybridPresetAppSetting2.getAppId());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public c(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<MicroApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.s.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            HybridCustomizeViewModel hybridCustomizeViewModel = this.a;
            wd4.a((Object) str, "id");
            MicroApp c = hybridCustomizeViewModel.c(str);
            if (c != null) {
                this.a.i.a(c);
            }
            return this.a.i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public d(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.s.a();
            local.d(a2, "transformMicroAppPosToId pos=" + str);
            HybridPreset hybridPreset = (HybridPreset) this.a.d.a();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wd4.a((Object) ((HybridPresetAppSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                if (hybridPresetAppSetting != null) {
                    this.a.h.b(hybridPresetAppSetting.getAppId());
                }
            }
            return this.a.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public e(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(HybridPreset hybridPreset) {
            ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
            Gson c = this.a.j;
            HybridPreset e = this.a.c;
            if (e != null) {
                boolean b = tj2.b(buttons, c, e.getButtons());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DianaCustomizeViewModel.G.a();
                local.d(a2, "isTheSameHybridAppSetting " + b);
                if (b) {
                    this.a.e.a(false);
                } else {
                    this.a.e.a(true);
                }
                return this.a.e;
            }
            wd4.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = HybridCustomizeViewModel.class.getSimpleName();
        wd4.a((Object) simpleName, "HybridCustomizeViewModel::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public HybridCustomizeViewModel(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        wd4.b(hybridPresetRepository, "mHybridPresetRepository");
        wd4.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        wd4.b(microAppRepository, "mMicroAppRepository");
        this.o = hybridPresetRepository;
        this.p = microAppLastSettingRepository;
        this.q = microAppRepository;
        LiveData<String> b2 = ic.b(this.g, new d(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026dMicroAppIdLiveData\n    }");
        this.k = b2;
        LiveData<MicroApp> b3 = ic.b(this.k, new c(this));
        wd4.a((Object) b3, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.l = b3;
        LiveData<Boolean> b4 = ic.b(this.d, new e(this));
        wd4.a((Object) b4, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.m = b4;
        this.n = new b(this);
    }

    @DexIgnore
    public final List<MicroApp> b(String str) {
        wd4.b(str, "category");
        ArrayList<MicroApp> arrayList = this.f;
        ArrayList arrayList2 = new ArrayList();
        for (T next : arrayList) {
            if (((MicroApp) next).getCategories().contains(str)) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public final MutableLiveData<HybridPreset> c() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        LiveData<Boolean> liveData = this.m;
        if (liveData != null) {
            return liveData;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final HybridPreset e() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<MicroApp> f() {
        LiveData<MicroApp> liveData = this.l;
        if (liveData != null) {
            return liveData;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> g() {
        return this.g;
    }

    @DexIgnore
    public final void h() {
        this.f.clear();
        this.f.addAll(this.q.getAllMicroApp(PortfolioApp.W.c().e()));
    }

    @DexIgnore
    public final boolean i() {
        Boolean a2 = this.e.a();
        if (a2 == null || a2 == null) {
            return false;
        }
        return a2.booleanValue();
    }

    @DexIgnore
    public final ri4 a(String str, String str2) {
        wd4.b(str, "presetId");
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new HybridCustomizeViewModel$init$Anon1(this, str, str2, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final MicroApp c(String str) {
        T t;
        wd4.b(str, "microAppId");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wd4.a((Object) str, (Object) ((MicroApp) t).getId())) {
                break;
            }
        }
        return (MicroApp) t;
    }

    @DexIgnore
    public final Parcelable d(String str) {
        T t;
        T t2;
        wd4.b(str, "appId");
        Ringtone ringtone = null;
        if (hl2.c.d(str)) {
            HybridPreset a2 = c().a();
            String str2 = "";
            if (a2 != null) {
                Iterator<T> it = a2.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wd4.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (hybridPresetAppSetting != null) {
                    String settings = hybridPresetAppSetting.getSettings();
                    if (settings != null) {
                        str2 = settings;
                    }
                }
            }
            if (str2.length() == 0) {
                MicroAppLastSetting microAppLastSetting = this.p.getMicroAppLastSetting(str);
                if (microAppLastSetting != null) {
                    str2 = microAppLastSetting.getSetting();
                }
                if (!TextUtils.isEmpty(str2)) {
                    if (a2 != null) {
                        HybridPreset clone = a2.clone();
                        Iterator<T> it2 = clone.getButtons().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it2.next();
                            if (wd4.a((Object) ((HybridPresetAppSetting) t).getAppId(), (Object) str)) {
                                break;
                            }
                        }
                        HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t;
                        if (hybridPresetAppSetting2 != null) {
                            hybridPresetAppSetting2.setSettings(str2);
                            a(clone);
                        }
                    }
                } else if (wd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                    List<Ringtone> c2 = AppHelper.f.c();
                    if (!c2.isEmpty()) {
                        str2 = sj2.a(c2.get(0));
                    }
                }
            }
            if (!pj2.a(str2)) {
                try {
                    if (wd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                        CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) this.j.a(str2, CommuteTimeSetting.class);
                        if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                            return commuteTimeSetting;
                        }
                    } else if (wd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                        SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) this.j.a(str2, SecondTimezoneSetting.class);
                        if (!TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                            return secondTimezoneSetting;
                        }
                    } else if (wd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                        Ringtone ringtone2 = (Ringtone) this.j.a(str2, Ringtone.class);
                        if (!TextUtils.isEmpty(ringtone2.getRingtoneName())) {
                            ringtone = ringtone2;
                        }
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "exception when parse setting from json " + e2);
                }
            }
        }
        return ringtone;
    }

    @DexIgnore
    public final boolean e(String str) {
        wd4.b(str, "microAppId");
        HybridPreset a2 = this.d.a();
        if (a2 == null) {
            return false;
        }
        Iterator<HybridPresetAppSetting> it = a2.getButtons().iterator();
        while (it.hasNext()) {
            if (wd4.a((Object) it.next().getAppId(), (Object) str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void f(String str) {
        wd4.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "setSelectedMicroApp watchAppPos=" + str);
        this.g.a(str);
    }

    @DexIgnore
    public final ri4 a(String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2) {
        wd4.b(str, "presetId");
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new HybridCustomizeViewModel$initFromSaveInstanceState$Anon1(this, str, hybridPreset, hybridPreset2, str2, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        wd4.b(hybridPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "savePreset newPreset=" + hybridPreset);
        this.d.a(hybridPreset.clone());
    }

    @DexIgnore
    public void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "onCleared originalPreset=" + this.c + " currentPreset=" + this.d.a());
        this.d.b(this.n);
        super.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(String str, kc4<? super cb4> kc4) {
        HybridCustomizeViewModel$initializePreset$Anon1 hybridCustomizeViewModel$initializePreset$Anon1;
        Object a2;
        int i2;
        HybridPreset hybridPreset;
        HybridCustomizeViewModel hybridCustomizeViewModel;
        HybridCustomizeViewModel hybridCustomizeViewModel2;
        gh4 a3;
        HybridCustomizeViewModel$initializePreset$Anon2 hybridCustomizeViewModel$initializePreset$Anon2;
        if (kc4 instanceof HybridCustomizeViewModel$initializePreset$Anon1) {
            hybridCustomizeViewModel$initializePreset$Anon1 = (HybridCustomizeViewModel$initializePreset$Anon1) kc4;
            int i3 = hybridCustomizeViewModel$initializePreset$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                hybridCustomizeViewModel$initializePreset$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = hybridCustomizeViewModel$initializePreset$Anon1.result;
                a2 = oc4.a();
                i2 = hybridCustomizeViewModel$initializePreset$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = r;
                    local.d(str2, "initializePreset presetId=" + str);
                    gh4 b2 = zh4.b();
                    HybridCustomizeViewModel$initializePreset$preset$Anon1 hybridCustomizeViewModel$initializePreset$preset$Anon1 = new HybridCustomizeViewModel$initializePreset$preset$Anon1(this, str, (kc4) null);
                    hybridCustomizeViewModel$initializePreset$Anon1.L$Anon0 = this;
                    hybridCustomizeViewModel$initializePreset$Anon1.L$Anon1 = str;
                    hybridCustomizeViewModel$initializePreset$Anon1.label = 1;
                    obj = kg4.a(b2, hybridCustomizeViewModel$initializePreset$preset$Anon1, hybridCustomizeViewModel$initializePreset$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                    hybridCustomizeViewModel2 = this;
                } else if (i2 == 1) {
                    str = (String) hybridCustomizeViewModel$initializePreset$Anon1.L$Anon1;
                    hybridCustomizeViewModel2 = (HybridCustomizeViewModel) hybridCustomizeViewModel$initializePreset$Anon1.L$Anon0;
                    za4.a(obj);
                } else if (i2 == 2) {
                    hybridPreset = (HybridPreset) hybridCustomizeViewModel$initializePreset$Anon1.L$Anon2;
                    String str3 = (String) hybridCustomizeViewModel$initializePreset$Anon1.L$Anon1;
                    hybridCustomizeViewModel = (HybridCustomizeViewModel) hybridCustomizeViewModel$initializePreset$Anon1.L$Anon0;
                    za4.a(obj);
                    if (hybridPreset != null) {
                        hybridCustomizeViewModel.c = hybridPreset.clone();
                        hybridCustomizeViewModel.d.a(hybridPreset.clone());
                    }
                    return cb4.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                HybridPreset hybridPreset2 = (HybridPreset) obj;
                a3 = zh4.a();
                hybridCustomizeViewModel$initializePreset$Anon2 = new HybridCustomizeViewModel$initializePreset$Anon2(hybridCustomizeViewModel2, (kc4) null);
                hybridCustomizeViewModel$initializePreset$Anon1.L$Anon0 = hybridCustomizeViewModel2;
                hybridCustomizeViewModel$initializePreset$Anon1.L$Anon1 = str;
                hybridCustomizeViewModel$initializePreset$Anon1.L$Anon2 = hybridPreset2;
                hybridCustomizeViewModel$initializePreset$Anon1.label = 2;
                if (kg4.a(a3, hybridCustomizeViewModel$initializePreset$Anon2, hybridCustomizeViewModel$initializePreset$Anon1) != a2) {
                    return a2;
                }
                hybridPreset = hybridPreset2;
                hybridCustomizeViewModel = hybridCustomizeViewModel2;
                if (hybridPreset != null) {
                }
                return cb4.a;
            }
        }
        hybridCustomizeViewModel$initializePreset$Anon1 = new HybridCustomizeViewModel$initializePreset$Anon1(this, kc4);
        Object obj2 = hybridCustomizeViewModel$initializePreset$Anon1.result;
        a2 = oc4.a();
        i2 = hybridCustomizeViewModel$initializePreset$Anon1.label;
        if (i2 != 0) {
        }
        HybridPreset hybridPreset22 = (HybridPreset) obj2;
        a3 = zh4.a();
        hybridCustomizeViewModel$initializePreset$Anon2 = new HybridCustomizeViewModel$initializePreset$Anon2(hybridCustomizeViewModel2, (kc4) null);
        hybridCustomizeViewModel$initializePreset$Anon1.L$Anon0 = hybridCustomizeViewModel2;
        hybridCustomizeViewModel$initializePreset$Anon1.L$Anon1 = str;
        hybridCustomizeViewModel$initializePreset$Anon1.L$Anon2 = hybridPreset22;
        hybridCustomizeViewModel$initializePreset$Anon1.label = 2;
        if (kg4.a(a3, hybridCustomizeViewModel$initializePreset$Anon2, hybridCustomizeViewModel$initializePreset$Anon1) != a2) {
        }
    }
}
