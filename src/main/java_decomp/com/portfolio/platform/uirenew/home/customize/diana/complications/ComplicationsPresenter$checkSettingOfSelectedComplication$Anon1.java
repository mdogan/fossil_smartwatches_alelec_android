package com.portfolio.platform.uirenew.home.customize.diana.complications;

import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.pk2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import java.util.Iterator;
import kotlin.Triple;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1", f = "ComplicationsPresenter.kt", l = {260}, m = "invokeSuspend")
public final class ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $complicationId;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1$Anon1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Parcelable>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 complicationsPresenter$checkSettingOfSelectedComplication$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = complicationsPresenter$checkSettingOfSelectedComplication$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return ComplicationsPresenter.g(this.this$Anon0.this$Anon0).g(this.this$Anon0.$complicationId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1(ComplicationsPresenter complicationsPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = complicationsPresenter;
        this.$complicationId = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 complicationsPresenter$checkSettingOfSelectedComplication$Anon1 = new ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1(this.this$Anon0, this.$complicationId, kc4);
        complicationsPresenter$checkSettingOfSelectedComplication$Anon1.p$ = (lh4) obj;
        return complicationsPresenter$checkSettingOfSelectedComplication$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0091  */
    public final Object invokeSuspend(T t) {
        boolean z;
        Ref$ObjectRef ref$ObjectRef;
        T t2;
        Ref$ObjectRef ref$ObjectRef2;
        T a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(t);
            lh4 lh4 = this.p$;
            ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            z = pk2.c.d(this.$complicationId);
            if (z) {
                gh4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = ref$ObjectRef;
                this.Z$Anon0 = z;
                this.L$Anon2 = ref$ObjectRef;
                this.label = 1;
                t = kg4.a(a2, anon1, this);
                if (t == a) {
                    return a;
                }
                ref$ObjectRef2 = ref$ObjectRef;
            }
            FLogger.INSTANCE.getLocal().d(ComplicationsPresenter.w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((Parcelable) ref$ObjectRef.element));
            if (((Parcelable) ref$ObjectRef.element) != null) {
                DianaPreset a3 = ComplicationsPresenter.g(this.this$Anon0).c().a();
                if (a3 != null) {
                    DianaPreset clone = a3.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it.next();
                        if (pc4.a(wd4.a((Object) ((DianaPresetComplicationSetting) t2).getId(), (Object) this.$complicationId)).booleanValue()) {
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                    if (dianaPresetComplicationSetting != null) {
                        dianaPresetComplicationSetting.setSettings(this.this$Anon0.l.a((Object) (Parcelable) ref$ObjectRef.element));
                        ComplicationsPresenter.g(this.this$Anon0).a(clone);
                    }
                }
            }
            this.this$Anon0.k.a(new Triple(this.$complicationId, pc4.a(z), (Parcelable) ref$ObjectRef.element));
            return cb4.a;
        } else if (i == 1) {
            ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon2;
            boolean z2 = this.Z$Anon0;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(t);
            z = z2;
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ref$ObjectRef2.element = (Parcelable) t;
        FLogger.INSTANCE.getLocal().d(ComplicationsPresenter.w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((Parcelable) ref$ObjectRef.element));
        if (((Parcelable) ref$ObjectRef.element) != null) {
        }
        this.this$Anon0.k.a(new Triple(this.$complicationId, pc4.a(z), (Parcelable) ref$ObjectRef.element));
        return cb4.a;
    }
}
