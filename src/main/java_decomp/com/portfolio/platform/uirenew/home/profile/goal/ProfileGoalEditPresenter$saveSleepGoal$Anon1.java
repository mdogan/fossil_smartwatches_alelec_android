package com.portfolio.platform.uirenew.home.profile.goal;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wh3;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.enums.Status;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$Anon1", f = "ProfileGoalEditPresenter.kt", l = {}, m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveSleepGoal$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements dc<ps3<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter$saveSleepGoal$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexIgnore
        public a(ProfileGoalEditPresenter$saveSleepGoal$Anon1 profileGoalEditPresenter$saveSleepGoal$Anon1, LiveData liveData) {
            this.a = profileGoalEditPresenter$saveSleepGoal$Anon1;
            this.b = liveData;
        }

        @DexIgnore
        public final void a(ps3<Integer> ps3) {
            FLogger.INSTANCE.getLocal().d(ProfileGoalEditPresenter.u, "saveSleepGoal observe");
            if ((ps3 != null ? ps3.f() : null) != Status.SUCCESS || ps3.d() == null) {
                if ((ps3 != null ? ps3.f() : null) == Status.ERROR) {
                    wh3 m = this.a.this$Anon0.m();
                    Integer c = ps3.c();
                    if (c != null) {
                        int intValue = c.intValue();
                        String e = ps3.e();
                        if (e != null) {
                            m.d(intValue, e);
                            this.b.a((LifecycleOwner) this.a.this$Anon0.m());
                            this.a.this$Anon0.o();
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                return;
            }
            this.a.this$Anon0.p();
            this.b.a((LifecycleOwner) this.a.this$Anon0.m());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveSleepGoal$Anon1(ProfileGoalEditPresenter profileGoalEditPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileGoalEditPresenter$saveSleepGoal$Anon1 profileGoalEditPresenter$saveSleepGoal$Anon1 = new ProfileGoalEditPresenter$saveSleepGoal$Anon1(this.this$Anon0, kc4);
        profileGoalEditPresenter$saveSleepGoal$Anon1.p$ = (lh4) obj;
        return profileGoalEditPresenter$saveSleepGoal$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$saveSleepGoal$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            LiveData<ps3<Integer>> updateLastSleepGoal = this.this$Anon0.r.updateLastSleepGoal(this.this$Anon0.j);
            wh3 m = this.this$Anon0.m();
            if (m != null) {
                updateLastSleepGoal.a((as2) m, new a(this, updateLastSleepGoal));
                return cb4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
