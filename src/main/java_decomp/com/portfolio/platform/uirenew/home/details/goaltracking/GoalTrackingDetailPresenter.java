package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ef3;
import com.fossil.blesdk.obfuscated.ff3;
import com.fossil.blesdk.obfuscated.gf3;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rl4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter extends ef3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ pl4 k; // = rl4.a(false, 1, (Object) null);
    @DexIgnore
    public List<GoalTrackingSummary> l; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary m;
    @DexIgnore
    public List<GoalTrackingData> n;
    @DexIgnore
    public LiveData<ps3<List<GoalTrackingSummary>>> o;
    @DexIgnore
    public Listing<GoalTrackingData> p;
    @DexIgnore
    public /* final */ ff3 q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;
    @DexIgnore
    public /* final */ GoalTrackingDao s;
    @DexIgnore
    public /* final */ GoalTrackingDatabase t;
    @DexIgnore
    public /* final */ i42 u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore
        public b(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<ps3<List<GoalTrackingSummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.r.getSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingDetailPresenter(ff3 ff3, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, i42 i42) {
        wd4.b(ff3, "mView");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(goalTrackingDao, "mGoalTrackingDao");
        wd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wd4.b(i42, "mAppExecutors");
        this.q = ff3;
        this.r = goalTrackingRepository;
        this.s = goalTrackingDao;
        this.t = goalTrackingDatabase;
        this.u = i42;
        LiveData<ps3<List<GoalTrackingSummary>>> b2 = ic.b(this.h, new b(this));
        wd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.o = b2;
    }

    @DexIgnore
    public final void d(Date date) {
        h();
        GoalTrackingRepository goalTrackingRepository = this.r;
        this.p = goalTrackingRepository.getGoalTrackingDataPaging(date, goalTrackingRepository, this.s, this.t, this.u, this);
        Listing<GoalTrackingData> listing = this.p;
        if (listing != null) {
            LiveData<rd<GoalTrackingData>> pagedList = listing.getPagedList();
            if (pagedList != null) {
                ff3 ff3 = this.q;
                if (ff3 != null) {
                    pagedList.a((gf3) ff3, new GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1(this));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        n();
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<ps3<List<GoalTrackingSummary>>> liveData = this.o;
        ff3 ff3 = this.q;
        if (ff3 != null) {
            liveData.a((LifecycleOwner) (gf3) ff3);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<GoalTrackingData> listing = this.p;
            if (listing != null) {
                LiveData<rd<GoalTrackingData>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    ff3 ff3 = this.q;
                    if (ff3 != null) {
                        pagedList.a((LifecycleOwner) (gf3) ff3);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.p;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void j() {
        Date l2 = sk2.l(this.g);
        wd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        c(l2);
    }

    @DexIgnore
    public void k() {
        Date m2 = sk2.m(this.g);
        wd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        c(m2);
    }

    @DexIgnore
    public void l() {
        this.q.a(this);
    }

    @DexIgnore
    public final ri4 m() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$showDetailChart$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void n() {
        LiveData<ps3<List<GoalTrackingSummary>>> liveData = this.o;
        ff3 ff3 = this.q;
        if (ff3 != null) {
            liveData.a((gf3) ff3, new GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void c(Date date) {
        wd4.b(date, "date");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$setDate$Anon1(this, date, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        d(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public void a(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$addRecord$Anon1(this, date, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(GoalTrackingData goalTrackingData) {
        wd4.b(goalTrackingData, OrmLiteConfigUtil.RAW_DIR_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$deleteRecord$Anon1(this, goalTrackingData, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final GoalTrackingSummary a(Date date, List<GoalTrackingSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (sk2.d(((GoalTrackingSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (GoalTrackingSummary) t2;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        if (eVar.a()) {
            this.q.f();
        }
    }
}
