package com.portfolio.platform.uirenew.home.profile.opt;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yi3;
import com.fossil.blesdk.obfuscated.zi3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileOptInPresenter extends yi3 {
    @DexIgnore
    public /* final */ String f; // = "ProfileOptInPresenter";
    @DexIgnore
    public MFUser g;
    @DexIgnore
    public /* final */ zi3 h;
    @DexIgnore
    public /* final */ UpdateUser i;
    @DexIgnore
    public /* final */ UserRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public a(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.i().E0();
            AnalyticsHelper.f.c().a(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.i().E(!this.b);
            this.a.i().E0();
            zi3 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public b(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.i().E0();
            AnalyticsHelper.f.c().a(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.i().H(!this.b);
            this.a.i().E0();
            zi3 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public ProfileOptInPresenter(zi3 zi3, UpdateUser updateUser, UserRepository userRepository) {
        wd4.b(zi3, "mView");
        wd4.b(updateUser, "mUpdateUser");
        wd4.b(userRepository, "mUserRepository");
        this.h = zi3;
        this.i = updateUser;
        this.j = userRepository;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(this.f, "presenter starts: Get user information");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileOptInPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(this.f, "presenter stop");
    }

    @DexIgnore
    public final UserRepository h() {
        return this.j;
    }

    @DexIgnore
    public final zi3 i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h.a(this);
    }

    @DexIgnore
    public void b(boolean z) {
        MFUser mFUser = this.g;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f;
            local.d(str, "setSubcribeEmailData() called with: checked = " + z);
            mFUser.setEmailOptIn(z);
            this.h.Z();
            if (this.i.a(new UpdateUser.b(mFUser), new b(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.f, "mMfUser is null");
    }

    @DexIgnore
    public void a(boolean z) {
        MFUser mFUser = this.g;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f;
            local.d(str, "setAnonymouslySendUsageData() called with: checked = " + z);
            mFUser.setDiagnosticEnabled(z);
            this.h.Z();
            if (this.i.a(new UpdateUser.b(mFUser), new a(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.f, "mMfUser is null");
    }
}
