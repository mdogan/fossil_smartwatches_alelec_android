package com.portfolio.platform.uirenew.home.details.calories;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<ActivitySample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesDetailPresenter$start$Anon1.Anon3.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1(CaloriesDetailPresenter$start$Anon1.Anon3.Anon1 anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1 = new CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1(this.this$Anon0, kc4);
        caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1.p$ = (lh4) obj;
        return caloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesDetailPresenter$start$Anon1$Anon3$Anon1$samples$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            CaloriesDetailPresenter caloriesDetailPresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
            return caloriesDetailPresenter.a(caloriesDetailPresenter.g, (List<ActivitySample>) this.this$Anon0.this$Anon0.a.this$Anon0.l);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
