package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.fc3;
import com.fossil.blesdk.obfuscated.gc3;
import com.fossil.blesdk.obfuscated.hc3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.enums.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewDayPresenter extends fc3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<ps3<List<HeartRateSample>>> g; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<List<WorkoutSession>>> h; // = new MutableLiveData();
    @DexIgnore
    public /* final */ gc3 i;
    @DexIgnore
    public /* final */ HeartRateSampleRepository j;
    @DexIgnore
    public /* final */ WorkoutSessionRepository k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<ps3<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore
        public b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(ps3<? extends List<WorkoutSession>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == Status.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.i.a(false, new ArrayList());
            } else {
                this.a.i.a(true, list);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewDayPresenter(gc3 gc3, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        wd4.b(gc3, "mView");
        wd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.i = gc3;
        this.j = heartRateSampleRepository;
        this.k = workoutSessionRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        Date date = heartRateOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        wd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<ps3<List<HeartRateSample>>> liveData = this.g;
        gc3 gc3 = this.i;
        if (gc3 != null) {
            liveData.a((hc3) gc3, new HeartRateOverviewDayPresenter$start$Anon1(this));
            this.h.a((LifecycleOwner) this.i, new b(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        LiveData<ps3<List<HeartRateSample>>> liveData = this.g;
        gc3 gc3 = this.i;
        if (gc3 != null) {
            liveData.a((LifecycleOwner) (hc3) gc3);
            this.h.a((LifecycleOwner) this.i);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wd4.d("mDate");
                throw null;
            } else if (sk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("HeartRateOverviewDayPresenter", sb.toString());
                    return;
                }
                wd4.d("mDate");
                throw null;
            }
        }
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("HeartRateOverviewDayPresenter", sb2.toString());
            HeartRateSampleRepository heartRateSampleRepository = this.j;
            Date date4 = this.f;
            if (date4 == null) {
                wd4.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.g = heartRateSampleRepository.getHeartRateSamples(date4, date4, false);
                WorkoutSessionRepository workoutSessionRepository = this.k;
                Date date5 = this.f;
                if (date5 == null) {
                    wd4.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.h = workoutSessionRepository.getWorkoutSessions(date5, date5, true);
                } else {
                    wd4.d("mDate");
                    throw null;
                }
            } else {
                wd4.d("mDate");
                throw null;
            }
        } else {
            wd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        be4 be4 = be4.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }
}
