package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.t33;
import com.fossil.blesdk.obfuscated.u33;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchSecondTimezonePresenter extends t33 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public SecondTimezoneSetting f;
    @DexIgnore
    public ArrayList<SecondTimezoneSetting> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ u33 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = SearchSecondTimezonePresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "SearchSecondTimezonePres\u2026er::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public SearchSecondTimezonePresenter(u33 u33) {
        wd4.b(u33, "mView");
        this.i = u33;
    }

    @DexIgnore
    public void f() {
        if (this.g.isEmpty()) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchSecondTimezonePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        } else {
            this.i.q(this.g);
        }
        SecondTimezoneSetting secondTimezoneSetting = this.f;
        if (secondTimezoneSetting != null) {
            this.i.I(secondTimezoneSetting.getTimeZoneName());
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.i.a(this);
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        try {
            this.f = (SecondTimezoneSetting) this.h.a(str, SecondTimezoneSetting.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local.d(str2, "exception when parse second timezone setting " + e);
        }
    }
}
