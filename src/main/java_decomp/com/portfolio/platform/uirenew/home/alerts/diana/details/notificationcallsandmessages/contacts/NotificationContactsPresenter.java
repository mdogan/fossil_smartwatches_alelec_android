package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.tx2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ww2;
import com.fossil.blesdk.obfuscated.xw2;
import com.fossil.blesdk.obfuscated.yw2;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsPresenter extends ww2 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public /* final */ List<ContactWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ List<ContactWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ xw2 h;
    @DexIgnore
    public /* final */ k62 i;
    @DexIgnore
    public /* final */ qx2 j;
    @DexIgnore
    public /* final */ tx2 k;
    @DexIgnore
    public /* final */ LoaderManager l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsPresenter.m;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.d<tx2.c, j62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter a;

        @DexIgnore
        public b(NotificationContactsPresenter notificationContactsPresenter) {
            this.a = notificationContactsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tx2.c cVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.a.h.close();
        }

        @DexIgnore
        public void a(j62.a aVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), ".Inside mSaveContactGroupsNotification onError");
            this.a.h.close();
        }
    }

    /*
    static {
        String simpleName = NotificationContactsPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsPresenter(xw2 xw2, k62 k62, qx2 qx2, tx2 tx2, LoaderManager loaderManager) {
        wd4.b(xw2, "mView");
        wd4.b(k62, "mUseCaseHandler");
        wd4.b(qx2, "mGetAllContactGroup");
        wd4.b(tx2, "mSaveContactGroupsNotification");
        wd4.b(loaderManager, "mLoaderManager");
        this.h = xw2;
        this.i = k62;
        this.j = qx2;
        this.k = tx2;
        this.l = loaderManager;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(m, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        cn2 cn2 = cn2.d;
        xw2 xw2 = this.h;
        if (xw2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (cn2.a(cn2, ((yw2) xw2).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(m, "stop");
    }

    @DexIgnore
    public void h() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List<T> c = wb4.c(this.g, this.f);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<T> it = c.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            Contact contact = ((ContactWrapper) next).getContact();
            if (contact != null) {
                num = Integer.valueOf(contact.getContactId());
            }
            Object obj = linkedHashMap.get(num);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(num, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it2 = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it2.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it2.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<ContactWrapper> arrayList3 = new ArrayList<>();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            tb4.a(arrayList3, (List) value.getValue());
        }
        for (ContactWrapper contactWrapper : arrayList3) {
            boolean z2 = false;
            for (ContactWrapper contact2 : this.g) {
                Contact contact3 = contact2.getContact();
                Integer valueOf = contact3 != null ? Integer.valueOf(contact3.getContactId()) : null;
                Contact contact4 = contactWrapper.getContact();
                if (wd4.a((Object) valueOf, (Object) contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                    arrayList.add(contactWrapper);
                    z2 = true;
                }
            }
            if (!z2) {
                arrayList2.add(contactWrapper);
            }
        }
        this.i.a(this.k, new tx2.b(arrayList, arrayList2), new b(this));
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(m, "onListContactWrapperChanged");
        List<T> c = wb4.c(this.g, this.f);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (T next : c) {
            Contact contact = ((ContactWrapper) next).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            tb4.a(arrayList, (List) value.getValue());
        }
        if (arrayList.isEmpty()) {
            this.h.K(false);
        } else {
            this.h.K(true);
        }
    }

    @DexIgnore
    public final List<ContactWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public final List<ContactWrapper> k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public rc<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new qc(PortfolioApp.W.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar, Cursor cursor) {
        wd4.b(rcVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.h.a(cursor);
    }

    @DexIgnore
    public void a(rc<Cursor> rcVar) {
        wd4.b(rcVar, "loader");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onLoaderReset");
        this.h.H();
    }
}
