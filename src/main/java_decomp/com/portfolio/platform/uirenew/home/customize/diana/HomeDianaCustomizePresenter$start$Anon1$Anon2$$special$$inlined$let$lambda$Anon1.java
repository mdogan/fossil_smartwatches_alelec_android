package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g13;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1.Anon2 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.a.this$Anon0.A.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends g13>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            pl4 pl4;
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                pl4 l = this.this$Anon0.this$Anon0.a.this$Anon0.q;
                this.L$Anon0 = lh4;
                this.L$Anon1 = l;
                this.label = 1;
                if (l.a((Object) null, this) == a) {
                    return a;
                }
                pl4 = l;
            } else if (i == 1) {
                pl4 = (pl4) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                CopyOnWriteArrayList<DianaPreset> m = this.this$Anon0.this$Anon0.a.this$Anon0.j;
                ArrayList arrayList = new ArrayList(pb4.a(m, 10));
                for (DianaPreset dianaPreset : m) {
                    HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
                    wd4.a((Object) dianaPreset, "it");
                    arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
                }
                return arrayList;
            } finally {
                pl4.a((Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1(List list, kc4 kc4, HomeDianaCustomizePresenter$start$Anon1.Anon2 anon2) {
        super(2, kc4);
        this.$it = list;
        this.this$Anon0 = anon2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 = new HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0);
        homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 c = this.this$Anon0.a.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(c, anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            MFUser mFUser = (MFUser) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            this.this$Anon0.a.this$Anon0.t.c((List<g13>) (List) obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser2 = (MFUser) obj;
        if ((!wd4.a((Object) this.this$Anon0.a.this$Anon0.n, (Object) this.$it)) || (!wd4.a((Object) this.this$Anon0.a.this$Anon0.p, (Object) mFUser2))) {
            this.this$Anon0.a.this$Anon0.p = mFUser2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "on real data change " + this.$it);
            this.this$Anon0.a.this$Anon0.n.clear();
            this.this$Anon0.a.this$Anon0.n.addAll(this.$it);
            if (true ^ this.this$Anon0.a.this$Anon0.j.isEmpty()) {
                gh4 c2 = this.this$Anon0.a.this$Anon0.b();
                Anon2 anon2 = new Anon2(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = mFUser2;
                this.label = 2;
                obj = kg4.a(c2, anon2, this);
                if (obj == a) {
                    return a;
                }
                this.this$Anon0.a.this$Anon0.t.c((List<g13>) (List) obj);
            }
        }
        return cb4.a;
    }
}
