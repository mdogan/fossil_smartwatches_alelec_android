package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.m33;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.n33;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchRingPhoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public SearchRingPhonePresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wd4.b(fragment, "context");
            wd4.b(str, "selectedRingtone");
            Intent intent = new Intent(fragment.getContext(), SearchRingPhoneActivity.class);
            intent.putExtra("KEY_SELECTED_RINGPHONE", str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 104);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        m33 m33 = (m33) getSupportFragmentManager().a((int) R.id.content);
        if (m33 == null) {
            m33 = m33.o.b();
            a((Fragment) m33, m33.o.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (m33 != null) {
            g.a(new n33(m33)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                SearchRingPhonePresenter searchRingPhonePresenter = this.B;
                if (searchRingPhonePresenter != null) {
                    String stringExtra = intent.getStringExtra("KEY_SELECTED_RINGPHONE");
                    wd4.a((Object) stringExtra, "it.getStringExtra(KEY_SELECTED_RINGPHONE)");
                    searchRingPhonePresenter.a(stringExtra);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                Ringtone ringtone = (Ringtone) bundle.getParcelable("KEY_SELECTED_RINGPHONE");
                if (ringtone != null) {
                    SearchRingPhonePresenter searchRingPhonePresenter2 = this.B;
                    if (searchRingPhonePresenter2 != null) {
                        searchRingPhonePresenter2.b(ringtone);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        SearchRingPhonePresenter searchRingPhonePresenter = this.B;
        if (searchRingPhonePresenter != null) {
            Ringtone h = searchRingPhonePresenter.h();
            if (bundle != null) {
                bundle.putParcelable("KEY_SELECTED_RINGPHONE", h);
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }
}
