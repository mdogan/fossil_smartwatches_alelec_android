package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.va3;
import com.fossil.blesdk.obfuscated.wa3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1", f = "CaloriesOverviewWeekPresenter.kt", l = {51}, m = "invokeSuspend")
public final class CaloriesOverviewWeekPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements dc<ps3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1$Anon2$Anon1", f = "CaloriesOverviewWeekPresenter.kt", l = {63}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1")
            @sc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1", f = "CaloriesOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0151Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public lh4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0151Anon1(Anon1 anon1, kc4 kc4) {
                    super(2, kc4);
                    this.this$Anon0 = anon1;
                }

                @DexIgnore
                public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                    wd4.b(kc4, "completion");
                    C0151Anon1 anon1 = new C0151Anon1(this.this$Anon0, kc4);
                    anon1.p$ = (lh4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0151Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    oc4.a();
                    if (this.label == 0) {
                        za4.a(obj);
                        CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
                        return caloriesOverviewWeekPresenter.a(CaloriesOverviewWeekPresenter.e(caloriesOverviewWeekPresenter), (List<ActivitySummary>) this.this$Anon0.$data);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, List list, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon2;
                this.$data = list;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter;
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter2 = this.this$Anon0.a.this$Anon0;
                    gh4 a2 = caloriesOverviewWeekPresenter2.b();
                    C0151Anon1 anon1 = new C0151Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = caloriesOverviewWeekPresenter2;
                    this.label = 1;
                    obj = kg4.a(a2, anon1, this);
                    if (obj == a) {
                        return a;
                    }
                    caloriesOverviewWeekPresenter = caloriesOverviewWeekPresenter2;
                } else if (i == 1) {
                    caloriesOverviewWeekPresenter = (CaloriesOverviewWeekPresenter) this.L$Anon1;
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                caloriesOverviewWeekPresenter.h = (BarChart.c) obj;
                va3 g = this.this$Anon0.a.this$Anon0.i;
                BarChart.c c = this.this$Anon0.a.this$Anon0.h;
                if (c == null) {
                    c = new BarChart.c(0, 0, (ArrayList) null, 7, (rd4) null);
                }
                g.a(c);
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon2(CaloriesOverviewWeekPresenter$start$Anon1 caloriesOverviewWeekPresenter$start$Anon1) {
            this.a = caloriesOverviewWeekPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(ps3<? extends List<ActivitySummary>> ps3) {
            Status a2 = ps3.a();
            List list = (List) ps3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("CaloriesOverviewWeekPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                ri4 unused = mg4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesOverviewWeekPresenter$start$Anon1(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = caloriesOverviewWeekPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CaloriesOverviewWeekPresenter$start$Anon1 caloriesOverviewWeekPresenter$start$Anon1 = new CaloriesOverviewWeekPresenter$start$Anon1(this.this$Anon0, kc4);
        caloriesOverviewWeekPresenter$start$Anon1.p$ = (lh4) obj;
        return caloriesOverviewWeekPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesOverviewWeekPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00e5  */
    public final Object invokeSuspend(Object obj) {
        va3 g;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            this.this$Anon0.h();
            if (this.this$Anon0.f == null || !sk2.s(CaloriesOverviewWeekPresenter.e(this.this$Anon0)).booleanValue()) {
                this.this$Anon0.f = new Date();
                gh4 a2 = this.this$Anon0.b();
                CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 caloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 = new CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, caloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            LiveData b = this.this$Anon0.g;
            g = this.this$Anon0.i;
            if (g == null) {
                b.a((wa3) g, new Anon2(this));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("CaloriesOverviewWeekPresenter", "start - mDate=" + CaloriesOverviewWeekPresenter.e(this.this$Anon0));
                return cb4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekFragment");
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("CaloriesOverviewWeekPresenter", "start - startDate=" + ((Date) pair.getFirst()) + ", endDate=" + ((Date) pair.getSecond()));
        CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter = this.this$Anon0;
        caloriesOverviewWeekPresenter.g = caloriesOverviewWeekPresenter.k.getSummaries((Date) pair.getFirst(), (Date) pair.getSecond(), false);
        LiveData b2 = this.this$Anon0.g;
        g = this.this$Anon0.i;
        if (g == null) {
        }
    }
}
