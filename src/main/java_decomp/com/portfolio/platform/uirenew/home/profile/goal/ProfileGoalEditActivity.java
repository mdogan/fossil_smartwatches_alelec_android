package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xh3;
import com.fossil.blesdk.obfuscated.zh3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileGoalEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((rd4) null);
    @DexIgnore
    public xh3 B;
    @DexIgnore
    public ProfileGoalEditPresenter C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            Intent intent = new Intent(context, ProfileGoalEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        xh3 xh3 = this.B;
        if (xh3 != null) {
            xh3.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
        xh3 xh3 = this.B;
        if (xh3 != null) {
            xh3.S0();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        this.B = (xh3) getSupportFragmentManager().a((int) R.id.content);
        if (this.B == null) {
            this.B = xh3.p.b();
            xh3 xh3 = this.B;
            if (xh3 != null) {
                a((Fragment) xh3, xh3.p.a(), (int) R.id.content);
            } else {
                wd4.a();
                throw null;
            }
        }
        m42 g = PortfolioApp.W.c().g();
        xh3 xh32 = this.B;
        if (xh32 != null) {
            g.a(new zh3(xh32)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditContract.View");
    }
}
