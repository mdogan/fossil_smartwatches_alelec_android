package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.a33;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.p23;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDefaultAddressActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle) {
            wd4.b(fragment, "fragment");
            wd4.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 111);
        }

        @DexIgnore
        public final void b(Fragment fragment, Bundle bundle) {
            wd4.b(fragment, "fragment");
            wd4.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 112);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        p23 p23 = (p23) getSupportFragmentManager().a((int) R.id.content);
        if (p23 == null) {
            p23 = p23.q.a();
            a((Fragment) p23, p23.q.b(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (p23 != null) {
            g.a(new a33(p23)).a(this);
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS");
            if (bundleExtra != null) {
                CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter = this.B;
                if (commuteTimeSettingsDefaultAddressPresenter != null) {
                    String string = bundleExtra.getString("AddressType");
                    if (string == null) {
                        string = "Home";
                    }
                    String string2 = bundleExtra.getString("KEY_DEFAULT_PLACE");
                    if (string2 == null) {
                        string2 = "";
                    }
                    commuteTimeSettingsDefaultAddressPresenter.a(string, string2);
                    return;
                }
                wd4.d("mCommuteTimeSettingsDefaultAddressPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressContract.View");
    }
}
