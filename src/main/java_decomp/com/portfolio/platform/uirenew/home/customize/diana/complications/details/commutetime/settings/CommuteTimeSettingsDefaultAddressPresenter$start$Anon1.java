package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.z23;
import com.fossil.blesdk.obfuscated.za4;
import com.google.android.libraries.places.api.Places;
import com.portfolio.platform.PortfolioApp;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$Anon1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
public final class CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(this.this$Anon0, kc4);
        commuteTimeSettingsDefaultAddressPresenter$start$Anon1.p$ = (lh4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDefaultAddressPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.c();
            CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        wd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$Anon0.k.clear();
        this.this$Anon0.k.addAll((List) obj);
        this.this$Anon0.f = Places.createClient(PortfolioApp.W.c());
        this.this$Anon0.j().a(this.this$Anon0.f);
        if (wd4.a((Object) this.this$Anon0.h, (Object) "Home")) {
            z23 j = this.this$Anon0.j();
            String d = this.this$Anon0.i;
            wd4.a((Object) d, "mHomeTitle");
            j.setTitle(d);
        } else {
            z23 j2 = this.this$Anon0.j();
            String g = this.this$Anon0.j;
            wd4.a((Object) g, "mWorkTitle");
            j2.setTitle(g);
        }
        this.this$Anon0.j().v(this.this$Anon0.g);
        this.this$Anon0.j().j(this.this$Anon0.k);
        return cb4.a;
    }
}
