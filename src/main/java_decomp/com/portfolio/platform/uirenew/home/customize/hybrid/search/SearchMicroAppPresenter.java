package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.b73;
import com.fossil.blesdk.obfuscated.c73;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchMicroAppPresenter extends b73 {
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ c73 l;
    @DexIgnore
    public /* final */ MicroAppRepository m;
    @DexIgnore
    public /* final */ fn2 n;
    @DexIgnore
    public /* final */ PortfolioApp o;

    @DexIgnore
    public SearchMicroAppPresenter(c73 c73, MicroAppRepository microAppRepository, fn2 fn2, PortfolioApp portfolioApp) {
        wd4.b(c73, "mView");
        wd4.b(microAppRepository, "mMicroAppRepository");
        wd4.b(fn2, "sharedPreferencesManager");
        wd4.b(portfolioApp, "mApp");
        this.l = c73;
        this.m = microAppRepository;
        this.n = fn2;
        this.o = portfolioApp;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public final void i() {
        if (this.k.isEmpty()) {
            this.l.b(a((List<MicroApp>) wb4.d(this.j)));
        } else {
            this.l.e(a((List<MicroApp>) wb4.d(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            c73 c73 = this.l;
            String str = this.i;
            if (str != null) {
                c73.a(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchMicroAppPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void h() {
        this.i = "";
        this.l.u();
        i();
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        wd4.b(str, "watchAppTop");
        wd4.b(str2, "watchAppMiddle");
        wd4.b(str3, "watchAppBottom");
        this.f = str;
        this.h = str3;
        this.g = str2;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchMicroAppPresenter$search$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(MicroApp microApp) {
        wd4.b(microApp, "selectedMicroApp");
        List<String> q = this.n.q();
        wd4.a((Object) q, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!q.contains(microApp.getId())) {
            q.add(0, microApp.getId());
            if (q.size() > 5) {
                q = q.subList(0, 5);
            }
            this.n.c(q);
        }
        this.l.a(microApp);
    }

    @DexIgnore
    public final List<Pair<MicroApp, String>> a(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : list) {
            String id = next.getId();
            if (wd4.a((Object) id, (Object) this.f)) {
                arrayList.add(new Pair(next, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (wd4.a((Object) id, (Object) this.g)) {
                arrayList.add(new Pair(next, "middle"));
            } else if (wd4.a((Object) id, (Object) this.h)) {
                arrayList.add(new Pair(next, "bottom"));
            } else {
                arrayList.add(new Pair(next, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.f);
            bundle.putString("middle", this.g);
            bundle.putString("bottom", this.h);
        }
        return bundle;
    }
}
