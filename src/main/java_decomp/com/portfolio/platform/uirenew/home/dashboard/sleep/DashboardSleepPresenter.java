package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ad3;
import com.fossil.blesdk.obfuscated.bd3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zc3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardSleepPresenter extends zc3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> g;
    @DexIgnore
    public /* final */ ad3 h;
    @DexIgnore
    public /* final */ SleepSummariesRepository i;
    @DexIgnore
    public /* final */ SleepSessionsRepository j;
    @DexIgnore
    public /* final */ FitnessDataRepository k;
    @DexIgnore
    public /* final */ SleepDao l;
    @DexIgnore
    public /* final */ SleepDatabase m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ i42 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DashboardSleepPresenter(ad3 ad3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, UserRepository userRepository, i42 i42) {
        wd4.b(ad3, "mView");
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(sleepDao, "mSleepDao");
        wd4.b(sleepDatabase, "mSleepDatabase");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(i42, "mAppExecutors");
        this.h = ad3;
        this.i = sleepSummariesRepository;
        this.j = sleepSessionsRepository;
        this.k = fitnessDataRepository;
        this.l = sleepDao;
        this.m = sleepDatabase;
        this.n = userRepository;
        this.o = i42;
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.g;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public final ad3 k() {
        return this.h;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!sk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<SleepSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardSleepPresenter$initDataSource$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<SleepSummary> listing = this.g;
            if (listing != null) {
                LiveData<rd<SleepSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    ad3 ad3 = this.h;
                    if (ad3 != null) {
                        pagedList.a((LifecycleOwner) (bd3) ad3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
