package com.portfolio.platform.uirenew.home.profile;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$Anon4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $deviceModel;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter$start$Anon4.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(Device device, kc4 kc4, HomeProfilePresenter$start$Anon4.Anon1 anon1) {
        super(2, kc4);
        this.$deviceModel = device;
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(this.$deviceModel, kc4, this.this$Anon0);
        homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (lh4) obj;
        return homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return this.this$Anon0.this$Anon0.a.w.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
