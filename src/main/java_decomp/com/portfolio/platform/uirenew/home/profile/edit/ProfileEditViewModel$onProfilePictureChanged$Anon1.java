package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.bl2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$Anon1", f = "ProfileEditViewModel.kt", l = {184}, m = "invokeSuspend")
public final class ProfileEditViewModel$onProfilePictureChanged$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Intent $data;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileEditViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileEditViewModel$onProfilePictureChanged$Anon1(ProfileEditViewModel profileEditViewModel, Intent intent, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = profileEditViewModel;
        this.$data = intent;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ProfileEditViewModel$onProfilePictureChanged$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1(this.this$Anon0, this.$data, kc4);
        profileEditViewModel$onProfilePictureChanged$Anon1.p$ = (lh4) obj;
        return profileEditViewModel$onProfilePictureChanged$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileEditViewModel$onProfilePictureChanged$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            Uri a2 = bl2.a(this.$data, PortfolioApp.W.c());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = ProfileEditViewModel.l.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult imageUri=");
            if (a2 != null) {
                sb.append(a2);
                local.d(a3, sb.toString());
                if (!PortfolioApp.W.c().a(this.$data, a2)) {
                    return cb4.a;
                }
                ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, a2, (Boolean) null, (Pair) null, false, (MFUser) null, false, 125, (Object) null);
                gh4 b = zh4.b();
                ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(a2, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = a2;
                this.label = 1;
                obj2 = kg4.a(b, profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1, this);
                if (obj2 == a) {
                    return a;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            Uri uri = (Uri) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                obj2 = obj;
            } catch (Exception e) {
                e.printStackTrace();
                ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, true, (MFUser) null, false, 111, (Object) null);
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Bitmap bitmap = (Bitmap) obj2;
        MFUser a4 = this.this$Anon0.d;
        if (a4 != null) {
            if (bitmap != null) {
                a4.setProfilePicture(wr3.a(bitmap));
            } else {
                wd4.a();
                throw null;
            }
        }
        this.this$Anon0.e = true;
        ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, (Uri) null, pc4.a(this.this$Anon0.e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
        return cb4.a;
    }
}
