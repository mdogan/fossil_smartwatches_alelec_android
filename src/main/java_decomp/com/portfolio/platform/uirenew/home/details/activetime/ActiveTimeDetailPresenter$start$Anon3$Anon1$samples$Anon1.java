package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$Anon3;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<ActivitySample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter$start$Anon3.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1(ActiveTimeDetailPresenter$start$Anon3.Anon1 anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1 activeTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1 = new ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1(this.this$Anon0, kc4);
        activeTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1.p$ = (lh4) obj;
        return activeTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$start$Anon3$Anon1$samples$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$Anon0.this$Anon0.a;
            return activeTimeDetailPresenter.a(activeTimeDetailPresenter.g, (List<ActivitySample>) this.this$Anon0.this$Anon0.a.l);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
