package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pj2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.uj2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetDianaPresetToWatchUseCase extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public DianaPreset e;
    @DexIgnore
    public /* final */ SetPresetBroadcastReceiver f; // = new SetPresetBroadcastReceiver();
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository h;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository i;
    @DexIgnore
    public /* final */ WatchFaceRepository j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetPresetBroadcastReceiver implements BleCommandResultManager.b {
        @DexIgnore
        public SetPresetBroadcastReceiver() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetDianaPresetToWatchUseCase.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetDianaPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_PRESET_APPS_DATA) {
                    SetDianaPresetToWatchUseCase.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - success");
                        SetDianaPresetToWatchUseCase.this.a(new d());
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - failed isSettingChangedOnly");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    DianaPreset e = SetDianaPresetToWatchUseCase.this.e();
                    if (e != null) {
                        if (mg4.b(SetDianaPresetToWatchUseCase.this.b(), (CoroutineContext) null, (CoroutineStart) null, new SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$Anon1(e, (kc4) null, this, intExtra2, integerArrayListExtra), 3, (Object) null) != null) {
                            return;
                        }
                    }
                    SetDianaPresetToWatchUseCase.this.a(new b(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            wd4.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ DianaPreset a;

        @DexIgnore
        public c(DianaPreset dianaPreset) {
            wd4.b(dianaPreset, "mPreset");
            this.a = dianaPreset;
        }

        @DexIgnore
        public final DianaPreset a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public SetDianaPresetToWatchUseCase(DianaPresetRepository dianaPresetRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository) {
        wd4.b(dianaPresetRepository, "mDianaPresetRepository");
        wd4.b(complicationLastSettingRepository, "mLastSettingRepository");
        wd4.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        wd4.b(watchFaceRepository, "watchFaceRepository");
        this.g = dianaPresetRepository;
        this.h = complicationLastSettingRepository;
        this.i = watchAppLastSettingRepository;
        this.j = watchFaceRepository;
    }

    @DexIgnore
    public String c() {
        return "SetDianaPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final DianaPreset e() {
        return this.e;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00e2, code lost:
        if (com.fossil.blesdk.obfuscated.pc4.a(com.portfolio.platform.PortfolioApp.W.c().a(r2, r1, r9, r3)) == null) goto L_0x00e6;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(c cVar, kc4<Object> kc4) {
        SetDianaPresetToWatchUseCase$run$Anon1 setDianaPresetToWatchUseCase$run$Anon1;
        int i2;
        SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase;
        BackgroundConfig backgroundConfig;
        String str;
        WatchAppMappingSettings watchAppMappingSettings;
        ComplicationAppMappingSettings complicationAppMappingSettings;
        if (kc4 instanceof SetDianaPresetToWatchUseCase$run$Anon1) {
            setDianaPresetToWatchUseCase$run$Anon1 = (SetDianaPresetToWatchUseCase$run$Anon1) kc4;
            int i3 = setDianaPresetToWatchUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                setDianaPresetToWatchUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = setDianaPresetToWatchUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i2 = setDianaPresetToWatchUseCase$run$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "executeUseCase");
                    if (cVar != null) {
                        this.d = true;
                        DianaPreset a3 = cVar.a();
                        this.e = this.g.getActivePresetBySerial(PortfolioApp.W.c().e());
                        String e2 = PortfolioApp.W.c().e();
                        WatchAppMappingSettings b2 = tj2.b(a3.getWatchapps(), new Gson());
                        ComplicationAppMappingSettings a4 = tj2.a(a3.getComplications(), new Gson());
                        WatchFace watchFaceWithId = this.j.getWatchFaceWithId(a3.getWatchFaceId());
                        BackgroundConfig a5 = watchFaceWithId != null ? uj2.a(watchFaceWithId, (List<DianaPresetComplicationSetting>) a3.getComplications()) : null;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon0 = this;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon1 = cVar;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon2 = cVar;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon3 = a3;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon4 = e2;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon5 = b2;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon6 = a4;
                        setDianaPresetToWatchUseCase$run$Anon1.L$Anon7 = a5;
                        setDianaPresetToWatchUseCase$run$Anon1.label = 1;
                        if (a(a3, (kc4<? super cb4>) setDianaPresetToWatchUseCase$run$Anon1) == a2) {
                            return a2;
                        }
                        setDianaPresetToWatchUseCase = this;
                        str = e2;
                        watchAppMappingSettings = b2;
                        complicationAppMappingSettings = a4;
                        backgroundConfig = a5;
                    } else {
                        setDianaPresetToWatchUseCase = this;
                        setDianaPresetToWatchUseCase.a(new b(-1, new ArrayList()));
                        return new Object();
                    }
                } else if (i2 == 1) {
                    backgroundConfig = (BackgroundConfig) setDianaPresetToWatchUseCase$run$Anon1.L$Anon7;
                    complicationAppMappingSettings = (ComplicationAppMappingSettings) setDianaPresetToWatchUseCase$run$Anon1.L$Anon6;
                    watchAppMappingSettings = (WatchAppMappingSettings) setDianaPresetToWatchUseCase$run$Anon1.L$Anon5;
                    str = (String) setDianaPresetToWatchUseCase$run$Anon1.L$Anon4;
                    DianaPreset dianaPreset = (DianaPreset) setDianaPresetToWatchUseCase$run$Anon1.L$Anon3;
                    c cVar2 = (c) setDianaPresetToWatchUseCase$run$Anon1.L$Anon2;
                    c cVar3 = (c) setDianaPresetToWatchUseCase$run$Anon1.L$Anon1;
                    setDianaPresetToWatchUseCase = (SetDianaPresetToWatchUseCase) setDianaPresetToWatchUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }
        setDianaPresetToWatchUseCase$run$Anon1 = new SetDianaPresetToWatchUseCase$run$Anon1(this, kc4);
        Object obj2 = setDianaPresetToWatchUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i2 = setDianaPresetToWatchUseCase$run$Anon1.label;
        if (i2 != 0) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(DianaPreset dianaPreset, kc4<? super cb4> kc4) {
        SetDianaPresetToWatchUseCase$setPresetToDb$Anon1 setDianaPresetToWatchUseCase$setPresetToDb$Anon1;
        int i2;
        SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase;
        Iterator<DianaPresetComplicationSetting> it;
        Iterator<DianaPresetWatchAppSetting> it2;
        if (kc4 instanceof SetDianaPresetToWatchUseCase$setPresetToDb$Anon1) {
            setDianaPresetToWatchUseCase$setPresetToDb$Anon1 = (SetDianaPresetToWatchUseCase$setPresetToDb$Anon1) kc4;
            int i3 = setDianaPresetToWatchUseCase$setPresetToDb$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                setDianaPresetToWatchUseCase$setPresetToDb$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = setDianaPresetToWatchUseCase$setPresetToDb$Anon1.result;
                Object a2 = oc4.a();
                i2 = setDianaPresetToWatchUseCase$setPresetToDb$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("SetDianaPresetToWatchUseCase", "setPresetToDb " + dianaPreset);
                    dianaPreset.setActive(true);
                    DianaPresetRepository dianaPresetRepository = this.g;
                    setDianaPresetToWatchUseCase$setPresetToDb$Anon1.L$Anon0 = this;
                    setDianaPresetToWatchUseCase$setPresetToDb$Anon1.L$Anon1 = dianaPreset;
                    setDianaPresetToWatchUseCase$setPresetToDb$Anon1.label = 1;
                    if (dianaPresetRepository.upsertPreset(dianaPreset, setDianaPresetToWatchUseCase$setPresetToDb$Anon1) == a2) {
                        return a2;
                    }
                    setDianaPresetToWatchUseCase = this;
                } else if (i2 == 1) {
                    dianaPreset = (DianaPreset) setDianaPresetToWatchUseCase$setPresetToDb$Anon1.L$Anon1;
                    setDianaPresetToWatchUseCase = (SetDianaPresetToWatchUseCase) setDianaPresetToWatchUseCase$setPresetToDb$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Calendar instance = Calendar.getInstance();
                wd4.a((Object) instance, "Calendar.getInstance()");
                String t = sk2.t(instance.getTime());
                it = dianaPreset.getComplications().iterator();
                while (it.hasNext()) {
                    DianaPresetComplicationSetting next = it.next();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetDianaPresetToWatchUseCase", "setPresetToWatch success save last user setting " + next.getSettings());
                    if (!pj2.a(next.getSettings())) {
                        ComplicationLastSettingRepository complicationLastSettingRepository = setDianaPresetToWatchUseCase.h;
                        String id = next.getId();
                        wd4.a((Object) t, "updatedAt");
                        String settings = next.getSettings();
                        if (settings == null) {
                            settings = "";
                        }
                        complicationLastSettingRepository.upsertComplicationLastSetting(new ComplicationLastSetting(id, t, settings));
                    }
                }
                it2 = dianaPreset.getWatchapps().iterator();
                while (it2.hasNext()) {
                    DianaPresetWatchAppSetting next2 = it2.next();
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("SetDianaPresetToWatchUseCase", "setPresetToWatch success save last user setting " + next2.getSettings());
                    if (!pj2.a(next2.getSettings())) {
                        WatchAppLastSettingRepository watchAppLastSettingRepository = setDianaPresetToWatchUseCase.i;
                        String id2 = next2.getId();
                        wd4.a((Object) t, "updatedAt");
                        String settings2 = next2.getSettings();
                        if (settings2 == null) {
                            settings2 = "";
                        }
                        watchAppLastSettingRepository.upsertWatchAppLastSetting(new WatchAppLastSetting(id2, t, settings2));
                    }
                }
                return cb4.a;
            }
        }
        setDianaPresetToWatchUseCase$setPresetToDb$Anon1 = new SetDianaPresetToWatchUseCase$setPresetToDb$Anon1(this, kc4);
        Object obj2 = setDianaPresetToWatchUseCase$setPresetToDb$Anon1.result;
        Object a22 = oc4.a();
        i2 = setDianaPresetToWatchUseCase$setPresetToDb$Anon1.label;
        if (i2 != 0) {
        }
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "Calendar.getInstance()");
        String t2 = sk2.t(instance2.getTime());
        it = dianaPreset.getComplications().iterator();
        while (it.hasNext()) {
        }
        it2 = dianaPreset.getWatchapps().iterator();
        while (it2.hasNext()) {
        }
        return cb4.a;
    }
}
