package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sb4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;
import java.util.Comparator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ArrayList<SecondTimezoneSetting>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $rawDataList;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<SecondTimezoneSetting> {
        @DexIgnore
        public static /* final */ a e; // = new a();

        @DexIgnore
        /* renamed from: a */
        public final int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
            return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(ArrayList arrayList, kc4 kc4) {
        super(2, kc4);
        this.$rawDataList = arrayList;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1 = new SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1(this.$rawDataList, kc4);
        searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1.p$ = (lh4) obj;
        return searchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchSecondTimezonePresenter$start$Anon1$sortedDataList$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            sb4.a(this.$rawDataList, a.e);
            return this.$rawDataList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
