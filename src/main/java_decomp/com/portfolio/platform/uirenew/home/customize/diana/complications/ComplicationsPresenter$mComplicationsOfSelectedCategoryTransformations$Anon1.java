package com.portfolio.platform.uirenew.home.customize.diana.complications;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1$Anon1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $complicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filteredComplicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ String $selectedComplicationId;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1 complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1, DianaPreset dianaPreset, List list, String str, ArrayList arrayList, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1;
            this.$currentPreset = dianaPreset;
            this.$complicationByCategories = list;
            this.$selectedComplicationId = str;
            this.$filteredComplicationByCategories = arrayList;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$currentPreset, this.$complicationByCategories, this.$selectedComplicationId, this.$filteredComplicationByCategories, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            T t;
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                if (this.$currentPreset != null) {
                    for (Complication complication : this.$complicationByCategories) {
                        Iterator<T> it = this.$currentPreset.getComplications().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                            boolean z = true;
                            if (!wd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) complication.getComplicationId()) || !(!wd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) this.$selectedComplicationId))) {
                                z = false;
                            }
                            if (pc4.a(z).booleanValue()) {
                                break;
                            }
                        }
                        if (((DianaPresetComplicationSetting) t) == null || wd4.a((Object) complication.getComplicationId(), (Object) "empty")) {
                            this.$filteredComplicationByCategories.add(complication);
                        }
                    }
                }
                this.this$Anon0.a.i.a(this.$filteredComplicationByCategories);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1(ComplicationsPresenter complicationsPresenter) {
        this.a = complicationsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final MutableLiveData<List<Complication>> apply(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String l = ComplicationsPresenter.w;
        local.d(l, "transform from category to list complication with category=" + str);
        DianaCustomizeViewModel g = ComplicationsPresenter.g(this.a);
        wd4.a((Object) str, "category");
        List<Complication> b = g.b(str);
        ArrayList arrayList = new ArrayList();
        DianaPreset a2 = ComplicationsPresenter.g(this.a).c().a();
        Complication a3 = ComplicationsPresenter.g(this.a).f().a();
        if (a3 != null) {
            String complicationId = a3.getComplicationId();
            ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, a2, b, complicationId, arrayList, (kc4) null), 3, (Object) null);
            return this.a.i;
        }
        wd4.a();
        throw null;
    }
}
