package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
public final class SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends MicroApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchMicroAppPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1(SearchMicroAppPresenter$start$Anon1 searchMicroAppPresenter$start$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = searchMicroAppPresenter$start$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1 searchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1 = new SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1(this.this$Anon0, kc4);
        searchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1.p$ = (lh4) obj;
        return searchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            MicroAppRepository e = this.this$Anon0.this$Anon0.m;
            List<String> q = this.this$Anon0.this$Anon0.n.q();
            wd4.a((Object) q, "sharedPreferencesManager.microAppSearchedIdsRecent");
            return e.getMicroAppByIds(q, this.this$Anon0.this$Anon0.o.e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
