package com.portfolio.platform.uirenew.home.profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.bl2;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.fv2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.ng3;
import com.fossil.blesdk.obfuscated.og3;
import com.fossil.blesdk.obfuscated.or2;
import com.fossil.blesdk.obfuscated.pg3;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rl4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter extends og3 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase A;
    @DexIgnore
    public /* final */ ArrayList<b> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Device> g; // = new ArrayList<>();
    @DexIgnore
    public MFUser h;
    @DexIgnore
    public LiveData<ps3<ActivityStatistic>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<ActivitySummary>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<ps3<SleepStatistic>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ LiveData<List<Device>> l; // = this.w.getAllDeviceAsLiveData();
    @DexIgnore
    public ActivityStatistic.ActivityDailyBest m;
    @DexIgnore
    public long n;
    @DexIgnore
    public /* final */ pl4 o; // = rl4.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ Handler p; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Runnable q; // = new f(this);
    @DexIgnore
    public /* final */ e r; // = new e(this);
    @DexIgnore
    public /* final */ pg3 s;
    @DexIgnore
    public /* final */ PortfolioApp t;
    @DexIgnore
    public /* final */ or2 u;
    @DexIgnore
    public /* final */ UpdateUser v;
    @DexIgnore
    public /* final */ DeviceRepository w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ SummariesRepository y;
    @DexIgnore
    public /* final */ SleepSummariesRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeProfilePresenter.B;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(String str, boolean z, String str2, int i, boolean z2, boolean z3) {
            wd4.b(str, "serial");
            wd4.b(str2, "deviceName");
            this.a = str;
            this.b = z;
            this.c = str2;
            this.d = i;
            this.e = z2;
            this.f = z3;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final boolean d() {
            return this.e;
        }

        @DexIgnore
        public final boolean e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (wd4.a((Object) this.a, (Object) bVar.a)) {
                        if ((this.b == bVar.b) && wd4.a((Object) this.c, (Object) bVar.c)) {
                            if (this.d == bVar.d) {
                                if (this.e == bVar.e) {
                                    if (this.f == bVar.f) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i2 = (hashCode + (z ? 1 : 0)) * 31;
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i3 = (((i2 + i) * 31) + this.d) * 31;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            int i4 = (i3 + (z2 ? 1 : 0)) * 31;
            boolean z3 = this.f;
            if (z3) {
                z3 = true;
            }
            return i4 + (z3 ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "DeviceWrapper(serial=" + this.a + ", isConnected=" + this.b + ", deviceName=" + this.c + ", batteryLevel=" + this.d + ", isActive=" + this.e + ", isLatestFw=" + this.f + ")";
        }

        @DexIgnore
        public final int a() {
            return this.d;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<or2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public c(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wd4.b(aVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(or2.a aVar) {
            wd4.b(aVar, "responseValue");
            MFUser a2 = aVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomeProfilePresenter.C.a();
            local.d(a3, "loadUser " + a2);
            if (a2 != null) {
                this.a.a(a2);
                this.a.l().a(a2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<DeleteLogoutUserUseCase.d, DeleteLogoutUserUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public d(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.l().d();
            this.a.l().L();
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.l().d();
            this.a.l().a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public e(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            wd4.b(context, "context");
            wd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
            boolean z = true;
            if (cg4.b(stringExtra, this.a.t.e(), true) && (!this.a.j().isEmpty())) {
                Iterator<T> it = this.a.j().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wd4.a((Object) ((b) t).c(), (Object) stringExtra)) {
                        break;
                    }
                }
                b bVar = (b) t;
                if (bVar == null) {
                    return;
                }
                if (intExtra == ConnectionStateChange.GATT_ON.ordinal() || intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                    if (intExtra != ConnectionStateChange.GATT_ON.ordinal()) {
                        z = false;
                    }
                    bVar.a(z);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = HomeProfilePresenter.C.a();
                    local2.d(a3, "active device status change connected=" + bVar.e());
                    this.a.l().b(this.a.j());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter e;

        @DexIgnore
        public f(HomeProfilePresenter homeProfilePresenter) {
            this.e = homeProfilePresenter;
        }

        @DexIgnore
        public final void run() {
            if (this.e.l().isActive() && (!this.e.j().isEmpty())) {
                this.e.l().I();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements dc<ps3<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public g(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public final void a(ps3<ActivitySummary> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mActivitySummaryLiveData -- resource=" + ps3);
            ActivitySummary activitySummary = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    activitySummary = ps3.d();
                }
                this.a.n = activitySummary != null ? (long) activitySummary.getSteps() : 0;
                HomeProfilePresenter homeProfilePresenter = this.a;
                homeProfilePresenter.a(homeProfilePresenter.m, this.a.n);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements dc<ps3<? extends ActivityStatistic>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public h(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public final void a(ps3<ActivityStatistic> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mActivityStatisticLiveData -- resource=" + ps3);
            ActivityStatistic.ActivityDailyBest activityDailyBest = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                ActivityStatistic d = ps3 != null ? ps3.d() : null;
                this.a.l().a(d);
                HomeProfilePresenter homeProfilePresenter = this.a;
                if (d != null) {
                    activityDailyBest = d.getStepsBestDay();
                }
                homeProfilePresenter.m = activityDailyBest;
                HomeProfilePresenter homeProfilePresenter2 = this.a;
                homeProfilePresenter2.a(homeProfilePresenter2.m, this.a.n);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements dc<ps3<? extends SleepStatistic>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public i(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public final void a(ps3<SleepStatistic> ps3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.C.a();
            local.d(a2, "start - mSleepStatisticLiveData -- resource=" + ps3);
            SleepStatistic sleepStatistic = null;
            if ((ps3 != null ? ps3.f() : null) != Status.DATABASE_LOADING) {
                if (ps3 != null) {
                    SleepStatistic d = ps3.d();
                }
                pg3 l = this.a.l();
                if (ps3 != null) {
                    sleepStatistic = ps3.d();
                }
                l.a(sleepStatistic);
            }
        }
    }

    /*
    static {
        String simpleName = HomeProfilePresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "HomeProfilePresenter::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public HomeProfilePresenter(pg3 pg3, PortfolioApp portfolioApp, or2 or2, UpdateUser updateUser, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        wd4.b(pg3, "mView");
        wd4.b(portfolioApp, "mApp");
        wd4.b(or2, "mGetUser");
        wd4.b(updateUser, "mUpdateUser");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(summariesRepository, "mSummariesRepository");
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.s = pg3;
        this.t = portfolioApp;
        this.u = or2;
        this.v = updateUser;
        this.w = deviceRepository;
        this.x = userRepository;
        this.y = summariesRepository;
        this.z = sleepSummariesRepository;
        this.A = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final pg3 l() {
        return this.s;
    }

    @DexIgnore
    public final void m() {
        this.p.removeCallbacksAndMessages((Object) null);
        this.p.postDelayed(this.q, 60000);
    }

    @DexIgnore
    public final void n() {
        this.u.a(null, new c(this));
    }

    @DexIgnore
    public void o() {
        this.s.a(this);
    }

    @DexIgnore
    public final void b(MFUser mFUser) {
        if (mFUser != null) {
            this.s.e();
            this.v.a(new UpdateUser.b(mFUser), new HomeProfilePresenter$updateUser$Anon1(this));
        }
    }

    @DexIgnore
    public void f() {
        PortfolioApp portfolioApp = this.t;
        e eVar = this.r;
        portfolioApp.registerReceiver(eVar, new IntentFilter(this.t.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        n();
        this.j = this.y.getSummary(new Date());
        this.i = this.y.getActivityStatistic(true);
        this.k = this.z.getSleepStatistic(true);
        LiveData<ps3<ActivitySummary>> liveData = this.j;
        pg3 pg3 = this.s;
        if (pg3 != null) {
            liveData.a((fv2) pg3, new g(this));
            this.i.a((LifecycleOwner) this.s, new h(this));
            this.k.a((LifecycleOwner) this.s, new i(this));
            this.l.a((LifecycleOwner) this.s, new HomeProfilePresenter$start$Anon4(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
    }

    @DexIgnore
    public void g() {
        LiveData<ps3<ActivitySummary>> liveData = this.j;
        pg3 pg3 = this.s;
        if (pg3 != null) {
            liveData.a((LifecycleOwner) (fv2) pg3);
            this.l.a((LifecycleOwner) this.s);
            this.i.a((LifecycleOwner) this.s);
            this.p.removeCallbacksAndMessages((Object) null);
            try {
                this.t.unregisterReceiver(this.r);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = B;
                local.d(str, "stop with " + e2);
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        }
    }

    @DexIgnore
    public void h() {
        this.s.e();
        DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.A;
        pg3 pg3 = this.s;
        if (pg3 != null) {
            FragmentActivity activity = ((fv2) pg3).getActivity();
            if (activity != null) {
                deleteLogoutUserUseCase.a(new DeleteLogoutUserUseCase.b(1, new WeakReference(activity)), new d(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
    }

    @DexIgnore
    public void i() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeProfilePresenter$openCameraIntent$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ArrayList<b> j() {
        return this.f;
    }

    @DexIgnore
    public final MFUser k() {
        return this.h;
    }

    @DexIgnore
    public void b(Intent intent) {
        wd4.b(intent, "intent");
        if (this.s.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && cg4.b(stringExtra, PortfolioApp.W.c().e(), true)) {
                this.s.I();
                m();
            }
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.h = mFUser;
    }

    @DexIgnore
    public void a(Intent intent) {
        Uri a2 = bl2.a(intent, PortfolioApp.W.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .onActivityResult imageUri=");
        if (a2 != null) {
            sb.append(a2);
            local.d(str, sb.toString());
            if (PortfolioApp.W.c().a(intent, a2)) {
                ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeProfilePresenter$onProfilePictureChanged$Anon1(this, a2, (kc4) null), 3, (Object) null);
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(ActivityStatistic.ActivityDailyBest activityDailyBest, long j2) {
        if (j2 > (activityDailyBest != null ? (long) activityDailyBest.getValue() : 0)) {
            this.s.a(new ng3(new Date(), j2));
        }
    }
}
