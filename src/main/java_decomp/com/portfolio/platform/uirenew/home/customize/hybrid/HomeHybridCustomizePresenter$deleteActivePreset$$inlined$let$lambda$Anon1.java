package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.a63;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 implements CoroutineUseCase.e<SetHybridPresetToWatchUseCase.d, SetHybridPresetToWatchUseCase.b> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset a;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset b;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0143Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0143Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0143Anon1 anon1 = new C0143Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0143Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    HybridPresetRepository e = this.this$Anon0.this$Anon0.c.q;
                    HybridPreset hybridPreset = this.this$Anon0.this$Anon0.a;
                    if (hybridPreset != null) {
                        String id = hybridPreset.getId();
                        this.L$Anon0 = lh4;
                        this.label = 1;
                        if (e.deletePresetById(id, this) == a) {
                            return a;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                gh4 b = this.this$Anon0.c.c();
                C0143Anon1 anon1 = new C0143Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                if (kg4.a(b, anon1, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$Anon0.c.o.m();
            this.this$Anon0.c.o.c(this.this$Anon0.c.j() - 1);
            this.this$Anon0.c.s.q(true);
            HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 = this.this$Anon0;
            boolean unused = homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1.c.b(homeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1.b);
            return cb4.a;
        }
    }

    @DexIgnore
    public HomeHybridCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(HybridPreset hybridPreset, HybridPreset hybridPreset2, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
        this.a = hybridPreset;
        this.b = hybridPreset2;
        this.c = homeHybridCustomizePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
        wd4.b(dVar, "responseValue");
        ri4 unused = mg4.b(this.c.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(SetHybridPresetToWatchUseCase.b bVar) {
        wd4.b(bVar, "errorValue");
        this.c.o.m();
        int b2 = bVar.b();
        if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            wd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            a63 l = this.c.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        this.c.o.j();
    }
}
