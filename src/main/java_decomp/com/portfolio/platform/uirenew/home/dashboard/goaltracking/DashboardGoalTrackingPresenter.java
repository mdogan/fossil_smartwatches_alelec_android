package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ab3;
import com.fossil.blesdk.obfuscated.bb3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardGoalTrackingPresenter extends za3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> g;
    @DexIgnore
    public /* final */ ab3 h;
    @DexIgnore
    public /* final */ GoalTrackingRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ GoalTrackingDao k;
    @DexIgnore
    public /* final */ GoalTrackingDatabase l;
    @DexIgnore
    public /* final */ i42 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public DashboardGoalTrackingPresenter(ab3 ab3, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, i42 i42) {
        wd4.b(ab3, "mView");
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(goalTrackingDao, "mGoalTrackingDao");
        wd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wd4.b(i42, "mAppExecutors");
        this.h = ab3;
        this.i = goalTrackingRepository;
        this.j = userRepository;
        this.k = goalTrackingDao;
        this.l = goalTrackingDatabase;
        this.m = i42;
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardGoalTrackingPresenter$initDataSource$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<GoalTrackingSummary> listing = this.g;
            if (listing != null) {
                LiveData<rd<GoalTrackingSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    ab3 ab3 = this.h;
                    if (ab3 != null) {
                        pagedList.a((LifecycleOwner) (bb3) ab3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.g;
        if (listing != null) {
            id4<cb4> retry = listing.getRetry();
            if (retry != null) {
                cb4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public final ab3 k() {
        return this.h;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!sk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<GoalTrackingSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        wd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
