package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.ql2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.Triple;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1", f = "WatchAppsPresenter.kt", l = {284}, m = "invokeSuspend")
public final class WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1$Anon1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Parcelable>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return WatchAppsPresenter.f(this.this$Anon0.this$Anon0).h(this.this$Anon0.$id);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(WatchAppsPresenter watchAppsPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = watchAppsPresenter;
        this.$id = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1 = new WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1(this.this$Anon0, this.$id, kc4);
        watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1.p$ = (lh4) obj;
        return watchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppsPresenter$checkSettingOfSelectedWatchApp$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        boolean z;
        Object a = oc4.a();
        int i = this.label;
        Parcelable parcelable = null;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            z = ql2.d.g(this.$id);
            if (z) {
                gh4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = null;
                this.Z$Anon0 = z;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$Anon0.k.a(new Triple(this.$id, pc4.a(z), parcelable));
            return cb4.a;
        } else if (i == 1) {
            boolean z2 = this.Z$Anon0;
            Parcelable parcelable2 = (Parcelable) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            z = z2;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        parcelable = (Parcelable) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
        this.this$Anon0.k.a(new Triple(this.$id, pc4.a(z), parcelable));
        return cb4.a;
    }
}
