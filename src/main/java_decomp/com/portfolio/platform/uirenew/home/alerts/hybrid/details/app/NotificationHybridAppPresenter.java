package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.a03;
import com.fossil.blesdk.obfuscated.b03;
import com.fossil.blesdk.obfuscated.i03;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.sequences.SequencesKt___SequencesKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppPresenter extends a03 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public /* final */ List<AppWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ b03 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ ArrayList<String> i;
    @DexIgnore
    public /* final */ k62 j;
    @DexIgnore
    public /* final */ i03 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridAppPresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.d<i03.b, j62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridAppPresenter a;

        @DexIgnore
        public b(NotificationHybridAppPresenter notificationHybridAppPresenter) {
            this.a = notificationHybridAppPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(i03.b bVar) {
            wd4.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationHybridAppPresenter.m.a(), "mGetApps onSuccess");
            ArrayList<AppWrapper> arrayList = new ArrayList<>(bVar.a());
            for (AppWrapper appWrapper : arrayList) {
                if (appWrapper.getUri() != null) {
                    if (this.a.i.contains(String.valueOf(appWrapper.getUri()))) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp != null) {
                            installedApp.setSelected(true);
                        }
                        appWrapper.setCurrentHandGroup(this.a.h);
                    } else if (appWrapper.getCurrentHandGroup() == this.a.h) {
                        InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        if (installedApp2 != null) {
                            installedApp2.setSelected(false);
                        }
                    }
                }
            }
            this.a.j().addAll(arrayList);
            this.a.g.a(this.a.j(), this.a.h);
            this.a.g.d();
        }

        @DexIgnore
        public void a(j62.a aVar) {
            wd4.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationHybridAppPresenter.m.a(), "mGetApps onError");
            this.a.g.d();
        }
    }

    /*
    static {
        String simpleName = NotificationHybridAppPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridAppPre\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridAppPresenter(b03 b03, int i2, ArrayList<String> arrayList, k62 k62, i03 i03) {
        wd4.b(b03, "mView");
        wd4.b(arrayList, "mListAppsSelected");
        wd4.b(k62, "mUseCaseHandler");
        wd4.b(i03, "mGetHybridApp");
        this.g = b03;
        this.h = i2;
        this.i = arrayList;
        this.j = k62;
        this.k = i03;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (this.f.isEmpty()) {
            this.g.e();
            this.j.a(this.k, null, new b(this));
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public void i() {
        this.g.a(new ArrayList(SequencesKt___SequencesKt.f(SequencesKt___SequencesKt.c(SequencesKt___SequencesKt.a(wb4.b(this.f), new NotificationHybridAppPresenter$passData$uriAppsSelected$Anon1(this)), NotificationHybridAppPresenter$passData$uriAppsSelected$Anon2.INSTANCE))));
    }

    @DexIgnore
    public final List<AppWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public void a(AppWrapper appWrapper, boolean z) {
        T t;
        wd4.b(appWrapper, "appWrapper");
        FLogger.INSTANCE.getLocal().d(l, "setAppState: appWrapper=" + appWrapper + ", selected=" + z);
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wd4.a((Object) ((AppWrapper) t).getUri(), (Object) appWrapper.getUri())) {
                break;
            }
        }
        AppWrapper appWrapper2 = (AppWrapper) t;
        if (appWrapper2 != null) {
            InstalledApp installedApp = appWrapper2.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(z);
            }
            InstalledApp installedApp2 = appWrapper2.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected == null) {
                wd4.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                int currentHandGroup = appWrapper2.getCurrentHandGroup();
                int i2 = this.h;
                if (currentHandGroup != i2) {
                    appWrapper2.setCurrentHandGroup(i2);
                }
            }
        }
    }
}
