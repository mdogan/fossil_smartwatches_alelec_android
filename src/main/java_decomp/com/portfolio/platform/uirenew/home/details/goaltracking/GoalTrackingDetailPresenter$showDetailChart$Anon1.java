package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {262, 229, 232}, m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingDetailPresenter$showDetailChart$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        goalTrackingDetailPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return goalTrackingDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cf A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00df A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101 A[Catch:{ all -> 0x0027 }] */
    public final Object invokeSuspend(Object obj) {
        pl4 pl4;
        pl4 pl42;
        Pair pair;
        ArrayList arrayList;
        GoalTrackingSummary g;
        int i;
        lh4 lh4;
        Object a;
        lh4 lh42;
        Object a2 = oc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            za4.a(obj);
            lh42 = this.p$;
            pl4 = this.this$Anon0.k;
            this.L$Anon0 = lh42;
            this.L$Anon1 = pl4;
            this.label = 1;
            if (pl4.a((Object) null, this) == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            pl4 = (pl4) this.L$Anon1;
            za4.a(obj);
            lh42 = (lh4) this.L$Anon0;
        } else if (i2 == 2) {
            pl4 = (pl4) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                Pair pair2 = (Pair) obj;
                ArrayList arrayList2 = (ArrayList) pair2.getFirst();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$Anon0.g + ", data=" + arrayList2);
                gh4 a3 = this.this$Anon0.b();
                GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1(arrayList2, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = pl4;
                this.L$Anon2 = pair2;
                this.L$Anon3 = arrayList2;
                this.label = 3;
                a = kg4.a(a3, goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1, this);
                if (a != a2) {
                    return a2;
                }
                arrayList = arrayList2;
                pl4 pl43 = pl4;
                pair = pair2;
                obj = a;
                pl42 = pl43;
                Integer num = (Integer) obj;
                g = this.this$Anon0.m;
                if (g != null) {
                }
                i = 8;
                this.this$Anon0.q.a(new BarChart.c(Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                cb4 cb4 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 3) {
            arrayList = (ArrayList) this.L$Anon3;
            pair = (Pair) this.L$Anon2;
            pl42 = (pl4) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                Integer num2 = (Integer) obj;
                g = this.this$Anon0.m;
                if (g != null) {
                    Integer a4 = pc4.a(g.getGoalTarget() / 16);
                    if (a4 != null) {
                        i = a4.intValue();
                        this.this$Anon0.q.a(new BarChart.c(Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                        cb4 cb42 = cb4.a;
                        pl42.a((Object) null);
                        return cb4.a;
                    }
                }
                i = 8;
                this.this$Anon0.q.a(new BarChart.c(Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                cb4 cb422 = cb4.a;
                pl42.a((Object) null);
                return cb4.a;
            } catch (Throwable th2) {
                th = th2;
                pl4 = pl42;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        gh4 a5 = this.this$Anon0.b();
        GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((kc4) null, this);
        this.L$Anon0 = lh42;
        this.L$Anon1 = pl4;
        this.label = 2;
        Object a6 = kg4.a(a5, goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1, this);
        if (a6 == a2) {
            return a2;
        }
        Object obj2 = a6;
        lh4 = lh42;
        obj = obj2;
        Pair pair22 = (Pair) obj;
        ArrayList arrayList22 = (ArrayList) pair22.getFirst();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$Anon0.g + ", data=" + arrayList22);
        gh4 a32 = this.this$Anon0.b();
        GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon12 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1(arrayList22, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = pl4;
        this.L$Anon2 = pair22;
        this.L$Anon3 = arrayList22;
        this.label = 3;
        a = kg4.a(a32, goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon12, this);
        if (a != a2) {
        }
        pl4.a((Object) null);
        throw th;
    }
}
