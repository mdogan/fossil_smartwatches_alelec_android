package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.u03;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1", f = "NotificationHybridContactPresenter.kt", l = {100}, m = "invokeSuspend")
public final class NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ u03.d $successResponse;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridContactPresenter$start$Anon1.Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1(NotificationHybridContactPresenter$start$Anon1.Anon2 anon2, u03.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon2;
        this.$successResponse = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 = new NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1(this.this$Anon0, this.$successResponse, kc4);
        notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1.p$ = (lh4) obj;
        return notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            sh4 a2 = mg4.a(lh4, this.this$Anon0.a.this$Anon0.b(), (CoroutineStart) null, new NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1(this, (kc4) null), 2, (Object) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (a2.a(this) == a) {
                return a;
            }
        } else if (i == 1) {
            sh4 sh4 = (sh4) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!this.this$Anon0.a.this$Anon0.i.isEmpty()) {
            for (ContactWrapper add : this.this$Anon0.a.this$Anon0.i) {
                this.this$Anon0.a.this$Anon0.j().add(add);
            }
        }
        this.this$Anon0.a.this$Anon0.g.a(this.this$Anon0.a.this$Anon0.j(), gs3.a.a(), this.this$Anon0.a.this$Anon0.h);
        this.this$Anon0.a.this$Anon0.j.a(0, new Bundle(), this.this$Anon0.a.this$Anon0);
        return cb4.a;
    }
}
