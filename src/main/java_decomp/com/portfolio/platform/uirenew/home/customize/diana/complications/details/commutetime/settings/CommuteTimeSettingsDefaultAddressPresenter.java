package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Context;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.y23;
import com.fossil.blesdk.obfuscated.z23;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDefaultAddressPresenter extends y23 {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public PlacesClient f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ String i; // = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Home);
    @DexIgnore
    public /* final */ String j; // = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Other);
    @DexIgnore
    public /* final */ ArrayList<String> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ z23 l;
    @DexIgnore
    public /* final */ fn2 m;
    @DexIgnore
    public /* final */ UserRepository n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = CommuteTimeSettingsDefaultAddressPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter(z23 z23, fn2 fn2, UserRepository userRepository) {
        wd4.b(z23, "mView");
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(userRepository, "mUserRepository");
        this.l = z23;
        this.m = fn2;
        this.n = userRepository;
    }

    @DexIgnore
    public final fn2 h() {
        return this.m;
    }

    @DexIgnore
    public final UserRepository i() {
        return this.n;
    }

    @DexIgnore
    public final z23 j() {
        return this.l;
    }

    @DexIgnore
    public void k() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        this.f = null;
    }

    @DexIgnore
    public void a(String str, String str2) {
        wd4.b(str, "addressType");
        wd4.b(str2, "defaultPlace");
        this.g = str2;
        this.h = str;
    }

    @DexIgnore
    public void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = o;
        local.d(str2, "onUserExit with address " + str);
        Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = false;
        Ref$BooleanRef ref$BooleanRef2 = new Ref$BooleanRef();
        ref$BooleanRef2.element = false;
        if (str == null || mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(str, (kc4) null, this, str, ref$BooleanRef, ref$BooleanRef2), 3, (Object) null) == null) {
            this.l.B((String) null);
            cb4 cb4 = cb4.a;
        }
    }
}
