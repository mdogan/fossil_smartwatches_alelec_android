package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$Anon1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
public final class WatchAppSearchPresenter$search$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $query;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppSearchPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$Anon1$Anon1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<WatchApp>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter$search$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppSearchPresenter$search$Anon1 watchAppSearchPresenter$search$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = watchAppSearchPresenter$search$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return wb4.d(this.this$Anon0.this$Anon0.m.queryWatchAppByName(this.this$Anon0.$query));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppSearchPresenter$search$Anon1(WatchAppSearchPresenter watchAppSearchPresenter, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = watchAppSearchPresenter;
        this.$query = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WatchAppSearchPresenter$search$Anon1 watchAppSearchPresenter$search$Anon1 = new WatchAppSearchPresenter$search$Anon1(this.this$Anon0, this.$query, kc4);
        watchAppSearchPresenter$search$Anon1.p$ = (lh4) obj;
        return watchAppSearchPresenter$search$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppSearchPresenter$search$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            list = new ArrayList();
            if (this.$query.length() > 0) {
                gh4 a2 = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = list;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            this.this$Anon0.l.b(this.this$Anon0.a((List<WatchApp>) list));
            this.this$Anon0.i = this.$query;
            return cb4.a;
        } else if (i == 1) {
            List list2 = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list = (List) obj;
        this.this$Anon0.l.b(this.this$Anon0.a((List<WatchApp>) list));
        this.this$Anon0.i = this.$query;
        return cb4.a;
    }
}
