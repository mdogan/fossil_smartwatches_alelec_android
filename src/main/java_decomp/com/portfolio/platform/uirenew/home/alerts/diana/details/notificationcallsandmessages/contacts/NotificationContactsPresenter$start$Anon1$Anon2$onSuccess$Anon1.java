package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1", f = "NotificationContactsPresenter.kt", l = {60}, m = "invokeSuspend")
public final class NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ qx2.d $responseValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsPresenter$start$Anon1.Anon2 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1$Anon1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1 notificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = notificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                for (ContactGroup contactGroup : this.this$Anon0.$responseValue.a()) {
                    List<Contact> contacts = contactGroup.getContacts();
                    wd4.a((Object) contacts, "it.contacts");
                    if (!contacts.isEmpty()) {
                        Contact contact = contactGroup.getContacts().get(0);
                        ContactWrapper contactWrapper = new ContactWrapper(contact, (String) null, 2, (rd4) null);
                        contactWrapper.setAdded(true);
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            wd4.a((Object) contact, "contact");
                            contact2.setDbRowId(contact.getDbRowId());
                        }
                        Contact contact3 = contactWrapper.getContact();
                        if (contact3 != null) {
                            wd4.a((Object) contact, "contact");
                            contact3.setUseSms(contact.isUseSms());
                        }
                        Contact contact4 = contactWrapper.getContact();
                        if (contact4 != null) {
                            wd4.a((Object) contact, "contact");
                            contact4.setUseCall(contact.isUseCall());
                        }
                        wd4.a((Object) contact, "contact");
                        List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                        wd4.a((Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                            wd4.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                            if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                contactWrapper.setHasPhoneNumber(true);
                                PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                wd4.a((Object) phoneNumber2, "contact.phoneNumbers[0]");
                                contactWrapper.setPhoneNumber(phoneNumber2.getNumber());
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a = NotificationContactsPresenter.n.a();
                                StringBuilder sb = new StringBuilder();
                                sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                wd4.a((Object) phoneNumber3, "contact.phoneNumbers[0]");
                                sb.append(phoneNumber3.getNumber());
                                local.d(a, sb.toString());
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a2 = NotificationContactsPresenter.n.a();
                        local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                        this.this$Anon0.this$Anon0.a.this$Anon0.j().add(contactWrapper);
                    }
                }
                return pc4.a(this.this$Anon0.this$Anon0.a.this$Anon0.k().addAll(wb4.d(this.this$Anon0.this$Anon0.a.this$Anon0.j())));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1(NotificationContactsPresenter$start$Anon1.Anon2 anon2, qx2.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon2;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1 notificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1 = new NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1(this.this$Anon0, this.$responseValue, kc4);
        notificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1.p$ = (lh4) obj;
        return notificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a2 = this.this$Anon0.a.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.a.this$Anon0.h.a(this.this$Anon0.a.this$Anon0.j(), gs3.a.a());
        this.this$Anon0.a.this$Anon0.l.a(0, new Bundle(), this.this$Anon0.a.this$Anon0);
        return cb4.a;
    }
}
