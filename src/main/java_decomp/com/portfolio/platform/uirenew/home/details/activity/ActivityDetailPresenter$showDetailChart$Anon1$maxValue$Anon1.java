package com.portfolio.platform.uirenew.home.details.activity;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $data;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(ArrayList arrayList, kc4 kc4) {
        super(2, kc4);
        this.$data = arrayList;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 activityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 = new ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(this.$data, kc4);
        activityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1.p$ = (lh4) obj;
        return activityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivityDetailPresenter$showDetailChart$Anon1$maxValue$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            Iterator it = this.$data.iterator();
            int i = 0;
            if (!it.hasNext()) {
                obj2 = null;
            } else {
                obj2 = it.next();
                if (it.hasNext()) {
                    ArrayList<BarChart.b> arrayList = ((BarChart.a) obj2).c().get(0);
                    wd4.a((Object) arrayList, "it.mListOfBarPoints[0]");
                    int i2 = 0;
                    for (BarChart.b e : arrayList) {
                        i2 += pc4.a(e.e()).intValue();
                    }
                    Integer a = pc4.a(i2);
                    do {
                        Object next = it.next();
                        ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).c().get(0);
                        wd4.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                        int i3 = 0;
                        for (BarChart.b e2 : arrayList2) {
                            i3 += pc4.a(e2.e()).intValue();
                        }
                        Integer a2 = pc4.a(i3);
                        if (a.compareTo(a2) < 0) {
                            obj2 = next;
                            a = a2;
                        }
                    } while (it.hasNext());
                }
            }
            BarChart.a aVar = (BarChart.a) obj2;
            if (aVar == null) {
                return null;
            }
            ArrayList<ArrayList<BarChart.b>> c = aVar.c();
            if (c == null) {
                return null;
            }
            ArrayList<BarChart.b> arrayList3 = c.get(0);
            if (arrayList3 == null) {
                return null;
            }
            for (BarChart.b e3 : arrayList3) {
                i += pc4.a(e3.e()).intValue();
            }
            return pc4.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
