package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pj2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.Calendar;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$Anon1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
public final class SetHybridPresetToWatchUseCase$setPresetToDb$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetHybridPresetToWatchUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHybridPresetToWatchUseCase$setPresetToDb$Anon1(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = setHybridPresetToWatchUseCase;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SetHybridPresetToWatchUseCase$setPresetToDb$Anon1 setHybridPresetToWatchUseCase$setPresetToDb$Anon1 = new SetHybridPresetToWatchUseCase$setPresetToDb$Anon1(this.this$Anon0, kc4);
        setHybridPresetToWatchUseCase$setPresetToDb$Anon1.p$ = (lh4) obj;
        return setHybridPresetToWatchUseCase$setPresetToDb$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetHybridPresetToWatchUseCase$setPresetToDb$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        HybridPreset hybridPreset;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            HybridPreset a2 = SetHybridPresetToWatchUseCase.c(this.this$Anon0).a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
            a2.setActive(true);
            HybridPresetRepository a3 = this.this$Anon0.h;
            this.L$Anon0 = lh4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (a3.upsertHybridPreset(a2, this) == a) {
                return a;
            }
            hybridPreset = a2;
        } else if (i == 1) {
            hybridPreset = (HybridPreset) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "Calendar.getInstance()");
        String t = sk2.t(instance.getTime());
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            if (!pj2.a(next.getSettings())) {
                SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase = this.this$Anon0;
                String appId = next.getAppId();
                String settings = next.getSettings();
                if (settings != null) {
                    setHybridPresetToWatchUseCase.a(appId, settings);
                    MicroAppLastSettingRepository b = this.this$Anon0.j;
                    String appId2 = next.getAppId();
                    wd4.a((Object) t, "updatedAt");
                    String settings2 = next.getSettings();
                    if (settings2 != null) {
                        b.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, t, settings2));
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        this.this$Anon0.a(new SetHybridPresetToWatchUseCase.d());
        return cb4.a;
    }
}
