package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as2;
import com.fossil.blesdk.obfuscated.du3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.h83;
import com.fossil.blesdk.obfuscated.l83;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.r83;
import com.fossil.blesdk.obfuscated.ra;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.t82;
import com.fossil.blesdk.obfuscated.ur3;
import com.fossil.blesdk.obfuscated.w83;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ye;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeOverviewFragment extends as2 {
    @DexIgnore
    public ur3<t82> j;
    @DexIgnore
    public ActiveTimeOverviewDayPresenter k;
    @DexIgnore
    public ActiveTimeOverviewWeekPresenter l;
    @DexIgnore
    public ActiveTimeOverviewMonthPresenter m;
    @DexIgnore
    public h83 n;
    @DexIgnore
    public w83 o;
    @DexIgnore
    public r83 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            ur3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(7, a != null ? (t82) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public c(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            ur3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(4, a != null ? (t82) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public d(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            ur3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(2, a != null ? (t82) a.a() : null);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onCreateView");
        t82 t82 = (t82) ra.a(layoutInflater, R.layout.fragment_active_time_overview, viewGroup, false, O0());
        g9.c((View) t82.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wd4.a((Object) t82, "binding");
        a(t82);
        this.j = new ur3<>(this, t82);
        ur3<t82> ur3 = this.j;
        if (ur3 != null) {
            t82 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(t82 t82) {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "initUI");
        this.n = (h83) getChildFragmentManager().a("ActiveTimeOverviewDayFragment");
        this.o = (w83) getChildFragmentManager().a("ActiveTimeOverviewWeekFragment");
        this.p = (r83) getChildFragmentManager().a("ActiveTimeOverviewMonthFragment");
        if (this.n == null) {
            this.n = new h83();
        }
        if (this.o == null) {
            this.o = new w83();
        }
        if (this.p == null) {
            this.p = new r83();
        }
        ArrayList arrayList = new ArrayList();
        h83 h83 = this.n;
        if (h83 != null) {
            arrayList.add(h83);
            w83 w83 = this.o;
            if (w83 != null) {
                arrayList.add(w83);
                r83 r83 = this.p;
                if (r83 != null) {
                    arrayList.add(r83);
                    RecyclerView recyclerView = t82.t;
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new du3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActiveTimeOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ye().a(recyclerView);
                    a(this.q, t82);
                    m42 g = PortfolioApp.W.c().g();
                    h83 h832 = this.n;
                    if (h832 != null) {
                        w83 w832 = this.o;
                        if (w832 != null) {
                            r83 r832 = this.p;
                            if (r832 != null) {
                                g.a(new l83(h832, w832, r832)).a(this);
                                t82.s.setOnClickListener(new b(this));
                                t82.q.setOnClickListener(new c(this));
                                t82.r.setOnClickListener(new d(this));
                                return;
                            }
                            wd4.a();
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, t82 t82) {
        if (t82 != null) {
            FlexibleTextView flexibleTextView = t82.s;
            wd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = t82.q;
            wd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = t82.r;
            wd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = t82.s;
            wd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = t82.q;
            wd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = t82.r;
            wd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = t82.r;
                wd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = t82.r;
                wd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = t82.q;
                wd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                ur3<t82> ur3 = this.j;
                if (ur3 != null) {
                    t82 a2 = ur3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = t82.q;
                wd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = t82.q;
                wd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = t82.q;
                wd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                ur3<t82> ur32 = this.j;
                if (ur32 != null) {
                    t82 a3 = ur32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = t82.s;
                wd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = t82.s;
                wd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = t82.q;
                wd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                ur3<t82> ur33 = this.j;
                if (ur33 != null) {
                    t82 a4 = ur33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = t82.s;
                wd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = t82.s;
                wd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = t82.q;
                wd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                ur3<t82> ur34 = this.j;
                if (ur34 != null) {
                    t82 a5 = ur34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
