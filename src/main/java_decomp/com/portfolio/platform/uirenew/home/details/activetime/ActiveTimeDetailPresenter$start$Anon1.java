package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Unit;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$start$Anon1(ActiveTimeDetailPresenter activeTimeDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activeTimeDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActiveTimeDetailPresenter$start$Anon1 activeTimeDetailPresenter$start$Anon1 = new ActiveTimeDetailPresenter$start$Anon1(this.this$Anon0, kc4);
        activeTimeDetailPresenter$start$Anon1.p$ = (lh4) obj;
        return activeTimeDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (r0 != null) goto L_0x001f;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Unit unit;
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$Anon0;
            MFUser currentUser = activeTimeDetailPresenter.v.getCurrentUser();
            if (currentUser != null) {
                unit = currentUser.getDistanceUnit();
            }
            unit = Unit.METRIC;
            activeTimeDetailPresenter.o = unit;
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
