package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.un1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$addLocation$Anon1<TResult> implements un1<FetchPlaceResponse> {
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;

    @DexIgnore
    public WeatherSettingPresenter$addLocation$Anon1(WeatherSettingPresenter weatherSettingPresenter, String str, String str2) {
        this.a = weatherSettingPresenter;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
        this.a.i.a();
        wd4.a((Object) fetchPlaceResponse, "response");
        Place place = fetchPlaceResponse.getPlace();
        wd4.a((Object) place, "response.place");
        LatLng latLng = place.getLatLng();
        if (latLng != null) {
            ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(latLng, (kc4) null, this), 3, (Object) null);
        }
    }
}
