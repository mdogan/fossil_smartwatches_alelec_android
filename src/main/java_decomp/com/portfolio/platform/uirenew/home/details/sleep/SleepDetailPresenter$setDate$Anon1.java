package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xf3;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$Anon1", f = "SleepDetailPresenter.kt", l = {146, 170, 171}, m = "invokeSuspend")
public final class SleepDetailPresenter$setDate$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$Anon1$Anon1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Date>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon1(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return PortfolioApp.W.c().k();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$setDate$Anon1(SleepDetailPresenter sleepDetailPresenter, Date date, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepDetailPresenter$setDate$Anon1 sleepDetailPresenter$setDate$Anon1 = new SleepDetailPresenter$setDate$Anon1(this.this$Anon0, this.$date, kc4);
        sleepDetailPresenter$setDate$Anon1.p$ = (lh4) obj;
        return sleepDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepDetailPresenter$setDate$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x019e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01c2  */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        MFSleepDay mFSleepDay;
        List list;
        Pair pair;
        lh4 lh4;
        Boolean bool;
        android.util.Pair<Date, Date> pair2;
        Object obj3;
        boolean z;
        lh4 lh42;
        android.util.Pair<Date, Date> a;
        Object obj4;
        SleepDetailPresenter sleepDetailPresenter;
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh42 = this.p$;
            if (this.this$Anon0.f == null) {
                sleepDetailPresenter = this.this$Anon0;
                gh4 a3 = sleepDetailPresenter.b();
                Anon1 anon1 = new Anon1((kc4) null);
                this.L$Anon0 = lh42;
                this.L$Anon1 = sleepDetailPresenter;
                this.label = 1;
                obj4 = kg4.a(a3, anon1, this);
                if (obj4 == a2) {
                    return a2;
                }
            }
            lh4 = lh42;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
            this.this$Anon0.g = this.$date;
            z = sk2.c(this.this$Anon0.f, this.$date);
            Boolean s = sk2.s(this.$date);
            xf3 n = this.this$Anon0.r;
            Date date = this.$date;
            wd4.a((Object) s, "isToday");
            n.a(date, z, s.booleanValue(), !sk2.c(new Date(), this.$date));
            a = sk2.a(this.$date, this.this$Anon0.f);
            wd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (Pair) this.this$Anon0.h.a();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("SleepDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new Pair(a.first, a.second));
            if (pair != null || !sk2.d((Date) pair.getFirst(), (Date) a.first) || !sk2.d((Date) pair.getSecond(), (Date) a.second)) {
                this.this$Anon0.j = false;
                this.this$Anon0.k = false;
                this.this$Anon0.h.a(new Pair(a.first, a.second));
                return cb4.a;
            }
            gh4 a4 = this.this$Anon0.b();
            SleepDetailPresenter$setDate$Anon1$summary$Anon1 sleepDetailPresenter$setDate$Anon1$summary$Anon1 = new SleepDetailPresenter$setDate$Anon1$summary$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.Z$Anon0 = z;
            this.L$Anon1 = s;
            this.L$Anon2 = a;
            this.L$Anon3 = pair;
            this.label = 2;
            obj3 = kg4.a(a4, sleepDetailPresenter$setDate$Anon1$summary$Anon1, this);
            if (obj3 == a2) {
                return a2;
            }
            pair2 = a;
            bool = s;
            MFSleepDay mFSleepDay2 = (MFSleepDay) obj3;
            gh4 a5 = this.this$Anon0.b();
            SleepDetailPresenter$setDate$Anon1$sessions$Anon1 sleepDetailPresenter$setDate$Anon1$sessions$Anon1 = new SleepDetailPresenter$setDate$Anon1$sessions$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair;
            this.L$Anon4 = mFSleepDay2;
            this.label = 3;
            obj2 = kg4.a(a5, sleepDetailPresenter$setDate$Anon1$sessions$Anon1, this);
            if (obj2 != a2) {
            }
        } else if (i == 1) {
            sleepDetailPresenter = (SleepDetailPresenter) this.L$Anon1;
            lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj4 = obj;
        } else if (i == 2) {
            bool = (Boolean) this.L$Anon1;
            boolean z2 = this.Z$Anon0;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            pair = (Pair) this.L$Anon3;
            pair2 = (android.util.Pair) this.L$Anon2;
            z = z2;
            obj3 = obj;
            MFSleepDay mFSleepDay22 = (MFSleepDay) obj3;
            gh4 a52 = this.this$Anon0.b();
            SleepDetailPresenter$setDate$Anon1$sessions$Anon1 sleepDetailPresenter$setDate$Anon1$sessions$Anon12 = new SleepDetailPresenter$setDate$Anon1$sessions$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.Z$Anon0 = z;
            this.L$Anon1 = bool;
            this.L$Anon2 = pair2;
            this.L$Anon3 = pair;
            this.L$Anon4 = mFSleepDay22;
            this.label = 3;
            obj2 = kg4.a(a52, sleepDetailPresenter$setDate$Anon1$sessions$Anon12, this);
            if (obj2 != a2) {
                return a2;
            }
            mFSleepDay = mFSleepDay22;
            list = (List) obj2;
            if (!wd4.a((Object) this.this$Anon0.n, (Object) mFSleepDay)) {
            }
            if (!wd4.a((Object) this.this$Anon0.o, (Object) list)) {
            }
            this.this$Anon0.r.a(mFSleepDay);
            ri4 unused = this.this$Anon0.k();
            ri4 unused2 = this.this$Anon0.l();
            return cb4.a;
        } else if (i == 3) {
            mFSleepDay = (MFSleepDay) this.L$Anon4;
            Pair pair3 = (Pair) this.L$Anon3;
            android.util.Pair pair4 = (android.util.Pair) this.L$Anon2;
            Boolean bool2 = (Boolean) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj2 = obj;
            list = (List) obj2;
            if (!wd4.a((Object) this.this$Anon0.n, (Object) mFSleepDay)) {
                this.this$Anon0.n = mFSleepDay;
            }
            if (!wd4.a((Object) this.this$Anon0.o, (Object) list)) {
                this.this$Anon0.o = list;
            }
            this.this$Anon0.r.a(mFSleepDay);
            if (this.this$Anon0.j && this.this$Anon0.k) {
                ri4 unused3 = this.this$Anon0.k();
                ri4 unused4 = this.this$Anon0.l();
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        sleepDetailPresenter.f = (Date) obj4;
        lh4 = lh42;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("SleepDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$Anon0.f);
        this.this$Anon0.g = this.$date;
        z = sk2.c(this.this$Anon0.f, this.$date);
        Boolean s2 = sk2.s(this.$date);
        xf3 n2 = this.this$Anon0.r;
        Date date2 = this.$date;
        wd4.a((Object) s2, "isToday");
        n2.a(date2, z, s2.booleanValue(), !sk2.c(new Date(), this.$date));
        a = sk2.a(this.$date, this.this$Anon0.f);
        wd4.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (Pair) this.this$Anon0.h.a();
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d("SleepDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$Anon0.j = false;
        this.this$Anon0.k = false;
        this.this$Anon0.h.a(new Pair(a.first, a.second));
        return cb4.a;
    }
}
