package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ad3;
import com.fossil.blesdk.obfuscated.bd3;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$Anon1", f = "DashboardSleepPresenter.kt", l = {65}, m = "invokeSuspend")
public final class DashboardSleepPresenter$initDataSource$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardSleepPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements dc<rd<SleepSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepPresenter$initDataSource$Anon1 a;

        @DexIgnore
        public a(DashboardSleepPresenter$initDataSource$Anon1 dashboardSleepPresenter$initDataSource$Anon1) {
            this.a = dashboardSleepPresenter$initDataSource$Anon1;
        }

        @DexIgnore
        public final void a(rd<SleepSummary> rdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(rdVar != null ? Integer.valueOf(rdVar.size()) : null);
            local.d("DashboardSleepPresenter", sb.toString());
            if (rdVar != null) {
                this.a.this$Anon0.k().a(rdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardSleepPresenter$initDataSource$Anon1(DashboardSleepPresenter dashboardSleepPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dashboardSleepPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DashboardSleepPresenter$initDataSource$Anon1 dashboardSleepPresenter$initDataSource$Anon1 = new DashboardSleepPresenter$initDataSource$Anon1(this.this$Anon0, kc4);
        dashboardSleepPresenter$initDataSource$Anon1.p$ = (lh4) obj;
        return dashboardSleepPresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardSleepPresenter$initDataSource$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 a3 = this.this$Anon0.c();
            DashboardSleepPresenter$initDataSource$Anon1$user$Anon1 dashboardSleepPresenter$initDataSource$Anon1$user$Anon1 = new DashboardSleepPresenter$initDataSource$Anon1$user$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a3, dashboardSleepPresenter$initDataSource$Anon1$user$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            Date d = sk2.d(mFUser.getCreatedAt());
            DashboardSleepPresenter dashboardSleepPresenter = this.this$Anon0;
            SleepSummariesRepository h = dashboardSleepPresenter.i;
            SleepSummariesRepository h2 = this.this$Anon0.i;
            SleepSessionsRepository f = this.this$Anon0.j;
            FitnessDataRepository c = this.this$Anon0.k;
            SleepDao d2 = this.this$Anon0.l;
            SleepDatabase e = this.this$Anon0.m;
            wd4.a((Object) d, "createdDate");
            dashboardSleepPresenter.g = h.getSummariesPaging(h2, f, c, d2, e, d, this.this$Anon0.o, this.this$Anon0);
            Listing g = this.this$Anon0.g;
            if (g != null) {
                LiveData pagedList = g.getPagedList();
                if (pagedList != null) {
                    ad3 k = this.this$Anon0.k();
                    if (k != null) {
                        pagedList.a((bd3) k, new a(this));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                }
            }
        }
        return cb4.a;
    }
}
