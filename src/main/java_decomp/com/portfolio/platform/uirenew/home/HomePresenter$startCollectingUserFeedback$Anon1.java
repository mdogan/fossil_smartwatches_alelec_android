package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomePresenter$startCollectingUserFeedback$Anon1 implements CoroutineUseCase.e<lr2.d, lr2.b> {
    @DexIgnore
    public /* final */ /* synthetic */ HomePresenter a;

    @DexIgnore
    public HomePresenter$startCollectingUserFeedback$Anon1(HomePresenter homePresenter) {
        this.a = homePresenter;
    }

    @DexIgnore
    public void a(lr2.b bVar) {
        wd4.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "startCollectingUserFeedback onError");
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(lr2.d dVar) {
        wd4.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "startCollectingUserFeedback onSuccess");
        ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
        ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
        this.a.l.a((ZendeskFeedbackConfiguration) new HomePresenter$startCollectingUserFeedback$Anon1$onSuccess$configuration$Anon1(dVar));
    }
}
