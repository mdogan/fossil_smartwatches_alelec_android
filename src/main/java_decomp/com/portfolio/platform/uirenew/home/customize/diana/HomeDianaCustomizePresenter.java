package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Intent;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ab4;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.d23;
import com.fossil.blesdk.obfuscated.e23;
import com.fossil.blesdk.obfuscated.g13;
import com.fossil.blesdk.obfuscated.g8;
import com.fossil.blesdk.obfuscated.i13;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rl4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.uj2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wu2;
import com.fossil.blesdk.obfuscated.xm2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter extends d23 {
    @DexIgnore
    public /* final */ UserRepository A;
    @DexIgnore
    public /* final */ WatchFaceRepository B;
    @DexIgnore
    public LiveData<List<DianaPreset>> f; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> g; // = this.z.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<DianaPreset> j; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public DianaPreset k;
    @DexIgnore
    public MutableLiveData<String> l;
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> n; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int o; // = 1;
    @DexIgnore
    public MFUser p;
    @DexIgnore
    public pl4 q; // = rl4.a(false, 1, (Object) null);
    @DexIgnore
    public Pair<Boolean, ? extends List<DianaPreset>> r; // = ab4.a(false, null);
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ e23 t;
    @DexIgnore
    public /* final */ WatchAppRepository u;
    @DexIgnore
    public /* final */ ComplicationRepository v;
    @DexIgnore
    public /* final */ DianaPresetRepository w;
    @DexIgnore
    public /* final */ SetDianaPresetToWatchUseCase x;
    @DexIgnore
    public /* final */ xm2 y;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SetDianaPresetToWatchUseCase.d, SetDianaPresetToWatchUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter a;

        @DexIgnore
        public b(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            this.a = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.t.m();
            this.a.t.e(0);
        }

        @DexIgnore
        public void a(SetDianaPresetToWatchUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.a.t.m();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
                wd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                e23 r = this.a.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
                if (array != null) {
                    PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                    r.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.t.j();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public HomeDianaCustomizePresenter(e23 e23, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, xm2 xm2, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        wd4.b(e23, "mView");
        wd4.b(watchAppRepository, "mWatchAppRepository");
        wd4.b(complicationRepository, "mComplicationRepository");
        wd4.b(dianaPresetRepository, "mDianaPresetRepository");
        wd4.b(setDianaPresetToWatchUseCase, "mSetDianaPresetToWatchUseCase");
        wd4.b(xm2, "mCustomizeRealDataManager");
        wd4.b(customizeRealDataRepository, "mCustomizeRealDataRepository");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(watchFaceRepository, "mWatchFaceRepository");
        wd4.b(portfolioApp, "mApp");
        this.t = e23;
        this.u = watchAppRepository;
        this.v = complicationRepository;
        this.w = dianaPresetRepository;
        this.x = setDianaPresetToWatchUseCase;
        this.y = xm2;
        this.z = customizeRealDataRepository;
        this.A = userRepository;
        this.B = watchFaceRepository;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.l = portfolioApp.f();
    }

    @DexIgnore
    public final void b(String str) {
        T t2;
        T t3;
        List a2 = this.f.a();
        if (a2 != null) {
            Boolean.valueOf(!a2.isEmpty());
        }
        Iterator<T> it = this.j.iterator();
        while (true) {
            t2 = null;
            if (!it.hasNext()) {
                t3 = null;
                break;
            }
            t3 = it.next();
            if (wd4.a((Object) ((DianaPreset) t3).getId(), (Object) str)) {
                break;
            }
        }
        DianaPreset dianaPreset = (DianaPreset) t3;
        if (dianaPreset != null) {
            Iterator<T> it2 = this.j.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next = it2.next();
                if (((DianaPreset) next).isActive()) {
                    t2 = next;
                    break;
                }
            }
            DianaPreset dianaPreset2 = (DianaPreset) t2;
            this.t.l();
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + dianaPreset2 + " set preset " + dianaPreset + " as active first");
            this.x.a(new SetDianaPresetToWatchUseCase.c(dianaPreset), new HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(dianaPreset2, this, str));
        }
    }

    @DexIgnore
    public final void c(int i2) {
        this.m = i2;
    }

    @DexIgnore
    public void f() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$start$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            MutableLiveData<String> mutableLiveData = this.l;
            e23 e23 = this.t;
            if (e23 != null) {
                mutableLiveData.a((LifecycleOwner) (wu2) e23);
                this.f.a((LifecycleOwner) this.t);
                this.g.a((LifecycleOwner) this.t);
                this.x.g();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e);
        }
    }

    @DexIgnore
    public void h() {
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$createNewPreset$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> i() {
        return this.n;
    }

    @DexIgnore
    public void j() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.k);
        Set<Integer> a2 = cn2.d.a(this.k);
        cn2 cn2 = cn2.d;
        e23 e23 = this.t;
        if (e23 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (cn2.a(((wu2) e23).getContext(), a2)) {
            DianaPreset dianaPreset = this.k;
            if (dianaPreset != null) {
                this.t.l();
                this.x.a(new SetDianaPresetToWatchUseCase.c(dianaPreset), new b(this));
            }
        }
    }

    @DexIgnore
    public final int k() {
        return this.m;
    }

    @DexIgnore
    public void l() {
        this.t.a(this);
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.j.size());
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$showPresets$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final g13 a(DianaPreset dianaPreset) {
        T t2;
        String str;
        T t3;
        String str2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "convertPresetToDianaPresetConfigWrapper watchAppsSize " + this.i.size() + " compsSize " + this.h.size());
        ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
        ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        WatchFace watchFaceWithId = this.B.getWatchFaceWithId(dianaPreset.getWatchFaceId());
        WatchFaceWrapper b2 = watchFaceWithId != null ? uj2.b(watchFaceWithId, complications) : null;
        Iterator<DianaPresetComplicationSetting> it = complications.iterator();
        while (it.hasNext()) {
            DianaPresetComplicationSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.h.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t3 = null;
                    break;
                }
                t3 = it2.next();
                if (wd4.a((Object) ((Complication) t3).getComplicationId(), (Object) component2)) {
                    break;
                }
            }
            Complication complication = (Complication) t3;
            if (complication != null) {
                String complicationId = complication.getComplicationId();
                String icon = complication.getIcon();
                if (icon != null) {
                    str2 = icon;
                } else {
                    str2 = "";
                }
                arrayList.add(new i13(complicationId, str2, tm2.a(PortfolioApp.W.c(), complication.getNameKey(), complication.getName()), component1, this.y.a(this.p, this.n, complication.getComplicationId(), dianaPreset)));
            } else {
                DianaPreset dianaPreset2 = dianaPreset;
            }
        }
        DianaPreset dianaPreset3 = dianaPreset;
        Iterator<DianaPresetWatchAppSetting> it3 = watchapps.iterator();
        while (it3.hasNext()) {
            DianaPresetWatchAppSetting next2 = it3.next();
            String component12 = next2.component1();
            String component22 = next2.component2();
            Iterator<T> it4 = this.i.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it4.next();
                if (wd4.a((Object) ((WatchApp) t2).getWatchappId(), (Object) component22)) {
                    break;
                }
            }
            WatchApp watchApp = (WatchApp) t2;
            if (watchApp != null) {
                String watchappId = watchApp.getWatchappId();
                String icon2 = watchApp.getIcon();
                if (icon2 != null) {
                    str = icon2;
                } else {
                    str = "";
                }
                arrayList2.add(new i13(watchappId, str, tm2.a(PortfolioApp.W.c(), watchApp.getNameKey(), watchApp.getName()), component12, (String) null, 16, (rd4) null));
            }
        }
        return new g13(dianaPreset.getId(), dianaPreset.getName(), arrayList, arrayList2, dianaPreset.isActive(), b2);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.o = i2;
        if (this.o == 1 && this.r.getFirst().booleanValue()) {
            List list = (List) this.r.getSecond();
            if (list != null) {
                a((List<DianaPreset>) list);
            }
            this.r = ab4.a(false, null);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.j.size() > i2) {
            this.m = i2;
            this.k = this.j.get(this.m);
            DianaPreset dianaPreset = this.k;
            if (dianaPreset != null) {
                this.t.d(dianaPreset.isActive());
            }
        } else {
            this.t.d(false);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "viewPos " + i2 + " presenterPos " + this.m + " size " + this.j.size());
    }

    @DexIgnore
    public void a(String str, String str2) {
        wd4.b(str, "name");
        wd4.b(str2, "presetId");
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$renameCurrentPreset$Anon1(this, str2, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "nextActivePresetId");
        DianaPreset dianaPreset = this.k;
        if (dianaPreset == null) {
            return;
        }
        if (dianaPreset.isActive()) {
            b(str);
        } else {
            ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$deletePreset$$inlined$let$lambda$Anon1(dianaPreset, (kc4) null, this, str), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.s = false;
        if (i2 == 100 && i3 == -1) {
            this.t.d(0);
        }
    }

    @DexIgnore
    public void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2) {
        wd4.b(list, "views");
        wd4.b(list2, "customizeWidgetViews");
        if (!this.s) {
            this.s = true;
            this.t.a(g13, list, list2);
        }
    }

    @DexIgnore
    public final void a(List<DianaPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "doShowingPreset");
        this.j.clear();
        this.j.addAll(list);
        int size = this.j.size();
        int i2 = this.m;
        if (size > i2 && i2 > 0) {
            this.k = this.j.get(i2);
        }
        m();
    }
}
