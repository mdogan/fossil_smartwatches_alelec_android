package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$createNewPreset$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {321}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$createNewPreset$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$createNewPreset$Anon1(HomeDianaCustomizePresenter homeDianaCustomizePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDianaCustomizePresenter$createNewPreset$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1 = new HomeDianaCustomizePresenter$createNewPreset$Anon1(this.this$Anon0, kc4);
        homeDianaCustomizePresenter$createNewPreset$Anon1.p$ = (lh4) obj;
        return homeDianaCustomizePresenter$createNewPreset$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$createNewPreset$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            Iterator it = this.this$Anon0.j.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (pc4.a(((DianaPreset) obj2).isActive()).booleanValue()) {
                    break;
                }
            }
            DianaPreset dianaPreset = (DianaPreset) obj2;
            if (dianaPreset != null) {
                DianaPreset cloneFrom = DianaPreset.Companion.cloneFrom(dianaPreset);
                gh4 d = this.this$Anon0.c();
                HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new HomeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(cloneFrom, (kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = dianaPreset;
                this.L$Anon2 = dianaPreset;
                this.L$Anon3 = cloneFrom;
                this.label = 1;
                if (kg4.a(d, homeDianaCustomizePresenter$createNewPreset$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                    return a;
                }
            }
            return cb4.a;
        } else if (i == 1) {
            DianaPreset dianaPreset2 = (DianaPreset) this.L$Anon3;
            DianaPreset dianaPreset3 = (DianaPreset) this.L$Anon2;
            DianaPreset dianaPreset4 = (DianaPreset) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0;
        homeDianaCustomizePresenter.c(homeDianaCustomizePresenter.k() + 1);
        this.this$Anon0.t.v();
        return cb4.a;
    }
}
