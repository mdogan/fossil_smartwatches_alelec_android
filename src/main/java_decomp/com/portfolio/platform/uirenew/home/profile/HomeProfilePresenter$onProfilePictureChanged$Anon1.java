package com.portfolio.platform.uirenew.home.profile;

import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$Anon1", f = "HomeProfilePresenter.kt", l = {335, 341}, m = "invokeSuspend")
public final class HomeProfilePresenter$onProfilePictureChanged$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Uri $imageUri;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$Anon1$Anon1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Bitmap bitmap, kc4 kc4) {
            super(2, kc4);
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.$bitmap, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                Bitmap bitmap = this.$bitmap;
                if (bitmap != null) {
                    return wr3.a(bitmap);
                }
                wd4.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$onProfilePictureChanged$Anon1(HomeProfilePresenter homeProfilePresenter, Uri uri, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeProfilePresenter;
        this.$imageUri = uri;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeProfilePresenter$onProfilePictureChanged$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1 = new HomeProfilePresenter$onProfilePictureChanged$Anon1(this.this$Anon0, this.$imageUri, kc4);
        homeProfilePresenter$onProfilePictureChanged$Anon1.p$ = (lh4) obj;
        return homeProfilePresenter$onProfilePictureChanged$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$onProfilePictureChanged$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 = new HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            mFUser = (MFUser) this.L$Anon2;
            Bitmap bitmap = (Bitmap) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            try {
                za4.a(obj);
                mFUser.setProfilePicture((String) obj);
                this.this$Anon0.b(this.this$Anon0.k());
            } catch (Exception e) {
                e.printStackTrace();
                this.this$Anon0.l().o();
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Bitmap bitmap2 = (Bitmap) obj;
        MFUser k = this.this$Anon0.k();
        if (k != null) {
            gh4 b2 = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(bitmap2, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = bitmap2;
            this.L$Anon2 = k;
            this.label = 2;
            obj = kg4.a(b2, anon1, this);
            if (obj == a) {
                return a;
            }
            mFUser = k;
            mFUser.setProfilePicture((String) obj);
        }
        this.this$Anon0.b(this.this$Anon0.k());
        return cb4.a;
    }
}
