package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.eg3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends eg3.b>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewDayPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(List list, kc4 kc4, SleepOverviewDayPresenter$showDetailChart$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1) {
        super(2, kc4);
        this.$it = list;
        this.this$Anon0 = sleepOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0);
        sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            SleepOverviewDayPresenter sleepOverviewDayPresenter = this.this$Anon0.this$Anon0;
            return sleepOverviewDayPresenter.a(SleepOverviewDayPresenter.b(sleepOverviewDayPresenter), (List<MFSleepSession>) this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
