package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$Anon1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
public final class SearchRingPhonePresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchRingPhonePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$Anon1$Anon1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<? extends Ringtone>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon1(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return AppHelper.f.c();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchRingPhonePresenter$start$Anon1(SearchRingPhonePresenter searchRingPhonePresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = searchRingPhonePresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SearchRingPhonePresenter$start$Anon1 searchRingPhonePresenter$start$Anon1 = new SearchRingPhonePresenter$start$Anon1(this.this$Anon0, kc4);
        searchRingPhonePresenter$start$Anon1.p$ = (lh4) obj;
        return searchRingPhonePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchRingPhonePresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ArrayList arrayList;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.f.isEmpty()) {
                ArrayList b = this.this$Anon0.f;
                gh4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((kc4) null);
                this.L$Anon0 = lh4;
                this.L$Anon1 = b;
                this.label = 1;
                obj = kg4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                arrayList = b;
            }
            this.this$Anon0.j().r(this.this$Anon0.f);
            return cb4.a;
        } else if (i == 1) {
            arrayList = (ArrayList) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList.addAll((Collection) obj);
        if (this.this$Anon0.g == null) {
            SearchRingPhonePresenter searchRingPhonePresenter = this.this$Anon0;
            searchRingPhonePresenter.g = (Ringtone) searchRingPhonePresenter.f.get(0);
        }
        this.this$Anon0.j().r(this.this$Anon0.f);
        return cb4.a;
    }
}
