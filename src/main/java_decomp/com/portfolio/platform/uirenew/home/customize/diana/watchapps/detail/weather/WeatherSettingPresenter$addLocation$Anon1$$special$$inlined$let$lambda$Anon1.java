package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.android.gms.maps.model.LatLng;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LatLng $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter$addLocation$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.a.i.o(this.this$Anon0.this$Anon0.a.h);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(LatLng latLng, kc4 kc4, WeatherSettingPresenter$addLocation$Anon1 weatherSettingPresenter$addLocation$Anon1) {
        super(2, kc4);
        this.$it = latLng;
        this.this$Anon0 = weatherSettingPresenter$addLocation$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0);
        weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            String a2 = us3.a(this.this$Anon0.b);
            List b = this.this$Anon0.a.h;
            String str = this.this$Anon0.c;
            LatLng latLng = this.$it;
            double d = latLng.e;
            double d2 = latLng.f;
            wd4.a((Object) a2, "name");
            WeatherLocationWrapper weatherLocationWrapper = r5;
            WeatherLocationWrapper weatherLocationWrapper2 = new WeatherLocationWrapper(str, d, d2, a2, this.this$Anon0.b, false, true, 32, (rd4) null);
            b.add(weatherLocationWrapper);
            bj4 c = zh4.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (kg4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            String str2 = (String) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
