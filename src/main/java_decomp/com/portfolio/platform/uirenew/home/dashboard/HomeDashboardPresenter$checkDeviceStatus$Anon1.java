package com.portfolio.platform.uirenew.home.dashboard;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$Anon1", f = "HomeDashboardPresenter.kt", l = {102, 103, 104, 105}, m = "invokeSuspend")
public final class HomeDashboardPresenter$checkDeviceStatus$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$checkDeviceStatus$Anon1(HomeDashboardPresenter homeDashboardPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = homeDashboardPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HomeDashboardPresenter$checkDeviceStatus$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1(this.this$Anon0, kc4);
        homeDashboardPresenter$checkDeviceStatus$Anon1.p$ = (lh4) obj;
        return homeDashboardPresenter$checkDeviceStatus$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDashboardPresenter$checkDeviceStatus$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0207  */
    public final Object invokeSuspend(Object obj) {
        SleepStatistic sleepStatistic;
        ActivityStatistic activityStatistic;
        List list;
        Object obj2;
        lh4 lh4;
        Object a;
        Object a2;
        lh4 lh42;
        Object a3 = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh42 = this.p$;
            gh4 a4 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.label = 1;
            obj = kg4.a(a4, homeDashboardPresenter$checkDeviceStatus$Anon1$activityStatistic$Anon1, this);
            if (obj == a3) {
                return a3;
            }
        } else if (i == 1) {
            lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            activityStatistic = (ActivityStatistic) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            SleepStatistic sleepStatistic2 = (SleepStatistic) obj;
            gh4 a5 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic2;
            this.label = 3;
            a2 = kg4.a(a5, homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1, this);
            if (a2 != a3) {
                return a3;
            }
            Object obj3 = a2;
            sleepStatistic = sleepStatistic2;
            obj = obj3;
            List list2 = (List) obj;
            gh4 a6 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic;
            this.L$Anon3 = list2;
            this.label = 4;
            a = kg4.a(a6, homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1, this);
            if (a != a3) {
            }
        } else if (i == 3) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            ActivityStatistic activityStatistic2 = (ActivityStatistic) this.L$Anon1;
            sleepStatistic = (SleepStatistic) this.L$Anon2;
            activityStatistic = activityStatistic2;
            List list22 = (List) obj;
            gh4 a62 = this.this$Anon0.c();
            HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon12 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = activityStatistic;
            this.L$Anon2 = sleepStatistic;
            this.L$Anon3 = list22;
            this.label = 4;
            a = kg4.a(a62, homeDashboardPresenter$checkDeviceStatus$Anon1$deviceName$Anon12, this);
            if (a != a3) {
                return a3;
            }
            list = list22;
            obj = a;
            String str = (String) obj;
            if (PortfolioApp.W.c().g(this.this$Anon0.h) != 2) {
            }
            this.this$Anon0.p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("checkDeviceStatus activity ");
            sb.append(activityStatistic == null ? pc4.a(activityStatistic.getTotalSteps()) : null);
            sb.append(" sleep ");
            sb.append(sleepStatistic == null ? pc4.a(sleepStatistic.getTotalSleeps()) : null);
            sb.append(" totalDevices ");
            sb.append(list.size());
            local.d("HomeDashboardPresenter", sb.toString());
            if (!(!list.isEmpty())) {
            }
            this.this$Anon0.a("release");
            this.this$Anon0.o();
            return cb4.a;
        } else if (i == 4) {
            list = (List) this.L$Anon3;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            sleepStatistic = (SleepStatistic) this.L$Anon2;
            activityStatistic = (ActivityStatistic) this.L$Anon1;
            String str2 = (String) obj;
            boolean z = PortfolioApp.W.c().g(this.this$Anon0.h) != 2;
            this.this$Anon0.p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("checkDeviceStatus activity ");
            sb2.append(activityStatistic == null ? pc4.a(activityStatistic.getTotalSteps()) : null);
            sb2.append(" sleep ");
            sb2.append(sleepStatistic == null ? pc4.a(sleepStatistic.getTotalSleeps()) : null);
            sb2.append(" totalDevices ");
            sb2.append(list.size());
            local2.d("HomeDashboardPresenter", sb2.toString());
            if (!(!list.isEmpty())) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (pc4.a(wd4.a((Object) ((Device) obj2).getDeviceId(), (Object) this.this$Anon0.h)).booleanValue()) {
                        break;
                    }
                }
                Device device = (Device) obj2;
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("checkDeviceStatus activeDevice ");
                sb3.append(device != null ? device.getDeviceId() : null);
                sb3.append(" currentSerial ");
                sb3.append(this.this$Anon0.h);
                local3.d("HomeDashboardPresenter", sb3.toString());
                if (device != null) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "checkDeviceStatus isActiveDeviceConnected " + z);
                    this.this$Anon0.x.a(this.this$Anon0.h, str2);
                    this.this$Anon0.x.a(true, true, this.this$Anon0.p);
                } else {
                    this.this$Anon0.x.a((String) null, "");
                    this.this$Anon0.x.a(true, false, this.this$Anon0.p);
                }
            } else {
                this.this$Anon0.x.a((String) null, "");
                this.this$Anon0.x.a(false, false, this.this$Anon0.p);
            }
            this.this$Anon0.a("release");
            this.this$Anon0.o();
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivityStatistic activityStatistic3 = (ActivityStatistic) obj;
        gh4 a7 = this.this$Anon0.c();
        HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1(this, (kc4) null);
        this.L$Anon0 = lh42;
        this.L$Anon1 = activityStatistic3;
        this.label = 2;
        Object a8 = kg4.a(a7, homeDashboardPresenter$checkDeviceStatus$Anon1$sleepStatistic$Anon1, this);
        if (a8 == a3) {
            return a3;
        }
        lh4 lh44 = lh42;
        activityStatistic = activityStatistic3;
        obj = a8;
        lh4 = lh44;
        SleepStatistic sleepStatistic22 = (SleepStatistic) obj;
        gh4 a52 = this.this$Anon0.c();
        HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1 homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon12 = new HomeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = activityStatistic;
        this.L$Anon2 = sleepStatistic22;
        this.label = 3;
        a2 = kg4.a(a52, homeDashboardPresenter$checkDeviceStatus$Anon1$totalDevices$Anon12, this);
        if (a2 != a3) {
        }
    }
}
