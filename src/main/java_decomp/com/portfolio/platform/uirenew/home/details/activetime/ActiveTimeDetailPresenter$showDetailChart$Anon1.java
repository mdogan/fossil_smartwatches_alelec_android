package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xr2;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {235, 238}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$showDetailChart$Anon1(ActiveTimeDetailPresenter activeTimeDetailPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activeTimeDetailPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActiveTimeDetailPresenter$showDetailChart$Anon1 activeTimeDetailPresenter$showDetailChart$Anon1 = new ActiveTimeDetailPresenter$showDetailChart$Anon1(this.this$Anon0, kc4);
        activeTimeDetailPresenter$showDetailChart$Anon1.p$ = (lh4) obj;
        return activeTimeDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$showDetailChart$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ba  */
    public final Object invokeSuspend(Object obj) {
        Pair pair;
        ArrayList arrayList;
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 a2 = this.this$Anon0.b();
            ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1 activeTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1 = new ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(a2, activeTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            arrayList = (ArrayList) this.L$Anon2;
            pair = (Pair) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            Integer num = (Integer) obj;
            int a3 = yk2.d.a(this.this$Anon0.m, GoalType.ACTIVE_TIME);
            this.this$Anon0.s.a((xr2) new BarChart.c(Math.max(num == null ? num.intValue() : 0, a3 / 16), a3, arrayList), (ArrayList<String>) (ArrayList) pair.getSecond());
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair2 = (Pair) obj;
        ArrayList arrayList2 = (ArrayList) pair2.getFirst();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailPresenter", "showDetailChart - date=" + this.this$Anon0.g + ", data=" + arrayList2);
        gh4 a4 = this.this$Anon0.b();
        ActiveTimeDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 activeTimeDetailPresenter$showDetailChart$Anon1$maxValue$Anon1 = new ActiveTimeDetailPresenter$showDetailChart$Anon1$maxValue$Anon1(arrayList2, (kc4) null);
        this.L$Anon0 = lh4;
        this.L$Anon1 = pair2;
        this.L$Anon2 = arrayList2;
        this.label = 2;
        Object a5 = kg4.a(a4, activeTimeDetailPresenter$showDetailChart$Anon1$maxValue$Anon1, this);
        if (a5 == a) {
            return a;
        }
        arrayList = arrayList2;
        Object obj2 = a5;
        pair = pair2;
        obj = obj2;
        Integer num2 = (Integer) obj;
        int a32 = yk2.d.a(this.this$Anon0.m, GoalType.ACTIVE_TIME);
        this.this$Anon0.s.a((xr2) new BarChart.c(Math.max(num2 == null ? num2.intValue() : 0, a32 / 16), a32, arrayList), (ArrayList<String>) (ArrayList) pair.getSecond());
        return cb4.a;
    }
}
