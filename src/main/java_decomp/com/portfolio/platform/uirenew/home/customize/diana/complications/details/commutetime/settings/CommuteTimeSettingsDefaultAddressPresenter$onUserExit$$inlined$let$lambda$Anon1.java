package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$BooleanRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $address$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$BooleanRef $isAddressSaved$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$BooleanRef $isChanged$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.h().a((List<String>) this.this$Anon0.this$Anon0.k);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.i().getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<MFUser>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(MFUser mFUser, kc4 kc4, CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) {
            super(2, kc4);
            this.$user = mFUser;
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon3 anon3 = new Anon3(this.$user, kc4, this.this$Anon0);
            anon3.p$ = (lh4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                UserRepository i2 = this.this$Anon0.this$Anon0.i();
                MFUser mFUser = this.$user;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = i2.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon4 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<MFUser>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4(MFUser mFUser, kc4 kc4, CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) {
            super(2, kc4);
            this.$user = mFUser;
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon4 anon4 = new Anon4(this.$user, kc4, this.this$Anon0);
            anon4.p$ = (lh4) obj;
            return anon4;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                UserRepository i2 = this.this$Anon0.this$Anon0.i();
                MFUser mFUser = this.$user;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = i2.updateUser(mFUser, false, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(String str, kc4 kc4, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, String str2, Ref$BooleanRef ref$BooleanRef, Ref$BooleanRef ref$BooleanRef2) {
        super(2, kc4);
        this.$it = str;
        this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter;
        this.$address$inlined = str2;
        this.$isChanged$inlined = ref$BooleanRef;
        this.$isAddressSaved$inlined = ref$BooleanRef2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
        commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x026a  */
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        ro2 ro2;
        lh4 lh4;
        MFUser mFUser2;
        MFUser mFUser3;
        ro2 ro22;
        lh4 lh42;
        Object a = oc4.a();
        int i = this.label;
        boolean z = false;
        if (i == 0) {
            za4.a(obj);
            lh42 = this.p$;
            if (!(this.$it.length() == 0) && !this.this$Anon0.k.contains(this.$it)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = CommuteTimeSettingsDefaultAddressPresenter.o;
                local.d(l, "add " + this.$address$inlined + " to recent search list");
                this.this$Anon0.k.add(0, this.$it);
                gh4 a2 = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (kc4) null);
                this.L$Anon0 = lh42;
                this.label = 1;
                if (kg4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
            gh4 a3 = this.this$Anon0.c();
            Anon2 anon2 = new Anon2(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.label = 2;
            obj = kg4.a(a3, anon2, this);
            if (obj == a) {
            }
        } else if (i == 1) {
            lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            gh4 a32 = this.this$Anon0.c();
            Anon2 anon22 = new Anon2(this, (kc4) null);
            this.L$Anon0 = lh42;
            this.label = 2;
            obj = kg4.a(a32, anon22, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 2) {
            lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 3) {
            mFUser3 = (MFUser) this.L$Anon2;
            mFUser2 = (MFUser) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            ro22 = (ro2) obj;
            if (!(ro22 instanceof so2)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String l2 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local2.d(l2, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " successfully");
                this.$isAddressSaved$inlined.element = true;
            } else if (ro22 instanceof qo2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String l3 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local3.d(l3, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " failed");
                gh4 a4 = this.this$Anon0.c();
                Anon4 anon4 = new Anon4(mFUser3, (kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = mFUser2;
                this.L$Anon2 = mFUser3;
                this.label = 4;
                obj = kg4.a(a4, anon4, this);
                if (obj == a) {
                    return a;
                }
                mFUser = mFUser3;
                ro2 = (ro2) obj;
                Ref$BooleanRef ref$BooleanRef = this.$isAddressSaved$inlined;
                if (!(ro2 instanceof so2)) {
                }
                ref$BooleanRef.element = z;
            }
            this.this$Anon0.j().a();
            if (this.$isAddressSaved$inlined.element) {
            }
            return cb4.a;
        } else if (i == 4) {
            mFUser = (MFUser) this.L$Anon2;
            MFUser mFUser4 = (MFUser) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            ro2 = (ro2) obj;
            Ref$BooleanRef ref$BooleanRef2 = this.$isAddressSaved$inlined;
            if (!(ro2 instanceof so2)) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String l4 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local4.d(l4, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " successfully");
                z = true;
            } else if (ro2 instanceof qo2) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String l5 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local5.d(l5, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " failed");
            } else {
                throw new NoWhenBranchMatchedException();
            }
            ref$BooleanRef2.element = z;
            this.this$Anon0.j().a();
            if (this.$isAddressSaved$inlined.element) {
                this.this$Anon0.j().B(this.$address$inlined);
            } else {
                this.this$Anon0.j().B((String) null);
            }
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        lh4 = lh42;
        MFUser mFUser5 = (MFUser) obj;
        if (mFUser5 != null) {
            String b = this.this$Anon0.h;
            if (b != null) {
                int hashCode = b.hashCode();
                if (hashCode != 2255103) {
                    if (hashCode == 76517104 && b.equals("Other")) {
                        this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getWork(), this.$address$inlined);
                        mFUser5.setWork(this.$address$inlined);
                    }
                } else if (b.equals("Home")) {
                    this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getHome(), this.$address$inlined);
                    mFUser5.setHome(this.$address$inlined);
                }
            }
            if (this.$isChanged$inlined.element) {
                this.this$Anon0.j().b();
                gh4 a5 = this.this$Anon0.c();
                Anon3 anon3 = new Anon3(mFUser5, (kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = mFUser5;
                this.L$Anon2 = mFUser5;
                this.label = 3;
                Object a6 = kg4.a(a5, anon3, this);
                if (a6 == a) {
                    return a;
                }
                mFUser2 = mFUser5;
                obj = a6;
                mFUser3 = mFUser2;
                ro22 = (ro2) obj;
                if (!(ro22 instanceof so2)) {
                }
                this.this$Anon0.j().a();
            }
            if (this.$isAddressSaved$inlined.element) {
            }
        }
        return cb4.a;
    }
}
