package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
public final class DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super SKUModel>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1(DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1 deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1 = new DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1(this.this$Anon0, kc4);
        deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1.p$ = (lh4) obj;
        return deleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1$skuModel$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return this.this$Anon0.this$Anon0.g.getSkuModelBySerialPrefix(PortfolioApp.W.c().e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
