package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
public final class NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1(NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1 notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1 = new NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1(this.this$Anon0, kc4);
        notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1.p$ = (lh4) obj;
        return notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            for (ContactGroup contactGroup : this.this$Anon0.$successResponse.a()) {
                for (Contact next : contactGroup.getContacts()) {
                    ContactWrapper contactWrapper = new ContactWrapper(next, (String) null, 2, (rd4) null);
                    contactWrapper.setAdded(true);
                    Contact contact = contactWrapper.getContact();
                    if (contact != null) {
                        wd4.a((Object) next, "contact");
                        contact.setDbRowId(next.getDbRowId());
                        contact.setUseSms(next.isUseSms());
                        contact.setUseCall(next.isUseCall());
                    }
                    contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                    wd4.a((Object) next, "contact");
                    List<PhoneNumber> phoneNumbers = next.getPhoneNumbers();
                    wd4.a((Object) phoneNumbers, "contact.phoneNumbers");
                    if (!phoneNumbers.isEmpty()) {
                        PhoneNumber phoneNumber = next.getPhoneNumbers().get(0);
                        wd4.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                        String number = phoneNumber.getNumber();
                        if (!TextUtils.isEmpty(number)) {
                            contactWrapper.setHasPhoneNumber(true);
                            contactWrapper.setPhoneNumber(number);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a = NotificationHybridContactPresenter.n.a();
                            local.d(a, " filter selected contact, phoneNumber=" + number);
                        }
                    }
                    Iterator it = this.this$Anon0.this$Anon0.a.this$Anon0.i.iterator();
                    int i = 0;
                    while (true) {
                        if (!it.hasNext()) {
                            i = -1;
                            break;
                        }
                        Contact contact2 = ((ContactWrapper) it.next()).getContact();
                        if (pc4.a(contact2 != null && contact2.getContactId() == next.getContactId()).booleanValue()) {
                            break;
                        }
                        i++;
                    }
                    if (i != -1) {
                        contactWrapper.setCurrentHandGroup(this.this$Anon0.this$Anon0.a.this$Anon0.h);
                        wd4.a(this.this$Anon0.this$Anon0.a.this$Anon0.i.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                    } else if (contactWrapper.getCurrentHandGroup() == this.this$Anon0.this$Anon0.a.this$Anon0.h) {
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a2 = NotificationHybridContactPresenter.n.a();
                    local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + next.getDbRowId() + ", isUseText = " + next.isUseSms() + ", isUseCall = " + next.isUseCall());
                    this.this$Anon0.this$Anon0.a.this$Anon0.j().add(contactWrapper);
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
