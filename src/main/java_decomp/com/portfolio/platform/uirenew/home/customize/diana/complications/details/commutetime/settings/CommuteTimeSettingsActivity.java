package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.e33;
import com.fossil.blesdk.obfuscated.q23;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public CommuteTimeSettingsPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            wd4.b(fragment, "fragment");
            wd4.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        q23 q23 = (q23) getSupportFragmentManager().a((int) R.id.content);
        if (q23 == null) {
            q23 = q23.q.a();
            a((Fragment) q23, q23.q.b(), (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new e33(q23)).a(this);
        if (getIntent() != null) {
            str = getIntent().getStringExtra(Constants.USER_SETTING);
            wd4.a((Object) str, "intent.getStringExtra(Constants.JSON_KEY_SETTINGS)");
        } else {
            str = "";
        }
        CommuteTimeSettingsPresenter commuteTimeSettingsPresenter = this.B;
        if (commuteTimeSettingsPresenter != null) {
            commuteTimeSettingsPresenter.c(str);
        } else {
            wd4.d("mCommuteTimeSettingsPresenter");
            throw null;
        }
    }
}
