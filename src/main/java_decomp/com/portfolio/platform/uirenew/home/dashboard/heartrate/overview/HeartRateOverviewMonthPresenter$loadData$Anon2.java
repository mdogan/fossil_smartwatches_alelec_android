package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qc3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$Anon2", f = "HeartRateOverviewMonthPresenter.kt", l = {94}, m = "invokeSuspend")
public final class HeartRateOverviewMonthPresenter$loadData$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewMonthPresenter$loadData$Anon2(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = heartRateOverviewMonthPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        HeartRateOverviewMonthPresenter$loadData$Anon2 heartRateOverviewMonthPresenter$loadData$Anon2 = new HeartRateOverviewMonthPresenter$loadData$Anon2(this.this$Anon0, kc4);
        heartRateOverviewMonthPresenter$loadData$Anon2.p$ = (lh4) obj;
        return heartRateOverviewMonthPresenter$loadData$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewMonthPresenter$loadData$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            HeartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 heartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1 = new HeartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = kg4.a(b, heartRateOverviewMonthPresenter$loadData$Anon2$currentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            this.this$Anon0.j = sk2.d(mFUser.getCreatedAt());
            qc3 l = this.this$Anon0.n;
            Date d = HeartRateOverviewMonthPresenter.d(this.this$Anon0);
            Date c = this.this$Anon0.j;
            if (c == null) {
                c = new Date();
            }
            l.a(d, c);
            this.this$Anon0.f.a(new Date());
        }
        return cb4.a;
    }
}
