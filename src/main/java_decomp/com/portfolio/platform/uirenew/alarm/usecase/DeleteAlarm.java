package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAlarm extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((rd4) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public c e;
    @DexIgnore
    public /* final */ DeleteAlarmsBroadcastReceiver f; // = new DeleteAlarmsBroadcastReceiver();
    @DexIgnore
    public /* final */ AlarmsRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DeleteAlarmsBroadcastReceiver implements BleCommandResultManager.b {
        @DexIgnore
        public DeleteAlarmsBroadcastReceiver() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && DeleteAlarm.this.d()) {
                DeleteAlarm.this.a(false);
                FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive");
                Alarm a2 = DeleteAlarm.this.e().a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive success");
                    ri4 unused = mg4.b(DeleteAlarm.this.b(), (CoroutineContext) null, (CoroutineStart) null, new DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1(this, a2, (kc4) null), 3, (Object) null);
                    DeleteAlarm.this.a(new d(a2));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = DeleteAlarm.i.a();
                local.d(a3, "onReceive error - errorCode=" + intExtra);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                DeleteAlarm.this.a(new b(a2, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteAlarm.h;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            wd4.b(arrayList, "errorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public c(String str, List<Alarm> list, Alarm alarm) {
            wd4.b(str, "deviceId");
            wd4.b(list, "alarms");
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public d(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        }
    }

    /*
    static {
        String simpleName = DeleteAlarm.class.getSimpleName();
        wd4.a((Object) simpleName, "DeleteAlarm::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public DeleteAlarm(AlarmsRepository alarmsRepository) {
        wd4.b(alarmsRepository, "mAlarmsRepository");
        this.g = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return h;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final c e() {
        c cVar = this.e;
        if (cVar != null) {
            return cVar;
        }
        wd4.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(c cVar, kc4<Object> kc4) {
        DeleteAlarm$run$Anon1 deleteAlarm$run$Anon1;
        int i2;
        Alarm alarm;
        if (kc4 instanceof DeleteAlarm$run$Anon1) {
            deleteAlarm$run$Anon1 = (DeleteAlarm$run$Anon1) kc4;
            int i3 = deleteAlarm$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deleteAlarm$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deleteAlarm$run$Anon1.result;
                Object a2 = oc4.a();
                i2 = deleteAlarm$run$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(h, "executeUseCase");
                    if (cVar != null) {
                        this.e = cVar;
                        this.d = true;
                        Alarm a3 = cVar.a();
                        if (a3.isActive()) {
                            ArrayList arrayList = new ArrayList();
                            for (Alarm next : cVar.b()) {
                                Alarm alarmById = this.g.getAlarmById(next.getUri());
                                if (alarmById == null) {
                                    arrayList.add(next);
                                } else if (!wd4.a((Object) alarmById.getUri(), (Object) a3.getUri())) {
                                    alarmById.setMinute(next.getMinute());
                                    alarmById.setDays(next.getDays());
                                    alarmById.setHour(next.getHour());
                                    alarmById.setActive(next.isActive());
                                    alarmById.setCreatedAt(next.getCreatedAt());
                                    alarmById.setRepeated(next.isRepeated());
                                    arrayList.add(alarmById);
                                }
                            }
                            a((List<Alarm>) arrayList, cVar.c());
                            return cb4.a;
                        }
                        AlarmsRepository alarmsRepository = this.g;
                        deleteAlarm$run$Anon1.L$Anon0 = this;
                        deleteAlarm$run$Anon1.L$Anon1 = cVar;
                        deleteAlarm$run$Anon1.L$Anon2 = a3;
                        deleteAlarm$run$Anon1.label = 1;
                        if (alarmsRepository.deleteAlarm(a3, deleteAlarm$run$Anon1) == a2) {
                            return a2;
                        }
                        alarm = a3;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    alarm = (Alarm) deleteAlarm$run$Anon1.L$Anon2;
                    c cVar2 = (c) deleteAlarm$run$Anon1.L$Anon1;
                    DeleteAlarm deleteAlarm = (DeleteAlarm) deleteAlarm$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return new d(alarm);
            }
        }
        deleteAlarm$run$Anon1 = new DeleteAlarm$run$Anon1(this, kc4);
        Object obj2 = deleteAlarm$run$Anon1.result;
        Object a22 = oc4.a();
        i2 = deleteAlarm$run$Anon1.label;
        if (i2 != 0) {
        }
        return new d(alarm);
    }

    @DexIgnore
    public final void a(List<Alarm> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "setAlarms - alarms=" + list);
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((Alarm) next).isActive()) {
                arrayList.add(next);
            }
        }
        PortfolioApp.W.c().a(str, (List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) oj2.a(arrayList));
    }
}
