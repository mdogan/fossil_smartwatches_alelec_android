package com.portfolio.platform.uirenew.alarm.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1", f = "DeleteAlarm.kt", l = {39}, m = "invokeSuspend")
public final class DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $requestAlarm;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAlarm.DeleteAlarmsBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1(DeleteAlarm.DeleteAlarmsBroadcastReceiver deleteAlarmsBroadcastReceiver, Alarm alarm, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deleteAlarmsBroadcastReceiver;
        this.$requestAlarm = alarm;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1 = new DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1(this.this$Anon0, this.$requestAlarm, kc4);
        deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1.p$ = (lh4) obj;
        return deleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            AlarmsRepository a2 = DeleteAlarm.this.g;
            Alarm alarm = this.$requestAlarm;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (a2.deleteAlarm(alarm, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
