package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.util.SparseIntArray;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zt2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.service.BleCommandResultManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$Anon1", f = "AlarmPresenter.kt", l = {61}, m = "invokeSuspend")
public final class AlarmPresenter$start$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$Anon1$Anon1", f = "AlarmPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(AlarmPresenter$start$Anon1 alarmPresenter$start$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = alarmPresenter$start$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.t.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmPresenter$start$Anon1(AlarmPresenter alarmPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = alarmPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        AlarmPresenter$start$Anon1 alarmPresenter$start$Anon1 = new AlarmPresenter$start$Anon1(this.this$Anon0, kc4);
        alarmPresenter$start$Anon1.p$ = (lh4) obj;
        return alarmPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmPresenter$start$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        AlarmPresenter alarmPresenter;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            FLogger.INSTANCE.getLocal().d(AlarmPresenter.v.a(), VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            PortfolioApp.W.b((Object) lh4);
            this.this$Anon0.q.f();
            this.this$Anon0.s.f();
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            AlarmPresenter alarmPresenter2 = this.this$Anon0;
            gh4 a2 = alarmPresenter2.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = alarmPresenter2;
            this.label = 1;
            obj = kg4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            alarmPresenter = alarmPresenter2;
        } else if (i == 1) {
            alarmPresenter = (AlarmPresenter) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        alarmPresenter.k = (MFUser) obj;
        MFUser j = this.this$Anon0.k;
        if (!(j == null || j.getUserId() == null)) {
            this.this$Anon0.m.l(this.this$Anon0.p != null);
            if (this.this$Anon0.i == null) {
                this.this$Anon0.i = new SparseIntArray();
            }
            if (this.this$Anon0.f == null) {
                if (this.this$Anon0.p != null) {
                    AlarmPresenter alarmPresenter3 = this.this$Anon0;
                    alarmPresenter3.f = alarmPresenter3.p.getTitle();
                    AlarmPresenter alarmPresenter4 = this.this$Anon0;
                    alarmPresenter4.g = alarmPresenter4.p.getTotalMinutes();
                    AlarmPresenter alarmPresenter5 = this.this$Anon0;
                    alarmPresenter5.h = alarmPresenter5.p.isRepeated();
                    AlarmPresenter alarmPresenter6 = this.this$Anon0;
                    alarmPresenter6.i = alarmPresenter6.a(alarmPresenter6.p.getDays());
                    AlarmPresenter alarmPresenter7 = this.this$Anon0;
                    alarmPresenter7.j = alarmPresenter7.p.isActive();
                    this.this$Anon0.m.e(false);
                    this.this$Anon0.m.K();
                } else {
                    this.this$Anon0.f = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_HybridAlarm_Title__Alarm);
                    this.this$Anon0.j = true;
                    this.this$Anon0.h = false;
                    this.this$Anon0.i = new SparseIntArray();
                    this.this$Anon0.k();
                    this.this$Anon0.m.e(true);
                }
            }
            this.this$Anon0.m.a(this.this$Anon0.g);
            this.this$Anon0.m.k(this.this$Anon0.h);
            zt2 l = this.this$Anon0.m;
            SparseIntArray d = this.this$Anon0.i;
            if (d != null) {
                l.a(d);
            } else {
                wd4.a();
                throw null;
            }
        }
        return cb4.a;
    }
}
