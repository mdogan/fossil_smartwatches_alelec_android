package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAlarms extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((rd4) null);
    @DexIgnore
    public c d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ SetAlarmsBroadcastReceiver f; // = new SetAlarmsBroadcastReceiver();
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ AlarmsRepository h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetAlarmsBroadcastReceiver implements BleCommandResultManager.b {
        @DexIgnore
        public SetAlarmsBroadcastReceiver() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && SetAlarms.this.d()) {
                SetAlarms.this.a(false);
                ri4 unused = mg4.b(SetAlarms.this.b(), (CoroutineContext) null, (CoroutineStart) null, new SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1(this, intent, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SetAlarms.i;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public b(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            wd4.b(arrayList, "mBLEErrorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public c(String str, List<Alarm> list, Alarm alarm) {
            wd4.b(str, "deviceId");
            wd4.b(list, "alarms");
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public d(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = SetAlarms.class.getSimpleName();
        wd4.a((Object) simpleName, "SetAlarms::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetAlarms(PortfolioApp portfolioApp, AlarmsRepository alarmsRepository) {
        wd4.b(portfolioApp, "mApp");
        wd4.b(alarmsRepository, "mAlarmsRepository");
        this.g = portfolioApp;
        this.h = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return i;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final c e() {
        c cVar = this.d;
        if (cVar != null) {
            return cVar;
        }
        wd4.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void a(Intent intent, Alarm alarm) {
        wd4.b(intent, "intent");
        wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "processBLEErrorCodes() - errorCode=" + intExtra);
        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
        if (integerArrayListExtra == null) {
            integerArrayListExtra = new ArrayList<>(intExtra);
        }
        vl2 c2 = AnalyticsHelper.f.c("set_alarms");
        if (c2 != null) {
            c2.a(String.valueOf(intExtra));
        }
        AnalyticsHelper.f.e("set_alarms");
        a(new b(alarm, intExtra, integerArrayListExtra));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(c cVar, kc4<Object> kc4) {
        SetAlarms$run$Anon1 setAlarms$run$Anon1;
        int i2;
        Alarm alarm;
        if (kc4 instanceof SetAlarms$run$Anon1) {
            setAlarms$run$Anon1 = (SetAlarms$run$Anon1) kc4;
            int i3 = setAlarms$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                setAlarms$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = setAlarms$run$Anon1.result;
                Object a2 = oc4.a();
                i2 = setAlarms$run$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(i, "executeUseCase");
                    vl2 b2 = AnalyticsHelper.f.b("set_alarms");
                    AnalyticsHelper.f.a("set_alarms", b2);
                    b2.d();
                    if (cVar != null) {
                        this.d = cVar;
                        Alarm a3 = cVar.a();
                        Alarm alarmById = this.h.getAlarmById(a3.getUri());
                        if (alarmById == null || alarmById.isActive() || a3.isActive()) {
                            FLogger.INSTANCE.getLocal().d(i, "alarm not found");
                            this.e = true;
                            a(cVar);
                            return cb4.a;
                        }
                        FLogger.INSTANCE.getLocal().d(i, "alarm found and not active");
                        AlarmsRepository alarmsRepository = this.h;
                        setAlarms$run$Anon1.L$Anon0 = this;
                        setAlarms$run$Anon1.L$Anon1 = cVar;
                        setAlarms$run$Anon1.L$Anon2 = a3;
                        setAlarms$run$Anon1.L$Anon3 = alarmById;
                        setAlarms$run$Anon1.label = 1;
                        if (alarmsRepository.updateAlarm(a3, setAlarms$run$Anon1) == a2) {
                            return a2;
                        }
                        alarm = a3;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    Alarm alarm2 = (Alarm) setAlarms$run$Anon1.L$Anon3;
                    alarm = (Alarm) setAlarms$run$Anon1.L$Anon2;
                    c cVar2 = (c) setAlarms$run$Anon1.L$Anon1;
                    SetAlarms setAlarms = (SetAlarms) setAlarms$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return new d(alarm);
            }
        }
        setAlarms$run$Anon1 = new SetAlarms$run$Anon1(this, kc4);
        Object obj2 = setAlarms$run$Anon1.result;
        Object a22 = oc4.a();
        i2 = setAlarms$run$Anon1.label;
        if (i2 != 0) {
        }
        return new d(alarm);
    }

    @DexIgnore
    public final void a(c cVar) {
        List<Alarm> b2 = cVar.b();
        ArrayList arrayList = new ArrayList();
        for (T next : b2) {
            if (((Alarm) next).isActive()) {
                arrayList.add(next);
            }
        }
        this.g.a(cVar.c(), (List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) oj2.a(arrayList));
    }
}
