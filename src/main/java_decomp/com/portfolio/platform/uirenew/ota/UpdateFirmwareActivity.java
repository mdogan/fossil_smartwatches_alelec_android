package com.portfolio.platform.uirenew.ota;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.km3;
import com.fossil.blesdk.obfuscated.ol3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwareActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a((rd4) null);
    @DexIgnore
    public UpdateFirmwarePresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareActivity.C;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            wd4.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z);
            Intent intent = new Intent(context, UpdateFirmwareActivity.class);
            intent.putExtra("IS_ONBOARDING_FLOW", z);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareActivity.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "UpdateFirmwareActivity::class.java.simpleName!!");
            C = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        km3 km3 = (km3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (km3 == null) {
            km3 = km3.o.a(z);
            a((Fragment) km3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new ol3(km3)).a(this);
    }
}
