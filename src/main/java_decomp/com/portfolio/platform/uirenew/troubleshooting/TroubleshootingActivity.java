package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.lp3;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.mp3;
import com.fossil.blesdk.obfuscated.op3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TroubleshootingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public op3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wd4.b(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            intent.putExtra("DEVICE_TYPE", DeviceHelper.o.f(str));
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        lp3 lp3 = (lp3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        wd4.a((Object) intent, "intent");
        boolean a2 = a(intent);
        if (lp3 == null) {
            lp3 = lp3.n.a(a2);
            a((Fragment) lp3, lp3.n.a(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (lp3 != null) {
            g.a(new mp3(lp3)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
