package com.portfolio.platform.uirenew.watchsetting;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Device $this_run;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchSettingViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super String>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1 watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                return this.this$Anon0.this$Anon0.m.getDeviceNameBySerial(this.this$Anon0.$this_run.getDeviceId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1(Device device, kc4 kc4, WatchSettingViewModel watchSettingViewModel, Device device2) {
        super(2, kc4);
        this.$this_run = device;
        this.this$Anon0 = watchSettingViewModel;
        this.$device$inlined = device2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1 watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1 = new WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1(this.$this_run, kc4, this.this$Anon0, this.$device$inlined);
        watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1.p$ = (lh4) obj;
        return watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        Object obj2;
        boolean z;
        boolean z2;
        Object a = oc4.a();
        int i = this.label;
        boolean z3 = false;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "onDeviceChanged - device: " + this.$device$inlined);
            str = PortfolioApp.W.c().e();
            boolean z4 = !FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
            if (this.$this_run.getVibrationStrength() == null) {
                this.$this_run.setVibrationStrength(pc4.a(25));
                cb4 cb4 = cb4.a;
            }
            gh4 b = zh4.b();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = str;
            this.Z$Anon0 = z4;
            this.label = 1;
            obj2 = kg4.a(b, anon1, this);
            if (obj2 == a) {
                return a;
            }
            z = z4;
        } else if (i == 1) {
            z = this.Z$Anon0;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            str = (String) this.L$Anon1;
            obj2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String str2 = (String) obj2;
        if (this.$this_run.getBatteryLevel() > 100) {
            this.$this_run.setBatteryLevel(100);
        }
        if (wd4.a((Object) this.$this_run.getDeviceId(), (Object) str)) {
            try {
                IButtonConnectivity b2 = PortfolioApp.W.b();
                if (b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2) {
                    z3 = true;
                }
                z2 = z3;
            } catch (Exception unused) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = WatchSettingViewModel.v.a();
                local2.e(a3, "exception when get gatt state of " + this.$this_run);
                z2 = false;
            }
            this.this$Anon0.g = new WatchSettingViewModel.c(this.$device$inlined, str2, z2, true, pc4.a(z));
        } else {
            this.this$Anon0.g = new WatchSettingViewModel.c(this.$device$inlined, str2, false, false, pc4.a(z));
        }
        WatchSettingViewModel.b h = this.this$Anon0.h;
        WatchSettingViewModel.c e = this.this$Anon0.g;
        if (e != null) {
            WatchSettingViewModel.b.a(h, false, true, false, e, (String) null, (Integer) null, (Pair) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, (ArrayList) null, 65525, (Object) null);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a4 = WatchSettingViewModel.v.a();
            local3.d(a4, "onDeviceChanged, mDeviceWrapper=" + this.this$Anon0.g);
            this.this$Anon0.d();
            this.this$Anon0.d.removeCallbacksAndMessages((Object) null);
            this.this$Anon0.n();
            return cb4.a;
        }
        wd4.a();
        throw null;
    }
}
