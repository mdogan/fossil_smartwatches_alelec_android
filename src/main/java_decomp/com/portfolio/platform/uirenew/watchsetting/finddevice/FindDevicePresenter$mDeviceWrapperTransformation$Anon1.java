package com.portfolio.platform.uirenew.watchsetting.finddevice;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindDevicePresenter$mDeviceWrapperTransformation$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ FindDevicePresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1", f = "FindDevicePresenter.kt", l = {65, 68}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter$mDeviceWrapperTransformation$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(FindDevicePresenter$mDeviceWrapperTransformation$Anon1 findDevicePresenter$mDeviceWrapperTransformation$Anon1, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = findDevicePresenter$mDeviceWrapperTransformation$Anon1;
            this.$serial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$serial, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00b9  */
        public final Object invokeSuspend(Object obj) {
            Device device;
            lh4 lh4;
            Object a = oc4.a();
            int i = this.label;
            Boolean bool = null;
            boolean z = true;
            if (i == 0) {
                za4.a(obj);
                lh4 = this.p$;
                gh4 a2 = this.this$Anon0.a.c();
                FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$deviceModel$Anon1 findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$deviceModel$Anon1 = new FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$deviceModel$Anon1(this, (kc4) null);
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = kg4.a(a2, findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$deviceModel$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else if (i == 2) {
                Device device2 = (Device) this.L$Anon1;
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                device = (Device) this.L$Anon2;
                String str = (String) obj;
                if (!wd4.a((Object) this.$serial, (Object) this.this$Anon0.a.i())) {
                    IButtonConnectivity b = PortfolioApp.W.b();
                    if (b != null) {
                        if (b.getGattState(this.$serial) != 2) {
                            z = false;
                        }
                        bool = pc4.a(z);
                    }
                    this.this$Anon0.a.g.a(new WatchSettingViewModel.c(device, str, bool != null ? bool.booleanValue() : false, true, (Boolean) null, 16, (rd4) null));
                } else {
                    this.this$Anon0.a.g.a(new WatchSettingViewModel.c(device, str, false, false, (Boolean) null, 16, (rd4) null));
                }
                this.this$Anon0.a.q.b(this.this$Anon0.a.p.F(), false);
                return cb4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device3 = (Device) obj;
            if (device3 != null) {
                gh4 a3 = this.this$Anon0.a.c();
                FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((kc4) null, this);
                this.L$Anon0 = lh4;
                this.L$Anon1 = device3;
                this.L$Anon2 = device3;
                this.label = 2;
                Object a4 = kg4.a(a3, findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this);
                if (a4 == a) {
                    return a;
                }
                device = device3;
                obj = a4;
                String str2 = (String) obj;
                if (!wd4.a((Object) this.$serial, (Object) this.this$Anon0.a.i())) {
                }
            }
            this.this$Anon0.a.q.b(this.this$Anon0.a.p.F(), false);
            return cb4.a;
        }
    }

    @DexIgnore
    public FindDevicePresenter$mDeviceWrapperTransformation$Anon1(FindDevicePresenter findDevicePresenter) {
        this.a = findDevicePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final MutableLiveData<WatchSettingViewModel.c> apply(String str) {
        ri4 unused = mg4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, str, (kc4) null), 3, (Object) null);
        return this.a.g;
    }
}
