package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.bs3;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.ho3;
import com.fossil.blesdk.obfuscated.io3;
import com.fossil.blesdk.obfuscated.k62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.ln2;
import com.fossil.blesdk.obfuscated.ls3;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.or2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sr2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.tr2;
import com.fossil.blesdk.obfuscated.uq2;
import com.fossil.blesdk.obfuscated.ur2;
import com.fossil.blesdk.obfuscated.vr2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wj2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpPresenter extends ho3 {
    @DexIgnore
    public static /* final */ String P;
    @DexIgnore
    public static /* final */ Pattern Q;
    @DexIgnore
    public static /* final */ a R; // = new a((rd4) null);
    @DexIgnore
    public CheckAuthenticationSocialExisting A;
    @DexIgnore
    public AnalyticsHelper B;
    @DexIgnore
    public or2 C;
    @DexIgnore
    public SummariesRepository D;
    @DexIgnore
    public SleepSummariesRepository E;
    @DexIgnore
    public GoalTrackingRepository F;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries G;
    @DexIgnore
    public FetchGoalTrackingData H;
    @DexIgnore
    public RequestEmailOtp I;
    @DexIgnore
    public GetSecretKeyUseCase J;
    @DexIgnore
    public WatchLocalizationRepository K;
    @DexIgnore
    public String L;
    @DexIgnore
    public String M;
    @DexIgnore
    public /* final */ io3 N;
    @DexIgnore
    public /* final */ BaseActivity O;
    @DexIgnore
    public sr2 f;
    @DexIgnore
    public ln2 g;
    @DexIgnore
    public tr2 h;
    @DexIgnore
    public vr2 i;
    @DexIgnore
    public ur2 j;
    @DexIgnore
    public LoginSocialUseCase k;
    @DexIgnore
    public UserRepository l;
    @DexIgnore
    public DeviceRepository m;
    @DexIgnore
    public k62 n;
    @DexIgnore
    public FetchSleepSessions o;
    @DexIgnore
    public FetchSleepSummaries p;
    @DexIgnore
    public FetchActivities q;
    @DexIgnore
    public FetchSummaries r;
    @DexIgnore
    public FetchHeartRateSamples s;
    @DexIgnore
    public FetchDailyHeartRateSummaries t;
    @DexIgnore
    public AlarmsRepository u;
    @DexIgnore
    public uq2 v;
    @DexIgnore
    public wj2 w;
    @DexIgnore
    public DownloadUserInfoUseCase x;
    @DexIgnore
    public fn2 y;
    @DexIgnore
    public CheckAuthenticationEmailExisting z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SignUpPresenter.P;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<or2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public b(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(or2.a aVar) {
            wd4.b(aVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Get current user success: " + aVar.a());
            MFUser a3 = aVar.a();
            if (a3 != null) {
                this.a.q().b(a3.getUserId());
            }
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wd4.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(SignUpPresenter.R.a(), "Get current user failed");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<CheckAuthenticationSocialExisting.d, CheckAuthenticationSocialExisting.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public c(SignUpPresenter signUpPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = signUpPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(CheckAuthenticationSocialExisting.d dVar) {
            wd4.b(dVar, "responseValue");
            boolean a2 = dVar.a();
            if (a2) {
                this.a.c(this.b);
            } else if (!a2) {
                this.a.N.b(this.b);
            }
        }

        @DexIgnore
        public void a(CheckAuthenticationSocialExisting.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.N.i();
            this.a.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<LoginSocialUseCase.d, LoginSocialUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public d(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginSocialUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            PortfolioApp.W.c().g().a(this.a);
            this.a.C();
        }

        @DexIgnore
        public void a(LoginSocialUseCase.b bVar) {
            wd4.b(bVar, "errorValue");
            this.a.N.i();
            this.a.a(bVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<uq2.e, uq2.d> {
        @DexIgnore
        public void a(uq2.d dVar) {
            wd4.b(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(uq2.e eVar) {
            wd4.b(eVar, "responseValue");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CoroutineUseCase.e<RequestEmailOtp.d, RequestEmailOtp.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth b;

        @DexIgnore
        public f(SignUpPresenter signUpPresenter, SignUpEmailAuth signUpEmailAuth) {
            this.a = signUpPresenter;
            this.b = signUpEmailAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(RequestEmailOtp.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.N.i();
            this.a.N.b(this.b);
        }

        @DexIgnore
        public void a(RequestEmailOtp.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.N.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "requestOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.N.f(cVar.a(), cVar.b());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements CoroutineUseCase.e<sr2.d, sr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public g(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sr2.d dVar) {
            wd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(sr2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.a.N.i();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements CoroutineUseCase.e<tr2.d, tr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public h(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tr2.d dVar) {
            wd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(tr2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.a.N.i();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CoroutineUseCase.e<ur2.d, ur2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public i(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ur2.d dVar) {
            wd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(ur2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.a.N.i();
            this.a.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CoroutineUseCase.e<vr2.d, vr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public j(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(vr2.d dVar) {
            wd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(vr2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.a.N.i();
            this.a.a(cVar.a(), "");
        }
    }

    /*
    static {
        String simpleName = SignUpPresenter.class.getSimpleName();
        wd4.a((Object) simpleName, "SignUpPresenter::class.java.simpleName");
        P = simpleName;
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            Q = compile;
        } else {
            wd4.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public SignUpPresenter(io3 io3, BaseActivity baseActivity) {
        wd4.b(io3, "mView");
        wd4.b(baseActivity, "mContext");
        this.N = io3;
        this.O = baseActivity;
    }

    @DexIgnore
    public final boolean A() {
        String str = this.L;
        if (str == null || str.length() == 0) {
            this.N.a(false, false, "");
            return false;
        } else if (!ls3.a(this.L)) {
            io3 io3 = this.N;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_SignUp_InputError_Text__InvalidEmailAddress);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            io3.a(false, true, a2);
            return false;
        } else {
            this.N.a(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean B() {
        if (!TextUtils.isEmpty(this.M)) {
            String str = this.M;
            if (str != null) {
                boolean z2 = str.length() >= 7;
                boolean matches = Q.matcher(this.M).matches();
                this.N.c(z2, matches);
                if (!z2 || !matches) {
                    return false;
                }
                return true;
            }
            wd4.a();
            throw null;
        }
        this.N.c(false, false);
        return false;
    }

    @DexIgnore
    public final void C() {
        FLogger.INSTANCE.getLocal().d(P, "onLoginSuccess download user info");
        DownloadUserInfoUseCase downloadUserInfoUseCase = this.x;
        if (downloadUserInfoUseCase != null) {
            downloadUserInfoUseCase.a(new DownloadUserInfoUseCase.c(), new SignUpPresenter$onLoginSocialSuccess$Anon1(this));
        } else {
            wd4.d("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void D() {
        this.N.a(this);
    }

    @DexIgnore
    public final void E() {
        Locale locale = Locale.getDefault();
        wd4.a((Object) locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            wd4.a((Object) locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                wd4.a((Object) locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append("_");
                Locale locale4 = Locale.getDefault();
                wd4.a((Object) locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = P;
                local.d(str, "language: " + sb2);
                if (cg4.b(sb2, "zh_CN", true) || cg4.b(sb2, "zh_SG", true)) {
                    this.N.z(true);
                    return;
                } else {
                    this.N.z(false);
                    return;
                }
            }
        }
        this.N.z(false);
    }

    @DexIgnore
    public final void F() {
        boolean z2 = TextUtils.isEmpty(this.L) || TextUtils.isEmpty(this.M);
        boolean A2 = A();
        boolean B2 = B();
        if (z2 || !B2 || !A2) {
            this.N.B0();
        } else {
            this.N.k0();
        }
    }

    @DexIgnore
    public void f() {
        this.N.g();
        F();
        E();
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        if (PortfolioApp.W.c().w()) {
            this.N.F();
        } else {
            a(601, "");
        }
    }

    @DexIgnore
    public void i() {
        this.N.k();
        sr2 sr2 = this.f;
        if (sr2 != null) {
            sr2.a(new sr2.b(new WeakReference(this.O)), new g(this));
        } else {
            wd4.d("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        this.N.k();
        tr2 tr2 = this.h;
        if (tr2 != null) {
            tr2.a(new tr2.b(new WeakReference(this.O)), new h(this));
        } else {
            wd4.d("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void k() {
        if (!AppHelper.f.a(this.O, "com.tencent.mm")) {
            AppHelper.f.b(this.O, "com.tencent.mm");
            return;
        }
        this.N.k();
        ur2 ur2 = this.j;
        if (ur2 != null) {
            ur2.a(new ur2.b(new WeakReference(this.O)), new i(this));
        } else {
            wd4.d("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void l() {
        this.N.k();
        vr2 vr2 = this.i;
        if (vr2 != null) {
            vr2.a(new vr2.b(new WeakReference(this.O)), new j(this));
        } else {
            wd4.d("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void m() {
        or2 or2 = this.C;
        if (or2 != null) {
            or2.a(null, new b(this));
        } else {
            wd4.d("mGetUser");
            throw null;
        }
    }

    @DexIgnore
    public final ri4 n() {
        return mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SignUpPresenter$checkOnboardingProgress$Anon1(this, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(LoginPresenter.O.a(), "downloadOptionalsResources");
        Date date = new Date();
        ri4 unused = mg4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SignUpPresenter$downloadOptionalsResources$Anon1(this, (kc4) null), 3, (Object) null);
        FetchSummaries fetchSummaries = this.r;
        if (fetchSummaries != null) {
            fetchSummaries.a(new FetchSummaries.b(date), (CoroutineUseCase.e) null);
            FetchActivities fetchActivities = this.q;
            if (fetchActivities != null) {
                fetchActivities.a(new FetchActivities.b(date), (CoroutineUseCase.e) null);
                FetchSleepSessions fetchSleepSessions = this.o;
                if (fetchSleepSessions != null) {
                    fetchSleepSessions.a(new FetchSleepSessions.b(date), (CoroutineUseCase.e) null);
                    FetchSleepSummaries fetchSleepSummaries = this.p;
                    if (fetchSleepSummaries != null) {
                        fetchSleepSummaries.a(new FetchSleepSummaries.b(date), (CoroutineUseCase.e) null);
                        FetchHeartRateSamples fetchHeartRateSamples = this.s;
                        if (fetchHeartRateSamples != null) {
                            fetchHeartRateSamples.a(new FetchHeartRateSamples.b(date), (CoroutineUseCase.e) null);
                            FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = this.t;
                            if (fetchDailyHeartRateSummaries != null) {
                                fetchDailyHeartRateSummaries.a(new FetchDailyHeartRateSummaries.b(date), (CoroutineUseCase.e) null);
                                FetchGoalTrackingData fetchGoalTrackingData = this.H;
                                if (fetchGoalTrackingData != null) {
                                    fetchGoalTrackingData.a(new FetchGoalTrackingData.b(date), (CoroutineUseCase.e) null);
                                    FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries = this.G;
                                    if (fetchDailyGoalTrackingSummaries != null) {
                                        fetchDailyGoalTrackingSummaries.a(new FetchDailyGoalTrackingSummaries.b(date), (CoroutineUseCase.e) null);
                                    } else {
                                        wd4.d("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    wd4.d("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                wd4.d("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            wd4.d("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        wd4.d("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    wd4.d("mFetchSleepSessions");
                    throw null;
                }
            } else {
                wd4.d("mFetchActivities");
                throw null;
            }
        } else {
            wd4.d("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final AlarmsRepository p() {
        AlarmsRepository alarmsRepository = this.u;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        wd4.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper q() {
        AnalyticsHelper analyticsHelper = this.B;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wd4.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository r() {
        DeviceRepository deviceRepository = this.m;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final wj2 s() {
        wj2 wj2 = this.w;
        if (wj2 != null) {
            return wj2;
        }
        wd4.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase t() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.J;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        wd4.d("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.F;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        wd4.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final fn2 v() {
        fn2 fn2 = this.y;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository w() {
        SleepSummariesRepository sleepSummariesRepository = this.E;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        wd4.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository x() {
        SummariesRepository summariesRepository = this.D;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        wd4.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository y() {
        UserRepository userRepository = this.l;
        if (userRepository != null) {
            return userRepository;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository z() {
        WatchLocalizationRepository watchLocalizationRepository = this.K;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        wd4.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public void a(boolean z2) {
        F();
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, "password");
        this.M = str;
        F();
    }

    @DexIgnore
    public final void c(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "auth");
        LoginSocialUseCase loginSocialUseCase = this.k;
        if (loginSocialUseCase != null) {
            loginSocialUseCase.a(new LoginSocialUseCase.c(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new d(this));
        } else {
            wd4.d("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "email");
        this.L = str;
        A();
    }

    @DexIgnore
    public final void b(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = P;
        local.d(str, "checkSocialAccountIsExisted " + signUpSocialAuth);
        CheckAuthenticationSocialExisting checkAuthenticationSocialExisting = this.A;
        if (checkAuthenticationSocialExisting != null) {
            checkAuthenticationSocialExisting.a(new CheckAuthenticationSocialExisting.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new c(this, signUpSocialAuth));
        } else {
            wd4.d("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        wd4.b(str, "email");
        wd4.b(str2, "password");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = P;
        local.d(str3, "signupEmail " + str + ' ' + str2);
        if (A()) {
            this.N.k();
            CheckAuthenticationEmailExisting checkAuthenticationEmailExisting = this.z;
            if (checkAuthenticationEmailExisting != null) {
                checkAuthenticationEmailExisting.a(new CheckAuthenticationEmailExisting.b(str), new SignUpPresenter$signupEmail$Anon1(this, str, str2));
            } else {
                wd4.d("mCheckAuthenticationEmailExisting");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c(String str) {
        wd4.b(str, "activeSerial");
        uq2 uq2 = this.v;
        if (uq2 != null) {
            uq2.a(new uq2.c(str), new e());
        } else {
            wd4.d("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "auth");
        this.N.k();
        b(signUpSocialAuth);
    }

    @DexIgnore
    public final void a(int i2, String str) {
        wd4.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = P;
        local.d(str2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.N.b(i2, str);
        } else if (!bs3.b(PortfolioApp.W.c())) {
            this.N.b(601, "");
        } else {
            this.N.b(i2, "");
        }
    }

    @DexIgnore
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "emailAuth");
        RequestEmailOtp requestEmailOtp = this.I;
        if (requestEmailOtp != null) {
            requestEmailOtp.a(new RequestEmailOtp.b(signUpEmailAuth.getEmail()), new f(this, signUpEmailAuth));
        } else {
            wd4.d("mRequestEmailOtp");
            throw null;
        }
    }
}
