package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.m42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.so3;
import com.fossil.blesdk.obfuscated.to3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xo3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EmailOtpVerificationActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((rd4) null);
    @DexIgnore
    public to3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            wd4.b(context, "context");
            wd4.b(signUpEmailAuth, "emailAuth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EmailOtpVerificationActivity", "start verify Otp with Email Auth =" + signUpEmailAuth);
            Intent intent = new Intent(context, EmailOtpVerificationActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        so3 so3 = (so3) getSupportFragmentManager().a((int) R.id.content);
        if (so3 == null) {
            so3 = so3.u.a();
            a((Fragment) so3, f(), (int) R.id.content);
        }
        m42 g = PortfolioApp.W.c().g();
        if (so3 != null) {
            g.a(new xo3(this, so3)).a(this);
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra("EMAIL_AUTH")) {
                FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent emailAddress");
                to3 to3 = this.B;
                if (to3 != null) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("EMAIL_AUTH");
                    wd4.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                    to3.a((SignUpEmailAuth) parcelableExtra);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationContract.View");
    }
}
