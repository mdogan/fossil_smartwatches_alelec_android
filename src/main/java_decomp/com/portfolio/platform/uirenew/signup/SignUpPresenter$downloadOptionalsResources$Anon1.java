package com.portfolio.platform.uirenew.signup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$Anon1", f = "SignUpPresenter.kt", l = {499, 509}, m = "invokeSuspend")
public final class SignUpPresenter$downloadOptionalsResources$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$Anon1$Anon1", f = "SignUpPresenter.kt", l = {500, 501, 502, 503, 504, 505}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$downloadOptionalsResources$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SignUpPresenter$downloadOptionalsResources$Anon1 signUpPresenter$downloadOptionalsResources$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = signUpPresenter$downloadOptionalsResources$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
            r6 = r5.this$Anon0.this$Anon0.x();
            r5.L$Anon0 = r1;
            r5.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x006e, code lost:
            if (r6.fetchActivitySettings(r5) != r0) goto L_0x0071;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            r6 = r5.this$Anon0.this$Anon0.w();
            r5.L$Anon0 = r1;
            r5.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0082, code lost:
            if (r6.fetchLastSleepGoal(r5) != r0) goto L_0x0085;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0084, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
            r6 = r5.this$Anon0.this$Anon0.u();
            r5.L$Anon0 = r1;
            r5.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
            if (r6.fetchGoalSetting(r5) != r0) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0098, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0099, code lost:
            r6 = r5.this$Anon0.this$Anon0.p();
            r5.L$Anon0 = r1;
            r5.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00aa, code lost:
            if (r6.downloadAlarms(r5) != r0) goto L_0x00ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00ac, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ad, code lost:
            r6 = r5.this$Anon0.this$Anon0.r();
            r5.L$Anon0 = r1;
            r5.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bf, code lost:
            if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r6, 0, r5, 1, (java.lang.Object) null) != r0) goto L_0x00c2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c1, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c4, code lost:
            return com.fossil.blesdk.obfuscated.cb4.a;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            lh4 lh4;
            Object a = oc4.a();
            switch (this.label) {
                case 0:
                    za4.a(obj);
                    lh4 = this.p$;
                    WatchLocalizationRepository z = this.this$Anon0.this$Anon0.z();
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    if (z.getWatchLocalizationFromServer(false, this) == a) {
                        return a;
                    }
                    break;
                case 1:
                    lh4 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                case 2:
                    lh4 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                case 3:
                    lh4 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                case 4:
                    lh4 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                case 5:
                    lh4 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                case 6:
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$downloadOptionalsResources$Anon1(SignUpPresenter signUpPresenter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = signUpPresenter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SignUpPresenter$downloadOptionalsResources$Anon1 signUpPresenter$downloadOptionalsResources$Anon1 = new SignUpPresenter$downloadOptionalsResources$Anon1(this.this$Anon0, kc4);
        signUpPresenter$downloadOptionalsResources$Anon1.p$ = (lh4) obj;
        return signUpPresenter$downloadOptionalsResources$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SignUpPresenter$downloadOptionalsResources$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    public final Object invokeSuspend(Object obj) {
        List list;
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            gh4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (kg4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            list = (List) obj;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.W.c().a((List<? extends Alarm>) oj2.a(list));
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        gh4 a2 = this.this$Anon0.b();
        SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 = new SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1(this, (kc4) null);
        this.L$Anon0 = lh4;
        this.label = 2;
        obj = kg4.a(a2, signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1, this);
        if (obj == a) {
            return a;
        }
        list = (List) obj;
        if (list == null) {
        }
        PortfolioApp.W.c().a((List<? extends Alarm>) oj2.a(list));
        return cb4.a;
    }
}
