package com.portfolio.platform.uirenew.signup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
public final class SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<Alarm>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter$downloadOptionalsResources$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1(SignUpPresenter$downloadOptionalsResources$Anon1 signUpPresenter$downloadOptionalsResources$Anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = signUpPresenter$downloadOptionalsResources$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 = new SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1(this.this$Anon0, kc4);
        signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1.p$ = (lh4) obj;
        return signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return this.this$Anon0.this$Anon0.p().getActiveAlarms();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
