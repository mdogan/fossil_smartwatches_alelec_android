package com.portfolio.platform.uirenew.signup;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.qn3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1", f = "SignUpPresenter.kt", l = {412, 420, 425, 429, 442, 450}, m = "invokeSuspend")
public final class SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $currentUser;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter$onLoginSocialSuccess$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1$Anon1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.a.y().clearAllUser();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1$Anon2", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;

        @DexIgnore
        public Anon2(kc4 kc4) {
            super(2, kc4);
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                PortfolioApp.W.c().S();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon4 implements CoroutineUseCase.e<qn3, on3> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef b;

        @DexIgnore
        public Anon4(SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1, Ref$ObjectRef ref$ObjectRef) {
            this.a = signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1;
            this.b = ref$ObjectRef;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qn3 qn3) {
            wd4.b(qn3, "responseValue");
            FLogger.INSTANCE.getLocal().d(SignUpPresenter.R.a(), "onLoginSuccess download device setting success");
            this.a.this$Anon0.a.c((String) this.b.element);
            PortfolioApp.W.c().a(this.a.this$Anon0.a.s(), false, 13);
            this.a.this$Anon0.a.N.i();
            this.a.this$Anon0.a.n();
        }

        @DexIgnore
        public void a(on3 on3) {
            wd4.b(on3, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "onLoginSuccess download device setting fail " + on3.a());
            ri4 unused = mg4.b(this.a.this$Anon0.a.e(), (CoroutineContext) null, (CoroutineStart) null, new SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1$Anon4$onError$Anon1(this, on3, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1(SignUpPresenter$onLoginSocialSuccess$Anon1 signUpPresenter$onLoginSocialSuccess$Anon1, MFUser mFUser, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = signUpPresenter$onLoginSocialSuccess$Anon1;
        this.$currentUser = mFUser;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 = new SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1(this.this$Anon0, this.$currentUser, kc4);
        signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b5, code lost:
        r13.this$Anon0.a.N.i();
        r13.this$Anon0.a.a(600, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00cb, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x010a, code lost:
        r13.this$Anon0.a.o();
        r14 = r13.this$Anon0.a.b();
        r6 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1.Anon2((com.fossil.blesdk.obfuscated.kc4) null);
        r13.L$Anon0 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0127, code lost:
        if (com.fossil.blesdk.obfuscated.kg4.a(r14, r6, r13) != r0) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0129, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x012a, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.signup.SignUpPresenter.R.a(), "onLoginSuccess download require device and preset");
        r14 = r13.this$Anon0.a.b();
        r6 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1$response$Anon1(r13, (com.fossil.blesdk.obfuscated.kc4) null);
        r13.L$Anon0 = r1;
        r13.label = 4;
        r14 = com.fossil.blesdk.obfuscated.kg4.a(r14, r6, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0151, code lost:
        if (r14 != r0) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0153, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0154, code lost:
        r14 = (com.fossil.blesdk.obfuscated.ro2) r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0158, code lost:
        if ((r14 instanceof com.fossil.blesdk.obfuscated.so2) == false) goto L_0x0296;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x015a, code lost:
        r6 = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.so2) r14).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0163, code lost:
        if (r6 == null) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0165, code lost:
        r5 = r6.get_items();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0169, code lost:
        r6 = new kotlin.jvm.internal.Ref$ObjectRef();
        r6.element = "";
        r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r7 = com.portfolio.platform.uirenew.signup.SignUpPresenter.R.a();
        r4.d(r7, "onLoginSuccess allDevices " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0190, code lost:
        if (r5 == null) goto L_0x01fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0192, code lost:
        r7 = r14;
        r8 = r1;
        r1 = r5.iterator();
        r4 = r5;
        r14 = r13;
        r5 = r6;
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01a1, code lost:
        if (r1.hasNext() == false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x01a3, code lost:
        r9 = (com.portfolio.platform.data.model.Device) r1.next();
        com.portfolio.platform.PortfolioApp.W.c().d(r9.getDeviceId(), r9.getMacAddress());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01be, code lost:
        if (r9.isActive() == false) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01c0, code lost:
        r5.element = r9.getDeviceId();
        com.portfolio.platform.PortfolioApp.W.c().c((java.lang.String) r5.element, r9.getMacAddress());
        r10 = com.portfolio.platform.util.DeviceUtils.g.a();
        r14.L$Anon0 = r8;
        r14.L$Anon1 = r7;
        r14.L$Anon2 = r6;
        r14.L$Anon3 = r5;
        r14.L$Anon4 = r4;
        r14.L$Anon5 = r1;
        r14.L$Anon6 = r9;
        r14.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01f2, code lost:
        if (r10.a((com.fossil.blesdk.obfuscated.kc4<? super com.fossil.blesdk.obfuscated.cb4>) r14) != r0) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01f5, code lost:
        r4 = r0;
        r0 = r5;
        r5 = r6;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01fa, code lost:
        r7 = r14;
        r4 = r0;
        r0 = r6;
        r14 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0206, code lost:
        if (android.text.TextUtils.isEmpty((java.lang.String) r0.element) != false) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0212, code lost:
        if (com.portfolio.platform.helper.DeviceHelper.o.e((java.lang.String) r0.element) == false) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0214, code lost:
        r14.this$Anon0.a.v().a((java.lang.String) r0.element, 0, false);
        r14.this$Anon0.a.v().b((java.lang.String) r0.element, true);
        r2 = r14.this$Anon0.a.t();
        r9 = r14.$currentUser.getUserId();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r9, "currentUser.userId");
        r6 = new com.portfolio.platform.usecase.GetSecretKeyUseCase.b((java.lang.String) r0.element, r9);
        r14.L$Anon0 = r1;
        r14.L$Anon1 = r7;
        r14.L$Anon2 = r5;
        r14.L$Anon3 = r0;
        r14.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x025e, code lost:
        if (com.fossil.blesdk.obfuscated.x52.a(r2, r6, r14) != r4) goto L_0x0261;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0260, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0261, code lost:
        r14.this$Anon0.a.s().a((java.lang.String) r0.element).a(new com.fossil.blesdk.obfuscated.pn3((java.lang.String) r0.element), new com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1.Anon4(r14, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0283, code lost:
        r14.this$Anon0.a.N.i();
        r14.this$Anon0.a.n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0298, code lost:
        if ((r14 instanceof com.fossil.blesdk.obfuscated.qo2) == false) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x029a, code lost:
        r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1 = com.portfolio.platform.uirenew.signup.SignUpPresenter.R.a();
        r2 = new java.lang.StringBuilder();
        r2.append("onLoginSuccess get all device fail ");
        r14 = (com.fossil.blesdk.obfuscated.qo2) r14;
        r2.append(r14.a());
        r0.d(r1, r2.toString());
        r13.this$Anon0.a.y().clearAllUser();
        r13.this$Anon0.a.N.i();
        r0 = r13.this$Anon0.a;
        r1 = r14.a();
        r14 = r14.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x02e2, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x02e4, code lost:
        r14 = r14.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x02e8, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x02eb, code lost:
        r14 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x02ec, code lost:
        r0.a(r1, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x02f1, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        SignUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1;
        lh4 lh4;
        Object a = oc4.a();
        List list = null;
        switch (this.label) {
            case 0:
                za4.a(obj);
                lh4 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SignUpPresenter.R.a();
                local.d(a2, "onLoginSuccess download userInfo success user " + this.$currentUser);
                if (this.$currentUser == null) {
                    gh4 b = this.this$Anon0.a.c();
                    Anon1 anon1 = new Anon1(this, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    if (kg4.a(b, anon1, this) == a) {
                        return a;
                    }
                } else {
                    en2.p.a().o();
                    this.this$Anon0.a.q().b(this.$currentUser.getUserId());
                    PortfolioApp c = PortfolioApp.W.c();
                    String userId = this.$currentUser.getUserId();
                    wd4.a((Object) userId, "currentUser.userId");
                    c.p(userId);
                    PortfolioApp c2 = PortfolioApp.W.c();
                    this.L$Anon0 = lh4;
                    this.label = 2;
                    if (c2.a((kc4<? super cb4>) this) == a) {
                        return a;
                    }
                }
                break;
            case 1:
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 2:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 3:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 4:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 5:
                Device device = (Device) this.L$Anon6;
                Iterator it = (Iterator) this.L$Anon5;
                List list2 = (List) this.L$Anon4;
                Ref$ObjectRef ref$ObjectRef = (Ref$ObjectRef) this.L$Anon3;
                List list3 = (List) this.L$Anon2;
                ro2 ro2 = (ro2) this.L$Anon1;
                lh4 lh43 = (lh4) this.L$Anon0;
                za4.a(obj);
                signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 = this;
                break;
            case 6:
                Ref$ObjectRef ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon3;
                List list4 = (List) this.L$Anon2;
                ro2 ro22 = (ro2) this.L$Anon1;
                lh4 lh44 = (lh4) this.L$Anon0;
                za4.a(obj);
                signUpPresenter$onLoginSocialSuccess$Anon1$onSuccess$Anon1 = this;
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
