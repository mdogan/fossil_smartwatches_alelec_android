package com.portfolio.platform.manager;

import com.fossil.blesdk.device.event.SynchronizationAction;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
public final class LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkStreamingManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(LinkStreamingManager linkStreamingManager, int i, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = linkStreamingManager;
        this.$action = i;
        this.$deviceId = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 = new LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(this.this$Anon0, this.$action, this.$deviceId, kc4);
        linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1.p$ = (lh4) obj;
        return linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            if (this.$action == SynchronizationAction.SET.ordinal()) {
                this.this$Anon0.d.a(new SetNotificationUseCase.b(this.$deviceId), (CoroutineUseCase.e) null);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
