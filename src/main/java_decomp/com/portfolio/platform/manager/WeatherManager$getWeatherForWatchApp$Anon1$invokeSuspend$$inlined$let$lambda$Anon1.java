package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Pair<? extends Weather, ? extends Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ WeatherLocationWrapper $location;
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ lh4 $this_launch$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager$getWeatherForWatchApp$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(WeatherLocationWrapper weatherLocationWrapper, kc4 kc4, String str, WeatherManager$getWeatherForWatchApp$Anon1 weatherManager$getWeatherForWatchApp$Anon1, lh4 lh4) {
        super(2, kc4);
        this.$location = weatherLocationWrapper;
        this.$tempUnit$inlined = str;
        this.this$Anon0 = weatherManager$getWeatherForWatchApp$Anon1;
        this.$this_launch$inlined = lh4;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$location, kc4, this.$tempUnit$inlined, this.this$Anon0, this.$this_launch$inlined);
        weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.$location.isUseCurrentLocation()) {
                WeatherManager weatherManager = this.this$Anon0.this$Anon0;
                String str = this.$tempUnit$inlined;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = weatherManager.a("weather", str, (kc4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            } else {
                WeatherManager weatherManager2 = this.this$Anon0.this$Anon0;
                double lat = this.$location.getLat();
                double lng = this.$location.getLng();
                String str2 = this.$tempUnit$inlined;
                this.L$Anon0 = lh4;
                this.label = 2;
                obj = weatherManager2.a(lat, lng, str2, (kc4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return (Pair) obj;
    }
}
