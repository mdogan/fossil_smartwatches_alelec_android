package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.NotificationInfo;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$Anon1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
public final class LightAndHapticsManager$checkAgainstAppFilter$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationInfo $info;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LightAndHapticsManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LightAndHapticsManager$checkAgainstAppFilter$Anon1(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = lightAndHapticsManager;
        this.$info = notificationInfo;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LightAndHapticsManager$checkAgainstAppFilter$Anon1 lightAndHapticsManager$checkAgainstAppFilter$Anon1 = new LightAndHapticsManager$checkAgainstAppFilter$Anon1(this.this$Anon0, this.$info, kc4);
        lightAndHapticsManager$checkAgainstAppFilter$Anon1.p$ = (lh4) obj;
        return lightAndHapticsManager$checkAgainstAppFilter$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LightAndHapticsManager$checkAgainstAppFilter$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            LightAndHapticsManager lightAndHapticsManager = this.this$Anon0;
            String packageName = this.$info.getPackageName();
            wd4.a((Object) packageName, "info.packageName");
            LightAndHaptics a = lightAndHapticsManager.a(packageName, this.$info.getBody());
            if (a != null) {
                a.setNotificationType(NotificationType.APP_FILTER);
                this.this$Anon0.a(a);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
