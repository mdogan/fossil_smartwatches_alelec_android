package com.portfolio.platform.manager;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.hn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.pj2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.enums.Unit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$Anon1", f = "WeatherManager.kt", l = {120, 128, 132}, m = "invokeSuspend")
public final class WeatherManager$getWeatherForWatchApp$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public int I$Anon1;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon10;
    @DexIgnore
    public Object L$Anon11;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForWatchApp$Anon1(WeatherManager weatherManager, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = weatherManager;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherManager$getWeatherForWatchApp$Anon1 weatherManager$getWeatherForWatchApp$Anon1 = new WeatherManager$getWeatherForWatchApp$Anon1(this.this$Anon0, kc4);
        weatherManager$getWeatherForWatchApp$Anon1.p$ = (lh4) obj;
        return weatherManager$getWeatherForWatchApp$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherForWatchApp$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x02ab  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0314  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0382  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0387  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x03a3  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x03ba  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x03ca  */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        DianaPreset dianaPreset;
        DianaPreset dianaPreset2;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        String str;
        lh4 lh4;
        MFUser mFUser;
        ArrayList arrayList;
        WeatherLocationWrapper weatherLocationWrapper;
        int i;
        Pair pair;
        Pair pair2;
        Pair pair3;
        WeatherManager$getWeatherForWatchApp$Anon1 weatherManager$getWeatherForWatchApp$Anon1;
        int i2;
        int i3;
        int i4;
        Pair pair4;
        Object obj3;
        String address;
        Weather weather;
        Pair pair5;
        Object obj4;
        Weather weather2;
        Object obj5;
        Weather weather3;
        Object obj6;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
        int i5;
        Object obj7;
        int i6;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
        String str2;
        String b;
        T t;
        Object a = oc4.a();
        int i7 = this.label;
        int i8 = 1;
        if (i7 == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            MFUser currentUser = this.this$Anon0.e().getCurrentUser();
            if (currentUser != null) {
                Unit temperatureUnit = currentUser.getTemperatureUnit();
                if (temperatureUnit != null) {
                    int i9 = hn2.a[temperatureUnit.ordinal()];
                    if (i9 == 1) {
                        str2 = Weather.TEMP_UNIT.CELSIUS.getValue();
                    } else if (i9 == 2) {
                        str2 = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                    }
                    str = str2;
                    DianaPresetRepository c = this.this$Anon0.c();
                    b = this.this$Anon0.i;
                    if (b == null) {
                        DianaPreset activePresetBySerial = c.getActivePresetBySerial(b);
                        if (activePresetBySerial != null) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String h = WeatherManager.l;
                            local.d(h, "getWeatherForWatchApp activePreset " + activePresetBySerial);
                            Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    t = null;
                                    break;
                                }
                                t = it.next();
                                if (pc4.a(wd4.a((Object) ((DianaPresetWatchAppSetting) t).getId(), (Object) "weather")).booleanValue()) {
                                    break;
                                }
                            }
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting5 = (DianaPresetWatchAppSetting) t;
                            if (dianaPresetWatchAppSetting5 != null) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String h2 = WeatherManager.l;
                                local2.d(h2, "getWeatherForWatchApp settings=" + dianaPresetWatchAppSetting5.getSettings());
                                if (!pj2.a(dianaPresetWatchAppSetting5.getSettings())) {
                                    this.this$Anon0.h = (WeatherWatchAppSetting) new Gson().a(dianaPresetWatchAppSetting5.getSettings(), WeatherWatchAppSetting.class);
                                    WeatherLocationWrapper weatherLocationWrapper2 = new WeatherLocationWrapper("", 0.0d, 0.0d, (String) null, (String) null, true, false, 94, (rd4) null);
                                    WeatherWatchAppSetting c2 = this.this$Anon0.h;
                                    if (c2 != null) {
                                        c2.getLocations().add(0, weatherLocationWrapper2);
                                        ArrayList arrayList2 = new ArrayList();
                                        WeatherWatchAppSetting c3 = this.this$Anon0.h;
                                        if (c3 != null) {
                                            for (WeatherLocationWrapper weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 : c3.getLocations()) {
                                                ArrayList arrayList3 = arrayList2;
                                                arrayList3.add(mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(weatherManager$getWeatherForWatchApp$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, (kc4) null, str, this, lh42), 3, (Object) null));
                                                arrayList2 = arrayList3;
                                            }
                                            arrayList = arrayList2;
                                            i3 = arrayList.size();
                                            weatherManager$getWeatherForWatchApp$Anon1 = this;
                                            obj2 = a;
                                            dianaPresetWatchAppSetting = dianaPresetWatchAppSetting5;
                                            weatherLocationWrapper = weatherLocationWrapper2;
                                            i2 = 0;
                                            pair3 = null;
                                            pair2 = null;
                                            pair = null;
                                            dianaPreset = activePresetBySerial;
                                            dianaPreset2 = dianaPreset;
                                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting;
                                            MFUser mFUser2 = currentUser;
                                            lh4 = lh42;
                                            mFUser = mFUser2;
                                        } else {
                                            wd4.a();
                                            throw null;
                                        }
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                }
                            }
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                str2 = Weather.TEMP_UNIT.CELSIUS.getValue();
                str = str2;
                DianaPresetRepository c4 = this.this$Anon0.c();
                b = this.this$Anon0.i;
                if (b == null) {
                }
            }
            return cb4.a;
        } else if (i7 == 1) {
            i5 = this.I$Anon1;
            int i10 = this.I$Anon0;
            pair3 = (Pair) this.L$Anon10;
            Pair pair6 = (Pair) this.L$Anon7;
            za4.a(obj);
            i4 = i10;
            dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) this.L$Anon9;
            obj6 = a;
            weatherLocationWrapper = (WeatherLocationWrapper) this.L$Anon6;
            obj3 = obj;
            arrayList = (List) this.L$Anon8;
            pair4 = (Pair) this.L$Anon11;
            weatherManager$getWeatherForWatchApp$Anon1 = this;
            String str3 = (String) this.L$Anon2;
            dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$Anon5;
            mFUser = (MFUser) this.L$Anon1;
            dianaPreset2 = (DianaPreset) this.L$Anon4;
            lh4 = (lh4) this.L$Anon0;
            dianaPreset = (DianaPreset) this.L$Anon3;
            str = str3;
            Pair pair7 = (Pair) obj3;
            Weather weather4 = (Weather) pair7.getFirst();
            address = weather4 == null ? weather4.getAddress() : null;
            if (TextUtils.isEmpty(address)) {
                address = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocation_Text__CurrentLocation);
            }
            weather = (Weather) pair7.getFirst();
            if (weather != null) {
                String a2 = us3.a(address);
                wd4.a((Object) a2, "Utils.formatLocationName(name)");
                weather.setAddress(a2);
            }
            pair2 = pair4;
            i = 1;
            pair = pair7;
            i2 = i4;
            i2 += i;
            i8 = 1;
        } else if (i7 == 2) {
            i6 = this.I$Anon1;
            i2 = this.I$Anon0;
            Pair pair8 = (Pair) this.L$Anon10;
            pair = (Pair) this.L$Anon7;
            za4.a(obj);
            obj4 = obj;
            pair5 = (Pair) this.L$Anon11;
            dianaPresetWatchAppSetting4 = (DianaPresetWatchAppSetting) this.L$Anon9;
            weatherManager$getWeatherForWatchApp$Anon1 = this;
            obj7 = a;
            weatherLocationWrapper = (WeatherLocationWrapper) this.L$Anon6;
            arrayList = (List) this.L$Anon8;
            String str4 = (String) this.L$Anon2;
            dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$Anon5;
            mFUser = (MFUser) this.L$Anon1;
            dianaPreset2 = (DianaPreset) this.L$Anon4;
            lh4 = (lh4) this.L$Anon0;
            dianaPreset = (DianaPreset) this.L$Anon3;
            str = str4;
            Pair pair9 = (Pair) obj4;
            weather2 = (Weather) pair9.getFirst();
            if (weather2 != null) {
                WeatherWatchAppSetting c5 = weatherManager$getWeatherForWatchApp$Anon1.this$Anon0.h;
                if (c5 != null) {
                    weather2.setAddress(c5.getLocations().get(i2).getName());
                }
                wd4.a();
                throw null;
            }
            pair3 = pair9;
            pair2 = pair5;
            i = 1;
            i2 += i;
            i8 = 1;
        } else if (i7 == 3) {
            i3 = this.I$Anon1;
            i2 = this.I$Anon0;
            Pair pair10 = (Pair) this.L$Anon11;
            pair = (Pair) this.L$Anon7;
            dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$Anon5;
            dianaPreset2 = (DianaPreset) this.L$Anon4;
            dianaPreset = (DianaPreset) this.L$Anon3;
            str = (String) this.L$Anon2;
            mFUser = (MFUser) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj5 = obj;
            dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) this.L$Anon9;
            obj2 = a;
            weatherLocationWrapper = (WeatherLocationWrapper) this.L$Anon6;
            pair3 = (Pair) this.L$Anon10;
            arrayList = (List) this.L$Anon8;
            weatherManager$getWeatherForWatchApp$Anon1 = this;
            Pair pair11 = (Pair) obj5;
            weather3 = (Weather) pair11.getFirst();
            if (weather3 != null) {
                WeatherWatchAppSetting c6 = weatherManager$getWeatherForWatchApp$Anon1.this$Anon0.h;
                if (c6 != null) {
                    weather3.setAddress(c6.getLocations().get(i2).getName());
                }
                wd4.a();
                throw null;
            }
            pair2 = pair11;
            i = 1;
            i2 += i;
            i8 = 1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (i2 >= i3) {
            weatherManager$getWeatherForWatchApp$Anon1.this$Anon0.a((Pair<Weather, Boolean>) pair, (Pair<Weather, Boolean>) pair3, (Pair<Weather, Boolean>) pair2);
        } else {
            if (i2 == 0) {
                int i11 = i3;
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting;
                Object obj8 = obj2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon0 = lh4;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon1 = mFUser;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon2 = str;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon7 = pair;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon8 = arrayList;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon9 = dianaPresetWatchAppSetting6;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon10 = pair3;
                pair4 = pair2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon11 = pair4;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon0 = i2;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon1 = i11;
                i4 = i2;
                weatherManager$getWeatherForWatchApp$Anon1.label = 1;
                obj3 = ((sh4) arrayList.get(i2)).a(weatherManager$getWeatherForWatchApp$Anon1);
                if (obj3 == obj8) {
                    return obj8;
                }
                obj6 = obj8;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting6;
                i5 = i11;
                Pair pair72 = (Pair) obj3;
                Weather weather42 = (Weather) pair72.getFirst();
                if (weather42 == null) {
                }
                if (TextUtils.isEmpty(address)) {
                }
                weather = (Weather) pair72.getFirst();
                if (weather != null) {
                }
                pair2 = pair4;
                i = 1;
                pair = pair72;
                i2 = i4;
                i2 += i;
                i8 = 1;
                if (i2 >= i3) {
                }
                return obj8;
            }
            if (i2 == i8) {
                int i12 = i3;
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting7 = dianaPresetWatchAppSetting;
                Object obj9 = obj2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon0 = lh4;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon1 = mFUser;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon2 = str;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon7 = pair;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon8 = arrayList;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon9 = dianaPresetWatchAppSetting7;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon10 = pair3;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon11 = pair2;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon0 = i2;
                int i13 = i12;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon1 = i13;
                pair5 = pair2;
                weatherManager$getWeatherForWatchApp$Anon1.label = 2;
                obj4 = ((sh4) arrayList.get(i2)).a(weatherManager$getWeatherForWatchApp$Anon1);
                if (obj4 == obj9) {
                    return obj9;
                }
                obj7 = obj9;
                i6 = i13;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting7;
                Pair pair92 = (Pair) obj4;
                weather2 = (Weather) pair92.getFirst();
                if (weather2 != null) {
                }
                pair3 = pair92;
                pair2 = pair5;
                i = 1;
                i2 += i;
                i8 = 1;
                if (i2 >= i3) {
                }
                return obj9;
            }
            if (i2 == 2) {
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon0 = lh4;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon1 = mFUser;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon2 = str;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon3 = dianaPreset;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon4 = dianaPreset2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon5 = dianaPresetWatchAppSetting2;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon6 = weatherLocationWrapper;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon7 = pair;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon8 = arrayList;
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting8 = dianaPresetWatchAppSetting;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon9 = dianaPresetWatchAppSetting8;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon10 = pair3;
                weatherManager$getWeatherForWatchApp$Anon1.L$Anon11 = pair2;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon0 = i2;
                weatherManager$getWeatherForWatchApp$Anon1.I$Anon1 = i3;
                weatherManager$getWeatherForWatchApp$Anon1.label = 3;
                obj5 = ((sh4) arrayList.get(i2)).a(weatherManager$getWeatherForWatchApp$Anon1);
                int i14 = i3;
                Object obj10 = obj2;
                if (obj5 == obj10) {
                    return obj10;
                }
                obj2 = obj10;
                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting8;
                i3 = i14;
                Pair pair112 = (Pair) obj5;
                weather3 = (Weather) pair112.getFirst();
                if (weather3 != null) {
                }
                pair2 = pair112;
                return obj10;
            }
            i = 1;
            i2 += i;
            i8 = 1;
            if (i2 >= i3) {
            }
        }
        weatherManager$getWeatherForWatchApp$Anon1.this$Anon0.a((Pair<Weather, Boolean>) pair, (Pair<Weather, Boolean>) pair3, (Pair<Weather, Boolean>) pair2);
        return cb4.a;
    }
}
