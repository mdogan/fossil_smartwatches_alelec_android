package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.hn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.enums.Unit;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForChanceOfRain$Anon1", f = "WeatherManager.kt", l = {156}, m = "invokeSuspend")
public final class WeatherManager$getWeatherForChanceOfRain$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForChanceOfRain$Anon1(WeatherManager weatherManager, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = weatherManager;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        WeatherManager$getWeatherForChanceOfRain$Anon1 weatherManager$getWeatherForChanceOfRain$Anon1 = new WeatherManager$getWeatherForChanceOfRain$Anon1(this.this$Anon0, kc4);
        weatherManager$getWeatherForChanceOfRain$Anon1.p$ = (lh4) obj;
        return weatherManager$getWeatherForChanceOfRain$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherForChanceOfRain$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            MFUser currentUser = this.this$Anon0.e().getCurrentUser();
            if (currentUser != null) {
                Unit temperatureUnit = currentUser.getTemperatureUnit();
                if (temperatureUnit != null) {
                    int i2 = hn2.b[temperatureUnit.ordinal()];
                    if (i2 == 1) {
                        str = Weather.TEMP_UNIT.CELSIUS.getValue();
                    } else if (i2 == 2) {
                        str = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                    }
                    WeatherManager weatherManager = this.this$Anon0;
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = currentUser;
                    this.L$Anon2 = str;
                    this.label = 1;
                    obj = weatherManager.a("chance-of-rain", str, (kc4<? super Pair<Weather, Boolean>>) this);
                    if (obj == a) {
                        return a;
                    }
                }
                str = Weather.TEMP_UNIT.CELSIUS.getValue();
                WeatherManager weatherManager2 = this.this$Anon0;
                this.L$Anon0 = lh4;
                this.L$Anon1 = currentUser;
                this.L$Anon2 = str;
                this.label = 1;
                obj = weatherManager2.a("chance-of-rain", str, (kc4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                }
            }
            return cb4.a;
        } else if (i == 1) {
            String str2 = (String) this.L$Anon2;
            MFUser mFUser = (MFUser) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        Weather weather = (Weather) pair.component1();
        boolean booleanValue = ((Boolean) pair.component2()).booleanValue();
        if (weather != null) {
            this.this$Anon0.a(weather, booleanValue);
        }
        return cb4.a;
    }
}
