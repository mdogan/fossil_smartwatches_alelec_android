package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {345, 188, 193}, m = "getWeatherBaseOnLocation")
public final class WeatherManager$getWeatherBaseOnLocation$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherBaseOnLocation$Anon1(WeatherManager weatherManager, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = weatherManager;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((String) null, (String) null, (kc4<? super Pair<Weather, Boolean>>) this);
    }
}
