package com.portfolio.platform.manager.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.bp2;
import com.fossil.blesdk.obfuscated.iz3;
import com.fossil.blesdk.obfuscated.jz3;
import com.fossil.blesdk.obfuscated.mn2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.vd0;
import com.fossil.blesdk.obfuscated.vs3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz3;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.WechatApiService;
import okhttp3.Interceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFLoginWechatManager implements yz3 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((rd4) null);
    @DexIgnore
    public WechatApiService e; // = ((WechatApiService) bp2.g.a(WechatApiService.class));
    @DexIgnore
    public boolean f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFLoginWechatManager.g;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager e;
        @DexIgnore
        public /* final */ /* synthetic */ mn2 f;

        @DexIgnore
        public b(MFLoginWechatManager mFLoginWechatManager, mn2 mn2) {
            this.e = mFLoginWechatManager;
            this.f = mn2;
        }

        @DexIgnore
        public final void run() {
            if (!this.e.b()) {
                this.f.a(500, (vd0) null, "");
            }
        }
    }

    /*
    static {
        String simpleName = MFLoginWechatManager.class.getSimpleName();
        wd4.a((Object) simpleName, "MFLoginWechatManager::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public MFLoginWechatManager() {
        bp2.g.a("https://api.weixin.qq.com/sns/");
        bp2.g.a((Interceptor) null);
    }

    @DexIgnore
    public final WechatApiService a() {
        return this.e;
    }

    @DexIgnore
    public final void a(String str) {
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "appId");
        vs3.a().a(str);
    }

    @DexIgnore
    public final void a(Intent intent) {
        wd4.b(intent, "intent");
        vs3.a().a(intent, this);
    }

    @DexIgnore
    public final void a(Activity activity, mn2 mn2) {
        wd4.b(activity, Constants.ACTIVITY);
        wd4.b(mn2, Constants.CALLBACK);
        vs3.a().a((Context) activity);
        vs3.a().a(activity.getIntent(), this);
        this.f = false;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new b(this, mn2), ScanService.BLE_SCAN_TIMEOUT);
        vs3.a().a((vs3.a) new MFLoginWechatManager$loginWithWechat$Anon2(this, handler, mn2));
    }

    @DexIgnore
    public void a(iz3 iz3) {
        wd4.b(iz3, "baseReq");
        vs3.a().a(iz3);
    }

    @DexIgnore
    public void a(jz3 jz3) {
        wd4.b(jz3, "baseResp");
        vs3.a().a(jz3);
    }
}
