package com.portfolio.platform.manager.login;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mn2;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.vd0;
import com.fossil.blesdk.obfuscated.vs3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFLoginWechatManager$loginWithWechat$Anon2 implements vs3.a {
    @DexIgnore
    public /* final */ /* synthetic */ MFLoginWechatManager a;
    @DexIgnore
    public /* final */ /* synthetic */ Handler b;
    @DexIgnore
    public /* final */ /* synthetic */ mn2 c;

    @DexIgnore
    public MFLoginWechatManager$loginWithWechat$Anon2(MFLoginWechatManager mFLoginWechatManager, Handler handler, mn2 mn2) {
        this.a = mFLoginWechatManager;
        this.b = handler;
        this.c = mn2;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "authToken");
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MFLoginWechatManager.h.a();
        local.d(a2, "Wechat onAuthSuccess: " + str);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat step 1: Login using wechat success");
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1(this, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public void b() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthAppNotInstalled");
        this.c.a(600, (vd0) null, "");
    }

    @DexIgnore
    public void c() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthCancel");
        this.c.a(2, (vd0) null, "");
    }

    @DexIgnore
    public void a() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthFailed");
        this.c.a(600, (vd0) null, "");
    }
}
