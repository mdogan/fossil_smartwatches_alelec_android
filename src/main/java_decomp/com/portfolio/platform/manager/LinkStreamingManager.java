package com.portfolio.platform.manager;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.enumerate.RingPhoneState;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.gn2;
import com.fossil.blesdk.obfuscated.hj2;
import com.fossil.blesdk.obfuscated.ij2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lj2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mj2;
import com.fossil.blesdk.obfuscated.px3;
import com.fossil.blesdk.obfuscated.qp2;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sp2;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.ServiceStatus;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkStreamingManager implements sp2 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<qp2> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ HybridPresetRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;
    @DexIgnore
    public /* final */ SetNotificationUseCase d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LinkStreamingManager.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager g;

        @DexIgnore
        public b(LinkStreamingManager linkStreamingManager, String str, int i) {
            wd4.b(str, "serial");
            this.g = linkStreamingManager;
            this.e = str;
            this.f = i;
        }

        @DexIgnore
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = LinkStreamingManager.f.a();
            local.d(a, "Inside SteamingAction .run - eventId=" + this.f + ", serial=" + this.e);
            if (TextUtils.isEmpty(this.e)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a2 = LinkStreamingManager.f.a();
                local2.e(a2, "Inside .onStreamingEvent of " + this.e + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.g.b.getActivePresetBySerial(this.e);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (wd4.a((Object) next.getAppId(), (Object) MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.f).getValue())) {
                        int i = this.f;
                        if (i == DeviceEventId.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a3 = LinkStreamingManager.f.a();
                            local3.d(a3, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            LinkStreamingManager linkStreamingManager = this.g;
                            String str = this.e;
                            String settings = next.getSettings();
                            if (settings != null) {
                                linkStreamingManager.a(str, settings);
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else if (i == DeviceEventId.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.A.a(PortfolioApp.W.c(), bundle, this.g);
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String canonicalName = LinkStreamingManager.class.getCanonicalName();
        if (canonicalName != null) {
            wd4.a((Object) canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            e = canonicalName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public LinkStreamingManager(HybridPresetRepository hybridPresetRepository, wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, fn2 fn2, AlarmsRepository alarmsRepository, SetNotificationUseCase setNotificationUseCase) {
        wd4.b(hybridPresetRepository, "mPresetRepository");
        wd4.b(wy2, "mGetApp");
        wd4.b(qx2, "mGetAllContactGroups");
        wd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wd4.b(notificationsRepository, "mNotificationsRepository");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(fn2, "mSharePref");
        wd4.b(alarmsRepository, "mAlarmsRepository");
        wd4.b(setNotificationUseCase, "mSetNotificationUseCase");
        this.b = hybridPresetRepository;
        this.c = alarmsRepository;
        this.d = setNotificationUseCase;
        PortfolioApp.W.b((Object) this);
    }

    @DexIgnore
    public final void d(String str, int i) {
        RingPhoneState ringPhoneState;
        FLogger.INSTANCE.getLocal().d(e, "startRingMyPhone");
        if (i == RingPhoneState.ON.ordinal()) {
            gn2.o.a().a(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000);
            ringPhoneState = RingPhoneState.ON;
        } else {
            gn2.o.a().b();
            ringPhoneState = RingPhoneState.OFF;
        }
        PortfolioApp.W.c().a((DeviceAppResponse) new RingPhoneComplicationAppInfo(ringPhoneState), str);
    }

    @DexIgnore
    @px3
    public final void onDeviceAppEvent(hj2 hj2) {
        wd4.b(hj2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onDeviceAppEvent - appId=" + hj2.a() + ", serial=" + hj2.c());
        int a2 = hj2.a();
        if (a2 == DeviceEventId.WEATHER_COMPLICATION.ordinal()) {
            String d2 = AnalyticsHelper.f.d("weather");
            vl2 b2 = AnalyticsHelper.f.b(a(d2));
            b2.d();
            AnalyticsHelper.f.a(d2, b2);
            WeatherManager a3 = WeatherManager.n.a();
            String c2 = hj2.c();
            wd4.a((Object) c2, "event.serial");
            a3.b(c2);
        } else if (a2 == DeviceEventId.CHANCE_OF_RAIN_COMPLICATION.ordinal()) {
            vl2 b3 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("chance-of-rain")));
            b3.d();
            AnalyticsHelper.f.a("chance-of-rain", b3);
            WeatherManager a4 = WeatherManager.n.a();
            String c3 = hj2.c();
            wd4.a((Object) c3, "event.serial");
            a4.a(c3);
        } else if (a2 == DeviceEventId.RING_PHONE.ordinal()) {
            int i = hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c4 = hj2.c();
            wd4.a((Object) c4, "event.serial");
            d(c4, i);
        } else if (a2 == DeviceEventId.WEATHER_WATCH_APP.ordinal()) {
            WeatherManager a5 = WeatherManager.n.a();
            String c5 = hj2.c();
            wd4.a((Object) c5, "event.serial");
            a5.c(c5);
        } else if (a2 == DeviceEventId.NOTIFICATION_FILTER_SYNC.ordinal()) {
            int i2 = hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c6 = hj2.c();
            wd4.a((Object) c6, "event.serial");
            c(c6, i2);
        } else if (a2 == DeviceEventId.ALARM_SYNC.ordinal()) {
            int i3 = hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c7 = hj2.c();
            wd4.a((Object) c7, "event.serial");
            a(c7, i3);
        } else if (a2 == DeviceEventId.DEVICE_CONFIG_SYNC.ordinal()) {
            int i4 = hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c8 = hj2.c();
            wd4.a((Object) c8, "event.serial");
            b(c8, i4);
        } else if (a2 == DeviceEventId.MUSIC_CONTROL.ordinal()) {
            NotifyMusicEventResponse.MusicMediaAction musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.values()[hj2.b().getInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.NONE.ordinal())];
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local2.d(str2, "Inside " + e + ".onMusicActionEvent - musicActionEvent=" + musicMediaAction + ", serial=" + hj2.c());
            ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.W.c())).a(musicMediaAction);
        } else if (a2 == DeviceEventId.APP_NOTIFICATION_CONTROL.ordinal()) {
            FLogger.INSTANCE.getLocal().d(e, "on app notification control received");
            a(hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1));
        } else if (a2 == DeviceEventId.COMMUTE_TIME_WATCH_APP.ordinal()) {
            vl2 b4 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("commute-time")));
            b4.d();
            AnalyticsHelper.f.a("commute-time", b4);
            int i5 = hj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String string = hj2.b().getString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, "");
            WatchAppCommuteTimeManager a6 = WatchAppCommuteTimeManager.s.a();
            String c9 = hj2.c();
            wd4.a((Object) c9, "event.serial");
            wd4.a((Object) string, ShareConstants.DESTINATION);
            a6.a(c9, string, i5);
        } else {
            String c10 = hj2.c();
            wd4.a((Object) c10, "event.serial");
            new Thread(new b(this, c10, hj2.a())).start();
        }
    }

    @DexIgnore
    @px3
    public final void onMicroAppCancelEvent(ij2 ij2) {
        wd4.b(ij2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onMicroAppCancelEvent - event=" + ij2.a() + ", serial=" + ij2.b());
        a((List<? extends qp2>) this.a);
    }

    @DexIgnore
    @px3
    public final void onMusicActionEvent(lj2 lj2) {
        wd4.b(lj2, Constants.EVENT);
        FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Inside ");
        sb.append(e);
        sb.append(".onMusicActionEvent - musicActionEvent=");
        lj2.a();
        throw null;
    }

    @DexIgnore
    @px3
    public final void onStreamingEvent(mj2 mj2) {
        wd4.b(mj2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onStreamingEvent - event=" + mj2.a() + ", serial=" + mj2.b());
        String b2 = mj2.b();
        wd4.a((Object) b2, "event.serial");
        new Thread(new b(this, b2, mj2.a())).start();
    }

    @DexIgnore
    public final void a() {
        PortfolioApp.W.c(this);
    }

    @DexIgnore
    public final ri4 c(String str, int i) {
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(this, i, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(int i) {
        if (DeviceHelper.o.l()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "process call event: action = " + i);
            if (i == AppNotificationControlAction.REJECT_PHONE_CALL.ordinal()) {
                dn2.c.a().b();
            } else if (i == AppNotificationControlAction.ACCEPT_PHONE_CALL.ordinal()) {
                dn2.c.a().a();
            } else {
                if (i == AppNotificationControlAction.DISMISS_NOTIFICATION.ordinal()) {
                }
            }
        }
    }

    @DexIgnore
    public void b(qp2 qp2) {
        wd4.b(qp2, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(e, "removeObject");
        if (!this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "removeObject mServiceListSize=" + this.a.size());
            Iterator<qp2> it = this.a.iterator();
            while (it.hasNext()) {
                qp2 next = it.next();
                wd4.a((Object) next, "autoStopBaseService");
                if (next.c() == qp2.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = e;
                    local2.d(str2, "removeObject actionId " + qp2.c());
                    this.a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, ServiceStatus serviceStatus) {
        wd4.b(serviceStatus, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "onStatusChanged action " + i);
    }

    @DexIgnore
    public void a(qp2 qp2) {
        wd4.b(qp2, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(e, "addObject");
        if (!this.a.contains(qp2)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "addObject service actionId " + qp2.c());
            this.a.add(qp2);
        }
    }

    @DexIgnore
    public final ri4 b(String str, int i) {
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1(i, str, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(List<? extends qp2> list) {
        if (list != null && !this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "stopServices serviceListSize=" + this.a.size());
            for (qp2 b2 : list) {
                b2.b();
            }
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wd4.b(str, "deviceId");
        wd4.b(str2, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = e;
        local.d(str3, "startRingMyPhone, setting=" + str2);
        try {
            if (TextUtils.isEmpty(str2)) {
                List<Ringtone> c2 = AppHelper.f.c();
                Ringtone ringtone = c2.get(0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = e;
                local2.d(str4, "ringtones=" + c2 + ", defaultRingtone=" + ringtone);
                gn2.o.a().b(ringtone);
                return;
            }
            String component1 = ((Ringtone) new Gson().a(str2, Ringtone.class)).component1();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = e;
            local3.d(str5, "MicroAppAction.run - ringtone " + str2);
            PortfolioApp.W.c().a((DeviceAppResponse) new RingMyPhoneMicroAppResponse(), str);
            ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$startRingMyPhone$Anon1(component1, (kc4) null), 3, (Object) null);
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AppUtil", "Error when read asset file - ex=" + e2);
        }
    }

    @DexIgnore
    public final ri4 a(String str, int i) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processAlarmSynchronizeEvent$Anon1(this, i, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final String a(String str) {
        be4 be4 = be4.a;
        Object[] objArr = {str};
        String format = String.format("update_%s", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
