package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$Anon1", f = "WeatherManager.kt", l = {221}, m = "invokeSuspend")
public final class WeatherManager$getAddressBaseOnLocation$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<yz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ double $lat;
    @DexIgnore
    public /* final */ /* synthetic */ double $lng;
    @DexIgnore
    public /* final */ /* synthetic */ String $type;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getAddressBaseOnLocation$response$Anon1(WeatherManager weatherManager, double d, double d2, String str, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = weatherManager;
        this.$lat = d;
        this.$lng = d2;
        this.$type = str;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new WeatherManager$getAddressBaseOnLocation$response$Anon1(this.this$Anon0, this.$lat, this.$lng, this.$type, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WeatherManager$getAddressBaseOnLocation$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            GoogleApiService d = this.this$Anon0.d();
            StringBuilder sb = new StringBuilder();
            sb.append(this.$lat);
            sb.append(',');
            sb.append(this.$lng);
            String sb2 = sb.toString();
            String str = this.$type;
            this.label = 1;
            obj = d.getAddressWithType(sb2, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
