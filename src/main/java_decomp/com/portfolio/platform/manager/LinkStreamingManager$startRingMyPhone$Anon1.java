package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$Anon1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
public final class LinkStreamingManager$startRingMyPhone$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $ringtoneName;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$startRingMyPhone$Anon1(String str, kc4 kc4) {
        super(2, kc4);
        this.$ringtoneName = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LinkStreamingManager$startRingMyPhone$Anon1 linkStreamingManager$startRingMyPhone$Anon1 = new LinkStreamingManager$startRingMyPhone$Anon1(this.$ringtoneName, kc4);
        linkStreamingManager$startRingMyPhone$Anon1.p$ = (lh4) obj;
        return linkStreamingManager$startRingMyPhone$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkStreamingManager$startRingMyPhone$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            List<Ringtone> c = AppHelper.f.c();
            if (!c.isEmpty()) {
                for (Ringtone next : c) {
                    if (wd4.a((Object) next.getRingtoneName(), (Object) this.$ringtoneName)) {
                        gn2.o.a().b(next);
                        return cb4.a;
                    }
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
