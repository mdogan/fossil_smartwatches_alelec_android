package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $cancellableContinuation;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0167Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $goalTrackingList;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public Object L$Anon2;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3$Anon1$Anon1$Anon1")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3$Anon1$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0168Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<ApiResponse<GoalDailySummary>>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public int label;
                @DexIgnore
                public lh4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0167Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0168Anon1(C0167Anon1 anon1, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, kc4 kc4) {
                    super(2, kc4);
                    this.this$Anon0 = anon1;
                    this.$startDate = ref$ObjectRef;
                    this.$endDate = ref$ObjectRef2;
                }

                @DexIgnore
                public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                    wd4.b(kc4, "completion");
                    C0168Anon1 anon1 = new C0168Anon1(this.this$Anon0, this.$startDate, this.$endDate, kc4);
                    anon1.p$ = (lh4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0168Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = oc4.a();
                    int i = this.label;
                    if (i == 0) {
                        za4.a(obj);
                        this.L$Anon0 = this.p$;
                        this.label = 1;
                        obj = this.this$Anon0.this$Anon0.a.this$Anon0.o.loadSummaries((Date) this.$startDate.element, (Date) this.$endDate.element, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        lh4 lh4 = (lh4) this.L$Anon0;
                        za4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0167Anon1(Anon1 anon1, List list, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
                this.$goalTrackingList = list;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0167Anon1 anon1 = new C0167Anon1(this.this$Anon0, this.$goalTrackingList, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0167Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                    ref$ObjectRef.element = ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                    Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                    ref$ObjectRef2.element = ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                    for (GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                        if (goalTrackingData.getDate().getTime() < ((Date) ref$ObjectRef.element).getTime()) {
                            ref$ObjectRef.element = goalTrackingData.getDate();
                        }
                        if (goalTrackingData.getDate().getTime() > ((Date) ref$ObjectRef2.element).getTime()) {
                            ref$ObjectRef2.element = goalTrackingData.getDate();
                        }
                    }
                    gh4 b = zh4.b();
                    C0168Anon1 anon1 = new C0168Anon1(this, ref$ObjectRef, ref$ObjectRef2, (kc4) null);
                    this.L$Anon0 = lh4;
                    this.L$Anon1 = ref$ObjectRef;
                    this.L$Anon2 = ref$ObjectRef2;
                    this.label = 1;
                    if (kg4.a(b, anon1, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    Ref$ObjectRef ref$ObjectRef3 = (Ref$ObjectRef) this.L$Anon2;
                    Ref$ObjectRef ref$ObjectRef4 = (Ref$ObjectRef) this.L$Anon1;
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.this$Anon0.a.$cancellableContinuation.isActive()) {
                    pg4 pg4 = this.this$Anon0.a.$cancellableContinuation;
                    Boolean a2 = pc4.a(true);
                    Result.a aVar = Result.Companion;
                    pg4.resumeWith(Result.m3constructorimpl(a2));
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon1(PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3) {
            this.a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3;
        }

        @DexIgnore
        public void onFail(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
            if (this.a.$cancellableContinuation.isActive()) {
                pg4 pg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(false));
            }
        }

        @DexIgnore
        public void onSuccess(List<GoalTrackingData> list) {
            wd4.b(list, "goalTrackingList");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onSuccess, go to next");
            ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new C0167Anon1(this, list, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3(pg4 pg4, kc4 kc4, PushPendingDataWorker pushPendingDataWorker) {
        super(2, kc4);
        this.$cancellableContinuation = pg4;
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3 = new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3(this.$cancellableContinuation, kc4, this.this$Anon0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3.p$ = (lh4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            GoalTrackingRepository b = this.this$Anon0.o;
            Anon1 anon1 = new Anon1(this);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (b.pushPendingGoalTrackingDataList(anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
