package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Date;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $cancellableContinuation;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements ActivitiesRepository.PushPendingActivitiesCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0164Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0164Anon1(Anon1 anon1, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
                this.$startDate = ref$ObjectRef;
                this.$endDate = ref$ObjectRef2;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0164Anon1 anon1 = new C0164Anon1(this.this$Anon0, this.$startDate, this.$endDate, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0164Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    SummariesRepository g = this.this$Anon0.a.this$Anon0.l;
                    Date date = ((DateTime) this.$startDate.element).toLocalDateTime().toDate();
                    wd4.a((Object) date, "startDate.toLocalDateTime().toDate()");
                    Date date2 = ((DateTime) this.$endDate.element).toLocalDateTime().toDate();
                    wd4.a((Object) date2, "endDate.toLocalDateTime().toDate()");
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    if (g.loadSummaries(date, date2, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon1(PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public void onFail(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onFail, go to next, errorCode = " + i);
            if (this.a.$cancellableContinuation.isActive()) {
                pg4 pg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(false));
            }
        }

        @DexIgnore
        public void onSuccess(List<ActivitySample> list) {
            wd4.b(list, "activityList");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onSuccess, go to next");
            if (!list.isEmpty()) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = list.get(0).getStartTime();
                Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                ref$ObjectRef2.element = list.get(0).getEndTime();
                for (ActivitySample next : list) {
                    if (next.getStartTime().getMillis() < ((DateTime) ref$ObjectRef.element).getMillis()) {
                        ref$ObjectRef.element = next.getStartTime();
                    }
                    if (next.getEndTime().getMillis() < ((DateTime) ref$ObjectRef2.element).getMillis()) {
                        ref$ObjectRef2.element = next.getEndTime();
                    }
                }
                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new C0164Anon1(this, ref$ObjectRef, ref$ObjectRef2, (kc4) null), 3, (Object) null);
            }
            if (this.a.$cancellableContinuation.isActive()) {
                pg4 pg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(true));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1(pg4 pg4, kc4 kc4, PushPendingDataWorker pushPendingDataWorker) {
        super(2, kc4);
        this.$cancellableContinuation = pg4;
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$cancellableContinuation, kc4, this.this$Anon0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (lh4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ActivitiesRepository a2 = this.this$Anon0.k;
            Anon1 anon1 = new Anon1(this);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (a2.pushPendingActivities(anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
