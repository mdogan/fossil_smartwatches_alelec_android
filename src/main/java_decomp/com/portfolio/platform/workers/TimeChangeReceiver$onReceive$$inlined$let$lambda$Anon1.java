package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ TimeChangeReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1(kc4 kc4, TimeChangeReceiver timeChangeReceiver) {
        super(2, kc4);
        this.this$Anon0 = timeChangeReceiver;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 = new TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1(kc4, this.this$Anon0);
        timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            String e = PortfolioApp.W.c().e();
            if (e.length() == 0) {
                return cb4.a;
            }
            long g = this.this$Anon0.b().g(e);
            long e2 = this.this$Anon0.b().e(e);
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + g + "; " + "lastReminderSyncTimeSuccess = " + e2);
            if (e2 > currentTimeMillis || g > currentTimeMillis) {
                fn2 b = this.this$Anon0.b();
                long j = currentTimeMillis - ((long) CommuteTimeService.y);
                b.c(j, e);
                this.this$Anon0.b().a(j, e);
                return cb4.a;
            }
            this.this$Anon0.a().e();
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
