package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeChangeReceiver extends BroadcastReceiver {
    @DexIgnore
    public AlarmHelper a;
    @DexIgnore
    public fn2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.a;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final fn2 b() {
        fn2 fn2 = this.b;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        if (intent != null) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            if (wd4.a((Object) action, (Object) "android.intent.action.TIME_SET") || wd4.a((Object) action, (Object) "android.intent.action.TIMEZONE_CHANGED") || wd4.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1((kc4) null, this), 3, (Object) null);
            }
        }
    }
}
