package com.portfolio.platform.workers;

import androidx.work.ListenableWorker;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {73}, m = "doWork")
public final class PushPendingDataWorker$doWork$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$doWork$Anon1(PushPendingDataWorker pushPendingDataWorker, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((kc4<? super ListenableWorker.a>) this);
    }
}
