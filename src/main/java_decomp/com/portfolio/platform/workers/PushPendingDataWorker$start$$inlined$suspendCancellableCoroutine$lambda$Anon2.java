package com.portfolio.platform.workers;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $cancellableContinuation;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements SleepSessionsRepository.PushPendingSleepSessionsCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0165Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $sleepSessionList;
            @DexIgnore
            public int I$Anon0;
            @DexIgnore
            public int I$Anon1;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public Object L$Anon2;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1$Anon1")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2$Anon1$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0166Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<yz1>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $end;
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $start;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public int label;
                @DexIgnore
                public lh4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0165Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0166Anon1(C0165Anon1 anon1, Calendar calendar, Calendar calendar2, kc4 kc4) {
                    super(2, kc4);
                    this.this$Anon0 = anon1;
                    this.$start = calendar;
                    this.$end = calendar2;
                }

                @DexIgnore
                public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                    wd4.b(kc4, "completion");
                    C0166Anon1 anon1 = new C0166Anon1(this.this$Anon0, this.$start, this.$end, kc4);
                    anon1.p$ = (lh4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0166Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = oc4.a();
                    int i = this.label;
                    if (i == 0) {
                        za4.a(obj);
                        lh4 lh4 = this.p$;
                        SleepSummariesRepository f = this.this$Anon0.this$Anon0.a.this$Anon0.n;
                        Calendar calendar = this.$start;
                        wd4.a((Object) calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        Date time = calendar.getTime();
                        wd4.a((Object) time, "start.time");
                        Calendar calendar2 = this.$end;
                        wd4.a((Object) calendar2, "end");
                        Date time2 = calendar2.getTime();
                        wd4.a((Object) time2, "end.time");
                        this.L$Anon0 = lh4;
                        this.label = 1;
                        obj = f.fetchSleepSummaries(time, time2, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        lh4 lh42 = (lh4) this.L$Anon0;
                        za4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0165Anon1(Anon1 anon1, List list, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
                this.$sleepSessionList = list;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0165Anon1 anon1 = new C0165Anon1(this.this$Anon0, this.$sleepSessionList, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0165Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    if (!this.$sleepSessionList.isEmpty()) {
                        int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                        int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                        for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                            if (mFSleepSession.getRealStartTime() < realStartTime) {
                                realStartTime = mFSleepSession.getRealStartTime();
                            }
                            if (mFSleepSession.getRealEndTime() > realEndTime) {
                                realEndTime = mFSleepSession.getRealEndTime();
                            }
                        }
                        Calendar instance = Calendar.getInstance();
                        wd4.a((Object) instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                        instance.setTimeInMillis((long) realStartTime);
                        Calendar instance2 = Calendar.getInstance();
                        wd4.a((Object) instance2, "end");
                        instance2.setTimeInMillis((long) realEndTime);
                        gh4 b = zh4.b();
                        C0166Anon1 anon1 = new C0166Anon1(this, instance, instance2, (kc4) null);
                        this.L$Anon0 = lh4;
                        this.I$Anon0 = realStartTime;
                        this.I$Anon1 = realEndTime;
                        this.L$Anon1 = instance;
                        this.L$Anon2 = instance2;
                        this.label = 1;
                        if (kg4.a(b, anon1, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    Calendar calendar = (Calendar) this.L$Anon2;
                    Calendar calendar2 = (Calendar) this.L$Anon1;
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.this$Anon0.a.$cancellableContinuation.isActive()) {
                    pg4 pg4 = this.this$Anon0.a.$cancellableContinuation;
                    Boolean a2 = pc4.a(true);
                    Result.a aVar = Result.Companion;
                    pg4.resumeWith(Result.m3constructorimpl(a2));
                }
                return cb4.a;
            }
        }

        @DexIgnore
        public Anon1(PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2) {
            this.a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2;
        }

        @DexIgnore
        public void onFail(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onFail, go to next, errorCode = " + i);
            if (this.a.$cancellableContinuation.isActive()) {
                pg4 pg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(true));
            }
        }

        @DexIgnore
        public void onSuccess(List<MFSleepSession> list) {
            wd4.b(list, "sleepSessionList");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onSuccess, go to next");
            ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new C0165Anon1(this, list, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2(pg4 pg4, kc4 kc4, PushPendingDataWorker pushPendingDataWorker) {
        super(2, kc4);
        this.$cancellableContinuation = pg4;
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2 = new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2(this.$cancellableContinuation, kc4, this.this$Anon0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2.p$ = (lh4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.m.pushPendingSleepSessions(new Anon1(this));
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
