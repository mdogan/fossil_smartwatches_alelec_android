package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.ActivitiesRepository;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$Anon5", f = "PushPendingDataWorker.kt", l = {207, 208, 210, 211, 213, 214, 215}, m = "invokeSuspend")
public final class PushPendingDataWorker$start$Anon5 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$Anon5(PushPendingDataWorker pushPendingDataWorker, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = pushPendingDataWorker;
        this.$startDate = ref$ObjectRef;
        this.$endDate = ref$ObjectRef2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        PushPendingDataWorker$start$Anon5 pushPendingDataWorker$start$Anon5 = new PushPendingDataWorker$start$Anon5(this.this$Anon0, this.$startDate, this.$endDate, kc4);
        pushPendingDataWorker$start$Anon5.p$ = (lh4) obj;
        return pushPendingDataWorker$start$Anon5;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$Anon5) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008a, code lost:
        r13 = r12.this$Anon0.l;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r4, "startDate.toDate()");
        r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r5, "endDate.toDate()");
        r12.L$Anon0 = r1;
        r12.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b3, code lost:
        if (r13.loadSummaries(r4, r5, r12) != r0) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b5, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b6, code lost:
        r4 = r12.this$Anon0.m;
        r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r5, "startDate.toDate()");
        r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r6, "endDate.toDate()");
        r12.L$Anon0 = r1;
        r12.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e5, code lost:
        if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00e7, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00e8, code lost:
        r13 = r12.this$Anon0.n;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r4, "startDate.toDate()");
        r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r5, "endDate.toDate()");
        r12.L$Anon0 = r1;
        r12.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0111, code lost:
        if (r13.fetchSleepSummaries(r4, r5, r12) != r0) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0113, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0114, code lost:
        r4 = r12.this$Anon0.p;
        r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r5, "startDate.toDate()");
        r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r6, "endDate.toDate()");
        r12.L$Anon0 = r1;
        r12.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0143, code lost:
        if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0145, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0146, code lost:
        r13 = r12.this$Anon0.q;
        r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r4, "startDate.toDate()");
        r3 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r3, "endDate.toDate()");
        r12.L$Anon0 = r1;
        r12.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x016f, code lost:
        if (r13.loadSummaries(r4, r3, r12) != r0) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0171, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0172, code lost:
        r13 = r12.this$Anon0.l;
        r12.L$Anon0 = r1;
        r12.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0181, code lost:
        if (r13.fetchActivityStatistic(r12) != r0) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0183, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0186, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        switch (this.label) {
            case 0:
                za4.a(obj);
                lh4 lh42 = this.p$;
                ActivitiesRepository a2 = this.this$Anon0.k;
                Date date = ((DateTime) this.$startDate.element).toDate();
                wd4.a((Object) date, "startDate.toDate()");
                Date date2 = ((DateTime) this.$endDate.element).toDate();
                wd4.a((Object) date2, "endDate.toDate()");
                this.L$Anon0 = lh42;
                this.label = 1;
                if (ActivitiesRepository.fetchActivitySamples$default(a2, date, date2, 0, 0, this, 12, (Object) null) != a) {
                    lh4 = lh42;
                    break;
                } else {
                    return a;
                }
            case 1:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 2:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 3:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 4:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 5:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 6:
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 7:
                lh4 lh43 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
