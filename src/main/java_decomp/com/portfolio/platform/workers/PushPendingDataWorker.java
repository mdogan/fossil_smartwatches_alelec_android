package com.portfolio.platform.workers;

import android.content.Context;
import androidx.work.CoroutineWorker;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.NetworkType;
import androidx.work.WorkerParameters;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fj;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kj;
import com.fossil.blesdk.obfuscated.lj;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.mu3;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.qg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.uc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.blesdk.obfuscated.zi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import javax.inject.Provider;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker extends CoroutineWorker {
    @DexIgnore
    public static /* final */ a y; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ActivitiesRepository k;
    @DexIgnore
    public /* final */ SummariesRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ SleepSummariesRepository n;
    @DexIgnore
    public /* final */ GoalTrackingRepository o;
    @DexIgnore
    public /* final */ HeartRateSampleRepository p;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository q;
    @DexIgnore
    public /* final */ FitnessDataRepository r;
    @DexIgnore
    public /* final */ AlarmsRepository s;
    @DexIgnore
    public /* final */ fn2 t;
    @DexIgnore
    public /* final */ DianaPresetRepository u;
    @DexIgnore
    public /* final */ HybridPresetRepository v;
    @DexIgnore
    public /* final */ ThirdPartyRepository w;
    @DexIgnore
    public /* final */ PortfolioApp x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a() {
            fj.a aVar = new fj.a(PushPendingDataWorker.class);
            zi.a aVar2 = new zi.a();
            aVar2.a(NetworkType.CONNECTED);
            zi a = aVar2.a();
            wd4.a((Object) a, "Constraints.Builder().se\u2026rkType.CONNECTED).build()");
            aVar.a(a);
            lj a2 = aVar.a();
            wd4.a((Object) a2, "uploadBuilder.build()");
            fj fjVar = (fj) a2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "startScheduleUploadPendingData() - id = " + fjVar.a());
            kj.a().a("PushPendingDataWorker", ExistingWorkPolicy.KEEP, fjVar);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements mu3<PushPendingDataWorker> {
        @DexIgnore
        public /* final */ Provider<ActivitiesRepository> a;
        @DexIgnore
        public /* final */ Provider<SummariesRepository> b;
        @DexIgnore
        public /* final */ Provider<SleepSessionsRepository> c;
        @DexIgnore
        public /* final */ Provider<SleepSummariesRepository> d;
        @DexIgnore
        public /* final */ Provider<GoalTrackingRepository> e;
        @DexIgnore
        public /* final */ Provider<HeartRateSampleRepository> f;
        @DexIgnore
        public /* final */ Provider<HeartRateSummaryRepository> g;
        @DexIgnore
        public /* final */ Provider<FitnessDataRepository> h;
        @DexIgnore
        public /* final */ Provider<AlarmsRepository> i;
        @DexIgnore
        public /* final */ Provider<fn2> j;
        @DexIgnore
        public /* final */ Provider<DianaPresetRepository> k;
        @DexIgnore
        public /* final */ Provider<HybridPresetRepository> l;
        @DexIgnore
        public /* final */ Provider<ThirdPartyRepository> m;
        @DexIgnore
        public /* final */ Provider<PortfolioApp> n;

        @DexIgnore
        public b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<fn2> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<PortfolioApp> provider14) {
            wd4.b(provider, "mActivitiesRepository");
            wd4.b(provider2, "mSummariesRepository");
            wd4.b(provider3, "mSleepSessionRepository");
            wd4.b(provider4, "mSleepSummariesRepository");
            wd4.b(provider5, "mGoalTrackingRepository");
            wd4.b(provider6, "mHeartRateSampleRepository");
            wd4.b(provider7, "mHeartRateSummaryRepository");
            wd4.b(provider8, "mFitnessDataRepository");
            wd4.b(provider9, "mAlarmsRepository");
            wd4.b(provider10, "mSharedPreferencesManager");
            wd4.b(provider11, "mDianaPresetRepository");
            wd4.b(provider12, "mHybridPresetRepository");
            wd4.b(provider13, "mThirdPartyRepository");
            wd4.b(provider14, "mApp");
            this.a = provider;
            this.b = provider2;
            this.c = provider3;
            this.d = provider4;
            this.e = provider5;
            this.f = provider6;
            this.g = provider7;
            this.h = provider8;
            this.i = provider9;
            this.j = provider10;
            this.k = provider11;
            this.l = provider12;
            this.m = provider13;
            this.n = provider14;
        }

        @DexIgnore
        public PushPendingDataWorker a(Context context, WorkerParameters workerParameters) {
            wd4.b(context, "context");
            wd4.b(workerParameters, "parameterName");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "Factory - create()");
            ActivitiesRepository activitiesRepository = this.a.get();
            wd4.a((Object) activitiesRepository, "mActivitiesRepository.get()");
            SummariesRepository summariesRepository = this.b.get();
            wd4.a((Object) summariesRepository, "mSummariesRepository.get()");
            SleepSessionsRepository sleepSessionsRepository = this.c.get();
            wd4.a((Object) sleepSessionsRepository, "mSleepSessionRepository.get()");
            SleepSummariesRepository sleepSummariesRepository = this.d.get();
            wd4.a((Object) sleepSummariesRepository, "mSleepSummariesRepository.get()");
            GoalTrackingRepository goalTrackingRepository = this.e.get();
            wd4.a((Object) goalTrackingRepository, "mGoalTrackingRepository.get()");
            HeartRateSampleRepository heartRateSampleRepository = this.f.get();
            wd4.a((Object) heartRateSampleRepository, "mHeartRateSampleRepository.get()");
            HeartRateSummaryRepository heartRateSummaryRepository = this.g.get();
            wd4.a((Object) heartRateSummaryRepository, "mHeartRateSummaryRepository.get()");
            FitnessDataRepository fitnessDataRepository = this.h.get();
            wd4.a((Object) fitnessDataRepository, "mFitnessDataRepository.get()");
            AlarmsRepository alarmsRepository = this.i.get();
            wd4.a((Object) alarmsRepository, "mAlarmsRepository.get()");
            fn2 fn2 = this.j.get();
            wd4.a((Object) fn2, "mSharedPreferencesManager.get()");
            DianaPresetRepository dianaPresetRepository = this.k.get();
            wd4.a((Object) dianaPresetRepository, "mDianaPresetRepository.get()");
            HybridPresetRepository hybridPresetRepository = this.l.get();
            wd4.a((Object) hybridPresetRepository, "mHybridPresetRepository.get()");
            ThirdPartyRepository thirdPartyRepository = this.m.get();
            wd4.a((Object) thirdPartyRepository, "mThirdPartyRepository.get()");
            PortfolioApp portfolioApp = this.n.get();
            wd4.a((Object) portfolioApp, "mApp.get()");
            return new PushPendingDataWorker(workerParameters, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, goalTrackingRepository, heartRateSampleRepository, heartRateSummaryRepository, fitnessDataRepository, alarmsRepository, fn2, dianaPresetRepository, hybridPresetRepository, thirdPartyRepository, portfolioApp);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ThirdPartyRepository.PushPendingThirdPartyDataCallback {
        @DexIgnore
        public /* final */ /* synthetic */ pg4 a;

        @DexIgnore
        public c(pg4 pg4) {
            this.a = pg4;
        }

        @DexIgnore
        public void onComplete() {
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "ThirdPartyRepository.pushPendingData - Complete");
            if (this.a.isActive()) {
                pg4 pg4 = this.a;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(true));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public PushPendingDataWorker(WorkerParameters workerParameters, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, AlarmsRepository alarmsRepository, fn2 fn2, DianaPresetRepository dianaPresetRepository, HybridPresetRepository hybridPresetRepository, ThirdPartyRepository thirdPartyRepository, PortfolioApp portfolioApp) {
        super(r0, r1);
        WorkerParameters workerParameters2 = workerParameters;
        ActivitiesRepository activitiesRepository2 = activitiesRepository;
        SummariesRepository summariesRepository2 = summariesRepository;
        SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
        SleepSummariesRepository sleepSummariesRepository2 = sleepSummariesRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        HeartRateSampleRepository heartRateSampleRepository2 = heartRateSampleRepository;
        HeartRateSummaryRepository heartRateSummaryRepository2 = heartRateSummaryRepository;
        FitnessDataRepository fitnessDataRepository2 = fitnessDataRepository;
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        fn2 fn22 = fn2;
        DianaPresetRepository dianaPresetRepository2 = dianaPresetRepository;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        ThirdPartyRepository thirdPartyRepository2 = thirdPartyRepository;
        wd4.b(workerParameters2, "mWorkerParameters");
        wd4.b(activitiesRepository2, "mActivitiesRepository");
        wd4.b(summariesRepository2, "mSummariesRepository");
        wd4.b(sleepSessionsRepository2, "mSleepSessionRepository");
        wd4.b(sleepSummariesRepository2, "mSleepSummariesRepository");
        wd4.b(goalTrackingRepository2, "mGoalTrackingRepository");
        wd4.b(heartRateSampleRepository2, "mHeartRateSampleRepository");
        wd4.b(heartRateSummaryRepository2, "mHeartRateSummaryRepository");
        wd4.b(fitnessDataRepository2, "mFitnessDataRepository");
        wd4.b(alarmsRepository2, "mAlarmsRepository");
        wd4.b(fn22, "mSharedPreferencesManager");
        wd4.b(dianaPresetRepository2, "mDianaPresetRepository");
        wd4.b(hybridPresetRepository2, "mHybridPresetRepository");
        wd4.b(thirdPartyRepository2, "mThirdPartyRepository");
        wd4.b(portfolioApp, "mApp");
        Context applicationContext = portfolioApp.getApplicationContext();
        wd4.a((Object) applicationContext, "mApp.applicationContext");
        this.k = activitiesRepository2;
        this.l = summariesRepository2;
        this.m = sleepSessionsRepository2;
        this.n = sleepSummariesRepository2;
        this.o = goalTrackingRepository2;
        this.p = heartRateSampleRepository2;
        this.q = heartRateSummaryRepository2;
        this.r = fitnessDataRepository2;
        this.s = alarmsRepository2;
        this.t = fn22;
        this.u = dianaPresetRepository2;
        this.v = hybridPresetRepository2;
        this.w = thirdPartyRepository2;
        this.x = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(kc4<? super ListenableWorker.a> kc4) {
        PushPendingDataWorker$doWork$Anon1 pushPendingDataWorker$doWork$Anon1;
        int i;
        if (kc4 instanceof PushPendingDataWorker$doWork$Anon1) {
            pushPendingDataWorker$doWork$Anon1 = (PushPendingDataWorker$doWork$Anon1) kc4;
            int i2 = pushPendingDataWorker$doWork$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                pushPendingDataWorker$doWork$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = pushPendingDataWorker$doWork$Anon1.result;
                Object a2 = oc4.a();
                i = pushPendingDataWorker$doWork$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("PushPendingDataWorker", "doWork() - id = " + c());
                    pushPendingDataWorker$doWork$Anon1.L$Anon0 = this;
                    pushPendingDataWorker$doWork$Anon1.label = 1;
                    if (b((kc4<? super cb4>) pushPendingDataWorker$doWork$Anon1) == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    PushPendingDataWorker pushPendingDataWorker = (PushPendingDataWorker) pushPendingDataWorker$doWork$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ListenableWorker.a c2 = ListenableWorker.a.c();
                wd4.a((Object) c2, "Result.success()");
                return c2;
            }
        }
        pushPendingDataWorker$doWork$Anon1 = new PushPendingDataWorker$doWork$Anon1(this, kc4);
        Object obj2 = pushPendingDataWorker$doWork$Anon1.result;
        Object a22 = oc4.a();
        i = pushPendingDataWorker$doWork$Anon1.label;
        if (i != 0) {
        }
        ListenableWorker.a c22 = ListenableWorker.a.c();
        wd4.a((Object) c22, "Result.success()");
        return c22;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ec, code lost:
        r2.L$Anon0 = r8;
        r2.Z$Anon0 = r4;
        r2.label = 2;
        r1 = new com.fossil.blesdk.obfuscated.qg4(kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt.a(r2), 1);
        com.fossil.blesdk.obfuscated.ri4 unused = com.fossil.blesdk.obfuscated.mg4.b(com.fossil.blesdk.obfuscated.mh4.a(com.fossil.blesdk.obfuscated.zh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon2(r1, (com.fossil.blesdk.obfuscated.kc4) null, r8), 3, (java.lang.Object) null);
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0118, code lost:
        if (r1 != com.fossil.blesdk.obfuscated.oc4.a()) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x011a, code lost:
        com.fossil.blesdk.obfuscated.uc4.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x011d, code lost:
        if (r1 != r3) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x011f, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0120, code lost:
        r2.L$Anon0 = r8;
        r2.Z$Anon0 = r4;
        r2.label = 3;
        r1 = new com.fossil.blesdk.obfuscated.qg4(kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt.a(r2), 1);
        com.fossil.blesdk.obfuscated.ri4 unused = com.fossil.blesdk.obfuscated.mg4.b(com.fossil.blesdk.obfuscated.mh4.a(com.fossil.blesdk.obfuscated.zh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon3(r1, (com.fossil.blesdk.obfuscated.kc4) null, r8), 3, (java.lang.Object) null);
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x014c, code lost:
        if (r1 != com.fossil.blesdk.obfuscated.oc4.a()) goto L_0x0151;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x014e, code lost:
        com.fossil.blesdk.obfuscated.uc4.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0151, code lost:
        if (r1 != r3) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0153, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0154, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData");
        r1 = r8.r;
        r2.L$Anon0 = r8;
        r2.Z$Anon0 = r4;
        r2.label = 4;
        r1 = r1.pushPendingFitnessData(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x016c, code lost:
        if (r1 != r3) goto L_0x016f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x016e, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x016f, code lost:
        r1 = (com.fossil.blesdk.obfuscated.ro2) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0173, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.so2) == false) goto L_0x0207;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0175, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData - Success");
        r1 = (java.util.List) ((com.fossil.blesdk.obfuscated.so2) r1).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0188, code lost:
        if (r1 == null) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x018f, code lost:
        if ((!r1.isEmpty()) == false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0191, code lost:
        r6 = new kotlin.jvm.internal.Ref$ObjectRef();
        r6.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.get(0)).getStartTimeTZ();
        r10 = new kotlin.jvm.internal.Ref$ObjectRef();
        r10.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.get(0)).getEndTimeTZ();
        r1 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01bc, code lost:
        if (r1.hasNext() == false) goto L_0x01f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01be, code lost:
        r9 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01d2, code lost:
        if (r9.getStartLongTime() >= ((org.joda.time.DateTime) r6.element).getMillis()) goto L_0x01da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01d4, code lost:
        r6.element = r9.getStartTimeTZ();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01e8, code lost:
        if (r9.getEndLongTime() <= ((org.joda.time.DateTime) r10.element).getMillis()) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01ea, code lost:
        r10.element = r9.getEndTimeTZ();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01f1, code lost:
        com.fossil.blesdk.obfuscated.ri4 unused = com.fossil.blesdk.obfuscated.mg4.b(com.fossil.blesdk.obfuscated.mh4.a(com.fossil.blesdk.obfuscated.zh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.PushPendingDataWorker$start$Anon5(r8, r6, r10, (com.fossil.blesdk.obfuscated.kc4) null), 3, (java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0209, code lost:
        if ((r1 instanceof com.fossil.blesdk.obfuscated.qo2) == false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x020b, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData - Failed ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0216, code lost:
        r1 = r8.s;
        r2.L$Anon0 = r8;
        r2.Z$Anon0 = r4;
        r2.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0223, code lost:
        if (r1.executePendingRequest(r2) != r3) goto L_0x0226;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0225, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0226, code lost:
        r5 = r4;
        r6 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0228, code lost:
        r4 = r6.x.e();
        r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0232, code lost:
        if (r1 != null) goto L_0x0235;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x023d, code lost:
        if (com.fossil.blesdk.obfuscated.nu3.a[r1.ordinal()] == 1) goto L_0x0251;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x023f, code lost:
        r1 = r6.v;
        r2.L$Anon0 = r6;
        r2.Z$Anon0 = r5;
        r2.L$Anon1 = r4;
        r2.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x024e, code lost:
        if (r1.executePendingRequest(r4, r2) != r3) goto L_0x0263;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0250, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0251, code lost:
        r1 = r6.u;
        r2.L$Anon0 = r6;
        r2.Z$Anon0 = r5;
        r2.L$Anon1 = r4;
        r2.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0260, code lost:
        if (r1.executePendingRequest(r4, r2) != r3) goto L_0x0263;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0262, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0263, code lost:
        r2.L$Anon0 = r6;
        r2.Z$Anon0 = r5;
        r2.L$Anon1 = r4;
        r2.label = 8;
        r1 = new com.fossil.blesdk.obfuscated.qg4(kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt.a(r2), 1);
        h(r6).pushPendingData(new com.portfolio.platform.workers.PushPendingDataWorker.c(r1));
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x028a, code lost:
        if (r1 != com.fossil.blesdk.obfuscated.oc4.a()) goto L_0x028f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x028c, code lost:
        com.fossil.blesdk.obfuscated.uc4.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x028f, code lost:
        if (r1 != r3) goto L_0x0292;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0291, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0294, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object b(kc4<? super cb4> kc4) {
        PushPendingDataWorker$start$Anon1 pushPendingDataWorker$start$Anon1;
        PushPendingDataWorker pushPendingDataWorker;
        boolean z;
        PushPendingDataWorker pushPendingDataWorker2;
        boolean z2;
        kc4<? super cb4> kc42 = kc4;
        if (kc42 instanceof PushPendingDataWorker$start$Anon1) {
            pushPendingDataWorker$start$Anon1 = (PushPendingDataWorker$start$Anon1) kc42;
            int i = pushPendingDataWorker$start$Anon1.label;
            if ((i & Integer.MIN_VALUE) != 0) {
                pushPendingDataWorker$start$Anon1.label = i - Integer.MIN_VALUE;
                Object obj = pushPendingDataWorker$start$Anon1.result;
                Object a2 = oc4.a();
                switch (pushPendingDataWorker$start$Anon1.label) {
                    case 0:
                        za4.a(obj);
                        boolean m2 = this.t.m(this.x.h());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("PushPendingDataWorker", "start push pending data, isMigrationComplete " + m2);
                        if (m2) {
                            pushPendingDataWorker$start$Anon1.L$Anon0 = this;
                            pushPendingDataWorker$start$Anon1.Z$Anon0 = m2;
                            pushPendingDataWorker$start$Anon1.label = 1;
                            qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(pushPendingDataWorker$start$Anon1), 1);
                            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1(qg4, (kc4) null, this), 3, (Object) null);
                            Object e = qg4.e();
                            if (e == oc4.a()) {
                                uc4.c(pushPendingDataWorker$start$Anon1);
                            }
                            if (e != a2) {
                                pushPendingDataWorker2 = this;
                                z2 = m2;
                                break;
                            } else {
                                return a2;
                            }
                        } else {
                            return cb4.a;
                        }
                    case 1:
                        z2 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        pushPendingDataWorker2 = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    case 2:
                        z2 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        pushPendingDataWorker2 = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    case 3:
                        z2 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        pushPendingDataWorker2 = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    case 4:
                        z2 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        pushPendingDataWorker2 = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    case 5:
                        boolean z3 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        za4.a(obj);
                        pushPendingDataWorker = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        z = z3;
                        break;
                    case 6:
                    case 7:
                        String str = (String) pushPendingDataWorker$start$Anon1.L$Anon1;
                        z = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        pushPendingDataWorker = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    case 8:
                        String str2 = (String) pushPendingDataWorker$start$Anon1.L$Anon1;
                        boolean z4 = pushPendingDataWorker$start$Anon1.Z$Anon0;
                        PushPendingDataWorker pushPendingDataWorker3 = (PushPendingDataWorker) pushPendingDataWorker$start$Anon1.L$Anon0;
                        za4.a(obj);
                        break;
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }
        pushPendingDataWorker$start$Anon1 = new PushPendingDataWorker$start$Anon1(this, kc42);
        Object obj2 = pushPendingDataWorker$start$Anon1.result;
        Object a22 = oc4.a();
        switch (pushPendingDataWorker$start$Anon1.label) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
            case 7:
                break;
            case 8:
                break;
        }
    }
}
