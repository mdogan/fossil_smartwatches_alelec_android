package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.bm2;
import com.fossil.blesdk.obfuscated.cm2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.jm2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Calendar;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SmsMmsReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public DianaNotificationComponent a;
    @DexIgnore
    public fn2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = SmsMmsReceiver.class.getSimpleName();
        wd4.a((Object) simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        wd4.b(intent, "intent");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        StringBuilder sb = new StringBuilder();
        sb.append("SmsMmsReceiver : ");
        String action = intent.getAction();
        if (action != null) {
            sb.append(action);
            local.d(str, sb.toString());
            fn2 fn2 = this.b;
            if (fn2 == null) {
                wd4.d("mSharePref");
                throw null;
            } else if (!fn2.M()) {
                String str2 = "";
                if (TextUtils.isEmpty(intent.getAction()) || !wd4.a((Object) intent.getAction(), (Object) "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
                    try {
                        SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        if (messagesFromIntent != null) {
                            if (!(messagesFromIntent.length == 0)) {
                                for (SmsMessage smsMessage : messagesFromIntent) {
                                    wd4.a((Object) smsMessage, "message");
                                    String messageBody = smsMessage.getMessageBody();
                                    String originatingAddress = smsMessage.getOriginatingAddress();
                                    ref$ObjectRef.element = new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, str2);
                                    if (!TextUtils.isEmpty(messageBody) && !TextUtils.isEmpty(originatingAddress)) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
                    }
                } else {
                    try {
                        if (intent.hasExtra("data")) {
                            cm2 a2 = new jm2(intent.getByteArrayExtra("data")).a();
                            if (a2 != null && a2.a() != null) {
                                bm2 a3 = a2.a();
                                wd4.a((Object) a3, "pdu.from");
                                String a4 = a3.a();
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + a4);
                                ref$ObjectRef.element = new NotificationInfo(NotificationSource.TEXT, a4, str2, str2);
                            }
                        }
                    } catch (Exception e2) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
                    }
                }
                if (((NotificationInfo) ref$ObjectRef.element) != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onReceive() - SMSMMS - sender info ");
                    String senderInfo = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
                    if (senderInfo != null) {
                        sb2.append(senderInfo);
                        local2.d(str3, sb2.toString());
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                            us3.b(((NotificationInfo) ref$ObjectRef.element).getSenderInfo());
                            fn2 fn22 = this.b;
                            if (fn22 == null) {
                                wd4.d("mSharePref");
                                throw null;
                            } else if (fn22.D()) {
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
                            } else {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str4 = c;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("onReceive() - SMS-MMS - sender info ");
                                String senderInfo2 = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
                                if (senderInfo2 != null) {
                                    sb3.append(senderInfo2);
                                    local3.d(str4, sb3.toString());
                                    ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new SmsMmsReceiver$onReceive$Anon1(ref$ObjectRef, (kc4) null), 3, (Object) null);
                                    return;
                                }
                                wd4.a();
                                throw null;
                            }
                        } else {
                            if (((NotificationInfo) ref$ObjectRef.element).getSenderInfo() != null) {
                                str2 = ((NotificationInfo) ref$ObjectRef.element).getSenderInfo();
                            }
                            DianaNotificationComponent dianaNotificationComponent = this.a;
                            if (dianaNotificationComponent == null) {
                                wd4.d("mDianaNotificationComponent");
                                throw null;
                            } else if (str2 != null) {
                                String body = ((NotificationInfo) ref$ObjectRef.element).getBody();
                                wd4.a((Object) body, "notificationInfo.body");
                                Calendar instance = Calendar.getInstance();
                                wd4.a((Object) instance, "Calendar.getInstance()");
                                dianaNotificationComponent.a(str2, body, instance.getTimeInMillis());
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
