package com.portfolio.platform.receiver;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.workers.PushPendingDataWorker;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$Anon2", f = "NetworkChangedReceiver.kt", l = {}, m = "invokeSuspend")
public final class NetworkChangedReceiver$onReceive$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isInternetAvailable;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkChangedReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkChangedReceiver$onReceive$Anon2(NetworkChangedReceiver networkChangedReceiver, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = networkChangedReceiver;
        this.$isInternetAvailable = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NetworkChangedReceiver$onReceive$Anon2 networkChangedReceiver$onReceive$Anon2 = new NetworkChangedReceiver$onReceive$Anon2(this.this$Anon0, this.$isInternetAvailable, kc4);
        networkChangedReceiver$onReceive$Anon2.p$ = (lh4) obj;
        return networkChangedReceiver$onReceive$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NetworkChangedReceiver$onReceive$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            if (this.this$Anon0.a().getCurrentUser() != null && !this.$isInternetAvailable) {
                PushPendingDataWorker.y.a();
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
