package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.qs3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RestartServiceReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        wd4.a((Object) simpleName, "RestartServiceReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        wd4.b(intent, "intent");
        FLogger.INSTANCE.getLocal().e(a, "Inside ---onReceive RestartServiceReceiver");
        Context context2 = context;
        qs3.a.a(qs3.a, context2, MFDeviceService.class, (String) null, 4, (Object) null);
        qs3.a.a(qs3.a, context2, ButtonService.class, (String) null, 4, (Object) null);
    }
}
