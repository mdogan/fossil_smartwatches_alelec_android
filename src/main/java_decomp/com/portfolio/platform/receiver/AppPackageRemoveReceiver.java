package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public NotificationsRepository a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        wd4.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        wd4.d("notificationsRepository");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0040, code lost:
        if (r7 != null) goto L_0x0045;
     */
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String str;
        wd4.b(context, "context");
        wd4.b(intent, "intent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "onReceive " + intent.getAction());
        if (wd4.a((Object) "android.intent.action.PACKAGE_FULLY_REMOVED", (Object) intent.getAction())) {
            Uri data = intent.getData();
            if (data != null) {
                str = data.getSchemeSpecificPart();
            }
            str = "";
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.d(str3, "package removed " + str);
            ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new AppPackageRemoveReceiver$onReceive$Anon1(this, str, (kc4) null), 3, (Object) null);
        }
    }
}
