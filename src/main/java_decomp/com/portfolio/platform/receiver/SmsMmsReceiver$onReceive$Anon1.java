package com.portfolio.platform.receiver;

import android.provider.Telephony;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.util.NotificationAppHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$Anon1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
public final class SmsMmsReceiver$onReceive$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $notificationInfo;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SmsMmsReceiver$onReceive$Anon1(Ref$ObjectRef ref$ObjectRef, kc4 kc4) {
        super(2, kc4);
        this.$notificationInfo = ref$ObjectRef;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SmsMmsReceiver$onReceive$Anon1 smsMmsReceiver$onReceive$Anon1 = new SmsMmsReceiver$onReceive$Anon1(this.$notificationInfo, kc4);
        smsMmsReceiver$onReceive$Anon1.p$ = (lh4) obj;
        return smsMmsReceiver$onReceive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SmsMmsReceiver$onReceive$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c());
            if (defaultSmsPackage == null || NotificationAppHelper.b.a(defaultSmsPackage)) {
                FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive() - SMS app filter is assigned - ignore");
            } else {
                LightAndHapticsManager.i.a().a((NotificationInfo) this.$notificationInfo.element);
                FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive) - SMS app filter is not assigned - add to Queue");
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
