package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmReceiver extends BroadcastReceiver {
    @DexIgnore
    public UserRepository a;
    @DexIgnore
    public fn2 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public AlarmHelper d;
    @DexIgnore
    public AlarmsRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.d;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        wd4.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final fn2 d() {
        fn2 fn2 = this.b;
        if (fn2 != null) {
            return fn2;
        }
        wd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        wd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        PortfolioApp.W.c().g().a(this);
        FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        if (intent != null) {
            int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmReceiver", "onReceive - action=" + intExtra);
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new AlarmReceiver$onReceive$Anon1(this, intExtra, intent, context, (kc4) null), 3, (Object) null);
        }
    }
}
