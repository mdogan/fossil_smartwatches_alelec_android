package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.blesdk.obfuscated.pc1;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.rc1;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends rc1 {
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ pc1 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(pg4 pg4, pc1 pc1, LocationRequest locationRequest) {
        this.$continuation = pg4;
        this.$this_requestLocationUpdates$inlined = pc1;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    public void onLocationResult(LocationResult locationResult) {
        wd4.b(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        Location H = locationResult.H();
        if (H != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationResult lastLocation=" + H);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "onLocationResult lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.a((rc1) this);
        if (this.$continuation.isActive()) {
            pg4 pg4 = this.$continuation;
            Result.a aVar = Result.Companion;
            pg4.resumeWith(Result.m3constructorimpl(H));
        }
    }
}
