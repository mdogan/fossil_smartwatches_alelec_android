package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc1;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.android.gms.location.LocationRequest;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$Anon2", f = "LocationSource.kt", l = {130}, m = "invokeSuspend")
public final class LocationSource$getLocationFromFuseLocationManager$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super Location>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pc1 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$Anon2(LocationSource locationSource, pc1 pc1, LocationRequest locationRequest, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = locationSource;
        this.$fusedLocationClient = pc1;
        this.$locationRequest = locationRequest;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LocationSource$getLocationFromFuseLocationManager$Anon2 locationSource$getLocationFromFuseLocationManager$Anon2 = new LocationSource$getLocationFromFuseLocationManager$Anon2(this.this$Anon0, this.$fusedLocationClient, this.$locationRequest, kc4);
        locationSource$getLocationFromFuseLocationManager$Anon2.p$ = (lh4) obj;
        return locationSource$getLocationFromFuseLocationManager$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LocationSource$getLocationFromFuseLocationManager$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            LocationSource locationSource = this.this$Anon0;
            pc1 pc1 = this.$fusedLocationClient;
            wd4.a((Object) pc1, "fusedLocationClient");
            LocationRequest locationRequest = this.$locationRequest;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = locationSource.requestLocationUpdates(pc1, locationRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
