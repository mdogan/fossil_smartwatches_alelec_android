package com.portfolio.platform.data;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum NotificationType {
    CONTACT(R.string.notifications_section_contacts),
    CODE_WORD(R.string.notifications_section_code_words),
    CALL(R.string.activity_contact_detail_calls),
    SMS(R.string.activity_contact_detail_texts),
    EMAIL(R.string.email_from_everyone),
    APP_FILTER(R.string.notifications_section_apps),
    FITNESS_GOAL_ACHIEVED(R.string.notifications_section_fitness),
    APP_MODE(R.string.notifications_app_mode),
    OTHER(R.string.notification_other),
    CONTACT_EMPTY(R.string.notifications_contact_empty_message),
    APP_EMPTY(R.string.notifications_app_empty_message);
    
    @DexIgnore
    public /* final */ int sectionTitleResId;

    @DexIgnore
    NotificationType(int i) {
        this.sectionTitleResId = i;
    }

    @DexIgnore
    public static NotificationType find(String str) {
        if (!TextUtils.isEmpty(str)) {
            return valueOf(str);
        }
        return null;
    }

    @DexIgnore
    public int getSectionTitleResId() {
        return this.sectionTitleResId;
    }

    @DexIgnore
    public String getSectionTitleString() {
        return tm2.a((Context) PortfolioApp.R, this.sectionTitleResId);
    }
}
