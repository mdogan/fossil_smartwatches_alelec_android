package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SecondTimezoneSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    @g02("timezoneCityCode")
    public String cityCode;
    @DexIgnore
    @g02("timezoneId")
    public String timeZoneId;
    @DexIgnore
    @g02("timezoneCityName")
    public String timeZoneName;
    @DexIgnore
    @g02("timezoneOffset")
    public int timezoneOffset;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SecondTimezoneSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public SecondTimezoneSetting createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new SecondTimezoneSetting(parcel);
        }

        @DexIgnore
        public SecondTimezoneSetting[] newArray(int i) {
            return new SecondTimezoneSetting[i];
        }
    }

    @DexIgnore
    public SecondTimezoneSetting() {
        this((String) null, (String) null, 0, (String) null, 15, (rd4) null);
    }

    @DexIgnore
    public SecondTimezoneSetting(String str, String str2, int i, String str3) {
        wd4.b(str, "timeZoneName");
        wd4.b(str2, "timeZoneId");
        wd4.b(str3, "cityCode");
        this.timeZoneName = str;
        this.timeZoneId = str2;
        this.timezoneOffset = i;
        this.cityCode = str3;
    }

    @DexIgnore
    public static /* synthetic */ SecondTimezoneSetting copy$default(SecondTimezoneSetting secondTimezoneSetting, String str, String str2, int i, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = secondTimezoneSetting.timeZoneName;
        }
        if ((i2 & 2) != 0) {
            str2 = secondTimezoneSetting.timeZoneId;
        }
        if ((i2 & 4) != 0) {
            i = secondTimezoneSetting.timezoneOffset;
        }
        if ((i2 & 8) != 0) {
            str3 = secondTimezoneSetting.cityCode;
        }
        return secondTimezoneSetting.copy(str, str2, i, str3);
    }

    @DexIgnore
    public final SecondTimezoneSetting clone() {
        String str = this.timeZoneName;
        String str2 = this.timeZoneId;
        return new SecondTimezoneSetting(str, str2, ConversionUtils.getTimezoneRawOffsetById(str2), this.cityCode);
    }

    @DexIgnore
    public final String component1() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final String component2() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component4() {
        return this.cityCode;
    }

    @DexIgnore
    public final SecondTimezoneSetting copy(String str, String str2, int i, String str3) {
        wd4.b(str, "timeZoneName");
        wd4.b(str2, "timeZoneId");
        wd4.b(str3, "cityCode");
        return new SecondTimezoneSetting(str, str2, i, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SecondTimezoneSetting) {
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) obj;
                if (wd4.a((Object) this.timeZoneName, (Object) secondTimezoneSetting.timeZoneName) && wd4.a((Object) this.timeZoneId, (Object) secondTimezoneSetting.timeZoneId)) {
                    if (!(this.timezoneOffset == secondTimezoneSetting.timezoneOffset) || !wd4.a((Object) this.cityCode, (Object) secondTimezoneSetting.cityCode)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCityCode() {
        return this.cityCode;
    }

    @DexIgnore
    public final String getTimeZoneId() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final String getTimeZoneName() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.timeZoneName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.timeZoneId;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.timezoneOffset) * 31;
        String str3 = this.cityCode;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setCityCode(String str) {
        wd4.b(str, "<set-?>");
        this.cityCode = str;
    }

    @DexIgnore
    public final void setTimeZoneId(String str) {
        wd4.b(str, "<set-?>");
        this.timeZoneId = str;
    }

    @DexIgnore
    public final void setTimeZoneName(String str) {
        wd4.b(str, "<set-?>");
        this.timeZoneName = str;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "SecondTimezoneSetting(timeZoneName=" + this.timeZoneName + ", timeZoneId=" + this.timeZoneId + ", timezoneOffset=" + this.timezoneOffset + ", cityCode=" + this.cityCode + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.timeZoneName);
        parcel.writeString(this.timeZoneId);
        parcel.writeInt(this.timezoneOffset);
        parcel.writeString(this.cityCode);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SecondTimezoneSetting(String str, String str2, int i, String str3, int i2, rd4 rd4) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? "" : str3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SecondTimezoneSetting(Parcel parcel) {
        this(r0, r2, r3, r5 == null ? "" : r5);
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        readString2 = readString2 == null ? "" : readString2;
        int readInt = parcel.readInt();
        String readString3 = parcel.readString();
    }
}
