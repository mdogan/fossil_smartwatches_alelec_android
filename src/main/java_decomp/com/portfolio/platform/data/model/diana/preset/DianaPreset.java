package com.portfolio.platform.data.model.diana.preset;

import android.content.Context;
import com.fossil.blesdk.obfuscated.e02;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    @g02("complications")
    @f02(DianaPresetComplicationSettingSerializer.class)
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @g02("createdAt")
    public String createdAt; // = "";
    @DexIgnore
    @g02("id")
    public String id;
    @DexIgnore
    @g02("isActive")
    public boolean isActive;
    @DexIgnore
    @g02("name")
    @e02
    public String name;
    @DexIgnore
    @jk2
    public int pinType; // = 1;
    @DexIgnore
    @g02("serialNumber")
    public String serialNumber;
    @DexIgnore
    @g02("updatedAt")
    public String updatedAt; // = "";
    @DexIgnore
    @g02("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @g02("buttons")
    @f02(DianaPresetWatchAppSettingSerializer.class)
    @e02
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final DianaPreset cloneFrom(DianaPreset dianaPreset) {
            wd4.b(dianaPreset, "preset");
            String t = sk2.t(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            String serialNumber = dianaPreset.getSerialNumber();
            String a = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_CreateNew_NewPreset_Title__NewPreset);
            wd4.a((Object) a, "LanguageHelper.getString\u2026wPreset_Title__NewPreset)");
            DianaPreset dianaPreset2 = new DianaPreset(uuid, serialNumber, a, false, tj2.b(dianaPreset.getComplications()), tj2.c(dianaPreset.getWatchapps()), dianaPreset.getWatchFaceId());
            dianaPreset2.setCreatedAt(t);
            dianaPreset2.setUpdatedAt(t);
            return dianaPreset2;
        }

        @DexIgnore
        public final DianaPreset cloneFromDefaultPreset(DianaRecommendPreset dianaRecommendPreset) {
            wd4.b(dianaRecommendPreset, "recommendPreset");
            String t = sk2.t(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            DianaPreset dianaPreset = new DianaPreset(uuid, dianaRecommendPreset.getSerialNumber(), dianaRecommendPreset.getName(), dianaRecommendPreset.isDefault(), tj2.b(dianaRecommendPreset.getComplications()), tj2.c(dianaRecommendPreset.getWatchapps()), dianaRecommendPreset.getWatchFaceId());
            dianaPreset.setCreatedAt(t);
            dianaPreset.setUpdatedAt(t);
            for (DianaPresetComplicationSetting localUpdateAt : dianaPreset.getComplications()) {
                wd4.a((Object) t, "timestamp");
                localUpdateAt.setLocalUpdateAt(t);
            }
            for (DianaPresetWatchAppSetting localUpdateAt2 : dianaPreset.getWatchapps()) {
                wd4.a((Object) t, "timestamp");
                localUpdateAt2.setLocalUpdateAt(t);
            }
            return dianaPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public DianaPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        wd4.b(str, "id");
        wd4.b(str2, "serialNumber");
        wd4.b(str3, "name");
        wd4.b(arrayList, "complications");
        wd4.b(arrayList2, "watchapps");
        wd4.b(str4, "watchFaceId");
        this.id = str;
        this.serialNumber = str2;
        this.name = str3;
        this.isActive = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPreset copy$default(DianaPreset dianaPreset, String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = dianaPreset.serialNumber;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            str3 = dianaPreset.name;
        }
        String str6 = str3;
        if ((i & 8) != 0) {
            z = dianaPreset.isActive;
        }
        boolean z2 = z;
        if ((i & 16) != 0) {
            arrayList = dianaPreset.complications;
        }
        ArrayList<DianaPresetComplicationSetting> arrayList3 = arrayList;
        if ((i & 32) != 0) {
            arrayList2 = dianaPreset.watchapps;
        }
        ArrayList<DianaPresetWatchAppSetting> arrayList4 = arrayList2;
        if ((i & 64) != 0) {
            str4 = dianaPreset.watchFaceId;
        }
        return dianaPreset.copy(str, str5, str6, z2, arrayList3, arrayList4, str4);
    }

    @DexIgnore
    public final DianaPreset clone() {
        DianaPreset dianaPreset = new DianaPreset(this.id, this.serialNumber, this.name, this.isActive, tj2.b(this.complications), tj2.c(this.watchapps), this.watchFaceId);
        dianaPreset.updatedAt = this.updatedAt;
        dianaPreset.createdAt = this.createdAt;
        return dianaPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isActive;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final DianaPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        wd4.b(str, "id");
        wd4.b(str2, "serialNumber");
        wd4.b(str3, "name");
        wd4.b(arrayList, "complications");
        wd4.b(arrayList2, "watchapps");
        String str5 = str4;
        wd4.b(str5, "watchFaceId");
        return new DianaPreset(str, str2, str3, z, arrayList, arrayList2, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaPreset) {
                DianaPreset dianaPreset = (DianaPreset) obj;
                if (wd4.a((Object) this.id, (Object) dianaPreset.id) && wd4.a((Object) this.serialNumber, (Object) dianaPreset.serialNumber) && wd4.a((Object) this.name, (Object) dianaPreset.name)) {
                    if (!(this.isActive == dianaPreset.isActive) || !wd4.a((Object) this.complications, (Object) dianaPreset.complications) || !wd4.a((Object) this.watchapps, (Object) dianaPreset.watchapps) || !wd4.a((Object) this.watchFaceId, (Object) dianaPreset.watchFaceId)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.serialNumber;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = (i2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = (hashCode4 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        String str4 = this.watchFaceId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        wd4.b(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wd4.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wd4.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        wd4.b(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        wd4.b(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.id + ", serialNumber=" + this.serialNumber + ", name=" + this.name + ", isActive=" + this.isActive + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ")";
    }
}
