package com.portfolio.platform.data.model.diana.preset;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingStyle {
    @DexIgnore
    @g02("data")
    public Data data;
    @DexIgnore
    @g02("id")
    public String id;
    @DexIgnore
    @g02("metadata")
    public MetaData metadata;

    @DexIgnore
    public RingStyle(String str, Data data2, MetaData metaData) {
        wd4.b(str, "id");
        wd4.b(data2, "data");
        this.id = str;
        this.data = data2;
        this.metadata = metaData;
    }

    @DexIgnore
    public static /* synthetic */ RingStyle copy$default(RingStyle ringStyle, String str, Data data2, MetaData metaData, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringStyle.id;
        }
        if ((i & 2) != 0) {
            data2 = ringStyle.data;
        }
        if ((i & 4) != 0) {
            metaData = ringStyle.metadata;
        }
        return ringStyle.copy(str, data2, metaData);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component3() {
        return this.metadata;
    }

    @DexIgnore
    public final RingStyle copy(String str, Data data2, MetaData metaData) {
        wd4.b(str, "id");
        wd4.b(data2, "data");
        return new RingStyle(str, data2, metaData);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RingStyle)) {
            return false;
        }
        RingStyle ringStyle = (RingStyle) obj;
        return wd4.a((Object) this.id, (Object) ringStyle.id) && wd4.a((Object) this.data, (Object) ringStyle.data) && wd4.a((Object) this.metadata, (Object) ringStyle.metadata);
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Data data2 = this.data;
        int hashCode2 = (hashCode + (data2 != null ? data2.hashCode() : 0)) * 31;
        MetaData metaData = this.metadata;
        if (metaData != null) {
            i = metaData.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setData(Data data2) {
        wd4.b(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetadata(MetaData metaData) {
        this.metadata = metaData;
    }

    @DexIgnore
    public String toString() {
        return "RingStyle(id=" + this.id + ", data=" + this.data + ", metadata=" + this.metadata + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RingStyle(String str, Data data2, MetaData metaData, int i, rd4 rd4) {
        this(str, data2, (i & 4) != 0 ? null : metaData);
    }
}
