package com.portfolio.platform.data.model.sleep;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDayData {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public String date;
    @DexIgnore
    public volatile boolean isDataUpdated;
    @DexIgnore
    public int mDayAwake;
    @DexIgnore
    public int mDayLight;
    @DexIgnore
    public int mDayRestful;
    @DexIgnore
    public ArrayList<SleepSessionData> sessionList;
    @DexIgnore
    public int sleepGoal;
    @DexIgnore
    public int totalSleepMinutes;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepDayData.class.getSimpleName();
        wd4.a((Object) simpleName, "SleepDayData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepDayData(String str, int i, int i2, ArrayList<SleepSessionData> arrayList) {
        wd4.b(str, "date");
        wd4.b(arrayList, "sessionList");
        this.date = str;
        this.totalSleepMinutes = i;
        this.sleepGoal = i2;
        this.sessionList = arrayList;
    }

    @DexIgnore
    public static /* synthetic */ SleepDayData copy$default(SleepDayData sleepDayData, String str, int i, int i2, ArrayList<SleepSessionData> arrayList, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = sleepDayData.date;
        }
        if ((i3 & 2) != 0) {
            i = sleepDayData.totalSleepMinutes;
        }
        if ((i3 & 4) != 0) {
            i2 = sleepDayData.sleepGoal;
        }
        if ((i3 & 8) != 0) {
            arrayList = sleepDayData.sessionList;
        }
        return sleepDayData.copy(str, i, i2, arrayList);
    }

    @DexIgnore
    private final void updateData() {
        int i;
        this.mDayAwake = 0;
        this.mDayLight = 0;
        this.mDayRestful = 0;
        Iterator<SleepSessionData> it = this.sessionList.iterator();
        while (it.hasNext()) {
            SleepSessionData next = it.next();
            List<WrapperSleepStateChange> sleepStates = next.getSleepStates();
            int size = next.getSleepStates().size();
            int durationInMinutes = next.getDurationInMinutes();
            for (int i2 = 0; i2 < size; i2++) {
                int i3 = sleepStates.get(i2).state;
                if (i2 < size - 1) {
                    i = ((int) sleepStates.get(i2 + 1).index) - ((int) sleepStates.get(i2).index);
                } else {
                    i = durationInMinutes - ((int) sleepStates.get(i2).index);
                }
                if (i3 == 1) {
                    this.mDayLight += i;
                } else if (i3 != 2) {
                    this.mDayAwake += i;
                } else {
                    this.mDayRestful += i;
                }
            }
        }
        this.isDataUpdated = true;
        FLogger.INSTANCE.getLocal().d(TAG, "awake: " + this.mDayAwake + ", light: " + this.mDayLight + ", restful: " + this.mDayRestful);
    }

    @DexIgnore
    public final String component1() {
        return this.date;
    }

    @DexIgnore
    public final int component2() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final int component3() {
        return this.sleepGoal;
    }

    @DexIgnore
    public final ArrayList<SleepSessionData> component4() {
        return this.sessionList;
    }

    @DexIgnore
    public final SleepDayData copy(String str, int i, int i2, ArrayList<SleepSessionData> arrayList) {
        wd4.b(str, "date");
        wd4.b(arrayList, "sessionList");
        return new SleepDayData(str, i, i2, arrayList);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepDayData) {
                SleepDayData sleepDayData = (SleepDayData) obj;
                if (wd4.a((Object) this.date, (Object) sleepDayData.date)) {
                    if (this.totalSleepMinutes == sleepDayData.totalSleepMinutes) {
                        if (!(this.sleepGoal == sleepDayData.sleepGoal) || !wd4.a((Object) this.sessionList, (Object) sleepDayData.sessionList)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getDayAwake() {
        if (!this.isDataUpdated) {
            updateData();
        }
        return this.mDayAwake;
    }

    @DexIgnore
    public final int getDayLight() {
        if (!this.isDataUpdated) {
            updateData();
        }
        return this.mDayLight;
    }

    @DexIgnore
    public final int getDayRestful() {
        if (!this.isDataUpdated) {
            updateData();
        }
        return this.mDayRestful;
    }

    @DexIgnore
    public final ArrayList<SleepSessionData> getSessionList() {
        return this.sessionList;
    }

    @DexIgnore
    public final int getSleepGoal() {
        return this.sleepGoal;
    }

    @DexIgnore
    public final int getTotalSleepMinutes() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.date;
        int i = 0;
        int hashCode = (((((str != null ? str.hashCode() : 0) * 31) + this.totalSleepMinutes) * 31) + this.sleepGoal) * 31;
        ArrayList<SleepSessionData> arrayList = this.sessionList;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setDate(String str) {
        wd4.b(str, "<set-?>");
        this.date = str;
    }

    @DexIgnore
    public final void setSessionList(ArrayList<SleepSessionData> arrayList) {
        wd4.b(arrayList, "<set-?>");
        this.sessionList = arrayList;
    }

    @DexIgnore
    public final void setSleepGoal(int i) {
        this.sleepGoal = i;
    }

    @DexIgnore
    public final void setTotalSleepMinutes(int i) {
        this.totalSleepMinutes = i;
    }

    @DexIgnore
    public String toString() {
        return "SleepDayData(date=" + this.date + ", totalSleepMinutes=" + this.totalSleepMinutes + ", sleepGoal=" + this.sleepGoal + ", sessionList=" + this.sessionList + ")";
    }
}
