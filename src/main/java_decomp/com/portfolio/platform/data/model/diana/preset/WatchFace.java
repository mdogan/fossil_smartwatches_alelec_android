package com.portfolio.platform.data.model.diana.preset;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFace {
    @DexIgnore
    @g02("background")
    public Background background;
    @DexIgnore
    @g02("id")
    public String id;
    @DexIgnore
    @g02("name")
    public String name;
    @DexIgnore
    @g02("previewUrl")
    public String previewUrl;
    @DexIgnore
    @g02("ringStyles")
    public ArrayList<RingStyleItem> ringStyleItems;
    @DexIgnore
    public String serial;

    @DexIgnore
    public WatchFace(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4) {
        wd4.b(str, "id");
        wd4.b(str2, "name");
        wd4.b(background2, Explore.COLUMN_BACKGROUND);
        wd4.b(str3, "previewUrl");
        wd4.b(str4, "serial");
        this.id = str;
        this.name = str2;
        this.ringStyleItems = arrayList;
        this.background = background2;
        this.previewUrl = str3;
        this.serial = str4;
    }

    @DexIgnore
    public static /* synthetic */ WatchFace copy$default(WatchFace watchFace, String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = watchFace.id;
        }
        if ((i & 2) != 0) {
            str2 = watchFace.name;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            arrayList = watchFace.ringStyleItems;
        }
        ArrayList<RingStyleItem> arrayList2 = arrayList;
        if ((i & 8) != 0) {
            background2 = watchFace.background;
        }
        Background background3 = background2;
        if ((i & 16) != 0) {
            str3 = watchFace.previewUrl;
        }
        String str6 = str3;
        if ((i & 32) != 0) {
            str4 = watchFace.serial;
        }
        return watchFace.copy(str, str5, arrayList2, background3, str6, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> component3() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final Background component4() {
        return this.background;
    }

    @DexIgnore
    public final String component5() {
        return this.previewUrl;
    }

    @DexIgnore
    public final String component6() {
        return this.serial;
    }

    @DexIgnore
    public final WatchFace copy(String str, String str2, ArrayList<RingStyleItem> arrayList, Background background2, String str3, String str4) {
        wd4.b(str, "id");
        wd4.b(str2, "name");
        wd4.b(background2, Explore.COLUMN_BACKGROUND);
        wd4.b(str3, "previewUrl");
        wd4.b(str4, "serial");
        return new WatchFace(str, str2, arrayList, background2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchFace)) {
            return false;
        }
        WatchFace watchFace = (WatchFace) obj;
        return wd4.a((Object) this.id, (Object) watchFace.id) && wd4.a((Object) this.name, (Object) watchFace.name) && wd4.a((Object) this.ringStyleItems, (Object) watchFace.ringStyleItems) && wd4.a((Object) this.background, (Object) watchFace.background) && wd4.a((Object) this.previewUrl, (Object) watchFace.previewUrl) && wd4.a((Object) this.serial, (Object) watchFace.serial);
    }

    @DexIgnore
    public final Background getBackground() {
        return this.background;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getPreviewUrl() {
        return this.previewUrl;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> getRingStyleItems() {
        return this.ringStyleItems;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<RingStyleItem> arrayList = this.ringStyleItems;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        Background background2 = this.background;
        int hashCode4 = (hashCode3 + (background2 != null ? background2.hashCode() : 0)) * 31;
        String str3 = this.previewUrl;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.serial;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setBackground(Background background2) {
        wd4.b(background2, "<set-?>");
        this.background = background2;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wd4.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPreviewUrl(String str) {
        wd4.b(str, "<set-?>");
        this.previewUrl = str;
    }

    @DexIgnore
    public final void setRingStyleItems(ArrayList<RingStyleItem> arrayList) {
        this.ringStyleItems = arrayList;
    }

    @DexIgnore
    public final void setSerial(String str) {
        wd4.b(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchFace(id=" + this.id + ", name=" + this.name + ", ringStyleItems=" + this.ringStyleItems + ", background=" + this.background + ", previewUrl=" + this.previewUrl + ", serial=" + this.serial + ")";
    }
}
