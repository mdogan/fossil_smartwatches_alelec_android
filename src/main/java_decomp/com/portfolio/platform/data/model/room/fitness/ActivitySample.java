package com.portfolio.platform.data.model.room.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.ActivityIntensities;
import java.io.Serializable;
import java.util.Date;
import kotlin.TypeCastException;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activity_sample";
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public double calories;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public double distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public ActivityIntensities intensityDistInSteps;
    @DexIgnore
    public String sourceId;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public double steps;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int timeZoneOffsetInSecond;
    @DexIgnore
    public /* final */ String uid;
    @DexIgnore
    public long updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivitySample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ActivitySample createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ActivitySample(parcel);
        }

        @DexIgnore
        public ActivitySample[] newArray(int i) {
            return new ActivitySample[i];
        }
    }

    @DexIgnore
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        ActivityIntensities activityIntensities2 = activityIntensities;
        String str3 = str2;
        wd4.b(str, "uid");
        wd4.b(date2, "date");
        wd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        wd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        wd4.b(activityIntensities2, "intensityDistInSteps");
        wd4.b(str3, SampleRaw.COLUMN_SOURCE_ID);
        this.uid = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.activeTime = i;
        this.intensityDistInSteps = activityIntensities2;
        this.timeZoneOffsetInSecond = i2;
        this.sourceId = str3;
        this.syncTime = j;
        this.createdAt = j2;
        this.updatedAt = j3;
        this.id = generateId();
    }

    @DexIgnore
    public static /* synthetic */ ActivitySample copy$default(ActivitySample activitySample, String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3, int i3, Object obj) {
        ActivitySample activitySample2 = activitySample;
        int i4 = i3;
        return activitySample.copy((i4 & 1) != 0 ? activitySample2.uid : str, (i4 & 2) != 0 ? activitySample2.date : date2, (i4 & 4) != 0 ? activitySample2.startTime : dateTime, (i4 & 8) != 0 ? activitySample2.endTime : dateTime2, (i4 & 16) != 0 ? activitySample2.steps : d, (i4 & 32) != 0 ? activitySample2.calories : d2, (i4 & 64) != 0 ? activitySample2.distance : d3, (i4 & 128) != 0 ? activitySample2.activeTime : i, (i4 & 256) != 0 ? activitySample2.intensityDistInSteps : activityIntensities, (i4 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? activitySample2.timeZoneOffsetInSecond : i2, (i4 & 1024) != 0 ? activitySample2.sourceId : str2, (i4 & 2048) != 0 ? activitySample2.syncTime : j, (i4 & 4096) != 0 ? activitySample2.createdAt : j2, (i4 & 8192) != 0 ? activitySample2.updatedAt : j3);
    }

    @DexIgnore
    private final String generateId() {
        return this.uid + ":device:" + (this.startTime.getMillis() / ((long) 1000));
    }

    @DexIgnore
    public final String component1() {
        return this.uid;
    }

    @DexIgnore
    public final int component10() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String component11() {
        return this.sourceId;
    }

    @DexIgnore
    public final long component12() {
        return this.syncTime;
    }

    @DexIgnore
    public final long component13() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component14() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final double component5() {
        return this.steps;
    }

    @DexIgnore
    public final double component6() {
        return this.calories;
    }

    @DexIgnore
    public final double component7() {
        return this.distance;
    }

    @DexIgnore
    public final int component8() {
        return this.activeTime;
    }

    @DexIgnore
    public final ActivityIntensities component9() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final ActivitySample copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j, long j2, long j3) {
        String str3 = str;
        wd4.b(str3, "uid");
        wd4.b(date2, "date");
        wd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        wd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        wd4.b(activityIntensities, "intensityDistInSteps");
        wd4.b(str2, SampleRaw.COLUMN_SOURCE_ID);
        return new ActivitySample(str3, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, j2, j3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivitySample) {
                ActivitySample activitySample = (ActivitySample) obj;
                if (wd4.a((Object) this.uid, (Object) activitySample.uid) && wd4.a((Object) this.date, (Object) activitySample.date) && wd4.a((Object) this.startTime, (Object) activitySample.startTime) && wd4.a((Object) this.endTime, (Object) activitySample.endTime) && Double.compare(this.steps, activitySample.steps) == 0 && Double.compare(this.calories, activitySample.calories) == 0 && Double.compare(this.distance, activitySample.distance) == 0) {
                    if ((this.activeTime == activitySample.activeTime) && wd4.a((Object) this.intensityDistInSteps, (Object) activitySample.intensityDistInSteps)) {
                        if ((this.timeZoneOffsetInSecond == activitySample.timeZoneOffsetInSecond) && wd4.a((Object) this.sourceId, (Object) activitySample.sourceId)) {
                            if (this.syncTime == activitySample.syncTime) {
                                if (this.createdAt == activitySample.createdAt) {
                                    if (this.updatedAt == activitySample.updatedAt) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityIntensities getIntensityDistInSteps() {
        return this.intensityDistInSteps;
    }

    @DexIgnore
    public final String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimeZoneOffsetInSecond() {
        return this.timeZoneOffsetInSecond;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.uid;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Date date2 = this.date;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        DateTime dateTime = this.startTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = dateTime2 != null ? dateTime2.hashCode() : 0;
        long doubleToLongBits = Double.doubleToLongBits(this.steps);
        long doubleToLongBits2 = Double.doubleToLongBits(this.calories);
        long doubleToLongBits3 = Double.doubleToLongBits(this.distance);
        int i2 = (((((((((hashCode3 + hashCode4) * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)))) * 31) + this.activeTime) * 31;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int hashCode5 = (((i2 + (activityIntensities != null ? activityIntensities.hashCode() : 0)) * 31) + this.timeZoneOffsetInSecond) * 31;
        String str2 = this.sourceId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        long j = this.syncTime;
        long j2 = this.createdAt;
        long j3 = this.updatedAt;
        return ((((((hashCode5 + i) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)));
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        wd4.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        wd4.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setIntensityDistInSteps(ActivityIntensities activityIntensities) {
        wd4.b(activityIntensities, "<set-?>");
        this.intensityDistInSteps = activityIntensities;
    }

    @DexIgnore
    public final void setSourceId(String str) {
        wd4.b(str, "<set-?>");
        this.sourceId = str;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        wd4.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        wd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        this.startTime = dateTime;
        this.id = generateId();
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.timeZoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySample(uid=" + this.uid + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timeZoneOffsetInSecond=" + this.timeZoneOffsetInSecond + ", sourceId=" + this.sourceId + ", syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.uid);
        parcel.writeSerializable(this.date);
        parcel.writeSerializable(this.startTime);
        parcel.writeSerializable(this.endTime);
        parcel.writeDouble(this.steps);
        parcel.writeDouble(this.calories);
        parcel.writeDouble(this.distance);
        parcel.writeInt(this.activeTime);
        parcel.writeParcelable(this.intensityDistInSteps, i);
        parcel.writeInt(this.timeZoneOffsetInSecond);
        parcel.writeLong(this.syncTime);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActivitySample(String str, Date date2, DateTime dateTime, DateTime dateTime2, double d, double d2, double d3, int i, ActivityIntensities activityIntensities, int i2, String str2, long j) {
        this(r1, date2, dateTime, dateTime2, d, d2, d3, i, activityIntensities, i2, str2, j, System.currentTimeMillis(), System.currentTimeMillis());
        String str3 = str;
        wd4.b(str3, "uid");
        wd4.b(date2, "date");
        wd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        wd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        wd4.b(activityIntensities, "intensityDistInSteps");
        wd4.b(str2, SampleRaw.COLUMN_SOURCE_ID);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActivitySample(Parcel parcel) {
        this(r4, r5, r6, r7, r8, r10, r12, r14, r15, r16, r17, parcel.readLong(), parcel.readLong(), parcel.readLong());
        String str;
        Parcel parcel2 = parcel;
        wd4.b(parcel2, "parcel");
        String readString = parcel.readString();
        String str2 = readString != null ? readString : "";
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            Date date2 = (Date) readSerializable;
            Serializable readSerializable2 = parcel.readSerializable();
            if (readSerializable2 != null) {
                DateTime dateTime = (DateTime) readSerializable2;
                Serializable readSerializable3 = parcel.readSerializable();
                if (readSerializable3 != null) {
                    DateTime dateTime2 = (DateTime) readSerializable3;
                    double readDouble = parcel.readDouble();
                    double readDouble2 = parcel.readDouble();
                    double readDouble3 = parcel.readDouble();
                    int readInt = parcel.readInt();
                    ActivityIntensities activityIntensities = (ActivityIntensities) parcel2.readParcelable(ActivityIntensities.class.getClassLoader());
                    ActivityIntensities activityIntensities2 = activityIntensities != null ? activityIntensities : new ActivityIntensities(0.0d, 0.0d, 0.0d);
                    int readInt2 = parcel.readInt();
                    String readString2 = parcel.readString();
                    if (readString2 != null) {
                        str = readString2;
                    } else {
                        str = "";
                    }
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type org.joda.time.DateTime");
            }
            throw new TypeCastException("null cannot be cast to non-null type org.joda.time.DateTime");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.Date");
    }
}
