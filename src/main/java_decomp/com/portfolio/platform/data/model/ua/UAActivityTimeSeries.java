package com.portfolio.platform.data.model.ua;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.ec4;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public yz1 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            wd4.b(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        wd4.b(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final yz1 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final yz1 getJsonData() {
        yz1 yz1 = new yz1();
        yz1.a("external_id", (JsonElement) new a02(this.mExternalId));
        yz1 yz12 = new yz1();
        if (this.stepsMap != null) {
            yz1 yz13 = new yz1();
            yz13.a("interval", (JsonElement) new a02((Number) 100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                yz13.a("values", (JsonElement) gson.a(cg4.a(cg4.a(ec4.a((HashMap) map).toString(), "(", "[", false), ")", "]", false), uz1.class));
                yz12.a("steps", (JsonElement) yz13);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            yz1 yz14 = new yz1();
            yz14.a("interval", (JsonElement) new a02((Number) 100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                yz14.a("values", (JsonElement) gson2.a(cg4.a(cg4.a(ec4.a((HashMap) map2).toString(), "(", "[", false), ")", "]", false), uz1.class));
                yz12.a("distance", (JsonElement) yz14);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            yz1 yz15 = new yz1();
            yz15.a("interval", (JsonElement) new a02((Number) 100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                yz15.a("values", (JsonElement) gson3.a(cg4.a(cg4.a(ec4.a((HashMap) map3).toString(), "(", "[", false), ")", "]", false), uz1.class));
                yz12.a("energy_expended", (JsonElement) yz15);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        yz1.a("time_series", (JsonElement) yz12);
        if (this.extraJsonObject != null) {
            uz1 uz1 = new uz1();
            uz1.a((JsonElement) this.extraJsonObject);
            yz1 yz16 = new yz1();
            yz16.a("data_source", (JsonElement) uz1);
            yz1.a("_links", (JsonElement) yz16);
        }
        return yz1;
    }

    @DexIgnore
    public final void setExtraJsonObject(yz1 yz1) {
        this.extraJsonObject = yz1;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public UAActivityTimeSeries(Builder builder) {
        this(r0, builder.getStepsMap(), builder.getDistanceMap(), builder.getCaloriesMap());
        wd4.b(builder, "builder");
        String mExternalId2 = builder.getMExternalId();
        if (mExternalId2 != null) {
        } else {
            wd4.a();
            throw null;
        }
    }
}
