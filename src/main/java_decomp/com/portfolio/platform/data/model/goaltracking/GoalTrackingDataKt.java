package com.portfolio.platform.data.model.goaltracking;

import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.util.Date;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDataKt {
    @DexIgnore
    public static final Pair<Date, Date> calculateRangeDownload(List<GoalTrackingData> list, Date date, Date date2) {
        wd4.b(list, "$this$calculateRangeDownload");
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new Pair<>(date, date2);
        }
        if (!sk2.d(((GoalTrackingData) wb4.f(list)).getDate(), date2)) {
            return new Pair<>(((GoalTrackingData) wb4.f(list)).getDate(), date2);
        }
        if (sk2.d(((GoalTrackingData) wb4.d(list)).getDate(), date)) {
            return null;
        }
        return new Pair<>(date, sk2.m(((GoalTrackingData) wb4.d(list)).getDate()));
    }
}
