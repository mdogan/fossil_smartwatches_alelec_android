package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.util.Date;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDataWrapperKt {
    @DexIgnore
    public static final Pair<Date, Date> calculateRangeDownload(List<FitnessDataWrapper> list, Date date, Date date2) {
        wd4.b(list, "$this$calculateRangeDownload");
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new Pair<>(date, date2);
        }
        if (!sk2.d(((FitnessDataWrapper) wb4.f(list)).getStartTimeTZ().toDate(), date2)) {
            return new Pair<>(((FitnessDataWrapper) wb4.f(list)).getStartTimeTZ().toDate(), date2);
        }
        if (sk2.d(((FitnessDataWrapper) wb4.d(list)).getStartTimeTZ().toDate(), date)) {
            return null;
        }
        return new Pair<>(date, sk2.m(((FitnessDataWrapper) wb4.d(list)).getStartTimeTZ().toDate()));
    }
}
