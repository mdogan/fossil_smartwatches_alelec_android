package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.blesdk.obfuscated.g02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Temperature {
    @DexIgnore
    @g02("currently")
    public float currently;
    @DexIgnore
    @g02("max")
    public float max;
    @DexIgnore
    @g02("min")
    public float min;
    @DexIgnore
    @g02("unit")
    public String unit;

    @DexIgnore
    public float getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public float getMax() {
        return this.max;
    }

    @DexIgnore
    public float getMin() {
        return this.min;
    }

    @DexIgnore
    public String getUnit() {
        return this.unit;
    }
}
