package com.portfolio.platform.data.model.microapp.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.qk2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettings implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    @g02("forecastType")
    public DISPLAY_FORMAT displayFormat;
    @DexIgnore
    @g02("forecast")
    public String forecast;
    @DexIgnore
    @g02("useCurrentLocation")
    public boolean isUseCurrentLocation;
    @DexIgnore
    public LatLng latLng;
    @DexIgnore
    @g02("area")
    public String location;
    @DexIgnore
    @g02("rainProbability")
    public float rainProbability;
    @DexIgnore
    @g02("unit")
    public TEMP_UNIT tempUnit;
    @DexIgnore
    @g02("temperature")
    public float temperature;
    @DexIgnore
    public long updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherSettings> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherSettings createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherSettings(parcel);
        }

        @DexIgnore
        public WeatherSettings[] newArray(int i) {
            return new WeatherSettings[i];
        }
    }

    @DexIgnore
    public enum DISPLAY_FORMAT {
        MIN(2),
        MAX(1),
        RANGE(3),
        CURRENT_TEMP(0);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ int value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final DISPLAY_FORMAT getDisplayFormat(int i) {
                for (DISPLAY_FORMAT display_format : DISPLAY_FORMAT.values()) {
                    if (display_format.getValue() == i) {
                        return display_format;
                    }
                }
                return DISPLAY_FORMAT.CURRENT_TEMP;
            }

            @DexIgnore
            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((rd4) null);
        }
        */

        @DexIgnore
        DISPLAY_FORMAT(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum TEMP_UNIT {
        CELSIUS("c"),
        FAHRENHEIT("f");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final TEMP_UNIT getTempUnit(String str) {
                wd4.b(str, "value");
                for (TEMP_UNIT temp_unit : TEMP_UNIT.values()) {
                    if (wd4.a((Object) temp_unit.getValue(), (Object) str)) {
                        return temp_unit;
                    }
                }
                return TEMP_UNIT.CELSIUS;
            }

            @DexIgnore
            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((rd4) null);
        }
        */

        @DexIgnore
        TEMP_UNIT(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public WeatherSettings() {
        this.location = "";
        double d = (double) 0;
        this.latLng = new LatLng(d, d);
        this.tempUnit = TEMP_UNIT.CELSIUS;
        this.displayFormat = DISPLAY_FORMAT.CURRENT_TEMP;
        this.forecast = "clear-day";
        this.temperature = 30.0f;
        this.rainProbability = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.isUseCurrentLocation = true;
        this.updatedAt = 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final DISPLAY_FORMAT getDisplayFormat() {
        return this.displayFormat;
    }

    @DexIgnore
    public final String getForecast() {
        return this.forecast;
    }

    @DexIgnore
    public final LatLng getLatLng() {
        return this.latLng;
    }

    @DexIgnore
    public final String getLocation() {
        return this.location;
    }

    @DexIgnore
    public final float getRainProbability() {
        return this.rainProbability;
    }

    @DexIgnore
    public final TEMP_UNIT getTempUnit() {
        return this.tempUnit;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.temperature;
    }

    @DexIgnore
    public final float getTemperatureIn(TEMP_UNIT temp_unit) {
        wd4.b(temp_unit, Constants.PROFILE_KEY_UNIT);
        TEMP_UNIT temp_unit2 = this.tempUnit;
        if (temp_unit2 == temp_unit) {
            return this.temperature;
        }
        if (temp_unit2 == TEMP_UNIT.CELSIUS && temp_unit == TEMP_UNIT.FAHRENHEIT) {
            return qk2.a(this.temperature);
        }
        if (this.tempUnit == TEMP_UNIT.FAHRENHEIT && temp_unit == TEMP_UNIT.CELSIUS) {
            return qk2.c(this.temperature);
        }
        return this.temperature;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isUseCurrentLocation() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final void setDisplayFormat(DISPLAY_FORMAT display_format) {
        wd4.b(display_format, "<set-?>");
        this.displayFormat = display_format;
    }

    @DexIgnore
    public final void setForecast(String str) {
        wd4.b(str, "<set-?>");
        this.forecast = str;
    }

    @DexIgnore
    public final void setLatLng(LatLng latLng2) {
        wd4.b(latLng2, "<set-?>");
        this.latLng = latLng2;
    }

    @DexIgnore
    public final void setLocation(String str) {
        wd4.b(str, "<set-?>");
        this.location = str;
    }

    @DexIgnore
    public final void setRainProbability(float f) {
        this.rainProbability = f;
    }

    @DexIgnore
    public final void setTempUnit(TEMP_UNIT temp_unit) {
        wd4.b(temp_unit, "<set-?>");
        this.tempUnit = temp_unit;
    }

    @DexIgnore
    public final void setTemperature(float f) {
        this.temperature = f;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public final void setUseCurrentLocation(boolean z) {
        this.isUseCurrentLocation = z;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.tempUnit.getValue());
        parcel.writeInt(this.displayFormat.getValue());
        parcel.writeString(String.valueOf(this.isUseCurrentLocation));
        parcel.writeString(this.forecast);
        parcel.writeFloat(this.temperature);
        parcel.writeParcelable(this.latLng, 0);
        parcel.writeFloat(this.rainProbability);
        parcel.writeLong(this.updatedAt);
    }

    @DexIgnore
    public WeatherSettings(Parcel parcel) {
        wd4.b(parcel, "in");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        TEMP_UNIT.Companion companion = TEMP_UNIT.Companion;
        String readString2 = parcel.readString();
        this.tempUnit = companion.getTempUnit(readString2 == null ? TEMP_UNIT.CELSIUS.getValue() : readString2);
        this.displayFormat = DISPLAY_FORMAT.Companion.getDisplayFormat(parcel.readInt());
        Boolean valueOf = Boolean.valueOf(parcel.readString());
        wd4.a((Object) valueOf, "java.lang.Boolean.valueOf(`in`.readString())");
        this.isUseCurrentLocation = valueOf.booleanValue();
        String readString3 = parcel.readString();
        this.forecast = readString3 == null ? "" : readString3;
        this.temperature = parcel.readFloat();
        LatLng latLng2 = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.latLng = latLng2 == null ? new LatLng(0.0d, 0.0d) : latLng2;
        this.rainProbability = parcel.readFloat();
        this.updatedAt = parcel.readLong();
    }
}
