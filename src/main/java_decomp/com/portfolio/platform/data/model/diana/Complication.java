package com.portfolio.platform.data.model.diana;

import com.fossil.blesdk.obfuscated.e02;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Complication {
    @DexIgnore
    @g02("categoryIds")
    @e02
    public ArrayList<String> categories;
    @DexIgnore
    @g02("id")
    @e02
    public String complicationId;
    @DexIgnore
    @g02("createdAt")
    @e02
    public String createdAt;
    @DexIgnore
    @g02("englishDescription")
    @e02
    public String description;
    @DexIgnore
    @g02("description")
    @e02
    public String descriptionKey;
    @DexIgnore
    @e02
    public String icon;
    @DexIgnore
    @g02("englishName")
    @e02
    public String name;
    @DexIgnore
    @g02("name")
    @e02
    public String nameKey;
    @DexIgnore
    @g02("updatedAt")
    @e02
    public String updatedAt;

    @DexIgnore
    public Complication(String str, String str2, String str3, ArrayList<String> arrayList, String str4, String str5, String str6, String str7, String str8) {
        wd4.b(str, "complicationId");
        wd4.b(str2, "name");
        wd4.b(str3, "nameKey");
        wd4.b(arrayList, "categories");
        wd4.b(str4, "description");
        wd4.b(str5, "descriptionKey");
        wd4.b(str7, "createdAt");
        wd4.b(str8, "updatedAt");
        this.complicationId = str;
        this.name = str2;
        this.nameKey = str3;
        this.categories = arrayList;
        this.description = str4;
        this.descriptionKey = str5;
        this.icon = str6;
        this.createdAt = str7;
        this.updatedAt = str8;
    }

    @DexIgnore
    public static /* synthetic */ Complication copy$default(Complication complication, String str, String str2, String str3, ArrayList arrayList, String str4, String str5, String str6, String str7, String str8, int i, Object obj) {
        Complication complication2 = complication;
        int i2 = i;
        return complication.copy((i2 & 1) != 0 ? complication2.complicationId : str, (i2 & 2) != 0 ? complication2.name : str2, (i2 & 4) != 0 ? complication2.nameKey : str3, (i2 & 8) != 0 ? complication2.categories : arrayList, (i2 & 16) != 0 ? complication2.description : str4, (i2 & 32) != 0 ? complication2.descriptionKey : str5, (i2 & 64) != 0 ? complication2.icon : str6, (i2 & 128) != 0 ? complication2.createdAt : str7, (i2 & 256) != 0 ? complication2.updatedAt : str8);
    }

    @DexIgnore
    public final String component1() {
        return this.complicationId;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final ArrayList<String> component4() {
        return this.categories;
    }

    @DexIgnore
    public final String component5() {
        return this.description;
    }

    @DexIgnore
    public final String component6() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String component7() {
        return this.icon;
    }

    @DexIgnore
    public final String component8() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Complication copy(String str, String str2, String str3, ArrayList<String> arrayList, String str4, String str5, String str6, String str7, String str8) {
        wd4.b(str, "complicationId");
        wd4.b(str2, "name");
        wd4.b(str3, "nameKey");
        wd4.b(arrayList, "categories");
        String str9 = str4;
        wd4.b(str9, "description");
        String str10 = str5;
        wd4.b(str10, "descriptionKey");
        String str11 = str7;
        wd4.b(str11, "createdAt");
        String str12 = str8;
        wd4.b(str12, "updatedAt");
        return new Complication(str, str2, str3, arrayList, str9, str10, str6, str11, str12);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Complication)) {
            return false;
        }
        Complication complication = (Complication) obj;
        return wd4.a((Object) this.complicationId, (Object) complication.complicationId) && wd4.a((Object) this.name, (Object) complication.name) && wd4.a((Object) this.nameKey, (Object) complication.nameKey) && wd4.a((Object) this.categories, (Object) complication.categories) && wd4.a((Object) this.description, (Object) complication.description) && wd4.a((Object) this.descriptionKey, (Object) complication.descriptionKey) && wd4.a((Object) this.icon, (Object) complication.icon) && wd4.a((Object) this.createdAt, (Object) complication.createdAt) && wd4.a((Object) this.updatedAt, (Object) complication.updatedAt);
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final String getComplicationId() {
        return this.complicationId;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.complicationId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.nameKey;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<String> arrayList = this.categories;
        int hashCode4 = (hashCode3 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        String str4 = this.description;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.descriptionKey;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.icon;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.createdAt;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.updatedAt;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return hashCode8 + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        wd4.b(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setComplicationId(String str) {
        wd4.b(str, "<set-?>");
        this.complicationId = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        wd4.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDescription(String str) {
        wd4.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        wd4.b(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wd4.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        wd4.b(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wd4.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Complication(complicationId=" + this.complicationId + ", name=" + this.name + ", nameKey=" + this.nameKey + ", categories=" + this.categories + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", icon=" + this.icon + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Complication(String str, String str2, String str3, ArrayList arrayList, String str4, String str5, String str6, String str7, String str8, int i, rd4 rd4) {
        this(str, str2, str3, arrayList, str4, str5, (i & 64) != 0 ? "" : str6, str7, str8);
    }
}
