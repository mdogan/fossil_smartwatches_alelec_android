package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.e02;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SkuModelList {
    @DexIgnore
    @g02("items")
    @e02
    public List<SKUModel> skuModelList;

    @DexIgnore
    public SkuModelList(List<SKUModel> list) {
        wd4.b(list, "skuModelList");
        this.skuModelList = list;
    }

    @DexIgnore
    public static /* synthetic */ SkuModelList copy$default(SkuModelList skuModelList2, List<SKUModel> list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = skuModelList2.skuModelList;
        }
        return skuModelList2.copy(list);
    }

    @DexIgnore
    public final List<SKUModel> component1() {
        return this.skuModelList;
    }

    @DexIgnore
    public final SkuModelList copy(List<SKUModel> list) {
        wd4.b(list, "skuModelList");
        return new SkuModelList(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof SkuModelList) && wd4.a((Object) this.skuModelList, (Object) ((SkuModelList) obj).skuModelList);
        }
        return true;
    }

    @DexIgnore
    public final List<SKUModel> getSkuModelList() {
        return this.skuModelList;
    }

    @DexIgnore
    public int hashCode() {
        List<SKUModel> list = this.skuModelList;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setSkuModelList(List<SKUModel> list) {
        wd4.b(list, "<set-?>");
        this.skuModelList = list;
    }

    @DexIgnore
    public String toString() {
        return "SkuModelList(skuModelList=" + this.skuModelList + ")";
    }
}
