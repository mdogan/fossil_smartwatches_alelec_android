package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    @g02("locations")
    public List<WeatherLocationWrapper> locations; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppSetting createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherWatchAppSetting(parcel);
        }

        @DexIgnore
        public WeatherWatchAppSetting[] newArray(int i) {
            return new WeatherWatchAppSetting[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppSetting() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WeatherLocationWrapper> getLocations() {
        return this.locations;
    }

    @DexIgnore
    public final void setLocations(List<WeatherLocationWrapper> list) {
        wd4.b(list, "<set-?>");
        this.locations = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeList(wb4.k(this.locations));
    }

    @DexIgnore
    public WeatherWatchAppSetting(Parcel parcel) {
        wd4.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, WeatherLocationWrapper.class.getClassLoader());
        this.locations = arrayList;
    }
}
