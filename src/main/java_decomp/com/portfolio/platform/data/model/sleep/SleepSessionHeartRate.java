package com.portfolio.platform.data.model.sleep;

import com.fossil.blesdk.obfuscated.wd4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSessionHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public SleepSessionHeartRate(float f, int i, int i2, int i3, List<Short> list) {
        wd4.b(list, "values");
        this.average = f;
        this.max = i;
        this.min = i2;
        this.resolutionInSecond = i3;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionHeartRate copy$default(SleepSessionHeartRate sleepSessionHeartRate, float f, int i, int i2, int i3, List<Short> list, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            f = sleepSessionHeartRate.average;
        }
        if ((i4 & 2) != 0) {
            i = sleepSessionHeartRate.max;
        }
        int i5 = i;
        if ((i4 & 4) != 0) {
            i2 = sleepSessionHeartRate.min;
        }
        int i6 = i2;
        if ((i4 & 8) != 0) {
            i3 = sleepSessionHeartRate.resolutionInSecond;
        }
        int i7 = i3;
        if ((i4 & 16) != 0) {
            list = sleepSessionHeartRate.values;
        }
        return sleepSessionHeartRate.copy(f, i5, i6, i7, list);
    }

    @DexIgnore
    public final float component1() {
        return this.average;
    }

    @DexIgnore
    public final int component2() {
        return this.max;
    }

    @DexIgnore
    public final int component3() {
        return this.min;
    }

    @DexIgnore
    public final int component4() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component5() {
        return this.values;
    }

    @DexIgnore
    public final SleepSessionHeartRate copy(float f, int i, int i2, int i3, List<Short> list) {
        wd4.b(list, "values");
        return new SleepSessionHeartRate(f, i, i2, i3, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepSessionHeartRate) {
                SleepSessionHeartRate sleepSessionHeartRate = (SleepSessionHeartRate) obj;
                if (Float.compare(this.average, sleepSessionHeartRate.average) == 0) {
                    if (this.max == sleepSessionHeartRate.max) {
                        if (this.min == sleepSessionHeartRate.min) {
                            if (!(this.resolutionInSecond == sleepSessionHeartRate.resolutionInSecond) || !wd4.a((Object) this.values, (Object) sleepSessionHeartRate.values)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = ((((((Float.floatToIntBits(this.average) * 31) + this.max) * 31) + this.min) * 31) + this.resolutionInSecond) * 31;
        List<Short> list = this.values;
        return floatToIntBits + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        wd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionHeartRate(average=" + this.average + ", max=" + this.max + ", min=" + this.min + ", resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ")";
    }
}
