package com.portfolio.platform.data.model.diana.preset;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetComplicationSetting {
    @DexIgnore
    @g02("appId")
    public String id;
    @DexIgnore
    @g02("localUpdatedAt")
    public String localUpdateAt;
    @DexIgnore
    @g02("complicationPosition")
    public String position;
    @DexIgnore
    @g02("settings")
    public String settings;

    @DexIgnore
    public DianaPresetComplicationSetting(String str, String str2, String str3, String str4) {
        wd4.b(str, "position");
        wd4.b(str2, "id");
        wd4.b(str3, "localUpdateAt");
        this.position = str;
        this.id = str2;
        this.localUpdateAt = str3;
        this.settings = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPresetComplicationSetting copy$default(DianaPresetComplicationSetting dianaPresetComplicationSetting, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaPresetComplicationSetting.position;
        }
        if ((i & 2) != 0) {
            str2 = dianaPresetComplicationSetting.id;
        }
        if ((i & 4) != 0) {
            str3 = dianaPresetComplicationSetting.localUpdateAt;
        }
        if ((i & 8) != 0) {
            str4 = dianaPresetComplicationSetting.settings;
        }
        return dianaPresetComplicationSetting.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final String component3() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String component4() {
        return this.settings;
    }

    @DexIgnore
    public final DianaPresetComplicationSetting copy(String str, String str2, String str3, String str4) {
        wd4.b(str, "position");
        wd4.b(str2, "id");
        wd4.b(str3, "localUpdateAt");
        return new DianaPresetComplicationSetting(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaPresetComplicationSetting)) {
            return false;
        }
        DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) obj;
        return wd4.a((Object) this.position, (Object) dianaPresetComplicationSetting.position) && wd4.a((Object) this.id, (Object) dianaPresetComplicationSetting.id) && wd4.a((Object) this.localUpdateAt, (Object) dianaPresetComplicationSetting.localUpdateAt) && wd4.a((Object) this.settings, (Object) dianaPresetComplicationSetting.settings);
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getLocalUpdateAt() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final String getSettings() {
        return this.settings;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.position;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.id;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.localUpdateAt;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.settings;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLocalUpdateAt(String str) {
        wd4.b(str, "<set-?>");
        this.localUpdateAt = str;
    }

    @DexIgnore
    public final void setPosition(String str) {
        wd4.b(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setSettings(String str) {
        this.settings = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetComplicationSetting(position=" + this.position + ", id=" + this.id + ", localUpdateAt=" + this.localUpdateAt + ", settings=" + this.settings + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DianaPresetComplicationSetting(String str, String str2, String str3) {
        this(str, str2, str3, "");
        wd4.b(str, "position");
        wd4.b(str2, "complicationId");
        wd4.b(str3, "localUpdateAt");
    }
}
