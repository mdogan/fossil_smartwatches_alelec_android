package com.portfolio.platform.data.model.room.microapp;

import android.content.Context;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.HybridPresetAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    @g02("buttons")
    @f02(HybridPresetAppSettingSerializer.class)
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @g02("createdAt")
    public String createdAt;
    @DexIgnore
    @g02("id")
    public String id;
    @DexIgnore
    @g02("isActive")
    public boolean isActive;
    @DexIgnore
    @g02("name")
    public String name;
    @DexIgnore
    @jk2
    public int pinType;
    @DexIgnore
    @g02("serialNumber")
    public String serialNumber;
    @DexIgnore
    @g02("updatedAt")
    public String updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final HybridPreset cloneFrom(HybridPreset hybridPreset) {
            wd4.b(hybridPreset, "preset");
            String t = sk2.t(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset2 = new HybridPreset(uuid, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_CreateNew_NewPreset_Title__NewPreset), hybridPreset.getSerialNumber(), hybridPreset.getButtons(), false);
            hybridPreset2.setCreatedAt(t);
            hybridPreset2.setUpdatedAt(t);
            return hybridPreset2;
        }

        @DexIgnore
        public final HybridPreset cloneFromDefault(HybridRecommendPreset hybridRecommendPreset) {
            wd4.b(hybridRecommendPreset, "preset");
            String t = sk2.t(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset = new HybridPreset(uuid, hybridRecommendPreset.getName(), hybridRecommendPreset.getSerialNumber(), hybridRecommendPreset.getButtons(), hybridRecommendPreset.isDefault());
            hybridPreset.setCreatedAt(t);
            hybridPreset.setUpdatedAt(t);
            for (HybridPresetAppSetting localUpdateAt : hybridPreset.getButtons()) {
                wd4.a((Object) t, "timestamp");
                localUpdateAt.setLocalUpdateAt(t);
            }
            return hybridPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public HybridPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        wd4.b(str, "id");
        wd4.b(str3, "serialNumber");
        wd4.b(arrayList, "buttons");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isActive = z;
        this.pinType = 1;
        this.createdAt = "";
        this.updatedAt = "";
    }

    @DexIgnore
    public static /* synthetic */ HybridPreset copy$default(HybridPreset hybridPreset, String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = hybridPreset.name;
        }
        String str4 = str2;
        if ((i & 4) != 0) {
            str3 = hybridPreset.serialNumber;
        }
        String str5 = str3;
        if ((i & 8) != 0) {
            arrayList = hybridPreset.buttons;
        }
        ArrayList<HybridPresetAppSetting> arrayList2 = arrayList;
        if ((i & 16) != 0) {
            z = hybridPreset.isActive;
        }
        return hybridPreset.copy(str, str4, str5, arrayList2, z);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, tj2.a((List<HybridPresetAppSetting>) this.buttons), this.isActive);
        hybridPreset.createdAt = this.createdAt;
        hybridPreset.updatedAt = this.updatedAt;
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isActive;
    }

    @DexIgnore
    public final HybridPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        wd4.b(str, "id");
        wd4.b(str3, "serialNumber");
        wd4.b(arrayList, "buttons");
        return new HybridPreset(str, str2, str3, arrayList, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HybridPreset) {
                HybridPreset hybridPreset = (HybridPreset) obj;
                if (wd4.a((Object) this.id, (Object) hybridPreset.id) && wd4.a((Object) this.name, (Object) hybridPreset.name) && wd4.a((Object) this.serialNumber, (Object) hybridPreset.serialNumber) && wd4.a((Object) this.buttons, (Object) hybridPreset.buttons)) {
                    if (this.isActive == hybridPreset.isActive) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.serialNumber;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        int i2 = (hashCode3 + i) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        wd4.b(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wd4.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isActive=" + this.isActive + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, int i, rd4 rd4) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z);
    }
}
