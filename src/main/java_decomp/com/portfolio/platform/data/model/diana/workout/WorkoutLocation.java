package com.portfolio.platform.data.model.diana.workout;

import com.fossil.blesdk.obfuscated.wd4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutLocation {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public List<Location> values;

    @DexIgnore
    public WorkoutLocation(int i, List<Location> list) {
        wd4.b(list, "values");
        this.resolution = i;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutLocation copy$default(WorkoutLocation workoutLocation, int i, List<Location> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutLocation.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutLocation.values;
        }
        return workoutLocation.copy(i, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Location> component2() {
        return this.values;
    }

    @DexIgnore
    public final WorkoutLocation copy(int i, List<Location> list) {
        wd4.b(list, "values");
        return new WorkoutLocation(i, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutLocation) {
                WorkoutLocation workoutLocation = (WorkoutLocation) obj;
                if (!(this.resolution == workoutLocation.resolution) || !wd4.a((Object) this.values, (Object) workoutLocation.values)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Location> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolution * 31;
        List<Location> list = this.values;
        return i + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setValues(List<Location> list) {
        wd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutLocation(resolution=" + this.resolution + ", values=" + this.values + ")";
    }
}
