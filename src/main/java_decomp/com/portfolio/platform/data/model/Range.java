package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.g02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Range {
    @DexIgnore
    @g02("hasNext")
    public boolean hasNext;
    @DexIgnore
    @g02("limit")
    public int limit;
    @DexIgnore
    @g02("offset")
    public int offset;

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public boolean isHasNext() {
        return this.hasNext;
    }

    @DexIgnore
    public void setHasNext(boolean z) {
        this.hasNext = z;
    }

    @DexIgnore
    public void setOffset(int i) {
        this.offset = i;
    }
}
