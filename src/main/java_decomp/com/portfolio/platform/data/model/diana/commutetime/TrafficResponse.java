package com.portfolio.platform.data.model.diana.commutetime;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrafficResponse {
    @DexIgnore
    @g02("durationEstimation")
    public long durationEstimation;
    @DexIgnore
    @g02("durationInTraffic")
    public long durationInTraffic;
    @DexIgnore
    @g02("trafficStatus")
    public String trafficStatus;

    @DexIgnore
    public TrafficResponse(long j, long j2, String str) {
        wd4.b(str, "trafficStatus");
        this.durationEstimation = j;
        this.durationInTraffic = j2;
        this.trafficStatus = str;
    }

    @DexIgnore
    public final long getDurationEstimation() {
        return this.durationEstimation;
    }

    @DexIgnore
    public final long getDurationInTraffic() {
        return this.durationInTraffic;
    }

    @DexIgnore
    public final String getTrafficStatus() {
        return this.trafficStatus;
    }

    @DexIgnore
    public final void setDurationEstimation(long j) {
        this.durationEstimation = j;
    }

    @DexIgnore
    public final void setDurationInTraffic(long j) {
        this.durationInTraffic = j;
    }

    @DexIgnore
    public final void setTrafficStatus(String str) {
        wd4.b(str, "<set-?>");
        this.trafficStatus = str;
    }
}
