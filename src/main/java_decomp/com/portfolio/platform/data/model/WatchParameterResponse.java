package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParameterResponse {
    @DexIgnore
    @g02("category")
    public String category;
    @DexIgnore
    @g02("data")
    public ResponseData data;
    @DexIgnore
    @g02("id")
    public String id;
    @DexIgnore
    @g02("metadata")
    public MetaData metaData;
    @DexIgnore
    @g02("name")
    public String name;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MetaData {
        @DexIgnore
        @g02("deviceLongName")
        public String deviceLongName;
        @DexIgnore
        @g02("deviceShortName")
        public String deviceShortName;
        @DexIgnore
        @g02("mainHandsFlipped")
        public boolean mainHandsFlipped;
        @DexIgnore
        @g02("prefixSerialNumbers")
        public ArrayList<String> serialNumbers;
        @DexIgnore
        @g02("themeMode")
        public String themeMode;
        @DexIgnore
        @g02("version")
        public Version version;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Version {
            @DexIgnore
            @g02("major")
            public int versionMajor;
            @DexIgnore
            @g02("minor")
            public int versionMinor;

            @DexIgnore
            public final int getVersionMajor() {
                return this.versionMajor;
            }

            @DexIgnore
            public final int getVersionMinor() {
                return this.versionMinor;
            }

            @DexIgnore
            public final void setVersionMajor(int i) {
                this.versionMajor = i;
            }

            @DexIgnore
            public final void setVersionMinor(int i) {
                this.versionMinor = i;
            }
        }

        @DexIgnore
        public final String getDeviceLongName() {
            return this.deviceLongName;
        }

        @DexIgnore
        public final String getDeviceShortName() {
            return this.deviceShortName;
        }

        @DexIgnore
        public final boolean getMainHandsFlipped() {
            return this.mainHandsFlipped;
        }

        @DexIgnore
        public final ArrayList<String> getSerialNumbers() {
            return this.serialNumbers;
        }

        @DexIgnore
        public final String getThemeMode() {
            return this.themeMode;
        }

        @DexIgnore
        public final Version getVersion() {
            Version version2 = this.version;
            if (version2 != null) {
                return version2;
            }
            wd4.d("version");
            throw null;
        }

        @DexIgnore
        public final void setDeviceLongName(String str) {
            this.deviceLongName = str;
        }

        @DexIgnore
        public final void setDeviceShortName(String str) {
            this.deviceShortName = str;
        }

        @DexIgnore
        public final void setMainHandsFlipped(boolean z) {
            this.mainHandsFlipped = z;
        }

        @DexIgnore
        public final void setSerialNumbers(ArrayList<String> arrayList) {
            this.serialNumbers = arrayList;
        }

        @DexIgnore
        public final void setThemeMode(String str) {
            this.themeMode = str;
        }

        @DexIgnore
        public final void setVersion(Version version2) {
            wd4.b(version2, "<set-?>");
            this.version = version2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ResponseData {
        @DexIgnore
        @g02("content")
        public String content;

        @DexIgnore
        public final String getContent() {
            return this.content;
        }

        @DexIgnore
        public final void setContent(String str) {
            this.content = str;
        }
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final ResponseData getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final void setCategory(String str) {
        this.category = str;
    }

    @DexIgnore
    public final void setData(ResponseData responseData) {
        this.data = responseData;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "gson.toJson(this)");
        return a;
    }

    @DexIgnore
    public final WatchParam toWatchParamModel(String str) {
        wd4.b(str, "serial");
        MetaData metaData2 = this.metaData;
        String str2 = null;
        if (metaData2 != null) {
            String valueOf = String.valueOf(metaData2.getVersion().getVersionMajor());
            MetaData metaData3 = this.metaData;
            if (metaData3 != null) {
                String valueOf2 = String.valueOf(metaData3.getVersion().getVersionMinor());
                ResponseData responseData = this.data;
                if (responseData != null) {
                    str2 = responseData.getContent();
                }
                return new WatchParam(str, valueOf, valueOf2, str2);
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }
}
