package com.portfolio.platform.data;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepStatistic {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sleep_statistic";
    @DexIgnore
    @g02("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @g02("id")
    public /* final */ String id;
    @DexIgnore
    @g02("sleepTimeBestDay")
    public /* final */ SleepDailyBest sleepTimeBestDay;
    @DexIgnore
    @g02("sleepTimeBestStreak")
    public /* final */ SleepDailyBest sleepTimeBestStreak;
    @DexIgnore
    @g02("totalDays")
    public /* final */ int totalDays;
    @DexIgnore
    @g02("totalSleepMinutes")
    public /* final */ int totalSleepMinutes;
    @DexIgnore
    @g02("totalSleepStateDistInMinute")
    public /* final */ List<Integer> totalSleepStateDistInMinute;
    @DexIgnore
    @g02("totalSleeps")
    public /* final */ int totalSleeps;
    @DexIgnore
    @g02("uid")
    public /* final */ String uid;
    @DexIgnore
    @g02("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SleepDailyBest {
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ String sleepDailySummaryId;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public SleepDailyBest(String str, Date date2, int i) {
            wd4.b(str, "sleepDailySummaryId");
            wd4.b(date2, "date");
            this.sleepDailySummaryId = str;
            this.date = date2;
            this.value = i;
        }

        @DexIgnore
        public static /* synthetic */ SleepDailyBest copy$default(SleepDailyBest sleepDailyBest, String str, Date date2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = sleepDailyBest.sleepDailySummaryId;
            }
            if ((i2 & 2) != 0) {
                date2 = sleepDailyBest.date;
            }
            if ((i2 & 4) != 0) {
                i = sleepDailyBest.value;
            }
            return sleepDailyBest.copy(str, date2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final int component3() {
            return this.value;
        }

        @DexIgnore
        public final SleepDailyBest copy(String str, Date date2, int i) {
            wd4.b(str, "sleepDailySummaryId");
            wd4.b(date2, "date");
            return new SleepDailyBest(str, date2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof SleepDailyBest) {
                    SleepDailyBest sleepDailyBest = (SleepDailyBest) obj;
                    if (wd4.a((Object) this.sleepDailySummaryId, (Object) sleepDailyBest.sleepDailySummaryId) && wd4.a((Object) this.date, (Object) sleepDailyBest.date)) {
                        if (this.value == sleepDailyBest.value) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getSleepDailySummaryId() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.sleepDailySummaryId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return ((hashCode + i) * 31) + this.value;
        }

        @DexIgnore
        public String toString() {
            return "SleepDailyBest(sleepDailySummaryId=" + this.sleepDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore
    public SleepStatistic(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        wd4.b(str, "id");
        wd4.b(str2, "uid");
        wd4.b(list, "totalSleepStateDistInMinute");
        wd4.b(dateTime, "createdAt");
        wd4.b(dateTime2, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.sleepTimeBestDay = sleepDailyBest;
        this.sleepTimeBestStreak = sleepDailyBest2;
        this.totalDays = i;
        this.totalSleeps = i2;
        this.totalSleepMinutes = i3;
        this.totalSleepStateDistInMinute = list;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ SleepStatistic copy$default(SleepStatistic sleepStatistic, String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List list, DateTime dateTime, DateTime dateTime2, int i4, Object obj) {
        SleepStatistic sleepStatistic2 = sleepStatistic;
        int i5 = i4;
        return sleepStatistic.copy((i5 & 1) != 0 ? sleepStatistic2.id : str, (i5 & 2) != 0 ? sleepStatistic2.uid : str2, (i5 & 4) != 0 ? sleepStatistic2.sleepTimeBestDay : sleepDailyBest, (i5 & 8) != 0 ? sleepStatistic2.sleepTimeBestStreak : sleepDailyBest2, (i5 & 16) != 0 ? sleepStatistic2.totalDays : i, (i5 & 32) != 0 ? sleepStatistic2.totalSleeps : i2, (i5 & 64) != 0 ? sleepStatistic2.totalSleepMinutes : i3, (i5 & 128) != 0 ? sleepStatistic2.totalSleepStateDistInMinute : list, (i5 & 256) != 0 ? sleepStatistic2.createdAt : dateTime, (i5 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? sleepStatistic2.updatedAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component2() {
        return this.uid;
    }

    @DexIgnore
    public final SleepDailyBest component3() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest component4() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int component5() {
        return this.totalDays;
    }

    @DexIgnore
    public final int component6() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final int component7() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> component8() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final SleepStatistic copy(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        wd4.b(str, "id");
        wd4.b(str2, "uid");
        List<Integer> list2 = list;
        wd4.b(list2, "totalSleepStateDistInMinute");
        DateTime dateTime3 = dateTime;
        wd4.b(dateTime3, "createdAt");
        DateTime dateTime4 = dateTime2;
        wd4.b(dateTime4, "updatedAt");
        return new SleepStatistic(str, str2, sleepDailyBest, sleepDailyBest2, i, i2, i3, list2, dateTime3, dateTime4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepStatistic) {
                SleepStatistic sleepStatistic = (SleepStatistic) obj;
                if (wd4.a((Object) this.id, (Object) sleepStatistic.id) && wd4.a((Object) this.uid, (Object) sleepStatistic.uid) && wd4.a((Object) this.sleepTimeBestDay, (Object) sleepStatistic.sleepTimeBestDay) && wd4.a((Object) this.sleepTimeBestStreak, (Object) sleepStatistic.sleepTimeBestStreak)) {
                    if (this.totalDays == sleepStatistic.totalDays) {
                        if (this.totalSleeps == sleepStatistic.totalSleeps) {
                            if (!(this.totalSleepMinutes == sleepStatistic.totalSleepMinutes) || !wd4.a((Object) this.totalSleepStateDistInMinute, (Object) sleepStatistic.totalSleepStateDistInMinute) || !wd4.a((Object) this.createdAt, (Object) sleepStatistic.createdAt) || !wd4.a((Object) this.updatedAt, (Object) sleepStatistic.updatedAt)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestDay() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestStreak() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int getTotalDays() {
        return this.totalDays;
    }

    @DexIgnore
    public final int getTotalSleepMinutes() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> getTotalSleepStateDistInMinute() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final int getTotalSleeps() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.uid;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        SleepDailyBest sleepDailyBest = this.sleepTimeBestDay;
        int hashCode3 = (hashCode2 + (sleepDailyBest != null ? sleepDailyBest.hashCode() : 0)) * 31;
        SleepDailyBest sleepDailyBest2 = this.sleepTimeBestStreak;
        int hashCode4 = (((((((hashCode3 + (sleepDailyBest2 != null ? sleepDailyBest2.hashCode() : 0)) * 31) + this.totalDays) * 31) + this.totalSleeps) * 31) + this.totalSleepMinutes) * 31;
        List<Integer> list = this.totalSleepStateDistInMinute;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        DateTime dateTime = this.createdAt;
        int hashCode6 = (hashCode5 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public String toString() {
        return "SleepStatistic(id=" + this.id + ", uid=" + this.uid + ", sleepTimeBestDay=" + this.sleepTimeBestDay + ", sleepTimeBestStreak=" + this.sleepTimeBestStreak + ", totalDays=" + this.totalDays + ", totalSleeps=" + this.totalSleeps + ", totalSleepMinutes=" + this.totalSleepMinutes + ", totalSleepStateDistInMinute=" + this.totalSleepStateDistInMinute + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
