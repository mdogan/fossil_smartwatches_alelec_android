package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantLocalDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wd4.b(str, "<set-?>");
            MicroAppVariantLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantLocalDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppVariantLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public final boolean addOrUpDateVariant(MicroAppVariant microAppVariant) {
        wd4.b(microAppVariant, "microAppVariant");
        String str = TAG;
        MFLogger.d(str, ".addOrUpDateVariant - serial=" + microAppVariant.getSerialNumbers() + ", major=" + microAppVariant.getMajorNumber() + " minor=" + microAppVariant.getMinorNumber() + " id=" + microAppVariant.getId());
        return en2.p.a().d().addOrUpdateMicroAppVariant(microAppVariant);
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        wd4.b(declarationFile, "declarationFile");
        wd4.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile declarationFileId=" + declarationFile.getFileId());
        if (en2.p.a().d().addOrUpdateDeclarationFile(declarationFile)) {
            addOrUpdateDeclarationFileCallback.onSuccess(declarationFile);
        } else {
            addOrUpdateDeclarationFileCallback.onFail();
        }
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wd4.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serial=" + str + " major=" + i + " minor=" + i2);
        List<MicroAppVariant> allMicroAppVariant = en2.p.a().d().getAllMicroAppVariant(str, i);
        if (allMicroAppVariant == null || !(!allMicroAppVariant.isEmpty())) {
            String str3 = TAG;
            MFLogger.d(str3, "getAllMicroAppVariants onFail serial=" + str);
            if (getVariantListCallback != null) {
                getVariantListCallback.onFail(600);
                return;
            }
            return;
        }
        String str4 = TAG;
        MFLogger.d(str4, "getAllMicroAppVariants onSuccess serial=" + str);
        if (getVariantListCallback != null) {
            getVariantListCallback.onSuccess(allMicroAppVariant);
        }
    }

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        wd4.b(str, "serialNumber");
        wd4.b(str2, "microAppId");
        wd4.b(str3, "variantName");
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppVariant serial=" + str + " microAppId=" + str2 + " major=" + i + " minor=" + i2);
        MicroAppVariant microAppVariant = en2.p.a().d().getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            String str5 = TAG;
            MFLogger.d(str5, "getMicroAppVariant onSuccess serial=" + str + " microAppId=" + str2);
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(microAppVariant);
                return;
            }
            return;
        }
        String str6 = TAG;
        MFLogger.d(str6, "getMicroAppVariant onFail serial=" + str + " microAppId=" + str2);
        if (getVariantCallback != null) {
            getVariantCallback.onFail(600);
        }
    }

    @DexIgnore
    public void removeMicroAppVariants(String str, int i, int i2) {
        wd4.b(str, "serialNumber");
        en2.p.a().d().deleteMicroAppVariants(str, i, i2);
    }
}
