package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease() {
        return new MicroAppGalleryLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, i42 i42) {
        wd4.b(shortcutApiService, "shortcutApiService");
        wd4.b(i42, "appExecutors");
        return new MicroAppGalleryRemoteDataSource(shortcutApiService, i42);
    }
}
