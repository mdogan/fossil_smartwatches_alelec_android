package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PresetRepository_Factory implements Factory<PresetRepository> {
    @DexIgnore
    public /* final */ Provider<i42> appExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingLocalSetDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingRemoteSetDataSourceProvider;

    @DexIgnore
    public PresetRepository_Factory(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<i42> provider3) {
        this.mappingRemoteSetDataSourceProvider = provider;
        this.mappingLocalSetDataSourceProvider = provider2;
        this.appExecutorsProvider = provider3;
    }

    @DexIgnore
    public static PresetRepository_Factory create(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<i42> provider3) {
        return new PresetRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static PresetRepository newPresetRepository(PresetDataSource presetDataSource, PresetDataSource presetDataSource2, i42 i42) {
        return new PresetRepository(presetDataSource, presetDataSource2, i42);
    }

    @DexIgnore
    public static PresetRepository provideInstance(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<i42> provider3) {
        return new PresetRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public PresetRepository get() {
        return provideInstance(this.mappingRemoteSetDataSourceProvider, this.mappingLocalSetDataSourceProvider, this.appExecutorsProvider);
    }
}
