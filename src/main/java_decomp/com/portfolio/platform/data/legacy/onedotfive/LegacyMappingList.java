package com.portfolio.platform.data.legacy.onedotfive;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.Mapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyMappingList {
    @DexIgnore
    @g02("action")
    public int action;
    @DexIgnore
    @g02("deviceId")
    public String deviceId;
    @DexIgnore
    @g02("extraInfo")
    public String extraInfo;
    @DexIgnore
    @g02("gesture")
    public String gesture;
    @DexIgnore
    @g02("isServiceCommand")
    public boolean isServiceCommand;
    @DexIgnore
    @g02("mDeviceFamily")
    public String mDeviceFamily;
    @DexIgnore
    @g02("updatedAt")
    public String updatedAt;

    @DexIgnore
    public LegacyMappingList(boolean z, int i, String str, String str2, String str3, String str4, String str5) {
        wd4.b(str, "mDeviceFamily");
        wd4.b(str2, "deviceId");
        wd4.b(str3, Mapping.COLUMN_EXTRA_INFO);
        wd4.b(str4, "gesture");
        wd4.b(str5, "updatedAt");
        this.isServiceCommand = z;
        this.action = i;
        this.mDeviceFamily = str;
        this.deviceId = str2;
        this.extraInfo = str3;
        this.gesture = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public final int getAction() {
        return this.action;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getExtraInfo() {
        return this.extraInfo;
    }

    @DexIgnore
    public final String getGesture() {
        return this.gesture;
    }

    @DexIgnore
    public final String getMDeviceFamily() {
        return this.mDeviceFamily;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isServiceCommand() {
        return this.isServiceCommand;
    }

    @DexIgnore
    public final void setAction(int i) {
        this.action = i;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        wd4.b(str, "<set-?>");
        this.deviceId = str;
    }

    @DexIgnore
    public final void setExtraInfo(String str) {
        wd4.b(str, "<set-?>");
        this.extraInfo = str;
    }

    @DexIgnore
    public final void setGesture(String str) {
        wd4.b(str, "<set-?>");
        this.gesture = str;
    }

    @DexIgnore
    public final void setMDeviceFamily(String str) {
        wd4.b(str, "<set-?>");
        this.mDeviceFamily = str;
    }

    @DexIgnore
    public final void setServiceCommand(boolean z) {
        this.isServiceCommand = z;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wd4.b(str, "<set-?>");
        this.updatedAt = str;
    }
}
