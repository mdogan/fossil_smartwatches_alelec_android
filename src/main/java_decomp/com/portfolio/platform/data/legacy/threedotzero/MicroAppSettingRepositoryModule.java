package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppSettingRepositoryModule {
    @DexIgnore
    @Local
    public MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource() {
        return new MicroAppSettingLocalDataSource();
    }

    @DexIgnore
    @Remote
    public MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource(ShortcutApiService shortcutApiService, i42 i42) {
        return new MicroAppSettingRemoteDataSource(shortcutApiService, i42);
    }
}
