package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$getMicroAppVariant$Anon1 implements MicroAppVariantDataSource.GetVariantCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ String $microAppId;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $variantName;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository this$Anon0;

    @DexIgnore
    public MicroAppVariantRepository$getMicroAppVariant$Anon1(MicroAppVariantRepository microAppVariantRepository, String str, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback, String str2, String str3) {
        this.this$Anon0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantCallback;
        this.$microAppId = str2;
        this.$variantName = str3;
    }

    @DexIgnore
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroAppVariant local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        this.this$Anon0.downloadAllVariants(this.$serialNumber, this.$major, this.$minor, new MicroAppVariantRepository$getMicroAppVariant$Anon1$onFail$Anon1(this, i));
    }

    @DexIgnore
    public void onSuccess(MicroAppVariant microAppVariant) {
        wd4.b(microAppVariant, "microAppVariant");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroAppVariant local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.$callback;
        if (getVariantCallback != null) {
            getVariantCallback.onSuccess(microAppVariant);
        }
    }
}
