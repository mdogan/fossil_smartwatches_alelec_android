package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory implements Factory<MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ Provider<i42> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppVariantRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, Provider<ShortcutApiService> provider, Provider<i42> provider2) {
        this.module = microAppVariantRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory create(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, Provider<ShortcutApiService> provider, Provider<i42> provider2) {
        return new MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory(microAppVariantRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppVariantDataSource provideInstance(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, Provider<ShortcutApiService> provider, Provider<i42> provider2) {
        return proxyProvideMicroAppVariantRemoteDataSource$app_fossilRelease(microAppVariantRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static MicroAppVariantDataSource proxyProvideMicroAppVariantRemoteDataSource$app_fossilRelease(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, ShortcutApiService shortcutApiService, i42 i42) {
        MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantRemoteDataSource$app_fossilRelease(shortcutApiService, i42);
        o44.a(provideMicroAppVariantRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    public MicroAppVariantDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
