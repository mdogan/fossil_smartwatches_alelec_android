package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryLocalDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wd4.b(str, "<set-?>");
            MicroAppGalleryLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryLocalDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppGalleryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public void deleteListMicroApp(String str) {
        wd4.b(str, "serial");
        String str2 = TAG;
        MFLogger.d(str2, "deleteListMicroApp serial=" + str);
        en2.p.a().d().deleteMicroAppList(str);
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        wd4.b(str2, "microAppId");
        String str3 = TAG;
        MFLogger.d(str3, "getMicroApp deviceSerial=" + str + " microAppId=" + str2);
        MicroApp microApp = en2.p.a().d().getMicroApp(str, str2);
        if (microApp != null) {
            MFLogger.d(TAG, "getMicroApp onSuccess");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onSuccess(microApp);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroApp onFail");
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        List<MicroApp> listMicroApp = en2.p.a().d().getListMicroApp(str);
        if (listMicroApp == null || !(!listMicroApp.isEmpty())) {
            MFLogger.d(TAG, "getMicroAppGallery onFail");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroAppGallery onSuccess");
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(listMicroApp);
        }
    }

    @DexIgnore
    public void updateListMicroApp(List<? extends MicroApp> list) {
        if (list != null && (!list.isEmpty())) {
            String str = TAG;
            MFLogger.d(str, "updateListMicroApp microAppListSize=" + list.size());
            en2.p.a().d().updateListMicroApp(list);
        }
    }
}
