package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppVariantRepository extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppVariantDataSource mMicroAppVariantRemoteDataSource;
    @DexIgnore
    public List<ShortcutDownloadingObserver> mShortcutDownloadingObservers;
    @DexIgnore
    public /* final */ UAppSystemVersionRepository mUAppSystemVersionRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wd4.b(str, "<set-?>");
            MicroAppVariantRepository.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[MicroAppInstruction.MicroAppID.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = MicroAppVariantRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppVariantRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRepository(@Remote MicroAppVariantDataSource microAppVariantDataSource, @Local MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, i42 i42) {
        wd4.b(microAppVariantDataSource, "mMicroAppVariantRemoteDataSource");
        wd4.b(microAppVariantDataSource2, "mMicroAppVariantLocalDataSource");
        wd4.b(uAppSystemVersionRepository, "mUAppSystemVersionRepository");
        wd4.b(i42, "mAppExecutors");
        this.mMicroAppVariantRemoteDataSource = microAppVariantDataSource;
        this.mMicroAppVariantLocalDataSource = microAppVariantDataSource2;
        this.mUAppSystemVersionRepository = uAppSystemVersionRepository;
        this.mAppExecutors = i42;
    }

    @DexIgnore
    private final void saveDeclarationFileList(List<? extends DeclarationFile> list, MicroAppVariant microAppVariant) {
        for (DeclarationFile declarationFile : list) {
            declarationFile.setMicroAppVariant(microAppVariant);
            addOrUpdateDeclarationFile(declarationFile, new MicroAppVariantRepository$saveDeclarationFileList$Anon1());
        }
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        wd4.b(declarationFile, "declarationFile");
        wd4.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile fileId=" + declarationFile.getId());
        this.mMicroAppVariantLocalDataSource.addOrUpdateDeclarationFile(declarationFile, addOrUpdateDeclarationFileCallback);
    }

    @DexIgnore
    public final void addStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        wd4.b(shortcutDownloadingObserver, "observer");
        if (this.mShortcutDownloadingObservers == null) {
            this.mShortcutDownloadingObservers = new CopyOnWriteArrayList();
        }
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list != null) {
            list.add(shortcutDownloadingObserver);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void downloadAllVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wd4.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$downloadAllVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final ArrayList<xo2> filterVariantList$app_fossilRelease(List<? extends xo2> list) {
        wd4.b(list, "microAppVariants");
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            xo2 xo2 = (xo2) list.get(i);
            Iterator it = ob4.a((Collection<?>) list).iterator();
            while (it.hasNext()) {
                xo2 xo22 = (xo2) list.get(((yb4) it).a());
                if (wd4.a((Object) xo22.b(), (Object) xo2.b()) && wd4.a((Object) xo22.e(), (Object) xo2.e()) && xo22.d() > xo2.d()) {
                    xo2 = xo22;
                }
            }
            hashMap.put(xo2.b() + xo2.e(), xo2);
        }
        return new ArrayList<>(hashMap.values());
    }

    @DexIgnore
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        wd4.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str + " major=" + i + " minor=" + i2);
        this.mMicroAppVariantLocalDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$getAllMicroAppVariants$Anon1(this, str, i, i2, getVariantListCallback));
    }

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        wd4.b(str4, "serialNumber");
        wd4.b(str5, "microAppId");
        wd4.b(str6, "variantName");
        String str7 = TAG;
        MFLogger.d(str7, "getMicroAppVariant serialNumber=" + str4 + "microAppId=" + str5 + " major=" + i + " minor=" + i2 + " name=" + str6);
        this.mMicroAppVariantLocalDataSource.getMicroAppVariant(str, str2, str3, i, i2, new MicroAppVariantRepository$getMicroAppVariant$Anon1(this, str, i, i2, getVariantCallback, str2, str3));
    }

    @DexIgnore
    public final void migrateMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback) {
        wd4.b(str, "serialNumber");
        notifyStatusChanged("DECLARATION_FILES_DOWNLOADING", str);
        this.mMicroAppVariantRemoteDataSource.getAllMicroAppVariants(str, i, i2, new MicroAppVariantRepository$migrateMicroAppVariants$Anon1(this, str, i, i2, migrateVariantCallback));
    }

    @DexIgnore
    public final void notifyStatusChanged(String str, String str2) {
        wd4.b(str, "status");
        wd4.b(str2, "serial");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            return;
        }
        if (list == null) {
            wd4.a();
            throw null;
        } else if (!list.isEmpty()) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                for (ShortcutDownloadingObserver onStatusChanged : list2) {
                    onStatusChanged.onStatusChanged(str, str2);
                }
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void removeStatusObserver(ShortcutDownloadingObserver shortcutDownloadingObserver) {
        wd4.b(shortcutDownloadingObserver, "observer");
        List<ShortcutDownloadingObserver> list = this.mShortcutDownloadingObservers;
        if (list == null) {
            wd4.a();
            throw null;
        } else if (list.contains(shortcutDownloadingObserver)) {
            List<ShortcutDownloadingObserver> list2 = this.mShortcutDownloadingObservers;
            if (list2 != null) {
                list2.remove(shortcutDownloadingObserver);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void saveMicroAppVariant$app_fossilRelease(String str, List<? extends xo2> list) {
        wd4.b(str, "serialNumber");
        wd4.b(list, "filter");
        for (xo2 xo2 : list) {
            xo2.d(str);
            MicroAppVariant a = xo2.a();
            wd4.a((Object) a, "parser.convertToMicroAppVariant()");
            String str2 = TAG;
            MFLogger.d(str2, "saveMicroAppVariant variantId=" + a.getId());
            MicroAppVariantDataSource microAppVariantDataSource = this.mMicroAppVariantLocalDataSource;
            if (microAppVariantDataSource != null) {
                ((MicroAppVariantLocalDataSource) microAppVariantDataSource).addOrUpDateVariant(a);
                List<DeclarationFile> c = xo2.c();
                wd4.a((Object) c, "declarationFileList");
                saveDeclarationFileList(c, a);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantLocalDataSource");
            }
        }
    }
}
