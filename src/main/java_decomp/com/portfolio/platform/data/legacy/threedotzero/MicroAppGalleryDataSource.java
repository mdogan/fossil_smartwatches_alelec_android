package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppGalleryDataSource {

    @DexIgnore
    public interface GetMicroAppCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(MicroApp microApp);
    }

    @DexIgnore
    public interface GetMicroAppGalleryCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(List<? extends MicroApp> list);
    }

    @DexIgnore
    public void deleteListMicroApp(String str) {
        wd4.b(str, "serial");
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, GetMicroAppCallback getMicroAppCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        wd4.b(str2, "microAppId");
    }

    @DexIgnore
    public abstract void getMicroAppGallery(String str, GetMicroAppGalleryCallback getMicroAppGalleryCallback);

    @DexIgnore
    public void updateListMicroApp(List<? extends MicroApp> list) {
    }

    @DexIgnore
    public void updateMicroApp(MicroApp microApp, GetMicroAppCallback getMicroAppCallback) {
        wd4.b(microApp, "microApp");
    }
}
