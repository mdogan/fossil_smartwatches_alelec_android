package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wd4.b(str, "<set-?>");
            MicroAppGalleryRepository.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppGalleryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRepository(@Remote MicroAppGalleryDataSource microAppGalleryDataSource, @Local MicroAppGalleryDataSource microAppGalleryDataSource2, i42 i42) {
        wd4.b(microAppGalleryDataSource, "mMicroAppSettingRemoteDataSource");
        wd4.b(microAppGalleryDataSource2, "mMicroAppSettingLocalDataSource");
        wd4.b(i42, "mAppExecutors");
        this.mMicroAppSettingRemoteDataSource = microAppGalleryDataSource;
        this.mMicroAppSettingLocalDataSource = microAppGalleryDataSource2;
        this.mAppExecutors = i42;
    }

    @DexIgnore
    public final void downloadMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "downloadMicroAppGallery deviceSerial=" + str);
        this.mMicroAppSettingRemoteDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$downloadMicroAppGallery$Anon1(this, str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        wd4.b(str2, "microAppId");
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroApp deviceSerial=empty");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroApp(str, str2, new MicroAppGalleryRepository$getMicroApp$Anon1(this, str2, getMicroAppCallback, str));
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroAppGallery deviceSerial=empty");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    public void updateMicroApp(MicroApp microApp, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        wd4.b(microApp, "microApp");
        String str = TAG;
        MFLogger.d(str, "updateMicroApp microApp=" + microApp.getAppId());
        this.mMicroAppSettingLocalDataSource.updateMicroApp(microApp, getMicroAppCallback);
    }
}
