package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.to2;
import com.fossil.blesdk.obfuscated.yz1;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppSettingRemoteDataSource implements MicroAppSettingDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ i42 mAppExecutors;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements qr4<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$microAppSettingList;

        @DexIgnore
        public Anon1(List list, MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
            this.val$microAppSettingList = list;
            this.val$callback = microAppSettingListCallback;
        }

        @DexIgnore
        public void onFailure(Call<yz1> call, Throwable th) {
            MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFail");
            if (this.val$callback == null) {
                return;
            }
            if (!this.val$microAppSettingList.isEmpty()) {
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFailure presetList not null");
                this.val$callback.onSuccess(this.val$microAppSettingList);
                return;
            }
            MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFailure presetList is null");
            this.val$callback.onFail();
        }

        @DexIgnore
        public void onResponse(Call<yz1> call, cs4<yz1> cs4) {
            if (cs4.d()) {
                yz1 a = cs4.a();
                String str = MicroAppSettingRemoteDataSource.TAG;
                MFLogger.d(str, "getMicroAppSettingList onSuccess response=" + a);
                to2 to2 = new to2();
                to2.a(a);
                this.val$microAppSettingList.addAll(to2.a());
                Range b = to2.b();
                if (b != null && b.isHasNext()) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onSuccess hasNext=true");
                    MicroAppSettingRemoteDataSource.this.getMicroAppSettingList(b.getOffset() + b.getLimit() + 1, b.getLimit(), this);
                } else if (this.val$callback != null) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onSuccess hasNext=false");
                    if (!this.val$microAppSettingList.isEmpty()) {
                        this.val$callback.onSuccess(this.val$microAppSettingList);
                    } else {
                        this.val$callback.onFail();
                    }
                }
            } else {
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful");
                if (!this.val$microAppSettingList.isEmpty()) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful presetList not null");
                    MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
                    if (microAppSettingListCallback != null) {
                        microAppSettingListCallback.onSuccess(this.val$microAppSettingList);
                        return;
                    }
                    return;
                }
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful presetList is null");
                MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback2 = this.val$callback;
                if (microAppSettingListCallback2 != null) {
                    microAppSettingListCallback2.onFail();
                }
            }
        }
    }

    @DexIgnore
    public MicroAppSettingRemoteDataSource(ShortcutApiService shortcutApiService, i42 i42) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = i42;
    }

    @DexIgnore
    public void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateMicroAppSetting microAppSetting=" + microAppSetting.getSetting() + " microAppId=" + microAppSetting.getMicroAppId());
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public void getMicroAppSetting(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
    }

    @DexIgnore
    public void getMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        MFLogger.d(TAG, "getMicroAppSettingList");
        getMicroAppSettingList(0, 100, new Anon1(new ArrayList(), microAppSettingListCallback));
    }

    @DexIgnore
    public void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
    }

    @DexIgnore
    public void getMicroAppSettingList(int i, int i2, qr4<yz1> qr4) {
        String str = TAG;
        MFLogger.d(str, "getMicroAppSettingList offset=" + i + " size=" + i2);
    }
}
