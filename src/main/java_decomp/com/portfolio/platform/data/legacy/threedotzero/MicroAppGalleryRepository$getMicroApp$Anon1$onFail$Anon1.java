package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1 implements MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$getMicroApp$Anon1 this$Anon0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1(MicroAppGalleryRepository$getMicroApp$Anon1 microAppGalleryRepository$getMicroApp$Anon1) {
        this.this$Anon0 = microAppGalleryRepository$getMicroApp$Anon1;
    }

    @DexIgnore
    public void onFail() {
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$Anon0.$microAppId + " remote onFail");
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$Anon0.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    public void onSuccess(List<? extends MicroApp> list) {
        wd4.b(list, "microAppList");
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$Anon0.$microAppId + " remote onSuccess");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((MicroApp) list.get(i)).setPlatform(this.this$Anon0.$deviceSerial);
            if (wd4.a((Object) ((MicroApp) list.get(i)).getAppId(), (Object) this.this$Anon0.$microAppId)) {
                MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$Anon0.$callback;
                if (getMicroAppCallback != null) {
                    getMicroAppCallback.onSuccess((MicroApp) list.get(i));
                }
            }
        }
        this.this$Anon0.this$Anon0.mAppExecutors.a().execute(new MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1$onSuccess$Anon1(this, list));
    }
}
