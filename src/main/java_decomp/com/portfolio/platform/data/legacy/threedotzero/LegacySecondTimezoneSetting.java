package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.e02;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "secondTimezone")
public class LegacySecondTimezoneSetting implements Parcelable, Comparable<LegacySecondTimezoneSetting> {
    @DexIgnore
    public static /* final */ String COLUMN_IS_ACTIVE; // = "isActiveAlarm";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_CITY_NAME; // = "timezone_city_name";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_ID; // = "timezone_id";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_OFFSET; // = "timezone_offset";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_TYPE; // = "timezone_type";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    public static /* final */ Parcelable.Creator<LegacySecondTimezoneSetting> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "isActiveAlarm")
    public boolean isActive;
    @DexIgnore
    @e02
    @DatabaseField(columnName = "timezone_city_name")
    public String timezoneCityName;
    @DexIgnore
    @e02
    @DatabaseField(columnName = "timezone_id")
    public String timezoneId;
    @DexIgnore
    @e02
    @DatabaseField(columnName = "timezone_offset")
    public long timezoneOffset;
    @DexIgnore
    @e02
    @DatabaseField(columnName = "timezone_type")
    public String timezoneType;
    @DexIgnore
    @e02
    @DatabaseField(columnName = "uri", id = true)
    public String uri;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<LegacySecondTimezoneSetting> {
        @DexIgnore
        public LegacySecondTimezoneSetting createFromParcel(Parcel parcel) {
            return new LegacySecondTimezoneSetting(parcel);
        }

        @DexIgnore
        public LegacySecondTimezoneSetting[] newArray(int i) {
            return new LegacySecondTimezoneSetting[i];
        }
    }

    @DexIgnore
    public LegacySecondTimezoneSetting() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void generateUri() {
        this.uri = "uri:fsl:timezone:" + this.timezoneCityName + ':' + this.timezoneId;
    }

    @DexIgnore
    public String getTimezoneCityName() {
        return this.timezoneCityName;
    }

    @DexIgnore
    public String getTimezoneId() {
        return this.timezoneId;
    }

    @DexIgnore
    public long getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public String getTimezoneType() {
        return this.timezoneType;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public void setTimezoneCityName(String str) {
        this.timezoneCityName = str;
    }

    @DexIgnore
    public void setTimezoneId(String str) {
        this.timezoneId = str;
    }

    @DexIgnore
    public void setTimezoneOffset(long j) {
        this.timezoneOffset = j;
    }

    @DexIgnore
    public void setTimezoneType(String str) {
        this.timezoneType = str;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.timezoneCityName);
        parcel.writeLong(this.timezoneOffset);
        parcel.writeString(this.timezoneType);
        parcel.writeString(this.timezoneId);
        parcel.writeString(this.uri);
    }

    @DexIgnore
    public LegacySecondTimezoneSetting(String str, String str2, long j, String str3) {
        this.timezoneId = str;
        this.timezoneCityName = str2;
        this.timezoneOffset = j;
        this.timezoneType = str3;
        this.uri = "uri:fsl:timezone:" + str2 + ':' + str;
    }

    @DexIgnore
    public LegacySecondTimezoneSetting clone() {
        LegacySecondTimezoneSetting legacySecondTimezoneSetting = new LegacySecondTimezoneSetting();
        legacySecondTimezoneSetting.timezoneCityName = this.timezoneCityName;
        legacySecondTimezoneSetting.timezoneOffset = this.timezoneOffset;
        legacySecondTimezoneSetting.timezoneType = this.timezoneType;
        legacySecondTimezoneSetting.timezoneId = this.timezoneId;
        legacySecondTimezoneSetting.generateUri();
        return legacySecondTimezoneSetting;
    }

    @DexIgnore
    public int compareTo(LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        return this.timezoneCityName.compareTo(legacySecondTimezoneSetting.timezoneCityName);
    }

    @DexIgnore
    public LegacySecondTimezoneSetting(Parcel parcel) {
        this.timezoneCityName = parcel.readString();
        this.timezoneOffset = parcel.readLong();
        this.timezoneType = parcel.readString();
        this.timezoneId = parcel.readString();
        this.uri = parcel.readString();
    }
}
