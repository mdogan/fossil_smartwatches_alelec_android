package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.xo2;
import com.misfit.frameworks.common.log.MFLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$downloadAllVariants$Anon1$onSuccess$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $variantParserList;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository$downloadAllVariants$Anon1 this$Anon0;

    @DexIgnore
    public MicroAppVariantRepository$downloadAllVariants$Anon1$onSuccess$Anon1(MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon1, ArrayList arrayList) {
        this.this$Anon0 = microAppVariantRepository$downloadAllVariants$Anon1;
        this.$variantParserList = arrayList;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadAllVariants");
        ArrayList<xo2> filterVariantList$app_fossilRelease = this.this$Anon0.this$Anon0.filterVariantList$app_fossilRelease(this.$variantParserList);
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon1 = this.this$Anon0;
        microAppVariantRepository$downloadAllVariants$Anon1.this$Anon0.saveMicroAppVariant$app_fossilRelease(microAppVariantRepository$downloadAllVariants$Anon1.$serialNumber, filterVariantList$app_fossilRelease);
        UAppSystemVersionModel uAppSystemVersionModel = this.this$Anon0.this$Anon0.mUAppSystemVersionRepository.getUAppSystemVersionModel(this.this$Anon0.$serialNumber);
        if (uAppSystemVersionModel != null && uAppSystemVersionModel.getMajorVersion() == this.this$Anon0.$major && uAppSystemVersionModel.getMinorVersion() == this.this$Anon0.$minor) {
            uAppSystemVersionModel.setPinType(0);
            this.this$Anon0.this$Anon0.mUAppSystemVersionRepository.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        }
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon12 = this.this$Anon0;
        microAppVariantRepository$downloadAllVariants$Anon12.this$Anon0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", microAppVariantRepository$downloadAllVariants$Anon12.$serialNumber);
        MicroAppVariantDataSource access$getMMicroAppVariantLocalDataSource$p = this.this$Anon0.this$Anon0.mMicroAppVariantLocalDataSource;
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon13 = this.this$Anon0;
        access$getMMicroAppVariantLocalDataSource$p.getAllMicroAppVariants(microAppVariantRepository$downloadAllVariants$Anon13.$serialNumber, microAppVariantRepository$downloadAllVariants$Anon13.$major, microAppVariantRepository$downloadAllVariants$Anon13.$minor, microAppVariantRepository$downloadAllVariants$Anon13.$callback);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadAllVariants");
    }
}
