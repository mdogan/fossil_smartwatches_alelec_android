package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.vo2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.model.Range;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import kotlin.TypeCastException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1 implements qr4<yz1> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $microAppVariants;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRemoteDataSource this$Anon0;

    @DexIgnore
    public MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(MicroAppVariantRemoteDataSource microAppVariantRemoteDataSource, String str, ArrayList arrayList, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$Anon0 = microAppVariantRemoteDataSource;
        this.$serialNumber = str;
        this.$microAppVariants = arrayList;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFailure(Call<yz1> call, Throwable th) {
        wd4.b(call, "call");
        wd4.b(th, "throwable");
        String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure");
        if (!this.$microAppVariants.isEmpty()) {
            String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
            if (getVariantListCallback != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback).onSuccess(this.$microAppVariants);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants is null");
        if (th instanceof SocketTimeoutException) {
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
            if (getVariantListCallback2 != null) {
                getVariantListCallback2.onFail(MFNetworkReturnCode.CLIENT_TIMEOUT);
                return;
            }
            return;
        }
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
        if (getVariantListCallback3 != null) {
            getVariantListCallback3.onFail(601);
        }
    }

    @DexIgnore
    public void onResponse(Call<yz1> call, cs4<yz1> cs4) {
        wd4.b(call, "call");
        wd4.b(cs4, "response");
        if (cs4.d()) {
            yz1 a = cs4.a();
            String tag = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag, "getMicroAppGallery deviceSerial=" + this.$serialNumber + " onSuccess response=" + a);
            vo2 vo2 = new vo2();
            vo2.a(a);
            this.$microAppVariants.addAll(vo2.a());
            Range b = vo2.b();
            if (b == null || !b.isHasNext()) {
                String tag2 = MicroAppVariantRemoteDataSource.Companion.getTAG();
                MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=false");
                if (this.$microAppVariants.isEmpty()) {
                    MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
                    if (getVariantListCallback != null) {
                        getVariantListCallback.onFail(cs4.b());
                        return;
                    }
                    return;
                }
                MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
                if (getVariantListCallback2 != null) {
                    ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback2).onSuccess(this.$microAppVariants);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
            }
            String tag3 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=true");
            this.this$Anon0.getAllMicroAppVariants$app_fossilRelease(this.$serialNumber, this.$major, this.$minor, b.getOffset() + b.getLimit(), b.getLimit(), this);
            return;
        }
        String tag4 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag4, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful");
        if (!this.$microAppVariants.isEmpty()) {
            String tag5 = MicroAppVariantRemoteDataSource.Companion.getTAG();
            MFLogger.d(tag5, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants not null");
            MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
            if (getVariantListCallback3 != null) {
                ((MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback3).onSuccess(this.$microAppVariants);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        String tag6 = MicroAppVariantRemoteDataSource.Companion.getTAG();
        MFLogger.d(tag6, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants is null");
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback4 = this.$callback;
        if (getVariantListCallback4 != null) {
            getVariantListCallback4.onFail(MFNetworkReturnCode.NOT_FOUND);
        }
    }
}
