package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRemoteDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wd4.b(str, "<set-?>");
            MicroAppGalleryRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppGalleryRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRemoteDataSource(ShortcutApiService shortcutApiService, i42 i42) {
        wd4.b(shortcutApiService, "mShortcutApiService");
        wd4.b(i42, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = i42;
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getMicroAppGallery deviceSerial=" + str);
        this.mShortcutApiService.getMicroAppGallery(0, 100, str).a(new MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }
}
