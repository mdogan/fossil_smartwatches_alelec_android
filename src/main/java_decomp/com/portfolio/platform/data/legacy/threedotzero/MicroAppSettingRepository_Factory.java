package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.i42;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppSettingRepository_Factory implements Factory<MicroAppSettingRepository> {
    @DexIgnore
    public /* final */ Provider<i42> appExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingDataSource> microAppSettingLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingDataSource> microAppSettingRemoteDataSourceProvider;

    @DexIgnore
    public MicroAppSettingRepository_Factory(Provider<MicroAppSettingDataSource> provider, Provider<MicroAppSettingDataSource> provider2, Provider<i42> provider3) {
        this.microAppSettingRemoteDataSourceProvider = provider;
        this.microAppSettingLocalDataSourceProvider = provider2;
        this.appExecutorsProvider = provider3;
    }

    @DexIgnore
    public static MicroAppSettingRepository_Factory create(Provider<MicroAppSettingDataSource> provider, Provider<MicroAppSettingDataSource> provider2, Provider<i42> provider3) {
        return new MicroAppSettingRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static MicroAppSettingRepository newMicroAppSettingRepository(MicroAppSettingDataSource microAppSettingDataSource, MicroAppSettingDataSource microAppSettingDataSource2, i42 i42) {
        return new MicroAppSettingRepository(microAppSettingDataSource, microAppSettingDataSource2, i42);
    }

    @DexIgnore
    public static MicroAppSettingRepository provideInstance(Provider<MicroAppSettingDataSource> provider, Provider<MicroAppSettingDataSource> provider2, Provider<i42> provider3) {
        return new MicroAppSettingRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public MicroAppSettingRepository get() {
        return provideInstance(this.microAppSettingRemoteDataSourceProvider, this.microAppSettingLocalDataSourceProvider, this.appExecutorsProvider);
    }
}
