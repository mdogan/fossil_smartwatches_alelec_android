package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<rd<T>> pagedList;
    @DexIgnore
    public /* final */ id4<cb4> refresh;
    @DexIgnore
    public /* final */ id4<cb4> retry;

    @DexIgnore
    public Listing(LiveData<rd<T>> liveData, LiveData<NetworkState> liveData2, id4<cb4> id4, id4<cb4> id42) {
        wd4.b(liveData, "pagedList");
        wd4.b(liveData2, "networkState");
        wd4.b(id4, "refresh");
        wd4.b(id42, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = id4;
        this.retry = id42;
    }

    @DexIgnore
    public static /* synthetic */ Listing copy$default(Listing listing, LiveData<rd<T>> liveData, LiveData<NetworkState> liveData2, id4<cb4> id4, id4<cb4> id42, int i, Object obj) {
        if ((i & 1) != 0) {
            liveData = listing.pagedList;
        }
        if ((i & 2) != 0) {
            liveData2 = listing.networkState;
        }
        if ((i & 4) != 0) {
            id4 = listing.refresh;
        }
        if ((i & 8) != 0) {
            id42 = listing.retry;
        }
        return listing.copy(liveData, liveData2, id4, id42);
    }

    @DexIgnore
    public final LiveData<rd<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final id4<cb4> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final id4<cb4> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<rd<T>> liveData, LiveData<NetworkState> liveData2, id4<cb4> id4, id4<cb4> id42) {
        wd4.b(liveData, "pagedList");
        wd4.b(liveData2, "networkState");
        wd4.b(id4, "refresh");
        wd4.b(id42, "retry");
        return new Listing<>(liveData, liveData2, id4, id42);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Listing)) {
            return false;
        }
        Listing listing = (Listing) obj;
        return wd4.a((Object) this.pagedList, (Object) listing.pagedList) && wd4.a((Object) this.networkState, (Object) listing.networkState) && wd4.a((Object) this.refresh, (Object) listing.refresh) && wd4.a((Object) this.retry, (Object) listing.retry);
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<rd<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final id4<cb4> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final id4<cb4> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        LiveData<rd<T>> liveData = this.pagedList;
        int i = 0;
        int hashCode = (liveData != null ? liveData.hashCode() : 0) * 31;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = (hashCode + (liveData2 != null ? liveData2.hashCode() : 0)) * 31;
        id4<cb4> id4 = this.refresh;
        int hashCode3 = (hashCode2 + (id4 != null ? id4.hashCode() : 0)) * 31;
        id4<cb4> id42 = this.retry;
        if (id42 != null) {
            i = id42.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
