package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WechatToken {
    @DexIgnore
    @g02("access_token")
    public /* final */ String mAccessToken;
    @DexIgnore
    @g02("expires_in")
    public /* final */ int mExpiresIn;
    @DexIgnore
    @g02("openid")
    public /* final */ String mOpenId;
    @DexIgnore
    @g02("refresh_token")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @g02("scope")
    public /* final */ String mScope;

    @DexIgnore
    public WechatToken(String str, int i, String str2, String str3, String str4) {
        wd4.b(str, "mAccessToken");
        wd4.b(str2, "mRefreshToken");
        wd4.b(str3, "mOpenId");
        wd4.b(str4, "mScope");
        this.mAccessToken = str;
        this.mExpiresIn = i;
        this.mRefreshToken = str2;
        this.mOpenId = str3;
        this.mScope = str4;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final int getExpiresIn() {
        return this.mExpiresIn;
    }

    @DexIgnore
    public final String getOpenId() {
        return this.mOpenId;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.mScope;
    }
}
