package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kk2;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2Dot1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public DianaPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiServiceV2Dot1");
        this.mApiServiceV2Dot1 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteDianaPreset(DianaPreset dianaPreset, kc4<? super ro2<Void>> kc4) {
        DianaPresetRemoteDataSource$deleteDianaPreset$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) {
            dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = (DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) kc4;
            int i2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    uz1 uz1 = new uz1();
                    uz1.a(dianaPreset.getId());
                    yz1.a("_ids", (JsonElement) uz1);
                    DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1(this, yz1, (kc4) null);
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon1 = dianaPreset;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon2 = yz1;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon3 = uz1;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1, dianaPresetRemoteDataSource$deleteDianaPreset$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    uz1 uz12 = (uz1) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon3;
                    yz1 yz12 = (yz1) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon2;
                    DianaPreset dianaPreset2 = (DianaPreset) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new so2((Object) null, false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$Anon1(this, kc4);
        Object obj2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
        Object a2 = oc4.a();
        i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadDianaPresetList(String str, kc4<? super ro2<ArrayList<DianaPreset>>> kc4) {
        DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) kc4;
            int i2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1(this, str, (kc4) null);
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    so2 so2 = (so2) ro2;
                    if (!so2.b()) {
                        Object a2 = so2.a();
                        if (a2 != null) {
                            for (DianaPreset dianaPreset : ((ApiResponse) a2).get_items()) {
                                dianaPreset.setPinType(0);
                                dianaPreset.setSerialNumber(str);
                                arrayList.add(dianaPreset);
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    return new so2(arrayList, so2.b());
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object downloadDianaRecommendPresetList(String str, kc4<? super ro2<ArrayList<DianaRecommendPreset>>> kc4) {
        DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) kc4;
            int i2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadDianaRecommendPresetList " + str);
                    DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1(this, str, (kc4) null);
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadDianaRecommendPresetList success ");
                    sb.append(arrayList);
                    sb.append(" isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(TAG, sb.toString());
                    if (!so2.b()) {
                        String t = sk2.t(new Date(System.currentTimeMillis()));
                        Object a2 = so2.a();
                        if (a2 != null) {
                            for (DianaRecommendPreset dianaRecommendPreset : ((ApiResponse) a2).get_items()) {
                                dianaRecommendPreset.setSerialNumber(str);
                                wd4.a((Object) t, "timestamp");
                                dianaRecommendPreset.setCreatedAt(t);
                                dianaRecommendPreset.setUpdatedAt(t);
                                arrayList.add(dianaRecommendPreset);
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    return new so2(arrayList, so2.b());
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadDianaRecommendPresetList fail code ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError ");
                    sb2.append(qo2.c());
                    local3.d(TAG, sb2.toString());
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object replaceDianaPresetList(List<DianaPreset> list, kc4<? super ro2<ArrayList<DianaPreset>>> kc4) {
        DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) kc4;
            int i2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    sz1 sz1 = new sz1();
                    sz1.b(new kk2());
                    Gson a2 = sz1.a();
                    yz1 yz1 = new yz1();
                    Object[] array = list.toArray(new DianaPreset[0]);
                    if (array != null) {
                        yz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "replaceDianaPresetList jsonObject " + yz1);
                        DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1(this, yz1, (kc4) null);
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon0 = this;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon1 = list;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon2 = a2;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon3 = yz1;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon2;
                    List list2 = (List) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((so2) ro2).a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "replaceDianaPresetList success " + arrayList);
                        return new so2(arrayList, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("replaceDianaPresetList fail code ");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" serverError ");
                    sb.append(qo2.c());
                    local3.d(TAG, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
        Object a4 = oc4.a();
        i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object upsertDianaPresetList(List<DianaPreset> list, kc4<? super ro2<ArrayList<DianaPreset>>> kc4) {
        DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1;
        int i;
        ro2 ro2;
        List<DianaPreset> list2 = list;
        kc4<? super ro2<ArrayList<DianaPreset>>> kc42 = kc4;
        if (kc42 instanceof DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) kc42;
            int i2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    sz1 sz1 = new sz1();
                    sz1.b(new kk2());
                    Gson a2 = sz1.a();
                    yz1 yz1 = new yz1();
                    Object[] array = list2.toArray(new DianaPreset[0]);
                    if (array != null) {
                        yz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "upsertPresetList jsonObject " + yz1);
                        DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1(this, yz1, (kc4) null);
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon0 = this;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon1 = list2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon2 = a2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon3 = yz1;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon2;
                    List list3 = (List) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success isCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(TAG, sb.toString());
                    Object a3 = so2.a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "update pin tpye as synced");
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d(TAG, "upsertPresetList success " + arrayList);
                        return new so2(arrayList, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("upsertPresetList fail code ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError ");
                    sb2.append(qo2.c());
                    local4.d(TAG, sb2.toString());
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1(this, kc42);
        Object obj2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
        Object a4 = oc4.a();
        i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
