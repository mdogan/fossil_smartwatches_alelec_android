package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.un1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<TResult> implements un1<Void> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GFitWorkoutSession $gFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0118Anon1 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            public C0118Anon1(Anon1 anon1) {
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final void run() {
                this.this$Anon0.this$Anon0.this$Anon0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().deleteGFitWorkoutSession(this.this$Anon0.this$Anon0.$gFitWorkoutSession);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.this$Anon0.getMThirdPartyDatabase().runInTransaction((Runnable) new C0118Anon1(this));
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(GFitWorkoutSession gFitWorkoutSession, GoogleSignInAccount googleSignInAccount, Ref$IntRef ref$IntRef, int i, pg4 pg4, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$gFitWorkoutSession = gFitWorkoutSession;
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfList$inlined = ref$IntRef;
        this.$sizeOfGFitWorkoutSessionList$inlined = i;
        this.$continuation$inlined = pg4;
        this.this$Anon0 = thirdPartyRepository;
        this.$gFitWorkoutSessionList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onSuccess(Void voidR) {
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitWorkoutSession insert was successful!");
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (kc4) null), 3, (Object) null);
        Ref$IntRef ref$IntRef = this.$countSizeOfList$inlined;
        ref$IntRef.element++;
        if (ref$IntRef.element >= this.$sizeOfGFitWorkoutSessionList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitWorkoutSessionToGoogleFit");
            pg4 pg4 = this.$continuation$inlined;
            Result.a aVar = Result.Companion;
            pg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
    }
}
