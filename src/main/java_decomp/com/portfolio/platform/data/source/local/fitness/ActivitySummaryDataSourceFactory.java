package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryDataSourceFactory extends md.b<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ ActivitySummaryDao activitySummaryDao;
    @DexIgnore
    public /* final */ i42 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ yk2 fitnessHelper;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, yk2 yk2, FitnessDataRepository fitnessDataRepository2, ActivitySummaryDao activitySummaryDao2, FitnessDatabase fitnessDatabase2, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(summariesRepository2, "summariesRepository");
        wd4.b(yk2, "fitnessHelper");
        wd4.b(fitnessDataRepository2, "fitnessDataRepository");
        wd4.b(activitySummaryDao2, "activitySummaryDao");
        wd4.b(fitnessDatabase2, "fitnessDatabase");
        wd4.b(date, "createdDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        wd4.b(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = yk2;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.activitySummaryDao = activitySummaryDao2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = i42;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public md<Date, ActivitySummary> create() {
        this.localDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.activitySummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.localDataSource;
        if (activitySummaryLocalDataSource != null) {
            return activitySummaryLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
