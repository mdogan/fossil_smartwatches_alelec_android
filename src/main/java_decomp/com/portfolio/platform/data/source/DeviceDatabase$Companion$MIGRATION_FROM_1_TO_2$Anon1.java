package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zf;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends zf {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(hg hgVar) {
        wd4.b(hgVar, "database");
        hgVar.s();
        try {
            hgVar.b("CREATE TABLE `watchParam` (`prefixSerial` TEXT, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXTPRIMARY KEY(`prefixSerial`))");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(DeviceDatabase.TAG, "migrate DeviceDatabase from 1->2 failed");
            hgVar.b("DROP TABLE IF EXISTS watchParam");
            hgVar.b("CREATE TABLE IF NOT EXISTS watchParam (prefixSerial TEXT PRIMARY KEY NOT NULL, versionMajor TEXT, versionMinor TEXT, data TEXT)");
        }
        hgVar.u();
        hgVar.v();
    }
}
