package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1", f = "ActivitiesRepository.kt", l = {156}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1(this.this$Anon0, this.$start, this.$end, this.$offset, this.$limit, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            String e = sk2.e(this.$start);
            wd4.a((Object) e, "DateHelper.formatShortDate(start)");
            String e2 = sk2.e(this.$end);
            wd4.a((Object) e2, "DateHelper.formatShortDate(end)");
            int i2 = this.$offset;
            int i3 = this.$limit;
            this.label = 1;
            obj = access$getMApiService$p.getActivities(e, e2, i2, i3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
