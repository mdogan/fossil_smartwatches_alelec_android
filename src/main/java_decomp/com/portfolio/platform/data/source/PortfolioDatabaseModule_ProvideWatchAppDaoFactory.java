package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideWatchAppDaoFactory implements Factory<WatchAppDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideWatchAppDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideWatchAppDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideWatchAppDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WatchAppDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return proxyProvideWatchAppDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static WatchAppDao proxyProvideWatchAppDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        WatchAppDao provideWatchAppDao = portfolioDatabaseModule.provideWatchAppDao(dianaCustomizeDatabase);
        o44.a(provideWatchAppDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideWatchAppDao;
    }

    @DexIgnore
    public WatchAppDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
