package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.List;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.FitnessDataRepository", f = "FitnessDataRepository.kt", l = {44}, m = "pushFitnessData$app_fossilRelease")
public final class FitnessDataRepository$pushFitnessData$Anon1 extends ContinuationImpl {
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public int I$Anon1;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FitnessDataRepository$pushFitnessData$Anon1(FitnessDataRepository fitnessDataRepository, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = fitnessDataRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.pushFitnessData$app_fossilRelease((List<FitnessDataWrapper>) null, this);
    }
}
