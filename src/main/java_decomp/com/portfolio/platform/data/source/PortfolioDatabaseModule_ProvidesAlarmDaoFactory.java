package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvidesAlarmDaoFactory implements Factory<AlarmDao> {
    @DexIgnore
    public /* final */ Provider<AlarmDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesAlarmDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesAlarmDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesAlarmDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AlarmDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDatabase> provider) {
        return proxyProvidesAlarmDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static AlarmDao proxyProvidesAlarmDao(PortfolioDatabaseModule portfolioDatabaseModule, AlarmDatabase alarmDatabase) {
        AlarmDao providesAlarmDao = portfolioDatabaseModule.providesAlarmDao(alarmDatabase);
        o44.a(providesAlarmDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesAlarmDao;
    }

    @DexIgnore
    public AlarmDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
