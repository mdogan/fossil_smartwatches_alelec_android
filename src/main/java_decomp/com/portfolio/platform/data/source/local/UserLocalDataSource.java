package com.portfolio.platform.data.source.local;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserLocalDataSource extends UserDataSource {
    @DexIgnore
    public void clearAllUser() {
        en2.p.a().n().f();
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, kc4<? super Integer> kc4) {
        en2.p.a().n().c(mFUser);
        return pc4.a(200);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return en2.p.a().n().b();
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        wd4.b(mFUser, "user");
        en2.p.a().n().b(mFUser);
    }

    @DexIgnore
    public Object updateUser(MFUser mFUser, boolean z, kc4<? super ro2<MFUser>> kc4) {
        en2.p.a().n().a(mFUser);
        return new so2(getCurrentUser(), false, 2, (rd4) null);
    }
}
