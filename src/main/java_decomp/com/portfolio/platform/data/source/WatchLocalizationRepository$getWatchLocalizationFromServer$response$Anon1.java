package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1", f = "WatchLocalizationRepository.kt", l = {25}, m = "invokeSuspend")
public final class WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<WatchLocalization>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $locale;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = watchLocalizationRepository;
        this.$locale = str;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(this.this$Anon0, this.$locale, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getApi$p = this.this$Anon0.api;
            String str = this.$locale;
            this.label = 1;
            obj = access$getApi$p.getWatchLocalizationData(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
