package com.portfolio.platform.data.source;

import android.content.Context;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.obfuscated.al2;
import com.fossil.blesdk.obfuscated.ap0;
import com.fossil.blesdk.obfuscated.bp0;
import com.fossil.blesdk.obfuscated.bq0;
import com.fossil.blesdk.obfuscated.cp0;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.fp0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.ie0;
import com.fossil.blesdk.obfuscated.ie4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.oe0;
import com.fossil.blesdk.obfuscated.qe4;
import com.fossil.blesdk.obfuscated.qg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.so0;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.uc4;
import com.fossil.blesdk.obfuscated.un1;
import com.fossil.blesdk.obfuscated.vd0;
import com.fossil.blesdk.obfuscated.wb0;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xn1;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.fitness.WorkoutType;
import com.fossil.wearables.fossil.R;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public al2 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public ThirdPartyDatabase mThirdPartyDatabase;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        void onComplete();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[WorkoutType.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(al2 al2, ThirdPartyDatabase thirdPartyDatabase, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        wd4.b(al2, "mGoogleFitHelper");
        wd4.b(thirdPartyDatabase, "mThirdPartyDatabase");
        wd4.b(activitiesRepository, "mActivitiesRepository");
        wd4.b(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = al2;
        this.mThirdPartyDatabase = thirdPartyDatabase;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession next : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(next.getRealSleepMinutes(), ((long) next.getRealStartTime()) * j, j * ((long) next.getRealEndTime())));
        }
        return arrayList;
    }

    @DexIgnore
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        List<T> h = wb4.h(gFitActiveTime.getActiveTimes());
        ie4 a2 = qe4.a((ie4) qe4.d(0, h.size()), 2);
        int a3 = a2.a();
        int b = a2.b();
        int c = a2.c();
        if (c < 0 ? a3 >= b : a3 <= b) {
            while (true) {
                DataPoint H = a.H();
                H.a(((Number) h.get(a3)).longValue(), ((Number) h.get(a3 + 1)).longValue(), TimeUnit.MILLISECONDS);
                H.a(cp0.h).e("unknown");
                a.a(H);
                if (a3 == b) {
                    break;
                }
                a3 += c;
            }
        }
        wd4.a((Object) a, "dataSetForActiveMinutes");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.s);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component3 = next.component3();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(cp0.H).a(component3);
            a.a(H);
        }
        wd4.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.D);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component2 = next.component2();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(cp0.u).a(component2);
            a.a(H);
        }
        wd4.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.A);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitHeartRate next : list) {
            float component1 = next.component1();
            long component2 = next.component2();
            long component3 = next.component3();
            DataPoint H = a.H();
            H.a(component2, component3, TimeUnit.MILLISECONDS);
            H.a(cp0.p).a(component1);
            a.a(H);
        }
        wd4.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.i);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            int component1 = next.component1();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(cp0.k).f(component1);
            a.a(H);
        }
        wd4.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.s);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOCalorie next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(cp0.H).a(next.getCalorie());
                a.a(H);
            }
        }
        wd4.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.D);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWODistance next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(cp0.u).a(next.getDistance());
                a.a(H);
            }
        }
        wd4.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.A);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOHeartRate next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(cp0.p).a(next.getHeartRate());
                a.a(H);
            }
        }
        wd4.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.i);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new bp0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOStep next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(cp0.k).f(next.getStep());
                a.a(H);
            }
        }
        wd4.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final bq0 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        fp0.a aVar = new fp0.a();
        aVar.d(workoutType.name());
        aVar.a(getFitnessActivitiesType(workoutType));
        aVar.b(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        aVar.a(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        fp0 a = aVar.a();
        bq0.a aVar2 = new bq0.a();
        aVar2.a(a);
        if (!createDataSetForWOSteps.isEmpty()) {
            aVar2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            aVar2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            aVar2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            aVar2.a(createDataSetForWOHeartRates);
        }
        bq0 a2 = aVar2.a();
        wd4.a((Object) a2, "sessionBuilder.build()");
        return a2;
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeries(List<UASample> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (UASample next : list) {
            int component1 = next.component1();
            double component2 = next.component2();
            double component3 = next.component3();
            long component4 = next.component4();
            builder.addSteps(component4, component1);
            builder.addDistance(component4, component2);
            builder.addCalories(component4, component3);
        }
        return builder.build();
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeriesOldFlow(List<SampleRaw> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (SampleRaw next : list) {
            long time = next.getStartTime().getTime() / ((long) 1000);
            builder.addSteps(time, (int) next.getSteps());
            builder.addDistance(time, next.getDistance());
            builder.addCalories(time, next.getCalories());
        }
        return builder.build();
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$Anon0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return FacebookRequestErrorClassification.KEY_OTHER;
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        if (i < WorkoutType.values().length) {
            return WorkoutType.values()[i];
        }
        return WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ void saveData$default(ThirdPartyRepository thirdPartyRepository, List list, List list2, GFitActiveTime gFitActiveTime, List list3, List list4, List list5, int i, Object obj) {
        if ((i & 1) != 0) {
            list = ob4.a();
        }
        if ((i & 2) != 0) {
            list2 = ob4.a();
        }
        List list6 = list2;
        if ((i & 4) != 0) {
            gFitActiveTime = null;
        }
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        if ((i & 8) != 0) {
            list3 = ob4.a();
        }
        List list7 = list3;
        if ((i & 16) != 0) {
            list4 = ob4.a();
        }
        List list8 = list4;
        if ((i & 32) != 0) {
            list5 = ob4.a();
        }
        thirdPartyRepository.saveData(list, list6, gFitActiveTime2, list7, list8, list5);
    }

    @DexIgnore
    public static /* synthetic */ ri4 uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, bp0 bp0) {
        wd4.b(gFitSleep, "gFitSleep");
        wd4.b(bp0, "device");
        ap0.a aVar = new ap0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(bp0);
        DataSet a = DataSet.a(aVar.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint H = a.H();
        H.a(startTime, endTime, TimeUnit.MILLISECONDS);
        H.a(cp0.h).f(gFitSleep.getSleepMins());
        a.a(H);
        wd4.a((Object) a, "dataSetForSleep");
        return a;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final ThirdPartyDatabase getMThirdPartyDatabase() {
        return this.mThirdPartyDatabase;
    }

    @DexIgnore
    public final void pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        wd4.b(pushPendingThirdPartyDataCallback, "pushPendingThirdPartyDataCallback");
        uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    public final void saveData(List<GFitSample> list, List<UASample> list2, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list3, List<GFitWorkoutSession> list4, List<MFSleepSession> list5) {
        List<GFitWorkoutSession> list6 = list4;
        List<MFSleepSession> list7 = list5;
        wd4.b(list, "listGFitSample");
        wd4.b(list2, "listUASample");
        wd4.b(list3, "listGFitHeartRate");
        wd4.b(list6, "listGFitWorkoutSession");
        wd4.b(list7, "listMFSleepSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("saveData = listGFitSample=");
        sb.append(list);
        sb.append(", listUASample=");
        sb.append(list2);
        sb.append(", gFitActiveTime=");
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        sb.append(gFitActiveTime);
        sb.append(", listGFitHeartRate=");
        sb.append(list3);
        sb.append(", listGFitWorkoutSession=");
        sb.append(list6);
        sb.append(", listMFSleepSession= ");
        sb.append(list7);
        local.d(TAG, sb.toString());
        this.mThirdPartyDatabase.runInTransaction((Runnable) new ThirdPartyRepository$saveData$Anon1(this, list, gFitActiveTime2, list3, list6, list2, list7));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00e5  */
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, kc4<Object> kc4) {
        Object e;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitActiveTimeToGoogleFit");
        he0.a aVar = new he0.a(getMPortfolioApp());
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.b, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.a, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write"});
        he0 a = aVar.a();
        vd0 a2 = a.a();
        wd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            wd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitActiveTime to Google Fit");
                int size = list.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (GFitActiveTime gFitActiveTime : list) {
                    ie0<Status> a3 = so0.c.a(a, createDataSetForActiveMinutes(gFitActiveTime, str));
                    he0 he0 = a;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    int i = size;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitActiveTime, a, ref$IntRef, size, qg4, this, list, str);
                    a3.a((oe0<? super Status>) thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    a = he0;
                    size = i;
                }
                e = qg4.e();
                if (e == oc4.a()) {
                    uc4.c(kc4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitActiveTime to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitActiveTimeToGoogleFit");
        if (qg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            qg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = qg4.e();
        if (e == oc4.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ea  */
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, kc4<Object> kc4) {
        Object e;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitHeartRateToGoogleFit");
        he0.a aVar = new he0.a(getMPortfolioApp());
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.b, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.a, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.d, new Scope[0]);
        aVar.a(new Scope("https://www.googleapis.com/auth/fitness.body.write"));
        he0 a = aVar.a();
        vd0 a2 = a.a();
        wd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            wd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitHeartRate to Google Fit");
                List<List<T>> b = wb4.b(list, 500);
                int size = b.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (List list2 : b) {
                    String str2 = str;
                    ie0<Status> a3 = so0.c.a(a, createDataSetForHeartRates(list2, str2));
                    he0 he0 = a;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(list2, a, ref$IntRef, size, qg4, this, list, str2);
                    a3.a((oe0<? super Status>) thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    List<GFitHeartRate> list3 = list;
                    a = he0;
                }
                e = qg4.e();
                if (e == oc4.a()) {
                    uc4.c(kc4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitHeartRate to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitHeartRateToGoogleFit");
        if (qg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            qg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = qg4.e();
        if (e == oc4.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x012e  */
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, kc4<Object> kc4) {
        Object e;
        ThirdPartyRepository thirdPartyRepository = this;
        String str2 = str;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        he0.a aVar = new he0.a(getMPortfolioApp());
        int i = 0;
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.b, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.a, new Scope[0]);
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write"});
        he0 a = aVar.a();
        vd0 a2 = a.a();
        wd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            wd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
                List<List<T>> b = wb4.b(list, 500);
                int size = b.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (List list2 : b) {
                    ArrayList arrayList = new ArrayList();
                    DataSet access$createDataSetForSteps = thirdPartyRepository.createDataSetForSteps(list2, str2);
                    DataSet access$createDataSetForDistances = thirdPartyRepository.createDataSetForDistances(list2, str2);
                    DataSet access$createDataSetForCalories = thirdPartyRepository.createDataSetForCalories(list2, str2);
                    arrayList.add(access$createDataSetForSteps);
                    arrayList.add(access$createDataSetForDistances);
                    arrayList.add(access$createDataSetForCalories);
                    int size2 = arrayList.size();
                    Ref$IntRef ref$IntRef2 = new Ref$IntRef();
                    ref$IntRef2.element = i;
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                        ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(ref$IntRef2, size2, list2, a, ref$IntRef, qg4, size, this, list, str);
                        so0.c.a(a, (DataSet) it.next()).a(thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                        List<GFitSample> list3 = list;
                        ref$IntRef2 = ref$IntRef2;
                        list2 = list2;
                        ref$IntRef = ref$IntRef;
                        i = 0;
                    }
                    thirdPartyRepository = this;
                    List<GFitSample> list4 = list;
                }
                e = qg4.e();
                if (e == oc4.a()) {
                    uc4.c(kc4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitSample to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
        if (qg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            qg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = qg4.e();
        if (e == oc4.a()) {
        }
        return e;
    }

    @DexIgnore
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, kc4<Object> kc4) {
        String str2 = str;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount a = wb0.a((Context) getMPortfolioApp());
        if (a != null) {
            int size = list.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            String nameBySerial = DeviceIdentityUtils.getNameBySerial(str);
            PortfolioApp mPortfolioApp2 = getMPortfolioApp();
            int i = R.string.app_name;
            bp0 bp0 = new bp0(mPortfolioApp2.getString(R.string.app_name), nameBySerial, str2, 3);
            ArrayList arrayList = new ArrayList();
            for (GFitSleep gFitSleep : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(gFitSleep, bp0);
                fp0.a aVar = new fp0.a();
                aVar.c(str2 + new DateTime());
                aVar.d(getMPortfolioApp().getString(i));
                aVar.b("User Sleep");
                aVar.b(gFitSleep.getStartTime(), TimeUnit.MILLISECONDS);
                aVar.a(gFitSleep.getEndTime(), TimeUnit.MILLISECONDS);
                aVar.a("sleep");
                fp0 a2 = aVar.a();
                bq0.a aVar2 = new bq0.a();
                aVar2.a(a2);
                aVar2.a(createDataSetForSleepData);
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                int i2 = size;
                int i3 = size;
                xn1<Void> a3 = so0.a(getMPortfolioApp(), a).a(aVar2.a());
                bp0 bp02 = bp0;
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitSleep, bp0, a, ref$IntRef, arrayList, i2, qg4, this, list, str);
                a3.a((un1<? super Void>) thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                a3.a((tn1) new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(bp02, a, ref$IntRef, arrayList, i3, qg4, this, list, str));
                str2 = str;
                size = i3;
                bp0 = bp02;
                i = R.string.app_name;
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (qg4.isActive()) {
                Result.a aVar3 = Result.Companion;
                qg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = qg4.e();
        if (e == oc4.a()) {
            uc4.c(kc4);
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0119  */
    public final /* synthetic */ Object saveGFitWorkoutSessionToGoogleFit(List<GFitWorkoutSession> list, String str, kc4<Object> kc4) {
        Object e;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitWorkoutSessionToGoogleFit");
        he0.a aVar = new he0.a(getMPortfolioApp());
        aVar.a((ee0<? extends ee0.d.C0011d>) so0.b, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.body.write"});
        he0 a = aVar.a();
        vd0 a2 = a.a();
        wd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            wd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                GoogleSignInAccount a3 = wb0.a((Context) getMPortfolioApp());
                if (a3 != null) {
                    FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitWorkoutSession to Google Fit");
                    int size = list.size();
                    Ref$IntRef ref$IntRef = new Ref$IntRef();
                    ref$IntRef.element = 0;
                    Iterator<T> it = list.iterator();
                    while (it.hasNext()) {
                        GFitWorkoutSession gFitWorkoutSession = (GFitWorkoutSession) it.next();
                        Iterator<T> it2 = it;
                        ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                        xn1<Void> a4 = so0.a(getMPortfolioApp(), a3).a(createWorkoutSession(gFitWorkoutSession, str));
                        ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitWorkoutSession, a3, ref$IntRef, size, qg4, this, list, str);
                        a4.a((un1<? super Void>) thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                        a4.a((tn1) new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a3, ref$IntRef, size, qg4, this, list, str));
                        it = it2;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
                    FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
                    if (qg4.isActive()) {
                        Result.a aVar2 = Result.Companion;
                        qg4.resumeWith(Result.m3constructorimpl((Object) null));
                    }
                }
                e = qg4.e();
                if (e == oc4.a()) {
                    uc4.c(kc4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitWorkoutSession to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
        if (qg4.isActive()) {
            Result.a aVar3 = Result.Companion;
            qg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = qg4.e();
        if (e == oc4.a()) {
        }
        return e;
    }

    @DexIgnore
    public final /* synthetic */ Object saveToUA(kc4<Object> kc4) {
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveToUA");
        List<UASample> allUASample = getMThirdPartyDatabase().getUASampleDao().getAllUASample();
        if (true ^ allUASample.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Sending UASample to UnderAmour");
            List<List<T>> b = wb4.b(allUASample, 500);
            int size = b.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            for (List list : b) {
                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(generateTimeSeries(list), list, (kc4) null, ref$IntRef, size, qg4, this), 3, (Object) null);
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "UASample is empty");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveToUA");
            if (qg4.isActive()) {
                Result.a aVar = Result.Companion;
                qg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = qg4.e();
        if (e == oc4.a()) {
            uc4.c(kc4);
        }
        return e;
    }

    @DexIgnore
    public final /* synthetic */ Object saveToUAOldFlow(kc4<Object> kc4) {
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveToUAOldFlow");
        List<SampleRaw> activityListByUAPinType = getMActivitiesRepository().getActivityListByUAPinType(1);
        if (true ^ activityListByUAPinType.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Sending SampleRaw to UnderAmour");
            List<List<T>> b = wb4.b(activityListByUAPinType, 500);
            int size = b.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            for (List list : b) {
                ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$saveToUAOldFlow$$inlined$suspendCancellableCoroutine$lambda$Anon1(generateTimeSeriesOldFlow(list), list, (kc4) null, ref$IntRef, size, qg4, this), 3, (Object) null);
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "All SampleRaw were synced!");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveToUAOldFlow");
            if (qg4.isActive()) {
                Result.a aVar = Result.Companion;
                qg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = qg4.e();
        if (e == oc4.a()) {
            uc4.c(kc4);
        }
        return e;
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        wd4.b(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        wd4.b(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void setMThirdPartyDatabase(ThirdPartyDatabase thirdPartyDatabase) {
        wd4.b(thirdPartyDatabase, "<set-?>");
        this.mThirdPartyDatabase = thirdPartyDatabase;
    }

    @DexIgnore
    public final ri4 uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$uploadData$Anon1(this, pushPendingThirdPartyDataCallback, (kc4) null), 3, (Object) null);
    }
}
