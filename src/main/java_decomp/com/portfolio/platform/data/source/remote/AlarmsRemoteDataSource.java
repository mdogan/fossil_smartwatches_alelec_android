package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.response.ResponseKt;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "AlarmsRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final uz1 intArray2JsonArray(int[] iArr) {
        uz1 uz1 = new uz1();
        if (iArr == null) {
            iArr = new int[0];
        }
        Calendar instance = Calendar.getInstance();
        int length = iArr.length;
        int i = 0;
        while (i < length) {
            instance.set(7, iArr[i]);
            String displayName = instance.getDisplayName(7, 2, Locale.US);
            wd4.a((Object) displayName, "calendar.getDisplayName(\u2026Calendar.LONG, Locale.US)");
            if (displayName != null) {
                String substring = displayName.substring(0, 3);
                wd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (substring != null) {
                    String lowerCase = substring.toLowerCase();
                    wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    uz1.a(lowerCase);
                    i++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return uz1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarm(String str, kc4<? super ro2<Void>> kc4) {
        AlarmsRemoteDataSource$deleteAlarm$Anon1 alarmsRemoteDataSource$deleteAlarm$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof AlarmsRemoteDataSource$deleteAlarm$Anon1) {
            alarmsRemoteDataSource$deleteAlarm$Anon1 = (AlarmsRemoteDataSource$deleteAlarm$Anon1) kc4;
            int i2 = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
                Object a = oc4.a();
                i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1(this, str, (kc4) null);
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon1 = str;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarm$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "deleteAlarm onResponse: response = " + ro2);
                if (!(ro2 instanceof so2)) {
                    return new so2((Object) null, false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        alarmsRemoteDataSource$deleteAlarm$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$Anon1(this, kc4);
        Object obj2 = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
        Object a2 = oc4.a();
        i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "deleteAlarm onResponse: response = " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarms(List<Alarm> list, kc4<? super ro2<Void>> kc4) {
        AlarmsRemoteDataSource$deleteAlarms$Anon1 alarmsRemoteDataSource$deleteAlarms$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof AlarmsRemoteDataSource$deleteAlarms$Anon1) {
            alarmsRemoteDataSource$deleteAlarms$Anon1 = (AlarmsRemoteDataSource$deleteAlarms$Anon1) kc4;
            int i2 = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
                Object a = oc4.a();
                i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    uz1 uz1 = new uz1();
                    for (Alarm id : list) {
                        uz1.a(id.getId());
                    }
                    yz1.a("_ids", (JsonElement) uz1);
                    AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1(this, yz1, (kc4) null);
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon1 = list;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon2 = yz1;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon3 = uz1;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    uz1 uz12 = (uz1) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon3;
                    yz1 yz12 = (yz1) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon2;
                    List list2 = (List) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "deleteAlarms onResponse: response = " + ro2);
                if (!(ro2 instanceof so2)) {
                    return new so2((Object) null, false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        alarmsRemoteDataSource$deleteAlarms$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$Anon1(this, kc4);
        Object obj2 = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
        Object a2 = oc4.a();
        i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "deleteAlarms onResponse: response = " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getAlarms(kc4<? super ro2<List<Alarm>>> kc4) {
        AlarmsRemoteDataSource$getAlarms$Anon1 alarmsRemoteDataSource$getAlarms$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof AlarmsRemoteDataSource$getAlarms$Anon1) {
            alarmsRemoteDataSource$getAlarms$Anon1 = (AlarmsRemoteDataSource$getAlarms$Anon1) kc4;
            int i2 = alarmsRemoteDataSource$getAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$getAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$getAlarms$Anon1.result;
                Object a = oc4.a();
                i = alarmsRemoteDataSource$getAlarms$Anon1.label;
                List list = null;
                if (i != 0) {
                    za4.a(obj);
                    AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1 alarmsRemoteDataSource$getAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1(this, (kc4) null);
                    alarmsRemoteDataSource$getAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$getAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$getAlarms$repoResponse$Anon1, alarmsRemoteDataSource$getAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$getAlarms$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "getAlarms onResponse: response = " + ro2);
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    ApiResponse apiResponse = (ApiResponse) so2.a();
                    if (apiResponse != null) {
                        list = apiResponse.get_items();
                    }
                    return new so2(list, so2.b());
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        alarmsRemoteDataSource$getAlarms$Anon1 = new AlarmsRemoteDataSource$getAlarms$Anon1(this, kc4);
        Object obj2 = alarmsRemoteDataSource$getAlarms$Anon1.result;
        Object a2 = oc4.a();
        i = alarmsRemoteDataSource$getAlarms$Anon1.label;
        List list2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getAlarms onResponse: response = " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object upsertAlarms(List<Alarm> list, kc4<? super ro2<List<Alarm>>> kc4) {
        AlarmsRemoteDataSource$upsertAlarms$Anon1 alarmsRemoteDataSource$upsertAlarms$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof AlarmsRemoteDataSource$upsertAlarms$Anon1) {
            alarmsRemoteDataSource$upsertAlarms$Anon1 = (AlarmsRemoteDataSource$upsertAlarms$Anon1) kc4;
            int i2 = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$upsertAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
                Object a = oc4.a();
                i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    uz1 uz1 = new uz1();
                    for (Alarm next : list) {
                        yz1 yz1 = new yz1();
                        yz1.a(AppFilter.COLUMN_HOUR, (Number) pc4.a(next.getHour()));
                        yz1.a(MFSleepGoal.COLUMN_MINUTE, (Number) pc4.a(next.getMinute()));
                        yz1.a("id", next.getUri());
                        yz1.a("isRepeated", pc4.a(next.isRepeated()));
                        yz1.a("isActive", pc4.a(next.isActive()));
                        yz1.a("title", next.getTitle());
                        yz1.a(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS, (JsonElement) intArray2JsonArray(next.getDays()));
                        uz1.a((JsonElement) yz1);
                    }
                    yz1 yz12 = new yz1();
                    yz12.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) uz1);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "upsertAlarms: body " + yz12);
                    AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1(this, yz12, (kc4) null);
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon1 = list;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon2 = uz1;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon3 = yz12;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1, alarmsRemoteDataSource$upsertAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz13 = (yz1) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon3;
                    uz1 uz12 = (uz1) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon2;
                    List list2 = (List) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "upsertAlarms onResponse: response = " + ro2);
                if (!(ro2 instanceof so2)) {
                    ApiResponse apiResponse = (ApiResponse) ((so2) ro2).a();
                    return new so2(apiResponse != null ? apiResponse.get_items() : null, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        alarmsRemoteDataSource$upsertAlarms$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$Anon1(this, kc4);
        Object obj2 = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
        Object a2 = oc4.a();
        i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local22.d(str22, "upsertAlarms onResponse: response = " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }
}
