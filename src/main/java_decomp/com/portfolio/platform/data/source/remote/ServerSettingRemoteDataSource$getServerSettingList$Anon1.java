package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.response.ResponseKt;
import java.net.SocketTimeoutException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$Anon1", f = "ServerSettingRemoteDataSource.kt", l = {38}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1(ServerSettingRemoteDataSource serverSettingRemoteDataSource, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this.this$Anon0, this.$callback, kc4);
        serverSettingRemoteDataSource$getServerSettingList$Anon1.p$ = (lh4) obj;
        return serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = ResponseKt.a(serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            FLogger.INSTANCE.getLocal().e(ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((ServerSettingList) ((so2) ro2).a());
        } else if (ro2 instanceof qo2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            qo2 qo2 = (qo2) ro2;
            sb.append(qo2.a());
            local.e(tAG$app_fossilRelease, sb.toString());
            if (qo2.d() instanceof SocketTimeoutException) {
                this.$callback.onFailed(MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return cb4.a;
    }
}
