package com.portfolio.platform.data.source;

import android.content.Context;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wk2;
import com.fossil.blesdk.obfuscated.wm2;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ WatchFaceDao watchFaceDao;
    @DexIgnore
    public /* final */ WatchFaceRemoteDataSource watchFaceRemoteDataSource;

    @DexIgnore
    public WatchFaceRepository(Context context2, WatchFaceDao watchFaceDao2, WatchFaceRemoteDataSource watchFaceRemoteDataSource2) {
        wd4.b(context2, "context");
        wd4.b(watchFaceDao2, "watchFaceDao");
        wd4.b(watchFaceRemoteDataSource2, "watchFaceRemoteDataSource");
        this.context = context2;
        this.watchFaceDao = watchFaceDao2;
        this.watchFaceRemoteDataSource = watchFaceRemoteDataSource2;
        String simpleName = WatchFaceRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "WatchFaceRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final void downloadFromURL(String str, String str2, List<String> list) {
        if (TextUtils.isEmpty(str)) {
            return;
        }
        if (str != null) {
            String str3 = str2 + File.separator + wm2.a(str);
            if (!wk2.a.a(str3) && !list.contains(str)) {
                list.add(str);
                processDownloadAndStore(str, str3);
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    private final sh4<Boolean> processDownloadAndStore(String str, String str2) {
        return mg4.a(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new WatchFaceRepository$processDownloadAndStore$Anon1(this, str, str2, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void deleteWatchFacesWithSerial(String str) {
        wd4.b(str, "serial");
        this.watchFaceDao.deleteWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public final WatchFace getWatchFaceWithId(String str) {
        wd4.b(str, "id");
        return this.watchFaceDao.getWatchFaceWithId(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getWatchFacesFromServer(String str, kc4<? super cb4> kc4) {
        WatchFaceRepository$getWatchFacesFromServer$Anon1 watchFaceRepository$getWatchFacesFromServer$Anon1;
        int i;
        WatchFaceRepository watchFaceRepository;
        ro2 ro2;
        if (kc4 instanceof WatchFaceRepository$getWatchFacesFromServer$Anon1) {
            watchFaceRepository$getWatchFacesFromServer$Anon1 = (WatchFaceRepository$getWatchFacesFromServer$Anon1) kc4;
            int i2 = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchFaceRepository$getWatchFacesFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
                Object a = oc4.a();
                i = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    WatchFaceRemoteDataSource watchFaceRemoteDataSource2 = this.watchFaceRemoteDataSource;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon0 = this;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon1 = str;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.label = 1;
                    obj = watchFaceRemoteDataSource2.getWatchFacesFromServer(str, watchFaceRepository$getWatchFacesFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchFaceRepository = this;
                } else if (i == 1) {
                    str = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon1;
                    watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList<WatchFace> arrayList = (ArrayList) ((so2) ro2).a();
                    if (arrayList != null) {
                        List<WatchFace> watchFacesWithSerial = watchFaceRepository.getWatchFacesWithSerial(str);
                        ArrayList arrayList2 = new ArrayList(pb4.a(arrayList, 10));
                        for (WatchFace serial : arrayList) {
                            serial.setSerial(str);
                            arrayList2.add(cb4.a);
                        }
                        if (!wd4.a((Object) watchFacesWithSerial, (Object) arrayList)) {
                            watchFaceRepository.watchFaceDao.insertAllWatchFaces(arrayList);
                        }
                        String file = watchFaceRepository.context.getFilesDir().toString();
                        wd4.a((Object) file, "context.filesDir.toString()");
                        ArrayList arrayList3 = new ArrayList();
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            WatchFace watchFace = (WatchFace) it.next();
                            ArrayList<RingStyleItem> component3 = watchFace.component3();
                            Background component4 = watchFace.component4();
                            String component5 = watchFace.component5();
                            watchFaceRepository.downloadFromURL(component4.getData().getUrl(), file, arrayList3);
                            watchFaceRepository.downloadFromURL(component4.getData().getPreviewUrl(), file, arrayList3);
                            watchFaceRepository.downloadFromURL(component5, file, arrayList3);
                            if (component3 != null) {
                                for (RingStyleItem ringStyleItem : component3) {
                                    watchFaceRepository.downloadFromURL(ringStyleItem.getRingStyle().getData().getUrl(), file, arrayList3);
                                    watchFaceRepository.downloadFromURL(ringStyleItem.getRingStyle().getData().getPreviewUrl(), file, arrayList3);
                                }
                            }
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().e(watchFaceRepository.TAG, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_FAILED);
                }
                return cb4.a;
            }
        }
        watchFaceRepository$getWatchFacesFromServer$Anon1 = new WatchFaceRepository$getWatchFacesFromServer$Anon1(this, kc4);
        Object obj2 = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
        Object a2 = oc4.a();
        i = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithSerial(String str) {
        wd4.b(str, "serial");
        return this.watchFaceDao.getWatchFacesLiveData(str);
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithSerial(String str) {
        wd4.b(str, "serial");
        return this.watchFaceDao.getWatchFacesWithSerial(str);
    }
}
