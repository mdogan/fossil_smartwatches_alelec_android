package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.w72;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceDao_Impl implements WatchFaceDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfWatchFace;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteWatchFacesWithSerial;
    @DexIgnore
    public /* final */ w72 __watchFaceTypeConverter; // = new w72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<WatchFace> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watch_face`(`id`,`name`,`ringStyleItems`,`background`,`previewUrl`,`serial`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, WatchFace watchFace) {
            if (watchFace.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, watchFace.getId());
            }
            if (watchFace.getName() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, watchFace.getName());
            }
            String a = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getRingStyleItems());
            if (a == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, a);
            }
            String a2 = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getBackground());
            if (a2 == null) {
                lgVar.a(4);
            } else {
                lgVar.a(4, a2);
            }
            if (watchFace.getPreviewUrl() == null) {
                lgVar.a(5);
            } else {
                lgVar.a(5, watchFace.getPreviewUrl());
            }
            if (watchFace.getSerial() == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, watchFace.getSerial());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watch_face WHERE serial = ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchFace> call() throws Exception {
            Cursor a = cg.a(WatchFaceDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "id");
                int b2 = bg.b(a, "name");
                int b3 = bg.b(a, "ringStyleItems");
                int b4 = bg.b(a, Explore.COLUMN_BACKGROUND);
                int b5 = bg.b(a, "previewUrl");
                int b6 = bg.b(a, "serial");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchFace(a.getString(b), a.getString(b2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(a.getString(b3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchFaceDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfWatchFace = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteWatchFacesWithSerial = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteWatchFacesWithSerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteWatchFacesWithSerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFacesWithSerial.release(acquire);
        }
    }

    @DexIgnore
    public WatchFace getWatchFaceWithId(String str) {
        WatchFace watchFace;
        String str2 = str;
        vf b = vf.b("SELECT * FROM watch_face WHERE id = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "name");
            int b4 = bg.b(a, "ringStyleItems");
            int b5 = bg.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = bg.b(a, "previewUrl");
            int b7 = bg.b(a, "serial");
            if (a.moveToFirst()) {
                watchFace = new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7));
            } else {
                watchFace = null;
            }
            return watchFace;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchFace>> getWatchFacesLiveData(String str) {
        vf b = vf.b("SELECT * FROM watch_face WHERE serial = ? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"watch_face"}, false, new Anon3(b));
    }

    @DexIgnore
    public List<WatchFace> getWatchFacesWithSerial(String str) {
        String str2 = str;
        vf b = vf.b("SELECT * FROM watch_face WHERE serial = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "name");
            int b4 = bg.b(a, "ringStyleItems");
            int b5 = bg.b(a, Explore.COLUMN_BACKGROUND);
            int b6 = bg.b(a, "previewUrl");
            int b7 = bg.b(a, "serial");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchFace(a.getString(b2), a.getString(b3), this.__watchFaceTypeConverter.b(a.getString(b4)), this.__watchFaceTypeConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertAllWatchFaces(List<WatchFace> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
