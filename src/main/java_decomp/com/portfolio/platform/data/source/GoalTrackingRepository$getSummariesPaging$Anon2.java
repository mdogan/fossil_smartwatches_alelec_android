package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummariesPaging$Anon2 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummariesPaging$Anon2(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        GoalTrackingSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
        if (a != null) {
            a.invalidate();
        }
    }
}
