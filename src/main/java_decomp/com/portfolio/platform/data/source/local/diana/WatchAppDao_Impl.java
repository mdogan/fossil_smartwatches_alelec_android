package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.v72;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppDao_Impl implements WatchAppDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfWatchApp;
    @DexIgnore
    public /* final */ xf __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ v72 __stringArrayConverter; // = new v72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<WatchApp> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchApp`(`watchappId`,`name`,`nameKey`,`description`,`descriptionKey`,`categories`,`icon`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, WatchApp watchApp) {
            if (watchApp.getWatchappId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, watchApp.getWatchappId());
            }
            if (watchApp.getName() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, watchApp.getName());
            }
            if (watchApp.getNameKey() == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, watchApp.getNameKey());
            }
            if (watchApp.getDescription() == null) {
                lgVar.a(4);
            } else {
                lgVar.a(4, watchApp.getDescription());
            }
            if (watchApp.getDescriptionKey() == null) {
                lgVar.a(5);
            } else {
                lgVar.a(5, watchApp.getDescriptionKey());
            }
            String a = WatchAppDao_Impl.this.__stringArrayConverter.a(watchApp.getCategories());
            if (a == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, a);
            }
            if (watchApp.getIcon() == null) {
                lgVar.a(7);
            } else {
                lgVar.a(7, watchApp.getIcon());
            }
            if (watchApp.getUpdatedAt() == null) {
                lgVar.a(8);
            } else {
                lgVar.a(8, watchApp.getUpdatedAt());
            }
            if (watchApp.getCreatedAt() == null) {
                lgVar.a(9);
            } else {
                lgVar.a(9, watchApp.getCreatedAt());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchApp";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchApp> call() throws Exception {
            Cursor a = cg.a(WatchAppDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "watchappId");
                int b2 = bg.b(a, "name");
                int b3 = bg.b(a, "nameKey");
                int b4 = bg.b(a, "description");
                int b5 = bg.b(a, "descriptionKey");
                int b6 = bg.b(a, "categories");
                int b7 = bg.b(a, "icon");
                int b8 = bg.b(a, "updatedAt");
                int b9 = bg.b(a, "createdAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchApp(a.getString(b), a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), WatchAppDao_Impl.this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfWatchApp = new Anon1(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public List<WatchApp> getAllWatchApp() {
        vf b = vf.b("SELECT * FROM watchApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "watchappId");
            int b3 = bg.b(a, "name");
            int b4 = bg.b(a, "nameKey");
            int b5 = bg.b(a, "description");
            int b6 = bg.b(a, "descriptionKey");
            int b7 = bg.b(a, "categories");
            int b8 = bg.b(a, "icon");
            int b9 = bg.b(a, "updatedAt");
            int b10 = bg.b(a, "createdAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchApp>> getAllWatchAppAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchApp"}, false, new Anon3(vf.b("SELECT * FROM watchApp", 0)));
    }

    @DexIgnore
    public WatchApp getWatchAppById(String str) {
        WatchApp watchApp;
        String str2 = str;
        vf b = vf.b("SELECT * FROM watchApp WHERE watchappId=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "watchappId");
            int b3 = bg.b(a, "name");
            int b4 = bg.b(a, "nameKey");
            int b5 = bg.b(a, "description");
            int b6 = bg.b(a, "descriptionKey");
            int b7 = bg.b(a, "categories");
            int b8 = bg.b(a, "icon");
            int b9 = bg.b(a, "updatedAt");
            int b10 = bg.b(a, "createdAt");
            if (a.moveToFirst()) {
                watchApp = new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10));
            } else {
                watchApp = null;
            }
            return watchApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<WatchApp> getWatchAppByIds(List<String> list) {
        StringBuilder a = eg.a();
        a.append("SELECT * FROM watchApp WHERE watchappId IN (");
        int size = list.size();
        eg.a(a, size);
        a.append(")");
        vf b = vf.b(a.toString(), size + 0);
        int i = 1;
        for (String next : list) {
            if (next == null) {
                b.a(i);
            } else {
                b.a(i, next);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a2, "watchappId");
            int b3 = bg.b(a2, "name");
            int b4 = bg.b(a2, "nameKey");
            int b5 = bg.b(a2, "description");
            int b6 = bg.b(a2, "descriptionKey");
            int b7 = bg.b(a2, "categories");
            int b8 = bg.b(a2, "icon");
            int b9 = bg.b(a2, "updatedAt");
            int b10 = bg.b(a2, "createdAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new WatchApp(a2.getString(b2), a2.getString(b3), a2.getString(b4), a2.getString(b5), a2.getString(b6), this.__stringArrayConverter.a(a2.getString(b7)), a2.getString(b8), a2.getString(b9), a2.getString(b10)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<WatchApp> queryWatchAppByName(String str) {
        String str2 = str;
        vf b = vf.b("SELECT * FROM watchApp WHERE name LIKE '%' || ? || '%'", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "watchappId");
            int b3 = bg.b(a, "name");
            int b4 = bg.b(a, "nameKey");
            int b5 = bg.b(a, "description");
            int b6 = bg.b(a, "descriptionKey");
            int b7 = bg.b(a, "categories");
            int b8 = bg.b(a, "icon");
            int b9 = bg.b(a, "updatedAt");
            int b10 = bg.b(a, "createdAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertWatchAppList(List<WatchApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
