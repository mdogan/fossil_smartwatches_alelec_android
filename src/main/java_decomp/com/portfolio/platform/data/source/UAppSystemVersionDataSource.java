package com.portfolio.platform.data.source;

import com.portfolio.platform.data.legacy.threedotzero.UAppSystemVersionModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface UAppSystemVersionDataSource {
    @DexIgnore
    void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel);

    @DexIgnore
    UAppSystemVersionModel getUAppSystemVersionModel(String str);
}
