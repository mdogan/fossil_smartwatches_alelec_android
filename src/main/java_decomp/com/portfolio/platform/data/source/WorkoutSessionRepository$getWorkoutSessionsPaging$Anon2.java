package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
        super(0);
        this.$sourceFactory = workoutSessionDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        WorkoutSessionLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
        if (a != null) {
            a.invalidate();
        }
    }
}
