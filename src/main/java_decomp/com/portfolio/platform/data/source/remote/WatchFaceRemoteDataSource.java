package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qm4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRemoteDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore
    public WatchFaceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "api");
        this.api = apiServiceV2;
        String simpleName = WatchFaceRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "WatchFaceRemoteDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final Object downloadWatchFaceFile(String str, String str2, kc4<? super ro2<qm4>> kc4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, "download: " + str + " path: " + str2);
        return ResponseKt.a(new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this, str, (kc4) null), kc4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getWatchFacesFromServer(String str, kc4<? super ro2<ArrayList<WatchFace>>> kc4) {
        WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1;
        int i;
        WatchFaceRemoteDataSource watchFaceRemoteDataSource;
        ro2 ro2;
        if (kc4 instanceof WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) {
            watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = (WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1) kc4;
            int i2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
                Object a = oc4.a();
                i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1(this, str, (kc4) null);
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon0 = this;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon1 = str;
                    watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(watchFaceRemoteDataSource$getWatchFacesFromServer$response$Anon1, watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchFaceRemoteDataSource = this;
                } else if (i == 1) {
                    String str2 = (String) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon1;
                    watchFaceRemoteDataSource = (WatchFaceRemoteDataSource) watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = watchFaceRemoteDataSource.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("data: ");
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 != null) {
                        sb.append(((ApiResponse) a2).get_items());
                        sb.append(" - isFromCache: ");
                        sb.append(so2.b());
                        local.d(str3, sb.toString());
                        List list = ((ApiResponse) so2.a()).get_items();
                        if (!(list instanceof ArrayList)) {
                            list = null;
                        }
                        return new so2((ArrayList) list, so2.b());
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 = new WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1(this, kc4);
        Object obj2 = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.result;
        Object a3 = oc4.a();
        i = watchFaceRemoteDataSource$getWatchFacesFromServer$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
