package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getActivityStatistic$Anon1 extends NetworkBoundResource<ActivityStatistic, ActivityStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$getActivityStatistic$Anon1(SummariesRepository summariesRepository, boolean z) {
        this.this$Anon0 = summariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(kc4<? super cs4<ActivityStatistic>> kc4) {
        return this.this$Anon0.mApiServiceV2.getActivityStatistic(kc4);
    }

    @DexIgnore
    public LiveData<ActivityStatistic> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivityStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivityStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(ActivityStatistic activityStatistic) {
        wd4.b(activityStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivityStatistic - saveCallResult -- item=" + activityStatistic);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1(this, activityStatistic, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(ActivityStatistic activityStatistic) {
        return this.$shouldFetch;
    }
}
