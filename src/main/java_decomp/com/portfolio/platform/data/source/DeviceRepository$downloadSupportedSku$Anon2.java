package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2", f = "DeviceRepository.kt", l = {158}, m = "invokeSuspend")
public final class DeviceRepository$downloadSupportedSku$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadSupportedSku$Anon2(DeviceRepository deviceRepository, int i, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deviceRepository;
        this.$offset = i;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeviceRepository$downloadSupportedSku$Anon2 deviceRepository$downloadSupportedSku$Anon2 = new DeviceRepository$downloadSupportedSku$Anon2(this.this$Anon0, this.$offset, kc4);
        deviceRepository$downloadSupportedSku$Anon2.p$ = (lh4) obj;
        return deviceRepository$downloadSupportedSku$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceRepository$downloadSupportedSku$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            if (this.this$Anon0.downloadSupportedSku(this.$offset + 100, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
