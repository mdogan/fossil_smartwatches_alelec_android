package com.portfolio.platform.data.source.local.diana;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DianaCustomizeDatabase extends RoomDatabase {
    @DexIgnore
    public abstract ComplicationDao getComplicationDao();

    @DexIgnore
    public abstract ComplicationLastSettingDao getComplicationSettingDao();

    @DexIgnore
    public abstract DianaPresetDao getPresetDao();

    @DexIgnore
    public abstract WatchAppDao getWatchAppDao();

    @DexIgnore
    public abstract WatchAppLastSettingDao getWatchAppSettingDao();

    @DexIgnore
    public abstract WatchFaceDao getWatchFaceDao();
}
