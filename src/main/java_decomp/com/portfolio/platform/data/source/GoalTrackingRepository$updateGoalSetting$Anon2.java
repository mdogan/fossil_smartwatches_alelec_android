package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon2", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$updateGoalSetting$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository.UpdateGoalSettingCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ GoalSetting $goalSetting;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$updateGoalSetting$Anon2(GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback, GoalSetting goalSetting, kc4 kc4) {
        super(2, kc4);
        this.$callback = updateGoalSettingCallback;
        this.$goalSetting = goalSetting;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingRepository$updateGoalSetting$Anon2 goalTrackingRepository$updateGoalSetting$Anon2 = new GoalTrackingRepository$updateGoalSetting$Anon2(this.$callback, this.$goalSetting, kc4);
        goalTrackingRepository$updateGoalSetting$Anon2.p$ = (lh4) obj;
        return goalTrackingRepository$updateGoalSetting$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$updateGoalSetting$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback = this.$callback;
            if (updateGoalSettingCallback == null) {
                return null;
            }
            updateGoalSettingCallback.onSuccess(new so2(this.$goalSetting, false, 2, (rd4) null));
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
