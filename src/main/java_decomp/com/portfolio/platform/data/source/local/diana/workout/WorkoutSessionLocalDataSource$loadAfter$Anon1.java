package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.helper.PagingRequestHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$Anon0;

    @DexIgnore
    public WorkoutSessionLocalDataSource$loadAfter$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.this$Anon0 = workoutSessionLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$Anon0;
        wd4.a((Object) aVar, "helperCallback");
        ri4 unused = workoutSessionLocalDataSource.loadData(aVar, this.this$Anon0.mOffset);
    }
}
