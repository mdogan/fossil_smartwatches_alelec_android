package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationsRepository_Factory implements Factory<NotificationsRepository> {
    @DexIgnore
    public /* final */ Provider<NotificationsDataSource> mappingLocalSetDataSourceProvider;

    @DexIgnore
    public NotificationsRepository_Factory(Provider<NotificationsDataSource> provider) {
        this.mappingLocalSetDataSourceProvider = provider;
    }

    @DexIgnore
    public static NotificationsRepository_Factory create(Provider<NotificationsDataSource> provider) {
        return new NotificationsRepository_Factory(provider);
    }

    @DexIgnore
    public static NotificationsRepository newNotificationsRepository(NotificationsDataSource notificationsDataSource) {
        return new NotificationsRepository(notificationsDataSource);
    }

    @DexIgnore
    public static NotificationsRepository provideInstance(Provider<NotificationsDataSource> provider) {
        return new NotificationsRepository(provider.get());
    }

    @DexIgnore
    public NotificationsRepository get() {
        return provideInstance(this.mappingLocalSetDataSourceProvider);
    }
}
