package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import java.util.Date;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1", f = "SleepSummaryLocalDataSource.kt", l = {176}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super ro2<yz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Pair $downloadingRange;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, Pair pair, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = pair;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(this.this$Anon0, this.$downloadingRange, kc4);
        sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1.p$ = (lh4) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            obj = SleepSessionsRepository.fetchSleepSessions$default(this.this$Anon0.this$Anon0.mSleepSessionsRepository, (Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
