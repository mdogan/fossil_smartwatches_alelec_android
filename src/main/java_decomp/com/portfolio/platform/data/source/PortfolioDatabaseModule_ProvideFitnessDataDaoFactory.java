package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideFitnessDataDaoFactory implements Factory<FitnessDataDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessDataDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessDataDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideFitnessDataDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FitnessDataDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideFitnessDataDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static FitnessDataDao proxyProvideFitnessDataDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        FitnessDataDao provideFitnessDataDao = portfolioDatabaseModule.provideFitnessDataDao(fitnessDatabase);
        o44.a(provideFitnessDataDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessDataDao;
    }

    @DexIgnore
    public FitnessDataDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
