package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$updateLastSleepGoal$Anon1 extends NetworkBoundResource<Integer, MFSleepSettings> {
    @DexIgnore
    public /* final */ /* synthetic */ int $sleepGoal;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    public SleepSummariesRepository$updateLastSleepGoal$Anon1(SleepSummariesRepository sleepSummariesRepository, int i) {
        this.this$Anon0 = sleepSummariesRepository;
        this.$sleepGoal = i;
    }

    @DexIgnore
    public Object createCall(kc4<? super cs4<MFSleepSettings>> kc4) {
        yz1 yz1 = new yz1();
        try {
            yz1.a("currentGoalMinutes", (Number) pc4.a(this.$sleepGoal));
            TimeZone timeZone = TimeZone.getDefault();
            wd4.a((Object) timeZone, "TimeZone.getDefault()");
            yz1.a("timezoneOffset", (Number) pc4.a(timeZone.getRawOffset() / 1000));
        } catch (Exception unused) {
        }
        return this.this$Anon0.mApiService.setSleepSetting(yz1, kc4);
    }

    @DexIgnore
    public LiveData<Integer> loadFromDb() {
        return this.this$Anon0.mSleepDao.getLastSleepGoal();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "fetchActivitySettings onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(MFSleepSettings mFSleepSettings) {
        wd4.b(mFSleepSettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "updateLastSleepGoal saveCallResult goal: " + mFSleepSettings);
        this.this$Anon0.saveSleepSettingToDB$app_fossilRelease(mFSleepSettings.getSleepGoal());
    }
}
