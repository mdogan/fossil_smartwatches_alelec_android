package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.g62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ MicroAppDao mMicroAppDao;
    @DexIgnore
    public /* final */ MicroAppRemoteDataSource mMicroAppRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "MicroAppRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        wd4.b(microAppDao, "mMicroAppDao");
        wd4.b(microAppRemoteDataSource, "mMicroAppRemoteDataSource");
        wd4.b(portfolioApp, "mPortfolioApp");
        this.mMicroAppDao = microAppDao;
        this.mMicroAppRemoteDataSource = microAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppDao.clearAllDeclarationFileTable();
        this.mMicroAppDao.clearAllMicroAppGalleryTable();
        this.mMicroAppDao.clearAllMicroAppSettingTable();
        this.mMicroAppDao.clearAllMicroAppVariantTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadAllMicroApp(String str, kc4<? super ro2<List<MicroApp>>> kc4) {
        MicroAppRepository$downloadAllMicroApp$Anon1 microAppRepository$downloadAllMicroApp$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        ro2 ro2;
        if (kc4 instanceof MicroAppRepository$downloadAllMicroApp$Anon1) {
            microAppRepository$downloadAllMicroApp$Anon1 = (MicroAppRepository$downloadAllMicroApp$Anon1) kc4;
            int i2 = microAppRepository$downloadAllMicroApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadAllMicroApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadAllMicroApp$Anon1.result;
                Object a = oc4.a();
                i = microAppRepository$downloadAllMicroApp$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "downloadAllMicroApp - serial=" + str);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadAllMicroApp$Anon1.L$Anon0 = this;
                    microAppRepository$downloadAllMicroApp$Anon1.L$Anon1 = str;
                    microAppRepository$downloadAllMicroApp$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroApp(str, microAppRepository$downloadAllMicroApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    str = (String) microAppRepository$downloadAllMicroApp$Anon1.L$Anon1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadAllMicroApp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadAllMicroApp - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(str3, sb.toString());
                    Object a2 = so2.a();
                    if (a2 != null) {
                        List list = (List) a2;
                        if (!so2.b() && (!list.isEmpty())) {
                            ArrayList arrayList = new ArrayList();
                            for (Object next : list) {
                                if (pc4.a(!StringsKt__StringsKt.a((CharSequence) g62.x.s(), (CharSequence) ((MicroApp) next).getId(), false, 2, (Object) null)).booleanValue()) {
                                    arrayList.add(next);
                                }
                            }
                            microAppRepository.mMicroAppDao.upsertListMicroApp(arrayList);
                        }
                        return new so2(list, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadAllMicroApp - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str4, sb2.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRepository$downloadAllMicroApp$Anon1 = new MicroAppRepository$downloadAllMicroApp$Anon1(this, kc4);
        Object obj2 = microAppRepository$downloadAllMicroApp$Anon1.result;
        Object a3 = oc4.a();
        i = microAppRepository$downloadAllMicroApp$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadMicroAppVariant(String str, String str2, String str3, kc4<? super ro2<List<MicroAppVariant>>> kc4) {
        MicroAppRepository$downloadMicroAppVariant$Anon1 microAppRepository$downloadMicroAppVariant$Anon1;
        int i;
        MicroAppRepository microAppRepository;
        ro2 ro2;
        if (kc4 instanceof MicroAppRepository$downloadMicroAppVariant$Anon1) {
            microAppRepository$downloadMicroAppVariant$Anon1 = (MicroAppRepository$downloadMicroAppVariant$Anon1) kc4;
            int i2 = microAppRepository$downloadMicroAppVariant$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRepository$downloadMicroAppVariant$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRepository$downloadMicroAppVariant$Anon1.result;
                Object a = oc4.a();
                i = microAppRepository$downloadMicroAppVariant$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "downloadMicroAppVariant - serial=" + str + " major " + str2 + " minor " + str3);
                    MicroAppRemoteDataSource microAppRemoteDataSource = this.mMicroAppRemoteDataSource;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon0 = this;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon1 = str;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon2 = str2;
                    microAppRepository$downloadMicroAppVariant$Anon1.L$Anon3 = str3;
                    microAppRepository$downloadMicroAppVariant$Anon1.label = 1;
                    obj = microAppRemoteDataSource.getAllMicroAppVariant(str, str2, str3, microAppRepository$downloadMicroAppVariant$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    microAppRepository = this;
                } else if (i == 1) {
                    String str5 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon3;
                    String str6 = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon2;
                    str = (String) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon1;
                    microAppRepository = (MicroAppRepository) microAppRepository$downloadMicroAppVariant$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadMicroAppVariant - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(str7, sb.toString());
                    Object a2 = so2.a();
                    if (a2 != null) {
                        List<MicroAppVariant> list = (List) a2;
                        if (!so2.b()) {
                            ArrayList arrayList = new ArrayList();
                            for (MicroAppVariant declarationFileList : list) {
                                pc4.a(arrayList.addAll(declarationFileList.getDeclarationFileList()));
                            }
                            microAppRepository.mMicroAppDao.upsertMicroAppVariantList(list);
                            microAppRepository.mMicroAppDao.upsertDeclarationFileList(arrayList);
                        }
                        return new so2(list, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadMicroAppVariant - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str8, sb2.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRepository$downloadMicroAppVariant$Anon1 = new MicroAppRepository$downloadMicroAppVariant$Anon1(this, kc4);
        Object obj2 = microAppRepository$downloadMicroAppVariant$Anon1.result;
        Object a3 = oc4.a();
        i = microAppRepository$downloadMicroAppVariant$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final List<MicroApp> getAllMicroApp(String str) {
        wd4.b(str, "serialNumber");
        return this.mMicroAppDao.getListMicroApp(str);
    }

    @DexIgnore
    public final List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        wd4.b(list, "ids");
        wd4.b(str, "serialNumber");
        return this.mMicroAppDao.getMicroAppByIds(list, str);
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, String str3, int i) {
        wd4.b(str, "serialNumber");
        wd4.b(str2, "microAppId");
        wd4.b(str3, "variantName");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    public final List<MicroApp> queryMicroAppByName(String str, String str2) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        wd4.b(str2, "serialNumber");
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : this.mMicroAppDao.getListMicroApp(str2)) {
            String normalize = Normalizer.normalize(tm2.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            wd4.a((Object) normalize, "name");
            wd4.a((Object) normalize2, "searchQuery");
            if (StringsKt__StringsKt.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        wd4.b(str, "serialNumber");
        wd4.b(str2, "microAppId");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }
}
