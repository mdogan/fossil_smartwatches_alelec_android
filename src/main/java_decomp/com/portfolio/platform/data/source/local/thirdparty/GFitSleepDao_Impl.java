package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitSleepDao_Impl implements GFitSleepDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __deletionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ xf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<GFitSleep> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSleep`(`id`,`sleepMins`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, GFitSleep gFitSleep) {
            lgVar.b(1, (long) gFitSleep.getId());
            lgVar.b(2, (long) gFitSleep.getSleepMins());
            lgVar.b(3, gFitSleep.getStartTime());
            lgVar.b(4, gFitSleep.getEndTime());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<GFitSleep> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitSleep` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(lg lgVar, GFitSleep gFitSleep) {
            lgVar.b(1, (long) gFitSleep.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM GFitSleep";
        }
    }

    @DexIgnore
    public GFitSleepDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGFitSleep = new Anon1(roomDatabase);
        this.__deletionAdapterOfGFitSleep = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSleep.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitSleep> getAllGFitSleep() {
        vf b = vf.b("SELECT * FROM gFitSleep", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "sleepMins");
            int b4 = bg.b(a, SampleRaw.COLUMN_START_TIME);
            int b5 = bg.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSleep gFitSleep = new GFitSleep(a.getInt(b3), a.getLong(b4), a.getLong(b5));
                gFitSleep.setId(a.getInt(b2));
                arrayList.add(gFitSleep);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitSleep(GFitSleep gFitSleep) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(gFitSleep);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
