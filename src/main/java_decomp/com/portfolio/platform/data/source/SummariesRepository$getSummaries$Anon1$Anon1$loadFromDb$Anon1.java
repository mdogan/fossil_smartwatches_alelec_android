package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1(SummariesRepository$getSummaries$Anon1.Anon1 anon1, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 = new SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1(this.this$Anon0, kc4);
        summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1.p$ = (lh4) obj;
        return summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ActivitySummary activitySummary = this.this$Anon0.this$Anon0.this$Anon0.mActivitySummaryDao.getActivitySummary(this.this$Anon0.this$Anon0.$endDate);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "todayRawSummary " + activitySummary);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
