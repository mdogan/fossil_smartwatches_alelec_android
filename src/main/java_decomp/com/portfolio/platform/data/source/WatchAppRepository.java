package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppRepository {
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ WatchAppDao mWatchAppDao;
    @DexIgnore
    public /* final */ WatchAppRemoteDataSource mWatchAppRemoteDataSource;

    @DexIgnore
    public WatchAppRepository(WatchAppDao watchAppDao, WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        wd4.b(watchAppDao, "mWatchAppDao");
        wd4.b(watchAppRemoteDataSource, "mWatchAppRemoteDataSource");
        wd4.b(portfolioApp, "mPortfolioApp");
        this.mWatchAppDao = watchAppDao;
        this.mWatchAppRemoteDataSource = watchAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppDao.clearAll();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadWatchApp(String str, kc4<? super cb4> kc4) {
        WatchAppRepository$downloadWatchApp$Anon1 watchAppRepository$downloadWatchApp$Anon1;
        int i;
        WatchAppRepository watchAppRepository;
        ro2 ro2;
        if (kc4 instanceof WatchAppRepository$downloadWatchApp$Anon1) {
            watchAppRepository$downloadWatchApp$Anon1 = (WatchAppRepository$downloadWatchApp$Anon1) kc4;
            int i2 = watchAppRepository$downloadWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRepository$downloadWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRepository$downloadWatchApp$Anon1.result;
                Object a = oc4.a();
                i = watchAppRepository$downloadWatchApp$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(ComplicationRepository.TAG, "downloadWatchApp of " + str);
                    WatchAppRemoteDataSource watchAppRemoteDataSource = this.mWatchAppRemoteDataSource;
                    watchAppRepository$downloadWatchApp$Anon1.L$Anon0 = this;
                    watchAppRepository$downloadWatchApp$Anon1.L$Anon1 = str;
                    watchAppRepository$downloadWatchApp$Anon1.label = 1;
                    obj = watchAppRemoteDataSource.getAllWatchApp(str, watchAppRepository$downloadWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchAppRepository = this;
                } else if (i == 1) {
                    str = (String) watchAppRepository$downloadWatchApp$Anon1.L$Anon1;
                    watchAppRepository = (WatchAppRepository) watchAppRepository$downloadWatchApp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadWatchApp of ");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    sb.append(" response ");
                    sb.append((List) so2.a());
                    local2.d(ComplicationRepository.TAG, sb.toString());
                    if (!so2.b()) {
                        Object a2 = so2.a();
                        if (a2 == null) {
                            wd4.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            watchAppRepository.mWatchAppDao.upsertWatchAppList((List) so2.a());
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadWatchApp of ");
                    sb2.append(str);
                    sb2.append(" fail!!! error=");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(ComplicationRepository.TAG, sb2.toString());
                }
                return cb4.a;
            }
        }
        watchAppRepository$downloadWatchApp$Anon1 = new WatchAppRepository$downloadWatchApp$Anon1(this, kc4);
        Object obj2 = watchAppRepository$downloadWatchApp$Anon1.result;
        Object a3 = oc4.a();
        i = watchAppRepository$downloadWatchApp$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final List<WatchApp> getAllWatchAppRaw() {
        return this.mWatchAppDao.getAllWatchApp();
    }

    @DexIgnore
    public final List<WatchApp> getWatchAppByIds(List<String> list) {
        wd4.b(list, "ids");
        if (!list.isEmpty()) {
            return wb4.a(this.mWatchAppDao.getWatchAppByIds(list), new WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    public final List<WatchApp> queryWatchAppByName(String str) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : this.mWatchAppDao.getAllWatchApp()) {
            String normalize = Normalizer.normalize(tm2.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            wd4.a((Object) normalize, "name");
            wd4.a((Object) normalize2, "searchQuery");
            if (StringsKt__StringsKt.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
