package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDataSourceFactory extends md.b<Long, GoalTrackingData> {
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public GoalTrackingDataLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.a mListener;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingDataLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, i42 i42, PagingRequestHelper.a aVar) {
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(goalTrackingDao, "mGoalTrackingDao");
        wd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wd4.b(date, "currentDate");
        wd4.b(i42, "mAppExecutors");
        wd4.b(aVar, "mListener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.currentDate = date;
        this.mAppExecutors = i42;
        this.mListener = aVar;
    }

    @DexIgnore
    public md<Long, GoalTrackingData> create() {
        this.localDataSource = new GoalTrackingDataLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDao, this.mGoalTrackingDatabase, this.currentDate, this.mAppExecutors, this.mListener);
        this.sourceLiveData.a(this.localDataSource);
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.localDataSource;
        if (goalTrackingDataLocalDataSource != null) {
            return goalTrackingDataLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDataLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingDataLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.localDataSource = goalTrackingDataLocalDataSource;
    }
}
