package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class FitnessDataDao {
    @DexIgnore
    public abstract void deleteAllFitnessData();

    @DexIgnore
    public abstract void deleteFitnessData(List<FitnessDataWrapper> list);

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        wd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(date2, "end");
        Date n = sk2.n(date);
        Date i = sk2.i(date2);
        wd4.a((Object) n, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(n.getTime());
        wd4.a((Object) i, SampleRaw.COLUMN_END_TIME);
        return getListFitnessData(dateTime, new DateTime(i.getTime()));
    }

    @DexIgnore
    public final LiveData<List<FitnessDataWrapper>> getFitnessDataLiveData(Date date, Date date2) {
        wd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(date2, "end");
        Date n = sk2.n(date);
        Date i = sk2.i(date2);
        wd4.a((Object) n, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(n.getTime());
        wd4.a((Object) i, SampleRaw.COLUMN_END_TIME);
        return getListFitnessDataLiveData(dateTime, new DateTime(i.getTime()));
    }

    @DexIgnore
    public abstract List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract List<FitnessDataWrapper> getPendingFitnessData();

    @DexIgnore
    public abstract void insertFitnessDataList(List<FitnessDataWrapper> list);
}
