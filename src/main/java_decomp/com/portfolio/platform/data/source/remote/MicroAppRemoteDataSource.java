package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public MicroAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object getAllMicroApp(String str, kc4<? super ro2<List<MicroApp>>> kc4) {
        MicroAppRemoteDataSource$getAllMicroApp$Anon1 microAppRemoteDataSource$getAllMicroApp$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof MicroAppRemoteDataSource$getAllMicroApp$Anon1) {
            microAppRemoteDataSource$getAllMicroApp$Anon1 = (MicroAppRemoteDataSource$getAllMicroApp$Anon1) kc4;
            int i2 = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRemoteDataSource$getAllMicroApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRemoteDataSource$getAllMicroApp$Anon1.result;
                Object a = oc4.a();
                i = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "getAllMicroApp " + str);
                    MicroAppRemoteDataSource$getAllMicroApp$response$Anon1 microAppRemoteDataSource$getAllMicroApp$response$Anon1 = new MicroAppRemoteDataSource$getAllMicroApp$response$Anon1(this, str, (kc4) null);
                    microAppRemoteDataSource$getAllMicroApp$Anon1.L$Anon0 = this;
                    microAppRemoteDataSource$getAllMicroApp$Anon1.L$Anon1 = str;
                    microAppRemoteDataSource$getAllMicroApp$Anon1.label = 1;
                    obj = ResponseKt.a(microAppRemoteDataSource$getAllMicroApp$response$Anon1, microAppRemoteDataSource$getAllMicroApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) microAppRemoteDataSource$getAllMicroApp$Anon1.L$Anon1;
                    MicroAppRemoteDataSource microAppRemoteDataSource = (MicroAppRemoteDataSource) microAppRemoteDataSource$getAllMicroApp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 != null) {
                        for (MicroApp microApp : ((ApiResponse) a2).get_items()) {
                            microApp.setSerialNumber(str);
                            arrayList.add(microApp);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "getAllMicroApp success isFromCache " + so2.b());
                        return new so2(arrayList, so2.b());
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getAllMicroApp fail code ");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" serverError ");
                    sb.append(qo2.c());
                    local3.d(TAG, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRemoteDataSource$getAllMicroApp$Anon1 = new MicroAppRemoteDataSource$getAllMicroApp$Anon1(this, kc4);
        Object obj2 = microAppRemoteDataSource$getAllMicroApp$Anon1.result;
        Object a3 = oc4.a();
        i = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    public final Object getAllMicroAppVariant(String str, String str2, String str3, kc4<? super ro2<List<MicroAppVariant>>> kc4) {
        MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
        int i;
        String str4;
        ro2 ro2;
        String str5 = str;
        String str6 = str2;
        String str7 = str3;
        kc4<? super ro2<List<MicroAppVariant>>> kc42 = kc4;
        if (kc42 instanceof MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1) {
            microAppRemoteDataSource$getAllMicroAppVariant$Anon1 = (MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1) kc42;
            int i2 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRemoteDataSource$getAllMicroAppVariant$Anon1.label = i2 - Integer.MIN_VALUE;
                MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon12 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
                Object obj = microAppRemoteDataSource$getAllMicroAppVariant$Anon12.result;
                Object a = oc4.a();
                i = microAppRemoteDataSource$getAllMicroAppVariant$Anon12.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "getAllMicroAppVariant " + str5 + " major " + str6 + " minor " + str7);
                    MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 = new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this, str, str2, str3, (kc4) null);
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon0 = this;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon1 = str5;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon2 = str6;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon3 = str7;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.label = 1;
                    obj = ResponseKt.a(microAppRemoteDataSource$getAllMicroAppVariant$response$Anon1, microAppRemoteDataSource$getAllMicroAppVariant$Anon12);
                    if (obj == a) {
                        return a;
                    }
                    str4 = str5;
                } else if (i == 1) {
                    String str8 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon3;
                    String str9 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon2;
                    str4 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon1;
                    MicroAppRemoteDataSource microAppRemoteDataSource = (MicroAppRemoteDataSource) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 != null) {
                        for (MicroAppVariant microAppVariant : ((ApiResponse) a2).get_items()) {
                            microAppVariant.setSerialNumber(str4);
                            for (DeclarationFile declarationFile : microAppVariant.getDeclarationFileList()) {
                                declarationFile.setAppId(microAppVariant.getAppId());
                                declarationFile.setSerialNumber(str4);
                                declarationFile.setVariantName(microAppVariant.getName());
                            }
                            arrayList.add(microAppVariant);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "getAllMicroApp success isFromCache " + so2.b());
                        return new so2(arrayList, so2.b());
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getAllMicroApp fail code ");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" serverError ");
                    sb.append(qo2.c());
                    local3.d(TAG, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        microAppRemoteDataSource$getAllMicroAppVariant$Anon1 = new MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1(this, kc42);
        MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon122 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
        Object obj2 = microAppRemoteDataSource$getAllMicroAppVariant$Anon122.result;
        Object a3 = oc4.a();
        i = microAppRemoteDataSource$getAllMicroAppVariant$Anon122.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
