package com.portfolio.platform.data.source.local.alarm;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zf;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class AlarmDatabase extends RoomDatabase {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final zf migrating3Or4To5(String str, int i) {
            wd4.b(str, ButtonService.USER_ID);
            return new AlarmDatabase$Companion$migrating3Or4To5$Anon1(i, str, i, 5);
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmDatabase.class.getSimpleName();
        wd4.a((Object) simpleName, "AlarmDatabase::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public abstract AlarmDao alarmDao();
}
