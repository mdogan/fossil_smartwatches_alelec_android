package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kk2;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.od;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import kotlin.NoWhenBranchMatchedException;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "SummariesRepository";
    @DexIgnore
    public /* final */ ActivitySummaryDao mActivitySummaryDao;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ yk2 mFitnessHelper;
    @DexIgnore
    public List<ActivitySummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public SummariesRepository(ApiServiceV2 apiServiceV2, ActivitySummaryDao activitySummaryDao, FitnessDataDao fitnessDataDao, yk2 yk2) {
        wd4.b(apiServiceV2, "mApiServiceV2");
        wd4.b(activitySummaryDao, "mActivitySummaryDao");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        wd4.b(yk2, "mFitnessHelper");
        this.mApiServiceV2 = apiServiceV2;
        this.mActivitySummaryDao = activitySummaryDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mFitnessHelper = yk2;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mActivitySummaryDao.deleteAllActivitySummaries();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, kc4<? super ro2<ActivityRecommendedGoals>> kc4) {
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon1;
        int i4;
        ro2 ro2;
        kc4<? super ro2<ActivityRecommendedGoals>> kc42 = kc4;
        if (kc42 instanceof SummariesRepository$downloadRecommendedGoals$Anon1) {
            summariesRepository$downloadRecommendedGoals$Anon1 = (SummariesRepository$downloadRecommendedGoals$Anon1) kc42;
            int i5 = summariesRepository$downloadRecommendedGoals$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                summariesRepository$downloadRecommendedGoals$Anon1.label = i5 - Integer.MIN_VALUE;
                SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon12 = summariesRepository$downloadRecommendedGoals$Anon1;
                Object obj = summariesRepository$downloadRecommendedGoals$Anon12.result;
                Object a = oc4.a();
                i4 = summariesRepository$downloadRecommendedGoals$Anon12.label;
                if (i4 != 0) {
                    za4.a(obj);
                    SummariesRepository$downloadRecommendedGoals$response$Anon1 summariesRepository$downloadRecommendedGoals$response$Anon1 = new SummariesRepository$downloadRecommendedGoals$response$Anon1(this, i, i2, i3, str, (kc4) null);
                    summariesRepository$downloadRecommendedGoals$Anon12.L$Anon0 = this;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon0 = i;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon1 = i2;
                    summariesRepository$downloadRecommendedGoals$Anon12.I$Anon2 = i3;
                    summariesRepository$downloadRecommendedGoals$Anon12.L$Anon1 = str;
                    summariesRepository$downloadRecommendedGoals$Anon12.label = 1;
                    obj = ResponseKt.a(summariesRepository$downloadRecommendedGoals$response$Anon1, summariesRepository$downloadRecommendedGoals$Anon12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i4 == 1) {
                    String str2 = (String) summariesRepository$downloadRecommendedGoals$Anon12.L$Anon1;
                    int i6 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon2;
                    int i7 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon1;
                    int i8 = summariesRepository$downloadRecommendedGoals$Anon12.I$Anon0;
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$downloadRecommendedGoals$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        return new so2((ActivityRecommendedGoals) a2, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        summariesRepository$downloadRecommendedGoals$Anon1 = new SummariesRepository$downloadRecommendedGoals$Anon1(this, kc42);
        SummariesRepository$downloadRecommendedGoals$Anon1 summariesRepository$downloadRecommendedGoals$Anon122 = summariesRepository$downloadRecommendedGoals$Anon1;
        Object obj2 = summariesRepository$downloadRecommendedGoals$Anon122.result;
        Object a3 = oc4.a();
        i4 = summariesRepository$downloadRecommendedGoals$Anon122.label;
        if (i4 != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object fetchActivitySettings(kc4<? super ro2<ActivitySettings>> kc4) {
        SummariesRepository$fetchActivitySettings$Anon1 summariesRepository$fetchActivitySettings$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ro2 ro2;
        if (kc4 instanceof SummariesRepository$fetchActivitySettings$Anon1) {
            summariesRepository$fetchActivitySettings$Anon1 = (SummariesRepository$fetchActivitySettings$Anon1) kc4;
            int i2 = summariesRepository$fetchActivitySettings$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivitySettings$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivitySettings$Anon1.result;
                Object a = oc4.a();
                i = summariesRepository$fetchActivitySettings$Anon1.label;
                String str = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchActivitySettings");
                    SummariesRepository$fetchActivitySettings$response$Anon1 summariesRepository$fetchActivitySettings$response$Anon1 = new SummariesRepository$fetchActivitySettings$response$Anon1(this, (kc4) null);
                    summariesRepository$fetchActivitySettings$Anon1.L$Anon0 = this;
                    summariesRepository$fetchActivitySettings$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivitySettings$response$Anon1, summariesRepository$fetchActivitySettings$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivitySettings$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    if (so2.a() != null) {
                        summariesRepository.saveActivitySettingsToDB$app_fossilRelease(new Date(), (ActivitySettings) so2.a());
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySettings - Failure -- code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(", message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb.append(str);
                            local.e(TAG, sb.toString());
                        }
                    }
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb.append(str);
                    local.e(TAG, sb.toString());
                }
                return ro2;
            }
        }
        summariesRepository$fetchActivitySettings$Anon1 = new SummariesRepository$fetchActivitySettings$Anon1(this, kc4);
        Object obj2 = summariesRepository$fetchActivitySettings$Anon1.result;
        Object a2 = oc4.a();
        i = summariesRepository$fetchActivitySettings$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return ro2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchActivityStatistic(kc4<? super ActivityStatistic> kc4) {
        SummariesRepository$fetchActivityStatistic$Anon1 summariesRepository$fetchActivityStatistic$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ro2 ro2;
        if (kc4 instanceof SummariesRepository$fetchActivityStatistic$Anon1) {
            summariesRepository$fetchActivityStatistic$Anon1 = (SummariesRepository$fetchActivityStatistic$Anon1) kc4;
            int i2 = summariesRepository$fetchActivityStatistic$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$fetchActivityStatistic$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$fetchActivityStatistic$Anon1.result;
                Object a = oc4.a();
                i = summariesRepository$fetchActivityStatistic$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    SummariesRepository$fetchActivityStatistic$response$Anon1 summariesRepository$fetchActivityStatistic$response$Anon1 = new SummariesRepository$fetchActivityStatistic$response$Anon1(this, (kc4) null);
                    summariesRepository$fetchActivityStatistic$Anon1.L$Anon0 = this;
                    summariesRepository$fetchActivityStatistic$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$fetchActivityStatistic$response$Anon1, summariesRepository$fetchActivityStatistic$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    summariesRepository = (SummariesRepository) summariesRepository$fetchActivityStatistic$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    if (so2.a() != null) {
                        summariesRepository.mActivitySummaryDao.upsertActivityStatistic((ActivityStatistic) so2.a());
                        return so2.a();
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getActivityStatisticAwait - Failure -- code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(", message=");
                    ServerError c = qo2.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local.e(TAG, sb.toString());
                }
                return null;
            }
        }
        summariesRepository$fetchActivityStatistic$Anon1 = new SummariesRepository$fetchActivityStatistic$Anon1(this, kc4);
        Object obj2 = summariesRepository$fetchActivityStatistic$Anon1.result;
        Object a2 = oc4.a();
        i = summariesRepository$fetchActivityStatistic$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return null;
    }

    @DexIgnore
    public final LiveData<ps3<ActivitySettings>> getActivitySettings() {
        return new SummariesRepository$getActivitySettings$Anon1(this).asLiveData();
    }

    @DexIgnore
    public final LiveData<ps3<ActivityStatistic>> getActivityStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getActivityStatistic - shouldFetch=" + z);
        return new SummariesRepository$getActivityStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getActivityStatisticAwait(kc4<? super ActivityStatistic> kc4) {
        SummariesRepository$getActivityStatisticAwait$Anon1 summariesRepository$getActivityStatisticAwait$Anon1;
        int i;
        if (kc4 instanceof SummariesRepository$getActivityStatisticAwait$Anon1) {
            summariesRepository$getActivityStatisticAwait$Anon1 = (SummariesRepository$getActivityStatisticAwait$Anon1) kc4;
            int i2 = summariesRepository$getActivityStatisticAwait$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$getActivityStatisticAwait$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$getActivityStatisticAwait$Anon1.result;
                Object a = oc4.a();
                i = summariesRepository$getActivityStatisticAwait$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "getActivityStatisticAwait");
                    ActivityStatistic activityStatistic = this.mActivitySummaryDao.getActivityStatistic();
                    if (activityStatistic != null) {
                        return activityStatistic;
                    }
                    summariesRepository$getActivityStatisticAwait$Anon1.L$Anon0 = this;
                    summariesRepository$getActivityStatisticAwait$Anon1.label = 1;
                    obj = fetchActivityStatistic(summariesRepository$getActivityStatisticAwait$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$getActivityStatisticAwait$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (ActivityStatistic) obj;
            }
        }
        summariesRepository$getActivityStatisticAwait$Anon1 = new SummariesRepository$getActivityStatisticAwait$Anon1(this, kc4);
        Object obj2 = summariesRepository$getActivityStatisticAwait$Anon1.result;
        Object a2 = oc4.a();
        i = summariesRepository$getActivityStatisticAwait$Anon1.label;
        if (i != 0) {
        }
        return (ActivityStatistic) obj2;
    }

    @DexIgnore
    public final ActivitySettings getCurrentActivitySettings() {
        ActivitySettings activitySetting = this.mActivitySummaryDao.getActivitySetting();
        if (activitySetting == null) {
            return new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getCurrentActivitySettings - " + "stepGoal=" + activitySetting.getCurrentStepGoal() + ", caloriesGoal=" + activitySetting.getCurrentCaloriesGoal() + ", " + "activeTimeGoal=" + activitySetting.getCurrentActiveTimeGoal());
        return new ActivitySettings(activitySetting.getCurrentStepGoal(), activitySetting.getCurrentCaloriesGoal(), activitySetting.getCurrentActiveTimeGoal());
    }

    @DexIgnore
    public final LiveData<ps3<List<ActivitySummary>>> getSummaries(Date date, Date date2, boolean z) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummaries - startDate=" + date + ", endDate=" + date2);
        LiveData<ps3<List<ActivitySummary>>> b = ic.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SummariesRepository$getSummaries$Anon1(this, date, date2, z));
        wd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<ActivitySummary> getSummariesPaging(SummariesRepository summariesRepository, yk2 yk2, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, Date date, i42 i42, PagingRequestHelper.a aVar) {
        Date date2 = date;
        wd4.b(summariesRepository, "summariesRepository");
        wd4.b(yk2, "fitnessHelper");
        wd4.b(fitnessDataRepository, "fitnessDataRepository");
        wd4.b(activitySummaryDao, "activitySummaryDao");
        FitnessDatabase fitnessDatabase2 = fitnessDatabase;
        wd4.b(fitnessDatabase2, "fitnessDatabase");
        wd4.b(date2, "createdDate");
        i42 i422 = i42;
        wd4.b(i422, "appExecutors");
        PagingRequestHelper.a aVar2 = aVar;
        wd4.b(aVar2, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummariesPaging - createdDate=" + date2);
        ActivitySummaryLocalDataSource.Companion companion = ActivitySummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        wd4.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date2);
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory = new ActivitySummaryDataSourceFactory(summariesRepository, yk2, fitnessDataRepository, activitySummaryDao, fitnessDatabase2, date2, i422, aVar2, instance2);
        this.mSourceFactoryList.add(activitySummaryDataSourceFactory);
        rd.f.a aVar3 = new rd.f.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        rd.f a = aVar3.a();
        wd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new od(activitySummaryDataSourceFactory, a).a();
        wd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = ic.b(activitySummaryDataSourceFactory.getSourceLiveData(), SummariesRepository$getSummariesPaging$Anon1.INSTANCE);
        wd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new SummariesRepository$getSummariesPaging$Anon2(activitySummaryDataSourceFactory), new SummariesRepository$getSummariesPaging$Anon3(activitySummaryDataSourceFactory));
    }

    @DexIgnore
    public final LiveData<ps3<ActivitySummary>> getSummary(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getSummary - date=" + date);
        LiveData<ps3<ActivitySummary>> b = ic.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon1(this, date));
        wd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insertFromDevice(List<ActivitySummary> list) {
        wd4.b(list, "summaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "insertFromDevice: summaries = " + list.size());
        this.mActivitySummaryDao.insertActivitySummaries(list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v9, resolved type: java.util.Date} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object loadSummaries(Date date, Date date2, kc4<? super ro2<yz1>> kc4) {
        SummariesRepository$loadSummaries$Anon1 summariesRepository$loadSummaries$Anon1;
        int i;
        SummariesRepository summariesRepository;
        ro2 ro2;
        if (kc4 instanceof SummariesRepository$loadSummaries$Anon1) {
            summariesRepository$loadSummaries$Anon1 = (SummariesRepository$loadSummaries$Anon1) kc4;
            int i2 = summariesRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$loadSummaries$Anon1.result;
                Object a = oc4.a();
                i = summariesRepository$loadSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "loadSummaries - startDate=" + date + ", endDate=" + date2);
                    SummariesRepository$loadSummaries$response$Anon1 summariesRepository$loadSummaries$response$Anon1 = new SummariesRepository$loadSummaries$response$Anon1(this, date, date2, (kc4) null);
                    summariesRepository$loadSummaries$Anon1.L$Anon0 = this;
                    summariesRepository$loadSummaries$Anon1.L$Anon1 = date;
                    summariesRepository$loadSummaries$Anon1.L$Anon2 = date2;
                    summariesRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(summariesRepository$loadSummaries$response$Anon1, summariesRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    summariesRepository = this;
                } else if (i == 1) {
                    date2 = summariesRepository$loadSummaries$Anon1.L$Anon2;
                    date = (Date) summariesRepository$loadSummaries$Anon1.L$Anon1;
                    summariesRepository = (SummariesRepository) summariesRepository$loadSummaries$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    if (so2.a() != null && !so2.b()) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            sz1 sz1 = new sz1();
                            sz1.a(Date.class, new GsonConvertDate());
                            sz1.a(DateTime.class, new GsonConvertDateTime());
                            ApiResponse apiResponse = (ApiResponse) sz1.a().a(((yz1) ((so2) ro2).a()).toString(), new SummariesRepository$loadSummaries$Anon2().getType());
                            if (apiResponse != null) {
                                List<FitnessDayData> list = apiResponse.get_items();
                                if (list != null) {
                                    for (FitnessDayData activitySummary : list) {
                                        ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                                        wd4.a((Object) activitySummary2, "it.toActivitySummary()");
                                        arrayList.add(activitySummary2);
                                    }
                                }
                            }
                            List<FitnessDataWrapper> fitnessData = summariesRepository.mFitnessDataDao.getFitnessData(date, date2);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d(TAG, "fitnessDataSize " + fitnessData.size() + " from " + date + " to " + date2);
                            if (fitnessData.isEmpty()) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                local3.d(TAG, "upsert 2 list " + arrayList);
                                summariesRepository.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries - e=");
                            e.printStackTrace();
                            sb.append(cb4.a);
                            local4.d(TAG, sb.toString());
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries - Failure -- code=");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(", message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb2.append(str);
                            local5.d(TAG, sb2.toString());
                        }
                    }
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb2.append(str);
                    local5.d(TAG, sb2.toString());
                }
                return ro2;
            }
        }
        summariesRepository$loadSummaries$Anon1 = new SummariesRepository$loadSummaries$Anon1(this, kc4);
        Object obj2 = summariesRepository$loadSummaries$Anon1.result;
        Object a2 = oc4.a();
        i = summariesRepository$loadSummaries$Anon1.label;
        String str2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return ro2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object pushActivitySettingsToServer(ActivitySettings activitySettings, kc4<? super cb4> kc4) {
        SummariesRepository$pushActivitySettingsToServer$Anon1 summariesRepository$pushActivitySettingsToServer$Anon1;
        int i;
        if (kc4 instanceof SummariesRepository$pushActivitySettingsToServer$Anon1) {
            summariesRepository$pushActivitySettingsToServer$Anon1 = (SummariesRepository$pushActivitySettingsToServer$Anon1) kc4;
            int i2 = summariesRepository$pushActivitySettingsToServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                summariesRepository$pushActivitySettingsToServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = summariesRepository$pushActivitySettingsToServer$Anon1.result;
                Object a = oc4.a();
                i = summariesRepository$pushActivitySettingsToServer$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "pushActivitySettingsToServer - settings=" + activitySettings);
                    sz1 sz1 = new sz1();
                    sz1.b(new kk2());
                    JsonElement b = sz1.a().b((Object) activitySettings);
                    wd4.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
                    yz1 d = b.d();
                    ApiServiceV2 apiServiceV2 = this.mApiServiceV2;
                    wd4.a((Object) d, "jsonObject");
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon0 = this;
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon1 = activitySettings;
                    summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon2 = d;
                    summariesRepository$pushActivitySettingsToServer$Anon1.label = 1;
                    if (apiServiceV2.updateActivitySetting(d, summariesRepository$pushActivitySettingsToServer$Anon1) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz1 = (yz1) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon2;
                    ActivitySettings activitySettings2 = (ActivitySettings) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon1;
                    SummariesRepository summariesRepository = (SummariesRepository) summariesRepository$pushActivitySettingsToServer$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cb4.a;
            }
        }
        summariesRepository$pushActivitySettingsToServer$Anon1 = new SummariesRepository$pushActivitySettingsToServer$Anon1(this, kc4);
        Object obj2 = summariesRepository$pushActivitySettingsToServer$Anon1.result;
        Object a2 = oc4.a();
        i = summariesRepository$pushActivitySettingsToServer$Anon1.label;
        if (i != 0) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (ActivitySummaryDataSourceFactory localDataSource : this.mSourceFactoryList) {
            ActivitySummaryLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    public final void saveActivitySettingsToDB$app_fossilRelease(Date date, ActivitySettings activitySettings) {
        Date date2 = date;
        ActivitySettings activitySettings2 = activitySettings;
        wd4.b(date2, "date");
        wd4.b(activitySettings2, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "saveActivitySettingsToDB - date=" + date2 + ", stepGoal=" + activitySettings.getCurrentStepGoal() + ", " + "caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + ", activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        ActivitySummary activitySummary = this.mActivitySummaryDao.getActivitySummary(date2);
        if (activitySummary == null) {
            DateTime dateTime = new DateTime();
            TimeZone timeZone = dateTime.getZone().toTimeZone();
            int year = dateTime.getYear();
            int monthOfYear = dateTime.getMonthOfYear();
            int dayOfMonth = dateTime.getDayOfMonth();
            wd4.a((Object) timeZone, "timeZone");
            activitySummary = new ActivitySummary(year, monthOfYear, dayOfMonth, timeZone.getID(), Integer.valueOf(timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0), 0.0d, 0.0d, 0.0d, ob4.d(0, 0, 0), 0, 0, 0, 0, 7680, (rd4) null);
        }
        activitySummary.setStepGoal(activitySettings.getCurrentStepGoal());
        activitySummary.setCaloriesGoal(activitySettings.getCurrentCaloriesGoal());
        activitySummary.setActiveTimeGoal(activitySettings.getCurrentActiveTimeGoal());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "updateDb activitySetting " + activitySettings2);
        this.mActivitySummaryDao.upsertActivitySettings(activitySettings2);
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d(TAG, "updateDb activitySummary " + activitySummary);
        this.mActivitySummaryDao.upsertActivitySummary(activitySummary);
        ActivitySummary activitySummary2 = this.mActivitySummaryDao.getActivitySummary(date2);
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "after upsertDb summary " + activitySummary2);
    }

    @DexIgnore
    public final LiveData<ps3<ActivitySettings>> updateActivitySettings(ActivitySettings activitySettings) {
        wd4.b(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "updateActivitySettings - settings=" + activitySettings);
        return new SummariesRepository$updateActivitySettings$Anon1(this, activitySettings).asLiveData();
    }

    @DexIgnore
    public final void upsertRecommendGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        wd4.b(activityRecommendedGoals, "recommendedGoals");
        this.mActivitySummaryDao.upsertActivityRecommendedGoals(activityRecommendedGoals);
    }

    @DexIgnore
    public final ActivitySummary getSummary(Calendar calendar) {
        wd4.b(calendar, "date");
        ActivitySummaryDao activitySummaryDao = this.mActivitySummaryDao;
        Date time = calendar.getTime();
        wd4.a((Object) time, "date.time");
        return activitySummaryDao.getActivitySummary(time);
    }
}
