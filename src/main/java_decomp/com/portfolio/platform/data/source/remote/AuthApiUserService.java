package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nt4;
import com.fossil.blesdk.obfuscated.yz1;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AuthApiUserService {
    @DexIgnore
    @nt4("rpc/auth/password/change")
    Call<yz1> changePassword(@bt4 yz1 yz1);

    @DexIgnore
    @nt4("rpc/auth/logout")
    Object logout(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);
}
