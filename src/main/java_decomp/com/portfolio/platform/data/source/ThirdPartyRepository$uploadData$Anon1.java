package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1", f = "ThirdPartyRepository.kt", l = {157, 158, 159, 160, 161, 162, 163}, m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon1", f = "ThirdPartyRepository.kt", l = {111}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSampleList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$gFitSampleList, this.$activeDeviceSerial, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitSampleList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon2", f = "ThirdPartyRepository.kt", l = {118}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitActiveTimeList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$gFitActiveTimeList, this.$activeDeviceSerial, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitActiveTimeList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon3", f = "ThirdPartyRepository.kt", l = {125}, m = "invokeSuspend")
    public static final class Anon3 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitHeartRateList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon3 anon3 = new Anon3(this.this$Anon0, this.$gFitHeartRateList, this.$activeDeviceSerial, kc4);
            anon3.p$ = (lh4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitHeartRateList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon4", f = "ThirdPartyRepository.kt", l = {132}, m = "invokeSuspend")
    public static final class Anon4 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSleepList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon4 anon4 = new Anon4(this.this$Anon0, this.$gFitSleepList, this.$activeDeviceSerial, kc4);
            anon4.p$ = (lh4) obj;
            return anon4;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitSleepList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon5", f = "ThirdPartyRepository.kt", l = {141}, m = "invokeSuspend")
    public static final class Anon5 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitWorkoutSessionList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, List list, String str, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon5 anon5 = new Anon5(this.this$Anon0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, kc4);
            anon5.p$ = (lh4) obj;
            return anon5;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon5) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                List list = this.$gFitWorkoutSessionList;
                String str = this.$activeDeviceSerial;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon6", f = "ThirdPartyRepository.kt", l = {150}, m = "invokeSuspend")
    public static final class Anon6 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon6(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon6 anon6 = new Anon6(this.this$Anon0, kc4);
            anon6.p$ = (lh4) obj;
            return anon6;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon6) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUA(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon1$Anon7", f = "ThirdPartyRepository.kt", l = {151}, m = "invokeSuspend")
    public static final class Anon7 extends SuspendLambda implements kd4<lh4, kc4<? super Object>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon7(ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = thirdPartyRepository$uploadData$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon7 anon7 = new Anon7(this.this$Anon0, kc4);
            anon7.p$ = (lh4) obj;
            return anon7;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon7) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = oc4.a();
            int i = this.label;
            if (i == 0) {
                za4.a(obj);
                lh4 lh4 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$Anon0.this$Anon0;
                this.L$Anon0 = lh4;
                this.label = 1;
                obj = thirdPartyRepository.saveToUAOldFlow(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                lh4 lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$Anon1(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ThirdPartyRepository$uploadData$Anon1 thirdPartyRepository$uploadData$Anon1 = new ThirdPartyRepository$uploadData$Anon1(this.this$Anon0, this.$pushPendingThirdPartyDataCallback, kc4);
        thirdPartyRepository$uploadData$Anon1.p$ = (lh4) obj;
        return thirdPartyRepository$uploadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ThirdPartyRepository$uploadData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x02d9, code lost:
        r11 = r2;
        r2 = r12;
        r19 = r6;
        r6 = r4;
        r4 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02e3, code lost:
        if (r8 == null) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x02e5, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x02fe, code lost:
        if (r8.a(r0) != r1) goto L_0x0301;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0300, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0301, code lost:
        if (r7 == null) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0303, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x031c, code lost:
        if (r7.a(r0) != r1) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x031e, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x031f, code lost:
        if (r6 == null) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0321, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x033a, code lost:
        if (r6.a(r0) != r1) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x033c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x033d, code lost:
        if (r5 == null) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x033f, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0358, code lost:
        if (r5.a(r0) != r1) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x035a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x035b, code lost:
        if (r4 == null) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x035d, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0376, code lost:
        if (r4.a(r0) != r1) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0378, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0379, code lost:
        if (r2 == null) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x037b, code lost:
        r0.L$Anon0 = r11;
        r0.L$Anon1 = r10;
        r0.L$Anon2 = r9;
        r0.L$Anon3 = r8;
        r0.L$Anon4 = r7;
        r0.L$Anon5 = r6;
        r0.L$Anon6 = r5;
        r0.L$Anon7 = r4;
        r0.L$Anon8 = r2;
        r0.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0394, code lost:
        if (r2.a(r0) != r1) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0396, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0397, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End uploadData");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x03a2, code lost:
        r1 = r0.$pushPendingThirdPartyDataCallback;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x03a4, code lost:
        if (r1 == null) goto L_0x03a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x03a6, code lost:
        r1.onComplete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x03ab, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        String str;
        sh4 sh4;
        sh4 sh42;
        sh4 sh43;
        sh4 sh44;
        sh4 sh45;
        sh4 sh46;
        sh4 sh47;
        sh4 sh48;
        sh4 sh49;
        sh4 sh410;
        lh4 lh42;
        sh4 sh411;
        sh4 sh412;
        sh4 sh413;
        sh4 sh414;
        sh4 sh415;
        Object a = oc4.a();
        switch (this.label) {
            case 0:
                za4.a(obj);
                lh42 = this.p$;
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Start uploadData");
                str = this.this$Anon0.getMPortfolioApp().e();
                if (str.length() > 0) {
                    sh49 = null;
                    if (this.this$Anon0.mGoogleFitHelper.e()) {
                        List<GFitSample> allGFitSample = this.this$Anon0.getMThirdPartyDatabase().getGFitSampleDao().getAllGFitSample();
                        if (!allGFitSample.isEmpty()) {
                            sh412 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, allGFitSample, str, (kc4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSample is empty");
                            sh412 = null;
                        }
                        List<GFitActiveTime> allGFitActiveTime = this.this$Anon0.getMThirdPartyDatabase().getGFitActiveTimeDao().getAllGFitActiveTime();
                        if (!allGFitActiveTime.isEmpty()) {
                            sh413 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new Anon2(this, allGFitActiveTime, str, (kc4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitActiveTime is empty");
                            sh413 = null;
                        }
                        List<GFitHeartRate> allGFitHeartRate = this.this$Anon0.getMThirdPartyDatabase().getGFitHeartRateDao().getAllGFitHeartRate();
                        if (!allGFitHeartRate.isEmpty()) {
                            sh414 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new Anon3(this, allGFitHeartRate, str, (kc4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitHeartRate is empty");
                            sh414 = null;
                        }
                        List<GFitSleep> allGFitSleep = this.this$Anon0.getMThirdPartyDatabase().getGFitSleepDao().getAllGFitSleep();
                        if (!allGFitSleep.isEmpty()) {
                            sh415 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new Anon4(this, allGFitSleep, str, (kc4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitSleep is empty");
                            sh415 = null;
                        }
                        List<GFitWorkoutSession> allGFitWorkoutSession = this.this$Anon0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().getAllGFitWorkoutSession();
                        if (!allGFitWorkoutSession.isEmpty()) {
                            sh411 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new Anon5(this, allGFitWorkoutSession, str, (kc4) null), 3, (Object) null);
                        } else {
                            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "GFitWorkoutSession is empty");
                            sh411 = null;
                        }
                        sh4 sh416 = sh415;
                        sh4 = sh412;
                        sh410 = sh416;
                        sh4 sh417 = sh414;
                        sh42 = sh413;
                        sh43 = sh417;
                    } else {
                        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Google Fit is not connected");
                        sh410 = null;
                        sh43 = null;
                        sh42 = null;
                        sh4 = null;
                        sh411 = null;
                    }
                    if (this.this$Anon0.getMPortfolioApp().r().b()) {
                        lh4 lh43 = lh42;
                        sh4 a2 = mg4.a(lh43, (CoroutineContext) null, (CoroutineStart) null, new Anon6(this, (kc4) null), 3, (Object) null);
                        sh48 = mg4.a(lh43, (CoroutineContext) null, (CoroutineStart) null, new Anon7(this, (kc4) null), 3, (Object) null);
                        sh49 = a2;
                    } else {
                        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Not logged in to UA");
                        sh48 = null;
                    }
                    if (sh4 != null) {
                        this.L$Anon0 = lh42;
                        this.L$Anon1 = str;
                        this.L$Anon2 = sh4;
                        this.L$Anon3 = sh42;
                        this.L$Anon4 = sh43;
                        this.L$Anon5 = sh410;
                        this.L$Anon6 = sh411;
                        this.L$Anon7 = sh49;
                        this.L$Anon8 = sh48;
                        this.label = 1;
                        if (sh4.a(this) == a) {
                            return a;
                        }
                    }
                    sh45 = sh411;
                    break;
                }
                break;
            case 1:
                sh45 = (sh4) this.L$Anon6;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                za4.a(obj);
                sh48 = (sh4) this.L$Anon8;
                lh42 = (lh4) this.L$Anon0;
                sh4 sh418 = (sh4) this.L$Anon5;
                sh49 = (sh4) this.L$Anon7;
                sh410 = sh418;
                break;
            case 2:
                sh47 = (sh4) this.L$Anon8;
                sh46 = (sh4) this.L$Anon7;
                sh45 = (sh4) this.L$Anon6;
                sh44 = (sh4) this.L$Anon5;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 3:
                sh47 = (sh4) this.L$Anon8;
                sh46 = (sh4) this.L$Anon7;
                sh45 = (sh4) this.L$Anon6;
                sh44 = (sh4) this.L$Anon5;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 4:
                sh47 = (sh4) this.L$Anon8;
                sh46 = (sh4) this.L$Anon7;
                sh45 = (sh4) this.L$Anon6;
                sh44 = (sh4) this.L$Anon5;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 5:
                sh47 = (sh4) this.L$Anon8;
                sh46 = (sh4) this.L$Anon7;
                sh45 = (sh4) this.L$Anon6;
                sh44 = (sh4) this.L$Anon5;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 6:
                sh47 = (sh4) this.L$Anon8;
                sh46 = (sh4) this.L$Anon7;
                sh45 = (sh4) this.L$Anon6;
                sh44 = (sh4) this.L$Anon5;
                sh43 = (sh4) this.L$Anon4;
                sh42 = (sh4) this.L$Anon3;
                sh4 = (sh4) this.L$Anon2;
                str = (String) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 7:
                sh4 sh419 = (sh4) this.L$Anon8;
                sh4 sh420 = (sh4) this.L$Anon7;
                sh4 sh421 = (sh4) this.L$Anon6;
                sh4 sh422 = (sh4) this.L$Anon5;
                sh4 sh423 = (sh4) this.L$Anon4;
                sh4 sh424 = (sh4) this.L$Anon3;
                sh4 sh425 = (sh4) this.L$Anon2;
                String str2 = (String) this.L$Anon1;
                lh4 lh44 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
