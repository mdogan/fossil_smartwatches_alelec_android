package com.portfolio.platform.data.source.local.sleep;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `sleep_session`");
            hgVar.b("DROP TABLE IF EXISTS `sleep_date`");
            hgVar.b("DROP TABLE IF EXISTS `sleep_settings`");
            hgVar.b("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            hgVar.b("DROP TABLE IF EXISTS `sleep_statistic`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SleepDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = SleepDatabase_Impl.this.mDatabase = hgVar;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SleepDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
            hashMap.put("date", new fg.a("date", "INTEGER", true, 0));
            hashMap.put("day", new fg.a("day", "TEXT", true, 0));
            hashMap.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new fg.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0));
            hashMap.put("syncTime", new fg.a("syncTime", "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_BOOKMARK_TIME, new fg.a(MFSleepSession.COLUMN_BOOKMARK_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, new fg.a(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, "REAL", true, 0));
            hashMap.put("source", new fg.a("source", "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_START_TIME, new fg.a(MFSleepSession.COLUMN_REAL_START_TIME, "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_END_TIME, new fg.a(MFSleepSession.COLUMN_REAL_END_TIME, "INTEGER", true, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, new fg.a(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, "INTEGER", true, 0));
            hashMap.put(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, new fg.a(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, "TEXT", true, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_START_TIME, new fg.a(MFSleepSession.COLUMN_EDITED_START_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_END_TIME, new fg.a(MFSleepSession.COLUMN_EDITED_END_TIME, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, new fg.a(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, "INTEGER", false, 0));
            hashMap.put(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, new fg.a(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, "TEXT", false, 0));
            hashMap.put(MFSleepSession.COLUMN_SLEEP_STATES, new fg.a(MFSleepSession.COLUMN_SLEEP_STATES, "TEXT", true, 0));
            hashMap.put("heartRate", new fg.a("heartRate", "TEXT", false, 0));
            hashMap.put("createdAt", new fg.a("createdAt", "INTEGER", true, 0));
            hashMap.put("updatedAt", new fg.a("updatedAt", "INTEGER", true, 0));
            hashMap.put("timezoneOffset", new fg.a("timezoneOffset", "INTEGER", true, 0));
            fg fgVar = new fg(MFSleepSession.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, MFSleepSession.TABLE_NAME);
            if (fgVar.equals(a)) {
                HashMap hashMap2 = new HashMap(8);
                hashMap2.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
                hashMap2.put("timezoneOffset", new fg.a("timezoneOffset", "INTEGER", true, 0));
                hashMap2.put("date", new fg.a("date", "TEXT", true, 1));
                hashMap2.put(MFSleepDay.COLUMN_GOAL_MINUTES, new fg.a(MFSleepDay.COLUMN_GOAL_MINUTES, "INTEGER", true, 0));
                hashMap2.put(MFSleepDay.COLUMN_SLEEP_MINUTES, new fg.a(MFSleepDay.COLUMN_SLEEP_MINUTES, "INTEGER", true, 0));
                hashMap2.put(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, new fg.a(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, "TEXT", false, 0));
                hashMap2.put("createdAt", new fg.a("createdAt", "INTEGER", false, 0));
                hashMap2.put("updatedAt", new fg.a("updatedAt", "INTEGER", false, 0));
                fg fgVar2 = new fg(MFSleepDay.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
                fg a2 = fg.a(hgVar, MFSleepDay.TABLE_NAME);
                if (fgVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(2);
                    hashMap3.put("id", new fg.a("id", "INTEGER", true, 1));
                    hashMap3.put("sleepGoal", new fg.a("sleepGoal", "INTEGER", true, 0));
                    fg fgVar3 = new fg("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
                    fg a3 = fg.a(hgVar, "sleep_settings");
                    if (fgVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(2);
                        hashMap4.put("id", new fg.a("id", "INTEGER", true, 1));
                        hashMap4.put("recommendedSleepGoal", new fg.a("recommendedSleepGoal", "INTEGER", true, 0));
                        fg fgVar4 = new fg("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
                        fg a4 = fg.a(hgVar, "sleepRecommendedGoals");
                        if (fgVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(10);
                            hashMap5.put("id", new fg.a("id", "TEXT", true, 1));
                            hashMap5.put("uid", new fg.a("uid", "TEXT", true, 0));
                            hashMap5.put("sleepTimeBestDay", new fg.a("sleepTimeBestDay", "TEXT", false, 0));
                            hashMap5.put("sleepTimeBestStreak", new fg.a("sleepTimeBestStreak", "TEXT", false, 0));
                            hashMap5.put("totalDays", new fg.a("totalDays", "INTEGER", true, 0));
                            hashMap5.put("totalSleeps", new fg.a("totalSleeps", "INTEGER", true, 0));
                            hashMap5.put("totalSleepMinutes", new fg.a("totalSleepMinutes", "INTEGER", true, 0));
                            hashMap5.put("totalSleepStateDistInMinute", new fg.a("totalSleepStateDistInMinute", "TEXT", true, 0));
                            hashMap5.put("createdAt", new fg.a("createdAt", "INTEGER", true, 0));
                            hashMap5.put("updatedAt", new fg.a("updatedAt", "INTEGER", true, 0));
                            fg fgVar5 = new fg(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
                            fg a5 = fg.a(hgVar, SleepStatistic.TABLE_NAME);
                            if (!fgVar5.equals(a5)) {
                                throw new IllegalStateException("Migration didn't properly handle sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + fgVar5 + "\n Found:\n" + a5);
                            }
                            return;
                        }
                        throw new IllegalStateException("Migration didn't properly handle sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + fgVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + fgVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + fgVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + fgVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `sleep_session`");
            a.b("DELETE FROM `sleep_date`");
            a.b("DELETE FROM `sleep_settings`");
            a.b("DELETE FROM `sleepRecommendedGoals`");
            a.b("DELETE FROM `sleep_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), MFSleepSession.TABLE_NAME, MFSleepDay.TABLE_NAME, "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME);
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
