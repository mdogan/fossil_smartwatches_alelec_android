package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.fossil.blesdk.obfuscated.z62;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmDao_Impl implements AlarmDao {
    @DexIgnore
    public /* final */ z62 __alarmConverter; // = new z62();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfAlarm;
    @DexIgnore
    public /* final */ xf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xf __preparedStmtOfRemoveAlarm;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<Alarm> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `alarm`(`id`,`uri`,`title`,`hour`,`minute`,`days`,`isActive`,`isRepeated`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, Alarm alarm) {
            if (alarm.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, alarm.getId());
            }
            if (alarm.getUri() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, alarm.getUri());
            }
            if (alarm.getTitle() == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, alarm.getTitle());
            }
            lgVar.b(4, (long) alarm.getHour());
            lgVar.b(5, (long) alarm.getMinute());
            String a = AlarmDao_Impl.this.__alarmConverter.a(alarm.getDays());
            if (a == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, a);
            }
            lgVar.b(7, alarm.isActive() ? 1 : 0);
            lgVar.b(8, alarm.isRepeated() ? 1 : 0);
            if (alarm.getCreatedAt() == null) {
                lgVar.a(9);
            } else {
                lgVar.a(9, alarm.getCreatedAt());
            }
            if (alarm.getUpdatedAt() == null) {
                lgVar.a(10);
            } else {
                lgVar.a(10, alarm.getUpdatedAt());
            }
            lgVar.b(11, (long) alarm.getPinType());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM alarm WHERE uri =?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM alarm";
        }
    }

    @DexIgnore
    public AlarmDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfAlarm = new Anon1(roomDatabase);
        this.__preparedStmtOfRemoveAlarm = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public List<Alarm> getActiveAlarms() {
        vf b = vf.b("SELECT *FROM alarm WHERE isActive = 1 and pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public Alarm getAlarmWithUri(String str) {
        Alarm alarm;
        String str2 = str;
        vf b = vf.b("SELECT * FROM alarm WHERE uri =?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Alarm> getAlarms() {
        vf b = vf.b("SELECT * FROM alarm", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Alarm> getAlarmsIgnoreDeleted() {
        vf b = vf.b("SELECT * FROM alarm WHERE pinType <> 3 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public Alarm getInComingActiveAlarm(int i) {
        Alarm alarm;
        vf b = vf.b("SELECT * FROM alarm WHERE isActive = 1 and minute + hour * 60 > ? ORDER BY hour, minute ASC LIMIT 1", 1);
        b.b(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Alarm> getPendingAlarms() {
        vf b = vf.b("SELECT*FROM alarm where pinType <> 0 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "uri");
            int b4 = bg.b(a, "title");
            int b5 = bg.b(a, AppFilter.COLUMN_HOUR);
            int b6 = bg.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b7 = bg.b(a, Alarm.COLUMN_DAYS);
            int b8 = bg.b(a, "isActive");
            int b9 = bg.b(a, "isRepeated");
            int b10 = bg.b(a, "createdAt");
            int b11 = bg.b(a, "updatedAt");
            int b12 = bg.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5), a.getInt(b6), this.__alarmConverter.a(a.getString(b7)), a.getInt(b8) != 0, a.getInt(b9) != 0, a.getString(b10), a.getString(b11), a.getInt(b12)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public long insertAlarm(Alarm alarm) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfAlarm.insertAndReturnId(alarm);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public Long[] insertAlarms(List<Alarm> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfAlarm.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public int removeAlarm(String str) {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfRemoveAlarm.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            int n = acquire.n();
            this.__db.setTransactionSuccessful();
            return n;
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAlarm.release(acquire);
        }
    }
}
