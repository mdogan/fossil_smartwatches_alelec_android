package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.SleepStatistic;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1", f = "SleepSummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepStatistic $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository$getSleepStatistic$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(SleepSummariesRepository$getSleepStatistic$Anon1 sleepSummariesRepository$getSleepStatistic$Anon1, SleepStatistic sleepStatistic, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepSummariesRepository$getSleepStatistic$Anon1;
        this.$item = sleepStatistic;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1 = new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(this.this$Anon0, this.$item, kc4);
        sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1.p$ = (lh4) obj;
        return sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.this$Anon0.mSleepDao.upsertSleepStatistic(this.$item);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
