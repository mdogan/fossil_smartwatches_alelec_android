package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kc4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public /* final */ /* synthetic */ List $serverAlarm$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(List list, kc4 kc4, AlarmsRepository alarmsRepository, kc4 kc42, List list2) {
        super(2, kc4);
        this.$it = list;
        this.this$Anon0 = alarmsRepository;
        this.$continuation$inlined = kc42;
        this.$serverAlarm$inlined = list2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 = new AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0, this.$continuation$inlined, this.$serverAlarm$inlined);
        alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.mAlarmsLocalDataSource.insertAlarms(this.$it);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
