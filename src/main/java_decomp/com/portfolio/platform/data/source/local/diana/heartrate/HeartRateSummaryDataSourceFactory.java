package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryDataSourceFactory extends md.b<Date, DailyHeartRateSummary> {
    @DexIgnore
    public /* final */ i42 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao heartRateSummaryDao;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public HeartRateSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<HeartRateSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository summariesRepository;

    @DexIgnore
    public HeartRateSummaryDataSourceFactory(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository2, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase2, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(heartRateSummaryRepository, "summariesRepository");
        wd4.b(fitnessDataRepository2, "fitnessDataRepository");
        wd4.b(heartRateDailySummaryDao, "heartRateSummaryDao");
        wd4.b(fitnessDatabase2, "fitnessDatabase");
        wd4.b(date, "createdDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        wd4.b(calendar, "mStartCalendar");
        this.summariesRepository = heartRateSummaryRepository;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.heartRateSummaryDao = heartRateDailySummaryDao;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = i42;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public md<Date, DailyHeartRateSummary> create() {
        this.localDataSource = new HeartRateSummaryLocalDataSource(this.summariesRepository, this.fitnessDataRepository, this.heartRateSummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.localDataSource;
        if (heartRateSummaryLocalDataSource != null) {
            return heartRateSummaryLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<HeartRateSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.localDataSource = heartRateSummaryLocalDataSource;
    }
}
