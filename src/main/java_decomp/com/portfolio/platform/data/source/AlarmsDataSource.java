package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AlarmsDataSource {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    Object deleteAlarm(Alarm alarm, kc4<? super ro2<Alarm>> kc4);

    @DexIgnore
    Object findAlarm(String str, kc4<? super Alarm> kc4);

    @DexIgnore
    Object getActiveAlarms(kc4<? super List<Alarm>> kc4);

    @DexIgnore
    Object getAlarms(kc4<? super ro2<List<Alarm>>> kc4);

    @DexIgnore
    Object getAllAlarmIgnorePinType(kc4<? super List<Alarm>> kc4);

    @DexIgnore
    Object setAlarm(Alarm alarm, kc4<? super ro2<Alarm>> kc4);
}
