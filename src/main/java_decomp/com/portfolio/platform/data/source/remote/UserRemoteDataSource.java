package com.portfolio.platform.data.source.remote;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.User;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserDataSource;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.response.ResponseKt;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserRemoteDataSource extends UserDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ AuthApiGuestService mAuthApiGuestService;
    @DexIgnore
    public /* final */ AuthApiUserService mAuthApiUserService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "UserRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRemoteDataSource(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        wd4.b(apiServiceV2, "mApiService");
        wd4.b(authApiGuestService, "mAuthApiGuestService");
        wd4.b(authApiUserService, "mAuthApiUserService");
        this.mApiService = apiServiceV2;
        this.mAuthApiGuestService = authApiGuestService;
        this.mAuthApiUserService = authApiUserService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object checkAuthenticationEmailExisting(String str, kc4<? super ro2<Boolean>> kc4) {
        UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1 userRemoteDataSource$checkAuthenticationEmailExisting$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1) {
            userRemoteDataSource$checkAuthenticationEmailExisting$Anon1 = (UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1) kc4;
            int i2 = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "checkAuthenticationEmailExisting for " + str);
                    yz1 yz1 = new yz1();
                    try {
                        yz1.a("email", str);
                    } catch (Exception e) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local2.d(str4, "Exception when generate jsonObject=" + e);
                    }
                    UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1 userRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1 = new UserRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon0 = this;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon1 = str;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon2 = yz1;
                    userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$checkAuthenticationEmailExisting$repoResponse$Anon1, userRemoteDataSource$checkAuthenticationEmailExisting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon2;
                    String str5 = (String) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "checkAuthenticationEmailExisting Success");
                    so2 so2 = (so2) ro2;
                    if (so2.a() == null || !((yz1) so2.a()).d("existing")) {
                        return new so2(pc4.a(false), false, 2, (rd4) null);
                    }
                    JsonElement a2 = ((yz1) so2.a()).a("existing");
                    wd4.a((Object) a2, "repoResponse.response.get(JSON_KEY_IS_EXISTING)");
                    return new so2(pc4.a(a2.a()), false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("checkAuthenticationEmailExisting failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local3.d(str6, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$checkAuthenticationEmailExisting$Anon1 = new UserRemoteDataSource$checkAuthenticationEmailExisting$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.result;
        Object a3 = oc4.a();
        i = userRemoteDataSource$checkAuthenticationEmailExisting$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object checkAuthenticationSocialExisting(String str, String str2, kc4<? super ro2<Boolean>> kc4) {
        UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1 userRemoteDataSource$checkAuthenticationSocialExisting$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1) {
            userRemoteDataSource$checkAuthenticationSocialExisting$Anon1 = (UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1) kc4;
            int i2 = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
                String str3 = null;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    try {
                        yz1.a(Constants.SERVICE, str);
                        yz1.a("token", str2);
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local.d(str4, "Exception when generate jsonObject=" + e);
                    }
                    UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1 userRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1 = new UserRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon0 = this;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon1 = str;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon2 = str2;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon3 = yz1;
                    userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$checkAuthenticationSocialExisting$repoResponse$Anon1, userRemoteDataSource$checkAuthenticationSocialExisting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon3;
                    String str5 = (String) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon2;
                    String str6 = (String) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "checkAuthenticationSocialExisting Success");
                    so2 so2 = (so2) ro2;
                    if (so2.a() == null || !((yz1) so2.a()).d("existing")) {
                        return new so2(pc4.a(false), false, 2, (rd4) null);
                    }
                    JsonElement a2 = ((yz1) so2.a()).a("existing");
                    wd4.a((Object) a2, "repoResponse.response.get(JSON_KEY_IS_EXISTING)");
                    return new so2(pc4.a(a2.a()), false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("checkAuthenticationSocialExisting failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str3 = c.getMessage();
                    }
                    sb.append(str3);
                    local2.d(str7, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$checkAuthenticationSocialExisting$Anon1 = new UserRemoteDataSource$checkAuthenticationSocialExisting$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.result;
        Object a3 = oc4.a();
        i = userRemoteDataSource$checkAuthenticationSocialExisting$Anon1.label;
        String str32 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object deleteUser(MFUser mFUser, kc4<? super Integer> kc4) {
        UserRemoteDataSource$deleteUser$Anon1 userRemoteDataSource$deleteUser$Anon1;
        int i;
        ro2 ro2;
        int i2;
        if (kc4 instanceof UserRemoteDataSource$deleteUser$Anon1) {
            userRemoteDataSource$deleteUser$Anon1 = (UserRemoteDataSource$deleteUser$Anon1) kc4;
            int i3 = userRemoteDataSource$deleteUser$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$deleteUser$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$deleteUser$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$deleteUser$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "deleteUser");
                    UserRemoteDataSource$deleteUser$response$Anon1 userRemoteDataSource$deleteUser$response$Anon1 = new UserRemoteDataSource$deleteUser$response$Anon1(this, (kc4) null);
                    userRemoteDataSource$deleteUser$Anon1.L$Anon0 = this;
                    userRemoteDataSource$deleteUser$Anon1.L$Anon1 = mFUser;
                    userRemoteDataSource$deleteUser$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$deleteUser$response$Anon1, userRemoteDataSource$deleteUser$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    MFUser mFUser2 = (MFUser) userRemoteDataSource$deleteUser$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$deleteUser$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    i2 = 200;
                } else if (ro2 instanceof qo2) {
                    i2 = ((qo2) ro2).a();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                return pc4.a(i2);
            }
        }
        userRemoteDataSource$deleteUser$Anon1 = new UserRemoteDataSource$deleteUser$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$deleteUser$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$deleteUser$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return pc4.a(i2);
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        wd4.b(mFUser, "user");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loadUserInfo(MFUser mFUser, kc4<? super ro2<MFUser>> kc4) {
        UserRemoteDataSource$loadUserInfo$Anon1 userRemoteDataSource$loadUserInfo$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$loadUserInfo$Anon1) {
            userRemoteDataSource$loadUserInfo$Anon1 = (UserRemoteDataSource$loadUserInfo$Anon1) kc4;
            int i2 = userRemoteDataSource$loadUserInfo$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loadUserInfo$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loadUserInfo$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$loadUserInfo$Anon1.label;
                String str = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loadUserInfo");
                    UserRemoteDataSource$loadUserInfo$response$Anon1 userRemoteDataSource$loadUserInfo$response$Anon1 = new UserRemoteDataSource$loadUserInfo$response$Anon1(this, (kc4) null);
                    userRemoteDataSource$loadUserInfo$Anon1.L$Anon0 = this;
                    userRemoteDataSource$loadUserInfo$Anon1.L$Anon1 = mFUser;
                    userRemoteDataSource$loadUserInfo$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loadUserInfo$response$Anon1, userRemoteDataSource$loadUserInfo$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    mFUser = (MFUser) userRemoteDataSource$loadUserInfo$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loadUserInfo$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "loadUserInfo Success");
                    User user = (User) ((so2) ro2).a();
                    if (user != null) {
                        MFUser mFUser2 = user.toMFUser(mFUser);
                        String updatedAt = mFUser.getUpdatedAt();
                        if (updatedAt != null) {
                            String updatedAt2 = mFUser2.getUpdatedAt();
                            if (updatedAt2 != null) {
                                Date d = sk2.d(updatedAt2);
                                wd4.a((Object) d, "DateHelper.parseJodaTime(it)");
                                long time = d.getTime();
                                Date d2 = sk2.d(updatedAt);
                                wd4.a((Object) d2, "DateHelper.parseJodaTime(localUpdateAt)");
                                if (time > d2.getTime() + 60000) {
                                    return new so2(mFUser2, false, 2, (rd4) null);
                                }
                                return new so2(mFUser, false, 2, (rd4) null);
                            }
                            FLogger.INSTANCE.getLocal().e(TAG, "remoteUpdateAt is null");
                            return new qo2(600, (ServerError) null, (Throwable) null, (String) null, 8, (rd4) null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local.e(str2, "localCreateAt is null - remoteUpdateAt: " + mFUser2.getUpdatedAt());
                        if (mFUser2.getUpdatedAt() != null) {
                            return new so2(mFUser2, false, 2, (rd4) null);
                        }
                        return new qo2(600, (ServerError) null, (Throwable) null, (String) null, 8, (rd4) null);
                    }
                    FLogger.INSTANCE.getLocal().e(TAG, "server return null-response");
                    return new qo2(600, (ServerError) null, (Throwable) null, (String) null, 8, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadUserInfo Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local2.d(str3, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$loadUserInfo$Anon1 = new UserRemoteDataSource$loadUserInfo$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$loadUserInfo$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$loadUserInfo$Anon1.label;
        String str4 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loginEmail(String str, String str2, kc4<? super ro2<Auth>> kc4) {
        UserRemoteDataSource$loginEmail$Anon1 userRemoteDataSource$loginEmail$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$loginEmail$Anon1) {
            userRemoteDataSource$loginEmail$Anon1 = (UserRemoteDataSource$loginEmail$Anon1) kc4;
            int i2 = userRemoteDataSource$loginEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loginEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loginEmail$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$loginEmail$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loginEmail");
                    yz1 yz1 = new yz1();
                    yz1.a("email", str);
                    yz1.a("password", str2);
                    yz1.a("clientId", AppHelper.f.a(""));
                    UserRemoteDataSource$loginEmail$response$Anon1 userRemoteDataSource$loginEmail$response$Anon1 = new UserRemoteDataSource$loginEmail$response$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$loginEmail$Anon1.L$Anon0 = this;
                    userRemoteDataSource$loginEmail$Anon1.L$Anon1 = str;
                    userRemoteDataSource$loginEmail$Anon1.L$Anon2 = str2;
                    userRemoteDataSource$loginEmail$Anon1.L$Anon3 = yz1;
                    userRemoteDataSource$loginEmail$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loginEmail$response$Anon1, userRemoteDataSource$loginEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$loginEmail$Anon1.L$Anon3;
                    String str3 = (String) userRemoteDataSource$loginEmail$Anon1.L$Anon2;
                    String str4 = (String) userRemoteDataSource$loginEmail$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loginEmail$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new so2(((so2) ro2).a(), false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        userRemoteDataSource$loginEmail$Anon1 = new UserRemoteDataSource$loginEmail$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$loginEmail$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$loginEmail$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object loginWithSocial(String str, String str2, String str3, kc4<? super ro2<Auth>> kc4) {
        UserRemoteDataSource$loginWithSocial$Anon1 userRemoteDataSource$loginWithSocial$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$loginWithSocial$Anon1) {
            userRemoteDataSource$loginWithSocial$Anon1 = (UserRemoteDataSource$loginWithSocial$Anon1) kc4;
            int i2 = userRemoteDataSource$loginWithSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$loginWithSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$loginWithSocial$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$loginWithSocial$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "loginWithSocial service " + str + " token " + str2 + " clientId " + str3);
                    yz1 yz1 = new yz1();
                    yz1.a(Constants.SERVICE, str);
                    yz1.a("token", str2);
                    yz1.a("clientId", AppHelper.f.a(""));
                    UserRemoteDataSource$loginWithSocial$response$Anon1 userRemoteDataSource$loginWithSocial$response$Anon1 = new UserRemoteDataSource$loginWithSocial$response$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$loginWithSocial$Anon1.L$Anon0 = this;
                    userRemoteDataSource$loginWithSocial$Anon1.L$Anon1 = str;
                    userRemoteDataSource$loginWithSocial$Anon1.L$Anon2 = str2;
                    userRemoteDataSource$loginWithSocial$Anon1.L$Anon3 = str3;
                    userRemoteDataSource$loginWithSocial$Anon1.L$Anon4 = yz1;
                    userRemoteDataSource$loginWithSocial$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$loginWithSocial$response$Anon1, userRemoteDataSource$loginWithSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$loginWithSocial$Anon1.L$Anon4;
                    String str5 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$Anon3;
                    String str6 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$Anon2;
                    String str7 = (String) userRemoteDataSource$loginWithSocial$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$loginWithSocial$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new so2(((so2) ro2).a(), false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        userRemoteDataSource$loginWithSocial$Anon1 = new UserRemoteDataSource$loginWithSocial$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$loginWithSocial$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$loginWithSocial$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object logoutUser(kc4<? super Integer> kc4) {
        UserRemoteDataSource$logoutUser$Anon1 userRemoteDataSource$logoutUser$Anon1;
        int i;
        ro2 ro2;
        int i2;
        if (kc4 instanceof UserRemoteDataSource$logoutUser$Anon1) {
            userRemoteDataSource$logoutUser$Anon1 = (UserRemoteDataSource$logoutUser$Anon1) kc4;
            int i3 = userRemoteDataSource$logoutUser$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$logoutUser$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$logoutUser$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$logoutUser$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "logoutUser");
                    UserRemoteDataSource$logoutUser$response$Anon1 userRemoteDataSource$logoutUser$response$Anon1 = new UserRemoteDataSource$logoutUser$response$Anon1(this, (kc4) null);
                    userRemoteDataSource$logoutUser$Anon1.L$Anon0 = this;
                    userRemoteDataSource$logoutUser$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$logoutUser$response$Anon1, userRemoteDataSource$logoutUser$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$logoutUser$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    i2 = 200;
                } else if (ro2 instanceof qo2) {
                    i2 = ((qo2) ro2).a();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                return pc4.a(i2);
            }
        }
        userRemoteDataSource$logoutUser$Anon1 = new UserRemoteDataSource$logoutUser$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$logoutUser$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$logoutUser$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
        return pc4.a(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object requestEmailOtp(String str, kc4<? super ro2<Void>> kc4) {
        UserRemoteDataSource$requestEmailOtp$Anon1 userRemoteDataSource$requestEmailOtp$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$requestEmailOtp$Anon1) {
            userRemoteDataSource$requestEmailOtp$Anon1 = (UserRemoteDataSource$requestEmailOtp$Anon1) kc4;
            int i2 = userRemoteDataSource$requestEmailOtp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$requestEmailOtp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$requestEmailOtp$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$requestEmailOtp$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "requestEmailOtp email: " + str);
                    yz1 yz1 = new yz1();
                    yz1.a("email", str);
                    UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1 userRemoteDataSource$requestEmailOtp$repoResponse$Anon1 = new UserRemoteDataSource$requestEmailOtp$repoResponse$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$requestEmailOtp$Anon1.L$Anon0 = this;
                    userRemoteDataSource$requestEmailOtp$Anon1.L$Anon1 = str;
                    userRemoteDataSource$requestEmailOtp$Anon1.L$Anon2 = yz1;
                    userRemoteDataSource$requestEmailOtp$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$requestEmailOtp$repoResponse$Anon1, userRemoteDataSource$requestEmailOtp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$requestEmailOtp$Anon1.L$Anon2;
                    String str4 = (String) userRemoteDataSource$requestEmailOtp$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$requestEmailOtp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "requestEmailOtp Success");
                    return new so2((Object) null, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("requestEmailOtp failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local2.d(str5, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$requestEmailOtp$Anon1 = new UserRemoteDataSource$requestEmailOtp$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$requestEmailOtp$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$requestEmailOtp$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object resetPassword(String str, kc4<? super ro2<Integer>> kc4) {
        UserRemoteDataSource$resetPassword$Anon1 userRemoteDataSource$resetPassword$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$resetPassword$Anon1) {
            userRemoteDataSource$resetPassword$Anon1 = (UserRemoteDataSource$resetPassword$Anon1) kc4;
            int i2 = userRemoteDataSource$resetPassword$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$resetPassword$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$resetPassword$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$resetPassword$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "resetPassword");
                    yz1 yz1 = new yz1();
                    yz1.a("email", str);
                    UserRemoteDataSource$resetPassword$repoResponse$Anon1 userRemoteDataSource$resetPassword$repoResponse$Anon1 = new UserRemoteDataSource$resetPassword$repoResponse$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$resetPassword$Anon1.L$Anon0 = this;
                    userRemoteDataSource$resetPassword$Anon1.L$Anon1 = str;
                    userRemoteDataSource$resetPassword$Anon1.L$Anon2 = yz1;
                    userRemoteDataSource$resetPassword$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$resetPassword$repoResponse$Anon1, userRemoteDataSource$resetPassword$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$resetPassword$Anon1.L$Anon2;
                    String str3 = (String) userRemoteDataSource$resetPassword$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$resetPassword$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "resetPassword Success");
                    return new so2(pc4.a(200), false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("resetPassword failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local.d(str4, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$resetPassword$Anon1 = new UserRemoteDataSource$resetPassword$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$resetPassword$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$resetPassword$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, kc4<? super ro2<Auth>> kc4) {
        UserRemoteDataSource$signUpEmail$Anon1 userRemoteDataSource$signUpEmail$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$signUpEmail$Anon1) {
            userRemoteDataSource$signUpEmail$Anon1 = (UserRemoteDataSource$signUpEmail$Anon1) kc4;
            int i2 = userRemoteDataSource$signUpEmail$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$signUpEmail$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$signUpEmail$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$signUpEmail$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "signUpEmail auth " + signUpEmailAuth);
                    UserRemoteDataSource$signUpEmail$response$Anon1 userRemoteDataSource$signUpEmail$response$Anon1 = new UserRemoteDataSource$signUpEmail$response$Anon1(this, signUpEmailAuth, (kc4) null);
                    userRemoteDataSource$signUpEmail$Anon1.L$Anon0 = this;
                    userRemoteDataSource$signUpEmail$Anon1.L$Anon1 = signUpEmailAuth;
                    userRemoteDataSource$signUpEmail$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$signUpEmail$response$Anon1, userRemoteDataSource$signUpEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SignUpEmailAuth signUpEmailAuth2 = (SignUpEmailAuth) userRemoteDataSource$signUpEmail$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$signUpEmail$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new so2(((so2) ro2).a(), false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        userRemoteDataSource$signUpEmail$Anon1 = new UserRemoteDataSource$signUpEmail$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$signUpEmail$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$signUpEmail$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, kc4<? super ro2<Auth>> kc4) {
        UserRemoteDataSource$signUpSocial$Anon1 userRemoteDataSource$signUpSocial$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$signUpSocial$Anon1) {
            userRemoteDataSource$signUpSocial$Anon1 = (UserRemoteDataSource$signUpSocial$Anon1) kc4;
            int i2 = userRemoteDataSource$signUpSocial$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$signUpSocial$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$signUpSocial$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$signUpSocial$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "signUpSocial auth " + signUpSocialAuth);
                    UserRemoteDataSource$signUpSocial$response$Anon1 userRemoteDataSource$signUpSocial$response$Anon1 = new UserRemoteDataSource$signUpSocial$response$Anon1(this, signUpSocialAuth, (kc4) null);
                    userRemoteDataSource$signUpSocial$Anon1.L$Anon0 = this;
                    userRemoteDataSource$signUpSocial$Anon1.L$Anon1 = signUpSocialAuth;
                    userRemoteDataSource$signUpSocial$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$signUpSocial$response$Anon1, userRemoteDataSource$signUpSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    SignUpSocialAuth signUpSocialAuth2 = (SignUpSocialAuth) userRemoteDataSource$signUpSocial$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$signUpSocial$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    return new so2(((so2) ro2).a(), false, 2, (rd4) null);
                }
                if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        userRemoteDataSource$signUpSocial$Anon1 = new UserRemoteDataSource$signUpSocial$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$signUpSocial$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$signUpSocial$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x028c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    public Object updateUser(MFUser mFUser, boolean z, kc4<? super ro2<MFUser>> kc4) {
        UserRemoteDataSource$updateUser$Anon1 userRemoteDataSource$updateUser$Anon1;
        int i;
        MFUser mFUser2;
        ro2 ro2;
        kc4<? super ro2<MFUser>> kc42 = kc4;
        if (kc42 instanceof UserRemoteDataSource$updateUser$Anon1) {
            userRemoteDataSource$updateUser$Anon1 = (UserRemoteDataSource$updateUser$Anon1) kc42;
            int i2 = userRemoteDataSource$updateUser$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$updateUser$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$updateUser$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$updateUser$Anon1.label;
                String str = null;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "updateUser");
                    yz1 yz1 = new yz1();
                    try {
                        String firstName = mFUser.getFirstName();
                        wd4.a((Object) firstName, "user.firstName");
                        Charset charset = nf4.a;
                        if (firstName != null) {
                            byte[] bytes = firstName.getBytes(charset);
                            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            Charset charset2 = StandardCharsets.UTF_8;
                            wd4.a((Object) charset2, "StandardCharsets.UTF_8");
                            yz1.a("firstName", new String(bytes, charset2));
                            String lastName = mFUser.getLastName();
                            wd4.a((Object) lastName, "user.lastName");
                            Charset charset3 = nf4.a;
                            if (lastName != null) {
                                byte[] bytes2 = lastName.getBytes(charset3);
                                wd4.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                                Charset charset4 = StandardCharsets.UTF_8;
                                wd4.a((Object) charset4, "StandardCharsets.UTF_8");
                                yz1.a("lastName", new String(bytes2, charset4));
                                if (mFUser.getHeightInCentimeters() != 0) {
                                    yz1.a("heightInCentimeters", (Number) pc4.a(mFUser.getHeightInCentimeters()));
                                }
                                if (mFUser.getWeightInGrams() != 0) {
                                    yz1.a("weightInGrams", (Number) pc4.a(mFUser.getWeightInGrams()));
                                }
                                Gender gender = mFUser.getGender();
                                yz1.a("gender", gender != null ? gender.toString() : null);
                                yz1 yz12 = new yz1();
                                Unit heightUnit = mFUser.getHeightUnit();
                                wd4.a((Object) heightUnit, "user.heightUnit");
                                yz12.a("height", heightUnit.getValue());
                                Unit weightUnit = mFUser.getWeightUnit();
                                wd4.a((Object) weightUnit, "user.weightUnit");
                                yz12.a(Constants.PROFILE_KEY_UNITS_WEIGHT, weightUnit.getValue());
                                Unit distanceUnit = mFUser.getDistanceUnit();
                                wd4.a((Object) distanceUnit, "user.distanceUnit");
                                yz12.a("distance", distanceUnit.getValue());
                                Unit temperatureUnit = mFUser.getTemperatureUnit();
                                wd4.a((Object) temperatureUnit, "user.temperatureUnit");
                                yz12.a("temperature", temperatureUnit.getValue());
                                yz1.a(Constants.PROFILE_KEY_UNIT_GROUP, (JsonElement) yz12);
                                yz1.a("heightInCentimeters", (Number) pc4.a(mFUser.getHeightInCentimeters()));
                                yz1.a("weightInGrams", (Number) pc4.a(mFUser.getWeightInGrams()));
                                yz1.a("activeDeviceId", mFUser.getActiveDeviceId());
                                yz1.a("emailOptIn", pc4.a(mFUser.isEmailOptIn()));
                                yz1.a("diagnosticEnabled", pc4.a(mFUser.isDiagnosticEnabled()));
                                yz1.a("isOnboardingComplete", pc4.a(mFUser.isOnboardingComplete()));
                                yz1.a(MFUser.USE_DEFAULT_BIOMETRIC, pc4.a(mFUser.isUseDefaultBiometric()));
                                yz1.a(MFUser.USE_DEFAULT_GOALS, pc4.a(mFUser.isUseDefaultGoals()));
                                if (!TextUtils.isEmpty(mFUser.getBirthday())) {
                                    yz1.a("birthday", mFUser.getBirthday());
                                }
                                uz1 uz1 = new uz1();
                                for (String a2 : mFUser.getIntegrations()) {
                                    uz1.a(a2);
                                }
                                yz1.a("integrations", (JsonElement) uz1);
                                yz1 yz13 = new yz1();
                                String home = mFUser.getHome();
                                String str2 = "";
                                if (home == null) {
                                    home = str2;
                                }
                                yz13.a("home", home);
                                String work = mFUser.getWork();
                                if (work != null) {
                                    str2 = work;
                                }
                                yz13.a("work", str2);
                                yz1.a("addresses", (JsonElement) yz13);
                                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                                    String profilePicture = mFUser.getProfilePicture();
                                    wd4.a((Object) profilePicture, "user.profilePicture");
                                    if (!StringsKt__StringsKt.a((CharSequence) profilePicture, (CharSequence) "https://", false, 2, (Object) null)) {
                                        String profilePicture2 = mFUser.getProfilePicture();
                                        wd4.a((Object) profilePicture2, "user.profilePicture");
                                        if (!StringsKt__StringsKt.a((CharSequence) profilePicture2, (CharSequence) "http://", false, 2, (Object) null)) {
                                            yz1.a("profilePicture", mFUser.getProfilePicture());
                                        }
                                    }
                                }
                                UserRemoteDataSource$updateUser$response$Anon1 userRemoteDataSource$updateUser$response$Anon1 = new UserRemoteDataSource$updateUser$response$Anon1(this, yz1, (kc4) null);
                                userRemoteDataSource$updateUser$Anon1.L$Anon0 = this;
                                mFUser2 = mFUser;
                                userRemoteDataSource$updateUser$Anon1.L$Anon1 = mFUser2;
                                userRemoteDataSource$updateUser$Anon1.Z$Anon0 = z;
                                userRemoteDataSource$updateUser$Anon1.L$Anon2 = yz1;
                                userRemoteDataSource$updateUser$Anon1.label = 1;
                                obj = ResponseKt.a(userRemoteDataSource$updateUser$response$Anon1, userRemoteDataSource$updateUser$Anon1);
                                if (obj == a) {
                                    return a;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local.d(str3, "Exception when generating user json object " + e);
                    }
                } else if (i == 1) {
                    yz1 yz14 = (yz1) userRemoteDataSource$updateUser$Anon1.L$Anon2;
                    boolean z2 = userRemoteDataSource$updateUser$Anon1.Z$Anon0;
                    mFUser2 = (MFUser) userRemoteDataSource$updateUser$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$updateUser$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "updateUser Success");
                    User user = (User) ((so2) ro2).a();
                    return new so2(user != null ? user.toMFUser(mFUser2) : null, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateUser Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local2.d(str4, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$updateUser$Anon1 = new UserRemoteDataSource$updateUser$Anon1(this, kc42);
        Object obj2 = userRemoteDataSource$updateUser$Anon1.result;
        Object a3 = oc4.a();
        i = userRemoteDataSource$updateUser$Anon1.label;
        String str5 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object verifyEmailOtp(String str, String str2, kc4<? super ro2<Void>> kc4) {
        UserRemoteDataSource$verifyEmailOtp$Anon1 userRemoteDataSource$verifyEmailOtp$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof UserRemoteDataSource$verifyEmailOtp$Anon1) {
            userRemoteDataSource$verifyEmailOtp$Anon1 = (UserRemoteDataSource$verifyEmailOtp$Anon1) kc4;
            int i2 = userRemoteDataSource$verifyEmailOtp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRemoteDataSource$verifyEmailOtp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRemoteDataSource$verifyEmailOtp$Anon1.result;
                Object a = oc4.a();
                i = userRemoteDataSource$verifyEmailOtp$Anon1.label;
                String str3 = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local.d(str4, "verifyEmailOtp email: " + str);
                    yz1 yz1 = new yz1();
                    yz1.a("email", str);
                    yz1.a("otp", str2);
                    UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1 userRemoteDataSource$verifyEmailOtp$repoResponse$Anon1 = new UserRemoteDataSource$verifyEmailOtp$repoResponse$Anon1(this, yz1, (kc4) null);
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon0 = this;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon1 = str;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon2 = str2;
                    userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon3 = yz1;
                    userRemoteDataSource$verifyEmailOtp$Anon1.label = 1;
                    obj = ResponseKt.a(userRemoteDataSource$verifyEmailOtp$repoResponse$Anon1, userRemoteDataSource$verifyEmailOtp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon3;
                    String str5 = (String) userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon2;
                    String str6 = (String) userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon1;
                    UserRemoteDataSource userRemoteDataSource = (UserRemoteDataSource) userRemoteDataSource$verifyEmailOtp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "verifyEmailOtp Success");
                    return new so2((Object) null, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("verifyEmailOtp failed with error=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str3 = c.getMessage();
                    }
                    sb.append(str3);
                    local2.d(str7, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        userRemoteDataSource$verifyEmailOtp$Anon1 = new UserRemoteDataSource$verifyEmailOtp$Anon1(this, kc4);
        Object obj2 = userRemoteDataSource$verifyEmailOtp$Anon1.result;
        Object a2 = oc4.a();
        i = userRemoteDataSource$verifyEmailOtp$Anon1.label;
        String str32 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
