package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$resetPassword$repoResponse$Anon1", f = "UserRemoteDataSource.kt", l = {217}, m = "invokeSuspend")
public final class UserRemoteDataSource$resetPassword$repoResponse$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yz1 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$resetPassword$repoResponse$Anon1(UserRemoteDataSource userRemoteDataSource, yz1 yz1, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = userRemoteDataSource;
        this.$jsonObject = yz1;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new UserRemoteDataSource$resetPassword$repoResponse$Anon1(this.this$Anon0, this.$jsonObject, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((UserRemoteDataSource$resetPassword$repoResponse$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            AuthApiGuestService access$getMAuthApiGuestService$p = this.this$Anon0.mAuthApiGuestService;
            yz1 yz1 = this.$jsonObject;
            this.label = 1;
            obj = access$getMAuthApiGuestService$p.passwordRequestReset(yz1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
