package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.o44;
import com.fossil.blesdk.obfuscated.yk2;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<yk2> {
    @DexIgnore
    public /* final */ Provider<ActivitySummaryDao> activitySummaryDaoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<fn2> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<fn2> provider, Provider<ActivitySummaryDao> provider2) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
        this.activitySummaryDaoProvider = provider2;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<fn2> provider, Provider<ActivitySummaryDao> provider2) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider, provider2);
    }

    @DexIgnore
    public static yk2 provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<fn2> provider, Provider<ActivitySummaryDao> provider2) {
        return proxyProvideFitnessHelper(portfolioDatabaseModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static yk2 proxyProvideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, fn2 fn2, ActivitySummaryDao activitySummaryDao) {
        yk2 provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(fn2, activitySummaryDao);
        o44.a(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    public yk2 get() {
        return provideInstance(this.module, this.sharedPreferencesManagerProvider, this.activitySummaryDaoProvider);
    }
}
