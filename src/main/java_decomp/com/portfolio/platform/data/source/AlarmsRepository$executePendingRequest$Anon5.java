package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$Anon5", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$Anon5 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $deleteAlarmPendingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$Anon5(AlarmsRepository alarmsRepository, List list, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = alarmsRepository;
        this.$deleteAlarmPendingList = list;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        AlarmsRepository$executePendingRequest$Anon5 alarmsRepository$executePendingRequest$Anon5 = new AlarmsRepository$executePendingRequest$Anon5(this.this$Anon0, this.$deleteAlarmPendingList, kc4);
        alarmsRepository$executePendingRequest$Anon5.p$ = (lh4) obj;
        return alarmsRepository$executePendingRequest$Anon5;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$executePendingRequest$Anon5) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            for (Alarm deleteAlarm : this.$deleteAlarmPendingList) {
                this.this$Anon0.mAlarmsLocalDataSource.deleteAlarm(deleteAlarm);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
