package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$response$Anon1", f = "DeviceRemoteDataSource.kt", l = {60}, m = "invokeSuspend")
public final class DeviceRemoteDataSource$generatePairingKey$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<yz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yz1 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$generatePairingKey$response$Anon1(DeviceRemoteDataSource deviceRemoteDataSource, yz1 yz1, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = deviceRemoteDataSource;
        this.$jsonObject = yz1;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new DeviceRemoteDataSource$generatePairingKey$response$Anon1(this.this$Anon0, this.$jsonObject, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DeviceRemoteDataSource$generatePairingKey$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            yz1 yz1 = this.$jsonObject;
            this.label = 1;
            obj = access$getMApiService$p.generatePairingKey(yz1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
