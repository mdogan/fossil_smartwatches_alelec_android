package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepStatistic$Anon1 extends NetworkBoundResource<SleepStatistic, SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$Anon1(SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$Anon0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(kc4<? super cs4<SleepStatistic>> kc4) {
        return this.this$Anon0.mApiService.getSleepStatistic(kc4);
    }

    @DexIgnore
    public LiveData<SleepStatistic> loadFromDb() {
        return this.this$Anon0.mSleepDao.getSleepStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(SleepStatistic sleepStatistic) {
        wd4.b(sleepStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getSleepStatistic - saveCallResult -- item=" + sleepStatistic);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(this, sleepStatistic, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }
}
