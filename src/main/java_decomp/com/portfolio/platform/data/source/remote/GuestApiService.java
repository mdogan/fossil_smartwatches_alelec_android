package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.st4;
import com.fossil.blesdk.obfuscated.yz1;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface GuestApiService {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, kc4 kc4, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    str3 = "android";
                }
                return guestApiService.getFirmwares(str, str2, str3, kc4);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @gt4("firmwares")
    Object getFirmwares(@st4("appVersion") String str, @st4("deviceModel") String str2, @st4("os") String str3, kc4<? super cs4<ApiResponse<Firmware>>> kc4);

    @DexIgnore
    @gt4("assets/app-localizations")
    Object getLocalizations(@st4("metadata.appVersion") String str, @st4("metadata.platform") String str2, kc4<? super cs4<ApiResponse<yz1>>> kc4);
}
