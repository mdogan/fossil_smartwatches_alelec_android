package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CategoryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "CategoryRepository";
    @DexIgnore
    public /* final */ CategoryDao mCategoryDao;
    @DexIgnore
    public /* final */ CategoryRemoteDataSource mCategoryRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public CategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        wd4.b(categoryDao, "mCategoryDao");
        wd4.b(categoryRemoteDataSource, "mCategoryRemoteDataSource");
        this.mCategoryDao = categoryDao;
        this.mCategoryRemoteDataSource = categoryRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadCategories(kc4<? super cb4> kc4) {
        CategoryRepository$downloadCategories$Anon1 categoryRepository$downloadCategories$Anon1;
        int i;
        CategoryRepository categoryRepository;
        ro2 ro2;
        if (kc4 instanceof CategoryRepository$downloadCategories$Anon1) {
            categoryRepository$downloadCategories$Anon1 = (CategoryRepository$downloadCategories$Anon1) kc4;
            int i2 = categoryRepository$downloadCategories$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                categoryRepository$downloadCategories$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = categoryRepository$downloadCategories$Anon1.result;
                Object a = oc4.a();
                i = categoryRepository$downloadCategories$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadCategories ");
                    CategoryRemoteDataSource categoryRemoteDataSource = this.mCategoryRemoteDataSource;
                    categoryRepository$downloadCategories$Anon1.L$Anon0 = this;
                    categoryRepository$downloadCategories$Anon1.label = 1;
                    obj = categoryRemoteDataSource.getAllCategory(categoryRepository$downloadCategories$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    categoryRepository = this;
                } else if (i == 1) {
                    categoryRepository = (CategoryRepository) categoryRepository$downloadCategories$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadCategories success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    sb.append(" response ");
                    sb.append((List) so2.a());
                    local.d(TAG, sb.toString());
                    if (!so2.b()) {
                        Object a2 = so2.a();
                        if (a2 == null) {
                            wd4.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            categoryRepository.mCategoryDao.upsertCategoryList((List) so2.a());
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadCategories fail!!! error=");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(TAG, sb2.toString());
                }
                return cb4.a;
            }
        }
        categoryRepository$downloadCategories$Anon1 = new CategoryRepository$downloadCategories$Anon1(this, kc4);
        Object obj2 = categoryRepository$downloadCategories$Anon1.result;
        Object a3 = oc4.a();
        i = categoryRepository$downloadCategories$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final List<Category> getAllCategories() {
        return this.mCategoryDao.getAllCategory();
    }
}
