package com.portfolio.platform.data.source;

import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface NotificationsDataSource {
    @DexIgnore
    void clearAllNotificationSetting();

    @DexIgnore
    void clearAllPhoneFavoritesContacts();

    @DexIgnore
    List<AppFilter> getAllAppFilterByHour(int i, int i2);

    @DexIgnore
    List<AppFilter> getAllAppFilterByVibration(int i);

    @DexIgnore
    List<AppFilter> getAllAppFilters(int i);

    @DexIgnore
    List<ContactGroup> getAllContactGroups(int i);

    @DexIgnore
    SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i);

    @DexIgnore
    AppFilter getAppFilterByType(String str, int i);

    @DexIgnore
    Contact getContactById(int i);

    @DexIgnore
    List<Integer> getContactGroupId(List<Integer> list);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingEmail(String str, int i);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingSms(String str, int i);

    @DexIgnore
    List<Integer> getLocalContactId();

    @DexIgnore
    void removeAllAppFilters();

    @DexIgnore
    void removeAppFilter(AppFilter appFilter);

    @DexIgnore
    void removeContact(Contact contact);

    @DexIgnore
    void removeContactGroup(ContactGroup contactGroup);

    @DexIgnore
    void removeContactGroupList(List<ContactGroup> list);

    @DexIgnore
    void removeListAppFilter(List<AppFilter> list);

    @DexIgnore
    void removeListContact(List<Contact> list);

    @DexIgnore
    void removeLocalRedundantContact(List<Integer> list);

    @DexIgnore
    void removeLocalRedundantContactGroup(List<Integer> list);

    @DexIgnore
    void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact);

    @DexIgnore
    void removePhoneFavoritesContact(String str);

    @DexIgnore
    void removePhoneNumber(List<Integer> list);

    @DexIgnore
    void removePhoneNumberByContactGroupId(int i);

    @DexIgnore
    void removeRedundantContact();

    @DexIgnore
    void saveAppFilter(AppFilter appFilter);

    @DexIgnore
    void saveContact(Contact contact);

    @DexIgnore
    void saveContactGroup(ContactGroup contactGroup);

    @DexIgnore
    void saveContactGroupList(List<ContactGroup> list);

    @DexIgnore
    void saveEmailAddress(EmailAddress emailAddress);

    @DexIgnore
    void saveListAppFilters(List<AppFilter> list);

    @DexIgnore
    void saveListContact(List<Contact> list);

    @DexIgnore
    void saveListPhoneNumber(List<PhoneNumber> list);

    @DexIgnore
    void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact);

    @DexIgnore
    void savePhoneNumber(PhoneNumber phoneNumber);
}
