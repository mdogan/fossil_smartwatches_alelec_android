package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.g72;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitActiveTimeDao_Impl implements GFitActiveTimeDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __deletionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ g72 __gFitActiveTimeConverter; // = new g72();
    @DexIgnore
    public /* final */ mf __insertionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ xf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<GFitActiveTime> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitActiveTime`(`id`,`activeTimes`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(lg lgVar, GFitActiveTime gFitActiveTime) {
            lgVar.b(1, (long) gFitActiveTime.getId());
            String a = GFitActiveTimeDao_Impl.this.__gFitActiveTimeConverter.a(gFitActiveTime.getActiveTimes());
            if (a == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, a);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<GFitActiveTime> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitActiveTime` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(lg lgVar, GFitActiveTime gFitActiveTime) {
            lgVar.b(1, (long) gFitActiveTime.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitActiveTime";
        }
    }

    @DexIgnore
    public GFitActiveTimeDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGFitActiveTime = new Anon1(roomDatabase);
        this.__deletionAdapterOfGFitActiveTime = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitActiveTime.handle(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitActiveTime> getAllGFitActiveTime() {
        vf b = vf.b("SELECT * FROM gFitActiveTime", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "activeTimes");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitActiveTime gFitActiveTime = new GFitActiveTime(this.__gFitActiveTimeConverter.a(a.getString(b3)));
                gFitActiveTime.setId(a.getInt(b2));
                arrayList.add(gFitActiveTime);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitActiveTime(List<GFitActiveTime> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
