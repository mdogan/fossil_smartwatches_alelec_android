package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.blesdk.obfuscated.me;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionDifference extends me.d<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        wd4.b(workoutSession, "oldItem");
        wd4.b(workoutSession2, "newItem");
        return wd4.a((Object) workoutSession, (Object) workoutSession2);
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        wd4.b(workoutSession, "oldItem");
        wd4.b(workoutSession2, "newItem");
        return wd4.a((Object) workoutSession.getId(), (Object) workoutSession2.getId());
    }
}
