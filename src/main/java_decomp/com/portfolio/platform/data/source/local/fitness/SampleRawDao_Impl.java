package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.a72;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.x62;
import com.fossil.blesdk.obfuscated.xf;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ x62 __activityIntensitiesConverter; // = new x62();
    @DexIgnore
    public /* final */ a72 __dateLongStringConverter; // = new a72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteAllActivitySamples;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<SampleRaw> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw`(`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, sampleRaw.getId());
            }
            lgVar.b(2, (long) sampleRaw.getPinType());
            lgVar.b(3, (long) sampleRaw.getUaPinType());
            String a = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a == null) {
                lgVar.a(4);
            } else {
                lgVar.a(4, a);
            }
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a2 == null) {
                lgVar.a(5);
            } else {
                lgVar.a(5, a2);
            }
            if (sampleRaw.getSourceId() == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                lgVar.a(7);
            } else {
                lgVar.a(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                lgVar.a(8);
            } else {
                lgVar.a(8, sampleRaw.getMovementTypeValue());
            }
            lgVar.a(9, sampleRaw.getSteps());
            lgVar.a(10, sampleRaw.getCalories());
            lgVar.a(11, sampleRaw.getDistance());
            lgVar.b(12, (long) sampleRaw.getActiveTime());
            String a3 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a3 == null) {
                lgVar.a(13);
            } else {
                lgVar.a(13, a3);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                lgVar.a(14);
            } else {
                lgVar.a(14, sampleRaw.getTimeZoneID());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfSampleRaw = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        vf vfVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        vf b = vf.b("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        b.b(1, (long) i);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "pinType");
            int b4 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = bg.b(a, "steps");
            int b11 = bg.b(a, "calories");
            int b12 = bg.b(a, "distance");
            int b13 = bg.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = bg.b(a, "intensityDistInSteps");
            vfVar = b;
            try {
                int b15 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i2 = b4;
                int i3 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i5 = i3;
                    int i6 = b2;
                    sampleRaw.setPinType(a.getInt(i5));
                    int i7 = i2;
                    sampleRaw.setUaPinType(a.getInt(i7));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i2 = i7;
                    b2 = i6;
                    i3 = i5;
                    b5 = i4;
                }
                a.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b;
            a.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples() {
        vf vfVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        vf b = vf.b("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, "pinType");
            int b4 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = bg.b(a, "steps");
            int b11 = bg.b(a, "calories");
            int b12 = bg.b(a, "distance");
            int b13 = bg.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = bg.b(a, "intensityDistInSteps");
            vfVar = b;
            try {
                int b15 = bg.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b;
            a.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        vf vfVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        vf b = vf.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a = sampleRawDao_Impl.__dateLongStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = sampleRawDao_Impl.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a3 = cg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = bg.b(a3, "id");
            int b3 = bg.b(a3, "pinType");
            int b4 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = bg.b(a3, "steps");
            int b11 = bg.b(a3, "calories");
            int b12 = bg.b(a3, "distance");
            int b13 = bg.b(a3, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = bg.b(a3, "intensityDistInSteps");
            vfVar = b;
            try {
                int b15 = bg.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b6)), a3.getString(b7), a3.getString(b8), a3.getString(b9), a3.getDouble(b10), a3.getDouble(b11), a3.getDouble(b12), a3.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a3.getString(b14)), a3.getString(b15));
                    sampleRaw.setId(a3.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a3.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a3.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a3.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b;
            a3.close();
            vfVar.c();
            throw th;
        }
    }
}
