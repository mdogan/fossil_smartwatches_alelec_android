package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.st4;
import com.fossil.blesdk.obfuscated.yz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface GoogleApiService {
    @DexIgnore
    @gt4("geocode/json")
    Object getAddress(@st4("latlng") String str, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("geocode/json")
    Object getAddressWithType(@st4("latlng") String str, @st4("result_type") String str2, kc4<? super cs4<yz1>> kc4);
}
