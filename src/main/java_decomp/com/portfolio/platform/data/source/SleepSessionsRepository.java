package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.SleepSession;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.helper.GsonISOConvertDateTime;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.response.sleep.SleepSessionParse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSessionsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return SleepSessionsRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingSleepSessionsCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MFSleepSession> list);
    }

    /*
    static {
        String simpleName = SleepSessionsRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "SleepSessionsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSessionsRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, SleepDatabase sleepDatabase, FitnessDataDao fitnessDataDao) {
        wd4.b(sleepDao, "mSleepDao");
        wd4.b(apiServiceV2, "mApiService");
        wd4.b(sleepDatabase, "mSleepDatabase");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        this.mSleepDao = sleepDao;
        this.mApiService = apiServiceV2;
        this.mSleepDatabase = sleepDatabase;
        this.mFitnessDataDao = fitnessDataDao;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchSleepSessions$default(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, kc4 kc4, int i3, Object obj) {
        return sleepSessionsRepository.fetchSleepSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, kc4);
    }

    @DexIgnore
    private final boolean isExistsSleepSession(MFSleepSession mFSleepSession) {
        List<MFSleepSession> sleepSessions = this.mSleepDao.getSleepSessions(mFSleepSession.getDay().getTime());
        long startTime = (long) mFSleepSession.getStartTime();
        long endTime = (long) mFSleepSession.getEndTime();
        for (MFSleepSession mFSleepSession2 : sleepSessions) {
            long startTime2 = (long) mFSleepSession2.getStartTime();
            long endTime2 = (long) mFSleepSession2.getEndTime();
            if (startTime2 <= startTime && endTime2 >= startTime) {
                return true;
            }
            if (startTime2 <= endTime && endTime2 >= endTime) {
                return true;
            }
            if (startTime <= startTime2 && endTime2 <= endTime) {
                return true;
            }
            if (startTime <= endTime2 && endTime >= endTime2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private final ri4 saveSleepSessionsToServer(String str, List<MFSleepSession> list, PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SleepSessionsRepository$saveSleepSessionsToServer$Anon1(this, list, str, pushPendingSleepSessionsCallback, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        this.mSleepDao.deleteAllSleepSessions();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, kc4<? super ro2<SleepRecommendedGoal>> kc4) {
        SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon1;
        int i4;
        ro2 ro2;
        kc4<? super ro2<SleepRecommendedGoal>> kc42 = kc4;
        if (kc42 instanceof SleepSessionsRepository$downloadRecommendedGoals$Anon1) {
            sleepSessionsRepository$downloadRecommendedGoals$Anon1 = (SleepSessionsRepository$downloadRecommendedGoals$Anon1) kc42;
            int i5 = sleepSessionsRepository$downloadRecommendedGoals$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$downloadRecommendedGoals$Anon1.label = i5 - Integer.MIN_VALUE;
                SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon12 = sleepSessionsRepository$downloadRecommendedGoals$Anon1;
                Object obj = sleepSessionsRepository$downloadRecommendedGoals$Anon12.result;
                Object a = oc4.a();
                i4 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.label;
                if (i4 != 0) {
                    za4.a(obj);
                    SleepSessionsRepository$downloadRecommendedGoals$response$Anon1 sleepSessionsRepository$downloadRecommendedGoals$response$Anon1 = new SleepSessionsRepository$downloadRecommendedGoals$response$Anon1(this, i, i2, i3, str, (kc4) null);
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$Anon0 = this;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon0 = i;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon1 = i2;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon2 = i3;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$Anon1 = str;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.label = 1;
                    obj = ResponseKt.a(sleepSessionsRepository$downloadRecommendedGoals$response$Anon1, sleepSessionsRepository$downloadRecommendedGoals$Anon12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i4 == 1) {
                    String str2 = (String) sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$Anon1;
                    int i6 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon2;
                    int i7 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon1;
                    int i8 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$Anon0;
                    SleepSessionsRepository sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        return new so2((SleepRecommendedGoal) a2, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        sleepSessionsRepository$downloadRecommendedGoals$Anon1 = new SleepSessionsRepository$downloadRecommendedGoals$Anon1(this, kc42);
        SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon122 = sleepSessionsRepository$downloadRecommendedGoals$Anon1;
        Object obj2 = sleepSessionsRepository$downloadRecommendedGoals$Anon122.result;
        Object a3 = oc4.a();
        i4 = sleepSessionsRepository$downloadRecommendedGoals$Anon122.label;
        if (i4 != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchSleepSessions(Date date, Date date2, int i, int i2, kc4<? super ro2<yz1>> kc4) {
        SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon1;
        int i3;
        ro2 ro2;
        ro2 ro22;
        Object obj;
        int i4;
        Date date3;
        SleepSessionsRepository sleepSessionsRepository;
        int i5;
        Date date4 = date;
        Date date5 = date2;
        kc4<? super ro2<yz1>> kc42 = kc4;
        if (kc42 instanceof SleepSessionsRepository$fetchSleepSessions$Anon1) {
            sleepSessionsRepository$fetchSleepSessions$Anon1 = (SleepSessionsRepository$fetchSleepSessions$Anon1) kc42;
            int i6 = sleepSessionsRepository$fetchSleepSessions$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$fetchSleepSessions$Anon1.label = i6 - Integer.MIN_VALUE;
                SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon12 = sleepSessionsRepository$fetchSleepSessions$Anon1;
                Object obj2 = sleepSessionsRepository$fetchSleepSessions$Anon12.result;
                Object a = oc4.a();
                i3 = sleepSessionsRepository$fetchSleepSessions$Anon12.label;
                if (i3 != 0) {
                    za4.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchSleepSessions: start = " + date4 + ", end = " + date5);
                    SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1 sleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1 = new SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1(this, date, date2, i, i2, (kc4) null);
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon0 = this;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon1 = date4;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon2 = date5;
                    i5 = i;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon0 = i5;
                    int i7 = i2;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon1 = i7;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.label = 1;
                    Object a2 = ResponseKt.a(sleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1, sleepSessionsRepository$fetchSleepSessions$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    sleepSessionsRepository = this;
                } else if (i3 == 1) {
                    int i8 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon1;
                    i5 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon0;
                    za4.a(obj2);
                    i4 = i8;
                    date3 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon2;
                    date4 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon1;
                    sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon0;
                } else if (i3 == 2) {
                    ApiResponse apiResponse = (ApiResponse) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon5;
                    ArrayList arrayList = (ArrayList) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon4;
                    ro22 = (ro2) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon3;
                    int i9 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon1;
                    int i10 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon0;
                    Date date6 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon2;
                    Date date7 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon1;
                    SleepSessionsRepository sleepSessionsRepository2 = (SleepSessionsRepository) sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon0;
                    try {
                        za4.a(obj2);
                        obj = obj2;
                        return (ro2) obj;
                    } catch (Exception e) {
                        e = e;
                        ro2 = ro22;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj2;
                String str2 = null;
                if (!(ro2 instanceof so2)) {
                    if (((so2) ro2).a() != null) {
                        ArrayList arrayList2 = new ArrayList();
                        try {
                            sz1 sz1 = new sz1();
                            sz1.a(DateTime.class, new GsonConvertDateTime());
                            sz1.a(Date.class, new GsonConverterShortDate());
                            ApiResponse apiResponse2 = (ApiResponse) sz1.a().a(((yz1) ((so2) ro2).a()).toString(), new SleepSessionsRepository$fetchSleepSessions$responseDate$Anon1().getType());
                            if (apiResponse2 != null) {
                                List<SleepSessionParse> list = apiResponse2.get_items();
                                if (list != null) {
                                    for (SleepSessionParse mfSleepSessionBySleepSessionParse : list) {
                                        arrayList2.add(mfSleepSessionBySleepSessionParse.getMfSleepSessionBySleepSessionParse());
                                    }
                                }
                            }
                            if (!((so2) ro2).b()) {
                                sleepSessionsRepository.insert$app_fossilRelease(arrayList2);
                            }
                            if ((apiResponse2 != null ? apiResponse2.get_range() : null) == null) {
                                return ro2;
                            }
                            Range range = apiResponse2.get_range();
                            if (range == null) {
                                wd4.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return ro2;
                            } else {
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon0 = sleepSessionsRepository;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon1 = date4;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon2 = date3;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon0 = i5;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.I$Anon1 = i4;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon3 = ro2;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon4 = arrayList2;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$Anon5 = apiResponse2;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.label = 2;
                                obj = sleepSessionsRepository.fetchSleepSessions(date4, date3, i5 + i4, i4, sleepSessionsRepository$fetchSleepSessions$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                ro22 = ro2;
                                return (ro2) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("fetchSleepSessions exception=");
                            e.printStackTrace();
                            sb.append(cb4.a);
                            local2.d(str3, sb.toString());
                            return ro2;
                        }
                    }
                    return ro2;
                }
                if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("fetchSleepSessions Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str2 = message;
                            if (str2 == null) {
                                str2 = "";
                            }
                            sb2.append(str2);
                            local3.d(str4, sb2.toString());
                        }
                    }
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str2 = c2.getUserMessage();
                    }
                    if (str2 == null) {
                    }
                    sb2.append(str2);
                    local3.d(str4, sb2.toString());
                }
                return ro2;
            }
        }
        sleepSessionsRepository$fetchSleepSessions$Anon1 = new SleepSessionsRepository$fetchSleepSessions$Anon1(this, kc42);
        SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon122 = sleepSessionsRepository$fetchSleepSessions$Anon1;
        Object obj22 = sleepSessionsRepository$fetchSleepSessions$Anon122.result;
        Object a3 = oc4.a();
        i3 = sleepSessionsRepository$fetchSleepSessions$Anon122.label;
        if (i3 != 0) {
        }
        ro2 = (ro2) obj22;
        String str22 = null;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final List<MFSleepSession> getPendingSleepSessions(Date date, Date date2) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        SleepDao sleepDao = this.mSleepDao;
        Date n = sk2.n(date);
        wd4.a((Object) n, "DateHelper.getStartOfDay(startDate)");
        Date i = sk2.i(date2);
        wd4.a((Object) i, "DateHelper.getEndOfDay(endDate)");
        return sleepDao.getPendingSleepSessions(n, i);
    }

    @DexIgnore
    public final LiveData<ps3<List<MFSleepSession>>> getSleepSessionList(Date date, Date date2, boolean z) {
        wd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(date2, "end");
        Date n = sk2.n(date);
        Date i = sk2.i(date2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSessionList: start = " + n + ", end = " + i);
        LiveData<ps3<List<MFSleepSession>>> b = ic.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SleepSessionsRepository$getSleepSessionList$Anon1(this, n, i, z));
        wd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insert$app_fossilRelease(List<MFSleepSession> list) {
        wd4.b(list, "sleepSessionList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insert: sleepSessionList = " + list);
        for (MFSleepSession next : list) {
            if (next.getDate() >= 0) {
                MFSleepSession sleepSession = this.mSleepDao.getSleepSession((long) next.getRealEndTime());
                if ((sleepSession != null ? sleepSession.getUpdatedAt() : null) == null) {
                    next.setEditedSleepMinutes(Integer.valueOf(next.getRealSleepMinutes()));
                    next.setEditedStartTime(Integer.valueOf(next.getRealStartTime()));
                    next.setEditedEndTime(Integer.valueOf(next.getRealEndTime()));
                    next.setEditedSleepStateDistInMinute(next.getRealSleepStateDistInMinute());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "Insert sleep session - date=" + sk2.e(new Date(next.getDate())) + ", sleepDistribution=" + next.getEditedSleepStateDistInMinute());
                    this.mSleepDao.upsertSleepSession(next);
                } else if (sleepSession.getUpdatedAt().getMillis() < next.getUpdatedAt().getMillis()) {
                    next.setEditedSleepMinutes(Integer.valueOf(next.getRealSleepMinutes()));
                    next.setEditedStartTime(Integer.valueOf(next.getRealStartTime()));
                    next.setEditedEndTime(Integer.valueOf(next.getRealEndTime()));
                    next.setEditedSleepStateDistInMinute(next.getRealSleepStateDistInMinute());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "Edit sleep session - date=" + sk2.e(new Date(next.getDate())) + ", sleepDistribution=" + next.getEditedSleepStateDistInMinute());
                    this.mSleepDao.upsertSleepSession(next);
                }
            }
        }
    }

    @DexIgnore
    public final void insertFromDevice(List<MFSleepSession> list) {
        wd4.b(list, "sleepSessionList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: sleepSessionList = " + list);
        for (MFSleepSession next : list) {
            if (isExistsSleepSession(next)) {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Sleep session already existed");
            } else if (this.mSleepDao.getSleepSession((long) next.getRealEndTime()) == null) {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Saving sleep session to local database");
                this.mSleepDao.addSleepSession(next);
            } else {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Sleep session already existed");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v5, resolved type: java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object insertSleepSessionList(String str, List<MFSleepSession> list, kc4<? super ro2<List<MFSleepSession>>> kc4) {
        SleepSessionsRepository$insertSleepSessionList$Anon1 sleepSessionsRepository$insertSleepSessionList$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof SleepSessionsRepository$insertSleepSessionList$Anon1) {
            sleepSessionsRepository$insertSleepSessionList$Anon1 = (SleepSessionsRepository$insertSleepSessionList$Anon1) kc4;
            int i2 = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$insertSleepSessionList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSessionsRepository$insertSleepSessionList$Anon1.result;
                Object a = oc4.a();
                i = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "insertSleepSessionList sleepSessionList=" + list);
                    uz1 uz1 = new uz1();
                    sz1 sz1 = new sz1();
                    sz1.a(Date.class, new GsonISOConvertDateTime());
                    Gson a2 = sz1.a();
                    for (MFSleepSession sleepSession : list) {
                        try {
                            uz1.a(a2.b(new SleepSession(str, sleepSession), SleepSession.class));
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("insertSleepSessionList exception=");
                            e.printStackTrace();
                            sb.append(cb4.a);
                            local2.e(str4, sb.toString());
                        }
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    local3.d(str5, "insertSleepSessionList jsonArray=" + uz1);
                    yz1 yz1 = new yz1();
                    yz1.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) uz1);
                    SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 sleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 = new SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1(this, yz1, (kc4) null);
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon0 = this;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon1 = str;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon2 = list;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon3 = uz1;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon4 = a2;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon5 = yz1;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1, sleepSessionsRepository$insertSleepSessionList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon5;
                    Gson gson = (Gson) sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon4;
                    uz1 uz12 = (uz1) sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon3;
                    list = sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon2;
                    String str6 = (String) sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon1;
                    SleepSessionsRepository sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$insertSleepSessionList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    local4.d(str7, "insertSleepSession onResponse: response = " + ro2);
                    return new so2(list, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("insertSleepSession Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb2.append(str2);
                    local5.e(str8, sb2.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), qo2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        sleepSessionsRepository$insertSleepSessionList$Anon1 = new SleepSessionsRepository$insertSleepSessionList$Anon1(this, kc4);
        Object obj2 = sleepSessionsRepository$insertSleepSessionList$Anon1.result;
        Object a3 = oc4.a();
        i = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final void pushPendingSleepSessions(PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingActivities fitnessDb=" + this.mSleepDatabase);
        List<MFSleepSession> pendingSleepSessions = this.mSleepDao.getPendingSleepSessions();
        MFUser b = en2.p.a().n().b();
        String userId = b != null ? b.getUserId() : null;
        if (!(!pendingSleepSessions.isEmpty()) || TextUtils.isEmpty(userId)) {
            if (pushPendingSleepSessionsCallback != null) {
                pushPendingSleepSessionsCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
            }
        } else if (userId != null) {
            saveSleepSessionsToServer(userId, pendingSleepSessions, pushPendingSleepSessionsCallback);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void upsertRecommendedGoals(SleepRecommendedGoal sleepRecommendedGoal) {
        wd4.b(sleepRecommendedGoal, "recommendedGoal");
        this.mSleepDao.upsertSleepRecommendedGoal(sleepRecommendedGoal);
    }
}
