package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory implements Factory<InAppNotificationDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static InAppNotificationDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideInAppNotificationDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static InAppNotificationDatabase proxyProvideInAppNotificationDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        InAppNotificationDatabase provideInAppNotificationDatabase = portfolioDatabaseModule.provideInAppNotificationDatabase(portfolioApp);
        o44.a(provideInAppNotificationDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideInAppNotificationDatabase;
    }

    @DexIgnore
    public InAppNotificationDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
