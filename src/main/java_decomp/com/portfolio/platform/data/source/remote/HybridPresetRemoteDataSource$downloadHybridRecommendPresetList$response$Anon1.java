package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1", f = "HybridPresetRemoteDataSource.kt", l = {25}, m = "invokeSuspend")
public final class HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<HybridRecommendPreset>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPresetRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1(HybridPresetRemoteDataSource hybridPresetRemoteDataSource, String str, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = hybridPresetRemoteDataSource;
        this.$serial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1(this.this$Anon0, this.$serial, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
            String str = this.$serial;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getHybridRecommendPresetList(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
