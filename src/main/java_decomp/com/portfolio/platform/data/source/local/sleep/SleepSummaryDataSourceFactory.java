package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryDataSourceFactory extends md.b<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.a mListener;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(sleepDao, "mSleepDao");
        wd4.b(sleepDatabase, "mSleepDatabase");
        wd4.b(date, "mCreatedDate");
        wd4.b(i42, "mAppExecutors");
        wd4.b(aVar, "mListener");
        wd4.b(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDao = sleepDao;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = i42;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public md<Date, SleepSummary> create() {
        this.localSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDao, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.localSource;
        if (sleepSummaryLocalDataSource != null) {
            return sleepSummaryLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
