package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppLastSettingDao_Impl implements MicroAppLastSettingDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfMicroAppLastSetting;
    @DexIgnore
    public /* final */ xf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteMicroAppLastSettingById;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<MicroAppLastSetting> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppLastSetting`(`appId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, MicroAppLastSetting microAppLastSetting) {
            if (microAppLastSetting.getAppId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, microAppLastSetting.getAppId());
            }
            if (microAppLastSetting.getUpdatedAt() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, microAppLastSetting.getUpdatedAt());
            }
            if (microAppLastSetting.getSetting() == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, microAppLastSetting.getSetting());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppLastSetting WHERE appId=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppLastSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<MicroAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon4(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MicroAppLastSetting> call() throws Exception {
            Cursor a = cg.a(MicroAppLastSettingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "appId");
                int b2 = bg.b(a, "updatedAt");
                int b3 = bg.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new MicroAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public MicroAppLastSettingDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfMicroAppLastSetting = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteMicroAppLastSettingById = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteMicroAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteMicroAppLastSettingById.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    public List<MicroAppLastSetting> getAllMicroAppLastSetting() {
        vf b = vf.b("SELECT * FROM microAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "appId");
            int b3 = bg.b(a, "updatedAt");
            int b4 = bg.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"microAppLastSetting"}, false, new Anon4(vf.b("SELECT * FROM microAppLastSetting", 0)));
    }

    @DexIgnore
    public MicroAppLastSetting getMicroAppLastSetting(String str) {
        vf b = vf.b("SELECT * FROM microAppLastSetting WHERE appId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new MicroAppLastSetting(a.getString(bg.b(a, "appId")), a.getString(bg.b(a, "updatedAt")), a.getString(bg.b(a, MicroAppSetting.SETTING))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(microAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
