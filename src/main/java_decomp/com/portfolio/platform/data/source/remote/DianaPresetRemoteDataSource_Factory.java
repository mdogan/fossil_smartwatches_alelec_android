package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetRemoteDataSource_Factory implements Factory<DianaPresetRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Dot1Provider;

    @DexIgnore
    public DianaPresetRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Dot1Provider = provider;
    }

    @DexIgnore
    public static DianaPresetRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new DianaPresetRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static DianaPresetRemoteDataSource newDianaPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new DianaPresetRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static DianaPresetRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new DianaPresetRemoteDataSource(provider.get());
    }

    @DexIgnore
    public DianaPresetRemoteDataSource get() {
        return provideInstance(this.mApiServiceV2Dot1Provider);
    }
}
