package com.portfolio.platform.data.source.local;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingLocalDataSource implements ServerSettingDataSource {
    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        en2.p.a().l().addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        en2.p.a().l().a(list);
    }

    @DexIgnore
    public void clearData() {
        en2.p.a().l().a();
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        wd4.b(str, "key");
        return en2.p.a().l().getServerSettingByKey(str);
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        wd4.b(onGetServerSettingList, Constants.CALLBACK);
    }
}
