package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import java.util.Set;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionLocalDataSource extends nd<Long, WorkoutSession> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ FitnessDataDao fitnessDataDao;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = kl2.a(this.mHelper);
    @DexIgnore
    public /* final */ qf.c mObserver;
    @DexIgnore
    public int mOffset;
    @DexIgnore
    public /* final */ WorkoutDao workoutDao;
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends qf.c {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = workoutSessionLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return WorkoutSessionLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionLocalDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "WorkoutSessionLocalDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionLocalDataSource(WorkoutSessionRepository workoutSessionRepository2, FitnessDataDao fitnessDataDao2, WorkoutDao workoutDao2, FitnessDatabase fitnessDatabase, Date date, i42 i42, PagingRequestHelper.a aVar) {
        wd4.b(workoutSessionRepository2, "workoutSessionRepository");
        wd4.b(fitnessDataDao2, "fitnessDataDao");
        wd4.b(workoutDao2, "workoutDao");
        wd4.b(fitnessDatabase, "workoutDatabase");
        wd4.b(date, "currentDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.fitnessDataDao = fitnessDataDao2;
        this.workoutDao = workoutDao2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.listener = aVar;
        this.mHelper = new PagingRequestHelper(i42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "workout_session", new String[0]);
        this.workoutDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<WorkoutSession> getDataInDatabase(int i) {
        return this.workoutDao.getWorkoutSessionsInDateDesc(this.currentDate, i);
    }

    @DexIgnore
    private final ri4 loadData(PagingRequestHelper.b.a aVar, int i) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new WorkoutSessionLocalDataSource$loadData$Anon1(this, i, aVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ ri4 loadData$default(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, PagingRequestHelper.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return workoutSessionLocalDataSource.loadData(aVar, i);
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.workoutDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(nd.f<Long> fVar, nd.a<WorkoutSession> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - params = " + ((Long) fVar.a) + " & currentDate = " + this.currentDate);
        WorkoutDao workoutDao2 = this.workoutDao;
        Date date = this.currentDate;
        Key key = fVar.a;
        wd4.a((Object) key, "params.key");
        aVar.a(workoutDao2.getWorkoutSessionsInDateAfterDesc(date, ((Number) key).longValue(), fVar.b));
        this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new WorkoutSessionLocalDataSource$loadAfter$Anon1(this));
    }

    @DexIgnore
    public void loadBefore(nd.f<Long> fVar, nd.a<WorkoutSession> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(nd.e<Long> eVar, nd.c<WorkoutSession> cVar) {
        wd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - currentDate = " + this.currentDate);
        cVar.a(getDataInDatabase(eVar.a));
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new WorkoutSessionLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.workoutDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        wd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(WorkoutSession workoutSession) {
        wd4.b(workoutSession, "item");
        return Long.valueOf(workoutSession.getCreatedAt());
    }
}
