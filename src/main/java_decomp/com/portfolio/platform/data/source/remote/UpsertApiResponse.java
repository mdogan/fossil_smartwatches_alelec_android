package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpsertApiResponse<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public String _etag;
    @DexIgnore
    public List<T> _items; // = new ArrayList();
    @DexIgnore
    public String message;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final /* synthetic */ <T> Class<T> getType() {
            wd4.a(4, "T");
            throw null;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final String get_etag() {
        return this._etag;
    }

    @DexIgnore
    public final List<T> get_items() {
        return this._items;
    }

    @DexIgnore
    public final void setMessage(String str) {
        this.message = str;
    }

    @DexIgnore
    public final void set_etag(String str) {
        this._etag = str;
    }

    @DexIgnore
    public final void set_items(List<T> list) {
        wd4.b(list, "<set-?>");
        this._items = list;
    }
}
