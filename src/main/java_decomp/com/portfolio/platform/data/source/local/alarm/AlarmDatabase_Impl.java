package com.portfolio.platform.data.source.local.alarm;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '14bf1872d7012ddb8a9749aba4e0984f')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `alarm`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = AlarmDatabase_Impl.this.mDatabase = hgVar;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("id", new fg.a("id", "TEXT", false, 0));
            hashMap.put("uri", new fg.a("uri", "TEXT", true, 1));
            hashMap.put("title", new fg.a("title", "TEXT", true, 0));
            hashMap.put(AppFilter.COLUMN_HOUR, new fg.a(AppFilter.COLUMN_HOUR, "INTEGER", true, 0));
            hashMap.put(MFSleepGoal.COLUMN_MINUTE, new fg.a(MFSleepGoal.COLUMN_MINUTE, "INTEGER", true, 0));
            hashMap.put(Alarm.COLUMN_DAYS, new fg.a(Alarm.COLUMN_DAYS, "TEXT", false, 0));
            hashMap.put("isActive", new fg.a("isActive", "INTEGER", true, 0));
            hashMap.put("isRepeated", new fg.a("isRepeated", "INTEGER", true, 0));
            hashMap.put("createdAt", new fg.a("createdAt", "TEXT", false, 0));
            hashMap.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
            hashMap.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
            fg fgVar = new fg(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, Alarm.TABLE_NAME);
            if (!fgVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + fgVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `alarm`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), Alarm.TABLE_NAME);
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(5), "14bf1872d7012ddb8a9749aba4e0984f", "49074030507e5c41c4f79c122790dd1c");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }
}
