package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.o44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory implements Factory<CustomizeRealDataDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CustomizeRealDataDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideCustomizeRealDataDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static CustomizeRealDataDatabase proxyProvideCustomizeRealDataDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        CustomizeRealDataDatabase provideCustomizeRealDataDatabase = portfolioDatabaseModule.provideCustomizeRealDataDatabase(portfolioApp);
        o44.a(provideCustomizeRealDataDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideCustomizeRealDataDatabase;
    }

    @DexIgnore
    public CustomizeRealDataDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
