package com.portfolio.platform.data.source.local.inapp;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class InAppNotificationDatabase extends RoomDatabase {
    @DexIgnore
    public abstract InAppNotificationDao inAppNotificationDao();
}
