package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$Anon2", f = "SleepSummariesRepository.kt", l = {140}, m = "invokeSuspend")
public final class SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 extends SuspendLambda implements jd4<kc4<? super cs4<MFSleepSettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yz1 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(SleepSummariesRepository sleepSummariesRepository, yz1 yz1, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = sleepSummariesRepository;
        this.$jsonObject = yz1;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this.this$Anon0, this.$jsonObject, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SleepSummariesRepository$pushLastSleepGoalToServer$Anon2) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            yz1 yz1 = this.$jsonObject;
            this.label = 1;
            obj = access$getMApiService$p.setSleepSetting(yz1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
