package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.pd;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryLocalDataSource extends pd<Date, ActivitySummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public /* final */ ActivitySummaryDao mActivitySummaryDao;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ yk2 mFitnessHelper;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = kl2.a(this.mHelper);
    @DexIgnore
    public /* final */ qf.c mObserver;
    @DexIgnore
    public List<Pair<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ SummariesRepository mSummariesRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends qf.c {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = activitySummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wd4.b(date, "date");
            wd4.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = sk2.c(instance);
            if (sk2.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wd4.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitySummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryLocalDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "ActivitySummaryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitySummaryLocalDataSource(SummariesRepository summariesRepository, yk2 yk2, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(summariesRepository, "mSummariesRepository");
        wd4.b(yk2, "mFitnessHelper");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(activitySummaryDao, "mActivitySummaryDao");
        wd4.b(fitnessDatabase, "mFitnessDatabase");
        wd4.b(date, "mCreatedDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        wd4.b(calendar, "key");
        this.mSummariesRepository = summariesRepository;
        this.mFitnessHelper = yk2;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mActivitySummaryDao = activitySummaryDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new PagingRequestHelper(i42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, SampleDay.TABLE_NAME, new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = sk2.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wd4.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (sk2.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final void calculateSummaries(List<ActivitySummary> list) {
        int i;
        int i2;
        int i3;
        int i4;
        List<ActivitySummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "endCalendar");
            instance.setTime(((ActivitySummary) wb4.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar p = sk2.p(instance.getTime());
                wd4.a((Object) p, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                ActivitySummaryDao activitySummaryDao = this.mActivitySummaryDao;
                Date time = p.getTime();
                wd4.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                wd4.a((Object) time2, "endCalendar.time");
                ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummaryDao.getTotalValuesOfWeek(time, time2);
                i2 = (int) totalValuesOfWeek.getTotalStepsOfWeek();
                i = (int) totalValuesOfWeek.getTotalCaloriesOfWeek();
                i3 = totalValuesOfWeek.getTotalActiveTimeOfWeek();
            } else {
                i3 = 0;
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            wd4.a((Object) instance2, "calendar");
            instance2.setTime(((ActivitySummary) wb4.d(list)).getDate());
            Calendar p2 = sk2.p(instance2.getTime());
            wd4.a((Object) p2, "DateHelper.getStartOfWeek(calendar.time)");
            p2.add(5, -1);
            int i5 = 0;
            int i6 = 0;
            double d = 0.0d;
            double d2 = 0.0d;
            int i7 = 0;
            for (T next : list) {
                int i8 = i6 + 1;
                if (i6 >= 0) {
                    ActivitySummary activitySummary = (ActivitySummary) next;
                    if (sk2.d(activitySummary.getDate(), p2.getTime())) {
                        i4 = i6;
                        list2.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
                        p2.add(5, -7);
                        i5 = i4;
                        d = 0.0d;
                        d2 = 0.0d;
                        i7 = 0;
                    } else {
                        i4 = i6;
                    }
                    d += activitySummary.getSteps();
                    d2 += activitySummary.getCalories();
                    i7 += activitySummary.getActiveTime();
                    if (i4 == list.size() - 1) {
                        d += (double) i2;
                        d2 += (double) i;
                        i7 += i3;
                    }
                    i6 = i8;
                } else {
                    ob4.c();
                    throw null;
                }
            }
            list2.get(i5).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i7));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummaries summaries.size=" + list.size());
    }

    @DexIgnore
    private final ActivitySummary dummySummary(ActivitySummary activitySummary, Date date) {
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendar");
        instance.setTime(date);
        ActivitySummary activitySummary2 = r1;
        ActivitySummary activitySummary3 = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), activitySummary.getTimezoneName(), activitySummary.getDstOffset(), 0.0d, 0.0d, 0.0d, ob4.d(0, 0, 0), 0, 0, 0, 0, 7680, (rd4) null);
        ActivitySummary activitySummary4 = activitySummary2;
        activitySummary4.setCreatedAt(DateTime.now());
        activitySummary4.setUpdatedAt(DateTime.now());
        return activitySummary4;
    }

    @DexIgnore
    private final List<ActivitySummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        Date date3 = date;
        Date date4 = date2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataInDatabase - startDate=" + date3 + ", endDate=" + date4);
        List<ActivitySummary> activitySummariesDesc = this.mActivitySummaryDao.getActivitySummariesDesc(sk2.b(date, date2) ? date4 : date3, date4);
        if (!activitySummariesDesc.isEmpty()) {
            Boolean s = sk2.s(((ActivitySummary) wb4.d(activitySummariesDesc)).getDate());
            wd4.a((Object) s, "DateHelper.isToday(summaries.first().getDate())");
            if (s.booleanValue()) {
                ((ActivitySummary) wb4.d(activitySummariesDesc)).setSteps(Math.max((double) this.mFitnessHelper.a(new Date()), ((ActivitySummary) wb4.d(activitySummariesDesc)).getSteps()));
            }
        }
        calculateSummaries(activitySummariesDesc);
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mActivitySummaryDao.getLastDate();
        if (lastDate == null) {
            lastDate = date3;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(activitySummariesDesc);
        ActivitySummary activitySummary = this.mActivitySummaryDao.getActivitySummary(date4);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "endCalendar");
        instance.setTime(date4);
        if (activitySummary == null) {
            activitySummary = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), "", 0, 0.0d, 0.0d, 0.0d, ob4.d(0, 0, 0), 0, 0, 0, 0, 7680, (rd4) null);
            activitySummary.setActiveTimeGoal(30);
            activitySummary.setStepGoal(VideoUploader.RETRY_DELAY_UNIT_MS);
            activitySummary.setCaloriesGoal(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        }
        if (!sk2.b(date3, lastDate)) {
            date3 = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + activitySummariesDesc.size() + ", summaryParent=" + activitySummary + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date3);
        while (sk2.c(date4, date3)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (sk2.d(((ActivitySummary) obj).getDate(), date4)) {
                    break;
                }
            }
            ActivitySummary activitySummary2 = (ActivitySummary) obj;
            if (activitySummary2 == null) {
                arrayList.add(dummySummary(activitySummary, date4));
            } else {
                arrayList.add(activitySummary2);
                arrayList2.remove(activitySummary2);
            }
            date4 = sk2.m(date4);
            wd4.a((Object) date4, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            ActivitySummary activitySummary3 = (ActivitySummary) wb4.d(arrayList);
            Boolean s2 = sk2.s(activitySummary3.getDate());
            wd4.a((Object) s2, "DateHelper.isToday(todaySummary.getDate())");
            if (s2.booleanValue()) {
                arrayList.add(0, new ActivitySummary(activitySummary3));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final void loadData(PagingRequestHelper.RequestType requestType, Date date, Date date2, PagingRequestHelper.b.a aVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadData start=" + date + ", end=" + date2);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ActivitySummaryLocalDataSource$loadData$Anon1(this, date, date2, requestType, aVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(pd.f<Date> fVar, pd.a<Date, ActivitySummary> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (sk2.b((Date) fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            wd4.a((Object) key2, "params.key");
            Date date = (Date) key2;
            Companion companion = Companion;
            Key key3 = fVar.a;
            wd4.a((Object) key3, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) key3, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date l = sk2.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : sk2.l(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + l + ", endQueryDate=" + date);
            wd4.a((Object) l, "startQueryDate");
            aVar.a(getDataInDatabase(l, date), calculateNextKey);
            if (sk2.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Pair(this.mStartDate, this.mEndDate));
                this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new ActivitySummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(pd.f<Date> fVar, pd.a<Date, ActivitySummary> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(pd.e<Date> eVar, pd.c<Date, ActivitySummary> cVar) {
        wd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date l = sk2.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : sk2.l(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + l + ", endQueryDate=" + date);
        wd4.a((Object) l, "startQueryDate");
        cVar.a(getDataInDatabase(l, date), null, this.key.getTime());
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new ActivitySummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wd4.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        wd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wd4.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
