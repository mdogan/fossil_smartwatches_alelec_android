package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon1;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon1.Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<I, O> implements m3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ List $resultList;
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(ActivitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1 activitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1, List list) {
            this.this$Anon0 = activitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1;
            this.$resultList = list;
        }

        @DexIgnore
        public final List<ActivitySample> apply(List<ActivitySample> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getActivityList getActivityList Transformations activitySamples=" + list);
            wd4.a((Object) list, "activitySamples");
            if (!list.isEmpty()) {
                List list2 = this.$resultList;
                this.this$Anon0.this$Anon0.this$Anon0.this$Anon0.mFitnessHelper.a(list, list.get(0).getUid());
                list2.addAll(list);
            }
            return this.$resultList;
        }
    }

    @DexIgnore
    public ActivitiesRepository$getActivityList$Anon1$Anon1$loadFromDb$Anon1(ActivitiesRepository$getActivityList$Anon1.Anon1 anon1) {
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> apply(List<ActivitySample> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = ActivitiesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getActivityList loadFromDb: resultList=" + list);
        ActivitiesRepository$getActivityList$Anon1 activitiesRepository$getActivityList$Anon1 = this.this$Anon0.this$Anon0;
        ActivitiesRepository activitiesRepository = activitiesRepository$getActivityList$Anon1.this$Anon0;
        Date date = activitiesRepository$getActivityList$Anon1.$endDate;
        wd4.a((Object) date, GoalPhase.COLUMN_END_DATE);
        return ic.a(activitiesRepository.getActivitySamplesInDate$app_fossilRelease(date), new Anon1(this, list));
    }
}
