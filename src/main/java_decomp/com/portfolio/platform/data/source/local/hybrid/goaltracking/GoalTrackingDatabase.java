package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class GoalTrackingDatabase extends RoomDatabase {
    @DexIgnore
    public abstract GoalTrackingDao getGoalTrackingDao();
}
