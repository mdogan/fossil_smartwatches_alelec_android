package com.portfolio.platform.data.source.local.inapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationDao_Impl extends InAppNotificationDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __deletionAdapterOfInAppNotification;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfInAppNotification;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<InAppNotification> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inAppNotification`(`id`,`title`,`content`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, inAppNotification.getId());
            }
            if (inAppNotification.getTitle() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, inAppNotification.getTitle());
            }
            if (inAppNotification.getContent() == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, inAppNotification.getContent());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<InAppNotification> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `inAppNotification` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(lg lgVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, inAppNotification.getId());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InAppNotification>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<InAppNotification> call() throws Exception {
            Cursor a = cg.a(InAppNotificationDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "id");
                int b2 = bg.b(a, "title");
                int b3 = bg.b(a, "content");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InAppNotification(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InAppNotificationDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfInAppNotification = new Anon1(roomDatabase);
        this.__deletionAdapterOfInAppNotification = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void delete(InAppNotification inAppNotification) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfInAppNotification.handle(inAppNotification);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public LiveData<List<InAppNotification>> getAllInAppNotification() {
        return this.__db.getInvalidationTracker().a(new String[]{"inAppNotification"}, false, new Anon3(vf.b("SELECT * FROM inAppNotification", 0)));
    }

    @DexIgnore
    public void insertListInAppNotification(List<InAppNotification> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInAppNotification.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
