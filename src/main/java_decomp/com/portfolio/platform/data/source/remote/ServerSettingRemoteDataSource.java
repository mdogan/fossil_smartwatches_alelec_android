package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRemoteDataSource implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 apiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRemoteDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerSettingRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "ServerSettingRemoteDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "apiService");
        this.apiService = apiServiceV2;
    }

    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        wd4.b(str, "key");
        return null;
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        wd4.b(onGetServerSettingList, Constants.CALLBACK);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this, onGetServerSettingList, (kc4) null), 3, (Object) null);
    }
}
