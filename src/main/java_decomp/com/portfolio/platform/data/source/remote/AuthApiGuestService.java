package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nt4;
import com.fossil.blesdk.obfuscated.yz1;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AuthApiGuestService {
    @DexIgnore
    @nt4("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @nt4("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @nt4("rpc/auth/login")
    Object loginWithEmail(@bt4 yz1 yz1, kc4<? super cs4<Auth>> kc4);

    @DexIgnore
    @nt4("rpc/auth/login-with")
    Object loginWithSocial(@bt4 yz1 yz1, kc4<? super cs4<Auth>> kc4);

    @DexIgnore
    @nt4("rpc/auth/password/request-reset")
    Object passwordRequestReset(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @nt4("rpc/auth/register")
    Object registerEmail(@bt4 SignUpEmailAuth signUpEmailAuth, kc4<? super cs4<Auth>> kc4);

    @DexIgnore
    @nt4("rpc/auth/register-with")
    Object registerSocial(@bt4 SignUpSocialAuth signUpSocialAuth, kc4<? super cs4<Auth>> kc4);

    @DexIgnore
    @nt4("rpc/auth/request-email-otp")
    Object requestEmailOtp(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @nt4("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@bt4 yz1 yz1);

    @DexIgnore
    @nt4("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@bt4 yz1 yz1);

    @DexIgnore
    @nt4("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);
}
