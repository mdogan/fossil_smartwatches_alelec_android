package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.st4;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface WechatApiService {
    @DexIgnore
    @gt4("oauth2/access_token")
    Call<WechatToken> getWechatToken(@st4("appid") String str, @st4("secret") String str2, @st4("code") String str3, @st4("grant_type") String str4);
}
