package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.pd;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryLocalDataSource extends pd<Date, SleepSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = kl2.a(this.mHelper);
    @DexIgnore
    public /* final */ qf.c mObserver;
    @DexIgnore
    public List<Pair<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends qf.c {
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = sleepSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wd4.b(date, "date");
            wd4.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = sk2.c(instance);
            if (sk2.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wd4.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public SleepSummaryLocalDataSource(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wd4.b(fitnessDataRepository, "mFitnessDataRepository");
        wd4.b(sleepDao, "mSleepDao");
        wd4.b(sleepDatabase, "mSleepDatabase");
        wd4.b(date, "mCreatedDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        wd4.b(calendar, "key");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDao = sleepDao;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new PagingRequestHelper(i42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, MFSleepDay.TABLE_NAME, new String[]{MFSleepSession.TABLE_NAME});
        this.mSleepDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = sk2.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wd4.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (sk2.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<SleepSummary> calculateSummaries(List<SleepSummary> list) {
        int i;
        List<SleepSummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "endCalendar");
            instance.setTime(((SleepSummary) wb4.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar p = sk2.p(instance.getTime());
                wd4.a((Object) p, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                SleepDao sleepDao = this.mSleepDao;
                Date time = p.getTime();
                wd4.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                wd4.a((Object) time2, "endCalendar.time");
                i = sleepDao.getTotalSleep(time, time2);
            } else {
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            wd4.a((Object) instance2, "calendar");
            instance2.setTime(((SleepSummary) wb4.d(list)).getDate());
            Calendar p2 = sk2.p(instance2.getTime());
            wd4.a((Object) p2, "DateHelper.getStartOfWeek(calendar.time)");
            p2.add(5, -1);
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            double d = 0.0d;
            for (T next : list) {
                int i5 = i3 + 1;
                if (i3 >= 0) {
                    SleepSummary sleepSummary = (SleepSummary) next;
                    if (sk2.d(sleepSummary.getDate(), p2.getTime())) {
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = list2.get(i2).getSleepDay();
                        if (sleepDay != null) {
                            if (i4 > 1) {
                                d /= (double) i4;
                            }
                            sleepDay.setAverageSleepOfWeek(Double.valueOf(d));
                        }
                        p2.add(5, -7);
                        i2 = i3;
                        i4 = 0;
                        d = 0.0d;
                    }
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay2 = sleepSummary.getSleepDay();
                    d += (double) (sleepDay2 != null ? sleepDay2.getSleepMinutes() : 0);
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay3 = sleepSummary.getSleepDay();
                    if ((sleepDay3 != null ? sleepDay3.getSleepMinutes() : 0) > 0) {
                        i4++;
                    }
                    if (i3 == list.size() - 1) {
                        d += (double) i;
                    }
                    i3 = i5;
                } else {
                    ob4.c();
                    throw null;
                }
            }
            com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay4 = list2.get(i2).getSleepDay();
            if (sleepDay4 != null) {
                if (i4 > 1) {
                    d /= (double) i4;
                }
                sleepDay4.setAverageSleepOfWeek(Double.valueOf(d));
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        return list2;
    }

    @DexIgnore
    private final void combineData(PagingRequestHelper.RequestType requestType, ro2<yz1> ro2, ro2<yz1> ro22, PagingRequestHelper.b.a aVar) {
        if ((ro2 instanceof so2) && (ro22 instanceof so2)) {
            if (requestType == PagingRequestHelper.RequestType.AFTER && (!this.mRequestAfterQueue.isEmpty())) {
                this.mRequestAfterQueue.remove(0);
            }
            aVar.a();
        } else if (ro2 instanceof qo2) {
            qo2 qo2 = (qo2) ro2;
            if (qo2.d() != null) {
                aVar.a(qo2.d());
            } else if (qo2.c() != null) {
                ServerError c = qo2.c();
                String userMessage = c.getUserMessage();
                String message = userMessage != null ? userMessage : c.getMessage();
                if (message == null) {
                    message = "";
                }
                aVar.a(new Throwable(message));
            }
        } else if (ro22 instanceof qo2) {
            qo2 qo22 = (qo2) ro22;
            if (qo22.d() != null) {
                aVar.a(qo22.d());
            } else if (qo22.c() != null) {
                ServerError c2 = qo22.c();
                String userMessage2 = c2.getUserMessage();
                String message2 = userMessage2 != null ? userMessage2 : c2.getMessage();
                if (message2 == null) {
                    message2 = "";
                }
                aVar.a(new Throwable(message2));
            }
        }
    }

    @DexIgnore
    private final SleepSummary dummySummary(SleepSummary sleepSummary, Date date) {
        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = sleepSummary.getSleepDay();
        return new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date, sleepDay != null ? sleepDay.getGoalMinutes() : 0, 0, new SleepDistribution(0, 0, 0, 7, (rd4) null), DateTime.now(), DateTime.now()), (List) null, 2, (rd4) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0127  */
    private final List<SleepSummary> getDataInDatabase(Date date, Date date2) {
        Date date3;
        SleepSummary sleepSummary;
        Object obj;
        Date date4;
        Object obj2;
        Date date5 = date;
        Date date6 = date2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getDataInDatabase - startDate=" + date5 + ", endDate=" + date6);
        List<SleepSummary> calculateSummaries = calculateSummaries(this.mSleepDao.getSleepSummariesDesc(sk2.b(date, date2) ? date6 : date5, date6));
        ArrayList arrayList = new ArrayList();
        com.portfolio.platform.data.model.room.sleep.MFSleepDay lastSleepDay = this.mSleepDao.getLastSleepDay();
        if (lastSleepDay != null) {
            Date date7 = lastSleepDay.getDate();
            if (date7 != null) {
                date3 = date7;
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(calculateSummaries);
                sleepSummary = this.mSleepDao.getSleepSummary(date6);
                if (sleepSummary != null) {
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay = r2;
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay mFSleepDay2 = new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date2, this.mSleepDao.getNearestSleepGoalFromDate(date6), 0, new SleepDistribution(0, 0, 0, 7, (rd4) null), DateTime.now(), DateTime.now());
                    obj = null;
                    sleepSummary = new SleepSummary(mFSleepDay, (List) null, 2, (rd4) null);
                } else {
                    obj = null;
                }
                if (!sk2.b(date5, date3)) {
                    date5 = date3;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary + ", " + "lastDate=" + date3 + ", startDateToFill=" + date5);
                date4 = date2;
                while (sk2.c(date4, date5)) {
                    Iterator it = arrayList2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj2 = obj;
                            break;
                        }
                        obj2 = it.next();
                        if (sk2.d(((SleepSummary) obj2).getDate(), date4)) {
                            break;
                        }
                    }
                    SleepSummary sleepSummary2 = (SleepSummary) obj2;
                    if (sleepSummary2 == null) {
                        arrayList.add(dummySummary(sleepSummary, date4));
                    } else {
                        arrayList.add(sleepSummary2);
                        arrayList2.remove(sleepSummary2);
                    }
                    date4 = sk2.m(date4);
                    wd4.a((Object) date4, "DateHelper.getPrevDate(endDateToFill)");
                }
                if (!arrayList.isEmpty()) {
                    SleepSummary sleepSummary3 = (SleepSummary) wb4.d(arrayList);
                    Boolean s = sk2.s(sleepSummary3.getDate());
                    wd4.a((Object) s, "DateHelper.isToday(todaySummary.getDate())");
                    if (s.booleanValue()) {
                        arrayList.add(0, sleepSummary3.copy(sleepSummary3.getSleepDay(), sleepSummary3.getSleepSessions()));
                    }
                }
                return arrayList;
            }
        }
        date3 = date5;
        ArrayList arrayList22 = new ArrayList();
        arrayList22.addAll(calculateSummaries);
        sleepSummary = this.mSleepDao.getSleepSummary(date6);
        if (sleepSummary != null) {
        }
        if (!sk2.b(date5, date3)) {
        }
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        local22.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary + ", " + "lastDate=" + date3 + ", startDateToFill=" + date5);
        date4 = date2;
        while (sk2.c(date4, date5)) {
        }
        if (!arrayList.isEmpty()) {
        }
        return arrayList;
    }

    @DexIgnore
    private final ri4 loadData(PagingRequestHelper.RequestType requestType, Date date, Date date2, PagingRequestHelper.b.a aVar) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SleepSummaryLocalDataSource$loadData$Anon1(this, date, date2, requestType, aVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mSleepDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(pd.f<Date> fVar, pd.a<Date, SleepSummary> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (sk2.b((Date) fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            wd4.a((Object) key2, "params.key");
            Date date = (Date) key2;
            Companion companion = Companion;
            Key key3 = fVar.a;
            wd4.a((Object) key3, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) key3, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date l = sk2.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : sk2.l(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + l + ", endQueryDate=" + date);
            wd4.a((Object) l, "startQueryDate");
            aVar.a(getDataInDatabase(l, date), this.key.getTime());
            if (sk2.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Pair(this.mStartDate, this.mEndDate));
                this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new SleepSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(pd.f<Date> fVar, pd.a<Date, SleepSummary> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(pd.e<Date> eVar, pd.c<Date, SleepSummary> cVar) {
        wd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date l = sk2.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : sk2.l(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + l + ", endQueryDate=" + date);
        wd4.a((Object) l, "startQueryDate");
        cVar.a(getDataInDatabase(l, date), null, this.key.getTime());
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new SleepSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mSleepDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wd4.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        wd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wd4.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
