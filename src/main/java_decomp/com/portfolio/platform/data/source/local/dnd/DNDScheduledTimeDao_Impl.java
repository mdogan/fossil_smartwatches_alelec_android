package com.portfolio.platform.data.source.local.dnd;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DNDScheduledTimeDao_Impl implements DNDScheduledTimeDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfDNDScheduledTimeModel;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDelete;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<DNDScheduledTimeModel> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dndScheduledTimeModel`(`scheduledTimeName`,`minutes`,`scheduledTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, DNDScheduledTimeModel dNDScheduledTimeModel) {
            if (dNDScheduledTimeModel.getScheduledTimeName() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, dNDScheduledTimeModel.getScheduledTimeName());
            }
            lgVar.b(2, (long) dNDScheduledTimeModel.getMinutes());
            lgVar.b(3, (long) dNDScheduledTimeModel.getScheduledTimeType());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dndScheduledTimeModel";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DNDScheduledTimeModel> call() throws Exception {
            Cursor a = cg.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "scheduledTimeName");
                int b2 = bg.b(a, "minutes");
                int b3 = bg.b(a, "scheduledTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DNDScheduledTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<DNDScheduledTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon4(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public DNDScheduledTimeModel call() throws Exception {
            Cursor a = cg.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false);
            try {
                return a.moveToFirst() ? new DNDScheduledTimeModel(a.getString(bg.b(a, "scheduledTimeName")), a.getInt(bg.b(a, "minutes")), a.getInt(bg.b(a, "scheduledTimeType"))) : null;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DNDScheduledTimeDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfDNDScheduledTimeModel = new Anon1(roomDatabase);
        this.__preparedStmtOfDelete = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i) {
        vf b = vf.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.b(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new DNDScheduledTimeModel(a.getString(bg.b(a, "scheduledTimeName")), a.getInt(bg.b(a, "minutes")), a.getInt(bg.b(a, "scheduledTimeType"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i) {
        vf b = vf.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.b(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, new Anon3(vf.b("SELECT * FROM dndScheduledTimeModel", 0)));
    }

    @DexIgnore
    public List<DNDScheduledTimeModel> getListDNDScheduledTimeModel() {
        vf b = vf.b("SELECT * FROM dndScheduledTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "scheduledTimeName");
            int b3 = bg.b(a, "minutes");
            int b4 = bg.b(a, "scheduledTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DNDScheduledTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(dNDScheduledTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
