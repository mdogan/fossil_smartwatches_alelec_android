package com.portfolio.platform.data.source.local.sleep;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class SleepDatabase extends RoomDatabase {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ zf MIGRATION_FROM_2_TO_5; // = new SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(2, 5);
    @DexIgnore
    public static /* final */ zf MIGRATION_FROM_3_TO_9; // = new SleepDatabase$Companion$MIGRATION_FROM_3_TO_9$Anon1(3, 9);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepDatabase";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_5$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_3_TO_9$annotations() {
        }

        @DexIgnore
        public final zf getMIGRATION_FROM_2_TO_5() {
            return SleepDatabase.MIGRATION_FROM_2_TO_5;
        }

        @DexIgnore
        public final zf getMIGRATION_FROM_3_TO_9() {
            return SleepDatabase.MIGRATION_FROM_3_TO_9;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public abstract SleepDao sleepDao();
}
