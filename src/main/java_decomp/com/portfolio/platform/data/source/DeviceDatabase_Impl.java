package com.portfolio.platform.data.source;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ebb28bd48a859f6e14a64ae279368044')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `device`");
            hgVar.b("DROP TABLE IF EXISTS `SKU`");
            hgVar.b("DROP TABLE IF EXISTS `watchParam`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = DeviceDatabase_Impl.this.mDatabase = hgVar;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("major", new fg.a("major", "INTEGER", true, 0));
            hashMap.put("minor", new fg.a("minor", "INTEGER", true, 0));
            hashMap.put("createdAt", new fg.a("createdAt", "TEXT", false, 0));
            hashMap.put("updatedAt", new fg.a("updatedAt", "TEXT", false, 0));
            hashMap.put("owner", new fg.a("owner", "TEXT", false, 0));
            hashMap.put("productDisplayName", new fg.a("productDisplayName", "TEXT", false, 0));
            hashMap.put("manufacturer", new fg.a("manufacturer", "TEXT", false, 0));
            hashMap.put("softwareRevision", new fg.a("softwareRevision", "TEXT", false, 0));
            hashMap.put("hardwareRevision", new fg.a("hardwareRevision", "TEXT", false, 0));
            hashMap.put("deviceId", new fg.a("deviceId", "TEXT", true, 1));
            hashMap.put("macAddress", new fg.a("macAddress", "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new fg.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new fg.a(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new fg.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0));
            hashMap.put("vibrationStrength", new fg.a("vibrationStrength", "INTEGER", false, 0));
            hashMap.put("isActive", new fg.a("isActive", "INTEGER", true, 0));
            fg fgVar = new fg("device", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, "device");
            if (fgVar.equals(a)) {
                HashMap hashMap2 = new HashMap(8);
                hashMap2.put("createdAt", new fg.a("createdAt", "TEXT", false, 0));
                hashMap2.put("updatedAt", new fg.a("updatedAt", "TEXT", false, 0));
                hashMap2.put("serialNumberPrefix", new fg.a("serialNumberPrefix", "TEXT", true, 1));
                hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new fg.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0));
                hashMap2.put("deviceName", new fg.a("deviceName", "TEXT", false, 0));
                hashMap2.put("groupName", new fg.a("groupName", "TEXT", false, 0));
                hashMap2.put("gender", new fg.a("gender", "TEXT", false, 0));
                hashMap2.put("deviceType", new fg.a("deviceType", "TEXT", false, 0));
                fg fgVar2 = new fg("SKU", hashMap2, new HashSet(0), new HashSet(0));
                fg a2 = fg.a(hgVar, "SKU");
                if (fgVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(4);
                    hashMap3.put("prefixSerial", new fg.a("prefixSerial", "TEXT", true, 1));
                    hashMap3.put("versionMajor", new fg.a("versionMajor", "TEXT", false, 0));
                    hashMap3.put("versionMinor", new fg.a("versionMinor", "TEXT", false, 0));
                    hashMap3.put("data", new fg.a("data", "TEXT", false, 0));
                    fg fgVar3 = new fg("watchParam", hashMap3, new HashSet(0), new HashSet(0));
                    fg a3 = fg.a(hgVar, "watchParam");
                    if (!fgVar3.equals(a3)) {
                        throw new IllegalStateException("Migration didn't properly handle watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + fgVar3 + "\n Found:\n" + a3);
                    }
                    return;
                }
                throw new IllegalStateException("Migration didn't properly handle SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + fgVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle device(com.portfolio.platform.data.model.Device).\n Expected:\n" + fgVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `device`");
            a.b("DELETE FROM `SKU`");
            a.b("DELETE FROM `watchParam`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "device", "SKU", "watchParam");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(2), "ebb28bd48a859f6e14a64ae279368044", "a98fb7f948bc0742615d4020fcfa6e4e");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
