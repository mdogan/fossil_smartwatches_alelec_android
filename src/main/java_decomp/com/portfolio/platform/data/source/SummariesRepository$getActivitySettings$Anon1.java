package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getActivitySettings$Anon1 extends NetworkBoundResource<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$getActivitySettings$Anon1(SummariesRepository summariesRepository) {
        this.this$Anon0 = summariesRepository;
    }

    @DexIgnore
    public Object createCall(kc4<? super cs4<ActivitySettings>> kc4) {
        return this.this$Anon0.mApiServiceV2.getActivitySetting(kc4);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        wd4.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivitySettings - saveCallResult -- item=" + activitySettings);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$getActivitySettings$Anon1$saveCallResult$Anon1(this, activitySettings, (kc4) null), 3, (Object) null);
    }
}
