package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.al2;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository_Factory implements Factory<ThirdPartyRepository> {
    @DexIgnore
    public /* final */ Provider<ActivitiesRepository> mActivitiesRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<al2> mGoogleFitHelperProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;
    @DexIgnore
    public /* final */ Provider<ThirdPartyDatabase> mThirdPartyDatabaseProvider;

    @DexIgnore
    public ThirdPartyRepository_Factory(Provider<al2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        this.mGoogleFitHelperProvider = provider;
        this.mThirdPartyDatabaseProvider = provider2;
        this.mActivitiesRepositoryProvider = provider3;
        this.mPortfolioAppProvider = provider4;
    }

    @DexIgnore
    public static ThirdPartyRepository_Factory create(Provider<al2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        return new ThirdPartyRepository_Factory(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static ThirdPartyRepository newThirdPartyRepository(al2 al2, ThirdPartyDatabase thirdPartyDatabase, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        return new ThirdPartyRepository(al2, thirdPartyDatabase, activitiesRepository, portfolioApp);
    }

    @DexIgnore
    public static ThirdPartyRepository provideInstance(Provider<al2> provider, Provider<ThirdPartyDatabase> provider2, Provider<ActivitiesRepository> provider3, Provider<PortfolioApp> provider4) {
        return new ThirdPartyRepository(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public ThirdPartyRepository get() {
        return provideInstance(this.mGoogleFitHelperProvider, this.mThirdPartyDatabaseProvider, this.mActivitiesRepositoryProvider, this.mPortfolioAppProvider);
    }
}
