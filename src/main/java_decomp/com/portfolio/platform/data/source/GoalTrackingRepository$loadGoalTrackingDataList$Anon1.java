package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import java.util.Date;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository", f = "GoalTrackingRepository.kt", l = {390, 400}, m = "loadGoalTrackingDataList")
public final class GoalTrackingRepository$loadGoalTrackingDataList$Anon1 extends ContinuationImpl {
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public int I$Anon1;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadGoalTrackingDataList$Anon1(GoalTrackingRepository goalTrackingRepository, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = goalTrackingRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.loadGoalTrackingDataList((Date) null, (Date) null, 0, 0, this);
    }
}
