package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpSocialAuth;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1", f = "UserRemoteDataSource.kt", l = {202}, m = "invokeSuspend")
public final class UserRemoteDataSource$signUpSocial$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpSocialAuth $socialAuth;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$signUpSocial$response$Anon1(UserRemoteDataSource userRemoteDataSource, SignUpSocialAuth signUpSocialAuth, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = userRemoteDataSource;
        this.$socialAuth = signUpSocialAuth;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new UserRemoteDataSource$signUpSocial$response$Anon1(this.this$Anon0, this.$socialAuth, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((UserRemoteDataSource$signUpSocial$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            AuthApiGuestService access$getMAuthApiGuestService$p = this.this$Anon0.mAuthApiGuestService;
            SignUpSocialAuth signUpSocialAuth = this.$socialAuth;
            this.label = 1;
            obj = access$getMAuthApiGuestService$p.registerSocial(signUpSocialAuth, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
