package com.portfolio.platform.data.source.local.thirdparty;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `gFitSample`");
            hgVar.b("DROP TABLE IF EXISTS `gFitActiveTime`");
            hgVar.b("DROP TABLE IF EXISTS `gFitHeartRate`");
            hgVar.b("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            hgVar.b("DROP TABLE IF EXISTS `uaSample`");
            hgVar.b("DROP TABLE IF EXISTS `gFitSleep`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = ThirdPartyDatabase_Impl.this.mDatabase = hgVar;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            hg hgVar2 = hgVar;
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new fg.a("id", "INTEGER", true, 1));
            hashMap.put("step", new fg.a("step", "INTEGER", true, 0));
            hashMap.put("distance", new fg.a("distance", "REAL", true, 0));
            hashMap.put("calorie", new fg.a("calorie", "REAL", true, 0));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new fg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new fg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
            fg fgVar = new fg("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar2, "gFitSample");
            if (fgVar.equals(a)) {
                HashMap hashMap2 = new HashMap(2);
                hashMap2.put("id", new fg.a("id", "INTEGER", true, 1));
                hashMap2.put("activeTimes", new fg.a("activeTimes", "TEXT", true, 0));
                fg fgVar2 = new fg("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
                fg a2 = fg.a(hgVar2, "gFitActiveTime");
                if (fgVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(4);
                    hashMap3.put("id", new fg.a("id", "INTEGER", true, 1));
                    hashMap3.put("value", new fg.a("value", "REAL", true, 0));
                    hashMap3.put(SampleRaw.COLUMN_START_TIME, new fg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                    hashMap3.put(SampleRaw.COLUMN_END_TIME, new fg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                    fg fgVar3 = new fg("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
                    fg a3 = fg.a(hgVar2, "gFitHeartRate");
                    if (fgVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(8);
                        hashMap4.put("id", new fg.a("id", "INTEGER", true, 1));
                        hashMap4.put(SampleRaw.COLUMN_START_TIME, new fg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                        hashMap4.put(SampleRaw.COLUMN_END_TIME, new fg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                        hashMap4.put("workoutType", new fg.a("workoutType", "INTEGER", true, 0));
                        hashMap4.put("steps", new fg.a("steps", "TEXT", true, 0));
                        hashMap4.put("calories", new fg.a("calories", "TEXT", true, 0));
                        hashMap4.put("distances", new fg.a("distances", "TEXT", true, 0));
                        hashMap4.put("heartRates", new fg.a("heartRates", "TEXT", true, 0));
                        fg fgVar4 = new fg("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
                        fg a4 = fg.a(hgVar2, "gFitWorkoutSession");
                        if (fgVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(5);
                            hashMap5.put("id", new fg.a("id", "INTEGER", true, 1));
                            hashMap5.put("step", new fg.a("step", "INTEGER", true, 0));
                            hashMap5.put("distance", new fg.a("distance", "REAL", true, 0));
                            hashMap5.put("calorie", new fg.a("calorie", "REAL", true, 0));
                            hashMap5.put(LogBuilder.KEY_TIME, new fg.a(LogBuilder.KEY_TIME, "INTEGER", true, 0));
                            fg fgVar5 = new fg("uaSample", hashMap5, new HashSet(0), new HashSet(0));
                            fg a5 = fg.a(hgVar2, "uaSample");
                            if (fgVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(4);
                                hashMap6.put("id", new fg.a("id", "INTEGER", true, 1));
                                hashMap6.put("sleepMins", new fg.a("sleepMins", "INTEGER", true, 0));
                                hashMap6.put(SampleRaw.COLUMN_START_TIME, new fg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                                hashMap6.put(SampleRaw.COLUMN_END_TIME, new fg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                                fg fgVar6 = new fg("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
                                fg a6 = fg.a(hgVar2, "gFitSleep");
                                if (!fgVar6.equals(a6)) {
                                    throw new IllegalStateException("Migration didn't properly handle gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + fgVar6 + "\n Found:\n" + a6);
                                }
                                return;
                            }
                            throw new IllegalStateException("Migration didn't properly handle uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + fgVar5 + "\n Found:\n" + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + fgVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + fgVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + fgVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + fgVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `gFitSample`");
            a.b("DELETE FROM `gFitActiveTime`");
            a.b("DELETE FROM `gFitHeartRate`");
            a.b("DELETE FROM `gFitWorkoutSession`");
            a.b("DELETE FROM `uaSample`");
            a.b("DELETE FROM `gFitSleep`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
