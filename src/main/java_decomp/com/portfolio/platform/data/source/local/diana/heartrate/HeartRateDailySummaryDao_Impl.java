package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.b72;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.r72;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDailySummaryDao_Impl extends HeartRateDailySummaryDao {
    @DexIgnore
    public /* final */ b72 __dateShortStringConverter; // = new b72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfDailyHeartRateSummary;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteAllHeartRateSummaries;
    @DexIgnore
    public /* final */ r72 __restingConverter; // = new r72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<DailyHeartRateSummary> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `daily_heart_rate_summary`(`average`,`date`,`createdAt`,`updatedAt`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, DailyHeartRateSummary dailyHeartRateSummary) {
            lgVar.a(1, (double) dailyHeartRateSummary.getAverage());
            String a = HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(dailyHeartRateSummary.getDate());
            if (a == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, a);
            }
            lgVar.b(3, dailyHeartRateSummary.getCreatedAt());
            lgVar.b(4, dailyHeartRateSummary.getUpdatedAt());
            lgVar.b(5, (long) dailyHeartRateSummary.getMin());
            lgVar.b(6, (long) dailyHeartRateSummary.getMax());
            lgVar.b(7, (long) dailyHeartRateSummary.getMinuteCount());
            String a2 = HeartRateDailySummaryDao_Impl.this.__restingConverter.a(dailyHeartRateSummary.getResting());
            if (a2 == null) {
                lgVar.a(8);
            } else {
                lgVar.a(8, a2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM daily_heart_rate_summary";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DailyHeartRateSummary> call() throws Exception {
            Cursor a = cg.a(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = bg.b(a, "date");
                int b3 = bg.b(a, "createdAt");
                int b4 = bg.b(a, "updatedAt");
                int b5 = bg.b(a, "min");
                int b6 = bg.b(a, "max");
                int b7 = bg.b(a, "minuteCount");
                int b8 = bg.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(a.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getLong(b3), a.getLong(b4), a.getInt(b5), a.getInt(b6), a.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(a.getString(b8))));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends md.b<Integer, DailyHeartRateSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1 extends ag<DailyHeartRateSummary> {
            @DexIgnore
            public Anon1(RoomDatabase roomDatabase, vf vfVar, boolean z, String... strArr) {
                super(roomDatabase, vfVar, z, strArr);
            }

            @DexIgnore
            public List<DailyHeartRateSummary> convertRows(Cursor cursor) {
                Cursor cursor2 = cursor;
                int b = bg.b(cursor2, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = bg.b(cursor2, "date");
                int b3 = bg.b(cursor2, "createdAt");
                int b4 = bg.b(cursor2, "updatedAt");
                int b5 = bg.b(cursor2, "min");
                int b6 = bg.b(cursor2, "max");
                int b7 = bg.b(cursor2, "minuteCount");
                int b8 = bg.b(cursor2, "resting");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(cursor2.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(cursor2.getString(b2)), cursor2.getLong(b3), cursor2.getLong(b4), cursor2.getInt(b5), cursor2.getInt(b6), cursor2.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(cursor2.getString(b8))));
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon4(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public ag<DailyHeartRateSummary> create() {
            return new Anon1(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, "daily_heart_rate_summary");
        }
    }

    @DexIgnore
    public HeartRateDailySummaryDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfDailyHeartRateSummary = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllHeartRateSummaries = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllHeartRateSummaries() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteAllHeartRateSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSummaries.release(acquire);
        }
    }

    @DexIgnore
    public List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2) {
        vf b = vf.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a3, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = bg.b(a3, "date");
            int b4 = bg.b(a3, "createdAt");
            int b5 = bg.b(a3, "updatedAt");
            int b6 = bg.b(a3, "min");
            int b7 = bg.b(a3, "max");
            int b8 = bg.b(a3, "minuteCount");
            int b9 = bg.b(a3, "resting");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                arrayList.add(new DailyHeartRateSummary(a3.getFloat(b2), this.__dateShortStringConverter.a(a3.getString(b3)), a3.getLong(b4), a3.getLong(b5), a3.getInt(b6), a3.getInt(b7), a3.getInt(b8), this.__restingConverter.a(a3.getString(b9))));
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2) {
        vf b = vf.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"daily_heart_rate_summary"}, false, new Anon3(b));
    }

    @DexIgnore
    public DailyHeartRateSummary getDailyHeartRateSummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary;
        vf b = vf.b("SELECT * FROM daily_heart_rate_summary WHERE date = ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a2, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = bg.b(a2, "date");
            int b4 = bg.b(a2, "createdAt");
            int b5 = bg.b(a2, "updatedAt");
            int b6 = bg.b(a2, "min");
            int b7 = bg.b(a2, "max");
            int b8 = bg.b(a2, "minuteCount");
            int b9 = bg.b(a2, "resting");
            if (a2.moveToFirst()) {
                dailyHeartRateSummary = new DailyHeartRateSummary(a2.getFloat(b2), this.__dateShortStringConverter.a(a2.getString(b3)), a2.getLong(b4), a2.getLong(b5), a2.getInt(b6), a2.getInt(b7), a2.getInt(b8), this.__restingConverter.a(a2.getString(b9)));
            } else {
                dailyHeartRateSummary = null;
            }
            return dailyHeartRateSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public Date getLastDate() {
        Date date;
        vf b = vf.b("SELECT date FROM daily_heart_rate_summary ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            if (a.moveToFirst()) {
                date = this.__dateShortStringConverter.a(a.getString(0));
            } else {
                date = null;
            }
            return date;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public md.b<Integer, DailyHeartRateSummary> getSummariesDataSource() {
        return new Anon4(vf.b("SELECT * FROM daily_heart_rate_summary ORDER BY date DESC", 0));
    }

    @DexIgnore
    public void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(dailyHeartRateSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
