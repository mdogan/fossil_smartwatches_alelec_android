package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1", f = "GoalTrackingRepository.kt", l = {637}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1(GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3;
        this.$startDate = ref$ObjectRef;
        this.$endDate = ref$ObjectRef2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 = new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1(this.this$Anon0, this.$startDate, this.$endDate, kc4);
        goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1.p$ = (lh4) obj;
        return goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            if (this.this$Anon0.this$Anon0.loadSummaries((Date) this.$startDate.element, (Date) this.$endDate.element, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
