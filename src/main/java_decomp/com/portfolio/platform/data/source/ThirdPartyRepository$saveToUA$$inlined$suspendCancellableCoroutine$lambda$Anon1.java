package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.hr3;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pg4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ UAActivityTimeSeries $activityTimeSeries;
    @DexIgnore
    public /* final */ /* synthetic */ pg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfLists$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements hr3.d {
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0119Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0119Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0119Anon1 anon1 = new C0119Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0119Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    this.this$Anon0.this$Anon0.this$Anon0.getMThirdPartyDatabase().getUASampleDao().deleteListUASample(this.this$Anon0.this$Anon0.$sampleList);
                    return cb4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Anon1(ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$Anon0 = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public void onSuccess() {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "Sending UASample to UnderAmour successfully!");
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new C0119Anon1(this, (kc4) null), 3, (Object) null);
            ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 = this.this$Anon0;
            Ref$IntRef ref$IntRef = thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$countSizeOfLists$inlined;
            ref$IntRef.element++;
            if (ref$IntRef.element >= thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$sizeOfLists$inlined && thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.$continuation$inlined.isActive()) {
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveToUA");
                pg4 pg4 = this.this$Anon0.$continuation$inlined;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(UAActivityTimeSeries uAActivityTimeSeries, List list, kc4 kc4, Ref$IntRef ref$IntRef, int i, pg4 pg4, ThirdPartyRepository thirdPartyRepository) {
        super(2, kc4);
        this.$activityTimeSeries = uAActivityTimeSeries;
        this.$sampleList = list;
        this.$countSizeOfLists$inlined = ref$IntRef;
        this.$sizeOfLists$inlined = i;
        this.$continuation$inlined = pg4;
        this.this$Anon0 = thirdPartyRepository;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$activityTimeSeries, this.$sampleList, kc4, this.$countSizeOfLists$inlined, this.$sizeOfLists$inlined, this.$continuation$inlined, this.this$Anon0);
        thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (lh4) obj;
        return thirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            hr3 r = this.this$Anon0.getMPortfolioApp().r();
            UAActivityTimeSeries uAActivityTimeSeries = this.$activityTimeSeries;
            Anon1 anon1 = new Anon1(this);
            this.L$Anon0 = lh4;
            this.label = 1;
            if (r.a(uAActivityTimeSeries, (hr3.d) anon1, (kc4<? super cb4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
