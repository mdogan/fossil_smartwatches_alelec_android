package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.ct4;
import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.it4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mt4;
import com.fossil.blesdk.obfuscated.nt4;
import com.fossil.blesdk.obfuscated.ot4;
import com.fossil.blesdk.obfuscated.qm4;
import com.fossil.blesdk.obfuscated.rt4;
import com.fossil.blesdk.obfuscated.st4;
import com.fossil.blesdk.obfuscated.wt4;
import com.fossil.blesdk.obfuscated.yz1;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessData;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.User;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ApiServiceV2 {
    @DexIgnore
    @it4(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @it4(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@bt4 yz1 yz1, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @ct4("users/me/alarms/{id}")
    Object deleteAlarm(@rt4("id") String str, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @it4(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<yz1>>> kc4);

    @DexIgnore
    @ct4("users/me/devices/{deviceId}")
    Object deleteDevice(@rt4("deviceId") String str, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @it4(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@bt4 yz1 yz1, kc4<? super cs4<GoalEvent>> kc4);

    @DexIgnore
    @ct4("users/me")
    Object deleteUser(kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @gt4
    Object downloadFile(@wt4 String str, kc4<? super cs4<qm4>> kc4);

    @DexIgnore
    @gt4("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @nt4("rpc/device/generate-pairing-key")
    Object generatePairingKey(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("users/me/activities")
    Object getActivities(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<Activity>>> kc4);

    @DexIgnore
    @gt4("users/me/activity-settings")
    Object getActivitySetting(kc4<? super cs4<ActivitySettings>> kc4);

    @DexIgnore
    @gt4("users/me/activity-statistic")
    Object getActivityStatistic(kc4<? super cs4<ActivityStatistic>> kc4);

    @DexIgnore
    @gt4("users/me/alarms")
    Object getAlarms(@st4("limit") int i, kc4<? super cs4<ApiResponse<Alarm>>> kc4);

    @DexIgnore
    @gt4("diana-complication-apps")
    Object getAllComplication(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<Complication>>> kc4);

    @DexIgnore
    @gt4("hybrid-apps")
    Object getAllMicroApp(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<MicroApp>>> kc4);

    @DexIgnore
    @gt4("hybrid-app-variants")
    Object getAllMicroAppVariant(@st4("serialNumber") String str, @st4("majorNumber") String str2, @st4("minorNumber") String str3, kc4<? super cs4<ApiResponse<MicroAppVariant>>> kc4);

    @DexIgnore
    @gt4("diana-pusher-apps")
    Object getAllWatchApp(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<WatchApp>>> kc4);

    @DexIgnore
    @gt4("app-categories")
    Object getCategories(kc4<? super cs4<ApiResponse<Category>>> kc4);

    @DexIgnore
    @gt4("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<yz1>>> kc4);

    @DexIgnore
    @gt4("users/me/devices/{deviceId}")
    Object getDevice(@rt4("deviceId") String str, kc4<? super cs4<Device>> kc4);

    @DexIgnore
    @gt4("assets/app-sku-images")
    Object getDeviceAssets(@st4("size") int i, @st4("offset") int i2, @st4("metadata.serialNumber") String str, @st4("metadata.feature") String str2, @st4("metadata.resolution") String str3, @st4("metadata.platform") String str4, kc4<? super cs4<ApiResponse<yz1>>> kc4);

    @DexIgnore
    @gt4("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@rt4("id") String str, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("users/me/devices")
    Object getDevices(kc4<? super cs4<ApiResponse<Device>>> kc4);

    @DexIgnore
    @gt4("users/me/diana-presets")
    Object getDianaPresetList(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<DianaPreset>>> kc4);

    @DexIgnore
    @gt4("diana-recommended-presets")
    Object getDianaRecommendPresetList(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<DianaRecommendPreset>>> kc4);

    @DexIgnore
    @gt4("users/me/goal-settings")
    Object getGoalSetting(kc4<? super cs4<GoalSetting>> kc4);

    @DexIgnore
    @gt4("users/me/goal-events")
    Object getGoalTrackingDataList(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<GoalEvent>>> kc4);

    @DexIgnore
    @gt4("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<GoalDailySummary>>> kc4);

    @DexIgnore
    @gt4("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@rt4("date") String str, kc4<? super cs4<GoalDailySummary>> kc4);

    @DexIgnore
    @gt4("users/me/heart-rates")
    Object getHeartRateSamples(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<HeartRate>>> kc4);

    @DexIgnore
    @gt4("users/me/hybrid-presets")
    Object getHybridPresetList(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<HybridPreset>>> kc4);

    @DexIgnore
    @gt4("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<HybridRecommendPreset>>> kc4);

    @DexIgnore
    @gt4("users/me/devices/latest-active")
    Object getLastActiveDevice(kc4<? super cs4<Device>> kc4);

    @DexIgnore
    @gt4("assets/watch-params")
    Object getLatestWatchParams(@st4("metadata.serialNumber") String str, @st4("metadata.version.major") int i, @st4("sortBy") String str2, @st4("offset") int i2, @st4("limit") int i3, kc4<? super cs4<ApiResponse<WatchParameterResponse>>> kc4);

    @DexIgnore
    @gt4("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@st4("age") int i, @st4("weightInGrams") int i2, @st4("heightInCentimeters") int i3, @st4("gender") String str, kc4<? super cs4<ActivityRecommendedGoals>> kc4);

    @DexIgnore
    @gt4("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@st4("age") int i, @st4("weightInGrams") int i2, @st4("heightInCentimeters") int i3, @st4("gender") String str, kc4<? super cs4<SleepRecommendedGoal>> kc4);

    @DexIgnore
    @gt4("server-settings")
    Object getServerSettingList(@st4("limit") int i, @st4("offset") int i2, kc4<? super cs4<ServerSettingList>> kc4);

    @DexIgnore
    @gt4("skus")
    Object getSkus(@st4("limit") int i, @st4("offset") int i2, kc4<? super cs4<ApiResponse<SKUModel>>> kc4);

    @DexIgnore
    @gt4("users/me/sleep-sessions")
    Object getSleepSessions(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("users/me/sleep-settings")
    Object getSleepSetting(kc4<? super cs4<MFSleepSettings>> kc4);

    @DexIgnore
    @gt4("users/me/sleep-statistic")
    Object getSleepStatistic(kc4<? super cs4<SleepStatistic>> kc4);

    @DexIgnore
    @gt4("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("users/me/activity-daily-summaries")
    Object getSummaries(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @nt4("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@bt4 TrafficRequest trafficRequest, kc4<? super cs4<TrafficResponse>> kc4);

    @DexIgnore
    @gt4("users/me/profile")
    Object getUser(kc4<? super cs4<User>> kc4);

    @DexIgnore
    @gt4("/v2/diana-watch-faces")
    Object getWatchFaces(@st4("serialNumber") String str, kc4<? super cs4<ApiResponse<WatchFace>>> kc4);

    @DexIgnore
    @gt4("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@st4("metadata.locale") String str, kc4<? super cs4<ApiResponse<WatchLocalization>>> kc4);

    @DexIgnore
    @gt4("weather-info")
    Object getWeather(@st4("lat") String str, @st4("lng") String str2, @st4("temperatureUnit") String str3, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @gt4("users/me/workout-sessions")
    Object getWorkoutSessions(@st4("startDate") String str, @st4("endDate") String str2, @st4("offset") int i, @st4("limit") int i2, kc4<? super cs4<ApiResponse<ServerWorkoutSession>>> kc4);

    @DexIgnore
    @nt4("users/me/activities")
    Object insertActivities(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<Activity>>> kc4);

    @DexIgnore
    @nt4("users/me/fitness-files")
    Object insertFitnessDataFiles(@bt4 ApiResponse<ServerFitnessData> apiResponse, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @nt4("users/me/goal-events")
    Object insertGoalTrackingDataList(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @ot4("users/me/installations")
    Object insertInstallation(@bt4 Installation installation, kc4<? super cs4<Installation>> kc4);

    @DexIgnore
    @nt4("users/me/sleep-sessions")
    Object insertSleepSessions(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @ot4("users/me/devices")
    Object linkDevice(@bt4 Device device, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @ot4("users/me/diana-presets")
    Object replaceDianaPresetList(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<DianaPreset>>> kc4);

    @DexIgnore
    @ot4("users/me/hybrid-presets")
    Object replaceHybridPresetList(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<HybridPreset>>> kc4);

    @DexIgnore
    @mt4("users/me/sleep-settings")
    Object setSleepSetting(@bt4 yz1 yz1, kc4<? super cs4<MFSleepSettings>> kc4);

    @DexIgnore
    @nt4("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @mt4("users/me/activity-settings")
    Object updateActivitySetting(@bt4 yz1 yz1, kc4<? super cs4<ActivitySettings>> kc4);

    @DexIgnore
    @mt4("users/me/devices/{deviceId}")
    Object updateDevice(@rt4("deviceId") String str, @bt4 Device device, kc4<? super cs4<Void>> kc4);

    @DexIgnore
    @mt4("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@rt4("id") String str, @bt4 yz1 yz1, kc4<? super cs4<yz1>> kc4);

    @DexIgnore
    @mt4("users/me/profile")
    Object updateUser(@bt4 yz1 yz1, kc4<? super cs4<User>> kc4);

    @DexIgnore
    @mt4("users/me/alarms")
    Object upsertAlarms(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<Alarm>>> kc4);

    @DexIgnore
    @mt4("users/me/diana-presets")
    Object upsertDianaPresetList(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<DianaPreset>>> kc4);

    @DexIgnore
    @mt4("users/me/goal-settings")
    Object upsertGoalSetting(@bt4 yz1 yz1, kc4<? super cs4<GoalSetting>> kc4);

    @DexIgnore
    @mt4("users/me/hybrid-presets")
    Object upsertHybridPresetList(@bt4 yz1 yz1, kc4<? super cs4<ApiResponse<HybridPreset>>> kc4);
}
