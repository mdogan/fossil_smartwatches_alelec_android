package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kk2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$updateActivitySettings$Anon1 extends NetworkBoundResource<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$updateActivitySettings$Anon1(SummariesRepository summariesRepository, ActivitySettings activitySettings) {
        this.this$Anon0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    public Object createCall(kc4<? super cs4<ActivitySettings>> kc4) {
        sz1 sz1 = new sz1();
        sz1.b(new kk2());
        JsonElement b = sz1.a().b((Object) this.$activitySettings);
        wd4.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
        yz1 d = b.d();
        ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
        wd4.a((Object) d, "jsonObject");
        return access$getMApiServiceV2$p.updateActivitySetting(d, kc4);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "updateActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        wd4.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "updateActivitySettings - saveCallResult -- item=" + activitySettings);
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1(this, activitySettings, (kc4) null), 3, (Object) null);
    }
}
