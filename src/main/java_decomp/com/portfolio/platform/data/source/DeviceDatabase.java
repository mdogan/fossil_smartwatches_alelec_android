package com.portfolio.platform.data.source;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceDatabase extends RoomDatabase {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ zf MIGRATION_FROM_1_TO_2; // = new DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ String TAG; // = "DeviceDatabase";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final zf getMIGRATION_FROM_1_TO_2() {
            return DeviceDatabase.MIGRATION_FROM_1_TO_2;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public abstract DeviceDao deviceDao();

    @DexIgnore
    public abstract SkuDao skuDao();
}
