package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppLastSettingDao_Impl implements WatchAppLastSettingDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfWatchAppLastSetting;
    @DexIgnore
    public /* final */ xf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteWatchAppLastSettingById;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<WatchAppLastSetting> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppLastSetting`(`watchAppId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, WatchAppLastSetting watchAppLastSetting) {
            if (watchAppLastSetting.getWatchAppId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, watchAppLastSetting.getWatchAppId());
            }
            if (watchAppLastSetting.getUpdatedAt() == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, watchAppLastSetting.getUpdatedAt());
            }
            if (watchAppLastSetting.getSetting() == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, watchAppLastSetting.getSetting());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting WHERE watchAppId=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends xf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WatchAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon4(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchAppLastSetting> call() throws Exception {
            Cursor a = cg.a(WatchAppLastSettingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "watchAppId");
                int b2 = bg.b(a, "updatedAt");
                int b3 = bg.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppLastSettingDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfWatchAppLastSetting = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteWatchAppLastSettingById = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteWatchAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteWatchAppLastSettingById.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    public List<WatchAppLastSetting> getAllWatchAppLastSetting() {
        vf b = vf.b("SELECT * FROM watchAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "watchAppId");
            int b3 = bg.b(a, "updatedAt");
            int b4 = bg.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchAppLastSetting"}, false, new Anon4(vf.b("SELECT * FROM watchAppLastSetting", 0)));
    }

    @DexIgnore
    public WatchAppLastSetting getWatchAppLastSetting(String str) {
        vf b = vf.b("SELECT * FROM watchAppLastSetting WHERE watchAppId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new WatchAppLastSetting(a.getString(bg.b(a, "watchAppId")), a.getString(bg.b(a, "updatedAt")), a.getString(bg.b(a, MicroAppSetting.SETTING))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppLastSetting.insert(watchAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
