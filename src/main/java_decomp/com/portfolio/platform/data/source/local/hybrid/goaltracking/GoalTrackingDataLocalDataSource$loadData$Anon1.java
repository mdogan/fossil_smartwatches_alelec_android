package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.blesdk.obfuscated.bj4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1", f = "GoalTrackingDataLocalDataSource.kt", l = {94, 97, 102}, m = "invokeSuspend")
public final class GoalTrackingDataLocalDataSource$loadData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon1", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = goalTrackingDataLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, kc4);
            anon1.p$ = (lh4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                this.this$Anon0.$helperCallback.a();
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon2", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ro2 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, ro2 ro2, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = goalTrackingDataLocalDataSource$loadData$Anon1;
            this.$data = ro2;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$data, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                if (((qo2) this.$data).d() != null) {
                    this.this$Anon0.$helperCallback.a(((qo2) this.$data).d());
                } else if (((qo2) this.$data).c() != null) {
                    ServerError c = ((qo2) this.$data).c();
                    PagingRequestHelper.b.a aVar = this.this$Anon0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDataLocalDataSource$loadData$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, int i, PagingRequestHelper.b.a aVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = goalTrackingDataLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1 = new GoalTrackingDataLocalDataSource$loadData$Anon1(this.this$Anon0, this.$offset, this.$helperCallback, kc4);
        goalTrackingDataLocalDataSource$loadData$Anon1.p$ = (lh4) obj;
        return goalTrackingDataLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDataLocalDataSource$loadData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingDataLocalDataSource.Companion.getTAG();
            local.d(tag, "loadData currentDate=" + this.this$Anon0.mCurrentDate + ", offset=" + this.$offset);
            GoalTrackingRepository access$getMGoalTrackingRepository$p = this.this$Anon0.mGoalTrackingRepository;
            Date access$getMCurrentDate$p = this.this$Anon0.mCurrentDate;
            Date access$getMCurrentDate$p2 = this.this$Anon0.mCurrentDate;
            int i2 = this.$offset;
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = GoalTrackingRepository.loadGoalTrackingDataList$default(access$getMGoalTrackingRepository$p, access$getMCurrentDate$p, access$getMCurrentDate$p2, i2, 0, this, 8, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else if (i == 2 || i == 3) {
            ro2 ro2 = (ro2) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro22 = (ro2) obj;
        if (ro22 instanceof so2) {
            GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$Anon0;
            goalTrackingDataLocalDataSource.mOffset = goalTrackingDataLocalDataSource.mOffset + 100;
            bj4 c = zh4.c();
            Anon1 anon1 = new Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = ro22;
            this.label = 2;
            if (kg4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (ro22 instanceof qo2) {
            bj4 c2 = zh4.c();
            Anon2 anon2 = new Anon2(this, ro22, (kc4) null);
            this.L$Anon0 = lh4;
            this.L$Anon1 = ro22;
            this.label = 3;
            if (kg4.a(c2, anon2, this) == a) {
                return a;
            }
        }
        return cb4.a;
    }
}
