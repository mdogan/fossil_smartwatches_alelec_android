package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, kc4 kc4) {
        return new so2(pc4.a(200), false, 2, (rd4) null);
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, kc4 kc4) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, kc4<? super ro2<Boolean>> kc4) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, kc4);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, kc4<? super ro2<Boolean>> kc4) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, kc4);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, kc4<? super Integer> kc4) {
        return pc4.a(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, kc4<? super ro2<MFUser>> kc4) {
        return loadUserInfo$suspendImpl(this, mFUser, kc4);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, kc4<? super ro2<Auth>> kc4) {
        return loginEmail$suspendImpl(this, str, str2, kc4);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, kc4<? super ro2<Auth>> kc4) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, kc4);
    }

    @DexIgnore
    public Object logoutUser(kc4<? super Integer> kc4) {
        return pc4.a(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, kc4<? super ro2<Void>> kc4) {
        return requestEmailOtp$suspendImpl(this, str, kc4);
    }

    @DexIgnore
    public Object resetPassword(String str, kc4<? super ro2<Integer>> kc4) {
        return resetPassword$suspendImpl(this, str, kc4);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, kc4<? super ro2<Auth>> kc4) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, kc4);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, kc4<? super ro2<Auth>> kc4) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, kc4);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, kc4<? super ro2<MFUser>> kc4);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, kc4<? super ro2<Void>> kc4) {
        return verifyEmailOtp$suspendImpl(this, str, str2, kc4);
    }
}
