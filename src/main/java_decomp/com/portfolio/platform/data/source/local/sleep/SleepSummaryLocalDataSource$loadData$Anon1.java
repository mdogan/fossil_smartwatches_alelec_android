package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1", f = "SleepSummaryLocalDataSource.kt", l = {179, 179}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.RequestType $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, Date date, Date date2, PagingRequestHelper.RequestType requestType, PagingRequestHelper.b.a aVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = requestType;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1(this.this$Anon0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, kc4);
        sleepSummaryLocalDataSource$loadData$Anon1.p$ = (lh4) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource;
        Object obj2;
        PagingRequestHelper.RequestType requestType;
        ro2 ro2;
        sh4 sh4;
        List<FitnessDataWrapper> list;
        Pair<Date, Date> pair;
        lh4 lh4;
        sh4 sh42;
        Object obj3;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SleepSummaryLocalDataSource.TAG, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            list = this.this$Anon0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            pair = FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
            if (pair != null) {
                lh4 lh43 = lh42;
                sh4 a2 = mg4.a(lh43, (CoroutineContext) null, (CoroutineStart) null, new SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1(this, pair, (kc4) null), 3, (Object) null);
                sh42 = mg4.a(lh43, (CoroutineContext) null, (CoroutineStart) null, new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(this, pair, (kc4) null), 3, (Object) null);
                sleepSummaryLocalDataSource = this.this$Anon0;
                PagingRequestHelper.RequestType requestType2 = this.$requestType;
                this.L$Anon0 = lh42;
                this.L$Anon1 = list;
                this.L$Anon2 = pair;
                this.L$Anon3 = a2;
                this.L$Anon4 = sh42;
                this.L$Anon5 = sleepSummaryLocalDataSource;
                this.L$Anon6 = requestType2;
                this.label = 1;
                obj3 = a2.a(this);
                if (obj3 == a) {
                    return a;
                }
                sh4 sh43 = a2;
                lh4 = lh42;
                requestType = requestType2;
                sh4 = sh43;
            } else {
                this.$helperCallback.a();
                return cb4.a;
            }
        } else if (i == 1) {
            requestType = (PagingRequestHelper.RequestType) this.L$Anon6;
            sh42 = (sh4) this.L$Anon4;
            pair = (Pair) this.L$Anon2;
            list = (List) this.L$Anon1;
            za4.a(obj);
            sh4 = (sh4) this.L$Anon3;
            lh4 = (lh4) this.L$Anon0;
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$Anon5;
            obj3 = obj;
        } else if (i == 2) {
            ro2 = (ro2) this.L$Anon7;
            requestType = (PagingRequestHelper.RequestType) this.L$Anon6;
            sh4 sh44 = (sh4) this.L$Anon4;
            sh4 sh45 = (sh4) this.L$Anon3;
            Pair pair2 = (Pair) this.L$Anon2;
            List list2 = (List) this.L$Anon1;
            lh4 lh44 = (lh4) this.L$Anon0;
            za4.a(obj);
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$Anon5;
            obj2 = obj;
            sleepSummaryLocalDataSource.combineData(requestType, ro2, (ro2) obj2, this.$helperCallback);
            return cb4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro22 = (ro2) obj3;
        this.L$Anon0 = lh4;
        this.L$Anon1 = list;
        this.L$Anon2 = pair;
        this.L$Anon3 = sh4;
        this.L$Anon4 = sh42;
        this.L$Anon5 = sleepSummaryLocalDataSource;
        this.L$Anon6 = requestType;
        this.L$Anon7 = ro22;
        this.label = 2;
        obj2 = sh42.a(this);
        if (obj2 == a) {
            return a;
        }
        ro2 = ro22;
        sleepSummaryLocalDataSource.combineData(requestType, ro2, (ro2) obj2, this.$helperCallback);
        return cb4.a;
    }
}
