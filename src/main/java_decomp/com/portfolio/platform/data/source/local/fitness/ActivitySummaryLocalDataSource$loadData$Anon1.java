package com.portfolio.platform.data.source.local.fitness;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource$loadData$Anon1", f = "ActivitySummaryLocalDataSource.kt", l = {185}, m = "invokeSuspend")
public final class ActivitySummaryLocalDataSource$loadData$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.RequestType $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitySummaryLocalDataSource$loadData$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, Date date, Date date2, PagingRequestHelper.RequestType requestType, PagingRequestHelper.b.a aVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = activitySummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = requestType;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ActivitySummaryLocalDataSource$loadData$Anon1 activitySummaryLocalDataSource$loadData$Anon1 = new ActivitySummaryLocalDataSource$loadData$Anon1(this.this$Anon0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, kc4);
        activitySummaryLocalDataSource$loadData$Anon1.p$ = (lh4) obj;
        return activitySummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivitySummaryLocalDataSource$loadData$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            List<FitnessDataWrapper> fitnessData = this.this$Anon0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            Pair<Date, Date> calculateRangeDownload = FitnessDataWrapperKt.calculateRangeDownload(fitnessData, this.$startDate, this.$endDate);
            if (calculateRangeDownload != null) {
                this.L$Anon0 = lh4;
                this.L$Anon1 = fitnessData;
                this.L$Anon2 = calculateRangeDownload;
                this.label = 1;
                obj = this.this$Anon0.mSummariesRepository.loadSummaries(calculateRangeDownload.getFirst(), calculateRangeDownload.getSecond(), this);
                if (obj == a) {
                    return a;
                }
            } else {
                this.$helperCallback.a();
                return cb4.a;
            }
        } else if (i == 1) {
            Pair pair = (Pair) this.L$Anon2;
            List list = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            if (this.$requestType == PagingRequestHelper.RequestType.AFTER && (!this.this$Anon0.mRequestAfterQueue.isEmpty())) {
                this.this$Anon0.mRequestAfterQueue.remove(0);
            }
            this.$helperCallback.a();
        } else if (ro2 instanceof qo2) {
            qo2 qo2 = (qo2) ro2;
            if (qo2.d() != null) {
                this.$helperCallback.a(qo2.d());
            } else if (qo2.c() != null) {
                ServerError c = qo2.c();
                PagingRequestHelper.b.a aVar = this.$helperCallback;
                String userMessage = c.getUserMessage();
                if (userMessage == null) {
                    userMessage = c.getMessage();
                }
                if (userMessage == null) {
                    userMessage = "";
                }
                aVar.a(new Throwable(userMessage));
            }
        }
        return cb4.a;
    }
}
