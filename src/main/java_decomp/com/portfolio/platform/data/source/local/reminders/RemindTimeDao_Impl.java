package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemindTimeDao_Impl implements RemindTimeDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfRemindTimeModel;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDelete;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<RemindTimeModel> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `remindTimeModel`(`remindTimeName`,`minutes`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, RemindTimeModel remindTimeModel) {
            if (remindTimeModel.getRemindTimeName() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, remindTimeModel.getRemindTimeName());
            }
            lgVar.b(2, (long) remindTimeModel.getMinutes());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM remindTimeModel";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public RemindTimeModel call() throws Exception {
            Cursor a = cg.a(RemindTimeDao_Impl.this.__db, this.val$_statement, false);
            try {
                return a.moveToFirst() ? new RemindTimeModel(a.getString(bg.b(a, "remindTimeName")), a.getInt(bg.b(a, "minutes"))) : null;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public RemindTimeDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfRemindTimeModel = new Anon1(roomDatabase);
        this.__preparedStmtOfDelete = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<RemindTimeModel> getRemindTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"remindTimeModel"}, false, new Anon3(vf.b("SELECT * FROM remindTimeModel", 0)));
    }

    @DexIgnore
    public RemindTimeModel getRemindTimeModel() {
        vf b = vf.b("SELECT * FROM remindTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new RemindTimeModel(a.getString(bg.b(a, "remindTimeName")), a.getInt(bg.b(a, "minutes"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertRemindTimeModel(RemindTimeModel remindTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfRemindTimeModel.insert(remindTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
