package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryDataSourceFactory extends md.b<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ i42 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.a mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, i42 i42, PagingRequestHelper.a aVar, Calendar calendar) {
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(goalTrackingDao, "mGoalTrackingDao");
        wd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wd4.b(date, "mCreatedDate");
        wd4.b(i42, "mAppExecutors");
        wd4.b(aVar, "mListener");
        wd4.b(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = i42;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public md<Date, GoalTrackingSummary> create() {
        this.localDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDao, this.mGoalTrackingDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource != null) {
            return goalTrackingSummaryLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
