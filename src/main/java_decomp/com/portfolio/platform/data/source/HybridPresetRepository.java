package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPresetRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HybridPresetDao mHybridPresetDao;
    @DexIgnore
    public /* final */ HybridPresetRemoteDataSource mHybridPresetRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return HybridPresetRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = HybridPresetRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "HybridPresetRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HybridPresetRepository(HybridPresetDao hybridPresetDao, HybridPresetRemoteDataSource hybridPresetRemoteDataSource) {
        wd4.b(hybridPresetDao, "mHybridPresetDao");
        wd4.b(hybridPresetRemoteDataSource, "mHybridPresetRemoteDataSource");
        this.mHybridPresetDao = hybridPresetDao;
        this.mHybridPresetRemoteDataSource = hybridPresetRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mHybridPresetDao.clearAllPresetTable();
        this.mHybridPresetDao.clearAllRecommendPresetTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deletePresetById(String str, kc4<? super cb4> kc4) {
        HybridPresetRepository$deletePresetById$Anon1 hybridPresetRepository$deletePresetById$Anon1;
        int i;
        HybridPreset hybridPreset;
        Object obj;
        HybridPresetRepository hybridPresetRepository;
        ro2 ro2;
        if (kc4 instanceof HybridPresetRepository$deletePresetById$Anon1) {
            hybridPresetRepository$deletePresetById$Anon1 = (HybridPresetRepository$deletePresetById$Anon1) kc4;
            int i2 = hybridPresetRepository$deletePresetById$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRepository$deletePresetById$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = hybridPresetRepository$deletePresetById$Anon1.result;
                Object a = oc4.a();
                i = hybridPresetRepository$deletePresetById$Anon1.label;
                Integer num = null;
                if (i != 0) {
                    za4.a(obj2);
                    hybridPreset = this.mHybridPresetDao.getPresetById(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("deletePresetById ");
                    sb.append(hybridPreset != null ? hybridPreset.getName() : null);
                    sb.append(" pinType ");
                    sb.append(hybridPreset != null ? pc4.a(hybridPreset.getPinType()) : null);
                    local.d(str2, sb.toString());
                    if (hybridPreset != null) {
                        this.mHybridPresetDao.deletePreset(hybridPreset.getId());
                        if (hybridPreset.getPinType() != 1) {
                            HybridPresetRemoteDataSource hybridPresetRemoteDataSource = this.mHybridPresetRemoteDataSource;
                            hybridPresetRepository$deletePresetById$Anon1.L$Anon0 = this;
                            hybridPresetRepository$deletePresetById$Anon1.L$Anon1 = str;
                            hybridPresetRepository$deletePresetById$Anon1.L$Anon2 = hybridPreset;
                            hybridPresetRepository$deletePresetById$Anon1.L$Anon3 = hybridPreset;
                            hybridPresetRepository$deletePresetById$Anon1.label = 1;
                            obj = hybridPresetRemoteDataSource.deletePreset(hybridPreset, hybridPresetRepository$deletePresetById$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            hybridPresetRepository = this;
                        }
                    }
                    return cb4.a;
                } else if (i == 1) {
                    HybridPreset hybridPreset2 = (HybridPreset) hybridPresetRepository$deletePresetById$Anon1.L$Anon3;
                    String str3 = (String) hybridPresetRepository$deletePresetById$Anon1.L$Anon1;
                    hybridPresetRepository = (HybridPresetRepository) hybridPresetRepository$deletePresetById$Anon1.L$Anon0;
                    za4.a(obj2);
                    Object obj3 = obj2;
                    hybridPreset = (HybridPreset) hybridPresetRepository$deletePresetById$Anon1.L$Anon2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "deletePreset success");
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("deletePreset fail!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverCode ");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(str4, sb2.toString());
                    hybridPreset.setPinType(3);
                    hybridPresetRepository.mHybridPresetDao.upsertPreset(hybridPreset);
                }
                return cb4.a;
            }
        }
        hybridPresetRepository$deletePresetById$Anon1 = new HybridPresetRepository$deletePresetById$Anon1(this, kc4);
        Object obj22 = hybridPresetRepository$deletePresetById$Anon1.result;
        Object a2 = oc4.a();
        i = hybridPresetRepository$deletePresetById$Anon1.label;
        Integer num2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadPresetList(String str, kc4<? super ro2<List<HybridPreset>>> kc4) {
        HybridPresetRepository$downloadPresetList$Anon1 hybridPresetRepository$downloadPresetList$Anon1;
        int i;
        HybridPresetRepository hybridPresetRepository;
        ro2 ro2;
        String str2;
        ro2 ro22;
        if (kc4 instanceof HybridPresetRepository$downloadPresetList$Anon1) {
            hybridPresetRepository$downloadPresetList$Anon1 = (HybridPresetRepository$downloadPresetList$Anon1) kc4;
            int i2 = hybridPresetRepository$downloadPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRepository$downloadPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRepository$downloadPresetList$Anon1.result;
                Object a = oc4.a();
                i = hybridPresetRepository$downloadPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "downloadPresetList serial " + str);
                    hybridPresetRepository$downloadPresetList$Anon1.L$Anon0 = this;
                    hybridPresetRepository$downloadPresetList$Anon1.L$Anon1 = str;
                    hybridPresetRepository$downloadPresetList$Anon1.label = 1;
                    obj = executePendingRequest(str, hybridPresetRepository$downloadPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    str2 = str;
                    hybridPresetRepository = this;
                } else if (i == 1) {
                    za4.a(obj);
                    HybridPresetRepository hybridPresetRepository2 = (HybridPresetRepository) hybridPresetRepository$downloadPresetList$Anon1.L$Anon0;
                    str2 = (String) hybridPresetRepository$downloadPresetList$Anon1.L$Anon1;
                    hybridPresetRepository = hybridPresetRepository2;
                } else if (i == 2) {
                    String str4 = (String) hybridPresetRepository$downloadPresetList$Anon1.L$Anon1;
                    hybridPresetRepository = (HybridPresetRepository) hybridPresetRepository$downloadPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                    ro2 = (ro2) obj;
                    Integer num = null;
                    if (!(ro2 instanceof so2)) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("downloadPresetList success isFromCache ");
                        so2 so2 = (so2) ro2;
                        sb.append(so2.b());
                        local2.d(str5, sb.toString());
                        if (!so2.b()) {
                            HybridPresetDao hybridPresetDao = hybridPresetRepository.mHybridPresetDao;
                            Object a2 = so2.a();
                            if (a2 != null) {
                                hybridPresetDao.upsertPresetList((List) a2);
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                        return new so2(so2.a(), so2.b());
                    } else if (ro2 instanceof qo2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("downloadPresetList fail!! ");
                        qo2 qo2 = (qo2) ro2;
                        sb2.append(qo2.a());
                        sb2.append(" serverCode ");
                        ServerError c = qo2.c();
                        if (c != null) {
                            num = c.getCode();
                        }
                        sb2.append(num);
                        local3.d(str6, sb2.toString());
                        return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro22 = (ro2) obj;
                if (!(ro22 instanceof so2)) {
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = hybridPresetRepository.mHybridPresetRemoteDataSource;
                    hybridPresetRepository$downloadPresetList$Anon1.L$Anon0 = hybridPresetRepository;
                    hybridPresetRepository$downloadPresetList$Anon1.L$Anon1 = str2;
                    hybridPresetRepository$downloadPresetList$Anon1.label = 2;
                    obj = hybridPresetRemoteDataSource.downloadHybridPresetList(str2, hybridPresetRepository$downloadPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    ro2 = (ro2) obj;
                    Integer num2 = null;
                    if (!(ro2 instanceof so2)) {
                    }
                } else if (ro22 instanceof qo2) {
                    return new qo2(600001, (ServerError) null, (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRepository$downloadPresetList$Anon1 = new HybridPresetRepository$downloadPresetList$Anon1(this, kc4);
        Object obj2 = hybridPresetRepository$downloadPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = hybridPresetRepository$downloadPresetList$Anon1.label;
        if (i != 0) {
        }
        ro22 = (ro2) obj2;
        if (!(ro22 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadRecommendPresetList(String str, kc4<? super cb4> kc4) {
        HybridPresetRepository$downloadRecommendPresetList$Anon1 hybridPresetRepository$downloadRecommendPresetList$Anon1;
        int i;
        HybridPresetRepository hybridPresetRepository;
        ro2 ro2;
        if (kc4 instanceof HybridPresetRepository$downloadRecommendPresetList$Anon1) {
            hybridPresetRepository$downloadRecommendPresetList$Anon1 = (HybridPresetRepository$downloadRecommendPresetList$Anon1) kc4;
            int i2 = hybridPresetRepository$downloadRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRepository$downloadRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRepository$downloadRecommendPresetList$Anon1.result;
                Object a = oc4.a();
                i = hybridPresetRepository$downloadRecommendPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "downloadRecommendPresetList - serial=" + str);
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = this.mHybridPresetRemoteDataSource;
                    hybridPresetRepository$downloadRecommendPresetList$Anon1.L$Anon0 = this;
                    hybridPresetRepository$downloadRecommendPresetList$Anon1.L$Anon1 = str;
                    hybridPresetRepository$downloadRecommendPresetList$Anon1.label = 1;
                    obj = hybridPresetRemoteDataSource.downloadHybridRecommendPresetList(str, hybridPresetRepository$downloadRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    hybridPresetRepository = this;
                } else if (i == 1) {
                    str = (String) hybridPresetRepository$downloadRecommendPresetList$Anon1.L$Anon1;
                    hybridPresetRepository = (HybridPresetRepository) hybridPresetRepository$downloadRecommendPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadRecommendPresetList - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(str3, sb.toString());
                    if (!so2.b()) {
                        Object a2 = so2.a();
                        if (a2 != null) {
                            hybridPresetRepository.mHybridPresetDao.upsertRecommendPresetList((ArrayList) a2);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadRecommendPresetList - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str4, sb2.toString());
                }
                return cb4.a;
            }
        }
        hybridPresetRepository$downloadRecommendPresetList$Anon1 = new HybridPresetRepository$downloadRecommendPresetList$Anon1(this, kc4);
        Object obj2 = hybridPresetRepository$downloadRecommendPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = hybridPresetRepository$downloadRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    /* JADX WARNING: type inference failed for: r11v12, types: [com.fossil.blesdk.obfuscated.so2] */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0105, code lost:
        return r11;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c5 A[SYNTHETIC, Splitter:B:29:0x00c5] */
    public final synchronized Object executePendingRequest(String str, kc4<? super ro2<List<HybridPreset>>> kc4) {
        HybridPresetRepository$executePendingRequest$Anon1 hybridPresetRepository$executePendingRequest$Anon1;
        int i;
        HybridPresetRepository hybridPresetRepository;
        ro2 ro2;
        qo2 qo2;
        if (kc4 instanceof HybridPresetRepository$executePendingRequest$Anon1) {
            hybridPresetRepository$executePendingRequest$Anon1 = (HybridPresetRepository$executePendingRequest$Anon1) kc4;
            if ((hybridPresetRepository$executePendingRequest$Anon1.label & Integer.MIN_VALUE) != 0) {
                hybridPresetRepository$executePendingRequest$Anon1.label -= Integer.MIN_VALUE;
                Object obj = hybridPresetRepository$executePendingRequest$Anon1.result;
                Object a = oc4.a();
                i = hybridPresetRepository$executePendingRequest$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    List<HybridPreset> allPendingPreset = this.mHybridPresetDao.getAllPendingPreset(str);
                    FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest pendingPreset=" + allPendingPreset);
                    if (!allPendingPreset.isEmpty()) {
                        List<HybridPreset> allPreset = this.mHybridPresetDao.getAllPreset(str);
                        HybridPresetRemoteDataSource hybridPresetRemoteDataSource = this.mHybridPresetRemoteDataSource;
                        hybridPresetRepository$executePendingRequest$Anon1.L$Anon0 = this;
                        hybridPresetRepository$executePendingRequest$Anon1.L$Anon1 = str;
                        hybridPresetRepository$executePendingRequest$Anon1.L$Anon2 = allPendingPreset;
                        hybridPresetRepository$executePendingRequest$Anon1.L$Anon3 = allPreset;
                        hybridPresetRepository$executePendingRequest$Anon1.label = 1;
                        obj = hybridPresetRemoteDataSource.replaceHybridPresetList(allPreset, hybridPresetRepository$executePendingRequest$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        hybridPresetRepository = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest success no pending");
                        return new so2(new ArrayList(), false, 2, (rd4) null);
                    }
                } else if (i == 1) {
                    List list = (List) hybridPresetRepository$executePendingRequest$Anon1.L$Anon3;
                    List list2 = (List) hybridPresetRepository$executePendingRequest$Anon1.L$Anon2;
                    String str2 = (String) hybridPresetRepository$executePendingRequest$Anon1.L$Anon1;
                    hybridPresetRepository = (HybridPresetRepository) hybridPresetRepository$executePendingRequest$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    HybridPresetDao hybridPresetDao = hybridPresetRepository.mHybridPresetDao;
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        hybridPresetDao.upsertPresetList((List) a2);
                        hybridPresetRepository.mHybridPresetDao.removeAllDeletePinTypePreset();
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest success sync with server");
                        qo2 = new so2(((so2) ro2).a(), false, 2, (rd4) null);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (ro2 instanceof qo2) {
                    FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest fail to sync with server " + ((qo2) ro2).a());
                    qo2 = new qo2(((qo2) ro2).a(), ((qo2) ro2).c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRepository$executePendingRequest$Anon1 = new HybridPresetRepository$executePendingRequest$Anon1(this, kc4);
        Object obj2 = hybridPresetRepository$executePendingRequest$Anon1.result;
        Object a3 = oc4.a();
        i = hybridPresetRepository$executePendingRequest$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final HybridPreset getActivePresetBySerial(String str) {
        wd4.b(str, "serial");
        return this.mHybridPresetDao.getActivePresetBySerial(str);
    }

    @DexIgnore
    public final List<HybridRecommendPreset> getHybridRecommendPresetList(String str) {
        wd4.b(str, "serial");
        return this.mHybridPresetDao.getRecommendPresetList(str);
    }

    @DexIgnore
    public final HybridPreset getPresetById(String str) {
        wd4.b(str, "id");
        return this.mHybridPresetDao.getPresetById(str);
    }

    @DexIgnore
    public final ArrayList<HybridPreset> getPresetList(String str) {
        wd4.b(str, "serial");
        List<HybridPreset> allPreset = this.mHybridPresetDao.getAllPreset(str);
        if (allPreset != null) {
            return (ArrayList) allPreset;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>");
    }

    @DexIgnore
    public final LiveData<List<HybridPreset>> getPresetListAsLiveData(String str) {
        wd4.b(str, "serial");
        return this.mHybridPresetDao.getAllPresetAsLiveData(str);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object upsertHybridPreset(HybridPreset hybridPreset, kc4<? super cb4> kc4) {
        HybridPreset hybridPreset2;
        Object obj;
        FLogger.INSTANCE.getLocal().d(TAG, "upsertHybridPreset " + hybridPreset + " pinType " + hybridPreset.getPinType());
        hybridPreset.setUpdatedAt(sk2.t(new Date(System.currentTimeMillis())));
        List<HybridPreset> allPreset = this.mHybridPresetDao.getAllPreset(hybridPreset.getSerialNumber());
        if (allPreset != null) {
            ArrayList arrayList = (ArrayList) allPreset;
            if (!(!arrayList.isEmpty())) {
                return cb4.a;
            }
            Iterator it = arrayList.iterator();
            while (true) {
                hybridPreset2 = null;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (pc4.a(wd4.a((Object) ((HybridPreset) obj).getId(), (Object) hybridPreset.getId())).booleanValue()) {
                    break;
                }
            }
            HybridPreset hybridPreset3 = (HybridPreset) obj;
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                if (pc4.a(next.isActive()).booleanValue()) {
                    hybridPreset2 = next;
                    break;
                }
            }
            HybridPreset hybridPreset4 = hybridPreset2;
            if (hybridPreset3 == null || hybridPreset3.getPinType() == 1) {
                hybridPreset.setPinType(1);
            } else {
                hybridPreset.setPinType(2);
            }
            if (hybridPreset4 != null && (!wd4.a((Object) hybridPreset4.getId(), (Object) hybridPreset.getId())) && hybridPreset.isActive()) {
                hybridPreset4.setActive(false);
                if (hybridPreset4.getPinType() != 1) {
                    hybridPreset4.setPinType(2);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (pc4.a(!wd4.a((Object) ((HybridPreset) next2).getId(), (Object) hybridPreset.getId())).booleanValue()) {
                    arrayList2.add(next2);
                }
            }
            arrayList2.add(hybridPreset);
            return upsertHybridPresetList(arrayList2, kc4);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object upsertHybridPresetList(List<HybridPreset> list, kc4<? super cb4> kc4) {
        HybridPresetRepository$upsertHybridPresetList$Anon1 hybridPresetRepository$upsertHybridPresetList$Anon1;
        int i;
        HybridPresetRepository hybridPresetRepository;
        ro2 ro2;
        if (kc4 instanceof HybridPresetRepository$upsertHybridPresetList$Anon1) {
            hybridPresetRepository$upsertHybridPresetList$Anon1 = (HybridPresetRepository$upsertHybridPresetList$Anon1) kc4;
            int i2 = hybridPresetRepository$upsertHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRepository$upsertHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRepository$upsertHybridPresetList$Anon1.result;
                Object a = oc4.a();
                i = hybridPresetRepository$upsertHybridPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "upsertHybridPresetList");
                    this.mHybridPresetDao.upsertPresetList(list);
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = this.mHybridPresetRemoteDataSource;
                    hybridPresetRepository$upsertHybridPresetList$Anon1.L$Anon0 = this;
                    hybridPresetRepository$upsertHybridPresetList$Anon1.L$Anon1 = list;
                    hybridPresetRepository$upsertHybridPresetList$Anon1.label = 1;
                    obj = hybridPresetRemoteDataSource.upsertHybridPresetList(list, hybridPresetRepository$upsertHybridPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    hybridPresetRepository = this;
                } else if (i == 1) {
                    List list2 = (List) hybridPresetRepository$upsertHybridPresetList$Anon1.L$Anon1;
                    hybridPresetRepository = (HybridPresetRepository) hybridPresetRepository$upsertHybridPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("upsertHybridPresetList success ");
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 != null) {
                        sb.append((ArrayList) a2);
                        local.d(str, sb.toString());
                        HybridPresetDao hybridPresetDao = hybridPresetRepository.mHybridPresetDao;
                        Object a3 = so2.a();
                        if (a3 != null) {
                            hybridPresetDao.upsertPresetList((List) a3);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("upsertHybridPresetList fail!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverCode ");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(str2, sb2.toString());
                }
                return cb4.a;
            }
        }
        hybridPresetRepository$upsertHybridPresetList$Anon1 = new HybridPresetRepository$upsertHybridPresetList$Anon1(this, kc4);
        Object obj2 = hybridPresetRepository$upsertHybridPresetList$Anon1.result;
        Object a4 = oc4.a();
        i = hybridPresetRepository$upsertHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }
}
