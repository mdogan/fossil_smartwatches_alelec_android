package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface WatchFaceDao {
    @DexIgnore
    void deleteWatchFacesWithSerial(String str);

    @DexIgnore
    WatchFace getWatchFaceWithId(String str);

    @DexIgnore
    LiveData<List<WatchFace>> getWatchFacesLiveData(String str);

    @DexIgnore
    List<WatchFace> getWatchFacesWithSerial(String str);

    @DexIgnore
    void insertAllWatchFaces(List<WatchFace> list);
}
