package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.b72;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.d72;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.r72;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSampleDao_Impl extends HeartRateSampleDao {
    @DexIgnore
    public /* final */ b72 __dateShortStringConverter; // = new b72();
    @DexIgnore
    public /* final */ d72 __dateTimeISOStringConverter; // = new d72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfHeartRateSample;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDeleteAllHeartRateSamples;
    @DexIgnore
    public /* final */ r72 __restingConverter; // = new r72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<HeartRateSample> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `heart_rate_sample`(`id`,`average`,`date`,`createdAt`,`updatedAt`,`endTime`,`startTime`,`timezoneOffset`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, HeartRateSample heartRateSample) {
            if (heartRateSample.getId() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, heartRateSample.getId());
            }
            lgVar.a(2, (double) heartRateSample.getAverage());
            String a = HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(heartRateSample.getDate());
            if (a == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, a);
            }
            lgVar.b(4, heartRateSample.getCreatedAt());
            lgVar.b(5, heartRateSample.getUpdatedAt());
            String a2 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getEndTime());
            if (a2 == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, a2);
            }
            String a3 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getStartTime());
            if (a3 == null) {
                lgVar.a(7);
            } else {
                lgVar.a(7, a3);
            }
            lgVar.b(8, (long) heartRateSample.getTimezoneOffsetInSecond());
            lgVar.b(9, (long) heartRateSample.getMin());
            lgVar.b(10, (long) heartRateSample.getMax());
            lgVar.b(11, (long) heartRateSample.getMinuteCount());
            String a4 = HeartRateSampleDao_Impl.this.__restingConverter.a(heartRateSample.getResting());
            if (a4 == null) {
                lgVar.a(12);
            } else {
                lgVar.a(12, a4);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM heart_rate_sample";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<HeartRateSample>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<HeartRateSample> call() throws Exception {
            Cursor a = cg.a(HeartRateSampleDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "id");
                int b2 = bg.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
                int b3 = bg.b(a, "date");
                int b4 = bg.b(a, "createdAt");
                int b5 = bg.b(a, "updatedAt");
                int b6 = bg.b(a, SampleRaw.COLUMN_END_TIME);
                int b7 = bg.b(a, SampleRaw.COLUMN_START_TIME);
                int b8 = bg.b(a, "timezoneOffset");
                int b9 = bg.b(a, "min");
                int b10 = bg.b(a, "max");
                int b11 = bg.b(a, "minuteCount");
                int b12 = bg.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b;
                    arrayList.add(new HeartRateSample(a.getString(b), a.getFloat(b2), HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getLong(b4), a.getLong(b5), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6)), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7)), a.getInt(b8), a.getInt(b9), a.getInt(b10), a.getInt(b11), HeartRateSampleDao_Impl.this.__restingConverter.a(a.getString(b12))));
                    b = i;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HeartRateSampleDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfHeartRateSample = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllHeartRateSamples = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllHeartRateSamples() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDeleteAllHeartRateSamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSamples.release(acquire);
        }
    }

    @DexIgnore
    public HeartRateSample getHeartRateSample(String str) {
        HeartRateSample heartRateSample;
        String str2 = str;
        vf b = vf.b("SELECT * FROM heart_rate_sample WHERE id = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "id");
            int b3 = bg.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
            int b4 = bg.b(a, "date");
            int b5 = bg.b(a, "createdAt");
            int b6 = bg.b(a, "updatedAt");
            int b7 = bg.b(a, SampleRaw.COLUMN_END_TIME);
            int b8 = bg.b(a, SampleRaw.COLUMN_START_TIME);
            int b9 = bg.b(a, "timezoneOffset");
            int b10 = bg.b(a, "min");
            int b11 = bg.b(a, "max");
            int b12 = bg.b(a, "minuteCount");
            int b13 = bg.b(a, "resting");
            if (a.moveToFirst()) {
                heartRateSample = new HeartRateSample(a.getString(b2), a.getFloat(b3), this.__dateShortStringConverter.a(a.getString(b4)), a.getLong(b5), a.getLong(b6), this.__dateTimeISOStringConverter.a(a.getString(b7)), this.__dateTimeISOStringConverter.a(a.getString(b8)), a.getInt(b9), a.getInt(b10), a.getInt(b11), a.getInt(b12), this.__restingConverter.a(a.getString(b13)));
            } else {
                heartRateSample = null;
            }
            return heartRateSample;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2) {
        vf b = vf.b("SELECT * FROM heart_rate_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"heart_rate_sample"}, false, new Anon3(b));
    }

    @DexIgnore
    public void insertHeartRateSample(HeartRateSample heartRateSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(heartRateSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertHeartRateSampleList(List<HeartRateSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
