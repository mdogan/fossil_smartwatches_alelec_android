package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1", f = "MicroAppRemoteDataSource.kt", l = {40}, m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<MicroAppVariant>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $majorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $minorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, String str, String str2, String str3, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = microAppRemoteDataSource;
        this.$serial = str;
        this.$majorNumber = str2;
        this.$minorNumber = str3;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this.this$Anon0, this.$serial, this.$majorNumber, this.$minorNumber, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
            String str = this.$serial;
            String str2 = this.$majorNumber;
            String str3 = this.$minorNumber;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getAllMicroAppVariant(str, str2, str3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
