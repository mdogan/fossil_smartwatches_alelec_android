package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.od;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public List<WorkoutSessionDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ WorkoutDao mWorkoutDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return WorkoutSessionRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "WorkoutSessionRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionRepository(WorkoutDao workoutDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        wd4.b(workoutDao, "mWorkoutDao");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        wd4.b(apiServiceV2, "mApiService");
        this.mWorkoutDao = workoutDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchWorkoutSessions$default(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, kc4 kc4, int i3, Object obj) {
        return workoutSessionRepository.fetchWorkoutSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, kc4);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mWorkoutDao.deleteAllWorkoutSession();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0202  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchWorkoutSessions(Date date, Date date2, int i, int i2, kc4<? super ro2<ApiResponse<ServerWorkoutSession>>> kc4) {
        WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon1;
        int i3;
        int i4;
        Date date3;
        WorkoutSessionRepository workoutSessionRepository;
        Date date4;
        int i5;
        ro2 ro2;
        Date date5 = date;
        Date date6 = date2;
        kc4<? super ro2<ApiResponse<ServerWorkoutSession>>> kc42 = kc4;
        if (kc42 instanceof WorkoutSessionRepository$fetchWorkoutSessions$Anon1) {
            workoutSessionRepository$fetchWorkoutSessions$Anon1 = (WorkoutSessionRepository$fetchWorkoutSessions$Anon1) kc42;
            int i6 = workoutSessionRepository$fetchWorkoutSessions$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                workoutSessionRepository$fetchWorkoutSessions$Anon1.label = i6 - Integer.MIN_VALUE;
                WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon12 = workoutSessionRepository$fetchWorkoutSessions$Anon1;
                Object obj = workoutSessionRepository$fetchWorkoutSessions$Anon12.result;
                Object a = oc4.a();
                i3 = workoutSessionRepository$fetchWorkoutSessions$Anon12.label;
                if (i3 != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchWorkoutSessions - start=" + date5 + ", end=" + date6);
                    WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1 workoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1 = new WorkoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1(this, date, date2, i, i2, (kc4) null);
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon0 = this;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon1 = date5;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon2 = date6;
                    i5 = i;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon0 = i5;
                    int i7 = i2;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon1 = i7;
                    workoutSessionRepository$fetchWorkoutSessions$Anon12.label = 1;
                    Object a2 = ResponseKt.a(workoutSessionRepository$fetchWorkoutSessions$repoResponse$Anon1, workoutSessionRepository$fetchWorkoutSessions$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj = a2;
                    date4 = date6;
                    date3 = date5;
                    workoutSessionRepository = this;
                } else if (i3 == 1) {
                    int i8 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon1;
                    i5 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon0;
                    date4 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon2;
                    za4.a(obj);
                    i4 = i8;
                    date3 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon1;
                    workoutSessionRepository = (WorkoutSessionRepository) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon0;
                } else if (i3 == 2) {
                    ro2 ro22 = (ro2) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon3;
                    int i9 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon1;
                    int i10 = workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon0;
                    Date date7 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon2;
                    Date date8 = (Date) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon1;
                    WorkoutSessionRepository workoutSessionRepository2 = (WorkoutSessionRepository) workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon0;
                    za4.a(obj);
                    return (ro2) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    if (so2.a() == null) {
                        return ro2;
                    }
                    if (!so2.b()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("fetchWorkoutSessions - Success -- item.size=");
                        sb.append(((ApiResponse) so2.a()).get_items().size());
                        sb.append(", hasNext=");
                        Range range = ((ApiResponse) so2.a()).get_range();
                        sb.append(range != null ? pc4.a(range.isHasNext()) : null);
                        local2.d(str2, sb.toString());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local3.d(str3, "fetchWorkoutSessions - Success -- items=" + ((ApiResponse) so2.a()).get_items());
                        ArrayList arrayList = new ArrayList();
                        for (ServerWorkoutSession workoutSession : ((ApiResponse) so2.a()).get_items()) {
                            WorkoutSession workoutSession2 = workoutSession.toWorkoutSession();
                            if (workoutSession2 != null) {
                                arrayList.add(workoutSession2);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            workoutSessionRepository.mWorkoutDao.upsertListWorkoutSession(arrayList);
                        }
                        FLogger.INSTANCE.getLocal().d(TAG, "fetchWorkoutSessions - saveCallResult -- DONE!!!");
                    }
                    if (((ApiResponse) so2.a()).get_range() == null) {
                        return ro2;
                    }
                    Range range2 = ((ApiResponse) so2.a()).get_range();
                    if (range2 == null) {
                        wd4.a();
                        throw null;
                    } else if (!range2.isHasNext()) {
                        return ro2;
                    } else {
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon0 = workoutSessionRepository;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon1 = date3;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon2 = date4;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon0 = i5;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.I$Anon1 = i4;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.L$Anon3 = ro2;
                        workoutSessionRepository$fetchWorkoutSessions$Anon12.label = 2;
                        obj = workoutSessionRepository.fetchWorkoutSessions(date3, date4, i5 + i4, i4, workoutSessionRepository$fetchWorkoutSessions$Anon12);
                        if (obj == a) {
                            return a;
                        }
                        return (ro2) obj;
                    }
                } else {
                    String str4 = null;
                    if (ro2 instanceof qo2) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("fetchWorkoutSessions - Failure -- code=");
                        qo2 qo2 = (qo2) ro2;
                        sb2.append(qo2.a());
                        sb2.append(", message=");
                        ServerError c = qo2.c();
                        if (c != null) {
                            String message = c.getMessage();
                            if (message != null) {
                                str4 = message;
                                if (str4 == null) {
                                    str4 = "";
                                }
                                sb2.append(str4);
                                local4.d(str5, sb2.toString());
                            }
                        }
                        ServerError c2 = qo2.c();
                        if (c2 != null) {
                            str4 = c2.getUserMessage();
                        }
                        if (str4 == null) {
                        }
                        sb2.append(str4);
                        local4.d(str5, sb2.toString());
                    }
                    return ro2;
                }
            }
        }
        workoutSessionRepository$fetchWorkoutSessions$Anon1 = new WorkoutSessionRepository$fetchWorkoutSessions$Anon1(this, kc42);
        WorkoutSessionRepository$fetchWorkoutSessions$Anon1 workoutSessionRepository$fetchWorkoutSessions$Anon122 = workoutSessionRepository$fetchWorkoutSessions$Anon1;
        Object obj2 = workoutSessionRepository$fetchWorkoutSessions$Anon122.result;
        Object a3 = oc4.a();
        i3 = workoutSessionRepository$fetchWorkoutSessions$Anon122.label;
        if (i3 != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final LiveData<ps3<List<WorkoutSession>>> getWorkoutSessions(Date date, Date date2, boolean z) {
        wd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(date2, "end");
        Date n = sk2.n(date);
        Date i = sk2.i(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        wd4.a((Object) n, GoalPhase.COLUMN_START_DATE);
        wd4.a((Object) i, GoalPhase.COLUMN_END_DATE);
        LiveData<ps3<List<WorkoutSession>>> b = ic.b(fitnessDataDao.getFitnessDataLiveData(n, i), new WorkoutSessionRepository$getWorkoutSessions$Anon1(this, z, date2, n, i));
        wd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<WorkoutSession> getWorkoutSessionsPaging(Date date, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, i42 i42, PagingRequestHelper.a aVar) {
        wd4.b(date, "currentDate");
        wd4.b(workoutSessionRepository, "workoutSessionRepository");
        wd4.b(fitnessDataDao, "fitnessDataDao");
        wd4.b(workoutDao, "workoutDao");
        wd4.b(fitnessDatabase, "workoutDatabase");
        wd4.b(i42, "appExecutors");
        PagingRequestHelper.a aVar2 = aVar;
        wd4.b(aVar2, "listener");
        WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory = new WorkoutSessionDataSourceFactory(workoutSessionRepository, fitnessDataDao, workoutDao, fitnessDatabase, date, i42, aVar2);
        this.mSourceDataFactoryList.add(workoutSessionDataSourceFactory);
        rd.f.a aVar3 = new rd.f.a();
        aVar3.a(100);
        aVar3.a(false);
        aVar3.b(100);
        aVar3.c(5);
        rd.f a = aVar3.a();
        wd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new od(workoutSessionDataSourceFactory, a).a();
        wd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = ic.b(workoutSessionDataSourceFactory.getSourceLiveData(), WorkoutSessionRepository$getWorkoutSessionsPaging$Anon1.INSTANCE);
        wd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(workoutSessionDataSourceFactory), new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon3(workoutSessionDataSourceFactory));
    }

    @DexIgnore
    public final void insertFromDevice(List<WorkoutSession> list) {
        wd4.b(list, "workoutSessions");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: workoutSessions = " + list);
        this.mWorkoutDao.upsertListWorkoutSession(list);
        FLogger.INSTANCE.getLocal().d(TAG, "insertFromDevice: workoutSessions = DONE!!!");
    }

    @DexIgnore
    public final void removePagingListener() {
        for (WorkoutSessionDataSourceFactory localDataSource : this.mSourceDataFactoryList) {
            WorkoutSessionLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
    }
}
