package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import java.util.Set;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDataLocalDataSource extends nd<Long, GoalTrackingData> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public DateTime latestTrackedTime;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public /* final */ Date mCurrentDate;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = kl2.a(this.mHelper);
    @DexIgnore
    public /* final */ qf.c mObserver;
    @DexIgnore
    public int mOffset;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends qf.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = goalTrackingDataLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingDataLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = GoalTrackingDataLocalDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "GoalTrackingDataLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingDataLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, i42 i42, PagingRequestHelper.a aVar) {
        wd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        wd4.b(goalTrackingDao, "mGoalTrackingDao");
        wd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wd4.b(date, "mCurrentDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCurrentDate = date;
        this.listener = aVar;
        this.mHelper = new PagingRequestHelper(i42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingRaw", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<GoalTrackingData> getDataInDatabase(int i) {
        return this.mGoalTrackingDao.getGoalTrackingDataListInitInDate(this.mCurrentDate, i);
    }

    @DexIgnore
    private final ri4 loadData(PagingRequestHelper.RequestType requestType, PagingRequestHelper.b.a aVar, int i) {
        return mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDataLocalDataSource$loadData$Anon1(this, i, aVar, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ ri4 loadData$default(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, PagingRequestHelper.RequestType requestType, PagingRequestHelper.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return goalTrackingDataLocalDataSource.loadData(requestType, aVar, i);
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(nd.f<Long> fVar, nd.a<GoalTrackingData> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - currentDate = " + this.mCurrentDate);
        GoalTrackingDao goalTrackingDao = this.mGoalTrackingDao;
        Date date = this.mCurrentDate;
        DateTime dateTime = this.latestTrackedTime;
        if (dateTime != null) {
            Key key = fVar.a;
            wd4.a((Object) key, "params.key");
            List<GoalTrackingData> goalTrackingDataListAfterInDate = goalTrackingDao.getGoalTrackingDataListAfterInDate(date, dateTime, ((Number) key).longValue(), fVar.b);
            if (!goalTrackingDataListAfterInDate.isEmpty()) {
                this.latestTrackedTime = ((GoalTrackingData) wb4.f(goalTrackingDataListAfterInDate)).getTrackedAt();
            }
            aVar.a(goalTrackingDataListAfterInDate);
            this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new GoalTrackingDataLocalDataSource$loadAfter$Anon1(this));
            return;
        }
        wd4.d("latestTrackedTime");
        throw null;
    }

    @DexIgnore
    public void loadBefore(nd.f<Long> fVar, nd.a<GoalTrackingData> aVar) {
        wd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(nd.e<Long> eVar, nd.c<GoalTrackingData> cVar) {
        wd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        wd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - mCurrentDate = " + this.mCurrentDate + ' ');
        List<GoalTrackingData> dataInDatabase = getDataInDatabase(eVar.a);
        if (!dataInDatabase.isEmpty()) {
            this.latestTrackedTime = ((GoalTrackingData) wb4.f(dataInDatabase)).getTrackedAt();
        }
        cVar.a(dataInDatabase);
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new GoalTrackingDataLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        wd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(GoalTrackingData goalTrackingData) {
        wd4.b(goalTrackingData, "item");
        return Long.valueOf(goalTrackingData.getUpdatedAt());
    }
}
