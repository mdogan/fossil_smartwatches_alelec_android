package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1", f = "ServerSettingRepository.kt", l = {}, m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository$getServerSettingList$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1(ServerSettingRepository$getServerSettingList$Anon1 serverSettingRepository$getServerSettingList$Anon1, ServerSettingList serverSettingList, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = serverSettingRepository$getServerSettingList$Anon1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1 = new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1(this.this$Anon0, this.$serverSettingList, kc4);
        serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1.p$ = (lh4) obj;
        return serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                List<ServerSetting> serverSettings = serverSettingList.getServerSettings();
                Boolean a = serverSettings != null ? pc4.a(!serverSettings.isEmpty()) : null;
                if (a == null) {
                    wd4.a();
                    throw null;
                } else if (a.booleanValue()) {
                    this.this$Anon0.this$Anon0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
