package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "InAppNotificationRepository";
    @DexIgnore
    public /* final */ InAppNotificationDao mInAppNotificationDao;
    @DexIgnore
    public /* final */ List<InAppNotification> mNotificationList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public InAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        wd4.b(inAppNotificationDao, "mInAppNotificationDao");
        this.mInAppNotificationDao = inAppNotificationDao;
    }

    @DexIgnore
    public final Object delete(InAppNotification inAppNotification, kc4<? super cb4> kc4) {
        this.mInAppNotificationDao.delete(inAppNotification);
        return cb4.a;
    }

    @DexIgnore
    public final LiveData<List<InAppNotification>> getAllInAppNotifications() {
        return this.mInAppNotificationDao.getAllInAppNotification();
    }

    @DexIgnore
    public final void handleReceivingNotification(InAppNotification inAppNotification) {
        wd4.b(inAppNotification, "inAppNotification");
        this.mNotificationList.add(inAppNotification);
    }

    @DexIgnore
    public final Object insert(List<InAppNotification> list, kc4<? super cb4> kc4) {
        this.mInAppNotificationDao.insertListInAppNotification(list);
        return cb4.a;
    }

    @DexIgnore
    public final void removeNotificationAfterSending(InAppNotification inAppNotification) {
        wd4.b(inAppNotification, "inAppNotification");
        this.mNotificationList.remove(inAppNotification);
    }
}
