package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getAllWatchApp(String str, kc4<? super ro2<List<WatchApp>>> kc4) {
        WatchAppRemoteDataSource$getAllWatchApp$Anon1 watchAppRemoteDataSource$getAllWatchApp$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof WatchAppRemoteDataSource$getAllWatchApp$Anon1) {
            watchAppRemoteDataSource$getAllWatchApp$Anon1 = (WatchAppRemoteDataSource$getAllWatchApp$Anon1) kc4;
            int i2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRemoteDataSource$getAllWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
                Object a = oc4.a();
                i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    WatchAppRemoteDataSource$getAllWatchApp$response$Anon1 watchAppRemoteDataSource$getAllWatchApp$response$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$response$Anon1(this, str, (kc4) null);
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon0 = this;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon1 = str;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.label = 1;
                    obj = ResponseKt.a(watchAppRemoteDataSource$getAllWatchApp$response$Anon1, watchAppRemoteDataSource$getAllWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon1;
                    WatchAppRemoteDataSource watchAppRemoteDataSource = (WatchAppRemoteDataSource) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ArrayList arrayList = new ArrayList();
                    so2 so2 = (so2) ro2;
                    ApiResponse apiResponse = (ApiResponse) so2.a();
                    if (apiResponse != null) {
                        List list = apiResponse.get_items();
                        if (list != null) {
                            pc4.a(arrayList.addAll(list));
                        }
                    }
                    return new so2(arrayList, so2.b());
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), (String) null, 8, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        watchAppRemoteDataSource$getAllWatchApp$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$Anon1(this, kc4);
        Object obj2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
        Object a2 = oc4.a();
        i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
