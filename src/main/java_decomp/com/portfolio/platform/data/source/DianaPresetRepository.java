package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ DianaPresetDao mDianaPresetDao;
    @DexIgnore
    public /* final */ DianaPresetRemoteDataSource mDianaPresetRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return DianaPresetRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = DianaPresetRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "DianaPresetRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DianaPresetRepository(DianaPresetDao dianaPresetDao, DianaPresetRemoteDataSource dianaPresetRemoteDataSource) {
        wd4.b(dianaPresetDao, "mDianaPresetDao");
        wd4.b(dianaPresetRemoteDataSource, "mDianaPresetRemoteDataSource");
        this.mDianaPresetDao = dianaPresetDao;
        this.mDianaPresetRemoteDataSource = dianaPresetRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mDianaPresetDao.clearDianaPresetTable();
        this.mDianaPresetDao.clearDianaRecommendPresetTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deletePresetById(String str, kc4<? super cb4> kc4) {
        DianaPresetRepository$deletePresetById$Anon1 dianaPresetRepository$deletePresetById$Anon1;
        int i;
        DianaPreset dianaPreset;
        Object obj;
        DianaPresetRepository dianaPresetRepository;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRepository$deletePresetById$Anon1) {
            dianaPresetRepository$deletePresetById$Anon1 = (DianaPresetRepository$deletePresetById$Anon1) kc4;
            int i2 = dianaPresetRepository$deletePresetById$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRepository$deletePresetById$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = dianaPresetRepository$deletePresetById$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRepository$deletePresetById$Anon1.label;
                Integer num = null;
                if (i != 0) {
                    za4.a(obj2);
                    dianaPreset = this.mDianaPresetDao.getPresetById(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("delete preset ");
                    sb.append(dianaPreset != null ? dianaPreset.getName() : null);
                    sb.append(" pinType ");
                    sb.append(dianaPreset != null ? pc4.a(dianaPreset.getPinType()) : null);
                    local.d(str2, sb.toString());
                    if (dianaPreset != null) {
                        this.mDianaPresetDao.deletePreset(dianaPreset.getId());
                        if (dianaPreset.getPinType() != 1) {
                            DianaPresetRemoteDataSource dianaPresetRemoteDataSource = this.mDianaPresetRemoteDataSource;
                            dianaPresetRepository$deletePresetById$Anon1.L$Anon0 = this;
                            dianaPresetRepository$deletePresetById$Anon1.L$Anon1 = str;
                            dianaPresetRepository$deletePresetById$Anon1.L$Anon2 = dianaPreset;
                            dianaPresetRepository$deletePresetById$Anon1.L$Anon3 = dianaPreset;
                            dianaPresetRepository$deletePresetById$Anon1.label = 1;
                            obj = dianaPresetRemoteDataSource.deleteDianaPreset(dianaPreset, dianaPresetRepository$deletePresetById$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            dianaPresetRepository = this;
                        }
                    }
                    return cb4.a;
                } else if (i == 1) {
                    DianaPreset dianaPreset2 = (DianaPreset) dianaPresetRepository$deletePresetById$Anon1.L$Anon3;
                    String str3 = (String) dianaPresetRepository$deletePresetById$Anon1.L$Anon1;
                    dianaPresetRepository = (DianaPresetRepository) dianaPresetRepository$deletePresetById$Anon1.L$Anon0;
                    za4.a(obj2);
                    Object obj3 = obj2;
                    dianaPreset = (DianaPreset) dianaPresetRepository$deletePresetById$Anon1.L$Anon2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "deletePreset success");
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("deletePreset fail!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverCode ");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(str4, sb2.toString());
                    dianaPreset.setPinType(3);
                    dianaPresetRepository.mDianaPresetDao.upsertPreset(dianaPreset);
                }
                return cb4.a;
            }
        }
        dianaPresetRepository$deletePresetById$Anon1 = new DianaPresetRepository$deletePresetById$Anon1(this, kc4);
        Object obj22 = dianaPresetRepository$deletePresetById$Anon1.result;
        Object a2 = oc4.a();
        i = dianaPresetRepository$deletePresetById$Anon1.label;
        Integer num2 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadPresetList(String str, kc4<? super ro2<List<DianaPreset>>> kc4) {
        DianaPresetRepository$downloadPresetList$Anon1 dianaPresetRepository$downloadPresetList$Anon1;
        int i;
        DianaPresetRepository dianaPresetRepository;
        ro2 ro2;
        String str2;
        ro2 ro22;
        if (kc4 instanceof DianaPresetRepository$downloadPresetList$Anon1) {
            dianaPresetRepository$downloadPresetList$Anon1 = (DianaPresetRepository$downloadPresetList$Anon1) kc4;
            int i2 = dianaPresetRepository$downloadPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRepository$downloadPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRepository$downloadPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRepository$downloadPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "downloadPresetList serial " + str);
                    dianaPresetRepository$downloadPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRepository$downloadPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRepository$downloadPresetList$Anon1.label = 1;
                    obj = executePendingRequest(str, dianaPresetRepository$downloadPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    str2 = str;
                    dianaPresetRepository = this;
                } else if (i == 1) {
                    za4.a(obj);
                    DianaPresetRepository dianaPresetRepository2 = (DianaPresetRepository) dianaPresetRepository$downloadPresetList$Anon1.L$Anon0;
                    str2 = (String) dianaPresetRepository$downloadPresetList$Anon1.L$Anon1;
                    dianaPresetRepository = dianaPresetRepository2;
                } else if (i == 2) {
                    String str4 = (String) dianaPresetRepository$downloadPresetList$Anon1.L$Anon1;
                    dianaPresetRepository = (DianaPresetRepository) dianaPresetRepository$downloadPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                    ro2 = (ro2) obj;
                    Integer num = null;
                    if (!(ro2 instanceof so2)) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("downloadPresetList success isFromCache ");
                        so2 so2 = (so2) ro2;
                        sb.append(so2.b());
                        local2.d(str5, sb.toString());
                        if (!so2.b()) {
                            DianaPresetDao dianaPresetDao = dianaPresetRepository.mDianaPresetDao;
                            Object a2 = so2.a();
                            if (a2 != null) {
                                dianaPresetDao.upsertPresetList((List) a2);
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                        return new so2(so2.a(), so2.b());
                    } else if (ro2 instanceof qo2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("downloadPresetList fail!! ");
                        qo2 qo2 = (qo2) ro2;
                        sb2.append(qo2.a());
                        sb2.append(" serverCode ");
                        ServerError c = qo2.c();
                        if (c != null) {
                            num = c.getCode();
                        }
                        sb2.append(num);
                        local3.d(str6, sb2.toString());
                        return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null, 12, (rd4) null);
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro22 = (ro2) obj;
                if (!(ro22 instanceof so2)) {
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = dianaPresetRepository.mDianaPresetRemoteDataSource;
                    dianaPresetRepository$downloadPresetList$Anon1.L$Anon0 = dianaPresetRepository;
                    dianaPresetRepository$downloadPresetList$Anon1.L$Anon1 = str2;
                    dianaPresetRepository$downloadPresetList$Anon1.label = 2;
                    obj = dianaPresetRemoteDataSource.downloadDianaPresetList(str2, dianaPresetRepository$downloadPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    ro2 = (ro2) obj;
                    Integer num2 = null;
                    if (!(ro2 instanceof so2)) {
                    }
                } else if (ro22 instanceof qo2) {
                    return new qo2(600001, (ServerError) null, (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRepository$downloadPresetList$Anon1 = new DianaPresetRepository$downloadPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRepository$downloadPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRepository$downloadPresetList$Anon1.label;
        if (i != 0) {
        }
        ro22 = (ro2) obj2;
        if (!(ro22 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadRecommendPresetList(String str, kc4<? super cb4> kc4) {
        DianaPresetRepository$downloadRecommendPresetList$Anon1 dianaPresetRepository$downloadRecommendPresetList$Anon1;
        int i;
        DianaPresetRepository dianaPresetRepository;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRepository$downloadRecommendPresetList$Anon1) {
            dianaPresetRepository$downloadRecommendPresetList$Anon1 = (DianaPresetRepository$downloadRecommendPresetList$Anon1) kc4;
            int i2 = dianaPresetRepository$downloadRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRepository$downloadRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRepository$downloadRecommendPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRepository$downloadRecommendPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "downloadRecommendPresetList - serial=" + str);
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = this.mDianaPresetRemoteDataSource;
                    dianaPresetRepository$downloadRecommendPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRepository$downloadRecommendPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRepository$downloadRecommendPresetList$Anon1.label = 1;
                    obj = dianaPresetRemoteDataSource.downloadDianaRecommendPresetList(str, dianaPresetRepository$downloadRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    dianaPresetRepository = this;
                } else if (i == 1) {
                    str = (String) dianaPresetRepository$downloadRecommendPresetList$Anon1.L$Anon1;
                    dianaPresetRepository = (DianaPresetRepository) dianaPresetRepository$downloadRecommendPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadRecommendPresetList - serial=");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    so2 so2 = (so2) ro2;
                    sb.append(so2.b());
                    local2.d(str3, sb.toString());
                    if (!so2.b()) {
                        Object a2 = so2.a();
                        if (a2 != null) {
                            dianaPresetRepository.mDianaPresetDao.upsertDianaRecommendPresetList((ArrayList) a2);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadRecommendPresetList - serial=");
                    sb2.append(str);
                    sb2.append(" failed!!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    sb2.append(" serverError=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(str4, sb2.toString());
                }
                return cb4.a;
            }
        }
        dianaPresetRepository$downloadRecommendPresetList$Anon1 = new DianaPresetRepository$downloadRecommendPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRepository$downloadRecommendPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRepository$downloadRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }

    /* JADX WARNING: type inference failed for: r11v12, types: [com.fossil.blesdk.obfuscated.so2] */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x010d, code lost:
        return r11;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cd A[SYNTHETIC, Splitter:B:29:0x00cd] */
    public final synchronized Object executePendingRequest(String str, kc4<? super ro2<List<DianaPreset>>> kc4) {
        DianaPresetRepository$executePendingRequest$Anon1 dianaPresetRepository$executePendingRequest$Anon1;
        int i;
        DianaPresetRepository dianaPresetRepository;
        ro2 ro2;
        qo2 qo2;
        if (kc4 instanceof DianaPresetRepository$executePendingRequest$Anon1) {
            dianaPresetRepository$executePendingRequest$Anon1 = (DianaPresetRepository$executePendingRequest$Anon1) kc4;
            if ((dianaPresetRepository$executePendingRequest$Anon1.label & Integer.MIN_VALUE) != 0) {
                dianaPresetRepository$executePendingRequest$Anon1.label -= Integer.MIN_VALUE;
                Object obj = dianaPresetRepository$executePendingRequest$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRepository$executePendingRequest$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    List<DianaPreset> allPendingPreset = this.mDianaPresetDao.getAllPendingPreset(str);
                    FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest pendingPreset=" + allPendingPreset + " of " + str);
                    if (!allPendingPreset.isEmpty()) {
                        List<DianaPreset> allPreset = this.mDianaPresetDao.getAllPreset(str);
                        DianaPresetRemoteDataSource dianaPresetRemoteDataSource = this.mDianaPresetRemoteDataSource;
                        dianaPresetRepository$executePendingRequest$Anon1.L$Anon0 = this;
                        dianaPresetRepository$executePendingRequest$Anon1.L$Anon1 = str;
                        dianaPresetRepository$executePendingRequest$Anon1.L$Anon2 = allPendingPreset;
                        dianaPresetRepository$executePendingRequest$Anon1.L$Anon3 = allPreset;
                        dianaPresetRepository$executePendingRequest$Anon1.label = 1;
                        obj = dianaPresetRemoteDataSource.replaceDianaPresetList(allPreset, dianaPresetRepository$executePendingRequest$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        dianaPresetRepository = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest success no pending");
                        return new so2(new ArrayList(), false, 2, (rd4) null);
                    }
                } else if (i == 1) {
                    List list = (List) dianaPresetRepository$executePendingRequest$Anon1.L$Anon3;
                    List list2 = (List) dianaPresetRepository$executePendingRequest$Anon1.L$Anon2;
                    String str2 = (String) dianaPresetRepository$executePendingRequest$Anon1.L$Anon1;
                    dianaPresetRepository = (DianaPresetRepository) dianaPresetRepository$executePendingRequest$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    DianaPresetDao dianaPresetDao = dianaPresetRepository.mDianaPresetDao;
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        dianaPresetDao.upsertPresetList((List) a2);
                        dianaPresetRepository.mDianaPresetDao.removeAllDeletePinTypePreset();
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest success sync with server");
                        qo2 = new so2(((so2) ro2).a(), false, 2, (rd4) null);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (ro2 instanceof qo2) {
                    FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest fail to sync with server " + ((qo2) ro2).a());
                    qo2 = new qo2(((qo2) ro2).a(), ((qo2) ro2).c(), (Throwable) null, (String) null, 12, (rd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRepository$executePendingRequest$Anon1 = new DianaPresetRepository$executePendingRequest$Anon1(this, kc4);
        Object obj2 = dianaPresetRepository$executePendingRequest$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRepository$executePendingRequest$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final DianaPreset getActivePresetBySerial(String str) {
        wd4.b(str, "serial");
        return this.mDianaPresetDao.getActivePresetBySerial(str);
    }

    @DexIgnore
    public final LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        wd4.b(str, "serial");
        return this.mDianaPresetDao.getActivePresetBySerialLiveData(str);
    }

    @DexIgnore
    public final DianaPreset getPresetById(String str) {
        wd4.b(str, "id");
        return this.mDianaPresetDao.getPresetById(str);
    }

    @DexIgnore
    public final ArrayList<DianaPreset> getPresetList(String str) {
        wd4.b(str, "serial");
        List<DianaPreset> allPreset = this.mDianaPresetDao.getAllPreset(str);
        if (allPreset != null) {
            return (ArrayList) allPreset;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset>");
    }

    @DexIgnore
    public final LiveData<List<DianaPreset>> getPresetListAsLiveData(String str) {
        wd4.b(str, "serial");
        return this.mDianaPresetDao.getAllPresetAsLiveData(str);
    }

    @DexIgnore
    public final List<DianaRecommendPreset> getRecommendPresetList(String str) {
        wd4.b(str, "serial");
        return this.mDianaPresetDao.getDianaRecommendPresetList(str);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPreset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPreset} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object upsertPreset(DianaPreset dianaPreset, kc4<? super cb4> kc4) {
        DianaPreset dianaPreset2;
        Object obj;
        FLogger.INSTANCE.getLocal().d(TAG, "upsertPreset " + dianaPreset + " pinType " + dianaPreset.getPinType());
        dianaPreset.setUpdatedAt(sk2.t(new Date(System.currentTimeMillis())));
        List<DianaPreset> allPreset = this.mDianaPresetDao.getAllPreset(dianaPreset.getSerialNumber());
        if (allPreset != null) {
            ArrayList arrayList = (ArrayList) allPreset;
            if (!(!arrayList.isEmpty())) {
                return cb4.a;
            }
            Iterator it = arrayList.iterator();
            while (true) {
                dianaPreset2 = null;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (pc4.a(wd4.a((Object) ((DianaPreset) obj).getId(), (Object) dianaPreset.getId())).booleanValue()) {
                    break;
                }
            }
            DianaPreset dianaPreset3 = (DianaPreset) obj;
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                if (pc4.a(next.isActive()).booleanValue()) {
                    dianaPreset2 = next;
                    break;
                }
            }
            DianaPreset dianaPreset4 = dianaPreset2;
            if (dianaPreset3 == null || dianaPreset3.getPinType() == 1) {
                dianaPreset.setPinType(1);
            } else {
                dianaPreset.setPinType(2);
            }
            if (dianaPreset4 != null && (!wd4.a((Object) dianaPreset4.getId(), (Object) dianaPreset.getId())) && dianaPreset.isActive()) {
                dianaPreset4.setActive(false);
                if (dianaPreset4.getPinType() != 1) {
                    dianaPreset4.setPinType(2);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (pc4.a(!wd4.a((Object) ((DianaPreset) next2).getId(), (Object) dianaPreset.getId())).booleanValue()) {
                    arrayList2.add(next2);
                }
            }
            arrayList2.add(dianaPreset);
            return upsertPresetList(arrayList2, kc4);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset>");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object upsertPresetList(List<DianaPreset> list, kc4<? super cb4> kc4) {
        DianaPresetRepository$upsertPresetList$Anon1 dianaPresetRepository$upsertPresetList$Anon1;
        int i;
        DianaPresetRepository dianaPresetRepository;
        ro2 ro2;
        if (kc4 instanceof DianaPresetRepository$upsertPresetList$Anon1) {
            dianaPresetRepository$upsertPresetList$Anon1 = (DianaPresetRepository$upsertPresetList$Anon1) kc4;
            int i2 = dianaPresetRepository$upsertPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRepository$upsertPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRepository$upsertPresetList$Anon1.result;
                Object a = oc4.a();
                i = dianaPresetRepository$upsertPresetList$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "upsertPresetList");
                    this.mDianaPresetDao.upsertPresetList(list);
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = this.mDianaPresetRemoteDataSource;
                    dianaPresetRepository$upsertPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRepository$upsertPresetList$Anon1.L$Anon1 = list;
                    dianaPresetRepository$upsertPresetList$Anon1.label = 1;
                    obj = dianaPresetRemoteDataSource.upsertDianaPresetList(list, dianaPresetRepository$upsertPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    dianaPresetRepository = this;
                } else if (i == 1) {
                    List list2 = (List) dianaPresetRepository$upsertPresetList$Anon1.L$Anon1;
                    dianaPresetRepository = (DianaPresetRepository) dianaPresetRepository$upsertPresetList$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                Integer num = null;
                if (!(ro2 instanceof so2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "upsertPresetList success");
                    DianaPresetDao dianaPresetDao = dianaPresetRepository.mDianaPresetDao;
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        dianaPresetDao.upsertPresetList((List) a2);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("upsertPresetList fail!! ");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" serverCode ");
                    ServerError c = qo2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb.append(num);
                    local.d(str, sb.toString());
                }
                return cb4.a;
            }
        }
        dianaPresetRepository$upsertPresetList$Anon1 = new DianaPresetRepository$upsertPresetList$Anon1(this, kc4);
        Object obj2 = dianaPresetRepository$upsertPresetList$Anon1.result;
        Object a3 = oc4.a();
        i = dianaPresetRepository$upsertPresetList$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        Integer num2 = null;
        if (!(ro2 instanceof so2)) {
        }
        return cb4.a;
    }
}
