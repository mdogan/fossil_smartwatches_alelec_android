package com.portfolio.platform.data.source.local.diana;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Explore;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeDatabase_Impl extends DianaCustomizeDatabase {
    @DexIgnore
    public volatile ComplicationDao _complicationDao;
    @DexIgnore
    public volatile ComplicationLastSettingDao _complicationLastSettingDao;
    @DexIgnore
    public volatile DianaPresetDao _dianaPresetDao;
    @DexIgnore
    public volatile WatchAppDao _watchAppDao;
    @DexIgnore
    public volatile WatchAppLastSettingDao _watchAppLastSettingDao;
    @DexIgnore
    public volatile WatchFaceDao _watchFaceDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `complication` (`complicationId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `watchApp` (`watchappId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `icon` TEXT, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, PRIMARY KEY(`watchappId`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `complicationLastSetting` (`complicationId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `watchAppLastSetting` (`watchAppId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`watchAppId`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `dianaPreset` (`createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `name` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `dianaRecommendPreset` (`serialNumber` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b36140bba6a58f9cc8d2af308d56724d')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `complication`");
            hgVar.b("DROP TABLE IF EXISTS `watchApp`");
            hgVar.b("DROP TABLE IF EXISTS `complicationLastSetting`");
            hgVar.b("DROP TABLE IF EXISTS `watchAppLastSetting`");
            hgVar.b("DROP TABLE IF EXISTS `dianaPreset`");
            hgVar.b("DROP TABLE IF EXISTS `dianaRecommendPreset`");
            hgVar.b("DROP TABLE IF EXISTS `watch_face`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = DianaCustomizeDatabase_Impl.this.mDatabase = hgVar;
            DianaCustomizeDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            hg hgVar2 = hgVar;
            HashMap hashMap = new HashMap(9);
            hashMap.put("complicationId", new fg.a("complicationId", "TEXT", true, 1));
            hashMap.put("name", new fg.a("name", "TEXT", true, 0));
            hashMap.put("nameKey", new fg.a("nameKey", "TEXT", true, 0));
            hashMap.put("categories", new fg.a("categories", "TEXT", true, 0));
            hashMap.put("description", new fg.a("description", "TEXT", true, 0));
            hashMap.put("descriptionKey", new fg.a("descriptionKey", "TEXT", true, 0));
            hashMap.put("icon", new fg.a("icon", "TEXT", false, 0));
            hashMap.put("createdAt", new fg.a("createdAt", "TEXT", true, 0));
            hashMap.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
            fg fgVar = new fg("complication", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar2, "complication");
            if (fgVar.equals(a)) {
                HashMap hashMap2 = new HashMap(9);
                hashMap2.put("watchappId", new fg.a("watchappId", "TEXT", true, 1));
                hashMap2.put("name", new fg.a("name", "TEXT", true, 0));
                hashMap2.put("nameKey", new fg.a("nameKey", "TEXT", true, 0));
                hashMap2.put("description", new fg.a("description", "TEXT", true, 0));
                hashMap2.put("descriptionKey", new fg.a("descriptionKey", "TEXT", true, 0));
                hashMap2.put("categories", new fg.a("categories", "TEXT", true, 0));
                hashMap2.put("icon", new fg.a("icon", "TEXT", false, 0));
                hashMap2.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
                hashMap2.put("createdAt", new fg.a("createdAt", "TEXT", true, 0));
                fg fgVar2 = new fg("watchApp", hashMap2, new HashSet(0), new HashSet(0));
                fg a2 = fg.a(hgVar2, "watchApp");
                if (fgVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(3);
                    hashMap3.put("complicationId", new fg.a("complicationId", "TEXT", true, 1));
                    hashMap3.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
                    hashMap3.put(MicroAppSetting.SETTING, new fg.a(MicroAppSetting.SETTING, "TEXT", true, 0));
                    fg fgVar3 = new fg("complicationLastSetting", hashMap3, new HashSet(0), new HashSet(0));
                    fg a3 = fg.a(hgVar2, "complicationLastSetting");
                    if (fgVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(3);
                        hashMap4.put("watchAppId", new fg.a("watchAppId", "TEXT", true, 1));
                        hashMap4.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
                        hashMap4.put(MicroAppSetting.SETTING, new fg.a(MicroAppSetting.SETTING, "TEXT", true, 0));
                        fg fgVar4 = new fg("watchAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
                        fg a4 = fg.a(hgVar2, "watchAppLastSetting");
                        if (fgVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(10);
                            hashMap5.put("createdAt", new fg.a("createdAt", "TEXT", false, 0));
                            hashMap5.put("updatedAt", new fg.a("updatedAt", "TEXT", false, 0));
                            hashMap5.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
                            hashMap5.put("id", new fg.a("id", "TEXT", true, 1));
                            hashMap5.put("serialNumber", new fg.a("serialNumber", "TEXT", true, 0));
                            hashMap5.put("name", new fg.a("name", "TEXT", true, 0));
                            hashMap5.put("isActive", new fg.a("isActive", "INTEGER", true, 0));
                            hashMap5.put("complications", new fg.a("complications", "TEXT", true, 0));
                            hashMap5.put("watchapps", new fg.a("watchapps", "TEXT", true, 0));
                            hashMap5.put("watchFaceId", new fg.a("watchFaceId", "TEXT", true, 0));
                            fg fgVar5 = new fg("dianaPreset", hashMap5, new HashSet(0), new HashSet(0));
                            fg a5 = fg.a(hgVar2, "dianaPreset");
                            if (fgVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(9);
                                hashMap6.put("serialNumber", new fg.a("serialNumber", "TEXT", true, 0));
                                hashMap6.put("id", new fg.a("id", "TEXT", true, 1));
                                hashMap6.put("name", new fg.a("name", "TEXT", true, 0));
                                hashMap6.put("isDefault", new fg.a("isDefault", "INTEGER", true, 0));
                                hashMap6.put("complications", new fg.a("complications", "TEXT", true, 0));
                                hashMap6.put("watchapps", new fg.a("watchapps", "TEXT", true, 0));
                                hashMap6.put("watchFaceId", new fg.a("watchFaceId", "TEXT", true, 0));
                                hashMap6.put("createdAt", new fg.a("createdAt", "TEXT", true, 0));
                                hashMap6.put("updatedAt", new fg.a("updatedAt", "TEXT", true, 0));
                                fg fgVar6 = new fg("dianaRecommendPreset", hashMap6, new HashSet(0), new HashSet(0));
                                fg a6 = fg.a(hgVar2, "dianaRecommendPreset");
                                if (fgVar6.equals(a6)) {
                                    HashMap hashMap7 = new HashMap(6);
                                    hashMap7.put("id", new fg.a("id", "TEXT", true, 1));
                                    hashMap7.put("name", new fg.a("name", "TEXT", true, 0));
                                    hashMap7.put("ringStyleItems", new fg.a("ringStyleItems", "TEXT", false, 0));
                                    hashMap7.put(Explore.COLUMN_BACKGROUND, new fg.a(Explore.COLUMN_BACKGROUND, "TEXT", true, 0));
                                    hashMap7.put("previewUrl", new fg.a("previewUrl", "TEXT", true, 0));
                                    hashMap7.put("serial", new fg.a("serial", "TEXT", true, 0));
                                    fg fgVar7 = new fg("watch_face", hashMap7, new HashSet(0), new HashSet(0));
                                    fg a7 = fg.a(hgVar2, "watch_face");
                                    if (!fgVar7.equals(a7)) {
                                        throw new IllegalStateException("Migration didn't properly handle watch_face(com.portfolio.platform.data.model.diana.preset.WatchFace).\n Expected:\n" + fgVar7 + "\n Found:\n" + a7);
                                    }
                                    return;
                                }
                                throw new IllegalStateException("Migration didn't properly handle dianaRecommendPreset(com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset).\n Expected:\n" + fgVar6 + "\n Found:\n" + a6);
                            }
                            throw new IllegalStateException("Migration didn't properly handle dianaPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset).\n Expected:\n" + fgVar5 + "\n Found:\n" + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle watchAppLastSetting(com.portfolio.platform.data.model.diana.WatchAppLastSetting).\n Expected:\n" + fgVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle complicationLastSetting(com.portfolio.platform.data.model.diana.ComplicationLastSetting).\n Expected:\n" + fgVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle watchApp(com.portfolio.platform.data.model.diana.WatchApp).\n Expected:\n" + fgVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle complication(com.portfolio.platform.data.model.diana.Complication).\n Expected:\n" + fgVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `complication`");
            a.b("DELETE FROM `watchApp`");
            a.b("DELETE FROM `complicationLastSetting`");
            a.b("DELETE FROM `watchAppLastSetting`");
            a.b("DELETE FROM `dianaPreset`");
            a.b("DELETE FROM `dianaRecommendPreset`");
            a.b("DELETE FROM `watch_face`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "complication", "watchApp", "complicationLastSetting", "watchAppLastSetting", "dianaPreset", "dianaRecommendPreset", "watch_face");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(13), "b36140bba6a58f9cc8d2af308d56724d", "997a26c4a63a92c3bb7a9ba027efafaa");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public ComplicationDao getComplicationDao() {
        ComplicationDao complicationDao;
        if (this._complicationDao != null) {
            return this._complicationDao;
        }
        synchronized (this) {
            if (this._complicationDao == null) {
                this._complicationDao = new ComplicationDao_Impl(this);
            }
            complicationDao = this._complicationDao;
        }
        return complicationDao;
    }

    @DexIgnore
    public ComplicationLastSettingDao getComplicationSettingDao() {
        ComplicationLastSettingDao complicationLastSettingDao;
        if (this._complicationLastSettingDao != null) {
            return this._complicationLastSettingDao;
        }
        synchronized (this) {
            if (this._complicationLastSettingDao == null) {
                this._complicationLastSettingDao = new ComplicationLastSettingDao_Impl(this);
            }
            complicationLastSettingDao = this._complicationLastSettingDao;
        }
        return complicationLastSettingDao;
    }

    @DexIgnore
    public DianaPresetDao getPresetDao() {
        DianaPresetDao dianaPresetDao;
        if (this._dianaPresetDao != null) {
            return this._dianaPresetDao;
        }
        synchronized (this) {
            if (this._dianaPresetDao == null) {
                this._dianaPresetDao = new DianaPresetDao_Impl(this);
            }
            dianaPresetDao = this._dianaPresetDao;
        }
        return dianaPresetDao;
    }

    @DexIgnore
    public WatchAppDao getWatchAppDao() {
        WatchAppDao watchAppDao;
        if (this._watchAppDao != null) {
            return this._watchAppDao;
        }
        synchronized (this) {
            if (this._watchAppDao == null) {
                this._watchAppDao = new WatchAppDao_Impl(this);
            }
            watchAppDao = this._watchAppDao;
        }
        return watchAppDao;
    }

    @DexIgnore
    public WatchAppLastSettingDao getWatchAppSettingDao() {
        WatchAppLastSettingDao watchAppLastSettingDao;
        if (this._watchAppLastSettingDao != null) {
            return this._watchAppLastSettingDao;
        }
        synchronized (this) {
            if (this._watchAppLastSettingDao == null) {
                this._watchAppLastSettingDao = new WatchAppLastSettingDao_Impl(this);
            }
            watchAppLastSettingDao = this._watchAppLastSettingDao;
        }
        return watchAppLastSettingDao;
    }

    @DexIgnore
    public WatchFaceDao getWatchFaceDao() {
        WatchFaceDao watchFaceDao;
        if (this._watchFaceDao != null) {
            return this._watchFaceDao;
        }
        synchronized (this) {
            if (this._watchFaceDao == null) {
                this._watchFaceDao = new WatchFaceDao_Impl(this);
            }
            watchFaceDao = this._watchFaceDao;
        }
        return watchFaceDao;
    }
}
