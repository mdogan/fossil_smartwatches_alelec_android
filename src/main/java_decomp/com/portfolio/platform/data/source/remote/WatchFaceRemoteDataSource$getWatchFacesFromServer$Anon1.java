package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource", f = "WatchFaceRemoteDataSource.kt", l = {16}, m = "getWatchFacesFromServer")
public final class WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRemoteDataSource$getWatchFacesFromServer$Anon1(WatchFaceRemoteDataSource watchFaceRemoteDataSource, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = watchFaceRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.getWatchFacesFromServer((String) null, this);
    }
}
