package com.portfolio.platform.data.source.local;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeRealDataDatabase_Impl extends CustomizeRealDataDatabase {
    @DexIgnore
    public volatile CustomizeRealDataDao _customizeRealDataDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `customizeRealData` (`id` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6ca914ed7b753a02dc3ff84030dc3e24')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `customizeRealData`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = CustomizeRealDataDatabase_Impl.this.mDatabase = hgVar;
            CustomizeRealDataDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new fg.a("id", "TEXT", true, 1));
            hashMap.put("value", new fg.a("value", "TEXT", true, 0));
            fg fgVar = new fg("customizeRealData", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, "customizeRealData");
            if (!fgVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle customizeRealData(com.portfolio.platform.data.model.CustomizeRealData).\n Expected:\n" + fgVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `customizeRealData`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "customizeRealData");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(1), "6ca914ed7b753a02dc3ff84030dc3e24", "8e8005f3461a63f640274ec577069e4f");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public CustomizeRealDataDao realDataDao() {
        CustomizeRealDataDao customizeRealDataDao;
        if (this._customizeRealDataDao != null) {
            return this._customizeRealDataDao;
        }
        synchronized (this) {
            if (this._customizeRealDataDao == null) {
                this._customizeRealDataDao = new CustomizeRealDataDao_Impl(this);
            }
            customizeRealDataDao = this._customizeRealDataDao;
        }
        return customizeRealDataDao;
    }
}
