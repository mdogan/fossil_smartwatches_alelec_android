package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRemoteDataSource.class.getSimpleName();
        wd4.a((Object) simpleName, "DeviceRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object forceLinkDevice(Device device, kc4<? super ro2<Void>> kc4) {
        return ResponseKt.a(new DeviceRemoteDataSource$forceLinkDevice$Anon2(this, device, (kc4) null), kc4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object generatePairingKey(String str, kc4<? super ro2<String>> kc4) {
        DeviceRemoteDataSource$generatePairingKey$Anon1 deviceRemoteDataSource$generatePairingKey$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DeviceRemoteDataSource$generatePairingKey$Anon1) {
            deviceRemoteDataSource$generatePairingKey$Anon1 = (DeviceRemoteDataSource$generatePairingKey$Anon1) kc4;
            int i2 = deviceRemoteDataSource$generatePairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$generatePairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$generatePairingKey$Anon1.result;
                Object a = oc4.a();
                i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    yz1.a("serialNumber", str);
                    DeviceRemoteDataSource$generatePairingKey$response$Anon1 deviceRemoteDataSource$generatePairingKey$response$Anon1 = new DeviceRemoteDataSource$generatePairingKey$response$Anon1(this, yz1, (kc4) null);
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon2 = yz1;
                    deviceRemoteDataSource$generatePairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$generatePairingKey$response$Anon1, deviceRemoteDataSource$generatePairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$generatePairingKey$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "generatePairingKey of " + str + " response " + ro2);
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 == null) {
                        wd4.a();
                        throw null;
                    } else if (!((yz1) a2).d("randomKey")) {
                        return new qo2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((yz1) so2.a()).a("randomKey");
                        wd4.a((Object) a3, "response.response.get(Co\u2026ants.JSON_KEY_RANDOM_KEY)");
                        return new so2(a3.f(), false, 2, (rd4) null);
                    }
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), qo2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$generatePairingKey$Anon1 = new DeviceRemoteDataSource$generatePairingKey$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$generatePairingKey$Anon1.result;
        Object a4 = oc4.a();
        i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "generatePairingKey of " + str + " response " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final Object getAllDevice(kc4<? super ro2<ApiResponse<Device>>> kc4) {
        FLogger.INSTANCE.getLocal().d(TAG, "getAllDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$getAllDevice$Anon2(this, (kc4) null), kc4);
    }

    @DexIgnore
    public final Object getDeviceBySerial(String str, kc4<? super ro2<Device>> kc4) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDeviceBySerial");
        return ResponseKt.a(new DeviceRemoteDataSource$getDeviceBySerial$Anon2(this, str, (kc4) null), kc4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getDeviceSecretKey(String str, kc4<? super ro2<String>> kc4) {
        DeviceRemoteDataSource$getDeviceSecretKey$Anon1 deviceRemoteDataSource$getDeviceSecretKey$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DeviceRemoteDataSource$getDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$getDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$getDeviceSecretKey$Anon1) kc4;
            int i2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
                Object a = oc4.a();
                i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1 deviceRemoteDataSource$getDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1(this, str, (kc4) null);
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getDeviceSecretKey$response$Anon1, deviceRemoteDataSource$getDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getDeviceSecretKey of " + str + " response " + ro2);
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 == null) {
                        wd4.a();
                        throw null;
                    } else if (!((yz1) a2).d("secretKey")) {
                        return new qo2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((yz1) so2.a()).a("secretKey");
                        if (a3 instanceof xz1) {
                            return new so2("", false, 2, (rd4) null);
                        }
                        wd4.a((Object) a3, "secretKey");
                        return new so2(a3.f(), false, 2, (rd4) null);
                    }
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), qo2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
        Object a4 = oc4.a();
        i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "getDeviceSecretKey of " + str + " response " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getLatestWatchParamFromServer(String str, int i, kc4<? super WatchParameterResponse> kc4) {
        DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1;
        int i2;
        ro2 ro2;
        if (kc4 instanceof DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) {
            deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = (DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) kc4;
            int i3 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
                Object a = oc4.a();
                i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this, str, i, (kc4) null);
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$Anon0 = i;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1, deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    i = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$Anon0;
                    str = (String) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
                if (!(ro2 instanceof so2)) {
                    ApiResponse apiResponse = (ApiResponse) ((so2) ro2).a();
                    List list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list == null || !(!list.isEmpty())) {
                        return null;
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, "getLatestWatchParamFromServer success, response = " + ((WatchParameterResponse) list.get(0)));
                    return (WatchParameterResponse) list.get(0);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.d(str4, "getLatestWatchParamFromServer failed, errorCode = " + ((qo2) ro2).a());
                    return null;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
        Object a2 = oc4.a();
        i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
        if (i2 != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local4.d(str22, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getSupportedSku(int i, kc4<? super ro2<ApiResponse<SKUModel>>> kc4) {
        DeviceRemoteDataSource$getSupportedSku$Anon1 deviceRemoteDataSource$getSupportedSku$Anon1;
        int i2;
        ro2 ro2;
        if (kc4 instanceof DeviceRemoteDataSource$getSupportedSku$Anon1) {
            deviceRemoteDataSource$getSupportedSku$Anon1 = (DeviceRemoteDataSource$getSupportedSku$Anon1) kc4;
            int i3 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getSupportedSku$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getSupportedSku$Anon1.result;
                Object a = oc4.a();
                i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
                if (i2 != 0) {
                    za4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchSupportedSkus");
                    DeviceRemoteDataSource$getSupportedSku$response$Anon1 deviceRemoteDataSource$getSupportedSku$response$Anon1 = new DeviceRemoteDataSource$getSupportedSku$response$Anon1(this, i, (kc4) null);
                    deviceRemoteDataSource$getSupportedSku$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$getSupportedSku$Anon1.I$Anon0 = i;
                    deviceRemoteDataSource$getSupportedSku$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getSupportedSku$response$Anon1, deviceRemoteDataSource$getSupportedSku$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    int i4 = deviceRemoteDataSource$getSupportedSku$Anon1.I$Anon0;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getSupportedSku$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    Object a2 = ((so2) ro2).a();
                    if (a2 != null) {
                        return new so2(a2, false, 2, (rd4) null);
                    }
                    wd4.a();
                    throw null;
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), (Throwable) null, (String) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$getSupportedSku$Anon1 = new DeviceRemoteDataSource$getSupportedSku$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$getSupportedSku$Anon1.result;
        Object a3 = oc4.a();
        i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
        if (i2 != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final Object removeDevice(Device device, kc4<? super ro2<Void>> kc4) {
        FLogger.INSTANCE.getLocal().d(TAG, "removeDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$removeDevice$Anon2(this, device, (kc4) null), kc4);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object swapPairingKey(String str, String str2, kc4<? super ro2<String>> kc4) {
        DeviceRemoteDataSource$swapPairingKey$Anon1 deviceRemoteDataSource$swapPairingKey$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof DeviceRemoteDataSource$swapPairingKey$Anon1) {
            deviceRemoteDataSource$swapPairingKey$Anon1 = (DeviceRemoteDataSource$swapPairingKey$Anon1) kc4;
            int i2 = deviceRemoteDataSource$swapPairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$swapPairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$swapPairingKey$Anon1.result;
                Object a = oc4.a();
                i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    yz1.a("serialNumber", str2);
                    yz1.a("encryptedData", str);
                    DeviceRemoteDataSource$swapPairingKey$response$Anon1 deviceRemoteDataSource$swapPairingKey$response$Anon1 = new DeviceRemoteDataSource$swapPairingKey$response$Anon1(this, yz1, (kc4) null);
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon2 = str2;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon3 = yz1;
                    deviceRemoteDataSource$swapPairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$swapPairingKey$response$Anon1, deviceRemoteDataSource$swapPairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon3;
                    str2 = deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$swapPairingKey$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "swapPairingKey " + str + " of " + str2 + " response " + ro2);
                if (!(ro2 instanceof so2)) {
                    so2 so2 = (so2) ro2;
                    Object a2 = so2.a();
                    if (a2 == null) {
                        wd4.a();
                        throw null;
                    } else if (!((yz1) a2).d("encryptedData")) {
                        return new qo2(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((yz1) so2.a()).a("encryptedData");
                        if (a3 instanceof xz1) {
                            return new qo2(600, (ServerError) null, (Throwable) null, (String) null);
                        }
                        wd4.a((Object) a3, "encryptedData");
                        return new so2(a3.f(), false, 2, (rd4) null);
                    }
                } else if (ro2 instanceof qo2) {
                    qo2 qo2 = (qo2) ro2;
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), qo2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        deviceRemoteDataSource$swapPairingKey$Anon1 = new DeviceRemoteDataSource$swapPairingKey$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$swapPairingKey$Anon1.result;
        Object a4 = oc4.a();
        i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "swapPairingKey " + str + " of " + str2 + " response " + ro2);
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final Object updateDevice(Device device, kc4<? super ro2<Void>> kc4) {
        FLogger.INSTANCE.getLocal().d(TAG, "updateDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$updateDevice$Anon2(this, device, (kc4) null), kc4);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object updateDeviceSecretKey(String str, String str2, kc4<? super cb4> kc4) {
        DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$Anon1;
        int i;
        if (kc4 instanceof DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) kc4;
            int i2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
                Object a = oc4.a();
                i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    yz1 yz1 = new yz1();
                    yz1.a("secretKey", str2);
                    DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1(this, str, yz1, (kc4) null);
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon0 = this;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon1 = str;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon2 = str2;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon3 = yz1;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1, deviceRemoteDataSource$updateDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon3;
                    str2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon2;
                    str = (String) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ((ro2) obj));
                return cb4.a;
            }
        }
        deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$Anon1(this, kc4);
        Object obj2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
        Object a2 = oc4.a();
        i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ((ro2) obj2));
        return cb4.a;
    }
}
