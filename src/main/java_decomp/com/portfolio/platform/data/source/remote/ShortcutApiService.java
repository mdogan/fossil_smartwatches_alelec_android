package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.st4;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ShortcutApiService {
    @DexIgnore
    @gt4("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@st4("offset") int i, @st4("size") int i2, @st4("serialNumber") String str);

    @DexIgnore
    @gt4("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@st4("page") int i, @st4("size") int i2, @st4("serialNumber") String str);
}
