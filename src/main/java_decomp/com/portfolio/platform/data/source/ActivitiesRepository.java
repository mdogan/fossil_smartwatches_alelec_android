package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ActivitySampleDao mActivitySampleDao;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ yk2 mFitnessHelper;
    @DexIgnore
    public /* final */ SampleRawDao mSampleRawDao;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitiesRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingActivitiesCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<ActivitySample> list);
    }

    /*
    static {
        String simpleName = ActivitiesRepository.class.getSimpleName();
        wd4.a((Object) simpleName, "ActivitiesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitiesRepository(ApiServiceV2 apiServiceV2, SampleRawDao sampleRawDao, ActivitySampleDao activitySampleDao, FitnessDatabase fitnessDatabase, FitnessDataDao fitnessDataDao, UserRepository userRepository, yk2 yk2) {
        wd4.b(apiServiceV2, "mApiService");
        wd4.b(sampleRawDao, "mSampleRawDao");
        wd4.b(activitySampleDao, "mActivitySampleDao");
        wd4.b(fitnessDatabase, "mFitnessDatabase");
        wd4.b(fitnessDataDao, "mFitnessDataDao");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(yk2, "mFitnessHelper");
        this.mApiService = apiServiceV2;
        this.mSampleRawDao = sampleRawDao;
        this.mActivitySampleDao = activitySampleDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mFitnessDataDao = fitnessDataDao;
        this.mUserRepository = userRepository;
        this.mFitnessHelper = yk2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchActivitySamples$default(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, kc4 kc4, int i3, Object obj) {
        return activitiesRepository.fetchActivitySamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, kc4);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        this.mActivitySampleDao.deleteAllActivitySamples();
        this.mSampleRawDao.deleteAllActivitySamples();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchActivitySamples(Date date, Date date2, int i, int i2, kc4<? super ro2<ApiResponse<Activity>>> kc4) {
        ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon1;
        int i3;
        ro2 ro2;
        ro2 ro22;
        Object obj;
        int i4;
        Date date3;
        ActivitiesRepository activitiesRepository;
        int i5;
        Date date4 = date;
        Date date5 = date2;
        kc4<? super ro2<ApiResponse<Activity>>> kc42 = kc4;
        if (kc42 instanceof ActivitiesRepository$fetchActivitySamples$Anon1) {
            activitiesRepository$fetchActivitySamples$Anon1 = (ActivitiesRepository$fetchActivitySamples$Anon1) kc42;
            int i6 = activitiesRepository$fetchActivitySamples$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$fetchActivitySamples$Anon1.label = i6 - Integer.MIN_VALUE;
                ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon12 = activitiesRepository$fetchActivitySamples$Anon1;
                Object obj2 = activitiesRepository$fetchActivitySamples$Anon12.result;
                Object a = oc4.a();
                i3 = activitiesRepository$fetchActivitySamples$Anon12.label;
                if (i3 != 0) {
                    za4.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchActivitySamples: start = " + date4 + ", end = " + date5);
                    ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1 activitiesRepository$fetchActivitySamples$repoResponse$Anon1 = new ActivitiesRepository$fetchActivitySamples$repoResponse$Anon1(this, date, date2, i, i2, (kc4) null);
                    activitiesRepository$fetchActivitySamples$Anon12.L$Anon0 = this;
                    activitiesRepository$fetchActivitySamples$Anon12.L$Anon1 = date4;
                    activitiesRepository$fetchActivitySamples$Anon12.L$Anon2 = date5;
                    i5 = i;
                    activitiesRepository$fetchActivitySamples$Anon12.I$Anon0 = i5;
                    int i7 = i2;
                    activitiesRepository$fetchActivitySamples$Anon12.I$Anon1 = i7;
                    activitiesRepository$fetchActivitySamples$Anon12.label = 1;
                    Object a2 = ResponseKt.a(activitiesRepository$fetchActivitySamples$repoResponse$Anon1, activitiesRepository$fetchActivitySamples$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    activitiesRepository = this;
                } else if (i3 == 1) {
                    int i8 = activitiesRepository$fetchActivitySamples$Anon12.I$Anon1;
                    i5 = activitiesRepository$fetchActivitySamples$Anon12.I$Anon0;
                    za4.a(obj2);
                    i4 = i8;
                    date3 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$Anon2;
                    date4 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$Anon1;
                    activitiesRepository = (ActivitiesRepository) activitiesRepository$fetchActivitySamples$Anon12.L$Anon0;
                } else if (i3 == 2) {
                    List list = (List) activitiesRepository$fetchActivitySamples$Anon12.L$Anon4;
                    ro22 = (ro2) activitiesRepository$fetchActivitySamples$Anon12.L$Anon3;
                    int i9 = activitiesRepository$fetchActivitySamples$Anon12.I$Anon1;
                    int i10 = activitiesRepository$fetchActivitySamples$Anon12.I$Anon0;
                    Date date6 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$Anon2;
                    Date date7 = (Date) activitiesRepository$fetchActivitySamples$Anon12.L$Anon1;
                    ActivitiesRepository activitiesRepository2 = (ActivitiesRepository) activitiesRepository$fetchActivitySamples$Anon12.L$Anon0;
                    try {
                        za4.a(obj2);
                        obj = obj2;
                        return (ro2) obj;
                    } catch (Exception e) {
                        e = e;
                        ro2 = ro22;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj2;
                if (!(ro2 instanceof so2)) {
                    if (((so2) ro2).a() != null) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            for (Activity activitySample : ((ApiResponse) ((so2) ro2).a()).get_items()) {
                                arrayList.add(activitySample.toActivitySample());
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("fetchActivitySamples: DBNAME=");
                            ig openHelper = activitiesRepository.mFitnessDatabase.getOpenHelper();
                            wd4.a((Object) openHelper, "mFitnessDatabase.openHelper");
                            sb.append(openHelper.getDatabaseName());
                            local2.d(str2, sb.toString());
                            if (!((so2) ro2).b()) {
                                activitiesRepository.mActivitySampleDao.upsertListActivitySample(arrayList);
                            }
                            if (((ApiResponse) ((so2) ro2).a()).get_range() == null) {
                                return ro2;
                            }
                            Range range = ((ApiResponse) ((so2) ro2).a()).get_range();
                            if (range == null) {
                                wd4.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return ro2;
                            } else {
                                activitiesRepository$fetchActivitySamples$Anon12.L$Anon0 = activitiesRepository;
                                activitiesRepository$fetchActivitySamples$Anon12.L$Anon1 = date4;
                                activitiesRepository$fetchActivitySamples$Anon12.L$Anon2 = date3;
                                activitiesRepository$fetchActivitySamples$Anon12.I$Anon0 = i5;
                                activitiesRepository$fetchActivitySamples$Anon12.I$Anon1 = i4;
                                activitiesRepository$fetchActivitySamples$Anon12.L$Anon3 = ro2;
                                activitiesRepository$fetchActivitySamples$Anon12.L$Anon4 = arrayList;
                                activitiesRepository$fetchActivitySamples$Anon12.label = 2;
                                obj = activitiesRepository.fetchActivitySamples(date4, date3, i5 + i4, i4, activitiesRepository$fetchActivitySamples$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                ro22 = ro2;
                                return (ro2) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("fetchActivitySamples exception=");
                            e.printStackTrace();
                            sb2.append(cb4.a);
                            local3.d(str3, sb2.toString());
                            return ro2;
                        }
                    }
                    return ro2;
                }
                String str4 = null;
                if (ro2 instanceof qo2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("fetchActivitySamples Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb3.append(qo2.a());
                    sb3.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str4 = message;
                            if (str4 == null) {
                                str4 = "";
                            }
                            sb3.append(str4);
                            local4.d(str5, sb3.toString());
                        }
                    }
                    ServerError c2 = qo2.c();
                    if (c2 != null) {
                        str4 = c2.getUserMessage();
                    }
                    if (str4 == null) {
                    }
                    sb3.append(str4);
                    local4.d(str5, sb3.toString());
                }
                return ro2;
            }
        }
        activitiesRepository$fetchActivitySamples$Anon1 = new ActivitiesRepository$fetchActivitySamples$Anon1(this, kc42);
        ActivitiesRepository$fetchActivitySamples$Anon1 activitiesRepository$fetchActivitySamples$Anon122 = activitiesRepository$fetchActivitySamples$Anon1;
        Object obj22 = activitiesRepository$fetchActivitySamples$Anon122.result;
        Object a3 = oc4.a();
        i3 = activitiesRepository$fetchActivitySamples$Anon122.label;
        if (i3 != 0) {
        }
        ro2 = (ro2) obj22;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final LiveData<ps3<List<ActivitySample>>> getActivityList(Date date, Date date2, boolean z) {
        wd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(date2, "end");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getActivityList: start = " + date + ", end = " + date2 + " shouldFetch=" + z);
        Date n = sk2.n(date);
        Date i = sk2.i(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        wd4.a((Object) n, GoalPhase.COLUMN_START_DATE);
        wd4.a((Object) i, GoalPhase.COLUMN_END_DATE);
        LiveData<ps3<List<ActivitySample>>> b = ic.b(fitnessDataDao.getFitnessDataLiveData(n, i), new ActivitiesRepository$getActivityList$Anon1(this, n, i, z, date2));
        wd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final List<SampleRaw> getActivityListByUAPinType(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getActivityListByUAPinType: uaPinType = " + i);
        return this.mSampleRawDao.getListActivitySampleByUaType(i);
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> getActivitySamplesInDate$app_fossilRelease(Date date) {
        wd4.b(date, "date");
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "calendarStart");
        instance.setTime(date);
        sk2.d(instance);
        Object clone = instance.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            sk2.a(calendar);
            wd4.a((Object) calendar, "DateHelper.getEndOfDay(calendarEnd)");
            ActivitySampleDao activitySampleDao = this.mActivitySampleDao;
            Date time = instance.getTime();
            wd4.a((Object) time, "calendarStart.time");
            Date time2 = calendar.getTime();
            wd4.a((Object) time2, "calendarEnd.time");
            return activitySampleDao.getActivitySamplesLiveData(time, time2);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final List<SampleRaw> getPendingActivities(Date date, Date date2) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "end");
        instance2.setTime(date2);
        SampleRawDao sampleRawDao = this.mSampleRawDao;
        Date n = sk2.n(instance.getTime());
        wd4.a((Object) n, "DateHelper.getStartOfDay(start.time)");
        Date i = sk2.i(instance2.getTime());
        wd4.a((Object) i, "DateHelper.getEndOfDay(end.time)");
        return sampleRawDao.getPendingActivitySamples(n, i);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert(List<SampleRaw> list, kc4<? super ro2<List<ActivitySample>>> kc4) {
        ActivitiesRepository$insert$Anon1 activitiesRepository$insert$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof ActivitiesRepository$insert$Anon1) {
            activitiesRepository$insert$Anon1 = (ActivitiesRepository$insert$Anon1) kc4;
            int i2 = activitiesRepository$insert$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = activitiesRepository$insert$Anon1.result;
                Object a = oc4.a();
                i = activitiesRepository$insert$Anon1.label;
                String str = null;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "insertActivities: sampleRawList =" + list.size());
                    MFUser currentUser = this.mUserRepository.getCurrentUser();
                    String userId = currentUser != null ? currentUser.getUserId() : null;
                    if (TextUtils.isEmpty(userId)) {
                        return new qo2(600, new ServerError(600, ""), (Throwable) null, (String) null, 8, (rd4) null);
                    }
                    uz1 uz1 = new uz1();
                    Gson gsonConverter = Activity.Companion.gsonConverter();
                    for (SampleRaw next : list) {
                        Activity.Companion companion = Activity.Companion;
                        if (userId != null) {
                            Activity activity = companion.toActivity(userId, next.toActivitySample());
                            JsonElement b = gsonConverter.b((Object) activity);
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.d(str3, "activity " + activity + " json " + b);
                            wd4.a((Object) b, "jsonTree");
                            if (!b.h()) {
                                uz1.a(b);
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    yz1 yz1 = new yz1();
                    yz1.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) uz1);
                    ActivitiesRepository$insert$repoResponse$Anon1 activitiesRepository$insert$repoResponse$Anon1 = new ActivitiesRepository$insert$repoResponse$Anon1(this, yz1, (kc4) null);
                    activitiesRepository$insert$Anon1.L$Anon0 = this;
                    activitiesRepository$insert$Anon1.L$Anon1 = list;
                    activitiesRepository$insert$Anon1.L$Anon2 = userId;
                    activitiesRepository$insert$Anon1.L$Anon3 = uz1;
                    activitiesRepository$insert$Anon1.L$Anon4 = gsonConverter;
                    activitiesRepository$insert$Anon1.L$Anon5 = yz1;
                    activitiesRepository$insert$Anon1.label = 1;
                    obj = ResponseKt.a(activitiesRepository$insert$repoResponse$Anon1, activitiesRepository$insert$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yz1 yz12 = (yz1) activitiesRepository$insert$Anon1.L$Anon5;
                    Gson gson = (Gson) activitiesRepository$insert$Anon1.L$Anon4;
                    uz1 uz12 = (uz1) activitiesRepository$insert$Anon1.L$Anon3;
                    String str4 = (String) activitiesRepository$insert$Anon1.L$Anon2;
                    List list2 = (List) activitiesRepository$insert$Anon1.L$Anon1;
                    ActivitiesRepository activitiesRepository = (ActivitiesRepository) activitiesRepository$insert$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    local3.d(str5, "insertActivity onResponse: response = " + ro2);
                    ArrayList arrayList = new ArrayList();
                    ApiResponse apiResponse = (ApiResponse) ((so2) ro2).a();
                    if (apiResponse != null) {
                        List<Activity> list3 = apiResponse.get_items();
                        if (list3 != null) {
                            for (Activity activitySample : list3) {
                                arrayList.add(activitySample.toActivitySample());
                            }
                        }
                    }
                    return new so2(arrayList, false, 2, (rd4) null);
                } else if (ro2 instanceof qo2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str6 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("insertActivity Failure code=");
                    qo2 qo2 = (qo2) ro2;
                    sb.append(qo2.a());
                    sb.append(" message=");
                    ServerError c = qo2.c();
                    if (c != null) {
                        str = c.getMessage();
                    }
                    sb.append(str);
                    local4.d(str6, sb.toString());
                    return new qo2(qo2.a(), qo2.c(), qo2.d(), qo2.b());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        activitiesRepository$insert$Anon1 = new ActivitiesRepository$insert$Anon1(this, kc4);
        Object obj2 = activitiesRepository$insert$Anon1.result;
        Object a2 = oc4.a();
        i = activitiesRepository$insert$Anon1.label;
        String str7 = null;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }

    @DexIgnore
    public final void insertFromDevice(List<ActivitySample> list) {
        wd4.b(list, "activityList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: activityList = " + list.size());
        this.mActivitySampleDao.insertActivitySamples(list);
    }

    @DexIgnore
    public final Object pushPendingActivities(PushPendingActivitiesCallback pushPendingActivitiesCallback, kc4<? super cb4> kc4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingActivities fitnessDb=" + this.mFitnessDatabase);
        List<SampleRaw> pendingActivitySamples = this.mSampleRawDao.getPendingActivitySamples();
        if (pendingActivitySamples.size() > 0) {
            return saveActivitiesToServer(pendingActivitySamples, pushPendingActivitiesCallback, kc4);
        }
        if (pushPendingActivitiesCallback != null) {
            pushPendingActivitiesCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
        }
        return cb4.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a0, code lost:
        if (r13.intValue() != 409000) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01b6, code lost:
        if (r7.intValue() != 409001) goto L_0x01b8;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x021d A[LOOP:1: B:69:0x0217->B:71:0x021d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final /* synthetic */ Object saveActivitiesToServer(List<SampleRaw> list, PushPendingActivitiesCallback pushPendingActivitiesCallback, kc4<? super cb4> kc4) {
        ActivitiesRepository activitiesRepository;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon1;
        int i;
        int i2;
        Object obj;
        Object obj2;
        List<SampleRaw> list2;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon12;
        int i3;
        List list3;
        int i4;
        List<SampleRaw> list4;
        PushPendingActivitiesCallback pushPendingActivitiesCallback2;
        ActivitiesRepository activitiesRepository2;
        ro2 ro2;
        ActivitiesRepository$saveActivitiesToServer$Anon1 activitiesRepository$saveActivitiesToServer$Anon13;
        List<SampleRaw> list5;
        Object obj3;
        ArrayList arrayList;
        kc4<? super cb4> kc42 = kc4;
        if (kc42 instanceof ActivitiesRepository$saveActivitiesToServer$Anon1) {
            activitiesRepository$saveActivitiesToServer$Anon1 = (ActivitiesRepository$saveActivitiesToServer$Anon1) kc42;
            int i5 = activitiesRepository$saveActivitiesToServer$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                activitiesRepository$saveActivitiesToServer$Anon1.label = i5 - Integer.MIN_VALUE;
                activitiesRepository = this;
                Object obj4 = activitiesRepository$saveActivitiesToServer$Anon1.result;
                Object a = oc4.a();
                i = activitiesRepository$saveActivitiesToServer$Anon1.label;
                i2 = 1;
                if (i != 0) {
                    za4.a(obj4);
                    list3 = new ArrayList();
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon1;
                    activitiesRepository2 = activitiesRepository;
                    obj3 = a;
                    i3 = 0;
                    list5 = list;
                    pushPendingActivitiesCallback2 = pushPendingActivitiesCallback;
                } else if (i == 1) {
                    list4 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$Anon4;
                    i4 = activitiesRepository$saveActivitiesToServer$Anon1.I$Anon1;
                    list3 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$Anon3;
                    i3 = activitiesRepository$saveActivitiesToServer$Anon1.I$Anon0;
                    list2 = (List) activitiesRepository$saveActivitiesToServer$Anon1.L$Anon1;
                    za4.a(obj4);
                    obj = a;
                    pushPendingActivitiesCallback2 = (PushPendingActivitiesCallback) activitiesRepository$saveActivitiesToServer$Anon1.L$Anon2;
                    activitiesRepository$saveActivitiesToServer$Anon12 = activitiesRepository$saveActivitiesToServer$Anon1;
                    activitiesRepository2 = (ActivitiesRepository) activitiesRepository$saveActivitiesToServer$Anon1.L$Anon0;
                    obj2 = obj4;
                    ro2 = (ro2) obj2;
                    i3 += 100;
                    if (ro2 instanceof so2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "saveActivitiesToServer success, bravo!!! startIndex=" + i3 + " endIndex=" + i4);
                        Object a2 = ((so2) ro2).a();
                        if (a2 != null) {
                            activitiesRepository2.updateActivityPinType(list4, 0);
                            list3.addAll((List) a2);
                            if (i3 >= list2.size()) {
                                if (pushPendingActivitiesCallback2 != null) {
                                    pushPendingActivitiesCallback2.onSuccess(list3);
                                }
                                return cb4.a;
                            }
                        }
                        wd4.a();
                        throw null;
                    } else if (ro2 instanceof qo2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("saveActivitiesToServer failed, errorCode=");
                        qo2 qo2 = (qo2) ro2;
                        sb.append(qo2.a());
                        sb.append(' ');
                        sb.append("startIndex=");
                        sb.append(i3);
                        sb.append(" endIndex=");
                        sb.append(i4);
                        local2.d(str2, sb.toString());
                        if (qo2.a() == 422) {
                            arrayList = new ArrayList();
                            if (!TextUtils.isEmpty(qo2.b())) {
                                try {
                                } catch (Exception e) {
                                    e = e;
                                }
                                Object a3 = new Gson().a(((qo2) ro2).b(), new ActivitiesRepository$saveActivitiesToServer$type$Anon1().getType());
                                wd4.a(a3, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list6 = ((UpsertApiResponse) a3).get_items();
                                if (!list6.isEmpty()) {
                                    int size = list6.size();
                                    int i6 = 0;
                                    while (i6 < size) {
                                        Integer code = ((Activity) list6.get(i6)).getCode();
                                        if (code == null) {
                                        }
                                        Integer code2 = ((Activity) list6.get(i6)).getCode();
                                        if (code2 == null) {
                                        }
                                        if (!TextUtils.isEmpty(((Activity) list6.get(i6)).getId())) {
                                            SampleRaw sampleRaw = list4.get(i6);
                                            try {
                                                sampleRaw.setPinType(0);
                                                arrayList.add(sampleRaw);
                                            } catch (Exception e2) {
                                                e = e2;
                                            }
                                            i6++;
                                        } else {
                                            i6++;
                                        }
                                    }
                                }
                                activitiesRepository2.mSampleRawDao.upsertListActivitySample(arrayList);
                                ArrayList arrayList2 = new ArrayList(pb4.a(list4, 10));
                                for (SampleRaw activitySample : list4) {
                                    arrayList2.add(activitySample.toActivitySample());
                                }
                                list3.addAll(arrayList2);
                                if (i3 >= list2.size()) {
                                    if (pushPendingActivitiesCallback2 != null) {
                                        pushPendingActivitiesCallback2.onSuccess(list3);
                                    }
                                    return cb4.a;
                                }
                            }
                        }
                        if (i3 >= list2.size()) {
                            if (pushPendingActivitiesCallback2 != null) {
                                pushPendingActivitiesCallback2.onFail(qo2.a());
                            }
                            return cb4.a;
                        }
                    }
                    i2 = 1;
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
                    list5 = list2;
                    obj3 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i3 < list5.size()) {
                    int i7 = i3 + 100;
                    if (i7 > list5.size()) {
                        i7 = list5.size();
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "saveActivitiesToServer startIndex=" + i3 + " endIndex=" + i7);
                    List<SampleRaw> subList = list5.subList(i3, i7);
                    activitiesRepository$saveActivitiesToServer$Anon13.L$Anon0 = activitiesRepository2;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$Anon1 = list5;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$Anon2 = pushPendingActivitiesCallback2;
                    activitiesRepository$saveActivitiesToServer$Anon13.I$Anon0 = i3;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$Anon3 = list3;
                    activitiesRepository$saveActivitiesToServer$Anon13.I$Anon1 = i7;
                    activitiesRepository$saveActivitiesToServer$Anon13.L$Anon4 = subList;
                    activitiesRepository$saveActivitiesToServer$Anon13.label = i2;
                    obj2 = activitiesRepository2.insert(subList, activitiesRepository$saveActivitiesToServer$Anon13);
                    if (obj2 == obj3) {
                        return obj3;
                    }
                    obj = obj3;
                    i4 = i7;
                    activitiesRepository$saveActivitiesToServer$Anon12 = activitiesRepository$saveActivitiesToServer$Anon13;
                    list4 = subList;
                    list2 = list5;
                    ro2 = (ro2) obj2;
                    i3 += 100;
                    if (ro2 instanceof so2) {
                    }
                    i2 = 1;
                    activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
                    list5 = list2;
                    obj3 = obj;
                    if (i3 < list5.size()) {
                    }
                    return obj3;
                }
                return cb4.a;
            }
        }
        activitiesRepository = this;
        activitiesRepository$saveActivitiesToServer$Anon1 = new ActivitiesRepository$saveActivitiesToServer$Anon1(activitiesRepository, kc42);
        Object obj42 = activitiesRepository$saveActivitiesToServer$Anon1.result;
        Object a4 = oc4.a();
        i = activitiesRepository$saveActivitiesToServer$Anon1.label;
        i2 = 1;
        if (i != 0) {
        }
        if (i3 < list5.size()) {
        }
        return cb4.a;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("saveActivitiesToServer ex=");
        e.printStackTrace();
        sb2.append(cb4.a);
        local4.d(str4, sb2.toString());
        activitiesRepository2.mSampleRawDao.upsertListActivitySample(arrayList);
        ArrayList arrayList22 = new ArrayList(pb4.a(list4, 10));
        while (r2.hasNext()) {
        }
        list3.addAll(arrayList22);
        if (i3 >= list2.size()) {
        }
        if (i3 >= list2.size()) {
        }
        i2 = 1;
        activitiesRepository$saveActivitiesToServer$Anon13 = activitiesRepository$saveActivitiesToServer$Anon12;
        list5 = list2;
        obj3 = obj;
        if (i3 < list5.size()) {
        }
        return cb4.a;
    }

    @DexIgnore
    public final void updateActivityPinType(List<SampleRaw> list, int i) {
        wd4.b(list, "activitySampleList");
        for (SampleRaw pinType : list) {
            pinType.setPinType(i);
        }
        this.mSampleRawDao.upsertListActivitySample(list);
    }

    @DexIgnore
    public final void updateActivityUAPinType(List<SampleRaw> list, int i) {
        wd4.b(list, "activityList");
        for (SampleRaw pinType : list) {
            pinType.setPinType(i);
        }
        this.mSampleRawDao.upsertListActivitySample(list);
    }
}
