package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `goalTrackingRaw`");
            hgVar.b("DROP TABLE IF EXISTS `goalTrackingDay`");
            hgVar.b("DROP TABLE IF EXISTS `goalSetting`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = GoalTrackingDatabase_Impl.this.mDatabase = hgVar;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
            hashMap.put("id", new fg.a("id", "TEXT", true, 1));
            hashMap.put(GoalTrackingEvent.COLUMN_TRACKED_AT, new fg.a(GoalTrackingEvent.COLUMN_TRACKED_AT, "TEXT", true, 0));
            hashMap.put("timezoneOffsetInSecond", new fg.a("timezoneOffsetInSecond", "INTEGER", true, 0));
            hashMap.put("date", new fg.a("date", "TEXT", true, 0));
            hashMap.put("createdAt", new fg.a("createdAt", "INTEGER", true, 0));
            hashMap.put("updatedAt", new fg.a("updatedAt", "INTEGER", true, 0));
            fg fgVar = new fg("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, "goalTrackingRaw");
            if (fgVar.equals(a)) {
                HashMap hashMap2 = new HashMap(6);
                hashMap2.put("pinType", new fg.a("pinType", "INTEGER", true, 0));
                hashMap2.put("date", new fg.a("date", "TEXT", true, 1));
                hashMap2.put("totalTracked", new fg.a("totalTracked", "INTEGER", true, 0));
                hashMap2.put("goalTarget", new fg.a("goalTarget", "INTEGER", true, 0));
                hashMap2.put("createdAt", new fg.a("createdAt", "INTEGER", true, 0));
                hashMap2.put("updatedAt", new fg.a("updatedAt", "INTEGER", true, 0));
                fg fgVar2 = new fg("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
                fg a2 = fg.a(hgVar, "goalTrackingDay");
                if (fgVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(2);
                    hashMap3.put("id", new fg.a("id", "INTEGER", true, 1));
                    hashMap3.put("currentTarget", new fg.a("currentTarget", "INTEGER", true, 0));
                    fg fgVar3 = new fg("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
                    fg a3 = fg.a(hgVar, "goalSetting");
                    if (!fgVar3.equals(a3)) {
                        throw new IllegalStateException("Migration didn't properly handle goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + fgVar3 + "\n Found:\n" + a3);
                    }
                    return;
                }
                throw new IllegalStateException("Migration didn't properly handle goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + fgVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + fgVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `goalTrackingRaw`");
            a.b("DELETE FROM `goalTrackingDay`");
            a.b("DELETE FROM `goalSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "goalTrackingRaw", "goalTrackingDay", "goalSetting");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
