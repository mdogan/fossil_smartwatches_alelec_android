package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.lg;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.vf;
import com.fossil.blesdk.obfuscated.xf;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InactivityNudgeTimeDao_Impl implements InactivityNudgeTimeDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ mf __insertionAdapterOfInactivityNudgeTimeModel;
    @DexIgnore
    public /* final */ xf __preparedStmtOfDelete;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends mf<InactivityNudgeTimeModel> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inactivityNudgeTimeModel`(`nudgeTimeName`,`minutes`,`nudgeTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(lg lgVar, InactivityNudgeTimeModel inactivityNudgeTimeModel) {
            if (inactivityNudgeTimeModel.getNudgeTimeName() == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, inactivityNudgeTimeModel.getNudgeTimeName());
            }
            lgVar.b(2, (long) inactivityNudgeTimeModel.getMinutes());
            lgVar.b(3, (long) inactivityNudgeTimeModel.getNudgeTimeType());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends xf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM inactivityNudgeTimeModel";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon3(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<InactivityNudgeTimeModel> call() throws Exception {
            Cursor a = cg.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = bg.b(a, "nudgeTimeName");
                int b2 = bg.b(a, "minutes");
                int b3 = bg.b(a, "nudgeTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InactivityNudgeTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<InactivityNudgeTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ vf val$_statement;

        @DexIgnore
        public Anon4(vf vfVar) {
            this.val$_statement = vfVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public InactivityNudgeTimeModel call() throws Exception {
            Cursor a = cg.a(InactivityNudgeTimeDao_Impl.this.__db, this.val$_statement, false);
            try {
                return a.moveToFirst() ? new InactivityNudgeTimeModel(a.getString(bg.b(a, "nudgeTimeName")), a.getInt(bg.b(a, "minutes")), a.getInt(bg.b(a, "nudgeTimeType"))) : null;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InactivityNudgeTimeDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfInactivityNudgeTimeModel = new Anon1(roomDatabase);
        this.__preparedStmtOfDelete = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        lg acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public InactivityNudgeTimeModel getInactivityNudgeTimeModelWithFieldNudgeTimeType(int i) {
        vf b = vf.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.b(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new InactivityNudgeTimeModel(a.getString(bg.b(a, "nudgeTimeName")), a.getInt(bg.b(a, "minutes")), a.getInt(bg.b(a, "nudgeTimeType"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<InactivityNudgeTimeModel> getInactivityNudgeTimeWithFieldNudgeTimeType(int i) {
        vf b = vf.b("SELECT * FROM inactivityNudgeTimeModel WHERE nudgeTimeType = ?", 1);
        b.b(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<InactivityNudgeTimeModel>> getListInactivityNudgeTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"inactivityNudgeTimeModel"}, false, new Anon3(vf.b("SELECT * FROM inactivityNudgeTimeModel", 0)));
    }

    @DexIgnore
    public List<InactivityNudgeTimeModel> getListInactivityNudgeTimeModel() {
        vf b = vf.b("SELECT * FROM inactivityNudgeTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = cg.a(this.__db, b, false);
        try {
            int b2 = bg.b(a, "nudgeTimeName");
            int b3 = bg.b(a, "minutes");
            int b4 = bg.b(a, "nudgeTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new InactivityNudgeTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertInactivityNudgeTime(InactivityNudgeTimeModel inactivityNudgeTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(inactivityNudgeTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListInactivityNudgeTime(List<InactivityNudgeTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInactivityNudgeTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
