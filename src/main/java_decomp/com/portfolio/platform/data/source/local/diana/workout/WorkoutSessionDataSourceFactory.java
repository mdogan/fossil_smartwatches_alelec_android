package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionDataSourceFactory extends md.b<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ i42 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ FitnessDataDao fitnessDataDao;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutDao workoutDao;
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDataDao fitnessDataDao2, WorkoutDao workoutDao2, FitnessDatabase fitnessDatabase, Date date, i42 i42, PagingRequestHelper.a aVar) {
        wd4.b(workoutSessionRepository2, "workoutSessionRepository");
        wd4.b(fitnessDataDao2, "fitnessDataDao");
        wd4.b(workoutDao2, "workoutDao");
        wd4.b(fitnessDatabase, "workoutDatabase");
        wd4.b(date, "currentDate");
        wd4.b(i42, "appExecutors");
        wd4.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.fitnessDataDao = fitnessDataDao2;
        this.workoutDao = workoutDao2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = i42;
        this.listener = aVar;
    }

    @DexIgnore
    public md<Long, WorkoutSession> create() {
        this.localDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.fitnessDataDao, this.workoutDao, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.sourceLiveData.a(this.localDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.localDataSource;
        if (workoutSessionLocalDataSource != null) {
            return workoutSessionLocalDataSource;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
