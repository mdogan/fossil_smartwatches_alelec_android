package com.portfolio.platform.data.source.local.fitness;

import android.database.sqlite.SQLiteConstraintException;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ActivitySummaryDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryDao.class.getSimpleName();
        wd4.a((Object) simpleName, "ActivitySummaryDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSummary(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        List list;
        ActivitySummary activitySummary3 = activitySummary2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummary - currentSummary=" + activitySummary + ", newSummary=" + activitySummary3);
        double steps = activitySummary2.getSteps() + activitySummary.getSteps();
        double calories = activitySummary2.getCalories() + activitySummary.getCalories();
        double distance = activitySummary2.getDistance() + activitySummary.getDistance();
        int activeTime = activitySummary2.getActiveTime() + activitySummary.getActiveTime();
        List<Integer> intensities = activitySummary2.getIntensities();
        List<Integer> intensities2 = activitySummary.getIntensities();
        if (intensities.size() > intensities2.size()) {
            ArrayList arrayList = new ArrayList(pb4.a(intensities, 10));
            int i = 0;
            for (T next : intensities) {
                int i2 = i + 1;
                if (i >= 0) {
                    arrayList.add(Integer.valueOf(((Number) next).intValue() + (i < intensities2.size() ? intensities2.get(i).intValue() : 0)));
                    i = i2;
                } else {
                    ob4.c();
                    throw null;
                }
            }
            list = wb4.d(arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(pb4.a(intensities2, 10));
            int i3 = 0;
            for (T next2 : intensities2) {
                int i4 = i3 + 1;
                if (i3 >= 0) {
                    arrayList2.add(Integer.valueOf(((Number) next2).intValue() + (i3 < intensities.size() ? intensities.get(i3).intValue() : 0)));
                    i3 = i4;
                } else {
                    ob4.c();
                    throw null;
                }
            }
            list = wb4.d(arrayList2);
        }
        activitySummary3.setSteps(steps);
        activitySummary3.setCalories(calories);
        activitySummary3.setDistance(distance);
        activitySummary3.setActiveTime(activeTime);
        activitySummary3.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary3.setIntensities(list);
        activitySummary3.setStepGoal(activitySummary.getStepGoal());
        activitySummary3.setCaloriesGoal(activitySummary.getCaloriesGoal());
        activitySummary3.setActiveTimeGoal(activitySummary.getActiveTimeGoal());
        if (activitySummary.getSteps() != activitySummary2.getSteps()) {
            activitySummary3.setUpdatedAt(new DateTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySummaries();

    @DexIgnore
    public abstract ActivitySettings getActivitySetting();

    @DexIgnore
    public abstract LiveData<ActivitySettings> getActivitySettingLiveData();

    @DexIgnore
    public abstract ActivityStatistic getActivityStatistic();

    @DexIgnore
    public abstract LiveData<ActivityStatistic> getActivityStatisticLiveData();

    @DexIgnore
    public abstract List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final List<ActivitySummary> getActivitySummariesDesc(Date date, Date date2) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesDesc(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final LiveData<List<ActivitySummary>> getActivitySummariesLiveData(Date date, Date date2) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesLiveData(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract ActivitySummary getActivitySummary(int i, int i2, int i3);

    @DexIgnore
    public final ActivitySummary getActivitySummary(Date date) {
        wd4.b(date, "date");
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "dateCalendar");
        instance.setTime(date);
        return getActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    @DexIgnore
    public abstract LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3);

    @DexIgnore
    public final Date getLastDate() {
        Calendar instance = Calendar.getInstance();
        ActivitySummary lastSummary = getLastSummary();
        if (lastSummary == null) {
            return null;
        }
        instance.set(lastSummary.getYear(), lastSummary.getMonth() - 1, lastSummary.getDay());
        wd4.a((Object) instance, "lastDate");
        return instance.getTime();
    }

    @DexIgnore
    public abstract ActivitySummary getLastSummary();

    @DexIgnore
    public abstract ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3);

    @DexIgnore
    public abstract ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(Date date, Date date2) {
        wd4.b(date, GoalPhase.COLUMN_START_DATE);
        wd4.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        wd4.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getTotalValuesOfWeek(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract long insertActivitySettings(ActivitySettings activitySettings);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d2  */
    public final void insertActivitySummaries(List<ActivitySummary> list) {
        Integer num;
        Integer num2;
        int currentStepGoal;
        wd4.b(list, "summaries");
        for (ActivitySummary activitySummary : list) {
            FLogger.INSTANCE.getLocal().d(TAG, "addActivitySummary summary=" + activitySummary);
            ActivitySummary activitySummary2 = getActivitySummary(activitySummary.getYear(), activitySummary.getMonth(), activitySummary.getDay());
            if (activitySummary2 != null) {
                calculateSummary(activitySummary2, activitySummary);
            } else {
                Calendar instance = Calendar.getInstance();
                instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
                ActivitySummary nearestSampleDayFromDate = getNearestSampleDayFromDate(instance.get(1), instance.get(2), instance.get(5));
                ActivitySettings activitySetting = getActivitySetting();
                Integer num3 = null;
                if (nearestSampleDayFromDate != null) {
                    currentStepGoal = nearestSampleDayFromDate.getStepGoal();
                } else if (activitySetting != null) {
                    currentStepGoal = activitySetting.getCurrentStepGoal();
                } else {
                    num = null;
                    activitySummary.setStepGoal(num == null ? num.intValue() : VideoUploader.RETRY_DELAY_UNIT_MS);
                    if (nearestSampleDayFromDate == null) {
                        num2 = Integer.valueOf(nearestSampleDayFromDate.getActiveTimeGoal());
                    } else {
                        num2 = activitySetting != null ? Integer.valueOf(activitySetting.getCurrentActiveTimeGoal()) : null;
                    }
                    activitySummary.setActiveTimeGoal(num2 == null ? num2.intValue() : 30);
                    if (nearestSampleDayFromDate == null) {
                        num3 = Integer.valueOf(nearestSampleDayFromDate.getCaloriesGoal());
                    } else if (activitySetting != null) {
                        num3 = Integer.valueOf(activitySetting.getCurrentCaloriesGoal());
                    }
                    activitySummary.setCaloriesGoal(num3 == null ? num3.intValue() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
                    activitySummary.setCreatedAt(new DateTime());
                    activitySummary.setUpdatedAt(new DateTime());
                }
                num = Integer.valueOf(currentStepGoal);
                activitySummary.setStepGoal(num == null ? num.intValue() : VideoUploader.RETRY_DELAY_UNIT_MS);
                if (nearestSampleDayFromDate == null) {
                }
                activitySummary.setActiveTimeGoal(num2 == null ? num2.intValue() : 30);
                if (nearestSampleDayFromDate == null) {
                }
                activitySummary.setCaloriesGoal(num3 == null ? num3.intValue() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
                activitySummary.setCreatedAt(new DateTime());
                activitySummary.setUpdatedAt(new DateTime());
            }
            FLogger.INSTANCE.getLocal().d(TAG, "updateSampleDay - after calculate summary=" + activitySummary);
        }
        upsertActivitySummaries(list);
    }

    @DexIgnore
    public abstract void updateActivitySettings(int i, int i2, int i3);

    @DexIgnore
    public abstract void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals);

    @DexIgnore
    public final void upsertActivitySettings(ActivitySettings activitySettings) {
        wd4.b(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "upsertActivitySettings stepGoal=" + activitySettings.getCurrentStepGoal() + " caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + " activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        try {
            insertActivitySettings(activitySettings);
        } catch (SQLiteConstraintException e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "upsertActivitySettings exception " + e);
            updateActivitySettings(activitySettings.getCurrentStepGoal(), activitySettings.getCurrentCaloriesGoal(), activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexIgnore
    public abstract long upsertActivityStatistic(ActivityStatistic activityStatistic);

    @DexIgnore
    public abstract void upsertActivitySummaries(List<ActivitySummary> list);

    @DexIgnore
    public abstract void upsertActivitySummary(ActivitySummary activitySummary);
}
