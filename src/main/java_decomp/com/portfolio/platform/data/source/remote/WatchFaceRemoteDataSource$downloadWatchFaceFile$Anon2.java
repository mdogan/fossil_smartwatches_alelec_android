package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qm4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2", f = "WatchFaceRemoteDataSource.kt", l = {33}, m = "invokeSuspend")
public final class WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2 extends SuspendLambda implements jd4<kc4<? super cs4<qm4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(WatchFaceRemoteDataSource watchFaceRemoteDataSource, String str, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = watchFaceRemoteDataSource;
        this.$url = str;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this.this$Anon0, this.$url, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 access$getApi$p = this.this$Anon0.api;
            String str = this.$url;
            this.label = 1;
            obj = access$getApi$p.downloadFile(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
