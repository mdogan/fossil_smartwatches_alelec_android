package com.portfolio.platform.data.source.local.dnd;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DNDSettingsDatabase_Impl extends DNDSettingsDatabase {
    @DexIgnore
    public volatile DNDScheduledTimeDao _dNDScheduledTimeDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `dndScheduledTimeModel` (`scheduledTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `scheduledTimeType` INTEGER NOT NULL, PRIMARY KEY(`scheduledTimeName`))");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'edbea9a651a6427903efe74c61fd6b87')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `dndScheduledTimeModel`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = DNDSettingsDatabase_Impl.this.mDatabase = hgVar;
            DNDSettingsDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("scheduledTimeName", new fg.a("scheduledTimeName", "TEXT", true, 1));
            hashMap.put("minutes", new fg.a("minutes", "INTEGER", true, 0));
            hashMap.put("scheduledTimeType", new fg.a("scheduledTimeType", "INTEGER", true, 0));
            fg fgVar = new fg("dndScheduledTimeModel", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, "dndScheduledTimeModel");
            if (!fgVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle dndScheduledTimeModel(com.portfolio.platform.data.model.DNDScheduledTimeModel).\n Expected:\n" + fgVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `dndScheduledTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "dndScheduledTimeModel");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(1), "edbea9a651a6427903efe74c61fd6b87", "1a7527e5c35fa2c8a6aef70719b78d76");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public DNDScheduledTimeDao getDNDScheduledTimeDao() {
        DNDScheduledTimeDao dNDScheduledTimeDao;
        if (this._dNDScheduledTimeDao != null) {
            return this._dNDScheduledTimeDao;
        }
        synchronized (this) {
            if (this._dNDScheduledTimeDao == null) {
                this._dNDScheduledTimeDao = new DNDScheduledTimeDao_Impl(this);
            }
            dNDScheduledTimeDao = this._dNDScheduledTimeDao;
        }
        return dNDScheduledTimeDao;
    }
}
