package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutLocation;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import com.portfolio.platform.enums.WorkoutSourceType;
import com.portfolio.platform.enums.WorkoutType;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerWorkoutSession extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    @g02("location")
    public WorkoutLocation location;
    @DexIgnore
    @g02("calorie")
    public /* final */ WorkoutCalorie mCalorie;
    @DexIgnore
    @g02("createdAt")
    public /* final */ DateTime mCreatedAt;
    @DexIgnore
    @g02("date")
    public /* final */ Date mDate;
    @DexIgnore
    @g02("deviceSerialNumber")
    public /* final */ String mDeviceSerialNumber;
    @DexIgnore
    @g02("distance")
    public /* final */ WorkoutDistance mDistance;
    @DexIgnore
    @g02("duration")
    public /* final */ int mDuration;
    @DexIgnore
    @g02("endTime")
    public /* final */ String mEndTime;
    @DexIgnore
    @g02("heartRate")
    public /* final */ WorkoutHeartRate mHeartRate;
    @DexIgnore
    @g02("id")
    public /* final */ String mId;
    @DexIgnore
    @g02("source")
    public /* final */ WorkoutSourceType mSourceType;
    @DexIgnore
    @g02("startTime")
    public /* final */ String mStartTime;
    @DexIgnore
    @g02("step")
    public /* final */ WorkoutStep mStep;
    @DexIgnore
    @g02("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @g02("updatedAt")
    public /* final */ DateTime mUpdatedAt;
    @DexIgnore
    @g02("type")
    public /* final */ WorkoutType mWorkoutType;
    @DexIgnore
    @g02("speed")
    public WorkoutSpeed speed;
    @DexIgnore
    @g02("states")
    public List<WorkoutStateChange> states;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerWorkoutSession.class.getSimpleName();
        wd4.a((Object) simpleName, "ServerWorkoutSession::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, WorkoutSourceType workoutSourceType, WorkoutStep workoutStep, int i, WorkoutType workoutType, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, WorkoutLocation workoutLocation, List<WorkoutStateChange> list) {
        String str5 = str4;
        DateTime dateTime3 = dateTime;
        DateTime dateTime4 = dateTime2;
        List<WorkoutStateChange> list2 = list;
        wd4.b(str, "mId");
        wd4.b(date, "mDate");
        wd4.b(str3, "mStartTime");
        wd4.b(str5, "mEndTime");
        wd4.b(dateTime3, "mCreatedAt");
        wd4.b(dateTime4, "mUpdatedAt");
        wd4.b(list2, "states");
        this.mId = str;
        this.mCalorie = workoutCalorie;
        this.mDate = date;
        this.mDeviceSerialNumber = str2;
        this.mDistance = workoutDistance;
        this.mStartTime = str3;
        this.mEndTime = str5;
        this.mHeartRate = workoutHeartRate;
        this.mSourceType = workoutSourceType;
        this.mStep = workoutStep;
        this.mTimeZoneOffsetInSecond = i;
        this.mWorkoutType = workoutType;
        this.mCreatedAt = dateTime3;
        this.mUpdatedAt = dateTime4;
        this.mDuration = i2;
        this.speed = workoutSpeed;
        this.location = workoutLocation;
        this.states = list2;
    }

    @DexIgnore
    public final WorkoutLocation getLocation() {
        return this.location;
    }

    @DexIgnore
    public final WorkoutCalorie getMCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMDeviceSerialNumber() {
        return this.mDeviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getMDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public final int getMDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public final String getMEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getMHeartRate() {
        return this.mHeartRate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final WorkoutSourceType getMSourceType() {
        return this.mSourceType;
    }

    @DexIgnore
    public final String getMStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public final WorkoutStep getMStep() {
        return this.mStep;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final WorkoutType getMWorkoutType() {
        return this.mWorkoutType;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final void setLocation(WorkoutLocation workoutLocation) {
        this.location = workoutLocation;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        wd4.b(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public final WorkoutSession toWorkoutSession() {
        WorkoutSession workoutSession;
        try {
            DateTimeFormatter withZone = ISODateTimeFormat.dateTime().withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            DateTime parseDateTime = withZone.parseDateTime(this.mStartTime);
            DateTime parseDateTime2 = withZone.parseDateTime(this.mEndTime);
            String str = this.mId;
            Date date = this.mDate;
            wd4.a((Object) parseDateTime, SampleRaw.COLUMN_START_TIME);
            wd4.a((Object) parseDateTime2, SampleRaw.COLUMN_END_TIME);
            workoutSession = new WorkoutSession(str, date, parseDateTime, parseDateTime2, this.mDeviceSerialNumber, this.mStep, this.mCalorie, this.mDistance, this.mHeartRate, this.mSourceType, this.mWorkoutType, this.mTimeZoneOffsetInSecond, this.mDuration, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
            try {
                workoutSession.setSpeed(this.speed);
                workoutSession.setLocation(this.location);
                workoutSession.getStates().addAll(this.states);
            } catch (ParseException e) {
                e = e;
            }
        } catch (ParseException e2) {
            e = e2;
            workoutSession = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            e.printStackTrace();
            return workoutSession;
        }
        return workoutSession;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, WorkoutSourceType workoutSourceType, WorkoutStep workoutStep, int i, WorkoutType workoutType, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, WorkoutLocation workoutLocation, List list, int i3, rd4 rd4) {
        this(str, workoutCalorie, date, str2, workoutDistance, str3, str4, workoutHeartRate, workoutSourceType, workoutStep, i, workoutType, dateTime, dateTime2, i2, workoutSpeed, workoutLocation, (i3 & 131072) != 0 ? new ArrayList() : list);
    }
}
