package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.e02;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemindTimeModel {
    @DexIgnore
    @e02
    public int minutes;
    @DexIgnore
    @e02
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        wd4.b(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        wd4.b(str, "<set-?>");
        this.remindTimeName = str;
    }
}
