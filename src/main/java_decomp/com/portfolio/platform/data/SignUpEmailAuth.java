package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpEmailAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    @g02("birthday")
    public String birthday;
    @DexIgnore
    @g02("clientId")
    public String clientId;
    @DexIgnore
    @g02("diagnosticEnabled")
    public boolean diagnosticEnabled;
    @DexIgnore
    @g02("email")
    public String email;
    @DexIgnore
    @g02("firstName")
    public String firstName;
    @DexIgnore
    @g02("gender")
    public String gender;
    @DexIgnore
    @g02("lastName")
    public String lastName;
    @DexIgnore
    @g02("password")
    public String password;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpEmailAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public SignUpEmailAuth createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new SignUpEmailAuth(parcel);
        }

        @DexIgnore
        public SignUpEmailAuth[] newArray(int i) {
            return new SignUpEmailAuth[i];
        }
    }

    @DexIgnore
    public SignUpEmailAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z) {
        wd4.b(str, "email");
        wd4.b(str2, "password");
        wd4.b(str3, "clientId");
        wd4.b(str4, "firstName");
        wd4.b(str5, "lastName");
        wd4.b(str6, "birthday");
        wd4.b(str7, "gender");
        this.email = str;
        this.password = str2;
        this.clientId = str3;
        this.firstName = str4;
        this.lastName = str5;
        this.birthday = str6;
        this.gender = str7;
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public static /* synthetic */ SignUpEmailAuth copy$default(SignUpEmailAuth signUpEmailAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z, int i, Object obj) {
        SignUpEmailAuth signUpEmailAuth2 = signUpEmailAuth;
        int i2 = i;
        return signUpEmailAuth.copy((i2 & 1) != 0 ? signUpEmailAuth2.email : str, (i2 & 2) != 0 ? signUpEmailAuth2.password : str2, (i2 & 4) != 0 ? signUpEmailAuth2.clientId : str3, (i2 & 8) != 0 ? signUpEmailAuth2.firstName : str4, (i2 & 16) != 0 ? signUpEmailAuth2.lastName : str5, (i2 & 32) != 0 ? signUpEmailAuth2.birthday : str6, (i2 & 64) != 0 ? signUpEmailAuth2.gender : str7, (i2 & 128) != 0 ? signUpEmailAuth2.diagnosticEnabled : z);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final String component2() {
        return this.password;
    }

    @DexIgnore
    public final String component3() {
        return this.clientId;
    }

    @DexIgnore
    public final String component4() {
        return this.firstName;
    }

    @DexIgnore
    public final String component5() {
        return this.lastName;
    }

    @DexIgnore
    public final String component6() {
        return this.birthday;
    }

    @DexIgnore
    public final String component7() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component8() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final SignUpEmailAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z) {
        wd4.b(str, "email");
        wd4.b(str2, "password");
        wd4.b(str3, "clientId");
        wd4.b(str4, "firstName");
        wd4.b(str5, "lastName");
        String str8 = str6;
        wd4.b(str8, "birthday");
        String str9 = str7;
        wd4.b(str9, "gender");
        return new SignUpEmailAuth(str, str2, str3, str4, str5, str8, str9, z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SignUpEmailAuth) {
                SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) obj;
                if (wd4.a((Object) this.email, (Object) signUpEmailAuth.email) && wd4.a((Object) this.password, (Object) signUpEmailAuth.password) && wd4.a((Object) this.clientId, (Object) signUpEmailAuth.clientId) && wd4.a((Object) this.firstName, (Object) signUpEmailAuth.firstName) && wd4.a((Object) this.lastName, (Object) signUpEmailAuth.lastName) && wd4.a((Object) this.birthday, (Object) signUpEmailAuth.birthday) && wd4.a((Object) this.gender, (Object) signUpEmailAuth.gender)) {
                    if (this.diagnosticEnabled == signUpEmailAuth.diagnosticEnabled) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getPassword() {
        return this.password;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.password;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.clientId;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.firstName;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.lastName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.birthday;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.gender;
        if (str7 != null) {
            i = str7.hashCode();
        }
        int i2 = (hashCode6 + i) * 31;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    @DexIgnore
    public final void setBirthday(String str) {
        wd4.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        wd4.b(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        wd4.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        wd4.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        wd4.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        wd4.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setPassword(String str) {
        wd4.b(str, "<set-?>");
        this.password = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpEmailAuth(email=" + this.email + ", password=" + this.password + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.password);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SignUpEmailAuth(Parcel parcel) {
        this(r3, r4, r5, r6, r7, r8, r9, parcel.readByte() != ((byte) 0));
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        String str7 = readString != null ? readString : "";
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        String readString4 = parcel.readString();
        if (readString4 != null) {
            str3 = readString4;
        } else {
            str3 = "";
        }
        String readString5 = parcel.readString();
        if (readString5 != null) {
            str4 = readString5;
        } else {
            str4 = "";
        }
        String readString6 = parcel.readString();
        if (readString6 != null) {
            str5 = readString6;
        } else {
            str5 = "";
        }
        String readString7 = parcel.readString();
        if (readString7 != null) {
            str6 = readString7;
        } else {
            str6 = "";
        }
    }

    @DexIgnore
    public SignUpEmailAuth() {
        this("", "", "", "", "", "", "", false);
    }
}
