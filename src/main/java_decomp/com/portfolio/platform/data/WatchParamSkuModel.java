package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.g02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParamSkuModel {
    @DexIgnore
    @g02("date")
    public DateResponse date;
    @DexIgnore
    @g02("deviceLongName")
    public String deviceLongName;
    @DexIgnore
    @g02("deviceShortName")
    public String deviceShortName;
    @DexIgnore
    @g02("enableAlarmAnimation")
    public boolean enableAlarmAnimation;
    @DexIgnore
    @g02("mainHandsFlipped")
    public boolean mainHandsFlipped;
    @DexIgnore
    @g02("progress")
    public Progress progress;
    @DexIgnore
    @g02("rawBase64")
    public String rawBase64;
    @DexIgnore
    @g02("subeye")
    public Subeye subeye;
    @DexIgnore
    @g02("version")
    public String version;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DateResponse {
        @DexIgnore
        @g02("direction")
        public String direction;
        @DexIgnore
        @g02("endAngle")
        public String endAngle;
        @DexIgnore
        @g02("hand")
        public String hand;
        @DexIgnore
        @g02("startAngle")
        public String startAngle;

        @DexIgnore
        public final String getDirection() {
            return this.direction;
        }

        @DexIgnore
        public final String getEndAngle() {
            return this.endAngle;
        }

        @DexIgnore
        public final String getHand() {
            return this.hand;
        }

        @DexIgnore
        public final String getStartAngle() {
            return this.startAngle;
        }

        @DexIgnore
        public final void setDirection(String str) {
            this.direction = str;
        }

        @DexIgnore
        public final void setEndAngle(String str) {
            this.endAngle = str;
        }

        @DexIgnore
        public final void setHand(String str) {
            this.hand = str;
        }

        @DexIgnore
        public final void setStartAngle(String str) {
            this.startAngle = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Progress {
        @DexIgnore
        @g02("direction")
        public String direction;
        @DexIgnore
        @g02("endAngle")
        public String endAngle;
        @DexIgnore
        @g02("hand")
        public String hand;
        @DexIgnore
        @g02("startAngle")
        public String startAngle;

        @DexIgnore
        public final String getDirection() {
            return this.direction;
        }

        @DexIgnore
        public final String getEndAngle() {
            return this.endAngle;
        }

        @DexIgnore
        public final String getHand() {
            return this.hand;
        }

        @DexIgnore
        public final String getStartAngle() {
            return this.startAngle;
        }

        @DexIgnore
        public final void setDirection(String str) {
            this.direction = str;
        }

        @DexIgnore
        public final void setEndAngle(String str) {
            this.endAngle = str;
        }

        @DexIgnore
        public final void setHand(String str) {
            this.hand = str;
        }

        @DexIgnore
        public final void setStartAngle(String str) {
            this.startAngle = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Subeye {
        @DexIgnore
        @g02("alarm")
        public String alarm;
        @DexIgnore
        @g02("alert")
        public String alert;
        @DexIgnore
        @g02("date")
        public String date;
        @DexIgnore
        @g02("time2")
        public String time2;

        @DexIgnore
        public final String getAlarm() {
            return this.alarm;
        }

        @DexIgnore
        public final String getAlert() {
            return this.alert;
        }

        @DexIgnore
        public final String getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getTime2() {
            return this.time2;
        }

        @DexIgnore
        public final void setAlarm(String str) {
            this.alarm = str;
        }

        @DexIgnore
        public final void setAlert(String str) {
            this.alert = str;
        }

        @DexIgnore
        public final void setDate(String str) {
            this.date = str;
        }

        @DexIgnore
        public final void setTime2(String str) {
            this.time2 = str;
        }
    }

    @DexIgnore
    public final DateResponse getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getDeviceLongName() {
        return this.deviceLongName;
    }

    @DexIgnore
    public final String getDeviceShortName() {
        return this.deviceShortName;
    }

    @DexIgnore
    public final boolean getEnableAlarmAnimation() {
        return this.enableAlarmAnimation;
    }

    @DexIgnore
    public final boolean getMainHandsFlipped() {
        return this.mainHandsFlipped;
    }

    @DexIgnore
    public final Progress getProgress() {
        return this.progress;
    }

    @DexIgnore
    public final String getRawBase64() {
        return this.rawBase64;
    }

    @DexIgnore
    public final Subeye getSubeye() {
        return this.subeye;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public final void setDate(DateResponse dateResponse) {
        this.date = dateResponse;
    }

    @DexIgnore
    public final void setDeviceLongName(String str) {
        this.deviceLongName = str;
    }

    @DexIgnore
    public final void setDeviceShortName(String str) {
        this.deviceShortName = str;
    }

    @DexIgnore
    public final void setEnableAlarmAnimation(boolean z) {
        this.enableAlarmAnimation = z;
    }

    @DexIgnore
    public final void setMainHandsFlipped(boolean z) {
        this.mainHandsFlipped = z;
    }

    @DexIgnore
    public final void setProgress(Progress progress2) {
        this.progress = progress2;
    }

    @DexIgnore
    public final void setRawBase64(String str) {
        this.rawBase64 = str;
    }

    @DexIgnore
    public final void setSubeye(Subeye subeye2) {
        this.subeye = subeye2;
    }

    @DexIgnore
    public final void setVersion(String str) {
        this.version = str;
    }
}
