package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.i42;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wa4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xe4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ wa4 instance$delegate; // = ya4.a(CloudImageHelper$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public PortfolioApp mApp;
    @DexIgnore
    public i42 mAppExecutors;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public static /* final */ /* synthetic */ xe4[] $$delegatedProperties;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(yd4.a(Companion.class), "instance", "getInstance()Lcom/portfolio/platform/cloudimage/CloudImageHelper;");
            yd4.a((PropertyReference1) propertyReference1Impl);
            $$delegatedProperties = new xe4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final CloudImageHelper getInstance() {
            wa4 access$getInstance$cp = CloudImageHelper.instance$delegate;
            Companion companion = CloudImageHelper.Companion;
            xe4 xe4 = $$delegatedProperties[0];
            return (CloudImageHelper) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public final String getTAG() {
            return CloudImageHelper.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Holder {
        @DexIgnore
        public static /* final */ Holder INSTANCE; // = new Holder();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ CloudImageHelper f2INSTANCE; // = new CloudImageHelper();

        @DexIgnore
        public final CloudImageHelper getINSTANCE() {
            return f2INSTANCE;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ItemImage {
        @DexIgnore
        public Constants.CalibrationType mCalibrationType; // = Constants.CalibrationType.NONE;
        @DexIgnore
        public Constants.DeviceType mDeviceType; // = Constants.DeviceType.NONE;
        @DexIgnore
        public File mFile;
        @DexIgnore
        public OnImageCallbackListener mListener;
        @DexIgnore
        public Integer mResourceId; // = -1;
        @DexIgnore
        public String mSerialNumber;
        @DexIgnore
        public String mSerialPrefix;
        @DexIgnore
        public WeakReference<ImageView> mWeakReferenceImageView;

        @DexIgnore
        public ItemImage() {
        }

        @DexIgnore
        public final void download() {
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$ItemImage$download$Anon1(this, (kc4) null), 3, (Object) null);
        }

        @DexIgnore
        public final void downloadForCalibration() {
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this, (kc4) null), 3, (Object) null);
        }

        @DexIgnore
        public final File getFile() {
            return this.mFile;
        }

        @DexIgnore
        public final ItemImage setFile(File file) {
            this.mFile = file;
            return this;
        }

        @DexIgnore
        public final ItemImage setImageCallback(OnImageCallbackListener onImageCallbackListener) {
            this.mListener = onImageCallbackListener;
            return this;
        }

        @DexIgnore
        public final ItemImage setPlaceHolder(ImageView imageView, int i) {
            wd4.b(imageView, "imageView");
            this.mWeakReferenceImageView = new WeakReference<>(imageView);
            this.mResourceId = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialNumber(String str) {
            wd4.b(str, "serialNumber");
            this.mSerialNumber = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialPrefix(String str) {
            wd4.b(str, "serialPrefix");
            this.mSerialPrefix = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.DeviceType deviceType) {
            wd4.b(deviceType, "deviceType");
            this.mDeviceType = deviceType;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.CalibrationType calibrationType) {
            wd4.b(calibrationType, "calibrationType");
            this.mCalibrationType = calibrationType;
            return this;
        }
    }

    @DexIgnore
    public interface OnForceDownloadCallbackListener {
        @DexIgnore
        void onDownloadCallback(boolean z);
    }

    @DexIgnore
    public interface OnImageCallbackListener {
        @DexIgnore
        void onImageCallback(String str, String str2);
    }

    /*
    static {
        String simpleName = CloudImageHelper$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        wd4.a((Object) simpleName, "CloudImageHelper::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageHelper() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public static final CloudImageHelper getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public final PortfolioApp getMApp() {
        PortfolioApp portfolioApp = this.mApp;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wd4.d("mApp");
        throw null;
    }

    @DexIgnore
    public final i42 getMAppExecutors() {
        i42 i42 = this.mAppExecutors;
        if (i42 != null) {
            return i42;
        }
        wd4.d("mAppExecutors");
        throw null;
    }

    @DexIgnore
    public final void setMApp(PortfolioApp portfolioApp) {
        wd4.b(portfolioApp, "<set-?>");
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public final void setMAppExecutors(i42 i42) {
        wd4.b(i42, "<set-?>");
        this.mAppExecutors = i42;
    }

    @DexIgnore
    public final ItemImage with() {
        ItemImage itemImage = new ItemImage();
        if (itemImage.getFile() == null) {
            ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$with$Anon1(this, itemImage, (kc4) null), 3, (Object) null);
        }
        return itemImage;
    }
}
