package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$Anon1", f = "URLRequestTaskHelper.kt", l = {62}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<ApiResponse<yz1>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$Anon1(URLRequestTaskHelper uRLRequestTaskHelper, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new URLRequestTaskHelper$execute$response$Anon1(this.this$Anon0, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((URLRequestTaskHelper$execute$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 mApiService = this.this$Anon0.getMApiService();
            String serialNumber$app_fossilRelease = this.this$Anon0.getSerialNumber$app_fossilRelease();
            if (serialNumber$app_fossilRelease != null) {
                String feature$app_fossilRelease = this.this$Anon0.getFeature$app_fossilRelease();
                if (feature$app_fossilRelease != null) {
                    String access$getResolution$p = this.this$Anon0.resolution;
                    if (access$getResolution$p != null) {
                        this.label = 1;
                        obj = mApiService.getDeviceAssets(20, 0, serialNumber$app_fossilRelease, feature$app_fossilRelease, access$getResolution$p, "ANDROID", this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
