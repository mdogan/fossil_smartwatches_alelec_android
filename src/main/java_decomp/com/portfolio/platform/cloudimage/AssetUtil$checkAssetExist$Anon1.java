package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import java.io.File;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.cloudimage.AssetUtil", f = "AssetUtil.kt", l = {18, 21, 26, 29}, m = "checkAssetExist")
public final class AssetUtil$checkAssetExist$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon10;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public boolean Z$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ AssetUtil this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$Anon1(AssetUtil assetUtil, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = assetUtil;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.checkAssetExist((File) null, (String) null, (String) null, (String) null, (String) null, (String) null, (CloudImageHelper.OnImageCallbackListener) null, this);
    }
}
