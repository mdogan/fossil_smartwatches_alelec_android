package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$Anon1", f = "CloudImageRunnable.kt", l = {62}, m = "invokeSuspend")
public final class CloudImageRunnable$downloadingDeviceAssets$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public /* final */ /* synthetic */ String $feature;
    @DexIgnore
    public /* final */ /* synthetic */ String $zipFilePath;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$downloadingDeviceAssets$Anon1(CloudImageRunnable cloudImageRunnable, String str, String str2, String str3, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = cloudImageRunnable;
        this.$zipFilePath = str;
        this.$destinationUnzipPath = str2;
        this.$feature = str3;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CloudImageRunnable$downloadingDeviceAssets$Anon1 cloudImageRunnable$downloadingDeviceAssets$Anon1 = new CloudImageRunnable$downloadingDeviceAssets$Anon1(this.this$Anon0, this.$zipFilePath, this.$destinationUnzipPath, this.$feature, kc4);
        cloudImageRunnable$downloadingDeviceAssets$Anon1.p$ = (lh4) obj;
        return cloudImageRunnable$downloadingDeviceAssets$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageRunnable$downloadingDeviceAssets$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            URLRequestTaskHelper access$prepareURLRequestTask = this.this$Anon0.prepareURLRequestTask();
            access$prepareURLRequestTask.init(this.$zipFilePath, this.$destinationUnzipPath, CloudImageRunnable.access$getSerialNumber$p(this.this$Anon0), this.$feature, CloudImageRunnable.access$getResolution$p(this.this$Anon0));
            this.L$Anon0 = lh4;
            this.L$Anon1 = access$prepareURLRequestTask;
            this.label = 1;
            if (access$prepareURLRequestTask.execute(this) == a) {
                return a;
            }
        } else if (i == 1) {
            URLRequestTaskHelper uRLRequestTaskHelper = (URLRequestTaskHelper) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
