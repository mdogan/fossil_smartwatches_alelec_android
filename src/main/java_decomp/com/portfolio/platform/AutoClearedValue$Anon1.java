package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import com.fossil.blesdk.obfuscated.ec;
import com.fossil.blesdk.obfuscated.u52;
import com.fossil.blesdk.obfuscated.xb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AutoClearedValue$Anon1 implements xb {
    @DexIgnore
    public /* final */ /* synthetic */ u52 a;

    @DexIgnore
    @ec(Lifecycle.Event.ON_DESTROY)
    public final void onDestroy() {
        this.a.a(null);
        throw null;
    }
}
