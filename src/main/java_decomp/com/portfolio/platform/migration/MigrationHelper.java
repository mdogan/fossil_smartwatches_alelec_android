package com.portfolio.platform.migration;

import com.fossil.blesdk.obfuscated.c62;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.portfolio.platform.MigrationManager;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MigrationHelper {
    @DexIgnore
    public /* final */ fn2 a;
    @DexIgnore
    public /* final */ MigrationManager b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public MigrationHelper(fn2 fn2, MigrationManager migrationManager, c62 c62) {
        wd4.b(fn2, "mSharedPreferencesManager");
        wd4.b(migrationManager, "mMigrationManager");
        wd4.b(c62, "mLegacyMigrationManager");
        this.a = fn2;
        this.b = migrationManager;
    }

    @DexIgnore
    public final boolean a(String str) {
        wd4.b(str, "version");
        return this.a.m(str);
    }

    @DexIgnore
    public final ri4 b(String str) {
        wd4.b(str, "version");
        return mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MigrationHelper$startMigrationForVersion$Anon1(this, str, (kc4) null), 3, (Object) null);
    }
}
