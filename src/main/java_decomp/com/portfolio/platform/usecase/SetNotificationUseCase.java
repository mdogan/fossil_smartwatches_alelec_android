package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationUseCase extends CoroutineUseCase<b, Object, c> {
    @DexIgnore
    public /* final */ wy2 d;
    @DexIgnore
    public /* final */ qx2 e;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase f;
    @DexIgnore
    public /* final */ NotificationsRepository g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ fn2 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(String str) {
            wd4.b(str, "message");
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public SetNotificationUseCase(wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, fn2 fn2) {
        wd4.b(wy2, "mGetApp");
        wd4.b(qx2, "mGetAllContactGroups");
        wd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wd4.b(notificationsRepository, "mNotificationsRepository");
        wd4.b(deviceRepository, "mDeviceRepository");
        wd4.b(fn2, "mSharePref");
        this.d = wy2;
        this.e = qx2;
        this.f = notificationSettingsDatabase;
        this.g = notificationsRepository;
        this.h = deviceRepository;
        this.i = fn2;
    }

    @DexIgnore
    public String c() {
        return "SetNotificationUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(b bVar, kc4<Object> kc4) {
        SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon1;
        int i2;
        String str;
        AppNotificationFilterSettings appNotificationFilterSettings;
        Object obj;
        if (kc4 instanceof SetNotificationUseCase$run$Anon1) {
            setNotificationUseCase$run$Anon1 = (SetNotificationUseCase$run$Anon1) kc4;
            int i3 = setNotificationUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                setNotificationUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon12 = setNotificationUseCase$run$Anon1;
                Object obj2 = setNotificationUseCase$run$Anon12.result;
                Object a2 = oc4.a();
                i2 = setNotificationUseCase$run$Anon12.label;
                if (i2 != 0) {
                    za4.a(obj2);
                    if (bVar == null || cg4.a(bVar.a())) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SetNotificationUseCase", "Invalid request: Request = " + bVar);
                        return new c("Invalid request");
                    }
                    str = bVar.a();
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        wy2 wy2 = this.d;
                        qx2 qx2 = this.e;
                        NotificationSettingsDatabase notificationSettingsDatabase = this.f;
                        fn2 fn2 = this.i;
                        setNotificationUseCase$run$Anon12.L$Anon0 = this;
                        setNotificationUseCase$run$Anon12.L$Anon1 = bVar;
                        setNotificationUseCase$run$Anon12.L$Anon2 = str;
                        setNotificationUseCase$run$Anon12.label = 1;
                        obj = notificationAppHelper.a(wy2, qx2, notificationSettingsDatabase, fn2, setNotificationUseCase$run$Anon12);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        appNotificationFilterSettings = NotificationAppHelper.b.a(this.g.getAllNotificationsByHour(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), NotificationAppHelper.b.a(this.h.getSkuModelBySerialPrefix(DeviceHelper.o.b(str)), str));
                        PortfolioApp.W.c().a(appNotificationFilterSettings, str);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                        return new Object();
                    }
                } else if (i2 == 1) {
                    b bVar2 = (b) setNotificationUseCase$run$Anon12.L$Anon1;
                    SetNotificationUseCase setNotificationUseCase = (SetNotificationUseCase) setNotificationUseCase$run$Anon12.L$Anon0;
                    za4.a(obj2);
                    Object obj3 = obj2;
                    str = (String) setNotificationUseCase$run$Anon12.L$Anon2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
                PortfolioApp.W.c().a(appNotificationFilterSettings, str);
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
                return new Object();
            }
        }
        setNotificationUseCase$run$Anon1 = new SetNotificationUseCase$run$Anon1(this, kc4);
        SetNotificationUseCase$run$Anon1 setNotificationUseCase$run$Anon122 = setNotificationUseCase$run$Anon1;
        Object obj22 = setNotificationUseCase$run$Anon122.result;
        Object a22 = oc4.a();
        i2 = setNotificationUseCase$run$Anon122.label;
        if (i2 != 0) {
        }
        appNotificationFilterSettings = new AppNotificationFilterSettings((List) obj, System.currentTimeMillis());
        PortfolioApp.W.c().a(appNotificationFilterSettings, str);
        ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
        local222.d("SetNotificationUseCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
        return new Object();
    }
}
