package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$Anon1$response$Anon1", f = "GetWeather.kt", l = {31}, m = "invokeSuspend")
public final class GetWeather$executeUseCase$Anon1$response$Anon1 extends SuspendLambda implements jd4<kc4<? super cs4<yz1>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather$executeUseCase$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetWeather$executeUseCase$Anon1$response$Anon1(GetWeather$executeUseCase$Anon1 getWeather$executeUseCase$Anon1, kc4 kc4) {
        super(1, kc4);
        this.this$Anon0 = getWeather$executeUseCase$Anon1;
    }

    @DexIgnore
    public final kc4<cb4> create(kc4<?> kc4) {
        wd4.b(kc4, "completion");
        return new GetWeather$executeUseCase$Anon1$response$Anon1(this.this$Anon0, kc4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((GetWeather$executeUseCase$Anon1$response$Anon1) create((kc4) obj)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            ApiServiceV2 a2 = this.this$Anon0.this$Anon0.d;
            String valueOf = String.valueOf(this.this$Anon0.$requestValues.a().e);
            String valueOf2 = String.valueOf(this.this$Anon0.$requestValues.a().f);
            String value = this.this$Anon0.$requestValues.b().getValue();
            this.label = 1;
            obj = a2.getWeather(valueOf, valueOf2, value, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
