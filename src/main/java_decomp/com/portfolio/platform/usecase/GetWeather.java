package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.j02;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetWeather extends j62<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return GetWeather.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.a {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "errorMessage");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.b {
        @DexIgnore
        public /* final */ WeatherSettings.TEMP_UNIT a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public c(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            wd4.b(latLng, "latLng");
            wd4.b(temp_unit, "tempUnit");
            j02.a(temp_unit);
            wd4.a((Object) temp_unit, "checkNotNull(tempUnit)");
            this.a = temp_unit;
            j02.a(latLng);
            wd4.a((Object) latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements j62.c {
        @DexIgnore
        public /* final */ Weather a;

        @DexIgnore
        public d(Weather weather) {
            wd4.b(weather, "weather");
            this.a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = GetWeather.class.getSimpleName();
        wd4.a((Object) simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public GetWeather(ApiServiceV2 apiServiceV2) {
        wd4.b(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    public void a(c cVar) {
        wd4.b(cVar, "requestValues");
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new GetWeather$executeUseCase$Anon1(this, cVar, (kc4) null), 3, (Object) null);
    }
}
