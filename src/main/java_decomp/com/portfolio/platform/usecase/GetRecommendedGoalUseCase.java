package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.enums.Gender;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetRecommendedGoalUseCase extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ SleepSummariesRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ Gender d;

        @DexIgnore
        public c(int i, int i2, int i3, Gender gender) {
            wd4.b(gender, "gender");
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = gender;
        }

        @DexIgnore
        public final Gender a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GetRecommendedGoalUseCase(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        wd4.b(summariesRepository, "mSummaryRepository");
        wd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        this.d = summariesRepository;
        this.e = sleepSummariesRepository;
        this.f = sleepSessionsRepository;
    }

    @DexIgnore
    public String c() {
        return "GetRecommendedGoalUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0139 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0253 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0271 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    public Object a(c cVar, kc4<Object> kc4) {
        GetRecommendedGoalUseCase$run$Anon1 getRecommendedGoalUseCase$run$Anon1;
        Object a2;
        int i;
        GetRecommendedGoalUseCase getRecommendedGoalUseCase;
        c cVar2;
        ro2 ro2;
        ro2 ro22;
        ActivityRecommendedGoals activityRecommendedGoals;
        SleepRecommendedGoal sleepRecommendedGoal;
        ActivitySettings activitySettings;
        SleepSummariesRepository sleepSummariesRepository;
        int recommendedSleepGoal;
        ActivityRecommendedGoals activityRecommendedGoals2;
        SleepRecommendedGoal sleepRecommendedGoal2;
        SummariesRepository summariesRepository;
        c cVar3;
        GetRecommendedGoalUseCase getRecommendedGoalUseCase2;
        Object downloadRecommendedGoals;
        c cVar4 = cVar;
        kc4<Object> kc42 = kc4;
        if (kc42 instanceof GetRecommendedGoalUseCase$run$Anon1) {
            getRecommendedGoalUseCase$run$Anon1 = (GetRecommendedGoalUseCase$run$Anon1) kc42;
            int i2 = getRecommendedGoalUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                getRecommendedGoalUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = getRecommendedGoalUseCase$run$Anon1.result;
                a2 = oc4.a();
                i = getRecommendedGoalUseCase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("height ");
                    if (cVar4 != null) {
                        sb.append(cVar.b());
                        sb.append(" weight ");
                        sb.append(cVar.d());
                        sb.append(" age ");
                        sb.append(cVar.c());
                        sb.append(" gender ");
                        sb.append(cVar.a());
                        local.d("GetRecommendedGoalUseCase", sb.toString());
                        SummariesRepository summariesRepository2 = this.d;
                        int c2 = cVar.c();
                        int d2 = cVar.d();
                        int b2 = cVar.b();
                        String value = cVar.a().getValue();
                        getRecommendedGoalUseCase$run$Anon1.L$Anon0 = this;
                        getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar4;
                        getRecommendedGoalUseCase$run$Anon1.label = 1;
                        obj = summariesRepository2.downloadRecommendedGoals(c2, d2, b2, value, getRecommendedGoalUseCase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        cVar3 = cVar4;
                        getRecommendedGoalUseCase2 = this;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    za4.a(obj);
                    cVar3 = (c) getRecommendedGoalUseCase$run$Anon1.L$Anon1;
                    getRecommendedGoalUseCase2 = (GetRecommendedGoalUseCase) getRecommendedGoalUseCase$run$Anon1.L$Anon0;
                } else if (i == 2) {
                    za4.a(obj);
                    ro2 = (ro2) getRecommendedGoalUseCase$run$Anon1.L$Anon2;
                    cVar2 = (c) getRecommendedGoalUseCase$run$Anon1.L$Anon1;
                    getRecommendedGoalUseCase = (GetRecommendedGoalUseCase) getRecommendedGoalUseCase$run$Anon1.L$Anon0;
                    ro22 = (ro2) obj;
                    if (!(ro2 instanceof so2)) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init activity recommended goal from server");
                        Object a3 = ((so2) ro2).a();
                        if (a3 != null) {
                            activityRecommendedGoals2 = (ActivityRecommendedGoals) a3;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else if (ro2 instanceof qo2) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init activity recommended goal from client");
                        activityRecommendedGoals2 = new ActivityRecommendedGoals(nk2.a.b(), nk2.a.a(cVar2.c(), cVar2.d() / 1000, cVar2.b(), cVar2.a()), nk2.a.a());
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    activityRecommendedGoals = activityRecommendedGoals2;
                    if (!(ro22 instanceof so2)) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init sleepRecommendedGoals from server");
                        Object a4 = ((so2) ro22).a();
                        if (a4 != null) {
                            sleepRecommendedGoal2 = (SleepRecommendedGoal) a4;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else if (ro22 instanceof qo2) {
                        FLogger.INSTANCE.getLocal().d("GetRecommendedGoalUseCase", "init sleepRecommendedGoals from client");
                        sleepRecommendedGoal2 = new SleepRecommendedGoal(nk2.a.a(cVar2.c()));
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    sleepRecommendedGoal = sleepRecommendedGoal2;
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("GetRecommendedGoalUseCase", "Update recommendActivityGoals " + activityRecommendedGoals + " recommendSleepGoal " + sleepRecommendedGoal);
                    getRecommendedGoalUseCase.d.upsertRecommendGoals(activityRecommendedGoals);
                    getRecommendedGoalUseCase.f.upsertRecommendedGoals(sleepRecommendedGoal);
                    activitySettings = new ActivitySettings(activityRecommendedGoals.getRecommendedStepsGoal(), activityRecommendedGoals.getRecommendedCaloriesGoal(), activityRecommendedGoals.getRecommendedActiveTimeGoal());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("GetRecommendedGoalUseCase", "Update ActivitySettings " + activitySettings + " sleepSetting " + sleepRecommendedGoal.getRecommendedSleepGoal());
                    summariesRepository = getRecommendedGoalUseCase.d;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon3 = ro22;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon4 = activityRecommendedGoals;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon5 = sleepRecommendedGoal;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon6 = activitySettings;
                    getRecommendedGoalUseCase$run$Anon1.label = 3;
                    if (summariesRepository.pushActivitySettingsToServer(activitySettings, getRecommendedGoalUseCase$run$Anon1) == a2) {
                        return a2;
                    }
                    sleepSummariesRepository = getRecommendedGoalUseCase.e;
                    recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                    getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon3 = ro22;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon4 = activityRecommendedGoals;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon5 = sleepRecommendedGoal;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon6 = activitySettings;
                    getRecommendedGoalUseCase$run$Anon1.label = 4;
                    if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, getRecommendedGoalUseCase$run$Anon1) == a2) {
                    }
                    return new d();
                } else if (i == 3) {
                    activitySettings = (ActivitySettings) getRecommendedGoalUseCase$run$Anon1.L$Anon6;
                    sleepRecommendedGoal = (SleepRecommendedGoal) getRecommendedGoalUseCase$run$Anon1.L$Anon5;
                    activityRecommendedGoals = (ActivityRecommendedGoals) getRecommendedGoalUseCase$run$Anon1.L$Anon4;
                    ro22 = (ro2) getRecommendedGoalUseCase$run$Anon1.L$Anon3;
                    ro2 = (ro2) getRecommendedGoalUseCase$run$Anon1.L$Anon2;
                    cVar2 = (c) getRecommendedGoalUseCase$run$Anon1.L$Anon1;
                    getRecommendedGoalUseCase = (GetRecommendedGoalUseCase) getRecommendedGoalUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                    sleepSummariesRepository = getRecommendedGoalUseCase.e;
                    recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                    getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro2;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon3 = ro22;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon4 = activityRecommendedGoals;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon5 = sleepRecommendedGoal;
                    getRecommendedGoalUseCase$run$Anon1.L$Anon6 = activitySettings;
                    getRecommendedGoalUseCase$run$Anon1.label = 4;
                    if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, getRecommendedGoalUseCase$run$Anon1) == a2) {
                        return a2;
                    }
                    return new d();
                } else if (i == 4) {
                    ActivitySettings activitySettings2 = (ActivitySettings) getRecommendedGoalUseCase$run$Anon1.L$Anon6;
                    SleepRecommendedGoal sleepRecommendedGoal3 = (SleepRecommendedGoal) getRecommendedGoalUseCase$run$Anon1.L$Anon5;
                    ActivityRecommendedGoals activityRecommendedGoals3 = (ActivityRecommendedGoals) getRecommendedGoalUseCase$run$Anon1.L$Anon4;
                    ro2 ro23 = (ro2) getRecommendedGoalUseCase$run$Anon1.L$Anon3;
                    ro2 ro24 = (ro2) getRecommendedGoalUseCase$run$Anon1.L$Anon2;
                    c cVar5 = (c) getRecommendedGoalUseCase$run$Anon1.L$Anon1;
                    GetRecommendedGoalUseCase getRecommendedGoalUseCase3 = (GetRecommendedGoalUseCase) getRecommendedGoalUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                    return new d();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 ro25 = (ro2) obj;
                SleepSessionsRepository sleepSessionsRepository = getRecommendedGoalUseCase2.f;
                int c3 = cVar3.c();
                int d3 = cVar3.d();
                int b3 = cVar3.b();
                String value2 = cVar3.a().getValue();
                getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase2;
                getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar3;
                getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro25;
                getRecommendedGoalUseCase$run$Anon1.label = 2;
                c cVar6 = cVar3;
                downloadRecommendedGoals = sleepSessionsRepository.downloadRecommendedGoals(c3, d3, b3, value2, getRecommendedGoalUseCase$run$Anon1);
                if (downloadRecommendedGoals != a2) {
                    return a2;
                }
                getRecommendedGoalUseCase = getRecommendedGoalUseCase2;
                ro2 = ro25;
                obj = downloadRecommendedGoals;
                cVar2 = cVar6;
                ro22 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                }
                activityRecommendedGoals = activityRecommendedGoals2;
                if (!(ro22 instanceof so2)) {
                }
                sleepRecommendedGoal = sleepRecommendedGoal2;
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("GetRecommendedGoalUseCase", "Update recommendActivityGoals " + activityRecommendedGoals + " recommendSleepGoal " + sleepRecommendedGoal);
                getRecommendedGoalUseCase.d.upsertRecommendGoals(activityRecommendedGoals);
                getRecommendedGoalUseCase.f.upsertRecommendedGoals(sleepRecommendedGoal);
                activitySettings = new ActivitySettings(activityRecommendedGoals.getRecommendedStepsGoal(), activityRecommendedGoals.getRecommendedCaloriesGoal(), activityRecommendedGoals.getRecommendedActiveTimeGoal());
                ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                local32.d("GetRecommendedGoalUseCase", "Update ActivitySettings " + activitySettings + " sleepSetting " + sleepRecommendedGoal.getRecommendedSleepGoal());
                summariesRepository = getRecommendedGoalUseCase.d;
                getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase;
                getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar2;
                getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro2;
                getRecommendedGoalUseCase$run$Anon1.L$Anon3 = ro22;
                getRecommendedGoalUseCase$run$Anon1.L$Anon4 = activityRecommendedGoals;
                getRecommendedGoalUseCase$run$Anon1.L$Anon5 = sleepRecommendedGoal;
                getRecommendedGoalUseCase$run$Anon1.L$Anon6 = activitySettings;
                getRecommendedGoalUseCase$run$Anon1.label = 3;
                if (summariesRepository.pushActivitySettingsToServer(activitySettings, getRecommendedGoalUseCase$run$Anon1) == a2) {
                }
                sleepSummariesRepository = getRecommendedGoalUseCase.e;
                recommendedSleepGoal = sleepRecommendedGoal.getRecommendedSleepGoal();
                getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase;
                getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar2;
                getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro2;
                getRecommendedGoalUseCase$run$Anon1.L$Anon3 = ro22;
                getRecommendedGoalUseCase$run$Anon1.L$Anon4 = activityRecommendedGoals;
                getRecommendedGoalUseCase$run$Anon1.L$Anon5 = sleepRecommendedGoal;
                getRecommendedGoalUseCase$run$Anon1.L$Anon6 = activitySettings;
                getRecommendedGoalUseCase$run$Anon1.label = 4;
                if (sleepSummariesRepository.pushLastSleepGoalToServer(recommendedSleepGoal, getRecommendedGoalUseCase$run$Anon1) == a2) {
                }
                return new d();
            }
        }
        getRecommendedGoalUseCase$run$Anon1 = new GetRecommendedGoalUseCase$run$Anon1(this, kc42);
        Object obj2 = getRecommendedGoalUseCase$run$Anon1.result;
        a2 = oc4.a();
        i = getRecommendedGoalUseCase$run$Anon1.label;
        if (i != 0) {
        }
        ro2 ro252 = (ro2) obj2;
        SleepSessionsRepository sleepSessionsRepository2 = getRecommendedGoalUseCase2.f;
        int c32 = cVar3.c();
        int d32 = cVar3.d();
        int b32 = cVar3.b();
        String value22 = cVar3.a().getValue();
        getRecommendedGoalUseCase$run$Anon1.L$Anon0 = getRecommendedGoalUseCase2;
        getRecommendedGoalUseCase$run$Anon1.L$Anon1 = cVar3;
        getRecommendedGoalUseCase$run$Anon1.L$Anon2 = ro252;
        getRecommendedGoalUseCase$run$Anon1.label = 2;
        c cVar62 = cVar3;
        downloadRecommendedGoals = sleepSessionsRepository2.downloadRecommendedGoals(c32, d32, b32, value22, getRecommendedGoalUseCase$run$Anon1);
        if (downloadRecommendedGoals != a2) {
        }
    }
}
