package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CheckAuthenticationEmailExisting extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "email");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            wd4.b(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public d(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public CheckAuthenticationEmailExisting(UserRepository userRepository) {
        wd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return "CheckAuthenticationEmailExisting";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(b bVar, kc4<Object> kc4) {
        CheckAuthenticationEmailExisting$run$Anon1 checkAuthenticationEmailExisting$run$Anon1;
        int i;
        ro2 ro2;
        if (kc4 instanceof CheckAuthenticationEmailExisting$run$Anon1) {
            checkAuthenticationEmailExisting$run$Anon1 = (CheckAuthenticationEmailExisting$run$Anon1) kc4;
            int i2 = checkAuthenticationEmailExisting$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                checkAuthenticationEmailExisting$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = checkAuthenticationEmailExisting$run$Anon1.result;
                Object a2 = oc4.a();
                i = checkAuthenticationEmailExisting$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    UserRepository userRepository = this.d;
                    String a3 = bVar.a();
                    checkAuthenticationEmailExisting$run$Anon1.L$Anon0 = this;
                    checkAuthenticationEmailExisting$run$Anon1.L$Anon1 = bVar;
                    checkAuthenticationEmailExisting$run$Anon1.label = 1;
                    obj = userRepository.checkAuthenticationEmailExisting(a3, checkAuthenticationEmailExisting$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) checkAuthenticationEmailExisting$run$Anon1.L$Anon1;
                    CheckAuthenticationEmailExisting checkAuthenticationEmailExisting = (CheckAuthenticationEmailExisting) checkAuthenticationEmailExisting$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("email existing ");
                    so2 so2 = (so2) ro2;
                    Object a4 = so2.a();
                    if (a4 != null) {
                        sb.append(((Boolean) a4).booleanValue());
                        local.d("CheckAuthenticationEmailExisting", sb.toString());
                        return new d(((Boolean) so2.a()).booleanValue());
                    }
                    wd4.a();
                    throw null;
                } else if (!(ro2 instanceof qo2)) {
                    return new c(600, "");
                } else {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("email existing failed ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.c());
                    local2.d("CheckAuthenticationEmailExisting", sb2.toString());
                    return new c(qo2.a(), "");
                }
            }
        }
        checkAuthenticationEmailExisting$run$Anon1 = new CheckAuthenticationEmailExisting$run$Anon1(this, kc4);
        Object obj2 = checkAuthenticationEmailExisting$run$Anon1.result;
        Object a22 = oc4.a();
        i = checkAuthenticationEmailExisting$run$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
