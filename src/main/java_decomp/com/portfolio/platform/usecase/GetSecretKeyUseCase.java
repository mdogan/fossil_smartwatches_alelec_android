package com.portfolio.platform.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nr3;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x52;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetSecretKeyUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ nr3 d;
    @DexIgnore
    public /* final */ DeviceRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            wd4.b(str, "deviceId");
            wd4.b(str2, ButtonService.USER_ID);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            wd4.b(str, "errorMesagge");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public GetSecretKeyUseCase(nr3 nr3, DeviceRepository deviceRepository) {
        wd4.b(nr3, "mEncryptValueKeyStoreUseCase");
        wd4.b(deviceRepository, "mDeviceRepository");
        this.d = nr3;
        this.e = deviceRepository;
    }

    @DexIgnore
    public String c() {
        return "GetSecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(b bVar, kc4<Object> kc4) {
        GetSecretKeyUseCase$run$Anon1 getSecretKeyUseCase$run$Anon1;
        int i;
        GetSecretKeyUseCase getSecretKeyUseCase;
        String str;
        ro2 ro2;
        if (kc4 instanceof GetSecretKeyUseCase$run$Anon1) {
            getSecretKeyUseCase$run$Anon1 = (GetSecretKeyUseCase$run$Anon1) kc4;
            int i2 = getSecretKeyUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                getSecretKeyUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = getSecretKeyUseCase$run$Anon1.result;
                Object a2 = oc4.a();
                i = getSecretKeyUseCase$run$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    DeviceRepository deviceRepository = this.e;
                    if (bVar != null) {
                        String a3 = bVar.a();
                        getSecretKeyUseCase$run$Anon1.L$Anon0 = this;
                        getSecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
                        getSecretKeyUseCase$run$Anon1.label = 1;
                        obj = deviceRepository.getDeviceSecretKey(a3, getSecretKeyUseCase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        getSecretKeyUseCase = this;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    bVar = (b) getSecretKeyUseCase$run$Anon1.L$Anon1;
                    getSecretKeyUseCase = (GetSecretKeyUseCase) getSecretKeyUseCase$run$Anon1.L$Anon0;
                    za4.a(obj);
                } else if (i == 2) {
                    String str2 = (String) getSecretKeyUseCase$run$Anon1.L$Anon4;
                    str = (String) getSecretKeyUseCase$run$Anon1.L$Anon3;
                    ro2 ro22 = (ro2) getSecretKeyUseCase$run$Anon1.L$Anon2;
                    b bVar2 = (b) getSecretKeyUseCase$run$Anon1.L$Anon1;
                    za4.a(obj);
                    getSecretKeyUseCase = (GetSecretKeyUseCase) getSecretKeyUseCase$run$Anon1.L$Anon0;
                    PortfolioApp.W.c().e(str, PortfolioApp.W.c().e());
                    getSecretKeyUseCase.a(new d());
                    return new Object();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ro2 = (ro2) obj;
                if (!(ro2 instanceof so2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success to get server secret key ");
                    so2 so2 = (so2) ro2;
                    sb.append((String) so2.a());
                    local.d("GetSecretKeyUseCase", sb.toString());
                    if (!TextUtils.isEmpty((CharSequence) so2.a())) {
                        Object a4 = so2.a();
                        if (a4 != null) {
                            String str3 = (String) a4;
                            String str4 = bVar.b() + ':' + bVar.a();
                            nr3 nr3 = getSecretKeyUseCase.d;
                            nr3.b bVar3 = new nr3.b(str4, str3);
                            getSecretKeyUseCase$run$Anon1.L$Anon0 = getSecretKeyUseCase;
                            getSecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
                            getSecretKeyUseCase$run$Anon1.L$Anon2 = ro2;
                            getSecretKeyUseCase$run$Anon1.L$Anon3 = str3;
                            getSecretKeyUseCase$run$Anon1.L$Anon4 = str4;
                            getSecretKeyUseCase$run$Anon1.label = 2;
                            if (x52.a(nr3, bVar3, getSecretKeyUseCase$run$Anon1) == a2) {
                                return a2;
                            }
                            str = str3;
                            PortfolioApp.W.c().e(str, PortfolioApp.W.c().e());
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                    getSecretKeyUseCase.a(new d());
                    return new Object();
                }
                if (ro2 instanceof qo2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("fail to get server secret key ");
                    qo2 qo2 = (qo2) ro2;
                    sb2.append(qo2.a());
                    local2.d("GetSecretKeyUseCase", sb2.toString());
                    getSecretKeyUseCase.a(new c(qo2.a(), ""));
                }
                return new Object();
            }
        }
        getSecretKeyUseCase$run$Anon1 = new GetSecretKeyUseCase$run$Anon1(this, kc4);
        Object obj2 = getSecretKeyUseCase$run$Anon1.result;
        Object a22 = oc4.a();
        i = getSecretKeyUseCase$run$Anon1.label;
        if (i != 0) {
        }
        ro2 = (ro2) obj2;
        if (!(ro2 instanceof so2)) {
        }
    }
}
