package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {73}, m = "getLocalSecretKey")
public final class VerifySecretKeyUseCase$getLocalSecretKey$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ VerifySecretKeyUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifySecretKeyUseCase$getLocalSecretKey$Anon1(VerifySecretKeyUseCase verifySecretKeyUseCase, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = verifySecretKeyUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a(this);
    }
}
