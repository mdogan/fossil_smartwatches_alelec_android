package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.usecase.GetRecommendedGoalUseCase", f = "GetRecommendedGoalUseCase.kt", l = {30, 32, 64, 65}, m = "run")
public final class GetRecommendedGoalUseCase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ GetRecommendedGoalUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetRecommendedGoalUseCase$run$Anon1(GetRecommendedGoalUseCase getRecommendedGoalUseCase, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = getRecommendedGoalUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((GetRecommendedGoalUseCase.c) null, (kc4<Object>) this);
    }
}
