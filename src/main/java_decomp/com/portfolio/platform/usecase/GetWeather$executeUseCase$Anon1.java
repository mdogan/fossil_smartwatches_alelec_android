package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wo2;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.enums.SyncErrorCode;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.usecase.GetWeather;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$Anon1", f = "GetWeather.kt", l = {31}, m = "invokeSuspend")
public final class GetWeather$executeUseCase$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather.c $requestValues;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetWeather$executeUseCase$Anon1(GetWeather getWeather, GetWeather.c cVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = getWeather;
        this.$requestValues = cVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        GetWeather$executeUseCase$Anon1 getWeather$executeUseCase$Anon1 = new GetWeather$executeUseCase$Anon1(this.this$Anon0, this.$requestValues, kc4);
        getWeather$executeUseCase$Anon1.p$ = (lh4) obj;
        return getWeather$executeUseCase$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GetWeather$executeUseCase$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            GetWeather$executeUseCase$Anon1$response$Anon1 getWeather$executeUseCase$Anon1$response$Anon1 = new GetWeather$executeUseCase$Anon1$response$Anon1(this, (kc4) null);
            this.L$Anon0 = lh4;
            this.label = 1;
            obj = ResponseKt.a(getWeather$executeUseCase$Anon1$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ro2 ro2 = (ro2) obj;
        if (ro2 instanceof so2) {
            wo2 wo2 = new wo2();
            wo2.a((yz1) ((so2) ro2).a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = GetWeather.f.a();
            local.d(a2, "onSuccess" + wo2.a());
            j62.d b = this.this$Anon0.a();
            Weather a3 = wo2.a();
            wd4.a((Object) a3, "mfWeatherResponse.weather");
            b.onSuccess(new GetWeather.d(a3));
        } else if (ro2 instanceof qo2) {
            this.this$Anon0.a().a(new GetWeather.b(SyncErrorCode.NETWORK_ERROR.name()));
        }
        return cb4.a;
    }
}
