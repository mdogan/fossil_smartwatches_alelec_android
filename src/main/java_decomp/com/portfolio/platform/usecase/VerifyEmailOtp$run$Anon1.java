package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.usecase.VerifyEmailOtp", f = "VerifyEmailOtp.kt", l = {21}, m = "run")
public final class VerifyEmailOtp$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ VerifyEmailOtp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifyEmailOtp$run$Anon1(VerifyEmailOtp verifyEmailOtp, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = verifyEmailOtp;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((VerifyEmailOtp.b) null, (kc4<Object>) this);
    }
}
