package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.blesdk.obfuscated.dk2;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.ik2;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.p8;
import com.fossil.blesdk.obfuscated.po;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class NotificationConfigurationSummaryView extends ViewGroup implements GestureDetector.OnGestureListener {
    @DexIgnore
    public static /* final */ int[] y; // = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public String g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Paint k;
    @DexIgnore
    public Rect l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public p8 w;
    @DexIgnore
    public a x;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public NotificationConfigurationSummaryView(Context context) {
        super(context);
        b();
    }

    @DexIgnore
    public void a(int i2, List<BaseFeatureModel> list) {
        AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        int i3 = this.s;
        layoutParams.width = i3;
        layoutParams.height = i3;
        appCompatImageView.setLayoutParams(layoutParams);
        dk2.a((View) appCompatImageView).a((Object) new ik2(list)).a((po<Bitmap>) new ok2()).a((ImageView) appCompatImageView);
        appCompatImageView.setTag(123456789, Integer.valueOf(i2));
        addView(appCompatImageView);
    }

    @DexIgnore
    public final boolean a(float f2, float f3, float f4) {
        return f3 <= f2 && f2 < f4;
    }

    @DexIgnore
    public final void b() {
        this.j = new Paint(1);
        this.j.setStyle(Paint.Style.FILL);
        this.j.setColor(this.h);
        this.k = new Paint(1);
        if (!TextUtils.isEmpty(this.g)) {
            this.k.setTypeface(Typeface.createFromAsset(getResources().getAssets(), this.g));
        }
        this.k.setTextSize((float) this.e);
        this.k.setColor(this.f);
        this.k.setStrokeWidth(2.0f);
        this.l = new Rect();
        this.w = new p8(getContext(), this);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (!this.r) {
            a();
            this.r = true;
        }
        a(canvas);
    }

    @DexIgnore
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @DexIgnore
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.v = (int) (((float) (Math.min(i4 - i2, i5 - i3) / 2)) - us3.a(40.0f));
        this.t = getWidth();
        this.u = getHeight();
        for (int i6 = 0; i6 < childCount; i6++) {
            AppCompatImageView appCompatImageView = (AppCompatImageView) getChildAt(i6);
            a(appCompatImageView, ((Integer) appCompatImageView.getTag(123456789)).intValue());
        }
    }

    @DexIgnore
    public void onLongPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onShowPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int a2 = a(a(motionEvent.getX(), motionEvent.getY()));
        a aVar = this.x;
        if (aVar == null) {
            return true;
        }
        aVar.a(a2);
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && this.w.a(motionEvent);
    }

    @DexIgnore
    public void setOnItemClickListener(a aVar) {
        this.x = aVar;
    }

    @DexIgnore
    public NotificationConfigurationSummaryView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i62.NotificationConfigurationSummaryView);
        this.e = (int) obtainStyledAttributes.getDimension(3, (float) us3.b(15.0f));
        this.f = obtainStyledAttributes.getColor(1, -1);
        this.g = obtainStyledAttributes.getString(2);
        this.h = obtainStyledAttributes.getColor(0, -65536);
        obtainStyledAttributes.recycle();
        b();
    }

    @DexIgnore
    public final void a(AppCompatImageView appCompatImageView, int i2) {
        double d = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.s;
        int cos = (int) (((double) ((this.t / 2) - (i3 / 2))) + (((double) this.v) * Math.cos(d)));
        int sin = (int) (((double) ((this.u / 2) - (i3 / 2))) + (((double) this.v) * Math.sin(d)));
        appCompatImageView.layout(cos, sin, cos + i3, i3 + sin);
    }

    @DexIgnore
    public final void a() {
        this.m = getMeasuredWidth();
        this.n = getMeasuredHeight();
        this.o = Math.min(this.m, this.n) / 2;
        this.p = (int) (((float) this.o) - us3.a(50.0f));
        this.q = (int) (((float) this.p) - us3.a(20.0f));
        int i2 = this.o;
        this.i = (int) (((float) i2) / 8.5f);
        this.s = (int) (((float) i2) / 3.5f);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        for (int i2 : y) {
            String valueOf = String.valueOf(i2);
            this.k.getTextBounds(valueOf, 0, valueOf.length(), this.l);
            double d = ((double) (i2 - 3)) * 0.5235987755982988d;
            int cos = (int) ((((double) (this.m / 2)) + (Math.cos(d) * ((double) this.q))) - ((double) (this.l.width() / 2)));
            int sin = (int) (((double) (this.n / 2)) + (Math.sin(d) * ((double) this.q)) + ((double) (this.l.height() / 2)));
            this.j.setColor(-1);
            canvas.drawCircle((float) ((this.l.width() / 2) + cos), (float) (sin - (this.l.height() / 2)), (float) this.i, this.j);
            this.j.setColor(this.h);
            canvas.drawCircle((float) ((this.l.width() / 2) + cos), (float) (sin - (this.l.height() / 2)), ((float) this.i) - us3.a(1.0f), this.j);
            canvas.drawText(valueOf, ((float) cos) + ((((float) this.l.width()) - this.k.measureText(valueOf)) / 2.0f), (float) sin, this.k);
        }
    }

    @DexIgnore
    public final float a(float f2, float f3) {
        return ((float) Math.toDegrees(Math.atan2((double) (f3 - ((float) (getHeight() / 2))), (double) (f2 - ((float) (getWidth() / 2)))))) + 90.0f;
    }

    @DexIgnore
    public final int a(float f2) {
        if (a(f2, -15.0f, 15.0f)) {
            return 12;
        }
        if (a(f2, 15.0f, 45.0f)) {
            return 1;
        }
        if (a(f2, 45.0f, 75.0f)) {
            return 2;
        }
        if (a(f2, 75.0f, 105.0f)) {
            return 3;
        }
        if (a(f2, 105.0f, 135.0f)) {
            return 4;
        }
        if (a(f2, 135.0f, 165.0f)) {
            return 5;
        }
        if (a(f2, 165.0f, 195.0f)) {
            return 6;
        }
        if (a(f2, 195.0f, 225.0f)) {
            return 7;
        }
        if (a(f2, 225.0f, 255.0f)) {
            return 8;
        }
        if (a(f2, 255.0f, 270.0f) || a(f2, -90.0f, -70.0f)) {
            return 9;
        }
        if (a(f2, -70.0f, -40.0f)) {
            return 10;
        }
        return a(f2, -40.0f, -15.0f) ? 11 : 0;
    }
}
