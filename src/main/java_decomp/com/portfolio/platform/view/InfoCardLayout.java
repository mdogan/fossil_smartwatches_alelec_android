package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InfoCardLayout extends FrameLayout {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ InfoCardLayout e;

        @DexIgnore
        public a(InfoCardLayout infoCardLayout) {
            this.e = infoCardLayout;
        }

        @DexIgnore
        public void onGlobalLayout() {
            this.e.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            InfoCardLayout infoCardLayout = this.e;
            BottomSheetBehavior b = BottomSheetBehavior.b(infoCardLayout);
            wd4.a((Object) b, "BottomSheetBehavior.from(this@InfoCardLayout)");
            infoCardLayout.setMBottomSheetBehavior$app_fossilRelease(b);
            this.e.getMBottomSheetBehavior$app_fossilRelease().b(false);
            this.e.getMBottomSheetBehavior$app_fossilRelease().b(0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InfoCardLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wd4.b(context, "context");
        getViewTreeObserver().addOnGlobalLayoutListener(new a(this));
    }

    @DexIgnore
    public final BottomSheetBehavior<FrameLayout> getMBottomSheetBehavior$app_fossilRelease() {
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.e;
        if (bottomSheetBehavior != null) {
            return bottomSheetBehavior;
        }
        wd4.d("mBottomSheetBehavior");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        wd4.b(motionEvent, Constants.EVENT);
        return true;
    }

    @DexIgnore
    public final void setMBottomSheetBehavior$app_fossilRelease(BottomSheetBehavior<FrameLayout> bottomSheetBehavior) {
        wd4.b(bottomSheetBehavior, "<set-?>");
        this.e = bottomSheetBehavior;
    }
}
