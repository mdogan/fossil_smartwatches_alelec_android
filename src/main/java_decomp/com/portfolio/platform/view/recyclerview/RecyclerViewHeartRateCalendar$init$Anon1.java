package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.bs3;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RecyclerViewHeartRateCalendar$init$Anon1 extends GridLayoutManager {
    @DexIgnore
    public /* final */ /* synthetic */ Context P;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar$init$Anon1(Context context, Context context2, int i, int i2, boolean z) {
        super(context2, i, i2, z);
        this.P = context;
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public int k(RecyclerView.State state) {
        wd4.b(state, "state");
        return bs3.a(this.P);
    }
}
