package com.portfolio.platform.view.recyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ye;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class RecyclerViewPager extends RecyclerView {
    @DexIgnore
    public int N0;
    @DexIgnore
    public boolean O0;
    @DexIgnore
    public boolean P0;
    @DexIgnore
    public /* final */ int Q0;
    @DexIgnore
    public int R0;
    @DexIgnore
    public /* final */ int S0;
    @DexIgnore
    public boolean T0;
    @DexIgnore
    public float U0;
    @DexIgnore
    public float V0;
    @DexIgnore
    public float W0;
    @DexIgnore
    public float X0;
    @DexIgnore
    public int Y0;
    @DexIgnore
    public /* final */ LinearLayoutManager Z0;
    @DexIgnore
    public a a1;
    @DexIgnore
    public int b1;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public RecyclerViewPager(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private void J() {
        this.N0 = -1;
        P();
    }

    @DexIgnore
    public final void P() {
        this.O0 = false;
        this.P0 = false;
    }

    @DexIgnore
    public final boolean a(View view, boolean z, int i, int i2, int i3) {
        View view2 = view;
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                int i4 = i2 + scrollX;
                if (i4 >= childAt.getLeft() && i4 < childAt.getRight()) {
                    int i5 = i3 + scrollY;
                    if (i5 >= childAt.getTop() && i5 < childAt.getBottom()) {
                        if (a(childAt, true, i, i4 - childAt.getLeft(), i5 - childAt.getTop())) {
                            return true;
                        }
                    }
                }
            }
        }
        if (!z || !view.canScrollHorizontally(-i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean b(float f, float f2) {
        return (f < ((float) this.R0) && f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) || (f > ((float) (getWidth() - this.R0)) && f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void d(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.N0) {
            int i = actionIndex == 0 ? 1 : 0;
            this.U0 = motionEvent.getX(i);
            this.N0 = motionEvent.getPointerId(i);
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z);
        }
    }

    @DexIgnore
    public void i(int i, int i2) {
        super.i(i, i2);
        int G = this.Z0.G();
        if (this.b1 != G && G >= 0) {
            this.b1 = G;
            a aVar = this.a1;
            if (aVar != null) {
                aVar.a(G);
            }
        }
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            J();
            return false;
        }
        if (action != 0) {
            if (this.O0) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            if (this.P0) {
                return false;
            }
        }
        if (action == 0) {
            float x = motionEvent.getX();
            this.W0 = x;
            this.U0 = x;
            float y = motionEvent.getY();
            this.X0 = y;
            this.V0 = y;
            this.N0 = motionEvent2.getPointerId(0);
            this.P0 = false;
            if (this.Y0 == 2) {
                this.O0 = true;
                e(true);
                this.Y0 = 1;
            } else {
                this.O0 = false;
            }
        } else if (action == 2) {
            int i = this.N0;
            if (i != -1) {
                int findPointerIndex = motionEvent2.findPointerIndex(i);
                float x2 = motionEvent2.getX(findPointerIndex);
                float f = x2 - this.U0;
                float abs = Math.abs(f);
                float y2 = motionEvent2.getY(findPointerIndex);
                float abs2 = Math.abs(y2 - this.X0);
                int i2 = (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i2 != 0 && !b(this.U0, f)) {
                    if (a(this, false, (int) f, (int) x2, (int) y2)) {
                        this.U0 = x2;
                        this.V0 = y2;
                        this.P0 = true;
                        return false;
                    }
                }
                if (abs > ((float) this.S0) && abs * 0.5f > abs2) {
                    this.O0 = true;
                    e(true);
                    this.Y0 = 1;
                    float f2 = this.W0;
                    float f3 = (float) this.S0;
                    this.U0 = i2 > 0 ? f2 + f3 : f2 - f3;
                    this.V0 = y2;
                } else if (abs2 > ((float) this.S0)) {
                    this.P0 = true;
                }
            }
        } else if (action == 6) {
            d(motionEvent);
        }
        if (!super.onInterceptTouchEvent(motionEvent) || !this.O0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.R0 = Math.min(getMeasuredWidth() / 10, this.Q0);
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.T0 || ((motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) || getAdapter() == null || getAdapter().getItemCount() == 0)) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action == 0) {
            float x = motionEvent.getX();
            this.W0 = x;
            this.U0 = x;
            float y = motionEvent.getY();
            this.X0 = y;
            this.V0 = y;
            this.N0 = motionEvent.getPointerId(0);
        } else if (action != 1) {
            if (action != 2) {
                if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.U0 = motionEvent.getX(actionIndex);
                        this.N0 = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        d(motionEvent);
                        this.U0 = motionEvent.getX(motionEvent.findPointerIndex(this.N0));
                    }
                } else if (this.O0) {
                    J();
                }
            } else if (!this.O0) {
                int findPointerIndex = motionEvent.findPointerIndex(this.N0);
                if (findPointerIndex == -1) {
                    J();
                } else {
                    float x2 = motionEvent.getX(findPointerIndex);
                    float abs = Math.abs(x2 - this.U0);
                    float y2 = motionEvent.getY(findPointerIndex);
                    float abs2 = Math.abs(y2 - this.V0);
                    if (abs > ((float) this.S0) && abs > abs2) {
                        this.O0 = true;
                        e(true);
                        float f = this.W0;
                        this.U0 = x2 - f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? f + ((float) this.S0) : f - ((float) this.S0);
                        this.V0 = y2;
                        this.Y0 = 1;
                        ViewParent parent = getParent();
                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }
                    }
                }
            }
        } else if (this.O0) {
            J();
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setOnPageChangeListener(a aVar) {
        this.a1 = aVar;
    }

    @DexIgnore
    public RecyclerViewPager(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public RecyclerViewPager(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.N0 = -1;
        this.T0 = false;
        this.Y0 = 0;
        this.Z0 = new LinearLayoutManager(context, 0, false);
        setLayoutManager(this.Z0);
        new ye().a((RecyclerView) this);
        this.Q0 = (int) (PortfolioApp.R.getResources().getDisplayMetrics().density * 16.0f);
        this.S0 = ViewConfiguration.get(context).getScaledPagingTouchSlop();
    }

    @DexIgnore
    public void d(boolean z) {
        this.T0 = z;
    }

    @DexIgnore
    public void i(int i) {
        if (i == this.Z0.G()) {
            i(0, 0);
        } else {
            super.i(i);
        }
    }
}
