package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xr3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ ArrayList<SleepSessionData> B;
    @DexIgnore
    public DrawMode C;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public enum DrawMode {
        All,
        Awake,
        Light,
        Restful
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = SleepDayDetailsChart.class.getSimpleName();
        wd4.a((Object) simpleName, "SleepDayDetailsChart::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wd4.b(context, "context");
        this.n = k6.a(context, (int) R.color.sleepDeep);
        this.o = context.getResources().getDimensionPixelSize(R.dimen.sp12);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = context.getResources().getDimensionPixelSize(R.dimen.dp3);
        this.y = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.z = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.A = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.B = new ArrayList<>();
        this.C = DrawMode.All;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, i62.SleepDayDetailsChart, 0, 0));
        }
        int a2 = k6.a(context, (int) R.color.gray_30);
        int i3 = R.font.font_regular;
        TypedArray mTypedArray = getMTypedArray();
        this.h = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.i = mTypedArray2 != null ? mTypedArray2.getColor(4, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.j = mTypedArray3 != null ? mTypedArray3.getColor(2, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.k = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.l = mTypedArray5 != null ? mTypedArray5.getColor(5, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.m = mTypedArray6 != null ? mTypedArray6.getColor(1, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.p = mTypedArray7 != null ? mTypedArray7.getResourceId(6, R.font.font_regular) : i3;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 2), this.w);
        canvas.drawRect(new Rect(0, i3 - 2, getWidth(), i3), this.w);
    }

    @DexIgnore
    public final void b(Canvas canvas, int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "positionX: " + i2 + ", positionY: " + i3);
        canvas.drawBitmap(getStartBitmap(), (float) (i2 - (getStartBitmap().getWidth() / 2)), (float) this.y, this.t);
        Path path = new Path();
        float f = (float) i2;
        path.moveTo(f, (float) (this.y + getStartBitmap().getHeight()));
        path.lineTo(f, (float) i3);
        canvas.drawPath(path, this.v);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        boolean z2;
        int i5;
        int i6;
        Canvas canvas2;
        RectF rectF;
        SleepDayDetailsChart sleepDayDetailsChart;
        int i7;
        SleepDayDetailsChart sleepDayDetailsChart2 = this;
        Canvas canvas3 = canvas;
        super.draw(canvas);
        if (canvas3 != null) {
            canvas3.drawColor(-1);
            Iterator<SleepSessionData> it = sleepDayDetailsChart2.B.iterator();
            int i8 = 0;
            int i9 = 0;
            int i10 = Integer.MAX_VALUE;
            while (it.hasNext()) {
                SleepSessionData next = it.next();
                int component1 = next.component1();
                String component2 = next.component2();
                String component3 = next.component3();
                i9 += component1;
                Rect rect = new Rect();
                Rect rect2 = new Rect();
                sleepDayDetailsChart2.u.getTextBounds(component2, 0, StringsKt__StringsKt.c(component2), rect);
                sleepDayDetailsChart2.u.getTextBounds(component3, 0, StringsKt__StringsKt.c(component3), rect2);
                i10 = Math.min(rect2.height(), Math.min(rect.height(), i10));
                i8++;
            }
            int width = getWidth() - (sleepDayDetailsChart2.y * ((i8 > 0 ? i8 - 1 : 0) + 2));
            int height = (getHeight() - i10) - (sleepDayDetailsChart2.y * 2);
            if (height < 0) {
                height = getHeight() - (sleepDayDetailsChart2.y * 2);
            }
            int i11 = sleepDayDetailsChart2.y;
            int size = sleepDayDetailsChart2.B.size();
            int i12 = i11;
            int i13 = 0;
            int i14 = 0;
            boolean z3 = false;
            while (i13 < size) {
                SleepSessionData sleepSessionData = sleepDayDetailsChart2.B.get(i13);
                wd4.a((Object) sleepSessionData, "mListSleepSession[sleepDataId]");
                SleepSessionData sleepSessionData2 = sleepSessionData;
                float measureText = sleepDayDetailsChart2.u.measureText(sleepSessionData2.getEndTimeString());
                int i15 = i10 / 2;
                int i16 = i10;
                canvas3.drawText(sleepSessionData2.getStartTimeString(), (float) i12, (float) (getHeight() - i15), sleepDayDetailsChart2.u);
                int durationInMinutes = sleepSessionData2.getDurationInMinutes();
                float f = (float) durationInMinutes;
                float f2 = f / ((float) i9);
                float f3 = ((float) width) * f2;
                int i17 = i9;
                int i18 = size;
                FLogger.INSTANCE.getLocal().d(E, "sessionPercentage: " + f2 + ", realWidth: " + width + ", sessionWidth: " + f3);
                List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
                int size2 = sleepStates.size();
                int i19 = i12;
                int i20 = i14;
                int i21 = 0;
                while (i21 < size2) {
                    int i22 = width;
                    int i23 = sleepStates.get(i21).state;
                    int i24 = size2;
                    int i25 = i13;
                    float f4 = measureText;
                    SleepSessionData sleepSessionData3 = sleepSessionData2;
                    if (i21 < sleepStates.size() - 1) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        z2 = z3;
                        String str = E;
                        i4 = i20;
                        StringBuilder sb = new StringBuilder();
                        sb.append("From ");
                        sb.append(sleepStates.get(i21).index);
                        sb.append(" to ");
                        int i26 = i21 + 1;
                        i3 = i2;
                        sb.append(sleepStates.get(i26).index);
                        sb.append(": stateType ");
                        sb.append(i23);
                        local.d(str, sb.toString());
                        i5 = ((int) sleepStates.get(i26).index) - ((int) sleepStates.get(i21).index);
                    } else {
                        i3 = i2;
                        i4 = i20;
                        z2 = z3;
                        FLogger.INSTANCE.getLocal().d(E, "From " + sleepStates.get(i21).index + " to " + durationInMinutes + ": stateType " + i23);
                        i5 = durationInMinutes - ((int) sleepStates.get(i21).index);
                    }
                    int i27 = (int) ((((float) i5) / f) * f3);
                    if (i27 < 1) {
                        i27 = 1;
                    }
                    if (i23 == 0) {
                        sleepDayDetailsChart = this;
                        canvas2 = canvas;
                        i2 = i3;
                        i6 = durationInMinutes;
                        float f5 = (float) i2;
                        rectF = new RectF((float) i19, (float) ((int) (0.8f * f5)), (float) (i19 + i27), f5);
                        Paint paint = sleepDayDetailsChart.q;
                        DrawMode drawMode = sleepDayDetailsChart.C;
                        if (drawMode == DrawMode.All) {
                            if (i21 == 0) {
                                if (sleepStates.get(i21 + 1).state != 0) {
                                    xr3.a(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                                } else {
                                    xr3.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                                }
                            } else if (i21 != sleepStates.size() - 1) {
                                int i28 = i21 - 1;
                                if (sleepStates.get(i28).state != 0 && sleepStates.get(i21 + 1).state != 0) {
                                    canvas2.drawRect(rectF, paint);
                                } else if (sleepStates.get(i28).state != 0) {
                                    xr3.b(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                                } else if (sleepStates.get(i21 + 1).state != 0) {
                                    xr3.a(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                                } else {
                                    xr3.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                                }
                            } else if (sleepStates.get(i21 - 1).state != 0) {
                                xr3.b(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                            } else {
                                xr3.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                            }
                        } else if (drawMode == DrawMode.Awake) {
                            xr3.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.x);
                        }
                        i7 = i4 + i5;
                        if (i7 < sleepDayDetailsChart.D && !z2) {
                            sleepDayDetailsChart.b(canvas2, (int) (rectF.left + (rectF.width() / ((float) 2))), i2);
                            z2 = true;
                        }
                        i19 += i27;
                        i21++;
                        sleepDayDetailsChart2 = sleepDayDetailsChart;
                        i13 = i25;
                        width = i22;
                        size2 = i24;
                        measureText = f4;
                        sleepSessionData2 = sleepSessionData3;
                        z3 = z2;
                        durationInMinutes = i6;
                        Canvas canvas4 = canvas2;
                        i20 = i7;
                        canvas3 = canvas4;
                    } else if (i23 != 1) {
                        i2 = i3;
                        float f6 = (float) i2;
                        rectF = new RectF((float) i19, (float) ((int) (0.3f * f6)), (float) (i19 + i27), f6);
                        sleepDayDetailsChart = this;
                        DrawMode drawMode2 = sleepDayDetailsChart.C;
                        if (drawMode2 == DrawMode.All || drawMode2 == DrawMode.Restful) {
                            canvas2 = canvas;
                            xr3.c(canvas2, rectF, sleepDayDetailsChart.s, (float) sleepDayDetailsChart.x);
                        } else {
                            canvas2 = canvas;
                        }
                        i6 = durationInMinutes;
                    } else {
                        sleepDayDetailsChart = this;
                        canvas2 = canvas;
                        i2 = i3;
                        Paint paint2 = sleepDayDetailsChart.r;
                        float f7 = (float) i2;
                        i6 = durationInMinutes;
                        RectF rectF2 = new RectF((float) i19, (float) ((int) (0.55f * f7)), (float) (i19 + i27), f7);
                        DrawMode drawMode3 = sleepDayDetailsChart.C;
                        if (drawMode3 == DrawMode.All) {
                            if (i21 == 0) {
                                if (sleepStates.get(i21 + 1).state != 0) {
                                    xr3.a(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                                } else {
                                    xr3.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                                }
                            } else if (i21 != sleepStates.size() - 1) {
                                int i29 = i21 - 1;
                                if (sleepStates.get(i29).state != 0 && sleepStates.get(i21 + 1).state != 0) {
                                    canvas2.drawRect(rectF2, paint2);
                                } else if (sleepStates.get(i29).state != 0) {
                                    xr3.b(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                                } else if (sleepStates.get(i21 + 1).state != 0) {
                                    xr3.a(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                                } else {
                                    xr3.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                                }
                            } else if (sleepStates.get(i21 - 1).state != 0) {
                                xr3.b(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                            } else {
                                xr3.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                            }
                        } else if (drawMode3 == DrawMode.Light) {
                            xr3.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.x);
                        }
                        rectF = rectF2;
                    }
                    i7 = i4 + i5;
                    if (i7 < sleepDayDetailsChart.D) {
                    }
                    i19 += i27;
                    i21++;
                    sleepDayDetailsChart2 = sleepDayDetailsChart;
                    i13 = i25;
                    width = i22;
                    size2 = i24;
                    measureText = f4;
                    sleepSessionData2 = sleepSessionData3;
                    z3 = z2;
                    durationInMinutes = i6;
                    Canvas canvas42 = canvas2;
                    i20 = i7;
                    canvas3 = canvas42;
                }
                int i30 = width;
                int i31 = i20;
                SleepDayDetailsChart sleepDayDetailsChart3 = sleepDayDetailsChart2;
                Canvas canvas5 = canvas3;
                canvas5.drawText(sleepSessionData2.getEndTimeString(), ((float) i19) - measureText, (float) (getHeight() - i15), sleepDayDetailsChart3.u);
                i12 = i19 + sleepDayDetailsChart3.y;
                i13++;
                sleepDayDetailsChart2 = sleepDayDetailsChart3;
                canvas3 = canvas5;
                i10 = i16;
                size = i18;
                i9 = i17;
                width = i30;
                z3 = z3;
                i14 = i31;
            }
            sleepDayDetailsChart2.a(canvas3, 0, i2);
            return;
        }
        SleepDayDetailsChart sleepDayDetailsChart4 = sleepDayDetailsChart2;
    }

    @DexIgnore
    public int getStarIconResId() {
        return R.drawable.ic_goal_not_meet;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.A;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.w.setColor(this.h);
        float f = (float) 2;
        this.w.setStrokeWidth(f);
        this.q.setColor(this.k);
        this.q.setStyle(Paint.Style.FILL);
        this.q.setStrokeWidth((float) this.z);
        this.r.setColor(this.j);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setStrokeWidth((float) this.z);
        this.s.setColor(this.i);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setStrokeWidth((float) this.z);
        this.u.setColor(this.l);
        this.u.setStyle(Paint.Style.FILL);
        this.u.setTextSize((float) this.o);
        this.u.setTypeface(r6.a(getContext(), this.p));
        this.v.setColor(this.m);
        this.v.setStyle(Paint.Style.STROKE);
        this.v.setStrokeWidth(f);
        this.v.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.t.setStyle(Paint.Style.FILL);
        this.t.setColorFilter(new PorterDuffColorFilter(this.n, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wd4.b(context, "context");
    }
}
