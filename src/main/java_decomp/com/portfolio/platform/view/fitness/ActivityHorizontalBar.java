package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public Typeface w;
    @DexIgnore
    public RectF x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wd4.b(context, "context");
        this.l = "";
        this.n = 5.0f;
        this.o = 8.0f;
        this.p = 8.0f;
        this.q = 1.0f;
        this.r = 8;
        this.s = -1;
        this.t = 10;
        this.v = true;
        this.x = new RectF();
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, i62.ActivityHorizontalBar, 0, 0);
            if (obtainStyledAttributes != null) {
                this.k = obtainStyledAttributes.getColor(0, 0);
                this.m = obtainStyledAttributes.getColor(2, 0);
                this.n = (float) obtainStyledAttributes.getDimensionPixelSize(4, 5);
                this.o = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
                this.p = (float) obtainStyledAttributes.getDimensionPixelSize(3, 8);
                this.q = obtainStyledAttributes.getFloat(6, 1.0f);
                this.s = obtainStyledAttributes.getResourceId(7, -1);
                this.r = obtainStyledAttributes.getDimensionPixelSize(8, 8);
                String string = obtainStyledAttributes.getString(9);
                this.l = string == null ? "" : string;
                this.u = obtainStyledAttributes.getColor(10, 0);
                this.t = obtainStyledAttributes.getDimensionPixelSize(11, 10);
                try {
                    this.w = r6.a(getContext(), obtainStyledAttributes.getResourceId(1, -1));
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ActivityHorizontalBar", "init - e=" + e);
                } catch (Throwable th) {
                    obtainStyledAttributes.recycle();
                    throw th;
                }
                obtainStyledAttributes.recycle();
            }
        }
        c();
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f) {
        float f2 = rectF.left + f;
        float f3 = rectF.top + f;
        float f4 = rectF.right - f;
        float f5 = rectF.bottom - f;
        float f6 = (float) 0;
        if (f4 <= f6 || f4 < f2) {
            f4 = f2 + f;
        }
        if (f5 <= f6 || f5 < f3) {
            f5 = f3 + f;
        }
        return new RectF(f2, f3, f4, f5);
    }

    @DexIgnore
    public final void c() {
        this.G.setAntiAlias(true);
        this.G.setStyle(Paint.Style.STROKE);
        this.C.setColor(this.m);
        this.C.setAntiAlias(true);
        this.C.setStyle(Paint.Style.FILL);
        this.E.setAlpha((int) (this.q * ((float) 255)));
        this.E.setColorFilter(new PorterDuffColorFilter(this.m, PorterDuff.Mode.SRC_IN));
        this.E.setAntiAlias(true);
        this.F.setColor(-1);
        this.F.setAntiAlias(true);
        this.F.setStyle(Paint.Style.FILL);
        this.D.setColor(this.u);
        this.D.setAntiAlias(true);
        this.D.setStyle(Paint.Style.FILL);
        this.D.setTextSize((float) this.t);
        Typeface typeface = this.w;
        if (typeface != null) {
            this.D.setTypeface(typeface);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return this.s;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.r;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityHorizontalBar", "onDraw - mValue=" + this.i + ", mGoal=" + this.j + ", mMax=" + this.h);
        this.y = ((float) getWidth()) - this.p;
        if (canvas != null) {
            canvas.drawColor(this.k);
            RectF rectF = this.x;
            float f = this.h;
            float f2 = (float) 0;
            float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f4 = f > f2 ? (this.y * this.i) / f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f5 = this.o;
            float f6 = (float) 2;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4 + (f5 * f6), f5 * ((float) 3));
            float f7 = this.i;
            if (f7 > f2) {
                if (f7 >= this.j) {
                    this.C.setAlpha((int) 15.299999999999999d);
                    RectF rectF2 = this.x;
                    float f8 = (float) 4;
                    float f9 = this.n;
                    canvas.drawRoundRect(rectF2, f8 * f9, f8 * f9, this.C);
                    this.C.setAlpha((int) 40.800000000000004d);
                    RectF a2 = a(this.x, this.o / f6);
                    float f10 = this.n;
                    canvas.drawRoundRect(a2, f10 * 2.0f, f10 * 2.0f, this.C);
                }
                this.C.setAlpha(255);
                RectF a3 = a(this.x, this.o);
                float f11 = this.n;
                canvas.drawRoundRect(a3, f11, f11, this.C);
            }
            float height = ((float) getHeight()) / 2.0f;
            float f12 = this.h;
            if (f12 > f2) {
                f3 = (this.y * this.j) / f12;
            }
            float f13 = this.o;
            this.z = f3 + f13;
            if (this.i <= this.j) {
                canvas.drawBitmap(getStartBitmap(), this.z - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.E);
            } else {
                canvas.drawCircle(this.z - (((float) this.r) / 2.0f), height, f13 / 4.0f, this.F);
            }
            if (this.v) {
                canvas.drawText(this.l, ((float) getWidth()) - this.A, height + (this.B / f6), this.D);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.o * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context) {
        this(context, (AttributeSet) null, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wd4.b(context, "context");
    }
}
