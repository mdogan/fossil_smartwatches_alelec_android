package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xr3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public /* final */ ArrayList<Pair<Float, Float>> A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public /* final */ String[] h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = ActivityWeekDetailsChart.class.getSimpleName();
        wd4.a((Object) simpleName, "ActivityWeekDetailsChart::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wd4.b(context, "context");
        String[] stringArray = context.getResources().getStringArray(R.array.days_of_week_alphabet);
        wd4.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.h = stringArray;
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.v = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.w = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.x = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.y = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.z = 4;
        this.A = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, i62.ActivityWeekDetailsChart, 0, 0));
        }
        int a2 = k6.a(context, (int) R.color.gray_30);
        String string = context.getString(R.string.font_path_regular);
        wd4.a((Object) string, "context.getString(R.string.font_path_regular)");
        TypedArray mTypedArray = getMTypedArray();
        this.i = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.j = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.k = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.l = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.m = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.o = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        if (mTypedArray7 != null) {
            String string2 = mTypedArray7.getString(5);
            if (string2 != null) {
                string = string2;
            }
        }
        this.n = string;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxSleepMinutes());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((Pair) t2).getSecond()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((Pair) next).getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        Pair pair = (Pair) t2;
        if (pair != null) {
            Float f = (Float) pair.getSecond();
            if (f != null) {
                return f.floatValue();
            }
        }
        return this.B;
    }

    @DexIgnore
    private final float getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((Pair) t2).getFirst()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((Pair) next).getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        Pair pair = (Pair) t2;
        if (pair != null) {
            Float f = (Float) pair.getFirst();
            if (f != null) {
                return f.floatValue();
            }
        }
        return this.C;
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, kd4<? super Integer, ? super List<Integer>, cb4> kd4) {
        int length = this.h.length;
        Rect rect = new Rect();
        String str = this.h[0];
        sd4 sd4 = sd4.a;
        Paint paint = this.r;
        wd4.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, StringsKt__StringsKt.c(str) > 0 ? StringsKt__StringsKt.c(str) : 1, rect);
        float measureText = this.r.measureText(str);
        float height = (float) rect.height();
        float f = (((float) (i2 - (this.v * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f2 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f2);
        float f3 = (float) this.v;
        LinkedList linkedList = new LinkedList();
        for (String drawText : this.h) {
            canvas.drawText(drawText, f3, height2, this.r);
            linkedList.add(Integer.valueOf((int) ((measureText / f2) + f3)));
            f3 += measureText + f;
        }
        kd4.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.y * 2);
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            a(canvas, width, (kd4<? super Integer, ? super List<Integer>, cb4>) new ActivityWeekDetailsChart$draw$Anon1$Anon1(ref$IntRef, ref$ObjectRef));
            int height = (getHeight() - ref$IntRef.element) - this.w;
            List list = (List) ref$ObjectRef.element;
            if (list != null) {
                a(canvas, (List<Integer>) list, height);
                a(canvas, list, width, height, height - 4);
            }
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.u;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.i);
        float f = (float) 4;
        this.p.setStrokeWidth(f);
        this.r.setColor(this.m);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.o);
        Paint paint = this.r;
        Context context = getContext();
        wd4.a((Object) context, "context");
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), this.n));
        this.q.setColor(this.j);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.s.setColor(this.k);
        this.s.setStrokeWidth((float) this.x);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.l);
        this.t.setStrokeWidth((float) this.x);
        this.t.setStyle(Paint.Style.FILL);
        this.z = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.A.isEmpty()) && this.A.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                int size = list.size();
                float height = (float) getStartBitmap().getHeight();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = D;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(((Number) this.A.get(i6).getSecond()).floatValue());
                    sb.append(", current goal: ");
                    sb.append(((Number) this.A.get(i5).getSecond()).floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list2.get(i5).intValue();
                    float f = (float) i3;
                    float f2 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (((Number) this.A.get(i5).getSecond()).floatValue() / mChartMax)) * f, f2), (float) this.z);
                    float intValue2 = (float) list2.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (((Number) this.A.get(i6).getSecond()).floatValue() / mChartMax)) * f, f2), (float) this.z);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.y), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas2.drawPath(path, this.q);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas2.drawPath(path, this.q);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas2.drawPath(path, this.q);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.y), max);
                            canvas2.drawPath(path, this.q);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.y), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "drawBars: " + list.size());
        if (!this.A.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                int intValue = list.get(i3).intValue();
                Pair<Float, Float> pair = this.A.get(i3);
                wd4.a((Object) pair, "mDataList[index]");
                float floatValue = ((Number) pair.getFirst()).floatValue() / mChartMax;
                float f = (float) i2;
                int i4 = intValue - (this.x / 2);
                xr3.c(canvas, new RectF((float) i4, Math.max(f - (floatValue * f), (float) this.z), (float) (i4 + this.x), f), floatValue < 1.0f ? this.s : this.t, ((float) this.x) / ((float) 3));
            }
        }
    }
}
