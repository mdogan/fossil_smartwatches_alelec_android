package com.portfolio.platform.view.fitness;

import android.graphics.RectF;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$FloatRef;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDayDetailsChart$draw$Anon1$Anon1$Anon1 extends Lambda implements jd4<RectF, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$FloatRef $goalDashLineBottom;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$FloatRef $goalDashLineX;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $goalPosition;
    @DexIgnore
    public /* final */ /* synthetic */ int $leftIndex;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart$draw$Anon1$Anon1$Anon1(int i, Ref$IntRef ref$IntRef, Ref$FloatRef ref$FloatRef, Ref$FloatRef ref$FloatRef2) {
        super(1);
        this.$leftIndex = i;
        this.$goalPosition = ref$IntRef;
        this.$goalDashLineBottom = ref$FloatRef;
        this.$goalDashLineX = ref$FloatRef2;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((RectF) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(RectF rectF) {
        wd4.b(rectF, "drawnRect");
        if (rectF.height() > ((float) 0) && this.$leftIndex == this.$goalPosition.element) {
            this.$goalDashLineBottom.element = rectF.top;
            this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
        }
    }
}
