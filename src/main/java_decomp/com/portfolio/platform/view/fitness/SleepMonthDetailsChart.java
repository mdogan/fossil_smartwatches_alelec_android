package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xr3;
import com.fossil.blesdk.obfuscated.xt3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public GoalType A;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public /* final */ String o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = SleepMonthDetailsChart.class.getSimpleName();
        wd4.a((Object) simpleName, "SleepMonthDetailsChart::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wd4.b(context, "context");
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.w = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.x = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.y = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.z = context.getResources().getDimensionPixelSize(R.dimen.dp3);
        this.A = GoalType.TOTAL_SLEEP;
        this.B = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, i62.SleepMonthDetailsChart, 0, 0));
        }
        int a2 = k6.a(context, (int) R.color.gray_30);
        String string = context.getString(R.string.font_path_regular);
        wd4.a((Object) string, "context.getString(R.string.font_path_regular)");
        TypedArray mTypedArray = getMTypedArray();
        this.h = mTypedArray != null ? mTypedArray.getColor(4, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.i = mTypedArray2 != null ? mTypedArray2.getColor(3, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.j = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.k = mTypedArray4 != null ? mTypedArray4.getColor(1, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.l = mTypedArray5 != null ? mTypedArray5.getColor(2, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.m = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.n = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            String string2 = mTypedArray8.getString(6);
            if (string2 != null) {
                string = string2;
            }
        }
        this.o = string;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = xt3.b[this.A.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "Max of awake: " + this.E);
            i2 = this.E;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = F;
            local2.d(str2, "Max of light: " + this.D);
            i2 = this.D;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = F;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = F;
            local4.d(str4, "Max of restful: " + this.C);
            i2 = this.C;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int sleepGoal = ((SleepDayData) t2).getSleepGoal();
                do {
                    T next = it.next();
                    int sleepGoal2 = ((SleepDayData) next).getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        t2 = next;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = ((SleepDayData) t2).getTotalSleepMinutes();
                do {
                    T next = it.next();
                    int totalSleepMinutes2 = ((SleepDayData) next).getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        t2 = next;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = xt3.a[this.A.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            Rect rect = new Rect();
            this.r.getTextBounds(AppEventsConstants.EVENT_PARAM_VALUE_YES, 0, StringsKt__StringsKt.c(AppEventsConstants.EVENT_PARAM_VALUE_YES) > 0 ? StringsKt__StringsKt.c(AppEventsConstants.EVENT_PARAM_VALUE_YES) : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.x)) - ((float) this.z));
            int width = getWidth() - getStartBitmap().getWidth();
            int i2 = this.x;
            int i3 = this.y;
            int i4 = (width - (i2 * 2)) - i3;
            a(canvas, i4, height, i3, i2 * 2, (jd4<? super LinkedList<Integer>, cb4>) new SleepMonthDetailsChart$draw$$inlined$let$lambda$Anon1(i4, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.h);
        float f = (float) 4;
        this.p.setStrokeWidth(f);
        this.r.setColor(this.m);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.n);
        Paint paint = this.r;
        Context context = getContext();
        wd4.a((Object) context, "context");
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), this.o));
        this.q.setColor(this.i);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.s.setColor(this.j);
        this.s.setStrokeWidth((float) this.w);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.k);
        this.t.setStrokeWidth((float) this.w);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.l);
        this.u.setStrokeWidth((float) this.w);
        this.u.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, jd4<? super LinkedList<Integer>, cb4> jd4) {
        float f;
        int i6 = i3;
        if (!this.B.isEmpty()) {
            int size = i2 / this.B.size();
            int i7 = this.w;
            int i8 = size < i7 ? size : i7;
            int i9 = i8 / 2;
            LinkedList linkedList = new LinkedList();
            float mChartMax = (float) getMChartMax();
            Iterator<SleepDayData> it = this.B.iterator();
            int i10 = i4;
            while (it.hasNext()) {
                SleepDayData next = it.next();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = F;
                local.d(str, "Actual sleep: " + next.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int i11 = i10 + (i9 / 2);
                int totalSleepMinutes = next.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int i12 = totalSleepMinutes;
                    f = mChartMax;
                    int dayAwake = next.getDayAwake();
                    a(canvas, this.s, i11, i3, (int) ((((float) dayAwake) / f) * ((float) i6)), i9, i5, dayAwake, i12);
                } else if (mSleepMode == 1) {
                    int i13 = totalSleepMinutes;
                    f = mChartMax;
                    int dayLight = next.getDayLight();
                    a(canvas, this.t, i11, i3, (int) ((((float) dayLight) / f) * ((float) i6)), i9, i5, dayLight, i13);
                } else if (mSleepMode != 2) {
                    wd4.a((Object) next, "sleepDayData");
                    a(canvas, i11, i3, (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i6)), i9, i5, next);
                    f = mChartMax;
                } else {
                    int dayRestful = next.getDayRestful();
                    int i14 = totalSleepMinutes;
                    f = mChartMax;
                    a(canvas, this.u, i11, i3, (int) ((((float) dayRestful) / mChartMax) * ((float) i6)), i9, i5, dayRestful, i14);
                }
                i10 += i8;
                linkedList.add(Integer.valueOf(i11));
                mChartMax = f;
            }
            jd4.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = i2 - (i5 / 2);
        if (i9 < 0) {
            i9 = 0;
        }
        RectF rectF = new RectF((float) i9, (float) ((i3 - ((int) ((((float) i7) / ((float) i8)) * ((float) i4)))) + i6), (float) (i5 + i9), (float) i3);
        xr3.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, int i6, SleepDayData sleepDayData) {
        int i7;
        int i8;
        int i9;
        int i10;
        Paint paint;
        int i11 = i2 - (i5 / 2);
        if (i11 < 0) {
            i11 = 0;
        }
        int i12 = i11 + i5;
        int size = (i4 - i6) - (this.z * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i13 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "session duration: " + component1);
            i13 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i14 = i3;
        int i15 = 0;
        while (i15 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i15);
            wd4.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f = (float) durationInMinutes;
            int i16 = (int) ((f / ((float) i13)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i17 = i14;
            int i18 = 0;
            while (i18 < size3) {
                int i19 = sleepStates.get(i18).state;
                if (i18 < size3 - 1) {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = ((int) sleepStates.get(i18 + 1).index) - ((int) sleepStates.get(i18).index);
                } else {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = durationInMinutes - ((int) sleepStates.get(i18).index);
                }
                int i20 = (int) ((((float) i10) / f) * ((float) i16));
                if (i20 < 1) {
                    i20 = 1;
                }
                int i21 = i17 - i20;
                int i22 = i11;
                RectF rectF = new RectF((float) i11, (float) i21, (float) i12, (float) i17);
                if (i19 == 1) {
                    paint = this.t;
                } else if (i19 != 2) {
                    paint = this.s;
                } else {
                    paint = this.u;
                }
                canvas.drawRect(rectF, paint);
                i18++;
                size = i8;
                size2 = i7;
                i17 = i21;
                i13 = i9;
                i11 = i22;
            }
            Canvas canvas2 = canvas;
            int i23 = size2;
            int i24 = i13;
            i14 = i17 - this.z;
            i15++;
            size = size;
            i11 = i11;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        int i2;
        if (!list.isEmpty()) {
            int size = list.size();
            int i3 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                i2 = 0;
                while (i3 < length) {
                    try {
                        i2 = numArr[i3].intValue();
                        a(canvas, String.valueOf(i2 + 1), list.get(i2).intValue());
                        i3++;
                    } catch (Exception e) {
                        e = e;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = F;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e2) {
                e = e2;
                i2 = 0;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = F;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.r.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.z), this.r);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            int size = list.size();
            float height = (float) getStartBitmap().getHeight();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = F;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.B.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.B.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f = (float) i3;
                float f2 = (float) i4;
                float f3 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.B.get(i6).getSleepGoal()) / mChartMax)) * f) + f2, f3);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.B.get(i7).getSleepGoal()) / mChartMax)) * f) + f2, f3);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.x), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas2.drawPath(path, this.q);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas2.drawPath(path, this.q);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas2.drawPath(path, this.q);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.x), min);
                        canvas2.drawPath(path, this.q);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.x), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
        }
    }
}
