package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import com.fossil.blesdk.obfuscated.ft3;
import com.fossil.blesdk.obfuscated.g9;
import com.fossil.blesdk.obfuscated.i62;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ProgressButton extends AppCompatButton {
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public long k; // = 0;
    @DexIgnore
    public Drawable[] l;
    @DexIgnore
    public ft3 m;
    @DexIgnore
    public ViewGroup n;
    @DexIgnore
    public FrameLayout o;

    @DexIgnore
    public ProgressButton(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a() {
        ft3 ft3 = this.m;
        if (ft3 == null) {
            return;
        }
        if (!this.h) {
            setText(ft3.a(isSelected()));
        } else {
            setText(ft3.a());
        }
    }

    @DexIgnore
    public void b() {
        setText(this.m.a(isSelected()));
        Drawable[] drawableArr = this.l;
        if (drawableArr != null) {
            setCompoundDrawablesWithIntrinsicBounds(drawableArr[0], drawableArr[1], drawableArr[2], drawableArr[3]);
        }
        if (this.g) {
            setClickable(true);
        }
        this.h = false;
        this.j = 0;
        ViewGroup viewGroup = this.n;
        if (viewGroup != null) {
            viewGroup.removeView(this.o);
        }
    }

    @DexIgnore
    public void c() {
        setText(this.m.a());
        this.l = (Drawable[]) Arrays.copyOf(getCompoundDrawables(), 4);
        setCompoundDrawables((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        if (this.g) {
            setClickable(false);
        }
        this.h = true;
        this.j = 0;
        if (this.n != null) {
            ViewParent parent = this.o.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(this.o);
            }
            this.n.addView(this.o);
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.i != null && this.h) {
            canvas.save();
            canvas.translate((float) ((getWidth() - this.i.getMinimumWidth()) / 2), (float) ((getHeight() - this.i.getMinimumHeight()) / 2));
            long drawingTime = getDrawingTime();
            if (drawingTime - this.k > 100) {
                this.k = drawingTime;
                this.j++;
                if (((long) this.j) >= 12) {
                    this.j = 0;
                }
            }
            this.i.setLevel((int) (((float) (this.j * 10000)) / 12.0f));
            this.i.draw(canvas);
            canvas.restore();
            g9.C(this);
        }
    }

    @DexIgnore
    public void setSelected(boolean z) {
        super.setSelected(z);
        a();
    }

    @DexIgnore
    public ProgressButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i62.ProgressButton);
        if (obtainStyledAttributes.hasValue(2)) {
            this.i = obtainStyledAttributes.getDrawable(2);
        }
        ft3 ft3 = new ft3();
        ft3.a((CharSequence) "");
        ft3.b(getText());
        ft3.c(getText());
        if (obtainStyledAttributes.hasValue(4)) {
            ft3.b(obtainStyledAttributes.getString(4));
        }
        if (obtainStyledAttributes.hasValue(5)) {
            ft3.c(obtainStyledAttributes.getString(5));
        }
        if (obtainStyledAttributes.hasValue(3)) {
            ft3.a((CharSequence) obtainStyledAttributes.getString(3));
        }
        if (obtainStyledAttributes.hasValue(1)) {
            this.h = obtainStyledAttributes.getBoolean(1, false);
        }
        if (obtainStyledAttributes.hasValue(0)) {
            this.g = obtainStyledAttributes.getBoolean(0, true);
        }
        this.m = ft3;
        obtainStyledAttributes.recycle();
        try {
            this.n = (ViewGroup) ((AppCompatActivity) context).getWindow().getDecorView().findViewById(16908290);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        this.o = new FrameLayout(getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        this.o.setBackgroundColor(0);
        this.o.setClickable(true);
        this.o.setLayoutParams(layoutParams);
        Drawable drawable = this.i;
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), this.i.getIntrinsicHeight());
            if (this.h) {
                c();
            } else {
                b();
            }
        }
    }

    @DexIgnore
    public ProgressButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }
}
