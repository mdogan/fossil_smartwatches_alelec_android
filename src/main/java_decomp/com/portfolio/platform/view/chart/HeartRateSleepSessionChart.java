package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.rt3;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSleepSessionChart extends BaseChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public Integer B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public int N;
    @DexIgnore
    public short O;
    @DexIgnore
    public short P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public ArrayList<rt3> U;
    @DexIgnore
    public float V;
    @DexIgnore
    public boolean W; // = true;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public Integer z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Path a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(Path path, int i) {
            wd4.b(path, "path");
            this.a = path;
            this.b = i;
        }

        @DexIgnore
        public final Path a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (wd4.a((Object) this.a, (Object) bVar.a)) {
                        if (this.b == bVar.b) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Path path = this.a;
            return ((path != null ? path.hashCode() : 0) * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "PathDist(path=" + this.a + ", sleepState=" + this.b + ")";
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSleepSessionChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wd4.b(context, "context");
        wd4.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i62.HeartRateSleepSessionChart);
        this.u = obtainStyledAttributes.getColor(1, -3355444);
        this.v = obtainStyledAttributes.getColor(0, k6.a(context, (int) R.color.activeTime));
        this.w = obtainStyledAttributes.getColor(5, k6.a(context, (int) R.color.heartRate));
        this.x = obtainStyledAttributes.getColor(2, k6.a(context, (int) R.color.heartRate));
        this.y = obtainStyledAttributes.getColor(6, -3355444);
        this.z = Integer.valueOf(obtainStyledAttributes.getResourceId(4, -1));
        this.A = obtainStyledAttributes.getDimension(3, us3.c(13.0f));
        this.B = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
        this.C = obtainStyledAttributes.getDimension(7, us3.c(13.0f));
        obtainStyledAttributes.recycle();
        this.Q = new Paint();
        this.Q.setColor(this.u);
        this.Q.setStrokeWidth(2.0f);
        this.Q.setStyle(Paint.Style.STROKE);
        this.R = new Paint(1);
        this.R.setColor(this.y);
        this.R.setStyle(Paint.Style.FILL);
        this.R.setTextSize(this.A);
        Integer num = this.z;
        if (num == null || num.intValue() != -1) {
            Paint paint = this.R;
            Integer num2 = this.z;
            if (num2 != null) {
                paint.setTypeface(r6.a(context, num2.intValue()));
            } else {
                wd4.a();
                throw null;
            }
        }
        this.S = new Paint(1);
        this.S.setColor(this.y);
        this.S.setStyle(Paint.Style.FILL);
        this.S.setTextSize(this.C);
        Integer num3 = this.B;
        if (num3 == null || num3.intValue() != -1) {
            Paint paint2 = this.S;
            Integer num4 = this.B;
            if (num4 != null) {
                paint2.setTypeface(r6.a(context, num4.intValue()));
            } else {
                wd4.a();
                throw null;
            }
        }
        this.T = new Paint(1);
        this.T.setStrokeWidth(us3.a(1.0f));
        this.T.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.R.getTextBounds("222", 0, 3, rect);
        this.K = (float) rect.width();
        this.L = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am);
        this.S.getTextBounds(a2, 0, a2.length(), rect2);
        this.M = (float) rect2.height();
        this.U = new ArrayList<>();
        a();
    }

    @DexIgnore
    public final void a(ArrayList<rt3> arrayList) {
        wd4.b(arrayList, "heartRatePointList");
        this.U.clear();
        this.U.addAll(arrayList);
        this.W = !this.U.isEmpty();
        getMGraph().invalidate();
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wd4.b(canvas, "canvas");
        super.b(canvas);
        float f = (float) 2;
        this.D = ((float) canvas.getHeight()) - (this.M * f);
        this.E = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.F = (float) canvas.getWidth();
        this.G = this.C;
        this.H = this.E + us3.a(5.0f);
        this.J = this.D - (this.C * f);
        this.I = this.F - this.K;
        this.V = (this.I - this.H) / ((float) this.N);
        f(canvas);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.E, this.J, (float) canvas.getWidth(), this.J, this.Q);
        canvas.drawLine(this.E, this.G, (float) canvas.getWidth(), this.G, this.Q);
        if (this.W) {
            float f = (float) 2;
            canvas.drawText(String.valueOf(this.O), this.I + (((((float) canvas.getWidth()) - this.I) - this.R.measureText(String.valueOf(this.O))) / f), this.J + (this.L * 1.5f), this.R);
            canvas.drawText(String.valueOf(this.P), this.I + (((((float) canvas.getWidth()) - this.I) - this.R.measureText(String.valueOf(this.P))) / f), this.G + (this.L * 1.5f), this.R);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        int i2;
        ArrayList<b> arrayList = new ArrayList<>();
        rt3 rt3 = (rt3) wb4.e(this.U);
        int d = rt3 != null ? rt3.d() : -1;
        Iterator<T> it = this.U.iterator();
        int i3 = d;
        Path path = null;
        boolean z2 = false;
        int i4 = 0;
        while (true) {
            boolean z3 = true;
            if (!it.hasNext()) {
                break;
            }
            rt3 rt32 = (rt3) it.next();
            int a2 = rt32.a();
            int b2 = rt32.b();
            int c = rt32.c();
            float f = this.H + (((float) b2) * this.V);
            float f2 = this.G;
            float f3 = this.J;
            short s = this.P;
            float f4 = f2 + (((f3 - f2) / ((float) (s - this.O))) * ((float) (s - a2)));
            if (!z2 || f4 > f3 || i3 != c) {
                if (!(f4 > this.J || i3 == c || path == null)) {
                    path.lineTo(f, f4);
                }
                if (path != null) {
                    if (i4 > 1) {
                        arrayList.add(new b(path, i3));
                    }
                    path = null;
                }
                if (f4 <= this.J) {
                    path = new Path();
                    path.moveTo(f, f4);
                    i2 = 1;
                } else {
                    i2 = 0;
                    z3 = false;
                }
                i4 = i2;
                i3 = c;
                z2 = z3;
            } else {
                if (path != null) {
                    path.lineTo(f, f4);
                }
                i4++;
            }
        }
        if (path != null && i4 > 1) {
            arrayList.add(new b(path, i3));
        }
        for (b bVar : arrayList) {
            Path a3 = bVar.a();
            int b3 = bVar.b();
            Paint paint = this.T;
            if (b3 == 0) {
                i = this.v;
            } else if (b3 != 1) {
                i = this.x;
            } else {
                i = this.w;
            }
            paint.setColor(i);
            canvas.drawPath(a3, this.T);
        }
    }

    @DexIgnore
    public final int getMDuration() {
        return this.N;
    }

    @DexIgnore
    public final short getMMaxHRValue() {
        return this.P;
    }

    @DexIgnore
    public final short getMMinHRValue() {
        return this.O;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setMDuration(int i) {
        this.N = i;
    }

    @DexIgnore
    public final void setMMaxHRValue(short s) {
        if (s == ((short) 0)) {
            s = (short) 100;
        }
        this.P = s;
    }

    @DexIgnore
    public final void setMMinHRValue(short s) {
        this.O = s;
    }
}
