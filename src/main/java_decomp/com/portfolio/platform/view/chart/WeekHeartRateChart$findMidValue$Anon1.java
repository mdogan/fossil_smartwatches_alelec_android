package com.portfolio.platform.view.chart;

import com.fossil.blesdk.obfuscated.jd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeekHeartRateChart$findMidValue$Anon1 extends Lambda implements jd4<Integer, Boolean> {
    @DexIgnore
    public static /* final */ WeekHeartRateChart$findMidValue$Anon1 INSTANCE; // = new WeekHeartRateChart$findMidValue$Anon1();

    @DexIgnore
    public WeekHeartRateChart$findMidValue$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke(((Number) obj).intValue()));
    }

    @DexIgnore
    public final boolean invoke(int i) {
        return i > 0;
    }
}
