package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.fossil.blesdk.obfuscated.gt3;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ss3;
import com.fossil.blesdk.obfuscated.tm2;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FlexibleTextView extends AppCompatTextView {
    @DexIgnore
    public static /* final */ int k; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public static /* final */ a l; // = new a((rd4) null);
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j; // = k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a() {
            return FlexibleTextView.k;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context) {
        super(context);
        wd4.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    @SuppressLint({"ResourceType"})
    public final void a(AttributeSet attributeSet) {
        this.h = 0;
        CharSequence text = getText();
        CharSequence hint = getHint();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, i62.FlexibleTextView);
            this.h = obtainStyledAttributes.getInt(1, 0);
            this.i = obtainStyledAttributes.getInt(0, 0);
            this.j = obtainStyledAttributes.getColor(2, k);
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            if (resourceId != -1) {
                text = a(resourceId);
            }
            int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
            if (resourceId2 != -1) {
                hint = a(resourceId2);
            }
            obtainStyledAttributes2.recycle();
        }
        if (!TextUtils.isEmpty(text)) {
            setText(text);
        }
        if (!TextUtils.isEmpty(hint)) {
            wd4.a((Object) hint, "hint");
            setHint(a(hint, this.i));
        }
        int i2 = this.j;
        if (i2 != k) {
            gt3.a((TextView) this, i2);
        }
    }

    @DexIgnore
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        wd4.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wd4.b(context, "context");
        wd4.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wd4.b(context, "context");
        wd4.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.h);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return ss3.a(charSequence);
        }
        if (i2 == 2) {
            return ss3.b(charSequence);
        }
        if (i2 == 3) {
            return ss3.d(charSequence);
        }
        if (i2 == 4) {
            return ss3.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return ss3.c(charSequence);
    }

    @DexIgnore
    public final String a(int i2) {
        String a2 = tm2.a((Context) PortfolioApp.W.c(), i2);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a2;
    }
}
