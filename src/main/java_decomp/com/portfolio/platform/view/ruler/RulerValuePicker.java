package com.portfolio.platform.view.ruler;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.blesdk.obfuscated.gu3;
import com.fossil.blesdk.obfuscated.hu3;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.r6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.Unit;
import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RulerValuePicker extends FrameLayout implements gu3.b {
    @DexIgnore
    public View e;
    @DexIgnore
    public boolean f; // = false;
    @DexIgnore
    public View g;
    @DexIgnore
    public RulerView h;
    @DexIgnore
    public gu3 i;
    @DexIgnore
    public hu3 j;
    @DexIgnore
    public Paint k;
    @DexIgnore
    public Path l;
    @DexIgnore
    public Formatter m;
    @DexIgnore
    public TextView n;
    @DexIgnore
    public float o; // = 0.6f;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public Unit q; // = Unit.METRIC;

    @DexIgnore
    public interface Formatter extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animator.AnimatorListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            RulerValuePicker rulerValuePicker = RulerValuePicker.this;
            rulerValuePicker.f = false;
            hu3 hu3 = rulerValuePicker.j;
            if (hu3 != null) {
                hu3.a(false);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            hu3 hu3 = RulerValuePicker.this.j;
            if (hu3 != null) {
                hu3.a(true);
            }
        }
    }

    @DexIgnore
    public RulerValuePicker(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        this.n = new TextView(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 49;
        this.n.setLayoutParams(layoutParams);
        this.n.setGravity(17);
        this.n.setBackground(getBackground());
        b();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, i62.RulerValuePicker);
            try {
                if (obtainStyledAttributes.hasValue(6)) {
                    this.p = obtainStyledAttributes.getColor(6, -1);
                    this.n.setTextColor(this.p);
                }
                if (obtainStyledAttributes.hasValue(9)) {
                    setTextColor(obtainStyledAttributes.getColor(9, -1));
                }
                if (obtainStyledAttributes.hasValue(10)) {
                    setTextSize(obtainStyledAttributes.getDimensionPixelSize(10, 14));
                }
                if (obtainStyledAttributes.hasValue(8)) {
                    setSelectedTextSize((int) obtainStyledAttributes.getDimension(8, 14.0f));
                }
                if (obtainStyledAttributes.hasValue(11)) {
                    setTextStyle(obtainStyledAttributes.getInt(11, 0));
                }
                if (obtainStyledAttributes.hasValue(0)) {
                    setIndicatorColor(obtainStyledAttributes.getColor(0, -1));
                }
                if (obtainStyledAttributes.hasValue(2)) {
                    setIndicatorWidth(obtainStyledAttributes.getDimensionPixelSize(2, 4));
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    setIndicatorIntervalDistance(obtainStyledAttributes.getDimensionPixelSize(1, 4));
                }
                if (obtainStyledAttributes.hasValue(3) || obtainStyledAttributes.hasValue(12)) {
                    this.o = obtainStyledAttributes.getFraction(3, 1, 1, 0.6f);
                    a(this.o, obtainStyledAttributes.getFraction(12, 1, 1, 0.4f));
                }
                if (obtainStyledAttributes.hasValue(5) || obtainStyledAttributes.hasValue(4)) {
                    a(obtainStyledAttributes.getInteger(5, 0), obtainStyledAttributes.getInteger(4, 100));
                }
                if (obtainStyledAttributes.hasValue(7)) {
                    Typeface a2 = r6.a(getContext(), obtainStyledAttributes.getResourceId(7, -1));
                    if (a2 != null) {
                        setTextTypeface(a2);
                    }
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("RulerValuePicker", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        this.k = new Paint(1);
        d();
        this.l = new Path();
    }

    @DexIgnore
    public final void b() {
        this.i = new gu3(getContext(), this);
        this.i.setHorizontalScrollBarEnabled(false);
        LinearLayout linearLayout = new LinearLayout(getContext());
        this.e = new View(getContext());
        linearLayout.addView(this.e);
        this.h = new RulerView(getContext());
        linearLayout.addView(this.h);
        this.g = new View(getContext());
        linearLayout.addView(this.g);
        this.i.removeAllViews();
        this.i.addView(linearLayout);
        removeAllViews();
        addView(this.i);
        addView(this.n);
    }

    @DexIgnore
    public final void c() {
        this.l.reset();
        float height = (float) getHeight();
        this.l.moveTo((float) (getWidth() / 2), height);
        this.l.lineTo((float) (getWidth() / 2), height * (1.0f - this.o));
    }

    @DexIgnore
    public final void d() {
        this.k.setColor(this.p);
        this.k.setStrokeWidth(5.0f);
        this.k.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @DexIgnore
    public int getCurrentValue() {
        int f2 = this.h.f() + (this.i.getScrollX() / this.h.b());
        if (f2 > this.h.e()) {
            return this.h.e();
        }
        return f2 < this.h.f() ? this.h.f() : f2;
    }

    @DexIgnore
    public int getIndicatorColor() {
        return this.h.a();
    }

    @DexIgnore
    public int getIndicatorIntervalWidth() {
        return this.h.b();
    }

    @DexIgnore
    public float getIndicatorWidth() {
        return this.h.c();
    }

    @DexIgnore
    public float getLongIndicatorHeightRatio() {
        return this.h.d();
    }

    @DexIgnore
    public int getMaxValue() {
        return this.h.e();
    }

    @DexIgnore
    public int getMinValue() {
        return this.h.f();
    }

    @DexIgnore
    public int getNotchColor() {
        return this.p;
    }

    @DexIgnore
    public float getShortIndicatorHeightRatio() {
        return this.h.g();
    }

    @DexIgnore
    public int getTextColor() {
        return this.h.h();
    }

    @DexIgnore
    public float getTextSize() {
        return this.h.i();
    }

    @DexIgnore
    public Unit getUnit() {
        return this.q;
    }

    @DexIgnore
    public void onDrawForeground(Canvas canvas) {
        super.onDrawForeground(canvas);
        canvas.drawPath(this.l, this.k);
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (z) {
            int width = getWidth();
            ViewGroup.LayoutParams layoutParams = this.e.getLayoutParams();
            int i6 = width / 2;
            layoutParams.width = i6;
            this.e.setLayoutParams(layoutParams);
            ViewGroup.LayoutParams layoutParams2 = this.g.getLayoutParams();
            layoutParams2.width = i6;
            this.g.setLayoutParams(layoutParams2);
            c();
            invalidate();
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        c(bVar.e);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        b bVar = new b(super.onSaveInstanceState());
        bVar.e = getCurrentValue();
        return bVar;
    }

    @DexIgnore
    public void onScrollChanged() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollChanged");
        int currentValue = getCurrentValue();
        this.n.setText(a(currentValue));
        hu3 hu3 = this.j;
        if (hu3 != null) {
            hu3.b(currentValue);
        }
    }

    @DexIgnore
    public void setFormatter(Formatter formatter) {
        if (!Objects.equals(formatter, this.m)) {
            this.m = formatter;
            this.h.a(this.m);
        }
    }

    @DexIgnore
    public void setIndicatorColor(int i2) {
        this.h.b(i2);
    }

    @DexIgnore
    public void setIndicatorColorRes(int i2) {
        setIndicatorColor(k6.a(getContext(), i2));
    }

    @DexIgnore
    public void setIndicatorIntervalDistance(int i2) {
        this.h.c(i2);
    }

    @DexIgnore
    public void setIndicatorWidth(int i2) {
        this.h.d(i2);
    }

    @DexIgnore
    public void setIndicatorWidthRes(int i2) {
        setIndicatorWidth(getContext().getResources().getDimensionPixelSize(i2));
    }

    @DexIgnore
    public void setNotchColor(int i2) {
        this.p = i2;
        this.n.setTextColor(i2);
        d();
        invalidate();
    }

    @DexIgnore
    public void setNotchColorRes(int i2) {
        setNotchColor(k6.a(getContext(), i2));
    }

    @DexIgnore
    public void setSelectedTextSize(int i2) {
        this.n.setTextSize(0, (float) i2);
    }

    @DexIgnore
    public void setTextColor(int i2) {
        this.h.e(i2);
    }

    @DexIgnore
    public void setTextColorRes(int i2) {
        setTextColor(k6.a(getContext(), i2));
    }

    @DexIgnore
    public void setTextSize(int i2) {
        this.h.f(i2);
    }

    @DexIgnore
    public void setTextStyle(int i2) {
        this.h.g(i2);
        TextView textView = this.n;
        textView.setTypeface(Typeface.create(textView.getTypeface(), i2));
    }

    @DexIgnore
    public void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            this.h.a(typeface);
            this.n.setTypeface(typeface);
        }
    }

    @DexIgnore
    public void setUnit(Unit unit) {
        this.q = unit;
    }

    @DexIgnore
    public void setValuePickerListener(hu3 hu3) {
        this.j = hu3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<b> CREATOR; // = new a();
        @DexIgnore
        public int e; // = 0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.Creator<b> {
            @DexIgnore
            public b createFromParcel(Parcel parcel) {
                return new b(parcel);
            }

            @DexIgnore
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        public b(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.e);
        }

        @DexIgnore
        public b(Parcel parcel) {
            super(parcel);
            this.e = parcel.readInt();
        }
    }

    @DexIgnore
    public static String d(int i2) {
        return String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(i2)});
    }

    @DexIgnore
    public void c(int i2) {
        int f2 = this.h.f();
        int e2 = this.h.e();
        int i3 = e2 - f2;
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this.i, "scrollX", new int[]{i3 / 2, (i2 < f2 ? 0 : i2 > e2 ? i3 : i2 - f2) * this.h.b()});
        ofInt.setDuration(200);
        ofInt.addListener(new a());
        ofInt.start();
    }

    @DexIgnore
    public RulerValuePicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    @DexIgnore
    public RulerValuePicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
    }

    @DexIgnore
    public final void b(int i2) {
        int scrollX = this.i.getScrollX() % i2;
        if (scrollX < i2 / 2) {
            this.i.scrollBy(-scrollX, 0);
        } else {
            this.i.scrollBy(i2 - scrollX, 0);
        }
    }

    @DexIgnore
    @TargetApi(21)
    public RulerValuePicker(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        a(attributeSet);
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollStopped");
        b(this.h.b());
        int currentValue = getCurrentValue();
        this.n.setText(a(currentValue));
        hu3 hu3 = this.j;
        if (hu3 != null) {
            hu3.a(currentValue);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3) {
        this.h.a(i2, i3);
        c(i2);
    }

    @DexIgnore
    public void a(int i2, int i3, int i4) {
        if (!this.f) {
            this.f = true;
            this.h.a(i2, i3);
            c(i4);
        }
    }

    @DexIgnore
    public void a(float f2, float f3) {
        this.h.a(f2, f3);
    }

    @DexIgnore
    public final String a(int i2) {
        Formatter formatter = this.m;
        return formatter != null ? formatter.format(i2) : d(i2);
    }
}
