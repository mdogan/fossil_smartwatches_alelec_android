package com.portfolio.platform.view.ruler;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Locale;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RulerView extends View {
    @DexIgnore
    public int e;
    @DexIgnore
    public Paint f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public int h; // = 14;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j; // = 100;
    @DexIgnore
    public float k; // = 0.6f;
    @DexIgnore
    public float l; // = 0.4f;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public int n; // = 0;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public int p; // = 0;
    @DexIgnore
    public Typeface q;
    @DexIgnore
    public RulerValuePicker.Formatter r;
    @DexIgnore
    public int s; // = -1;
    @DexIgnore
    public int t; // = 36;
    @DexIgnore
    public float u; // = 4.0f;

    @DexIgnore
    public RulerView(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, i62.RulerView, 0, 0);
            try {
                if (obtainStyledAttributes.hasValue(6)) {
                    this.o = obtainStyledAttributes.getColor(6, -1);
                }
                if (obtainStyledAttributes.hasValue(7)) {
                    this.t = obtainStyledAttributes.getDimensionPixelSize(7, 24);
                }
                if (obtainStyledAttributes.hasValue(0)) {
                    this.s = obtainStyledAttributes.getColor(0, -1);
                }
                if (obtainStyledAttributes.hasValue(2)) {
                    this.u = (float) obtainStyledAttributes.getDimensionPixelSize(2, 4);
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.h = obtainStyledAttributes.getDimensionPixelSize(1, 4);
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.k = obtainStyledAttributes.getFraction(3, 1, 1, 0.6f);
                }
                if (obtainStyledAttributes.hasValue(8)) {
                    this.l = obtainStyledAttributes.getFraction(8, 1, 1, 0.4f);
                }
                a(this.k, this.l);
                if (obtainStyledAttributes.hasValue(5)) {
                    this.i = obtainStyledAttributes.getInteger(5, 0);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.j = obtainStyledAttributes.getInteger(4, 100);
                }
                a(this.i, this.j);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        j();
    }

    @DexIgnore
    public final void b(float f2, float f3) {
        int i2 = this.e;
        this.m = (int) (((float) i2) * f2);
        this.n = (int) (((float) i2) * f3);
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2) {
        canvas.drawText(a(this.i + i2), (float) (this.h * i2), this.g.getTextSize(), this.g);
    }

    @DexIgnore
    public void d(int i2) {
        this.u = (float) i2;
        j();
    }

    @DexIgnore
    public void e(int i2) {
        this.o = i2;
        j();
    }

    @DexIgnore
    public void f(int i2) {
        this.t = i2;
        j();
    }

    @DexIgnore
    public void g(int i2) {
        this.p = i2;
        j();
    }

    @DexIgnore
    public int h() {
        return this.s;
    }

    @DexIgnore
    public float i() {
        return (float) this.t;
    }

    @DexIgnore
    public final void j() {
        this.f = new Paint(1);
        this.f.setColor(this.s);
        this.f.setStrokeWidth(this.u);
        this.f.setStyle(Paint.Style.STROKE);
        this.g = new Paint(1);
        this.g.setColor(this.o);
        this.g.setTextSize((float) this.t);
        if (this.q == null) {
            this.q = Typeface.create(this.g.getTypeface(), this.p);
        }
        this.g.setTypeface(this.q);
        this.g.setTextAlign(Paint.Align.CENTER);
        invalidate();
        requestLayout();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        for (int i2 = 1; i2 < this.j - this.i; i2++) {
            RulerValuePicker.Formatter formatter = this.r;
            if (formatter == null || ((ProfileFormatter) formatter).getMUnit() != 3) {
                if (i2 % 5 == 0) {
                    a(canvas, i2);
                    if (i2 % 10 == 0) {
                        c(canvas, i2);
                    }
                } else {
                    b(canvas, i2);
                }
            } else if (i2 % 12 == 0) {
                a(canvas, i2);
                c(canvas, i2);
            } else {
                b(canvas, i2);
            }
        }
        b(canvas, 0);
        b(canvas, getWidth());
        canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.e, (float) getMeasuredWidth(), (float) this.e, this.f);
        super.onDraw(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        this.e = View.MeasureSpec.getSize(i3);
        int i4 = ((this.j - this.i) - 1) * this.h;
        b(this.k, this.l);
        setMeasuredDimension(i4, this.e);
    }

    @DexIgnore
    public static String h(int i2) {
        return String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(i2)});
    }

    @DexIgnore
    public final void b(Canvas canvas, int i2) {
        int i3 = this.h;
        int i4 = this.e;
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) (i3 * i2), (float) i4, (float) (i3 * i2), (float) (i4 - this.n), this.f);
    }

    @DexIgnore
    public float d() {
        return this.k;
    }

    @DexIgnore
    public int e() {
        return this.j;
    }

    @DexIgnore
    public int f() {
        return this.i;
    }

    @DexIgnore
    public float g() {
        return this.l;
    }

    @DexIgnore
    public void b(int i2) {
        this.s = i2;
        j();
    }

    @DexIgnore
    public float c() {
        return this.u;
    }

    @DexIgnore
    public void c(int i2) {
        if (i2 > 0) {
            this.h = i2;
            invalidate();
            return;
        }
        throw new IllegalArgumentException("Interval cannot be negative or zero.");
    }

    @DexIgnore
    public int b() {
        return this.h;
    }

    @DexIgnore
    public RulerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2) {
        int i3 = this.h;
        int i4 = this.e;
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) (i3 * i2), (float) i4, (float) (i3 * i2), (float) (i4 - this.m), this.f);
    }

    @DexIgnore
    public void a(Typeface typeface) {
        if (typeface != null) {
            this.q = typeface;
            j();
        }
    }

    @DexIgnore
    public void a(RulerValuePicker.Formatter formatter) {
        if (!Objects.equals(formatter, this.r)) {
            this.r = formatter;
            invalidate();
            requestLayout();
        }
    }

    @DexIgnore
    public RulerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
    }

    @DexIgnore
    public int a() {
        return this.s;
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public void a(float f2, float f3) {
        if (f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f3 > 1.0f) {
            throw new IllegalArgumentException("Sort indicator height must be between 0 to 1.");
        } else if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("Long indicator height must be between 0 to 1.");
        } else if (f3 <= f2) {
            this.k = f2;
            this.l = f3;
            b(this.k, this.l);
            invalidate();
        } else {
            throw new IllegalArgumentException("Long indicator height cannot be less than sort indicator height.");
        }
    }

    @DexIgnore
    public final String a(int i2) {
        RulerValuePicker.Formatter formatter = this.r;
        return formatter != null ? formatter.format(i2) : h(i2);
    }
}
