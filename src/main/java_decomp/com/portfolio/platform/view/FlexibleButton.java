package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.tm2;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FlexibleButton extends AppCompatButton {
    @DexIgnore
    public FlexibleButton(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        CharSequence text = getText();
        if (attributeSet != null) {
            getContext().obtainStyledAttributes(attributeSet, i62.FlexibleButton).recycle();
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087}, 0, 0);
            int resourceId = obtainStyledAttributes.getResourceId(0, -1);
            if (resourceId != -1) {
                text = a(resourceId);
            }
            obtainStyledAttributes.recycle();
        }
        if (!TextUtils.isEmpty(text)) {
            setText(text);
        }
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
    }

    @DexIgnore
    public FlexibleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    @DexIgnore
    public FlexibleButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet);
    }

    @DexIgnore
    public final String a(int i) {
        return tm2.a((Context) PortfolioApp.R, i);
    }
}
