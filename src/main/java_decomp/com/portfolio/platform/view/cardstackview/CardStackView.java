package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.nt3;
import com.fossil.blesdk.obfuscated.ot3;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.cardstackview.CardContainerView;
import java.util.LinkedList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CardStackView extends FrameLayout {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public /* final */ nt3 e;
    @DexIgnore
    public /* final */ ot3 f;
    @DexIgnore
    public BaseAdapter g;
    @DexIgnore
    public /* final */ LinkedList<CardContainerView> h;
    @DexIgnore
    public a i;
    @DexIgnore
    public /* final */ d j;
    @DexIgnore
    public /* final */ c k;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(float f, float f2);

        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(SwipeDirection swipeDirection);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CardContainerView.c {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        public c(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void a(float f, float f2) {
            this.a.a(f, f2);
        }

        @DexIgnore
        public void b() {
            this.a.c();
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a();
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(Point point, SwipeDirection swipeDirection) {
            wd4.b(point, "point");
            wd4.b(swipeDirection, "direction");
            this.a.b(point, swipeDirection);
        }

        @DexIgnore
        public void a() {
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a(this.a.getState$app_fossilRelease().a);
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends DataSetObserver {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        public d(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void onChanged() {
            if (this.a.getAdapter$app_fossilRelease() != null) {
                boolean z = false;
                if (this.a.getState$app_fossilRelease().d) {
                    this.a.getState$app_fossilRelease().d = false;
                } else {
                    int i = this.a.getState$app_fossilRelease().c;
                    BaseAdapter adapter$app_fossilRelease = this.a.getAdapter$app_fossilRelease();
                    if (adapter$app_fossilRelease != null) {
                        if (i == adapter$app_fossilRelease.getCount()) {
                            z = true;
                        }
                        z = !z;
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                this.a.a(z);
                ot3 state$app_fossilRelease = this.a.getState$app_fossilRelease();
                BaseAdapter adapter$app_fossilRelease2 = this.a.getAdapter$app_fossilRelease();
                if (adapter$app_fossilRelease2 != null) {
                    state$app_fossilRelease.c = adapter$app_fossilRelease2.getCount();
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;
        @DexIgnore
        public /* final */ /* synthetic */ Point b;
        @DexIgnore
        public /* final */ /* synthetic */ SwipeDirection c;

        @DexIgnore
        public e(CardStackView cardStackView, Point point, SwipeDirection swipeDirection) {
            this.a = cardStackView;
            this.b = point;
            this.c = swipeDirection;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            wd4.b(animator, "animator");
            this.a.a(this.b, this.c);
        }
    }

    /*
    static {
        new b((rd4) null);
        String simpleName = CardStackView.class.getSimpleName();
        wd4.a((Object) simpleName, "CardStackView::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        wd4.b(context, "context");
        this.e = new nt3();
        this.f = new ot3();
        this.h = new LinkedList<>();
        this.j = new d(this);
        this.k = new c(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i62.CardStackView);
        setVisibleCount(obtainStyledAttributes.getInt(11, this.e.a));
        setSwipeThreshold(obtainStyledAttributes.getFloat(8, this.e.b));
        setTranslationDiff(obtainStyledAttributes.getFloat(10, this.e.c));
        setScaleDiff(obtainStyledAttributes.getFloat(4, this.e.d));
        setStackFrom(StackFrom.values()[obtainStyledAttributes.getInt(5, this.e.e.ordinal())]);
        setElevationEnabled(obtainStyledAttributes.getBoolean(1, this.e.f));
        setSwipeEnabled(obtainStyledAttributes.getBoolean(7, this.e.g));
        List<SwipeDirection> from = SwipeDirection.from(obtainStyledAttributes.getInt(6, 0));
        wd4.a((Object) from, "SwipeDirection.from(arra\u2026kView_swipeDirection, 0))");
        setSwipeDirection(from);
        setLeftOverlay(obtainStyledAttributes.getResourceId(2, 0));
        setRightOverlay(obtainStyledAttributes.getResourceId(3, 0));
        setBottomOverlay(obtainStyledAttributes.getResourceId(0, 0));
        setTopOverlay(obtainStyledAttributes.getResourceId(9, 0));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final CardContainerView getBottomView() {
        CardContainerView last = this.h.getLast();
        wd4.a((Object) last, "containers.last");
        return last;
    }

    @DexIgnore
    private final CardContainerView getTopView() {
        CardContainerView first = this.h.getFirst();
        wd4.a((Object) first, "containers.first");
        return first;
    }

    @DexIgnore
    private final void setBottomOverlay(int i2) {
        this.e.j = i2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setElevationEnabled(boolean z) {
        this.e.f = z;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setLeftOverlay(int i2) {
        this.e.h = i2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setRightOverlay(int i2) {
        this.e.i = i2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setScaleDiff(float f2) {
        this.e.d = f2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setStackFrom(StackFrom stackFrom) {
        this.e.e = stackFrom;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeDirection(List<? extends SwipeDirection> list) {
        this.e.l = list;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeEnabled(boolean z) {
        this.e.g = z;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeThreshold(float f2) {
        this.e.b = f2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTopOverlay(int i2) {
        this.e.k = i2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTranslationDiff(float f2) {
        this.e.c = f2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setVisibleCount(int i2) {
        this.e.a = i2;
        if (this.g != null) {
            a(false);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        b(z);
        e();
        c();
        d();
    }

    @DexIgnore
    public final void b(boolean z) {
        if (z) {
            this.f.a();
        }
    }

    @DexIgnore
    public final void c() {
        a();
        a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void d() {
        if (this.g != null) {
            int i2 = this.e.a;
            int i3 = 0;
            while (i3 < i2) {
                CardContainerView cardContainerView = this.h.get(i3);
                wd4.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                int i4 = this.f.a + i3;
                BaseAdapter baseAdapter = this.g;
                if (baseAdapter != null) {
                    if (i4 < baseAdapter.getCount()) {
                        ViewGroup contentContainer = cardContainerView2.getContentContainer();
                        BaseAdapter baseAdapter2 = this.g;
                        if (baseAdapter2 != null) {
                            View view = baseAdapter2.getView(i4, contentContainer.getChildAt(0), contentContainer);
                            wd4.a((Object) contentContainer, "parent");
                            if (contentContainer.getChildCount() == 0) {
                                contentContainer.addView(view);
                            }
                            cardContainerView2.setVisibility(0);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        cardContainerView2.setVisibility(8);
                    }
                    i3++;
                } else {
                    wd4.a();
                    throw null;
                }
            }
            BaseAdapter baseAdapter3 = this.g;
            if (baseAdapter3 == null) {
                wd4.a();
                throw null;
            } else if (!baseAdapter3.isEmpty()) {
                getTopView().setDraggable(true);
            }
        }
    }

    @DexIgnore
    public final void e() {
        removeAllViews();
        this.h.clear();
        int i2 = this.e.a;
        int i3 = 0;
        while (i3 < i2) {
            View inflate = LayoutInflater.from(getContext()).inflate(R.layout.card_container, this, false);
            if (inflate != null) {
                CardContainerView cardContainerView = (CardContainerView) inflate;
                cardContainerView.setDraggable(false);
                cardContainerView.setCardStackOption(this.e);
                nt3 nt3 = this.e;
                cardContainerView.a(nt3.h, nt3.i, nt3.j, nt3.k);
                this.h.add(0, cardContainerView);
                addView(cardContainerView);
                i3++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.cardstackview.CardContainerView");
            }
        }
        this.h.getFirst().setContainerEventListener(this.k);
        this.f.e = true;
    }

    @DexIgnore
    public final void f() {
        int i2 = (this.f.a + this.e.a) - 1;
        BaseAdapter baseAdapter = this.g;
        if (baseAdapter == null) {
            return;
        }
        if (baseAdapter != null) {
            boolean z = false;
            if (i2 < baseAdapter.getCount()) {
                CardContainerView bottomView = getBottomView();
                bottomView.setDraggable(false);
                ViewGroup contentContainer = bottomView.getContentContainer();
                BaseAdapter baseAdapter2 = this.g;
                if (baseAdapter2 != null) {
                    View view = baseAdapter2.getView(i2, contentContainer.getChildAt(0), contentContainer);
                    wd4.a((Object) contentContainer, "parent");
                    if (contentContainer.getChildCount() == 0) {
                        contentContainer.addView(view);
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                CardContainerView bottomView2 = getBottomView();
                bottomView2.setDraggable(false);
                bottomView2.setVisibility(8);
            }
            int i3 = this.f.a;
            BaseAdapter baseAdapter3 = this.g;
            if (baseAdapter3 != null) {
                if (i3 < baseAdapter3.getCount()) {
                    z = true;
                }
                if (z) {
                    getTopView().setDraggable(true);
                    return;
                }
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void g() {
        a(getTopView());
        LinkedList<CardContainerView> linkedList = this.h;
        linkedList.addLast(linkedList.removeFirst());
    }

    @DexIgnore
    public final BaseAdapter getAdapter$app_fossilRelease() {
        return this.g;
    }

    @DexIgnore
    public final a getCardEventListener$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public final ot3 getState$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.f.e && i2 == 0) {
            c();
        }
    }

    @DexIgnore
    public final void setAdapter(BaseAdapter baseAdapter) {
        wd4.b(baseAdapter, "adapter");
        BaseAdapter baseAdapter2 = this.g;
        if (baseAdapter2 != null) {
            try {
                baseAdapter2.unregisterDataSetObserver(this.j);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = l;
                local.d(str, "Exception when unregisterDataSetObserver e=" + e2);
            }
        }
        this.g = baseAdapter;
        BaseAdapter baseAdapter3 = this.g;
        if (baseAdapter3 != null) {
            baseAdapter3.registerDataSetObserver(this.j);
            this.f.c = baseAdapter.getCount();
            a(true);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void setAdapter$app_fossilRelease(BaseAdapter baseAdapter) {
        this.g = baseAdapter;
    }

    @DexIgnore
    public final void setCardEventListener$app_fossilRelease(a aVar) {
        this.i = aVar;
    }

    @DexIgnore
    public final void b() {
        this.h.getFirst().setContainerEventListener((CardContainerView.c) null);
        this.h.getFirst().setDraggable(false);
        if (this.h.size() > 1) {
            this.h.get(1).setContainerEventListener(this.k);
            this.h.get(1).setDraggable(true);
        }
    }

    @DexIgnore
    public final void a() {
        int i2 = this.e.a;
        for (int i3 = 0; i3 < i2; i3++) {
            CardContainerView cardContainerView = this.h.get(i3);
            wd4.a((Object) cardContainerView, "containers[i]");
            CardContainerView cardContainerView2 = cardContainerView;
            cardContainerView2.b();
            cardContainerView2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setScaleX(1.0f);
            cardContainerView2.setScaleY(1.0f);
            cardContainerView2.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void b(Point point, SwipeDirection swipeDirection) {
        wd4.b(point, "point");
        wd4.b(swipeDirection, "direction");
        b();
        a(point, (Animator.AnimatorListener) new e(this, point, swipeDirection));
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        a aVar = this.i;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(f2, f3);
            } else {
                wd4.a();
                throw null;
            }
        }
        nt3 nt3 = this.e;
        if (nt3.f) {
            int i2 = nt3.a;
            for (int i3 = 1; i3 < i2; i3++) {
                CardContainerView cardContainerView = this.h.get(i3);
                wd4.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                float f4 = (float) i3;
                float f5 = this.e.d;
                float f6 = 1.0f - (f4 * f5);
                float f7 = (float) (i3 - 1);
                float abs = f6 + (((1.0f - (f5 * f7)) - f6) * Math.abs(f2));
                cardContainerView2.setScaleX(abs);
                cardContainerView2.setScaleY(abs);
                float a2 = f4 * us3.a(this.e.c);
                if (this.e.e == StackFrom.Top) {
                    a2 *= -1.0f;
                }
                float a3 = f7 * us3.a(this.e.c);
                if (this.e.e == StackFrom.Top) {
                    a3 *= -1.0f;
                }
                cardContainerView2.setTranslationY(a2 - (Math.abs(f2) * (a2 - a3)));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context) {
        this(context, (AttributeSet) null);
        wd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Point point, Animator.AnimatorListener animatorListener) {
        getTopView().animate().translationX((float) point.x).translationY(-((float) point.y)).setDuration(400).setListener(animatorListener).start();
    }

    @DexIgnore
    public final void a(CardContainerView cardContainerView) {
        CardStackView cardStackView = (CardStackView) cardContainerView.getParent();
        if (cardStackView != null) {
            cardStackView.removeView(cardContainerView);
            cardStackView.addView(cardContainerView, 0);
        }
    }

    @DexIgnore
    public final void a(Point point, SwipeDirection swipeDirection) {
        wd4.b(point, "point");
        wd4.b(swipeDirection, "direction");
        g();
        this.f.b = point;
        c();
        this.f.a++;
        a aVar = this.i;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(swipeDirection);
            } else {
                wd4.a();
                throw null;
            }
        }
        f();
        this.h.getLast().setContainerEventListener((CardContainerView.c) null);
        this.h.getFirst().setContainerEventListener(this.k);
    }
}
