package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.ll2;
import com.fossil.blesdk.obfuscated.zt3;
import com.fossil.wearables.fossil.R;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class LinePageIndicator extends View implements zt3 {
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public RecyclerView h;
    @DexIgnore
    public ViewPager i;
    @DexIgnore
    public ViewPager.i j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public int o;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0161a();
        @DexIgnore
        public int e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.LinePageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.LinePageIndicator$a$a  reason: collision with other inner class name */
        public static class C0161a implements Parcelable.Creator<a> {
            @DexIgnore
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.e);
        }

        @DexIgnore
        public a(Parcel parcel) {
            super(parcel);
            this.e = parcel.readInt();
        }
    }

    @DexIgnore
    public LinePageIndicator(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private void setStrokeWidth(float f2) {
        this.f.setStrokeWidth(f2);
        this.e.setStrokeWidth(f2);
        invalidate();
    }

    @DexIgnore
    private void setUnselectedBorderWidth(float f2) {
        this.g.setStyle(Paint.Style.STROKE);
        this.g.setStrokeWidth(f2);
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void b(int i2) {
        ViewPager.i iVar = this.j;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            f2 = (float) size;
        } else {
            float strokeWidth = this.f.getStrokeWidth() + ((float) getPaddingTop()) + ((float) getPaddingBottom());
            f2 = mode == Integer.MIN_VALUE ? Math.min(strokeWidth, (float) size) : strokeWidth;
        }
        return (int) Math.ceil((double) f2);
    }

    @DexIgnore
    public final int d(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode != 1073741824) {
            ViewPager viewPager = this.i;
            if (viewPager != null) {
                int a2 = viewPager.getAdapter().a();
                f2 = ((float) (getPaddingLeft() + getPaddingRight())) + (((float) a2) * this.m) + (((float) (a2 - 1)) * this.n);
                if (mode == Integer.MIN_VALUE) {
                    f2 = Math.min(f2, (float) size);
                }
                return (int) Math.ceil((double) f2);
            }
        }
        f2 = (float) size;
        return (int) Math.ceil((double) f2);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        super.onDraw(canvas);
        ViewPager viewPager = this.i;
        int i3 = 0;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.h;
            i2 = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.h.getAdapter().getItemCount();
        } else {
            i2 = this.i.getAdapter().a();
        }
        if (i2 != 0) {
            if (this.k >= i2) {
                setCurrentItem(i2 - 1);
                return;
            }
            float f2 = this.m;
            float f3 = this.n;
            float f4 = f2 + f3;
            float f5 = (((float) i2) * f4) - f3;
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
            if (this.l) {
                paddingLeft += (((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (f5 / 2.0f);
            }
            float strokeWidth = this.f.getStrokeWidth();
            float strokeWidth2 = this.g.getStrokeWidth();
            while (i3 < i2) {
                float f6 = (((float) i3) * f4) + paddingLeft;
                float f7 = f6 + this.m;
                canvas.drawLine(f6, height, f7, height, i3 == this.k ? this.f : this.e);
                if (i3 != this.k) {
                    float f8 = strokeWidth2 / 2.0f;
                    canvas.drawRect(f6 + f8, height + ((strokeWidth2 - strokeWidth) / 2.0f), f7 - f8, height + ((strokeWidth - strokeWidth2) / 2.0f), this.g);
                }
                i3++;
            }
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(d(i2), c(i3));
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        this.k = aVar.e;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.e = this.k;
        return aVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.i;
        int i2 = 0;
        if (viewPager == null || viewPager.getAdapter() == null || this.i.getAdapter().a() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x = motionEvent.getX(motionEvent.findPointerIndex(this.q));
                    float f2 = x - this.p;
                    if (!this.r && Math.abs(f2) > ((float) this.o)) {
                        this.r = true;
                    }
                    if (this.r) {
                        this.p = x;
                        if (this.i.g() || this.i.a()) {
                            this.i.b(f2);
                        }
                    }
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.p = motionEvent.getX(actionIndex);
                        this.q = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.q) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.q = motionEvent.getPointerId(i2);
                        }
                        this.p = motionEvent.getX(motionEvent.findPointerIndex(this.q));
                    }
                }
            }
            if (!this.r) {
                int a2 = this.i.getAdapter().a();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.k > 0 && motionEvent.getX() < f3 - f4) {
                    if (action != 3) {
                        this.i.setCurrentItem(this.k - 1);
                    }
                    return true;
                } else if (this.k < a2 - 1 && motionEvent.getX() > f3 + f4) {
                    if (action != 3) {
                        this.i.setCurrentItem(this.k + 1);
                    }
                    return true;
                }
            }
            this.r = false;
            this.q = -1;
            if (this.i.g()) {
                this.i.d();
            }
        } else {
            this.q = motionEvent.getPointerId(0);
            this.p = motionEvent.getX();
        }
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.i == null && this.h == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.i;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (ll2.a(getContext())) {
                i2 = (this.i.getAdapter().a() - i2) - 1;
            }
            this.k = i2;
        } else {
            if (ll2.a(getContext())) {
                i2 = (this.h.getAdapter().getItemCount() - i2) - 1;
            }
            this.k = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.j = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.i, viewPager)) {
            if (viewPager.getAdapter() != null) {
                this.i = viewPager;
                this.i.a((ViewPager.i) this);
                invalidate();
                setCurrentItem(0);
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiLinePageIndicatorStyle);
    }

    @DexIgnore
    public LinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.e = new Paint(1);
        this.f = new Paint(1);
        this.g = new Paint(1);
        this.p = -1.0f;
        this.q = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = k6.a(getContext(), (int) R.color.fossilOrange);
            int a3 = k6.a(getContext(), (int) R.color.coolGrey);
            float dimension = resources.getDimension(R.dimen.default_line_indicator_line_width);
            float dimension2 = resources.getDimension(R.dimen.default_line_indicator_gap_width);
            float dimension3 = resources.getDimension(R.dimen.default_line_indicator_stroke_width);
            float dimension4 = resources.getDimension(R.dimen.default_line_indicator_stroke_width);
            boolean z = resources.getBoolean(R.bool.default_line_indicator_centered);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i62.LinePageIndicator, i2, 0);
            this.l = obtainStyledAttributes.getBoolean(2, z);
            this.m = obtainStyledAttributes.getDimension(8, dimension);
            this.n = obtainStyledAttributes.getDimension(1, dimension2);
            setStrokeWidth(obtainStyledAttributes.getDimension(4, dimension3));
            setUnselectedBorderWidth(obtainStyledAttributes.getDimension(6, dimension4));
            this.e.setColor(obtainStyledAttributes.getColor(7, a3));
            this.f.setColor(obtainStyledAttributes.getColor(3, a2));
            this.g.setColor(obtainStyledAttributes.getColor(5, a3));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            this.o = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    public void a(int i2, float f2, int i3) {
        ViewPager.i iVar = this.j;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.k = ll2.a(getContext()) ? (this.i.getAdapter().a() - i2) - 1 : i2;
        invalidate();
        ViewPager.i iVar = this.j;
        if (iVar != null) {
            iVar.a(i2);
        }
    }
}
