package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WeatherCondition {
    CLEAR_DAY(WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY, "clear-day"),
    CLEAR_NIGHT(WeatherComplicationAppInfo.WeatherCondition.CLEAR_NIGHT, "clear-night"),
    RAIN(WeatherComplicationAppInfo.WeatherCondition.RAIN, "rain"),
    SNOW(WeatherComplicationAppInfo.WeatherCondition.SNOW, "snow"),
    SLEET(WeatherComplicationAppInfo.WeatherCondition.SLEET, "sleet"),
    WIND(WeatherComplicationAppInfo.WeatherCondition.WIND, "wind"),
    FOG(WeatherComplicationAppInfo.WeatherCondition.FOG, "fog"),
    CLOUDY(WeatherComplicationAppInfo.WeatherCondition.CLOUDY, "cloudy"),
    PARTLY_CLOUDY_DAY(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_DAY, "partly-cloudy-day"),
    PARTLY_CLOUDY_NIGHT(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_NIGHT, "partly-cloudy-night");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String mConditionDescription;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition mConditionValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WeatherComplicationAppInfo.WeatherCondition a(String str) {
            WeatherCondition weatherCondition;
            WeatherCondition[] values = WeatherCondition.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    weatherCondition = null;
                    break;
                }
                weatherCondition = values[i];
                if (wd4.a((Object) weatherCondition.mConditionDescription, (Object) str)) {
                    break;
                }
                i++;
            }
            if (weatherCondition != null) {
                WeatherComplicationAppInfo.WeatherCondition access$getMConditionValue$p = weatherCondition.mConditionValue;
                if (access$getMConditionValue$p != null) {
                    return access$getMConditionValue$p;
                }
            }
            return WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    WeatherCondition(WeatherComplicationAppInfo.WeatherCondition weatherCondition, String str) {
        this.mConditionValue = weatherCondition;
        this.mConditionDescription = str;
    }

    @DexIgnore
    public static final WeatherComplicationAppInfo.WeatherCondition getConditionValue(String str) {
        return Companion.a(str);
    }
}
