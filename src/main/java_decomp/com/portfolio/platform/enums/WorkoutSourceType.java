package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WorkoutSourceType {
    DEVICE("device"),
    MANUAL("manual");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WorkoutSourceType a(String str) {
            WorkoutSourceType workoutSourceType;
            wd4.b(str, "value");
            WorkoutSourceType[] values = WorkoutSourceType.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutSourceType = null;
                    break;
                }
                workoutSourceType = values[i];
                String mValue = workoutSourceType.getMValue();
                String lowerCase = str.toLowerCase();
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (wd4.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return workoutSourceType != null ? workoutSourceType : WorkoutSourceType.DEVICE;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    WorkoutSourceType(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        wd4.b(str, "<set-?>");
        this.mValue = str;
    }
}
