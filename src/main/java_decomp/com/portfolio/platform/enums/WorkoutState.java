package com.portfolio.platform.enums;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WorkoutState {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop"),
    PAUSE("pause"),
    RESUME("resume"),
    IDLE("idle");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WorkoutState a(String str) {
            WorkoutState workoutState;
            wd4.b(str, "value");
            WorkoutState[] values = WorkoutState.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    workoutState = null;
                    break;
                }
                workoutState = values[i];
                String mValue = workoutState.getMValue();
                String lowerCase = str.toLowerCase();
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (wd4.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return workoutState != null ? workoutState : WorkoutState.IDLE;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final WorkoutState a(int i) {
            if (R.attr.type == com.fossil.fitness.WorkoutState.START.ordinal()) {
                return WorkoutState.START;
            }
            if (R.attr.type == com.fossil.fitness.WorkoutState.END.ordinal()) {
                return WorkoutState.STOP;
            }
            if (R.attr.type == com.fossil.fitness.WorkoutState.PAUSE.ordinal()) {
                return WorkoutState.PAUSE;
            }
            if (R.attr.type == com.fossil.fitness.WorkoutState.RESUME.ordinal()) {
                return WorkoutState.RESUME;
            }
            return WorkoutState.IDLE;
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    WorkoutState(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        wd4.b(str, "<set-?>");
        this.mValue = str;
    }
}
