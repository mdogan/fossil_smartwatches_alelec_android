package com.portfolio.platform.enums;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FitnessMovementType {
    WALKING("walking"),
    RUNNING("running"),
    BIKING("biking");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    FitnessMovementType(String str) {
        this.value = str;
    }

    @DexIgnore
    public static FitnessMovementType fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (FitnessMovementType fitnessMovementType : values()) {
                if (str.equalsIgnoreCase(fitnessMovementType.value)) {
                    return fitnessMovementType;
                }
            }
        }
        return WALKING;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
