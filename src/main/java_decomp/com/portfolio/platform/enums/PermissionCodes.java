package com.portfolio.platform.enums;

import com.fossil.blesdk.obfuscated.tt1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum PermissionCodes {
    BLUETOOTH_OFF(0),
    LOCATION_PERMISSION_OFF(1),
    LOCATION_SERVICE_OFF(2),
    LOCATION_PERMISSION_FEATURE_OFF(3),
    LOCATION_SERVICE_FEATURE_OFF(4),
    BACKGROUND_LOCATION_PERMISSION_OFF(5);
    
    @DexIgnore
    public int mCode;

    @DexIgnore
    PermissionCodes(int i) {
        this.mCode = i;
    }

    @DexIgnore
    public static List<PermissionCodes> convertBLEPermissionErrorCode(ArrayList<Integer> arrayList) {
        tt1.a(arrayList);
        ArrayList arrayList2 = new ArrayList();
        Iterator<Integer> it = arrayList.iterator();
        while (it.hasNext()) {
            int intValue = it.next().intValue();
            if (intValue == 1101) {
                arrayList2.add(BLUETOOTH_OFF);
            } else if (intValue == 1112) {
                arrayList2.add(LOCATION_SERVICE_OFF);
            } else if (intValue == 1113) {
                arrayList2.add(LOCATION_PERMISSION_OFF);
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public int getCode() {
        return this.mCode;
    }
}
