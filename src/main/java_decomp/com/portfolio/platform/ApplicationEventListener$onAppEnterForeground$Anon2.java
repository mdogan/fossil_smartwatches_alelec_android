package com.portfolio.platform;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$Anon2", f = "ApplicationEventListener.kt", l = {77, 78, 79, 81, 82, 83, 86, 87, 91, 92, 93, 94, 95, 98, 99, 100}, m = "invokeSuspend")
public final class ApplicationEventListener$onAppEnterForeground$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $mActiveSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ApplicationEventListener this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ApplicationEventListener$onAppEnterForeground$Anon2(ApplicationEventListener applicationEventListener, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = applicationEventListener;
        this.$mActiveSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        ApplicationEventListener$onAppEnterForeground$Anon2 applicationEventListener$onAppEnterForeground$Anon2 = new ApplicationEventListener$onAppEnterForeground$Anon2(this.this$Anon0, this.$mActiveSerial, kc4);
        applicationEventListener$onAppEnterForeground$Anon2.p$ = (lh4) obj;
        return applicationEventListener$onAppEnterForeground$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ApplicationEventListener$onAppEnterForeground$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0152, code lost:
        r14 = r13.this$Anon0.i;
        r13.L$Anon0 = r6;
        r13.L$Anon1 = r5;
        r13.L$Anon2 = r1;
        r13.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0165, code lost:
        if (r14.downloadDeviceList(r13) != r0) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0167, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0168, code lost:
        r14 = r13.this$Anon0.j;
        r13.L$Anon0 = r6;
        r13.L$Anon1 = r5;
        r13.L$Anon2 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x017b, code lost:
        if (r14.loadUserInfo(r13) != r0) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x017d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x017e, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, com.portfolio.platform.PortfolioApp.W.c().e(), com.portfolio.platform.ApplicationEventListener.p.a(), "[App Open] Start download firmware");
        r14 = com.portfolio.platform.util.DeviceUtils.g.a();
        r13.L$Anon0 = r6;
        r13.L$Anon1 = r5;
        r13.L$Anon2 = r1;
        r13.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01b0, code lost:
        if (r14.a((com.fossil.blesdk.obfuscated.kc4<? super com.fossil.blesdk.obfuscated.cb4>) r13) != r0) goto L_0x01b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01b2, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01b3, code lost:
        r14 = r13.this$Anon0.i;
        r13.L$Anon0 = r6;
        r13.L$Anon1 = r5;
        r13.L$Anon2 = r1;
        r13.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01c6, code lost:
        if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r14, 0, r13, 1, (java.lang.Object) null) != r0) goto L_0x01c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01c8, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01c9, code lost:
        r2 = r5;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01cb, code lost:
        r14 = com.portfolio.platform.PortfolioApp.W.c().p();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01d5, code lost:
        if (r14 == null) goto L_0x0302;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x01d7, code lost:
        r14 = r14.a((com.portfolio.platform.localization.LocalizationManager.d) null);
        r13.L$Anon0 = r5;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01e8, code lost:
        if (r14.a((com.fossil.blesdk.obfuscated.kc4<? super com.fossil.blesdk.obfuscated.cb4>) r13) != r0) goto L_0x01eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01ea, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01eb, code lost:
        r3 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01f2, code lost:
        if (android.text.TextUtils.isEmpty(r13.$mActiveSerial) != false) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01f4, code lost:
        r14 = r13.this$Anon0.d;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0207, code lost:
        if (r14.downloadCategories(r13) != r0) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0209, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x020a, code lost:
        r14 = r13.this$Anon0.l;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x021e, code lost:
        if (r14.downloadAlarms(r13) != r0) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0220, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0221, code lost:
        r14 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r13.$mActiveSerial);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0227, code lost:
        if (r14 != null) goto L_0x022a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0232, code lost:
        if (com.fossil.blesdk.obfuscated.n42.a[r14.ordinal()] == 1) goto L_0x027f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0234, code lost:
        r14 = r13.this$Anon0.c;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x024a, code lost:
        if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x024c, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x024d, code lost:
        r14 = r13.this$Anon0.g;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0263, code lost:
        if (r14.downloadAllMicroApp(r4, r13) != r0) goto L_0x0266;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0265, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0266, code lost:
        r14 = r13.this$Anon0.c;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x027c, code lost:
        if (r14.downloadPresetList(r4, r13) != r0) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x027e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x027f, code lost:
        r14 = r13.this$Anon0.e;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0295, code lost:
        if (r14.downloadWatchApp(r4, r13) != r0) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0297, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0298, code lost:
        r14 = r13.this$Anon0.f;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x02ae, code lost:
        if (r14.downloadAllComplication(r4, r13) != r0) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x02b0, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x02b1, code lost:
        r14 = r13.this$Anon0.h;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x02c7, code lost:
        if (r14.downloadPresetList(r4, r13) != r0) goto L_0x02ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x02c9, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x02ca, code lost:
        r14 = r13.this$Anon0.h;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02e0, code lost:
        if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x02e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02e2, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x02e3, code lost:
        r14 = r13.this$Anon0.m;
        r4 = r13.$mActiveSerial;
        r13.L$Anon0 = r3;
        r13.L$Anon1 = r2;
        r13.L$Anon2 = r1;
        r13.label = 13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02f9, code lost:
        if (r14.getWatchFacesFromServer(r4, r13) != r0) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x02fb, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x02fc, code lost:
        r13.this$Anon0.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0302, code lost:
        com.fossil.blesdk.obfuscated.wd4.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0305, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0319, code lost:
        return com.fossil.blesdk.obfuscated.cb4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        lh4 lh4;
        MFUser mFUser;
        MFUser mFUser2;
        lh4 lh42;
        MFUser mFUser3;
        Object a = oc4.a();
        switch (this.label) {
            case 0:
                za4.a(obj);
                lh4 lh43 = this.p$;
                mFUser2 = this.this$Anon0.j.getCurrentUser();
                if (mFUser2 == null) {
                    FLogger.INSTANCE.getLocal().d(ApplicationEventListener.p.a(), "user is not log in yet, do nothing");
                    break;
                } else {
                    if (!TextUtils.isEmpty(this.$mActiveSerial)) {
                        this.this$Anon0.a();
                    }
                    if (PortfolioApp.W.c().w()) {
                        WatchLocalizationRepository m = this.this$Anon0.n;
                        this.L$Anon0 = lh43;
                        this.L$Anon1 = mFUser2;
                        this.L$Anon2 = mFUser2;
                        this.label = 1;
                        if (m.getWatchLocalizationFromServer(false, this) != a) {
                            lh42 = lh43;
                            mFUser3 = mFUser2;
                            break;
                        } else {
                            return a;
                        }
                    }
                }
                break;
            case 1:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser3 = (MFUser) this.L$Anon1;
                lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 2:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser3 = (MFUser) this.L$Anon1;
                lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 3:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser3 = (MFUser) this.L$Anon1;
                lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 4:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser3 = (MFUser) this.L$Anon1;
                lh42 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 5:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 lh44 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 6:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 7:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 8:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 9:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 10:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 11:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 12:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 13:
            case 16:
                MFUser mFUser4 = (MFUser) this.L$Anon2;
                MFUser mFUser5 = (MFUser) this.L$Anon1;
                lh4 lh45 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 14:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            case 15:
                mFUser2 = (MFUser) this.L$Anon2;
                mFUser = (MFUser) this.L$Anon1;
                lh4 = (lh4) this.L$Anon0;
                za4.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
