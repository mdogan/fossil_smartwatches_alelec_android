package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x52;
import com.fossil.blesdk.obfuscated.za4;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.MigrationManager$migrateFor4Dot0$Anon7", f = "MigrationManager.kt", l = {259}, m = "invokeSuspend")
public final class MigrationManager$migrateFor4Dot0$Anon7 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor4Dot0$Anon7(MigrationManager migrationManager, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = migrationManager;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MigrationManager$migrateFor4Dot0$Anon7 migrationManager$migrateFor4Dot0$Anon7 = new MigrationManager$migrateFor4Dot0$Anon7(this.this$Anon0, kc4);
        migrationManager$migrateFor4Dot0$Anon7.p$ = (lh4) obj;
        return migrationManager$migrateFor4Dot0$Anon7;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationManager$migrateFor4Dot0$Anon7) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            GetHybridDeviceSettingUseCase b = this.this$Anon0.d;
            pn3 pn3 = new pn3(this.this$Anon0.e.e());
            this.L$Anon0 = lh4;
            this.label = 1;
            if (x52.a(b, pn3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
