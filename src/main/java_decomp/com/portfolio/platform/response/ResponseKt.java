package com.portfolio.platform.response;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qm4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ResponseKt {
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public static final <T> Object a(jd4<? super kc4<? super cs4<T>>, ? extends Object> jd4, kc4<? super ro2<T>> kc4) {
        ResponseKt$handleRequest$Anon1 responseKt$handleRequest$Anon1;
        int i;
        if (kc4 instanceof ResponseKt$handleRequest$Anon1) {
            responseKt$handleRequest$Anon1 = (ResponseKt$handleRequest$Anon1) kc4;
            int i2 = responseKt$handleRequest$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                responseKt$handleRequest$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = responseKt$handleRequest$Anon1.result;
                Object a = oc4.a();
                i = responseKt$handleRequest$Anon1.label;
                if (i != 0) {
                    za4.a((Object) obj);
                    responseKt$handleRequest$Anon1.L$Anon0 = jd4;
                    responseKt$handleRequest$Anon1.label = 1;
                    obj = jd4.invoke(responseKt$handleRequest$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    jd4 jd42 = (jd4) responseKt$handleRequest$Anon1.L$Anon0;
                    try {
                        za4.a((Object) obj);
                    } catch (Exception e) {
                        Exception exc = e;
                        if (exc instanceof SocketTimeoutException) {
                            return new qo2(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, exc, (String) null, 8, (rd4) null);
                        }
                        if (exc instanceof UnknownHostException) {
                            return new qo2(601, (ServerError) null, exc, (String) null, 8, (rd4) null);
                        }
                        return new qo2(600, (ServerError) null, exc, (String) null, 8, (rd4) null);
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return a((cs4) obj);
            }
        }
        responseKt$handleRequest$Anon1 = new ResponseKt$handleRequest$Anon1(kc4);
        Object obj2 = responseKt$handleRequest$Anon1.result;
        Object a2 = oc4.a();
        i = responseKt$handleRequest$Anon1.label;
        if (i != 0) {
        }
        return a((cs4) obj2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0102, code lost:
        if (r1 != null) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0123, code lost:
        if (r1 != null) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0171, code lost:
        if (r1 != null) goto L_0x0178;
     */
    @DexIgnore
    public static final <T> ro2<T> b(cs4<T> cs4) {
        String str;
        String str2;
        qo2 qo2;
        String str3;
        boolean z = true;
        Integer num = null;
        if (cs4.d()) {
            T a = cs4.a();
            if (cs4.f().A() != null) {
                FLogger.INSTANCE.getLocal().d("RepoResponse", "cacheResponse valid");
            } else {
                z = false;
            }
            if (cs4.f().G() != null) {
                Response G = cs4.f().G();
                if (G == null) {
                    wd4.a();
                    throw null;
                } else if (G.B() != 304) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("networkResponse valid httpCode ");
                    Response G2 = cs4.f().G();
                    if (G2 != null) {
                        num = Integer.valueOf(G2.B());
                    }
                    sb.append(num);
                    local.d("RepoResponse", sb.toString());
                    z = false;
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("RepoResponse", "isFromCache=" + z);
            return new so2(a, z);
        }
        int b = cs4.b();
        if (lb4.b((T[]) new Integer[]{504, 503, 500, 401, Integer.valueOf(MFNetworkReturnCode.RATE_LIMIT_EXEEDED), 601, Integer.valueOf(MFNetworkReturnCode.CLIENT_TIMEOUT), 413}, Integer.valueOf(b))) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            qm4 c = cs4.c();
            if (c != null) {
                str3 = c.F();
            }
            str3 = cs4.e();
            serverError.setMessage(str3);
            return new qo2(b, serverError, (Throwable) null, (String) null, 8, (rd4) null);
        }
        qm4 c2 = cs4.c();
        if (c2 != null) {
            str = c2.F();
        }
        str = cs4.e();
        try {
            ServerError serverError2 = (ServerError) new Gson().a(str, ServerError.class);
            if (serverError2 != null) {
                Integer code = serverError2.getCode();
                if (code != null) {
                    if (code.intValue() == 0) {
                    }
                }
                qo2 = new qo2(cs4.b(), serverError2, (Throwable) null, (String) null, 8, (rd4) null);
                return qo2;
            }
            qo2 = new qo2(cs4.b(), (ServerError) null, (Throwable) null, str);
            return qo2;
        } catch (Exception unused) {
            qm4 c3 = cs4.c();
            if (c3 != null) {
                str2 = c3.F();
            }
            str2 = cs4.e();
            return new qo2(cs4.b(), new ServerError(b, str2), (Throwable) null, (String) null, 8, (rd4) null);
        }
    }

    @DexIgnore
    public static final <T> ro2<T> a(cs4<T> cs4) {
        wd4.b(cs4, "$this$createRepoResponse");
        try {
            return b(cs4);
        } catch (Throwable th) {
            return a(th);
        }
    }

    @DexIgnore
    public static final <T> qo2<T> a(Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("RepoResponse", "create=" + th.getMessage());
        if (th instanceof SocketTimeoutException) {
            return new qo2(MFNetworkReturnCode.CLIENT_TIMEOUT, (ServerError) null, th, (String) null, 8, (rd4) null);
        }
        if (th instanceof UnknownHostException) {
            return new qo2(601, (ServerError) null, th, (String) null, 8, (rd4) null);
        }
        return new qo2(600, (ServerError) null, th, (String) null, 8, (rd4) null);
    }
}
