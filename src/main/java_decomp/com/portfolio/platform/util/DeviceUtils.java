package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kg4;
import com.fossil.blesdk.obfuscated.lg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.vn2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.response.ResponseKt;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceUtils {
    @DexIgnore
    public static DeviceUtils e;
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a((rd4) null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public fn2 b;
    @DexIgnore
    public GuestApiService c;
    @DexIgnore
    public FirmwareFileRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(DeviceUtils deviceUtils) {
            DeviceUtils.e = deviceUtils;
        }

        @DexIgnore
        public final DeviceUtils b() {
            return DeviceUtils.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final synchronized DeviceUtils a() {
            DeviceUtils b;
            if (DeviceUtils.g.b() == null) {
                DeviceUtils.g.a(new DeviceUtils((rd4) null));
            }
            b = DeviceUtils.g.b();
            if (b == null) {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.util.DeviceUtils");
            }
            return b;
        }
    }

    /*
    static {
        String simpleName = DeviceUtils.class.getSimpleName();
        wd4.a((Object) simpleName, "DeviceUtils::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public DeviceUtils() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final GuestApiService a() {
        GuestApiService guestApiService = this.c;
        if (guestApiService != null) {
            return guestApiService;
        }
        wd4.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final boolean b(String str) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        return ((Boolean) lg4.a((CoroutineContext) null, new DeviceUtils$isDeviceDianaEV1Java$Anon1(this, str, (kc4) null), 1, (Object) null)).booleanValue();
    }

    @DexIgnore
    public /* synthetic */ DeviceUtils(rd4 rd4) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x017b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object a(kc4<? super cb4> kc4) {
        DeviceUtils deviceUtils;
        DeviceUtils$downloadActiveDeviceFirmware$Anon1 deviceUtils$downloadActiveDeviceFirmware$Anon1;
        int i;
        Object obj;
        Iterator it;
        String str;
        ArrayList arrayList;
        DeviceUtils deviceUtils2;
        DeviceUtils deviceUtils3;
        ArrayList arrayList2;
        String str2;
        Iterator it2;
        Object obj2;
        String str3;
        ro2 ro2;
        kc4<? super cb4> kc42 = kc4;
        if (kc42 instanceof DeviceUtils$downloadActiveDeviceFirmware$Anon1) {
            deviceUtils$downloadActiveDeviceFirmware$Anon1 = (DeviceUtils$downloadActiveDeviceFirmware$Anon1) kc42;
            int i2 = deviceUtils$downloadActiveDeviceFirmware$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceUtils$downloadActiveDeviceFirmware$Anon1.label = i2 - Integer.MIN_VALUE;
                deviceUtils = this;
                Object obj3 = deviceUtils$downloadActiveDeviceFirmware$Anon1.result;
                Object a2 = oc4.a();
                i = deviceUtils$downloadActiveDeviceFirmware$Anon1.label;
                int i3 = 1;
                if (i != 0) {
                    za4.a(obj3);
                    FLogger.INSTANCE.getLocal().d(f, "downloadActiveDeviceFirmware");
                    if (en2.p.a().n().b() == null) {
                        return cb4.a;
                    }
                    ArrayList arrayList3 = new ArrayList();
                    str = PortfolioApp.W.c().e();
                    if (str.length() > 0) {
                        List<String> c2 = DeviceHelper.o.c(str);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str4 = f;
                        local.d(str4, "active device skus " + c2);
                        if (!c2.isEmpty()) {
                            arrayList3.addAll(c2);
                        } else {
                            String[] b2 = DeviceHelper.o.b();
                            arrayList3.addAll(Arrays.asList((String[]) Arrays.copyOf(b2, b2.length)));
                        }
                    } else {
                        String[] b3 = DeviceHelper.o.b();
                        arrayList3.addAll(Arrays.asList((String[]) Arrays.copyOf(b3, b3.length)));
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = f;
                    local2.d(str5, "download firmware for models " + arrayList3);
                    it = arrayList3.iterator();
                    obj = a2;
                    arrayList = arrayList3;
                    deviceUtils2 = deviceUtils;
                } else if (i == 1) {
                    it2 = (Iterator) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon4;
                    za4.a(obj3);
                    DeviceUtils deviceUtils4 = (DeviceUtils) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon0;
                    obj2 = a2;
                    str3 = (String) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon3;
                    str2 = (String) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon2;
                    arrayList2 = (ArrayList) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon1;
                    deviceUtils3 = deviceUtils4;
                    ro2 = (ro2) obj3;
                    if (ro2 instanceof so2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = f;
                        StringBuilder sb = new StringBuilder();
                        sb.append("downloadActiveDeviceFirmware - get latest fw SUCCESS isFromCache ");
                        so2 so2 = (so2) ro2;
                        sb.append(so2.b());
                        local3.d(str6, sb.toString());
                        if (!so2.b()) {
                            gh4 b4 = zh4.b();
                            DeviceUtils$downloadActiveDeviceFirmware$Anon2 deviceUtils$downloadActiveDeviceFirmware$Anon2 = new DeviceUtils$downloadActiveDeviceFirmware$Anon2(deviceUtils3, ro2, (kc4) null);
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon0 = deviceUtils3;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon1 = arrayList2;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon2 = str2;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon3 = str3;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon4 = it2;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon5 = ro2;
                            deviceUtils$downloadActiveDeviceFirmware$Anon1.label = 2;
                            if (kg4.a(b4, deviceUtils$downloadActiveDeviceFirmware$Anon2, deviceUtils$downloadActiveDeviceFirmware$Anon1) == obj2) {
                                return obj2;
                            }
                            a2 = obj2;
                            deviceUtils2 = deviceUtils3;
                            ArrayList arrayList4 = arrayList2;
                            obj = a2;
                            arrayList = arrayList4;
                            String str7 = str2;
                            it = it2;
                            str = str7;
                            i3 = 1;
                            return obj2;
                        }
                    } else if (ro2 instanceof qo2) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str8 = f;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("downloadActiveDeviceFirmware - get latest firmwares FAILED!!! {code=");
                        qo2 qo2 = (qo2) ro2;
                        sb2.append(qo2.a());
                        sb2.append(", message=");
                        ServerError c3 = qo2.c();
                        sb2.append(c3 != null ? c3.getMessage() : null);
                        sb2.append('}');
                        local4.e(str8, sb2.toString());
                    }
                    arrayList = arrayList2;
                    deviceUtils2 = deviceUtils3;
                    obj = obj2;
                    String str9 = str2;
                    it = it2;
                    str = str9;
                    i3 = 1;
                } else if (i == 2) {
                    ro2 ro22 = (ro2) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon5;
                    it2 = (Iterator) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon4;
                    String str10 = (String) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon3;
                    str2 = (String) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon2;
                    arrayList2 = (ArrayList) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon1;
                    deviceUtils3 = (DeviceUtils) deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon0;
                    za4.a(obj3);
                    deviceUtils2 = deviceUtils3;
                    ArrayList arrayList42 = arrayList2;
                    obj = a2;
                    arrayList = arrayList42;
                    String str72 = str2;
                    it = it2;
                    str = str72;
                    i3 = 1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (it.hasNext()) {
                    String str11 = (String) it.next();
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str12 = f;
                    local5.d(str12, "downloadActiveDeviceFirmware - get latest fw by model=" + str11);
                    DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1 deviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1 = new DeviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1(deviceUtils2, str11, (kc4) null);
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon0 = deviceUtils2;
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon1 = arrayList;
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon2 = str;
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon3 = str11;
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.L$Anon4 = it;
                    deviceUtils$downloadActiveDeviceFirmware$Anon1.label = i3;
                    Object a3 = ResponseKt.a(deviceUtils$downloadActiveDeviceFirmware$repoResponse$Anon1, deviceUtils$downloadActiveDeviceFirmware$Anon1);
                    if (a3 == obj) {
                        return obj;
                    }
                    String str13 = str11;
                    deviceUtils3 = deviceUtils2;
                    obj3 = a3;
                    obj2 = obj;
                    arrayList2 = arrayList;
                    str3 = str13;
                    Iterator it3 = it;
                    str2 = str;
                    it2 = it3;
                    ro2 = (ro2) obj3;
                    if (ro2 instanceof so2) {
                    }
                    arrayList = arrayList2;
                    deviceUtils2 = deviceUtils3;
                    obj = obj2;
                    String str92 = str2;
                    it = it2;
                    str = str92;
                    i3 = 1;
                    if (it.hasNext()) {
                    }
                    return obj;
                }
                return cb4.a;
            }
        }
        deviceUtils = this;
        deviceUtils$downloadActiveDeviceFirmware$Anon1 = new DeviceUtils$downloadActiveDeviceFirmware$Anon1(deviceUtils, kc42);
        Object obj32 = deviceUtils$downloadActiveDeviceFirmware$Anon1.result;
        Object a22 = oc4.a();
        i = deviceUtils$downloadActiveDeviceFirmware$Anon1.label;
        int i32 = 1;
        if (i != 0) {
        }
        if (it.hasNext()) {
        }
        return cb4.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(Firmware firmware, kc4<? super Boolean> kc4) {
        DeviceUtils$downloadDetailFirmware$Anon1 deviceUtils$downloadDetailFirmware$Anon1;
        Object obj;
        int i;
        vn2 vn2;
        Firmware firmware2;
        if (kc4 instanceof DeviceUtils$downloadDetailFirmware$Anon1) {
            deviceUtils$downloadDetailFirmware$Anon1 = (DeviceUtils$downloadDetailFirmware$Anon1) kc4;
            int i2 = deviceUtils$downloadDetailFirmware$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceUtils$downloadDetailFirmware$Anon1.label = i2 - Integer.MIN_VALUE;
                obj = deviceUtils$downloadDetailFirmware$Anon1.result;
                Object a2 = oc4.a();
                i = deviceUtils$downloadDetailFirmware$Anon1.label;
                boolean z = true;
                if (i != 0) {
                    za4.a(obj);
                    vn2 e2 = en2.p.a().e();
                    Firmware a3 = e2.a(firmware.getDeviceModel());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = f;
                    local.d(str, "downloadDetailFirmware - latestFw=" + firmware + ", localFw=" + a3 + ", from URL=" + firmware.getDownloadUrl());
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    String e3 = PortfolioApp.W.c().e();
                    String str2 = f;
                    remote.i(component, session, e3, str2, "[FW Download] START DL detail FW of " + firmware.getVersionNumber());
                    if (a3 != null && cg4.b(a3.getVersionNumber(), firmware.getVersionNumber(), true)) {
                        FirmwareFileRepository firmwareFileRepository = this.d;
                        if (firmwareFileRepository != null) {
                            String versionNumber = a3.getVersionNumber();
                            wd4.a((Object) versionNumber, "localFirmware.versionNumber");
                            String checksum = a3.getChecksum();
                            wd4.a((Object) checksum, "localFirmware.checksum");
                            if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String str3 = f;
                                local2.d(str3, "Local fw of model=" + a3.getDeviceModel() + " is already latest, no need to re-download");
                                return pc4.a(z);
                            }
                        } else {
                            wd4.d("mFirmwareFileRepository");
                            throw null;
                        }
                    }
                    FirmwareFileRepository firmwareFileRepository2 = this.d;
                    if (firmwareFileRepository2 != null) {
                        String versionNumber2 = firmware.getVersionNumber();
                        wd4.a((Object) versionNumber2, "latestFirmware.versionNumber");
                        String downloadUrl = firmware.getDownloadUrl();
                        wd4.a((Object) downloadUrl, "latestFirmware.downloadUrl");
                        String checksum2 = firmware.getChecksum();
                        wd4.a((Object) checksum2, "latestFirmware.checksum");
                        deviceUtils$downloadDetailFirmware$Anon1.L$Anon0 = this;
                        deviceUtils$downloadDetailFirmware$Anon1.L$Anon1 = firmware;
                        deviceUtils$downloadDetailFirmware$Anon1.L$Anon2 = e2;
                        deviceUtils$downloadDetailFirmware$Anon1.L$Anon3 = a3;
                        deviceUtils$downloadDetailFirmware$Anon1.label = 1;
                        Object downloadFirmware = firmwareFileRepository2.downloadFirmware(versionNumber2, downloadUrl, checksum2, deviceUtils$downloadDetailFirmware$Anon1);
                        if (downloadFirmware == a2) {
                            return a2;
                        }
                        firmware2 = firmware;
                        vn2 = e2;
                        obj = downloadFirmware;
                    } else {
                        wd4.d("mFirmwareFileRepository");
                        throw null;
                    }
                } else if (i == 1) {
                    Firmware firmware3 = (Firmware) deviceUtils$downloadDetailFirmware$Anon1.L$Anon3;
                    vn2 = (vn2) deviceUtils$downloadDetailFirmware$Anon1.L$Anon2;
                    firmware2 = (Firmware) deviceUtils$downloadDetailFirmware$Anon1.L$Anon1;
                    DeviceUtils deviceUtils = (DeviceUtils) deviceUtils$downloadDetailFirmware$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((File) obj) == null) {
                    vn2.a(firmware2);
                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component2 = FLogger.Component.APP;
                    FLogger.Session session2 = FLogger.Session.OTHER;
                    String e4 = PortfolioApp.W.c().e();
                    String str4 = f;
                    remote2.i(component2, session2, e4, str4, "[FW Download] DONE DL detail FW of " + firmware2.getVersionNumber());
                } else {
                    z = false;
                }
                return pc4.a(z);
            }
        }
        deviceUtils$downloadDetailFirmware$Anon1 = new DeviceUtils$downloadDetailFirmware$Anon1(this, kc4);
        obj = deviceUtils$downloadDetailFirmware$Anon1.result;
        Object a22 = oc4.a();
        i = deviceUtils$downloadDetailFirmware$Anon1.label;
        boolean z2 = true;
        if (i != 0) {
        }
        if (((File) obj) == null) {
        }
        return pc4.a(z2);
    }

    @DexIgnore
    public final boolean a(String str) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        DeviceRepository deviceRepository = this.a;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
            if (deviceBySerial == null) {
                return false;
            }
            String firmwareRevision = deviceBySerial.getFirmwareRevision();
            if (firmwareRevision != null) {
                return cg4.c(firmwareRevision, "DN0.0.0.", true);
            }
            return false;
        }
        wd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object a(String str, Device device, kc4<? super Boolean> kc4) {
        DeviceUtils$isLatestFirmware$Anon1 deviceUtils$isLatestFirmware$Anon1;
        int i;
        Device device2;
        Firmware firmware;
        String str2 = str;
        Device device3 = device;
        kc4<? super Boolean> kc42 = kc4;
        if (kc42 instanceof DeviceUtils$isLatestFirmware$Anon1) {
            deviceUtils$isLatestFirmware$Anon1 = (DeviceUtils$isLatestFirmware$Anon1) kc42;
            int i2 = deviceUtils$isLatestFirmware$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceUtils$isLatestFirmware$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceUtils$isLatestFirmware$Anon1.result;
                Object a2 = oc4.a();
                i = deviceUtils$isLatestFirmware$Anon1.label;
                if (i != 0) {
                    za4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("isLatestFirmware - deviceSerial=");
                    sb.append(str2);
                    sb.append(", activeDevice=");
                    sb.append(device3);
                    sb.append(", isSkipOTA=");
                    fn2 fn2 = this.b;
                    String str4 = null;
                    if (fn2 != null) {
                        sb.append(fn2.T());
                        local.d(str3, sb.toString());
                        fn2 fn22 = this.b;
                        if (fn22 == null) {
                            wd4.d("mSharePrefs");
                            throw null;
                        } else if (fn22.T()) {
                            return pc4.a(true);
                        } else {
                            if (device3 == null) {
                                DeviceRepository deviceRepository = this.a;
                                if (deviceRepository != null) {
                                    device2 = deviceRepository.getDeviceBySerial(str2);
                                } else {
                                    wd4.d("mDeviceRepository");
                                    throw null;
                                }
                            } else {
                                device2 = device3;
                            }
                            if (device2 == null) {
                                return pc4.a(true);
                            }
                            String firmwareRevision = device2.getFirmwareRevision();
                            if (!cg4.b("release", "release", true)) {
                                fn2 fn23 = this.b;
                                if (fn23 == null) {
                                    wd4.d("mSharePrefs");
                                    throw null;
                                } else if (fn23.C()) {
                                    fn2 fn24 = this.b;
                                    if (fn24 != null) {
                                        firmware = fn24.a(device2.getSku());
                                        if (firmware == null) {
                                            firmware = en2.p.a().e().a(device2.getSku());
                                        }
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str5 = f;
                                        local2.d(str5, "isLatestFirmware - latestFw=" + firmware);
                                        if (firmware != null) {
                                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                            String str6 = f;
                                            local3.e(str6, "isLatestFirmware - Error when update firmware, can't find latest fw of model=" + device2.getSku());
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon0 = this;
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon1 = str2;
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon2 = device3;
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon3 = device2;
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon4 = firmwareRevision;
                                            deviceUtils$isLatestFirmware$Anon1.L$Anon5 = firmware;
                                            deviceUtils$isLatestFirmware$Anon1.label = 1;
                                            if (a((kc4<? super cb4>) deviceUtils$isLatestFirmware$Anon1) == a2) {
                                                return a2;
                                            }
                                        } else {
                                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                            String str7 = f;
                                            local4.d(str7, "isLatestFirmware - CheckFirmwareActiveDeviceUseCase currentDeviceFw=" + firmwareRevision + ", latestFw=" + firmware.getVersionNumber());
                                            return pc4.a(cg4.b(firmware.getVersionNumber(), firmwareRevision, true));
                                        }
                                    } else {
                                        wd4.d("mSharePrefs");
                                        throw null;
                                    }
                                }
                            }
                            firmware = en2.p.a().e().a(device2.getSku());
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.OTHER;
                            String e2 = PortfolioApp.W.c().e();
                            String str8 = f;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("[Check FW] Latest FW of SKU ");
                            sb2.append(device2.getSku());
                            sb2.append(" in DB ");
                            if (firmware != null) {
                                str4 = firmware.getVersionNumber();
                            }
                            sb2.append(str4);
                            remote.i(component, session, e2, str8, sb2.toString());
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            String str52 = f;
                            local22.d(str52, "isLatestFirmware - latestFw=" + firmware);
                            if (firmware != null) {
                            }
                        }
                    } else {
                        wd4.d("mSharePrefs");
                        throw null;
                    }
                } else if (i == 1) {
                    Firmware firmware2 = (Firmware) deviceUtils$isLatestFirmware$Anon1.L$Anon5;
                    String str9 = (String) deviceUtils$isLatestFirmware$Anon1.L$Anon4;
                    Device device4 = (Device) deviceUtils$isLatestFirmware$Anon1.L$Anon3;
                    Device device5 = (Device) deviceUtils$isLatestFirmware$Anon1.L$Anon2;
                    String str10 = (String) deviceUtils$isLatestFirmware$Anon1.L$Anon1;
                    DeviceUtils deviceUtils = (DeviceUtils) deviceUtils$isLatestFirmware$Anon1.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return pc4.a(true);
            }
        }
        deviceUtils$isLatestFirmware$Anon1 = new DeviceUtils$isLatestFirmware$Anon1(this, kc42);
        Object obj2 = deviceUtils$isLatestFirmware$Anon1.result;
        Object a22 = oc4.a();
        i = deviceUtils$isLatestFirmware$Anon1.label;
        if (i != 0) {
        }
        return pc4.a(true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b8  */
    public final Pair<String, String> a(String str, Device device) {
        Firmware firmware;
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f;
        local.d(str2, "fetchFirmwaresInfo - deviceSerial=" + str + ", activeDevice=" + device);
        String str3 = null;
        if (device == null) {
            DeviceRepository deviceRepository = this.a;
            if (deviceRepository != null) {
                device = deviceRepository.getDeviceBySerial(str);
            } else {
                wd4.d("mDeviceRepository");
                throw null;
            }
        }
        if (device == null) {
            return new Pair<>(null, null);
        }
        String firmwareRevision = device.getFirmwareRevision();
        if (!cg4.b("release", "release", true)) {
            fn2 fn2 = this.b;
            if (fn2 == null) {
                wd4.d("mSharePrefs");
                throw null;
            } else if (fn2.C()) {
                fn2 fn22 = this.b;
                if (fn22 != null) {
                    firmware = fn22.a(device.getSku());
                    if (firmware == null) {
                        firmware = en2.p.a().e().a(device.getSku());
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = f;
                    local2.d(str4, "fetchFirmwaresInfo - latestFw=" + firmware);
                    if (firmware != null) {
                        str3 = firmware.getVersionNumber();
                    }
                    return new Pair<>(firmwareRevision, str3);
                }
                wd4.d("mSharePrefs");
                throw null;
            }
        }
        firmware = en2.p.a().e().a(device.getSku());
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str42 = f;
        local22.d(str42, "fetchFirmwaresInfo - latestFw=" + firmware);
        if (firmware != null) {
        }
        return new Pair<>(firmwareRevision, str3);
    }
}
