package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.id4;
import com.portfolio.platform.util.UserUtils;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserUtils$Companion$INSTANCE$Anon2 extends Lambda implements id4<UserUtils> {
    @DexIgnore
    public static /* final */ UserUtils$Companion$INSTANCE$Anon2 INSTANCE; // = new UserUtils$Companion$INSTANCE$Anon2();

    @DexIgnore
    public UserUtils$Companion$INSTANCE$Anon2() {
        super(0);
    }

    @DexIgnore
    public final UserUtils invoke() {
        return UserUtils.b.b.a();
    }
}
