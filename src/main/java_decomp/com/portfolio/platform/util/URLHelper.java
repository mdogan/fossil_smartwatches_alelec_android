package com.portfolio.platform.util;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.vr3;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class URLHelper {

    @DexIgnore
    public enum Feature {
        USING_YOUR_WATCH,
        LOW_BATTERY,
        SHOP_BATTERY,
        HOUR_TIME_24,
        ACTIVITY,
        ALARM,
        DATE,
        NOTIFICATIONS,
        Q_LINK,
        SECOND_TIME_ZONE,
        GOAL_TRACKING,
        CUSTOMIZE_DEVICE,
        STOP_WATCH
    }

    @DexIgnore
    public enum StaticPage {
        PRIVACY,
        SUPPORT,
        STORE,
        TERMS,
        CALL,
        FEATURES,
        REPAIR_CENTER,
        DEVICE_FEATURE,
        CHAT
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Feature.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[StaticPage.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(44:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|(3:61|62|64)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(49:0|(2:1|2)|3|5|6|7|(2:9|10)|11|(2:13|14)|15|17|18|19|(2:21|22)|23|(2:25|26)|27|29|30|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|64) */
        /* JADX WARNING: Can't wrap try/catch for region: R(51:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|(2:21|22)|23|25|26|27|29|30|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|64) */
        /* JADX WARNING: Can't wrap try/catch for region: R(52:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|(2:21|22)|23|25|26|27|29|30|31|(2:33|34)|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|64) */
        /* JADX WARNING: Can't wrap try/catch for region: R(53:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|(2:21|22)|23|25|26|27|29|30|31|33|34|35|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|64) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x0081 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x008b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0095 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x009f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00a9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00b3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00bd */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00c7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x00d1 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00dd */
        /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00e9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x00f5 */
        /*
        static {
            try {
                b[StaticPage.CHAT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[StaticPage.PRIVACY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[StaticPage.SUPPORT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[StaticPage.STORE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[StaticPage.TERMS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                b[StaticPage.REPAIR_CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                b[StaticPage.CALL.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                b[StaticPage.DEVICE_FEATURE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                b[StaticPage.FEATURES.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            a[Feature.USING_YOUR_WATCH.ordinal()] = 1;
            a[Feature.LOW_BATTERY.ordinal()] = 2;
            a[Feature.SHOP_BATTERY.ordinal()] = 3;
            a[Feature.HOUR_TIME_24.ordinal()] = 4;
            a[Feature.ACTIVITY.ordinal()] = 5;
            a[Feature.ALARM.ordinal()] = 6;
            a[Feature.DATE.ordinal()] = 7;
            a[Feature.NOTIFICATIONS.ordinal()] = 8;
            a[Feature.Q_LINK.ordinal()] = 9;
            a[Feature.SECOND_TIME_ZONE.ordinal()] = 10;
            a[Feature.GOAL_TRACKING.ordinal()] = 11;
            a[Feature.CUSTOMIZE_DEVICE.ordinal()] = 12;
            try {
                a[Feature.STOP_WATCH.ordinal()] = 13;
            } catch (NoSuchFieldError unused10) {
            }
        }
        */
    }

    @DexIgnore
    public static String a(StaticPage staticPage, Feature feature) {
        return a(staticPage, feature, (String) null);
    }

    @DexIgnore
    public static String a(StaticPage staticPage, Feature feature, String str) {
        StringBuilder sb = new StringBuilder(vr3.b.a(1));
        if (TextUtils.isEmpty(str)) {
            str = PortfolioApp.R.e();
        }
        switch (a.b[staticPage.ordinal()]) {
            case 1:
                return "http://chat.fossil.com/chat/";
            case 2:
                sb.append("/privacy");
                break;
            case 3:
                sb.append("/support");
                break;
            case 4:
                sb.append("/store");
                break;
            case 5:
                sb.append("/terms");
                break;
            case 6:
                sb.append("/service_centers");
                break;
            case 7:
                sb.append("/call");
                break;
            case 8:
                sb.append("/device_features/customize_device");
                break;
            case 9:
                sb.append(DeviceHelper.o.h(str) ? "/tracker" : "/hybrid");
                if (feature == null) {
                    sb.append("/using_your_watch");
                    break;
                } else {
                    switch (a.a[feature.ordinal()]) {
                        case 1:
                            sb.append("/using_your_watch");
                            break;
                        case 2:
                            sb.append("/low_battery");
                            break;
                        case 3:
                            sb.append("/shop_battery");
                            break;
                        case 4:
                            sb.append("/24_hour_time");
                            break;
                        case 5:
                            sb.append("/activity");
                            break;
                        case 6:
                            sb.append("/alarm");
                            break;
                        case 7:
                            sb.append("/date");
                            break;
                        case 8:
                            sb.append("/notifications");
                            break;
                        case 9:
                            sb.append("/link");
                            break;
                        case 10:
                            sb.append("/second_time_zone");
                            break;
                        case 11:
                            sb.append("/service_centers");
                            break;
                        case 12:
                            sb.append("/customize_device");
                            break;
                        case 13:
                            sb.append("/stopwatch");
                            break;
                        default:
                            sb.append("/using_your_watch");
                            break;
                    }
                }
        }
        sb.append(String.format("?locale=%s&platform=android&serialNumber=%s&version=%s", new Object[]{a(), str, "2"}));
        return sb.toString();
    }

    @DexIgnore
    public static String a() {
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (TextUtils.isEmpty(country)) {
            return language;
        }
        return String.format(Locale.US, "%s_%s", new Object[]{language, country});
    }
}
