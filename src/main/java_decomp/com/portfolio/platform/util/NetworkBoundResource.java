package com.portfolio.platform.util;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.bc;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.dc;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class NetworkBoundResource<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ bc<ps3<ResultType>> result; // = new bc<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<T> implements dc<S> {
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1")
        @sc4(c = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0158Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1$a")
            /* renamed from: com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1$a */
            public static final class a<T> implements dc<S> {
                @DexIgnore
                public /* final */ /* synthetic */ C0158Anon1 a;

                @DexIgnore
                public a(C0158Anon1 anon1) {
                    this.a = anon1;
                }

                @DexIgnore
                public final void a(ResultType resulttype) {
                    this.a.this$Anon0.a.setValue(ps3.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0158Anon1(Anon1 anon1, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                C0158Anon1 anon1 = new C0158Anon1(this.this$Anon0, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0158Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                oc4.a();
                if (this.label == 0) {
                    za4.a(obj);
                    this.this$Anon0.a.result.a(this.this$Anon0.b, new a(this));
                    return cb4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Anon1(NetworkBoundResource networkBoundResource, LiveData liveData) {
            this.a = networkBoundResource;
            this.b = liveData;
        }

        @DexIgnore
        public final void a(ResultType resulttype) {
            this.a.result.a(this.b);
            if (this.a.shouldFetch(resulttype)) {
                this.a.fetchFromNetwork(this.b);
            } else {
                ri4 unused = mg4.b(mh4.a(zh4.c()), (CoroutineContext) null, (CoroutineStart) null, new C0158Anon1(this, (kc4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public NetworkBoundResource() {
        this.result.b(ps3.e.a(null));
        this.isFromCache = true;
        LiveData loadFromDb = loadFromDb();
        this.result.a(loadFromDb, new Anon1(this, loadFromDb));
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new NetworkBoundResource$fetchFromNetwork$Anon1(this, liveData, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    private final void setValue(ps3<? extends ResultType> ps3) {
        if (!wd4.a((Object) this.result.a(), (Object) ps3)) {
            this.result.b(ps3);
        }
    }

    @DexIgnore
    public final LiveData<ps3<ResultType>> asLiveData() {
        bc<ps3<ResultType>> bcVar = this.result;
        if (bcVar != null) {
            return bcVar;
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(kc4<? super cs4<RequestType>> kc4);

    @DexIgnore
    public abstract LiveData<ResultType> loadFromDb();

    @DexIgnore
    public abstract void onFetchFailed(Throwable th);

    @DexIgnore
    public boolean processContinueFetching(RequestType requesttype) {
        return false;
    }

    @DexIgnore
    public RequestType processResponse(so2<RequestType> so2) {
        wd4.b(so2, "response");
        RequestType a = so2.a();
        if (a != null) {
            return a;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public abstract void saveCallResult(RequestType requesttype);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
