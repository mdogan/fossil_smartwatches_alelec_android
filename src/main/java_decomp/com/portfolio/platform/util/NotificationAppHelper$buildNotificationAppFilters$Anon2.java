package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.qx2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$Anon2", f = "NotificationAppHelper.kt", l = {152, 153}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ qx2 $getAllContactGroup;
    @DexIgnore
    public /* final */ /* synthetic */ wy2 $getApps;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ fn2 $sharedPrefs;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$Anon2(fn2 fn2, NotificationSettingsDatabase notificationSettingsDatabase, qx2 qx2, wy2 wy2, kc4 kc4) {
        super(2, kc4);
        this.$sharedPrefs = fn2;
        this.$notificationSettingsDatabase = notificationSettingsDatabase;
        this.$getAllContactGroup = qx2;
        this.$getApps = wy2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationAppHelper$buildNotificationAppFilters$Anon2 notificationAppHelper$buildNotificationAppFilters$Anon2 = new NotificationAppHelper$buildNotificationAppFilters$Anon2(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, kc4);
        notificationAppHelper$buildNotificationAppFilters$Anon2.p$ = (lh4) obj;
        return notificationAppHelper$buildNotificationAppFilters$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppHelper$buildNotificationAppFilters$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object obj2;
        List list2;
        sh4 sh4;
        sh4 sh42;
        Ref$IntRef ref$IntRef;
        Ref$IntRef ref$IntRef2;
        NotificationSettingsModel notificationSettingsModel;
        boolean z;
        NotificationSettingsModel notificationSettingsModel2;
        lh4 lh4;
        Object obj3;
        List list3;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            ref$IntRef2 = new Ref$IntRef();
            ref$IntRef2.element = 0;
            list = new ArrayList();
            z = this.$sharedPrefs.A();
            notificationSettingsModel2 = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
            notificationSettingsModel = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppHelper.a;
            local.d(a2, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsModel2 + ", messageSettingsTypeModel = " + notificationSettingsModel + ", isAllAppToggleEnabled = " + z);
            if (notificationSettingsModel2 != null) {
                ref$IntRef.element = notificationSettingsModel2.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            }
            if (notificationSettingsModel != null) {
                ref$IntRef2.element = notificationSettingsModel.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
            }
            sh4 a3 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1(this, ref$IntRef, ref$IntRef2, (kc4) null), 3, (Object) null);
            sh42 = mg4.a(lh42, (CoroutineContext) null, (CoroutineStart) null, new NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(this, list, z, (kc4) null), 3, (Object) null);
            this.L$Anon0 = lh42;
            this.L$Anon1 = ref$IntRef;
            this.L$Anon2 = ref$IntRef2;
            this.L$Anon3 = list;
            this.Z$Anon0 = z;
            this.L$Anon4 = notificationSettingsModel2;
            this.L$Anon5 = notificationSettingsModel;
            this.L$Anon6 = a3;
            this.L$Anon7 = sh42;
            this.L$Anon8 = list;
            this.label = 1;
            obj3 = a3.a(this);
            if (obj3 == a) {
                return a;
            }
            sh4 = a3;
            lh4 = lh42;
            list3 = list;
        } else if (i == 1) {
            list3 = (List) this.L$Anon8;
            notificationSettingsModel2 = (NotificationSettingsModel) this.L$Anon4;
            z = this.Z$Anon0;
            ref$IntRef2 = (Ref$IntRef) this.L$Anon2;
            ref$IntRef = (Ref$IntRef) this.L$Anon1;
            za4.a(obj);
            sh4 = (sh4) this.L$Anon6;
            lh4 = (lh4) this.L$Anon0;
            sh42 = (sh4) this.L$Anon7;
            obj3 = obj;
            List list4 = (List) this.L$Anon3;
            notificationSettingsModel = (NotificationSettingsModel) this.L$Anon5;
            list = list4;
        } else if (i == 2) {
            list2 = (List) this.L$Anon8;
            sh4 sh43 = (sh4) this.L$Anon7;
            sh4 sh44 = (sh4) this.L$Anon6;
            NotificationSettingsModel notificationSettingsModel3 = (NotificationSettingsModel) this.L$Anon5;
            NotificationSettingsModel notificationSettingsModel4 = (NotificationSettingsModel) this.L$Anon4;
            Ref$IntRef ref$IntRef3 = (Ref$IntRef) this.L$Anon2;
            Ref$IntRef ref$IntRef4 = (Ref$IntRef) this.L$Anon1;
            lh4 lh43 = (lh4) this.L$Anon0;
            za4.a(obj);
            list = (List) this.L$Anon3;
            obj2 = obj;
            list2.addAll((Collection) obj2);
            return list;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((Collection) obj3);
        this.L$Anon0 = lh4;
        this.L$Anon1 = ref$IntRef;
        this.L$Anon2 = ref$IntRef2;
        this.L$Anon3 = list;
        this.Z$Anon0 = z;
        this.L$Anon4 = notificationSettingsModel2;
        this.L$Anon5 = notificationSettingsModel;
        this.L$Anon6 = sh4;
        this.L$Anon7 = sh42;
        this.L$Anon8 = list;
        this.label = 2;
        obj2 = sh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list2 = list;
        list2.addAll((Collection) obj2);
        return list;
    }
}
