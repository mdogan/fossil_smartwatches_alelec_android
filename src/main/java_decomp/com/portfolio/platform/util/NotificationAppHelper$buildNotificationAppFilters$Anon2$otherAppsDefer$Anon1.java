package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.ms3;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wy2;
import com.fossil.blesdk.obfuscated.x52;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1", f = "NotificationAppHelper.kt", l = {145}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
    @DexIgnore
    public /* final */ /* synthetic */ List $notificationAllFilterList;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppHelper$buildNotificationAppFilters$Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(NotificationAppHelper$buildNotificationAppFilters$Anon2 notificationAppHelper$buildNotificationAppFilters$Anon2, List list, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = notificationAppHelper$buildNotificationAppFilters$Anon2;
        this.$notificationAllFilterList = list;
        this.$isAllAppToggleEnabled = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 = new NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(this.this$Anon0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, kc4);
        notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1.p$ = (lh4) obj;
        return notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ArrayList arrayList = new ArrayList();
            wy2 wy2 = this.this$Anon0.$getApps;
            this.L$Anon0 = lh4;
            this.L$Anon1 = arrayList;
            this.label = 1;
            obj = x52.a(wy2, null, this);
            if (obj == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (List) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
        if (cVar instanceof wy2.a) {
            this.$notificationAllFilterList.addAll(ms3.a(((wy2.a) cVar).a(), this.$isAllAppToggleEnabled));
        }
        return list;
    }
}
