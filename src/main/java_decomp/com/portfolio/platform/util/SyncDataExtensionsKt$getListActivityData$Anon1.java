package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncDataExtensionsKt$getListActivityData$Anon1 extends Lambda implements jd4<FitnessDataWrapper, Integer> {
    @DexIgnore
    public static /* final */ SyncDataExtensionsKt$getListActivityData$Anon1 INSTANCE; // = new SyncDataExtensionsKt$getListActivityData$Anon1();

    @DexIgnore
    public SyncDataExtensionsKt$getListActivityData$Anon1() {
        super(1);
    }

    @DexIgnore
    public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
        wd4.b(fitnessDataWrapper, "it");
        return fitnessDataWrapper.getTimezoneOffsetInSecond();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Integer.valueOf(invoke((FitnessDataWrapper) obj));
    }
}
