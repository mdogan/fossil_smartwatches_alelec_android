package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wa4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xe4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserUtils {
    @DexIgnore
    public static /* final */ wa4 b; // = ya4.a(UserUtils$Companion$INSTANCE$Anon2.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((rd4) null);
    @DexIgnore
    public fn2 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ xe4[] a;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(yd4.a(a.class), "INSTANCE", "getINSTANCE()Lcom/portfolio/platform/util/UserUtils;");
            yd4.a((PropertyReference1) propertyReference1Impl);
            a = new xe4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final UserUtils a() {
            wa4 b = UserUtils.b;
            a aVar = UserUtils.d;
            xe4 xe4 = a[0];
            return (UserUtils) b.getValue();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ UserUtils a; // = new UserUtils();
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public final UserUtils a() {
            return a;
        }
    }

    /*
    static {
        String simpleName = UserUtils.class.getSimpleName();
        wd4.a((Object) simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public UserUtils() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        fn2 fn2 = this.a;
        if (fn2 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - fn2.n());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", " + "successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                fn2 fn22 = this.a;
                if (fn22 == null) {
                    wd4.d("mSharePrefs");
                    throw null;
                } else if (fn22.e() >= i2) {
                    return true;
                }
            }
            return false;
        }
        wd4.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        fn2 fn2 = this.a;
        if (fn2 != null) {
            return fn2.K();
        }
        wd4.d("mSharePrefs");
        throw null;
    }
}
