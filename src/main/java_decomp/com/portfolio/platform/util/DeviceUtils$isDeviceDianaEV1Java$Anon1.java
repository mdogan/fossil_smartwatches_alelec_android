package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$Anon1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
public final class DeviceUtils$isDeviceDianaEV1Java$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceUtils this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceUtils$isDeviceDianaEV1Java$Anon1(DeviceUtils deviceUtils, String str, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deviceUtils;
        this.$deviceSerial = str;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeviceUtils$isDeviceDianaEV1Java$Anon1 deviceUtils$isDeviceDianaEV1Java$Anon1 = new DeviceUtils$isDeviceDianaEV1Java$Anon1(this.this$Anon0, this.$deviceSerial, kc4);
        deviceUtils$isDeviceDianaEV1Java$Anon1.p$ = (lh4) obj;
        return deviceUtils$isDeviceDianaEV1Java$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceUtils$isDeviceDianaEV1Java$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return pc4.a(this.this$Anon0.a(this.$deviceSerial));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
