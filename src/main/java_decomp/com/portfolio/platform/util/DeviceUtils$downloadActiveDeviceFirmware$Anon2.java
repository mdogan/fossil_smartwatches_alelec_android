package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.df4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.so2;
import com.fossil.blesdk.obfuscated.vn2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$Anon2", f = "DeviceUtils.kt", l = {97}, m = "invokeSuspend")
public final class DeviceUtils$downloadActiveDeviceFirmware$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ro2 $repoResponse;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceUtils this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements jd4<Firmware, sh4<? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ lh4 $this_withContext;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils$downloadActiveDeviceFirmware$Anon2 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @sc4(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$Anon2$Anon2$Anon1", f = "DeviceUtils.kt", l = {93}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super Boolean>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Firmware $it;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public lh4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, Firmware firmware, kc4 kc4) {
                super(2, kc4);
                this.this$Anon0 = anon2;
                this.$it = firmware;
            }

            @DexIgnore
            public final kc4<cb4> create(Object obj, kc4<?> kc4) {
                wd4.b(kc4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, this.$it, kc4);
                anon1.p$ = (lh4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = oc4.a();
                int i = this.label;
                if (i == 0) {
                    za4.a(obj);
                    lh4 lh4 = this.p$;
                    DeviceUtils deviceUtils = this.this$Anon0.this$Anon0.this$Anon0;
                    Firmware firmware = this.$it;
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    obj = deviceUtils.a(firmware, (kc4<? super Boolean>) this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    lh4 lh42 = (lh4) this.L$Anon0;
                    za4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(DeviceUtils$downloadActiveDeviceFirmware$Anon2 deviceUtils$downloadActiveDeviceFirmware$Anon2, lh4 lh4) {
            super(1);
            this.this$Anon0 = deviceUtils$downloadActiveDeviceFirmware$Anon2;
            this.$this_withContext = lh4;
        }

        @DexIgnore
        public final sh4<Boolean> invoke(Firmware firmware) {
            wd4.b(firmware, "it");
            return mg4.a(this.$this_withContext, (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, firmware, (kc4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceUtils$downloadActiveDeviceFirmware$Anon2(DeviceUtils deviceUtils, ro2 ro2, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = deviceUtils;
        this.$repoResponse = ro2;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DeviceUtils$downloadActiveDeviceFirmware$Anon2 deviceUtils$downloadActiveDeviceFirmware$Anon2 = new DeviceUtils$downloadActiveDeviceFirmware$Anon2(this.this$Anon0, this.$repoResponse, kc4);
        deviceUtils$downloadActiveDeviceFirmware$Anon2.p$ = (lh4) obj;
        return deviceUtils$downloadActiveDeviceFirmware$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceUtils$downloadActiveDeviceFirmware$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    public final Object invokeSuspend(Object obj) {
        DeviceUtils$downloadActiveDeviceFirmware$Anon2 deviceUtils$downloadActiveDeviceFirmware$Anon2;
        boolean z;
        lh4 lh4;
        List list;
        vn2 vn2;
        Ref$BooleanRef ref$BooleanRef;
        df4 df4;
        Iterator it;
        Ref$BooleanRef ref$BooleanRef2;
        Object obj2;
        DeviceUtils$downloadActiveDeviceFirmware$Anon2 deviceUtils$downloadActiveDeviceFirmware$Anon22;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh42 = this.p$;
            ApiResponse apiResponse = (ApiResponse) ((so2) this.$repoResponse).a();
            List<Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
            if (list2 != null) {
                vn2 e = en2.p.a().e();
                Ref$BooleanRef ref$BooleanRef3 = new Ref$BooleanRef();
                ref$BooleanRef3.element = true;
                for (Firmware a2 : list2) {
                    e.a(a2);
                }
                df4 c = SequencesKt___SequencesKt.c(wb4.b(list2), new Anon2(this, lh42));
                lh4 = lh42;
                list = list2;
                ref$BooleanRef2 = ref$BooleanRef3;
                df4 = c;
                deviceUtils$downloadActiveDeviceFirmware$Anon2 = this;
                vn2 = e;
                it = c.iterator();
            }
            return cb4.a;
        } else if (i == 1) {
            ref$BooleanRef2 = (Ref$BooleanRef) this.L$Anon8;
            sh4 sh4 = (sh4) this.L$Anon7;
            it = (Iterator) this.L$Anon5;
            df4 = (df4) this.L$Anon4;
            ref$BooleanRef = (Ref$BooleanRef) this.L$Anon3;
            vn2 = (vn2) this.L$Anon2;
            list = (List) this.L$Anon1;
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            obj2 = a;
            deviceUtils$downloadActiveDeviceFirmware$Anon22 = this;
            if (!((Boolean) obj).booleanValue()) {
                deviceUtils$downloadActiveDeviceFirmware$Anon2 = deviceUtils$downloadActiveDeviceFirmware$Anon22;
                a = obj2;
                z = true;
                ref$BooleanRef2.element = z;
                ref$BooleanRef2 = ref$BooleanRef;
            }
            deviceUtils$downloadActiveDeviceFirmware$Anon2 = deviceUtils$downloadActiveDeviceFirmware$Anon22;
            a = obj2;
            z = false;
            ref$BooleanRef2.element = z;
            ref$BooleanRef2 = ref$BooleanRef;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!it.hasNext()) {
            Object next = it.next();
            sh4 sh42 = (sh4) next;
            if (ref$BooleanRef2.element) {
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon0 = lh4;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon1 = list;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon2 = vn2;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon3 = ref$BooleanRef2;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon4 = df4;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon5 = it;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon6 = next;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon7 = sh42;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.L$Anon8 = ref$BooleanRef2;
                deviceUtils$downloadActiveDeviceFirmware$Anon2.label = 1;
                Object a3 = sh42.a(deviceUtils$downloadActiveDeviceFirmware$Anon2);
                if (a3 == a) {
                    return a;
                }
                obj2 = a;
                deviceUtils$downloadActiveDeviceFirmware$Anon22 = deviceUtils$downloadActiveDeviceFirmware$Anon2;
                obj = a3;
                ref$BooleanRef = ref$BooleanRef2;
                if (!((Boolean) obj).booleanValue()) {
                    deviceUtils$downloadActiveDeviceFirmware$Anon2 = deviceUtils$downloadActiveDeviceFirmware$Anon22;
                    a = obj2;
                    z = false;
                    ref$BooleanRef2.element = z;
                    ref$BooleanRef2 = ref$BooleanRef;
                    if (!it.hasNext()) {
                    }
                }
                deviceUtils$downloadActiveDeviceFirmware$Anon2 = deviceUtils$downloadActiveDeviceFirmware$Anon22;
                a = obj2;
                z = true;
                ref$BooleanRef2.element = z;
                ref$BooleanRef2 = ref$BooleanRef;
                if (!it.hasNext()) {
                }
                return a;
            }
            ref$BooleanRef = ref$BooleanRef2;
            z = false;
            ref$BooleanRef2.element = z;
            ref$BooleanRef2 = ref$BooleanRef;
            if (!it.hasNext()) {
            }
        }
        if (!ref$BooleanRef2.element) {
            FLogger.INSTANCE.getLocal().e(DeviceUtils.f, "downloadActiveDeviceFirmware - download detail fw FAILED in one or more parts. Retry later.");
        }
        return cb4.a;
    }
}
