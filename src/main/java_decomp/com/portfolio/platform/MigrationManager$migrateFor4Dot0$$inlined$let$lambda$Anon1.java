package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sk2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacyGoalTrackingSettings $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1(LegacyGoalTrackingSettings legacyGoalTrackingSettings, kc4 kc4, MigrationManager migrationManager) {
        super(2, kc4);
        this.$it = legacyGoalTrackingSettings;
        this.this$Anon0 = migrationManager;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 = new MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1(this.$it, kc4, this.this$Anon0);
        migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1.p$ = (lh4) obj;
        return migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MigrationManager.n.a();
            local.d(a2, "Migrate goal tracking with target " + this.$it.getTarget() + " value " + this.$it.getValue());
            this.this$Anon0.g.getGoalTrackingDao().upsertGoalSettings(new GoalSetting(this.$it.getTarget()));
            ArrayList arrayList = new ArrayList();
            int value = this.$it.getValue();
            for (int i2 = 0; i2 < value; i2++) {
                String uuid = UUID.randomUUID().toString();
                wd4.a((Object) uuid, "UUID.randomUUID().toString()");
                Date date = new Date();
                TimeZone timeZone = TimeZone.getDefault();
                wd4.a((Object) timeZone, "TimeZone.getDefault()");
                DateTime a3 = sk2.a(date, timeZone.getRawOffset() / 1000);
                wd4.a((Object) a3, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                wd4.a((Object) timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = r7;
                GoalTrackingData goalTrackingData2 = new GoalTrackingData(uuid, a3, timeZone2.getRawOffset() / 1000, new Date(), new Date().getTime(), new Date().getTime());
                arrayList.add(goalTrackingData);
            }
            GoalTrackingRepository d = this.this$Anon0.f;
            List d2 = wb4.d(arrayList);
            this.L$Anon0 = lh4;
            this.L$Anon1 = arrayList;
            this.label = 1;
            if (d.insertFromDevice(d2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            ArrayList arrayList2 = (ArrayList) this.L$Anon1;
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
