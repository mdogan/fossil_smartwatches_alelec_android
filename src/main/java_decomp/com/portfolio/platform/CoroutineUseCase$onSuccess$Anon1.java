package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$Anon1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
public final class CoroutineUseCase$onSuccess$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.d $response;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$onSuccess$Anon1(CoroutineUseCase coroutineUseCase, CoroutineUseCase.d dVar, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = coroutineUseCase;
        this.$response = dVar;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        CoroutineUseCase$onSuccess$Anon1 coroutineUseCase$onSuccess$Anon1 = new CoroutineUseCase$onSuccess$Anon1(this.this$Anon0, this.$response, kc4);
        coroutineUseCase$onSuccess$Anon1.p$ = (lh4) obj;
        return coroutineUseCase$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CoroutineUseCase$onSuccess$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            FLogger.INSTANCE.getLocal().d(this.this$Anon0.b, "Trigger UseCase callback success");
            CoroutineUseCase.e a = this.this$Anon0.a;
            if (a != null) {
                a.onSuccess(this.$response);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
