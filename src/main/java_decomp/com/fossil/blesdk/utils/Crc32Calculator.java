package com.fossil.blesdk.utils;

import com.fossil.blesdk.obfuscated.fb0;
import com.fossil.blesdk.obfuscated.gb0;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Crc32Calculator {
    @DexIgnore
    public static /* final */ Crc32Calculator a; // = new Crc32Calculator();

    @DexIgnore
    public enum CrcType {
        CRC32,
        CRC32C
    }

    @DexIgnore
    public final long a(byte[] bArr, CrcType crcType) {
        wd4.b(bArr, "data");
        wd4.b(crcType, "crcType");
        Checksum a2 = a(crcType);
        a2.update(bArr, 0, bArr.length);
        return a2.getValue();
    }

    @DexIgnore
    public final long a(byte[] bArr, int i, int i2, CrcType crcType) {
        wd4.b(bArr, "data");
        wd4.b(crcType, "crcType");
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            return 0;
        }
        Checksum a2 = a(crcType);
        a2.update(bArr, i, i2);
        return a2.getValue();
    }

    @DexIgnore
    public final Checksum a(CrcType crcType) {
        int i = gb0.a[crcType.ordinal()];
        if (i == 1) {
            return new CRC32();
        }
        if (i == 2) {
            return new fb0();
        }
        throw new NoWhenBranchMatchedException();
    }
}
