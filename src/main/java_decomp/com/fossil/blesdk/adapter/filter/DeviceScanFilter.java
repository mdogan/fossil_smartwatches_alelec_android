package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceScanFilter extends AbstractScanFilter {
    @DexIgnore
    public /* final */ HashMap<String, AbstractScanFilter> deviceFilters; // = new HashMap<>();

    @DexIgnore
    public DeviceImplementation[] filter$blesdk_productionRelease(DeviceImplementation[] deviceImplementationArr) {
        wd4.b(deviceImplementationArr, Constants.DEVICES);
        Object[] copyOf = Arrays.copyOf(deviceImplementationArr, deviceImplementationArr.length);
        wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        DeviceImplementation[] deviceImplementationArr2 = (DeviceImplementation[]) copyOf;
        for (AbstractScanFilter filter$blesdk_productionRelease : this.deviceFilters.values()) {
            deviceImplementationArr2 = filter$blesdk_productionRelease.filter$blesdk_productionRelease(deviceImplementationArr2);
        }
        return deviceImplementationArr2;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        wd4.b(deviceImplementation, "device");
        for (AbstractScanFilter matches$blesdk_productionRelease : this.deviceFilters.values()) {
            if (!matches$blesdk_productionRelease.matches$blesdk_productionRelease(deviceImplementation)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final DeviceScanFilter setDeviceTypes(DeviceType[] deviceTypeArr) {
        wd4.b(deviceTypeArr, "deviceTypes");
        HashMap<String, AbstractScanFilter> hashMap = this.deviceFilters;
        String simpleName = DeviceTypesScanFilter.class.getSimpleName();
        wd4.a((Object) simpleName, "DeviceTypesScanFilter::class.java.simpleName");
        hashMap.put(simpleName, new DeviceTypesScanFilter(deviceTypeArr));
        return this;
    }

    @DexIgnore
    public final DeviceScanFilter setSerialNumberPattern(String str) {
        wd4.b(str, "serialNumberRegex");
        HashMap<String, AbstractScanFilter> hashMap = this.deviceFilters;
        String simpleName = SerialNumberPatternScanFilter.class.getSimpleName();
        wd4.a((Object) simpleName, "SerialNumberPatternScanF\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new SerialNumberPatternScanFilter(str));
        return this;
    }

    @DexIgnore
    public final DeviceScanFilter setSerialNumberPrefixes(String[] strArr) {
        wd4.b(strArr, "serialNumberPrefixes");
        HashMap<String, AbstractScanFilter> hashMap = this.deviceFilters;
        String simpleName = SerialNumberPrefixesScanFilter.class.getSimpleName();
        wd4.a((Object) simpleName, "SerialNumberPrefixesScan\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new SerialNumberPrefixesScanFilter(strArr));
        return this;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.DEVICE_FILTER;
        Collection<AbstractScanFilter> values = this.deviceFilters.values();
        wd4.a((Object) values, "deviceFilters.values");
        Object[] array = values.toArray(new AbstractScanFilter[0]);
        if (array != null) {
            return xa0.a(jSONObject, jSONKey, k00.a((JSONAbleObject[]) array));
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
