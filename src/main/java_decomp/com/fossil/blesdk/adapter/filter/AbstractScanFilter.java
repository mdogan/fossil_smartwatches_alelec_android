package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class AbstractScanFilter extends JSONAbleObject {
    @DexIgnore
    public DeviceImplementation[] filter$blesdk_productionRelease(DeviceImplementation[] deviceImplementationArr) {
        wd4.b(deviceImplementationArr, Constants.DEVICES);
        ArrayList arrayList = new ArrayList();
        for (DeviceImplementation deviceImplementation : deviceImplementationArr) {
            if (matches$blesdk_productionRelease(deviceImplementation)) {
                arrayList.add(deviceImplementation);
            }
        }
        Object[] array = arrayList.toArray(new DeviceImplementation[0]);
        if (array != null) {
            return (DeviceImplementation[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public abstract boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation);
}
