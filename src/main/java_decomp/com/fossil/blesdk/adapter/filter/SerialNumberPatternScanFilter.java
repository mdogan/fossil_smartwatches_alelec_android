package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.regex.Pattern;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SerialNumberPatternScanFilter extends AbstractScanFilter {
    @DexIgnore
    public Pattern serialNumberPattern;

    @DexIgnore
    public SerialNumberPatternScanFilter(String str) throws IllegalArgumentException {
        wd4.b(str, "serialNumberRegex");
        Pattern compile = Pattern.compile(str);
        wd4.a((Object) compile, "Pattern.compile(serialNumberRegex)");
        this.serialNumberPattern = compile;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        wd4.b(deviceImplementation, "device");
        return this.serialNumberPattern.matcher(deviceImplementation.getDeviceInformation().getSerialNumber()).matches();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.FILTER_TYPE, "serial_number_pattern"), JSONKey.SERIAL_NUMBER_REGEX, this.serialNumberPattern.toString());
    }
}
