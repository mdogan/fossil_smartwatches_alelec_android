package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SerialNumberPrefixesScanFilter extends AbstractScanFilter {
    @DexIgnore
    public /* final */ String[] serialNumberPrefixes;

    @DexIgnore
    public SerialNumberPrefixesScanFilter(String[] strArr) {
        wd4.b(strArr, "serialNumberPrefixes");
        this.serialNumberPrefixes = strArr;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        wd4.b(deviceImplementation, "device");
        if (this.serialNumberPrefixes.length == 0) {
            return true;
        }
        String serialNumber = deviceImplementation.getDeviceInformation().getSerialNumber();
        for (String c : this.serialNumberPrefixes) {
            if (cg4.c(serialNumber, c, false, 2, (Object) null)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.FILTER_TYPE, "serial_number_prefixes"), JSONKey.SERIAL_NUMBER_PREFIXES, j90.a(this.serialNumberPrefixes));
    }
}
