package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.obfuscated.u00;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceTypesScanFilter extends AbstractScanFilter {
    @DexIgnore
    public /* final */ DeviceType[] deviceTypes;

    @DexIgnore
    public DeviceTypesScanFilter(DeviceType[] deviceTypeArr) {
        wd4.b(deviceTypeArr, "deviceTypes");
        this.deviceTypes = deviceTypeArr;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        wd4.b(deviceImplementation, "device");
        if (this.deviceTypes.length == 0) {
            return true;
        }
        for (DeviceType deviceType : this.deviceTypes) {
            if (deviceType == deviceImplementation.getDeviceInformation().getDeviceType()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.FILTER_TYPE, "device_type"), JSONKey.DEVICE_TYPES, u00.a(this.deviceTypes));
    }
}
