package com.fossil.blesdk.adapter;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon4 extends Lambda implements jd4<Phase.Result, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice $bluetoothDevice;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon4(BluetoothDevice bluetoothDevice) {
        super(1);
        this.$bluetoothDevice = bluetoothDevice;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase.Result) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase.Result result) {
        wd4.b(result, "it");
        BluetoothLeAdapter.d.remove(this.$bluetoothDevice.getAddress());
    }
}
