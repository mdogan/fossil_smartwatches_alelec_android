package com.fossil.blesdk.setting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SharedPreferenceFileName {
    MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE,
    DEBUG_LOG_PREFERENCE,
    SDK_LOG_PREFERENCE,
    MINUTE_DATA_REFERENCE,
    HARDWARE_LOG_REFERENCE,
    TEXT_ENCRYPTION_PREFERENCE
}
