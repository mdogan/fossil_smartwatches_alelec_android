package com.fossil.blesdk.database.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.n00;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceFile extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ long createdTimeStamp;
    @DexIgnore
    public /* final */ String deviceMacAddress;
    @DexIgnore
    public /* final */ long fileCrc;
    @DexIgnore
    public /* final */ byte fileIndex;
    @DexIgnore
    public /* final */ long fileLength;
    @DexIgnore
    public /* final */ byte fileType;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ boolean isCompleted;
    @DexIgnore
    public /* final */ byte[] rawData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DeviceFile> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final byte a(short s) {
            return ByteBuffer.allocate(2).putShort(s).get(1);
        }

        @DexIgnore
        public final byte b(short s) {
            return ByteBuffer.allocate(2).putShort(s).get(0);
        }

        @DexIgnore
        public DeviceFile createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new DeviceFile(parcel, (rd4) null);
        }

        @DexIgnore
        public DeviceFile[] newArray(int i) {
            return new DeviceFile[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DeviceFile(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ DeviceFile copy$default(DeviceFile deviceFile, String str, byte b, byte b2, byte[] bArr, long j, long j2, long j3, boolean z, int i, Object obj) {
        DeviceFile deviceFile2 = deviceFile;
        int i2 = i;
        return deviceFile.copy((i2 & 1) != 0 ? deviceFile2.deviceMacAddress : str, (i2 & 2) != 0 ? deviceFile2.fileType : b, (i2 & 4) != 0 ? deviceFile2.fileIndex : b2, (i2 & 8) != 0 ? deviceFile2.rawData : bArr, (i2 & 16) != 0 ? deviceFile2.fileLength : j, (i2 & 32) != 0 ? deviceFile2.fileCrc : j2, (i2 & 64) != 0 ? deviceFile2.createdTimeStamp : j3, (i2 & 128) != 0 ? deviceFile2.isCompleted : z);
    }

    @DexIgnore
    public static /* synthetic */ JSONObject toJSONObject$default(DeviceFile deviceFile, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = true;
        }
        return deviceFile.toJSONObject(z);
    }

    @DexIgnore
    public final String component1() {
        return this.deviceMacAddress;
    }

    @DexIgnore
    public final byte component2() {
        return this.fileType;
    }

    @DexIgnore
    public final byte component3() {
        return this.fileIndex;
    }

    @DexIgnore
    public final byte[] component4() {
        return this.rawData;
    }

    @DexIgnore
    public final long component5() {
        return this.fileLength;
    }

    @DexIgnore
    public final long component6() {
        return this.fileCrc;
    }

    @DexIgnore
    public final long component7() {
        return this.createdTimeStamp;
    }

    @DexIgnore
    public final boolean component8() {
        return this.isCompleted;
    }

    @DexIgnore
    public final DeviceFile copy(String str, byte b, byte b2, byte[] bArr, long j, long j2, long j3, boolean z) {
        wd4.b(str, "deviceMacAddress");
        byte[] bArr2 = bArr;
        wd4.b(bArr2, "rawData");
        return new DeviceFile(str, b, b2, bArr2, j, j2, j3, z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) DeviceFile.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DeviceFile deviceFile = (DeviceFile) obj;
            return !(wd4.a((Object) this.deviceMacAddress, (Object) deviceFile.deviceMacAddress) ^ true) && this.fileType == deviceFile.fileType && this.fileIndex == deviceFile.fileIndex && Arrays.equals(this.rawData, deviceFile.rawData) && this.fileLength == deviceFile.fileLength && this.fileCrc == deviceFile.fileCrc && this.createdTimeStamp == deviceFile.createdTimeStamp && this.isCompleted == deviceFile.isCompleted;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.database.entity.DeviceFile");
    }

    @DexIgnore
    public final long getCreatedTimeStamp() {
        return this.createdTimeStamp;
    }

    @DexIgnore
    public final String getDeviceMacAddress() {
        return this.deviceMacAddress;
    }

    @DexIgnore
    public final long getFileCrc() {
        return this.fileCrc;
    }

    @DexIgnore
    public final short getFileHandle$blesdk_productionRelease() {
        return ByteBuffer.allocate(2).put(this.fileType).put(this.fileIndex).getShort(0);
    }

    @DexIgnore
    public final byte getFileIndex() {
        return this.fileIndex;
    }

    @DexIgnore
    public final long getFileLength() {
        return this.fileLength;
    }

    @DexIgnore
    public final byte getFileType() {
        return this.fileType;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final byte[] getRawData() {
        return this.rawData;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((((this.deviceMacAddress.hashCode() * 31) + this.fileType) * 31) + this.fileIndex) * 31) + Arrays.hashCode(this.rawData)) * 31) + Long.valueOf(this.fileLength).hashCode()) * 31) + Long.valueOf(this.fileCrc).hashCode()) * 31) + Long.valueOf(this.createdTimeStamp).hashCode()) * 31) + Boolean.valueOf(this.isCompleted).hashCode();
    }

    @DexIgnore
    public final boolean isCompleted() {
        return this.isCompleted;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return toJSONObject(true);
    }

    @DexIgnore
    public String toString() {
        return "DeviceFile(deviceMacAddress=" + this.deviceMacAddress + ", fileType=" + this.fileType + ", fileIndex=" + this.fileIndex + ", rawData=" + Arrays.toString(this.rawData) + ", fileLength=" + this.fileLength + ", fileCrc=" + this.fileCrc + ", createdTimeStamp=" + this.createdTimeStamp + ", isCompleted=" + this.isCompleted + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.deviceMacAddress);
        }
        if (parcel != null) {
            parcel.writeByte(this.fileType);
        }
        if (parcel != null) {
            parcel.writeByte(this.fileIndex);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.rawData);
        }
        if (parcel != null) {
            parcel.writeLong(this.fileLength);
        }
        if (parcel != null) {
            parcel.writeLong(this.fileCrc);
        }
        if (parcel != null) {
            parcel.writeLong(this.createdTimeStamp);
        }
        if (parcel != null) {
            parcel.writeInt(this.isCompleted ? 1 : 0);
        }
    }

    @DexIgnore
    public DeviceFile(String str, byte b, byte b2, byte[] bArr, long j, long j2, long j3, boolean z) {
        wd4.b(str, "deviceMacAddress");
        wd4.b(bArr, "rawData");
        this.deviceMacAddress = str;
        this.fileType = b;
        this.fileIndex = b2;
        this.rawData = bArr;
        this.fileLength = j;
        this.fileCrc = j2;
        this.createdTimeStamp = j3;
        this.isCompleted = z;
    }

    @DexIgnore
    public final JSONObject toJSONObject(boolean z) {
        JSONObject a2 = xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.MAC_ADDRESS, this.deviceMacAddress), JSONKey.FILE_HANDLE, o90.a(getFileHandle$blesdk_productionRelease())), JSONKey.FILE_SIZE, Long.valueOf(this.fileLength)), JSONKey.FILE_CRC, Long.valueOf(this.fileCrc)), JSONKey.RAW_DATA_LENGTH, Integer.valueOf(this.rawData.length)), JSONKey.CREATED_AT, Double.valueOf(o90.a(this.createdTimeStamp))), JSONKey.IS_COMPLETED, Boolean.valueOf(this.isCompleted)), JSONKey.FILE_HANDLE_DESCRIPTION, FileHandle.Companion.a(getFileHandle$blesdk_productionRelease()));
        if (z) {
            xa0.a(a2, JSONKey.RAW_DATA, l90.a(this.rawData, (String) null, 1, (Object) null));
        }
        FileType a3 = FileType.Companion.a(this.fileType);
        if (a3 != null) {
            int i = n00.a[a3.ordinal()];
            if (i == 1) {
                xa0.a(a2, JSONKey.ABSOLUTE_FILE_NUMBER, -1);
                byte[] bArr = this.rawData;
                if (bArr.length >= 18) {
                    ByteBuffer order = ByteBuffer.wrap(kb4.a(bArr, 16, 18)).order(ByteOrder.LITTLE_ENDIAN);
                    wd4.a((Object) order, "byteBuffer");
                    xa0.a(a2, JSONKey.ABSOLUTE_FILE_NUMBER, Integer.valueOf(o90.b(order.getShort())));
                }
            } else if (i == 2) {
                xa0.a(a2, JSONKey.FILE_VERSION, "unknown");
                xa0.a(a2, JSONKey.ABSOLUTE_FILE_NUMBER, -1);
                int i2 = 0;
                while (true) {
                    int i3 = i2 + 16;
                    byte[] bArr2 = this.rawData;
                    if (i3 > bArr2.length) {
                        break;
                    }
                    byte[] a4 = kb4.a(bArr2, i2, i3);
                    byte b = a4[0];
                    if (b != -59) {
                        if (b == 0) {
                            JSONKey jSONKey = JSONKey.FILE_VERSION;
                            StringBuilder sb = new StringBuilder();
                            sb.append(o90.b(a4[1]));
                            sb.append('.');
                            sb.append(o90.b(a4[2]));
                            xa0.a(a2, jSONKey, sb.toString());
                        }
                    } else if (a4[1] == 9) {
                        ByteBuffer order2 = ByteBuffer.wrap(kb4.a(kb4.a(a4, 2, 5), (byte) 0)).order(ByteOrder.LITTLE_ENDIAN);
                        JSONKey jSONKey2 = JSONKey.ABSOLUTE_FILE_NUMBER;
                        wd4.a((Object) order2, "byteBuffer");
                        xa0.a(a2, jSONKey2, Integer.valueOf(order2.getInt()));
                    }
                    i2 = i3;
                }
            }
        }
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DeviceFile(String str, short s, long j, long j2) {
        this(str, CREATOR.b(r0), CREATOR.a(r0), new byte[0], j, j2, 0, false);
        short s2 = s;
        wd4.b(str, "deviceMacAddress");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DeviceFile(String str, short s, byte[] bArr, long j, long j2) {
        this(str, CREATOR.b(r0), CREATOR.a(r0), r6, j, j2, 0, false);
        short s2 = s;
        wd4.b(str, "deviceMacAddress");
        byte[] bArr2 = bArr;
        wd4.b(bArr2, "rawData");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DeviceFile(Parcel parcel) {
        this(r2, r3, r4, r0 == null ? new byte[0] : r0, parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readInt() != 0);
        String readString = parcel.readString();
        String str = readString == null ? "" : readString;
        byte readByte = parcel.readByte();
        byte readByte2 = parcel.readByte();
        byte[] createByteArray = parcel.createByteArray();
    }
}
