package com.fossil.blesdk.database;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.l00;
import com.fossil.blesdk.obfuscated.m00;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SdkDatabase_Impl extends SdkDatabase {
    @DexIgnore
    public volatile l00 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends uf.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `DeviceFile` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `deviceMacAddress` TEXT NOT NULL, `fileType` INTEGER NOT NULL, `fileIndex` INTEGER NOT NULL, `rawData` BLOB NOT NULL, `fileLength` INTEGER NOT NULL, `fileCrc` INTEGER NOT NULL, `createdTimeStamp` INTEGER NOT NULL, `isCompleted` INTEGER NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f0501e661379ccab31d0b3858cee8e83')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `DeviceFile`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SdkDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = SdkDatabase_Impl.this.mDatabase = hgVar;
            SdkDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SdkDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("id", new fg.a("id", "INTEGER", true, 1));
            hashMap.put("deviceMacAddress", new fg.a("deviceMacAddress", "TEXT", true, 0));
            hashMap.put("fileType", new fg.a("fileType", "INTEGER", true, 0));
            hashMap.put("fileIndex", new fg.a("fileIndex", "INTEGER", true, 0));
            hashMap.put("rawData", new fg.a("rawData", "BLOB", true, 0));
            hashMap.put("fileLength", new fg.a("fileLength", "INTEGER", true, 0));
            hashMap.put("fileCrc", new fg.a("fileCrc", "INTEGER", true, 0));
            hashMap.put("createdTimeStamp", new fg.a("createdTimeStamp", "INTEGER", true, 0));
            hashMap.put("isCompleted", new fg.a("isCompleted", "INTEGER", true, 0));
            fg fgVar = new fg("DeviceFile", hashMap, new HashSet(0), new HashSet(0));
            fg a2 = fg.a(hgVar, "DeviceFile");
            if (!fgVar.equals(a2)) {
                throw new IllegalStateException("Migration didn't properly handle DeviceFile(com.fossil.blesdk.database.entity.DeviceFile).\n Expected:\n" + fgVar + "\n Found:\n" + a2);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a2 = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a2.b("DELETE FROM `DeviceFile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a2.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a2.x()) {
                a2.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "DeviceFile");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new a(3), "f0501e661379ccab31d0b3858cee8e83", "05778a001d8750bf160c2cf361f4e16f");
        ig.b.a a2 = ig.b.a(kfVar.b);
        a2.a(kfVar.c);
        a2.a((ig.a) ufVar);
        return kfVar.a.a(a2.a());
    }

    @DexIgnore
    public l00 a() {
        l00 l00;
        if (this.f != null) {
            return this.f;
        }
        synchronized (this) {
            if (this.f == null) {
                this.f = new m00(this);
            }
            l00 = this.f;
        }
        return l00;
    }
}
