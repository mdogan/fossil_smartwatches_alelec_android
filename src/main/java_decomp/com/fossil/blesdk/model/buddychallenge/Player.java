package com.fossil.blesdk.model.buddychallenge;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Player extends JSONAbleObject implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ int rank;
    @DexIgnore
    public /* final */ int step;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<Player> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public Player createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new Player(parcel);
        }

        @DexIgnore
        public Player[] newArray(int i) {
            return new Player[i];
        }
    }

    @DexIgnore
    public Player(String str, int i, int i2) {
        wd4.b(str, "name");
        this.name = str;
        this.step = i;
        this.rank = i2;
    }

    @DexIgnore
    public static /* synthetic */ Player copy$default(Player player, String str, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = player.name;
        }
        if ((i3 & 2) != 0) {
            i = player.step;
        }
        if ((i3 & 4) != 0) {
            i2 = player.rank;
        }
        return player.copy(str, i, i2);
    }

    @DexIgnore
    public final String component1() {
        return this.name;
    }

    @DexIgnore
    public final int component2() {
        return this.step;
    }

    @DexIgnore
    public final int component3() {
        return this.rank;
    }

    @DexIgnore
    public final Player copy(String str, int i, int i2) {
        wd4.b(str, "name");
        return new Player(str, i, i2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Player) {
                Player player = (Player) obj;
                if (wd4.a((Object) this.name, (Object) player.name)) {
                    if (this.step == player.step) {
                        if (this.rank == player.rank) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getRank() {
        return this.rank;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.name;
        return ((((str != null ? str.hashCode() : 0) * 31) + this.step) * 31) + this.rank;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject a2 = xa0.a(new JSONObject(), JSONKey.NAME, this.name);
        if (cg4.a(this.name)) {
            xa0.a(xa0.a(a2, JSONKey.STEP, ""), JSONKey.RANK, "");
        } else {
            xa0.a(xa0.a(a2, JSONKey.STEP, Integer.valueOf(this.step)), JSONKey.RANK, Integer.valueOf(this.rank));
        }
        return a2;
    }

    @DexIgnore
    public String toString() {
        return "Player(name=" + this.name + ", step=" + this.step + ", rank=" + this.rank + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.name);
        parcel.writeInt(this.step);
        parcel.writeInt(this.rank);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Player(Parcel parcel) {
        this(r0, parcel.readInt(), parcel.readInt());
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
        } else {
            wd4.a();
            throw null;
        }
    }
}
