package com.fossil.blesdk.model.background.config.position;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundPositionConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_ANGLE; // = 359;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_DISTANCE_FROM_CENTER; // = 120;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_ANGLE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_DISTANCE_FROM_CENTER; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_Z_INDEX; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int RECOMMENDED_DISTANCE_FROM_CENTER; // = 62;
    @DexIgnore
    public /* final */ int angle;
    @DexIgnore
    public /* final */ int distanceFromCenter;
    @DexIgnore
    public /* final */ int zIndex;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BackgroundPositionConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public BackgroundPositionConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new BackgroundPositionConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public BackgroundPositionConfig[] newArray(int i) {
            return new BackgroundPositionConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundPositionConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void angle$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void distanceFromCenter$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void zIndex$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) BackgroundPositionConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BackgroundPositionConfig backgroundPositionConfig = (BackgroundPositionConfig) obj;
            return this.angle == backgroundPositionConfig.angle && this.distanceFromCenter == backgroundPositionConfig.distanceFromCenter && this.zIndex == backgroundPositionConfig.zIndex;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.angle;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.distanceFromCenter;
    }

    @DexIgnore
    public final int getZIndex() {
        return this.zIndex;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.angle * 31) + this.distanceFromCenter) * 31) + this.zIndex;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        int i = this.angle;
        boolean z = true;
        if (i >= 0 && 359 >= i) {
            int i2 = this.distanceFromCenter;
            if (i2 >= 0 && 120 >= i2) {
                if (this.zIndex < 0) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException("z index(" + this.zIndex + ") must be larger than " + "[0].");
                }
                return;
            }
            throw new IllegalArgumentException("distanceFromCenter(" + this.distanceFromCenter + ") is out of " + "range [0, 120].");
        }
        throw new IllegalArgumentException("angle(" + this.angle + ") is out of range " + "[0, 359].");
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("angle", this.angle).put("distance", this.distanceFromCenter).put("z_index", this.zIndex);
        wd4.a((Object) put, "JSONObject()\n           \u2026Constant.Z_INDEX, zIndex)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.angle);
        }
        if (parcel != null) {
            parcel.writeInt(this.distanceFromCenter);
        }
        if (parcel != null) {
            parcel.writeInt(this.zIndex);
        }
    }

    @DexIgnore
    public BackgroundPositionConfig(int i, int i2, int i3) throws IllegalArgumentException {
        this.angle = i;
        this.distanceFromCenter = i2;
        this.zIndex = i3;
        i();
    }

    @DexIgnore
    public BackgroundPositionConfig(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
    }
}
