package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.event.request.CommuteTimeETAMicroAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.MicroAppRequest;
import com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.w20;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeETAMicroAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ int hour;
    @DexIgnore
    public /* final */ Version microAppVersion;
    @DexIgnore
    public /* final */ int minute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeETAMicroAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeETAMicroAppData createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeETAMicroAppData(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeETAMicroAppData[] newArray(int i) {
            return new CommuteTimeETAMicroAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeETAMicroAppData(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void microAppVersion$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) CommuteTimeETAMicroAppData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            CommuteTimeETAMicroAppData commuteTimeETAMicroAppData = (CommuteTimeETAMicroAppData) obj;
            return !(wd4.a((Object) this.microAppVersion, (Object) commuteTimeETAMicroAppData.microAppVersion) ^ true) && this.hour == commuteTimeETAMicroAppData.hour && this.minute == commuteTimeETAMicroAppData.minute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData");
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final Version getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        wd4.b(version, "version");
        try {
            w20 w20 = w20.c;
            DeviceRequest deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return w20.a(s, version, new MicroAppCommuteTimeResponse(((MicroAppRequest) deviceRequest).getMicroAppEvent$blesdk_productionRelease(), new Version(this.microAppVersion.getMajor(), this.microAppVersion.getMinor()), this.hour, this.minute).getData());
            }
            throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (FileFormatException e) {
            ea0.l.a(e);
            return new byte[0];
        }
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return xa0.a(xa0.a(xa0.a(super.getResponseJSONLog(), JSONKey.MICRO_APP_VERSION, this.microAppVersion.toString()), JSONKey.HOUR, Integer.valueOf(this.hour)), JSONKey.MINUTE, Integer.valueOf(this.minute));
    }

    @DexIgnore
    public int hashCode() {
        return (((((super.hashCode() * 31) + this.microAppVersion.hashCode()) * 31) + Integer.valueOf(this.hour).hashCode()) * 31) + Integer.valueOf(this.minute).hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppVersion, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.hour);
        }
        if (parcel != null) {
            parcel.writeInt(this.minute);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppData(CommuteTimeETAMicroAppRequest commuteTimeETAMicroAppRequest, Version version, int i, int i2) {
        super(commuteTimeETAMicroAppRequest, (String) null);
        wd4.b(commuteTimeETAMicroAppRequest, "commuteTimeETAMicroAppRequest");
        wd4.b(version, "microAppVersion");
        this.hour = i;
        this.minute = i2;
        this.microAppVersion = version;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeETAMicroAppData(Parcel parcel) {
        this(r0, (Version) r2, parcel.readInt(), parcel.readInt());
        Parcelable readParcelable = parcel.readParcelable(CommuteTimeETAMicroAppRequest.class.getClassLoader());
        if (readParcelable != null) {
            CommuteTimeETAMicroAppRequest commuteTimeETAMicroAppRequest = (CommuteTimeETAMicroAppRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
