package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.RingPhoneRequest;
import com.fossil.blesdk.model.enumerate.RingPhoneState;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.ha0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingPhoneData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ RingPhoneState state;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<RingPhoneData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public RingPhoneData createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new RingPhoneData(parcel, (rd4) null);
        }

        @DexIgnore
        public RingPhoneData[] newArray(int i) {
            return new RingPhoneData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ RingPhoneData(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) RingPhoneData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            return this.state == ((RingPhoneData) obj).state;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingPhoneData");
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        wd4.b(version, "version");
        ha0 ha0 = ha0.a;
        DeviceRequest deviceRequest = getDeviceRequest();
        return ha0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return xa0.a(super.getResponseJSONLog(), JSONKey.STATE, this.state.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public final RingPhoneState getState() {
        return this.state;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.state.hashCode();
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RING_MY_PHONE, new JSONObject().put(Constants.RESULT, this.state.getLogName$blesdk_productionRelease()));
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.state.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneData(RingPhoneRequest ringPhoneRequest, RingPhoneState ringPhoneState) {
        super(ringPhoneRequest, (String) null);
        wd4.b(ringPhoneRequest, "ringPhoneRequest");
        wd4.b(ringPhoneState, "state");
        this.state = ringPhoneState;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneData(RingPhoneState ringPhoneState) {
        super((DeviceRequest) null, (String) null);
        wd4.b(ringPhoneState, "state");
        this.state = ringPhoneState;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public RingPhoneData(Parcel parcel) {
        super((DeviceRequest) r0, parcel.readString());
        Parcelable readParcelable = parcel.readParcelable(RingPhoneRequest.class.getClassLoader());
        if (readParcelable != null) {
            this.state = RingPhoneState.values()[parcel.readInt()];
            return;
        }
        wd4.a();
        throw null;
    }
}
