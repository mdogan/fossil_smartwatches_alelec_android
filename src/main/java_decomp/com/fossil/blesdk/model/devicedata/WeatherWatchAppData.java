package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.WeatherInfo;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.WeatherWatchAppRequest;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.ha0;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int NUMBER_OF_REQUIRED_WEATHER_INFO; // = 1;
    @DexIgnore
    @Keep
    public static /* final */ int NUMBER_OF_SUPPORTED_WEATHER_INFO; // = 3;
    @DexIgnore
    public /* final */ WeatherInfo[] weatherInfoArray;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherWatchAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppData createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherWatchAppData(parcel, (rd4) null);
        }

        @DexIgnore
        public WeatherWatchAppData[] newArray(int i) {
            return new WeatherWatchAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherWatchAppData(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void weatherInfoArray$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wd4.a((Object) WeatherWatchAppData.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !Arrays.equals(this.weatherInfoArray, ((WeatherWatchAppData) obj).weatherInfoArray);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherWatchAppData");
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        wd4.b(version, "version");
        ha0 ha0 = ha0.a;
        DeviceRequest deviceRequest = getDeviceRequest();
        return ha0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return xa0.a(super.getResponseJSONLog(), JSONKey.WEATHER_CONFIGS, k00.a(this.weatherInfoArray));
    }

    @DexIgnore
    public final WeatherInfo[] getWeatherInfoArray() {
        return this.weatherInfoArray;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + Arrays.hashCode(this.weatherInfoArray);
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!(this.weatherInfoArray.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                int min = Math.min(this.weatherInfoArray.length, 3);
                for (int i = 0; i < min; i++) {
                    jSONArray.put(this.weatherInfoArray[i].getSettingJSONData$blesdk_productionRelease());
                }
                jSONObject.put("weatherApp._.config.locations", jSONArray);
            } else {
                String message = getMessage();
                if (message == null) {
                    message = "";
                }
                JSONObject jSONObject2 = new JSONObject();
                int min2 = Math.min(32, message.length());
                if (message != null) {
                    String substring = message.substring(0, min2);
                    wd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    jSONObject.put("weatherApp._.config.locations", jSONObject2.put("message", substring));
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        boolean z = true;
        if (this.weatherInfoArray.length < 1 && getMessage() == null) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException("weatherInfoArray must have at least 1 elements.".toString());
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.weatherInfoArray, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherWatchAppData(WeatherWatchAppRequest weatherWatchAppRequest, WeatherInfo[] weatherInfoArr) throws IllegalArgumentException {
        super(weatherWatchAppRequest, (String) null);
        wd4.b(weatherWatchAppRequest, "weatherWatchAppRequest");
        wd4.b(weatherInfoArr, "weatherInfoArray");
        this.weatherInfoArray = weatherInfoArr;
        j();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherWatchAppData(WeatherWatchAppRequest weatherWatchAppRequest, String str) throws IllegalArgumentException {
        super(weatherWatchAppRequest, str);
        wd4.b(weatherWatchAppRequest, "weatherWatchAppRequest");
        wd4.b(str, "message");
        this.weatherInfoArray = new WeatherInfo[0];
        j();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherWatchAppData(String str) throws IllegalArgumentException {
        super((DeviceRequest) null, str);
        wd4.b(str, "message");
        this.weatherInfoArray = new WeatherInfo[0];
        j();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherWatchAppData(WeatherInfo[] weatherInfoArr) throws IllegalArgumentException {
        super((DeviceRequest) null, (String) null);
        wd4.b(weatherInfoArr, "weatherInfoArray");
        this.weatherInfoArray = weatherInfoArr;
        j();
    }

    @DexIgnore
    public WeatherWatchAppData(Parcel parcel) {
        super((DeviceRequest) parcel.readParcelable(WeatherWatchAppRequest.class.getClassLoader()), parcel.readString());
        Object[] createTypedArray = parcel.createTypedArray(WeatherInfo.CREATOR);
        if (createTypedArray != null) {
            this.weatherInfoArray = (WeatherInfo[]) createTypedArray;
        } else {
            wd4.a();
            throw null;
        }
    }
}
