package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.ha0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class IFTTTWatchAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<IFTTTWatchAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public IFTTTWatchAppData createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new IFTTTWatchAppData(parcel, (rd4) null);
        }

        @DexIgnore
        public IFTTTWatchAppData[] newArray(int i) {
            return new IFTTTWatchAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ IFTTTWatchAppData(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        wd4.b(version, "version");
        ha0 ha0 = ha0.a;
        DeviceRequest deviceRequest = getDeviceRequest();
        return ha0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("iftttApp._.config.query_res_msg", getMessage());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public IFTTTWatchAppData(IFTTTWatchAppRequest iFTTTWatchAppRequest, String str) {
        super(iFTTTWatchAppRequest, str);
        wd4.b(iFTTTWatchAppRequest, "iftttWatchAppRequest");
        wd4.b(str, "message");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public IFTTTWatchAppData(Parcel parcel) {
        this(r0, r3);
        Parcelable readParcelable = parcel.readParcelable(IFTTTWatchAppRequest.class.getClassLoader());
        if (readParcelable != null) {
            IFTTTWatchAppRequest iFTTTWatchAppRequest = (IFTTTWatchAppRequest) readParcelable;
            String readString = parcel.readString();
            if (readString != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
