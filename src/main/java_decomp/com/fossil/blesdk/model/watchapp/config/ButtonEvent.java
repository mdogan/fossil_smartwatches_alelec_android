package com.fossil.blesdk.model.watchapp.config;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ButtonEvent implements Serializable {
    TOP_PRESS,
    TOP_HOLD,
    TOP_SHORT_PRESS_RELEASE,
    TOP_LONG_PRESS_RELEASE,
    TOP_SINGLE_CLICK,
    TOP_DOUBLE_CLICK,
    MIDDLE_PRESS,
    MIDDLE_HOLD,
    MIDDLE_SHORT_PRESS_RELEASE,
    MIDDLE_LONG_PRESS_RELEASE,
    MIDDLE_SINGLE_CLICK,
    MIDDLE_DOUBLE_CLICK,
    BOTTOM_PRESS,
    BOTTOM_HOLD,
    BOTTOM_SHORT_PRESS_RELEASE,
    BOTTOM_LONG_PRESS_RELEASE,
    BOTTOM_SINGLE_CLICK,
    BOTTOM_DOUBLE_CLICK;
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ButtonEvent a(String str) {
            wd4.b(str, "logName");
            for (ButtonEvent buttonEvent : ButtonEvent.values()) {
                if (wd4.a((Object) buttonEvent.getLogName(), (Object) str)) {
                    return buttonEvent;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    public final String getLogName() {
        return this.logName;
    }
}
