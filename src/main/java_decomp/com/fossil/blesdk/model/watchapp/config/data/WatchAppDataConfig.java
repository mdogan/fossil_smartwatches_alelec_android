package com.fossil.blesdk.model.watchapp.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.na0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class WatchAppDataConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ WatchAppDataConfigId dataConfigId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchAppDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WatchAppDataConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            WatchAppDataConfigId watchAppDataConfigId = WatchAppDataConfigId.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = na0.a[watchAppDataConfigId.ordinal()];
            if (i == 1) {
                return WatchAppEmptyDataConfig.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return CommuteTimeWatchAppDataConfig.CREATOR.createFromParcel(parcel);
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public WatchAppDataConfig[] newArray(int i) {
            return new WatchAppDataConfig[i];
        }
    }

    @DexIgnore
    public WatchAppDataConfig(WatchAppDataConfigId watchAppDataConfigId) {
        wd4.b(watchAppDataConfigId, "dataConfigId");
        this.dataConfigId = watchAppDataConfigId;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.dataConfigId == ((WatchAppDataConfig) obj).dataConfigId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.dataConfigId.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.dataConfigId.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WatchAppDataConfig(Parcel parcel) {
        this(WatchAppDataConfigId.values()[parcel.readInt()]);
        wd4.b(parcel, "parcel");
    }
}
