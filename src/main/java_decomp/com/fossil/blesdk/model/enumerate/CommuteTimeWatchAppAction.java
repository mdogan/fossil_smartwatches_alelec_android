package com.fossil.blesdk.model.enumerate;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum CommuteTimeWatchAppAction {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String jsonName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CommuteTimeWatchAppAction a(String str) {
            wd4.b(str, "jsonName");
            for (CommuteTimeWatchAppAction commuteTimeWatchAppAction : CommuteTimeWatchAppAction.values()) {
                if (wd4.a((Object) commuteTimeWatchAppAction.getJsonName$blesdk_productionRelease(), (Object) str)) {
                    return commuteTimeWatchAppAction;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    CommuteTimeWatchAppAction(String str) {
        this.jsonName = str;
    }

    @DexIgnore
    public final String getJsonName$blesdk_productionRelease() {
        return this.jsonName;
    }
}
