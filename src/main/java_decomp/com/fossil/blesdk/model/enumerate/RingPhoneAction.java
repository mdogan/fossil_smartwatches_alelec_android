package com.fossil.blesdk.model.enumerate;

import com.facebook.LegacyTokenHelper;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum RingPhoneAction {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final RingPhoneAction a(String str) {
            wd4.b(str, LegacyTokenHelper.TYPE_STRING);
            for (RingPhoneAction ringPhoneAction : RingPhoneAction.values()) {
                String logName$blesdk_productionRelease = ringPhoneAction.getLogName$blesdk_productionRelease();
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                String lowerCase = str.toLowerCase(locale);
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (wd4.a((Object) logName$blesdk_productionRelease, (Object) lowerCase)) {
                    return ringPhoneAction;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
