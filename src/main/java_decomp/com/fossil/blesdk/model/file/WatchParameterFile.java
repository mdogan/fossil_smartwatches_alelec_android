package com.fossil.blesdk.model.file;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParameterFile extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ int MINIMUM_DATA_SIZE; // = 16;
    @DexIgnore
    public /* final */ byte[] data;
    @DexIgnore
    public /* final */ short fileHandle;
    @DexIgnore
    public /* final */ Version watchParameterVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchParameterFile> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WatchParameterFile createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WatchParameterFile(parcel, (rd4) null);
        }

        @DexIgnore
        public WatchParameterFile[] newArray(int i) {
            return new WatchParameterFile[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WatchParameterFile(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final short getFileHandle$blesdk_productionRelease() {
        return this.fileHandle;
    }

    @DexIgnore
    public final Version getWatchParameterVersion() {
        return this.watchParameterVersion;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (!(this.data.length >= 16)) {
            throw new IllegalArgumentException("data.size(" + this.data.length + ") is not equal or larger " + "than 16");
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.FILE_HANDLE, o90.a(this.fileHandle)), JSONKey.FILE_VERSION, this.watchParameterVersion.toString()), JSONKey.FILE_CRC_C, Long.valueOf(Crc32Calculator.a.a(this.data, Crc32Calculator.CrcType.CRC32C))), JSONKey.FILE_SIZE, Integer.valueOf(this.data.length));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.data);
        }
    }

    @DexIgnore
    public WatchParameterFile(byte[] bArr) throws IllegalArgumentException {
        wd4.b(bArr, "data");
        this.data = bArr;
        i();
        this.fileHandle = ByteBuffer.wrap(kb4.a(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
        this.watchParameterVersion = new Version(bArr[2], bArr[3]);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WatchParameterFile(Parcel parcel) {
        this(r1);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
        } else {
            wd4.a();
            throw null;
        }
    }
}
