package com.fossil.blesdk.model.file;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationIcon extends AssetFile {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationIcon> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public NotificationIcon createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new NotificationIcon(parcel, (rd4) null);
        }

        @DexIgnore
        public NotificationIcon[] newArray(int i) {
            return new NotificationIcon[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationIcon(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) NotificationIcon.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIcon notificationIcon = (NotificationIcon) obj;
            if (getFileDataCrc() == notificationIcon.getFileDataCrc() || wd4.a((Object) o90.a((int) getFileDataCrc()), (Object) notificationIcon.getFileName()) || wd4.a((Object) getFileName(), (Object) o90.a((int) notificationIcon.getFileDataCrc()))) {
                return true;
            }
            return false;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.file.NotificationIcon");
    }

    @DexIgnore
    public int hashCode() {
        return (int) getFileDataCrc();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationIcon(String str, byte[] bArr) {
        super(str, bArr);
        wd4.b(str, "fileName");
        wd4.b(bArr, "fileData");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public NotificationIcon(Parcel parcel) throws IllegalArgumentException {
        this(r0, r3);
        String readString = parcel.readString();
        if (readString != null) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
