package com.fossil.blesdk.model.file;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.background.BackgroundImage;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class AssetFile extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a Companion; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ byte ENTRY_HEADER_LENGTH; // = 2;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_FILE_NAME_LENGTH; // = 31;
    @DexIgnore
    public /* final */ byte[] fileData;
    @DexIgnore
    public /* final */ long fileDataCrc;
    @DexIgnore
    public /* final */ String fileName;
    @DexIgnore
    public /* final */ String shortFileName; // = o90.a((int) this.fileDataCrc);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public AssetFile(String str, byte[] bArr) {
        wd4.b(str, "fileName");
        wd4.b(bArr, "fileData");
        this.fileName = a(str);
        this.fileData = bArr;
        this.fileDataCrc = Crc32Calculator.a.a(bArr, Crc32Calculator.CrcType.CRC32);
    }

    @DexIgnore
    public final String a(String str) {
        Charset f = va0.y.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f);
            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] copyOf = Arrays.copyOf(bytes, Math.min(bytes.length, 31));
            wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
            return new String(copyOf, va0.y.f());
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (true ^ wd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.fileData, ((BackgroundImage) obj).getFileData());
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImage");
    }

    @DexIgnore
    public final byte[] getEntryData$blesdk_productionRelease() {
        if (!(!(this.fileData.length == 0))) {
            return new byte[0];
        }
        String str = this.shortFileName;
        Charset defaultCharset = Charset.defaultCharset();
        wd4.a((Object) defaultCharset, "Charset.defaultCharset()");
        if (str != null) {
            byte[] bytes = str.getBytes(defaultCharset);
            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] a2 = kb4.a(bytes, (byte) 0);
            int length = this.fileData.length;
            ByteBuffer allocate = ByteBuffer.allocate(a2.length + 2 + length);
            wd4.a((Object) allocate, "ByteBuffer.allocate(totalLen)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putShort((short) (a2.length + length));
            allocate.put(a2);
            allocate.put(this.fileData);
            byte[] array = allocate.array();
            wd4.a((Object) array, "result.array()");
            return array;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] getFileData() {
        return this.fileData;
    }

    @DexIgnore
    public final long getFileDataCrc() {
        return this.fileDataCrc;
    }

    @DexIgnore
    public final String getFileName() {
        return this.fileName;
    }

    @DexIgnore
    public final Object getJsonFileName$blesdk_productionRelease() {
        Object obj;
        if (isEmptyFile$blesdk_productionRelease()) {
            obj = JSONObject.NULL;
        } else {
            obj = this.shortFileName;
        }
        wd4.a(obj, "if (isEmptyFile()) {\n   \u2026ortFileName\n            }");
        return obj;
    }

    @DexIgnore
    public final String getShortFileName$blesdk_productionRelease() {
        return this.shortFileName;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.fileData);
    }

    @DexIgnore
    public final boolean isEmptyFile$blesdk_productionRelease() {
        return this.fileData.length == 0;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            xa0.a(jSONObject, JSONKey.NAME, this.fileName);
            xa0.a(jSONObject, JSONKey.FILE_SIZE, Integer.valueOf(this.fileData.length));
            xa0.a(jSONObject, JSONKey.FILE_CRC, Long.valueOf(this.fileDataCrc));
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.fileName);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.fileData);
        }
    }
}
