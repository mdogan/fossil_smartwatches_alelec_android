package com.fossil.blesdk.model.notification.filter;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.notification.NotificationType;
import com.fossil.blesdk.model.file.NotificationIcon;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationIconConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ NotificationIcon defaultIcon;
    @DexIgnore
    public /* final */ Hashtable<NotificationType, NotificationIcon> typeIcons;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationIconConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public NotificationIconConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new NotificationIconConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public NotificationIconConfig[] newArray(int i) {
            return new NotificationIconConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationIconConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) NotificationIconConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIconConfig notificationIconConfig = (NotificationIconConfig) obj;
            if (!wd4.a((Object) this.defaultIcon, (Object) notificationIconConfig.defaultIcon) || !wd4.a((Object) this.typeIcons, (Object) notificationIconConfig.typeIcons)) {
                return false;
            }
            return true;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.notification.filter.NotificationIconConfig");
    }

    @DexIgnore
    public final NotificationIcon getDefaultIcon() {
        return this.defaultIcon;
    }

    @DexIgnore
    public final NotificationIcon[] getDistinctIcons$blesdk_productionRelease() {
        Collection<NotificationIcon> values = this.typeIcons.values();
        wd4.a((Object) values, "typeIcons.values");
        Object[] array = wb4.c(wb4.a(values, this.defaultIcon)).toArray(new NotificationIcon[0]);
        if (array != null) {
            return (NotificationIcon[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final NotificationIcon getIconForType(NotificationType notificationType) {
        wd4.b(notificationType, "notificationType");
        return this.typeIcons.get(notificationType);
    }

    @DexIgnore
    public final byte[] getNotificationFilterIconConfigData$blesdk_productionRelease() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        for (NotificationType notificationType : NotificationType.values()) {
            NotificationIcon notificationIcon = this.typeIcons.get(notificationType);
            int ordinal = 1 << notificationType.ordinal();
            if (notificationIcon == null || notificationIcon.isEmptyFile$blesdk_productionRelease()) {
                i |= ordinal;
            } else {
                String shortFileName$blesdk_productionRelease = notificationIcon.getShortFileName$blesdk_productionRelease();
                Charset f = va0.y.f();
                if (shortFileName$blesdk_productionRelease != null) {
                    byte[] bytes = shortFileName$blesdk_productionRelease.getBytes(f);
                    wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] a2 = kb4.a(bytes, (byte) 0);
                    ByteBuffer order = ByteBuffer.allocate(a2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                    wd4.a((Object) order, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                    order.putShort((short) ordinal).put((byte) a2.length).put(a2);
                    byteArrayOutputStream.write(order.array());
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        if (i != 0) {
            String shortFileName$blesdk_productionRelease2 = this.defaultIcon.getShortFileName$blesdk_productionRelease();
            Charset f2 = va0.y.f();
            if (shortFileName$blesdk_productionRelease2 != null) {
                byte[] bytes2 = shortFileName$blesdk_productionRelease2.getBytes(f2);
                wd4.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] a3 = kb4.a(bytes2, (byte) 0);
                ByteBuffer order2 = ByteBuffer.allocate(a3.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                wd4.a((Object) order2, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                order2.putShort((short) i).put((byte) a3.length).put(a3);
                byteArrayOutputStream.write(order2.array());
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wd4.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public int hashCode() {
        return (this.defaultIcon.hashCode() * 31) + this.typeIcons.hashCode();
    }

    @DexIgnore
    public final NotificationIconConfig setIconForType(NotificationType notificationType, NotificationIcon notificationIcon) {
        wd4.b(notificationType, "notificationType");
        if (notificationIcon != null) {
            this.typeIcons.put(notificationType, notificationIcon);
        } else {
            this.typeIcons.remove(notificationType);
        }
        return this;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject a2 = xa0.a(new JSONObject(), JSONKey.DEFAULT_ICON, this.defaultIcon.toJSONObject());
        for (Map.Entry next : this.typeIcons.entrySet()) {
            a2.put(((NotificationType) next.getKey()).getLogName$blesdk_productionRelease(), ((NotificationIcon) next.getValue()).toJSONObject());
        }
        return a2;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.defaultIcon, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.typeIcons);
        }
    }

    @DexIgnore
    public NotificationIconConfig(NotificationIcon notificationIcon) {
        wd4.b(notificationIcon, "defaultIcon");
        this.typeIcons = new Hashtable<>();
        this.defaultIcon = notificationIcon;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public NotificationIconConfig(Parcel parcel) {
        this((NotificationIcon) r0);
        Parcelable readParcelable = parcel.readParcelable(NotificationIcon.class.getClassLoader());
        if (readParcelable != null) {
            Hashtable<NotificationType, NotificationIcon> hashtable = this.typeIcons;
            Serializable readSerializable = parcel.readSerializable();
            if (readSerializable != null) {
                hashtable.putAll((Hashtable) readSerializable);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.Hashtable<com.fossil.blesdk.device.data.notification.NotificationType, com.fossil.blesdk.model.file.NotificationIcon>");
        }
        wd4.a();
        throw null;
    }
}
