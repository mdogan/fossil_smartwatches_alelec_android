package com.fossil.blesdk.model.complication.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationEmptyDataConfig extends ComplicationDataConfig {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationEmptyDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ComplicationEmptyDataConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ComplicationEmptyDataConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public ComplicationEmptyDataConfig[] newArray(int i) {
            return new ComplicationEmptyDataConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationEmptyDataConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return new JSONObject();
    }

    @DexIgnore
    public ComplicationEmptyDataConfig() {
        super(ComplicationDataConfigId.EMPTY);
    }

    @DexIgnore
    public ComplicationEmptyDataConfig(Parcel parcel) {
        super(parcel);
    }
}
