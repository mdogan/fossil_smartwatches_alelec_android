package com.fossil.blesdk.model.complication.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ga0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ComplicationDataConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ComplicationDataConfigId dataConfigId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ComplicationDataConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            ComplicationDataConfigId complicationDataConfigId = ComplicationDataConfigId.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = ga0.a[complicationDataConfigId.ordinal()];
            if (i == 1) {
                return ComplicationEmptyDataConfig.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return TimeZoneTwoComplicationDataConfig.CREATOR.createFromParcel(parcel);
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public ComplicationDataConfig[] newArray(int i) {
            return new ComplicationDataConfig[i];
        }
    }

    @DexIgnore
    public ComplicationDataConfig(ComplicationDataConfigId complicationDataConfigId) {
        wd4.b(complicationDataConfigId, "dataConfigId");
        this.dataConfigId = complicationDataConfigId;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.dataConfigId == ((ComplicationDataConfig) obj).dataConfigId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.dataConfigId.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.dataConfigId.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ComplicationDataConfig(Parcel parcel) {
        this(ComplicationDataConfigId.values()[parcel.readInt()]);
        wd4.b(parcel, "parcel");
    }
}
