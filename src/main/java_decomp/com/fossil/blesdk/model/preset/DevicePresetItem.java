package com.fossil.blesdk.model.preset;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.device.data.complication.ComplicationConfig;
import com.fossil.blesdk.device.data.watchapp.WatchAppConfig;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DevicePresetItem extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public Version fileVersion; // = va0.y.g();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DevicePresetItem> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DevicePresetItem createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (wd4.a((Object) readString, (Object) ComplicationConfig.class.getCanonicalName())) {
                return ComplicationConfig.CREATOR.createFromParcel(parcel);
            }
            if (wd4.a((Object) readString, (Object) WatchAppConfig.class.getCanonicalName())) {
                return WatchAppConfig.CREATOR.createFromParcel(parcel);
            }
            if (wd4.a((Object) readString, (Object) BackgroundImageConfig.class.getCanonicalName())) {
                return BackgroundImageConfig.CREATOR.createFromParcel(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        public DevicePresetItem[] newArray(int i) {
            return new DevicePresetItem[i];
        }
    }

    @DexIgnore
    public DevicePresetItem() {
    }

    @DexIgnore
    public abstract JSONObject getAssignmentJSON$blesdk_productionRelease();

    @DexIgnore
    public final Version getFileVersion$blesdk_productionRelease() {
        return this.fileVersion;
    }

    @DexIgnore
    public final JSONObject getSettingAssignmentJSON$blesdk_productionRelease() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", getAssignmentJSON$blesdk_productionRelease()));
            wd4.a((Object) put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            ea0.l.a(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public final void setFileVersion$blesdk_productionRelease(Version version) {
        wd4.b(version, "<set-?>");
        this.fileVersion = version;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }

    @DexIgnore
    public DevicePresetItem(Parcel parcel) {
        wd4.b(parcel, "parcel");
        parcel.readString();
    }
}
