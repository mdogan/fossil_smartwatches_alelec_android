package com.fossil.blesdk.model.microapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.model.microapp.animation.HandAnimation;
import com.fossil.blesdk.model.microapp.enumerate.Direction;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppHandId;
import com.fossil.blesdk.model.microapp.enumerate.MovingType;
import com.fossil.blesdk.model.microapp.enumerate.Speed;
import com.fossil.blesdk.model.microapp.enumerate.VibePattern;
import com.fossil.blesdk.model.microapp.instruction.AnimationInstr;
import com.fossil.blesdk.model.microapp.instruction.CloseInstr;
import com.fossil.blesdk.model.microapp.instruction.DelayInstr;
import com.fossil.blesdk.model.microapp.instruction.Instruction;
import com.fossil.blesdk.model.microapp.instruction.StartCriticalInstr;
import com.fossil.blesdk.model.microapp.instruction.VibeInstr;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppCommuteTimeResponse extends MicroAppResponse {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public int hour;
    @DexIgnore
    public int minute;
    @DexIgnore
    public boolean shipHandsToTwelve;
    @DexIgnore
    public /* final */ int travelTimeInMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppCommuteTimeResponse> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MicroAppCommuteTimeResponse createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MicroAppCommuteTimeResponse(parcel, (rd4) null);
        }

        @DexIgnore
        public MicroAppCommuteTimeResponse[] newArray(int i) {
            return new MicroAppCommuteTimeResponse[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppCommuteTimeResponse(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MicroAppCommuteTimeResponse.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppCommuteTimeResponse microAppCommuteTimeResponse = (MicroAppCommuteTimeResponse) obj;
            return this.shipHandsToTwelve == microAppCommuteTimeResponse.shipHandsToTwelve && this.travelTimeInMinute == microAppCommuteTimeResponse.travelTimeInMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse");
    }

    @DexIgnore
    public List<Instruction> getInstructions() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new StartCriticalInstr());
        if (this.shipHandsToTwelve) {
            arrayList.add(new AnimationInstr(new HandAnimation[]{new HandAnimation(MicroAppHandId.HOUR, Direction.SHORTEST_PATH, MovingType.POSITION, Speed.FULL, 0), new HandAnimation(MicroAppHandId.MINUTE, Direction.SHORTEST_PATH, MovingType.POSITION, Speed.FULL, 0)}));
        }
        arrayList.add(new DelayInstr(0.5d));
        arrayList.add(new VibeInstr(VibePattern.ERROR));
        arrayList.add(new AnimationInstr(new HandAnimation[]{new HandAnimation(MicroAppHandId.HOUR, Direction.CLOCK_WISE, l(), Speed.FULL, j()), new HandAnimation(MicroAppHandId.MINUTE, Direction.CLOCK_WISE, l(), Speed.FULL, k())}));
        arrayList.add(new DelayInstr(8.0d));
        arrayList.add(new CloseInstr());
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        return (Boolean.valueOf(this.shipHandsToTwelve).hashCode() * 31) + this.travelTimeInMinute;
    }

    @DexIgnore
    public final short j() {
        int i;
        if (m()) {
            i = (this.hour * 30) + ((this.minute * 30) / 60);
        } else {
            int i2 = this.travelTimeInMinute;
            i = i2 >= 60 ? (i2 / 60) * 30 : i2 * 6;
        }
        return (short) i;
    }

    @DexIgnore
    public final short k() {
        int i;
        if (m()) {
            i = this.minute;
        } else {
            i = this.travelTimeInMinute;
            if (i >= 60) {
                i %= 60;
            }
        }
        return (short) (i * 6);
    }

    @DexIgnore
    public final MovingType l() {
        if (m()) {
            return MovingType.POSITION;
        }
        if (this.travelTimeInMinute >= 60) {
            return MovingType.POSITION;
        }
        return MovingType.DISTANCE;
    }

    @DexIgnore
    public final boolean m() {
        return this.travelTimeInMinute == 0;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(super.toJSONObject(), JSONKey.SHIP_HANDS_TO_TWELVE, Integer.valueOf(this.shipHandsToTwelve ? 1 : 0)), JSONKey.TRAVEL_TIME_IN_MINUTE, Integer.valueOf(this.travelTimeInMinute));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.shipHandsToTwelve ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.travelTimeInMinute);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppCommuteTimeResponse(MicroAppEvent microAppEvent, Version version, boolean z, int i, int i2, rd4 rd4) {
        this(microAppEvent, version, (i2 & 4) != 0 ? true : z, i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppCommuteTimeResponse(MicroAppEvent microAppEvent, Version version, boolean z, int i) {
        super(microAppEvent, version);
        wd4.b(microAppEvent, Constants.EVENT);
        wd4.b(version, "systemVersion");
        this.shipHandsToTwelve = z;
        this.travelTimeInMinute = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MicroAppCommuteTimeResponse(MicroAppEvent microAppEvent, Version version, int i, int i2) {
        this(microAppEvent, version, false, 0);
        wd4.b(microAppEvent, Constants.EVENT);
        wd4.b(version, "systemVersion");
        this.hour = i;
        this.minute = i2;
    }

    @DexIgnore
    public MicroAppCommuteTimeResponse(Parcel parcel) {
        super(parcel);
        this.shipHandsToTwelve = parcel.readInt() != 0;
        this.travelTimeInMinute = parcel.readInt();
    }
}
