package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.model.microapp.enumerate.VibePattern;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VibeInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ VibePattern pattern;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<VibeInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public VibeInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new VibeInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public VibeInstr[] newArray(int i) {
            return new VibeInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ VibeInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) VibeInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.pattern == ((VibeInstr) obj).pattern;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.VibeInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        wd4.a((Object) order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.pattern.getValue());
        byte[] array = order.array();
        wd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return this.pattern.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(super.toJSONObject(), JSONKey.VIBE_PATTERN, this.pattern.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.pattern.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VibeInstr(VibePattern vibePattern) {
        super(InstructionId.VIBE);
        wd4.b(vibePattern, "pattern");
        this.pattern = vibePattern;
    }

    @DexIgnore
    public VibeInstr(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.pattern = VibePattern.valueOf(readString);
        } else {
            wd4.a();
            throw null;
        }
    }
}
