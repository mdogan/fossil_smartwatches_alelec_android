package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StartRepeatInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ byte times;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<StartRepeatInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public StartRepeatInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new StartRepeatInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public StartRepeatInstr[] newArray(int i) {
            return new StartRepeatInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ StartRepeatInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) StartRepeatInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.times == ((StartRepeatInstr) obj).times;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.StartRepeatInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        wd4.a((Object) order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.times);
        byte[] array = order.array();
        wd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return this.times;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(super.toJSONObject(), JSONKey.TIMES, Byte.valueOf(this.times));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.times);
        }
    }

    @DexIgnore
    public StartRepeatInstr(byte b) {
        super(InstructionId.START_REPEAT);
        this.times = b;
    }

    @DexIgnore
    public StartRepeatInstr(Parcel parcel) {
        super(parcel);
        this.times = parcel.readByte();
    }
}
