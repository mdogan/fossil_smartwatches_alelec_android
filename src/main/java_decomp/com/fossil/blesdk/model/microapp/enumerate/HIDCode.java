package com.fossil.blesdk.model.microapp.enumerate;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum HIDCode {
    PLAY_PAUSE(205, HIDType.MEDIA),
    VOLUME_UP(233, HIDType.MEDIA),
    VOLUME_DOWN(234, HIDType.MEDIA),
    NEXT(181, HIDType.MEDIA),
    PREVIOUS(182, HIDType.MEDIA),
    MUTE(226, HIDType.MEDIA),
    LOW_WHITE(5, HIDType.KEYBOARD),
    LOW_BLACK(26, HIDType.KEYBOARD),
    RIGHT(79, HIDType.KEYBOARD),
    LEFT(80, HIDType.KEYBOARD),
    PAGE_UP(75, HIDType.KEYBOARD),
    PAGE_DOWN(78, HIDType.KEYBOARD),
    ENTER(88, HIDType.KEYBOARD);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ HIDType type;
    @DexIgnore
    public /* final */ short value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    HIDCode(short s, HIDType hIDType) {
        this.value = s;
        this.type = hIDType;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final HIDType getType() {
        return this.type;
    }

    @DexIgnore
    public final short getValue() {
        return this.value;
    }
}
