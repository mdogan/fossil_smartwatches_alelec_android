package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StartCriticalInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<StartCriticalInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public StartCriticalInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new StartCriticalInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public StartCriticalInstr[] newArray(int i) {
            return new StartCriticalInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ StartCriticalInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        return new byte[0];
    }

    @DexIgnore
    public StartCriticalInstr() {
        super(InstructionId.START_CRITICAL);
    }

    @DexIgnore
    public StartCriticalInstr(Parcel parcel) {
        super(parcel);
    }
}
