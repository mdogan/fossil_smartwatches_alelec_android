package com.fossil.blesdk.model.microapp.customization;

import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ia0;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppCustomization extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public ia0 microAppHandle;

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        ia0 ia0 = this.microAppHandle;
        if (ia0 != null) {
            int length = ia0.a().length + 3 + parameters$blesdk_productionRelease().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            wd4.a((Object) allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            ia0 ia02 = this.microAppHandle;
            if (ia02 != null) {
                allocate.put(ia02.a());
                allocate.putShort((short) length);
                allocate.put((byte) numberOfCustomizationType$blesdk_productionRelease());
                allocate.put(parameters$blesdk_productionRelease());
                byte[] array = allocate.array();
                wd4.a((Object) array, "byteBuffer.array()");
                return array;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final ia0 getMicroAppHandle$blesdk_productionRelease() {
        return this.microAppHandle;
    }

    @DexIgnore
    public abstract int numberOfCustomizationType$blesdk_productionRelease();

    @DexIgnore
    public abstract byte[] parameters$blesdk_productionRelease();

    @DexIgnore
    public final void setMicroAppHandle$blesdk_productionRelease(ia0 ia0) {
        this.microAppHandle = ia0;
    }
}
