package com.fossil.blesdk.model.microapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.model.microapp.animation.HandAnimation;
import com.fossil.blesdk.model.microapp.enumerate.Direction;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppHandId;
import com.fossil.blesdk.model.microapp.enumerate.MovingType;
import com.fossil.blesdk.model.microapp.enumerate.Speed;
import com.fossil.blesdk.model.microapp.enumerate.VibePattern;
import com.fossil.blesdk.model.microapp.instruction.AnimationInstr;
import com.fossil.blesdk.model.microapp.instruction.CloseInstr;
import com.fossil.blesdk.model.microapp.instruction.Instruction;
import com.fossil.blesdk.model.microapp.instruction.StartCriticalInstr;
import com.fossil.blesdk.model.microapp.instruction.VibeInstr;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppErrorResponse extends MicroAppResponse {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ boolean shipHandsToTwelve;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppErrorResponse> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MicroAppErrorResponse createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MicroAppErrorResponse(parcel, (rd4) null);
        }

        @DexIgnore
        public MicroAppErrorResponse[] newArray(int i) {
            return new MicroAppErrorResponse[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppErrorResponse(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MicroAppErrorResponse.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.shipHandsToTwelve == ((MicroAppErrorResponse) obj).shipHandsToTwelve;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse");
    }

    @DexIgnore
    public List<Instruction> getInstructions() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new StartCriticalInstr());
        if (this.shipHandsToTwelve) {
            arrayList.add(new AnimationInstr(new HandAnimation[]{new HandAnimation(MicroAppHandId.HOUR, Direction.SHORTEST_PATH, MovingType.POSITION, Speed.FULL, 0), new HandAnimation(MicroAppHandId.HOUR, Direction.SHORTEST_PATH, MovingType.POSITION, Speed.FULL, 0)}));
        }
        arrayList.add(new VibeInstr(VibePattern.ERROR));
        arrayList.add(new CloseInstr());
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.shipHandsToTwelve).hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(super.toJSONObject(), JSONKey.SHIP_HANDS_TO_TWELVE, Integer.valueOf(this.shipHandsToTwelve ? 1 : 0));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.shipHandsToTwelve ? 1 : 0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppErrorResponse(MicroAppEvent microAppEvent, Version version, boolean z) {
        super(microAppEvent, version);
        wd4.b(microAppEvent, Constants.EVENT);
        wd4.b(version, "systemVersion");
        this.shipHandsToTwelve = z;
    }

    @DexIgnore
    public MicroAppErrorResponse(Parcel parcel) {
        super(parcel);
        this.shipHandsToTwelve = parcel.readInt() != 0;
    }
}
