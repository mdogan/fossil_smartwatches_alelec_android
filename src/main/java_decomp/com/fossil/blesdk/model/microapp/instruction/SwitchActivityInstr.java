package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActivityInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ byte activityId;
    @DexIgnore
    public /* final */ boolean isResume;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<SwitchActivityInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public SwitchActivityInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new SwitchActivityInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public SwitchActivityInstr[] newArray(int i) {
            return new SwitchActivityInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ SwitchActivityInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) SwitchActivityInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            SwitchActivityInstr switchActivityInstr = (SwitchActivityInstr) obj;
            return this.isResume == switchActivityInstr.isResume && this.activityId == switchActivityInstr.activityId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.SwitchActivityInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        wd4.a((Object) order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.activityId);
        order.put(this.isResume ? (byte) 1 : 0);
        byte[] array = order.array();
        wd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return (this.activityId * 31) + Boolean.valueOf(this.isResume).hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(super.toJSONObject(), JSONKey.ACTIVITY_ID, Byte.valueOf(this.activityId)), JSONKey.IS_RESUME, Boolean.valueOf(this.isResume));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.activityId);
        }
        if (parcel != null) {
            parcel.writeInt(this.isResume ? 1 : 0);
        }
    }

    @DexIgnore
    public SwitchActivityInstr(byte b, boolean z) {
        super(InstructionId.SWITCH_ACTIVITY);
        this.activityId = b;
        this.isResume = z;
    }

    @DexIgnore
    public SwitchActivityInstr(Parcel parcel) {
        super(parcel);
        this.activityId = parcel.readByte();
        this.isResume = parcel.readInt() != 0;
    }
}
