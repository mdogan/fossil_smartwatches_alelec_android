package com.fossil.blesdk.model.microapp.enumerate;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;
import kotlin.TypeCastException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum MicroAppId {
    UNDEFINED((byte) 0),
    MUSIC_CONTROL((byte) 1),
    ACTIVITY_TAGGING((byte) 2),
    GOAL_TRACKING((byte) 3),
    DATE((byte) 4),
    TIME2((byte) 5),
    ALERT((byte) 6),
    ALARM((byte) 7),
    PROGRESS((byte) 8),
    TWENTY_FOUR_HOUR((byte) 9),
    BOLT_CONTROL((byte) 10),
    WEATHER((byte) 11),
    COMMUTE_TIME((byte) 12),
    SELFIE(DateTimeFieldType.HALFDAY_OF_DAY),
    RING_PHONE(DateTimeFieldType.HOUR_OF_HALFDAY),
    STOPWATCH(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    MicroAppId(byte b) {
        this.id = b;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
