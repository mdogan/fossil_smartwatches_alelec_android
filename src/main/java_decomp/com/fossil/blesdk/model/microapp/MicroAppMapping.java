package com.fossil.blesdk.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppButton;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppMapping extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MicroAppButton microAppButton;
    @DexIgnore
    public /* final */ MicroAppDeclaration[] microAppDeclarations;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppMapping> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MicroAppMapping createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MicroAppMapping(parcel, (rd4) null);
        }

        @DexIgnore
        public MicroAppMapping[] newArray(int i) {
            return new MicroAppMapping[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppMapping(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MicroAppMapping.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppMapping microAppMapping = (MicroAppMapping) obj;
            MicroAppButton microAppButton2 = this.microAppButton;
            MicroAppButton microAppButton3 = microAppMapping.microAppButton;
            return microAppButton2 == microAppButton3 && microAppButton2 == microAppButton3 && Arrays.equals(this.microAppDeclarations, microAppMapping.microAppDeclarations);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.MicroAppMapping");
    }

    @DexIgnore
    public final MicroAppButton getMicroAppButton() {
        return this.microAppButton;
    }

    @DexIgnore
    public final MicroAppDeclaration[] getMicroAppDeclarations() {
        return this.microAppDeclarations;
    }

    @DexIgnore
    public int hashCode() {
        return (this.microAppButton.hashCode() * 31) + this.microAppDeclarations.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (MicroAppDeclaration jSONObject : this.microAppDeclarations) {
            jSONArray.put(jSONObject.toJSONObject());
        }
        return xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.BUTTON, this.microAppButton.getLogName$blesdk_productionRelease()), JSONKey.DECLARATIONS, jSONArray), JSONKey.TOTAL_DECLARATIONS, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.microAppButton.name());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.microAppDeclarations, i);
        }
    }

    @DexIgnore
    public MicroAppMapping(MicroAppButton microAppButton2, MicroAppDeclaration[] microAppDeclarationArr) {
        wd4.b(microAppButton2, "microAppButton");
        wd4.b(microAppDeclarationArr, "microAppDeclarations");
        this.microAppButton = microAppButton2;
        this.microAppDeclarations = microAppDeclarationArr;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppMapping(Parcel parcel) {
        this(r0, (MicroAppDeclaration[]) r4);
        String readString = parcel.readString();
        if (readString != null) {
            MicroAppButton valueOf = MicroAppButton.valueOf(readString);
            Object[] createTypedArray = parcel.createTypedArray(MicroAppDeclaration.CREATOR);
            if (createTypedArray != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
