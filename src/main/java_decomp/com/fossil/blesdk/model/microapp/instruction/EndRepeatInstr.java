package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EndRepeatInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<EndRepeatInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public EndRepeatInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new EndRepeatInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public EndRepeatInstr[] newArray(int i) {
            return new EndRepeatInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ EndRepeatInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        return new byte[0];
    }

    @DexIgnore
    public EndRepeatInstr() {
        super(InstructionId.END_REPEAT);
    }

    @DexIgnore
    public EndRepeatInstr(Parcel parcel) {
        super(parcel);
    }
}
