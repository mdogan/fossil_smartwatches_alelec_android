package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloseInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CloseInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CloseInstr createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CloseInstr(parcel, (rd4) null);
        }

        @DexIgnore
        public CloseInstr[] newArray(int i) {
            return new CloseInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CloseInstr(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        return new byte[0];
    }

    @DexIgnore
    public CloseInstr() {
        super(InstructionId.CLOSE);
    }

    @DexIgnore
    public CloseInstr(Parcel parcel) {
        super(parcel);
    }
}
