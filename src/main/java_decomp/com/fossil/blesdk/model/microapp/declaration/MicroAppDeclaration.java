package com.fossil.blesdk.model.microapp.declaration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.microapp.customization.MicroAppCustomization;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;
import com.fossil.blesdk.obfuscated.ja0;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppDeclaration extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MicroAppCustomization customization;
    @DexIgnore
    public /* final */ byte[] data;
    @DexIgnore
    public /* final */ short declarationFileId;
    @DexIgnore
    public /* final */ boolean hasCustomization;
    @DexIgnore
    public /* final */ byte majorVersion;
    @DexIgnore
    public /* final */ MicroAppId microAppId;
    @DexIgnore
    public /* final */ MicroAppVariantId microAppVariantId;
    @DexIgnore
    public /* final */ byte microAppVersion;
    @DexIgnore
    public /* final */ byte minorVersion;
    @DexIgnore
    public /* final */ short runtime;
    @DexIgnore
    public /* final */ byte variationNumber;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppDeclaration> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MicroAppDeclaration createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MicroAppDeclaration(parcel, (rd4) null);
        }

        @DexIgnore
        public MicroAppDeclaration[] newArray(int i) {
            return new MicroAppDeclaration[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppDeclaration(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void hasCustomization$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void majorVersion$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void minorVersion$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void runtime$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MicroAppDeclaration.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppDeclaration microAppDeclaration = (MicroAppDeclaration) obj;
            return Arrays.equals(this.data, microAppDeclaration.data) && !(wd4.a((Object) this.customization, (Object) microAppDeclaration.customization) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration");
    }

    @DexIgnore
    public final MicroAppCustomization getCustomization() {
        return this.customization;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final short getDeclarationFileId$blesdk_productionRelease() {
        return this.declarationFileId;
    }

    @DexIgnore
    public final boolean getHasCustomization() {
        return this.hasCustomization;
    }

    @DexIgnore
    public final byte getMajorVersion() {
        return this.majorVersion;
    }

    @DexIgnore
    public final MicroAppId getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public final MicroAppVariantId getMicroAppVariantId() {
        return this.microAppVariantId;
    }

    @DexIgnore
    public final byte getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public final byte getMinorVersion() {
        return this.minorVersion;
    }

    @DexIgnore
    public final short getRuntime$blesdk_productionRelease() {
        return this.runtime;
    }

    @DexIgnore
    public final byte getVariationNumber() {
        return this.variationNumber;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.data) * 31;
        MicroAppCustomization microAppCustomization = this.customization;
        return hashCode + (microAppCustomization != null ? microAppCustomization.hashCode() : 0);
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.MICRO_APP_ID, this.microAppId.name()), JSONKey.MICRO_APP_VERSION, Byte.valueOf(this.microAppVersion)), JSONKey.MINOR_VERSION, Byte.valueOf(this.minorVersion)), JSONKey.MAJOR_VERSION, Byte.valueOf(this.majorVersion)), JSONKey.VARIATION_NUMBER, Byte.valueOf(this.variationNumber)), JSONKey.VARIANT, this.microAppVariantId), JSONKey.RUN_TIME, Short.valueOf(this.runtime)), JSONKey.HAS_CUSTOMIZATION, Boolean.valueOf(this.hasCustomization)), JSONKey.CRC, Long.valueOf(Crc32Calculator.a.a(this.data, Crc32Calculator.CrcType.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.data);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.customization, i);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppDeclaration(byte[] bArr, MicroAppCustomization microAppCustomization, int i, rd4 rd4) {
        this(bArr, (i & 2) != 0 ? null : microAppCustomization);
    }

    @DexIgnore
    public MicroAppDeclaration(byte[] bArr, MicroAppCustomization microAppCustomization) {
        wd4.b(bArr, "data");
        this.data = bArr;
        this.customization = microAppCustomization;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        wd4.a((Object) order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        boolean z = false;
        this.majorVersion = order.get(0);
        this.minorVersion = order.get(1);
        this.microAppVersion = order.get(2);
        this.declarationFileId = order.getShort(3);
        this.hasCustomization = order.getShort(7) > 0 ? true : z;
        this.runtime = o90.b(order.get(9));
        this.variationNumber = order.get(10);
        this.microAppId = ja0.b.a(this.declarationFileId);
        this.microAppVariantId = ja0.b.b(this.declarationFileId);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppDeclaration(Parcel parcel) {
        this(r0, (MicroAppCustomization) r4);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            Parcelable readParcelable = parcel.readParcelable(MicroAppCustomization.class.getClassLoader());
            if (readParcelable != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
