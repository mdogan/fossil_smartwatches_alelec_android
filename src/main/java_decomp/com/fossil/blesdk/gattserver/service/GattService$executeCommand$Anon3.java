package com.fossil.blesdk.gattserver.service;

import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattService$executeCommand$Anon3 extends Lambda implements jd4<Command, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GattService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattService$executeCommand$Anon3(GattService gattService) {
        super(1);
        this.this$Anon0 = gattService;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Command) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Command command) {
        wd4.b(command, "executedCommand");
        this.this$Anon0.b(command);
    }
}
