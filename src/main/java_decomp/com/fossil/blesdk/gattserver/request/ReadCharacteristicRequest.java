package com.fossil.blesdk.gattserver.request;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadCharacteristicRequest extends ClientRequest {
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic characteristic;
    @DexIgnore
    public /* final */ int offset;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadCharacteristicRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super(bluetoothDevice, i);
        wd4.b(bluetoothDevice, "device");
        wd4.b(bluetoothGattCharacteristic, "characteristic");
        this.offset = i2;
        this.characteristic = bluetoothGattCharacteristic;
    }

    @DexIgnore
    public final BluetoothGattCharacteristic getCharacteristic$blesdk_productionRelease() {
        return this.characteristic;
    }

    @DexIgnore
    public final int getOffset$blesdk_productionRelease() {
        return this.offset;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.OFFSET, Integer.valueOf(this.offset)), JSONKey.CHARACTERISTIC, this.characteristic.getUuid().toString());
    }
}
