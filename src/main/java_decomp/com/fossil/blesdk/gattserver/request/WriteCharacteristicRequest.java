package com.fossil.blesdk.gattserver.request;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WriteCharacteristicRequest extends ClientRequest {
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic characteristic;
    @DexIgnore
    public /* final */ int offset;
    @DexIgnore
    public /* final */ boolean preparedWrite;
    @DexIgnore
    public /* final */ boolean responseNeeded;
    @DexIgnore
    public /* final */ byte[] value;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WriteCharacteristicRequest(BluetoothDevice bluetoothDevice, int i, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, boolean z2, int i2, byte[] bArr) {
        super(bluetoothDevice, i);
        wd4.b(bluetoothDevice, "device");
        wd4.b(bluetoothGattCharacteristic, "characteristic");
        wd4.b(bArr, "value");
        this.characteristic = bluetoothGattCharacteristic;
        this.preparedWrite = z;
        this.responseNeeded = z2;
        this.offset = i2;
        this.value = bArr;
    }

    @DexIgnore
    public final BluetoothGattCharacteristic getCharacteristic$blesdk_productionRelease() {
        return this.characteristic;
    }

    @DexIgnore
    public final int getOffset$blesdk_productionRelease() {
        return this.offset;
    }

    @DexIgnore
    public final boolean getPreparedWrite$blesdk_productionRelease() {
        return this.preparedWrite;
    }

    @DexIgnore
    public final boolean getResponseNeeded$blesdk_productionRelease() {
        return this.responseNeeded;
    }

    @DexIgnore
    public final byte[] getValue$blesdk_productionRelease() {
        return this.value;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(super.toJSONObject(), JSONKey.CHARACTERISTIC, this.characteristic.getUuid().toString()), JSONKey.PREPARED_WRITE, Boolean.valueOf(this.preparedWrite)), JSONKey.RESPONSE_NEEDED, Boolean.valueOf(this.responseNeeded)), JSONKey.OFFSET, Integer.valueOf(this.offset)), JSONKey.VALUE, l90.a(this.value, (String) null, 1, (Object) null));
    }
}
