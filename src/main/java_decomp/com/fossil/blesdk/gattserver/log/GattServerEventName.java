package com.fossil.blesdk.gattserver.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum GattServerEventName {
    CONNECTION_STATE_CHANGE,
    SERVICE_ADDED,
    CHARACTERISTIC_READ,
    CHARACTERISTIC_WRITE,
    DESCRIPTOR_READ,
    DESCRIPTOR_WRITE,
    EXECUTE_WRITE,
    MTU_CHANGED,
    SEND_RESPONSE,
    NOTIFY_CHARACTERISTIC_CHANGED,
    GATT_SERVER_STARTED,
    GATT_SERVER_STOPPED;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
