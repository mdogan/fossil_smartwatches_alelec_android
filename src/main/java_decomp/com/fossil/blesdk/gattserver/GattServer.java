package com.fossil.blesdk.gattserver;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.gattserver.command.CommandQueue;
import com.fossil.blesdk.gattserver.log.GattServerEventName;
import com.fossil.blesdk.gattserver.request.ClientRequest;
import com.fossil.blesdk.gattserver.request.ReadCharacteristicRequest;
import com.fossil.blesdk.gattserver.request.ReadDescriptorRequest;
import com.fossil.blesdk.gattserver.request.WriteCharacteristicRequest;
import com.fossil.blesdk.gattserver.request.WriteDescriptorRequest;
import com.fossil.blesdk.gattserver.service.GattService;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.eb0;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.sa0;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattServer {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public BluetoothGattServer a;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<GattService> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ CommandQueue d; // = new CommandQueue(this);
    @DexIgnore
    public /* final */ BluetoothGattServerCallback e; // = new c(this);
    @DexIgnore
    public /* final */ a f; // = new a();
    @DexIgnore
    public /* final */ GattService.a g; // = new d(this);
    @DexIgnore
    public /* final */ Context h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ sa0<GattOperationResult> a; // = new sa0<>();

        @DexIgnore
        public final sa0<GattOperationResult> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BluetoothGattServerCallback {
        @DexIgnore
        public /* final */ /* synthetic */ GattServer a;

        @DexIgnore
        public c(GattServer gattServer) {
            this.a = gattServer;
        }

        @DexIgnore
        public void onCharacteristicReadRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            Integer num;
            int i3;
            Object obj;
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            int i4 = i;
            int i5 = i2;
            BluetoothGattCharacteristic bluetoothGattCharacteristic2 = bluetoothGattCharacteristic;
            wd4.b(bluetoothDevice2, "device");
            wd4.b(bluetoothGattCharacteristic2, "characteristic");
            u90.c.a(GattServer.i, "onCharacteristicReadRequest: device=" + bluetoothDevice2 + ", " + "requestId=" + i4 + ", offset=" + i5 + ", characteristic=" + bluetoothGattCharacteristic.getUuid() + '.');
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.CHARACTERISTIC_READ.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            SdkLogEntry sdkLogEntry = r6;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(i)), JSONKey.OFFSET, Integer.valueOf(i2)), JSONKey.CHARACTERISTIC, bluetoothGattCharacteristic.getUuid().toString()), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            if ((bluetoothGattCharacteristic.getPermissions() | 1 | 2 | 4) != 0) {
                Iterator it = this.a.b.iterator();
                while (true) {
                    i3 = null;
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    GattService gattService = (GattService) obj;
                    wd4.a((Object) gattService, Constants.SERVICE);
                    UUID uuid2 = gattService.getUuid();
                    BluetoothGattService service = bluetoothGattCharacteristic.getService();
                    if (wd4.a((Object) uuid2, (Object) service != null ? service.getUuid() : null)) {
                        break;
                    }
                }
                GattService gattService2 = (GattService) obj;
                if (gattService2 == null) {
                    num = 257;
                } else {
                    if (!gattService2.a((ClientRequest) new ReadCharacteristicRequest(bluetoothDevice2, i4, i5, bluetoothGattCharacteristic2))) {
                        i3 = 6;
                    }
                    num = i3;
                }
            } else {
                num = 2;
            }
            if (num != null) {
                BluetoothGattServer a2 = this.a.a;
                if (a2 != null) {
                    a2.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
                }
            }
        }

        @DexIgnore
        public void onCharacteristicWriteRequest(BluetoothDevice bluetoothDevice, int i, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, boolean z2, int i2, byte[] bArr) {
            Integer num;
            Object obj;
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            boolean z3 = z2;
            byte[] bArr2 = bArr;
            wd4.b(bluetoothDevice2, "device");
            wd4.b(bluetoothGattCharacteristic, "characteristic");
            wd4.b(bArr2, "value");
            u90.c.a(GattServer.i, "onCharacteristicWriteRequest: device=" + bluetoothDevice2 + ", " + "requestId=" + i + ", characteristic=" + bluetoothGattCharacteristic.getUuid() + ", " + "preparedWrite=" + z + ", responseNeeded=" + z3 + ", " + "offset=" + i2 + ", value=" + l90.a(bArr2, (String) null, 1, (Object) null) + '.');
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.CHARACTERISTIC_WRITE.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(i)), JSONKey.CHARACTERISTIC, bluetoothGattCharacteristic.getUuid().toString()), JSONKey.PREPARED_WRITE, Boolean.valueOf(z)), JSONKey.RESPONSE_NEEDED, Boolean.valueOf(z2)), JSONKey.OFFSET, Integer.valueOf(i2)), JSONKey.VALUE, l90.a(bArr2, (String) null, 1, (Object) null)), 448, (rd4) null));
            if ((bluetoothGattCharacteristic.getPermissions() | 16 | 32 | 64 | 128 | 256) != 0) {
                Iterator it = this.a.b.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    GattService gattService = (GattService) obj;
                    wd4.a((Object) gattService, Constants.SERVICE);
                    UUID uuid2 = gattService.getUuid();
                    BluetoothGattService service = bluetoothGattCharacteristic.getService();
                    if (wd4.a((Object) uuid2, (Object) service != null ? service.getUuid() : null)) {
                        break;
                    }
                }
                GattService gattService2 = (GattService) obj;
                if (gattService2 == null) {
                    num = 257;
                } else {
                    num = !gattService2.a((ClientRequest) new WriteCharacteristicRequest(bluetoothDevice, i, bluetoothGattCharacteristic, z, z2, i2, bArr)) ? 6 : null;
                }
            } else {
                num = 3;
            }
            if (num != null && z3) {
                BluetoothGattServer a2 = this.a.a;
                if (a2 != null) {
                    a2.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
                }
            }
        }

        @DexIgnore
        public void onConnectionStateChange(BluetoothDevice bluetoothDevice, int i, int i2) {
            wd4.b(bluetoothDevice, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onConnectionStateChange: device=" + bluetoothDevice + ", " + "status=" + i + ", newState=" + i2 + '.');
            for (GattService a2 : this.a.b) {
                a2.a(bluetoothDevice, i, i2);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x00bc, code lost:
            if (r12 != null) goto L_0x00c1;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0132 A[EDGE_INSN: B:34:0x0132->B:21:0x0132 ?: BREAK  , SYNTHETIC] */
        public void onDescriptorReadRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattDescriptor bluetoothGattDescriptor) {
            Object obj;
            Integer num;
            int i3;
            Object obj2;
            UUID uuid;
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            int i4 = i;
            int i5 = i2;
            BluetoothGattDescriptor bluetoothGattDescriptor2 = bluetoothGattDescriptor;
            wd4.b(bluetoothDevice2, "device");
            wd4.b(bluetoothGattDescriptor2, "descriptor");
            u90 u90 = u90.c;
            String d = GattServer.i;
            StringBuilder sb = new StringBuilder();
            sb.append("onDescriptorReadRequest: device=");
            sb.append(bluetoothDevice2);
            sb.append(", ");
            sb.append("requestId=");
            sb.append(i4);
            sb.append(", offset=");
            sb.append(i5);
            sb.append(", ");
            sb.append("characteristic=");
            BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
            wd4.a((Object) characteristic, "descriptor.characteristic");
            sb.append(characteristic.getUuid());
            sb.append(", ");
            sb.append("descriptor=");
            sb.append(bluetoothGattDescriptor.getUuid());
            sb.append('.');
            u90.a(d, sb.toString());
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.DESCRIPTOR_READ.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid2 = UUID.randomUUID().toString();
            wd4.a((Object) uuid2, "UUID.randomUUID().toString()");
            JSONObject a2 = xa0.a(xa0.a(new JSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(i)), JSONKey.OFFSET, Integer.valueOf(i2));
            JSONKey jSONKey = JSONKey.CHARACTERISTIC;
            BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
            if (characteristic2 != null) {
                UUID uuid3 = characteristic2.getUuid();
                if (uuid3 != null) {
                    obj = uuid3.toString();
                }
            }
            obj = JSONObject.NULL;
            SdkLogEntry sdkLogEntry = r6;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid2, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(a2, jSONKey, obj), JSONKey.DESCRIPTOR, bluetoothGattDescriptor.getUuid().toString()), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            if ((bluetoothGattDescriptor.getPermissions() | 1 | 2 | 4) != 0) {
                Iterator it = this.a.b.iterator();
                while (true) {
                    i3 = null;
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    GattService gattService = (GattService) obj2;
                    wd4.a((Object) gattService, Constants.SERVICE);
                    UUID uuid4 = gattService.getUuid();
                    BluetoothGattCharacteristic characteristic3 = bluetoothGattDescriptor.getCharacteristic();
                    if (characteristic3 != null) {
                        BluetoothGattService service = characteristic3.getService();
                        if (service != null) {
                            uuid = service.getUuid();
                            if (wd4.a((Object) uuid4, (Object) uuid)) {
                                break;
                            }
                        }
                    }
                    uuid = null;
                    if (wd4.a((Object) uuid4, (Object) uuid)) {
                    }
                }
                GattService gattService2 = (GattService) obj2;
                if (gattService2 == null) {
                    num = 257;
                } else {
                    if (!gattService2.a((ClientRequest) new ReadDescriptorRequest(bluetoothDevice2, i4, i5, bluetoothGattDescriptor2))) {
                        i3 = 6;
                    }
                    num = i3;
                }
            } else {
                num = 2;
            }
            if (num != null) {
                BluetoothGattServer a3 = this.a.a;
                if (a3 != null) {
                    a3.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x00cf, code lost:
            if (r12 != null) goto L_0x00d4;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0174 A[EDGE_INSN: B:34:0x0174->B:21:0x0174 ?: BREAK  , SYNTHETIC] */
        public void onDescriptorWriteRequest(BluetoothDevice bluetoothDevice, int i, BluetoothGattDescriptor bluetoothGattDescriptor, boolean z, boolean z2, int i2, byte[] bArr) {
            Object obj;
            Integer num;
            Object obj2;
            UUID uuid;
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            boolean z3 = z2;
            byte[] bArr2 = bArr;
            wd4.b(bluetoothDevice2, "device");
            wd4.b(bluetoothGattDescriptor, "descriptor");
            wd4.b(bArr2, "value");
            u90 u90 = u90.c;
            String d = GattServer.i;
            StringBuilder sb = new StringBuilder();
            sb.append("onDescriptorWriteRequest: device=");
            sb.append(bluetoothDevice2);
            sb.append(", ");
            sb.append("requestId=");
            sb.append(i);
            sb.append(", characteristic=");
            BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
            wd4.a((Object) characteristic, "descriptor.characteristic");
            sb.append(characteristic.getUuid());
            sb.append(", ");
            sb.append("preparedWrite=");
            sb.append(z);
            sb.append(", responseNeeded=");
            sb.append(z3);
            sb.append(", ");
            sb.append("offset=");
            sb.append(i2);
            sb.append(", value=");
            sb.append(l90.a(bArr2, (String) null, 1, (Object) null));
            sb.append('.');
            u90.a(d, sb.toString());
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.DESCRIPTOR_WRITE.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid2 = UUID.randomUUID().toString();
            wd4.a((Object) uuid2, "UUID.randomUUID().toString()");
            JSONObject a2 = xa0.a(new JSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(i));
            JSONKey jSONKey = JSONKey.CHARACTERISTIC;
            BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
            if (characteristic2 != null) {
                UUID uuid3 = characteristic2.getUuid();
                if (uuid3 != null) {
                    obj = uuid3.toString();
                }
            }
            obj = JSONObject.NULL;
            ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid2, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(a2, jSONKey, obj), JSONKey.DESCRIPTOR, bluetoothGattDescriptor.getUuid().toString()), JSONKey.PREPARED_WRITE, Boolean.valueOf(z)), JSONKey.RESPONSE_NEEDED, Boolean.valueOf(z2)), JSONKey.OFFSET, Integer.valueOf(i2)), JSONKey.VALUE, l90.a(bArr2, (String) null, 1, (Object) null)), 448, (rd4) null));
            if ((bluetoothGattDescriptor.getPermissions() | 16 | 32 | 64 | 128 | 256) != 0) {
                Iterator it = this.a.b.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    GattService gattService = (GattService) obj2;
                    wd4.a((Object) gattService, Constants.SERVICE);
                    UUID uuid4 = gattService.getUuid();
                    BluetoothGattCharacteristic characteristic3 = bluetoothGattDescriptor.getCharacteristic();
                    if (characteristic3 != null) {
                        BluetoothGattService service = characteristic3.getService();
                        if (service != null) {
                            uuid = service.getUuid();
                            if (wd4.a((Object) uuid4, (Object) uuid)) {
                                break;
                            }
                        }
                    }
                    uuid = null;
                    if (wd4.a((Object) uuid4, (Object) uuid)) {
                    }
                }
                GattService gattService2 = (GattService) obj2;
                if (gattService2 == null) {
                    num = 257;
                } else {
                    num = !gattService2.a((ClientRequest) new WriteDescriptorRequest(bluetoothDevice, i, bluetoothGattDescriptor, z, z2, i2, bArr)) ? 6 : null;
                }
            } else {
                num = 3;
            }
            if (num != null && z3) {
                BluetoothGattServer a3 = this.a.a;
                if (a3 != null) {
                    a3.sendResponse(bluetoothDevice, i, num.intValue(), 0, (byte[]) null);
                }
            }
        }

        @DexIgnore
        public void onExecuteWrite(BluetoothDevice bluetoothDevice, int i, boolean z) {
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            wd4.b(bluetoothDevice2, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onExecuteWrite: device=" + bluetoothDevice2 + ", " + "requestId=" + i + ", execute=" + z + '.');
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.EXECUTE_WRITE.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(new JSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(i)), JSONKey.EXECUTE, Boolean.valueOf(z)), 448, (rd4) null));
            BluetoothGattServer a2 = this.a.a;
            if (a2 != null) {
                a2.sendResponse(bluetoothDevice, i, 257, 0, (byte[]) null);
            }
        }

        @DexIgnore
        public void onMtuChanged(BluetoothDevice bluetoothDevice, int i) {
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            int i2 = i;
            wd4.b(bluetoothDevice2, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onMtuChanged: device=" + bluetoothDevice2 + ", mtu=" + i2 + '.');
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.MTU_CHANGED.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String address = bluetoothDevice.getAddress();
            wd4.a((Object) address, "device.address");
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            SdkLogEntry sdkLogEntry = r3;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(new JSONObject(), JSONKey.EXCHANGED_MTU, Integer.valueOf(i)), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            for (GattService a2 : this.a.b) {
                a2.a(bluetoothDevice, i2);
            }
        }

        @DexIgnore
        public void onNotificationSent(BluetoothDevice bluetoothDevice, int i) {
            wd4.b(bluetoothDevice, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onNotificationSent: device=" + bluetoothDevice + ", " + "status=" + i + '.');
            this.a.f.a().c();
            this.a.f.a().a(new t90(GattOperationResult.GattResult.Companion.a(i), bluetoothDevice));
        }

        @DexIgnore
        public void onPhyRead(BluetoothDevice bluetoothDevice, int i, int i2, int i3) {
            wd4.b(bluetoothDevice, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onPhyRead: device=" + bluetoothDevice + ", " + "txPhy=" + i + ", rxPhy=" + i2 + ", status=" + i3 + '.');
        }

        @DexIgnore
        public void onPhyUpdate(BluetoothDevice bluetoothDevice, int i, int i2, int i3) {
            wd4.b(bluetoothDevice, "device");
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onPhyUpdate: device=" + bluetoothDevice + ", " + "txPhy=" + i + ", rxPhy=" + i2 + ", status=" + i3 + '.');
        }

        @DexIgnore
        public void onServiceAdded(int i, BluetoothGattService bluetoothGattService) {
            BluetoothGattService bluetoothGattService2 = bluetoothGattService;
            wd4.b(bluetoothGattService2, Constants.SERVICE);
            u90 u90 = u90.c;
            String d = GattServer.i;
            u90.a(d, "onServiceAdded: status=" + i + ", " + "service=" + bluetoothGattService.getUuid() + '.');
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.SERVICE_ADDED.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            SdkLogEntry sdkLogEntry = r3;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, "", "", uuid, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(new JSONObject(), JSONKey.STATUS, Integer.valueOf(i)), JSONKey.SERVICE, bluetoothGattService.getUuid().toString()), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            for (GattService a2 : this.a.b) {
                a2.a(i, bluetoothGattService2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements GattService.a {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ a b;
        @DexIgnore
        public /* final */ /* synthetic */ GattServer c;

        @DexIgnore
        public d(GattServer gattServer) {
            this.c = gattServer;
            this.a = gattServer.h;
            this.b = gattServer.f;
        }

        @DexIgnore
        public a a() {
            return this.b;
        }

        @DexIgnore
        public Context getContext() {
            return this.a;
        }

        @DexIgnore
        public void a(Command command) {
            wd4.b(command, Constants.COMMAND);
            this.c.a(command);
        }
    }

    /*
    static {
        new b((rd4) null);
        String simpleName = GattServer.class.getSimpleName();
        wd4.a((Object) simpleName, "GattServer::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public GattServer(Context context) {
        wd4.b(context, "context");
        this.h = context;
    }

    @DexIgnore
    public final boolean b() {
        if (this.c) {
            return true;
        }
        return eb0.d.a(new GattServer$start$Anon1(this));
    }

    @DexIgnore
    public final void c() {
        BluetoothGattServer bluetoothGattServer = this.a;
        if (bluetoothGattServer != null) {
            for (GattService f2 : this.b) {
                f2.f();
            }
            this.b.clear();
            bluetoothGattServer.close();
            this.a = null;
            this.c = false;
            u90.c.a(i, "GattServer stopped.");
            ea0 ea0 = ea0.l;
            String logName$blesdk_productionRelease = GattServerEventName.GATT_SERVER_STOPPED.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.GATT_SERVER_EVENT;
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, "", "", uuid, this.c, (String) null, (DeviceInformation) null, (fa0) null, (JSONObject) null, 960, (rd4) null));
        }
    }

    @DexIgnore
    public final GattService.a a() {
        return this.g;
    }

    @DexIgnore
    public final boolean a(BluetoothDevice bluetoothDevice, int i2, int i3, int i4, byte[] bArr) {
        wd4.b(bluetoothDevice, "device");
        BluetoothGattServer bluetoothGattServer = this.a;
        if (bluetoothGattServer != null) {
            return bluetoothGattServer.sendResponse(bluetoothDevice, i2, i3, i4, bArr);
        }
        return false;
    }

    @DexIgnore
    public final boolean a(BluetoothDevice bluetoothDevice, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z) {
        wd4.b(bluetoothDevice, "device");
        wd4.b(bluetoothGattCharacteristic, "characteristic");
        BluetoothGattServer bluetoothGattServer = this.a;
        if (bluetoothGattServer != null) {
            return bluetoothGattServer.notifyCharacteristicChanged(bluetoothDevice, bluetoothGattCharacteristic, z);
        }
        return false;
    }

    @DexIgnore
    public final boolean a(Command command) {
        wd4.b(command, Constants.COMMAND);
        if (this.a == null) {
            return false;
        }
        this.d.a(command);
        return true;
    }

    @DexIgnore
    public final boolean a(GattService gattService) {
        wd4.b(gattService, Constants.SERVICE);
        if (!this.c) {
            return this.b.add(gattService);
        }
        BluetoothGattServer bluetoothGattServer = this.a;
        if (bluetoothGattServer == null || !bluetoothGattServer.addService(gattService)) {
            return false;
        }
        return this.b.add(gattService);
    }
}
