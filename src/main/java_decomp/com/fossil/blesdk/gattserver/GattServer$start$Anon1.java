package com.fossil.blesdk.gattserver;

import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import com.facebook.places.PlaceManager;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.gattserver.log.GattServerEventName;
import com.fossil.blesdk.gattserver.service.GattService;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;
import java.util.UUID;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Lambda;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattServer$start$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GattServer this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements jd4<BluetoothGattService, String> {
        @DexIgnore
        public static /* final */ Anon2 INSTANCE; // = new Anon2();

        @DexIgnore
        public Anon2() {
            super(1);
        }

        @DexIgnore
        public final String invoke(BluetoothGattService bluetoothGattService) {
            wd4.a((Object) bluetoothGattService, Constants.SERVICE);
            String uuid = bluetoothGattService.getUuid().toString();
            wd4.a((Object) uuid, "service.uuid.toString()");
            return uuid;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 extends Lambda implements jd4<BluetoothGattService, String> {
        @DexIgnore
        public static /* final */ Anon3 INSTANCE; // = new Anon3();

        @DexIgnore
        public Anon3() {
            super(1);
        }

        @DexIgnore
        public final String invoke(BluetoothGattService bluetoothGattService) {
            wd4.a((Object) bluetoothGattService, Constants.SERVICE);
            String uuid = bluetoothGattService.getUuid().toString();
            wd4.a((Object) uuid, "service.uuid.toString()");
            return uuid;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattServer$start$Anon1(GattServer gattServer) {
        super(0);
        this.this$Anon0 = gattServer;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f0, code lost:
        if (r6 != null) goto L_0x00f5;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00d6  */
    public final void invoke() {
        String str;
        BluetoothGattServer a;
        Object obj;
        Object systemService = this.this$Anon0.h.getSystemService(PlaceManager.PARAM_BLUETOOTH);
        if (systemService != null) {
            GattServer gattServer = this.this$Anon0;
            gattServer.a = ((BluetoothManager) systemService).openGattServer(gattServer.h, this.this$Anon0.e);
            for (GattService gattService : this.this$Anon0.b) {
                BluetoothGattServer a2 = this.this$Anon0.a;
                if (a2 != null) {
                    a2.addService(gattService);
                }
            }
            GattServer gattServer2 = this.this$Anon0;
            gattServer2.c = gattServer2.a != null;
            u90 u90 = u90.c;
            String d = GattServer.i;
            StringBuilder sb = new StringBuilder();
            sb.append("GattServer started: success=");
            sb.append(this.this$Anon0.c);
            sb.append(", ");
            sb.append("services=");
            BluetoothGattServer a3 = this.this$Anon0.a;
            if (a3 != null) {
                List<BluetoothGattService> services = a3.getServices();
                if (services != null) {
                    str = wb4.a(services, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, Anon2.INSTANCE, 31, (Object) null);
                    sb.append(str);
                    sb.append('.');
                    u90.a(d, sb.toString());
                    ea0 ea0 = ea0.l;
                    String logName$blesdk_productionRelease = GattServerEventName.GATT_SERVER_STARTED.getLogName$blesdk_productionRelease();
                    EventType eventType = EventType.GATT_SERVER_EVENT;
                    String uuid = UUID.randomUUID().toString();
                    wd4.a((Object) uuid, "UUID.randomUUID().toString()");
                    boolean f = this.this$Anon0.c;
                    JSONObject jSONObject = new JSONObject();
                    JSONKey jSONKey = JSONKey.SERVICES;
                    a = this.this$Anon0.a;
                    if (a != null) {
                        List<BluetoothGattService> services2 = a.getServices();
                        if (services2 != null) {
                            obj = wb4.a(services2, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, Anon3.INSTANCE, 31, (Object) null);
                        }
                    }
                    obj = JSONObject.NULL;
                    ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, "", "", uuid, f, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(jSONObject, jSONKey, obj), 448, (rd4) null));
                    return;
                }
            }
            str = null;
            sb.append(str);
            sb.append('.');
            u90.a(d, sb.toString());
            ea0 ea02 = ea0.l;
            String logName$blesdk_productionRelease2 = GattServerEventName.GATT_SERVER_STARTED.getLogName$blesdk_productionRelease();
            EventType eventType2 = EventType.GATT_SERVER_EVENT;
            String uuid2 = UUID.randomUUID().toString();
            wd4.a((Object) uuid2, "UUID.randomUUID().toString()");
            boolean f2 = this.this$Anon0.c;
            JSONObject jSONObject2 = new JSONObject();
            JSONKey jSONKey2 = JSONKey.SERVICES;
            a = this.this$Anon0.a;
            if (a != null) {
            }
            obj = JSONObject.NULL;
            ea02.b(new SdkLogEntry(logName$blesdk_productionRelease2, eventType2, "", "", uuid2, f2, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(jSONObject2, jSONKey2, obj), 448, (rd4) null));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.bluetooth.BluetoothManager");
    }
}
