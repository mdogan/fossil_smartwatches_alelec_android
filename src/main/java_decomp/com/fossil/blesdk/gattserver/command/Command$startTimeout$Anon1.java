package com.fossil.blesdk.gattserver.command;

import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Command$startTimeout$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Command this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Command$startTimeout$Anon1(Command command) {
        super(0);
        this.this$Anon0 = command;
    }

    @DexIgnore
    public final void invoke() {
        this.this$Anon0.a(Command.Result.ResultCode.TIMEOUT);
    }
}
