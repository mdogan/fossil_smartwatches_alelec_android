package com.fossil.blesdk.gattserver.command;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommandQueue$execute$Anon1 extends Lambda implements jd4<Command, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ CommandQueue this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommandQueue$execute$Anon1(CommandQueue commandQueue) {
        super(1);
        this.this$Anon0 = commandQueue;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Command) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Command command) {
        wd4.b(command, "it");
        this.this$Anon0.a();
    }
}
