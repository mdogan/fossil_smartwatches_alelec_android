package com.fossil.blesdk.supportedclass;

import com.fossil.blesdk.obfuscated.wd4;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PriorityQueue<T> extends CopyOnWriteArrayList<T> {
    @DexIgnore
    public /* final */ Comparator<T> comparator;

    @DexIgnore
    public PriorityQueue(Comparator<T> comparator2) {
        wd4.b(comparator2, "comparator");
        this.comparator = comparator2;
    }

    @DexIgnore
    public final int a(T t) {
        synchronized (this) {
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.comparator.compare(t, get(i)) > 0) {
                    return i;
                }
            }
            int size2 = size();
            return size2;
        }
    }

    @DexIgnore
    public boolean add(T t) {
        synchronized (this) {
            add(a(t), t);
        }
        return true;
    }

    @DexIgnore
    public final Comparator<T> getComparator$blesdk_productionRelease() {
        return this.comparator;
    }

    @DexIgnore
    public /* bridge */ int getSize() {
        return super.size();
    }

    @DexIgnore
    public final /* bridge */ T remove(int i) {
        return removeAt(i);
    }

    @DexIgnore
    public /* bridge */ Object removeAt(int i) {
        return super.remove(i);
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }
}
