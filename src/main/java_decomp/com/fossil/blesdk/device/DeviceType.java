package com.fossil.blesdk.device;

import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.z00;
import java.lang.reflect.Type;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceType {
    UNKNOWN(z00.f.h()),
    DIANA(z00.f.b()),
    SE1(z00.f.f()),
    SLIM(z00.f.g()),
    MINI(z00.f.e()),
    HELLAS(z00.f.c());
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ Type[] supportedFeature;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DeviceType a(String str) {
            wd4.b(str, "modelNumber");
            if (cg4.c(str, "DN", false, 2, (Object) null)) {
                return DeviceType.DIANA;
            }
            if (cg4.c(str, "HW", false, 2, (Object) null)) {
                return DeviceType.SE1;
            }
            if (cg4.c(str, "HL", false, 2, (Object) null)) {
                return DeviceType.SLIM;
            }
            if (cg4.c(str, "HM", false, 2, (Object) null)) {
                return DeviceType.MINI;
            }
            if (cg4.c(str, "AW", false, 2, (Object) null)) {
                return DeviceType.HELLAS;
            }
            return DeviceType.UNKNOWN;
        }

        @DexIgnore
        public final DeviceType b(String str) {
            wd4.b(str, "serialNumber");
            if (!DeviceInformation.Companion.a(str)) {
                return DeviceType.UNKNOWN;
            }
            if (cg4.c(str, "D", false, 2, (Object) null)) {
                return DeviceType.DIANA;
            }
            if (cg4.c(str, "W", false, 2, (Object) null)) {
                return DeviceType.SE1;
            }
            if (cg4.c(str, "L", false, 2, (Object) null)) {
                return DeviceType.SLIM;
            }
            if (cg4.c(str, "M", false, 2, (Object) null)) {
                return DeviceType.MINI;
            }
            if (cg4.c(str, "Y", false, 2, (Object) null)) {
                return DeviceType.HELLAS;
            }
            return DeviceType.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    DeviceType(Type[] typeArr) {
        this.supportedFeature = typeArr;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final Type[] getSupportedFeature$blesdk_productionRelease() {
        return this.supportedFeature;
    }
}
