package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeTravelMicroAppRequest extends MicroAppRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MicroAppEvent commuteTimeTravelMicroAppEvent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeTravelMicroAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppRequest createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeTravelMicroAppRequest(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppRequest[] newArray(int i) {
            return new CommuteTimeTravelMicroAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeTravelMicroAppRequest(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final MicroAppEvent getCommuteTimeTravelMicroAppEvent() {
        return this.commuteTimeTravelMicroAppEvent;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppRequest(byte b, MicroAppEvent microAppEvent) {
        super(DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP, b, microAppEvent);
        wd4.b(microAppEvent, "commuteTimeTravelMicroAppEvent");
        this.commuteTimeTravelMicroAppEvent = microAppEvent;
    }

    @DexIgnore
    public CommuteTimeTravelMicroAppRequest(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(MicroAppEvent.class.getClassLoader());
        if (readParcelable != null) {
            this.commuteTimeTravelMicroAppEvent = (MicroAppEvent) readParcelable;
        } else {
            wd4.a();
            throw null;
        }
    }
}
