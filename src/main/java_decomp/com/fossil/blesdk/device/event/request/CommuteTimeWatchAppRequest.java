package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ String destination;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeWatchAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchAppRequest createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeWatchAppRequest(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeWatchAppRequest[] newArray(int i) {
            return new CommuteTimeWatchAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeWatchAppRequest(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wd4.a((Object) CommuteTimeWatchAppRequest.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(wd4.a((Object) this.destination, (Object) ((CommuteTimeWatchAppRequest) obj).destination) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest");
    }

    @DexIgnore
    public final String getDestination() {
        return this.destination;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.destination.hashCode();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppRequest(byte b, int i, String str) {
        super(DeviceEventId.COMMUTE_TIME_WATCH_APP, b, i);
        wd4.b(str, ShareConstants.DESTINATION);
        this.destination = str;
    }

    @DexIgnore
    public CommuteTimeWatchAppRequest(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.destination = readString;
        } else {
            wd4.a();
            throw null;
        }
    }
}
