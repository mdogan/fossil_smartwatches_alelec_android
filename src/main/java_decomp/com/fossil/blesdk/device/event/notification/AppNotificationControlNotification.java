package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotificationControlNotification extends DeviceNotification {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ AppNotificationControlAction action;
    @DexIgnore
    public /* final */ int notificationUid;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AppNotificationControlNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public AppNotificationControlNotification createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new AppNotificationControlNotification(parcel, (rd4) null);
        }

        @DexIgnore
        public AppNotificationControlNotification[] newArray(int i) {
            return new AppNotificationControlNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationControlNotification(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wd4.a((Object) AppNotificationControlNotification.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            AppNotificationControlNotification appNotificationControlNotification = (AppNotificationControlNotification) obj;
            return this.action == appNotificationControlNotification.action && this.notificationUid == appNotificationControlNotification.notificationUid;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.AppNotificationControlNotification");
    }

    @DexIgnore
    public final AppNotificationControlAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.notificationUid;
    }

    @DexIgnore
    public int hashCode() {
        return (((super.hashCode() * 31) + this.action.hashCode()) * 31) + Integer.valueOf(this.notificationUid).hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(super.toJSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease()), JSONKey.NOTIFICATION_UID, Integer.valueOf(this.notificationUid));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.notificationUid);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppNotificationControlNotification(byte b, int i, AppNotificationControlAction appNotificationControlAction) {
        super(DeviceEventId.APP_NOTIFICATION_CONTROL, b);
        wd4.b(appNotificationControlAction, "action");
        this.action = appNotificationControlAction;
        this.notificationUid = i;
    }

    @DexIgnore
    public AppNotificationControlNotification(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.action = AppNotificationControlAction.valueOf(readString);
            this.notificationUid = parcel.readInt();
            return;
        }
        wd4.a();
        throw null;
    }
}
