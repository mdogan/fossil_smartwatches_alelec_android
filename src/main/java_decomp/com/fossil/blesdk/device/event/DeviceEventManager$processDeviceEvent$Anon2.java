package com.fossil.blesdk.device.event;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent;
import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction;
import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.event.notification.AlarmSyncNotification;
import com.fossil.blesdk.device.event.notification.DeviceConfigSyncNotification;
import com.fossil.blesdk.device.event.notification.NotificationFilterSyncNotification;
import com.fossil.blesdk.obfuscated.c30;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceEventManager$processDeviceEvent$Anon2 extends Lambda implements jd4<cb4, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation $device;
    @DexIgnore
    public /* final */ /* synthetic */ AsyncEvent $event;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceEventManager$processDeviceEvent$Anon2(AsyncEvent asyncEvent, DeviceImplementation deviceImplementation) {
        super(1);
        this.$event = asyncEvent;
        this.$device = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cb4) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(cb4 cb4) {
        wd4.b(cb4, "it");
        for (BackgroundSyncFrame backgroundSyncFrame : ((BackgroundSyncEvent) this.$event).getBackgroundSyncFrames()) {
            FileType a = FileType.Companion.a(backgroundSyncFrame.getFileHandle());
            if (a != null) {
                switch (c30.a[a.ordinal()]) {
                    case 1:
                        if (backgroundSyncFrame.getAction() != BackgroundSyncAction.GET) {
                            break;
                        } else {
                            DeviceImplementation.b(this.$device, false, true, (Priority) null, 4, (Object) null);
                            break;
                        }
                    case 2:
                        this.$device.a((DeviceEvent) new AlarmSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 3:
                        if (backgroundSyncFrame.getAction() != BackgroundSyncAction.GET) {
                            break;
                        } else {
                            this.$device.y();
                            break;
                        }
                    case 4:
                        this.$device.a((DeviceEvent) new NotificationFilterSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 5:
                        this.$device.a((DeviceEvent) new DeviceConfigSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 6:
                        if (backgroundSyncFrame.getAction() != BackgroundSyncAction.GET) {
                            break;
                        } else {
                            DeviceImplementation.a(this.$device, false, true, (Priority) null, 4, (Object) null);
                            break;
                        }
                }
            }
        }
    }
}
