package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.enumerate.CommuteTimeWatchAppAction;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppNotification extends DeviceNotification {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ CommuteTimeWatchAppAction action;
    @DexIgnore
    public /* final */ String destination;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeWatchAppNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchAppNotification createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeWatchAppNotification(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeWatchAppNotification[] newArray(int i) {
            return new CommuteTimeWatchAppNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeWatchAppNotification(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final CommuteTimeWatchAppAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final String getDestination() {
        return this.destination;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.destination);
        }
        if (parcel != null) {
            parcel.writeInt(this.action.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppNotification(byte b, String str, CommuteTimeWatchAppAction commuteTimeWatchAppAction) {
        super(DeviceEventId.COMMUTE_TIME_WATCH_APP, b);
        wd4.b(str, ShareConstants.DESTINATION);
        wd4.b(commuteTimeWatchAppAction, "action");
        this.destination = str;
        this.action = commuteTimeWatchAppAction;
    }

    @DexIgnore
    public CommuteTimeWatchAppNotification(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.destination = readString;
            this.action = CommuteTimeWatchAppAction.values()[parcel.readInt()];
            return;
        }
        wd4.a();
        throw null;
    }
}
