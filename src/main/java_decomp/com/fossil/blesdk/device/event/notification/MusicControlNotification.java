package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.music.MusicAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlNotification extends DeviceNotification {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MusicAction action;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MusicControlNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MusicControlNotification createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MusicControlNotification(parcel, (rd4) null);
        }

        @DexIgnore
        public MusicControlNotification[] newArray(int i) {
            return new MusicControlNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MusicControlNotification(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MusicControlNotification.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.action == ((MusicControlNotification) obj).action;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.MusicControlNotification");
    }

    @DexIgnore
    public final MusicAction getAction() {
        return this.action;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.action.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(super.toJSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlNotification(byte b, MusicAction musicAction) {
        super(DeviceEventId.MUSIC_CONTROL, b);
        wd4.b(musicAction, "action");
        this.action = musicAction;
    }

    @DexIgnore
    public MusicControlNotification(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.action = MusicAction.valueOf(readString);
        } else {
            wd4.a();
            throw null;
        }
    }
}
