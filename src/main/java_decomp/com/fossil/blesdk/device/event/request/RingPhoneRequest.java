package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.enumerate.RingPhoneAction;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingPhoneRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ RingPhoneAction action;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<RingPhoneRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public RingPhoneRequest createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new RingPhoneRequest(parcel, (rd4) null);
        }

        @DexIgnore
        public RingPhoneRequest[] newArray(int i) {
            return new RingPhoneRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ RingPhoneRequest(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wd4.a((Object) RingPhoneRequest.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.action == ((RingPhoneRequest) obj).action;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.RingPhoneRequest");
    }

    @DexIgnore
    public final RingPhoneAction getAction() {
        return this.action;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.action.hashCode();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneRequest(byte b, int i, RingPhoneAction ringPhoneAction) {
        super(DeviceEventId.RING_PHONE, b, i);
        wd4.b(ringPhoneAction, "action");
        this.action = ringPhoneAction;
    }

    @DexIgnore
    public RingPhoneRequest(Parcel parcel) {
        super(parcel);
        this.action = RingPhoneAction.values()[parcel.readInt()];
    }
}
