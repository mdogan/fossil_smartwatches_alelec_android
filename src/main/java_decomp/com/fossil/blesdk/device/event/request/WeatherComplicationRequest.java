package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherComplicationRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherComplicationRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherComplicationRequest createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherComplicationRequest(parcel, (rd4) null);
        }

        @DexIgnore
        public WeatherComplicationRequest[] newArray(int i) {
            return new WeatherComplicationRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherComplicationRequest(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public WeatherComplicationRequest(byte b, int i) {
        super(DeviceEventId.WEATHER_COMPLICATION, b, i);
    }

    @DexIgnore
    public WeatherComplicationRequest(Parcel parcel) {
        super(parcel);
    }
}
