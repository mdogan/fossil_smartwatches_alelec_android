package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.SynchronizationAction;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationFilterSyncNotification extends DeviceNotification {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ SynchronizationAction action;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationFilterSyncNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public NotificationFilterSyncNotification createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new NotificationFilterSyncNotification(parcel, (rd4) null);
        }

        @DexIgnore
        public NotificationFilterSyncNotification[] newArray(int i) {
            return new NotificationFilterSyncNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationFilterSyncNotification(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final SynchronizationAction getAction() {
        return this.action;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.action.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationFilterSyncNotification(byte b, BackgroundSyncAction backgroundSyncAction) {
        super(DeviceEventId.NOTIFICATION_FILTER_SYNC, b);
        wd4.b(backgroundSyncAction, "backgroundSyncAction");
        this.action = SynchronizationAction.Companion.a(backgroundSyncAction);
    }

    @DexIgnore
    public NotificationFilterSyncNotification(Parcel parcel) {
        super(parcel);
        this.action = SynchronizationAction.values()[parcel.readInt()];
    }
}
