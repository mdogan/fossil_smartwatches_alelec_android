package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainComplicationRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ChanceOfRainComplicationRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ChanceOfRainComplicationRequest createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ChanceOfRainComplicationRequest(parcel, (rd4) null);
        }

        @DexIgnore
        public ChanceOfRainComplicationRequest[] newArray(int i) {
            return new ChanceOfRainComplicationRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ChanceOfRainComplicationRequest(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public ChanceOfRainComplicationRequest(byte b, int i) {
        super(DeviceEventId.CHANCE_OF_RAIN_COMPLICATION, b, i);
    }

    @DexIgnore
    public ChanceOfRainComplicationRequest(Parcel parcel) {
        super(parcel);
    }
}
