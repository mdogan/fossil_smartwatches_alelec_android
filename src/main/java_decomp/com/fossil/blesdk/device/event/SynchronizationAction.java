package com.fossil.blesdk.device.event;

import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction;
import com.fossil.blesdk.obfuscated.f30;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SynchronizationAction {
    GET,
    SET;
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SynchronizationAction a(BackgroundSyncAction backgroundSyncAction) {
            wd4.b(backgroundSyncAction, "backgroundSyncAction");
            int i = f30.a[backgroundSyncAction.ordinal()];
            if (i == 1) {
                return SynchronizationAction.GET;
            }
            if (i == 2) {
                return SynchronizationAction.SET;
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
