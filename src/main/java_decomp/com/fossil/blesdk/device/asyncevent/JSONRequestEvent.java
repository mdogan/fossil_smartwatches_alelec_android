package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class JSONRequestEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ int requestId;
    @DexIgnore
    public /* final */ JSONObject requestedApps;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<JSONRequestEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public JSONRequestEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new JSONRequestEvent(parcel, (rd4) null);
        }

        @DexIgnore
        public JSONRequestEvent[] newArray(int i) {
            return new JSONRequestEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ JSONRequestEvent(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) JSONRequestEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            JSONRequestEvent jSONRequestEvent = (JSONRequestEvent) obj;
            return getEventType$blesdk_productionRelease() == jSONRequestEvent.getEventType$blesdk_productionRelease() && getEventSequence$blesdk_productionRelease() == jSONRequestEvent.getEventSequence$blesdk_productionRelease() && this.requestId == jSONRequestEvent.requestId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public final int getRequestId$blesdk_productionRelease() {
        return this.requestId;
    }

    @DexIgnore
    public final JSONObject getRequestedApps$blesdk_productionRelease() {
        return this.requestedApps;
    }

    @DexIgnore
    public int hashCode() {
        return (((getEventType$blesdk_productionRelease().hashCode() * 31) + getEventSequence$blesdk_productionRelease()) * 31) + this.requestId;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(super.toJSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(this.requestId)), JSONKey.REQUEST_DATA, this.requestedApps);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.requestId);
        }
        if (parcel != null) {
            parcel.writeString(this.requestedApps.toString());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public JSONRequestEvent(byte b, int i, JSONObject jSONObject) {
        super(AsyncEventType.JSON_FILE_EVENT, b);
        wd4.b(jSONObject, "requestedApps");
        this.requestId = i;
        this.requestedApps = jSONObject;
    }

    @DexIgnore
    public JSONRequestEvent(Parcel parcel) {
        super(parcel);
        this.requestId = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.requestedApps = new JSONObject(readString);
        } else {
            wd4.a();
            throw null;
        }
    }
}
