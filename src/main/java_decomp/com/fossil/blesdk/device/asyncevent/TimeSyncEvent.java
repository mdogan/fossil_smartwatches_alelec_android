package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeSyncEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ int DATA_LENGTH; // = 8;
    @DexIgnore
    public TimeData timeData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<TimeSyncEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public TimeSyncEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new TimeSyncEvent(parcel, (rd4) null);
        }

        @DexIgnore
        public TimeSyncEvent[] newArray(int i) {
            return new TimeSyncEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ TimeSyncEvent(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getEventResponseData() {
        this.timeData = new TimeData(System.currentTimeMillis());
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        TimeData timeData2 = this.timeData;
        if (timeData2 != null) {
            ByteBuffer putInt = order.putInt((int) timeData2.getTimestampInSecond());
            TimeData timeData3 = this.timeData;
            if (timeData3 != null) {
                ByteBuffer putShort = putInt.putShort((short) ((int) timeData3.getPartialSecond()));
                TimeData timeData4 = this.timeData;
                if (timeData4 != null) {
                    byte[] array = putShort.putShort((short) timeData4.getTimeZoneOffsetInMinute()).array();
                    wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
                    return array;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        JSONKey jSONKey = JSONKey.VALUE;
        TimeData timeData2 = this.timeData;
        return xa0.a(jSONObject, jSONKey, timeData2 != null ? timeData2.toJSONObject() : null);
    }

    @DexIgnore
    public TimeSyncEvent(byte b) {
        super(AsyncEventType.TIME_SYNC_EVENT, b);
    }

    @DexIgnore
    public TimeSyncEvent(Parcel parcel) {
        super(parcel);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class TimeData extends JSONAbleObject implements Parcelable {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a((rd4) null);
        @DexIgnore
        public /* final */ long partialSecond;
        @DexIgnore
        public /* final */ int timeZoneOffsetInMinute;
        @DexIgnore
        public /* final */ long timestamp;
        @DexIgnore
        public /* final */ long timestampInSecond;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<TimeData> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(rd4 rd4) {
                this();
            }

            @DexIgnore
            public TimeData createFromParcel(Parcel parcel) {
                wd4.b(parcel, "parcel");
                return new TimeData(parcel);
            }

            @DexIgnore
            public TimeData[] newArray(int i) {
                return new TimeData[i];
            }
        }

        @DexIgnore
        public TimeData(long j) {
            this.timestamp = j;
            long j2 = this.timestamp;
            long j3 = (long) 1000;
            this.timestampInSecond = j2 / j3;
            this.partialSecond = j2 - (this.timestampInSecond * j3);
            this.timeZoneOffsetInMinute = (TimeZone.getDefault().getOffset(this.timestamp) / 1000) / 60;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public final long getPartialSecond() {
            return this.partialSecond;
        }

        @DexIgnore
        public final int getTimeZoneOffsetInMinute() {
            return this.timeZoneOffsetInMinute;
        }

        @DexIgnore
        public final long getTimestamp() {
            return this.timestamp;
        }

        @DexIgnore
        public final long getTimestampInSecond() {
            return this.timestampInSecond;
        }

        @DexIgnore
        public JSONObject toJSONObject() {
            return xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.SECOND, Long.valueOf(this.timestampInSecond)), JSONKey.MILLISECOND, Long.valueOf(this.partialSecond)), JSONKey.TIMEZONE_OFFSET_IN_MINUTE, Integer.valueOf(this.timeZoneOffsetInMinute));
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            wd4.b(parcel, "parcel");
            parcel.writeLong(this.timestamp);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public TimeData(Parcel parcel) {
            this(parcel.readLong());
            wd4.b(parcel, "parcel");
        }
    }
}
