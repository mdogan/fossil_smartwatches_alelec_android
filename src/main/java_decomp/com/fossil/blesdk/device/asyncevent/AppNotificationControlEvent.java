package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotificationControlEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ int NOTIFICATION_EVENT_ACTION_INDEX; // = 4;
    @DexIgnore
    public static /* final */ int NOTIFICATION_EVENT_UID_INDEX; // = 0;
    @DexIgnore
    public /* final */ AppNotificationControlAction action;
    @DexIgnore
    public /* final */ int notificationUid;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AppNotificationControlEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public AppNotificationControlEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new AppNotificationControlEvent(parcel, (rd4) null);
        }

        @DexIgnore
        public AppNotificationControlEvent[] newArray(int i) {
            return new AppNotificationControlEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationControlEvent(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final AppNotificationControlAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.notificationUid;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(super.toJSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease()), JSONKey.NOTIFICATION_UID, Integer.valueOf(this.notificationUid));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.notificationUid);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppNotificationControlEvent(byte b, int i, AppNotificationControlAction appNotificationControlAction) {
        super(AsyncEventType.APP_NOTIFICATION_EVENT, b);
        wd4.b(appNotificationControlAction, "action");
        this.action = appNotificationControlAction;
        this.notificationUid = i;
    }

    @DexIgnore
    public AppNotificationControlEvent(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.action = AppNotificationControlAction.valueOf(readString);
            this.notificationUid = parcel.readInt();
            return;
        }
        wd4.a();
        throw null;
    }
}
