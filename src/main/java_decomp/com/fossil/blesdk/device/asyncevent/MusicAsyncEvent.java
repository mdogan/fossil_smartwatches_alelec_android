package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.music.MusicAction;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicAsyncEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MusicAction action;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MusicAsyncEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MusicAsyncEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MusicAsyncEvent(parcel, (rd4) null);
        }

        @DexIgnore
        public MusicAsyncEvent[] newArray(int i) {
            return new MusicAsyncEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MusicAsyncEvent(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final MusicAction getAction() {
        return this.action;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicAsyncEvent(byte b, MusicAction musicAction) {
        super(AsyncEventType.MUSIC_EVENT, b);
        wd4.b(musicAction, "action");
        this.action = musicAction;
    }

    @DexIgnore
    public MusicAsyncEvent(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.action = MusicAction.valueOf(readString);
        } else {
            wd4.a();
            throw null;
        }
    }
}
