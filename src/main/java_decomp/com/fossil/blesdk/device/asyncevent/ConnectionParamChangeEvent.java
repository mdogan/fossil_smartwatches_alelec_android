package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionParamChangeEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ConnectionParamChangeEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ConnectionParamChangeEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ConnectionParamChangeEvent(parcel, (rd4) null);
        }

        @DexIgnore
        public ConnectionParamChangeEvent[] newArray(int i) {
            return new ConnectionParamChangeEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ConnectionParamChangeEvent(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public ConnectionParamChangeEvent(byte b) {
        super(AsyncEventType.CONNECTION_PARAM_CHANGE_EVENT, b);
    }

    @DexIgnore
    public ConnectionParamChangeEvent(Parcel parcel) {
        super(parcel);
    }
}
