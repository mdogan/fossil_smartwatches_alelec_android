package com.fossil.blesdk.device;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FeatureError extends Error {
    @DexIgnore
    public static /* final */ a Companion; // = new a((rd4) null);
    @DexIgnore
    public /* final */ FeatureErrorCode errorCode;
    @DexIgnore
    public /* final */ Phase.Result phaseResult;

    @DexIgnore
    public enum PhaseResultToFeatureErrorConversionOption {
        HAS_SERVICE_CHANGED
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final FeatureError a(Phase.Result result, HashMap<PhaseResultToFeatureErrorConversionOption, Object> hashMap) {
            wd4.b(result, "phaseResult");
            wd4.b(hashMap, "options");
            return new FeatureError(FeatureErrorCode.Companion.a(result, hashMap), result);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FeatureError(FeatureErrorCode featureErrorCode, Phase.Result result, int i, rd4 rd4) {
        this(featureErrorCode, (i & 2) != 0 ? new Phase.Result((PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (rd4) null) : result);
    }

    @DexIgnore
    public final Phase.Result getPhaseResult$blesdk_productionRelease() {
        return this.phaseResult;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        if (this.phaseResult.getResultCode() != Phase.Result.ResultCode.SUCCESS) {
            xa0.a(jSONObject, JSONKey.PHASE_RESULT, this.phaseResult.toJSONObject());
        }
        return jSONObject;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FeatureError(FeatureErrorCode featureErrorCode, Phase.Result result) {
        super(featureErrorCode);
        wd4.b(featureErrorCode, "errorCode");
        wd4.b(result, "phaseResult");
        this.errorCode = featureErrorCode;
        this.phaseResult = result;
    }

    @DexIgnore
    public FeatureErrorCode getErrorCode() {
        return this.errorCode;
    }
}
