package com.fossil.blesdk.device;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31 extends Lambda implements kd4<Phase, Float, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ g90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31 e;
        @DexIgnore
        public /* final */ /* synthetic */ Phase f;
        @DexIgnore
        public /* final */ /* synthetic */ float g;

        @DexIgnore
        public a(DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31 deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31, Phase phase, float f2) {
            this.e = deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31;
            this.f = phase;
            this.g = f2;
        }

        @DexIgnore
        public final void run() {
            DeviceImplementation deviceImplementation = this.e.this$Anon0;
            LogLevel logLevel = LogLevel.DEBUG;
            String logName$blesdk_productionRelease = this.f.g().getLogName$blesdk_productionRelease();
            DeviceImplementation.a(deviceImplementation, logLevel, logName$blesdk_productionRelease, "Progress: " + this.g, false, 8, (Object) null);
            this.e.$progressTask.a(this.g);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31(g90 g90, DeviceImplementation deviceImplementation, Phase phase) {
        super(2);
        this.$progressTask = g90;
        this.this$Anon0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Phase) obj, ((Number) obj2).floatValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase, float f) {
        wd4.b(phase, "executingPhase");
        this.this$Anon0.f.post(new a(this, phase, f));
    }
}
