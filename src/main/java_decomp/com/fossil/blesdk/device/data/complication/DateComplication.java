package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DateComplication extends Complication {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DateComplication> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DateComplication createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new DateComplication(parcel, (rd4) null);
        }

        @DexIgnore
        public DateComplication[] newArray(int i) {
            return new DateComplication[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DateComplication(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public DateComplication() {
        super(ComplicationId.DATE, (ComplicationDataConfig) null, (ComplicationPositionConfig) null, (ComplicationThemeConfig) null, 14, (rd4) null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DateComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig, int i, rd4 rd4) {
        this(complicationPositionConfig, (i & 2) != 0 ? new ComplicationThemeConfig(ComplicationThemeConfig.CREATOR.a()) : complicationThemeConfig);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DateComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig) {
        super(ComplicationId.DATE, (ComplicationDataConfig) null, complicationPositionConfig, complicationThemeConfig, 2, (rd4) null);
        wd4.b(complicationPositionConfig, "positionConfig");
        wd4.b(complicationThemeConfig, "themeConfig");
    }

    @DexIgnore
    public DateComplication(Parcel parcel) {
        super(parcel);
    }
}
