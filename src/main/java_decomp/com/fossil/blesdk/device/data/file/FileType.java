package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.util.Locale;
import kotlin.TypeCastException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FileType {
    OTA((byte) 0),
    ACTIVITY_FILE((byte) 1),
    HARDWARE_LOG((byte) 2),
    FONT((byte) 3),
    MUSIC_CONTROL((byte) 4),
    UI_SCRIPT((byte) 5),
    MICRO_APP((byte) 6),
    ASSET((byte) 7),
    DEVICE_CONFIG((byte) 8),
    NOTIFICATION((byte) 9),
    ALARM((byte) 10),
    DEVICE_INFO((byte) 11),
    NOTIFICATION_FILTER((byte) 12),
    UI_PACKAGE_FILE(DateTimeFieldType.HALFDAY_OF_DAY),
    WATCH_PARAMETERS_FILE(DateTimeFieldType.HOUR_OF_HALFDAY),
    LUTS_FILE(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    RATE_FILE(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    DATA_COLLECTION_FILE(DateTimeFieldType.HOUR_OF_DAY),
    ALL_FILE((byte) 255);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public static /* final */ byte MASKED_INDEX; // = -1;
    @DexIgnore
    public static /* final */ byte MIN_INDEX; // = 0;
    @DexIgnore
    public /* final */ short fileHandleMask;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final FileType a(short s) {
            for (FileType fileType : FileType.values()) {
                if (((short) (fileType.getFileHandleMask$blesdk_productionRelease() | s)) == fileType.getFileHandleMask$blesdk_productionRelease()) {
                    return fileType;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final FileType a(byte b) {
            for (FileType fileType : FileType.values()) {
                if (fileType.getId$blesdk_productionRelease() == b) {
                    return fileType;
                }
            }
            return null;
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    FileType(byte b) {
        this.id = b;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.id, (byte) 255});
            wd4.a((Object) wrap, "ByteBuffer.wrap(byteArrayOf(id, (0xFF).toByte()))");
            this.fileHandleMask = wrap.getShort();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final short getFileHandle$blesdk_productionRelease(byte b) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.id, b});
        wd4.a((Object) wrap, "ByteBuffer.wrap(byteArrayOf(id, index))");
        return wrap.getShort();
    }

    @DexIgnore
    public final short getFileHandleMask$blesdk_productionRelease() {
        return this.fileHandleMask;
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
