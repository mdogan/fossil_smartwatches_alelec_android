package com.fossil.blesdk.device.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.bg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Version extends JSONAbleObject implements Parcelable, Comparable<Version> {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ byte major;
    @DexIgnore
    public /* final */ byte minor;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<Version> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Version a(byte[] bArr) {
            wd4.b(bArr, "data");
            if (bArr.length < 2) {
                return null;
            }
            return new Version(bArr[0], bArr[1]);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public Version createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new Version(parcel, (rd4) null);
        }

        @DexIgnore
        public Version[] newArray(int i) {
            return new Version[i];
        }

        @DexIgnore
        public final Version a(String str) {
            wd4.b(str, "versionString");
            List a = StringsKt__StringsKt.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null);
            if (a.size() >= 2) {
                Byte a2 = bg4.a((String) a.get(0));
                Byte a3 = bg4.a((String) a.get(1));
                if (!(a2 == null || a3 == null)) {
                    return new Version(a2.byteValue(), a3.byteValue());
                }
            }
            return new Version((byte) 0, (byte) 0);
        }
    }

    @DexIgnore
    public /* synthetic */ Version(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) Version.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Version version = (Version) obj;
            return this.major == version.major && this.minor == version.minor;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.Version");
    }

    @DexIgnore
    public final byte getMajor() {
        return this.major;
    }

    @DexIgnore
    public final byte getMinor() {
        return this.minor;
    }

    @DexIgnore
    public final String getShortDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public int hashCode() {
        return (this.major * 31) + this.minor;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("major", Byte.valueOf(this.major)).put("minor", Byte.valueOf(this.minor));
        wd4.a((Object) put, "JSONObject()\n           \u2026     .put(\"minor\", minor)");
        return put;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.major);
        }
        if (parcel != null) {
            parcel.writeByte(this.minor);
        }
    }

    @DexIgnore
    public Version(byte b, byte b2) {
        this.major = b;
        this.minor = b2;
    }

    @DexIgnore
    public int compareTo(Version version) {
        wd4.b(version, FacebookRequestErrorClassification.KEY_OTHER);
        byte b = this.major;
        byte b2 = version.major;
        if (b <= b2) {
            if (b < b2) {
                return -1;
            }
            byte b3 = this.minor;
            byte b4 = version.minor;
            if (b3 <= b4) {
                if (b3 < b4) {
                    return -1;
                }
                return 0;
            }
        }
        return 1;
    }

    @DexIgnore
    public Version(Parcel parcel) {
        this(parcel.readByte(), parcel.readByte());
    }
}
