package com.fossil.blesdk.device.data.watchapp;

import com.fossil.blesdk.obfuscated.b30;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WatchAppId {
    DIAGNOSTICS("diagnosticsApp"),
    WELLNESS("wellnessApp"),
    WORKOUT("workoutApp"),
    MUSIC("musicApp"),
    NOTIFICATIONS_PANEL("notificationsPanelApp"),
    EMPTY("empty"),
    STOP_WATCH("stopwatchApp"),
    ASSISTANT("assistantApp"),
    TIMER("timerApp"),
    WEATHER("weatherApp"),
    COMMUTE("commuteApp");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ String rawName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    WatchAppId(String str) {
        this.rawName = str;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final Object getJsonName$blesdk_productionRelease() {
        Object obj;
        if (b30.a[ordinal()] != 1) {
            obj = this.rawName;
        } else {
            obj = JSONObject.NULL;
        }
        wd4.a(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final String getRawName$blesdk_productionRelease() {
        return this.rawName;
    }
}
