package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppEmptyDataConfig;
import com.fossil.blesdk.obfuscated.a30;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class WatchApp extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public ButtonEvent buttonEvent;
    @DexIgnore
    public /* final */ WatchAppId id;
    @DexIgnore
    public /* final */ WatchAppDataConfig mDataConfig;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WatchApp createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                WatchAppId valueOf = WatchAppId.valueOf(readString);
                parcel.setDataPosition(0);
                switch (a30.a[valueOf.ordinal()]) {
                    case 1:
                        return DiagnosticsWatchApp.CREATOR.createFromParcel(parcel);
                    case 2:
                        return WellnessWatchApp.CREATOR.createFromParcel(parcel);
                    case 3:
                        return WorkoutWatchApp.CREATOR.createFromParcel(parcel);
                    case 4:
                        return MusicWatchApp.CREATOR.createFromParcel(parcel);
                    case 5:
                        return NotificationPanelWatchApp.CREATOR.createFromParcel(parcel);
                    case 6:
                        return EmptyWatchApp.CREATOR.createFromParcel(parcel);
                    case 7:
                        return StopWatchWatchApp.CREATOR.createFromParcel(parcel);
                    case 8:
                        return AssistantWatchApp.CREATOR.createFromParcel(parcel);
                    case 9:
                        return TimerWatchApp.CREATOR.createFromParcel(parcel);
                    case 10:
                        return WeatherWatchApp.CREATOR.createFromParcel(parcel);
                    case 11:
                        return CommuteTimeWatchApp.CREATOR.createFromParcel(parcel);
                    default:
                        throw new NoWhenBranchMatchedException();
                }
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public WatchApp[] newArray(int i) {
            return new WatchApp[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchApp(WatchAppId watchAppId, ButtonEvent buttonEvent2, WatchAppDataConfig watchAppDataConfig, int i, rd4 rd4) {
        this(watchAppId, (i & 2) != 0 ? ButtonEvent.TOP_SHORT_PRESS_RELEASE : buttonEvent2, (i & 4) != 0 ? new WatchAppEmptyDataConfig() : watchAppDataConfig);
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WatchApp watchApp = (WatchApp) obj;
            return this.id == watchApp.id && this.buttonEvent == watchApp.buttonEvent && !(wd4.a((Object) this.mDataConfig, (Object) watchApp.mDataConfig) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchApp");
    }

    @DexIgnore
    public final ButtonEvent getButtonEvent() {
        return this.buttonEvent;
    }

    @DexIgnore
    public JSONObject getDataConfigJSONObject$blesdk_productionRelease() {
        return new JSONObject();
    }

    @DexIgnore
    public final WatchAppId getId() {
        return this.id;
    }

    @DexIgnore
    public final WatchAppDataConfig getMDataConfig() {
        return this.mDataConfig;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.id.hashCode() * 31) + this.buttonEvent.hashCode()) * 31) + this.mDataConfig.hashCode();
    }

    @DexIgnore
    public final void setButtonEvent$blesdk_productionRelease(ButtonEvent buttonEvent2) {
        wd4.b(buttonEvent2, "<set-?>");
        this.buttonEvent = buttonEvent2;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.id.getJsonName$blesdk_productionRelease()).put("button_evt", this.buttonEvent.getLogName());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.id.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.buttonEvent.ordinal());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.mDataConfig, i);
        }
    }

    @DexIgnore
    public WatchApp(WatchAppId watchAppId, ButtonEvent buttonEvent2, WatchAppDataConfig watchAppDataConfig) {
        wd4.b(watchAppId, "id");
        wd4.b(buttonEvent2, "buttonEvent");
        wd4.b(watchAppDataConfig, "dataConfig");
        this.id = watchAppId;
        this.buttonEvent = buttonEvent2;
        this.mDataConfig = watchAppDataConfig;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WatchApp(Parcel parcel) {
        this(r0, r2, (WatchAppDataConfig) r5);
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
            WatchAppId valueOf = WatchAppId.valueOf(readString);
            ButtonEvent buttonEvent2 = ButtonEvent.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(WatchAppDataConfig.class.getClassLoader());
            if (readParcelable != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
