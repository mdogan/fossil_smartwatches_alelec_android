package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.ve4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yd4;
import kotlin.jvm.internal.FunctionReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon5 extends FunctionReference implements jd4<byte[], DailyCalorieGoalConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon5(DailyCalorieGoalConfig.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final ve4 getOwner() {
        return yd4.a(DailyCalorieGoalConfig.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyCalorieGoalConfig;";
    }

    @DexIgnore
    public final DailyCalorieGoalConfig invoke(byte[] bArr) {
        wd4.b(bArr, "p1");
        return ((DailyCalorieGoalConfig.a) this.receiver).a(bArr);
    }
}
