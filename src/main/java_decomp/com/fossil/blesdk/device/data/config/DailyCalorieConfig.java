package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.vd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyCalorieConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_CALORIE; // = 0;
    @DexIgnore
    public static /* final */ long e; // = o90.a(vd4.a);
    @DexIgnore
    public /* final */ long calorie;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailyCalorieConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailyCalorieConfig a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 4) {
                return new DailyCalorieConfig(o90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 4");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DailyCalorieConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new DailyCalorieConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public DailyCalorieConfig[] newArray(int i) {
            return new DailyCalorieConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailyCalorieConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) DailyCalorieConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.calorie == ((DailyCalorieConfig) obj).calorie;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
    }

    @DexIgnore
    public final long getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.calorie).array();
        wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return Long.valueOf(this.calorie).hashCode();
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        long j = e;
        long j2 = this.calorie;
        if (!(0 <= j2 && j >= j2)) {
            throw new IllegalArgumentException("calorie(" + this.calorie + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.calorie);
        }
    }

    @DexIgnore
    public DailyCalorieConfig(long j) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_CALORIE);
        this.calorie = j;
        j();
    }

    @DexIgnore
    public Long valueDescription() {
        return Long.valueOf(this.calorie);
    }

    @DexIgnore
    public DailyCalorieConfig(Parcel parcel) {
        super(parcel);
        this.calorie = parcel.readLong();
        j();
    }
}
