package com.fossil.blesdk.device.data.enumerate;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;
import kotlin.TypeCastException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Weekday {
    SUNDAY((byte) 1, "Sun"),
    MONDAY((byte) 2, "Mon"),
    TUESDAY((byte) 4, "Tue"),
    WEDNESDAY((byte) 8, "Wed"),
    THURSDAY(DateTimeFieldType.CLOCKHOUR_OF_DAY, "Thu"),
    FRIDAY((byte) 32, "Fri"),
    SATURDAY((byte) 64, "Sat");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ String uiScriptName;
    @DexIgnore
    public /* final */ byte value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Set<Weekday> a(byte b) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (Weekday weekday : Weekday.values()) {
                if (((byte) (weekday.getValue$blesdk_productionRelease() & b)) == weekday.getValue$blesdk_productionRelease()) {
                    linkedHashSet.add(weekday);
                }
            }
            return linkedHashSet;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final Weekday[] a(String[] strArr) {
            Weekday weekday;
            wd4.b(strArr, "stringArray");
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                Weekday[] values = Weekday.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        weekday = null;
                        break;
                    }
                    weekday = values[i];
                    if (wd4.a((Object) weekday.getLogName$blesdk_productionRelease(), (Object) str) || wd4.a((Object) weekday.name(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                if (weekday != null) {
                    arrayList.add(weekday);
                }
            }
            Object[] array = arrayList.toArray(new Weekday[0]);
            if (array != null) {
                return (Weekday[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    Weekday(byte b, String str) {
        this.value = b;
        this.uiScriptName = str;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final String getUiScriptName$blesdk_productionRelease() {
        return this.uiScriptName;
    }

    @DexIgnore
    public final byte getValue$blesdk_productionRelease() {
        return this.value;
    }
}
