package com.fossil.blesdk.device.data.background;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig;
import com.fossil.blesdk.model.file.AssetFile;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundImage extends AssetFile {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public BackgroundPositionConfig positionConfig;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BackgroundImage> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public BackgroundImage createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new BackgroundImage(parcel, (rd4) null);
        }

        @DexIgnore
        public BackgroundImage[] newArray(int i) {
            return new BackgroundImage[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundImage(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final JSONObject getAssignmentJSON$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("image_name", getJsonFileName$blesdk_productionRelease()).put("pos", this.positionConfig.toJSONObject());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final BackgroundPositionConfig getPositionConfig() {
        return this.positionConfig;
    }

    @DexIgnore
    public final void setPositionConfig$blesdk_productionRelease(BackgroundPositionConfig backgroundPositionConfig) {
        wd4.b(backgroundPositionConfig, "<set-?>");
        this.positionConfig = backgroundPositionConfig;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject put = super.toJSONObject().put("pos", this.positionConfig.toJSONObject());
        wd4.a((Object) put, "super.toJSONObject()\n   \u2026ionConfig.toJSONObject())");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.positionConfig, i);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BackgroundImage(String str, byte[] bArr, BackgroundPositionConfig backgroundPositionConfig, int i, rd4 rd4) {
        this(str, bArr, (i & 4) != 0 ? new BackgroundPositionConfig(0, 62, 0) : backgroundPositionConfig);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BackgroundImage(String str, byte[] bArr, BackgroundPositionConfig backgroundPositionConfig) {
        super(str, bArr);
        wd4.b(str, "fileName");
        wd4.b(bArr, "fileData");
        wd4.b(backgroundPositionConfig, "positionConfig");
        this.positionConfig = backgroundPositionConfig;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public BackgroundImage(Parcel parcel) throws IllegalArgumentException {
        this(r0, r2, (BackgroundPositionConfig) r5);
        String readString = parcel.readString();
        if (readString != null) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                Parcelable readParcelable = parcel.readParcelable(BackgroundPositionConfig.class.getClassLoader());
                if (readParcelable != null) {
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
