package com.fossil.blesdk.device.data.enumerate;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum TemperatureUnit {
    C(0),
    F(1);
    
    @DexIgnore
    public static /* final */ float CELSIUS_TO_FAHRENHEIT_SCALE; // = 1.8f;
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public static /* final */ int FREEZING_POINT_OF_WATER_IN_FAHRENHEIT; // = 32;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ int value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final TemperatureUnit a(int i) {
            for (TemperatureUnit temperatureUnit : TemperatureUnit.values()) {
                if (temperatureUnit.getValue$blesdk_productionRelease() == i) {
                    return temperatureUnit;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    TemperatureUnit(int i) {
        this.value = i;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final int getValue$blesdk_productionRelease() {
        return this.value;
    }
}
