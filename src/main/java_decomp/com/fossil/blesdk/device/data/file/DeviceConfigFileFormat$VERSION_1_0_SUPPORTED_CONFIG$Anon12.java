package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.TimeConfig;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.ve4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yd4;
import kotlin.jvm.internal.FunctionReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon12 extends FunctionReference implements jd4<byte[], TimeConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon12(TimeConfig.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final ve4 getOwner() {
        return yd4.a(TimeConfig.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/TimeConfig;";
    }

    @DexIgnore
    public final TimeConfig invoke(byte[] bArr) {
        wd4.b(bArr, "p1");
        return ((TimeConfig.a) this.receiver).a(bArr);
    }
}
