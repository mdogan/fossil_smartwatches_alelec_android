package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.TemperatureUnit;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import java.io.Serializable;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherInfo extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int NUMBER_OF_REQUIRED_DAY_FORECAST; // = 3;
    @DexIgnore
    @Keep
    public static /* final */ int NUMBER_OF_REQUIRED_HOUR_FORECAST; // = 3;
    @DexIgnore
    public /* final */ CurrentWeatherInfo currentWeatherInfo;
    @DexIgnore
    public /* final */ WeatherDayForecast[] dayForecast;
    @DexIgnore
    public /* final */ long expiredTimeStampInSecond;
    @DexIgnore
    public /* final */ WeatherHourForecast[] hourForecast;
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ TemperatureUnit temperatureUnit;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherInfo createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherInfo(parcel, (rd4) null);
        }

        @DexIgnore
        public WeatherInfo[] newArray(int i) {
            return new WeatherInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherInfo(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) WeatherInfo.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WeatherInfo weatherInfo = (WeatherInfo) obj;
            return this.expiredTimeStampInSecond == weatherInfo.expiredTimeStampInSecond && this.temperatureUnit == weatherInfo.temperatureUnit && !(wd4.a((Object) this.location, (Object) weatherInfo.location) ^ true) && !(wd4.a((Object) this.currentWeatherInfo, (Object) weatherInfo.currentWeatherInfo) ^ true) && Arrays.equals(this.hourForecast, weatherInfo.hourForecast) && Arrays.equals(this.dayForecast, weatherInfo.dayForecast);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherInfo");
    }

    @DexIgnore
    public final CurrentWeatherInfo getCurrentWeatherInfo() {
        return this.currentWeatherInfo;
    }

    @DexIgnore
    public final WeatherDayForecast[] getDayForecast() {
        return this.dayForecast;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.expiredTimeStampInSecond;
    }

    @DexIgnore
    public final WeatherHourForecast[] getHourForecast() {
        return this.hourForecast;
    }

    @DexIgnore
    public final String getLocation() {
        return this.location;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            jSONArray.put(this.hourForecast[i].getSettingJSONData$blesdk_productionRelease());
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = 0; i2 < 3; i2++) {
            jSONArray2.put(this.dayForecast[i2].getSettingJSONData$blesdk_productionRelease());
        }
        JSONObject put = new JSONObject().put("alive", this.expiredTimeStampInSecond).put("city", this.location).put(Constants.PROFILE_KEY_UNIT, this.temperatureUnit.getLogName$blesdk_productionRelease());
        wd4.a((Object) put, "JSONObject().put(UIScrip\u2026 temperatureUnit.logName)");
        JSONObject put2 = n90.a(put, this.currentWeatherInfo.getSettingJSONData$blesdk_productionRelease()).put("forecast_day", jSONArray).put("forecast_week", jSONArray2);
        wd4.a((Object) put2, "JSONObject().put(UIScrip\u2026ST_WEEK, dayForecastJSON)");
        return put2;
    }

    @DexIgnore
    public final TemperatureUnit getTemperatureUnit() {
        return this.temperatureUnit;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((Long.valueOf(this.expiredTimeStampInSecond).hashCode() * 31) + this.temperatureUnit.hashCode()) * 31) + this.location.hashCode()) * 31) + this.currentWeatherInfo.hashCode()) * 31) + Arrays.hashCode(this.hourForecast)) * 31) + Arrays.hashCode(this.dayForecast);
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (this.hourForecast.length < 3) {
            throw new IllegalArgumentException("hourForecast must have at least 3 elements.");
        } else if (this.dayForecast.length < 3) {
            throw new IllegalArgumentException("dayForecast must have at least 3 elements.");
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.EXPIRED_TIMESTAMP_IN_SECOND, Long.valueOf(this.expiredTimeStampInSecond)), JSONKey.LOCATION, this.location), JSONKey.TEMP_UNIT, this.temperatureUnit.getLogName$blesdk_productionRelease()), JSONKey.CURRENT_WEATHER_INFO, this.currentWeatherInfo.toJSONObject()), JSONKey.HOURLY_FORECAST, k00.a(this.hourForecast)), JSONKey.DAILY_FORECAST, k00.a(this.dayForecast));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.expiredTimeStampInSecond);
        }
        if (parcel != null) {
            parcel.writeString(this.location);
        }
        if (parcel != null) {
            parcel.writeString(this.temperatureUnit.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.currentWeatherInfo, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.hourForecast, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.dayForecast, i);
        }
    }

    @DexIgnore
    public WeatherInfo(long j, String str, TemperatureUnit temperatureUnit2, CurrentWeatherInfo currentWeatherInfo2, WeatherHourForecast[] weatherHourForecastArr, WeatherDayForecast[] weatherDayForecastArr) throws IllegalArgumentException {
        wd4.b(str, PlaceFields.LOCATION);
        wd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        wd4.b(currentWeatherInfo2, "currentWeatherInfo");
        wd4.b(weatherHourForecastArr, "hourForecast");
        wd4.b(weatherDayForecastArr, "dayForecast");
        this.expiredTimeStampInSecond = j;
        this.location = str;
        this.temperatureUnit = temperatureUnit2;
        this.currentWeatherInfo = currentWeatherInfo2;
        this.hourForecast = weatherHourForecastArr;
        this.dayForecast = weatherDayForecastArr;
        i();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WeatherInfo(Parcel parcel) {
        this(r1, r3, r4, r5, r6, (WeatherDayForecast[]) r9);
        long readLong = parcel.readLong();
        String readString = parcel.readString();
        if (readString != null) {
            String readString2 = parcel.readString();
            if (readString2 != null) {
                TemperatureUnit valueOf = TemperatureUnit.valueOf(readString2);
                Parcelable readParcelable = parcel.readParcelable(CurrentWeatherInfo.class.getClassLoader());
                if (readParcelable != null) {
                    CurrentWeatherInfo currentWeatherInfo2 = (CurrentWeatherInfo) readParcelable;
                    Object[] createTypedArray = parcel.createTypedArray(WeatherHourForecast.CREATOR);
                    if (createTypedArray != null) {
                        WeatherHourForecast[] weatherHourForecastArr = (WeatherHourForecast[]) createTypedArray;
                        Object[] createTypedArray2 = parcel.createTypedArray(WeatherDayForecast.CREATOR);
                        if (createTypedArray2 != null) {
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }
}
