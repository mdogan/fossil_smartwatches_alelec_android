package com.fossil.blesdk.device.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VersionInformation extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Version currentVersion;
    @DexIgnore
    public /* final */ Version supportedVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<VersionInformation> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public VersionInformation createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new VersionInformation(parcel, (rd4) null);
        }

        @DexIgnore
        public VersionInformation[] newArray(int i) {
            return new VersionInformation[i];
        }
    }

    @DexIgnore
    public /* synthetic */ VersionInformation(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) VersionInformation.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            VersionInformation versionInformation = (VersionInformation) obj;
            return !(wd4.a((Object) this.currentVersion, (Object) versionInformation.currentVersion) ^ true) && !(wd4.a((Object) this.supportedVersion, (Object) versionInformation.supportedVersion) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.VersionInformation");
    }

    @DexIgnore
    public final Version getCurrentVersion() {
        return this.currentVersion;
    }

    @DexIgnore
    public final Version getSupportedVersion() {
        return this.supportedVersion;
    }

    @DexIgnore
    public int hashCode() {
        return (this.currentVersion.hashCode() * 31) + this.supportedVersion.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.CURRENT_VERSION, this.currentVersion), JSONKey.SUPPORTED_VERSION, this.supportedVersion);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.currentVersion, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.supportedVersion, i);
        }
    }

    @DexIgnore
    public VersionInformation(Version version, Version version2) {
        wd4.b(version, "currentVersion");
        wd4.b(version2, "supportedVersion");
        this.currentVersion = version;
        this.supportedVersion = version2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public VersionInformation(Parcel parcel) {
        this(r0, (Version) r4);
        Parcelable readParcelable = parcel.readParcelable(Version.class.getClassLoader());
        if (readParcelable != null) {
            Version version = (Version) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
