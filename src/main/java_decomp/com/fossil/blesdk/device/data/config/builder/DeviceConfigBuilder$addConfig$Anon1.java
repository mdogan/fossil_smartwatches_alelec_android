package com.fossil.blesdk.device.data.config.builder;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceConfigBuilder$addConfig$Anon1 extends Lambda implements jd4<DeviceConfigItem, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceConfigItem $config;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceConfigBuilder$addConfig$Anon1(DeviceConfigItem deviceConfigItem) {
        super(1);
        this.$config = deviceConfigItem;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((DeviceConfigItem) obj));
    }

    @DexIgnore
    public final boolean invoke(DeviceConfigItem deviceConfigItem) {
        wd4.b(deviceConfigItem, "deviceConfigItem");
        return deviceConfigItem.getKey() == this.$config.getKey();
    }
}
