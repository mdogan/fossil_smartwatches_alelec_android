package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.preset.DevicePresetItem;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.HashSet;
import java.util.Iterator;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppConfig extends DevicePresetItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ ButtonEvent e; // = ButtonEvent.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ButtonEvent f; // = ButtonEvent.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ButtonEvent g; // = ButtonEvent.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<WatchApp> watchAppAssignment;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchAppConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WatchAppConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WatchAppConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public WatchAppConfig[] newArray(int i) {
            return new WatchAppConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WatchAppConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void bottomButton$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void middleButton$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void topButton$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) WatchAppConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wd4.a((Object) this.watchAppAssignment, (Object) ((WatchAppConfig) obj).watchAppAssignment) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public JSONObject getAssignmentJSON$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (WatchApp watchApp : this.watchAppAssignment) {
            jSONArray.put(watchApp.toJSONObject());
            n90.a(jSONObject, watchApp.getDataConfigJSONObject$blesdk_productionRelease(), false, 2, (Object) null);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public final WatchApp getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == g) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final WatchApp getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == f) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final WatchApp getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == e) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.watchAppAssignment.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return getAssignmentJSON$blesdk_productionRelease();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.watchAppAssignment.toArray(new WatchApp[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public WatchAppConfig(WatchApp[] watchAppArr) {
        wd4.b(watchAppArr, "watchAppAssignment");
        this.watchAppAssignment = new HashSet<>();
        tb4.a(this.watchAppAssignment, (T[]) watchAppArr);
    }

    @DexIgnore
    public WatchAppConfig(WatchApp watchApp, WatchApp watchApp2, WatchApp watchApp3) {
        wd4.b(watchApp, "topButton");
        wd4.b(watchApp2, "middleButton");
        wd4.b(watchApp3, "bottomButton");
        this.watchAppAssignment = new HashSet<>();
        watchApp.setButtonEvent$blesdk_productionRelease(e);
        watchApp2.setButtonEvent$blesdk_productionRelease(f);
        watchApp3.setButtonEvent$blesdk_productionRelease(g);
        this.watchAppAssignment.add(watchApp);
        this.watchAppAssignment.add(watchApp2);
        this.watchAppAssignment.add(watchApp3);
    }

    @DexIgnore
    public WatchAppConfig(Parcel parcel) {
        super(parcel);
        this.watchAppAssignment = new HashSet<>();
        HashSet<WatchApp> hashSet = this.watchAppAssignment;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(WatchApp.class.getClassLoader());
        if (readParcelableArray != null) {
            tb4.a(hashSet, (T[]) (WatchApp[]) readParcelableArray);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }
}
