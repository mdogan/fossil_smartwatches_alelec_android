package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.ae4;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailySleepConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MINUTES; // = 0;
    @DexIgnore
    public static /* final */ int e; // = o90.a(ae4.a);
    @DexIgnore
    public /* final */ int awakeInMinute;
    @DexIgnore
    public /* final */ int deepSleepInMinute;
    @DexIgnore
    public /* final */ int lightSleepInMinute;
    @DexIgnore
    public /* final */ int totalSleepInMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailySleepConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailySleepConfig a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new DailySleepConfig(o90.b(order.getShort(0)), o90.b(order.getShort(2)), o90.b(order.getShort(4)), o90.b(order.getShort(6)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 8");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DailySleepConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new DailySleepConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public DailySleepConfig[] newArray(int i) {
            return new DailySleepConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailySleepConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) DailySleepConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DailySleepConfig dailySleepConfig = (DailySleepConfig) obj;
            return this.totalSleepInMinute == dailySleepConfig.totalSleepInMinute && this.awakeInMinute == dailySleepConfig.awakeInMinute && this.lightSleepInMinute == dailySleepConfig.lightSleepInMinute && this.deepSleepInMinute == dailySleepConfig.deepSleepInMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailySleepConfig");
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.awakeInMinute;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.totalSleepInMinute).putShort((short) this.awakeInMinute).putShort((short) this.lightSleepInMinute).putShort((short) this.deepSleepInMinute).array();
        wd4.a((Object) array, "ByteBuffer.allocate(Dail\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.deepSleepInMinute;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.lightSleepInMinute;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.totalSleepInMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.totalSleepInMinute * 31) + this.awakeInMinute) * 31) + this.lightSleepInMinute) * 31) + this.deepSleepInMinute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        int i = e;
        int i2 = this.totalSleepInMinute;
        boolean z = true;
        if (i2 >= 0 && i >= i2) {
            int i3 = e;
            int i4 = this.awakeInMinute;
            if (i4 >= 0 && i3 >= i4) {
                int i5 = e;
                int i6 = this.lightSleepInMinute;
                if (i6 >= 0 && i5 >= i6) {
                    int i7 = e;
                    int i8 = this.deepSleepInMinute;
                    if (i8 < 0 || i7 < i8) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalArgumentException("deepSleepInMinute (" + this.deepSleepInMinute + ") is out of range " + "[0, " + e + "].");
                    }
                    return;
                }
                throw new IllegalArgumentException("lightSleepInMinute (" + this.lightSleepInMinute + ") is out of range " + "[0, " + e + "].");
            }
            throw new IllegalArgumentException("awakeInMinute (" + this.awakeInMinute + ") is out of range " + "[0, " + e + "].");
        }
        throw new IllegalArgumentException("totalSleepInMinute (" + this.totalSleepInMinute + ") is out of range " + "[0, " + e + "].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.totalSleepInMinute);
        }
        if (parcel != null) {
            parcel.writeInt(this.awakeInMinute);
        }
        if (parcel != null) {
            parcel.writeInt(this.lightSleepInMinute);
        }
        if (parcel != null) {
            parcel.writeInt(this.deepSleepInMinute);
        }
    }

    @DexIgnore
    public DailySleepConfig(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_SLEEP);
        this.totalSleepInMinute = i;
        this.awakeInMinute = i2;
        this.lightSleepInMinute = i3;
        this.deepSleepInMinute = i4;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            xa0.a(xa0.a(xa0.a(xa0.a(jSONObject, JSONKey.TOTAL_SLEEP_IN_MINUTE, Integer.valueOf(this.totalSleepInMinute)), JSONKey.AWAKE_IN_MINUTE, Integer.valueOf(this.awakeInMinute)), JSONKey.LIGHT_SLEEP_IN_MINUTE, Integer.valueOf(this.lightSleepInMinute)), JSONKey.DEEP_SLEEP_IN_MINUTE, Integer.valueOf(this.deepSleepInMinute));
        } catch (JSONException e2) {
            ea0.l.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public DailySleepConfig(Parcel parcel) {
        super(parcel);
        this.totalSleepInMinute = parcel.readInt();
        this.awakeInMinute = parcel.readInt();
        this.lightSleepInMinute = parcel.readInt();
        this.deepSleepInMinute = parcel.readInt();
        j();
    }
}
