package com.fossil.blesdk.device.data.calibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HandMovingConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ int DATA_LENGTH; // = 5;
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ HandId handId;
    @DexIgnore
    public /* final */ HandMovingDirection movingDirection;
    @DexIgnore
    public /* final */ HandMovingSpeed movingSpeed;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HandMovingConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public HandMovingConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new HandMovingConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public HandMovingConfig[] newArray(int i) {
            return new HandMovingConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HandMovingConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void degree$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void handId$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void movingDirection$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void movingSpeed$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) HandMovingConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            HandMovingConfig handMovingConfig = (HandMovingConfig) obj;
            return this.handId == handMovingConfig.handId && this.degree == handMovingConfig.degree && this.movingDirection == handMovingConfig.movingDirection && this.movingSpeed == handMovingConfig.movingSpeed;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.calibration.HandMovingConfig");
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        byte[] array = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).put(this.handId.getId$blesdk_productionRelease()).putShort((short) this.degree).put(this.movingDirection.getId$blesdk_productionRelease()).put(this.movingSpeed.getId$blesdk_productionRelease()).array();
        wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final HandId getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final HandMovingDirection getMovingDirection() {
        return this.movingDirection;
    }

    @DexIgnore
    public final HandMovingSpeed getMovingSpeed() {
        return this.movingSpeed;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.handId.hashCode() * 31) + this.degree) * 31) + this.movingDirection.hashCode()) * 31) + this.movingSpeed.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.HAND_ID, this.handId.getLogName$blesdk_productionRelease()), JSONKey.DEGREE, Integer.valueOf(this.degree)), JSONKey.DIRECTION, this.movingDirection.getLogName$blesdk_productionRelease()), JSONKey.SPEED, this.movingSpeed.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.handId.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.degree);
        }
        if (parcel != null) {
            parcel.writeString(this.movingDirection.name());
        }
        if (parcel != null) {
            parcel.writeString(this.movingSpeed.name());
        }
    }

    @DexIgnore
    public HandMovingConfig(HandId handId2, int i, HandMovingDirection handMovingDirection, HandMovingSpeed handMovingSpeed) {
        wd4.b(handId2, "handId");
        wd4.b(handMovingDirection, "movingDirection");
        wd4.b(handMovingSpeed, "movingSpeed");
        this.handId = handId2;
        this.degree = i;
        this.movingDirection = handMovingDirection;
        this.movingSpeed = handMovingSpeed;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HandMovingConfig(Parcel parcel) {
        this(r0, r2, r3, HandMovingSpeed.valueOf(r5));
        String readString = parcel.readString();
        if (readString != null) {
            HandId valueOf = HandId.valueOf(readString);
            int readInt = parcel.readInt();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                HandMovingDirection valueOf2 = HandMovingDirection.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
