package com.fossil.blesdk.device.data.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OneShotAlarm extends Alarm implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Weekday dayOfWeek;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<OneShotAlarm> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final LinkedHashSet<Weekday> a(Weekday weekday) {
            LinkedHashSet<Weekday> linkedHashSet = new LinkedHashSet<>();
            if (weekday == null) {
                tb4.a(linkedHashSet, (T[]) Weekday.values());
            } else {
                linkedHashSet.add(weekday);
            }
            return linkedHashSet;
        }

        @DexIgnore
        public OneShotAlarm createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new OneShotAlarm(parcel, (rd4) null);
        }

        @DexIgnore
        public OneShotAlarm[] newArray(int i) {
            return new OneShotAlarm[i];
        }
    }

    @DexIgnore
    public /* synthetic */ OneShotAlarm(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final Weekday getDayOfWeek() {
        return this.dayOfWeek;
    }

    @DexIgnore
    public OneShotAlarm(byte b, byte b2, Weekday weekday) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(weekday), false, false, true);
        this.dayOfWeek = weekday;
    }

    @DexIgnore
    public OneShotAlarm(Parcel parcel) {
        super(parcel);
        this.dayOfWeek = (getDaysOfWeek$blesdk_productionRelease().isEmpty() || getDaysOfWeek$blesdk_productionRelease().size() == Weekday.values().length) ? null : (Weekday) wb4.d(getDaysOfWeek$blesdk_productionRelease());
    }
}
