package com.fossil.blesdk.device.data.backgroundsync;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundSyncFrame extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ int BACKGROUND_SYNC_FRAME_LENGTH; // = 3;
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ BackgroundSyncAction action;
    @DexIgnore
    public /* final */ short fileHandle;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BackgroundSyncFrame> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final BackgroundSyncFrame a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                BackgroundSyncAction a = BackgroundSyncAction.Companion.a(order.get(0));
                if (a != null) {
                    return new BackgroundSyncFrame(a, order.getShort(1));
                }
                throw new IllegalArgumentException("Invalid action: " + order.get(0) + '.');
            }
            throw new IllegalArgumentException("Invalid data length (" + bArr.length + "), " + "require 3.");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public BackgroundSyncFrame createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new BackgroundSyncFrame(parcel, (rd4) null);
        }

        @DexIgnore
        public BackgroundSyncFrame[] newArray(int i) {
            return new BackgroundSyncFrame[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundSyncFrame(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) BackgroundSyncFrame.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BackgroundSyncFrame backgroundSyncFrame = (BackgroundSyncFrame) obj;
            return this.action == backgroundSyncFrame.action && this.fileHandle == backgroundSyncFrame.fileHandle;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame");
    }

    @DexIgnore
    public final BackgroundSyncAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final short getFileHandle() {
        return this.fileHandle;
    }

    @DexIgnore
    public int hashCode() {
        return (this.action.hashCode() * 31) + this.fileHandle;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease()), JSONKey.FILE_HANDLE, o90.a(this.fileHandle));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.fileHandle);
        }
    }

    @DexIgnore
    public BackgroundSyncFrame(BackgroundSyncAction backgroundSyncAction, short s) {
        wd4.b(backgroundSyncAction, "action");
        this.action = backgroundSyncAction;
        this.fileHandle = s;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public BackgroundSyncFrame(Parcel parcel) {
        this(BackgroundSyncAction.valueOf(r0), (short) parcel.readInt());
        String readString = parcel.readString();
        if (readString != null) {
        } else {
            wd4.a();
            throw null;
        }
    }
}
