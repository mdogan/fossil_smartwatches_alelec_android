package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainInfo extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_CHANCE_OF_RAIN; // = 100;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_CHANCE_OF_RAIN; // = 0;
    @DexIgnore
    public /* final */ int chanceOfRain;
    @DexIgnore
    public /* final */ long expiredTimeStampInSecond;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ChanceOfRainInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ChanceOfRainInfo createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ChanceOfRainInfo(parcel, (rd4) null);
        }

        @DexIgnore
        public ChanceOfRainInfo[] newArray(int i) {
            return new ChanceOfRainInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ChanceOfRainInfo(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void chanceOfRain$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void expiredTimeStampInSecond$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) ChanceOfRainInfo.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ChanceOfRainInfo chanceOfRainInfo = (ChanceOfRainInfo) obj;
            return this.expiredTimeStampInSecond == chanceOfRainInfo.expiredTimeStampInSecond && this.chanceOfRain == chanceOfRainInfo.chanceOfRain;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.ChanceOfRainInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.chanceOfRain;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.expiredTimeStampInSecond;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.expiredTimeStampInSecond);
            jSONObject.put("rain", this.chanceOfRain);
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int hashCode() {
        return (((int) this.expiredTimeStampInSecond) * 31) + this.chanceOfRain;
    }

    @DexIgnore
    public final void i() {
        int i = this.chanceOfRain;
        if (!(i >= 0 && 100 >= i)) {
            throw new IllegalArgumentException("chanceOfRain(" + this.chanceOfRain + ") is out of range " + "[0, 100].");
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.EXPIRED_TIMESTAMP_IN_SECOND, Long.valueOf(this.expiredTimeStampInSecond)), JSONKey.CHANCE_OF_RAIN, Integer.valueOf(this.chanceOfRain));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.expiredTimeStampInSecond);
        }
        if (parcel != null) {
            parcel.writeInt(this.chanceOfRain);
        }
    }

    @DexIgnore
    public ChanceOfRainInfo(long j, int i) {
        this.expiredTimeStampInSecond = j;
        this.chanceOfRain = i;
        i();
    }

    @DexIgnore
    public ChanceOfRainInfo(Parcel parcel) {
        this(parcel.readLong(), parcel.readInt());
    }
}
