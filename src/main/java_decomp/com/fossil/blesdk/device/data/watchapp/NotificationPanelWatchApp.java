package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationPanelWatchApp extends WatchApp {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationPanelWatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public NotificationPanelWatchApp createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new NotificationPanelWatchApp(parcel, (rd4) null);
        }

        @DexIgnore
        public NotificationPanelWatchApp[] newArray(int i) {
            return new NotificationPanelWatchApp[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationPanelWatchApp(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public NotificationPanelWatchApp() {
        super(WatchAppId.NOTIFICATIONS_PANEL, (ButtonEvent) null, (WatchAppDataConfig) null, 6, (rd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationPanelWatchApp(ButtonEvent buttonEvent) {
        super(WatchAppId.NOTIFICATIONS_PANEL, buttonEvent, (WatchAppDataConfig) null, 4, (rd4) null);
        wd4.b(buttonEvent, "buttonEvent");
    }

    @DexIgnore
    public NotificationPanelWatchApp(Parcel parcel) {
        super(parcel);
    }
}
