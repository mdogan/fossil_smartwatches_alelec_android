package com.fossil.blesdk.device.data.workoutsession;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSession extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ long activeCaloriesInCalorie;
    @DexIgnore
    public /* final */ ActivityType activityType;
    @DexIgnore
    public /* final */ short averageHeartRate;
    @DexIgnore
    public /* final */ short currentHeartRate;
    @DexIgnore
    public /* final */ long distanceInMeter;
    @DexIgnore
    public /* final */ long durationInSecond;
    @DexIgnore
    public /* final */ short maximumHeartRate;
    @DexIgnore
    public /* final */ long numberOfStep;
    @DexIgnore
    public /* final */ long sessionId;
    @DexIgnore
    public /* final */ SessionState sessionState;
    @DexIgnore
    public /* final */ long totalCaloriesInCalorie;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WorkoutSession> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WorkoutSession createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WorkoutSession(parcel, (rd4) null);
        }

        @DexIgnore
        public WorkoutSession[] newArray(int i) {
            return new WorkoutSession[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WorkoutSession(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) WorkoutSession.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WorkoutSession workoutSession = (WorkoutSession) obj;
            return this.sessionId == workoutSession.sessionId && this.activityType == workoutSession.activityType && this.sessionState == workoutSession.sessionState && this.durationInSecond == workoutSession.durationInSecond && this.numberOfStep == workoutSession.numberOfStep && this.distanceInMeter == workoutSession.distanceInMeter && this.activeCaloriesInCalorie == workoutSession.activeCaloriesInCalorie && this.totalCaloriesInCalorie == workoutSession.totalCaloriesInCalorie && this.currentHeartRate == workoutSession.currentHeartRate && this.averageHeartRate == workoutSession.averageHeartRate && this.maximumHeartRate == workoutSession.maximumHeartRate;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutSession");
    }

    @DexIgnore
    public final long getActiveCaloriesInCalorie() {
        return this.activeCaloriesInCalorie;
    }

    @DexIgnore
    public final ActivityType getActivityType() {
        return this.activityType;
    }

    @DexIgnore
    public final short getAverageHeartRate() {
        return this.averageHeartRate;
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.currentHeartRate;
    }

    @DexIgnore
    public final long getDistanceInMeter() {
        return this.distanceInMeter;
    }

    @DexIgnore
    public final long getDurationInSecond() {
        return this.durationInSecond;
    }

    @DexIgnore
    public final short getMaximumHeartRate() {
        return this.maximumHeartRate;
    }

    @DexIgnore
    public final long getNumberOfStep() {
        return this.numberOfStep;
    }

    @DexIgnore
    public final long getSessionId() {
        return this.sessionId;
    }

    @DexIgnore
    public final SessionState getSessionState() {
        return this.sessionState;
    }

    @DexIgnore
    public final long getTotalCaloriesInCalorie() {
        return this.totalCaloriesInCalorie;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((((((((((Long.valueOf(this.sessionId).hashCode() * 31) + this.activityType.hashCode()) * 31) + this.sessionState.hashCode()) * 31) + Long.valueOf(this.durationInSecond).hashCode()) * 31) + Long.valueOf(this.numberOfStep).hashCode()) * 31) + Long.valueOf(this.distanceInMeter).hashCode()) * 31) + Long.valueOf(this.activeCaloriesInCalorie).hashCode()) * 31) + Long.valueOf(this.totalCaloriesInCalorie).hashCode()) * 31) + this.currentHeartRate) * 31) + this.averageHeartRate) * 31) + this.maximumHeartRate;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put(Constants.SESSION_ID, this.sessionId).put("type", this.activityType.getLogName$blesdk_productionRelease()).put("state", this.sessionState.getLogName$blesdk_productionRelease()).put("duration_in_second", this.durationInSecond).put("number_of_step", this.numberOfStep).put("distance_in_meter", this.distanceInMeter).put("active_calorie", this.activeCaloriesInCalorie).put("total_calorie", this.totalCaloriesInCalorie).put("current_heart_rate", Short.valueOf(this.currentHeartRate)).put("average_heart_rate", Short.valueOf(this.averageHeartRate)).put("maximum_heart_rate", Short.valueOf(this.maximumHeartRate));
        wd4.a((Object) put, "JSONObject()\n           \u2026_rate\", maximumHeartRate)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.sessionId);
        }
        if (parcel != null) {
            parcel.writeString(this.activityType.name());
        }
        if (parcel != null) {
            parcel.writeString(this.sessionState.name());
        }
        if (parcel != null) {
            parcel.writeLong(this.durationInSecond);
        }
        if (parcel != null) {
            parcel.writeLong(this.numberOfStep);
        }
        if (parcel != null) {
            parcel.writeLong(this.distanceInMeter);
        }
        if (parcel != null) {
            parcel.writeLong(this.activeCaloriesInCalorie);
        }
        if (parcel != null) {
            parcel.writeLong(this.totalCaloriesInCalorie);
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.currentHeartRate));
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.averageHeartRate));
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.maximumHeartRate));
        }
    }

    @DexIgnore
    public WorkoutSession(long j, ActivityType activityType2, SessionState sessionState2, long j2, long j3, long j4, long j5, long j6, short s, short s2, short s3) {
        wd4.b(activityType2, "activityType");
        wd4.b(sessionState2, "sessionState");
        this.sessionId = j;
        this.activityType = activityType2;
        this.sessionState = sessionState2;
        this.durationInSecond = j2;
        this.numberOfStep = j3;
        this.distanceInMeter = j4;
        this.activeCaloriesInCalorie = j5;
        this.totalCaloriesInCalorie = j6;
        this.currentHeartRate = s;
        this.averageHeartRate = s2;
        this.maximumHeartRate = s3;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WorkoutSession(Parcel parcel) {
        this(r1, r18, SessionState.valueOf(r0), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), (short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt());
        long readLong = parcel.readLong();
        String readString = parcel.readString();
        if (readString != null) {
            ActivityType valueOf = ActivityType.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }
}
