package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.nd4;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InactiveNudgeConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_HOUR; // = 23;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_MINUTE; // = 59;
    @DexIgnore
    @Keep
    public static /* final */ short MAXIMUM_REPEAT_INTERVAL; // = o90.a(nd4.a);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_HOUR; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MINUTE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_REPEAT_INTERVAL; // = 0;
    @DexIgnore
    public /* final */ short repeatInterval;
    @DexIgnore
    public /* final */ byte startHour;
    @DexIgnore
    public /* final */ byte startMinute;
    @DexIgnore
    public /* final */ State state;
    @DexIgnore
    public /* final */ byte stopHour;
    @DexIgnore
    public /* final */ byte stopMinute;

    @DexIgnore
    public enum State {
        DISABLE((byte) 0),
        ENABLE((byte) 1);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ byte id;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final State a(byte b) throws IllegalArgumentException {
                State state;
                State[] values = State.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        state = null;
                        break;
                    }
                    state = values[i];
                    if (state.getId$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                if (state != null) {
                    return state;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }

            @DexIgnore
            public /* synthetic */ a(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((rd4) null);
        }
        */

        @DexIgnore
        State(byte b) {
            this.id = b;
            String name = name();
            Locale locale = Locale.US;
            wd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<InactiveNudgeConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final InactiveNudgeConfig a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 6) {
                return new InactiveNudgeConfig(bArr[0], bArr[1], bArr[2], bArr[3], o90.b(bArr[4]), State.Companion.a(bArr[5]));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 6");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public InactiveNudgeConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new InactiveNudgeConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public InactiveNudgeConfig[] newArray(int i) {
            return new InactiveNudgeConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ InactiveNudgeConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) InactiveNudgeConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            InactiveNudgeConfig inactiveNudgeConfig = (InactiveNudgeConfig) obj;
            return this.startHour == inactiveNudgeConfig.startHour && this.startMinute == inactiveNudgeConfig.startMinute && this.stopHour == inactiveNudgeConfig.stopHour && this.stopMinute == inactiveNudgeConfig.stopMinute && this.repeatInterval == inactiveNudgeConfig.repeatInterval && this.state == inactiveNudgeConfig.state;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.InactiveNudgeConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put(this.startHour).put(this.startMinute).put(this.stopHour).put(this.stopMinute).put((byte) this.repeatInterval).put(this.state.getId$blesdk_productionRelease()).array();
        wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.repeatInterval;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.startHour;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.startMinute;
    }

    @DexIgnore
    public final State getState() {
        return this.state;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.stopHour;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.stopMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((super.hashCode() * 31) + this.startHour) * 31) + this.startMinute) * 31) + this.stopHour) * 31) + this.stopMinute) * 31) + this.repeatInterval) * 31) + this.state.hashCode();
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        byte b = this.startHour;
        boolean z = true;
        if (b >= 0 && 23 >= b) {
            byte b2 = this.startMinute;
            if (b2 >= 0 && 59 >= b2) {
                byte b3 = this.stopHour;
                if (b3 >= 0 && 23 >= b3) {
                    byte b4 = this.stopMinute;
                    if (b4 >= 0 && 59 >= b4) {
                        short s = MAXIMUM_REPEAT_INTERVAL;
                        short s2 = this.repeatInterval;
                        if (s2 < 0 || s < s2) {
                            z = false;
                        }
                        if (!z) {
                            throw new IllegalArgumentException("repeatInterval(" + this.repeatInterval + ") is out of range " + "[0, " + MAXIMUM_REPEAT_INTERVAL + "] (in minute.");
                        }
                        return;
                    }
                    throw new IllegalArgumentException("stopMinute(" + this.stopMinute + ") is out of range " + "[0, 59].");
                }
                throw new IllegalArgumentException("stopHour(" + this.stopHour + ") is out of range " + "[0, 23].");
            }
            throw new IllegalArgumentException("startMinute(" + this.startMinute + ") is out of range " + "[0, 59].");
        }
        throw new IllegalArgumentException("startHour(" + this.startHour + ") is out of range " + "[0, 23].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.startHour);
        }
        if (parcel != null) {
            parcel.writeByte(this.startMinute);
        }
        if (parcel != null) {
            parcel.writeByte(this.stopHour);
        }
        if (parcel != null) {
            parcel.writeByte(this.stopMinute);
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.repeatInterval));
        }
        if (parcel != null) {
            parcel.writeString(this.state.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InactiveNudgeConfig(byte b, byte b2, byte b3, byte b4, short s, State state2) throws IllegalArgumentException {
        super(DeviceConfigKey.INACTIVE_NUDGE);
        wd4.b(state2, "state");
        this.startHour = b;
        this.startMinute = b2;
        this.stopHour = b3;
        this.stopMinute = b4;
        this.repeatInterval = s;
        this.state = state2;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.startHour));
            jSONObject.put("start_minute", Byte.valueOf(this.startMinute));
            jSONObject.put("stop_hour", Byte.valueOf(this.stopHour));
            jSONObject.put("stop_minute", Byte.valueOf(this.stopMinute));
            jSONObject.put("repeat_interval", Short.valueOf(this.repeatInterval));
            jSONObject.put("state", this.state.getLogName$blesdk_productionRelease());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public InactiveNudgeConfig(Parcel parcel) {
        super(parcel);
        this.startHour = parcel.readByte();
        this.startMinute = parcel.readByte();
        this.stopHour = parcel.readByte();
        this.stopMinute = parcel.readByte();
        this.repeatInterval = (short) parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.state = State.valueOf(readString);
            j();
            return;
        }
        wd4.a();
        throw null;
    }
}
