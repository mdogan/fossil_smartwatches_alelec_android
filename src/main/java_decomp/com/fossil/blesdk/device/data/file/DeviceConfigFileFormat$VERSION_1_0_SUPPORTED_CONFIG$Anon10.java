package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.ve4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yd4;
import kotlin.jvm.internal.FunctionReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon10 extends FunctionReference implements jd4<byte[], VibeStrengthConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon10(VibeStrengthConfig.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final ve4 getOwner() {
        return yd4.a(VibeStrengthConfig.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/VibeStrengthConfig;";
    }

    @DexIgnore
    public final VibeStrengthConfig invoke(byte[] bArr) {
        wd4.b(bArr, "p1");
        return ((VibeStrengthConfig.a) this.receiver).a(bArr);
    }
}
