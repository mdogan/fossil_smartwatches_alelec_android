package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.vd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_MILLISECOND; // = 1000;
    @DexIgnore
    @Keep
    public static /* final */ long MAXIMUM_SECOND; // = o90.a(vd4.a);
    @DexIgnore
    @Keep
    public static /* final */ short MAXIMUM_TIMEZONE_OFFSET_IN_MINUTE; // = Short.MAX_VALUE;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MILLISECOND; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_SECOND; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ short MINIMUM_TIMEZONE_OFFSET_IN_MINUTE; // = Short.MIN_VALUE;
    @DexIgnore
    public /* final */ short millisecond;
    @DexIgnore
    public /* final */ long second;
    @DexIgnore
    public /* final */ short timezoneOffsetInMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<TimeConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final TimeConfig a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new TimeConfig(o90.b(order.getInt(0)), order.getShort(4), order.getShort(6));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 8");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public TimeConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new TimeConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public TimeConfig[] newArray(int i) {
            return new TimeConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ TimeConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) TimeConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            TimeConfig timeConfig = (TimeConfig) obj;
            return this.second == timeConfig.second && this.millisecond == timeConfig.millisecond && this.timezoneOffsetInMinute == timeConfig.timezoneOffsetInMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.TimeConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.second).putShort(this.millisecond).putShort(this.timezoneOffsetInMinute).array();
        wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final short getMillisecond() {
        return this.millisecond;
    }

    @DexIgnore
    public final long getSecond() {
        return this.second;
    }

    @DexIgnore
    public final short getTimezoneOffsetInMinute() {
        return this.timezoneOffsetInMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((super.hashCode() * 31) + Long.valueOf(this.second).hashCode()) * 31) + this.millisecond) * 31) + this.timezoneOffsetInMinute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        long j = MAXIMUM_SECOND;
        long j2 = this.second;
        boolean z = true;
        if (0 <= j2 && j >= j2) {
            short s = this.millisecond;
            if (s >= 0 && 1000 >= s) {
                short s2 = this.timezoneOffsetInMinute;
                if (Short.MIN_VALUE > s2 || Short.MAX_VALUE < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException("timezoneOffsetInMinute(" + this.timezoneOffsetInMinute + ") is out of range " + "[-32768, 32767].");
                }
                return;
            }
            throw new IllegalArgumentException("millisecond(" + this.millisecond + ") is out of range " + "[0, 1000].");
        }
        throw new IllegalArgumentException("second(" + this.second + ") is out of range " + "[0, " + MAXIMUM_SECOND + "].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.second);
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.millisecond));
        }
        if (parcel != null) {
            parcel.writeInt(o90.b(this.timezoneOffsetInMinute));
        }
    }

    @DexIgnore
    public TimeConfig(long j, short s, short s2) throws IllegalArgumentException {
        super(DeviceConfigKey.TIME);
        this.second = j;
        this.millisecond = s;
        this.timezoneOffsetInMinute = s2;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("second", this.second);
            jSONObject.put("millisecond", Short.valueOf(this.millisecond));
            jSONObject.put("timezone_offset_in_minute", Short.valueOf(this.timezoneOffsetInMinute));
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public TimeConfig(Parcel parcel) {
        super(parcel);
        this.second = parcel.readLong();
        this.millisecond = (short) parcel.readInt();
        this.timezoneOffsetInMinute = (short) parcel.readInt();
        j();
    }
}
