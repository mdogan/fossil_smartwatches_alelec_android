package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateComplication extends Complication {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HeartRateComplication> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public HeartRateComplication createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new HeartRateComplication(parcel, (rd4) null);
        }

        @DexIgnore
        public HeartRateComplication[] newArray(int i) {
            return new HeartRateComplication[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HeartRateComplication(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public HeartRateComplication() {
        super(ComplicationId.HEART_RATE, (ComplicationDataConfig) null, (ComplicationPositionConfig) null, (ComplicationThemeConfig) null, 14, (rd4) null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig, int i, rd4 rd4) {
        this(complicationPositionConfig, (i & 2) != 0 ? new ComplicationThemeConfig(ComplicationThemeConfig.CREATOR.a()) : complicationThemeConfig);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig) {
        super(ComplicationId.HEART_RATE, (ComplicationDataConfig) null, complicationPositionConfig, complicationThemeConfig, 2, (rd4) null);
        wd4.b(complicationPositionConfig, "positionConfig");
        wd4.b(complicationThemeConfig, "themeConfig");
    }

    @DexIgnore
    public HeartRateComplication(Parcel parcel) {
        super(parcel);
    }
}
