package com.fossil.blesdk.device.data.backgroundsync;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum BackgroundSyncAction {
    GET((byte) 0),
    SET((byte) 1);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final BackgroundSyncAction a(byte b) {
            for (BackgroundSyncAction backgroundSyncAction : BackgroundSyncAction.values()) {
                if (backgroundSyncAction.getId$blesdk_productionRelease() == b) {
                    return backgroundSyncAction;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    BackgroundSyncAction(byte b) {
        this.id = b;
        String name = name();
        if (name != null) {
            String lowerCase = name.toLowerCase();
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
