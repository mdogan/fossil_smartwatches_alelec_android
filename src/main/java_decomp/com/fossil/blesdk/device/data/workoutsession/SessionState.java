package com.fossil.blesdk.device.data.workoutsession;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SessionState {
    START((byte) 0),
    RESUMED((byte) 1),
    PAUSED((byte) 2),
    COMPLETED((byte) 3);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SessionState a(byte b) {
            for (SessionState sessionState : SessionState.values()) {
                if (sessionState.getId$blesdk_productionRelease() == b) {
                    return sessionState;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    SessionState(byte b) {
        this.id = b;
        String name = name();
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
