package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeZoneTwoComplication extends Complication {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<TimeZoneTwoComplication> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public TimeZoneTwoComplication createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new TimeZoneTwoComplication(parcel, (rd4) null);
        }

        @DexIgnore
        public TimeZoneTwoComplication[] newArray(int i) {
            return new TimeZoneTwoComplication[i];
        }
    }

    @DexIgnore
    public /* synthetic */ TimeZoneTwoComplication(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void dataConfig$annotations() {
    }

    @DexIgnore
    public final TimeZoneTwoComplicationDataConfig getDataConfig() {
        ComplicationDataConfig mDataConfig = super.getMDataConfig();
        if (mDataConfig != null) {
            return (TimeZoneTwoComplicationDataConfig) mDataConfig;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeZoneTwoComplication(TimeZoneTwoComplicationDataConfig timeZoneTwoComplicationDataConfig) {
        super(ComplicationId.SECOND_TIMEZONE, timeZoneTwoComplicationDataConfig, (ComplicationPositionConfig) null, (ComplicationThemeConfig) null, 12, (rd4) null);
        wd4.b(timeZoneTwoComplicationDataConfig, "timeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TimeZoneTwoComplication(TimeZoneTwoComplicationDataConfig timeZoneTwoComplicationDataConfig, ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig, int i, rd4 rd4) {
        this(timeZoneTwoComplicationDataConfig, complicationPositionConfig, (i & 4) != 0 ? new ComplicationThemeConfig(ComplicationThemeConfig.CREATOR.a()) : complicationThemeConfig);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeZoneTwoComplication(TimeZoneTwoComplicationDataConfig timeZoneTwoComplicationDataConfig, ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig) {
        super(ComplicationId.SECOND_TIMEZONE, timeZoneTwoComplicationDataConfig, complicationPositionConfig, complicationThemeConfig);
        wd4.b(timeZoneTwoComplicationDataConfig, "timeZoneTwoComplicationDataConfig");
        wd4.b(complicationPositionConfig, "positionConfig");
        wd4.b(complicationThemeConfig, "themeConfig");
    }

    @DexIgnore
    public TimeZoneTwoComplication(Parcel parcel) {
        super(parcel);
    }
}
