package com.fossil.blesdk.device.data.file;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FileAction {
    GET((byte) 0),
    PUT((byte) 1);
    
    @DexIgnore
    public /* final */ byte id;

    @DexIgnore
    FileAction(byte b) {
        this.id = b;
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }
}
