package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.TemperatureUnit;
import com.fossil.blesdk.device.data.enumerate.WeatherCondition;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CompactWeatherInfo extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ long expiredTimeStampInSecond;
    @DexIgnore
    public /* final */ float temperature;
    @DexIgnore
    public /* final */ TemperatureUnit temperatureUnit;
    @DexIgnore
    public /* final */ WeatherCondition weatherCondition;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CompactWeatherInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CompactWeatherInfo createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CompactWeatherInfo(parcel, (rd4) null);
        }

        @DexIgnore
        public CompactWeatherInfo[] newArray(int i) {
            return new CompactWeatherInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CompactWeatherInfo(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void expiredTimeStampInSecond$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void temperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void temperatureUnit$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void weatherCondition$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) CompactWeatherInfo.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            CompactWeatherInfo compactWeatherInfo = (CompactWeatherInfo) obj;
            return this.expiredTimeStampInSecond == compactWeatherInfo.expiredTimeStampInSecond && this.temperatureUnit == compactWeatherInfo.temperatureUnit && this.temperature == compactWeatherInfo.temperature && this.weatherCondition == compactWeatherInfo.weatherCondition;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CompactWeatherInfo");
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.expiredTimeStampInSecond;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.expiredTimeStampInSecond);
            jSONObject.put(Constants.PROFILE_KEY_UNIT, this.temperatureUnit.getLogName$blesdk_productionRelease());
            jSONObject.put("temp", Float.valueOf(this.temperature));
            jSONObject.put("cond_id", this.weatherCondition.getId$blesdk_productionRelease());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.temperature;
    }

    @DexIgnore
    public final TemperatureUnit getTemperatureUnit() {
        return this.temperatureUnit;
    }

    @DexIgnore
    public final WeatherCondition getWeatherCondition() {
        return this.weatherCondition;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((int) this.expiredTimeStampInSecond) * 31) + this.temperatureUnit.hashCode()) * 31) + Float.valueOf(this.temperature).hashCode()) * 31) + this.weatherCondition.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.EXPIRED_TIMESTAMP_IN_SECOND, Long.valueOf(this.expiredTimeStampInSecond)), JSONKey.TEMP_UNIT, this.temperatureUnit.getLogName$blesdk_productionRelease()), JSONKey.TEMPERATURE, Float.valueOf(this.temperature)), JSONKey.WEATHER_CONDITION, this.weatherCondition.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.expiredTimeStampInSecond);
        }
        if (parcel != null) {
            parcel.writeString(this.temperatureUnit.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.temperature);
        }
        if (parcel != null) {
            parcel.writeString(this.weatherCondition.name());
        }
    }

    @DexIgnore
    public CompactWeatherInfo(long j, TemperatureUnit temperatureUnit2, float f, WeatherCondition weatherCondition2) {
        wd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        wd4.b(weatherCondition2, "weatherCondition");
        this.expiredTimeStampInSecond = j;
        this.temperatureUnit = temperatureUnit2;
        this.temperature = o90.a(f, 2);
        this.weatherCondition = weatherCondition2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CompactWeatherInfo(Parcel parcel) {
        this(r1, r4, r5, WeatherCondition.valueOf(r7));
        long readLong = parcel.readLong();
        String readString = parcel.readString();
        if (readString != null) {
            TemperatureUnit valueOf = TemperatureUnit.valueOf(readString);
            float readFloat = parcel.readFloat();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }
}
