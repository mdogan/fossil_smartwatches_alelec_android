package com.fossil.blesdk.device.data.music;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicEvent extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MusicAction action;
    @DexIgnore
    public /* final */ MusicActionStatus actionStatus;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MusicEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public MusicEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new MusicEvent(parcel);
        }

        @DexIgnore
        public MusicEvent[] newArray(int i) {
            return new MusicEvent[i];
        }
    }

    @DexIgnore
    public MusicEvent(MusicAction musicAction, MusicActionStatus musicActionStatus) {
        wd4.b(musicAction, "action");
        wd4.b(musicActionStatus, "actionStatus");
        this.action = musicAction;
        this.actionStatus = musicActionStatus;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) MusicEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MusicEvent musicEvent = (MusicEvent) obj;
            return this.action == musicEvent.action && this.actionStatus == musicEvent.actionStatus;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.MusicEvent");
    }

    @DexIgnore
    public final MusicAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final MusicActionStatus getActionStatus() {
        return this.actionStatus;
    }

    @DexIgnore
    public int hashCode() {
        return (this.action.hashCode() * 31) + this.actionStatus.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(new JSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease()), JSONKey.STATUS, this.actionStatus.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
        if (parcel != null) {
            parcel.writeString(this.actionStatus.name());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MusicEvent(Parcel parcel) {
        this(r0, MusicActionStatus.valueOf(r3));
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
            MusicAction valueOf = MusicAction.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
