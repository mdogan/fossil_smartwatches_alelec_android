package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.i20;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceConfigItem extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public static /* final */ int ITEM_HEADER_ENTRY_LENGTH_LENGTH; // = 1;
    @DexIgnore
    public static /* final */ int ITEM_HEADER_ID_LENGTH; // = 2;
    @DexIgnore
    public static /* final */ int ITEM_HEADER_LENGTH; // = 3;
    @DexIgnore
    public /* final */ DeviceConfigKey key;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DeviceConfigItem> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DeviceConfigItem createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                DeviceConfigKey valueOf = DeviceConfigKey.valueOf(readString);
                parcel.setDataPosition(0);
                switch (i20.a[valueOf.ordinal()]) {
                    case 1:
                        return BiometricProfile.CREATOR.createFromParcel(parcel);
                    case 2:
                        return DailyStepConfig.CREATOR.createFromParcel(parcel);
                    case 3:
                        return DailyStepGoalConfig.CREATOR.createFromParcel(parcel);
                    case 4:
                        return DailyCalorieConfig.CREATOR.createFromParcel(parcel);
                    case 5:
                        return DailyCalorieGoalConfig.CREATOR.createFromParcel(parcel);
                    case 6:
                        return DailyTotalActiveMinuteConfig.CREATOR.createFromParcel(parcel);
                    case 7:
                        return DailyActiveMinuteGoalConfig.CREATOR.createFromParcel(parcel);
                    case 8:
                        return DailyDistanceConfig.CREATOR.createFromParcel(parcel);
                    case 9:
                        return InactiveNudgeConfig.CREATOR.createFromParcel(parcel);
                    case 10:
                        return VibeStrengthConfig.CREATOR.createFromParcel(parcel);
                    case 11:
                        return DoNotDisturbScheduleConfig.CREATOR.createFromParcel(parcel);
                    case 12:
                        return TimeConfig.CREATOR.createFromParcel(parcel);
                    case 13:
                        return BatteryConfig.CREATOR.createFromParcel(parcel);
                    case 14:
                        return HeartRateModeConfig.CREATOR.createFromParcel(parcel);
                    case 15:
                        return DailySleepConfig.CREATOR.createFromParcel(parcel);
                    case 16:
                        return DisplayUnitConfig.CREATOR.createFromParcel(parcel);
                    case 17:
                        return SecondTimezoneOffsetConfig.CREATOR.createFromParcel(parcel);
                    case 18:
                        return CurrentHeartRateConfig.CREATOR.createFromParcel(parcel);
                    default:
                        throw new NoWhenBranchMatchedException();
                }
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public DeviceConfigItem[] newArray(int i) {
            return new DeviceConfigItem[i];
        }
    }

    @DexIgnore
    public DeviceConfigItem(DeviceConfigKey deviceConfigKey) {
        wd4.b(deviceConfigKey, "key");
        this.key = deviceConfigKey;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.key == ((DeviceConfigItem) obj).key;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DeviceConfigItem");
    }

    @DexIgnore
    public final byte[] getDataConfig$blesdk_productionRelease() {
        byte[] array = ByteBuffer.allocate(getDataContent().length + 3).put(i()).put(getDataContent()).array();
        wd4.a((Object) array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] getDataContent();

    @DexIgnore
    public final DeviceConfigKey getKey() {
        return this.key;
    }

    @DexIgnore
    public int hashCode() {
        return this.key.hashCode();
    }

    @DexIgnore
    public final byte[] i() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.key.getEntryId$blesdk_productionRelease()).put((byte) getDataContent().length).array();
        wd4.a((Object) array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            xa0.a(jSONObject, JSONKey.KEY, this.key.getLogName$blesdk_productionRelease());
            xa0.a(jSONObject, JSONKey.VALUE, valueDescription());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract Object valueDescription();

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.key.name());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DeviceConfigItem(Parcel parcel) {
        this(DeviceConfigKey.valueOf(r2));
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
        } else {
            wd4.a();
            throw null;
        }
    }
}
