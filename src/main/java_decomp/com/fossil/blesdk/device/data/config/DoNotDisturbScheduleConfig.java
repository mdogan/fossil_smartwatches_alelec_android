package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DoNotDisturbScheduleConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_HOUR; // = 23;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_MINUTE; // = 59;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_HOUR; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MINUTE; // = 0;
    @DexIgnore
    public /* final */ byte startHour;
    @DexIgnore
    public /* final */ byte startMinute;
    @DexIgnore
    public /* final */ byte stopHour;
    @DexIgnore
    public /* final */ byte stopMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DoNotDisturbScheduleConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DoNotDisturbScheduleConfig a(byte[] bArr) throws IllegalArgumentException {
            wd4.b(bArr, "rawData");
            if (bArr.length == 4) {
                return new DoNotDisturbScheduleConfig(bArr[0], bArr[1], bArr[2], bArr[3]);
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 4");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DoNotDisturbScheduleConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new DoNotDisturbScheduleConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public DoNotDisturbScheduleConfig[] newArray(int i) {
            return new DoNotDisturbScheduleConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DoNotDisturbScheduleConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wd4.a((Object) DoNotDisturbScheduleConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DoNotDisturbScheduleConfig doNotDisturbScheduleConfig = (DoNotDisturbScheduleConfig) obj;
            return this.startHour == doNotDisturbScheduleConfig.startHour && this.startMinute == doNotDisturbScheduleConfig.startMinute && this.stopHour == doNotDisturbScheduleConfig.stopHour && this.stopMinute == doNotDisturbScheduleConfig.stopMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DoNotDisturbScheduleConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.startHour).put(this.startMinute).put(this.stopHour).put(this.stopMinute).array();
        wd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.startHour;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.startMinute;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.stopHour;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.stopMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((super.hashCode() * 31) + this.startHour) * 31) + this.startMinute) * 31) + this.stopHour) * 31) + this.stopMinute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        byte b = this.startHour;
        boolean z = true;
        if (b >= 0 && 23 >= b) {
            byte b2 = this.startMinute;
            if (b2 >= 0 && 59 >= b2) {
                byte b3 = this.stopHour;
                if (b3 >= 0 && 23 >= b3) {
                    byte b4 = this.stopMinute;
                    if (b4 < 0 || 59 < b4) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalArgumentException("stopMinute(" + this.stopMinute + ") is out of range " + "[0, 59].");
                    }
                    return;
                }
                throw new IllegalArgumentException("stopHour(" + this.stopHour + ") is out of range " + "[0, 23].");
            }
            throw new IllegalArgumentException("startMinute(" + this.startMinute + ") is out of range " + "[0, 59].");
        }
        throw new IllegalArgumentException("startHour(" + this.startHour + ") is out of range " + "[0, 23].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.startHour);
        }
        if (parcel != null) {
            parcel.writeByte(this.startMinute);
        }
        if (parcel != null) {
            parcel.writeByte(this.stopHour);
        }
        if (parcel != null) {
            parcel.writeByte(this.stopMinute);
        }
    }

    @DexIgnore
    public DoNotDisturbScheduleConfig(byte b, byte b2, byte b3, byte b4) throws IllegalArgumentException {
        super(DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE);
        this.startHour = b;
        this.startMinute = b2;
        this.stopHour = b3;
        this.stopMinute = b4;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.startHour));
            jSONObject.put("start_minute", Byte.valueOf(this.startMinute));
            jSONObject.put("stop_hour", Byte.valueOf(this.stopHour));
            jSONObject.put("stop_minute", Byte.valueOf(this.stopMinute));
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public DoNotDisturbScheduleConfig(Parcel parcel) {
        super(parcel);
        this.startHour = parcel.readByte();
        this.startMinute = parcel.readByte();
        this.stopHour = parcel.readByte();
        this.stopMinute = parcel.readByte();
        j();
    }
}
