package com.fossil.blesdk.device.data.commutetime;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeInfo extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_TRAFFIC_LENGTH; // = 10;
    @DexIgnore
    public /* final */ int commuteTimeInMinute;
    @DexIgnore
    public /* final */ String destination;
    @DexIgnore
    public /* final */ String traffic;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeInfo createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeInfo(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeInfo[] newArray(int i) {
            return new CommuteTimeInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeInfo(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void commuteTimeInMinute$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void destination$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void traffic$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.commuteTimeInMinute;
    }

    @DexIgnore
    public final String getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.traffic;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (!(this.traffic.length() <= 10)) {
            throw new IllegalArgumentException(("traffic(" + this.traffic + ") length must be less than or equal to 10").toString());
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.DESTINATION, this.destination), JSONKey.COMMUTE_TIME_IN_MINUTE, Integer.valueOf(this.commuteTimeInMinute)), JSONKey.TRAFFIC, this.traffic);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeInfo(Parcel parcel) {
        this(r0, r2, r4);
        String readString = parcel.readString();
        if (readString != null) {
            int readInt = parcel.readInt();
            String readString2 = parcel.readString();
            if (readString2 != null) {
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public CommuteTimeInfo(String str, int i, String str2) throws IllegalArgumentException {
        wd4.b(str, ShareConstants.DESTINATION);
        wd4.b(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
        i();
    }
}
