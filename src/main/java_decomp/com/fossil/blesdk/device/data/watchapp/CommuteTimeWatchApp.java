package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchApp extends WatchApp {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeWatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchApp createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new CommuteTimeWatchApp(parcel, (rd4) null);
        }

        @DexIgnore
        public CommuteTimeWatchApp[] newArray(int i) {
            return new CommuteTimeWatchApp[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeWatchApp(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void dataConfig$annotations() {
    }

    @DexIgnore
    public final CommuteTimeWatchAppDataConfig getDataConfig() {
        WatchAppDataConfig mDataConfig = super.getMDataConfig();
        if (mDataConfig != null) {
            return (CommuteTimeWatchAppDataConfig) mDataConfig;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public JSONObject getDataConfigJSONObject$blesdk_productionRelease() {
        JSONObject put = super.getDataConfigJSONObject$blesdk_productionRelease().put("commuteApp._.config.destinations", j90.a(getDataConfig().getDestinations()));
        wd4.a((Object) put, "super.getDataConfigJSONO\u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchApp(CommuteTimeWatchAppDataConfig commuteTimeWatchAppDataConfig) {
        super(WatchAppId.COMMUTE, (ButtonEvent) null, commuteTimeWatchAppDataConfig, 2, (rd4) null);
        wd4.b(commuteTimeWatchAppDataConfig, "dataConfig");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchApp(CommuteTimeWatchAppDataConfig commuteTimeWatchAppDataConfig, ButtonEvent buttonEvent) {
        super(WatchAppId.COMMUTE, buttonEvent, commuteTimeWatchAppDataConfig);
        wd4.b(commuteTimeWatchAppDataConfig, "dataConfig");
        wd4.b(buttonEvent, "buttonEvent");
    }

    @DexIgnore
    public CommuteTimeWatchApp(Parcel parcel) {
        super(parcel);
    }
}
