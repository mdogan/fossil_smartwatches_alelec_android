package com.fossil.blesdk.device;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.w00;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$makeDeviceReady$Anon2 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ w00 $task;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$makeDeviceReady$Anon2(DeviceImplementation deviceImplementation, w00 w00) {
        super(1);
        this.this$Anon0 = deviceImplementation;
        this.$task = w00;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "executedPhase");
        this.this$Anon0.h = false;
        this.this$Anon0.q = ((MakeDeviceReadyPhase) phase).H();
        this.this$Anon0.a(Device.State.CONNECTED);
        this.$task.c(this.this$Anon0.getDeviceInformation());
    }
}
