package com.fossil.blesdk.device;

import com.fossil.blesdk.device.FeatureError;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.ab4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.fitness.FitnessData;
import kotlin.Pair;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ g90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public a(DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1, Object obj) {
            this.e = deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1;
            this.f = obj;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.c(this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 e;

        @DexIgnore
        public b(DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1) {
            this.e = deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.b(new FeatureError(FeatureErrorCode.UNKNOWN_ERROR, (Phase.Result) null, 2, (rd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 e;
        @DexIgnore
        public /* final */ /* synthetic */ Phase f;

        @DexIgnore
        public c(DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1 deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1, Phase phase) {
            this.e = deviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1;
            this.f = phase;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.b(FeatureError.Companion.a(this.f.k(), dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, Boolean.valueOf(this.e.this$Anon0.q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90 g90, DeviceImplementation deviceImplementation, Phase phase) {
        super(1);
        this.$progressTask = g90;
        this.this$Anon0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "executedPhase");
        if (phase.k().getResultCode() == Phase.Result.ResultCode.SUCCESS) {
            Object i = phase.i();
            if (i instanceof FitnessData[]) {
                this.this$Anon0.f.post(new a(this, i));
            } else {
                this.this$Anon0.f.post(new b(this));
            }
        } else {
            this.this$Anon0.f.post(new c(this, phase));
            this.this$Anon0.a(phase.k());
        }
    }
}
