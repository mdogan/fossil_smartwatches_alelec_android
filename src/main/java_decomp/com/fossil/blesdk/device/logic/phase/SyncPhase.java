package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.a50;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.z00;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.fitness.BinaryFile;
import com.fossil.fitness.FitnessAlgorithm;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.Result;
import com.fossil.fitness.StatusCode;
import com.fossil.fitness.UserProfile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SyncPhase extends GetFilePhase {
    @DexIgnore
    public /* final */ boolean R; // = true;
    @DexIgnore
    public FitnessData[] S;
    @DexIgnore
    public StatusCode T;
    @DexIgnore
    public /* final */ BiometricProfile U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SyncPhase(Peripheral peripheral, Phase.a aVar, BiometricProfile biometricProfile, HashMap<GetFilePhase.GetFileOption, Object> hashMap, String str) {
        super(peripheral, aVar, PhaseId.SYNC, a50.b.a(peripheral.k(), FileType.ACTIVITY_FILE), r5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r7, 32, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(biometricProfile, "biometricProfile");
        HashMap<GetFilePhase.GetFileOption, Object> hashMap2 = hashMap;
        wd4.b(hashMap2, "options");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        this.U = biometricProfile;
    }

    @DexIgnore
    public final void b(ArrayList<DeviceFile> arrayList) {
        String str;
        ArrayList arrayList2 = new ArrayList(pb4.a(arrayList, 10));
        for (DeviceFile deviceFile : arrayList) {
            arrayList2.add(new BinaryFile(deviceFile.getRawData(), (int) (deviceFile.getCreatedTimeStamp() / ((long) 1000))));
        }
        Result parse = FitnessAlgorithm.create().parse(new ArrayList(arrayList2), new UserProfile((short) this.U.getAge(), this.U.getGender().toFitnessAlgorithmGender$blesdk_productionRelease(), ((float) this.U.getHeightInCentimeter()) / 100.0f, (float) this.U.getWeightInKilogram()));
        wd4.a((Object) parse, "parsedResult");
        this.T = parse.getStatus();
        if (parse.getStatus() == StatusCode.SUCCESS) {
            ArrayList<FitnessData> fitnessData = parse.getFitnessData();
            wd4.a((Object) fitnessData, "parsedResult.fitnessData");
            Object[] array = fitnessData.toArray(new FitnessData[0]);
            if (array != null) {
                this.S = (FitnessData[]) array;
                u90 u90 = u90.c;
                String r = r();
                StringBuilder sb = new StringBuilder();
                sb.append("Sync Result: ");
                FitnessData[] fitnessDataArr = this.S;
                if (fitnessDataArr != null) {
                    JSONArray a = m90.a(fitnessDataArr);
                    if (a != null) {
                        str = a.toString(2);
                        sb.append(str);
                        u90.a(r, sb.toString());
                        return;
                    }
                }
                str = null;
                sb.append(str);
                u90.a(r, sb.toString());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        u90 u902 = u90.c;
        String r2 = r();
        u902.a(r2, "Sync Result: FAIL, status " + parse.getStatus());
    }

    @DexIgnore
    public boolean c() {
        return this.R;
    }

    @DexIgnore
    public void t() {
        if (z00.f.b(e().getDeviceInformation())) {
            Phase.a((Phase) this, (Phase) new LegacySyncPhase(j(), e(), this.U, false, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 32, (rd4) null), (jd4) new SyncPhase$onStart$Anon1(this), (jd4) new SyncPhase$onStart$Anon2(this), (kd4) new SyncPhase$onStart$Anon3(this), (jd4) null, (jd4) null, 48, (Object) null);
            return;
        }
        super.t();
    }

    @DexIgnore
    public JSONObject u() {
        JSONObject put = super.u().put(DeviceConfigKey.BIOMETRIC_PROFILE.getLogName$blesdk_productionRelease(), this.U.valueDescription());
        wd4.a((Object) put, "super.optionDescription(\u2026ofile.valueDescription())");
        return put;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r2 != null) goto L_0x003f;
     */
    @DexIgnore
    public JSONObject x() {
        Object obj;
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.FITNESS_DATA;
        FitnessData[] fitnessDataArr = this.S;
        JSONObject a = xa0.a(x, jSONKey, fitnessDataArr != null ? m90.a(fitnessDataArr) : null);
        JSONKey jSONKey2 = JSONKey.MSL_STATUS_CODE;
        StatusCode statusCode = this.T;
        if (statusCode != null) {
            String name = statusCode.name();
            if (name != null) {
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    obj = name.toLowerCase(locale);
                    wd4.a(obj, "(this as java.lang.String).toLowerCase(locale)");
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        obj = JSONObject.NULL;
        return xa0.a(a, jSONKey2, obj);
    }

    @DexIgnore
    public void a(ArrayList<DeviceFile> arrayList) {
        wd4.b(arrayList, "filesData");
        b(arrayList);
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public FitnessData[] i() {
        FitnessData[] fitnessDataArr = this.S;
        return fitnessDataArr != null ? fitnessDataArr : new FitnessData[0];
    }
}
