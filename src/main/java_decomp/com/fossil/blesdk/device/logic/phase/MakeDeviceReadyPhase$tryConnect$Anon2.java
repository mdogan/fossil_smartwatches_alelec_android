package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r60;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$tryConnect$Anon2 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ MakeDeviceReadyPhase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements jd4<Request, cb4> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public Anon1() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return cb4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            wd4.b(request, "it");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements jd4<Request, cb4> {
        @DexIgnore
        public static /* final */ Anon2 INSTANCE; // = new Anon2();

        @DexIgnore
        public Anon2() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return cb4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            wd4.b(request, "it");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 extends Lambda implements jd4<Request, cb4> {
        @DexIgnore
        public /* final */ /* synthetic */ MakeDeviceReadyPhase$tryConnect$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(MakeDeviceReadyPhase$tryConnect$Anon2 makeDeviceReadyPhase$tryConnect$Anon2) {
            super(1);
            this.this$Anon0 = makeDeviceReadyPhase$tryConnect$Anon2;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return cb4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            wd4.b(request, "it");
            this.this$Anon0.this$Anon0.M();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$tryConnect$Anon2(MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$Anon0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        this.this$Anon0.I = System.currentTimeMillis();
        MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$Anon0;
        makeDeviceReadyPhase.b(Phase.Result.copy$default(makeDeviceReadyPhase.k(), (PhaseId) null, Phase.Result.ResultCode.Companion.a(request.n()), request.n(), 1, (Object) null));
        MakeDeviceReadyPhase makeDeviceReadyPhase2 = this.this$Anon0;
        makeDeviceReadyPhase2.J = makeDeviceReadyPhase2.I - this.this$Anon0.H < 1000 && request.n().getCommandResult().getGattResult().getBluetoothGattStatus() == 133;
        r60 r60 = new r60(this.this$Anon0.j());
        r60.b(false);
        Phase.a((Phase) this.this$Anon0, (Request) r60, (jd4) Anon1.INSTANCE, (jd4) Anon2.INSTANCE, (kd4) null, (jd4) new Anon3(this), (jd4) null, 40, (Object) null);
    }
}
