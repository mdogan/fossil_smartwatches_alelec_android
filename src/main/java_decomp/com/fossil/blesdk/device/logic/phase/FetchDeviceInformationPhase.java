package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.t50;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.z60;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FetchDeviceInformationPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A;
    @DexIgnore
    public DeviceInformation z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ FetchDeviceInformationPhase(Peripheral peripheral, Phase.a aVar, String str, int i, rd4 rd4) {
        this(peripheral, aVar, str);
        if ((i & 4) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Phase) new t50(j(), e(), 0, l(), 4, (rd4) null), (jd4) new FetchDeviceInformationPhase$readDeviceInformationFile$Anon1(this), (jd4) new FetchDeviceInformationPhase$readDeviceInformationFile$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Phase) new ReadDeviceInformationByCharacteristicPhase(j(), e(), l()), (jd4) new FetchDeviceInformationPhase$readDeviceInformationFromCharacteristics$Anon1(this), (jd4) new FetchDeviceInformationPhase$readDeviceInformationFromCharacteristics$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new z60(j()), (jd4) new FetchDeviceInformationPhase$readSoftwareRevision$Anon1(this), (jd4) new FetchDeviceInformationPhase$readSoftwareRevision$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        C();
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(super.x(), JSONKey.DEVICE_INFO, this.z.toJSONObject());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public FetchDeviceInformationPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(r1, r2, PhaseId.FETCH_DEVICE_INFORMATION, r3);
        Peripheral peripheral2 = peripheral;
        Phase.a aVar2 = aVar;
        String str2 = str;
        wd4.b(peripheral2, "peripheral");
        wd4.b(aVar2, "delegate");
        wd4.b(str2, "phaseUuid");
        this.z = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (rd4) null);
        this.A = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));
    }

    @DexIgnore
    public DeviceInformation i() {
        return this.z;
    }
}
