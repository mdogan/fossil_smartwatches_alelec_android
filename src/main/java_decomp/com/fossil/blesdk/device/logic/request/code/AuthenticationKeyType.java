package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AuthenticationKeyType {
    PRE_SHARED_KEY((byte) 0),
    SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY((byte) 1),
    SIXTEEN_BYTES_LSB_ECDH_SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a(AuthenticationKeyType authenticationKeyType, byte[] bArr) {
            wd4.b(authenticationKeyType, "keyType");
            wd4.b(bArr, "data");
            int i = o70.a[authenticationKeyType.ordinal()];
            if (i == 1) {
                return lb4.c(bArr);
            }
            if (i == 2 || i == 3) {
                return bArr;
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    AuthenticationKeyType(byte b) {
        this.id = b;
        String name = name();
        if (name != null) {
            String lowerCase = name.toLowerCase();
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
