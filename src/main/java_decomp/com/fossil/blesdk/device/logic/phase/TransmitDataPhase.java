package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.TransferDataRequest;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.g60;
import com.fossil.blesdk.obfuscated.h80;
import com.fossil.blesdk.obfuscated.i60;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k80;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l60;
import com.fossil.blesdk.obfuscated.m80;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class TransmitDataPhase extends g60 {
    @DexIgnore
    public byte[] B;
    @DexIgnore
    public long C;
    @DexIgnore
    public float D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public int H;
    @DexIgnore
    public GattCharacteristic.CharacteristicId I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public long L;
    @DexIgnore
    public long M;
    @DexIgnore
    public /* final */ boolean N;
    @DexIgnore
    public /* final */ float O;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ TransmitDataPhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, boolean z, short s, float f, String str, int i, rd4 rd4) {
        this(peripheral, aVar, phaseId, z, s, r7, r8);
        String str2;
        float f2 = (i & 32) != 0 ? 0.001f : f;
        if ((i & 64) != 0) {
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    public final h80 B() {
        h80 h80 = new h80(A(), j(), 0, 4, (rd4) null);
        h80.c((jd4<? super Request, cb4>) new TransmitDataPhase$buildGetFileSizeWrittenRequest$Anon1(this));
        return h80;
    }

    @DexIgnore
    public final k80 C() {
        if (this.J == 0) {
            this.J = System.currentTimeMillis();
        }
        k80 k80 = new k80(this.F, G() - this.F, G(), A(), j(), 0, 32, (rd4) null);
        k80.c((jd4<? super Request, cb4>) new TransmitDataPhase$buildPutFileRequest$Anon1(this));
        return k80;
    }

    @DexIgnore
    public final TransferDataRequest D() {
        if (this.K == 0) {
            this.K = System.currentTimeMillis();
        }
        if (this.L == 0) {
            this.L = System.currentTimeMillis();
        }
        long G2 = G() - this.F;
        TransferDataRequest transferDataRequest = new TransferDataRequest(A(), new l60(kb4.a(this.B, (int) this.F, (int) G()), j().l(), this.I), j());
        transferDataRequest.a((kd4<? super Request, ? super Float, cb4>) new TransmitDataPhase$buildTransferDataRequest$transferDataRequest$Anon1(this, G2));
        transferDataRequest.c((jd4<? super Request, cb4>) new TransmitDataPhase$buildTransferDataRequest$transferDataRequest$Anon2(this));
        transferDataRequest.b((jd4<? super Request, cb4>) new TransmitDataPhase$buildTransferDataRequest$transferDataRequest$Anon3(this));
        TransferDataRequest transferDataRequest2 = transferDataRequest;
        transferDataRequest2.a(h());
        return transferDataRequest2;
    }

    @DexIgnore
    public final m80 E() {
        m80 H2 = H();
        H2.c((jd4<? super Request, cb4>) new TransmitDataPhase$buildVerifyFileRequest$Anon1(this));
        if (H2 != null) {
            return H2;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.logic.request.file.VerifyFileRequest");
    }

    @DexIgnore
    public abstract byte[] F();

    @DexIgnore
    public final long G() {
        return o90.b(this.B.length);
    }

    @DexIgnore
    public m80 H() {
        return new m80(A(), j());
    }

    @DexIgnore
    public void I() {
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public final boolean J() {
        if (this.G) {
            this.G = false;
            return true;
        } else if ((((float) (this.F - this.E)) * 1.0f) / ((float) G()) > 0.01f) {
            this.H = 0;
            return true;
        } else {
            this.H++;
            if (this.H < 3) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public final void K() {
        if (!(!(this.B.length == 0))) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (this.N) {
            Phase.a(this, RequestId.PUT_FILE, (RequestId) null, 2, (Object) null);
        } else {
            Phase.a(this, RequestId.GET_FILE_SIZE_WRITTEN, (RequestId) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public final void L() {
        Peripheral j = j();
        Phase.a e = e();
        String k = j().k();
        short A = A();
        byte[] bArr = this.B;
        Phase.a((Phase) this, (Phase) new FindCorrectOffsetPhase(j, e, new DeviceFile(k, A, bArr, o90.b(bArr.length), Crc32Calculator.a.a(this.B, Crc32Calculator.CrcType.CRC32)), l()), (jd4) new TransmitDataPhase$startVerifyData$Anon1(this), (jd4) new TransmitDataPhase$startVerifyData$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void M() {
        if (J()) {
            Phase.a(this, RequestId.PUT_FILE, (RequestId) null, 2, (Object) null);
        } else {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.DATA_TRANSFER_RETRY_REACH_THRESHOLD, (Request.Result) null, 5, (Object) null));
        }
    }

    @DexIgnore
    public void t() {
        K();
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(xa0.a(super.u(), JSONKey.FILE_SIZE, Integer.valueOf(this.B.length)), JSONKey.FILE_CRC, Long.valueOf(Crc32Calculator.a.a(this.B, Crc32Calculator.CrcType.CRC32))), JSONKey.SKIP_RESUME, Boolean.valueOf(this.N));
    }

    @DexIgnore
    public void v() {
        try {
            this.B = F();
            this.C = 0;
            this.E = 0;
            this.F = 0;
            this.G = true;
            this.H = 0;
            this.I = GattCharacteristic.CharacteristicId.UNKNOWN;
        } catch (FileFormatException e) {
            ea0.l.a(e);
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INCOMPATIBLE_FIRMWARE, (Request.Result) null, 5, (Object) null));
        }
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(xa0.a(super.x(), JSONKey.ERASE_DURATION_IN_MS, Long.valueOf(Math.max(this.K - this.J, 0))), JSONKey.TRANSFER_DURATION_IN_MS, Long.valueOf(Math.max(this.M - this.L, 0)));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransmitDataPhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, boolean z, short s, float f, String str) {
        super(peripheral, aVar, phaseId, s, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(phaseId, "id");
        wd4.b(str, "phaseUuid");
        this.N = z;
        this.O = f;
        this.B = new byte[0];
        this.G = true;
        this.I = GattCharacteristic.CharacteristicId.UNKNOWN;
    }

    @DexIgnore
    public final void b(long j) {
        this.E = this.F;
        this.F = j;
        this.C = this.F;
    }

    @DexIgnore
    public final void a(long j, long j2) {
        long G2 = G();
        if (1 <= j && G2 >= j) {
            long a2 = Crc32Calculator.a.a(this.B, Crc32Calculator.CrcType.CRC32);
            b(j);
            if (j2 != a2) {
                L();
            } else if (j == G()) {
                Phase.a(this, RequestId.VERIFY_FILE, (RequestId) null, 2, (Object) null);
            } else {
                M();
            }
        } else {
            b(0);
            M();
        }
    }

    @DexIgnore
    public final void a(FindCorrectOffsetPhase findCorrectOffsetPhase) {
        b(findCorrectOffsetPhase.B());
        this.C = this.F;
        float G2 = (((float) this.C) * 1.0f) / ((float) G());
        if (Math.abs(G2 - this.D) > this.O || G2 == 1.0f) {
            this.D = G2;
            a(G2);
        }
        Phase.a(this, RequestId.PUT_FILE, (RequestId) null, 2, (Object) null);
    }

    @DexIgnore
    public Request a(RequestId requestId) {
        wd4.b(requestId, "requestId");
        int i = i60.a[requestId.ordinal()];
        if (i == 1) {
            return B();
        }
        if (i == 2) {
            return C();
        }
        if (i == 3) {
            return E();
        }
        if (i != 4) {
            return null;
        }
        return D();
    }
}
