package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectHIDPhase$disconnectHID$Anon3 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ ConnectHIDPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectHIDPhase$disconnectHID$Anon3(ConnectHIDPhase connectHIDPhase) {
        super(1);
        this.this$Anon0 = connectHIDPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "it");
        ConnectHIDPhase connectHIDPhase = this.this$Anon0;
        connectHIDPhase.a(connectHIDPhase.k());
    }
}
