package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationFilterPhase$transmitIconFile$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ SetNotificationFilterPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFilterPhase$transmitIconFile$Anon1(SetNotificationFilterPhase setNotificationFilterPhase) {
        super(1);
        this.this$Anon0 = setNotificationFilterPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "it");
        this.this$Anon0.B();
    }
}
