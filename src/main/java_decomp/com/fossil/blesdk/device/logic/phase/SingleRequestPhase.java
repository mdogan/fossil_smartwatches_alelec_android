package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.TransferDataRequest;
import com.fossil.blesdk.device.logic.request.file.FileDataRequest;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.e70;
import com.fossil.blesdk.obfuscated.f80;
import com.fossil.blesdk.obfuscated.g70;
import com.fossil.blesdk.obfuscated.i70;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k70;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o80;
import com.fossil.blesdk.obfuscated.p60;
import com.fossil.blesdk.obfuscated.q70;
import com.fossil.blesdk.obfuscated.r80;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u60;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x60;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SingleRequestPhase extends Phase {
    @DexIgnore
    public /* final */ Request A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = k90.a(super.n(), A());

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SingleRequestPhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, Request request) {
        super(peripheral, aVar, phaseId, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(phaseId, "phaseId");
        wd4.b(request, "request");
        this.A = request;
    }

    @DexIgnore
    public final ArrayList<ResourceType> A() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Request request = this.A;
        if (!(request instanceof x60) && !(request instanceof i70)) {
            if (request instanceof q70) {
                linkedHashSet.add(ResourceType.DEVICE_CONFIG);
            } else if (request instanceof FileDataRequest) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof TransferDataRequest) {
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof f80) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
            } else if ((request instanceof r80) || (request instanceof o80)) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof k70) {
                linkedHashSet.add(ResourceType.AUTHENTICATION);
            } else if ((request instanceof u60) || (request instanceof e70)) {
                linkedHashSet.add(ResourceType.ASYNC);
            } else if (request instanceof p60) {
                linkedHashSet.add(((p60) request).B().getResourceType$blesdk_productionRelease());
            } else if (request instanceof g70) {
                linkedHashSet.add(((g70) request).B().getResourceType$blesdk_productionRelease());
                linkedHashSet.add(((g70) this.A).E().getResourceType$blesdk_productionRelease());
            } else {
                linkedHashSet.add(ResourceType.DEVICE_INFORMATION);
            }
        }
        return new ArrayList<>(linkedHashSet);
    }

    @DexIgnore
    public void c(Request request) {
        wd4.b(request, "request");
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public final void t() {
        Phase.a((Phase) this, this.A, (jd4) new SingleRequestPhase$onStart$Anon1(this), (jd4) SingleRequestPhase$onStart$Anon2.INSTANCE, (kd4) null, (jd4) new SingleRequestPhase$onStart$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject u() {
        return n90.a(super.u(), this.A.t());
    }
}
