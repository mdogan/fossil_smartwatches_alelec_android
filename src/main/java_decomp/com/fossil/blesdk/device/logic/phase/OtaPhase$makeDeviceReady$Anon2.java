package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaPhase$makeDeviceReady$Anon2 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ OtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OtaPhase$makeDeviceReady$Anon2(OtaPhase otaPhase) {
        super(1);
        this.this$Anon0 = otaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "completedPhase");
        this.this$Anon0.R = System.currentTimeMillis();
        this.this$Anon0.b(Phase.Result.copy$default(phase.k(), this.this$Anon0.k().getPhaseId(), (Phase.Result.ResultCode) null, (Request.Result) null, 6, (Object) null));
        this.this$Anon0.a(1.0f);
        this.this$Anon0.P();
    }
}
