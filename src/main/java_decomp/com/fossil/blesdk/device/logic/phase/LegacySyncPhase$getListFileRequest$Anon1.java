package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.v80;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getListFileRequest$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacySyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getListFileRequest$Anon1(LegacySyncPhase legacySyncPhase) {
        super(1);
        this.this$Anon0 = legacySyncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        v80 v80 = (v80) request;
        this.this$Anon0.A = v80.K();
        this.this$Anon0.B = v80.L();
        this.this$Anon0.C = 0;
        if (this.this$Anon0.A <= 0) {
            LegacySyncPhase legacySyncPhase = this.this$Anon0;
            legacySyncPhase.a(Phase.Result.copy$default(legacySyncPhase.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (this.this$Anon0.I) {
            this.this$Anon0.B();
        } else {
            this.this$Anon0.D();
        }
    }
}
