package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$getFile$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GetFilePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$getFile$Anon1(GetFilePhase getFilePhase) {
        super(0);
        this.this$Anon0 = getFilePhase;
    }

    @DexIgnore
    public final void invoke() {
        DeviceFile c = this.this$Anon0.H();
        if (c != null) {
            FileHandle fileHandle = new FileHandle(c.getFileHandle$blesdk_productionRelease());
            DeviceFile a = this.this$Anon0.a(fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease());
            if (a == null) {
                a = this.this$Anon0.H();
                if (a == null) {
                    wd4.a();
                    throw null;
                }
            }
            this.this$Anon0.b(a);
            return;
        }
        wd4.a();
        throw null;
    }
}
