package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.TransferDataRequest;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d80;
import com.fossil.blesdk.obfuscated.f80;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m80;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$tryExecuteRequest$Anon3 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ id4 $nextAction;
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements jd4<Request, cb4> {
        @DexIgnore
        public /* final */ /* synthetic */ Request $originalRequest;
        @DexIgnore
        public /* final */ /* synthetic */ Phase$tryExecuteRequest$Anon3 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Phase$tryExecuteRequest$Anon3 phase$tryExecuteRequest$Anon3, Request request) {
            super(1);
            this.this$Anon0 = phase$tryExecuteRequest$Anon3;
            this.$originalRequest = request;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return cb4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            wd4.b(request, "it");
            Request request2 = this.$originalRequest;
            if ((request2 instanceof m80) || request2.n().getResultCode() == Request.Result.ResultCode.INTERRUPTED) {
                this.this$Anon0.this$Anon0.a(this.$originalRequest.n());
            } else {
                this.this$Anon0.$nextAction.invoke();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements jd4<Request, cb4> {
        @DexIgnore
        public /* final */ /* synthetic */ Phase$tryExecuteRequest$Anon3 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(Phase$tryExecuteRequest$Anon3 phase$tryExecuteRequest$Anon3) {
            super(1);
            this.this$Anon0 = phase$tryExecuteRequest$Anon3;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return cb4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            wd4.b(request, "abortRequest");
            if (this.this$Anon0.this$Anon0.k().getResultCode() == Phase.Result.ResultCode.INTERRUPTED) {
                Phase phase = this.this$Anon0.this$Anon0;
                phase.a(phase.k());
                return;
            }
            this.this$Anon0.this$Anon0.a(request.n());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$tryExecuteRequest$Anon3(Phase phase, id4 id4) {
        super(1);
        this.this$Anon0 = phase;
        this.$nextAction = id4;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Short sh;
        wd4.b(request, "originalRequest");
        Phase phase = this.this$Anon0;
        phase.m = phase.m + 1;
        Phase phase2 = this.this$Anon0;
        phase2.b(Phase.Result.copy$default(phase2.k(), (PhaseId) null, Phase.Result.ResultCode.Companion.a(request.n()), request.n(), 1, (Object) null));
        if (request.n().getResultCode() == Request.Result.ResultCode.CONNECTION_DROPPED || request.n().getResultCode() == Request.Result.ResultCode.BLUETOOTH_OFF || request.n().getCommandResult().getGattResult().getResultCode() == GattOperationResult.GattResult.ResultCode.GATT_NULL) {
            this.this$Anon0.a(request.n());
            return;
        }
        if (request instanceof f80) {
            sh = Short.valueOf(((f80) request).I());
        } else {
            sh = request instanceof TransferDataRequest ? Short.valueOf(((TransferDataRequest) request).D()) : null;
        }
        if (sh != null) {
            Phase.a(this.this$Anon0, (Request) new d80(sh.shortValue(), this.this$Anon0.j(), 0, 4, (rd4) null), (jd4) new Anon1(this, request), (jd4) new Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
        } else {
            this.$nextAction.invoke();
        }
    }
}
