package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncFlowPhase$runGetHWLogPhase$Anon3 extends Lambda implements kd4<Phase, Float, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ SyncFlowPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase$runGetHWLogPhase$Anon3(SyncFlowPhase syncFlowPhase) {
        super(2);
        this.this$Anon0 = syncFlowPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Phase) obj, ((Number) obj2).floatValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase, float f) {
        wd4.b(phase, "<anonymous parameter 0>");
        if (f == 1.0f) {
            this.this$Anon0.a(f);
            return;
        }
        SyncFlowPhase syncFlowPhase = this.this$Anon0;
        syncFlowPhase.a(syncFlowPhase.F + (f * this.this$Anon0.G));
    }
}
