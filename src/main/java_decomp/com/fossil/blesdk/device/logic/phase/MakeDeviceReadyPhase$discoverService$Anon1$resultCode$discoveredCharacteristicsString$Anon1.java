package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredCharacteristicsString$Anon1 extends Lambda implements jd4<GattCharacteristic.CharacteristicId, String> {
    @DexIgnore
    public static /* final */ MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredCharacteristicsString$Anon1 INSTANCE; // = new MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredCharacteristicsString$Anon1();

    @DexIgnore
    public MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredCharacteristicsString$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(GattCharacteristic.CharacteristicId characteristicId) {
        wd4.b(characteristicId, "characteristicId");
        return characteristicId.name();
    }
}
