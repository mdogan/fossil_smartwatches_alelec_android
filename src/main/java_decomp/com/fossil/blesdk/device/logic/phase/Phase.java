package com.fossil.blesdk.device.logic.phase;

import android.os.Handler;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.logic.PhaseManager;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.aa0;
import com.fossil.blesdk.obfuscated.cb0;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.ib0;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.j50;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k50;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o50;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.p50;
import com.fossil.blesdk.obfuscated.p70;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.z90;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Phase {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public Request b;
    @DexIgnore
    public /* final */ Peripheral.f c;
    @DexIgnore
    public CopyOnWriteArrayList<jd4<Phase, cb4>> d;
    @DexIgnore
    public CopyOnWriteArrayList<jd4<Phase, cb4>> e;
    @DexIgnore
    public CopyOnWriteArrayList<jd4<Phase, cb4>> f;
    @DexIgnore
    public CopyOnWriteArrayList<jd4<Phase, cb4>> g;
    @DexIgnore
    public CopyOnWriteArrayList<kd4<Phase, Float, cb4>> h;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public Priority k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public Phase n;
    @DexIgnore
    public /* final */ Handler o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ cb0 q;
    @DexIgnore
    public /* final */ PhaseManager.a r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Result u;
    @DexIgnore
    public /* final */ Peripheral v;
    @DexIgnore
    public /* final */ a w;
    @DexIgnore
    public /* final */ PhaseId x;
    @DexIgnore
    public String y;

    @DexIgnore
    public enum AuthenticationState {
        NOT_AUTHENTICATED,
        AUTHENTICATING,
        AUTHENTICATED
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        PhaseManager a();

        @DexIgnore
        DeviceInformation getDeviceInformation();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Peripheral.f {
        @DexIgnore
        public /* final */ /* synthetic */ Phase a;

        @DexIgnore
        public b(Phase phase) {
            this.a = phase;
        }

        @DexIgnore
        public void a(Peripheral peripheral, Peripheral.HIDState hIDState, Peripheral.HIDState hIDState2) {
            wd4.b(peripheral, "peripheral");
            wd4.b(hIDState, "previousHIDState");
            wd4.b(hIDState2, "newHIDState");
        }

        @DexIgnore
        public void a(Peripheral peripheral, Peripheral.State state) {
            wd4.b(peripheral, "peripheral");
            wd4.b(state, "newState");
            if (!this.a.s()) {
                this.a.a(state);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements PhaseManager.a {
        @DexIgnore
        public /* final */ /* synthetic */ Phase a;

        @DexIgnore
        public c(Phase phase) {
            this.a = phase;
        }

        @DexIgnore
        public void a() {
            Phase phase = this.a;
            phase.a(phase.q);
            Peripheral j = this.a.j();
            LogLevel logLevel = LogLevel.DEBUG;
            String r = this.a.r();
            Peripheral.a(j, logLevel, r, "onResourceAllocated, " + "phase started: phaseUuid=" + this.a.l() + ", " + "option=" + this.a.u().toString(2), false, 8, (Object) null);
            ea0.l.b(new SdkLogEntry(this.a.g().getLogName$blesdk_productionRelease(), EventType.PHASE_START, this.a.j().k(), this.a.g().getLogName$blesdk_productionRelease(), this.a.l(), true, (String) null, (DeviceInformation) null, (fa0) null, this.a.u(), 448, (rd4) null));
            Phase phase2 = this.a;
            phase2.b(Result.copy$default(phase2.k(), (PhaseId) null, Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
            this.a.j().a(this.a.c);
            for (jd4 invoke : this.a.d) {
                invoke.invoke(this.a);
            }
            this.a.a(System.currentTimeMillis());
            this.a.w();
        }
    }

    @DexIgnore
    public Phase(Peripheral peripheral, a aVar, PhaseId phaseId, String str) {
        Peripheral peripheral2 = peripheral;
        a aVar2 = aVar;
        PhaseId phaseId2 = phaseId;
        String str2 = str;
        wd4.b(peripheral2, "peripheral");
        wd4.b(aVar2, "delegate");
        wd4.b(phaseId2, "id");
        wd4.b(str2, "phaseUuid");
        this.v = peripheral2;
        this.w = aVar2;
        this.x = phaseId2;
        this.y = str2;
        this.a = this.x.getLogName$blesdk_productionRelease();
        this.c = new b(this);
        this.d = new CopyOnWriteArrayList<>();
        this.e = new CopyOnWriteArrayList<>();
        this.f = new CopyOnWriteArrayList<>();
        this.g = new CopyOnWriteArrayList<>();
        this.h = new CopyOnWriteArrayList<>();
        this.i = ob4.a((T[]) new ResourceType[]{ResourceType.DEVICE_INFORMATION});
        AuthenticationState authenticationState = AuthenticationState.NOT_AUTHENTICATED;
        this.k = Priority.NORMAL;
        this.l = System.currentTimeMillis();
        this.o = ib0.a.a();
        this.p = 30000;
        this.q = new cb0(new Phase$resourceAllocationWaitingTimeoutRunnable$Anon1(this));
        this.r = new c(this);
        ea0.l.b(new SdkLogEntry(this.x.getLogName$blesdk_productionRelease(), EventType.PHASE_INIT, this.v.k(), this.x.getLogName$blesdk_productionRelease(), this.y, true, (String) null, (DeviceInformation) null, (fa0) null, (JSONObject) null, 960, (rd4) null));
        this.u = new Result(this.x, Result.ResultCode.NOT_START, (Request.Result) null, 4, (rd4) null);
    }

    @DexIgnore
    public Request a(RequestId requestId) {
        wd4.b(requestId, "requestId");
        return null;
    }

    @DexIgnore
    public boolean a(Request request) {
        return false;
    }

    @DexIgnore
    public final void b(Request request) {
        this.b = request;
    }

    @DexIgnore
    public final boolean h() {
        return c() && this.w.getDeviceInformation().getDeviceSecurityVersion$blesdk_productionRelease().compareTo(va0.y.m()) >= 0;
    }

    @DexIgnore
    public Object i() {
        return cb4.a;
    }

    @DexIgnore
    public final Peripheral j() {
        return this.v;
    }

    @DexIgnore
    public final Result k() {
        return this.u;
    }

    @DexIgnore
    public final String l() {
        return this.y;
    }

    @DexIgnore
    public Priority m() {
        return this.k;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.i;
    }

    @DexIgnore
    public long o() {
        return this.p;
    }

    @DexIgnore
    public final long p() {
        return this.l;
    }

    @DexIgnore
    public final Phase q() {
        return this.n;
    }

    @DexIgnore
    public final String r() {
        return this.a;
    }

    @DexIgnore
    public final boolean s() {
        return this.s;
    }

    @DexIgnore
    public abstract void t();

    @DexIgnore
    public JSONObject u() {
        return new JSONObject();
    }

    @DexIgnore
    public void v() {
    }

    @DexIgnore
    public final void w() {
        v();
        if (!this.s) {
            if (h()) {
                Peripheral peripheral = this.v;
                a(this, (Phase) new AuthenticatePhase(peripheral, this.w, AuthenticationKeyType.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY, d90.b.a(peripheral.k()).a(), this.y), (jd4) new Phase$prepareNecessaryCondition$Anon1(this), (jd4) new Phase$prepareNecessaryCondition$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
                return;
            }
            t();
        }
    }

    @DexIgnore
    public JSONObject x() {
        return new JSONObject();
    }

    @DexIgnore
    public final void y() {
        if (!this.s) {
            Peripheral peripheral = this.v;
            LogLevel logLevel = LogLevel.DEBUG;
            String str = this.a;
            Peripheral.a(peripheral, logLevel, str, "requestResource: " + "phaseUuid=" + this.y + ", requiredResource=" + n(), false, 8, (Object) null);
            z();
            this.w.a().a(this, this.r);
        }
    }

    @DexIgnore
    public final void z() {
        Peripheral peripheral = this.v;
        LogLevel logLevel = LogLevel.DEBUG;
        String str = this.a;
        Peripheral.a(peripheral, logLevel, str, "startResourceAllocationWaitingTimeOut: " + "phaseUuid=" + this.y + ", " + "resourceAllocationWaitingTimeoutInMillis=" + o(), false, 8, (Object) null);
        if (o() > 0) {
            this.o.postDelayed(this.q, o());
        }
    }

    @DexIgnore
    public final void b(Result result) {
        wd4.b(result, "<set-?>");
        this.u = result;
    }

    @DexIgnore
    public boolean c() {
        return this.j;
    }

    @DexIgnore
    public final Request d() {
        return this.b;
    }

    @DexIgnore
    public final a e() {
        return this.w;
    }

    @DexIgnore
    public final Handler f() {
        return this.o;
    }

    @DexIgnore
    public final PhaseId g() {
        return this.x;
    }

    @DexIgnore
    public void a(Priority priority) {
        wd4.b(priority, "<set-?>");
        this.k = priority;
    }

    @DexIgnore
    public final boolean b(Phase phase) {
        wd4.b(phase, "subPhase");
        if (wd4.a((Object) this.n, (Object) phase)) {
            return true;
        }
        Phase phase2 = this.n;
        return phase2 != null && phase2.b(phase);
    }

    @DexIgnore
    public final Phase c(jd4<? super Phase, cb4> jd4) {
        wd4.b(jd4, "actionOnPrepareToStart");
        if (!this.s) {
            this.d.add(jd4);
        }
        return this;
    }

    @DexIgnore
    public final Phase d(jd4<? super Phase, cb4> jd4) {
        wd4.b(jd4, "actionOnPhaseSuccess");
        if (!this.s) {
            this.e.add(jd4);
        } else if (this.u.getResultCode() == Result.ResultCode.SUCCESS) {
            jd4.invoke(this);
        }
        return this;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result extends JSONAbleObject {
        @DexIgnore
        public /* final */ PhaseId phaseId;
        @DexIgnore
        public /* final */ Request.Result requestResult;
        @DexIgnore
        public /* final */ ResultCode resultCode;

        @DexIgnore
        public enum ResultCode {
            SUCCESS(0),
            NOT_START(1),
            REQUEST_ERROR(2),
            LACK_OF_SERVICE(3),
            LACK_OF_CHARACTERISTIC(4),
            INVALID_SERIAL_NUMBER(5),
            DATA_TRANSFER_RETRY_REACH_THRESHOLD(6),
            FLOW_BROKEN(7),
            EXCHANGED_VALUE_NOT_SATISFIED(8),
            CONNECTION_DROPPED(9),
            INVALID_FILE_LENGTH(10),
            MISMATCH_VERSION(11),
            INVALID_FILE_CRC(12),
            INVALID_RESPONSE(13),
            INVALID_DATA_LENGTH(14),
            INCORRECT_FILE_DATA(15),
            WAITING_FOR_EXECUTION_TIMEOUT(16),
            NOT_ENOUGH_FILE_TO_PROCESS(17),
            INCOMPATIBLE_FIRMWARE(18),
            REQUEST_UNSUPPORTED(19),
            UNSUPPORTED_FILE_HANDLE(20),
            BLUETOOTH_OFF(21),
            AUTHENTICATION_FAILED(22),
            INVALID_PARAMETER(23),
            WRONG_RANDOM_NUMBER(24),
            SECRET_KEY_IS_REQUIRED(25),
            NOT_ALLOW_TO_START(26),
            DATABASE_ERROR(27),
            HID_INPUT_DEVICE_DISABLED(257),
            INTERRUPTED(254);
            
            @DexIgnore
            public static /* final */ a Companion; // = null;
            @DexIgnore
            public /* final */ int id;
            @DexIgnore
            public /* final */ String logName;

            @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a {
                @DexIgnore
                public a() {
                }

                @DexIgnore
                public final ResultCode a(Request.Result result) {
                    wd4.b(result, "requestResult");
                    switch (o50.a[result.getResultCode().ordinal()]) {
                        case 1:
                            return ResultCode.SUCCESS;
                        case 2:
                            return ResultCode.INTERRUPTED;
                        case 3:
                            return ResultCode.CONNECTION_DROPPED;
                        case 4:
                            return ResultCode.REQUEST_UNSUPPORTED;
                        case 5:
                            return ResultCode.UNSUPPORTED_FILE_HANDLE;
                        case 6:
                            return ResultCode.BLUETOOTH_OFF;
                        case 7:
                            return ResultCode.REQUEST_UNSUPPORTED;
                        case 8:
                            return ResultCode.HID_INPUT_DEVICE_DISABLED;
                        default:
                            return ResultCode.REQUEST_ERROR;
                    }
                }

                @DexIgnore
                public /* synthetic */ a(rd4 rd4) {
                    this();
                }
            }

            /*
            static {
                Companion = new a((rd4) null);
            }
            */

            @DexIgnore
            ResultCode(int i) {
                this.id = i;
                String name = name();
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    this.logName = lowerCase;
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            @DexIgnore
            public final int getId() {
                return this.id;
            }

            @DexIgnore
            public final String getLogName$blesdk_productionRelease() {
                return this.logName;
            }
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(PhaseId phaseId2, ResultCode resultCode2, Request.Result result, int i, rd4 rd4) {
            this((i & 1) != 0 ? PhaseId.UNKNOWN : phaseId2, resultCode2, (i & 4) != 0 ? new Request.Result((RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (p70) null, 13, (rd4) null) : result);
        }

        @DexIgnore
        public static /* synthetic */ Result copy$default(Result result, PhaseId phaseId2, ResultCode resultCode2, Request.Result result2, int i, Object obj) {
            if ((i & 1) != 0) {
                phaseId2 = result.phaseId;
            }
            if ((i & 2) != 0) {
                resultCode2 = result.resultCode;
            }
            if ((i & 4) != 0) {
                result2 = result.requestResult;
            }
            return result.copy(phaseId2, resultCode2, result2);
        }

        @DexIgnore
        public final PhaseId component1() {
            return this.phaseId;
        }

        @DexIgnore
        public final ResultCode component2() {
            return this.resultCode;
        }

        @DexIgnore
        public final Request.Result component3() {
            return this.requestResult;
        }

        @DexIgnore
        public final Result copy(PhaseId phaseId2, ResultCode resultCode2, Request.Result result) {
            wd4.b(phaseId2, "phaseId");
            wd4.b(resultCode2, "resultCode");
            wd4.b(result, "requestResult");
            return new Result(phaseId2, resultCode2, result);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return wd4.a((Object) this.phaseId, (Object) result.phaseId) && wd4.a((Object) this.resultCode, (Object) result.resultCode) && wd4.a((Object) this.requestResult, (Object) result.requestResult);
        }

        @DexIgnore
        public final PhaseId getPhaseId() {
            return this.phaseId;
        }

        @DexIgnore
        public final Request.Result getRequestResult() {
            return this.requestResult;
        }

        @DexIgnore
        public final ResultCode getResultCode() {
            return this.resultCode;
        }

        @DexIgnore
        public int hashCode() {
            PhaseId phaseId2 = this.phaseId;
            int i = 0;
            int hashCode = (phaseId2 != null ? phaseId2.hashCode() : 0) * 31;
            ResultCode resultCode2 = this.resultCode;
            int hashCode2 = (hashCode + (resultCode2 != null ? resultCode2.hashCode() : 0)) * 31;
            Request.Result result = this.requestResult;
            if (result != null) {
                i = result.hashCode();
            }
            return hashCode2 + i;
        }

        @DexIgnore
        public JSONObject toJSONObject() {
            JSONObject jSONObject = new JSONObject();
            try {
                xa0.a(xa0.a(jSONObject, JSONKey.PHASE_ID, this.phaseId.getLogName$blesdk_productionRelease()), JSONKey.RESULT_CODE, this.resultCode.getLogName$blesdk_productionRelease());
                if (this.requestResult.getResultCode() != Request.Result.ResultCode.SUCCESS) {
                    xa0.a(jSONObject, JSONKey.REQUEST_RESULT, this.requestResult.toJSONObject());
                }
            } catch (JSONException e) {
                ea0.l.a(e);
            }
            return jSONObject;
        }

        @DexIgnore
        public String toString() {
            return "Result(phaseId=" + this.phaseId + ", resultCode=" + this.resultCode + ", requestResult=" + this.requestResult + ")";
        }

        @DexIgnore
        public Result(PhaseId phaseId2, ResultCode resultCode2, Request.Result result) {
            wd4.b(phaseId2, "phaseId");
            wd4.b(resultCode2, "resultCode");
            wd4.b(result, "requestResult");
            this.phaseId = phaseId2;
            this.resultCode = resultCode2;
            this.requestResult = result;
        }
    }

    @DexIgnore
    public final void a(long j2) {
        this.l = j2;
    }

    @DexIgnore
    public final void b() {
        if (!this.s) {
            this.s = true;
            this.t = false;
            this.v.b(this.c);
            this.w.a().b(this);
            SdkLogEntry a2 = a();
            ea0.l.b(a2);
            if (this instanceof SyncPhase) {
                aa0.l.b(a2);
            } else if ((this instanceof k50) || (this instanceof j50)) {
                z90.l.b(a2);
            }
            if (this.u.getResultCode() == Result.ResultCode.SUCCESS) {
                Peripheral peripheral = this.v;
                LogLevel logLevel = LogLevel.DEBUG;
                String str = this.a;
                Peripheral.a(peripheral, logLevel, str, "completePhase: " + "phaseUuid=" + this.y + ", " + "result=" + JSONAbleObject.toJSONString$default(this.u, 0, 1, (Object) null) + ",\n" + "data=" + x().toString(2), false, 8, (Object) null);
                for (jd4 invoke : this.e) {
                    invoke.invoke(this);
                }
            } else {
                Peripheral peripheral2 = this.v;
                LogLevel logLevel2 = LogLevel.ERROR;
                String str2 = this.a;
                Peripheral.a(peripheral2, logLevel2, str2, "completePhase: ERROR, " + "phaseUuid=" + this.y + ", " + "result=" + JSONAbleObject.toJSONString$default(this.u, 0, 1, (Object) null), false, 8, (Object) null);
                for (jd4 invoke2 : this.f) {
                    invoke2.invoke(this);
                }
            }
            for (jd4 invoke3 : this.g) {
                invoke3.invoke(this);
            }
        }
    }

    @DexIgnore
    public boolean a(Phase phase) {
        wd4.b(phase, "otherPhase");
        return b(phase) || this.x != phase.x;
    }

    @DexIgnore
    public static /* synthetic */ void a(Phase phase, Request request, jd4 jd4, jd4 jd42, kd4 kd4, jd4 jd43, jd4 jd44, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                kd4 = Phase$executeRequest$Anon1.INSTANCE;
            }
            kd4 kd42 = kd4;
            if ((i2 & 16) != 0) {
                jd43 = Phase$executeRequest$Anon2.INSTANCE;
            }
            jd4 jd45 = jd43;
            if ((i2 & 32) != 0) {
                jd44 = Phase$executeRequest$Anon3.INSTANCE;
            }
            phase.a(request, (jd4<? super Request, cb4>) jd4, (jd4<? super Request, cb4>) jd42, (kd4<? super Request, ? super Float, cb4>) kd42, (jd4<? super Request, cb4>) jd45, (jd4<? super Request.Result, Boolean>) jd44);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeRequest");
    }

    @DexIgnore
    public final void a(Request request, jd4<? super Request, cb4> jd4, jd4<? super Request, cb4> jd42, kd4<? super Request, ? super Float, cb4> kd4, jd4<? super Request, cb4> jd43, jd4<? super Request.Result, Boolean> jd44) {
        wd4.b(request, "request");
        wd4.b(jd4, "actionOnSuccess");
        wd4.b(jd42, "actionOnError");
        wd4.b(kd4, "actionOnProgressChanged");
        wd4.b(jd43, "actionOnDone");
        wd4.b(jd44, "isTerminatedRequestError");
        if (!this.s) {
            boolean z = this.t;
            if (!z || (z && a(request))) {
                request.b(this.y);
                request.a(this.x.getLogName$blesdk_productionRelease());
                this.b = request;
                Request request2 = this.b;
                if (request2 != null) {
                    request2.c(jd4);
                    if (request2 != null) {
                        request2.b((jd4<? super Request, cb4>) new Phase$executeRequest$Anon4(this, jd44, jd42));
                        if (request2 != null) {
                            request2.a(kd4);
                            if (request2 != null) {
                                request2.a(jd43);
                                if (request2 != null) {
                                    request2.v();
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
        }
        a(this.u);
    }

    @DexIgnore
    public final void a(Request.Result result) {
        wd4.b(result, "requestResult");
        this.u = Result.copy$default(this.u, (PhaseId) null, Result.ResultCode.Companion.a(result), result, 1, (Object) null);
        b();
    }

    @DexIgnore
    public final void a(Result result) {
        wd4.b(result, Constants.RESULT);
        this.u = Result.copy$default(result, (PhaseId) null, (Result.ResultCode) null, (Request.Result) null, 7, (Object) null);
        b();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ Phase(Peripheral peripheral, a aVar, PhaseId phaseId, String str, int i2, rd4 rd4) {
        this(peripheral, aVar, phaseId, str);
        if ((i2 & 8) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final SdkLogEntry a() {
        String str;
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.MESSAGE;
        if (this.u.getResultCode() == Result.ResultCode.SUCCESS) {
            str = "success";
        } else {
            str = FeatureErrorCode.a.a(FeatureErrorCode.Companion, this.u, (HashMap) null, 2, (Object) null).getLogName();
        }
        JSONObject a2 = n90.a(xa0.a(jSONObject, jSONKey, str), x());
        if (this.u.getResultCode() != Result.ResultCode.SUCCESS) {
            xa0.a(a2, JSONKey.ERROR_DETAIL, this.u.toJSONObject());
        }
        return new SdkLogEntry(this.x.getLogName$blesdk_productionRelease(), EventType.PHASE_END, this.v.k(), this.x.getLogName$blesdk_productionRelease(), this.y, this.u.getResultCode() == Result.ResultCode.SUCCESS, (String) null, (DeviceInformation) null, (fa0) null, a2, 448, (rd4) null);
    }

    @DexIgnore
    public final Phase b(jd4<? super Phase, cb4> jd4) {
        wd4.b(jd4, "actionOnPhaseError");
        if (!this.s) {
            this.f.add(jd4);
        } else if (this.u.getResultCode() != Result.ResultCode.SUCCESS) {
            jd4.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final void a(float f2) {
        for (kd4 invoke : this.h) {
            invoke.invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void a(Peripheral.State state) {
        wd4.b(state, "newState");
        if (p50.a[state.ordinal()] == 1) {
            a(Result.ResultCode.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(Phase phase, RequestId requestId, RequestId requestId2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                requestId2 = requestId;
            }
            phase.a(requestId, requestId2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryExecuteRequest");
    }

    @DexIgnore
    public final void a(RequestId requestId, RequestId requestId2) {
        wd4.b(requestId, "requestId");
        wd4.b(requestId2, "nextRequestId");
        a(requestId, (id4<cb4>) new Phase$tryExecuteRequest$Anon1(this, requestId2));
    }

    @DexIgnore
    public final void a(RequestId requestId, id4<cb4> id4) {
        wd4.b(requestId, "requestId");
        wd4.b(id4, "nextAction");
        Request a2 = a(requestId);
        if (a2 == null) {
            a(Result.copy$default(this.u, (PhaseId) null, Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else if (this.m < a2.o()) {
            a(this, a2, (jd4) Phase$tryExecuteRequest$Anon2.INSTANCE, (jd4) new Phase$tryExecuteRequest$Anon3(this, id4), (kd4) null, (jd4) new Phase$tryExecuteRequest$Anon4(this), (jd4) Phase$tryExecuteRequest$Anon5.INSTANCE, 8, (Object) null);
        } else {
            a(this.u);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(Phase phase, Phase phase2, jd4 jd4, jd4 jd42, kd4 kd4, jd4 jd43, jd4 jd44, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                kd4 = Phase$executeSubPhase$Anon1.INSTANCE;
            }
            kd4 kd42 = kd4;
            if ((i2 & 16) != 0) {
                jd43 = Phase$executeSubPhase$Anon2.INSTANCE;
            }
            jd4 jd45 = jd43;
            if ((i2 & 32) != 0) {
                jd44 = Phase$executeSubPhase$Anon3.INSTANCE;
            }
            phase.a(phase2, (jd4<? super Phase, cb4>) jd4, (jd4<? super Phase, cb4>) jd42, (kd4<? super Phase, ? super Float, cb4>) kd42, (jd4<? super Phase, cb4>) jd45, (jd4<? super Result, Boolean>) jd44);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeSubPhase");
    }

    @DexIgnore
    public final void a(Phase phase, jd4<? super Phase, cb4> jd4, jd4<? super Phase, cb4> jd42, kd4<? super Phase, ? super Float, cb4> kd4, jd4<? super Phase, cb4> jd43, jd4<? super Result, Boolean> jd44) {
        wd4.b(phase, "subPhase");
        wd4.b(jd4, "actionOnSuccess");
        wd4.b(jd42, "actionOnError");
        wd4.b(kd4, "actionOnProgressChanged");
        wd4.b(jd43, "actionOnDone");
        wd4.b(jd44, "isTerminatedPhaseError");
        if (!this.s) {
            this.n = phase;
            Phase phase2 = this.n;
            if (phase2 != null && phase2 != null) {
                phase2.d(jd4);
                if (phase2 != null) {
                    phase2.b((jd4<? super Phase, cb4>) new Phase$executeSubPhase$Anon4(this, jd44, jd42));
                    if (phase2 != null) {
                        phase2.a(kd4);
                        if (phase2 != null) {
                            phase2.a(jd43);
                            if (phase2 != null) {
                                phase2.y();
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        a(this.u);
    }

    @DexIgnore
    public void a(Result.ResultCode resultCode) {
        wd4.b(resultCode, "resultCode");
        if (!this.s && !this.t) {
            Peripheral peripheral = this.v;
            LogLevel logLevel = LogLevel.DEBUG;
            String str = this.a;
            Peripheral.a(peripheral, logLevel, str, "stopWithResultCode: " + "phaseUuid=" + this.y + ", " + "resultCode=" + resultCode.getLogName$blesdk_productionRelease() + '.', false, 8, (Object) null);
            this.o.removeCallbacksAndMessages((Object) null);
            this.t = true;
            this.u = Result.copy$default(this.u, (PhaseId) null, resultCode, (Request.Result) null, 5, (Object) null);
            Request request = this.b;
            if (request == null || request.r()) {
                Phase phase = this.n;
                if (phase == null || phase.s) {
                    a(this.u);
                } else if (phase != null) {
                    phase.a(resultCode);
                }
            } else {
                Request request2 = this.b;
                if (request2 != null) {
                    request2.w();
                }
            }
        }
    }

    @DexIgnore
    public final Phase a(jd4<? super Phase, cb4> jd4) {
        wd4.b(jd4, "actionOnPhaseDone");
        if (!this.s) {
            this.g.add(jd4);
        } else {
            jd4.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final Phase a(kd4<? super Phase, ? super Float, cb4> kd4) {
        wd4.b(kd4, "actionOnPhaseProgressChanged");
        if (!this.s) {
            this.h.add(kd4);
        }
        return this;
    }

    @DexIgnore
    public final void a(cb0 cb0) {
        if (!cb0.b()) {
            cb0.a();
            this.o.removeCallbacksAndMessages(cb0);
        }
    }
}
