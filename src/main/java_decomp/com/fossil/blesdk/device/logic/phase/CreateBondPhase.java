package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o60;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CreateBondPhase extends Phase {
    @DexIgnore
    public Peripheral.BondState z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CreateBondPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(peripheral, aVar, PhaseId.CREATE_BOND, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(str, "phaseUuid");
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new o60(j()), (jd4) new CreateBondPhase$createBond$Anon1(this), (jd4) CreateBondPhase$createBond$Anon2.INSTANCE, (kd4) null, (jd4) new CreateBondPhase$createBond$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.CURRENT_BOND_STATE, j().getBondState().getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.NEW_BOND_STATE;
        Peripheral.BondState bondState = this.z;
        return xa0.a(x, jSONKey, bondState != null ? bondState.getLogName$blesdk_productionRelease() : null);
    }

    @DexIgnore
    public Peripheral.BondState i() {
        Peripheral.BondState bondState = this.z;
        return bondState != null ? bondState : Peripheral.BondState.BOND_NONE;
    }
}
