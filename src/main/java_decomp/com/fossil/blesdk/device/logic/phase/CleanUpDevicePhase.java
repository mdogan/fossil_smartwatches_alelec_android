package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.SdkDatabaseWrapper;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.DataType;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.e80;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.y70;
import com.fossil.blesdk.obfuscated.z00;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CleanUpDevicePhase extends Phase {
    @DexIgnore
    public int A;
    @DexIgnore
    public /* final */ BiometricProfile B; // = new BiometricProfile((byte) 26, BiometricProfile.Gender.MALE, 170, 65, BiometricProfile.WearingPosition.LEFT_WRIST);
    @DexIgnore
    public /* final */ ArrayList<ResourceType> C; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG, ResourceType.FILE_CONFIG}));
    @DexIgnore
    public /* final */ boolean z; // = true;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CleanUpDevicePhase(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.CLEAN_UP_DEVICE, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new y70(j()), (jd4) new CleanUpDevicePhase$cleanUpDevice$Anon1(this), (jd4) new CleanUpDevicePhase$cleanUpDevice$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void B() {
        int i = this.A;
        if (i < 2) {
            this.A = i + 1;
            if (z00.f.b(e().getDeviceInformation())) {
                Phase.a((Phase) this, (Phase) new LegacySyncPhase(j(), e(), this.B, true, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 32, (rd4) null), (jd4) new CleanUpDevicePhase$eraseActivityFile$Anon1(this), (jd4) new CleanUpDevicePhase$eraseActivityFile$Anon2(this), (kd4) new CleanUpDevicePhase$eraseActivityFile$Anon3(this), (jd4) null, (jd4) null, 48, (Object) null);
                return;
            }
            Phase.a((Phase) this, (Request) new e80(DataType.ACTIVITY_FILE.getFileHandleMask$blesdk_productionRelease(), j(), 0, 4, (rd4) null), (jd4) new CleanUpDevicePhase$eraseActivityFile$Anon4(this), (jd4) new CleanUpDevicePhase$eraseActivityFile$Anon5(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
            return;
        }
        C();
    }

    @DexIgnore
    public final void C() {
        SdkDatabaseWrapper.b.a(j().k());
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public boolean c() {
        return this.z;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.C;
    }

    @DexIgnore
    public void t() {
        A();
    }
}
