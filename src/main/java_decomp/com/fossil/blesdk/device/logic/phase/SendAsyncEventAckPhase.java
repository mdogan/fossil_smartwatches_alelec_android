package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.e70;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SendAsyncEventAckPhase extends Phase {
    @DexIgnore
    public /* final */ AsyncEvent A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.ASYNC}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SendAsyncEventAckPhase(Peripheral peripheral, Phase.a aVar, AsyncEvent asyncEvent) {
        super(peripheral, aVar, PhaseId.SEND_ASYNC_EVENT_ACK, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(asyncEvent, "asyncEvent");
        this.A = asyncEvent;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        Phase.a((Phase) this, (Request) new e70(j(), this.A), (jd4) SendAsyncEventAckPhase$onStart$Anon1.INSTANCE, (jd4) SendAsyncEventAckPhase$onStart$Anon2.INSTANCE, (kd4) null, (jd4) new SendAsyncEventAckPhase$onStart$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.DEVICE_EVENT, this.A.toJSONObject());
    }
}
