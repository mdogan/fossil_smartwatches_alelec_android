package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncPhase$onStart$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ SyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncPhase$onStart$Anon1(SyncPhase syncPhase) {
        super(1);
        this.this$Anon0 = syncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "executedPhase");
        this.this$Anon0.J().clear();
        this.this$Anon0.J().addAll(((LegacySyncPhase) phase).E());
        SyncPhase syncPhase = this.this$Anon0;
        syncPhase.b(syncPhase.J());
        SyncPhase syncPhase2 = this.this$Anon0;
        syncPhase2.a(Phase.Result.copy$default(syncPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }
}
