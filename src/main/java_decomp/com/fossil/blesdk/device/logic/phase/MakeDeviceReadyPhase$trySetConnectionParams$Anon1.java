package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$trySetConnectionParams$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public static /* final */ MakeDeviceReadyPhase$trySetConnectionParams$Anon1 INSTANCE; // = new MakeDeviceReadyPhase$trySetConnectionParams$Anon1();

    @DexIgnore
    public MakeDeviceReadyPhase$trySetConnectionParams$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "it");
    }
}
