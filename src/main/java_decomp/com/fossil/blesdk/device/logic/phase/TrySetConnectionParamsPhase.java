package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.b80;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrySetConnectionParamsPhase extends Phase {
    @DexIgnore
    public ConnectionParameters A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> B; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ ConnectionParametersSet[] C;
    @DexIgnore
    public int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrySetConnectionParamsPhase(Peripheral peripheral, Phase.a aVar, String str, ConnectionParametersSet[] connectionParametersSetArr) {
        super(peripheral, aVar, PhaseId.TRY_SET_CONNECTION_PARAMS, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(str, "phaseUuid");
        wd4.b(connectionParametersSetArr, "connectionParamList");
        this.C = connectionParametersSetArr;
    }

    @DexIgnore
    public final ConnectionParameters A() {
        return this.A;
    }

    @DexIgnore
    public final void B() {
        int i = this.z;
        ConnectionParametersSet[] connectionParametersSetArr = this.C;
        if (i < connectionParametersSetArr.length) {
            Phase.a((Phase) this, (Request) new b80(connectionParametersSetArr[i], j()), (jd4) new TrySetConnectionParamsPhase$trySetConnectionParameters$Anon1(this), (jd4) new TrySetConnectionParamsPhase$trySetConnectionParameters$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
            return;
        }
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.EXCHANGED_VALUE_NOT_SATISFIED, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.B;
    }

    @DexIgnore
    public void t() {
        B();
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.ACCEPTED_CONNECTION_PARAMS;
        ConnectionParameters connectionParameters = this.A;
        return xa0.a(x, jSONKey, connectionParameters != null ? connectionParameters.toJSONObject() : null);
    }

    @DexIgnore
    public ConnectionParameters i() {
        ConnectionParameters connectionParameters = this.A;
        return connectionParameters != null ? connectionParameters : new ConnectionParameters(0, 0, 0);
    }
}
