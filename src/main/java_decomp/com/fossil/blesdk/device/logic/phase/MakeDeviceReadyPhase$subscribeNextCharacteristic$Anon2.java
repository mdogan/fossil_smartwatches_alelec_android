package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.ButtonService;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon2 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ MakeDeviceReadyPhase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon2 e;

        @DexIgnore
        public a(MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon2 makeDeviceReadyPhase$subscribeNextCharacteristic$Anon2) {
            this.e = makeDeviceReadyPhase$subscribeNextCharacteristic$Anon2;
        }

        @DexIgnore
        public final void run() {
            this.e.this$Anon0.E();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon2(MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$Anon0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        if (this.this$Anon0.D < 2) {
            this.this$Anon0.b((Request) null);
            this.this$Anon0.f().postDelayed(new a(this), ButtonService.CONNECT_TIMEOUT);
            return;
        }
        MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$Anon0;
        makeDeviceReadyPhase.c(Phase.Result.copy$default(makeDeviceReadyPhase.k(), (PhaseId) null, Phase.Result.ResultCode.Companion.a(request.n()), request.n(), 1, (Object) null));
    }
}
