package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.b50;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.v70;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetConnectionParamsPhase$getConnectionParameters$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ SetConnectionParamsPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetConnectionParamsPhase$getConnectionParameters$Anon1(SetConnectionParamsPhase setConnectionParamsPhase) {
        super(1);
        this.this$Anon0 = setConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        this.this$Anon0.z = ((v70) request).I();
        ConnectionParameters B = this.this$Anon0.B();
        if (B == null) {
            SetConnectionParamsPhase setConnectionParamsPhase = this.this$Anon0;
            setConnectionParamsPhase.a(Phase.Result.copy$default(setConnectionParamsPhase.k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else if (b50.a.a(B, this.this$Anon0.B)) {
            SetConnectionParamsPhase setConnectionParamsPhase2 = this.this$Anon0;
            setConnectionParamsPhase2.a(Phase.Result.copy$default(setConnectionParamsPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            this.this$Anon0.C();
        }
    }
}
