package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildGetFileRequest$getFileRequest$Anon2 extends Lambda implements kd4<Request, Float, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GetFilePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildGetFileRequest$getFileRequest$Anon2(GetFilePhase getFilePhase) {
        super(2);
        this.this$Anon0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Request) obj, ((Number) obj2).floatValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request, float f) {
        wd4.b(request, "<anonymous parameter 0>");
        if (this.this$Anon0.F > 0) {
            float i = (float) (this.this$Anon0.G + this.this$Anon0.J);
            DeviceFile c = this.this$Anon0.H();
            if (c != null) {
                f = (i + (((float) (c.getFileLength() - this.this$Anon0.J)) * f)) / ((float) this.this$Anon0.F);
            } else {
                wd4.a();
                throw null;
            }
        }
        if (Math.abs(f - this.this$Anon0.H) > this.this$Anon0.Q || f == 1.0f) {
            this.this$Anon0.H = f;
            this.this$Anon0.a(f);
        }
    }
}
