package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d60;
import com.fossil.blesdk.obfuscated.i70;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StreamingPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A; // = new ArrayList<>();
    @DexIgnore
    public jd4<? super AsyncEvent, cb4> z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StreamingPhase(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.STREAMING, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
    }

    @DexIgnore
    public final void A() {
        i70 i70 = new i70(j());
        i70.d(new StreamingPhase$startStreaming$streamingRequest$Anon1(this));
        Phase.a((Phase) this, (Request) i70, (jd4) new StreamingPhase$startStreaming$Anon1(this), (jd4) new StreamingPhase$startStreaming$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final StreamingPhase e(jd4<? super AsyncEvent, cb4> jd4) {
        wd4.b(jd4, "actionOnEventReceived");
        this.z = jd4;
        return this;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    public void a(Peripheral.State state) {
        wd4.b(state, "newState");
        int i = d60.a[state.ordinal()];
    }
}
