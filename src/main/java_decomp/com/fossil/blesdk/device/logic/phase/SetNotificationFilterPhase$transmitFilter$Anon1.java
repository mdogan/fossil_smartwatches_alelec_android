package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationFilterPhase$transmitFilter$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ SetNotificationFilterPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFilterPhase$transmitFilter$Anon1(SetNotificationFilterPhase setNotificationFilterPhase) {
        super(1);
        this.this$Anon0 = setNotificationFilterPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "it");
        SetNotificationFilterPhase setNotificationFilterPhase = this.this$Anon0;
        setNotificationFilterPhase.a(Phase.Result.copy$default(setNotificationFilterPhase.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }
}
