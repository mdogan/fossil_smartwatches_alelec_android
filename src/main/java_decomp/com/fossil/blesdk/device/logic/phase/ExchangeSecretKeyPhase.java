package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l70;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.m70;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.fossil.crypto.EllipticCurveKeyPair;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExchangeSecretKeyPhase extends Phase {
    @DexIgnore
    public byte[] A;
    @DexIgnore
    public /* final */ EllipticCurveKeyPair B;
    @DexIgnore
    public /* final */ byte[] C;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.AUTHENTICATION}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExchangeSecretKeyPhase(Peripheral peripheral, Phase.a aVar, byte[] bArr) {
        super(peripheral, aVar, PhaseId.EXCHANGE_SECRET_KEY, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(bArr, "bothSidesRandomNumbers");
        this.C = bArr;
        EllipticCurveKeyPair create = EllipticCurveKeyPair.create();
        wd4.a((Object) create, "EllipticCurveKeyPair.create()");
        this.B = create;
    }

    @DexIgnore
    public final void A() {
        byte[] bArr = this.A;
        if (bArr != null) {
            Phase.a((Phase) this, (Phase) new AuthenticatePhase(j(), e(), va0.y.e(), kb4.a(bArr, 0, 16), l()), (jd4) ExchangeSecretKeyPhase$authenticateWithSecretKey$Anon1$Anon1.INSTANCE, (jd4) ExchangeSecretKeyPhase$authenticateWithSecretKey$Anon1$Anon2.INSTANCE, (kd4) null, (jd4) new ExchangeSecretKeyPhase$authenticateWithSecretKey$$inlined$let$lambda$Anon1(this), (jd4) null, 40, (Object) null);
            return;
        }
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public final void B() {
        Peripheral j = j();
        byte[] publicKey = this.B.publicKey();
        wd4.a((Object) publicKey, "keyPair.publicKey()");
        Phase.a((Phase) this, (Request) new l70(j, publicKey), (jd4) new ExchangeSecretKeyPhase$exchangePublicKeys$Anon1(this), (jd4) new ExchangeSecretKeyPhase$exchangePublicKeys$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final byte[] C() {
        return this.A;
    }

    @DexIgnore
    public final void D() {
        Phase.a((Phase) this, (Request) new m70(j(), AuthenticationKeyType.PRE_SHARED_KEY, this.C), (jd4) new ExchangeSecretKeyPhase$sendBothSidesRandomNumbers$Anon1(this), (jd4) new ExchangeSecretKeyPhase$sendBothSidesRandomNumbers$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        if (this.C.length != 16) {
            a(Phase.Result.ResultCode.INVALID_PARAMETER);
        } else {
            D();
        }
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.BOTH_SIDES_RANDOM_NUMBERS, l90.a(this.C, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        byte[] bArr = this.A;
        if (bArr != null) {
            xa0.a(x, JSONKey.SECRET_KEY_CRC, Long.valueOf(Crc32Calculator.a.a(bArr, Crc32Calculator.CrcType.CRC32)));
        }
        return x;
    }

    @DexIgnore
    public byte[] i() {
        byte[] bArr = this.A;
        return bArr != null ? bArr : new byte[0];
    }
}
