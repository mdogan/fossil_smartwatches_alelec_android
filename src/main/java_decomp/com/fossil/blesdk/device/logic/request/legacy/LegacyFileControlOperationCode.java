package com.fossil.blesdk.device.logic.request.legacy;

import com.fossil.blesdk.obfuscated.rd4;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum LegacyFileControlOperationCode {
    LEGACY_ABORT_FILE((byte) 7),
    LEGACY_PUT_FILE((byte) 11),
    LEGACY_VERIFY_FILE(DateTimeFieldType.HALFDAY_OF_DAY),
    LEGACY_PUT_FILE_EOF(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    LEGACY_VERIFY_SEGMENT(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    LEGACY_ERASE_SEGMENT(DateTimeFieldType.MINUTE_OF_DAY),
    LEGACY_GET_SIZE_WRITTEN(DateTimeFieldType.SECOND_OF_DAY),
    LEGACY_LIST_FILE((byte) 5),
    LEGACY_GET_FILE((byte) 1),
    LEGACY_ERASE_FILE((byte) 3);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte code;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    LegacyFileControlOperationCode(byte b) {
        this.code = b;
    }

    @DexIgnore
    public final byte getCode() {
        return this.code;
    }

    @DexIgnore
    public final byte responseCode() {
        return (byte) (this.code + 1);
    }
}
