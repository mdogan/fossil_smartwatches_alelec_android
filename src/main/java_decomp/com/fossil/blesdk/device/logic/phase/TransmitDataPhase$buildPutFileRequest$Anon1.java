package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k80;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransmitDataPhase$buildPutFileRequest$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ TransmitDataPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransmitDataPhase$buildPutFileRequest$Anon1(TransmitDataPhase transmitDataPhase) {
        super(1);
        this.this$Anon0 = transmitDataPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "it");
        this.this$Anon0.I = ((k80) request).J();
        d90.b.a(this.this$Anon0.j().k()).c(this.this$Anon0.I);
        this.this$Anon0.a(RequestId.TRANSFER_DATA, RequestId.GET_FILE_SIZE_WRITTEN);
    }
}
