package com.fossil.blesdk.device.logic.request.file;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.p70;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FileDataRequest$actionOnEOFTimeout$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Peripheral $peripheral;
    @DexIgnore
    public /* final */ /* synthetic */ FileDataRequest this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileDataRequest$actionOnEOFTimeout$Anon1(FileDataRequest fileDataRequest, Peripheral peripheral) {
        super(0);
        this.this$Anon0 = fileDataRequest;
        this.$peripheral = peripheral;
    }

    @DexIgnore
    public final void invoke() {
        Peripheral.a(this.$peripheral, LogLevel.DEBUG, this.this$Anon0.q(), "Request EOF timeout.", false, 8, (Object) null);
        FileDataRequest fileDataRequest = this.this$Anon0;
        fileDataRequest.a(Request.Result.copy$default(fileDataRequest.n(), (RequestId) null, Request.Result.ResultCode.EOF_TIME_OUT, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
    }
}
