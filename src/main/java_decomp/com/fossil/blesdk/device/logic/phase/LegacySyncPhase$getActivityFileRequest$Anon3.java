package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getActivityFileRequest$Anon3 extends Lambda implements kd4<Request, Float, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacySyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getActivityFileRequest$Anon3(LegacySyncPhase legacySyncPhase) {
        super(2);
        this.this$Anon0 = legacySyncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Request) obj, ((Number) obj2).floatValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request, float f) {
        wd4.b(request, "<anonymous parameter 0>");
        int i = 0;
        for (DeviceFile fileLength : this.this$Anon0.E()) {
            i += (int) fileLength.getFileLength();
        }
        float k = (((float) i) + (f * ((float) this.this$Anon0.B))) / ((float) this.this$Anon0.B);
        if (k - this.this$Anon0.G > this.this$Anon0.K || k == 1.0f) {
            this.this$Anon0.G = k;
            this.this$Anon0.a(k);
        }
    }
}
