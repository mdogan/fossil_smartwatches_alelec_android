package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r60;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DisconnectPhase extends Phase {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DisconnectPhase(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.DISCONNECT, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new r60(j()), (jd4) DisconnectPhase$disconnect$Anon1.INSTANCE, (jd4) DisconnectPhase$disconnect$Anon2.INSTANCE, (kd4) null, (jd4) new DisconnectPhase$disconnect$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public void t() {
        A();
    }
}
