package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.m70;
import com.fossil.blesdk.obfuscated.n70;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.fossil.blesdk.utils.EncryptionAES128;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AuthenticatePhase extends Phase {
    @DexIgnore
    public static /* final */ EncryptionAES128.Transformation F; // = EncryptionAES128.Transformation.CBC_NO_PADDING;
    @DexIgnore
    public static /* final */ byte[] G; // = new byte[16];
    @DexIgnore
    public static /* final */ a H; // = new a((rd4) null);
    @DexIgnore
    public byte[] A; // = new byte[8];
    @DexIgnore
    public byte[] B; // = new byte[0];
    @DexIgnore
    public /* final */ ArrayList<ResourceType> C; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.AUTHENTICATION}));
    @DexIgnore
    public /* final */ AuthenticationKeyType D;
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public byte[] z; // = new byte[8];

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a() {
            return AuthenticatePhase.G;
        }

        @DexIgnore
        public final EncryptionAES128.Transformation b() {
            return AuthenticatePhase.F;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AuthenticatePhase(Peripheral peripheral, Phase.a aVar, AuthenticationKeyType authenticationKeyType, byte[] bArr, String str) {
        super(peripheral, aVar, PhaseId.AUTHENTICATE, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(authenticationKeyType, "keyType");
        wd4.b(str, "phaseUuid");
        this.D = authenticationKeyType;
        this.E = bArr;
    }

    @DexIgnore
    public final byte[] A() {
        return this.A;
    }

    @DexIgnore
    public final byte[] B() {
        return this.z;
    }

    @DexIgnore
    public final void C() {
        Peripheral j = j();
        AuthenticationKeyType authenticationKeyType = this.D;
        Phase.a((Phase) this, (Request) new n70(j, authenticationKeyType, AuthenticationKeyType.Companion.a(authenticationKeyType, this.z)), (jd4) new AuthenticatePhase$sendPhoneRandomNumber$Anon1(this), (jd4) new AuthenticatePhase$sendPhoneRandomNumber$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void b(byte[] bArr) {
        if (bArr.length != 16) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INVALID_DATA_LENGTH, (Request.Result) null, 5, (Object) null));
            return;
        }
        byte[] a2 = AuthenticationKeyType.Companion.a(this.D, EncryptionAES128.a.a(F, this.B, G, bArr));
        if (a2.length != 16) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INVALID_DATA_LENGTH, (Request.Result) null, 5, (Object) null));
            return;
        }
        this.A = kb4.a(a2, 0, 8);
        if (Arrays.equals(this.z, kb4.a(a2, 8, 16))) {
            a(AuthenticationKeyType.Companion.a(this.D, EncryptionAES128.a.b(F, this.B, G, kb4.a(this.z, this.A))));
            return;
        }
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.WRONG_RANDOM_NUMBER, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.C;
    }

    @DexIgnore
    public void t() {
        new SecureRandom().nextBytes(this.z);
        byte[] bArr = this.E;
        if (bArr == null) {
            a(Phase.Result.ResultCode.SECRET_KEY_IS_REQUIRED);
        } else if (bArr.length < 16) {
            a(Phase.Result.ResultCode.INVALID_PARAMETER);
        } else {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.B = copyOf;
            C();
        }
    }

    @DexIgnore
    public JSONObject u() {
        Object obj;
        JSONObject a2 = xa0.a(super.u(), JSONKey.AUTHENTICATION_KEY_TYPE, this.D.getLogName$blesdk_productionRelease());
        JSONKey jSONKey = JSONKey.SECRET_KEY_CRC;
        byte[] bArr = this.E;
        if (bArr != null) {
            obj = Long.valueOf(Crc32Calculator.a.a(bArr, Crc32Calculator.CrcType.CRC32));
        } else {
            obj = JSONObject.NULL;
        }
        return xa0.a(a2, jSONKey, obj);
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(xa0.a(super.x(), JSONKey.PHONE_RANDOM_NUMBER, l90.a(this.z, (String) null, 1, (Object) null)), JSONKey.DEVICE_RANDOM_NUMBER, l90.a(this.A, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        Phase.a((Phase) this, (Request) new m70(j(), this.D, bArr), (jd4) AuthenticatePhase$confirmBothSidesRandomNumbers$Anon1.INSTANCE, (jd4) AuthenticatePhase$confirmBothSidesRandomNumbers$Anon2.INSTANCE, (kd4) null, (jd4) new AuthenticatePhase$confirmBothSidesRandomNumbers$Anon3(this), (jd4) null, 40, (Object) null);
    }
}
