package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.legacy.LegacyTransferDataRequest;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.a90;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l60;
import com.fossil.blesdk.obfuscated.n80;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.q80;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u80;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.w80;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x80;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.y80;
import com.fossil.blesdk.obfuscated.z80;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.util.ArrayList;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase extends Phase {
    @DexIgnore
    public /* final */ long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public float C;
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public /* final */ ConnectionParametersSet J;
    @DexIgnore
    public /* final */ byte[] K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ short M;
    @DexIgnore
    public /* final */ float N;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ LegacyOtaPhase(Peripheral peripheral, Phase.a aVar, byte[] bArr, boolean z2, short s, float f, String str, int i, rd4 rd4) {
        this(peripheral, aVar, bArr, r5, r6, r7, r8);
        String str2;
        boolean z3 = (i & 8) != 0 ? false : z2;
        short s2 = (i & 16) != 0 ? 23131 : s;
        float f2 = (i & 32) != 0 ? 0.001f : f;
        if ((i & 64) != 0) {
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new n80(this.M, j(), 0, 4, (rd4) null), (jd4) new LegacyOtaPhase$abortFile$Anon1(this), (jd4) new LegacyOtaPhase$abortFile$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Request) new q80(Math.max(0, this.D - ((long) 4)), this.M, j(), 0, 8, (rd4) null), (jd4) new LegacyOtaPhase$eraseSegment$Anon1(this), (jd4) new LegacyOtaPhase$eraseSegment$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new u80(this.M, j(), 0, 4, (rd4) null), (jd4) new LegacyOtaPhase$getFileSizeWritten$Anon1(this), (jd4) new LegacyOtaPhase$getFileSizeWritten$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void D() {
        Phase.a((Phase) this, (Request) new w80(j()), (jd4) new LegacyOtaPhase$sendOTAEnterRequest$Anon1(this), (jd4) new LegacyOtaPhase$sendOTAEnterRequest$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void E() {
        Phase.a((Phase) this, (Request) new x80(j()), (jd4) LegacyOtaPhase$sendOTAResetRequest$Anon1.INSTANCE, (jd4) LegacyOtaPhase$sendOTAResetRequest$Anon2.INSTANCE, (kd4) null, (jd4) new LegacyOtaPhase$sendOTAResetRequest$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void F() {
        long j = this.F + this.G;
        long min = Math.min(6144, this.A - j);
        long j2 = this.A;
        short s = this.M;
        Phase.a((Phase) this, (Request) new y80(j, min, j2, s, j(), 0, 32, (rd4) null), (jd4) new LegacyOtaPhase$sendPutFileRequest$Anon1(this), (jd4) new LegacyOtaPhase$sendPutFileRequest$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void G() {
        Phase.a((Phase) this, (Phase) new SetConnectionParamsPhase(j(), e(), this.J, l()), (jd4) new LegacyOtaPhase$setConnectionParams$Anon1(this), (jd4) new LegacyOtaPhase$setConnectionParams$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void H() {
        Phase.a((Phase) this, (Request) new z80(this.M, j(), 0, 4, (rd4) null), (jd4) new LegacyOtaPhase$verifyFile$Anon1(this), (jd4) new LegacyOtaPhase$verifyFile$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void I() {
        long j = this.E;
        long a2 = Crc32Calculator.a.a(this.K, (int) 0, (int) j, Crc32Calculator.CrcType.CRC32);
        long j2 = this.A;
        Phase.a((Phase) this, (Request) new a90(0, j, j2, this.M, j(), 0, 32, (rd4) null), (jd4) new LegacyOtaPhase$verifySegment$Anon1(this, a2), (jd4) new LegacyOtaPhase$verifySegment$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(xa0.a(super.u(), JSONKey.FILE_CRC, Long.valueOf(Crc32Calculator.a.a(this.K, Crc32Calculator.CrcType.CRC32))), JSONKey.SKIP_RESUME, Boolean.valueOf(this.L)), JSONKey.FILE_HANDLE, o90.a(this.M));
    }

    @DexIgnore
    public void v() {
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = 0;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase(Peripheral peripheral, Phase.a aVar, byte[] bArr, boolean z2, short s, float f, String str) {
        super(peripheral, aVar, PhaseId.LEGACY_OTA, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(bArr, "fileData");
        wd4.b(str, "phaseUuid");
        this.K = bArr;
        this.L = z2;
        this.M = s;
        this.N = f;
        this.z = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));
        this.A = (long) this.K.length;
        this.J = va0.y.n();
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        G();
    }

    @DexIgnore
    public final void b(long j) {
        this.F = 0;
        this.E = 0;
        this.G = 0;
        this.D = j;
        long j2 = this.D;
        long j3 = this.A;
        if (j2 > j3 || j2 <= 0) {
            this.D = 0;
            F();
        } else if (j2 == j3) {
            H();
        } else {
            this.E = j2;
            I();
        }
    }

    @DexIgnore
    public final void a(byte[] bArr, long j) {
        Phase.a((Phase) this, (Request) new LegacyTransferDataRequest(this.M, new l60(bArr, j().l(), GattCharacteristic.CharacteristicId.FTD), j()), (jd4) new LegacyOtaPhase$transferData$Anon2(this, bArr), (jd4) new LegacyOtaPhase$transferData$Anon3(this), (kd4) new LegacyOtaPhase$transferData$Anon1(this, bArr, j), (jd4) null, (jd4) null, 48, (Object) null);
    }
}
