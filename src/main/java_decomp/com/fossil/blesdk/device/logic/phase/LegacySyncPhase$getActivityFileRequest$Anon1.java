package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.t80;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.utils.Crc32Calculator;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getActivityFileRequest$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ short $fileHandleGetFile;
    @DexIgnore
    public /* final */ /* synthetic */ LegacySyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getActivityFileRequest$Anon1(LegacySyncPhase legacySyncPhase, short s) {
        super(1);
        this.this$Anon0 = legacySyncPhase;
        this.$fileHandleGetFile = s;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        byte[] K = ((t80) request).K();
        if (K.length == 0) {
            LegacySyncPhase legacySyncPhase = this.this$Anon0;
            legacySyncPhase.a(Phase.Result.copy$default(legacySyncPhase.k(), (PhaseId) null, Phase.Result.ResultCode.INVALID_RESPONSE, (Request.Result) null, 5, (Object) null));
            return;
        }
        this.this$Anon0.E().add(new DeviceFile(this.this$Anon0.j().k(), new FileHandle(this.$fileHandleGetFile).getFileType$blesdk_productionRelease(), new FileHandle(this.$fileHandleGetFile).getFileIndex$blesdk_productionRelease(), K, (long) K.length, Crc32Calculator.a.a(K, Crc32Calculator.CrcType.CRC32), this.this$Anon0.p(), true));
        this.this$Anon0.G();
    }
}
