package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m60;
import com.fossil.blesdk.obfuscated.q60;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectHIDPhase extends Phase {
    @DexIgnore
    public /* final */ HashMap<ConnectHIDOption, Object> z;

    @DexIgnore
    public enum ConnectHIDOption {
        CONNECTION_TIME_OUT;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectHIDPhase(Peripheral peripheral, Phase.a aVar, HashMap<ConnectHIDOption, Object> hashMap) {
        super(peripheral, aVar, PhaseId.CONNECT_HID, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(hashMap, "options");
        this.z = hashMap;
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new m60(j(), D()), (jd4) ConnectHIDPhase$connectHID$Anon1.INSTANCE, (jd4) ConnectHIDPhase$connectHID$Anon2.INSTANCE, (kd4) null, (jd4) new ConnectHIDPhase$connectHID$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Phase) new CreateBondPhase(j(), e(), l()), (jd4) new ConnectHIDPhase$createBond$Anon1(this), (jd4) new ConnectHIDPhase$createBond$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new q60(j()), (jd4) ConnectHIDPhase$disconnectHID$Anon1.INSTANCE, (jd4) ConnectHIDPhase$disconnectHID$Anon2.INSTANCE, (kd4) null, (jd4) new ConnectHIDPhase$disconnectHID$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public final long D() {
        Long l = (Long) this.z.get(ConnectHIDOption.CONNECTION_TIME_OUT);
        if (l != null) {
            return l.longValue();
        }
        return 60000;
    }

    @DexIgnore
    public final void c(Phase.Result result) {
        if (result.getResultCode() == Phase.Result.ResultCode.SUCCESS) {
            a(result);
            return;
        }
        b(result);
        C();
    }

    @DexIgnore
    public void t() {
        B();
    }
}
