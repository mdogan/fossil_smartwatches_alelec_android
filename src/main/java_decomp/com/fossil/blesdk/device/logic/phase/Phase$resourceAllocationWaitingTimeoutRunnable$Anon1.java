package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$resourceAllocationWaitingTimeoutRunnable$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$resourceAllocationWaitingTimeoutRunnable$Anon1(Phase phase) {
        super(0);
        this.this$Anon0 = phase;
    }

    @DexIgnore
    public final void invoke() {
        this.this$Anon0.a(Phase.Result.ResultCode.WAITING_FOR_EXECUTION_TIMEOUT);
    }
}
