package com.fossil.blesdk.device.logic.data.connectionparameter;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionParametersSet extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ int latency;
    @DexIgnore
    public /* final */ int maximumInterval;
    @DexIgnore
    public /* final */ int minimumInterval;
    @DexIgnore
    public /* final */ int timeout;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ConnectionParametersSet> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ConnectionParametersSet createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ConnectionParametersSet(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public ConnectionParametersSet[] newArray(int i) {
            return new ConnectionParametersSet[i];
        }
    }

    @DexIgnore
    public ConnectionParametersSet(int i, int i2, int i3, int i4) {
        boolean z = false;
        if (true == (i < 6)) {
            i = 6;
        } else {
            if (true == (i > 3200)) {
                i = 3200;
            }
        }
        this.minimumInterval = i;
        this.maximumInterval = (this.minimumInterval > i2 || 3200 < i2) ? this.minimumInterval : this.minimumInterval;
        int i5 = 499;
        if (true == (i3 < 0)) {
            i5 = 0;
        } else {
            if (true != (i3 > 499)) {
                i5 = i3;
            }
        }
        this.latency = i5;
        int i6 = 10;
        if (true != (i4 < 10)) {
            i6 = true == (i4 > 3200 ? true : z) ? 3200 : i4;
        }
        this.timeout = i6;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getLatency$blesdk_productionRelease() {
        return this.latency;
    }

    @DexIgnore
    public final int getMaximumInterval$blesdk_productionRelease() {
        return this.maximumInterval;
    }

    @DexIgnore
    public final int getMinimumInterval$blesdk_productionRelease() {
        return this.minimumInterval;
    }

    @DexIgnore
    public final int getTimeout$blesdk_productionRelease() {
        return this.timeout;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return xa0.a(xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.MIN_INTERVAL, Integer.valueOf(this.minimumInterval)), JSONKey.MAX_INTERVAL, Integer.valueOf(this.maximumInterval)), JSONKey.LATENCY, Integer.valueOf(this.latency)), JSONKey.TIMEOUT, Integer.valueOf(this.timeout));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeInt(this.minimumInterval);
        parcel.writeInt(this.maximumInterval);
        parcel.writeInt(this.latency);
        parcel.writeInt(this.timeout);
    }
}
