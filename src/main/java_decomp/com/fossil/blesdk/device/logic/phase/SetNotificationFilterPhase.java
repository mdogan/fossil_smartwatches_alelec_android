package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.model.file.NotificationIcon;
import com.fossil.blesdk.model.notification.filter.NotificationIconConfig;
import com.fossil.blesdk.obfuscated.a50;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.h60;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.p20;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x20;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationFilterPhase extends Phase {
    @DexIgnore
    public static /* final */ a A; // = new a((rd4) null);
    @DexIgnore
    public /* final */ NotificationFilter[] z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a(NotificationIcon[] notificationIconArr, short s, Version version) {
            wd4.b(notificationIconArr, "notificationIcons");
            wd4.b(version, "version");
            Object[] array = lb4.c((T[]) notificationIconArr).toArray(new NotificationIcon[0]);
            if (array != null) {
                NotificationIcon[] notificationIconArr2 = (NotificationIcon[]) array;
                if (notificationIconArr2.length == 0) {
                    return new byte[0];
                }
                return p20.c.a(s, version, notificationIconArr2);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SetNotificationFilterPhase(Peripheral peripheral, Phase.a aVar, NotificationFilter[] notificationFilterArr, String str, int i, rd4 rd4) {
        this(peripheral, aVar, notificationFilterArr, str);
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final NotificationIcon[] A() {
        ArrayList arrayList = new ArrayList();
        for (NotificationFilter iconConfig : this.z) {
            NotificationIconConfig iconConfig2 = iconConfig.getIconConfig();
            if (iconConfig2 != null) {
                tb4.a(arrayList, (T[]) iconConfig2.getDistinctIcons$blesdk_productionRelease());
            }
        }
        Object[] array = arrayList.toArray(new NotificationIcon[0]);
        if (array != null) {
            return (NotificationIcon[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void B() {
        short b = a50.b.b(j().k(), FileType.NOTIFICATION_FILTER);
        try {
            x20 x20 = x20.c;
            Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.NOTIFICATION_FILTER.getFileHandleMask$blesdk_productionRelease()));
            if (version == null) {
                version = va0.y.g();
            }
            byte[] a2 = x20.a(b, version, this.z);
            Phase.a((Phase) this, (Phase) new h60(j(), e(), PhaseId.PUT_NOTIFICATION_FILTER_RULE, true, b, a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 64, (rd4) null), (jd4) new SetNotificationFilterPhase$transmitFilter$Anon1(this), (jd4) new SetNotificationFilterPhase$transmitFilter$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
        } catch (FileFormatException e) {
            ea0.l.a(e);
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INCOMPATIBLE_FIRMWARE, (Request.Result) null, 5, (Object) null));
        }
    }

    @DexIgnore
    public final void C() {
        NotificationIcon[] A2 = A();
        if (!(A2.length == 0)) {
            try {
                a aVar = A;
                Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.NOTIFICATION.getFileHandleMask$blesdk_productionRelease()));
                if (version == null) {
                    version = va0.y.g();
                }
                Phase.a((Phase) this, (Phase) new h60(j(), e(), PhaseId.PUT_NOTIFICATION_ICON, true, 1793, aVar.a(A2, 1793, version), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 64, (rd4) null), (jd4) new SetNotificationFilterPhase$transmitIconFile$Anon1(this), (jd4) new SetNotificationFilterPhase$transmitIconFile$Anon2(this), (kd4) new SetNotificationFilterPhase$transmitIconFile$Anon3(this), (jd4) null, (jd4) null, 48, (Object) null);
            } catch (FileFormatException e) {
                ea0.l.a(e);
                a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INCOMPATIBLE_FIRMWARE, (Request.Result) null, 5, (Object) null));
            }
        } else {
            B();
        }
    }

    @DexIgnore
    public void t() {
        C();
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NOTIFICATION_FILTERS, k00.a(this.z));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFilterPhase(Peripheral peripheral, Phase.a aVar, NotificationFilter[] notificationFilterArr, String str) {
        super(peripheral, aVar, PhaseId.SET_NOTIFICATION_FILTER, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(notificationFilterArr, "notificationFilters");
        wd4.b(str, "phaseUuid");
        this.z = notificationFilterArr;
    }
}
