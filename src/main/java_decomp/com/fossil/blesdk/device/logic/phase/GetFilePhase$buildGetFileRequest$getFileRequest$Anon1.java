package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g80;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildGetFileRequest$getFileRequest$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ GetFilePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildGetFileRequest$getFileRequest$Anon1(GetFilePhase getFilePhase) {
        super(1);
        this.this$Anon0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0089, code lost:
        if (r4 != null) goto L_0x008f;
     */
    @DexIgnore
    public final void invoke(Request request) {
        byte[] bArr;
        Request request2 = request;
        wd4.b(request2, "executedRequest");
        g80 g80 = (g80) request2;
        DeviceFile c = this.this$Anon0.H();
        String str = null;
        if (c != null) {
            FileHandle fileHandle = new FileHandle(c.getFileHandle$blesdk_productionRelease());
            DeviceFile a = this.this$Anon0.a(fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease());
            u90 u90 = u90.c;
            String r = this.this$Anon0.r();
            StringBuilder sb = new StringBuilder();
            sb.append("Get file in DB for ");
            sb.append(this.this$Anon0.j().k());
            sb.append(", ");
            sb.append("fileType=");
            sb.append(fileHandle.getFileType$blesdk_productionRelease());
            sb.append(", ");
            sb.append("fileIndex=");
            sb.append(fileHandle.getFileIndex$blesdk_productionRelease());
            sb.append(", ");
            sb.append("result=");
            if (a != null) {
                str = a.toString();
            }
            sb.append(str);
            u90.a(r, sb.toString());
            if (a != null) {
                bArr = a.getRawData();
            }
            bArr = new byte[0];
            DeviceFile deviceFile = new DeviceFile(this.this$Anon0.j().k(), fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease(), l90.a(bArr, g80.L()), g80.K(), g80.J(), this.this$Anon0.p(), false);
            if (request.n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
                this.this$Anon0.d(deviceFile);
            } else {
                long unused = this.this$Anon0.a(deviceFile);
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
