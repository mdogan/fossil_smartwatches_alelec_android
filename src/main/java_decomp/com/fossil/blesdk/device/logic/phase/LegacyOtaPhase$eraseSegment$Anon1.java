package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.q80;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$eraseSegment$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$eraseSegment$Anon1(LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$Anon0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        this.this$Anon0.D = ((q80) request).J();
        if (this.this$Anon0.F >= this.this$Anon0.D) {
            LegacyOtaPhase legacyOtaPhase = this.this$Anon0;
            legacyOtaPhase.F = legacyOtaPhase.D;
            this.this$Anon0.F();
            return;
        }
        this.this$Anon0.B();
    }
}
