package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d50;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.l80;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindCorrectOffsetPhase extends Phase {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public /* final */ long D; // = o90.b(this.E.getRawData().length);
    @DexIgnore
    public /* final */ DeviceFile E;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FindCorrectOffsetPhase(Peripheral peripheral, Phase.a aVar, DeviceFile deviceFile, String str) {
        super(peripheral, aVar, PhaseId.FIND_CORRECT_OFFSET, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(deviceFile, "deviceFile");
        wd4.b(str, "phaseUuid");
        this.E = deviceFile;
    }

    @DexIgnore
    public final l80 A() {
        long j = this.B;
        l80 l80 = new l80(j, this.C - j, this.E.getFileHandle$blesdk_productionRelease(), j(), 0, 16, (rd4) null);
        l80.c((jd4<? super Request, cb4>) new FindCorrectOffsetPhase$buildVerifyDataRequest$Anon1(this));
        return l80;
    }

    @DexIgnore
    public final long B() {
        return this.A;
    }

    @DexIgnore
    public final DeviceFile C() {
        return this.E;
    }

    @DexIgnore
    public final void D() {
        this.B = 0;
        this.C = o90.b(this.E.getRawData().length);
        if (this.B == this.C) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        D();
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.DEVICE_FILE, this.E.toJSONObject(false));
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(super.x(), JSONKey.CORRECT_OFFSET, Long.valueOf(this.A));
    }

    @DexIgnore
    public Request a(RequestId requestId) {
        wd4.b(requestId, "requestId");
        if (d50.a[requestId.ordinal()] != 1) {
            return null;
        }
        return A();
    }

    @DexIgnore
    public Long i() {
        return Long.valueOf(this.A);
    }

    @DexIgnore
    public final void a(long j, long j2, long j3) {
        if (0 == j2) {
            this.A = this.B;
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (j != this.B) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else {
            this.C = j + j2;
            long a = Crc32Calculator.a.a(this.E.getRawData(), (int) j, (int) j2, Crc32Calculator.CrcType.CRC32);
            u90 u90 = u90.c;
            String r = r();
            u90.a(r, "onVerifyDataSuccess: transferredDataCrc=" + a);
            if (j3 == a) {
                long j4 = this.C;
                long j5 = this.D;
                if (j4 == j5) {
                    this.A = j4;
                    a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
                    return;
                }
                this.C = Math.min(((j4 - this.B) / ((long) 2)) + j4, j5);
                this.B = j4;
                Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
                return;
            }
            this.C = (this.C + this.B) / ((long) 2);
            Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
        }
    }
}
