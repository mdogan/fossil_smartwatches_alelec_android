package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AuthenticatePhase$confirmBothSidesRandomNumbers$Anon3 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ AuthenticatePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AuthenticatePhase$confirmBothSidesRandomNumbers$Anon3(AuthenticatePhase authenticatePhase) {
        super(1);
        this.this$Anon0 = authenticatePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Phase.Result.ResultCode resultCode;
        wd4.b(request, "executedRequest");
        if (request.n().getResultCode() == Request.Result.ResultCode.RESPONSE_ERROR) {
            resultCode = Phase.Result.ResultCode.AUTHENTICATION_FAILED;
        } else {
            resultCode = Phase.Result.ResultCode.Companion.a(request.n());
        }
        Phase.Result.ResultCode resultCode2 = resultCode;
        AuthenticatePhase authenticatePhase = this.this$Anon0;
        authenticatePhase.a(Phase.Result.copy$default(authenticatePhase.k(), (PhaseId) null, resultCode2, request.n(), 1, (Object) null));
    }
}
