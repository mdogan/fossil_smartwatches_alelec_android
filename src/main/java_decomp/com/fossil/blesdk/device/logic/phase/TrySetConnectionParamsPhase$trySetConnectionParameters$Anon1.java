package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.b50;
import com.fossil.blesdk.obfuscated.b80;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrySetConnectionParamsPhase$trySetConnectionParameters$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ TrySetConnectionParamsPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrySetConnectionParamsPhase$trySetConnectionParameters$Anon1(TrySetConnectionParamsPhase trySetConnectionParamsPhase) {
        super(1);
        this.this$Anon0 = trySetConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        this.this$Anon0.A = ((b80) request).I();
        ConnectionParameters A = this.this$Anon0.A();
        if (A == null) {
            TrySetConnectionParamsPhase trySetConnectionParamsPhase = this.this$Anon0;
            trySetConnectionParamsPhase.a(Phase.Result.copy$default(trySetConnectionParamsPhase.k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else if (b50.a.a(A, this.this$Anon0.C[this.this$Anon0.z])) {
            TrySetConnectionParamsPhase trySetConnectionParamsPhase2 = this.this$Anon0;
            trySetConnectionParamsPhase2.a(Phase.Result.copy$default(trySetConnectionParamsPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            TrySetConnectionParamsPhase trySetConnectionParamsPhase3 = this.this$Anon0;
            trySetConnectionParamsPhase3.z = trySetConnectionParamsPhase3.z + 1;
            this.this$Anon0.B();
        }
    }
}
