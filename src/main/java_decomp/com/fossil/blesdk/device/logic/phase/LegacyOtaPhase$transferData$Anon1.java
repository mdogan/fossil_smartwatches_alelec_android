package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$transferData$Anon1 extends Lambda implements kd4<Request, Float, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $dataToTransfer;
    @DexIgnore
    public /* final */ /* synthetic */ long $offset;
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$transferData$Anon1(LegacyOtaPhase legacyOtaPhase, byte[] bArr, long j) {
        super(2);
        this.this$Anon0 = legacyOtaPhase;
        this.$dataToTransfer = bArr;
        this.$offset = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Request) obj, ((Number) obj2).floatValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request, float f) {
        wd4.b(request, "<anonymous parameter 0>");
        this.this$Anon0.B = this.$offset + ((long) (f * ((float) this.$dataToTransfer.length)));
        float o = (((float) this.this$Anon0.B) * 1.0f) / ((float) this.this$Anon0.A);
        if (Math.abs(o - this.this$Anon0.C) > this.this$Anon0.N || o == 1.0f) {
            this.this$Anon0.C = o;
            this.this$Anon0.a(o);
        }
    }
}
