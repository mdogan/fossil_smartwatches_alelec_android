package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.c80;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.ArrayList;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StopCurrentWorkoutSessionPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ StopCurrentWorkoutSessionPhase(Peripheral peripheral, Phase.a aVar, String str, int i, rd4 rd4) {
        this(peripheral, aVar, str);
        if ((i & 4) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new c80(j()), (jd4) StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon1.INSTANCE, (jd4) StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon2.INSTANCE, (kd4) null, (jd4) new StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StopCurrentWorkoutSessionPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(peripheral, aVar, PhaseId.STOP_CURRENT_WORKOUT_SESSION, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(str, "phaseUuid");
        this.z = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));
    }
}
