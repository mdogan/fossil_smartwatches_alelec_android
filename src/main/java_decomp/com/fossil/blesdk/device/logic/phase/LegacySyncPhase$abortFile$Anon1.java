package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$abortFile$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public static /* final */ LegacySyncPhase$abortFile$Anon1 INSTANCE; // = new LegacySyncPhase$abortFile$Anon1();

    @DexIgnore
    public LegacySyncPhase$abortFile$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "it");
    }
}
