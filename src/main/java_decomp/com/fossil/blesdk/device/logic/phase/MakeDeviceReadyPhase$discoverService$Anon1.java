package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.s60;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.List;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ MakeDeviceReadyPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$discoverService$Anon1(MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$Anon0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Phase.Result.ResultCode resultCode;
        wd4.b(request, "executedRequest");
        s60 s60 = (s60) request;
        List e = lb4.e(s60.C());
        List e2 = lb4.e(s60.B());
        if (e.containsAll(this.this$Anon0.E)) {
            String a = wb4.a(e2, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredCharacteristicsString$Anon1.INSTANCE, 31, (Object) null);
            u90 u90 = u90.c;
            String r = this.this$Anon0.r();
            u90.a(r, "Discovered Characteristic: " + a);
            this.this$Anon0.a((List<? extends GattCharacteristic.CharacteristicId>) e2);
            this.this$Anon0.L();
            resultCode = Phase.Result.ResultCode.SUCCESS;
        } else {
            String a2 = wb4.a(e, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredServiceUuidString$Anon1.INSTANCE, 31, (Object) null);
            u90 u902 = u90.c;
            String r2 = this.this$Anon0.r();
            u902.b(r2, "Discovered Services: " + a2);
            resultCode = Phase.Result.ResultCode.LACK_OF_SERVICE;
        }
        Phase.Result.ResultCode resultCode2 = resultCode;
        if (resultCode2 != Phase.Result.ResultCode.SUCCESS) {
            MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$Anon0;
            makeDeviceReadyPhase.c(Phase.Result.copy$default(makeDeviceReadyPhase.k(), (PhaseId) null, resultCode2, (Request.Result) null, 5, (Object) null));
        }
    }
}
