package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.n70;
import com.fossil.blesdk.obfuscated.ob4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StartAuthenticationPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.AUTHENTICATION}));
    @DexIgnore
    public /* final */ AuthenticationKeyType B;
    @DexIgnore
    public /* final */ byte[] C;
    @DexIgnore
    public byte[] z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartAuthenticationPhase(Peripheral peripheral, Phase.a aVar, AuthenticationKeyType authenticationKeyType, byte[] bArr) {
        super(peripheral, aVar, PhaseId.START_AUTHENTICATION, (String) null, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(authenticationKeyType, "startAuthenticateKeyType");
        wd4.b(bArr, "randomNumber");
        this.B = authenticationKeyType;
        this.C = bArr;
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new n70(j(), this.B, this.C), (jd4) new StartAuthenticationPhase$sendPhoneRandomNumber$Anon1(this), (jd4) StartAuthenticationPhase$sendPhoneRandomNumber$Anon2.INSTANCE, (kd4) null, (jd4) new StartAuthenticationPhase$sendPhoneRandomNumber$Anon3(this), (jd4) null, 40, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        if (this.C.length != 8) {
            a(Phase.Result.ResultCode.INVALID_PARAMETER);
        } else {
            A();
        }
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(super.u(), JSONKey.AUTHENTICATION_KEY_TYPE, this.B.getLogName$blesdk_productionRelease()), JSONKey.PHONE_RANDOM_NUMBER, l90.a(this.C, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.BOTH_SIDES_RANDOM_NUMBERS;
        byte[] bArr = this.z;
        String str = null;
        if (bArr != null) {
            str = l90.a(bArr, (String) null, 1, (Object) null);
        }
        return xa0.a(x, jSONKey, str);
    }

    @DexIgnore
    public byte[] i() {
        byte[] bArr = this.z;
        return bArr != null ? bArr : new byte[0];
    }
}
