package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.l80;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindCorrectOffsetPhase$buildVerifyDataRequest$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ FindCorrectOffsetPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FindCorrectOffsetPhase$buildVerifyDataRequest$Anon1(FindCorrectOffsetPhase findCorrectOffsetPhase) {
        super(1);
        this.this$Anon0 = findCorrectOffsetPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "it");
        l80 l80 = (l80) request;
        this.this$Anon0.a(l80.L(), l80.K(), l80.J());
    }
}
