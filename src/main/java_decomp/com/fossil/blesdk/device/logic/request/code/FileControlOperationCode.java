package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.rd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FileControlOperationCode {
    GET_FILE((byte) 1),
    LIST_FILE((byte) 2),
    PUT_FILE((byte) 3),
    VERIFY_FILE((byte) 4),
    GET_SIZE_WRITTEN((byte) 5),
    VERIFY_DATA((byte) 6),
    ERASE_DATA((byte) 7),
    EOF_REACH((byte) 8),
    ABORT_FILE((byte) 9),
    WAITING_REQUEST((byte) 10),
    DELETE_FILE((byte) 11);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte code;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    FileControlOperationCode(byte b) {
        this.code = b;
    }

    @DexIgnore
    public final byte getCode() {
        return this.code;
    }

    @DexIgnore
    public final byte responseCode() {
        return (byte) (this.code | Byte.MIN_VALUE);
    }
}
