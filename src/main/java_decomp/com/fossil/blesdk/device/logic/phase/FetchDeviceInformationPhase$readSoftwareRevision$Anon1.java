package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.z00;
import com.fossil.blesdk.obfuscated.z60;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchDeviceInformationPhase$readSoftwareRevision$Anon1 extends Lambda implements jd4<Request, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ FetchDeviceInformationPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchDeviceInformationPhase$readSoftwareRevision$Anon1(FetchDeviceInformationPhase fetchDeviceInformationPhase) {
        super(1);
        this.this$Anon0 = fetchDeviceInformationPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        wd4.b(request, "executedRequest");
        if (wd4.a((Object) Version.CREATOR.a(((z60) request).B()), (Object) z00.f.d())) {
            this.this$Anon0.A();
        } else {
            this.this$Anon0.B();
        }
    }
}
