package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.v60;
import com.fossil.blesdk.obfuscated.w60;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.y60;
import com.fossil.blesdk.setting.JSONKey;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadDeviceInformationByCharacteristicPhase extends Phase {
    @DexIgnore
    public DeviceInformation z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ReadDeviceInformationByCharacteristicPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(r1, r2, PhaseId.READ_DEVICE_INFO_CHARACTERISTICS, r3);
        Peripheral peripheral2 = peripheral;
        Phase.a aVar2 = aVar;
        String str2 = str;
        wd4.b(peripheral2, "peripheral");
        wd4.b(aVar2, "delegate");
        wd4.b(str2, "phaseUuid");
        this.z = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (rd4) null);
    }

    @DexIgnore
    public final DeviceInformation A() {
        return this.z;
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Request) new w60(j()), (jd4) new ReadDeviceInformationByCharacteristicPhase$readDeviceModel$Anon1(this), (jd4) new ReadDeviceInformationByCharacteristicPhase$readDeviceModel$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new v60(j()), (jd4) new ReadDeviceInformationByCharacteristicPhase$readFirmwareVersion$Anon1(this), (jd4) new ReadDeviceInformationByCharacteristicPhase$readFirmwareVersion$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void D() {
        Phase.a((Phase) this, (Request) new y60(j()), (jd4) new ReadDeviceInformationByCharacteristicPhase$readSerialNumber$Anon1(this), (jd4) new ReadDeviceInformationByCharacteristicPhase$readSerialNumber$Anon2(this), (kd4) null, (jd4) null, (jd4) null, 56, (Object) null);
    }

    @DexIgnore
    public void t() {
        D();
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(super.x(), JSONKey.DEVICE_INFO, this.z.toJSONObject());
    }

    @DexIgnore
    public DeviceInformation i() {
        return this.z;
    }
}
