package com.fossil.blesdk.device;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$makeDeviceReady$task$Anon1 extends Lambda implements jd4<Phase.Result, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $phaseController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$makeDeviceReady$task$Anon1(Ref$ObjectRef ref$ObjectRef) {
        super(1);
        this.$phaseController = ref$ObjectRef;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase.Result) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase.Result result) {
        wd4.b(result, "it");
        Phase phase = (Phase) this.$phaseController.element;
        if (phase != null) {
            phase.a(Phase.Result.ResultCode.INTERRUPTED);
        }
    }
}
