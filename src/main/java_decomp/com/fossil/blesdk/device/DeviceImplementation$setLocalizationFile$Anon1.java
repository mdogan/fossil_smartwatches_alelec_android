package com.fossil.blesdk.device;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.r50;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setLocalizationFile$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$setLocalizationFile$Anon1(DeviceImplementation deviceImplementation) {
        super(1);
        this.this$Anon0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        Phase phase2 = phase;
        wd4.b(phase2, "executedPhase");
        r50 r50 = (r50) phase2;
        DeviceImplementation deviceImplementation = this.this$Anon0;
        deviceImplementation.q = DeviceInformation.copy$default(deviceImplementation.getDeviceInformation(), (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, r50.O().getLocaleString(), (Version) null, 98303, (Object) null);
        this.this$Anon0.getDeviceInformation().getCurrentFilesVersion$blesdk_productionRelease().put((short) 1794, r50.O().getFileVersion$blesdk_productionRelease());
    }
}
