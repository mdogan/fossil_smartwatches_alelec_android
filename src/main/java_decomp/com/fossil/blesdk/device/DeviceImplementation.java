package com.fossil.blesdk.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.alarm.Alarm;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingType;
import com.fossil.blesdk.device.data.complication.ComplicationConfig;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.enumerate.Channel;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.device.data.music.TrackInfo;
import com.fossil.blesdk.device.data.notification.AppNotification;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.device.data.watchapp.WatchAppConfig;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.device.event.DeviceEventManager;
import com.fossil.blesdk.device.logic.PhaseManager;
import com.fossil.blesdk.device.logic.phase.CleanUpDevicePhase;
import com.fossil.blesdk.device.logic.phase.ConnectHIDPhase;
import com.fossil.blesdk.device.logic.phase.DisconnectPhase;
import com.fossil.blesdk.device.logic.phase.ExchangeSecretKeyPhase;
import com.fossil.blesdk.device.logic.phase.FetchDeviceInformationPhase;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase;
import com.fossil.blesdk.device.logic.phase.OtaPhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SendAsyncEventAckPhase;
import com.fossil.blesdk.device.logic.phase.SetBackgroundImagePhase;
import com.fossil.blesdk.device.logic.phase.SetNotificationFilterPhase;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.phase.StartAuthenticationPhase;
import com.fossil.blesdk.device.logic.phase.StopCurrentWorkoutSessionPhase;
import com.fossil.blesdk.device.logic.phase.StreamingPhase;
import com.fossil.blesdk.device.logic.phase.SyncFlowPhase;
import com.fossil.blesdk.device.logic.phase.VerifySecretKeyPhase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppData;
import com.fossil.blesdk.model.devicedata.ChanceOfRainComplicationData;
import com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData;
import com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData;
import com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.devicedata.IFTTTWatchAppData;
import com.fossil.blesdk.model.devicedata.MicroAppErrorData;
import com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData;
import com.fossil.blesdk.model.devicedata.RingPhoneData;
import com.fossil.blesdk.model.devicedata.WeatherComplicationData;
import com.fossil.blesdk.model.devicedata.WeatherWatchAppData;
import com.fossil.blesdk.model.file.LocalizationFile;
import com.fossil.blesdk.model.file.WatchParameterFile;
import com.fossil.blesdk.model.microapp.MicroAppMapping;
import com.fossil.blesdk.obfuscated.a40;
import com.fossil.blesdk.obfuscated.a60;
import com.fossil.blesdk.obfuscated.ab4;
import com.fossil.blesdk.obfuscated.b40;
import com.fossil.blesdk.obfuscated.b60;
import com.fossil.blesdk.obfuscated.b90;
import com.fossil.blesdk.obfuscated.c40;
import com.fossil.blesdk.obfuscated.c50;
import com.fossil.blesdk.obfuscated.c60;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.d40;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.e40;
import com.fossil.blesdk.obfuscated.e50;
import com.fossil.blesdk.obfuscated.e60;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.f40;
import com.fossil.blesdk.obfuscated.f50;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.g30;
import com.fossil.blesdk.obfuscated.g40;
import com.fossil.blesdk.obfuscated.g50;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h30;
import com.fossil.blesdk.obfuscated.h40;
import com.fossil.blesdk.obfuscated.h50;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i30;
import com.fossil.blesdk.obfuscated.i40;
import com.fossil.blesdk.obfuscated.ib0;
import com.fossil.blesdk.obfuscated.j30;
import com.fossil.blesdk.obfuscated.j40;
import com.fossil.blesdk.obfuscated.j50;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.k30;
import com.fossil.blesdk.obfuscated.k40;
import com.fossil.blesdk.obfuscated.k50;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l30;
import com.fossil.blesdk.obfuscated.l40;
import com.fossil.blesdk.obfuscated.l90;
import com.fossil.blesdk.obfuscated.lb0;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.m30;
import com.fossil.blesdk.obfuscated.m40;
import com.fossil.blesdk.obfuscated.m50;
import com.fossil.blesdk.obfuscated.n30;
import com.fossil.blesdk.obfuscated.n40;
import com.fossil.blesdk.obfuscated.nb0;
import com.fossil.blesdk.obfuscated.o30;
import com.fossil.blesdk.obfuscated.o40;
import com.fossil.blesdk.obfuscated.p30;
import com.fossil.blesdk.obfuscated.p40;
import com.fossil.blesdk.obfuscated.p60;
import com.fossil.blesdk.obfuscated.q30;
import com.fossil.blesdk.obfuscated.q40;
import com.fossil.blesdk.obfuscated.q50;
import com.fossil.blesdk.obfuscated.r00;
import com.fossil.blesdk.obfuscated.r30;
import com.fossil.blesdk.obfuscated.r40;
import com.fossil.blesdk.obfuscated.r50;
import com.fossil.blesdk.obfuscated.r70;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.s30;
import com.fossil.blesdk.obfuscated.s40;
import com.fossil.blesdk.obfuscated.s50;
import com.fossil.blesdk.obfuscated.s70;
import com.fossil.blesdk.obfuscated.t30;
import com.fossil.blesdk.obfuscated.t40;
import com.fossil.blesdk.obfuscated.t70;
import com.fossil.blesdk.obfuscated.u30;
import com.fossil.blesdk.obfuscated.u40;
import com.fossil.blesdk.obfuscated.u50;
import com.fossil.blesdk.obfuscated.u70;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.v30;
import com.fossil.blesdk.obfuscated.v40;
import com.fossil.blesdk.obfuscated.v50;
import com.fossil.blesdk.obfuscated.w00;
import com.fossil.blesdk.obfuscated.w30;
import com.fossil.blesdk.obfuscated.w40;
import com.fossil.blesdk.obfuscated.w50;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x30;
import com.fossil.blesdk.obfuscated.x40;
import com.fossil.blesdk.obfuscated.x50;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.y30;
import com.fossil.blesdk.obfuscated.y40;
import com.fossil.blesdk.obfuscated.y50;
import com.fossil.blesdk.obfuscated.z00;
import com.fossil.blesdk.obfuscated.z30;
import com.fossil.blesdk.obfuscated.z50;
import com.fossil.blesdk.obfuscated.za0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation implements Device, h40, w30, b40, c40, g30, e40, j40, j30, k30, l30, n30, l40, o30, p30, r30, u30, v30, w40, x40, y40, q30, m40, h30, y30, z30, o40, a40, s40, v40, p40, t30, m30, d40, f40, g40, n40, i40, k40, t40, u40, q40, s30, r40, i30, x30 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Phase.a e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ Peripheral g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ PhaseManager j;
    @DexIgnore
    public /* final */ DeviceEventManager k;
    @DexIgnore
    public /* final */ d l;
    @DexIgnore
    public /* final */ jd4<AsyncEvent, cb4> m;
    @DexIgnore
    public Device.b n;
    @DexIgnore
    public String o;
    @DexIgnore
    public /* final */ Object p;
    @DexIgnore
    public DeviceInformation q;
    @DexIgnore
    public Device.State r;
    @DexIgnore
    public Device.a s;
    @DexIgnore
    public /* final */ BluetoothDevice t;
    @DexIgnore
    public /* final */ String u;

    @DexIgnore
    public enum DeviceSynchronousEventName {
        ENABLE_MAINTAINING_CONNECTION,
        ENABLE_MAINTAINING_HID_CONNECTION,
        APP_DISCONNECT,
        APP_DISCONNECT_HID,
        SET_SECRET_KEY,
        DEVICE_STATE_CHANGED;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DeviceImplementation> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public DeviceImplementation createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            b bVar = b.d;
            Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
            if (readParcelable != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) readParcelable;
                String readString = parcel.readString();
                if (readString != null) {
                    return bVar.a(bluetoothDevice, readString);
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }

        @DexIgnore
        public DeviceImplementation[] newArray(int i) {
            return new DeviceImplementation[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ String a; // = a;
        @DexIgnore
        public static /* final */ Hashtable<String, DeviceImplementation> b; // = new Hashtable<>();
        @DexIgnore
        public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
        @DexIgnore
        public static /* final */ b d;

        /*
        static {
            b bVar = new b();
            d = bVar;
            bVar.a();
        }
        */

        @DexIgnore
        public static /* synthetic */ DeviceImplementation a(b bVar, BluetoothDevice bluetoothDevice, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                str = "";
            }
            return bVar.a(bluetoothDevice, str);
        }

        @DexIgnore
        public final String b(String str) {
            wd4.b(str, "macAddress");
            synchronized (c) {
                for (Map.Entry next : c.entrySet()) {
                    String str2 = (String) next.getKey();
                    if (cg4.b((String) next.getValue(), str, true)) {
                        return str2;
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public final DeviceImplementation a(BluetoothDevice bluetoothDevice, String str) {
            DeviceImplementation deviceImplementation;
            DeviceImplementation deviceImplementation2;
            BluetoothDevice bluetoothDevice2 = bluetoothDevice;
            String str2 = str;
            wd4.b(bluetoothDevice2, "bluetoothDevice");
            wd4.b(str2, "serialNumber");
            u90 u90 = u90.c;
            String str3 = a;
            u90.a(str3, "getDevice: MAC: " + bluetoothDevice.getAddress() + ", " + "serial number: " + str2);
            synchronized (b) {
                DeviceImplementation deviceImplementation3 = b.get(bluetoothDevice.getAddress());
                if (deviceImplementation3 == null) {
                    deviceImplementation = new DeviceImplementation(bluetoothDevice2, str2, (rd4) null);
                    b.put(deviceImplementation.getDeviceInformation().getMacAddress(), deviceImplementation);
                    if (DeviceInformation.Companion.a(str2)) {
                        d.a(str2, deviceImplementation.getDeviceInformation().getMacAddress());
                    }
                } else {
                    if (DeviceInformation.Companion.a(deviceImplementation3.getDeviceInformation().getSerialNumber()) || !DeviceInformation.Companion.a(str2)) {
                        deviceImplementation2 = deviceImplementation3;
                    } else {
                        deviceImplementation2 = deviceImplementation3;
                        deviceImplementation2.q = DeviceInformation.copy$default(deviceImplementation3.getDeviceInformation(), (String) null, (String) null, str, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131067, (Object) null);
                        b.put(deviceImplementation2.getDeviceInformation().getMacAddress(), deviceImplementation2);
                        d.a(str, deviceImplementation2.getDeviceInformation().getMacAddress());
                    }
                    deviceImplementation = deviceImplementation2;
                }
            }
            return deviceImplementation;
        }

        @DexIgnore
        public final void b() {
            JSONArray jSONArray = new JSONArray();
            synchronized (c) {
                for (Map.Entry next : c.entrySet()) {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put(Constants.SERIAL_NUMBER, next.getKey());
                        jSONObject.put(PlaceManager.PARAM_MAC_ADDRESS, next.getValue());
                    } catch (Exception e) {
                        ea0.l.a(e);
                    }
                    jSONArray.put(jSONObject);
                }
                cb4 cb4 = cb4.a;
            }
            String jSONArray2 = jSONArray.toString();
            wd4.a((Object) jSONArray2, "array.toString()");
            String b2 = lb0.c.b(jSONArray2);
            SharedPreferences a2 = za0.a(SharedPreferenceFileName.MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE);
            if (a2 != null) {
                a2.edit().putString("com.fossil.blesdk.device.cache", b2).apply();
            }
        }

        @DexIgnore
        public final String a(String str) {
            String str2;
            wd4.b(str, "serialNumber");
            synchronized (c) {
                str2 = c.get(str);
            }
            return str2;
        }

        @DexIgnore
        public final void a(String str, String str2) {
            wd4.b(str, "serialNumber");
            wd4.b(str2, "macAddress");
            if (DeviceInformation.Companion.a(str) && BluetoothAdapter.checkBluetoothAddress(str2)) {
                synchronized (c) {
                    c.put(str, str2);
                    cb4 cb4 = cb4.a;
                }
            }
        }

        @DexIgnore
        public final void a() {
            SharedPreferences a2 = za0.a(SharedPreferenceFileName.MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE);
            if (a2 != null) {
                String string = a2.getString("com.fossil.blesdk.device.cache", (String) null);
                if (string != null) {
                    String a3 = lb0.c.a(string);
                    if (a3 != null) {
                        try {
                            JSONArray jSONArray = new JSONArray(a3);
                            int length = jSONArray.length();
                            for (int i = 0; i < length; i++) {
                                JSONObject jSONObject = jSONArray.getJSONObject(i);
                                String string2 = jSONObject.getString(PlaceManager.PARAM_MAC_ADDRESS);
                                String string3 = jSONObject.getString(Constants.SERIAL_NUMBER);
                                wd4.a((Object) string3, "serialNumber");
                                wd4.a((Object) string2, "macAddress");
                                a(string3, string2);
                            }
                        } catch (Exception e) {
                            ea0.l.a(e);
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation e;
        @DexIgnore
        public /* final */ /* synthetic */ Device.State f;
        @DexIgnore
        public /* final */ /* synthetic */ Device.State g;

        @DexIgnore
        public c(DeviceImplementation deviceImplementation, Device.State state, Device.State state2) {
            this.e = deviceImplementation;
            this.f = state;
            this.g = state2;
        }

        @DexIgnore
        public final void run() {
            Device.a n = this.e.n();
            if (n != null) {
                n.onDeviceStateChanged(this.e, this.f, this.g);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Phase.a {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation a;

        @DexIgnore
        public e(DeviceImplementation deviceImplementation) {
            this.a = deviceImplementation;
        }

        @DexIgnore
        public PhaseManager a() {
            return this.a.j;
        }

        @DexIgnore
        public DeviceInformation getDeviceInformation() {
            return this.a.getDeviceInformation();
        }
    }

    @DexIgnore
    public /* synthetic */ DeviceImplementation(BluetoothDevice bluetoothDevice, String str, rd4 rd4) {
        this(bluetoothDevice, str);
    }

    @DexIgnore
    public g90<cb4> cleanUp() {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "cleanUp invoked.", false, 8, (Object) null);
        CleanUpDevicePhase cleanUpDevicePhase = new CleanUpDevicePhase(this.g, this.e);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) cleanUpDevicePhase);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                cleanUpDevicePhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon41(this, cleanUpDevicePhase));
                cleanUpDevicePhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon42(this, cleanUpDevicePhase));
                cleanUpDevicePhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon43(g90, this, cleanUpDevicePhase));
                cleanUpDevicePhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$cleanUp$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, cleanUpDevicePhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                cleanUpDevicePhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public Device.BondState getBondState() {
        return Device.BondState.Companion.a(this.g.getBondState());
    }

    @DexIgnore
    public DeviceInformation getDeviceInformation() {
        return this.q;
    }

    @DexIgnore
    public Device.State getState() {
        return this.r;
    }

    @DexIgnore
    public h90<cb4> h() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "releaseHands invoked.", false, 8, (Object) null);
        Peripheral peripheral = this.g;
        SingleRequestPhase singleRequestPhase = new SingleRequestPhase(peripheral, this.e, PhaseId.RELEASE_HANDS, new s70(peripheral));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) singleRequestPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                singleRequestPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon85(this, singleRequestPhase));
                singleRequestPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon86(this, singleRequestPhase));
                singleRequestPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon87(g90, this, singleRequestPhase));
                singleRequestPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$releaseHands$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, singleRequestPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                singleRequestPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> i() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "requestHands invoked.", false, 8, (Object) null);
        Peripheral peripheral = this.g;
        SingleRequestPhase singleRequestPhase = new SingleRequestPhase(peripheral, this.e, PhaseId.REQUEST_HANDS, new t70(peripheral));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) singleRequestPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                singleRequestPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon77(this, singleRequestPhase));
                singleRequestPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon78(this, singleRequestPhase));
                singleRequestPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon79(g90, this, singleRequestPhase));
                singleRequestPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$requestHands$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, singleRequestPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                singleRequestPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public boolean isActive() {
        return ConnectionManager.j.d(this);
    }

    @DexIgnore
    public g90<HashMap<DeviceConfigKey, DeviceConfigItem>> j() {
        g90<HashMap<DeviceConfigKey, DeviceConfigItem>> g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "getConfigurations invoked.", false, 8, (Object) null);
        h50 h50 = new h50(this.g, this.e, 0, 4, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) h50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                h50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon53(this, h50));
                h50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon54(this, h50));
                h50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon55(g90, this, h50));
                h50.a((jd4<? super Phase, cb4>) new DeviceImplementation$getConfigurations$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, h50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                h50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<WorkoutSession> k() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "getCurrentWorkoutSession invoked.", false, 8, (Object) null);
        e50 e50 = new e50(this.g, this.e);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) e50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                e50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon125(this, e50));
                e50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon126(this, e50));
                e50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon127(g90, this, e50));
                e50.a((jd4<? super Phase, cb4>) new DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, e50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                e50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> l() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "setCalibrationPosition invoked.", false, 8, (Object) null);
        Peripheral peripheral = this.g;
        SingleRequestPhase singleRequestPhase = new SingleRequestPhase(peripheral, this.e, PhaseId.SET_CALIBRATION_POSITION, new u70(peripheral));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) singleRequestPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                singleRequestPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon89(this, singleRequestPhase));
                singleRequestPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon90(this, singleRequestPhase));
                singleRequestPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon91(g90, this, singleRequestPhase));
                singleRequestPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$setCalibrationPosition$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, singleRequestPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                singleRequestPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public final void m() {
        this.j.b(Phase.Result.ResultCode.CONNECTION_DROPPED, new PhaseId[]{PhaseId.STREAMING});
        this.j.a(new b90.b().a());
        this.h = false;
    }

    @DexIgnore
    public Device.a n() {
        return this.s;
    }

    @DexIgnore
    public Device.HIDState o() {
        return Device.HIDState.Companion.a(this.g.j());
    }

    @DexIgnore
    public final Peripheral p() {
        return this.g;
    }

    @DexIgnore
    public final boolean q() {
        boolean z;
        synchronized (Boolean.valueOf(this.i)) {
            z = this.i;
        }
        return z;
    }

    @DexIgnore
    public final void r() {
        a(this, LogLevel.DEBUG, "DeviceImplementation", "internalDisconnect invoked.", false, 8, (Object) null);
        this.j.a(Phase.Result.ResultCode.INTERRUPTED, new PhaseId[]{PhaseId.STREAMING});
        if (getState() != Device.State.DISCONNECTED && !this.h) {
            DisconnectPhase disconnectPhase = new DisconnectPhase(this.g, this.e);
            disconnectPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$internalDisconnect$Anon1(this));
            disconnectPhase.y();
        }
    }

    @DexIgnore
    public final boolean s() {
        Phase[] b2 = this.j.b();
        int length = b2.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (!(b2[i2] instanceof StreamingPhase)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void t() {
        this.j.d();
    }

    @DexIgnore
    public final void u() {
        DeviceInformation deviceInformation = r0;
        DeviceInformation deviceInformation2 = new DeviceInformation(getDeviceInformation().getName(), getDeviceInformation().getMacAddress(), getDeviceInformation().getSerialNumber(), (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131064, (rd4) null);
        this.q = deviceInformation;
    }

    @DexIgnore
    public final boolean v() {
        T t2;
        boolean z;
        if (this.g.getState() == Peripheral.State.CONNECTED) {
            Iterator<T> it = this.j.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (((Phase) t2).g() == PhaseId.STREAMING) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void w() {
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        this.o = uuid;
        da0.e.a(getDeviceInformation().getMacAddress(), this.o);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.t, i2);
        }
        if (parcel != null) {
            parcel.writeString(this.u);
        }
    }

    @DexIgnore
    public final void x() {
        StreamingPhase streamingPhase = new StreamingPhase(this.g, this.e);
        streamingPhase.e(this.m);
        streamingPhase.d((jd4<? super Phase, cb4>) DeviceImplementation$startStreaming$streamingPhase$Anon1.INSTANCE);
        streamingPhase.b((jd4<? super Phase, cb4>) new DeviceImplementation$startStreaming$streamingPhase$Anon2(this));
        streamingPhase.y();
    }

    @DexIgnore
    public final h90<cb4> y() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        u90 u90 = u90.c;
        String simpleName = DeviceImplementation.class.getSimpleName();
        wd4.a((Object) simpleName, "this::class.java.simpleName");
        u90.c(simpleName, "syncInBackground invoked.");
        e60 e60 = new e60(this.g, this.e, (HashMap) null, 4, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) e60);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                e60.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon9(this, e60));
                e60.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon10(this, e60));
                e60.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon11(g90, this, e60));
                e60.a((jd4<? super Phase, cb4>) new DeviceImplementation$syncInBackground$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, e60));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                e60.y();
            }
        }
        return g90.f(DeviceImplementation$syncInBackground$Anon1.INSTANCE);
    }

    @DexIgnore
    public DeviceImplementation(BluetoothDevice bluetoothDevice, String str) {
        this.t = bluetoothDevice;
        this.u = str;
        this.e = new e(this);
        this.f = ib0.a.a();
        this.g = Peripheral.e.b.a(this.t);
        this.j = new PhaseManager(new b90.b().a());
        this.k = new DeviceEventManager();
        this.l = new d(this);
        this.m = new DeviceImplementation$streamingEventListener$Anon1(this);
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        this.o = uuid;
        this.p = new Object();
        this.q = new DeviceInformation(this.g.i(), this.g.k(), this.u, "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (rd4) null);
        this.r = Device.State.DISCONNECTED;
        this.g.a((Peripheral.f) this.l);
        a(getDeviceInformation());
    }

    @DexIgnore
    public final void b(boolean z) {
        synchronized (Boolean.valueOf(this.i)) {
            this.i = z;
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public g90<DeviceInformation> c() {
        g90<DeviceInformation> g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "fetchDeviceInformation invoked.", false, 8, (Object) null);
        if (getState() == Device.State.CONNECTED) {
            g90<DeviceInformation> g902 = new g90<>();
            g902.a(1.0f);
            g902.c(getDeviceInformation());
            return g902;
        }
        FetchDeviceInformationPhase fetchDeviceInformationPhase = new FetchDeviceInformationPhase(this.g, this.e, (String) null, 4, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) fetchDeviceInformationPhase);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                fetchDeviceInformationPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon29(this, fetchDeviceInformationPhase));
                fetchDeviceInformationPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon30(this, fetchDeviceInformationPhase));
                fetchDeviceInformationPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon31(g90, this, fetchDeviceInformationPhase));
                fetchDeviceInformationPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$fetchDeviceInformation$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, fetchDeviceInformationPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                fetchDeviceInformationPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> d() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "stopCurrentWorkoutSession invoked.", false, 8, (Object) null);
        StopCurrentWorkoutSessionPhase stopCurrentWorkoutSessionPhase = new StopCurrentWorkoutSessionPhase(this.g, this.e, (String) null, 4, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) stopCurrentWorkoutSessionPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                stopCurrentWorkoutSessionPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon129(this, stopCurrentWorkoutSessionPhase));
                stopCurrentWorkoutSessionPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon130(this, stopCurrentWorkoutSessionPhase));
                stopCurrentWorkoutSessionPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon131(g90, this, stopCurrentWorkoutSessionPhase));
                stopCurrentWorkoutSessionPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$stopCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, stopCurrentWorkoutSessionPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                stopCurrentWorkoutSessionPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public void e(byte[] bArr) {
        byte[] bArr2 = bArr;
        wd4.b(bArr2, "secretKey");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setSecretKey = " + l90.a(bArr2, (String) null, 1, (Object) null) + '.', false, 8, (Object) null);
        ea0 ea0 = ea0.l;
        String logName$blesdk_productionRelease = DeviceSynchronousEventName.SET_SECRET_KEY.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.REQUEST;
        String k2 = this.g.k();
        String logName$blesdk_productionRelease2 = DeviceSynchronousEventName.SET_SECRET_KEY.getLogName$blesdk_productionRelease();
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, k2, logName$blesdk_productionRelease2, uuid, true, this.o, (DeviceInformation) null, (fa0) null, xa0.a(new JSONObject(), JSONKey.SECRET_KEY_CRC, Long.valueOf(Crc32Calculator.a.a(bArr2, Crc32Calculator.CrcType.CRC32))), 384, (rd4) null));
        d90.b.a(this.g.k(), bArr2);
    }

    @DexIgnore
    public boolean f() {
        boolean z;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "enableMaintainingConnection invoked.", false, 8, (Object) null);
        if (isActive()) {
            z = true;
        } else {
            z = ConnectionManager.j.h(this);
        }
        ea0 ea0 = ea0.l;
        String logName$blesdk_productionRelease = DeviceSynchronousEventName.ENABLE_MAINTAINING_CONNECTION.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.REQUEST;
        String k2 = this.g.k();
        String logName$blesdk_productionRelease2 = DeviceSynchronousEventName.ENABLE_MAINTAINING_CONNECTION.getLogName$blesdk_productionRelease();
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, k2, logName$blesdk_productionRelease2, uuid, z, this.o, (DeviceInformation) null, (fa0) null, (JSONObject) null, 896, (rd4) null));
        return z;
    }

    @DexIgnore
    public h90<Integer> g() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "readRssi invoked.", false, 8, (Object) null);
        u50 u50 = new u50(this.g, this.e);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) u50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                u50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon57(this, u50));
                u50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon58(this, u50));
                u50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon59(g90, this, u50));
                u50.a((jd4<? super Phase, cb4>) new DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, u50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                u50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public void b(Device.State state) {
        wd4.b(state, "<set-?>");
        this.r = state;
    }

    @DexIgnore
    public final w00<DeviceInformation, Phase.Result> b(HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object> hashMap) {
        HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object> hashMap2 = hashMap;
        wd4.b(hashMap2, "options");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "makeDeviceReady invoked: options=" + hashMap2 + '.', false, 8, (Object) null);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        w00<DeviceInformation, Phase.Result> b2 = new w00().b(new DeviceImplementation$makeDeviceReady$task$Anon1(ref$ObjectRef));
        if (getState() == Device.State.CONNECTED) {
            b2.c(getDeviceInformation());
        } else if (!this.h) {
            w();
            ref$ObjectRef.element = new MakeDeviceReadyPhase(this.g, this.e, hashMap, (String) null, 8, (rd4) null);
            Phase phase = (Phase) ref$ObjectRef.element;
            phase.c((jd4<? super Phase, cb4>) new DeviceImplementation$makeDeviceReady$Anon1(this));
            phase.d((jd4<? super Phase, cb4>) new DeviceImplementation$makeDeviceReady$Anon2(this, b2));
            phase.b((jd4<? super Phase, cb4>) new DeviceImplementation$makeDeviceReady$Anon3(this, b2));
            ((Phase) ref$ObjectRef.element).y();
        } else {
            a(this, LogLevel.DEBUG, "DeviceImplementation", "makeDeviceReady: isMakingDeviceReady=true, not start another making device ready.", false, 8, (Object) null);
            b2.b(new Phase.Result(PhaseId.MAKE_DEVICE_READY, Phase.Result.ResultCode.NOT_ALLOW_TO_START, (Request.Result) null, 4, (rd4) null));
        }
        return b2;
    }

    @DexIgnore
    public void a(Device.a aVar) {
        this.s = aVar;
    }

    @DexIgnore
    public final h90<cb4> a(AsyncEvent asyncEvent) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(asyncEvent, "asyncEvent");
        u90 u90 = u90.c;
        String simpleName = DeviceImplementation.class.getSimpleName();
        wd4.a((Object) simpleName, "this::class.java.simpleName");
        u90.c(simpleName, "sendAsyncEventAck invoked: " + asyncEvent.toJSONString(2));
        SendAsyncEventAckPhase sendAsyncEventAckPhase = new SendAsyncEventAckPhase(this.g, this.e, asyncEvent);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) sendAsyncEventAckPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                sendAsyncEventAckPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon1(this, sendAsyncEventAckPhase));
                sendAsyncEventAckPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon2(this, sendAsyncEventAckPhase));
                sendAsyncEventAckPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon3(g90, this, sendAsyncEventAckPhase));
                sendAsyncEventAckPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$sendAsyncEventAck$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, sendAsyncEventAckPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                sendAsyncEventAckPhase.y();
            }
        }
        return g90;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Peripheral.f {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation a;

        @DexIgnore
        public d(DeviceImplementation deviceImplementation) {
            this.a = deviceImplementation;
        }

        @DexIgnore
        public void a(Peripheral peripheral, Peripheral.State state) {
            Phase phase;
            Phase phase2;
            wd4.b(peripheral, "peripheral");
            wd4.b(state, "newState");
            if (r00.a[state.ordinal()] == 1) {
                this.a.b(false);
            }
            if (this.a.v()) {
                this.a.x();
            }
            Phase[] c = this.a.j.c();
            int length = c.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    phase = null;
                    break;
                }
                phase = c[i];
                if (phase.g() == PhaseId.OTA) {
                    break;
                }
                i++;
            }
            if (phase == null) {
                int i2 = r00.b[state.ordinal()];
                if (i2 == 1) {
                    Phase[] c2 = this.a.j.c();
                    int length2 = c2.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length2) {
                            phase2 = null;
                            break;
                        }
                        phase2 = c2[i3];
                        if (phase2.g() == PhaseId.MAKE_DEVICE_READY) {
                            break;
                        }
                        i3++;
                    }
                    if (phase2 == null) {
                        this.a.a(Device.State.DISCONNECTED);
                    }
                } else if (i2 == 2) {
                    this.a.a(Device.State.CONNECTING);
                } else if (i2 != 3 && i2 == 4) {
                    this.a.a(Device.State.DISCONNECTING);
                }
            }
        }

        @DexIgnore
        public void a(Peripheral peripheral, Peripheral.HIDState hIDState, Peripheral.HIDState hIDState2) {
            wd4.b(peripheral, "peripheral");
            wd4.b(hIDState, "previousHIDState");
            wd4.b(hIDState2, "newHIDState");
            if (wd4.a((Object) peripheral, (Object) this.a.p())) {
                this.a.b(Device.HIDState.Companion.a(hIDState), Device.HIDState.Companion.a(hIDState2));
            }
        }
    }

    @DexIgnore
    public byte[] e() {
        return d90.b.a(this.g.k()).b();
    }

    @DexIgnore
    public static /* synthetic */ g90 b(DeviceImplementation deviceImplementation, boolean z, boolean z2, Priority priority, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                priority = Priority.LOW;
            } else {
                priority = Priority.NORMAL;
            }
        }
        return deviceImplementation.b(z, z2, priority);
    }

    @DexIgnore
    public final g90<byte[][]> b(boolean z, boolean z2, Priority priority) {
        k50 k50;
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(priority, "phasePriority");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "getHardwareLog invoked: skip erase=" + z + ", " + "phasePriority=" + priority + ", " + "triggerByAsyncEvent=" + z2, false, 8, (Object) null);
        if (z2) {
            k50 = new j50(this.g, this.e, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, false), ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS, false)}));
        } else {
            k50 k502 = new k50(this.g, this.e, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, Boolean.valueOf(z)), ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))}), (String) null, 8, (rd4) null);
            k502.a(priority);
            k50 = k502;
        }
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) k50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                k50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon5(this, k50));
                k50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon6(this, k50));
                k50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon7(g90, this, k50));
                k50.a((jd4<? super Phase, cb4>) new DeviceImplementation$getHardwareLog$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, k50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                k50.y();
            }
        }
        return g90.f(DeviceImplementation$getHardwareLog$Anon1.INSTANCE);
    }

    @DexIgnore
    public h90<byte[]> d(byte[] bArr) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(bArr, "bothSidesEncryptedRandomNumbers");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "exchangeSecretKey invoked: " + "bothSidesEncryptedRandomNumbers = " + l90.a(bArr, (String) null, 1, (Object) null), false, 8, (Object) null);
        ExchangeSecretKeyPhase exchangeSecretKeyPhase = new ExchangeSecretKeyPhase(this.g, this.e, bArr);
        exchangeSecretKeyPhase.d((jd4<? super Phase, cb4>) new DeviceImplementation$exchangeSecretKey$Anon1(this));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) exchangeSecretKeyPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                exchangeSecretKeyPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon149(this, exchangeSecretKeyPhase));
                exchangeSecretKeyPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon150(this, exchangeSecretKeyPhase));
                exchangeSecretKeyPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon151(g90, this, exchangeSecretKeyPhase));
                exchangeSecretKeyPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$exchangeSecretKey$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, exchangeSecretKeyPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                exchangeSecretKeyPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<Boolean> c(byte[] bArr) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(bArr, "secretKey");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "verifySecretKey invoked: " + "secretKeyCrc = " + Crc32Calculator.a.a(bArr, Crc32Calculator.CrcType.CRC32), false, 8, (Object) null);
        VerifySecretKeyPhase verifySecretKeyPhase = new VerifySecretKeyPhase(this.g, this.e, bArr);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) verifySecretKeyPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                verifySecretKeyPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon153(this, verifySecretKeyPhase));
                verifySecretKeyPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon154(this, verifySecretKeyPhase));
                verifySecretKeyPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon155(g90, this, verifySecretKeyPhase));
                verifySecretKeyPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$verifySecretKey$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, verifySecretKeyPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                verifySecretKeyPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public static /* synthetic */ g90 a(DeviceImplementation deviceImplementation, boolean z, boolean z2, Priority priority, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                priority = Priority.LOW;
            } else {
                priority = Priority.NORMAL;
            }
        }
        return deviceImplementation.a(z, z2, priority);
    }

    @DexIgnore
    public final g90<byte[][]> a(boolean z, boolean z2, Priority priority) {
        g50 g50;
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(priority, "phasePriority");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "getDataCollectionFile invoked: skip erase=" + z + ", " + "phasePriority=" + priority + ", " + "triggerByAsyncEvent=" + z2, false, 8, (Object) null);
        if (z2) {
            g50 = new f50(this.g, this.e, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, false), ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS, true)}));
        } else {
            g50 g502 = new g50(this.g, this.e, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, Boolean.valueOf(z)), ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))}), (String) null, 8, (rd4) null);
            g502.a(priority);
            g50 = g502;
        }
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) g50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                g50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon13(this, g50));
                g50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon14(this, g50));
                g50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon15(g90, this, g50));
                g50.a((jd4<? super Phase, cb4>) new DeviceImplementation$getDataCollectionFile$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, g50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                g50.y();
            }
        }
        return g90.f(DeviceImplementation$getDataCollectionFile$Anon1.INSTANCE);
    }

    @DexIgnore
    public final void b(Device.State state, Device.State state2) {
        a(state, state2);
        ib0.a.a().post(new c(this, state, state2));
    }

    @DexIgnore
    public final void b(Device.HIDState hIDState, Device.HIDState hIDState2) {
        a(hIDState, hIDState2);
    }

    @DexIgnore
    public g90<String> b(byte[] bArr) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(bArr, "firmwareData");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "ota invoked: file size: " + bArr.length, false, 8, (Object) null);
        OtaPhase otaPhase = new OtaPhase(this.g, this.e, bArr);
        otaPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$ota$Anon1(this));
        otaPhase.d((jd4<? super Phase, cb4>) new DeviceImplementation$ota$Anon2(this));
        otaPhase.b((jd4<? super Phase, cb4>) new DeviceImplementation$ota$Anon3(this));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) otaPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                otaPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon33(this, otaPhase));
                otaPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon34(this, otaPhase));
                otaPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon35(g90, this, otaPhase));
                otaPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$ota$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, otaPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                otaPhase.y();
            }
        }
        return g90.f(DeviceImplementation$ota$Anon4.INSTANCE);
    }

    @DexIgnore
    public static /* synthetic */ h90 a(DeviceImplementation deviceImplementation, HashMap hashMap, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            hashMap = new HashMap();
        }
        return deviceImplementation.a((HashMap<ConnectHIDPhase.ConnectHIDOption, Object>) hashMap);
    }

    @DexIgnore
    public final h90<cb4> a(HashMap<ConnectHIDPhase.ConnectHIDOption, Object> hashMap) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(hashMap, "options");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "internalConnectHID invoked: options=" + hashMap + '.', false, 8, (Object) null);
        ConnectHIDPhase connectHIDPhase = new ConnectHIDPhase(this.g, this.e, hashMap);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) connectHIDPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                connectHIDPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon17(this, connectHIDPhase));
                connectHIDPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon18(this, connectHIDPhase));
                connectHIDPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon19(g90, this, connectHIDPhase));
                connectHIDPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$internalConnectHID$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, connectHIDPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                connectHIDPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> b() {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "playAnimation invoked.", false, 8, (Object) null);
        q50 q50 = new q50(this.g, this.e);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) q50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                q50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon37(this, q50));
                q50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon38(this, q50));
                q50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon39(g90, this, q50));
                q50.a((jd4<? super Phase, cb4>) new DeviceImplementation$playAnimation$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, q50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                q50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public final void a(Error error) {
        wd4.b(error, "error");
        Device.b bVar = this.n;
        if (bVar != null) {
            bVar.a(this, error);
        }
    }

    @DexIgnore
    public final void a(Device.State state) {
        Device.State state2;
        synchronized (getState()) {
            state2 = getState();
            b(state);
            cb4 cb4 = cb4.a;
        }
        if (state2 != state) {
            ea0 ea0 = ea0.l;
            SdkLogEntry sdkLogEntry = r4;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry(DeviceSynchronousEventName.DEVICE_STATE_CHANGED.getLogName$blesdk_productionRelease(), EventType.DEVICE_EVENT, this.g.k(), "", "", true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(new JSONObject(), JSONKey.PREV_STATE, state2.getLogName$blesdk_productionRelease()), JSONKey.NEW_STATE, state.getLogName$blesdk_productionRelease()), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            b(state2, getState());
            int i2 = r00.c[state.ordinal()];
            if (i2 == 1) {
                u();
                m();
            } else if (i2 == 2) {
                u();
            } else if (i2 == 3) {
                a(getDeviceInformation());
                t();
            } else if (i2 != 4 && i2 == 5) {
                u();
            }
        }
    }

    @DexIgnore
    public final void a(Device.State state, Device.State state2) {
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "broadcastDeviceStateChange: device=" + this.t + ", " + "previousState=" + state + ", newState=" + state2, false, 8, (Object) null);
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE", state);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE", state2);
        nb0.a.a(intent);
    }

    @DexIgnore
    public final void a(Device.HIDState hIDState, Device.HIDState hIDState2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE", hIDState);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE", hIDState2);
        nb0.a.a(intent);
    }

    @DexIgnore
    public final void a(DeviceEvent deviceEvent) {
        wd4.b(deviceEvent, Constants.EVENT);
        Device.a n2 = n();
        if (n2 != null) {
            n2.onEventReceived(this, deviceEvent);
        }
    }

    @DexIgnore
    public final void a(Phase.Result result) {
        if (true == (result.getResultCode() == Phase.Result.ResultCode.REQUEST_ERROR && result.getRequestResult().getResultCode() == Request.Result.ResultCode.COMMAND_ERROR && result.getRequestResult().getCommandResult().getResultCode() == BluetoothCommand.Result.ResultCode.GATT_ERROR && (result.getRequestResult().getCommandResult().getGattResult().getResultCode() == GattOperationResult.GattResult.ResultCode.GATT_NULL || result.getRequestResult().getCommandResult().getGattResult().getResultCode() == GattOperationResult.GattResult.ResultCode.START_FAIL))) {
            r();
        }
    }

    @DexIgnore
    public final FeatureErrorCode a(Phase phase) {
        T t2;
        boolean z;
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            return FeatureErrorCode.BLUETOOTH_OFF;
        }
        int i2 = r00.e[phase.g().ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            return null;
        }
        if (i2 == 5) {
            ArrayList<Phase> a2 = this.j.a();
            if (a2.isEmpty()) {
                return null;
            }
            Iterator<T> it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                Phase phase2 = (Phase) t2;
                if (phase2.g() == PhaseId.STREAMING || phase2.g() == PhaseId.MAKE_DEVICE_READY || phase2.m().compareTo(Priority.LOW) <= 0) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return null;
            }
            return FeatureErrorCode.DEVICE_BUSY;
        } else if (getState() != Device.State.UPGRADING_FIRMWARE) {
            return null;
        } else {
            return FeatureErrorCode.DEVICE_BUSY;
        }
    }

    @DexIgnore
    public final void a(DeviceInformation deviceInformation) {
        da0.e.a(deviceInformation.getMacAddress(), deviceInformation);
    }

    @DexIgnore
    public static /* synthetic */ void a(DeviceImplementation deviceImplementation, LogLevel logLevel, String str, String str2, boolean z, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            z = true;
        }
        deviceImplementation.a(logLevel, str, str2, z);
    }

    @DexIgnore
    public final void a(LogLevel logLevel, String str, String str2, boolean z) {
        u90 u90 = u90.c;
        int priority$blesdk_productionRelease = logLevel.getPriority$blesdk_productionRelease();
        if (z) {
            str2 = getDeviceInformation().getMacAddress() + '(' + getDeviceInformation().getSerialNumber() + ") - " + str2;
        }
        u90.a(priority$blesdk_productionRelease, str, str2);
    }

    @DexIgnore
    public <T> T a(Class<T> cls) {
        Class<T> cls2 = cls;
        wd4.b(cls2, "feature");
        if (lb4.b((T[]) getDeviceInformation().getDeviceType().getSupportedFeature$blesdk_productionRelease(), cls2)) {
            LogLevel logLevel = LogLevel.DEBUG;
            a(this, logLevel, "DeviceImplementation", "asFeature invoked: feature=" + cls.getSimpleName() + ' ' + "success.", false, 8, (Object) null);
            return this;
        }
        LogLevel logLevel2 = LogLevel.DEBUG;
        a(this, logLevel2, "DeviceImplementation", "asFeature invoked: feature=" + cls.getSimpleName() + ' ' + "unsupported, return null.", false, 8, (Object) null);
        return null;
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        a(this, LogLevel.DEBUG, "DeviceImplementation", "disconnect invoked.", false, 8, (Object) null);
        if (!isActive()) {
            z = true;
        } else {
            z = ConnectionManager.j.i(this);
        }
        ea0 ea0 = ea0.l;
        String logName$blesdk_productionRelease = DeviceSynchronousEventName.APP_DISCONNECT.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.REQUEST;
        String k2 = this.g.k();
        String logName$blesdk_productionRelease2 = DeviceSynchronousEventName.APP_DISCONNECT.getLogName$blesdk_productionRelease();
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        ea0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, k2, logName$blesdk_productionRelease2, uuid, z, this.o, (DeviceInformation) null, (fa0) null, (JSONObject) null, 896, (rd4) null));
        if (z) {
            r();
        }
        return z;
    }

    @DexIgnore
    public g90<FitnessData[]> a(BiometricProfile biometricProfile) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(biometricProfile, "biometricProfile");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "sync invoked: " + biometricProfile.toJSONString(2), false, 8, (Object) null);
        SyncFlowPhase syncFlowPhase = new SyncFlowPhase(this.g, this.e, biometricProfile, (HashMap) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) syncFlowPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                syncFlowPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon45(this, syncFlowPhase));
                syncFlowPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon46(this, syncFlowPhase));
                syncFlowPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon47(g90, this, syncFlowPhase));
                syncFlowPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$sync$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, syncFlowPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                syncFlowPhase.y();
            }
        }
        return g90.f(DeviceImplementation$sync$Anon1.INSTANCE);
    }

    @DexIgnore
    public g90<DeviceConfigKey[]> a(DeviceConfigItem[] deviceConfigItemArr) {
        g90<DeviceConfigKey[]> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(deviceConfigItemArr, "configs");
        a(this, LogLevel.DEBUG, "DeviceImplementation", "setConfigurations invoked: " + lb4.a(deviceConfigItemArr, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, DeviceImplementation$setConfigurations$Anon1.INSTANCE, 31, (Object) null), false, 8, (Object) null);
        ArrayList arrayList = new ArrayList();
        for (DeviceConfigItem deviceConfigItem : deviceConfigItemArr) {
            if (lb4.b((T[]) getDeviceInformation().getSupportedDeviceConfigKeys$blesdk_productionRelease(), deviceConfigItem.getKey())) {
                arrayList.add(deviceConfigItem);
            }
        }
        if (arrayList.isEmpty()) {
            g90<DeviceConfigKey[]> g902 = new g90<>();
            g902.c(new DeviceConfigKey[0]);
            return g902;
        }
        Peripheral peripheral = this.g;
        Phase.a aVar = this.e;
        Object[] array = arrayList.toArray(new DeviceConfigItem[0]);
        if (array != null) {
            a60 a60 = new a60(peripheral, aVar, (DeviceConfigItem[]) array, 0, (String) null, 24, (rd4) null);
            synchronized (this.p) {
                FeatureErrorCode a2 = a((Phase) a60);
                g90 = new g90<>();
                if (a2 != null) {
                    if (r00.d[a2.ordinal()] != 1) {
                        resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                    } else {
                        resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                    }
                    a60.a(resultCode);
                    g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
                } else {
                    g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon49(this, a60));
                    a60.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon50(this, a60));
                    a60.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon51(g90, this, a60));
                    a60.a((jd4<? super Phase, cb4>) new DeviceImplementation$setConfigurations$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, a60));
                    if (getState() != Device.State.CONNECTED && !this.h) {
                        b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                    }
                    a60.y();
                }
            }
            return g90;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public h90<cb4> a(byte[] bArr, Channel channel) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(bArr, "commandData");
        wd4.b(channel, LogBuilder.KEY_CHANNEL);
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "sendCustomCommand invoked: " + "commandData=" + l90.a(bArr, (String) null, 1, (Object) null) + ", " + "channel=" + channel, false, 8, (Object) null);
        Peripheral peripheral = this.g;
        SingleRequestPhase singleRequestPhase = new SingleRequestPhase(peripheral, this.e, PhaseId.SEND_CUSTOM_COMMAND, new p60(peripheral, bArr, channel.getGattCharacteristicId$blesdk_productionRelease()));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) singleRequestPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                singleRequestPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon61(this, singleRequestPhase));
                singleRequestPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon62(this, singleRequestPhase));
                singleRequestPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon63(g90, this, singleRequestPhase));
                singleRequestPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$sendCustomCommand$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, singleRequestPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                singleRequestPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(ComplicationConfig complicationConfig) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(complicationConfig, "config");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setComplication invoked: " + complicationConfig.toJSONString(2) + '}', false, 8, (Object) null);
        z50 z50 = new z50(this.g, this.e, complicationConfig, (String) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) z50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                z50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon65(this, z50));
                z50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon66(this, z50));
                z50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon67(g90, this, z50));
                z50.a((jd4<? super Phase, cb4>) new DeviceImplementation$setComplication$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, z50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                z50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(WatchAppConfig watchAppConfig) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(watchAppConfig, "config");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setWatchApp invoked: " + watchAppConfig.toJSONString(2) + '}', false, 8, (Object) null);
        c60 c60 = new c60(this.g, this.e, watchAppConfig, (String) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) c60);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                c60.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon69(this, c60));
                c60.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon70(this, c60));
                c60.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon71(g90, this, c60));
                c60.a((jd4<? super Phase, cb4>) new DeviceImplementation$setWatchApp$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, c60));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                c60.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(DeviceData deviceData) {
        FileType fileType;
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        DeviceData deviceData2 = deviceData;
        wd4.b(deviceData2, "deviceData");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "sendData invoked: " + deviceData2.toJSONString(2), false, 8, (Object) null);
        if ((deviceData2 instanceof ChanceOfRainComplicationData) || (deviceData2 instanceof WeatherComplicationData) || (deviceData2 instanceof RingPhoneData) || (deviceData2 instanceof WeatherWatchAppData) || (deviceData2 instanceof CommuteTimeWatchAppData) || (deviceData2 instanceof BuddyChallengeWatchAppData) || (deviceData2 instanceof IFTTTWatchAppData)) {
            fileType = FileType.UI_SCRIPT;
        } else if (!(deviceData2 instanceof RingMyPhoneMicroAppData) && !(deviceData2 instanceof CommuteTimeTravelMicroAppData) && !(deviceData2 instanceof CommuteTimeETAMicroAppData) && !(deviceData2 instanceof MicroAppErrorData)) {
            g90<cb4> g902 = new g90<>();
            g902.b(new FeatureError(FeatureErrorCode.INVALID_PARAMETERS, (Phase.Result) null, 2, (rd4) null));
            return g902;
        } else {
            fileType = FileType.MICRO_APP;
        }
        w50 w50 = new w50(this.g, this.e, deviceData, fileType, (String) null, 16, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) w50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                w50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon73(this, w50));
                w50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon74(this, w50));
                w50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon75(g90, this, w50));
                w50.a((jd4<? super Phase, cb4>) new DeviceImplementation$sendData$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, w50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                w50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> a(HandMovingType handMovingType, HandMovingConfig[] handMovingConfigArr) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(handMovingType, "handMovingType");
        wd4.b(handMovingConfigArr, "handMovingConfigs");
        LogLevel logLevel = LogLevel.DEBUG;
        StringBuilder sb = new StringBuilder();
        sb.append("moveHands invoked: movingType: ");
        sb.append(handMovingType);
        sb.append("};");
        sb.append("handConfigs: ");
        String arrays = Arrays.toString(handMovingConfigArr);
        wd4.a((Object) arrays, "java.util.Arrays.toString(this)");
        sb.append(arrays);
        a(this, logLevel, "DeviceImplementation", sb.toString(), false, 8, (Object) null);
        Peripheral peripheral = this.g;
        SingleRequestPhase singleRequestPhase = new SingleRequestPhase(peripheral, this.e, PhaseId.MOVE_HANDS, new r70(peripheral, handMovingType, handMovingConfigArr));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) singleRequestPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                singleRequestPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon81(this, singleRequestPhase));
                singleRequestPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon82(this, singleRequestPhase));
                singleRequestPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon83(g90, this, singleRequestPhase));
                singleRequestPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$moveHands$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, singleRequestPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                singleRequestPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(Alarm[] alarmArr) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        Alarm[] alarmArr2 = alarmArr;
        wd4.b(alarmArr2, "alarms");
        LogLevel logLevel = LogLevel.DEBUG;
        StringBuilder sb = new StringBuilder();
        sb.append("setAlarms invoked: ");
        String arrays = Arrays.toString(alarmArr);
        wd4.a((Object) arrays, "java.util.Arrays.toString(this)");
        sb.append(arrays);
        a(this, logLevel, "DeviceImplementation", sb.toString(), false, 8, (Object) null);
        y50 y50 = new y50(this.g, this.e, alarmArr2, 0, (String) null, 24, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) y50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                y50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon93(this, y50));
                y50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon94(this, y50));
                y50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon95(g90, this, y50));
                y50.a((jd4<? super Phase, cb4>) new DeviceImplementation$setAlarms$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, y50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                y50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(AppNotification appNotification) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        AppNotification appNotification2 = appNotification;
        wd4.b(appNotification2, "appNotification");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "sendAppNotification invoked: " + appNotification2, false, 8, (Object) null);
        v50 v50 = new v50(this.g, this.e, appNotification, 0, (String) null, 24, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) v50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                v50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon101(this, v50));
                v50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon102(this, v50));
                v50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon103(g90, this, v50));
                v50.a((jd4<? super Phase, cb4>) new DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, v50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                v50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(TrackInfo trackInfo) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        TrackInfo trackInfo2 = trackInfo;
        wd4.b(trackInfo2, "trackInfo");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "sendTrackInfo invoked: " + trackInfo2, false, 8, (Object) null);
        x50 x50 = new x50(this.g, this.e, trackInfo, 0, (String) null, 24, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) x50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                x50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon105(this, x50));
                x50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon106(this, x50));
                x50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon107(g90, this, x50));
                x50.a((jd4<? super Phase, cb4>) new DeviceImplementation$sendTrackInfo$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, x50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                x50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<cb4> a(MusicEvent musicEvent) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(musicEvent, "musicEvent");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "musicEvent invoked: " + musicEvent, false, 8, (Object) null);
        m50 m50 = new m50(this.g, this.e, musicEvent);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) m50);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                m50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon109(this, m50));
                m50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon110(this, m50));
                m50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon111(g90, this, m50));
                m50.a((jd4<? super Phase, cb4>) new DeviceImplementation$notifyMusicEvent$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, m50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                m50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(boolean z) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setFrontLight invoked: " + z, false, 8, (Object) null);
        b60 b60 = new b60(this.g, this.e, z, (String) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) b60);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                b60.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon113(this, b60));
                b60.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon114(this, b60));
                b60.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon115(g90, this, b60));
                b60.a((jd4<? super Phase, cb4>) new DeviceImplementation$setFrontLightEnabled$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, b60));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                b60.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(NotificationFilter[] notificationFilterArr) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(notificationFilterArr, "notificationFilters");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setNotificationFilter invoked: " + lb4.a(notificationFilterArr, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, DeviceImplementation$setNotificationFilter$Anon1.INSTANCE, 31, (Object) null), false, 8, (Object) null);
        SetNotificationFilterPhase setNotificationFilterPhase = new SetNotificationFilterPhase(this.g, this.e, notificationFilterArr, (String) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) setNotificationFilterPhase);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                setNotificationFilterPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon117(this, setNotificationFilterPhase));
                setNotificationFilterPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon118(this, setNotificationFilterPhase));
                setNotificationFilterPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon119(g90, this, setNotificationFilterPhase));
                setNotificationFilterPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$setNotificationFilter$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, setNotificationFilterPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                setNotificationFilterPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(BackgroundImageConfig backgroundImageConfig) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(backgroundImageConfig, "backgroundImageConfig");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setBackgroundImage invoked: " + backgroundImageConfig.toJSONString(2) + '.', false, 8, (Object) null);
        SetBackgroundImagePhase setBackgroundImagePhase = new SetBackgroundImagePhase(this.g, this.e, backgroundImageConfig, (String) null, 8, (rd4) null);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) setBackgroundImagePhase);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                setBackgroundImagePhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon133(this, setBackgroundImagePhase));
                setBackgroundImagePhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon134(this, setBackgroundImagePhase));
                setBackgroundImagePhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon135(g90, this, setBackgroundImagePhase));
                setBackgroundImagePhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$setBackgroundImage$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, setBackgroundImagePhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                setBackgroundImagePhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public h90<byte[]> a(byte[] bArr) {
        g90 g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(bArr, "phoneRandomNumber");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "startAuthentication invoked: " + "phoneRandomNumber = " + l90.a(bArr, (String) null, 1, (Object) null), false, 8, (Object) null);
        StartAuthenticationPhase startAuthenticationPhase = new StartAuthenticationPhase(this.g, this.e, AuthenticationKeyType.PRE_SHARED_KEY, bArr);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) startAuthenticationPhase);
            g90 = new g90();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                startAuthenticationPhase.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon145(this, startAuthenticationPhase));
                startAuthenticationPhase.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon146(this, startAuthenticationPhase));
                startAuthenticationPhase.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon147(g90, this, startAuthenticationPhase));
                startAuthenticationPhase.a((jd4<? super Phase, cb4>) new DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, startAuthenticationPhase));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                startAuthenticationPhase.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<String> a(LocalizationFile localizationFile) {
        g90<String> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(localizationFile, "localizationFile");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setLocalizationFile invoked: " + "localizationFile = " + localizationFile.toJSONString(2), false, 8, (Object) null);
        r50 r50 = new r50(this.g, this.e, localizationFile);
        r50.d((jd4<? super Phase, cb4>) new DeviceImplementation$setLocalizationFile$Anon1(this));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) r50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                r50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon157(this, r50));
                r50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon158(this, r50));
                r50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon159(g90, this, r50));
                r50.a((jd4<? super Phase, cb4>) new DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, r50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                r50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(MicroAppMapping[] microAppMappingArr) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(microAppMappingArr, "mappings");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "configureMicroApp invoked: " + "mappings = " + lb4.a(microAppMappingArr, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, DeviceImplementation$configureMicroApp$Anon1.INSTANCE, 31, (Object) null), false, 8, (Object) null);
        c50 c50 = new c50(this.g, this.e, microAppMappingArr);
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) c50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                c50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon165(this, c50));
                c50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon166(this, c50));
                c50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon167(g90, this, c50));
                c50.a((jd4<? super Phase, cb4>) new DeviceImplementation$configureMicroApp$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, c50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                c50.y();
            }
        }
        return g90;
    }

    @DexIgnore
    public g90<cb4> a(WatchParameterFile watchParameterFile) {
        g90<cb4> g90;
        Phase.Result.ResultCode resultCode;
        wd4.b(watchParameterFile, "watchParameterFile");
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "DeviceImplementation", "setWatchParameterFile invoked: " + "watchParameterFile = " + watchParameterFile.toJSONString(2), false, 8, (Object) null);
        s50 s50 = new s50(this.g, this.e, watchParameterFile);
        s50.d((jd4<? super Phase, cb4>) new DeviceImplementation$setWatchParameterFile$Anon1(this));
        synchronized (this.p) {
            FeatureErrorCode a2 = a((Phase) s50);
            g90 = new g90<>();
            if (a2 != null) {
                if (r00.d[a2.ordinal()] != 1) {
                    resultCode = Phase.Result.ResultCode.NOT_ALLOW_TO_START;
                } else {
                    resultCode = Phase.Result.ResultCode.BLUETOOTH_OFF;
                }
                s50.a(resultCode);
                g90.b(new FeatureError(a2, (Phase.Result) null, 2, (rd4) null));
            } else {
                g90.b((jd4<? super Error, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon169(this, s50));
                s50.c((jd4<? super Phase, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon170(this, s50));
                s50.a((kd4<? super Phase, ? super Float, cb4>) new DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon171(g90, this, s50));
                s50.a((jd4<? super Phase, cb4>) new DeviceImplementation$setWatchParameterFile$$inlined$validateAndRunPhase$blesdk_productionRelease$Anon1(g90, this, s50));
                if (getState() != Device.State.CONNECTED && !this.h) {
                    b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(z00.f.a(true))), ab4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)}));
                }
                s50.y();
            }
        }
        return g90;
    }
}
