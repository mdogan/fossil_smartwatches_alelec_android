package com.fossil.blesdk.device;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.y90;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$sync$Anon1 extends Lambda implements jd4<cb4, cb4> {
    @DexIgnore
    public static /* final */ DeviceImplementation$sync$Anon1 INSTANCE; // = new DeviceImplementation$sync$Anon1();

    @DexIgnore
    public DeviceImplementation$sync$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cb4) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(cb4 cb4) {
        wd4.b(cb4, "it");
        y90.a(ea0.l, 0, 1, (Object) null);
    }
}
