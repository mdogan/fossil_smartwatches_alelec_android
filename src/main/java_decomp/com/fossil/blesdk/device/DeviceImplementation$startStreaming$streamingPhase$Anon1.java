package com.fossil.blesdk.device;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$startStreaming$streamingPhase$Anon1 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public static /* final */ DeviceImplementation$startStreaming$streamingPhase$Anon1 INSTANCE; // = new DeviceImplementation$startStreaming$streamingPhase$Anon1();

    @DexIgnore
    public DeviceImplementation$startStreaming$streamingPhase$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "it");
    }
}
