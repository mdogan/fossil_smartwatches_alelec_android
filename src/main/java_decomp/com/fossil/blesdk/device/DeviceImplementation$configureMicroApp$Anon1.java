package com.fossil.blesdk.device;

import com.fossil.blesdk.model.microapp.MicroAppMapping;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$configureMicroApp$Anon1 extends Lambda implements jd4<MicroAppMapping, String> {
    @DexIgnore
    public static /* final */ DeviceImplementation$configureMicroApp$Anon1 INSTANCE; // = new DeviceImplementation$configureMicroApp$Anon1();

    @DexIgnore
    public DeviceImplementation$configureMicroApp$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(MicroAppMapping microAppMapping) {
        wd4.b(microAppMapping, "mapping");
        return microAppMapping.toJSONString(2);
    }
}
