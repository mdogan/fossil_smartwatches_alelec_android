package com.fossil.blesdk.device.core;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$bluetoothGattCallback$Anon1 extends BluetoothGattCallback {
    @DexIgnore
    public /* final */ /* synthetic */ Peripheral a;

    @DexIgnore
    public Peripheral$bluetoothGattCallback$Anon1(Peripheral peripheral) {
        this.a = peripheral;
    }

    @DexIgnore
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        Peripheral peripheral = this.a;
        GattCharacteristic.a aVar = GattCharacteristic.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            wd4.a((Object) uuid, "characteristic!!.uuid");
            GattCharacteristic.CharacteristicId a2 = aVar.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            peripheral.a(a2, value);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        Peripheral peripheral = this.a;
        GattOperationResult.GattResult a2 = GattOperationResult.GattResult.Companion.a(i);
        GattCharacteristic.a aVar = GattCharacteristic.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            wd4.a((Object) uuid, "characteristic!!.uuid");
            GattCharacteristic.CharacteristicId a3 = aVar.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            peripheral.a(a2, a3, value);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        wd4.b(bluetoothGatt, "gatt");
        wd4.b(bluetoothGattCharacteristic, "characteristic");
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        Peripheral peripheral = this.a;
        GattOperationResult.GattResult a2 = GattOperationResult.GattResult.Companion.a(i);
        GattCharacteristic.a aVar = GattCharacteristic.b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        wd4.a((Object) uuid, "characteristic.uuid");
        GattCharacteristic.CharacteristicId a3 = aVar.a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        peripheral.b(a2, a3, value);
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        wd4.b(bluetoothGatt, "gatt");
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        Peripheral peripheral = this.a;
        LogLevel logLevel = LogLevel.DEBUG;
        Peripheral.a(peripheral, logLevel, "Peripheral", "BluetoothGattCallback.onConnectionStateChange: status (" + i3 + "), " + "state (" + i4 + ").", false, 8, (Object) null);
        ea0 ea0 = ea0.l;
        SdkLogEntry sdkLogEntry = r4;
        SdkLogEntry sdkLogEntry2 = new SdkLogEntry("connection_state_changed", EventType.CENTRAL_EVENT, this.a.k(), "", "", i3 == 0, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(xa0.a(new JSONObject(), JSONKey.STATUS, Integer.valueOf(i)), JSONKey.NEW_STATE, this.a.c(i4).getLogName$blesdk_productionRelease()), 448, (rd4) null);
        ea0.b(sdkLogEntry);
        this.a.a(i4, i3);
    }

    @DexIgnore
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        wd4.b(bluetoothGatt, "gatt");
        wd4.b(bluetoothGattDescriptor, "descriptor");
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        Peripheral peripheral = this.a;
        LogLevel logLevel = LogLevel.DEBUG;
        StringBuilder sb = new StringBuilder();
        sb.append("onDescriptorWrite: ");
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        wd4.a((Object) characteristic, "descriptor\n                    .characteristic");
        sb.append(characteristic.getUuid());
        Peripheral.a(peripheral, logLevel, "Peripheral", sb.toString(), false, 8, (Object) null);
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        if (uuid == null || uuid2 == null) {
            Peripheral peripheral2 = this.a;
            LogLevel logLevel2 = LogLevel.DEBUG;
            Peripheral.a(peripheral2, logLevel2, "Peripheral", "onDescriptorWrite: ERROR, " + "characteristicUuid=" + uuid + ", " + "descriptorUuid=" + uuid2 + '.', false, 8, (Object) null);
            return;
        }
        this.a.a(GattOperationResult.GattResult.Companion.a(i), GattCharacteristic.b.a(uuid), GattDescriptor.d.a(uuid2), value);
    }

    @DexIgnore
    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        wd4.b(bluetoothGatt, "gatt");
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.a.m = i - 3;
        }
        this.a.b(GattOperationResult.GattResult.Companion.a(i2), i);
    }

    @DexIgnore
    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        this.a.a(GattOperationResult.GattResult.Companion.a(i2), i);
    }

    @DexIgnore
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        wd4.b(bluetoothGatt, "gatt");
        super.onServicesDiscovered(bluetoothGatt, i);
        List<BluetoothGattService> services = bluetoothGatt.getServices();
        Peripheral peripheral = this.a;
        wd4.a((Object) services, "services");
        peripheral.a((List<? extends BluetoothGattService>) services);
        Peripheral peripheral2 = this.a;
        Peripheral peripheral3 = peripheral2;
        LogLevel logLevel = LogLevel.DEBUG;
        Peripheral.a(peripheral3, logLevel, "onServicesDiscovered", wb4.a(services, "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, Peripheral$bluetoothGattCallback$Anon1$onServicesDiscovered$Anon1.INSTANCE, 30, (Object) null), false, 8, (Object) null);
        Peripheral peripheral4 = this.a;
        GattOperationResult.GattResult a2 = GattOperationResult.GattResult.Companion.a(i);
        ArrayList arrayList = new ArrayList(pb4.a(services, 10));
        for (BluetoothGattService bluetoothGattService : services) {
            wd4.a((Object) bluetoothGattService, "it");
            arrayList.add(bluetoothGattService.getUuid());
        }
        Object[] array = arrayList.toArray(new UUID[0]);
        if (array != null) {
            UUID[] uuidArr = (UUID[]) array;
            Set keySet = this.a.l.keySet();
            wd4.a((Object) keySet, "gattCharacteristicMap.keys");
            Object[] array2 = keySet.toArray(new GattCharacteristic.CharacteristicId[0]);
            if (array2 != null) {
                peripheral4.a(a2, uuidArr, (GattCharacteristic.CharacteristicId[]) array2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
