package com.fossil.blesdk.device.core;

import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$executeBluetoothCommand$Anon1 extends Lambda implements jd4<BluetoothCommand, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothCommand $bluetoothCommand;
    @DexIgnore
    public /* final */ /* synthetic */ Peripheral this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Peripheral$executeBluetoothCommand$Anon1(Peripheral peripheral, BluetoothCommand bluetoothCommand) {
        super(1);
        this.this$Anon0 = peripheral;
        this.$bluetoothCommand = bluetoothCommand;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((BluetoothCommand) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, "it");
        this.this$Anon0.c(this.$bluetoothCommand);
    }
}
