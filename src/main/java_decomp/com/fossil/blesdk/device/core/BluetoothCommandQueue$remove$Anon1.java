package com.fossil.blesdk.device.core;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothCommandQueue$remove$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public static /* final */ BluetoothCommandQueue$remove$Anon1 INSTANCE; // = new BluetoothCommandQueue$remove$Anon1();

    @DexIgnore
    public BluetoothCommandQueue$remove$Anon1() {
        super(0);
    }

    @DexIgnore
    public final void invoke() {
    }
}
