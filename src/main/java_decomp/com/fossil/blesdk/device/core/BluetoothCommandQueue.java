package com.fossil.blesdk.device.core;

import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothCommandQueue {
    @DexIgnore
    public /* final */ LinkedList<BluetoothCommand> a; // = new LinkedList<>();
    @DexIgnore
    public BluetoothCommand b;
    @DexIgnore
    public /* final */ Peripheral c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public BluetoothCommandQueue(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        this.c = peripheral;
    }

    @DexIgnore
    public final void b(BluetoothCommand bluetoothCommand) {
        this.b = bluetoothCommand;
        BluetoothCommand bluetoothCommand2 = this.b;
        if (bluetoothCommand2 != null) {
            bluetoothCommand2.a((id4<cb4>) new BluetoothCommandQueue$execute$Anon1(this));
            if (bluetoothCommand2 != null) {
                bluetoothCommand2.b(this.c);
            }
        }
    }

    @DexIgnore
    public final int c(BluetoothCommand bluetoothCommand) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (bluetoothCommand.d() < this.a.get(i).d()) {
                    return i;
                }
            }
            int size2 = this.a.size();
            return size2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        r8.a.add(r9);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public final void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, "bluetoothCommand");
        Peripheral peripheral = this.c;
        LogLevel logLevel = LogLevel.DEBUG;
        Peripheral.a(peripheral, logLevel, "BluetoothCommandQueue", "enqueue: " + bluetoothCommand.c().getLogName$blesdk_productionRelease(), false, 8, (Object) null);
        synchronized (this.a) {
            this.a.add(c(bluetoothCommand), bluetoothCommand);
            cb4 cb4 = cb4.a;
        }
        if (this.b == null) {
            BluetoothCommand poll = this.a.poll();
            if (poll != null) {
                b(poll);
            }
        }
    }

    @DexIgnore
    public final void a(BluetoothCommand bluetoothCommand, BluetoothCommand.Result.ResultCode resultCode) {
        wd4.b(bluetoothCommand, "bluetoothCommand");
        wd4.b(resultCode, "resultCode");
        if (!wd4.a((Object) bluetoothCommand, (Object) this.b)) {
            bluetoothCommand.a((id4<cb4>) BluetoothCommandQueue$remove$Anon1.INSTANCE);
            this.a.remove(bluetoothCommand);
        }
        bluetoothCommand.a(resultCode);
    }

    @DexIgnore
    public final void a() {
        BluetoothCommand poll = this.a.poll();
        if (poll != null) {
            b(poll);
        } else {
            this.b = null;
        }
    }

    @DexIgnore
    public final void a(BluetoothCommand.Result.ResultCode resultCode) {
        wd4.b(resultCode, "resultCode");
        LinkedList linkedList = new LinkedList(this.a);
        this.a.clear();
        BluetoothCommand bluetoothCommand = this.b;
        if (bluetoothCommand != null) {
            bluetoothCommand.a(resultCode);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            ((BluetoothCommand) it.next()).a(resultCode);
        }
    }
}
