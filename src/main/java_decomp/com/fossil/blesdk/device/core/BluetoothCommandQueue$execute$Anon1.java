package com.fossil.blesdk.device.core;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothCommandQueue$execute$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothCommandQueue this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BluetoothCommandQueue$execute$Anon1(BluetoothCommandQueue bluetoothCommandQueue) {
        super(0);
        this.this$Anon0 = bluetoothCommandQueue;
    }

    @DexIgnore
    public final void invoke() {
        this.this$Anon0.a();
    }
}
