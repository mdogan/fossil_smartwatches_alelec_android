package com.fossil.blesdk.device.core.gatt;

import com.fossil.blesdk.obfuscated.rd4;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum GattCharacteristicProperty {
    PROPERTY_BROADCAST(1),
    PROPERTY_READ(2),
    PROPERTY_WRITE_NO_RESPONSE(4),
    PROPERTY_WRITE(8),
    PROPERTY_NOTIFY(16),
    PROPERTY_INDICATE(32),
    PROPERTY_SIGNED_WRITE(64),
    PROPERTY_EXTENDED_PROPS(128);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ int value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final GattCharacteristicProperty[] a(int i) {
            GattCharacteristicProperty[] values = GattCharacteristicProperty.values();
            ArrayList arrayList = new ArrayList();
            for (GattCharacteristicProperty gattCharacteristicProperty : values) {
                if ((gattCharacteristicProperty.getValue$blesdk_productionRelease() & i) != 0) {
                    arrayList.add(gattCharacteristicProperty);
                }
            }
            Object[] array = arrayList.toArray(new GattCharacteristicProperty[0]);
            if (array != null) {
                return (GattCharacteristicProperty[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((rd4) null);
    }
    */

    @DexIgnore
    GattCharacteristicProperty(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue$blesdk_productionRelease() {
        return this.value;
    }
}
