package com.fossil.blesdk.device.core.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.q10;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattCharacteristic {
    @DexIgnore
    public static /* final */ a b; // = new a((rd4) null);
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic a;

    @DexIgnore
    public enum CharacteristicId {
        UNKNOWN("unknown", r2),
        MODEL_NUMBER("model_number", r2),
        SERIAL_NUMBER(Constants.SERIAL_NUMBER, r2),
        FIRMWARE_VERSION(Constants.FIRMWARE_VERSION, r2),
        SOFTWARE_REVISION("software_revision", r2),
        DC("device_config", r2),
        FTC("file_transfer_control", r2),
        FTD("file_transfer_data", (byte) 0),
        ASYNC("async", r2),
        FTD_1("file_transfer_data_1", (byte) 1),
        AUTHENTICATION("authentication", r2);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ byte socketId;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final CharacteristicId a(byte b) {
                CharacteristicId characteristicId;
                CharacteristicId[] values = CharacteristicId.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        characteristicId = null;
                        break;
                    }
                    characteristicId = values[i];
                    if (characteristicId.getSocketId$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                return characteristicId != null ? characteristicId : CharacteristicId.UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ a(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((rd4) null);
        }
        */

        @DexIgnore
        CharacteristicId(String str, byte b) {
            this.logName = str;
            this.socketId = b;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final ResourceType getResourceType$blesdk_productionRelease() {
            switch (q10.a[ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    return ResourceType.DEVICE_INFORMATION;
                case 5:
                    return ResourceType.DEVICE_CONFIG;
                case 6:
                    return ResourceType.FILE_CONFIG;
                case 7:
                case 8:
                    return ResourceType.TRANSFER_DATA;
                case 9:
                    return ResourceType.AUTHENTICATION;
                case 10:
                    return ResourceType.ASYNC;
                default:
                    return ResourceType.UNKNOWN;
            }
        }

        @DexIgnore
        public final int getResourceTypeQuotaWeight$blesdk_productionRelease() {
            switch (q10.b[ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    return Integer.MAX_VALUE;
                case 8:
                case 9:
                case 10:
                    return 1;
                default:
                    return 0;
            }
        }

        @DexIgnore
        public final byte getSocketId$blesdk_productionRelease() {
            return this.socketId;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CharacteristicId a(UUID uuid) {
            wd4.b(uuid, "uuid");
            if (wd4.a((Object) uuid, (Object) va0.y.j())) {
                return CharacteristicId.MODEL_NUMBER;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.k())) {
                return CharacteristicId.SERIAL_NUMBER;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.i())) {
                return CharacteristicId.FIRMWARE_VERSION;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.l())) {
                return CharacteristicId.SOFTWARE_REVISION;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.q())) {
                return CharacteristicId.DC;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.r())) {
                return CharacteristicId.FTC;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.s())) {
                return CharacteristicId.FTD;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.o())) {
                return CharacteristicId.ASYNC;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.t())) {
                return CharacteristicId.FTD_1;
            }
            if (wd4.a((Object) uuid, (Object) va0.y.p())) {
                return CharacteristicId.AUTHENTICATION;
            }
            return CharacteristicId.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public GattCharacteristic(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        wd4.b(bluetoothGattCharacteristic, "bluetoothGattCharacteristic");
        this.a = bluetoothGattCharacteristic;
        a aVar = b;
        UUID uuid = this.a.getUuid();
        wd4.a((Object) uuid, "this.bluetoothGattCharacteristic.uuid");
        aVar.a(uuid);
    }

    @DexIgnore
    public final BluetoothGattCharacteristic a() {
        return this.a;
    }
}
