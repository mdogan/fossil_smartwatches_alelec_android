package com.fossil.blesdk.device.core.gatt.operation;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Locale;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GattOperationResult {
    @DexIgnore
    public /* final */ GattResult a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class GattResult extends JSONAbleObject {
        @DexIgnore
        public static /* final */ a Companion; // = new a((rd4) null);
        @DexIgnore
        public /* final */ int bluetoothGattStatus;
        @DexIgnore
        public /* final */ ResultCode resultCode;

        @DexIgnore
        public enum ResultCode {
            SUCCESS(0),
            GATT_NULL(1),
            CHARACTERISTIC_NOT_FOUND(2),
            DESCRIPTOR_NOT_FOUND(3),
            START_FAIL(4),
            GATT_ERROR(5),
            BLUETOOTH_OFF(6),
            HID_PROXY_NOT_CONNECTED(256),
            HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
            HID_INPUT_DEVICE_DISABLED(258),
            HID_UNKNOWN_ERROR(511);
            
            @DexIgnore
            public static /* final */ a Companion; // = null;
            @DexIgnore
            public /* final */ int id;
            @DexIgnore
            public /* final */ String logName;

            @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a {
                @DexIgnore
                public a() {
                }

                @DexIgnore
                public final ResultCode a(int i) {
                    if (i == -1) {
                        return ResultCode.BLUETOOTH_OFF;
                    }
                    if (i == 0) {
                        return ResultCode.SUCCESS;
                    }
                    if (i == 511) {
                        return ResultCode.HID_UNKNOWN_ERROR;
                    }
                    switch (i) {
                        case 256:
                            return ResultCode.HID_PROXY_NOT_CONNECTED;
                        case 257:
                            return ResultCode.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
                        case 258:
                            return ResultCode.HID_INPUT_DEVICE_DISABLED;
                        default:
                            return ResultCode.GATT_ERROR;
                    }
                }

                @DexIgnore
                public /* synthetic */ a(rd4 rd4) {
                    this();
                }
            }

            /*
            static {
                Companion = new a((rd4) null);
            }
            */

            @DexIgnore
            ResultCode(int i) {
                this.id = i;
                String name = name();
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    this.logName = lowerCase;
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            @DexIgnore
            public final int getId() {
                return this.id;
            }

            @DexIgnore
            public final String getLogName$blesdk_productionRelease() {
                return this.logName;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final GattResult a(int i) {
                return new GattResult(ResultCode.Companion.a(i), i);
            }

            @DexIgnore
            public /* synthetic */ a(rd4 rd4) {
                this();
            }
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ GattResult(ResultCode resultCode2, int i, int i2, rd4 rd4) {
            this(resultCode2, (i2 & 2) != 0 ? 0 : i);
        }

        @DexIgnore
        public final int getBluetoothGattStatus() {
            return this.bluetoothGattStatus;
        }

        @DexIgnore
        public final ResultCode getResultCode() {
            return this.resultCode;
        }

        @DexIgnore
        public JSONObject toJSONObject() {
            JSONObject jSONObject = new JSONObject();
            try {
                xa0.a(jSONObject, JSONKey.RESULT_CODE, this.resultCode.getLogName$blesdk_productionRelease());
                if (this.bluetoothGattStatus != 0) {
                    xa0.a(jSONObject, JSONKey.GATT_STATUS, Integer.valueOf(this.bluetoothGattStatus));
                }
            } catch (JSONException e) {
                ea0.l.a(e);
            }
            return jSONObject;
        }

        @DexIgnore
        public GattResult(ResultCode resultCode2, int i) {
            wd4.b(resultCode2, "resultCode");
            this.resultCode = resultCode2;
            this.bluetoothGattStatus = i;
        }
    }

    @DexIgnore
    public GattOperationResult(GattResult gattResult) {
        wd4.b(gattResult, "gattResult");
        this.a = gattResult;
    }

    @DexIgnore
    public final GattResult a() {
        return this.a;
    }
}
