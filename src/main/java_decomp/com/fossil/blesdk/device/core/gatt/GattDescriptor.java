package com.fossil.blesdk.device.core.gatt;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.s10;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wd4;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattDescriptor {
    @DexIgnore
    public static /* final */ byte[] a; // = {1, 0};
    @DexIgnore
    public static /* final */ byte[] b; // = {2, 0};
    @DexIgnore
    public static /* final */ byte[] c; // = {0, 0};
    @DexIgnore
    public static /* final */ a d; // = new a((rd4) null);

    @DexIgnore
    public enum DescriptorId {
        CLIENT_CHARACTERISTIC_CONFIGURATION,
        UNKNOWN;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final UUID getUuid() {
            if (s10.a[ordinal()] != 1) {
                return null;
            }
            return va0.y.a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DescriptorId a(UUID uuid) {
            wd4.b(uuid, "uuid");
            if (wd4.a((Object) uuid, (Object) va0.y.a())) {
                return DescriptorId.CLIENT_CHARACTERISTIC_CONFIGURATION;
            }
            return DescriptorId.UNKNOWN;
        }

        @DexIgnore
        public final byte[] b() {
            return GattDescriptor.b;
        }

        @DexIgnore
        public final byte[] c() {
            return GattDescriptor.a;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final byte[] a() {
            return GattDescriptor.c;
        }
    }
}
