package com.fossil.blesdk.device;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.logic.phase.OtaPhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$ota$Anon2 extends Lambda implements jd4<Phase, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$ota$Anon2(DeviceImplementation deviceImplementation) {
        super(1);
        this.this$Anon0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        wd4.b(phase, "phase");
        this.this$Anon0.q = ((OtaPhase) phase).O();
        this.this$Anon0.a(Device.State.CONNECTED);
    }
}
