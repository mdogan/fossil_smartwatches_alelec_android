package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qw0 extends ww0 {
    @DexIgnore
    public /* final */ /* synthetic */ nw0 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qw0(nw0 nw0) {
        super(nw0, (ow0) null);
        this.f = nw0;
    }

    @DexIgnore
    public /* synthetic */ qw0(nw0 nw0, ow0 ow0) {
        this(nw0);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new pw0(this.f, (ow0) null);
    }
}
