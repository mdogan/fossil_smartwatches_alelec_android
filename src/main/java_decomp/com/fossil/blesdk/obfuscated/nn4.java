package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.km4;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.Response;
import okio.ByteString;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn4 {
    /*
    static {
        ByteString.encodeUtf8("\"\\");
        ByteString.encodeUtf8("\t ,=");
    }
    */

    @DexIgnore
    public static long a(Response response) {
        return a(response.D());
    }

    @DexIgnore
    public static boolean b(km4 km4) {
        return c(km4).contains(Marker.ANY_MARKER);
    }

    @DexIgnore
    public static boolean c(Response response) {
        return b(response.D());
    }

    @DexIgnore
    public static Set<String> d(Response response) {
        return c(response.D());
    }

    @DexIgnore
    public static km4 e(Response response) {
        return a(response.G().L().c(), response.D());
    }

    @DexIgnore
    public static long a(km4 km4) {
        return a(km4.a("Content-Length"));
    }

    @DexIgnore
    public static boolean b(Response response) {
        if (response.L().e().equals("HEAD")) {
            return false;
        }
        int B = response.B();
        if (((B >= 100 && B < 200) || B == 204 || B == 304) && a(response) == -1 && !"chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static Set<String> c(km4 km4) {
        Set<String> emptySet = Collections.emptySet();
        int b = km4.b();
        Set<String> set = emptySet;
        for (int i = 0; i < b; i++) {
            if ("Vary".equalsIgnoreCase(km4.a(i))) {
                String b2 = km4.b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b2.split(",")) {
                    set.add(trim.trim());
                }
            }
        }
        return set;
    }

    @DexIgnore
    public static long a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static boolean a(Response response, km4 km4, pm4 pm4) {
        for (String next : d(response)) {
            if (!vm4.a((Object) km4.b(next), (Object) pm4.b(next))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static km4 a(km4 km4, km4 km42) {
        Set<String> c = c(km42);
        if (c.isEmpty()) {
            return new km4.a().a();
        }
        km4.a aVar = new km4.a();
        int b = km4.b();
        for (int i = 0; i < b; i++) {
            String a = km4.a(i);
            if (c.contains(a)) {
                aVar.a(a, km4.b(i));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static int b(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt != ' ' && charAt != 9) {
                break;
            }
            i++;
        }
        return i;
    }

    @DexIgnore
    public static void a(dm4 dm4, lm4 lm4, km4 km4) {
        if (dm4 != dm4.a) {
            List<cm4> a = cm4.a(lm4, km4);
            if (!a.isEmpty()) {
                dm4.a(lm4, a);
            }
        }
    }

    @DexIgnore
    public static int a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static int a(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
