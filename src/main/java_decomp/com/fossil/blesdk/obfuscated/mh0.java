package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mh0 implements qn1<Boolean, Void> {
    @DexIgnore
    public final /* synthetic */ Object then(xn1 xn1) throws Exception {
        if (((Boolean) xn1.b()).booleanValue()) {
            return null;
        }
        throw new ApiException(new Status(13, "listener already unregistered"));
    }
}
