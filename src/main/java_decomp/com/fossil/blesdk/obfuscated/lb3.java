package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lb3 {
    @DexIgnore
    public /* final */ gb3 a;
    @DexIgnore
    public /* final */ vb3 b;
    @DexIgnore
    public /* final */ qb3 c;

    @DexIgnore
    public lb3(gb3 gb3, vb3 vb3, qb3 qb3) {
        wd4.b(gb3, "mGoalTrackingOverviewDayView");
        wd4.b(vb3, "mGoalTrackingOverviewWeekView");
        wd4.b(qb3, "mGoalTrackingOverviewMonthView");
        this.a = gb3;
        this.b = vb3;
        this.c = qb3;
    }

    @DexIgnore
    public final gb3 a() {
        return this.a;
    }

    @DexIgnore
    public final qb3 b() {
        return this.c;
    }

    @DexIgnore
    public final vb3 c() {
        return this.b;
    }
}
