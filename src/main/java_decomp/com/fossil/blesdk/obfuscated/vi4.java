package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class vi4 {
    @DexIgnore
    public static final yg4 a(ri4 ri4) {
        return new ti4(ri4);
    }

    @DexIgnore
    public static final ai4 a(ri4 ri4, ai4 ai4) {
        wd4.b(ri4, "$this$disposeOnCompletion");
        wd4.b(ai4, "handle");
        return ri4.a((jd4<? super Throwable, cb4>) new ci4(ri4, ai4));
    }

    @DexIgnore
    public static /* synthetic */ void a(CoroutineContext coroutineContext, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        ui4.a(coroutineContext, cancellationException);
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, CancellationException cancellationException) {
        wd4.b(coroutineContext, "$this$cancel");
        ri4 ri4 = (ri4) coroutineContext.get(ri4.d);
        if (ri4 != null) {
            ri4.a(cancellationException);
        }
    }
}
