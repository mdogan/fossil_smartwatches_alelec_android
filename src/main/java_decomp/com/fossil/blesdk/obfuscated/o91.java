package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o91 implements pa1 {
    @DexIgnore
    public static /* final */ w91 b; // = new p91();
    @DexIgnore
    public /* final */ w91 a;

    @DexIgnore
    public o91() {
        this(new q91(t81.a(), a()));
    }

    @DexIgnore
    public final <T> oa1<T> a(Class<T> cls) {
        Class<u81> cls2 = u81.class;
        qa1.a((Class<?>) cls);
        v91 a2 = this.a.a(cls);
        if (a2.b()) {
            if (cls2.isAssignableFrom(cls)) {
                return da1.a(qa1.c(), m81.b(), a2.a());
            }
            return da1.a(qa1.a(), m81.c(), a2.a());
        } else if (cls2.isAssignableFrom(cls)) {
            if (a(a2)) {
                return ba1.a(cls, a2, ha1.b(), j91.b(), qa1.c(), m81.b(), u91.b());
            }
            return ba1.a(cls, a2, ha1.b(), j91.b(), qa1.c(), (k81<?>) null, u91.b());
        } else if (a(a2)) {
            return ba1.a(cls, a2, ha1.a(), j91.a(), qa1.a(), m81.c(), u91.a());
        } else {
            return ba1.a(cls, a2, ha1.a(), j91.a(), qa1.b(), (k81<?>) null, u91.a());
        }
    }

    @DexIgnore
    public o91(w91 w91) {
        w81.a(w91, "messageInfoFactory");
        this.a = w91;
    }

    @DexIgnore
    public static boolean a(v91 v91) {
        return v91.c() == u81.e.i;
    }

    @DexIgnore
    public static w91 a() {
        try {
            return (w91) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }
}
