package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vt2 extends ut2<c> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public /* final */ ArrayList<String> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> m; // = new ArrayList<>();
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public b p;
    @DexIgnore
    public AlphabetIndexer q;
    @DexIgnore
    public /* final */ List<ContactWrapper> r;
    @DexIgnore
    public /* final */ int s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(ContactWrapper contactWrapper);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ rh2 h;
        @DexIgnore
        public /* final */ /* synthetic */ vt2 i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.e.b();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vt2 vt2, rh2 rh2) {
            super(rh2.d());
            wd4.b(rh2, "binding");
            this.i = vt2;
            this.h = rh2;
            this.h.q.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void b() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.r.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((ContactWrapper) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 == -1) {
                    this.i.r.add(a());
                    this.i.notifyItemChanged(adapterPosition);
                } else if (((ContactWrapper) this.i.r.get(i2)).getCurrentHandGroup() != this.i.s) {
                    b d2 = this.i.p;
                    if (d2 != null) {
                        d2.a((ContactWrapper) this.i.r.get(i2));
                    }
                } else {
                    this.i.r.remove(i2);
                    this.i.notifyItemChanged(adapterPosition);
                }
            }
        }

        @DexIgnore
        public final void a(Cursor cursor, int i2) {
            boolean z;
            Object obj;
            boolean z2;
            wd4.b(cursor, "cursor");
            cursor.moveToPosition(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c2 = vt2.t;
            local.d(c2, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            String b2 = us3.b(this.c);
            if (this.i.m.contains(Integer.valueOf(i2)) || (this.i.o < i2 && this.i.n == this.e && this.i.l.contains(b2))) {
                if (!this.i.m.contains(Integer.valueOf(i2))) {
                    this.i.m.add(Integer.valueOf(i2));
                }
                a(8);
                z = false;
            } else {
                if (i2 > this.i.o) {
                    this.i.o = i2;
                }
                if (this.i.n != this.e) {
                    this.i.l.clear();
                    this.i.n = this.e;
                }
                this.i.l.add(b2);
                a(0);
                if (!cursor.moveToPrevious() || cursor.getInt(cursor.getColumnIndex("contact_id")) != this.e) {
                    z = false;
                } else {
                    AppCompatCheckBox appCompatCheckBox = this.h.q;
                    wd4.a((Object) appCompatCheckBox, "binding.accbSelect");
                    appCompatCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    wd4.a((Object) flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                    z = true;
                }
                cursor.moveToNext();
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            wd4.a((Object) flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                wd4.a((Object) flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
            }
            Iterator it = this.i.r.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                Contact contact = ((ContactWrapper) obj).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            ContactWrapper contactWrapper = (ContactWrapper) obj;
            if (contactWrapper == null) {
                AppCompatCheckBox appCompatCheckBox2 = this.h.q;
                wd4.a((Object) appCompatCheckBox2, "binding.accbSelect");
                appCompatCheckBox2.setChecked(false);
            } else if (contactWrapper.getCurrentHandGroup() == 0 || contactWrapper.getCurrentHandGroup() == this.i.s) {
                AppCompatCheckBox appCompatCheckBox3 = this.h.q;
                wd4.a((Object) appCompatCheckBox3, "binding.accbSelect");
                appCompatCheckBox3.setChecked(true);
            } else {
                FlexibleTextView flexibleTextView4 = this.h.v;
                wd4.a((Object) flexibleTextView4, "binding.pickContactPhone");
                be4 be4 = be4.a;
                String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                Object[] objArr = {Integer.valueOf(contactWrapper.getCurrentHandGroup())};
                String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                AppCompatCheckBox appCompatCheckBox4 = this.h.q;
                wd4.a((Object) appCompatCheckBox4, "binding.accbSelect");
                appCompatCheckBox4.setChecked(false);
                if (z) {
                    a(8);
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String c3 = vt2.t;
            local2.d(c3, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ',' + " phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView5 = this.h.t;
                wd4.a((Object) flexibleTextView5, "binding.ftvAlphabet");
                flexibleTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView6 = this.h.t;
                wd4.a((Object) flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setText(Character.toString(ml2.b.a(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView7 = this.h.t;
            wd4.a((Object) flexibleTextView7, "binding.ftvAlphabet");
            flexibleTextView7.setVisibility(8);
        }

        @DexIgnore
        public final void a(int i2) {
            AppCompatCheckBox appCompatCheckBox = this.h.q;
            wd4.a((Object) appCompatCheckBox, "binding.accbSelect");
            appCompatCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            wd4.a((Object) constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            wd4.a((Object) flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.u;
            wd4.a((Object) constraintLayout2, "binding.llTextContainer");
            constraintLayout2.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            wd4.a((Object) flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            wd4.a((Object) flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            wd4.a((Object) view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout3 = this.h.s;
            wd4.a((Object) constraintLayout3, "binding.clRoot");
            constraintLayout3.setVisibility(i2);
        }

        @DexIgnore
        public final ContactWrapper a() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            ContactWrapper contactWrapper = new ContactWrapper(contact, (String) null, 2, (rd4) null);
            if (this.f == 1) {
                contactWrapper.setHasPhoneNumber(true);
                contactWrapper.setPhoneNumber(this.c);
            } else {
                contactWrapper.setHasPhoneNumber(false);
            }
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
            }
            Contact contact3 = contactWrapper.getContact();
            if (contact3 != null) {
                contact3.setUseCall(true);
            }
            contactWrapper.setFavorites(this.g);
            contactWrapper.setAdded(true);
            contactWrapper.setCurrentHandGroup(this.i.s);
            return contactWrapper;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = vt2.class.getSimpleName();
        wd4.a((Object) simpleName, "HybridCursorContactSearc\u2026er::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vt2(Cursor cursor, List<ContactWrapper> list, int i) {
        super(cursor);
        wd4.b(list, "mContactWrapperList");
        this.r = list;
        this.s = i;
    }

    @DexIgnore
    public int getPositionForSection(int i) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i);
            }
            wd4.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i);
            }
            wd4.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        rh2 a2 = rh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wd4.a((Object) a2, "ItemContactHybridBinding\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        this.q = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.l.clear();
            this.m.clear();
            this.n = -1;
            this.o = -1;
            return super.c(cursor);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(c cVar, Cursor cursor, int i) {
        wd4.b(cVar, "holder");
        if (cursor != null) {
            cVar.a(cursor, i);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "itemClickListener");
        this.p = bVar;
    }
}
