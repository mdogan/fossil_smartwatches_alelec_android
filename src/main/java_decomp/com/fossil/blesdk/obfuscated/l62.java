package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l62 {
    @DexIgnore
    <R extends j62.c, E extends j62.a> void a(E e, j62.d<R, E> dVar);

    @DexIgnore
    <R extends j62.c, E extends j62.a> void a(R r, j62.d<R, E> dVar);

    @DexIgnore
    void execute(Runnable runnable);
}
