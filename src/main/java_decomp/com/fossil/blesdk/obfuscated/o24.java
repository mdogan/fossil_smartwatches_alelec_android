package com.fossil.blesdk.obfuscated;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o24 {
    @DexIgnore
    public int a;
    @DexIgnore
    public JSONObject b; // = new JSONObject();
    @DexIgnore
    public String c;
    @DexIgnore
    public int d; // = 0;

    @DexIgnore
    public o24(int i) {
        this.a = i;
    }

    @DexIgnore
    public String a() {
        return this.b.toString();
    }
}
