package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uc0 extends as0 implements tc0 {
    @DexIgnore
    public uc0() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                a((GoogleSignInAccount) bs0.a(parcel, GoogleSignInAccount.CREATOR), (Status) bs0.a(parcel, Status.CREATOR));
                throw null;
            case 102:
                a((Status) bs0.a(parcel, Status.CREATOR));
                break;
            case 103:
                b((Status) bs0.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
