package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aj3 extends as2 implements zi3 {
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public ur3<ne2> j;
    @DexIgnore
    public yi3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final aj3 a() {
            return new aj3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ aj3 e;

        @DexIgnore
        public b(aj3 aj3) {
            this.e = aj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ aj3 e;
        @DexIgnore
        public /* final */ /* synthetic */ ne2 f;

        @DexIgnore
        public c(aj3 aj3, ne2 ne2) {
            this.e = aj3;
            this.f = ne2;
        }

        @DexIgnore
        public final void onClick(View view) {
            aj3 aj3 = this.e;
            SwitchCompat switchCompat = this.f.q;
            wd4.a((Object) switchCompat, "binding.anonymousSwitch");
            aj3.c("Usage_Data", switchCompat.isChecked());
            AnalyticsHelper a = this.e.P0();
            SwitchCompat switchCompat2 = this.f.q;
            wd4.a((Object) switchCompat2, "binding.anonymousSwitch");
            a.c(switchCompat2.isChecked());
            yi3 b = aj3.b(this.e);
            SwitchCompat switchCompat3 = this.f.q;
            wd4.a((Object) switchCompat3, "binding.anonymousSwitch");
            b.a(switchCompat3.isChecked());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ aj3 e;
        @DexIgnore
        public /* final */ /* synthetic */ ne2 f;

        @DexIgnore
        public d(aj3 aj3, ne2 ne2) {
            this.e = aj3;
            this.f = ne2;
        }

        @DexIgnore
        public final void onClick(View view) {
            aj3 aj3 = this.e;
            SwitchCompat switchCompat = this.f.t;
            wd4.a((Object) switchCompat, "binding.scSubcribeEmail");
            aj3.c("Emails", switchCompat.isChecked());
            AnalyticsHelper a = this.e.P0();
            SwitchCompat switchCompat2 = this.f.t;
            wd4.a((Object) switchCompat2, "binding.scSubcribeEmail");
            a.b(switchCompat2.isChecked());
            yi3 b = aj3.b(this.e);
            SwitchCompat switchCompat3 = this.f.t;
            wd4.a((Object) switchCompat3, "binding.scSubcribeEmail");
            b.b(switchCompat3.isChecked());
        }
    }

    /*
    static {
        wd4.a((Object) aj3.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ yi3 b(aj3 aj3) {
        yi3 yi3 = aj3.k;
        if (yi3 != null) {
            return yi3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void E(boolean z) {
        ur3<ne2> ur3 = this.j;
        if (ur3 != null) {
            ne2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.q;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void E0() {
        a();
    }

    @DexIgnore
    public void H(boolean z) {
        ur3<ne2> ur3 = this.j;
        if (ur3 != null) {
            ne2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.t;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void Z() {
        String string = getString(R.string.Onboarding_Login_LoggingIn_Text__PleaseWait);
        wd4.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public final void c(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        a("profile_optin", (Map<String, String>) hashMap);
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ne2 ne2 = (ne2) ra.a(LayoutInflater.from(getContext()), (int) R.layout.fragment_opt_in, (ViewGroup) null, false);
        ne2.s.setOnClickListener(new b(this));
        FlexibleTextView flexibleTextView = ne2.r;
        wd4.a((Object) flexibleTextView, "binding.ftvDescriptionSubcribeEmail");
        be4 be4 = be4.a;
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Select_Optin_List__GetTipsAboutBrandsTools);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        Object[] objArr = {PortfolioApp.W.c().i()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        ne2.q.setOnClickListener(new c(this, ne2));
        ne2.t.setOnClickListener(new d(this, ne2));
        this.j = new ur3<>(this, ne2);
        wd4.a((Object) ne2, "binding");
        return ne2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        yi3 yi3 = this.k;
        if (yi3 != null) {
            yi3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        yi3 yi3 = this.k;
        if (yi3 != null) {
            yi3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, String str) {
        wd4.b(str, "message");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(yi3 yi3) {
        wd4.b(yi3, "presenter");
        tt1.a(yi3);
        wd4.a((Object) yi3, "checkNotNull(presenter)");
        this.k = yi3;
    }
}
