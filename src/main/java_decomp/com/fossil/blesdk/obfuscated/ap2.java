package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.portfolio.platform.data.model.ServerError;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ap2<T> implements qr4<T> {
    @DexIgnore
    public abstract void a(Call<T> call, cs4<T> cs4);

    @DexIgnore
    public abstract void a(Call<T> call, ServerError serverError);

    @DexIgnore
    public abstract void a(Call<T> call, Throwable th);

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        wd4.b(call, "call");
        wd4.b(th, "t");
        a(call, th);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        if (r0 != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0090, code lost:
        if (r0 != null) goto L_0x0097;
     */
    @DexIgnore
    public void onResponse(Call<T> call, cs4<T> cs4) {
        String str;
        String str2;
        String str3;
        wd4.b(call, "call");
        wd4.b(cs4, "response");
        if (cs4.d()) {
            a(call, cs4);
            return;
        }
        int b = cs4.b();
        if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            qm4 c = cs4.c();
            if (c != null) {
                str = c.F();
            }
            str = cs4.e();
            serverError.setMessage(str);
            a(call, serverError);
            return;
        }
        try {
            Gson gson = new Gson();
            qm4 c2 = cs4.c();
            if (c2 != null) {
                str3 = c2.F();
                if (str3 != null) {
                    ServerError serverError2 = (ServerError) gson.a(str3, ServerError.class);
                    wd4.a((Object) serverError2, "serverError");
                    a(call, serverError2);
                }
            }
            str3 = cs4.e();
            ServerError serverError22 = (ServerError) gson.a(str3, ServerError.class);
            wd4.a((Object) serverError22, "serverError");
            a(call, serverError22);
        } catch (Exception unused) {
            ServerError serverError3 = new ServerError();
            serverError3.setCode(Integer.valueOf(b));
            qm4 c3 = cs4.c();
            if (c3 != null) {
                str2 = c3.F();
            }
            str2 = cs4.e();
            serverError3.setMessage(str2);
            a(call, serverError3);
        }
    }
}
