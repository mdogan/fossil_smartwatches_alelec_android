package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vd2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ LinearLayout v;
    @DexIgnore
    public /* final */ LinearLayout w;
    @DexIgnore
    public /* final */ RecyclerView x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vd2(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, ImageView imageView3, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView4, ImageView imageView5, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, RecyclerView recyclerView, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView3;
        this.s = imageView4;
        this.t = imageView5;
        this.u = linearLayout;
        this.v = linearLayout2;
        this.w = linearLayout3;
        this.x = recyclerView;
    }
}
