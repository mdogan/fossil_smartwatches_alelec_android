package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.widget.Filter;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tt2 extends Filter {
    @DexIgnore
    public a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        Cursor a();

        @DexIgnore
        Cursor a(CharSequence charSequence);

        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        CharSequence b(Cursor cursor);
    }

    @DexIgnore
    public tt2(a aVar) {
        wd4.b(aVar, "mClient");
        this.a = aVar;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        wd4.b(obj, "resultValue");
        return this.a.b((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        wd4.b(charSequence, "constraint");
        Cursor a2 = this.a.a(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (a2 != null) {
            filterResults.count = a2.getCount();
            filterResults.values = a2;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        wd4.b(charSequence, "constraint");
        wd4.b(filterResults, "results");
        Cursor a2 = this.a.a();
        Object obj = filterResults.values;
        if (obj != null && (!wd4.a(obj, (Object) a2))) {
            a aVar = this.a;
            Object obj2 = filterResults.values;
            if (obj2 != null) {
                aVar.a((Cursor) obj2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.database.Cursor");
        }
    }
}
