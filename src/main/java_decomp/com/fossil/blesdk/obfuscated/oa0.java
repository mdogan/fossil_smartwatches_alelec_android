package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oa0 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((rd4) null);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<oa0> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public oa0 createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new oa0(parcel, (rd4) null);
        }

        @DexIgnore
        public oa0[] newArray(int i) {
            return new oa0[i];
        }
    }

    @DexIgnore
    public /* synthetic */ oa0(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public final String a() {
        return this.f;
    }

    @DexIgnore
    public final String b() {
        return this.g;
    }

    @DexIgnore
    public final String c() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
        if (parcel != null) {
            parcel.writeString(this.g);
        }
    }

    @DexIgnore
    public oa0(String str, String str2, String str3) {
        wd4.b(str, "url");
        wd4.b(str2, "accessKey");
        wd4.b(str3, "secretKey");
        this.e = str;
        this.f = str2;
        this.g = str3;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public oa0(Parcel parcel) {
        this(r0, r2, r4);
        String readString = parcel.readString();
        if (readString != null) {
            String readString2 = parcel.readString();
            if (readString2 != null) {
                String readString3 = parcel.readString();
                if (readString3 != null) {
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
