package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.PinObject;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ao2 extends BaseProvider {
    @DexIgnore
    void a(PinObject pinObject);

    @DexIgnore
    List<PinObject> b(String str);
}
