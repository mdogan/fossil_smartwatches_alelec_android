package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class je0 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends ne0> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public a(he0 he0, R r) {
            super(he0);
            this.q = r;
        }

        @DexIgnore
        public final R a(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static ie0<Status> a(Status status, he0 he0) {
        ck0.a(status, (Object) "Result must not be null");
        ff0 ff0 = new ff0(he0);
        ff0.a(status);
        return ff0;
    }

    @DexIgnore
    public static <R extends ne0> ie0<R> a(R r, he0 he0) {
        ck0.a(r, (Object) "Result must not be null");
        ck0.a(!r.G().L(), (Object) "Status code must not be SUCCESS");
        a aVar = new a(he0, r);
        aVar.a(r);
        return aVar;
    }
}
