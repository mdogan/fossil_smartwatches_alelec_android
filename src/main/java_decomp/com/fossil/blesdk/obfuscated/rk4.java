package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class rk4 {
    @DexIgnore
    public static /* final */ int a; // = Runtime.getRuntime().availableProcessors();

    @DexIgnore
    public static final int a() {
        return a;
    }

    @DexIgnore
    public static final String a(String str) {
        wd4.b(str, "propertyName");
        try {
            return System.getProperty(str);
        } catch (SecurityException unused) {
            return null;
        }
    }
}
