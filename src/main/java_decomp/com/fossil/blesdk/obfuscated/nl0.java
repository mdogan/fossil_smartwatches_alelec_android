package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nl0 implements Parcelable.Creator<fk0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        Scope[] scopeArr = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                i3 = SafeParcelReader.q(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                scopeArr = (Scope[]) SafeParcelReader.b(parcel, a, Scope.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new fk0(i, i2, i3, scopeArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new fk0[i];
    }
}
