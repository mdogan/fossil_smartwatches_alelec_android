package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.qj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gg0 implements qj0.a {
    @DexIgnore
    public /* final */ /* synthetic */ fg0 a;

    @DexIgnore
    public gg0(fg0 fg0) {
        this.a = fg0;
    }

    @DexIgnore
    public final boolean c() {
        return this.a.g();
    }

    @DexIgnore
    public final Bundle n() {
        return null;
    }
}
