package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.pm4;
import com.misfit.frameworks.common.enums.Action;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ConnectionShutdownException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sn4 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public volatile in4 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public sn4(OkHttpClient okHttpClient, boolean z) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public void a() {
        this.d = true;
        in4 in4 = this.b;
        if (in4 != null) {
            in4.a();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        pm4 n = chain.n();
        pn4 pn4 = (pn4) chain;
        vl4 e = pn4.e();
        hm4 f = pn4.f();
        in4 in4 = new in4(this.a.e(), a(n.g()), e, f, this.c);
        this.b = in4;
        Response response = null;
        int i = 0;
        while (!this.d) {
            try {
                Response a2 = pn4.a(n, in4, (ln4) null, (fn4) null);
                if (response != null) {
                    Response.a H = a2.H();
                    Response.a H2 = response.H();
                    H2.a((qm4) null);
                    H.d(H2.a());
                    a2 = H.a();
                }
                try {
                    pm4 a3 = a(a2, in4.h());
                    if (a3 == null) {
                        in4.f();
                        return a2;
                    }
                    vm4.a((Closeable) a2.y());
                    int i2 = i + 1;
                    if (i2 <= 20) {
                        a3.a();
                        if (!a(a2, a3.g())) {
                            in4.f();
                            in4 = new in4(this.a.e(), a(a3.g()), e, f, this.c);
                            this.b = in4;
                        } else if (in4.b() != null) {
                            throw new IllegalStateException("Closing the body of " + a2 + " didn't close its backing stream. Bad interceptor?");
                        }
                        response = a2;
                        n = a3;
                        i = i2;
                    } else {
                        in4.f();
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                } catch (IOException e2) {
                    in4.f();
                    throw e2;
                }
            } catch (RouteException e3) {
                if (!a(e3.getLastConnectException(), in4, false, n)) {
                    throw e3.getFirstConnectException();
                }
            } catch (IOException e4) {
                if (!a(e4, in4, !(e4 instanceof ConnectionShutdownException), n)) {
                    throw e4;
                }
            } catch (Throwable th) {
                in4.a((IOException) null);
                in4.f();
                throw th;
            }
        }
        in4.f();
        throw new IOException("Canceled");
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj;
    }

    @DexIgnore
    public final sl4 a(lm4 lm4) {
        xl4 xl4;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (lm4.h()) {
            SSLSocketFactory H = this.a.H();
            hostnameVerifier = this.a.m();
            sSLSocketFactory = H;
            xl4 = this.a.c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            xl4 = null;
        }
        return new sl4(lm4.g(), lm4.k(), this.a.i(), this.a.G(), sSLSocketFactory, hostnameVerifier, xl4, this.a.C(), this.a.B(), this.a.A(), this.a.f(), this.a.D());
    }

    @DexIgnore
    public final boolean a(IOException iOException, in4 in4, boolean z, pm4 pm4) {
        in4.a(iOException);
        if (!this.a.F()) {
            return false;
        }
        if (z) {
            pm4.a();
        }
        if (a(iOException, z) && in4.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(IOException iOException, boolean z) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                return false;
            }
            return true;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final pm4 a(Response response, rm4 rm4) throws IOException {
        Proxy proxy;
        if (response != null) {
            int B = response.B();
            String e = response.L().e();
            RequestBody requestBody = null;
            if (B == 307 || B == 308) {
                if (!e.equals("GET") && !e.equals("HEAD")) {
                    return null;
                }
            } else if (B == 401) {
                return this.a.a().authenticate(rm4, response);
            } else {
                if (B != 503) {
                    if (B == 407) {
                        if (rm4 != null) {
                            proxy = rm4.b();
                        } else {
                            proxy = this.a.B();
                        }
                        if (proxy.type() == Proxy.Type.HTTP) {
                            return this.a.C().authenticate(rm4, response);
                        }
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    } else if (B != 408) {
                        switch (B) {
                            case 300:
                            case Action.Presenter.NEXT /*301*/:
                            case Action.Presenter.PREVIOUS /*302*/:
                            case Action.Presenter.BLACKOUT /*303*/:
                                break;
                            default:
                                return null;
                        }
                    } else if (!this.a.F()) {
                        return null;
                    } else {
                        response.L().a();
                        if ((response.I() == null || response.I().B() != 408) && a(response, 0) <= 0) {
                            return response.L();
                        }
                        return null;
                    }
                } else if ((response.I() == null || response.I().B() != 503) && a(response, Integer.MAX_VALUE) == 0) {
                    return response.L();
                } else {
                    return null;
                }
            }
            if (!this.a.k()) {
                return null;
            }
            String e2 = response.e("Location");
            if (e2 == null) {
                return null;
            }
            lm4 b2 = response.L().g().b(e2);
            if (b2 == null) {
                return null;
            }
            if (!b2.n().equals(response.L().g().n()) && !this.a.l()) {
                return null;
            }
            pm4.a f = response.L().f();
            if (on4.b(e)) {
                boolean d2 = on4.d(e);
                if (on4.c(e)) {
                    f.a("GET", (RequestBody) null);
                } else {
                    if (d2) {
                        requestBody = response.L().a();
                    }
                    f.a(e, requestBody);
                }
                if (!d2) {
                    f.a("Transfer-Encoding");
                    f.a("Content-Length");
                    f.a(GraphRequest.CONTENT_TYPE_HEADER);
                }
            }
            if (!a(response, b2)) {
                f.a("Authorization");
            }
            f.a(b2);
            return f.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final int a(Response response, int i) {
        String e = response.e("Retry-After");
        if (e == null) {
            return i;
        }
        if (e.matches("\\d+")) {
            return Integer.valueOf(e).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public final boolean a(Response response, lm4 lm4) {
        lm4 g = response.L().g();
        return g.g().equals(lm4.g()) && g.k() == lm4.k() && g.n().equals(lm4.n());
    }
}
