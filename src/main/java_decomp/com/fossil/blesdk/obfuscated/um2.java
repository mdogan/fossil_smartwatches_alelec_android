package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class um2 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public void a(yz1 yz1) {
        if (yz1.d("platform")) {
            yz1.a("platform").f();
        }
        if (yz1.d("data")) {
            this.a = yz1.a("data").d().a("url").f();
        }
        if (yz1.d("metadata")) {
            this.b = yz1.a("metadata").d().a("checksum").f();
        }
        if (yz1.d("metadata")) {
            this.c = yz1.a("metadata").d().a("appVersion").f();
        }
        if (yz1.d("createdAt")) {
            yz1.a("createdAt").f();
        }
        if (yz1.d("updatedAt")) {
            this.d = yz1.a("updatedAt").f();
        }
        if (yz1.d("objectId")) {
            yz1.a("objectId").f();
        }
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
