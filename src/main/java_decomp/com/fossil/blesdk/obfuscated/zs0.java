package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.UserManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zs0<T> {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context h;
    @DexIgnore
    public static volatile Boolean i;
    @DexIgnore
    public static volatile Boolean j;
    @DexIgnore
    public /* final */ jt0 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ T d;
    @DexIgnore
    public volatile ws0 e;
    @DexIgnore
    public volatile SharedPreferences f;

    @DexIgnore
    public zs0(jt0 jt0, String str, T t) {
        this.e = null;
        this.f = null;
        if (jt0.a == null && jt0.b == null) {
            throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
        } else if (jt0.a == null || jt0.b == null) {
            this.a = jt0;
            String valueOf = String.valueOf(jt0.c);
            String valueOf2 = String.valueOf(str);
            this.c = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
            String valueOf3 = String.valueOf(jt0.d);
            String valueOf4 = String.valueOf(str);
            this.b = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
            this.d = t;
        } else {
            throw new IllegalArgumentException("Must pass one of SharedPreferences file name or ContentProvider URI");
        }
    }

    @DexIgnore
    public /* synthetic */ zs0(jt0 jt0, String str, Object obj, dt0 dt0) {
        this(jt0, str, obj);
    }

    @DexIgnore
    public static <T> zs0<T> a(jt0 jt0, String str, T t, it0<T> it0) {
        return new gt0(jt0, str, t, it0);
    }

    @DexIgnore
    public static zs0<String> a(jt0 jt0, String str, String str2) {
        return new ft0(jt0, str, str2);
    }

    @DexIgnore
    public static zs0<Boolean> a(jt0 jt0, String str, boolean z) {
        return new et0(jt0, str, Boolean.valueOf(z));
    }

    @DexIgnore
    public static <V> V a(ht0<V> ht0) {
        long clearCallingIdentity;
        try {
            return ht0.a();
        } catch (SecurityException unused) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V a2 = ht0.a();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return a2;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }

    @DexIgnore
    public static void a(Context context) {
        if (h == null) {
            synchronized (g) {
                if (Build.VERSION.SDK_INT < 24 || !context.isDeviceProtectedStorage()) {
                    Context applicationContext = context.getApplicationContext();
                    if (applicationContext != null) {
                        context = applicationContext;
                    }
                }
                if (h != context) {
                    i = null;
                }
                h = context;
            }
        }
    }

    @DexIgnore
    public static boolean a(String str, boolean z) {
        if (e()) {
            return ((Boolean) a(new ct0(str, false))).booleanValue();
        }
        return false;
    }

    @DexIgnore
    public static boolean e() {
        if (i == null) {
            Context context = h;
            boolean z = false;
            if (context == null) {
                return false;
            }
            if (l6.a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0) {
                z = true;
            }
            i = Boolean.valueOf(z);
        }
        return i.booleanValue();
    }

    @DexIgnore
    public final T a() {
        if (h != null) {
            if (this.a.f) {
                T c2 = c();
                if (c2 != null) {
                    return c2;
                }
                T b2 = b();
                if (b2 != null) {
                    return b2;
                }
            } else {
                T b3 = b();
                if (b3 != null) {
                    return b3;
                }
                T c3 = c();
                if (c3 != null) {
                    return c3;
                }
            }
            return this.d;
        }
        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
    }

    @DexIgnore
    public abstract T a(SharedPreferences sharedPreferences);

    @DexIgnore
    public abstract T a(String str);

    @DexIgnore
    @TargetApi(24)
    public final T b() {
        boolean z;
        if (a("gms:phenotype:phenotype_flag:debug_bypass_phenotype", false)) {
            String valueOf = String.valueOf(this.b);
            Log.w("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
        } else if (this.a.b != null) {
            if (this.e == null) {
                this.e = ws0.a(h.getContentResolver(), this.a.b);
            }
            String str = (String) a(new at0(this, this.e));
            if (str != null) {
                return a(str);
            }
        } else if (this.a.a != null) {
            if (Build.VERSION.SDK_INT < 24 || h.isDeviceProtectedStorage()) {
                z = true;
            } else {
                if (j == null || !j.booleanValue()) {
                    j = Boolean.valueOf(((UserManager) h.getSystemService(UserManager.class)).isUserUnlocked());
                }
                z = j.booleanValue();
            }
            if (!z) {
                return null;
            }
            if (this.f == null) {
                this.f = h.getSharedPreferences(this.a.a, 0);
            }
            SharedPreferences sharedPreferences = this.f;
            if (sharedPreferences.contains(this.b)) {
                return a(sharedPreferences);
            }
        }
        return null;
    }

    @DexIgnore
    public final T c() {
        if (this.a.e || !e()) {
            return null;
        }
        String str = (String) a(new bt0(this));
        if (str != null) {
            return a(str);
        }
        return null;
    }

    @DexIgnore
    public final /* synthetic */ String d() {
        return xy0.a(h.getContentResolver(), this.c, (String) null);
    }
}
