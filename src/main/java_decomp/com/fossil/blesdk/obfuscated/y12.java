package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y12 extends a22 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public y12(a22 a22, int i, int i2) {
        super(a22);
        this.c = (short) i;
        this.d = (short) i2;
    }

    @DexIgnore
    public void a(b22 b22, byte[] bArr) {
        b22.a(this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        short s = this.c;
        short s2 = this.d;
        short s3 = (s & ((1 << s2) - 1)) | (1 << s2);
        return SimpleComparison.LESS_THAN_OPERATION + Integer.toBinaryString(s3 | (1 << this.d)).substring(1) + '>';
    }
}
