package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nk extends mk<hk> {
    @DexIgnore
    public nk(Context context, am amVar) {
        super(yk.a(context, amVar).c());
    }

    @DexIgnore
    public boolean a(il ilVar) {
        return ilVar.j.b() == NetworkType.CONNECTED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(hk hkVar) {
        if (Build.VERSION.SDK_INT < 26) {
            return !hkVar.a();
        }
        if (!hkVar.a() || !hkVar.d()) {
            return true;
        }
        return false;
    }
}
