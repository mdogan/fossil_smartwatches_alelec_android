package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ko3 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ io3 b;

    @DexIgnore
    public ko3(BaseActivity baseActivity, io3 io3) {
        wd4.b(baseActivity, "mContext");
        wd4.b(io3, "mView");
        this.a = baseActivity;
        this.b = io3;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final io3 b() {
        return this.b;
    }
}
