package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.km4;
import com.fossil.blesdk.obfuscated.pm4;
import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jn4 implements Interceptor {
    @DexIgnore
    public /* final */ dm4 a;

    @DexIgnore
    public jn4(dm4 dm4) {
        this.a = dm4;
    }

    @DexIgnore
    public final String a(List<cm4> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            cm4 cm4 = list.get(i);
            sb.append(cm4.a());
            sb.append('=');
            sb.append(cm4.b());
        }
        return sb.toString();
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        pm4 n = chain.n();
        pm4.a f = n.f();
        RequestBody a2 = n.a();
        if (a2 != null) {
            mm4 b = a2.b();
            if (b != null) {
                f.b(GraphRequest.CONTENT_TYPE_HEADER, b.toString());
            }
            long a3 = a2.a();
            if (a3 != -1) {
                f.b("Content-Length", Long.toString(a3));
                f.a("Transfer-Encoding");
            } else {
                f.b("Transfer-Encoding", "chunked");
                f.a("Content-Length");
            }
        }
        boolean z = false;
        if (n.a("Host") == null) {
            f.b("Host", vm4.a(n.g(), false));
        }
        if (n.a("Connection") == null) {
            f.b("Connection", "Keep-Alive");
        }
        if (n.a("Accept-Encoding") == null && n.a("Range") == null) {
            z = true;
            f.b("Accept-Encoding", "gzip");
        }
        List<cm4> a4 = this.a.a(n.g());
        if (!a4.isEmpty()) {
            f.b("Cookie", a(a4));
        }
        if (n.a("User-Agent") == null) {
            f.b("User-Agent", wm4.a());
        }
        Response a5 = chain.a(f.a());
        nn4.a(this.a, n.g(), a5.D());
        Response.a H = a5.H();
        H.a(n);
        if (z && "gzip".equalsIgnoreCase(a5.e(GraphRequest.CONTENT_ENCODING_HEADER)) && nn4.b(a5)) {
            cp4 cp4 = new cp4(a5.y().E());
            km4.a a6 = a5.D().a();
            a6.c(GraphRequest.CONTENT_ENCODING_HEADER);
            a6.c("Content-Length");
            H.a(a6.a());
            H.a((qm4) new qn4(a5.e(GraphRequest.CONTENT_TYPE_HEADER), -1, ep4.a((kp4) cp4)));
        }
        return H.a();
    }
}
