package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.im4;
import com.fossil.blesdk.obfuscated.km4;
import com.fossil.blesdk.obfuscated.lm4;
import com.fossil.blesdk.obfuscated.nm4;
import com.fossil.blesdk.obfuscated.pm4;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class as4 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ lm4 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public lm4.a d;
    @DexIgnore
    public /* final */ pm4.a e; // = new pm4.a();
    @DexIgnore
    public /* final */ km4.a f;
    @DexIgnore
    public mm4 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public nm4.a i;
    @DexIgnore
    public im4.a j;
    @DexIgnore
    public RequestBody k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RequestBody {
        @DexIgnore
        public /* final */ RequestBody a;
        @DexIgnore
        public /* final */ mm4 b;

        @DexIgnore
        public a(RequestBody requestBody, mm4 mm4) {
            this.a = requestBody;
            this.b = mm4;
        }

        @DexIgnore
        public long a() throws IOException {
            return this.a.a();
        }

        @DexIgnore
        public mm4 b() {
            return this.b;
        }

        @DexIgnore
        public void a(wo4 wo4) throws IOException {
            this.a.a(wo4);
        }
    }

    @DexIgnore
    public as4(String str, lm4 lm4, String str2, km4 km4, mm4 mm4, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = lm4;
        this.c = str2;
        this.g = mm4;
        this.h = z;
        if (km4 != null) {
            this.f = km4.a();
        } else {
            this.f = new km4.a();
        }
        if (z2) {
            this.j = new im4.a();
        } else if (z3) {
            this.i = new nm4.a();
            this.i.a(nm4.f);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj.toString();
    }

    @DexIgnore
    public void b(String str, String str2, boolean z) {
        if (this.c != null) {
            String a2 = a(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", a2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void c(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            this.d = this.b.a(str3);
            if (this.d != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(str)) {
            try {
                this.g = mm4.a(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public static String a(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                vo4 vo4 = new vo4();
                vo4.a(str, 0, i2);
                a(vo4, str, i2, length, z);
                return vo4.z();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void a(vo4 vo4, String str, int i2, int i3, boolean z) {
        vo4 vo42 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (vo42 == null) {
                        vo42 = new vo4();
                    }
                    vo42.c(codePointAt);
                    while (!vo42.g()) {
                        byte readByte = vo42.readByte() & FileType.MASKED_INDEX;
                        vo4.writeByte(37);
                        vo4.writeByte((int) l[(readByte >> 4) & 15]);
                        vo4.writeByte((int) l[readByte & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
                    }
                } else {
                    vo4.c(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void a(km4 km4, RequestBody requestBody) {
        this.i.a(km4, requestBody);
    }

    @DexIgnore
    public void a(nm4.b bVar) {
        this.i.a(bVar);
    }

    @DexIgnore
    public void a(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public pm4.a a() {
        lm4 lm4;
        lm4.a aVar = this.d;
        if (aVar != null) {
            lm4 = aVar.a();
        } else {
            lm4 = this.b.b(this.c);
            if (lm4 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        a aVar2 = this.k;
        if (aVar2 == null) {
            im4.a aVar3 = this.j;
            if (aVar3 != null) {
                aVar2 = aVar3.a();
            } else {
                nm4.a aVar4 = this.i;
                if (aVar4 != null) {
                    aVar2 = aVar4.a();
                } else if (this.h) {
                    aVar2 = RequestBody.a((mm4) null, new byte[0]);
                }
            }
        }
        mm4 mm4 = this.g;
        if (mm4 != null) {
            if (aVar2 != null) {
                aVar2 = new a(aVar2, mm4);
            } else {
                this.f.a(GraphRequest.CONTENT_TYPE_HEADER, mm4.toString());
            }
        }
        pm4.a aVar5 = this.e;
        aVar5.a(lm4);
        aVar5.a(this.f.a());
        aVar5.a(this.a, aVar2);
        return aVar5;
    }
}
