package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dl1 extends cl1 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public dl1(el1 el1) {
        super(el1);
        this.b.a(this);
    }

    @DexIgnore
    public final boolean p() {
        return this.c;
    }

    @DexIgnore
    public final void q() {
        if (!p()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public abstract boolean r();

    @DexIgnore
    public final void s() {
        if (!this.c) {
            r();
            this.b.A();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
