package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nr2 implements Factory<mr2> {
    @DexIgnore
    public static /* final */ nr2 a; // = new nr2();

    @DexIgnore
    public static nr2 a() {
        return a;
    }

    @DexIgnore
    public static mr2 b() {
        return new mr2();
    }

    @DexIgnore
    public mr2 get() {
        return b();
    }
}
