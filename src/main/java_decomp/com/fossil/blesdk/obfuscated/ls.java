package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ls<T> implements po<T> {
    @DexIgnore
    public static /* final */ po<?> b; // = new ls();

    @DexIgnore
    public static <T> ls<T> a() {
        return (ls) b;
    }

    @DexIgnore
    public bq<T> a(Context context, bq<T> bqVar, int i, int i2) {
        return bqVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
    }
}
