package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.view.WindowInsets;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o9 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public o9(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public o9 a() {
        if (Build.VERSION.SDK_INT >= 20) {
            return new o9(((WindowInsets) this.a).consumeSystemWindowInsets());
        }
        return null;
    }

    @DexIgnore
    public int b() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.a).getSystemWindowInsetBottom();
        }
        return 0;
    }

    @DexIgnore
    public int c() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.a).getSystemWindowInsetLeft();
        }
        return 0;
    }

    @DexIgnore
    public int d() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.a).getSystemWindowInsetRight();
        }
        return 0;
    }

    @DexIgnore
    public int e() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.a).getSystemWindowInsetTop();
        }
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || o9.class != obj.getClass()) {
            return false;
        }
        Object obj2 = this.a;
        Object obj3 = ((o9) obj).a;
        if (obj2 != null) {
            return obj2.equals(obj3);
        }
        if (obj3 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.a).hasSystemWindowInsets();
        }
        return false;
    }

    @DexIgnore
    public boolean g() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((WindowInsets) this.a).isConsumed();
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Object obj = this.a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @DexIgnore
    public o9 a(int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 20) {
            return new o9(((WindowInsets) this.a).replaceSystemWindowInsets(i, i2, i3, i4));
        }
        return null;
    }

    @DexIgnore
    public static o9 a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new o9(obj);
    }

    @DexIgnore
    public static Object a(o9 o9Var) {
        if (o9Var == null) {
            return null;
        }
        return o9Var.a;
    }
}
