package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z42 implements Factory<FirmwareFileRepository> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public z42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static z42 a(o42 o42) {
        return new z42(o42);
    }

    @DexIgnore
    public static FirmwareFileRepository b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static FirmwareFileRepository c(o42 o42) {
        FirmwareFileRepository g = o42.g();
        o44.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    public FirmwareFileRepository get() {
        return b(this.a);
    }
}
