package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uc4 {
    @DexIgnore
    public static final <T> kc4<T> a(kc4<? super T> kc4) {
        wd4.b(kc4, "completion");
        return kc4;
    }

    @DexIgnore
    public static final void b(kc4<?> kc4) {
        wd4.b(kc4, "frame");
    }

    @DexIgnore
    public static final void c(kc4<?> kc4) {
        wd4.b(kc4, "frame");
    }
}
