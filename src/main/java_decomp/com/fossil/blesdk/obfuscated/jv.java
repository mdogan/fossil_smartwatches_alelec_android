package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jv {
    @DexIgnore
    public /* final */ AtomicReference<tw> a; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ g4<tw, List<Class<?>>> b; // = new g4<>();

    @DexIgnore
    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        List<Class<?>> list;
        tw andSet = this.a.getAndSet((Object) null);
        if (andSet == null) {
            andSet = new tw(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
        }
        synchronized (this.b) {
            list = this.b.get(andSet);
        }
        this.a.set(andSet);
        return list;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new tw(cls, cls2, cls3), list);
        }
    }
}
