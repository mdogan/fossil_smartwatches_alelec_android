package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zd1 extends m31 implements xd1 {
    @DexIgnore
    public zd1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }
}
