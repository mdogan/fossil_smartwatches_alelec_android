package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x80 extends q70 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x80(Peripheral peripheral) {
        super(DeviceConfigOperationCode.LEGACY_OTA_RESET, RequestId.LEGACY_OTA_RESET, peripheral, 0, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void a(e20 e20) {
        wd4.b(e20, "connectionStateChangedNotification");
        if (e20.a() != Peripheral.State.DISCONNECTED) {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.UNKNOWN_ERROR, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
        } else if (e20.b() == 19 || H()) {
            a(n());
        } else {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.CONNECTION_DROPPED, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
        }
    }
}
