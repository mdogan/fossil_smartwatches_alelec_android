package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class l50 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Peripheral.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[DeviceInformation.BondRequirement.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[RequestId.values().length];

    /*
    static {
        a[Peripheral.State.DISCONNECTED.ordinal()] = 1;
        a[Peripheral.State.CONNECTING.ordinal()] = 2;
        a[Peripheral.State.CONNECTED.ordinal()] = 3;
        a[Peripheral.State.DISCONNECTING.ordinal()] = 4;
        b[DeviceInformation.BondRequirement.NO_REQUIRE.ordinal()] = 1;
        b[DeviceInformation.BondRequirement.REQUIRE.ordinal()] = 2;
        c[RequestId.DISCONNECT.ordinal()] = 1;
    }
    */
}
