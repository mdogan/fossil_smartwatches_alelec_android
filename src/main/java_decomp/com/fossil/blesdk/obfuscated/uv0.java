package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uv0 extends vv0, Cloneable {
    @DexIgnore
    uv0 a(tv0 tv0);

    @DexIgnore
    tv0 s();

    @DexIgnore
    tv0 v();
}
