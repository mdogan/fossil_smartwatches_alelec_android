package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class al3 extends wk3 {
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ xk3 g;
    @DexIgnore
    public /* final */ ResetPasswordUseCase h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<ResetPasswordUseCase.d, ResetPasswordUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ al3 a;

        @DexIgnore
        public b(al3 al3) {
            this.a = al3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ResetPasswordUseCase.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.h().i();
            this.a.h().u0();
        }

        @DexIgnore
        public void a(ResetPasswordUseCase.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.h().i();
            int a2 = cVar.a();
            if (a2 == 400005) {
                xk3 h = this.a.h();
                String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_InputError_Text__InvalidEmailAddress);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                h.H(a3);
                this.a.h().G(false);
            } else if (a2 != 404001) {
                xk3 h2 = this.a.h();
                int a4 = cVar.a();
                String b = cVar.b();
                if (b == null) {
                    b = "";
                }
                h2.a(a4, b);
                this.a.h().G(false);
            } else {
                xk3 h3 = this.a.h();
                String a5 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_EmailNotRegistered_Text__EmailIsNotRegistered);
                wd4.a((Object) a5, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                h3.H(a5);
                this.a.h().G(true);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public al3(xk3 xk3, ResetPasswordUseCase resetPasswordUseCase) {
        wd4.b(xk3, "mView");
        wd4.b(resetPasswordUseCase, "mResetPasswordUseCase");
        this.g = xk3;
        this.h = resetPasswordUseCase;
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, "email");
        this.f = str;
        if (str.length() == 0) {
            this.g.l0();
        } else {
            this.g.a0();
        }
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, "email");
        if (i()) {
            this.g.k();
            this.h.a(new ResetPasswordUseCase.b(str), new b(this));
        }
    }

    @DexIgnore
    public final void c(String str) {
        wd4.b(str, "email");
        this.f = str;
    }

    @DexIgnore
    public void f() {
        this.g.O(this.f);
    }

    @DexIgnore
    public final xk3 h() {
        return this.g;
    }

    @DexIgnore
    public final boolean i() {
        if (this.f.length() == 0) {
            this.g.H("");
            return false;
        } else if (!ls3.a(this.f)) {
            xk3 xk3 = this.g;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_InputError_Text__InvalidEmailAddress);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            xk3.H(a2);
            return false;
        } else {
            this.g.H("");
            return true;
        }
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        wd4.b(str, "key");
        wd4.b(bundle, "outState");
        bundle.putString(str, this.f);
    }
}
