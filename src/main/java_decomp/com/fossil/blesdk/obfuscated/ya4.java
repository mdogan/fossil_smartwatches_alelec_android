package com.fossil.blesdk.obfuscated;

import kotlin.LazyThreadSafetyMode;
import kotlin.NoWhenBranchMatchedException;
import kotlin.SafePublicationLazyImpl;
import kotlin.SynchronizedLazyImpl;
import kotlin.UnsafeLazyImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ya4 {
    @DexIgnore
    public static final <T> wa4<T> a(id4<? extends T> id4) {
        wd4.b(id4, "initializer");
        return new SynchronizedLazyImpl(id4, (Object) null, 2, (rd4) null);
    }

    @DexIgnore
    public static final <T> wa4<T> a(LazyThreadSafetyMode lazyThreadSafetyMode, id4<? extends T> id4) {
        wd4.b(lazyThreadSafetyMode, "mode");
        wd4.b(id4, "initializer");
        int i = xa4.a[lazyThreadSafetyMode.ordinal()];
        if (i == 1) {
            return new SynchronizedLazyImpl(id4, (Object) null, 2, (rd4) null);
        }
        if (i == 2) {
            return new SafePublicationLazyImpl(id4);
        }
        if (i == 3) {
            return new UnsafeLazyImpl(id4);
        }
        throw new NoWhenBranchMatchedException();
    }
}
