package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ty {
    @DexIgnore
    public /* final */ e a;
    @DexIgnore
    public /* final */ AlertDialog.Builder b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e e;

        @DexIgnore
        public a(e eVar) {
            this.e = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.e.a(true);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e e;

        @DexIgnore
        public b(e eVar) {
            this.e = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.e.a(false);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d e;
        @DexIgnore
        public /* final */ /* synthetic */ e f;

        @DexIgnore
        public c(d dVar, e eVar) {
            this.e = dVar;
            this.f = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.e.a(true);
            this.f.a(true);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public ty(AlertDialog.Builder builder, e eVar) {
        this.a = eVar;
        this.b = builder;
    }

    @DexIgnore
    public static int a(float f, int i) {
        return (int) (f * ((float) i));
    }

    @DexIgnore
    public static ty a(Activity activity, y74 y74, d dVar) {
        e eVar = new e((a) null);
        hz hzVar = new hz(activity, y74);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        ScrollView a2 = a(activity, hzVar.c());
        builder.setView(a2).setTitle(hzVar.e()).setCancelable(false).setNeutralButton(hzVar.d(), new a(eVar));
        if (y74.d) {
            builder.setNegativeButton(hzVar.b(), new b(eVar));
        }
        if (y74.f) {
            builder.setPositiveButton(hzVar.a(), new c(dVar, eVar));
        }
        return new ty(builder, eVar);
    }

    @DexIgnore
    public boolean b() {
        return this.a.b();
    }

    @DexIgnore
    public void c() {
        this.b.show();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ CountDownLatch b;

        @DexIgnore
        public e() {
            this.a = false;
            this.b = new CountDownLatch(1);
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
            this.b.countDown();
        }

        @DexIgnore
        public boolean b() {
            return this.a;
        }

        @DexIgnore
        public void a() {
            try {
                this.b.await();
            } catch (InterruptedException unused) {
            }
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }
    }

    @DexIgnore
    public static ScrollView a(Activity activity, String str) {
        float f = activity.getResources().getDisplayMetrics().density;
        int a2 = a(f, 5);
        TextView textView = new TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(a2, a2, a2, a2);
        textView.setFocusable(false);
        ScrollView scrollView = new ScrollView(activity);
        scrollView.setPadding(a(f, 14), a(f, 2), a(f, 10), a(f, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }
}
