package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr0 extends Binder {
    @DexIgnore
    public /* final */ lr0 e;

    @DexIgnore
    public pr0(lr0 lr0) {
        this.e = lr0;
    }

    @DexIgnore
    public final void a(nr0 nr0) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.e.e.execute(new qr0(this, nr0));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
