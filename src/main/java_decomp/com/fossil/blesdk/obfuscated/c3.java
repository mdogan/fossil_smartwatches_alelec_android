package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c3 implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public static c3 n;
    @DexIgnore
    public static c3 o;
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public /* final */ CharSequence f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ Runnable h; // = new a();
    @DexIgnore
    public /* final */ Runnable i; // = new b();
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public d3 l;
    @DexIgnore
    public boolean m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            c3.this.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            c3.this.c();
        }
    }

    @DexIgnore
    public c3(View view, CharSequence charSequence) {
        this.e = view;
        this.f = charSequence;
        this.g = h9.a(ViewConfiguration.get(this.e.getContext()));
        b();
        this.e.setOnLongClickListener(this);
        this.e.setOnHoverListener(this);
    }

    @DexIgnore
    public static void a(View view, CharSequence charSequence) {
        c3 c3Var = n;
        if (c3Var != null && c3Var.e == view) {
            a((c3) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            c3 c3Var2 = o;
            if (c3Var2 != null && c3Var2.e == view) {
                c3Var2.c();
            }
            view.setOnLongClickListener((View.OnLongClickListener) null);
            view.setLongClickable(false);
            view.setOnHoverListener((View.OnHoverListener) null);
            return;
        }
        new c3(view, charSequence);
    }

    @DexIgnore
    public final void b() {
        this.j = Integer.MAX_VALUE;
        this.k = Integer.MAX_VALUE;
    }

    @DexIgnore
    public void c() {
        if (o == this) {
            o = null;
            d3 d3Var = this.l;
            if (d3Var != null) {
                d3Var.a();
                this.l = null;
                b();
                this.e.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (n == this) {
            a((c3) null);
        }
        this.e.removeCallbacks(this.i);
    }

    @DexIgnore
    public final void d() {
        this.e.postDelayed(this.h, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.l != null && this.m) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.e.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                b();
                c();
            }
        } else if (this.e.isEnabled() && this.l == null && a(motionEvent)) {
            a(this);
        }
        return false;
    }

    @DexIgnore
    public boolean onLongClick(View view) {
        this.j = view.getWidth() / 2;
        this.k = view.getHeight() / 2;
        a(true);
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        c();
    }

    @DexIgnore
    public void a(boolean z) {
        long j2;
        int i2;
        long j3;
        if (g9.y(this.e)) {
            a((c3) null);
            c3 c3Var = o;
            if (c3Var != null) {
                c3Var.c();
            }
            o = this;
            this.m = z;
            this.l = new d3(this.e.getContext());
            this.l.a(this.e, this.j, this.k, this.m, this.f);
            this.e.addOnAttachStateChangeListener(this);
            if (this.m) {
                j2 = 2500;
            } else {
                if ((g9.s(this.e) & 1) == 1) {
                    j3 = 3000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                } else {
                    j3 = 15000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j3 - ((long) i2);
            }
            this.e.removeCallbacks(this.i);
            this.e.postDelayed(this.i, j2);
        }
    }

    @DexIgnore
    public static void a(c3 c3Var) {
        c3 c3Var2 = n;
        if (c3Var2 != null) {
            c3Var2.a();
        }
        n = c3Var;
        c3 c3Var3 = n;
        if (c3Var3 != null) {
            c3Var3.d();
        }
    }

    @DexIgnore
    public final void a() {
        this.e.removeCallbacks(this.h);
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.j) <= this.g && Math.abs(y - this.k) <= this.g) {
            return false;
        }
        this.j = x;
        this.k = y;
        return true;
    }
}
