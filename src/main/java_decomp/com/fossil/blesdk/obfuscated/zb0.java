package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Scope;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zb0 {
    @DexIgnore
    List<Scope> b();
}
