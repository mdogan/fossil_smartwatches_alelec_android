package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i00 {
    /*
    static {
        a(1, 3);
        a(1, 4);
        a(2, 0);
        a(3, 2);
    }
    */

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }
}
