package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ux0 implements Cloneable {
    @DexIgnore
    public sx0<?, ?> e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public List<Object> g; // = new ArrayList();

    @DexIgnore
    public final void a(qx0 qx0) throws IOException {
        if (this.f == null) {
            Iterator<Object> it = this.g.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NoSuchMethodError();
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final byte[] a() throws IOException {
        byte[] bArr = new byte[b()];
        a(qx0.a(bArr));
        return bArr;
    }

    @DexIgnore
    public final int b() {
        if (this.f == null) {
            Iterator<Object> it = this.g.iterator();
            if (!it.hasNext()) {
                return 0;
            }
            it.next();
            throw new NoSuchMethodError();
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    /* renamed from: c */
    public final ux0 clone() {
        Object clone;
        ux0 ux0 = new ux0();
        try {
            ux0.e = this.e;
            if (this.g == null) {
                ux0.g = null;
            } else {
                ux0.g.addAll(this.g);
            }
            if (this.f != null) {
                if (this.f instanceof wx0) {
                    clone = (wx0) ((wx0) this.f).clone();
                } else if (this.f instanceof byte[]) {
                    clone = ((byte[]) this.f).clone();
                } else {
                    int i = 0;
                    if (this.f instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f;
                        byte[][] bArr2 = new byte[bArr.length][];
                        ux0.f = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f instanceof boolean[]) {
                        clone = ((boolean[]) this.f).clone();
                    } else if (this.f instanceof int[]) {
                        clone = ((int[]) this.f).clone();
                    } else if (this.f instanceof long[]) {
                        clone = ((long[]) this.f).clone();
                    } else if (this.f instanceof float[]) {
                        clone = ((float[]) this.f).clone();
                    } else if (this.f instanceof double[]) {
                        clone = ((double[]) this.f).clone();
                    } else if (this.f instanceof wx0[]) {
                        wx0[] wx0Arr = (wx0[]) this.f;
                        wx0[] wx0Arr2 = new wx0[wx0Arr.length];
                        ux0.f = wx0Arr2;
                        while (i < wx0Arr.length) {
                            wx0Arr2[i] = (wx0) wx0Arr[i].clone();
                            i++;
                        }
                    }
                }
                ux0.f = clone;
            }
            return ux0;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ux0)) {
            return false;
        }
        ux0 ux0 = (ux0) obj;
        if (this.f == null || ux0.f == null) {
            List<Object> list = this.g;
            if (list != null) {
                List<Object> list2 = ux0.g;
                if (list2 != null) {
                    return list.equals(list2);
                }
            }
            try {
                return Arrays.equals(a(), ux0.a());
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        } else {
            sx0<?, ?> sx0 = this.e;
            if (sx0 != ux0.e) {
                return false;
            }
            if (!sx0.a.isArray()) {
                return this.f.equals(ux0.f);
            }
            Object obj2 = this.f;
            return obj2 instanceof byte[] ? Arrays.equals((byte[]) obj2, (byte[]) ux0.f) : obj2 instanceof int[] ? Arrays.equals((int[]) obj2, (int[]) ux0.f) : obj2 instanceof long[] ? Arrays.equals((long[]) obj2, (long[]) ux0.f) : obj2 instanceof float[] ? Arrays.equals((float[]) obj2, (float[]) ux0.f) : obj2 instanceof double[] ? Arrays.equals((double[]) obj2, (double[]) ux0.f) : obj2 instanceof boolean[] ? Arrays.equals((boolean[]) obj2, (boolean[]) ux0.f) : Arrays.deepEquals((Object[]) obj2, (Object[]) ux0.f);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return Arrays.hashCode(a()) + 527;
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }
}
