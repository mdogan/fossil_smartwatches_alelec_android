package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sh2 extends rh2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.ftv_alphabet, 1);
        A.put(R.id.cl_main_container, 2);
        A.put(R.id.accb_select, 3);
        A.put(R.id.ll_text_container, 4);
        A.put(R.id.pick_contact_title, 5);
        A.put(R.id.pick_contact_phone, 6);
        A.put(R.id.v_line_separation, 7);
    }
    */

    @DexIgnore
    public sh2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 8, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sh2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[3], objArr[2], objArr[0], objArr[1], objArr[4], objArr[6], objArr[5], objArr[7]);
        this.y = -1;
        this.s.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
