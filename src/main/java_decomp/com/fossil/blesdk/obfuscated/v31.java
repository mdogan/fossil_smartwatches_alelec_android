package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface v31 extends IInterface {
    @DexIgnore
    void a(l41 l41) throws RemoteException;

    @DexIgnore
    void a(uc1 uc1, x31 x31, String str) throws RemoteException;

    @DexIgnore
    void a(w41 w41) throws RemoteException;

    @DexIgnore
    Location b(String str) throws RemoteException;

    @DexIgnore
    void e(boolean z) throws RemoteException;
}
