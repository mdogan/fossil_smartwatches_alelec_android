package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yt1<K, V> implements Map.Entry<K, V> {
    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!st1.a(getKey(), entry.getKey()) || !st1.a(getValue(), entry.getValue())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public abstract K getKey();

    @DexIgnore
    public abstract V getValue();

    @DexIgnore
    public int hashCode() {
        int i;
        Object key = getKey();
        Object value = getValue();
        int i2 = 0;
        if (key == null) {
            i = 0;
        } else {
            i = key.hashCode();
        }
        if (value != null) {
            i2 = value.hashCode();
        }
        return i ^ i2;
    }

    @DexIgnore
    public V setValue(V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return getKey() + SimpleComparison.EQUAL_TO_OPERATION + getValue();
    }
}
