package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fossil.blesdk.obfuscated.mu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ou implements mu {
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ mu.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ BroadcastReceiver i; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ou ouVar = ou.this;
            boolean z = ouVar.g;
            ouVar.g = ouVar.a(context);
            if (z != ou.this.g) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + ou.this.g);
                }
                ou ouVar2 = ou.this;
                ouVar2.f.a(ouVar2.g);
            }
        }
    }

    @DexIgnore
    public ou(Context context, mu.a aVar) {
        this.e = context.getApplicationContext();
        this.f = aVar;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        uw.a(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (RuntimeException e2) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e2);
            }
            return true;
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void c() {
        e();
    }

    @DexIgnore
    public final void d() {
        if (!this.h) {
            this.g = a(this.e);
            try {
                this.e.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.h = true;
            } catch (SecurityException e2) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e2);
                }
            }
        }
    }

    @DexIgnore
    public final void e() {
        if (this.h) {
            this.e.unregisterReceiver(this.i);
            this.h = false;
        }
    }

    @DexIgnore
    public void a() {
        d();
    }
}
