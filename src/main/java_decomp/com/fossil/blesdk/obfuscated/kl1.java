package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kl1 extends dl1 {
    @DexIgnore
    public kl1(el1 el1) {
        super(el1);
    }

    @DexIgnore
    public static Object b(d61 d61, String str) {
        e61 a = a(d61, str);
        if (a == null) {
            return null;
        }
        String str2 = a.d;
        if (str2 != null) {
            return str2;
        }
        Long l = a.e;
        if (l != null) {
            return l;
        }
        Double d = a.g;
        if (d != null) {
            return d;
        }
        return null;
    }

    @DexIgnore
    public final void a(j61 j61, Object obj) {
        ck0.a(obj);
        j61.e = null;
        j61.f = null;
        j61.h = null;
        if (obj instanceof String) {
            j61.e = (String) obj;
        } else if (obj instanceof Long) {
            j61.f = (Long) obj;
        } else if (obj instanceof Double) {
            j61.h = (Double) obj;
        } else {
            d().s().a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final int[] t() {
        Map<String, String> a = kg1.a(this.b.getContext());
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = kg1.R.a().intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (((String) next.getKey()).startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt((String) next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            d().v().a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    d().v().a("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            iArr[i2] = ((Integer) obj).intValue();
            i2++;
        }
        return iArr;
    }

    @DexIgnore
    public final String b(f61 f61) {
        g61[] g61Arr;
        d61[] d61Arr;
        String str;
        d61[] d61Arr2;
        String str2;
        int i;
        int i2;
        g61[] g61Arr2;
        String str3;
        j61[] j61Arr;
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        g61[] g61Arr3 = f61.c;
        if (g61Arr3 != null) {
            int length = g61Arr3.length;
            int i3 = 0;
            while (i3 < length) {
                g61 g61 = g61Arr3[i3];
                if (g61 == null || g61 == null) {
                    g61Arr = g61Arr3;
                } else {
                    a(sb, 1);
                    sb.append("bundle {\n");
                    a(sb, 1, "protocol_version", (Object) g61.c);
                    a(sb, 1, "platform", (Object) g61.k);
                    a(sb, 1, "gmp_version", (Object) g61.s);
                    a(sb, 1, "uploading_gmp_version", (Object) g61.t);
                    a(sb, 1, "config_version", (Object) g61.I);
                    a(sb, 1, "gmp_app_id", (Object) g61.A);
                    a(sb, 1, "admob_app_id", (Object) g61.N);
                    a(sb, 1, "app_id", (Object) g61.q);
                    a(sb, 1, "app_version", (Object) g61.r);
                    a(sb, 1, "app_version_major", (Object) g61.E);
                    a(sb, 1, "firebase_instance_id", (Object) g61.D);
                    a(sb, 1, "dev_cert_hash", (Object) g61.x);
                    a(sb, 1, "app_store", (Object) g61.p);
                    a(sb, 1, "upload_timestamp_millis", (Object) g61.f);
                    a(sb, 1, "start_timestamp_millis", (Object) g61.g);
                    a(sb, 1, "end_timestamp_millis", (Object) g61.h);
                    a(sb, 1, "previous_bundle_start_timestamp_millis", (Object) g61.i);
                    a(sb, 1, "previous_bundle_end_timestamp_millis", (Object) g61.j);
                    a(sb, 1, "app_instance_id", (Object) g61.w);
                    a(sb, 1, "resettable_device_id", (Object) g61.u);
                    a(sb, 1, "device_id", (Object) g61.H);
                    a(sb, 1, "ds_id", (Object) g61.K);
                    a(sb, 1, "limited_ad_tracking", (Object) g61.v);
                    a(sb, 1, Constants.OS_VERSION, (Object) g61.l);
                    a(sb, 1, "device_model", (Object) g61.m);
                    a(sb, 1, "user_default_language", (Object) g61.n);
                    a(sb, 1, "time_zone_offset_minutes", (Object) g61.o);
                    a(sb, 1, "bundle_sequential_index", (Object) g61.y);
                    a(sb, 1, "service_upload", (Object) g61.B);
                    a(sb, 1, "health_monitor", (Object) g61.z);
                    Long l = g61.J;
                    if (!(l == null || l.longValue() == 0)) {
                        a(sb, 1, "android_id", (Object) g61.J);
                    }
                    Integer num = g61.M;
                    if (num != null) {
                        a(sb, 1, "retry_counter", (Object) num);
                    }
                    j61[] j61Arr2 = g61.e;
                    String str4 = "string_value";
                    String str5 = "name";
                    int i4 = 2;
                    if (j61Arr2 != null) {
                        int length2 = j61Arr2.length;
                        int i5 = 0;
                        while (i5 < length2) {
                            j61 j61 = j61Arr2[i5];
                            if (j61 != null) {
                                a(sb, 2);
                                sb.append("user_property {\n");
                                j61Arr = j61Arr2;
                                a(sb, 2, "set_timestamp_millis", (Object) j61.c);
                                a(sb, 2, str5, (Object) i().c(j61.d));
                                a(sb, 2, str4, (Object) j61.e);
                                a(sb, 2, "int_value", (Object) j61.f);
                                a(sb, 2, "double_value", (Object) j61.h);
                                a(sb, 2);
                                sb.append("}\n");
                            } else {
                                j61Arr = j61Arr2;
                            }
                            i5++;
                            j61Arr2 = j61Arr;
                        }
                    }
                    b61[] b61Arr = g61.C;
                    String str6 = g61.q;
                    if (b61Arr != null) {
                        int length3 = b61Arr.length;
                        int i6 = 0;
                        while (i6 < length3) {
                            b61 b61 = b61Arr[i6];
                            if (b61 != null) {
                                a(sb, i4);
                                sb.append("audience_membership {\n");
                                i2 = i6;
                                i = length3;
                                a(sb, 2, "audience_id", (Object) b61.c);
                                a(sb, 2, "new_audience", (Object) b61.f);
                                h61 h61 = b61.d;
                                StringBuilder sb2 = sb;
                                str2 = str5;
                                g61Arr2 = g61Arr3;
                                str3 = str4;
                                String str7 = str6;
                                a(sb2, 2, "current_data", h61, str7);
                                a(sb2, 2, "previous_data", b61.e, str7);
                                a(sb, 2);
                                sb.append("}\n");
                            } else {
                                i2 = i6;
                                i = length3;
                                str2 = str5;
                                g61Arr2 = g61Arr3;
                                str3 = str4;
                            }
                            i6 = i2 + 1;
                            str4 = str3;
                            g61Arr3 = g61Arr2;
                            length3 = i;
                            str5 = str2;
                            i4 = 2;
                        }
                    }
                    String str8 = str5;
                    g61Arr = g61Arr3;
                    int i7 = 2;
                    String str9 = str4;
                    d61[] d61Arr3 = g61.d;
                    if (d61Arr3 != null) {
                        int length4 = d61Arr3.length;
                        int i8 = 0;
                        while (i8 < length4) {
                            d61 d61 = d61Arr3[i8];
                            if (d61 != null) {
                                a(sb, i7);
                                sb.append("event {\n");
                                str = str8;
                                a(sb, i7, str, (Object) i().a(d61.d));
                                a(sb, i7, "timestamp_millis", (Object) d61.e);
                                a(sb, i7, "previous_timestamp_millis", (Object) d61.f);
                                a(sb, i7, "count", (Object) d61.g);
                                e61[] e61Arr = d61.c;
                                if (e61Arr != null) {
                                    int length5 = e61Arr.length;
                                    int i9 = 0;
                                    while (i9 < length5) {
                                        e61 e61 = e61Arr[i9];
                                        if (e61 != null) {
                                            a(sb, 3);
                                            sb.append("param {\n");
                                            d61Arr2 = d61Arr3;
                                            a(sb, 3, str, (Object) i().b(e61.c));
                                            a(sb, 3, str9, (Object) e61.d);
                                            a(sb, 3, "int_value", (Object) e61.e);
                                            a(sb, 3, "double_value", (Object) e61.g);
                                            a(sb, 3);
                                            sb.append("}\n");
                                        } else {
                                            d61Arr2 = d61Arr3;
                                        }
                                        i9++;
                                        d61Arr3 = d61Arr2;
                                        i7 = 2;
                                    }
                                }
                                d61Arr = d61Arr3;
                                a(sb, i7);
                                sb.append("}\n");
                            } else {
                                d61Arr = d61Arr3;
                                str = str8;
                            }
                            i8++;
                            str8 = str;
                            d61Arr3 = d61Arr;
                        }
                    }
                    a(sb, 1);
                    sb.append("}\n");
                }
                i3++;
                g61Arr3 = g61Arr;
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final void a(e61 e61, Object obj) {
        ck0.a(obj);
        e61.d = null;
        e61.e = null;
        e61.g = null;
        if (obj instanceof String) {
            e61.d = (String) obj;
        } else if (obj instanceof Long) {
            e61.e = (Long) obj;
        } else if (obj instanceof Double) {
            e61.g = (Double) obj;
        } else {
            d().s().a("Ignoring invalid (type) event param value", obj);
        }
    }

    @DexIgnore
    public final byte[] a(f61 f61) {
        try {
            byte[] bArr = new byte[f61.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            f61.a(a);
            a.b();
            return bArr;
        } catch (IOException e) {
            d().s().a("Data loss. Failed to serialize batch", e);
            return null;
        }
    }

    @DexIgnore
    public static e61 a(d61 d61, String str) {
        for (e61 e61 : d61.c) {
            if (e61.c.equals(str)) {
                return e61;
            }
        }
        return null;
    }

    @DexIgnore
    public static e61[] a(e61[] e61Arr, String str, Object obj) {
        for (e61 e61 : e61Arr) {
            if (str.equals(e61.c)) {
                e61.e = null;
                e61.d = null;
                e61.g = null;
                if (obj instanceof Long) {
                    e61.e = (Long) obj;
                } else if (obj instanceof String) {
                    e61.d = (String) obj;
                } else if (obj instanceof Double) {
                    e61.g = (Double) obj;
                }
                return e61Arr;
            }
        }
        e61[] e61Arr2 = new e61[(e61Arr.length + 1)];
        System.arraycopy(e61Arr, 0, e61Arr2, 0, e61Arr.length);
        e61 e612 = new e61();
        e612.c = str;
        if (obj instanceof Long) {
            e612.e = (Long) obj;
        } else if (obj instanceof String) {
            e612.d = (String) obj;
        } else if (obj instanceof Double) {
            e612.g = (Double) obj;
        }
        e61Arr2[e61Arr.length] = e612;
        return e61Arr2;
    }

    @DexIgnore
    public final String a(t51 t51) {
        if (t51 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        a(sb, 0, "filter_id", (Object) t51.c);
        a(sb, 0, BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, (Object) i().a(t51.d));
        a(sb, 1, "event_count_filter", t51.g);
        sb.append("  filters {\n");
        for (u51 a : t51.e) {
            a(sb, 2, a);
        }
        a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(w51 w51) {
        if (w51 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        a(sb, 0, "filter_id", (Object) w51.c);
        a(sb, 0, "property_name", (Object) i().c(w51.d));
        a(sb, 1, w51.e);
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, h61 h61, String str2) {
        StringBuilder sb2 = sb;
        h61 h612 = h61;
        if (h612 != null) {
            a(sb2, 3);
            sb2.append(str);
            sb2.append(" {\n");
            if (h612.d != null) {
                a(sb2, 4);
                sb2.append("results: ");
                long[] jArr = h612.d;
                int length = jArr.length;
                int i2 = 0;
                int i3 = 0;
                while (i2 < length) {
                    Long valueOf = Long.valueOf(jArr[i2]);
                    int i4 = i3 + 1;
                    if (i3 != 0) {
                        sb2.append(", ");
                    }
                    sb2.append(valueOf);
                    i2++;
                    i3 = i4;
                }
                sb2.append(10);
            }
            if (h612.c != null) {
                a(sb2, 4);
                sb2.append("status: ");
                long[] jArr2 = h612.c;
                int length2 = jArr2.length;
                int i5 = 0;
                int i6 = 0;
                while (i5 < length2) {
                    Long valueOf2 = Long.valueOf(jArr2[i5]);
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb2.append(", ");
                    }
                    sb2.append(valueOf2);
                    i5++;
                    i6 = i7;
                }
                sb2.append(10);
            }
            if (l().j(str2)) {
                if (h612.e != null) {
                    a(sb2, 4);
                    sb2.append("dynamic_filter_timestamps: {");
                    c61[] c61Arr = h612.e;
                    int length3 = c61Arr.length;
                    int i8 = 0;
                    int i9 = 0;
                    while (i8 < length3) {
                        c61 c61 = c61Arr[i8];
                        int i10 = i9 + 1;
                        if (i9 != 0) {
                            sb2.append(", ");
                        }
                        sb2.append(c61.c);
                        sb2.append(":");
                        sb2.append(c61.d);
                        i8++;
                        i9 = i10;
                    }
                    sb2.append("}\n");
                }
                if (h612.f != null) {
                    a(sb2, 4);
                    sb2.append("sequence_filter_timestamps: {");
                    i61[] i61Arr = h612.f;
                    int length4 = i61Arr.length;
                    int i11 = 0;
                    int i12 = 0;
                    while (i11 < length4) {
                        i61 i61 = i61Arr[i11];
                        int i13 = i12 + 1;
                        if (i12 != 0) {
                            sb2.append(", ");
                        }
                        sb2.append(i61.c);
                        sb2.append(": [");
                        long[] jArr3 = i61.d;
                        int length5 = jArr3.length;
                        int i14 = 0;
                        int i15 = 0;
                        while (i14 < length5) {
                            long j = jArr3[i14];
                            int i16 = i15 + 1;
                            if (i15 != 0) {
                                sb2.append(", ");
                            }
                            sb2.append(j);
                            i14++;
                            i15 = i16;
                        }
                        sb2.append("]");
                        i11++;
                        i12 = i13;
                    }
                    sb2.append("}\n");
                }
            }
            a(sb2, 3);
            sb2.append("}\n");
        }
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            d().s().a("Failed to gzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, v51 v51) {
        if (v51 != null) {
            a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            Integer num = v51.c;
            if (num != null) {
                int intValue = num.intValue();
                a(sb, i, "comparison_type", (Object) intValue != 1 ? intValue != 2 ? intValue != 3 ? intValue != 4 ? "UNKNOWN_COMPARISON_TYPE" : "BETWEEN" : "EQUAL" : "GREATER_THAN" : "LESS_THAN");
            }
            a(sb, i, "match_as_float", (Object) v51.d);
            a(sb, i, "comparison_value", (Object) v51.e);
            a(sb, i, "min_comparison_value", (Object) v51.f);
            a(sb, i, "max_comparison_value", (Object) v51.g);
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, u51 u51) {
        String str;
        if (u51 != null) {
            a(sb, i);
            sb.append("filter {\n");
            a(sb, i, "complement", (Object) u51.e);
            a(sb, i, "param_name", (Object) i().b(u51.f));
            int i2 = i + 1;
            x51 x51 = u51.c;
            if (x51 != null) {
                a(sb, i2);
                sb.append("string_filter");
                sb.append(" {\n");
                Integer num = x51.c;
                if (num != null) {
                    switch (num.intValue()) {
                        case 1:
                            str = "REGEXP";
                            break;
                        case 2:
                            str = "BEGINS_WITH";
                            break;
                        case 3:
                            str = "ENDS_WITH";
                            break;
                        case 4:
                            str = "PARTIAL";
                            break;
                        case 5:
                            str = "EXACT";
                            break;
                        case 6:
                            str = "IN_LIST";
                            break;
                        default:
                            str = "UNKNOWN_MATCH_TYPE";
                            break;
                    }
                    a(sb, i2, "match_type", (Object) str);
                }
                a(sb, i2, "expression", (Object) x51.d);
                a(sb, i2, "case_sensitive", (Object) x51.e);
                if (x51.f.length > 0) {
                    a(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String append : x51.f) {
                        a(sb, i2 + 2);
                        sb.append(append);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                a(sb, i2);
                sb.append("}\n");
            }
            a(sb, i2, "number_filter", u51.d);
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        d().s().a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    public final <T extends Parcelable> T a(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        T t = (Parcelable) creator.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }

    @DexIgnore
    public final boolean a(ig1 ig1, sl1 sl1) {
        ck0.a(ig1);
        ck0.a(sl1);
        if (!TextUtils.isEmpty(sl1.f) || !TextUtils.isEmpty(sl1.v)) {
            return true;
        }
        b();
        return false;
    }

    @DexIgnore
    public static boolean a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    @DexIgnore
    public static boolean a(long[] jArr, int i) {
        if (i >= (jArr.length << 6)) {
            return false;
        }
        if (((1 << (i % 64)) & jArr[i / 64]) != 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static long[] a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        long[] jArr = new long[length];
        for (int i = 0; i < length; i++) {
            jArr[i] = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    jArr[i] = jArr[i] | (1 << i2);
                }
            }
        }
        return jArr;
    }

    @DexIgnore
    public final boolean a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(c().b() - j) > j2;
    }

    @DexIgnore
    public final byte[] a(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            d().s().a("Failed to ungzip content", e);
            throw e;
        }
    }
}
