package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pt2 extends RecyclerView.g<c> implements Filterable {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((rd4) null);
    @DexIgnore
    public List<AppWrapper> e; // = new ArrayList();
    @DexIgnore
    public b f;
    @DexIgnore
    public List<AppWrapper> g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return pt2.h;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ hh2 a;
        @DexIgnore
        public /* final */ /* synthetic */ pt2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.e.b.g;
                    if (b == null) {
                        wd4.a();
                        throw null;
                    } else if (((AppWrapper) b.get(adapterPosition)).getUri() != null) {
                        List b2 = this.e.b.g;
                        if (b2 != null) {
                            InstalledApp installedApp = ((AppWrapper) b2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a = pt2.i.a();
                                local.d(a, "isSelected = " + booleanValue);
                                b c = this.e.b.f;
                                if (c != null) {
                                    List b3 = this.e.b.g;
                                    if (b3 != null) {
                                        c.a((AppWrapper) b3.get(adapterPosition), !booleanValue);
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pt2 pt2, hh2 hh2) {
            super(hh2.d());
            wd4.b(hh2, "binding");
            this.b = pt2;
            this.a = hh2;
            this.a.s.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            wd4.b(appWrapper, "appWrapper");
            ImageView imageView = this.a.r;
            wd4.a((Object) imageView, "binding.ivAppIcon");
            gk2 a2 = dk2.a(imageView.getContext());
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new ak2(installedApp)).b(appWrapper.getIconResourceId()).c().a(this.a.r);
                FlexibleTextView flexibleTextView = this.a.q;
                wd4.a((Object) flexibleTextView, "binding.ftvAppName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (appWrapper.getUri() != null) {
                    FlexibleTextView flexibleTextView2 = this.a.q;
                    wd4.a((Object) flexibleTextView2, "binding.ftvAppName");
                    flexibleTextView2.setTextColor(flexibleTextView2.getTextColors().withAlpha(255));
                    SwitchCompat switchCompat = this.a.s;
                    wd4.a((Object) switchCompat, "binding.swEnabled");
                    switchCompat.setEnabled(true);
                    SwitchCompat switchCompat2 = this.a.s;
                    wd4.a((Object) switchCompat2, "binding.swEnabled");
                    Drawable background = switchCompat2.getBackground();
                    wd4.a((Object) background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    FlexibleTextView flexibleTextView3 = this.a.q;
                    wd4.a((Object) flexibleTextView3, "binding.ftvAppName");
                    flexibleTextView3.setTextColor(flexibleTextView3.getTextColors().withAlpha(100));
                    SwitchCompat switchCompat3 = this.a.s;
                    wd4.a((Object) switchCompat3, "binding.swEnabled");
                    switchCompat3.setEnabled(false);
                    SwitchCompat switchCompat4 = this.a.s;
                    wd4.a((Object) switchCompat4, "binding.swEnabled");
                    Drawable background2 = switchCompat4.getBackground();
                    wd4.a((Object) background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                SwitchCompat switchCompat5 = this.a.s;
                wd4.a((Object) switchCompat5, "binding.swEnabled");
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    switchCompat5.setChecked(isSelected.booleanValue());
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ pt2 a;

        @DexIgnore
        public d(pt2 pt2) {
            this.a = pt2;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            wd4.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.e;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.e) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp != null) {
                            String title = installedApp.getTitle();
                            if (title != null) {
                                if (title != null) {
                                    str = title.toLowerCase();
                                    wd4.a((Object) str, "(this as java.lang.String).toLowerCase()");
                                    if (str != null && StringsKt__StringsKt.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                                        arrayList.add(appWrapper);
                                    }
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            }
                        }
                        str = null;
                        arrayList.add(appWrapper);
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            wd4.b(charSequence, "charSequence");
            wd4.b(filterResults, "results");
            this.a.g = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = pt2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationAppsAdapter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public pt2() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new d(this);
    }

    @DexIgnore
    public int getItemCount() {
        List<AppWrapper> list = this.g;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public long getItemId(int i2) {
        return (long) i2;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wd4.b(viewGroup, "parent");
        hh2 a2 = hh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wd4.a((Object) a2, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i2) {
        wd4.b(cVar, "holder");
        List<AppWrapper> list = this.g;
        if (list != null) {
            cVar.a(list.get(i2));
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        wd4.b(list, "listAppWrapper");
        this.e.clear();
        this.e.addAll(list);
        this.g = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "listener");
        this.f = bVar;
    }
}
