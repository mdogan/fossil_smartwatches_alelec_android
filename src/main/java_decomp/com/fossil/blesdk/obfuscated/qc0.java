package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qc0 {
    @DexIgnore
    public static qc0 c;
    @DexIgnore
    public dc0 a;
    @DexIgnore
    public GoogleSignInAccount b; // = this.a.b();

    @DexIgnore
    public qc0(Context context) {
        this.a = dc0.a(context);
        this.a.c();
    }

    @DexIgnore
    public static synchronized qc0 a(Context context) {
        qc0 b2;
        synchronized (qc0.class) {
            b2 = b(context.getApplicationContext());
        }
        return b2;
    }

    @DexIgnore
    public static synchronized qc0 b(Context context) {
        qc0 qc0;
        synchronized (qc0.class) {
            if (c == null) {
                c = new qc0(context);
            }
            qc0 = c;
        }
        return qc0;
    }

    @DexIgnore
    public final synchronized void a() {
        this.a.a();
        this.b = null;
    }

    @DexIgnore
    public final synchronized GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final synchronized void a(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.a.a(googleSignInAccount, googleSignInOptions);
        this.b = googleSignInAccount;
    }
}
