package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface p70 {
    @DexIgnore
    String getLogName();

    @DexIgnore
    boolean isSuccessCode();
}
