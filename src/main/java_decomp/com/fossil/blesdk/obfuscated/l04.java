package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l04 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public void a(String str) {
        this.a = str;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.e;
    }

    @DexIgnore
    public boolean e() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return "StatSpecifyReportedInfo [appKey=" + this.a + ", installChannel=" + this.b + ", version=" + this.c + ", sendImmediately=" + this.d + ", isImportant=" + this.e + "]";
    }
}
