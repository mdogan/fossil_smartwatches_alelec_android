package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zs implements po<Drawable> {
    @DexIgnore
    public /* final */ po<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public zs(po<Bitmap> poVar, boolean z) {
        this.b = poVar;
        this.c = z;
    }

    @DexIgnore
    public bq<Drawable> a(Context context, bq<Drawable> bqVar, int i, int i2) {
        kq c2 = sn.a(context).c();
        Drawable drawable = bqVar.get();
        bq<Bitmap> a = ys.a(c2, drawable, i, i2);
        if (a != null) {
            bq<Bitmap> a2 = this.b.a(context, a, i, i2);
            if (!a2.equals(a)) {
                return a(context, a2);
            }
            a2.a();
            return bqVar;
        } else if (!this.c) {
            return bqVar;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    public po<BitmapDrawable> a() {
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof zs) {
            return this.b.equals(((zs) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final bq<Drawable> a(Context context, bq<Bitmap> bqVar) {
        return et.a(context.getResources(), bqVar);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
