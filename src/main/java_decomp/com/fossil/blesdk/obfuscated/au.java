package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class au implements no<Cdo, Bitmap> {
    @DexIgnore
    public /* final */ kq a;

    @DexIgnore
    public au(kq kqVar) {
        this.a = kqVar;
    }

    @DexIgnore
    public boolean a(Cdo doVar, mo moVar) {
        return true;
    }

    @DexIgnore
    public bq<Bitmap> a(Cdo doVar, int i, int i2, mo moVar) {
        return qs.a(doVar.a(), this.a);
    }
}
