package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mi0 implements dh0 {
    @DexIgnore
    public /* final */ /* synthetic */ ji0 a;

    @DexIgnore
    public mi0(ji0 ji0) {
        this.a = ji0;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        this.a.q.lock();
        try {
            vd0 unused = this.a.o = vd0.i;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ mi0(ji0 ji0, ki0 ki0) {
        this(ji0);
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        this.a.q.lock();
        try {
            vd0 unused = this.a.o = vd0;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a.q.lock();
        try {
            if (this.a.p) {
                boolean unused = this.a.p = false;
                this.a.a(i, z);
                return;
            }
            boolean unused2 = this.a.p = true;
            this.a.h.f(i);
            this.a.q.unlock();
        } finally {
            this.a.q.unlock();
        }
    }
}
