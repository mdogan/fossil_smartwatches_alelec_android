package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bp4 extends lp4 {
    @DexIgnore
    public lp4 e;

    @DexIgnore
    public bp4(lp4 lp4) {
        if (lp4 != null) {
            this.e = lp4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public final bp4 a(lp4 lp4) {
        if (lp4 != null) {
            this.e = lp4;
            return this;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public lp4 b() {
        return this.e.b();
    }

    @DexIgnore
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    public boolean d() {
        return this.e.d();
    }

    @DexIgnore
    public void e() throws IOException {
        this.e.e();
    }

    @DexIgnore
    public final lp4 g() {
        return this.e;
    }

    @DexIgnore
    public lp4 a(long j, TimeUnit timeUnit) {
        return this.e.a(j, timeUnit);
    }

    @DexIgnore
    public lp4 a(long j) {
        return this.e.a(j);
    }

    @DexIgnore
    public lp4 a() {
        return this.e.a();
    }
}
