package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m72 {
    @DexIgnore
    public final List<Integer> a(String str) {
        wd4.b(str, "arrayValue");
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return arrayList;
        }
        JSONArray jSONArray = new JSONArray(str);
        int i = 0;
        int length = jSONArray.length();
        while (i < length) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                arrayList.add((Integer) obj);
                i++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final String a(List<Integer> list) {
        if (list == null) {
            return "";
        }
        String jSONArray = new JSONArray(list).toString();
        wd4.a((Object) jSONArray, "JSONArray(value).toString()");
        return jSONArray;
    }
}
