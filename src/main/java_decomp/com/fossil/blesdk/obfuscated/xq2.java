package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xq2 implements Factory<SetVibrationStrengthUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<fn2> b;

    @DexIgnore
    public xq2(Provider<DeviceRepository> provider, Provider<fn2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static xq2 a(Provider<DeviceRepository> provider, Provider<fn2> provider2) {
        return new xq2(provider, provider2);
    }

    @DexIgnore
    public static SetVibrationStrengthUseCase b(Provider<DeviceRepository> provider, Provider<fn2> provider2) {
        return new SetVibrationStrengthUseCase(provider.get(), provider2.get());
    }

    @DexIgnore
    public SetVibrationStrengthUseCase get() {
        return b(this.a, this.b);
    }
}
