package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vu0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wu0<T extends vu0> {
    @DexIgnore
    T zzb(int i);
}
