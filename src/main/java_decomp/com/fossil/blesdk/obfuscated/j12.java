package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j12 extends m12 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public j12() {
        a();
    }

    @DexIgnore
    public j12 a() {
        this.a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    public j12 a(k12 k12) throws IOException {
        while (true) {
            int j = k12.j();
            if (j == 0) {
                return this;
            }
            if (j == 18) {
                this.a = k12.i();
            } else if (j == 26) {
                this.b = k12.i();
            } else if (j == 50) {
                k12.i();
            } else if (!o12.b(k12, j)) {
                return this;
            }
        }
    }
}
