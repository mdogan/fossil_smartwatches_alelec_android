package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ui4 {
    @DexIgnore
    public static final yg4 a(ri4 ri4) {
        return vi4.a(ri4);
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, CancellationException cancellationException) {
        vi4.a(coroutineContext, cancellationException);
    }

    @DexIgnore
    public static final ai4 a(ri4 ri4, ai4 ai4) {
        return vi4.a(ri4, ai4);
    }
}
