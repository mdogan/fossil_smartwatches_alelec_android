package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ud1 extends IInterface {
    @DexIgnore
    void a(LocationAvailability locationAvailability) throws RemoteException;

    @DexIgnore
    void a(LocationResult locationResult) throws RemoteException;
}
