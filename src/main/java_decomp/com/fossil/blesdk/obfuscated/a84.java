package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a84 {
    @DexIgnore
    public /* final */ AtomicReference<c84> a;
    @DexIgnore
    public /* final */ CountDownLatch b;
    @DexIgnore
    public b84 c;
    @DexIgnore
    public boolean d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ a84 a; // = new a84();
    }

    @DexIgnore
    public static a84 d() {
        return b.a;
    }

    @DexIgnore
    public synchronized a84 a(w44 w44, IdManager idManager, a74 a74, String str, String str2, String str3, p54 p54) {
        w44 w442 = w44;
        synchronized (this) {
            if (this.d) {
                return this;
            }
            if (this.c == null) {
                Context l = w44.l();
                String d2 = idManager.d();
                String d3 = new l54().d(l);
                String g = idManager.g();
                y54 y54 = new y54();
                u74 u74 = new u74();
                s74 s74 = new s74(w442);
                String c2 = CommonUtils.c(l);
                String str4 = str3;
                v74 v74 = new v74(w442, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", new Object[]{d2}), a74);
                String h = idManager.h();
                String str5 = h;
                String str6 = str2;
                String str7 = str;
                w44 w443 = w44;
                this.c = new t74(w443, new f84(d3, str5, idManager.i(), idManager.j(), idManager.e(), CommonUtils.a(CommonUtils.n(l)), str6, str7, DeliveryMechanism.determineFrom(g).getId(), c2), y54, u74, s74, v74, p54);
            }
            this.d = true;
            return this;
        }
    }

    @DexIgnore
    public synchronized boolean b() {
        c84 a2;
        a2 = this.c.a();
        a(a2);
        return a2 != null;
    }

    @DexIgnore
    public synchronized boolean c() {
        c84 a2;
        a2 = this.c.a(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        a(a2);
        if (a2 == null) {
            r44.g().e("Fabric", "Failed to force reload of settings from Crashlytics.", (Throwable) null);
        }
        return a2 != null;
    }

    @DexIgnore
    public a84() {
        this.a = new AtomicReference<>();
        this.b = new CountDownLatch(1);
        this.d = false;
    }

    @DexIgnore
    public c84 a() {
        try {
            this.b.await();
            return this.a.get();
        } catch (InterruptedException unused) {
            r44.g().e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    @DexIgnore
    public final void a(c84 c84) {
        this.a.set(c84);
        this.b.countDown();
    }
}
