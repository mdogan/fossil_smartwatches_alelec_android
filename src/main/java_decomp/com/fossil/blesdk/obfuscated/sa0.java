package com.fossil.blesdk.obfuscated;

import java.util.Vector;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sa0<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Vector<ta0<T>> b; // = new Vector<>();

    @DexIgnore
    public final synchronized void a(ta0<T> ta0) {
        wd4.b(ta0, "o");
        if (!this.b.contains(ta0)) {
            this.b.addElement(ta0);
        }
    }

    @DexIgnore
    public final synchronized void b(ta0<T> ta0) {
        wd4.b(ta0, "o");
        this.b.removeElement(ta0);
    }

    @DexIgnore
    public final synchronized void c() {
        this.a = true;
    }

    @DexIgnore
    public final synchronized boolean b() {
        return this.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        r1 = ((com.fossil.blesdk.obfuscated.ta0[]) r0.element).length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r1 = r1 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        if (r1 < 0) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        ((com.fossil.blesdk.obfuscated.ta0[]) r0.element)[r1].a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    @DexIgnore
    public final void a(T t) {
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        synchronized (this) {
            if (b()) {
                Object[] array = this.b.toArray(new ta0[0]);
                if (array != null) {
                    ref$ObjectRef.element = (ta0[]) array;
                    a();
                    cb4 cb4 = cb4.a;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        }
    }

    @DexIgnore
    public final synchronized void a() {
        this.a = false;
    }
}
