package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.pm4;
import com.zendesk.sdk.network.Constants;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import kotlin.TypeCastException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ra0 implements Interceptor {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ra0(String str, String str2) {
        wd4.b(str, "accessKey");
        wd4.b(str2, "secretKey");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a(String str, String str2) {
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            String str3 = str2 + str;
            Charset f = va0.y.f();
            if (str3 != null) {
                byte[] bytes = str3.getBytes(f);
                wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                instance.update(bytes);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte valueOf : digest) {
                    be4 be4 = be4.a;
                    Object[] objArr = {Byte.valueOf(valueOf)};
                    String format = String.format("%02X", Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                }
                String sb2 = sb.toString();
                wd4.a((Object) sb2, "stringBuilder.toString()");
                return sb2;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        } catch (Exception e) {
            ea0.l.a(e);
            return "";
        }
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) {
        wd4.b(chain, "chain");
        String a2 = o90.a(System.currentTimeMillis(), false);
        pm4.a f = chain.n().f();
        f.b(GraphRequest.CONTENT_TYPE_HEADER, Constants.APPLICATION_JSON);
        f.b("X-Cyc-Auth-Method", "signature");
        f.b("X-Cyc-Access-Key-Id", this.a);
        f.b("X-Cyc-Timestamp", a2);
        f.b("Authorization", "Signature=" + a(this.b, a2));
        Response a3 = chain.a(f.a());
        wd4.a((Object) a3, "chain.proceed(request)");
        return a3;
    }
}
