package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj4 extends gh4 {
    @DexIgnore
    public static /* final */ rj4 e; // = new rj4();

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean b(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        return false;
    }

    @DexIgnore
    public String toString() {
        return "Unconfined";
    }
}
