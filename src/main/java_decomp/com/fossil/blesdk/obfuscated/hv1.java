package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hv1 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    public <T> T a(nv1<T> nv1) throws IOException {
        tt1.a(nv1);
        kv1 y = kv1.y();
        try {
            Reader a = a();
            y.a(a);
            T a2 = iv1.a(a, nv1);
            y.close();
            return a2;
        } catch (Throwable th) {
            y.close();
            throw th;
        }
    }
}
