package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rh1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ yh1 e;
    @DexIgnore
    public /* final */ /* synthetic */ long f;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle g;
    @DexIgnore
    public /* final */ /* synthetic */ Context h;
    @DexIgnore
    public /* final */ /* synthetic */ ug1 i;
    @DexIgnore
    public /* final */ /* synthetic */ BroadcastReceiver.PendingResult j;

    @DexIgnore
    public rh1(ph1 ph1, yh1 yh1, long j2, Bundle bundle, Context context, ug1 ug1, BroadcastReceiver.PendingResult pendingResult) {
        this.e = yh1;
        this.f = j2;
        this.g = bundle;
        this.h = context;
        this.i = ug1;
        this.j = pendingResult;
    }

    @DexIgnore
    public final void run() {
        long a = this.e.t().j.a();
        long j2 = this.f;
        if (a > 0 && (j2 >= a || j2 <= 0)) {
            j2 = a - 1;
        }
        if (j2 > 0) {
            this.g.putLong("click_timestamp", j2);
        }
        this.g.putString("_cis", "referrer broadcast");
        yh1.a(this.h, (pg1) null).k().b("auto", "_cmp", this.g);
        this.i.A().a("Install campaign recorded");
        BroadcastReceiver.PendingResult pendingResult = this.j;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
