package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gy1 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ Map<Integer, yn1<Void>> b; // = new g4();
    @DexIgnore
    public /* final */ by1 c;

    @DexIgnore
    public gy1(by1 by1) {
        this.c = by1;
    }

    @DexIgnore
    public final synchronized boolean a() {
        return b() != null;
    }

    @DexIgnore
    public final String b() {
        String b2;
        synchronized (this.c) {
            b2 = this.c.b();
        }
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        String[] split = b2.split(",");
        if (split.length <= 1 || TextUtils.isEmpty(split[1])) {
            return null;
        }
        return split[1];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (a(r5, r0) != false) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2 = r4.b.remove(java.lang.Integer.valueOf(r4.a));
        a(r0);
        r4.a++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (r2 == null) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        r2.a(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        return true;
     */
    @DexIgnore
    public final boolean a(FirebaseInstanceId firebaseInstanceId) {
        while (true) {
            synchronized (this) {
                String b2 = b();
                if (b2 == null) {
                    if (FirebaseInstanceId.o()) {
                        Log.d("FirebaseInstanceId", "topic sync succeeded");
                    }
                }
            }
        }
        while (true) {
        }
    }

    @DexIgnore
    public final synchronized boolean a(String str) {
        synchronized (this.c) {
            String b2 = this.c.b();
            String valueOf = String.valueOf(str);
            if (!b2.startsWith(valueOf.length() != 0 ? ",".concat(valueOf) : new String(","))) {
                return false;
            }
            String valueOf2 = String.valueOf(str);
            this.c.a(b2.substring((valueOf2.length() != 0 ? ",".concat(valueOf2) : new String(",")).length()));
            return true;
        }
    }

    @DexIgnore
    public static boolean a(FirebaseInstanceId firebaseInstanceId, String str) {
        String[] split = str.split("!");
        if (split.length == 2) {
            String str2 = split[0];
            String str3 = split[1];
            char c2 = 65535;
            try {
                int hashCode = str2.hashCode();
                if (hashCode != 83) {
                    if (hashCode == 85) {
                        if (str2.equals("U")) {
                            c2 = 1;
                        }
                    }
                } else if (str2.equals(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX)) {
                    c2 = 0;
                }
                if (c2 == 0) {
                    firebaseInstanceId.a(str3);
                    if (FirebaseInstanceId.o()) {
                        Log.d("FirebaseInstanceId", "subscribe operation succeeded");
                    }
                } else if (c2 == 1) {
                    firebaseInstanceId.b(str3);
                    if (FirebaseInstanceId.o()) {
                        Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
                    }
                }
            } catch (IOException e) {
                String valueOf = String.valueOf(e.getMessage());
                Log.e("FirebaseInstanceId", valueOf.length() != 0 ? "Topic sync failed: ".concat(valueOf) : new String("Topic sync failed: "));
                return false;
            }
        }
        return true;
    }
}
