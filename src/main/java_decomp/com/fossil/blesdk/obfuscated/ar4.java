package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.c0;
import pub.devrel.easypermissions.AppSettingsDialogHolderActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ar4 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ar4> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public Object l;
    @DexIgnore
    public Context m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ar4> {
        @DexIgnore
        public ar4 createFromParcel(Parcel parcel) {
            return new ar4(parcel, (a) null);
        }

        @DexIgnore
        public ar4[] newArray(int i) {
            return new ar4[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public int h; // = -1;
        @DexIgnore
        public boolean i; // = false;

        @DexIgnore
        public b(Fragment fragment) {
            this.a = fragment;
            this.b = fragment.getContext();
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b b(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public ar4 a() {
            this.d = TextUtils.isEmpty(this.d) ? this.b.getString(dr4.rationale_ask_again) : this.d;
            this.e = TextUtils.isEmpty(this.e) ? this.b.getString(dr4.title_settings_dialog) : this.e;
            this.f = TextUtils.isEmpty(this.f) ? this.b.getString(17039370) : this.f;
            this.g = TextUtils.isEmpty(this.g) ? this.b.getString(17039360) : this.g;
            int i2 = this.h;
            if (i2 <= 0) {
                i2 = 16061;
            }
            this.h = i2;
            return new ar4(this.a, this.c, this.d, this.e, this.f, this.g, this.h, this.i ? 268435456 : 0, (a) null);
        }
    }

    @DexIgnore
    public /* synthetic */ ar4(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public static ar4 a(Intent intent, Activity activity) {
        ar4 ar4 = (ar4) intent.getParcelableExtra("extra_app_settings");
        ar4.a((Object) activity);
        return ar4;
    }

    @DexIgnore
    public void b() {
        a(AppSettingsDialogHolderActivity.a(this.m, this));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeInt(this.j);
        parcel.writeInt(this.k);
    }

    @DexIgnore
    public /* synthetic */ ar4(Object obj, int i2, String str, String str2, String str3, String str4, int i3, int i4, a aVar) {
        this(obj, i2, str, str2, str3, str4, i3, i4);
    }

    @DexIgnore
    public ar4(Parcel parcel) {
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readInt();
        this.k = parcel.readInt();
    }

    @DexIgnore
    public final void a(Object obj) {
        this.l = obj;
        if (obj instanceof Activity) {
            this.m = (Activity) obj;
        } else if (obj instanceof Fragment) {
            this.m = ((Fragment) obj).getContext();
        } else {
            throw new IllegalStateException("Unknown object: " + obj);
        }
    }

    @DexIgnore
    public final void a(Intent intent) {
        Object obj = this.l;
        if (obj instanceof Activity) {
            ((Activity) obj).startActivityForResult(intent, this.j);
        } else if (obj instanceof Fragment) {
            ((Fragment) obj).startActivityForResult(intent, this.j);
        }
    }

    @DexIgnore
    public ar4(Object obj, int i2, String str, String str2, String str3, String str4, int i3, int i4) {
        a(obj);
        this.e = i2;
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.i = str4;
        this.j = i3;
        this.k = i4;
    }

    @DexIgnore
    public c0 a(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        c0.a aVar;
        int i2 = this.e;
        if (i2 > 0) {
            aVar = new c0.a(this.m, i2);
        } else {
            aVar = new c0.a(this.m);
        }
        aVar.a(false);
        aVar.b((CharSequence) this.g);
        aVar.a((CharSequence) this.f);
        aVar.b(this.h, onClickListener);
        aVar.a((CharSequence) this.i, onClickListener2);
        return aVar.c();
    }

    @DexIgnore
    public int a() {
        return this.k;
    }
}
