package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iv {
    @DexIgnore
    public static /* final */ zp<?, ?, ?> c; // = new zp(Object.class, Object.class, Object.class, Collections.singletonList(new pp(Object.class, Object.class, Object.class, Collections.emptyList(), new ju(), (h8<List<Throwable>>) null)), (h8<List<Throwable>>) null);
    @DexIgnore
    public /* final */ g4<tw, zp<?, ?, ?>> a; // = new g4<>();
    @DexIgnore
    public /* final */ AtomicReference<tw> b; // = new AtomicReference<>();

    @DexIgnore
    public boolean a(zp<?, ?, ?> zpVar) {
        return c.equals(zpVar);
    }

    @DexIgnore
    public final tw b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        tw andSet = this.b.getAndSet((Object) null);
        if (andSet == null) {
            andSet = new tw();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public <Data, TResource, Transcode> zp<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        zp<Data, TResource, Transcode> zpVar;
        tw b2 = b(cls, cls2, cls3);
        synchronized (this.a) {
            zpVar = this.a.get(b2);
        }
        this.b.set(b2);
        return zpVar;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, zp<?, ?, ?> zpVar) {
        synchronized (this.a) {
            g4<tw, zp<?, ?, ?>> g4Var = this.a;
            tw twVar = new tw(cls, cls2, cls3);
            if (zpVar == null) {
                zpVar = c;
            }
            g4Var.put(twVar, zpVar);
        }
    }
}
