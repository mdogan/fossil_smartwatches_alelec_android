package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a91<E> extends List<E>, RandomAccess {
    @DexIgnore
    boolean A();

    @DexIgnore
    void B();

    @DexIgnore
    a91<E> b(int i);
}
