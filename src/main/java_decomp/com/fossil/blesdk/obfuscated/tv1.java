package com.fossil.blesdk.obfuscated;

import com.google.common.reflect.TypeToken;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tv1 extends AccessibleObject implements Member {
    @DexIgnore
    public /* final */ AccessibleObject e;
    @DexIgnore
    public /* final */ Member f;

    @DexIgnore
    public <M extends AccessibleObject & Member> tv1(M m) {
        tt1.a(m);
        this.e = m;
        this.f = (Member) m;
    }

    @DexIgnore
    public TypeToken<?> a() {
        return TypeToken.of(getDeclaringClass());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof tv1)) {
            return false;
        }
        tv1 tv1 = (tv1) obj;
        if (!a().equals(tv1.a()) || !this.f.equals(tv1.f)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return this.e.getAnnotation(cls);
    }

    @DexIgnore
    public final Annotation[] getAnnotations() {
        return this.e.getAnnotations();
    }

    @DexIgnore
    public final Annotation[] getDeclaredAnnotations() {
        return this.e.getDeclaredAnnotations();
    }

    @DexIgnore
    public Class<?> getDeclaringClass() {
        return this.f.getDeclaringClass();
    }

    @DexIgnore
    public final int getModifiers() {
        return this.f.getModifiers();
    }

    @DexIgnore
    public final String getName() {
        return this.f.getName();
    }

    @DexIgnore
    public int hashCode() {
        return this.f.hashCode();
    }

    @DexIgnore
    public final boolean isAccessible() {
        return this.e.isAccessible();
    }

    @DexIgnore
    public final boolean isAnnotationPresent(Class<? extends Annotation> cls) {
        return this.e.isAnnotationPresent(cls);
    }

    @DexIgnore
    public final boolean isSynthetic() {
        return this.f.isSynthetic();
    }

    @DexIgnore
    public final void setAccessible(boolean z) throws SecurityException {
        this.e.setAccessible(z);
    }

    @DexIgnore
    public String toString() {
        return this.f.toString();
    }
}
