package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ed implements cd {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public ed(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ed)) {
            return false;
        }
        ed edVar = (ed) obj;
        if (TextUtils.equals(this.a, edVar.a) && this.b == edVar.b && this.c == edVar.c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return f8.a(this.a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
