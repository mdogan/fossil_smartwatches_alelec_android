package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x11 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<x11> CREATOR; // = new y11();
    @DexIgnore
    public /* final */ List<DataType> e;

    @DexIgnore
    public x11(List<DataType> list) {
        this.e = list;
    }

    @DexIgnore
    public final List<DataType> H() {
        return Collections.unmodifiableList(this.e);
    }

    @DexIgnore
    public final String toString() {
        ak0.a a = ak0.a((Object) this);
        a.a("dataTypes", this.e);
        return a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.b(parcel, 1, Collections.unmodifiableList(this.e), false);
        lk0.a(parcel, a);
    }
}
