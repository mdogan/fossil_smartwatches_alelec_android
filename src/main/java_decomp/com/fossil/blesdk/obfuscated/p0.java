package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p0 extends Drawable implements Drawable.Callback {
    @DexIgnore
    public Drawable e;

    @DexIgnore
    public p0(Drawable drawable) {
        a(drawable);
    }

    @DexIgnore
    public Drawable a() {
        return this.e;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.e.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return this.e.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.e.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.e.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.e.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.e.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.e.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.e.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.e.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.e.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.e.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return c7.f(this.e);
    }

    @DexIgnore
    public boolean isStateful() {
        return this.e.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        c7.g(this.e);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.e.setBounds(rect);
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.e.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.e.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        c7.a(this.e, z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.e.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.e.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.e.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.e.setFilterBitmap(z);
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        c7.a(this.e, f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        c7.a(this.e, i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return this.e.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        c7.b(this.e, i);
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        c7.a(this.e, colorStateList);
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        c7.a(this.e, mode);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.e.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        Drawable drawable2 = this.e;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.e = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
