package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ke1 extends IInterface {
    @DexIgnore
    h51 a(lf1 lf1) throws RemoteException;

    @DexIgnore
    void a(tn0 tn0) throws RemoteException;

    @DexIgnore
    void b(tn0 tn0) throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    pe1 m() throws RemoteException;
}
