package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fc4<T> extends gb4<T> implements RandomAccess {
    @DexIgnore
    public /* final */ Object[] f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ int i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fb4<T> {
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;
        @DexIgnore
        public /* final */ /* synthetic */ fc4 i;

        @DexIgnore
        public a(fc4 fc4) {
            this.i = fc4;
            this.g = fc4.size();
            this.h = fc4.g;
        }

        @DexIgnore
        public void a() {
            if (this.g == 0) {
                b();
                return;
            }
            b(this.i.f[this.h]);
            this.h = (this.h + 1) % this.i.b();
            this.g--;
        }
    }

    @DexIgnore
    public fc4(int i2) {
        this.i = i2;
        if (this.i >= 0) {
            this.f = new Object[this.i];
            return;
        }
        throw new IllegalArgumentException(("ring buffer capacity should not be negative but it is " + this.i).toString());
    }

    @DexIgnore
    public final void add(T t) {
        if (!c()) {
            this.f[(this.g + size()) % b()] = t;
            this.h = size() + 1;
            return;
        }
        throw new IllegalStateException("ring buffer is full");
    }

    @DexIgnore
    public final boolean c() {
        return size() == this.i;
    }

    @DexIgnore
    public T get(int i2) {
        gb4.e.a(i2, size());
        return this.f[(this.g + i2) % b()];
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        wd4.b(tArr, "array");
        if (tArr.length < size()) {
            tArr = Arrays.copyOf(tArr, size());
            wd4.a((Object) tArr, "java.util.Arrays.copyOf(this, newSize)");
        }
        int size = size();
        int i2 = 0;
        int i3 = this.g;
        int i4 = 0;
        while (i4 < size && i3 < this.i) {
            tArr[i4] = this.f[i3];
            i4++;
            i3++;
        }
        while (i4 < size) {
            tArr[i4] = this.f[i2];
            i4++;
            i2++;
        }
        if (tArr.length > size()) {
            tArr[size()] = null;
        }
        if (tArr != null) {
            return tArr;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public int a() {
        return this.h;
    }

    @DexIgnore
    public final int b() {
        return this.i;
    }

    @DexIgnore
    public final void a(int i2) {
        boolean z = true;
        if (i2 >= 0) {
            if (i2 > size()) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(("n shouldn't be greater than the buffer size: n = " + i2 + ", size = " + size()).toString());
            } else if (i2 > 0) {
                int i3 = this.g;
                int b = (i3 + i2) % b();
                if (i3 > b) {
                    kb4.a(this.f, null, i3, this.i);
                    kb4.a(this.f, null, 0, b);
                } else {
                    kb4.a(this.f, null, i3, b);
                }
                this.g = b;
                this.h = size() - i2;
            }
        } else {
            throw new IllegalArgumentException(("n shouldn't be negative but it is " + i2).toString());
        }
    }

    @DexIgnore
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }
}
