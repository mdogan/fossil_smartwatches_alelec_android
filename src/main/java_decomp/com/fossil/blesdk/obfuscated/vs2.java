package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vs2 extends RecyclerView.g<b> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<MicroApp> c;
    @DexIgnore
    public c d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public MicroApp d;
        @DexIgnore
        public /* final */ /* synthetic */ vs2 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.e.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    c c = this.e.e.c();
                    if (c != null) {
                        Object obj = this.e.e.c.get(this.e.getAdapterPosition());
                        wd4.a(obj, "mData[adapterPosition]");
                        c.a((MicroApp) obj);
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vs2$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.vs2$b$b  reason: collision with other inner class name */
        public static final class C0106b implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0106b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                wd4.b(customizeWidget, "view");
                this.a.e.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vs2 vs2, View view) {
            super(view);
            wd4.b(view, "view");
            this.e = vs2;
            this.c = view.findViewById(R.id.iv_indicator);
            View findViewById = view.findViewById(R.id.wc_watch_app);
            wd4.a((Object) findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_watch_app_name);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new a(this));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x008a, code lost:
            if (r0 != null) goto L_0x008f;
         */
        @DexIgnore
        public final void a(MicroApp microApp) {
            String str;
            wd4.b(microApp, "microApp");
            this.d = microApp;
            if (this.d != null) {
                this.a.c(microApp.getId());
                this.b.setText(tm2.a(PortfolioApp.W.c(), microApp.getNameKey(), microApp.getName()));
            }
            this.a.setSelectedWc(wd4.a((Object) microApp.getId(), (Object) this.e.b));
            if (wd4.a((Object) microApp.getId(), (Object) this.e.b)) {
                View view = this.c;
                wd4.a((Object) view, "ivIndicator");
                view.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_arrow));
            } else {
                View view2 = this.c;
                wd4.a((Object) view2, "ivIndicator");
                view2.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_grey));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            MicroApp microApp2 = this.d;
            if (microApp2 != null) {
                str = microApp2.getId();
            }
            str = "";
            Intent putExtra = intent.putExtra("KEY_ID", str);
            wd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "WATCH_APP", putExtra, (View.OnDragListener) null, new C0106b(this), 4, (Object) null);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(MicroApp microApp);
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ vs2(ArrayList arrayList, c cVar, int i, rd4 rd4) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final c c() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d("MicroAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_watch_app, viewGroup, false);
        wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public vs2(ArrayList<MicroApp> arrayList, c cVar) {
        wd4.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<MicroApp> list) {
        wd4.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "microAppId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        wd4.b(str, "microAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wd4.a((Object) ((MicroApp) t).getId(), (Object) str)) {
                break;
            }
        }
        MicroApp microApp = (MicroApp) t;
        if (microApp != null) {
            return this.c.indexOf(microApp);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wd4.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            MicroApp microApp = this.c.get(i);
            wd4.a((Object) microApp, "mData[position]");
            bVar.a(microApp);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wd4.b(cVar, "listener");
        this.d = cVar;
    }
}
