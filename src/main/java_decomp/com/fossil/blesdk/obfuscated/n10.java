package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.GattCharacteristicProperty;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n10 extends b10 {
    @DexIgnore
    public byte[] m; // = new byte[0];
    @DexIgnore
    public /* final */ boolean n;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n10(GattCharacteristic.CharacteristicId characteristicId, boolean z, Peripheral.c cVar) {
        super(BluetoothCommandId.SUBSCRIBE_CHARACTERISTIC, characteristicId, cVar);
        wd4.b(characteristicId, "characteristicId");
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.n = z;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return xa0.a(super.a(z), JSONKey.ENABLE, Boolean.valueOf(this.n));
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        if (gattOperationResult instanceof w10) {
            w10 w10 = (w10) gattOperationResult;
            return w10.b() == i() && w10.d() == GattDescriptor.DescriptorId.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().l();
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        byte[] bArr;
        wd4.b(peripheral, "peripheral");
        if (peripheral.a(i(), this.n)) {
            GattCharacteristicProperty[] a = peripheral.a(i());
            boolean z = false;
            if (true == (!this.n)) {
                bArr = GattDescriptor.d.a();
            } else if (true == lb4.b((T[]) a, GattCharacteristicProperty.PROPERTY_NOTIFY)) {
                bArr = GattDescriptor.d.c();
            } else {
                bArr = true == lb4.b((T[]) a, GattCharacteristicProperty.PROPERTY_INDICATE) ? GattDescriptor.d.b() : new byte[0];
            }
            this.m = bArr;
            if (this.m.length == 0) {
                z = true;
            }
            if (z) {
                peripheral.a(i(), !this.n);
                a(BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNSUPPORTED, (GattOperationResult.GattResult) null, 5, (Object) null));
                a();
                return;
            }
            peripheral.a(i(), GattDescriptor.DescriptorId.CLIENT_CHARACTERISTIC_CONFIGURATION, this.m);
            b(true);
            return;
        }
        a(BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT, (GattOperationResult.GattResult) null, 5, (Object) null));
        a();
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        BluetoothCommand.Result result;
        BluetoothCommand.Result.ResultCode resultCode;
        wd4.b(gattOperationResult, "gattOperationResult");
        b(false);
        if (gattOperationResult.a().getResultCode() == GattOperationResult.GattResult.ResultCode.SUCCESS) {
            if (Arrays.equals(this.m, ((w10) gattOperationResult).c())) {
                resultCode = BluetoothCommand.Result.ResultCode.SUCCESS;
            } else {
                resultCode = BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT;
            }
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, resultCode, gattOperationResult.a(), 1, (Object) null);
        } else {
            BluetoothCommand.Result a = BluetoothCommand.Result.Companion.a(gattOperationResult.a());
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, a.getResultCode(), a.getGattResult(), 1, (Object) null);
        }
        a(result);
    }
}
