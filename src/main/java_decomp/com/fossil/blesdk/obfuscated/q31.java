package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q31 implements Parcelable.Creator<p31> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                status = (Status) SafeParcelReader.a(parcel, a, Status.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new p31(status);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new p31[i];
    }
}
