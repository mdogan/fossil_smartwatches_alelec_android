package com.fossil.blesdk.obfuscated;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gn {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ List<qm> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ InputStream d;

    @DexIgnore
    public gn(int i, List<qm> list) {
        this(i, list, -1, (InputStream) null);
    }

    @DexIgnore
    public final InputStream a() {
        return this.d;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final List<qm> c() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public gn(int i, List<qm> list, int i2, InputStream inputStream) {
        this.a = i;
        this.b = list;
        this.c = i2;
        this.d = inputStream;
    }
}
