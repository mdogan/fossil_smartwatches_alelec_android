package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.enums.GoalType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vh3 extends v52 {
    @DexIgnore
    public abstract void a(int i, GoalType goalType);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
