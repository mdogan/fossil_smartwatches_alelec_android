package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b83 extends as2 implements a83, kt2, ls2 {
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ur3<va2> j;
    @DexIgnore
    public z73 k;
    @DexIgnore
    public et2 l;
    @DexIgnore
    public ActiveTimeOverviewFragment m;
    @DexIgnore
    public cu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final b83 a() {
            return new b83();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cu3 {
        @DexIgnore
        public /* final */ /* synthetic */ b83 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, b83 b83, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = b83;
        }

        @DexIgnore
        public void a(int i) {
            b83.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ z73 a(b83 b83) {
        z73 z73 = b83.k;
        if (z73 != null) {
            return z73;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardActiveTimeFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(Q0());
        sb.append(", isRunning=");
        wl2 Q0 = Q0();
        sb.append(Q0 != null ? Boolean.valueOf(Q0.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            wl2 Q02 = Q0();
            if (Q02 != null) {
                Q02.d();
            }
            if (isVisible() && this.j != null) {
                va2 T0 = T0();
                if (T0 != null) {
                    RecyclerView recyclerView = T0.q;
                    if (recyclerView != null) {
                        RecyclerView.ViewHolder c = recyclerView.c(0);
                        if (c != null) {
                            View view = c.itemView;
                            if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                return;
                            }
                        }
                        recyclerView.j(0);
                        cu3 cu3 = this.n;
                        if (cu3 != null) {
                            cu3.a();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        wl2 Q03 = Q0();
        if (Q03 != null) {
            Q03.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DashboardActiveTimeFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final va2 T0() {
        ur3<va2> ur3 = this.j;
        if (ur3 != null) {
            return ur3.a();
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onDayClicked - date=" + date);
        Context context = getContext();
        if (context != null) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.D;
            wd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        cu3 cu3 = this.n;
        if (cu3 != null) {
            cu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (va2) ra.a(layoutInflater, R.layout.fragment_dashboard_active_time, viewGroup, false, O0()));
        ur3<va2> ur3 = this.j;
        if (ur3 != null) {
            va2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimeFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        z73 z73 = this.k;
        if (z73 != null) {
            z73.i();
            super.onDestroyView();
            N0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        z73 z73 = this.k;
        if (z73 != null) {
            z73.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        z73 z73 = this.k;
        if (z73 != null) {
            z73.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m = (ActiveTimeOverviewFragment) getChildFragmentManager().a("ActiveTimeOverviewFragment");
        if (this.m == null) {
            this.m = new ActiveTimeOverviewFragment();
        }
        dt2 dt2 = new dt2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wd4.a((Object) childFragmentManager, "childFragmentManager");
        ActiveTimeOverviewFragment activeTimeOverviewFragment = this.m;
        if (activeTimeOverviewFragment != null) {
            this.l = new et2(dt2, c, this, childFragmentManager, activeTimeOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            va2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    et2 et2 = this.l;
                    if (et2 != null) {
                        recyclerView.setAdapter(et2);
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            this.n = new b(recyclerView, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                            cu3 cu3 = this.n;
                            if (cu3 != null) {
                                recyclerView.a((RecyclerView.q) cu3);
                                recyclerView.setItemViewCacheSize(0);
                                m73 m73 = new m73(linearLayoutManager.M());
                                Drawable c2 = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_dashboard_line_1w);
                                if (c2 != null) {
                                    wd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                    m73.a(c2);
                                    recyclerView.a((RecyclerView.l) m73);
                                    z73 z73 = this.k;
                                    if (z73 != null) {
                                        z73.h();
                                    } else {
                                        wd4.d("mPresenter");
                                        throw null;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                    } else {
                        wd4.d("mDashboardActiveTimeAdapter");
                        throw null;
                    }
                }
            }
            va2 T02 = T0();
            if (T02 != null) {
                RecyclerView recyclerView2 = T02.q;
                if (recyclerView2 != null) {
                    wd4.a((Object) recyclerView2, "recyclerView");
                    RecyclerView.j itemAnimator = recyclerView2.getItemAnimator();
                    if (itemAnimator instanceof bf) {
                        ((bf) itemAnimator).setSupportsChangeAnimations(false);
                    }
                }
            }
            R("active_minutes_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                jc a2 = mc.a(activity).a(ju3.class);
                wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                ju3 ju3 = (ju3) a2;
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(rd<ActivitySummary> rdVar) {
        et2 et2 = this.l;
        if (et2 != null) {
            et2.c(rdVar);
        } else {
            wd4.d("mDashboardActiveTimeAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(z73 z73) {
        wd4.b(z73, "presenter");
        this.k = z73;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wd4.b(date, "startWeekDate");
        wd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
