package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.android.AndroidDispatcherFactory;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk4 {
    @DexIgnore
    public static /* final */ boolean a; // = qk4.a("kotlinx.coroutines.fast.service.loader", true);
    @DexIgnore
    public static /* final */ bj4 b;

    /*
    static {
        hk4 hk4 = new hk4();
        b = hk4.a();
    }
    */

    @DexIgnore
    public final bj4 a() {
        List<S> list;
        T t;
        try {
            if (a) {
                Class<MainDispatcherFactory> cls = MainDispatcherFactory.class;
                bk4 bk4 = bk4.a;
                ClassLoader classLoader = cls.getClassLoader();
                wd4.a((Object) classLoader, "clz.classLoader");
                list = bk4.a(cls, classLoader);
            } else {
                Iterator it = Arrays.asList(new MainDispatcherFactory[]{new AndroidDispatcherFactory()}).iterator();
                wd4.a((Object) it, "ServiceLoader.load(\n    \u2026             ).iterator()");
                list = SequencesKt___SequencesKt.f(if4.a(it));
            }
            Iterator<T> it2 = list.iterator();
            if (!it2.hasNext()) {
                t = null;
            } else {
                t = it2.next();
                if (it2.hasNext()) {
                    int loadPriority = ((MainDispatcherFactory) t).getLoadPriority();
                    do {
                        T next = it2.next();
                        int loadPriority2 = ((MainDispatcherFactory) next).getLoadPriority();
                        if (loadPriority < loadPriority2) {
                            t = next;
                            loadPriority = loadPriority2;
                        }
                    } while (it2.hasNext());
                }
            }
            MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory) t;
            if (mainDispatcherFactory != null) {
                bj4 a2 = ik4.a(mainDispatcherFactory, list);
                if (a2 != null) {
                    return a2;
                }
            }
            return new jk4((Throwable) null, (String) null, 2, (rd4) null);
        } catch (Throwable th) {
            return new jk4(th, (String) null, 2, (rd4) null);
        }
    }
}
