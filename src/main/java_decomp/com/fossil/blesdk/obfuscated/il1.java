package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class il1 implements Callable<String> {
    @DexIgnore
    public /* final */ /* synthetic */ sl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ el1 f;

    @DexIgnore
    public il1(el1 el1, sl1 sl1) {
        this.f = el1;
        this.e = sl1;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        rl1 rl1;
        if (this.f.i().i(this.e.e)) {
            rl1 = this.f.d(this.e);
        } else {
            rl1 = this.f.l().b(this.e.e);
        }
        if (rl1 != null) {
            return rl1.a();
        }
        this.f.d().v().a("App info was null when attempting to get app instance id");
        return null;
    }
}
