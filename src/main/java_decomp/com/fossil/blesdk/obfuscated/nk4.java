package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nk4 {
    @DexIgnore
    public static final Throwable a(gg4<?> gg4, Throwable th) {
        wd4.b(gg4, "$this$tryRecover");
        wd4.b(th, "exception");
        if (!(gg4 instanceof mk4)) {
            gg4 = null;
        }
        mk4 mk4 = (mk4) gg4;
        if (mk4 != null) {
            kc4<T> kc4 = mk4.h;
            if (kc4 != null) {
                return ok4.a(th, (kc4<?>) kc4);
            }
        }
        return th;
    }
}
