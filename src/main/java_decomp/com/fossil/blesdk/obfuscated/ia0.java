package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ia0 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore
    public ia0(short s, byte b2, byte b3) {
        this.b = s;
        this.c = b2;
        this.d = b3;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.c).putShort(this.b).put(this.d).array();
        wd4.a((Object) array, "ByteBuffer.allocate(4)\n \u2026ber)\n            .array()");
        this.a = array;
    }

    @DexIgnore
    public final byte[] a() {
        return this.a;
    }
}
