package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.fossil.blesdk.log.generic.LogEntry;
import com.fossil.blesdk.log.generic.LogUploader;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class y90 extends HandlerThread {
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ x90 f;
    @DexIgnore
    public /* final */ LogUploader g;
    @DexIgnore
    public /* final */ cb4 h; // = cb4.a;
    @DexIgnore
    public /* final */ SharedPreferenceFileName i;
    @DexIgnore
    public boolean j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y90(String str, int i2, String str2, String str3, oa0 oa0, int i3, w90 w90, SharedPreferenceFileName sharedPreferenceFileName, boolean z) {
        super(y90.class.getSimpleName(), 10);
        SharedPreferenceFileName sharedPreferenceFileName2 = sharedPreferenceFileName;
        wd4.b(str, "logDir");
        wd4.b(str2, "logFileNamePrefix");
        wd4.b(str3, "logFileNameExtension");
        wd4.b(oa0, "endpoint");
        wd4.b(w90, "logFileParser");
        wd4.b(sharedPreferenceFileName2, "sharedPreferenceFileName");
        this.i = sharedPreferenceFileName2;
        this.j = z;
        start();
        this.e = new Handler(getLooper());
        int i4 = i2;
        this.f = new x90(str, i4, this.e, str2, str3);
        this.g = new LogUploader(this.f, oa0, w90, this.e, this.j);
        a(i3);
    }

    @DexIgnore
    public final boolean a(oa0 oa0) {
        wd4.b(oa0, "endpoint");
        return this.g.a(oa0);
    }

    @DexIgnore
    public boolean b(LogEntry logEntry) {
        wd4.b(logEntry, "logEntry");
        a(logEntry);
        return this.f.a(lb0.c.b(logEntry.toJSONString(0)));
    }

    @DexIgnore
    public final void start() {
        super.start();
    }

    @DexIgnore
    public final void a(int i2) {
        if (i2 > 0) {
            this.g.c(((long) i2) * ((long) 1000));
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(y90 y90, long j2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                j2 = 0;
            }
            y90.a(j2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startUploadLog");
    }

    @DexIgnore
    public final void a(long j2) {
        this.g.a(j2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b  */
    @SuppressLint({"ApplySharedPref"})
    public final long a() {
        Long l;
        long longValue;
        synchronized (this.h) {
            SharedPreferences a2 = za0.a(this.i);
            Long valueOf = a2 != null ? Long.valueOf(a2.getLong("log_line_number", ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD)) : null;
            if (valueOf != null) {
                if (valueOf.longValue() != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                    l = Long.valueOf(valueOf.longValue() + 1);
                    if (a2 != null) {
                        SharedPreferences.Editor edit = a2.edit();
                        if (edit != null) {
                            SharedPreferences.Editor putLong = edit.putLong("log_line_number", l.longValue());
                            if (putLong != null) {
                                putLong.commit();
                            }
                        }
                    }
                    longValue = l.longValue();
                }
            }
            l = 0L;
            if (a2 != null) {
            }
            longValue = l.longValue();
        }
        return longValue;
    }

    @DexIgnore
    public void a(LogEntry logEntry) {
        wd4.b(logEntry, "logEntry");
        logEntry.setLineNumber(a());
    }
}
