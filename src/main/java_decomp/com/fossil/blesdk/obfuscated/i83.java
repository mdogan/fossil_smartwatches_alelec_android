package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i83 implements Factory<ActiveTimeOverviewDayPresenter> {
    @DexIgnore
    public static ActiveTimeOverviewDayPresenter a(g83 g83, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new ActiveTimeOverviewDayPresenter(g83, summariesRepository, activitiesRepository, workoutSessionRepository);
    }
}
