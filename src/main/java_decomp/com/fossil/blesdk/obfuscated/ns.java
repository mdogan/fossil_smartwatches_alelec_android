package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ns implements oo<BitmapDrawable> {
    @DexIgnore
    public /* final */ kq a;
    @DexIgnore
    public /* final */ oo<Bitmap> b;

    @DexIgnore
    public ns(kq kqVar, oo<Bitmap> ooVar) {
        this.a = kqVar;
        this.b = ooVar;
    }

    @DexIgnore
    public boolean a(bq<BitmapDrawable> bqVar, File file, mo moVar) {
        return this.b.a(new qs(bqVar.get().getBitmap(), this.a), file, moVar);
    }

    @DexIgnore
    public EncodeStrategy a(mo moVar) {
        return this.b.a(moVar);
    }
}
