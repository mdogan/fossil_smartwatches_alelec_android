package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lf<T> extends xf {
    @DexIgnore
    public lf(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    @DexIgnore
    public abstract void bind(lg lgVar, T t);

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public final int handle(T t) {
        lg acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.n();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(Iterable<T> iterable) {
        lg acquire = acquire();
        int i = 0;
        try {
            for (T bind : iterable) {
                bind(acquire, bind);
                i += acquire.n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        lg acquire = acquire();
        try {
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                i += acquire.n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
