package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j84 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ AttributeSet c;
    @DexIgnore
    public /* final */ View d;
    @DexIgnore
    public /* final */ i84 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public j84(String str, Context context, AttributeSet attributeSet, View view, i84 i84) {
        wd4.b(str, "name");
        wd4.b(context, "context");
        wd4.b(i84, "fallbackViewCreator");
        this.a = str;
        this.b = context;
        this.c = attributeSet;
        this.d = view;
        this.e = i84;
    }

    @DexIgnore
    public final AttributeSet a() {
        return this.c;
    }

    @DexIgnore
    public final Context b() {
        return this.b;
    }

    @DexIgnore
    public final i84 c() {
        return this.e;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final View e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j84)) {
            return false;
        }
        j84 j84 = (j84) obj;
        return wd4.a((Object) this.a, (Object) j84.a) && wd4.a((Object) this.b, (Object) j84.b) && wd4.a((Object) this.c, (Object) j84.c) && wd4.a((Object) this.d, (Object) j84.d) && wd4.a((Object) this.e, (Object) j84.e);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Context context = this.b;
        int hashCode2 = (hashCode + (context != null ? context.hashCode() : 0)) * 31;
        AttributeSet attributeSet = this.c;
        int hashCode3 = (hashCode2 + (attributeSet != null ? attributeSet.hashCode() : 0)) * 31;
        View view = this.d;
        int hashCode4 = (hashCode3 + (view != null ? view.hashCode() : 0)) * 31;
        i84 i84 = this.e;
        if (i84 != null) {
            i = i84.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "InflateRequest(name=" + this.a + ", context=" + this.b + ", attrs=" + this.c + ", parent=" + this.d + ", fallbackViewCreator=" + this.e + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ j84(String str, Context context, AttributeSet attributeSet, View view, i84 i84, int i, rd4 rd4) {
        this(str, context, (i & 4) != 0 ? null : attributeSet, (i & 8) != 0 ? null : view, i84);
    }
}
