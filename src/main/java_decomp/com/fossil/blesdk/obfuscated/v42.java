package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v42 implements Factory<AuthApiGuestService> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public v42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static v42 a(o42 o42) {
        return new v42(o42);
    }

    @DexIgnore
    public static AuthApiGuestService b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static AuthApiGuestService c(o42 o42) {
        AuthApiGuestService d = o42.d();
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public AuthApiGuestService get() {
        return b(this.a);
    }
}
