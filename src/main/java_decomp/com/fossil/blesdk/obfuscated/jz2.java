package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jz2 {
    @DexIgnore
    public /* final */ hz2 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public jz2(hz2 hz2, int i, LoaderManager loaderManager) {
        wd4.b(hz2, "mView");
        wd4.b(loaderManager, "mLoaderManager");
        this.a = hz2;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final hz2 c() {
        return this.a;
    }
}
