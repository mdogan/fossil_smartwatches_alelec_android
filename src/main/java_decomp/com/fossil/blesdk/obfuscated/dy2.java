package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dy2 extends as2 implements cy2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public by2 j;
    @DexIgnore
    public ur3<je2> k;
    @DexIgnore
    public wx2 l;
    @DexIgnore
    public ly2 m;
    @DexIgnore
    public InactivityNudgeTimePresenter n;
    @DexIgnore
    public RemindTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return dy2.q;
        }

        @DexIgnore
        public final dy2 b() {
            return new dy2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public b(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public c(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public d(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public e(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public f(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public g(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            us3.a(view);
            wx2 a = this.e.l;
            if (a != null) {
                a.p(0);
            }
            wx2 a2 = this.e.l;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, wx2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public h(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            us3.a(view);
            wx2 a = this.e.l;
            if (a != null) {
                a.p(1);
            }
            wx2 a2 = this.e.l;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, wx2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public i(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            us3.a(view);
            ly2 c = this.e.m;
            if (c != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                c.show(childFragmentManager, ly2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dy2 e;

        @DexIgnore
        public j(dy2 dy2) {
            this.e = dy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dy2.b(this.e).l();
        }
    }

    /*
    static {
        String simpleName = dy2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ by2 b(dy2 dy2) {
        by2 by2 = dy2.j;
        if (by2 != null) {
            return by2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void C(boolean z) {
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.E;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void D(boolean z) {
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.C;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.q;
                wd4.a((Object) constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.q.getChildAt(i2);
                    wd4.a((Object) childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L(String str) {
        wd4.b(str, LogBuilder.KEY_TIME);
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        by2 by2 = this.j;
        if (by2 != null) {
            by2.l();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d(SpannableString spannableString) {
        wd4.b(spannableString, LogBuilder.KEY_TIME);
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(boolean z) {
        int i2;
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
                FlexibleButton flexibleButton2 = a2.s;
                if (flexibleButton2 != null) {
                    if (z) {
                        i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.colorPrimary);
                    } else {
                        i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.fossilCoolGray);
                    }
                    flexibleButton2.setBackgroundColor(i2);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        je2 je2 = (je2) ra.a(layoutInflater, R.layout.fragment_notification_watch_reminders, viewGroup, false, O0());
        this.l = (wx2) getChildFragmentManager().a(wx2.r.a());
        if (this.l == null) {
            this.l = wx2.r.b();
        }
        this.m = (ly2) getChildFragmentManager().a(ly2.r.a());
        if (this.m == null) {
            this.m = ly2.r.b();
        }
        FlexibleTextView flexibleTextView = je2.w;
        wd4.a((Object) flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setSelected(true);
        je2.x.setOnClickListener(new b(this));
        je2.C.setOnClickListener(new c(this));
        je2.D.setOnClickListener(new d(this));
        je2.E.setOnClickListener(new e(this));
        je2.B.setOnClickListener(new f(this));
        je2.z.setOnClickListener(new g(this));
        je2.y.setOnClickListener(new h(this));
        je2.A.setOnClickListener(new i(this));
        je2.s.setOnClickListener(new j(this));
        this.k = new ur3<>(this, je2);
        m42 g2 = PortfolioApp.W.c().g();
        wx2 wx2 = this.l;
        if (wx2 != null) {
            ly2 ly2 = this.m;
            if (ly2 != null) {
                g2.a(new py2(wx2, ly2)).a(this);
                R("reminder_view");
                wd4.a((Object) je2, "binding");
                return je2.d();
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        by2 by2 = this.j;
        if (by2 != null) {
            by2.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        by2 by2 = this.j;
        if (by2 != null) {
            by2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                wd4.a((Object) constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                FlexibleButton flexibleButton = a2.s;
                wd4.a((Object) flexibleButton, "fbSave");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void u(boolean z) {
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.B;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void w(boolean z) {
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.D;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(SpannableString spannableString) {
        wd4.b(spannableString, LogBuilder.KEY_TIME);
        ur3<je2> ur3 = this.k;
        if (ur3 != null) {
            je2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(by2 by2) {
        wd4.b(by2, "presenter");
        this.j = by2;
    }
}
