package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class av0<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, yu0> e;

    @DexIgnore
    public av0(Map.Entry<K, yu0> entry) {
        this.e = entry;
    }

    @DexIgnore
    public final yu0 a() {
        return this.e.getValue();
    }

    @DexIgnore
    public final K getKey() {
        return this.e.getKey();
    }

    @DexIgnore
    public final Object getValue() {
        if (this.e.getValue() == null) {
            return null;
        }
        yu0.c();
        throw null;
    }

    @DexIgnore
    public final Object setValue(Object obj) {
        if (obj instanceof tv0) {
            return this.e.getValue().b((tv0) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
