package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.UserManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vs0 {
    @DexIgnore
    public static volatile UserManager a;
    @DexIgnore
    public static volatile boolean b; // = (!a());

    @DexIgnore
    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public static boolean a(Context context) {
        return a() && !b(context);
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean b(Context context) {
        boolean z = b;
        if (!z) {
            UserManager userManager = a;
            if (userManager == null) {
                synchronized (vs0.class) {
                    userManager = a;
                    if (userManager == null) {
                        UserManager userManager2 = (UserManager) context.getSystemService(UserManager.class);
                        a = userManager2;
                        if (userManager2 == null) {
                            b = true;
                            return true;
                        }
                        userManager = userManager2;
                    }
                }
            }
            z = userManager.isUserUnlocked();
            b = z;
            if (z) {
                a = null;
            }
        }
        return z;
    }
}
