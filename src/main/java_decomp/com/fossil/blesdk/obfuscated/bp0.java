package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bp0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bp0> CREATOR; // = new up0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore
    public bp0(String str, String str2, String str3, int i2) {
        this(str, str2, str3, i2, 0);
    }

    @DexIgnore
    public final String H() {
        return this.e;
    }

    @DexIgnore
    public final String I() {
        return this.f;
    }

    @DexIgnore
    public final String J() {
        return String.format("%s:%s:%s", new Object[]{this.e, this.f, this.g});
    }

    @DexIgnore
    public final int K() {
        return this.h;
    }

    @DexIgnore
    public final String L() {
        return this.g;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bp0)) {
            return false;
        }
        bp0 bp0 = (bp0) obj;
        return ak0.a(this.e, bp0.e) && ak0.a(this.f, bp0.f) && ak0.a(this.g, bp0.g) && this.h == bp0.h && this.i == bp0.i;
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.e, this.f, this.g, Integer.valueOf(this.h));
    }

    @DexIgnore
    public final String toString() {
        return String.format("Device{%s:%s:%s}", new Object[]{J(), Integer.valueOf(this.h), Integer.valueOf(this.i)});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, H(), false);
        lk0.a(parcel, 2, I(), false);
        lk0.a(parcel, 4, L(), false);
        lk0.a(parcel, 5, K());
        lk0.a(parcel, 6, this.i);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public bp0(String str, String str2, String str3, int i2, int i3) {
        ck0.a(str);
        this.e = str;
        ck0.a(str2);
        this.f = str2;
        if (str3 != null) {
            this.g = str3;
            this.h = i2;
            this.i = i3;
            return;
        }
        throw new IllegalStateException("Device UID is null.");
    }
}
