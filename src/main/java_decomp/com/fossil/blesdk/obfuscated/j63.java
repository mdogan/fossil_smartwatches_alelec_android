package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j63 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        hybridCustomizeEditActivity.B = hybridCustomizeEditPresenter;
    }

    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, k42 k42) {
        hybridCustomizeEditActivity.C = k42;
    }
}
