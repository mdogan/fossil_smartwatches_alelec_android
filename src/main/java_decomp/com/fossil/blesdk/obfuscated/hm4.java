package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hm4 {
    @DexIgnore
    public static /* final */ hm4 a; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hm4 {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public hm4 a(vl4 vl4) {
            return hm4.this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        hm4 a(vl4 vl4);
    }

    @DexIgnore
    public static c a(hm4 hm4) {
        return new b();
    }

    @DexIgnore
    public void a(vl4 vl4) {
    }

    @DexIgnore
    public void a(vl4 vl4, long j) {
    }

    @DexIgnore
    public void a(vl4 vl4, jm4 jm4) {
    }

    @DexIgnore
    public void a(vl4 vl4, pm4 pm4) {
    }

    @DexIgnore
    public void a(vl4 vl4, zl4 zl4) {
    }

    @DexIgnore
    public void a(vl4 vl4, IOException iOException) {
    }

    @DexIgnore
    public void a(vl4 vl4, String str) {
    }

    @DexIgnore
    public void a(vl4 vl4, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void a(vl4 vl4, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void a(vl4 vl4, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol) {
    }

    @DexIgnore
    public void a(vl4 vl4, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol, IOException iOException) {
    }

    @DexIgnore
    public void a(vl4 vl4, Response response) {
    }

    @DexIgnore
    public void b(vl4 vl4) {
    }

    @DexIgnore
    public void b(vl4 vl4, long j) {
    }

    @DexIgnore
    public void b(vl4 vl4, zl4 zl4) {
    }

    @DexIgnore
    public void c(vl4 vl4) {
    }

    @DexIgnore
    public void d(vl4 vl4) {
    }

    @DexIgnore
    public void e(vl4 vl4) {
    }

    @DexIgnore
    public void f(vl4 vl4) {
    }

    @DexIgnore
    public void g(vl4 vl4) {
    }
}
