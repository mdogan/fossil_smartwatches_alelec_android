package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l73 implements Factory<k73> {
    @DexIgnore
    public static /* final */ l73 a; // = new l73();

    @DexIgnore
    public static l73 a() {
        return a;
    }

    @DexIgnore
    public static k73 b() {
        return new k73();
    }

    @DexIgnore
    public k73 get() {
        return b();
    }
}
