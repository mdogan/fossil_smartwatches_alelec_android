package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class py2 {
    @DexIgnore
    public /* final */ vx2 a;
    @DexIgnore
    public /* final */ ky2 b;

    @DexIgnore
    public py2(vx2 vx2, ky2 ky2) {
        wd4.b(vx2, "mInActivityNudgeTimeContractView");
        wd4.b(ky2, "mRemindTimeContractView");
        this.a = vx2;
        this.b = ky2;
    }

    @DexIgnore
    public final vx2 a() {
        return this.a;
    }

    @DexIgnore
    public final ky2 b() {
        return this.b;
    }
}
