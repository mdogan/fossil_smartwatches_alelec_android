package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rj {
    @DexIgnore
    public static /* final */ String a; // = ej.a("Schedulers");

    @DexIgnore
    public static void a(yi yiVar, WorkDatabase workDatabase, List<qj> list) {
        if (list != null && list.size() != 0) {
            jl d = workDatabase.d();
            workDatabase.beginTransaction();
            try {
                List<il> a2 = d.a(yiVar.e());
                if (a2 != null && a2.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (il ilVar : a2) {
                        d.a(ilVar.a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (a2 != null && a2.size() > 0) {
                    il[] ilVarArr = (il[]) a2.toArray(new il[0]);
                    for (qj a3 : list) {
                        a3.a(ilVarArr);
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static qj a(Context context, uj ujVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            fk fkVar = new fk(context, ujVar);
            sl.a(context, SystemJobService.class, true);
            ej.a().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return fkVar;
        }
        qj a2 = a(context);
        if (a2 != null) {
            return a2;
        }
        ck ckVar = new ck(context);
        sl.a(context, SystemAlarmService.class, true);
        ej.a().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
        return ckVar;
    }

    @DexIgnore
    public static qj a(Context context) {
        try {
            qj qjVar = (qj) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            ej.a().a(a, String.format("Created %s", new Object[]{"androidx.work.impl.background.gcm.GcmScheduler"}), new Throwable[0]);
            return qjVar;
        } catch (Throwable th) {
            ej.a().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
