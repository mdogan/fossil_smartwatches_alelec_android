package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ft implements no<Uri, Bitmap> {
    @DexIgnore
    public /* final */ pt a;
    @DexIgnore
    public /* final */ kq b;

    @DexIgnore
    public ft(pt ptVar, kq kqVar) {
        this.a = ptVar;
        this.b = kqVar;
    }

    @DexIgnore
    public boolean a(Uri uri, mo moVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @DexIgnore
    public bq<Bitmap> a(Uri uri, int i, int i2, mo moVar) {
        bq<Drawable> a2 = this.a.a(uri, i, i2, moVar);
        if (a2 == null) {
            return null;
        }
        return ys.a(this.b, a2.get(), i, i2);
    }
}
