package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j10 extends b10 {
    @DexIgnore
    public byte[] m; // = new byte[0];

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j10(GattCharacteristic.CharacteristicId characteristicId, Peripheral.c cVar) {
        super(BluetoothCommandId.READ_CHARACTERISTIC, characteristicId, cVar);
        wd4.b(characteristicId, "characteristicId");
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.b(i());
        b(true);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return (gattOperationResult instanceof y10) && ((y10) gattOperationResult).b() == i();
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        this.m = ((y10) gattOperationResult).c();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().g();
    }

    @DexIgnore
    public final byte[] j() {
        return this.m;
    }
}
