package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.ViewModelStore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nc {
    @DexIgnore
    ViewModelStore getViewModelStore();
}
