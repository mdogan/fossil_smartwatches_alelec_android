package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wk2 {
    @DexIgnore
    public static /* final */ wk2 a; // = new wk2();

    @DexIgnore
    public final boolean a(String str) {
        wd4.b(str, "path");
        try {
            return new File(str).exists();
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public final String b(String str) {
        wd4.b(str, "nameFile");
        try {
            File filesDir = PortfolioApp.W.c().getFilesDir();
            String encodeToString = Base64.encodeToString(op4.a(new FileInputStream(new File(filesDir + File.separator + str))), 0);
            wd4.a((Object) encodeToString, "Base64.encodeToString(IO\u2026tStream), Base64.DEFAULT)");
            return encodeToString;
        } catch (Exception unused) {
            return "";
        }
    }

    @DexIgnore
    public final Drawable c(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable b = sm2.b().b(str);
        if (b != null) {
            return b;
        }
        try {
            Bitmap a2 = a(str, i, i2);
            if (a2 == null) {
                return null;
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.W.c().getResources(), Bitmap.createScaledBitmap(a2, i, i2, false));
            sm2.b().a(str, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0041 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0046 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004d A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    public final boolean a(qm4 qm4, String str) {
        InputStream inputStream;
        wd4.b(qm4, "body");
        wd4.b(str, "path");
        FileOutputStream fileOutputStream = null;
        try {
            byte[] bArr = new byte[4096];
            inputStream = qm4.y();
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(str);
                if (inputStream != null) {
                    while (true) {
                        try {
                            int read = inputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream2.write(bArr, 0, read);
                        } catch (IOException unused) {
                            fileOutputStream = fileOutputStream2;
                            if (inputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream2;
                            if (inputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            throw th;
                        }
                    }
                }
                fileOutputStream2.flush();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused2) {
                        return false;
                    }
                }
                fileOutputStream2.close();
                return true;
            } catch (IOException unused3) {
                if (inputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                throw th;
            }
        } catch (IOException unused4) {
            inputStream = null;
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final Drawable b(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable b = sm2.b().b(str);
        if (b != null) {
            return b;
        }
        try {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.W.c().getResources(), a(str, i, i2));
            sm2.b().a(str, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Bitmap a(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        File filesDir = PortfolioApp.W.c().getFilesDir();
        try {
            return bl2.a((InputStream) new FileInputStream(new File(filesDir + File.separator + wm2.a(str))), i, i2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
