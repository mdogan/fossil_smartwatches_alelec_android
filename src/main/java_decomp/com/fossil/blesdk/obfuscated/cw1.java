package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ak0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cw1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public cw1(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        ck0.b(!sm0.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static cw1 a(Context context) {
        ik0 ik0 = new ik0(context);
        String a2 = ik0.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new cw1(a2, ik0.a("google_api_key"), ik0.a("firebase_database_url"), ik0.a("ga_trackingId"), ik0.a("gcm_defaultSenderId"), ik0.a("google_storage_bucket"), ik0.a("project_id"));
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof cw1)) {
            return false;
        }
        cw1 cw1 = (cw1) obj;
        if (!ak0.a(this.b, cw1.b) || !ak0.a(this.a, cw1.a) || !ak0.a(this.c, cw1.c) || !ak0.a(this.d, cw1.d) || !ak0.a(this.e, cw1.e) || !ak0.a(this.f, cw1.f) || !ak0.a(this.g, cw1.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public final String toString() {
        ak0.a a2 = ak0.a((Object) this);
        a2.a("applicationId", this.b);
        a2.a("apiKey", this.a);
        a2.a("databaseUrl", this.c);
        a2.a("gcmSenderId", this.e);
        a2.a("storageBucket", this.f);
        a2.a("projectId", this.g);
        return a2.toString();
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
