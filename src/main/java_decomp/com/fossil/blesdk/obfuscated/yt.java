package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yt implements po<vt> {
    @DexIgnore
    public /* final */ po<Bitmap> b;

    @DexIgnore
    public yt(po<Bitmap> poVar) {
        uw.a(poVar);
        this.b = poVar;
    }

    @DexIgnore
    public bq<vt> a(Context context, bq<vt> bqVar, int i, int i2) {
        vt vtVar = bqVar.get();
        qs qsVar = new qs(vtVar.e(), sn.a(context).c());
        bq<Bitmap> a = this.b.a(context, qsVar, i, i2);
        if (!qsVar.equals(a)) {
            qsVar.a();
        }
        vtVar.a(this.b, a.get());
        return bqVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof yt) {
            return this.b.equals(((yt) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
