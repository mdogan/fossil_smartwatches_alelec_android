package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.lj0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oi0 implements ch0 {
    @DexIgnore
    public /* final */ Map<ee0.c<?>, ni0<?>> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<ee0.c<?>, ni0<?>> f; // = new HashMap();
    @DexIgnore
    public /* final */ Map<ee0<?>, Boolean> g;
    @DexIgnore
    public /* final */ we0 h;
    @DexIgnore
    public /* final */ fg0 i;
    @DexIgnore
    public /* final */ Lock j;
    @DexIgnore
    public /* final */ Looper k;
    @DexIgnore
    public /* final */ zd0 l;
    @DexIgnore
    public /* final */ Condition m;
    @DexIgnore
    public /* final */ lj0 n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ Queue<ue0<?, ?>> q; // = new LinkedList();
    @DexIgnore
    public boolean r;
    @DexIgnore
    public Map<zh0<?>, vd0> s;
    @DexIgnore
    public Map<zh0<?>, vd0> t;
    @DexIgnore
    public jf0 u;
    @DexIgnore
    public vd0 v;

    @DexIgnore
    public oi0(Context context, Lock lock, Looper looper, zd0 zd0, Map<ee0.c<?>, ee0.f> map, lj0 lj0, Map<ee0<?>, Boolean> map2, ee0.a<? extends mn1, wm1> aVar, ArrayList<hi0> arrayList, fg0 fg0, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.j = lock;
        this.k = looper;
        this.m = lock.newCondition();
        this.l = zd0;
        this.i = fg0;
        this.g = map2;
        this.n = lj0;
        this.o = z;
        HashMap hashMap = new HashMap();
        for (ee0 next : map2.keySet()) {
            hashMap.put(next.a(), next);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            hi0 hi0 = arrayList.get(i2);
            i2++;
            hi0 hi02 = hi0;
            hashMap2.put(hi02.e, hi02);
        }
        boolean z5 = true;
        boolean z6 = false;
        boolean z7 = true;
        boolean z8 = false;
        for (Map.Entry next2 : map.entrySet()) {
            ee0 ee0 = (ee0) hashMap.get(next2.getKey());
            ee0.f fVar = (ee0.f) next2.getValue();
            if (fVar.h()) {
                z3 = z7;
                z4 = !this.g.get(ee0).booleanValue() ? true : z8;
                z2 = true;
            } else {
                z2 = z6;
                z4 = z8;
                z3 = false;
            }
            ni0 ni0 = r1;
            ni0 ni02 = new ni0(context, ee0, looper, fVar, (hi0) hashMap2.get(ee0), lj0, aVar);
            this.e.put((ee0.c) next2.getKey(), ni0);
            if (fVar.l()) {
                this.f.put((ee0.c) next2.getKey(), ni0);
            }
            z8 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.p = (!z6 || z7 || z8) ? false : z5;
        this.h = we0.e();
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t2) {
        ee0.c i2 = t2.i();
        if (this.o && c(t2)) {
            return t2;
        }
        this.i.y.a(t2);
        this.e.get(i2).c(t2);
        return t2;
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t2) {
        if (this.o && c(t2)) {
            return t2;
        }
        if (!c()) {
            this.q.add(t2);
            return t2;
        }
        this.i.y.a(t2);
        this.e.get(t2.i()).b(t2);
        return t2;
    }

    @DexIgnore
    public final <T extends ue0<? extends ne0, ? extends ee0.b>> boolean c(T t2) {
        ee0.c i2 = t2.i();
        vd0 a = a((ee0.c<?>) i2);
        if (a == null || a.H() != 4) {
            return false;
        }
        t2.c(new Status(4, (String) null, this.h.a((zh0<?>) this.e.get(i2).h(), System.identityHashCode(this.i))));
        return true;
    }

    @DexIgnore
    public final void d() {
    }

    @DexIgnore
    public final void e() {
        this.j.lock();
        try {
            this.h.a();
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            if (this.t == null) {
                this.t = new g4(this.f.size());
            }
            vd0 vd0 = new vd0(4);
            for (ni0<?> h2 : this.f.values()) {
                this.t.put(h2.h(), vd0);
            }
            if (this.s != null) {
                this.s.putAll(this.t);
            }
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final vd0 f() {
        b();
        while (g()) {
            try {
                this.m.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new vd0(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return vd0.i;
        }
        vd0 vd0 = this.v;
        if (vd0 != null) {
            return vd0;
        }
        return new vd0(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        this.j.lock();
        try {
            return this.s == null && this.r;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final boolean h() {
        this.j.lock();
        try {
            if (this.r) {
                if (this.o) {
                    for (ee0.c<?> a : this.f.keySet()) {
                        vd0 a2 = a(a);
                        if (a2 != null) {
                            if (!a2.L()) {
                            }
                        }
                        this.j.unlock();
                        return false;
                    }
                    this.j.unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        lj0 lj0 = this.n;
        if (lj0 == null) {
            this.i.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(lj0.i());
        Map<ee0<?>, lj0.b> f2 = this.n.f();
        for (ee0 next : f2.keySet()) {
            vd0 a = a((ee0<?>) next);
            if (a != null && a.L()) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        this.i.q = hashSet;
    }

    @DexIgnore
    public final void j() {
        while (!this.q.isEmpty()) {
            a(this.q.remove());
        }
        this.i.a((Bundle) null);
    }

    @DexIgnore
    public final vd0 k() {
        vd0 vd0 = null;
        vd0 vd02 = null;
        int i2 = 0;
        int i3 = 0;
        for (ni0 next : this.e.values()) {
            ee0 c = next.c();
            vd0 vd03 = this.s.get(next.h());
            if (!vd03.L() && (!this.g.get(c).booleanValue() || vd03.K() || this.l.c(vd03.H()))) {
                if (vd03.H() != 4 || !this.o) {
                    int a = c.c().a();
                    if (vd0 == null || i2 > a) {
                        vd0 = vd03;
                        i2 = a;
                    }
                } else {
                    int a2 = c.c().a();
                    if (vd02 == null || i3 > a2) {
                        vd02 = vd03;
                        i3 = a2;
                    }
                }
            }
        }
        return (vd0 == null || vd02 == null || i2 <= i3) ? vd0 : vd02;
    }

    @DexIgnore
    public final void a() {
        this.j.lock();
        try {
            this.r = false;
            this.s = null;
            this.t = null;
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            this.v = null;
            while (!this.q.isEmpty()) {
                ue0 remove = this.q.remove();
                remove.a((th0) null);
                remove.a();
            }
            this.m.signalAll();
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final void b() {
        this.j.lock();
        try {
            if (!this.r) {
                this.r = true;
                this.s = null;
                this.t = null;
                this.u = null;
                this.v = null;
                this.h.c();
                this.h.a((Iterable<? extends ge0<?>>) this.e.values()).a((Executor) new um0(this.k), new qi0(this));
                this.j.unlock();
            }
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final boolean c() {
        this.j.lock();
        try {
            return this.s != null && this.v == null;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final vd0 a(ee0<?> ee0) {
        return a(ee0.a());
    }

    @DexIgnore
    public final vd0 a(ee0.c<?> cVar) {
        this.j.lock();
        try {
            ni0 ni0 = this.e.get(cVar);
            if (this.s != null && ni0 != null) {
                return this.s.get(ni0.h());
            }
            this.j.unlock();
            return null;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean a(df0 df0) {
        this.j.lock();
        try {
            if (!this.r || h()) {
                this.j.unlock();
                return false;
            }
            this.h.c();
            this.u = new jf0(this, df0);
            this.h.a((Iterable<? extends ge0<?>>) this.f.values()).a((Executor) new um0(this.k), this.u);
            this.j.unlock();
            return true;
        } catch (Throwable th) {
            this.j.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(ni0<?> ni0, vd0 vd0) {
        return !vd0.L() && !vd0.K() && this.g.get(ni0.c()).booleanValue() && ni0.i().h() && this.l.c(vd0.H());
    }
}
