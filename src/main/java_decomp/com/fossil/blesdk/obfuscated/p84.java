package com.fossil.blesdk.obfuscated;

import android.util.Log;
import io.github.inflationx.calligraphy3.ReflectionUtils;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p84 {
    @DexIgnore
    public static final void a(Field field, Object obj, Object obj2) {
        wd4.b(field, "receiver$Anon0");
        wd4.b(obj, "obj");
        wd4.b(obj2, "value");
        try {
            field.set(obj, obj2);
        } catch (IllegalAccessException unused) {
        }
    }

    @DexIgnore
    public static final Method a(Class<?> cls, String str) {
        wd4.b(cls, "receiver$Anon0");
        wd4.b(str, "methodName");
        for (Method method : cls.getMethods()) {
            wd4.a((Object) method, "method");
            if (wd4.a((Object) method.getName(), (Object) str)) {
                method.setAccessible(true);
                return method;
            }
        }
        return null;
    }

    @DexIgnore
    public static final void a(Method method, Object obj, Object... objArr) {
        wd4.b(obj, "target");
        wd4.b(objArr, "args");
        if (method != null) {
            try {
                method.invoke(obj, Arrays.copyOf(objArr, objArr.length));
            } catch (IllegalAccessException e) {
                Log.d(ReflectionUtils.TAG, "Can't access method using reflection", e);
            } catch (InvocationTargetException e2) {
                Log.d(ReflectionUtils.TAG, "Can't invoke method using reflection", e2);
            }
        }
    }
}
