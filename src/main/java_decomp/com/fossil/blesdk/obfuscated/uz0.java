package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uz0 extends l21<y01> {
    @DexIgnore
    public static /* final */ ee0.g<uz0> E; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0<Object> F; // = new ee0<>("Fitness.API", new wz0(), E);

    /*
    static {
        new ee0("Fitness.CLIENT", new yz0(), E);
    }
    */

    @DexIgnore
    public uz0(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 57, bVar, cVar, lj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
        if (queryLocalInterface instanceof y01) {
            return (y01) queryLocalInterface;
        }
        return new a11(iBinder);
    }

    @DexIgnore
    public final int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.HistoryApi";
    }
}
