package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.home.HomePresenter;
import com.portfolio.platform.util.UserUtils;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dv2 implements Factory<HomePresenter> {
    @DexIgnore
    public static HomePresenter a(su2 su2, fn2 fn2, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, k62 k62, wj2 wj2, rr2 rr2, UserUtils userUtils, ServerSettingRepository serverSettingRepository, lr2 lr2, bk3 bk3) {
        return new HomePresenter(su2, fn2, deviceRepository, portfolioApp, updateFirmwareUsecase, k62, wj2, rr2, userUtils, serverSettingRepository, lr2, bk3);
    }
}
