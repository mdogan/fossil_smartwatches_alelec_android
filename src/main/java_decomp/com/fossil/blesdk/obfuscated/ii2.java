package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ii2 extends hi2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.iv_hybrid_icon, 1);
        x.put(R.id.ftv_hybrid_name, 2);
        x.put(R.id.ftv_hybrid_feature, 3);
        x.put(R.id.iv_remove, 4);
    }
    */

    @DexIgnore
    public ii2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 5, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public ii2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[0], objArr[3], objArr[2], objArr[1], objArr[4]);
        this.v = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
