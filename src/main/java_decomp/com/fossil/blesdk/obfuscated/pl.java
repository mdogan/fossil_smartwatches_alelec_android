package com.fossil.blesdk.obfuscated;

import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import com.fossil.blesdk.obfuscated.gj;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pl implements Runnable {
    @DexIgnore
    public /* final */ oj e; // = new oj();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pl {
        @DexIgnore
        public /* final */ /* synthetic */ uj f;
        @DexIgnore
        public /* final */ /* synthetic */ String g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        public a(uj ujVar, String str, boolean z) {
            this.f = ujVar;
            this.g = str;
            this.h = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public void a() {
            WorkDatabase g2 = this.f.g();
            g2.beginTransaction();
            try {
                for (String a : g2.d().c(this.g)) {
                    a(this.f, a);
                }
                g2.setTransactionSuccessful();
                g2.endTransaction();
                if (this.h) {
                    a(this.f);
                }
            } catch (Throwable th) {
                g2.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void a(uj ujVar, String str) {
        a(ujVar.g(), str);
        ujVar.e().d(str);
        for (qj a2 : ujVar.f()) {
            a2.a(str);
        }
    }

    @DexIgnore
    public void run() {
        try {
            a();
            this.e.a(gj.a);
        } catch (Throwable th) {
            this.e.a(new gj.b.a(th));
        }
    }

    @DexIgnore
    public void a(uj ujVar) {
        rj.a(ujVar.c(), ujVar.g(), ujVar.f());
    }

    @DexIgnore
    public final void a(WorkDatabase workDatabase, String str) {
        jl d = workDatabase.d();
        al a2 = workDatabase.a();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            WorkInfo.State d2 = d.d(str2);
            if (!(d2 == WorkInfo.State.SUCCEEDED || d2 == WorkInfo.State.FAILED)) {
                d.a(WorkInfo.State.CANCELLED, str2);
            }
            linkedList.addAll(a2.a(str2));
        }
    }

    @DexIgnore
    public static pl a(String str, uj ujVar, boolean z) {
        return new a(ujVar, str, z);
    }
}
