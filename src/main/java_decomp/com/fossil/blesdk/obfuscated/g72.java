package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g72 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new b().getType();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends Long>> {
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public final String a(List<Long> list) {
        wd4.b(list, "activeTimes");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = this.a.a((Object) list, this.b);
            wd4.a((Object) a2, "mGson.toJson(activeTimes, mType)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("GFitActiveTimeConverter", sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final List<Long> a(String str) {
        wd4.b(str, "data");
        if (str.length() == 0) {
            return ob4.a();
        }
        try {
            Object a2 = this.a.a(str, this.b);
            wd4.a(a2, "mGson.fromJson(data, mType)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListLong: ");
            e.printStackTrace();
            sb.append(cb4.a);
            local.e("GFitActiveTimeConverter", sb.toString());
            return ob4.a();
        }
    }
}
