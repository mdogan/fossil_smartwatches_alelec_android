package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tg4 extends si4<ri4> {
    @DexIgnore
    public /* final */ qg4<?> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tg4(ri4 ri4, qg4<?> qg4) {
        super(ri4);
        wd4.b(ri4, "parent");
        wd4.b(qg4, "child");
        this.i = qg4;
    }

    @DexIgnore
    public void b(Throwable th) {
        qg4<?> qg4 = this.i;
        qg4.a(qg4.a((ri4) this.h));
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildContinuation[" + this.i + ']';
    }
}
