package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.nio.charset.Charset;
import java.util.UUID;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class va0 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ UUID b; // = UUID.fromString("00001805-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID c; // = UUID.fromString("00002a2b-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID d; // = UUID.fromString("00002a0f-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID e; // = UUID.fromString("3dda0001-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ UUID g; // = UUID.fromString("3dda0002-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID h; // = UUID.fromString("3dda0003-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID i; // = UUID.fromString("3dda0004-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID j; // = UUID.fromString("3dda0005-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID k; // = UUID.fromString("3dda0006-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID l; // = UUID.fromString("3dda0007-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID m; // = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID n; // = UUID.fromString("00002a25-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID o; // = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID p; // = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID q; // = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ Charset r; // = nf4.a;
    @DexIgnore
    public static /* final */ AuthenticationKeyType s; // = AuthenticationKeyType.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY;
    @DexIgnore
    public static /* final */ Version t; // = new Version((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ Version u; // = new Version((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ Version v; // = new Version((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ Version w; // = new Version((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ ConnectionParametersSet x; // = new ConnectionParametersSet(12, 12, 0, 600);
    @DexIgnore
    public static /* final */ va0 y; // = new va0();

    /*
    static {
        String property = System.getProperty("line.separator");
        if (property == null) {
            property = "\n";
        }
        a = property;
        UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
        String a2 = cg4.a("3dda0001-957f-7d4a-34a6-74696673696d", ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "", false, 4, (Object) null);
        if (a2 != null) {
            String upperCase = a2.toUpperCase();
            wd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            f = upperCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    */

    @DexIgnore
    public final UUID a() {
        return q;
    }

    @DexIgnore
    public final UUID b() {
        return c;
    }

    @DexIgnore
    public final UUID c() {
        return d;
    }

    @DexIgnore
    public final UUID d() {
        return b;
    }

    @DexIgnore
    public final AuthenticationKeyType e() {
        return s;
    }

    @DexIgnore
    public final Charset f() {
        return r;
    }

    @DexIgnore
    public final Version g() {
        return w;
    }

    @DexIgnore
    public final Version h() {
        return t;
    }

    @DexIgnore
    public final UUID i() {
        return o;
    }

    @DexIgnore
    public final UUID j() {
        return m;
    }

    @DexIgnore
    public final UUID k() {
        return n;
    }

    @DexIgnore
    public final UUID l() {
        return p;
    }

    @DexIgnore
    public final Version m() {
        return u;
    }

    @DexIgnore
    public final ConnectionParametersSet n() {
        return x;
    }

    @DexIgnore
    public final UUID o() {
        return k;
    }

    @DexIgnore
    public final UUID p() {
        return j;
    }

    @DexIgnore
    public final UUID q() {
        return g;
    }

    @DexIgnore
    public final UUID r() {
        return h;
    }

    @DexIgnore
    public final UUID s() {
        return i;
    }

    @DexIgnore
    public final UUID t() {
        return l;
    }

    @DexIgnore
    public final UUID u() {
        return e;
    }

    @DexIgnore
    public final String v() {
        return f;
    }

    @DexIgnore
    public final String w() {
        return a;
    }

    @DexIgnore
    public final Version x() {
        return v;
    }
}
