package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj1 extends qk1 {
    @DexIgnore
    public rj1 c;
    @DexIgnore
    public volatile rj1 d;
    @DexIgnore
    public rj1 e;
    @DexIgnore
    public /* final */ Map<Activity, rj1> f; // = new g4();
    @DexIgnore
    public String g;

    @DexIgnore
    public sj1(yh1 yh1) {
        super(yh1);
    }

    @DexIgnore
    public final rj1 A() {
        v();
        e();
        return this.c;
    }

    @DexIgnore
    public final rj1 B() {
        f();
        return this.d;
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        if (this.d == null) {
            d().v().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f.get(activity) == null) {
            d().v().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.d.b.equals(str2);
            boolean e2 = ol1.e(this.d.a, str);
            if (equals && e2) {
                d().x().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                d().v().a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                d().A().a("Setting current screen to name, class", str == null ? "null" : str, str2);
                rj1 rj1 = new rj1(str, str2, j().s());
                this.f.put(activity, rj1);
                a(activity, rj1, true);
            } else {
                d().v().a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @DexIgnore
    public final void b(Activity activity) {
        rj1 d2 = d(activity);
        this.e = this.d;
        this.d = null;
        a().a((Runnable) new vj1(this, d2));
    }

    @DexIgnore
    public final void c(Activity activity) {
        a(activity, d(activity), false);
        bg1 n = n();
        n.a().a((Runnable) new dj1(n, n.c().c()));
    }

    @DexIgnore
    public final rj1 d(Activity activity) {
        ck0.a(activity);
        rj1 rj1 = this.f.get(activity);
        if (rj1 != null) {
            return rj1;
        }
        rj1 rj12 = new rj1((String) null, a(activity.getClass().getCanonicalName()), j().s());
        this.f.put(activity, rj12);
        return rj12;
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final void b(Activity activity, Bundle bundle) {
        if (bundle != null) {
            rj1 rj1 = this.f.get(activity);
            if (rj1 != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putLong("id", rj1.c);
                bundle2.putString("name", rj1.a);
                bundle2.putString("referrer_name", rj1.b);
                bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
            }
        }
    }

    @DexIgnore
    public final void a(Activity activity, rj1 rj1, boolean z) {
        rj1 rj12 = this.d == null ? this.e : this.d;
        if (rj1.b == null) {
            rj1 = new rj1(rj1.a, a(activity.getClass().getCanonicalName()), rj1.c);
        }
        this.e = this.d;
        this.d = rj1;
        a().a((Runnable) new tj1(this, z, rj12, rj1));
    }

    @DexIgnore
    public final void a(rj1 rj1, boolean z) {
        n().a(c().c());
        if (t().a(rj1.d, z)) {
            rj1.d = false;
        }
    }

    @DexIgnore
    public static void a(rj1 rj1, Bundle bundle, boolean z) {
        if (bundle != null && rj1 != null && (!bundle.containsKey("_sc") || z)) {
            String str = rj1.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", rj1.b);
            bundle.putLong("_si", rj1.c);
        } else if (bundle != null && rj1 == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @DexIgnore
    public final void a(String str, rj1 rj1) {
        e();
        synchronized (this) {
            if (this.g == null || this.g.equals(str) || rj1 != null) {
                this.g = str;
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @DexIgnore
    public final void a(Activity activity, Bundle bundle) {
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle("com.google.app_measurement.screen_service");
            if (bundle2 != null) {
                this.f.put(activity, new rj1(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
            }
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        this.f.remove(activity);
    }
}
