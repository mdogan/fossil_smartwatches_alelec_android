package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h61 extends wb1<h61> {
    @DexIgnore
    public long[] c;
    @DexIgnore
    public long[] d;
    @DexIgnore
    public c61[] e; // = c61.e();
    @DexIgnore
    public i61[] f; // = i61.e();

    @DexIgnore
    public h61() {
        long[] jArr = ec1.b;
        this.c = jArr;
        this.d = jArr;
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        long[] jArr = this.c;
        int i = 0;
        if (jArr != null && jArr.length > 0) {
            int i2 = 0;
            while (true) {
                long[] jArr2 = this.c;
                if (i2 >= jArr2.length) {
                    break;
                }
                vb1.a(1, jArr2[i2]);
                i2++;
            }
        }
        long[] jArr3 = this.d;
        if (jArr3 != null && jArr3.length > 0) {
            int i3 = 0;
            while (true) {
                long[] jArr4 = this.d;
                if (i3 >= jArr4.length) {
                    break;
                }
                vb1.a(2, jArr4[i3]);
                i3++;
            }
        }
        c61[] c61Arr = this.e;
        if (c61Arr != null && c61Arr.length > 0) {
            int i4 = 0;
            while (true) {
                c61[] c61Arr2 = this.e;
                if (i4 >= c61Arr2.length) {
                    break;
                }
                c61 c61 = c61Arr2[i4];
                if (c61 != null) {
                    vb1.a(3, (bc1) c61);
                }
                i4++;
            }
        }
        i61[] i61Arr = this.f;
        if (i61Arr != null && i61Arr.length > 0) {
            while (true) {
                i61[] i61Arr2 = this.f;
                if (i >= i61Arr2.length) {
                    break;
                }
                i61 i61 = i61Arr2[i];
                if (i61 != null) {
                    vb1.a(4, (bc1) i61);
                }
                i++;
            }
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h61)) {
            return false;
        }
        h61 h61 = (h61) obj;
        if (!ac1.a(this.c, h61.c) || !ac1.a(this.d, h61.d) || !ac1.a((Object[]) this.e, (Object[]) h61.e) || !ac1.a((Object[]) this.f, (Object[]) h61.f)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(h61.b);
        }
        yb1 yb12 = h61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((((((((h61.class.getName().hashCode() + 527) * 31) + ac1.a(this.c)) * 31) + ac1.a(this.d)) * 31) + ac1.a((Object[]) this.e)) * 31) + ac1.a((Object[]) this.f)) * 31;
        yb1 yb1 = this.b;
        return hashCode + ((yb1 == null || yb1.a()) ? 0 : this.b.hashCode());
    }

    @DexIgnore
    public final int a() {
        long[] jArr;
        long[] jArr2;
        int a = super.a();
        long[] jArr3 = this.c;
        int i = 0;
        if (jArr3 != null && jArr3.length > 0) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                jArr2 = this.c;
                if (i2 >= jArr2.length) {
                    break;
                }
                i3 += vb1.b(jArr2[i2]);
                i2++;
            }
            a = a + i3 + (jArr2.length * 1);
        }
        long[] jArr4 = this.d;
        if (jArr4 != null && jArr4.length > 0) {
            int i4 = 0;
            int i5 = 0;
            while (true) {
                jArr = this.d;
                if (i4 >= jArr.length) {
                    break;
                }
                i5 += vb1.b(jArr[i4]);
                i4++;
            }
            a = a + i5 + (jArr.length * 1);
        }
        c61[] c61Arr = this.e;
        if (c61Arr != null && c61Arr.length > 0) {
            int i6 = a;
            int i7 = 0;
            while (true) {
                c61[] c61Arr2 = this.e;
                if (i7 >= c61Arr2.length) {
                    break;
                }
                c61 c61 = c61Arr2[i7];
                if (c61 != null) {
                    i6 += vb1.b(3, (bc1) c61);
                }
                i7++;
            }
            a = i6;
        }
        i61[] i61Arr = this.f;
        if (i61Arr != null && i61Arr.length > 0) {
            while (true) {
                i61[] i61Arr2 = this.f;
                if (i >= i61Arr2.length) {
                    break;
                }
                i61 i61 = i61Arr2[i];
                if (i61 != null) {
                    a += vb1.b(4, (bc1) i61);
                }
                i++;
            }
        }
        return a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                int a = ec1.a(ub1, 8);
                long[] jArr = this.c;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = ub1.f();
                    ub1.c();
                    length++;
                }
                jArr2[length] = ub1.f();
                this.c = jArr2;
            } else if (c2 == 10) {
                int c3 = ub1.c(ub1.e());
                int a2 = ub1.a();
                int i = 0;
                while (ub1.l() > 0) {
                    ub1.f();
                    i++;
                }
                ub1.f(a2);
                long[] jArr3 = this.c;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.c, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = ub1.f();
                    length2++;
                }
                this.c = jArr4;
                ub1.d(c3);
            } else if (c2 == 16) {
                int a3 = ec1.a(ub1, 16);
                long[] jArr5 = this.d;
                int length3 = jArr5 == null ? 0 : jArr5.length;
                long[] jArr6 = new long[(a3 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.d, 0, jArr6, 0, length3);
                }
                while (length3 < jArr6.length - 1) {
                    jArr6[length3] = ub1.f();
                    ub1.c();
                    length3++;
                }
                jArr6[length3] = ub1.f();
                this.d = jArr6;
            } else if (c2 == 18) {
                int c4 = ub1.c(ub1.e());
                int a4 = ub1.a();
                int i2 = 0;
                while (ub1.l() > 0) {
                    ub1.f();
                    i2++;
                }
                ub1.f(a4);
                long[] jArr7 = this.d;
                int length4 = jArr7 == null ? 0 : jArr7.length;
                long[] jArr8 = new long[(i2 + length4)];
                if (length4 != 0) {
                    System.arraycopy(this.d, 0, jArr8, 0, length4);
                }
                while (length4 < jArr8.length) {
                    jArr8[length4] = ub1.f();
                    length4++;
                }
                this.d = jArr8;
                ub1.d(c4);
            } else if (c2 == 26) {
                int a5 = ec1.a(ub1, 26);
                c61[] c61Arr = this.e;
                int length5 = c61Arr == null ? 0 : c61Arr.length;
                c61[] c61Arr2 = new c61[(a5 + length5)];
                if (length5 != 0) {
                    System.arraycopy(this.e, 0, c61Arr2, 0, length5);
                }
                while (length5 < c61Arr2.length - 1) {
                    c61Arr2[length5] = new c61();
                    ub1.a((bc1) c61Arr2[length5]);
                    ub1.c();
                    length5++;
                }
                c61Arr2[length5] = new c61();
                ub1.a((bc1) c61Arr2[length5]);
                this.e = c61Arr2;
            } else if (c2 == 34) {
                int a6 = ec1.a(ub1, 34);
                i61[] i61Arr = this.f;
                int length6 = i61Arr == null ? 0 : i61Arr.length;
                i61[] i61Arr2 = new i61[(a6 + length6)];
                if (length6 != 0) {
                    System.arraycopy(this.f, 0, i61Arr2, 0, length6);
                }
                while (length6 < i61Arr2.length - 1) {
                    i61Arr2[length6] = new i61();
                    ub1.a((bc1) i61Arr2[length6]);
                    ub1.c();
                    length6++;
                }
                i61Arr2[length6] = new i61();
                ub1.a((bc1) i61Arr2[length6]);
                this.f = i61Arr2;
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
