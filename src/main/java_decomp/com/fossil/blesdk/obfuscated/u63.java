package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class u63 extends v52 {
    @DexIgnore
    public abstract void a(Category category);

    @DexIgnore
    public abstract void a(HybridCustomizeViewModel hybridCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();
}
