package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g41 extends s41 {
    @DexIgnore
    public /* final */ z31 G;

    @DexIgnore
    public g41(Context context, Looper looper, he0.b bVar, he0.c cVar, String str, lj0 lj0) {
        super(context, looper, bVar, cVar, str, lj0);
        this.G = new z31(context, this.F);
    }

    @DexIgnore
    public final Location G() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.c();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final void a(af0.a<sc1> aVar, s31 s31) throws RemoteException {
        this.G.a(aVar, s31);
    }

    @DexIgnore
    public final void a(j41 j41, af0<rc1> af0, s31 s31) throws RemoteException {
        synchronized (this.G) {
            this.G.a(j41, af0, s31);
        }
    }

    @DexIgnore
    public final void a(uc1 uc1, ve0<wc1> ve0, String str) throws RemoteException {
        p();
        boolean z = true;
        ck0.a(uc1 != null, (Object) "locationSettingsRequest can't be null nor empty.");
        if (ve0 == null) {
            z = false;
        }
        ck0.a(z, (Object) "listener can't be null.");
        ((v31) x()).a(uc1, new i41(ve0), str);
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, af0<sc1> af0, s31 s31) throws RemoteException {
        synchronized (this.G) {
            this.G.a(locationRequest, af0, s31);
        }
    }

    @DexIgnore
    public final void b(af0.a<rc1> aVar, s31 s31) throws RemoteException {
        this.G.b(aVar, s31);
    }
}
