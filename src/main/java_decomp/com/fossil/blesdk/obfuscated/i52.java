package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i52 implements Factory<g42> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public i52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static i52 a(o42 o42) {
        return new i52(o42);
    }

    @DexIgnore
    public static g42 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static g42 c(o42 o42) {
        g42 k = o42.k();
        o44.a(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    public g42 get() {
        return b(this.a);
    }
}
