package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rk extends mk<Boolean> {
    @DexIgnore
    public rk(Context context, am amVar) {
        super(yk.a(context, amVar).d());
    }

    @DexIgnore
    public boolean a(il ilVar) {
        return ilVar.j.i();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
