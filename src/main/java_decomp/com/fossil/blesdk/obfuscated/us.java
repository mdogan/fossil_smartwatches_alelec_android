package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class us extends rs {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.CenterCrop".getBytes(ko.a);

    @DexIgnore
    public Bitmap a(kq kqVar, Bitmap bitmap, int i, int i2) {
        return ht.a(kqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof us;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterCrop".hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
