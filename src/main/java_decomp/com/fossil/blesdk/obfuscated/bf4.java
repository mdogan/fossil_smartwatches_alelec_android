package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bf4<T> implements df4<T> {
    @DexIgnore
    public /* final */ df4<T> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ jd4<T, Boolean> c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, de4 {
        @DexIgnore
        public /* final */ Iterator<T> e;
        @DexIgnore
        public int f; // = -1;
        @DexIgnore
        public T g;
        @DexIgnore
        public /* final */ /* synthetic */ bf4 h;

        @DexIgnore
        public a(bf4 bf4) {
            this.h = bf4;
            this.e = bf4.a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.e.hasNext()) {
                T next = this.e.next();
                if (((Boolean) this.h.c.invoke(next)).booleanValue() == this.h.b) {
                    this.g = next;
                    this.f = 1;
                    return;
                }
            }
            this.f = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.f == -1) {
                a();
            }
            return this.f == 1;
        }

        @DexIgnore
        public T next() {
            if (this.f == -1) {
                a();
            }
            if (this.f != 0) {
                T t = this.g;
                this.g = null;
                this.f = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public bf4(df4<? extends T> df4, boolean z, jd4<? super T, Boolean> jd4) {
        wd4.b(df4, "sequence");
        wd4.b(jd4, "predicate");
        this.a = df4;
        this.b = z;
        this.c = jd4;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
