package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u50 extends SingleRequestPhase {
    @DexIgnore
    public int B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u50(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.READ_RSSI, new x60(peripheral));
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
    }

    @DexIgnore
    public void c(Request request) {
        wd4.b(request, "request");
        if (request instanceof x60) {
            this.B = ((x60) request).B();
        }
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(super.x(), JSONKey.RSSI, Integer.valueOf(this.B));
    }

    @DexIgnore
    public Integer i() {
        return Integer.valueOf(this.B);
    }
}
