package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class im4 extends RequestBody {
    @DexIgnore
    public static /* final */ mm4 c; // = mm4.a("application/x-www-form-urlencoded");
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public a() {
            this((Charset) null);
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(lm4.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(lm4.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(lm4.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(lm4.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a(Charset charset) {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public im4 a() {
            return new im4(this.a, this.b);
        }
    }

    @DexIgnore
    public im4(List<String> list, List<String> list2) {
        this.a = vm4.a(list);
        this.b = vm4.a(list2);
    }

    @DexIgnore
    public long a() {
        return a((wo4) null, true);
    }

    @DexIgnore
    public mm4 b() {
        return c;
    }

    @DexIgnore
    public void a(wo4 wo4) throws IOException {
        a(wo4, false);
    }

    @DexIgnore
    public final long a(wo4 wo4, boolean z) {
        vo4 vo4;
        if (z) {
            vo4 = new vo4();
        } else {
            vo4 = wo4.a();
        }
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                vo4.writeByte(38);
            }
            vo4.a(this.a.get(i));
            vo4.writeByte(61);
            vo4.a(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long B = vo4.B();
        vo4.w();
        return B;
    }
}
