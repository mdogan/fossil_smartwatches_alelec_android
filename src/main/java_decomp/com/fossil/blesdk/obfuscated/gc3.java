package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface gc3 extends w52<fc3> {
    @DexIgnore
    void a(int i, List<st3> list, List<Triple<Integer, Pair<Integer, Float>, String>> list2);

    @DexIgnore
    void a(boolean z, List<WorkoutSession> list);
}
