package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p42 implements Factory<AlarmHelper> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<fn2> c;
    @DexIgnore
    public /* final */ Provider<UserRepository> d;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> e;

    @DexIgnore
    public p42(o42 o42, Provider<Context> provider, Provider<fn2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
    }

    @DexIgnore
    public static p42 a(o42 o42, Provider<Context> provider, Provider<fn2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        return new p42(o42, provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static AlarmHelper b(o42 o42, Provider<Context> provider, Provider<fn2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        return a(o42, provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public static AlarmHelper a(o42 o42, Context context, fn2 fn2, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        AlarmHelper a2 = o42.a(context, fn2, userRepository, alarmsRepository);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AlarmHelper get() {
        return b(this.a, this.b, this.c, this.d, this.e);
    }
}
