package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l41 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<l41> CREATOR; // = new m41();
    @DexIgnore
    public int e;
    @DexIgnore
    public j41 f;
    @DexIgnore
    public xd1 g;
    @DexIgnore
    public PendingIntent h;
    @DexIgnore
    public ud1 i;
    @DexIgnore
    public s31 j;

    @DexIgnore
    public l41(int i2, j41 j41, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        this.e = i2;
        this.f = j41;
        s31 s31 = null;
        this.g = iBinder == null ? null : yd1.a(iBinder);
        this.h = pendingIntent;
        this.i = iBinder2 == null ? null : vd1.a(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            s31 = queryLocalInterface instanceof s31 ? (s31) queryLocalInterface : new u31(iBinder3);
        }
        this.j = s31;
    }

    @DexIgnore
    public static l41 a(ud1 ud1, s31 s31) {
        return new l41(2, (j41) null, (IBinder) null, (PendingIntent) null, ud1.asBinder(), s31 != null ? s31.asBinder() : null);
    }

    @DexIgnore
    public static l41 a(xd1 xd1, s31 s31) {
        return new l41(2, (j41) null, xd1.asBinder(), (PendingIntent) null, (IBinder) null, s31 != null ? s31.asBinder() : null);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, (Parcelable) this.f, i2, false);
        xd1 xd1 = this.g;
        IBinder iBinder = null;
        lk0.a(parcel, 3, xd1 == null ? null : xd1.asBinder(), false);
        lk0.a(parcel, 4, (Parcelable) this.h, i2, false);
        ud1 ud1 = this.i;
        lk0.a(parcel, 5, ud1 == null ? null : ud1.asBinder(), false);
        s31 s31 = this.j;
        if (s31 != null) {
            iBinder = s31.asBinder();
        }
        lk0.a(parcel, 6, iBinder, false);
        lk0.a(parcel, a);
    }
}
