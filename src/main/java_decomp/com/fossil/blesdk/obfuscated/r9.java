package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.recyclerview.widget.RecyclerView;
import com.sina.weibo.sdk.api.ImageObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r9 {
    @DexIgnore
    public /* final */ AccessibilityNodeInfo a;
    @DexIgnore
    public int b; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a b; // = new a(1, (CharSequence) null);
        @DexIgnore
        public static /* final */ a c; // = new a(2, (CharSequence) null);
        @DexIgnore
        public static /* final */ a d; // = new a(16, (CharSequence) null);
        @DexIgnore
        public /* final */ Object a;

        /*
        static {
            AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new a(4, (CharSequence) null);
            new a(8, (CharSequence) null);
            new a(32, (CharSequence) null);
            new a(64, (CharSequence) null);
            new a(128, (CharSequence) null);
            new a(256, (CharSequence) null);
            new a(RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN, (CharSequence) null);
            new a(1024, (CharSequence) null);
            new a(2048, (CharSequence) null);
            new a(4096, (CharSequence) null);
            new a(8192, (CharSequence) null);
            new a(RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE, (CharSequence) null);
            new a(32768, (CharSequence) null);
            new a(65536, (CharSequence) null);
            new a(131072, (CharSequence) null);
            new a(262144, (CharSequence) null);
            new a(524288, (CharSequence) null);
            new a(1048576, (CharSequence) null);
            new a(ImageObject.DATA_SIZE, (CharSequence) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null);
            new a(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null);
            new a(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null);
            new a(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null);
            if (Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new a(accessibilityAction);
        }
        */

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(int i, CharSequence charSequence) {
            this(Build.VERSION.SDK_INT >= 21 ? new AccessibilityNodeInfo.AccessibilityAction(i, charSequence) : null);
        }

        @DexIgnore
        public a(Object obj) {
            this.a = obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static b a(int i, int i2, boolean z, int i3) {
            int i4 = Build.VERSION.SDK_INT;
            if (i4 >= 21) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3));
            }
            if (i4 >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new b((Object) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public c(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static c a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2));
            }
            if (i5 >= 19) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new c((Object) null);
        }
    }

    @DexIgnore
    public r9(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.a = accessibilityNodeInfo;
    }

    @DexIgnore
    public static r9 a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new r9(accessibilityNodeInfo);
    }

    @DexIgnore
    public static String c(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    @DexIgnore
    public static r9 d(View view) {
        return a(AccessibilityNodeInfo.obtain(view));
    }

    @DexIgnore
    public static r9 x() {
        return a(AccessibilityNodeInfo.obtain());
    }

    @DexIgnore
    public int b() {
        return this.a.getChildCount();
    }

    @DexIgnore
    public void c(View view) {
        this.a.setSource(view);
    }

    @DexIgnore
    public void e(boolean z) {
        this.a.setClickable(z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || r9.class != obj.getClass()) {
            return false;
        }
        r9 r9Var = (r9) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            if (r9Var.a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(r9Var.a)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int f() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.getMovementGranularities();
        }
        return 0;
    }

    @DexIgnore
    public CharSequence g() {
        return this.a.getPackageName();
    }

    @DexIgnore
    public void h(boolean z) {
        this.a.setEnabled(z);
    }

    @DexIgnore
    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    @DexIgnore
    public void i(boolean z) {
        this.a.setFocusable(z);
    }

    @DexIgnore
    public void j(boolean z) {
        this.a.setFocused(z);
    }

    @DexIgnore
    public boolean k() {
        return this.a.isCheckable();
    }

    @DexIgnore
    public boolean l() {
        return this.a.isChecked();
    }

    @DexIgnore
    public void m(boolean z) {
        this.a.setSelected(z);
    }

    @DexIgnore
    public boolean n() {
        return this.a.isEnabled();
    }

    @DexIgnore
    public boolean o() {
        return this.a.isFocusable();
    }

    @DexIgnore
    public boolean p() {
        return this.a.isFocused();
    }

    @DexIgnore
    public boolean q() {
        return this.a.isLongClickable();
    }

    @DexIgnore
    public boolean r() {
        return this.a.isPassword();
    }

    @DexIgnore
    public boolean s() {
        return this.a.isScrollable();
    }

    @DexIgnore
    public boolean t() {
        return this.a.isSelected();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(g());
        sb.append("; className: ");
        sb.append(c());
        sb.append("; text: ");
        sb.append(h());
        sb.append("; contentDescription: ");
        sb.append(d());
        sb.append("; viewId: ");
        sb.append(i());
        sb.append("; checkable: ");
        sb.append(k());
        sb.append("; checked: ");
        sb.append(l());
        sb.append("; focusable: ");
        sb.append(o());
        sb.append("; focused: ");
        sb.append(p());
        sb.append("; selected: ");
        sb.append(t());
        sb.append("; clickable: ");
        sb.append(m());
        sb.append("; longClickable: ");
        sb.append(q());
        sb.append("; enabled: ");
        sb.append(n());
        sb.append("; password: ");
        sb.append(r());
        sb.append("; scrollable: " + s());
        sb.append("; [");
        int a2 = a();
        while (a2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(a2);
            a2 &= ~numberOfTrailingZeros;
            sb.append(c(numberOfTrailingZeros));
            if (a2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public boolean u() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isVisibleToUser();
        }
        return false;
    }

    @DexIgnore
    public void v() {
        this.a.recycle();
    }

    @DexIgnore
    public AccessibilityNodeInfo w() {
        return this.a;
    }

    @DexIgnore
    public static r9 a(r9 r9Var) {
        return a(AccessibilityNodeInfo.obtain(r9Var.a));
    }

    @DexIgnore
    public boolean b(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.a.removeAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
        return false;
    }

    @DexIgnore
    public void c(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setSource(view, i);
        }
    }

    @DexIgnore
    public void d(Rect rect) {
        this.a.setBoundsInScreen(rect);
    }

    @DexIgnore
    public void e(CharSequence charSequence) {
        this.a.setPackageName(charSequence);
    }

    @DexIgnore
    public void g(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setDismissable(z);
        }
    }

    @DexIgnore
    public CharSequence h() {
        return this.a.getText();
    }

    @DexIgnore
    public String i() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.a.getViewIdResourceName();
        }
        return null;
    }

    @DexIgnore
    public boolean j() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isAccessibilityFocused();
        }
        return false;
    }

    @DexIgnore
    public void k(boolean z) {
        this.a.setLongClickable(z);
    }

    @DexIgnore
    public void l(boolean z) {
        this.a.setScrollable(z);
    }

    @DexIgnore
    public boolean m() {
        return this.a.isClickable();
    }

    @DexIgnore
    public void n(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.a.setShowingHintText(z);
        } else {
            a(4, z);
        }
    }

    @DexIgnore
    public void o(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setVisibleToUser(z);
        }
    }

    @DexIgnore
    public void a(View view) {
        this.a.addChild(view);
    }

    @DexIgnore
    public void d(boolean z) {
        this.a.setChecked(z);
    }

    @DexIgnore
    public Bundle e() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.a.getExtras();
        }
        return new Bundle();
    }

    @DexIgnore
    public void f(CharSequence charSequence) {
        this.a.setText(charSequence);
    }

    @DexIgnore
    public void a(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.addChild(view, i);
        }
    }

    @DexIgnore
    public void b(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setMovementGranularities(i);
        }
    }

    @DexIgnore
    public void c(Rect rect) {
        this.a.setBoundsInParent(rect);
    }

    @DexIgnore
    public CharSequence d() {
        return this.a.getContentDescription();
    }

    @DexIgnore
    public void f(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setContentInvalid(z);
        }
    }

    @DexIgnore
    public void c(boolean z) {
        this.a.setCheckable(z);
    }

    @DexIgnore
    public void d(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.a.setHintText(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    @DexIgnore
    public int a() {
        return this.a.getActions();
    }

    @DexIgnore
    public void b(View view) {
        this.a.setParent(view);
    }

    @DexIgnore
    public CharSequence c() {
        return this.a.getClassName();
    }

    @DexIgnore
    public void a(int i) {
        this.a.addAction(i);
    }

    @DexIgnore
    public void b(View view, int i) {
        this.b = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setParent(view, i);
        }
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.setError(charSequence);
        }
    }

    @DexIgnore
    public void a(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.addAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
    }

    @DexIgnore
    public void a(Rect rect) {
        this.a.getBoundsInParent(rect);
    }

    @DexIgnore
    public void b(Rect rect) {
        this.a.getBoundsInScreen(rect);
    }

    @DexIgnore
    public void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setAccessibilityFocused(z);
        }
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a.setContentDescription(charSequence);
    }

    @DexIgnore
    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionItemInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionItemInfo) ((c) obj).a);
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.a.setClassName(charSequence);
    }

    @DexIgnore
    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionInfo) ((b) obj).a);
        }
    }

    @DexIgnore
    public void b(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCanOpenPopup(z);
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        Bundle e = e();
        if (e != null) {
            int i2 = e.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (~i);
            if (!z) {
                i = 0;
            }
            e.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
