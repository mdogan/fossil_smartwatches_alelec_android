package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ex;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ex<T extends ex> {
    @DexIgnore
    public /* final */ fx a; // = new fx(20, 100, r44.h());
    @DexIgnore
    public /* final */ dx b; // = new dx(this.a);

    @DexIgnore
    public Map<String, Object> a() {
        return this.b.b;
    }

    @DexIgnore
    public T a(String str, String str2) {
        this.b.a(str, str2);
        return this;
    }

    @DexIgnore
    public T a(String str, Number number) {
        this.b.a(str, number);
        return this;
    }
}
