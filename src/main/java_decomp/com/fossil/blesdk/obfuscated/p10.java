package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p10 extends b10 {
    @DexIgnore
    public /* final */ GattDescriptor.DescriptorId m;
    @DexIgnore
    public /* final */ byte[] n;

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.a(i(), this.m, this.n);
        b(true);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        if (gattOperationResult instanceof w10) {
            w10 w10 = (w10) gattOperationResult;
            return w10.b() == i() && w10.d() == this.m;
        }
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().l();
    }

    @DexIgnore
    public final byte[] j() {
        return this.n;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        BluetoothCommand.Result result;
        wd4.b(gattOperationResult, "gattOperationResult");
        b(false);
        if (!wd4.a((Object) gattOperationResult.a(), (Object) new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (rd4) null))) {
            BluetoothCommand.Result a = BluetoothCommand.Result.Companion.a(gattOperationResult.a());
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, a.getResultCode(), a.getGattResult(), 1, (Object) null);
        } else if (Arrays.equals(this.n, ((w10) gattOperationResult).c())) {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (Object) null);
        } else {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT, (GattOperationResult.GattResult) null, 5, (Object) null);
        }
        a(result);
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = super.a(z);
        if (z) {
            byte[] bArr = this.n;
            if (bArr.length < 100) {
                xa0.a(a, JSONKey.RAW_DATA, l90.a(bArr, (String) null, 1, (Object) null));
                return a;
            }
        }
        xa0.a(a, JSONKey.RAW_DATA_LENGTH, Integer.valueOf(this.n.length));
        return a;
    }
}
