package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gy implements jy {
    @DexIgnore
    public /* final */ cx a;

    @DexIgnore
    public gy(cx cxVar) {
        this.a = cxVar;
    }

    @DexIgnore
    public static gy a() throws NoClassDefFoundError, IllegalStateException {
        return a(cx.w());
    }

    @DexIgnore
    public static gy a(cx cxVar) throws IllegalStateException {
        if (cxVar != null) {
            return new gy(cxVar);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    public void a(iy iyVar) {
        try {
            this.a.a(iyVar.a());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
