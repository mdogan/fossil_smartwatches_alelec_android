package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sz2 extends as2 implements rz2 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public qz2 j;
    @DexIgnore
    public ur3<zd2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return sz2.m;
        }

        @DexIgnore
        public final sz2 b() {
            return new sz2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sz2 e;

        @DexIgnore
        public b(sz2 sz2) {
            this.e = sz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationSummaryDialView.b {
        @DexIgnore
        public /* final */ /* synthetic */ sz2 a;

        @DexIgnore
        public c(sz2 sz2) {
            this.a = sz2;
        }

        @DexIgnore
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.C.a(this.a, i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ zd2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public d(zd2 zd2, Ref$ObjectRef ref$ObjectRef) {
            this.e = zd2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.e.r.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.e.r;
            wd4.a((Object) notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
        }
    }

    /*
    static {
        String simpleName = sz2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        zd2 zd2 = (zd2) ra.a(layoutInflater, R.layout.fragment_notification_dial_landing, viewGroup, false, O0());
        zd2.q.setOnClickListener(new b(this));
        zd2.r.setOnItemClickListener(new c(this));
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        ref$ObjectRef.element = new d(zd2, ref$ObjectRef);
        NotificationSummaryDialView notificationSummaryDialView = zd2.r;
        wd4.a((Object) notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
        this.k = new ur3<>(this, zd2);
        wd4.a((Object) zd2, "binding");
        return zd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        qz2 qz2 = this.j;
        if (qz2 != null) {
            qz2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        qz2 qz2 = this.j;
        if (qz2 != null) {
            qz2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(qz2 qz2) {
        wd4.b(qz2, "presenter");
        this.j = qz2;
    }

    @DexIgnore
    public void a(SparseArray<List<BaseFeatureModel>> sparseArray) {
        wd4.b(sparseArray, "data");
        ur3<zd2> ur3 = this.k;
        if (ur3 != null) {
            zd2 a2 = ur3.a();
            if (a2 != null) {
                NotificationSummaryDialView notificationSummaryDialView = a2.r;
                if (notificationSummaryDialView != null) {
                    notificationSummaryDialView.setDataAsync(sparseArray);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
