package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.jj0;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wf0 extends dg0 {
    @DexIgnore
    public /* final */ Map<ee0.f, vf0> f;
    @DexIgnore
    public /* final */ /* synthetic */ tf0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wf0(tf0 tf0, Map<ee0.f, vf0> map) {
        super(tf0, (uf0) null);
        this.g = tf0;
        this.f = map;
    }

    @DexIgnore
    public final void a() {
        tj0 tj0 = new tj0(this.g.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (ee0.f next : this.f.keySet()) {
            if (!next.h() || this.f.get(next).c) {
                arrayList2.add(next);
            } else {
                arrayList.add(next);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = tj0.a(this.g.c, (ee0.f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = tj0.a(this.g.c, (ee0.f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.g.a.a((pg0) new xf0(this, this.g, new vd0(i, (PendingIntent) null)));
            return;
        }
        if (this.g.m && this.g.k != null) {
            this.g.k.b();
        }
        for (ee0.f next2 : this.f.keySet()) {
            jj0.c cVar = this.f.get(next2);
            if (!next2.h() || tj0.a(this.g.c, next2) == 0) {
                next2.a(cVar);
            } else {
                this.g.a.a((pg0) new yf0(this, this.g, cVar));
            }
        }
    }
}
