package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class if4 extends hf4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements df4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public a(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    @DexIgnore
    public static final <T> df4<T> a(Iterator<? extends T> it) {
        wd4.b(it, "$this$asSequence");
        return a(new a(it));
    }

    @DexIgnore
    public static final <T> df4<T> a(df4<? extends T> df4) {
        wd4.b(df4, "$this$constrainOnce");
        return df4 instanceof af4 ? df4 : new af4(df4);
    }

    @DexIgnore
    public static final <T> df4<T> a(id4<? extends T> id4, jd4<? super T, ? extends T> jd4) {
        wd4.b(id4, "seedFunction");
        wd4.b(jd4, "nextFunction");
        return new cf4(id4, jd4);
    }
}
