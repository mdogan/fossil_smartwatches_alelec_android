package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l42 implements Factory<k42> {
    @DexIgnore
    public /* final */ Provider<Map<Class<? extends jc>, Provider<jc>>> a;

    @DexIgnore
    public l42(Provider<Map<Class<? extends jc>, Provider<jc>>> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static l42 a(Provider<Map<Class<? extends jc>, Provider<jc>>> provider) {
        return new l42(provider);
    }

    @DexIgnore
    public static k42 b(Provider<Map<Class<? extends jc>, Provider<jc>>> provider) {
        return new k42(provider.get());
    }

    @DexIgnore
    public k42 get() {
        return b(this.a);
    }
}
