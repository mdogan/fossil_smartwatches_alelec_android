package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.fi4;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gi4 extends ei4 {
    @DexIgnore
    public abstract Thread I();

    @DexIgnore
    public final void J() {
        Thread I = I();
        if (Thread.currentThread() != I) {
            oj4 a = pj4.a();
            if (a != null) {
                a.a(I);
            } else {
                LockSupport.unpark(I);
            }
        }
    }

    @DexIgnore
    public final void a(long j, fi4.c cVar) {
        wd4.b(cVar, "delayedTask");
        if (oh4.a()) {
            if (!(this != qh4.k)) {
                throw new AssertionError();
            }
        }
        qh4.k.b(j, cVar);
    }
}
