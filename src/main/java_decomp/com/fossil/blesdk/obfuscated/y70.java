package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y70 extends q70 {
    @DexIgnore
    public long L; // = 5000;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.CLEAN_UP_DEVICE, RequestId.CLEAN_UP_DEVICE, peripheral, 0, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
        c(true);
    }

    @DexIgnore
    public void a(long j) {
        this.L = j;
    }

    @DexIgnore
    public long m() {
        return this.L;
    }
}
