package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m41 implements Parcelable.Creator<l41> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        j41 j41 = null;
        IBinder iBinder = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 2:
                    j41 = SafeParcelReader.a(parcel, a, j41.CREATOR);
                    break;
                case 3:
                    iBinder = SafeParcelReader.p(parcel, a);
                    break;
                case 4:
                    pendingIntent = SafeParcelReader.a(parcel, a, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = SafeParcelReader.p(parcel, a);
                    break;
                case 6:
                    iBinder3 = SafeParcelReader.p(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new l41(i, j41, iBinder, pendingIntent, iBinder2, iBinder3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new l41[i];
    }
}
