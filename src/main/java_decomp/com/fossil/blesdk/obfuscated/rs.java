package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class rs implements po<Bitmap> {
    @DexIgnore
    public abstract Bitmap a(kq kqVar, Bitmap bitmap, int i, int i2);

    @DexIgnore
    public final bq<Bitmap> a(Context context, bq<Bitmap> bqVar, int i, int i2) {
        if (vw.b(i, i2)) {
            kq c = sn.a(context).c();
            Bitmap bitmap = bqVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap a = a(c, bitmap, i, i2);
            return bitmap.equals(a) ? bqVar : qs.a(a, c);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
