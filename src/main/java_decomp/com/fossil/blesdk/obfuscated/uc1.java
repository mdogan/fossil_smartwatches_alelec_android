package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uc1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uc1> CREATOR; // = new gd1();
    @DexIgnore
    public /* final */ List<LocationRequest> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public ed1 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<LocationRequest> a; // = new ArrayList<>();
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public final a a(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.a.add(locationRequest);
            }
            return this;
        }

        @DexIgnore
        public final uc1 a() {
            return new uc1(this.a, this.b, this.c, (ed1) null);
        }
    }

    @DexIgnore
    public uc1(List<LocationRequest> list, boolean z, boolean z2, ed1 ed1) {
        this.e = list;
        this.f = z;
        this.g = z2;
        this.h = ed1;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = lk0.a(parcel);
        lk0.b(parcel, 1, Collections.unmodifiableList(this.e), false);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 5, (Parcelable) this.h, i, false);
        lk0.a(parcel, a2);
    }
}
