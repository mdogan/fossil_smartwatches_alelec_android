package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.km4;
import com.fossil.blesdk.obfuscated.zm4;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xm4 implements Interceptor {
    @DexIgnore
    public /* final */ cn4 a;

    @DexIgnore
    public xm4(cn4 cn4) {
        this.a = cn4;
    }

    @DexIgnore
    public static Response a(Response response) {
        if (response == null || response.y() == null) {
            return response;
        }
        Response.a H = response.H();
        H.a((qm4) null);
        return H.a();
    }

    @DexIgnore
    public static boolean b(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        cn4 cn4 = this.a;
        Response b = cn4 != null ? cn4.b(chain.n()) : null;
        zm4 c = new zm4.a(System.currentTimeMillis(), chain.n(), b).c();
        pm4 pm4 = c.a;
        Response response = c.b;
        cn4 cn42 = this.a;
        if (cn42 != null) {
            cn42.a(c);
        }
        if (b != null && response == null) {
            vm4.a((Closeable) b.y());
        }
        if (pm4 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.a(chain.n());
            aVar.a(Protocol.HTTP_1_1);
            aVar.a(504);
            aVar.a("Unsatisfiable Request (only-if-cached)");
            aVar.a(vm4.c);
            aVar.b(-1);
            aVar.a(System.currentTimeMillis());
            return aVar.a();
        } else if (pm4 == null) {
            Response.a H = response.H();
            H.a(a(response));
            return H.a();
        } else {
            try {
                Response a2 = chain.a(pm4);
                if (a2 == null && b != null) {
                }
                if (response != null) {
                    if (a2.B() == 304) {
                        Response.a H2 = response.H();
                        H2.a(a(response.D(), a2.D()));
                        H2.b(a2.M());
                        H2.a(a2.K());
                        H2.a(a(response));
                        H2.c(a(a2));
                        Response a3 = H2.a();
                        a2.y().close();
                        this.a.a();
                        this.a.a(response, a3);
                        return a3;
                    }
                    vm4.a((Closeable) response.y());
                }
                Response.a H3 = a2.H();
                H3.a(a(response));
                H3.c(a(a2));
                Response a4 = H3.a();
                if (this.a != null) {
                    if (nn4.b(a4) && zm4.a(a4, pm4)) {
                        return a(this.a.a(a4), a4);
                    }
                    if (on4.a(pm4.e())) {
                        try {
                            this.a.a(pm4);
                        } catch (IOException unused) {
                        }
                    }
                }
                return a4;
            } finally {
                if (b != null) {
                    vm4.a((Closeable) b.y());
                }
            }
        }
    }

    @DexIgnore
    public final Response a(ym4 ym4, Response response) throws IOException {
        if (ym4 == null) {
            return response;
        }
        jp4 a2 = ym4.a();
        if (a2 == null) {
            return response;
        }
        a aVar = new a(this, response.y().E(), ym4, ep4.a(a2));
        String e = response.e(GraphRequest.CONTENT_TYPE_HEADER);
        long C = response.y().C();
        Response.a H = response.H();
        H.a((qm4) new qn4(e, C, ep4.a((kp4) aVar)));
        return H.a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements kp4 {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 f;
        @DexIgnore
        public /* final */ /* synthetic */ ym4 g;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 h;

        @DexIgnore
        public a(xm4 xm4, xo4 xo4, ym4 ym4, wo4 wo4) {
            this.f = xo4;
            this.g = ym4;
            this.h = wo4;
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            try {
                long b = this.f.b(vo4, j);
                if (b == -1) {
                    if (!this.e) {
                        this.e = true;
                        this.h.close();
                    }
                    return -1;
                }
                vo4.a(this.h.a(), vo4.B() - b, b);
                this.h.d();
                return b;
            } catch (IOException e2) {
                if (!this.e) {
                    this.e = true;
                    this.g.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.e && !vm4.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                this.e = true;
                this.g.abort();
            }
            this.f.close();
        }

        @DexIgnore
        public lp4 b() {
            return this.f.b();
        }
    }

    @DexIgnore
    public static km4 a(km4 km4, km4 km42) {
        km4.a aVar = new km4.a();
        int b = km4.b();
        for (int i = 0; i < b; i++) {
            String a2 = km4.a(i);
            String b2 = km4.b(i);
            if ((!"Warning".equalsIgnoreCase(a2) || !b2.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_YES)) && (a(a2) || !b(a2) || km42.a(a2) == null)) {
                tm4.a.a(aVar, a2, b2);
            }
        }
        int b3 = km42.b();
        for (int i2 = 0; i2 < b3; i2++) {
            String a3 = km42.a(i2);
            if (!a(a3) && b(a3)) {
                tm4.a.a(aVar, a3, km42.b(i2));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return "Content-Length".equalsIgnoreCase(str) || GraphRequest.CONTENT_ENCODING_HEADER.equalsIgnoreCase(str) || GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(str);
    }
}
