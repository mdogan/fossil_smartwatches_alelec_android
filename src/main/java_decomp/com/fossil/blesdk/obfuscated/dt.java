package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dt implements no<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ ps a; // = new ps();

    @DexIgnore
    public boolean a(InputStream inputStream, mo moVar) throws IOException {
        return true;
    }

    @DexIgnore
    public bq<Bitmap> a(InputStream inputStream, int i, int i2, mo moVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(lw.a(inputStream)), i, i2, moVar);
    }
}
