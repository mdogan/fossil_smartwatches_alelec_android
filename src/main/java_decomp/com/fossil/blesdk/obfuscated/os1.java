package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class os1 extends h1 {
    @DexIgnore
    public os1(Context context) {
        super(context);
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        k1 k1Var = (k1) a(i, i2, i3, charSequence);
        qs1 qs1 = new qs1(e(), this, k1Var);
        k1Var.a((v1) qs1);
        return qs1;
    }
}
