package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class is4<T> extends z84<cs4<T>> {
    @DexIgnore
    public /* final */ Call<T> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements j94, qr4<T> {
        @DexIgnore
        public /* final */ Call<?> e;
        @DexIgnore
        public /* final */ b94<? super cs4<T>> f;
        @DexIgnore
        public volatile boolean g;
        @DexIgnore
        public boolean h; // = false;

        @DexIgnore
        public a(Call<?> call, b94<? super cs4<T>> b94) {
            this.e = call;
            this.f = b94;
        }

        @DexIgnore
        public boolean a() {
            return this.g;
        }

        @DexIgnore
        public void dispose() {
            this.g = true;
            this.e.cancel();
        }

        @DexIgnore
        public void onFailure(Call<T> call, Throwable th) {
            if (!call.o()) {
                try {
                    this.f.onError(th);
                } catch (Throwable th2) {
                    l94.b(th2);
                    ta4.b(new CompositeException(th, th2));
                }
            }
        }

        @DexIgnore
        public void onResponse(Call<T> call, cs4<T> cs4) {
            if (!this.g) {
                try {
                    this.f.onNext(cs4);
                    if (!this.g) {
                        this.h = true;
                        this.f.onComplete();
                    }
                } catch (Throwable th) {
                    l94.b(th);
                    ta4.b(new CompositeException(th, th));
                }
            }
        }
    }

    @DexIgnore
    public is4(Call<T> call) {
        this.e = call;
    }

    @DexIgnore
    public void b(b94<? super cs4<T>> b94) {
        Call<T> clone = this.e.clone();
        a aVar = new a(clone, b94);
        b94.onSubscribe(aVar);
        if (!aVar.a()) {
            clone.a(aVar);
        }
    }
}
