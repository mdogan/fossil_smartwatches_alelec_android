package com.fossil.blesdk.obfuscated;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dd implements cd {
    @DexIgnore
    public /* final */ MediaSessionManager.RemoteUserInfo a;

    @DexIgnore
    public dd(String str, int i, int i2) {
        this.a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dd)) {
            return false;
        }
        return this.a.equals(((dd) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return f8.a(this.a);
    }
}
