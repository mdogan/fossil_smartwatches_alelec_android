package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dq implements ko {
    @DexIgnore
    public static /* final */ rw<Class<?>, byte[]> j; // = new rw<>(50);
    @DexIgnore
    public /* final */ hq b;
    @DexIgnore
    public /* final */ ko c;
    @DexIgnore
    public /* final */ ko d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ mo h;
    @DexIgnore
    public /* final */ po<?> i;

    @DexIgnore
    public dq(hq hqVar, ko koVar, ko koVar2, int i2, int i3, po<?> poVar, Class<?> cls, mo moVar) {
        this.b = hqVar;
        this.c = koVar;
        this.d = koVar2;
        this.e = i2;
        this.f = i3;
        this.i = poVar;
        this.g = cls;
        this.h = moVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        po<?> poVar = this.i;
        if (poVar != null) {
            poVar.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
        this.b.put(bArr);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof dq)) {
            return false;
        }
        dq dqVar = (dq) obj;
        if (this.f != dqVar.f || this.e != dqVar.e || !vw.b((Object) this.i, (Object) dqVar.i) || !this.g.equals(dqVar.g) || !this.c.equals(dqVar.c) || !this.d.equals(dqVar.d) || !this.h.equals(dqVar.h)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        po<?> poVar = this.i;
        if (poVar != null) {
            hashCode = (hashCode * 31) + poVar.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }

    @DexIgnore
    public final byte[] a() {
        byte[] a = j.a(this.g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.g.getName().getBytes(ko.a);
        j.b(this.g, bytes);
        return bytes;
    }
}
