package com.fossil.blesdk.obfuscated;

import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tw1 {
    @DexIgnore
    public /* final */ jw1<?> a;
    @DexIgnore
    public /* final */ Set<tw1> b; // = new HashSet();
    @DexIgnore
    public /* final */ Set<tw1> c; // = new HashSet();

    @DexIgnore
    public tw1(jw1<?> jw1) {
        this.a = jw1;
    }

    @DexIgnore
    public final void a(tw1 tw1) {
        this.b.add(tw1);
    }

    @DexIgnore
    public final void b(tw1 tw1) {
        this.c.add(tw1);
    }

    @DexIgnore
    public final void c(tw1 tw1) {
        this.c.remove(tw1);
    }

    @DexIgnore
    public final boolean d() {
        return this.b.isEmpty();
    }

    @DexIgnore
    public final Set<tw1> a() {
        return this.b;
    }

    @DexIgnore
    public final jw1<?> b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c.isEmpty();
    }
}
