package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.adapter.rxjava2.HttpException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hs4<T> extends z84<T> {
    @DexIgnore
    public /* final */ z84<cs4<T>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements b94<cs4<R>> {
        @DexIgnore
        public /* final */ b94<? super R> e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public a(b94<? super R> b94) {
            this.e = b94;
        }

        @DexIgnore
        /* renamed from: a */
        public void onNext(cs4<R> cs4) {
            if (cs4.d()) {
                this.e.onNext(cs4.a());
                return;
            }
            this.f = true;
            HttpException httpException = new HttpException(cs4);
            try {
                this.e.onError(httpException);
            } catch (Throwable th) {
                l94.b(th);
                ta4.b(new CompositeException(httpException, th));
            }
        }

        @DexIgnore
        public void onComplete() {
            if (!this.f) {
                this.e.onComplete();
            }
        }

        @DexIgnore
        public void onError(Throwable th) {
            if (!this.f) {
                this.e.onError(th);
                return;
            }
            AssertionError assertionError = new AssertionError("This should never happen! Report as a bug with the full stacktrace.");
            assertionError.initCause(th);
            ta4.b(assertionError);
        }

        @DexIgnore
        public void onSubscribe(j94 j94) {
            this.e.onSubscribe(j94);
        }
    }

    @DexIgnore
    public hs4(z84<cs4<T>> z84) {
        this.e = z84;
    }

    @DexIgnore
    public void b(b94<? super T> b94) {
        this.e.a(new a(b94));
    }
}
