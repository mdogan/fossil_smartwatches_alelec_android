package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i84 {
    @DexIgnore
    View onCreateView(View view, String str, Context context, AttributeSet attributeSet);
}
