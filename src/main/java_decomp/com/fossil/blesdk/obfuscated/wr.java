package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wr<Model, Data> implements tr<Model, Data> {
    @DexIgnore
    public /* final */ List<tr<Model, Data>> a;
    @DexIgnore
    public /* final */ h8<List<Throwable>> b;

    @DexIgnore
    public wr(List<tr<Model, Data>> list, h8<List<Throwable>> h8Var) {
        this.a = list;
        this.b = h8Var;
    }

    @DexIgnore
    public tr.a<Data> a(Model model, int i, int i2, mo moVar) {
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        ko koVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            tr trVar = this.a.get(i3);
            if (trVar.a(model)) {
                tr.a a2 = trVar.a(model, i, i2, moVar);
                if (a2 != null) {
                    koVar = a2.a;
                    arrayList.add(a2.c);
                }
            }
        }
        if (arrayList.isEmpty() || koVar == null) {
            return null;
        }
        return new tr.a<>(koVar, new a(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements to<Data>, to.a<Data> {
        @DexIgnore
        public /* final */ List<to<Data>> e;
        @DexIgnore
        public /* final */ h8<List<Throwable>> f;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public Priority h;
        @DexIgnore
        public to.a<? super Data> i;
        @DexIgnore
        public List<Throwable> j;
        @DexIgnore
        public boolean k;

        @DexIgnore
        public a(List<to<Data>> list, h8<List<Throwable>> h8Var) {
            this.f = h8Var;
            uw.a(list);
            this.e = list;
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super Data> aVar) {
            this.h = priority;
            this.i = aVar;
            this.j = this.f.a();
            this.e.get(this.g).a(priority, this);
            if (this.k) {
                cancel();
            }
        }

        @DexIgnore
        public DataSource b() {
            return this.e.get(0).b();
        }

        @DexIgnore
        public final void c() {
            if (!this.k) {
                if (this.g < this.e.size() - 1) {
                    this.g++;
                    a(this.h, this.i);
                    return;
                }
                uw.a(this.j);
                this.i.a((Exception) new GlideException("Fetch failed", (List<Throwable>) new ArrayList(this.j)));
            }
        }

        @DexIgnore
        public void cancel() {
            this.k = true;
            for (to<Data> cancel : this.e) {
                cancel.cancel();
            }
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.e.get(0).getDataClass();
        }

        @DexIgnore
        public void a() {
            List<Throwable> list = this.j;
            if (list != null) {
                this.f.a(list);
            }
            this.j = null;
            for (to<Data> a : this.e) {
                a.a();
            }
        }

        @DexIgnore
        public void a(Data data) {
            if (data != null) {
                this.i.a(data);
            } else {
                c();
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            List<Throwable> list = this.j;
            uw.a(list);
            list.add(exc);
            c();
        }
    }

    @DexIgnore
    public boolean a(Model model) {
        for (tr<Model, Data> a2 : this.a) {
            if (a2.a(model)) {
                return true;
            }
        }
        return false;
    }
}
