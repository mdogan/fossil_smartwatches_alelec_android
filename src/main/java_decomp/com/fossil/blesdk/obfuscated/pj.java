package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import com.fossil.blesdk.obfuscated.vj;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pj implements nj {
    @DexIgnore
    public static /* final */ String n; // = ej.a("Processor");
    @DexIgnore
    public Context e;
    @DexIgnore
    public yi f;
    @DexIgnore
    public am g;
    @DexIgnore
    public WorkDatabase h;
    @DexIgnore
    public Map<String, vj> i; // = new HashMap();
    @DexIgnore
    public List<qj> j;
    @DexIgnore
    public Set<String> k;
    @DexIgnore
    public /* final */ List<nj> l;
    @DexIgnore
    public /* final */ Object m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public nj e;
        @DexIgnore
        public String f;
        @DexIgnore
        public aw1<Boolean> g;

        @DexIgnore
        public a(nj njVar, String str, aw1<Boolean> aw1) {
            this.e = njVar;
            this.f = str;
            this.g = aw1;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.g.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.e.a(this.f, z);
        }
    }

    @DexIgnore
    public pj(Context context, yi yiVar, am amVar, WorkDatabase workDatabase, List<qj> list) {
        this.e = context;
        this.f = yiVar;
        this.g = amVar;
        this.h = workDatabase;
        this.j = list;
        this.k = new HashSet();
        this.l = new ArrayList();
        this.m = new Object();
    }

    @DexIgnore
    public boolean a(String str, WorkerParameters.a aVar) {
        synchronized (this.m) {
            if (this.i.containsKey(str)) {
                ej.a().a(n, String.format("Work %s is already enqueued for processing", new Object[]{str}), new Throwable[0]);
                return false;
            }
            vj.c cVar = new vj.c(this.e, this.f, this.g, this.h, str);
            cVar.a(this.j);
            cVar.a(aVar);
            vj a2 = cVar.a();
            aw1<Boolean> a3 = a2.a();
            a3.a(new a(this, str, a3), this.g.a());
            this.i.put(str, a2);
            this.g.b().execute(a2);
            ej.a().a(n, String.format("%s: processing %s", new Object[]{pj.class.getSimpleName(), str}), new Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        boolean containsKey;
        synchronized (this.m) {
            containsKey = this.i.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public boolean c(String str) {
        return a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public boolean d(String str) {
        synchronized (this.m) {
            ej.a().a(n, String.format("Processor cancelling %s", new Object[]{str}), new Throwable[0]);
            this.k.add(str);
            vj remove = this.i.remove(str);
            if (remove != null) {
                remove.a(true);
                ej.a().a(n, String.format("WorkerWrapper cancelled for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            ej.a().a(n, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public boolean e(String str) {
        synchronized (this.m) {
            ej.a().a(n, String.format("Processor stopping %s", new Object[]{str}), new Throwable[0]);
            vj remove = this.i.remove(str);
            if (remove != null) {
                remove.a(false);
                ej.a().a(n, String.format("WorkerWrapper stopped for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            ej.a().a(n, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public void b(nj njVar) {
        synchronized (this.m) {
            this.l.remove(njVar);
        }
    }

    @DexIgnore
    public boolean a(String str) {
        boolean contains;
        synchronized (this.m) {
            contains = this.k.contains(str);
        }
        return contains;
    }

    @DexIgnore
    public void a(nj njVar) {
        synchronized (this.m) {
            this.l.add(njVar);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.m) {
            this.i.remove(str);
            ej.a().a(n, String.format("%s %s executed; reschedule = %s", new Object[]{getClass().getSimpleName(), str, Boolean.valueOf(z)}), new Throwable[0]);
            for (nj a2 : this.l) {
                a2.a(str, z);
            }
        }
    }
}
