package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ku implements ru {
    @DexIgnore
    public /* final */ Set<su> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public void a(su suVar) {
        this.a.add(suVar);
        if (this.c) {
            suVar.b();
        } else if (this.b) {
            suVar.a();
        } else {
            suVar.c();
        }
    }

    @DexIgnore
    public void b(su suVar) {
        this.a.remove(suVar);
    }

    @DexIgnore
    public void c() {
        this.b = false;
        for (T c2 : vw.a(this.a)) {
            c2.c();
        }
    }

    @DexIgnore
    public void b() {
        this.b = true;
        for (T a2 : vw.a(this.a)) {
            a2.a();
        }
    }

    @DexIgnore
    public void a() {
        this.c = true;
        for (T b2 : vw.a(this.a)) {
            b2.b();
        }
    }
}
