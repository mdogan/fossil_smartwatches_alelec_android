package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ww;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aq<Z> implements bq<Z>, ww.f {
    @DexIgnore
    public static /* final */ h8<aq<?>> i; // = ww.a(20, new a());
    @DexIgnore
    public /* final */ yw e; // = yw.b();
    @DexIgnore
    public bq<Z> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ww.d<aq<?>> {
        @DexIgnore
        public aq<?> a() {
            return new aq<>();
        }
    }

    @DexIgnore
    public static <Z> aq<Z> b(bq<Z> bqVar) {
        aq<Z> a2 = i.a();
        uw.a(a2);
        aq<Z> aqVar = a2;
        aqVar.a(bqVar);
        return aqVar;
    }

    @DexIgnore
    public final void a(bq<Z> bqVar) {
        this.h = false;
        this.g = true;
        this.f = bqVar;
    }

    @DexIgnore
    public Class<Z> c() {
        return this.f.c();
    }

    @DexIgnore
    public final void d() {
        this.f = null;
        i.a(this);
    }

    @DexIgnore
    public synchronized void e() {
        this.e.a();
        if (this.g) {
            this.g = false;
            if (this.h) {
                a();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    @DexIgnore
    public Z get() {
        return this.f.get();
    }

    @DexIgnore
    public yw i() {
        return this.e;
    }

    @DexIgnore
    public int b() {
        return this.f.b();
    }

    @DexIgnore
    public synchronized void a() {
        this.e.a();
        this.h = true;
        if (!this.g) {
            this.f.a();
            d();
        }
    }
}
