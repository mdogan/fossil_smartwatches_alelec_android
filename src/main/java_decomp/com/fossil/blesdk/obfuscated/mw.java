package com.fossil.blesdk.obfuscated;

import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mw<K, V> extends g4<K, V> {
    @DexIgnore
    public int m;

    @DexIgnore
    public V a(int i, V v) {
        this.m = 0;
        return super.a(i, v);
    }

    @DexIgnore
    public void clear() {
        this.m = 0;
        super.clear();
    }

    @DexIgnore
    public V d(int i) {
        this.m = 0;
        return super.d(i);
    }

    @DexIgnore
    public int hashCode() {
        if (this.m == 0) {
            this.m = super.hashCode();
        }
        return this.m;
    }

    @DexIgnore
    public V put(K k, V v) {
        this.m = 0;
        return super.put(k, v);
    }

    @DexIgnore
    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.m = 0;
        super.a(simpleArrayMap);
    }
}
