package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;

    @DexIgnore
    public w04(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new c14(this.e, (Map<String, Integer>) null, (l04) null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            k04.m.a(th);
            k04.a(this.e, th);
        }
    }
}
