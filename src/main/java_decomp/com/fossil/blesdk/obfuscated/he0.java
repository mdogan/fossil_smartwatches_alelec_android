package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.lj0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class he0 {
    @DexIgnore
    public static /* final */ Set<he0> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public interface b {
        @DexIgnore
        void e(Bundle bundle);

        @DexIgnore
        void f(int i);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(vd0 vd0);
    }

    @DexIgnore
    public static Set<he0> i() {
        Set<he0> set;
        synchronized (a) {
            set = a;
        }
        return set;
    }

    @DexIgnore
    public <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract vd0 a();

    @DexIgnore
    public abstract void a(c cVar);

    @DexIgnore
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public abstract ie0<Status> b();

    @DexIgnore
    public <A extends ee0.b, T extends ue0<? extends ne0, A>> T b(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void b(c cVar);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public Context e() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public void h() {
        throw new UnsupportedOperationException();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<ee0<?>, lj0.b> h; // = new g4();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<ee0<?>, ee0.d> j; // = new g4();
        @DexIgnore
        public ye0 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public c m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public yd0 o; // = yd0.a();
        @DexIgnore
        public ee0.a<? extends mn1, wm1> p; // = jn1.c;
        @DexIgnore
        public /* final */ ArrayList<b> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<c> r; // = new ArrayList<>();

        @DexIgnore
        public a(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final a a(Handler handler) {
            ck0.a(handler, (Object) "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final lj0 b() {
            wm1 wm1 = wm1.m;
            if (this.j.containsKey(jn1.e)) {
                wm1 = (wm1) this.j.get(jn1.e);
            }
            return new lj0(this.a, this.b, this.h, this.d, this.e, this.f, this.g, wm1, false);
        }

        @DexIgnore
        public final a a(b bVar) {
            ck0.a(bVar, (Object) "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @DexIgnore
        public final a a(c cVar) {
            ck0.a(cVar, (Object) "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @DexIgnore
        public final a a(Scope scope) {
            ck0.a(scope, (Object) "Scope must not be null");
            this.b.add(scope);
            return this;
        }

        @DexIgnore
        public final a a(String[] strArr) {
            for (String scope : strArr) {
                this.b.add(new Scope(scope));
            }
            return this;
        }

        @DexIgnore
        public final a a(ee0<? extends ee0.d.C0011d> ee0) {
            ck0.a(ee0, (Object) "Api must not be null");
            this.j.put(ee0, (Object) null);
            List<Scope> a2 = ee0.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a a(ee0<? extends ee0.d.C0011d> ee0, Scope... scopeArr) {
            ck0.a(ee0, (Object) "Api must not be null");
            this.j.put(ee0, (Object) null);
            a(ee0, (ee0.d) null, scopeArr);
            return this;
        }

        @DexIgnore
        public final <O extends ee0.d.c> a a(ee0<O> ee0, O o2) {
            ck0.a(ee0, (Object) "Api must not be null");
            ck0.a(o2, (Object) "Null options are not permitted for this Api");
            this.j.put(ee0, o2);
            List<Scope> a2 = ee0.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final he0 a() {
            ck0.a(!this.j.isEmpty(), (Object) "must call addApi() to add at least one API");
            lj0 b2 = b();
            ee0 ee0 = null;
            Map<ee0<?>, lj0.b> f2 = b2.f();
            g4 g4Var = new g4();
            g4 g4Var2 = new g4();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (ee0 next : this.j.keySet()) {
                ee0.d dVar = this.j.get(next);
                boolean z2 = f2.get(next) != null;
                g4Var.put(next, Boolean.valueOf(z2));
                hi0 hi0 = new hi0(next, z2);
                arrayList.add(hi0);
                ee0.a d2 = next.d();
                ee0 ee02 = next;
                ee0.f a2 = d2.a(this.i, this.n, b2, dVar, hi0, hi0);
                g4Var2.put(ee02.a(), a2);
                if (d2.a() == 1) {
                    z = dVar != null;
                }
                if (a2.d()) {
                    if (ee0 == null) {
                        ee0 = ee02;
                    } else {
                        String b3 = ee02.b();
                        String b4 = ee0.b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b3).length() + 21 + String.valueOf(b4).length());
                        sb.append(b3);
                        sb.append(" cannot be used with ");
                        sb.append(b4);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (ee0 != null) {
                if (!z) {
                    ck0.b(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", ee0.b());
                    ck0.b(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", ee0.b());
                } else {
                    String b5 = ee0.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b5).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b5);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            fg0 fg0 = new fg0(this.i, new ReentrantLock(), this.n, b2, this.o, this.p, g4Var, this.q, this.r, g4Var2, this.l, fg0.a((Iterable<ee0.f>) g4Var2.values(), true), arrayList, false);
            synchronized (he0.a) {
                he0.a.add(fg0);
            }
            if (this.l >= 0) {
                ai0.b(this.k).a(this.l, fg0, this.m);
            }
            return fg0;
        }

        @DexIgnore
        public final <O extends ee0.d> void a(ee0<O> ee0, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(ee0.c().a(o2));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.h.put(ee0, new lj0.b(hashSet));
        }
    }

    @DexIgnore
    public boolean a(df0 df0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void a(nh0 nh0) {
        throw new UnsupportedOperationException();
    }
}
