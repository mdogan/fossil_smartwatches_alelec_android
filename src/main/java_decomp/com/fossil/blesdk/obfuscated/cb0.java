package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cb0 implements Runnable {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ id4<cb4> f;

    @DexIgnore
    public cb0(id4<cb4> id4) {
        wd4.b(id4, "task");
        this.f = id4;
    }

    @DexIgnore
    public final void a() {
        this.e = true;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public void run() {
        if (!this.e) {
            this.f.invoke();
        }
    }
}
