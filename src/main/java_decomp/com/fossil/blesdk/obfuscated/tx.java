package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tx {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public tx(String str, Bundle bundle) {
        this.a = str;
        this.b = bundle;
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public Bundle b() {
        return this.b;
    }
}
