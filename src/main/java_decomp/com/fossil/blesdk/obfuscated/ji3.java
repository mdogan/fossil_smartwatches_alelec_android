package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ji3 extends as2 implements ii3, View.OnClickListener, xs3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<rc2> j;
    @DexIgnore
    public hi3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ji3.m;
        }

        @DexIgnore
        public final ji3 b() {
            return new ji3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public b(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public c(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://support.fossil.com/hc/%s/categories/360000064626-Smartwatch-FAQ", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ji3.n.a();
            local.d(a2, "FAQ URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public d(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(ji3.n.a(), "Repair Center URL = https://c.fossil.com/web/service_centers");
            this.e.U("https://c.fossil.com/web/service_centers");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public e(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().i();
            this.e.T0().a("Contact Us - From app [Fossil] - [Android]");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public f(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://support.fossil.com/hc/%s?wearablesChat=true", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ji3.n.a();
            local.d(a2, "Chat URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public g(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(ji3.n.a(), "Call Us URL = https://c.fossil.com/web/call");
            this.e.U("https://c.fossil.com/web/call");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ji3 e;

        @DexIgnore
        public h(ji3 ji3, String str) {
            this.e = ji3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                DeleteAccountActivity.a aVar = DeleteAccountActivity.C;
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    wd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = ji3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "HelpFragment::class.java.simpleName!!");
            m = simpleName;
            new String[]{"smartwatches@fossil.com"};
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final hi3 T0() {
        hi3 hi3 = this.k;
        if (hi3 != null) {
            return hi3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void U(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), m);
    }

    @DexIgnore
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wd4.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1000) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            hi3 hi3 = this.k;
            if (hi3 != null) {
                hi3.h();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wd4.b(view, "v");
        if (view.getId() == R.id.aciv_back) {
            n();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        rc2 rc2 = (rc2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_help, (ViewGroup) null, false, O0());
        this.j = new ur3<>(this, rc2);
        wd4.a((Object) rc2, "binding");
        return rc2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        hi3 hi3 = this.k;
        if (hi3 != null) {
            hi3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        hi3 hi3 = this.k;
        if (hi3 != null) {
            hi3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        String h2 = PortfolioApp.W.c().h();
        ur3<rc2> ur3 = this.j;
        if (ur3 != null) {
            rc2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "binding.tvAppVersion");
                be4 be4 = be4.a;
                String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_Overview_Text__AppVersionNumber);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026w_Text__AppVersionNumber)");
                Object[] objArr = {h2};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.setOnClickListener(new b(this, h2));
                a2.r.setOnClickListener(new c(this, h2));
                a2.s.setOnClickListener(new d(this, h2));
                a2.w.setOnClickListener(new e(this, h2));
                a2.x.setOnClickListener(new f(this, h2));
                a2.v.setOnClickListener(new g(this, h2));
                a2.u.setOnClickListener(new h(this, h2));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(hi3 hi3) {
        wd4.b(hi3, "presenter");
        tt1.a(hi3);
        wd4.a((Object) hi3, "Preconditions.checkNotNull(presenter)");
        this.k = hi3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = rj3.n.a();
        local.d(a2, "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.a(str, i, intent);
        }
    }
}
