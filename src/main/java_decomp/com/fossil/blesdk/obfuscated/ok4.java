package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.PinObject;
import java.util.ArrayDeque;
import kotlin.Pair;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.ExceptionsConstuctorKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ok4 {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        Object obj;
        Object obj2;
        try {
            Result.a aVar = Result.Companion;
            Class<?> cls = Class.forName("kotlin.coroutines.jvm.internal.BaseContinuationImpl");
            wd4.a((Object) cls, "Class.forName(baseContinuationImplClass)");
            obj = Result.m3constructorimpl(cls.getCanonicalName());
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            obj = Result.m3constructorimpl(za4.a(th));
        }
        if (Result.m6exceptionOrNullimpl(obj) != null) {
            obj = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) obj;
        try {
            Result.a aVar3 = Result.Companion;
            Class<?> cls2 = Class.forName("com.fossil.blesdk.obfuscated.ok4");
            wd4.a((Object) cls2, "Class.forName(stackTraceRecoveryClass)");
            obj2 = Result.m3constructorimpl(cls2.getCanonicalName());
        } catch (Throwable th2) {
            Result.a aVar4 = Result.Companion;
            obj2 = Result.m3constructorimpl(za4.a(th2));
        }
        if (Result.m6exceptionOrNullimpl(obj2) != null) {
            obj2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        String str = (String) obj2;
    }
    */

    @DexIgnore
    public static final <E extends Throwable> E b(E e, rc4 rc4) {
        Pair a2 = a(e);
        E e2 = (Throwable) a2.component1();
        StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) a2.component2();
        E a3 = ExceptionsConstuctorKt.a(e2);
        if (a3 == null) {
            return e;
        }
        ArrayDeque<StackTraceElement> a4 = a(rc4);
        if (a4.isEmpty()) {
            return e;
        }
        if (e2 != e) {
            a(stackTraceElementArr, a4);
        }
        a(e2, a3, a4);
        return a3;
    }

    @DexIgnore
    public static final <E extends Throwable> E a(E e, kc4<?> kc4) {
        wd4.b(e, "exception");
        wd4.b(kc4, "continuation");
        return (!oh4.d() || !(kc4 instanceof rc4)) ? e : b(e, (rc4) kc4);
    }

    @DexIgnore
    public static final <E extends Throwable> E a(E e, E e2, ArrayDeque<StackTraceElement> arrayDeque) {
        arrayDeque.addFirst(a("Coroutine boundary"));
        StackTraceElement[] stackTrace = e.getStackTrace();
        wd4.a((Object) stackTrace, "causeTrace");
        String str = a;
        wd4.a((Object) str, "baseContinuationImplClassName");
        int a2 = a(stackTrace, str);
        int i = 0;
        if (a2 == -1) {
            Object[] array = arrayDeque.toArray(new StackTraceElement[0]);
            if (array != null) {
                e2.setStackTrace((StackTraceElement[]) array);
                return e2;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[(arrayDeque.size() + a2)];
        for (int i2 = 0; i2 < a2; i2++) {
            stackTraceElementArr[i2] = stackTrace[i2];
        }
        for (StackTraceElement stackTraceElement : arrayDeque) {
            stackTraceElementArr[a2 + i] = stackTraceElement;
            i++;
        }
        e2.setStackTrace(stackTraceElementArr);
        return e2;
    }

    @DexIgnore
    public static final <E extends Throwable> E b(E e) {
        wd4.b(e, "exception");
        if (!oh4.d()) {
            return e;
        }
        E cause = e.getCause();
        if (cause != null) {
            boolean z = true;
            if (!(!wd4.a((Object) cause.getClass(), (Object) e.getClass()))) {
                StackTraceElement[] stackTrace = e.getStackTrace();
                wd4.a((Object) stackTrace, "exception.stackTrace");
                int length = stackTrace.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i];
                    wd4.a((Object) stackTraceElement, "it");
                    if (a(stackTraceElement)) {
                        break;
                    }
                    i++;
                }
                if (z) {
                    return cause;
                }
            }
        }
        return e;
    }

    @DexIgnore
    public static final <E extends Throwable> Pair<E, StackTraceElement[]> a(E e) {
        boolean z;
        Throwable cause = e.getCause();
        if (cause == null || !wd4.a((Object) cause.getClass(), (Object) e.getClass())) {
            return ab4.a(e, new StackTraceElement[0]);
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        wd4.a((Object) stackTrace, "currentTrace");
        int length = stackTrace.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            }
            StackTraceElement stackTraceElement = stackTrace[i];
            wd4.a((Object) stackTraceElement, "it");
            if (a(stackTraceElement)) {
                z = true;
                break;
            }
            i++;
        }
        if (z) {
            return ab4.a(cause, stackTrace);
        }
        return ab4.a(e, new StackTraceElement[0]);
    }

    @DexIgnore
    public static final ArrayDeque<StackTraceElement> a(rc4 rc4) {
        ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<>();
        StackTraceElement stackTraceElement = rc4.getStackTraceElement();
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
        }
        while (true) {
            if (!(rc4 instanceof rc4)) {
                rc4 = null;
            }
            if (rc4 == null) {
                break;
            }
            rc4 = rc4.getCallerFrame();
            if (rc4 == null) {
                break;
            }
            StackTraceElement stackTraceElement2 = rc4.getStackTraceElement();
            if (stackTraceElement2 != null) {
                arrayDeque.add(stackTraceElement2);
            }
        }
        return arrayDeque;
    }

    @DexIgnore
    public static final StackTraceElement a(String str) {
        wd4.b(str, "message");
        return new StackTraceElement("\b\b\b(" + str, "\b", "\b", -1);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement) {
        wd4.b(stackTraceElement, "$this$isArtificial");
        String className = stackTraceElement.getClassName();
        wd4.a((Object) className, PinObject.COLUMN_CLASS_NAME);
        return cg4.c(className, "\b\b\b", false, 2, (Object) null);
    }

    @DexIgnore
    public static final boolean a(StackTraceElement stackTraceElement, StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && wd4.a((Object) stackTraceElement.getMethodName(), (Object) stackTraceElement2.getMethodName()) && wd4.a((Object) stackTraceElement.getFileName(), (Object) stackTraceElement2.getFileName()) && wd4.a((Object) stackTraceElement.getClassName(), (Object) stackTraceElement2.getClassName());
    }

    @DexIgnore
    public static final void a(StackTraceElement[] stackTraceElementArr, ArrayDeque<StackTraceElement> arrayDeque) {
        int length = stackTraceElementArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (a(stackTraceElementArr[i])) {
                break;
            } else {
                i++;
            }
        }
        int i2 = i + 1;
        int length2 = stackTraceElementArr.length - 1;
        if (length2 >= i2) {
            while (true) {
                StackTraceElement stackTraceElement = stackTraceElementArr[length2];
                StackTraceElement last = arrayDeque.getLast();
                wd4.a((Object) last, "result.last");
                if (a(stackTraceElement, last)) {
                    arrayDeque.removeLast();
                }
                arrayDeque.addFirst(stackTraceElementArr[length2]);
                if (length2 != i2) {
                    length2--;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public static final int a(StackTraceElement[] stackTraceElementArr, String str) {
        int length = stackTraceElementArr.length;
        for (int i = 0; i < length; i++) {
            if (wd4.a((Object) str, (Object) stackTraceElementArr[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }
}
