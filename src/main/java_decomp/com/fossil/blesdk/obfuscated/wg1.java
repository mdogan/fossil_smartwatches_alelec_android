package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wg1 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ ug1 d;

    @DexIgnore
    public wg1(ug1 ug1, int i, boolean z, boolean z2) {
        this.d = ug1;
        this.a = i;
        this.b = z;
        this.c = z2;
    }

    @DexIgnore
    public final void a(String str) {
        this.d.a(this.a, this.b, this.c, str, (Object) null, (Object) null, (Object) null);
    }

    @DexIgnore
    public final void a(String str, Object obj) {
        this.d.a(this.a, this.b, this.c, str, obj, (Object) null, (Object) null);
    }

    @DexIgnore
    public final void a(String str, Object obj, Object obj2) {
        this.d.a(this.a, this.b, this.c, str, obj, obj2, (Object) null);
    }

    @DexIgnore
    public final void a(String str, Object obj, Object obj2, Object obj3) {
        this.d.a(this.a, this.b, this.c, str, obj, obj2, obj3);
    }
}
