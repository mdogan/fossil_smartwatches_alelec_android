package com.fossil.blesdk.obfuscated;

import com.android.volley.Request;
import com.android.volley.VolleyError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wm {
    @DexIgnore
    void a(Request<?> request, VolleyError volleyError);

    @DexIgnore
    void a(Request<?> request, vm<?> vmVar);

    @DexIgnore
    void a(Request<?> request, vm<?> vmVar, Runnable runnable);
}
