package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class up2<T, A> {
    @DexIgnore
    public jd4<? super A, ? extends T> a;
    @DexIgnore
    public volatile T b;

    @DexIgnore
    public up2(jd4<? super A, ? extends T> jd4) {
        wd4.b(jd4, "creator");
        this.a = jd4;
    }

    @DexIgnore
    public final T a(A a2) {
        T t;
        T t2 = this.b;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            t = this.b;
            if (t == null) {
                jd4 jd4 = this.a;
                if (jd4 != null) {
                    t = jd4.invoke(a2);
                    this.b = t;
                    this.a = null;
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
