package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rc3 extends as2 implements qc3 {
    @DexIgnore
    public ur3<nc2> j;
    @DexIgnore
    public pc3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewHeartRateCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ rc3 a;

        @DexIgnore
        public b(rc3 rc3) {
            this.a = rc3;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wd4.b(calendar, "calendar");
            pc3 a2 = rc3.a(this.a);
            Date time = calendar.getTime();
            wd4.a((Object) time, "calendar.time");
            a2.a(time);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewHeartRateCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ rc3 a;

        @DexIgnore
        public c(rc3 rc3) {
            this.a = rc3;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            wd4.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
                Date time = calendar.getTime();
                wd4.a((Object) time, "it.time");
                wd4.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ pc3 a(rc3 rc3) {
        pc3 pc3 = rc3.k;
        if (pc3 != null) {
            return pc3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        nc2 nc2 = (nc2) ra.a(layoutInflater, R.layout.fragment_heartrate_overview_month, viewGroup, false, O0());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = nc2.q;
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        nc2.q.setOnCalendarMonthChanged(new b(this));
        nc2.q.setOnCalendarItemClickListener(new c(this));
        this.j = new ur3<>(this, nc2);
        ur3<nc2> ur3 = this.j;
        if (ur3 != null) {
            nc2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        pc3 pc3 = this.k;
        if (pc3 != null) {
            pc3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        pc3 pc3 = this.k;
        if (pc3 != null) {
            pc3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View p(int i) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        View view = (View) this.l.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.l.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(pc3 pc3) {
        wd4.b(pc3, "presenter");
        this.k = pc3;
    }

    @DexIgnore
    public void a(TreeMap<Long, Integer> treeMap) {
        wd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ur3<nc2> ur3 = this.j;
        if (ur3 != null) {
            nc2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = a2.q;
                if (recyclerViewHeartRateCalendar != null) {
                    recyclerViewHeartRateCalendar.setData(treeMap);
                }
            }
            ((RecyclerViewHeartRateCalendar) p(h62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        wd4.b(date, "selectDate");
        wd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ur3<nc2> ur3 = this.j;
        if (ur3 != null) {
            nc2 a2 = ur3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                wd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                wd4.a((Object) instance2, "startCalendar");
                instance2.setTime(sk2.n(date2));
                wd4.a((Object) instance3, "endCalendar");
                instance3.setTime(sk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
