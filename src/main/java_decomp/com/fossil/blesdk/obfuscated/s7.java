package com.fossil.blesdk.obfuscated;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface s7 {
    @DexIgnore
    Object a();

    @DexIgnore
    void a(Locale... localeArr);

    @DexIgnore
    boolean equals(Object obj);

    @DexIgnore
    Locale get(int i);

    @DexIgnore
    int hashCode();

    @DexIgnore
    String toString();
}
