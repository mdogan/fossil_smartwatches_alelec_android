package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ad0 implements Comparator {
    @DexIgnore
    public static /* final */ Comparator e; // = new ad0();

    @DexIgnore
    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).H().compareTo(((Scope) obj2).H());
    }
}
