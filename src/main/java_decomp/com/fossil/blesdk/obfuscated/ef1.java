package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ef1 extends b51 implements df1 {
    @DexIgnore
    public ef1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final me1 a(tn0 tn0, GoogleMapOptions googleMapOptions) throws RemoteException {
        me1 me1;
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        d51.a(o, (Parcelable) googleMapOptions);
        Parcel a = a(3, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            me1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            if (queryLocalInterface instanceof me1) {
                me1 = queryLocalInterface;
            } else {
                me1 = new hf1(readStrongBinder);
            }
        }
        a.recycle();
        return me1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final le1 c(tn0 tn0) throws RemoteException {
        le1 le1;
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        Parcel a = a(2, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            le1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            if (queryLocalInterface instanceof le1) {
                le1 = queryLocalInterface;
            } else {
                le1 = new gf1(readStrongBinder);
            }
        }
        a.recycle();
        return le1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final je1 zze() throws RemoteException {
        je1 je1;
        Parcel a = a(4, o());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            je1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            if (queryLocalInterface instanceof je1) {
                je1 = queryLocalInterface;
            } else {
                je1 = new ve1(readStrongBinder);
            }
        }
        a.recycle();
        return je1;
    }

    @DexIgnore
    public final e51 zzf() throws RemoteException {
        Parcel a = a(5, o());
        e51 a2 = f51.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(tn0 tn0, int i) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        o.writeInt(i);
        b(6, o);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final oe1 a(tn0 tn0, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        oe1 oe1;
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        d51.a(o, (Parcelable) streetViewPanoramaOptions);
        Parcel a = a(7, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            oe1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            if (queryLocalInterface instanceof oe1) {
                oe1 = queryLocalInterface;
            } else {
                oe1 = new ze1(readStrongBinder);
            }
        }
        a.recycle();
        return oe1;
    }
}
