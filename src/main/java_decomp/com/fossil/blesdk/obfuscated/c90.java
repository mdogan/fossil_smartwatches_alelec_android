package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c90 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<GattCharacteristic.CharacteristicId, f90> d; // = new Hashtable<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public final void a(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    public final byte[] b() {
        return this.a;
    }

    @DexIgnore
    public final void c(GattCharacteristic.CharacteristicId characteristicId) {
        wd4.b(characteristicId, "characteristicId");
        b(characteristicId).a(this.b, this.c);
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final f90 b(GattCharacteristic.CharacteristicId characteristicId) {
        f90 f90 = this.d.get(characteristicId);
        if (f90 == null) {
            f90 = new f90(characteristicId, this.b, this.c, 0, 0);
        }
        this.d.put(characteristicId, f90);
        return f90;
    }

    @DexIgnore
    public final void a(byte[] bArr, byte[] bArr2) {
        wd4.b(bArr, "phoneRandomNumber");
        wd4.b(bArr2, "deviceRandomNumber");
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final void a(GattCharacteristic.CharacteristicId characteristicId, int i) {
        wd4.b(characteristicId, "characteristicId");
        f90 f90 = this.d.get(characteristicId);
        if (f90 != null) {
            f90.a(i);
        }
    }

    @DexIgnore
    public final byte[] a(GattCharacteristic.CharacteristicId characteristicId) {
        wd4.b(characteristicId, "characteristicId");
        return b(characteristicId).a();
    }
}
