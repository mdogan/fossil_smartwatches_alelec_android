package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uq;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xq implements uq.a {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        File a();
    }

    @DexIgnore
    public xq(a aVar, long j) {
        this.a = j;
        this.b = aVar;
    }

    @DexIgnore
    public uq build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return yq.a(a2, this.a);
        }
        return null;
    }
}
