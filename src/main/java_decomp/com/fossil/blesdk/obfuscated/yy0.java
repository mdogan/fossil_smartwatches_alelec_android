package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yy0 extends ContentObserver {
    @DexIgnore
    public yy0(Handler handler) {
        super((Handler) null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        xy0.e.set(true);
    }
}
