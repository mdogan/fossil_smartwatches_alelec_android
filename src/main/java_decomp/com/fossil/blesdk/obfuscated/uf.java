package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import com.fossil.blesdk.obfuscated.ig;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uf extends ig.a {
    @DexIgnore
    public kf b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(hg hgVar);

        @DexIgnore
        public abstract void dropAllTables(hg hgVar);

        @DexIgnore
        public abstract void onCreate(hg hgVar);

        @DexIgnore
        public abstract void onOpen(hg hgVar);

        @DexIgnore
        public abstract void onPostMigrate(hg hgVar);

        @DexIgnore
        public abstract void onPreMigrate(hg hgVar);

        @DexIgnore
        public abstract void validateMigration(hg hgVar);
    }

    @DexIgnore
    public uf(kf kfVar, a aVar, String str, String str2) {
        super(aVar.version);
        this.b = kfVar;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean h(hg hgVar) {
        Cursor d2 = hgVar.d("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (d2.moveToFirst() && d2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public void a(hg hgVar) {
        super.a(hgVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public void b(hg hgVar, int i, int i2) {
        boolean z;
        kf kfVar = this.b;
        if (kfVar != null) {
            List<zf> a2 = kfVar.d.a(i, i2);
            if (a2 != null) {
                this.c.onPreMigrate(hgVar);
                for (zf migrate : a2) {
                    migrate.migrate(hgVar);
                }
                this.c.validateMigration(hgVar);
                this.c.onPostMigrate(hgVar);
                g(hgVar);
                z = true;
                if (z) {
                    kf kfVar2 = this.b;
                    if (kfVar2 == null || kfVar2.a(i, i2)) {
                        throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
                    }
                    this.c.dropAllTables(hgVar);
                    this.c.createAllTables(hgVar);
                    return;
                }
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    public void c(hg hgVar) {
        g(hgVar);
        this.c.createAllTables(hgVar);
        this.c.onCreate(hgVar);
    }

    @DexIgnore
    public void d(hg hgVar) {
        super.d(hgVar);
        e(hgVar);
        this.c.onOpen(hgVar);
        this.b = null;
    }

    @DexIgnore
    public final void e(hg hgVar) {
        String str = null;
        if (h(hgVar)) {
            Cursor a2 = hgVar.a(new gg("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a2.moveToFirst()) {
                    str = a2.getString(0);
                }
            } finally {
                a2.close();
            }
        }
        if (!this.d.equals(str) && !this.e.equals(str)) {
            throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
        }
    }

    @DexIgnore
    public final void f(hg hgVar) {
        hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void g(hg hgVar) {
        f(hgVar);
        hgVar.b(tf.a(this.d));
    }

    @DexIgnore
    public void a(hg hgVar, int i, int i2) {
        b(hgVar, i, i2);
    }
}
