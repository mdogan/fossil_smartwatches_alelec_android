package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yb4 implements Iterator<Integer>, de4 {
    @DexIgnore
    public abstract int a();

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final Integer next() {
        return Integer.valueOf(a());
    }
}
