package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k62 {
    @DexIgnore
    public /* final */ l62 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends j62.c, E extends j62.a> implements j62.d<R, E> {
        @DexIgnore
        public /* final */ j62.d<R, E> a;
        @DexIgnore
        public /* final */ k62 b;

        @DexIgnore
        public a(j62.d<R, E> dVar, k62 k62) {
            this.a = dVar;
            this.b = k62;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(R r) {
            this.b.a(r, this.a);
        }

        @DexIgnore
        public void a(E e) {
            this.b.a(e, this.a);
        }
    }

    @DexIgnore
    public k62(l62 l62) {
        this.a = l62;
    }

    @DexIgnore
    public <Q extends j62.b, R extends j62.c, E extends j62.a> void a(j62<Q, R, E> j62, Q q, j62.d<R, E> dVar) {
        j62.b(q);
        j62.a((j62.d<R, E>) new a(dVar, this));
        fs3.c();
        this.a.execute(new h42(j62));
    }

    @DexIgnore
    public static /* synthetic */ void a(j62 j62) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + fs3.b());
        j62.b();
        if (!fs3.b().a()) {
            fs3.a();
        }
    }

    @DexIgnore
    public <R extends j62.c, E extends j62.a> void a(R r, j62.d<R, E> dVar) {
        this.a.a(r, dVar);
    }

    @DexIgnore
    public <R extends j62.c, E extends j62.a> void a(E e, j62.d<R, E> dVar) {
        this.a.a(e, dVar);
    }
}
