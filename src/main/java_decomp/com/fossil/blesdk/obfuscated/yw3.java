package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.iv3;
import java.io.IOException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yw3 {
    @DexIgnore
    public static /* final */ Comparator<String> a; // = new a();
    @DexIgnore
    public static /* final */ String b; // = vv3.c().a();
    @DexIgnore
    public static /* final */ String c; // = (b + "-Sent-Millis");
    @DexIgnore
    public static /* final */ String d; // = (b + "-Received-Millis");
    @DexIgnore
    public static /* final */ String e; // = (b + "-Selected-Protocol");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<String> {
        @DexIgnore
        /* renamed from: a */
        public int compare(String str, String str2) {
            if (str == str2) {
                return 0;
            }
            if (str == null) {
                return -1;
            }
            if (str2 == null) {
                return 1;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(str, str2);
        }
    }

    @DexIgnore
    public static long a(iv3 iv3) {
        return a(iv3.c());
    }

    @DexIgnore
    public static long b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static Set<String> c(kv3 kv3) {
        return c(kv3.g());
    }

    @DexIgnore
    public static dv3 d(kv3 kv3) {
        return a(kv3.i().l().c(), kv3.g());
    }

    @DexIgnore
    public static long a(kv3 kv3) {
        return a(kv3.g());
    }

    @DexIgnore
    public static Map<String, List<String>> b(dv3 dv3, String str) {
        TreeMap treeMap = new TreeMap(a);
        int b2 = dv3.b();
        for (int i = 0; i < b2; i++) {
            String a2 = dv3.a(i);
            String b3 = dv3.b(i);
            ArrayList arrayList = new ArrayList();
            List list = (List) treeMap.get(a2);
            if (list != null) {
                arrayList.addAll(list);
            }
            arrayList.add(b3);
            treeMap.put(a2, Collections.unmodifiableList(arrayList));
        }
        if (str != null) {
            treeMap.put((Object) null, Collections.unmodifiableList(Collections.singletonList(str)));
        }
        return Collections.unmodifiableMap(treeMap);
    }

    @DexIgnore
    public static Set<String> c(dv3 dv3) {
        Set<String> emptySet = Collections.emptySet();
        int b2 = dv3.b();
        Set<String> set = emptySet;
        for (int i = 0; i < b2; i++) {
            if ("Vary".equalsIgnoreCase(dv3.a(i))) {
                String b3 = dv3.b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b3.split(",")) {
                    set.add(trim.trim());
                }
            }
        }
        return set;
    }

    @DexIgnore
    public static long a(dv3 dv3) {
        return b(dv3.a("Content-Length"));
    }

    @DexIgnore
    public static void a(iv3.b bVar, Map<String, List<String>> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (("Cookie".equalsIgnoreCase(str) || "Cookie2".equalsIgnoreCase(str)) && !((List) next.getValue()).isEmpty()) {
                bVar.a(str, a((List<String>) (List) next.getValue()));
            }
        }
    }

    @DexIgnore
    public static String a(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(kv3 kv3) {
        return b(kv3.g());
    }

    @DexIgnore
    public static boolean b(dv3 dv3) {
        return c(dv3).contains(Marker.ANY_MARKER);
    }

    @DexIgnore
    public static boolean a(kv3 kv3, dv3 dv3, iv3 iv3) {
        for (String next : c(kv3)) {
            if (!xv3.a((Object) dv3.c(next), (Object) iv3.b(next))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static dv3 a(dv3 dv3, dv3 dv32) {
        Set<String> c2 = c(dv32);
        if (c2.isEmpty()) {
            return new dv3.b().a();
        }
        dv3.b bVar = new dv3.b();
        int b2 = dv3.b();
        for (int i = 0; i < b2; i++) {
            String a2 = dv3.a(i);
            if (c2.contains(a2)) {
                bVar.a(a2, dv3.b(i));
            }
        }
        return bVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static List<wu3> a(dv3 dv3, String str) {
        ArrayList arrayList = new ArrayList();
        int b2 = dv3.b();
        for (int i = 0; i < b2; i++) {
            if (str.equalsIgnoreCase(dv3.a(i))) {
                String b3 = dv3.b(i);
                int i2 = 0;
                while (i2 < b3.length()) {
                    int a2 = sw3.a(b3, i2, " ");
                    String trim = b3.substring(i2, a2).trim();
                    int b4 = sw3.b(b3, a2);
                    if (!b3.regionMatches(true, b4, "realm=\"", 0, 7)) {
                        break;
                    }
                    int i3 = b4 + 7;
                    int a3 = sw3.a(b3, i3, "\"");
                    String substring = b3.substring(i3, a3);
                    i2 = sw3.b(b3, sw3.a(b3, a3 + 1, ",") + 1);
                    arrayList.add(new wu3(trim, substring));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static iv3 a(ru3 ru3, kv3 kv3, Proxy proxy) throws IOException {
        if (kv3.e() == 407) {
            return ru3.a(proxy, kv3);
        }
        return ru3.b(proxy, kv3);
    }
}
