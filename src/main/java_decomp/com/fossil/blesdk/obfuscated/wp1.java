package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wp1 extends IInterface {
    @DexIgnore
    void a(aq1 aq1) throws RemoteException;

    @DexIgnore
    void a(dq1 dq1) throws RemoteException;

    @DexIgnore
    void a(fq1 fq1) throws RemoteException;

    @DexIgnore
    void a(lp1 lp1) throws RemoteException;

    @DexIgnore
    void a(pp1 pp1) throws RemoteException;

    @DexIgnore
    void a(yp1 yp1) throws RemoteException;

    @DexIgnore
    void a(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void b(aq1 aq1) throws RemoteException;

    @DexIgnore
    void b(List<aq1> list) throws RemoteException;
}
