package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hn0 extends wl0 {
    @DexIgnore
    public int e;

    @DexIgnore
    public hn0(byte[] bArr) {
        ck0.a(bArr.length == 25);
        this.e = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] b(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof vl0)) {
            try {
                vl0 vl0 = (vl0) obj;
                if (vl0.zzc() != hashCode()) {
                    return false;
                }
                tn0 zzb = vl0.zzb();
                if (zzb == null) {
                    return false;
                }
                return Arrays.equals(o(), (byte[]) vn0.d(zzb));
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.e;
    }

    @DexIgnore
    public abstract byte[] o();

    @DexIgnore
    public final tn0 zzb() {
        return vn0.a(o());
    }

    @DexIgnore
    public final int zzc() {
        return hashCode();
    }
}
