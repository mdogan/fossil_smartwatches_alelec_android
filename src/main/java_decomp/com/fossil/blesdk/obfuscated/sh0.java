package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sh0 implements IBinder.DeathRecipient, th0 {
    @DexIgnore
    public /* final */ WeakReference<BasePendingResult<?>> a;
    @DexIgnore
    public /* final */ WeakReference<wi0> b;
    @DexIgnore
    public /* final */ WeakReference<IBinder> c;

    @DexIgnore
    public sh0(BasePendingResult<?> basePendingResult, wi0 wi0, IBinder iBinder) {
        this.b = new WeakReference<>(wi0);
        this.a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    @DexIgnore
    public final void a(BasePendingResult<?> basePendingResult) {
        a();
    }

    @DexIgnore
    public final void binderDied() {
        a();
    }

    @DexIgnore
    public final void a() {
        BasePendingResult basePendingResult = (BasePendingResult) this.a.get();
        wi0 wi0 = (wi0) this.b.get();
        if (!(wi0 == null || basePendingResult == null)) {
            wi0.a(basePendingResult.e().intValue());
        }
        IBinder iBinder = (IBinder) this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    @DexIgnore
    public /* synthetic */ sh0(BasePendingResult basePendingResult, wi0 wi0, IBinder iBinder, rh0 rh0) {
        this(basePendingResult, (wi0) null, iBinder);
    }
}
