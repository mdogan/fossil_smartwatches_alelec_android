package com.fossil.blesdk.obfuscated;

import android.database.sqlite.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qg extends pg implements lg {
    @DexIgnore
    public /* final */ SQLiteStatement f;

    @DexIgnore
    public qg(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.f = sQLiteStatement;
    }

    @DexIgnore
    public int n() {
        return this.f.executeUpdateDelete();
    }

    @DexIgnore
    public long o() {
        return this.f.executeInsert();
    }
}
