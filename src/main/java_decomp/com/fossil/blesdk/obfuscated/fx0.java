package com.fossil.blesdk.obfuscated;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fx0 extends AbstractList<String> implements ev0, RandomAccess {
    @DexIgnore
    public /* final */ ev0 e;

    @DexIgnore
    public fx0(ev0 ev0) {
        this.e = ev0;
    }

    @DexIgnore
    public final List<?> D() {
        return this.e.D();
    }

    @DexIgnore
    public final ev0 F() {
        return this;
    }

    @DexIgnore
    public final Object e(int i) {
        return this.e.e(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return (String) this.e.get(i);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new hx0(this);
    }

    @DexIgnore
    public final ListIterator<String> listIterator(int i) {
        return new gx0(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }
}
