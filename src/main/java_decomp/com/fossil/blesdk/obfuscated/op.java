package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.engine.DecodeJob;
import com.fossil.blesdk.obfuscated.tr;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class op<Transcode> {
    @DexIgnore
    public /* final */ List<tr.a<?>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<ko> b; // = new ArrayList();
    @DexIgnore
    public un c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public DecodeJob.e h;
    @DexIgnore
    public mo i;
    @DexIgnore
    public Map<Class<?>, po<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ko n;
    @DexIgnore
    public Priority o;
    @DexIgnore
    public qp p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    public <R> void a(un unVar, Object obj, ko koVar, int i2, int i3, qp qpVar, Class<?> cls, Class<R> cls2, Priority priority, mo moVar, Map<Class<?>, po<?>> map, boolean z, boolean z2, DecodeJob.e eVar) {
        this.c = unVar;
        this.d = obj;
        this.n = koVar;
        this.e = i2;
        this.f = i3;
        this.p = qpVar;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = priority;
        this.i = moVar;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public hq b() {
        return this.c.a();
    }

    @DexIgnore
    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    @DexIgnore
    public uq d() {
        return this.h.a();
    }

    @DexIgnore
    public qp e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<tr.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List a2 = this.c.f().a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                tr.a a3 = ((tr) a2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a3 != null) {
                    this.a.add(a3);
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public Class<?> h() {
        return this.d.getClass();
    }

    @DexIgnore
    public mo i() {
        return this.i;
    }

    @DexIgnore
    public Priority j() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> k() {
        return this.c.f().c(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public ko l() {
        return this.n;
    }

    @DexIgnore
    public Class<?> m() {
        return this.k;
    }

    @DexIgnore
    public int n() {
        return this.e;
    }

    @DexIgnore
    public boolean o() {
        return this.r;
    }

    @DexIgnore
    public <Z> po<Z> b(Class<Z> cls) {
        po<Z> poVar = this.j.get(cls);
        if (poVar == null) {
            Iterator<Map.Entry<Class<?>, po<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    poVar = (po) next.getValue();
                    break;
                }
            }
        }
        if (poVar != null) {
            return poVar;
        }
        if (!this.j.isEmpty() || !this.q) {
            return ls.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public List<ko> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<tr.a<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                tr.a aVar = g2.get(i2);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public boolean b(bq<?> bqVar) {
        return this.c.f().b(bqVar);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public <Data> zp<Data, ?, Transcode> a(Class<Data> cls) {
        return this.c.f().b(cls, this.g, this.k);
    }

    @DexIgnore
    public <Z> oo<Z> a(bq<Z> bqVar) {
        return this.c.f().a(bqVar);
    }

    @DexIgnore
    public List<tr<File, ?>> a(File file) throws Registry.NoModelLoaderAvailableException {
        return this.c.f().a(file);
    }

    @DexIgnore
    public boolean a(ko koVar) {
        List<tr.a<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).a.equals(koVar)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public <X> io<X> a(X x) throws Registry.NoSourceEncoderAvailableException {
        return this.c.f().c(x);
    }
}
