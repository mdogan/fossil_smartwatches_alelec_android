package com.fossil.blesdk.obfuscated;

import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g5 {
    @DexIgnore
    public HashSet<g5> a; // = new HashSet<>(2);
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public void a(g5 g5Var) {
        this.a.add(g5Var);
    }

    @DexIgnore
    public void b() {
        this.b = 0;
        Iterator<g5> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().b();
        }
    }

    @DexIgnore
    public boolean c() {
        return this.b == 1;
    }

    @DexIgnore
    public void d() {
        this.b = 0;
        this.a.clear();
    }

    @DexIgnore
    public void e() {
    }

    @DexIgnore
    public void a() {
        this.b = 1;
        Iterator<g5> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().e();
        }
    }
}
