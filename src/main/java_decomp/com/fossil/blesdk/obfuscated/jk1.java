package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ lg1 e;
    @DexIgnore
    public /* final */ /* synthetic */ ik1 f;

    @DexIgnore
    public jk1(ik1 ik1, lg1 lg1) {
        this.f = ik1;
        this.e = lg1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f) {
            boolean unused = this.f.a = false;
            if (!this.f.c.B()) {
                this.f.c.d().A().a("Connected to service");
                this.f.c.a(this.e);
            }
        }
    }
}
