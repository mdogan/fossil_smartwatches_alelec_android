package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.pm4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.util.UserUtils;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o42 {
    @DexIgnore
    public /* final */ PortfolioApp a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ o42 a;

        @DexIgnore
        public a(o42 o42) {
            this.a = o42;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            pm4.a f = chain.n().f();
            f.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
            f.a("User-Agent", zi2.b.a());
            f.a("locale", this.a.a.m());
            return chain.a(f.a());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ o42 a;

        @DexIgnore
        public b(o42 o42) {
            this.a = o42;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            pm4.a f = chain.n().f();
            f.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
            f.a("User-Agent", zi2.b.a());
            f.a("locale", this.a.a.m());
            return chain.a(f.a());
        }
    }

    @DexIgnore
    public o42(PortfolioApp portfolioApp) {
        wd4.b(portfolioApp, "mApplication");
        this.a = portfolioApp;
    }

    @DexIgnore
    public final PortfolioApp b() {
        return this.a;
    }

    @DexIgnore
    public final Context c() {
        Context applicationContext = this.a.getApplicationContext();
        wd4.a((Object) applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ShortcutApiService d(yo2 yo2, cp2 cp2) {
        wd4.b(yo2, "interceptor");
        wd4.b(cp2, "authenticator");
        return (ShortcutApiService) a(yo2, cp2, ShortcutApiService.class);
    }

    @DexIgnore
    public final ContentResolver e() {
        ContentResolver contentResolver = this.a.getContentResolver();
        wd4.a((Object) contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final jn2 f() {
        return new jn2();
    }

    @DexIgnore
    public final FirmwareFileRepository g() {
        Context applicationContext = this.a.getApplicationContext();
        wd4.a((Object) applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    public final GuestApiService h() {
        b bVar = new b(this);
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((Interceptor) bVar);
        return (GuestApiService) bp2.g.a(GuestApiService.class);
    }

    @DexIgnore
    public final sc i() {
        sc a2 = sc.a((Context) this.a);
        wd4.a((Object) a2, "LocalBroadcastManager.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final kn2 j() {
        return new kn2();
    }

    @DexIgnore
    public final g42 k() {
        g42 a2 = g42.a((Context) this.a);
        wd4.a((Object) a2, "MFLocationService.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final en2 l() {
        return en2.p.a();
    }

    @DexIgnore
    public final k62 m() {
        return new k62(new m62());
    }

    @DexIgnore
    public final UserUtils n() {
        return UserUtils.d.a();
    }

    @DexIgnore
    public final rl2 o() {
        rl2 b2 = rl2.b();
        wd4.a((Object) b2, "WatchHelper.getInstance()");
        return b2;
    }

    @DexIgnore
    public final MFLoginWechatManager p() {
        return new MFLoginWechatManager();
    }

    @DexIgnore
    public final ln2 q() {
        return new ln2();
    }

    @DexIgnore
    public final fn2 a(Context context) {
        wd4.b(context, "context");
        return new fn2(context);
    }

    @DexIgnore
    public final AuthApiUserService b(yo2 yo2, cp2 cp2) {
        wd4.b(yo2, "interceptor");
        wd4.b(cp2, "authenticator");
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((tl4) cp2);
        bp2.g.a((Interceptor) yo2);
        return (AuthApiUserService) bp2.g.a(AuthApiUserService.class);
    }

    @DexIgnore
    public final GoogleApiService c(yo2 yo2, cp2 cp2) {
        wd4.b(yo2, "interceptor");
        wd4.b(cp2, "authenticator");
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 4) + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((tl4) cp2);
        bp2.g.a((Interceptor) yo2);
        return (GoogleApiService) bp2.g.a(GoogleApiService.class);
    }

    @DexIgnore
    public final AuthApiGuestService d() {
        a aVar = new a(this);
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((Interceptor) aVar);
        return (AuthApiGuestService) bp2.g.a(AuthApiGuestService.class);
    }

    @DexIgnore
    public final ApplicationEventListener a(PortfolioApp portfolioApp, fn2 fn2, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, wj2 wj2, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository) {
        PortfolioApp portfolioApp2 = portfolioApp;
        wd4.b(portfolioApp2, "app");
        fn2 fn22 = fn2;
        wd4.b(fn22, "sharedPreferencesManager");
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        wd4.b(hybridPresetRepository2, "hybridPresetRepository");
        CategoryRepository categoryRepository2 = categoryRepository;
        wd4.b(categoryRepository2, "categoryRepository");
        WatchAppRepository watchAppRepository2 = watchAppRepository;
        wd4.b(watchAppRepository2, "watchAppRepository");
        ComplicationRepository complicationRepository2 = complicationRepository;
        wd4.b(complicationRepository2, "complicationRepository");
        MicroAppRepository microAppRepository2 = microAppRepository;
        wd4.b(microAppRepository2, "microAppRepository");
        DianaPresetRepository dianaPresetRepository2 = dianaPresetRepository;
        wd4.b(dianaPresetRepository2, "dianaPresetRepository");
        DeviceRepository deviceRepository2 = deviceRepository;
        wd4.b(deviceRepository2, "deviceRepository");
        UserRepository userRepository2 = userRepository;
        wd4.b(userRepository2, "userRepository");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        wd4.b(alarmsRepository2, "alarmsRepository");
        wj2 wj22 = wj2;
        wd4.b(wj22, "deviceSettingFactory");
        WatchFaceRepository watchFaceRepository2 = watchFaceRepository;
        wd4.b(watchFaceRepository2, "watchFaceRepository");
        WatchLocalizationRepository watchLocalizationRepository2 = watchLocalizationRepository;
        wd4.b(watchLocalizationRepository2, "watchLocalizationRepository");
        return new ApplicationEventListener(portfolioApp2, fn22, hybridPresetRepository2, categoryRepository2, watchAppRepository2, complicationRepository2, microAppRepository2, dianaPresetRepository2, deviceRepository2, userRepository2, wj22, alarmsRepository2, watchFaceRepository2, watchLocalizationRepository2);
    }

    @DexIgnore
    public final WatchParamHelper a(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        wd4.b(deviceRepository, "deviceRepository");
        wd4.b(portfolioApp, "app");
        return new WatchParamHelper(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final AnalyticsHelper a() {
        return AnalyticsHelper.f.c();
    }

    @DexIgnore
    public final AlarmHelper a(Context context, fn2 fn2, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        wd4.b(context, "context");
        wd4.b(fn2, "sharedPreferencesManager");
        wd4.b(userRepository, "userRepository");
        wd4.b(alarmsRepository, "alarmsRepository");
        return new AlarmHelper(context, fn2, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final al2 a(Context context, i42 i42, fn2 fn2) {
        wd4.b(context, "context");
        wd4.b(i42, "appExecutors");
        wd4.b(fn2, "sharedPreferencesManager");
        return new al2(context, i42.b(), fn2);
    }

    @DexIgnore
    public final <S> S a(yo2 yo2, cp2 cp2, Class<S> cls) {
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 0) + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((tl4) cp2);
        bp2.g.a((Interceptor) yo2);
        return bp2.g.a(cls);
    }

    @DexIgnore
    public final ApiServiceV2 a(yo2 yo2, cp2 cp2) {
        wd4.b(yo2, "interceptor");
        wd4.b(cp2, "authenticator");
        bp2 bp2 = bp2.g;
        bp2.a(vr3.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        bp2 bp22 = bp2.g;
        File cacheDir = this.a.getCacheDir();
        wd4.a((Object) cacheDir, "mApplication.cacheDir");
        bp22.a(cacheDir);
        bp2.g.a((tl4) cp2);
        bp2.g.a((Interceptor) yo2);
        return (ApiServiceV2) bp2.g.a(ApiServiceV2.class);
    }

    @DexIgnore
    public final MigrationManager a(fn2 fn2, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, wj2 wj2, AlarmsRepository alarmsRepository) {
        wd4.b(fn2, "mSharedPrefs");
        UserRepository userRepository2 = userRepository;
        wd4.b(userRepository2, "mUserRepository");
        GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase2 = getHybridDeviceSettingUseCase;
        wd4.b(getHybridDeviceSettingUseCase2, "getHybridUseCase");
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        wd4.b(notificationsRepository2, "mNotificationRepository");
        PortfolioApp portfolioApp2 = portfolioApp;
        wd4.b(portfolioApp2, "mApp");
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        wd4.b(goalTrackingRepository2, "goalTrackingRepo");
        GoalTrackingDatabase goalTrackingDatabase2 = goalTrackingDatabase;
        wd4.b(goalTrackingDatabase2, "goalTrackingDatabase");
        DeviceDao deviceDao2 = deviceDao;
        wd4.b(deviceDao2, "deviceDao");
        HybridCustomizeDatabase hybridCustomizeDatabase2 = hybridCustomizeDatabase;
        wd4.b(hybridCustomizeDatabase2, "mHybridCustomizeDatabase");
        MicroAppLastSettingRepository microAppLastSettingRepository2 = microAppLastSettingRepository;
        wd4.b(microAppLastSettingRepository2, "microAppLastSettingRepository");
        wj2 wj22 = wj2;
        wd4.b(wj22, "deviceSettingFactory");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        wd4.b(alarmsRepository2, "alarmsRepository");
        return new MigrationManager(fn2, userRepository2, notificationsRepository2, getHybridDeviceSettingUseCase2, portfolioApp2, goalTrackingRepository2, goalTrackingDatabase2, deviceDao2, hybridCustomizeDatabase2, microAppLastSettingRepository2, wj22, alarmsRepository2);
    }

    @DexIgnore
    public final c62 a(fn2 fn2, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        wd4.b(fn2, "mSharedPrefs");
        wd4.b(userRepository, "userRepository");
        wd4.b(hybridPresetDao, "hybridPresetDao");
        wd4.b(notificationsRepository, "notificationsRepository");
        wd4.b(deviceDao, "deviceDao");
        wd4.b(portfolioApp, "app");
        return new c62(fn2, userRepository, hybridPresetDao, deviceDao, notificationsRepository, portfolioApp);
    }

    @DexIgnore
    public final MigrationHelper a(fn2 fn2, MigrationManager migrationManager, c62 c62) {
        wd4.b(fn2, "mSharedPrefs");
        wd4.b(migrationManager, "migrationManager");
        wd4.b(c62, "legacyMigrationManager");
        return new MigrationHelper(fn2, migrationManager, c62);
    }

    @DexIgnore
    public final LinkStreamingManager a(HybridPresetRepository hybridPresetRepository, wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, fn2 fn2, AlarmsRepository alarmsRepository, SetNotificationUseCase setNotificationUseCase) {
        wd4.b(hybridPresetRepository, "hybridPresetRepository");
        wd4.b(wy2, "getApps");
        wd4.b(qx2, "getAllContactGroups");
        wd4.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        wd4.b(notificationsRepository2, "notificationsRepository");
        DeviceRepository deviceRepository2 = deviceRepository;
        wd4.b(deviceRepository2, "deviceRepository");
        fn2 fn22 = fn2;
        wd4.b(fn22, "sharedPref");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        wd4.b(alarmsRepository2, "alarmsRepository");
        SetNotificationUseCase setNotificationUseCase2 = setNotificationUseCase;
        wd4.b(setNotificationUseCase2, "setNotificationUseCase");
        return new LinkStreamingManager(hybridPresetRepository, wy2, qx2, notificationSettingsDatabase, notificationsRepository2, deviceRepository2, fn22, alarmsRepository2, setNotificationUseCase2);
    }

    @DexIgnore
    public final bk3 a(InAppNotificationRepository inAppNotificationRepository) {
        wd4.b(inAppNotificationRepository, "repository");
        return new bk3(inAppNotificationRepository);
    }
}
