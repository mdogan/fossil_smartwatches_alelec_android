package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kq0 extends Service {
    @DexIgnore
    public a e;

    @DexIgnore
    public abstract List<ap0> a(List<DataType> list);

    @DexIgnore
    @TargetApi(19)
    public final void a() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (qm0.e()) {
            ((AppOpsManager) getSystemService("appops")).checkPackage(callingUid, "com.google.android.gms");
            return;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        if (packagesForUid != null) {
            int length = packagesForUid.length;
            int i = 0;
            while (i < length) {
                if (!packagesForUid[i].equals("com.google.android.gms")) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new SecurityException("Unauthorized caller");
    }

    @DexIgnore
    public abstract boolean a(ap0 ap0);

    @DexIgnore
    public abstract boolean a(lq0 lq0);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.fitness.service.FitnessSensorService".equals(intent.getAction())) {
            return null;
        }
        if (Log.isLoggable("FitnessSensorService", 3)) {
            String valueOf = String.valueOf(intent);
            String name = kq0.class.getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20 + String.valueOf(name).length());
            sb.append("Intent ");
            sb.append(valueOf);
            sb.append(" received by ");
            sb.append(name);
            Log.d("FitnessSensorService", sb.toString());
        }
        a aVar = this.e;
        aVar.asBinder();
        return aVar;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.e = new a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends c21 {
        @DexIgnore
        public /* final */ kq0 e;

        @DexIgnore
        public a(kq0 kq0) {
            this.e = kq0;
        }

        @DexIgnore
        public final void a(x11 x11, p01 p01) throws RemoteException {
            this.e.a();
            p01.a(new iq0(this.e.a(x11.H()), Status.i));
        }

        @DexIgnore
        public final void a(lq0 lq0, h11 h11) throws RemoteException {
            this.e.a();
            if (this.e.a(lq0)) {
                h11.c(Status.i);
            } else {
                h11.c(new Status(13));
            }
        }

        @DexIgnore
        public final void a(z11 z11, h11 h11) throws RemoteException {
            this.e.a();
            if (this.e.a(z11.H())) {
                h11.c(Status.i);
            } else {
                h11.c(new Status(13));
            }
        }
    }
}
