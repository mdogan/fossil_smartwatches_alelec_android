package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b23 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public ArrayList<g13> a;
    @DexIgnore
    public d b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public View e;
        @DexIgnore
        public TextView f;
        @DexIgnore
        public /* final */ /* synthetic */ b23 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(b23 b23, View view) {
            super(view);
            wd4.b(view, "view");
            this.g = b23;
            View findViewById = view.findViewById(R.id.btn_add);
            wd4.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.e = findViewById;
            View findViewById2 = view.findViewById(R.id.tv_create_new_preset);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.f = (TextView) findViewById2;
            this.e.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_grey));
                this.e.setClickable(false);
                this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_MaximumPresets_Actions_Text__YouveSavedMaximumNumberOfPresets));
                return;
            }
            this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_black));
            this.e.setClickable(true);
            this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwipedCreate_Text__CreateANewPreset));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == R.id.btn_add) {
                this.g.b().z0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2);

        @DexIgnore
        void b(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void c(String str, String str2);

        @DexIgnore
        void y0();

        @DexIgnore
        void z0();
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public b23(ArrayList<g13> arrayList, d dVar) {
        wd4.b(arrayList, "mData");
        wd4.b(dVar, "mListener");
        this.a = arrayList;
        this.b = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final d b() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.a.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.a.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            g13 g13 = this.a.get(i);
            wd4.a((Object) g13, "mData[position]");
            g13 g132 = g13;
            if (i != 0) {
                z = false;
            }
            bVar.a(g132, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.a.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_diana_customize_preset_detail, viewGroup, false);
            wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_customize_preset_add, viewGroup, false);
        wd4.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(List<g13> list) {
        wd4.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + list);
        this.a.clear();
        this.a.addAll(list);
        this.a.add(new g13("", "", new ArrayList(), new ArrayList(), false, (WatchFaceWrapper) null, 32, (rd4) null));
        notifyDataSetChanged();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView e;
        @DexIgnore
        public CustomizeWidget f;
        @DexIgnore
        public CustomizeWidget g;
        @DexIgnore
        public CustomizeWidget h;
        @DexIgnore
        public CustomizeWidget i;
        @DexIgnore
        public View j;
        @DexIgnore
        public ImageView k;
        @DexIgnore
        public CustomizeWidget l;
        @DexIgnore
        public CustomizeWidget m;
        @DexIgnore
        public CustomizeWidget n;
        @DexIgnore
        public ImageButton o;
        @DexIgnore
        public TextView p;
        @DexIgnore
        public View q;
        @DexIgnore
        public TextView r;
        @DexIgnore
        public View s;
        @DexIgnore
        public View t;
        @DexIgnore
        public View u;
        @DexIgnore
        public View v;
        @DexIgnore
        public g13 w;
        @DexIgnore
        public /* final */ /* synthetic */ b23 x;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(b23 b23, View view) {
            super(view);
            wd4.b(view, "view");
            this.x = b23;
            View findViewById = view.findViewById(R.id.tv_preset_name);
            wd4.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.e = (TextView) findViewById;
            this.e.setOnClickListener(this);
            wd4.a((Object) view.findViewById(R.id.cl_complications), "view.findViewById(R.id.cl_complications)");
            wd4.a((Object) view.findViewById(R.id.cl_preset_buttons), "view.findViewById(R.id.cl_preset_buttons)");
            View findViewById2 = view.findViewById(R.id.iv_watch_hands_background);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.iv_watch_hands_background)");
            this.j = findViewById2;
            View findViewById3 = view.findViewById(R.id.iv_watch_theme_background);
            wd4.a((Object) findViewById3, "view.findViewById(R.id.iv_watch_theme_background)");
            this.k = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(R.id.wc_top);
            wd4.a((Object) findViewById4, "view.findViewById(R.id.wc_top)");
            this.f = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(R.id.wc_bottom);
            wd4.a((Object) findViewById5, "view.findViewById(R.id.wc_bottom)");
            this.g = (CustomizeWidget) findViewById5;
            View findViewById6 = view.findViewById(R.id.wc_start);
            wd4.a((Object) findViewById6, "view.findViewById(R.id.wc_start)");
            this.h = (CustomizeWidget) findViewById6;
            View findViewById7 = view.findViewById(R.id.wc_end);
            wd4.a((Object) findViewById7, "view.findViewById(R.id.wc_end)");
            this.i = (CustomizeWidget) findViewById7;
            View findViewById8 = view.findViewById(R.id.ib_edit_themes);
            wd4.a((Object) findViewById8, "view.findViewById(R.id.ib_edit_themes)");
            this.o = (ImageButton) findViewById8;
            View findViewById9 = view.findViewById(R.id.v_underline);
            wd4.a((Object) findViewById9, "view.findViewById(R.id.v_underline)");
            this.v = findViewById9;
            View findViewById10 = view.findViewById(R.id.wa_top);
            wd4.a((Object) findViewById10, "view.findViewById(R.id.wa_top)");
            this.l = (CustomizeWidget) findViewById10;
            View findViewById11 = view.findViewById(R.id.wa_middle);
            wd4.a((Object) findViewById11, "view.findViewById(R.id.wa_middle)");
            this.m = (CustomizeWidget) findViewById11;
            View findViewById12 = view.findViewById(R.id.wa_bottom);
            wd4.a((Object) findViewById12, "view.findViewById(R.id.wa_bottom)");
            this.n = (CustomizeWidget) findViewById12;
            View findViewById13 = view.findViewById(R.id.line_bottom);
            wd4.a((Object) findViewById13, "view.findViewById(R.id.line_bottom)");
            this.u = findViewById13;
            View findViewById14 = view.findViewById(R.id.line_center);
            wd4.a((Object) findViewById14, "view.findViewById(R.id.line_center)");
            this.t = findViewById14;
            View findViewById15 = view.findViewById(R.id.line_top);
            wd4.a((Object) findViewById15, "view.findViewById(R.id.line_top)");
            this.s = findViewById15;
            View findViewById16 = view.findViewById(R.id.layout_right);
            wd4.a((Object) findViewById16, "view.findViewById(R.id.layout_right)");
            this.q = findViewById16;
            View findViewById17 = view.findViewById(R.id.tv_right);
            wd4.a((Object) findViewById17, "view.findViewById(R.id.tv_right)");
            this.r = (TextView) findViewById17;
            View findViewById18 = view.findViewById(R.id.tv_left);
            wd4.a((Object) findViewById18, "view.findViewById(R.id.tv_left)");
            this.p = (TextView) findViewById18;
            this.q.setOnClickListener(this);
            this.p.setOnClickListener(this);
            this.f.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.h.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.o.setOnClickListener(this);
            this.l.setOnClickListener(this);
            this.m.setOnClickListener(this);
            this.n.setOnClickListener(this);
        }

        @DexIgnore
        public final List<g8<View, String>> a() {
            g8[] g8VarArr = new g8[7];
            TextView textView = this.e;
            g8VarArr[0] = new g8(textView, textView.getTransitionName());
            View view = this.j;
            g8VarArr[1] = new g8(view, view.getTransitionName());
            ImageView imageView = this.k;
            if (imageView != null) {
                g8VarArr[2] = new g8(imageView, imageView.getTransitionName());
                View view2 = this.v;
                g8VarArr[3] = new g8(view2, view2.getTransitionName());
                View view3 = this.u;
                g8VarArr[4] = new g8(view3, view3.getTransitionName());
                View view4 = this.t;
                g8VarArr[5] = new g8(view4, view4.getTransitionName());
                View view5 = this.s;
                g8VarArr[6] = new g8(view5, view5.getTransitionName());
                return ob4.c(g8VarArr);
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<g8<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.f;
            CustomizeWidget customizeWidget2 = this.g;
            CustomizeWidget customizeWidget3 = this.i;
            CustomizeWidget customizeWidget4 = this.h;
            CustomizeWidget customizeWidget5 = this.l;
            CustomizeWidget customizeWidget6 = this.m;
            CustomizeWidget customizeWidget7 = this.n;
            return ob4.c(new g8(customizeWidget, customizeWidget.getTransitionName()), new g8(customizeWidget2, customizeWidget2.getTransitionName()), new g8(customizeWidget3, customizeWidget3.getTransitionName()), new g8(customizeWidget4, customizeWidget4.getTransitionName()), new g8(customizeWidget5, customizeWidget5.getTransitionName()), new g8(customizeWidget6, customizeWidget6.getTransitionName()), new g8(customizeWidget7, customizeWidget7.getTransitionName()));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0104, code lost:
            if (r0 != null) goto L_0x0108;
         */
        @DexIgnore
        public void onClick(View view) {
            String str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.w == null) && view != null) {
                switch (view.getId()) {
                    case R.id.ib_edit_themes /*2131362344*/:
                        this.x.b().b(this.w, a(), b());
                        return;
                    case R.id.layout_right /*2131362478*/:
                        this.x.b().y0();
                        return;
                    case R.id.tv_left /*2131362897*/:
                        if (this.x.a.size() > 2) {
                            Object obj = this.x.a.get(1);
                            wd4.a(obj, "mData[1]");
                            g13 g13 = (g13) obj;
                            d b = this.x.b();
                            g13 g132 = this.w;
                            if (g132 != null) {
                                boolean b2 = g132.b();
                                g13 g133 = this.w;
                                if (g133 != null) {
                                    b.b(b2, g133.d(), g13.d(), g13.c());
                                    return;
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case R.id.tv_preset_name /*2131362920*/:
                        d b3 = this.x.b();
                        g13 g134 = this.w;
                        String str2 = "";
                        if (g134 != null) {
                            str = g134.d();
                            break;
                        }
                        str = str2;
                        g13 g135 = this.w;
                        if (g135 != null) {
                            String c = g135.c();
                            if (c != null) {
                                str2 = c;
                            }
                        }
                        b3.c(str, str2);
                        return;
                    case R.id.wa_bottom /*2131363029*/:
                        this.x.b().a(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wa_middle /*2131363030*/:
                        this.x.b().a(this.w, a(), b(), "middle", getAdapterPosition());
                        return;
                    case R.id.wa_top /*2131363031*/:
                        this.x.b().a(this.w, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    case R.id.wc_bottom /*2131363032*/:
                        this.x.b().b(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wc_end /*2131363036*/:
                        this.x.b().b(this.w, a(), b(), "right", getAdapterPosition());
                        return;
                    case R.id.wc_start /*2131363039*/:
                        this.x.b().b(this.w, a(), b(), ViewHierarchy.DIMENSION_LEFT_KEY, getAdapterPosition());
                        return;
                    case R.id.wc_top /*2131363040*/:
                        this.x.b().b(this.w, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0146  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x019c  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01f1  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0246  */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0290  */
        public final void a(g13 g13, boolean z) {
            Iterator it;
            WatchFaceWrapper f2;
            WatchFaceWrapper f3;
            WatchFaceWrapper f4;
            WatchFaceWrapper f5;
            wd4.b(g13, "preset");
            if (z) {
                this.q.setSelected(true);
                this.r.setText(PortfolioApp.W.c().getString(R.string.Customization_Delete_PresetDeleted_Label__Applied));
                this.r.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_tint, 0, 0, 0);
                this.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.white));
                this.q.setClickable(false);
                if (this.x.a.size() == 2) {
                    this.p.setVisibility(4);
                } else {
                    this.p.setVisibility(0);
                }
            } else {
                this.q.setSelected(false);
                this.r.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwiped_CTA__Apply));
                this.r.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor));
                this.q.setClickable(true);
                this.p.setVisibility(0);
            }
            this.w = g13;
            this.e.setText(g13.d());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(g13.a());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(g13.e());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with complications=" + arrayList + " watchapps=" + arrayList2 + " mWatchFaceWrapper=" + g13.f());
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                i13 i13 = (i13) it2.next();
                String f6 = i13.f();
                if (f6 != null) {
                    switch (f6.hashCode()) {
                        case -1383228885:
                            if (!f6.equals("bottom")) {
                                break;
                            } else {
                                this.g.b(i13.d());
                                this.g.setBottomContent(i13.g());
                                WatchFaceWrapper f7 = g13.f();
                                if (f7 != null) {
                                    Drawable topComplication = f7.getTopComplication();
                                    if (topComplication != null) {
                                        this.g.setBackgroundDrawableCus(topComplication);
                                        f2 = g13.f();
                                        if (f2 != null) {
                                            WatchFaceWrapper.MetaData bottomMetaData = f2.getBottomMetaData();
                                            if (bottomMetaData != null) {
                                                this.g.a((Integer) null, (Integer) null, bottomMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.g.h();
                                                break;
                                            }
                                        }
                                        this.g.g();
                                        this.g.h();
                                    }
                                }
                                this.g.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f2 = g13.f();
                                if (f2 != null) {
                                }
                                this.g.g();
                                this.g.h();
                            }
                        case 115029:
                            if (!f6.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                break;
                            } else {
                                this.f.b(i13.d());
                                this.f.setBottomContent(i13.g());
                                WatchFaceWrapper f8 = g13.f();
                                if (f8 != null) {
                                    Drawable topComplication2 = f8.getTopComplication();
                                    if (topComplication2 != null) {
                                        this.f.setBackgroundDrawableCus(topComplication2);
                                        f3 = g13.f();
                                        if (f3 != null) {
                                            WatchFaceWrapper.MetaData topMetaData = f3.getTopMetaData();
                                            if (topMetaData != null) {
                                                this.f.a((Integer) null, (Integer) null, topMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.f.h();
                                                break;
                                            }
                                        }
                                        this.f.g();
                                        this.f.h();
                                    }
                                }
                                this.f.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f3 = g13.f();
                                if (f3 != null) {
                                }
                                this.f.g();
                                this.f.h();
                            }
                        case 3317767:
                            if (!f6.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                break;
                            } else {
                                this.h.b(i13.d());
                                this.h.setBottomContent(i13.g());
                                WatchFaceWrapper f9 = g13.f();
                                if (f9 != null) {
                                    Drawable leftComplication = f9.getLeftComplication();
                                    if (leftComplication != null) {
                                        this.h.setBackgroundDrawableCus(leftComplication);
                                        f4 = g13.f();
                                        if (f4 != null) {
                                            WatchFaceWrapper.MetaData leftMetaData = f4.getLeftMetaData();
                                            if (leftMetaData != null) {
                                                this.h.a((Integer) null, (Integer) null, leftMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.h.h();
                                                break;
                                            }
                                        }
                                        this.h.g();
                                        this.h.h();
                                    }
                                }
                                this.h.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f4 = g13.f();
                                if (f4 != null) {
                                }
                                this.h.g();
                                this.h.h();
                            }
                        case 108511772:
                            if (!f6.equals("right")) {
                                break;
                            } else {
                                this.i.b(i13.d());
                                this.i.setBottomContent(i13.g());
                                WatchFaceWrapper f10 = g13.f();
                                if (f10 != null) {
                                    Drawable rightComplication = f10.getRightComplication();
                                    if (rightComplication != null) {
                                        this.i.setBackgroundDrawableCus(rightComplication);
                                        f5 = g13.f();
                                        if (f5 != null) {
                                            WatchFaceWrapper.MetaData rightMetaData = f5.getRightMetaData();
                                            if (rightMetaData != null) {
                                                this.i.a((Integer) null, (Integer) null, rightMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.i.h();
                                                break;
                                            }
                                        }
                                        this.i.g();
                                        this.i.h();
                                    }
                                }
                                this.i.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f5 = g13.f();
                                if (f5 != null) {
                                }
                                this.i.g();
                                this.i.h();
                            }
                    }
                }
            }
            WatchFaceWrapper f11 = g13.f();
            if (f11 != null) {
                Drawable background = f11.getBackground();
                if (background != null) {
                    this.k.setImageDrawable(background);
                    it = arrayList2.iterator();
                    while (it.hasNext()) {
                        i13 i132 = (i13) it.next();
                        String f12 = i132.f();
                        if (f12 != null) {
                            int hashCode = f12.hashCode();
                            if (hashCode != -1383228885) {
                                if (hashCode != -1074341483) {
                                    if (hashCode == 115029 && f12.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        this.l.d(i132.d());
                                        this.l.h();
                                    }
                                } else if (f12.equals("middle")) {
                                    this.m.d(i132.d());
                                    this.m.h();
                                }
                            } else if (f12.equals("bottom")) {
                                this.n.d(i132.d());
                                this.n.h();
                            }
                        }
                    }
                }
            }
            this.k.setImageDrawable(PortfolioApp.W.c().getDrawable(R.drawable.nothing_bg));
            it = arrayList2.iterator();
            while (it.hasNext()) {
            }
        }
    }
}
