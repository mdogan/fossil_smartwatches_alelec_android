package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.cr;
import com.fossil.blesdk.obfuscated.sn;
import com.fossil.blesdk.obfuscated.uq;
import com.fossil.blesdk.obfuscated.vu;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tn {
    @DexIgnore
    public /* final */ Map<Class<?>, zn<?, ?>> a; // = new g4();
    @DexIgnore
    public rp b;
    @DexIgnore
    public kq c;
    @DexIgnore
    public hq d;
    @DexIgnore
    public br e;
    @DexIgnore
    public er f;
    @DexIgnore
    public er g;
    @DexIgnore
    public uq.a h;
    @DexIgnore
    public cr i;
    @DexIgnore
    public nu j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public sn.a l; // = new a(this);
    @DexIgnore
    public vu.b m;
    @DexIgnore
    public er n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<rv<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s; // = 700;
    @DexIgnore
    public int t; // = 128;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements sn.a {
        @DexIgnore
        public a(tn tnVar) {
        }

        @DexIgnore
        public sv build() {
            return new sv();
        }
    }

    @DexIgnore
    public void a(vu.b bVar) {
        this.m = bVar;
    }

    @DexIgnore
    public sn a(Context context) {
        Context context2 = context;
        if (this.f == null) {
            this.f = er.d();
        }
        if (this.g == null) {
            this.g = er.c();
        }
        if (this.n == null) {
            this.n = er.b();
        }
        if (this.i == null) {
            this.i = new cr.a(context2).a();
        }
        if (this.j == null) {
            this.j = new pu();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new qq((long) b2);
            } else {
                this.c = new lq();
            }
        }
        if (this.d == null) {
            this.d = new pq(this.i.a());
        }
        if (this.e == null) {
            this.e = new ar((long) this.i.c());
        }
        if (this.h == null) {
            this.h = new zq(context2);
        }
        if (this.b == null) {
            this.b = new rp(this.e, this.h, this.g, this.f, er.e(), this.n, this.o);
        }
        List<rv<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new sn(context, this.b, this.e, this.c, this.d, new vu(this.m), this.j, this.k, this.l, this.a, this.p, this.q, this.r, this.s, this.t);
    }
}
