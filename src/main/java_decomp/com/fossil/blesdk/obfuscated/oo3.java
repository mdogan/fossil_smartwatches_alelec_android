package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oo3 implements MembersInjector<SignUpPresenter> {
    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, sr2 sr2) {
        signUpPresenter.f = sr2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, ln2 ln2) {
        signUpPresenter.g = ln2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, tr2 tr2) {
        signUpPresenter.h = tr2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, vr2 vr2) {
        signUpPresenter.i = vr2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, ur2 ur2) {
        signUpPresenter.j = ur2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, LoginSocialUseCase loginSocialUseCase) {
        signUpPresenter.k = loginSocialUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, UserRepository userRepository) {
        signUpPresenter.l = userRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, DeviceRepository deviceRepository) {
        signUpPresenter.m = deviceRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, k62 k62) {
        signUpPresenter.n = k62;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSleepSessions fetchSleepSessions) {
        signUpPresenter.o = fetchSleepSessions;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSleepSummaries fetchSleepSummaries) {
        signUpPresenter.p = fetchSleepSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchActivities fetchActivities) {
        signUpPresenter.q = fetchActivities;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSummaries fetchSummaries) {
        signUpPresenter.r = fetchSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        signUpPresenter.s = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        signUpPresenter.t = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, AlarmsRepository alarmsRepository) {
        signUpPresenter.u = alarmsRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, uq2 uq2) {
        signUpPresenter.v = uq2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, wj2 wj2) {
        signUpPresenter.w = wj2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        signUpPresenter.x = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, fn2 fn2) {
        signUpPresenter.y = fn2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, CheckAuthenticationEmailExisting checkAuthenticationEmailExisting) {
        signUpPresenter.z = checkAuthenticationEmailExisting;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        signUpPresenter.A = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, AnalyticsHelper analyticsHelper) {
        signUpPresenter.B = analyticsHelper;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, or2 or2) {
        signUpPresenter.C = or2;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, SummariesRepository summariesRepository) {
        signUpPresenter.D = summariesRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, SleepSummariesRepository sleepSummariesRepository) {
        signUpPresenter.E = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, GoalTrackingRepository goalTrackingRepository) {
        signUpPresenter.F = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        signUpPresenter.G = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchGoalTrackingData fetchGoalTrackingData) {
        signUpPresenter.H = fetchGoalTrackingData;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, RequestEmailOtp requestEmailOtp) {
        signUpPresenter.I = requestEmailOtp;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        signUpPresenter.J = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        signUpPresenter.K = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter) {
        signUpPresenter.D();
    }
}
