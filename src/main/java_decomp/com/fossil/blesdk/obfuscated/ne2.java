package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ne2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ SwitchCompat q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ SwitchCompat t;

    @DexIgnore
    public ne2(Object obj, View view, int i, SwitchCompat switchCompat, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, SwitchCompat switchCompat2, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = switchCompat;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = switchCompat2;
    }
}
