package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w20 extends t20<byte[], byte[]> {
    @DexIgnore
    public static /* final */ l20<byte[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<byte[]>[] b; // = new m20[0];
    @DexIgnore
    public static /* final */ w20 c; // = new w20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<byte[]> {
        @DexIgnore
        public /* bridge */ /* synthetic */ byte[] a(Object obj) {
            byte[] bArr = (byte[]) obj;
            a(bArr);
            return bArr;
        }

        @DexIgnore
        public byte[] a(byte[] bArr) {
            wd4.b(bArr, "entries");
            byte[] unused = w20.c.b(bArr);
            return bArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<byte[]> {
        @DexIgnore
        public /* bridge */ /* synthetic */ byte[] a(Object obj) {
            byte[] bArr = (byte[]) obj;
            a(bArr);
            return bArr;
        }

        @DexIgnore
        public byte[] a(byte[] bArr) {
            wd4.b(bArr, "entries");
            byte[] unused = w20.c.b(bArr);
            return bArr;
        }
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) {
        return bArr;
    }

    @DexIgnore
    public m20<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    public l20<byte[]>[] a() {
        return a;
    }
}
