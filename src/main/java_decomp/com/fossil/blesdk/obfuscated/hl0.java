package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.bk0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hl0 implements bk0.b {
    @DexIgnore
    public final ApiException a(Status status) {
        return ij0.a(status);
    }
}
