package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l93 {
    @DexIgnore
    public /* final */ g93 a;
    @DexIgnore
    public /* final */ v93 b;
    @DexIgnore
    public /* final */ q93 c;

    @DexIgnore
    public l93(g93 g93, v93 v93, q93 q93) {
        wd4.b(g93, "mActivityOverviewDayView");
        wd4.b(v93, "mActivityOverviewWeekView");
        wd4.b(q93, "mActivityOverviewMonthView");
        this.a = g93;
        this.b = v93;
        this.c = q93;
    }

    @DexIgnore
    public final g93 a() {
        return this.a;
    }

    @DexIgnore
    public final q93 b() {
        return this.c;
    }

    @DexIgnore
    public final v93 c() {
        return this.b;
    }
}
