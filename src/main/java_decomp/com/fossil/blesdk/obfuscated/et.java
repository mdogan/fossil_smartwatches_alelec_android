package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class et implements bq<BitmapDrawable>, xp {
    @DexIgnore
    public /* final */ Resources e;
    @DexIgnore
    public /* final */ bq<Bitmap> f;

    @DexIgnore
    public et(Resources resources, bq<Bitmap> bqVar) {
        uw.a(resources);
        this.e = resources;
        uw.a(bqVar);
        this.f = bqVar;
    }

    @DexIgnore
    public static bq<BitmapDrawable> a(Resources resources, bq<Bitmap> bqVar) {
        if (bqVar == null) {
            return null;
        }
        return new et(resources, bqVar);
    }

    @DexIgnore
    public int b() {
        return this.f.b();
    }

    @DexIgnore
    public Class<BitmapDrawable> c() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    public void d() {
        bq<Bitmap> bqVar = this.f;
        if (bqVar instanceof xp) {
            ((xp) bqVar).d();
        }
    }

    @DexIgnore
    public void a() {
        this.f.a();
    }

    @DexIgnore
    public BitmapDrawable get() {
        return new BitmapDrawable(this.e, this.f.get());
    }
}
