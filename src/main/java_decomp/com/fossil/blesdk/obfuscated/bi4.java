package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bi4 extends ng4 {
    @DexIgnore
    public /* final */ ai4 e;

    @DexIgnore
    public bi4(ai4 ai4) {
        wd4.b(ai4, "handle");
        this.e = ai4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCancel[" + this.e + ']';
    }
}
