package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class pw1 implements lw1 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public pw1(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static lw1 a(Object obj) {
        return new pw1(obj);
    }

    @DexIgnore
    public final Object a(kw1 kw1) {
        Object obj = this.a;
        jw1.a(obj);
        return obj;
    }
}
