package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class el4 implements gl4 {
    @DexIgnore
    public static /* final */ TaskMode e; // = TaskMode.NON_BLOCKING;
    @DexIgnore
    public static /* final */ el4 f; // = new el4();

    @DexIgnore
    public void A() {
    }

    @DexIgnore
    public TaskMode B() {
        return e;
    }
}
