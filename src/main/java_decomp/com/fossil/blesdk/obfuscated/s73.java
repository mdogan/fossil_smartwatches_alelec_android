package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s73 implements Factory<ab3> {
    @DexIgnore
    public static ab3 a(o73 o73) {
        ab3 d = o73.d();
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
