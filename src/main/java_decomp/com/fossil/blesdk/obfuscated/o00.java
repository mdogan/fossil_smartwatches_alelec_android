package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class o00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[BluetoothLeAdapter.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[Device.HIDState.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[Device.HIDState.values().length];

    /*
    static {
        a[BluetoothLeAdapter.State.ENABLED.ordinal()] = 1;
        b[Device.HIDState.CONNECTING.ordinal()] = 1;
        b[Device.HIDState.DISCONNECTED.ordinal()] = 2;
        b[Device.HIDState.CONNECTED.ordinal()] = 3;
        b[Device.HIDState.DISCONNECTING.ordinal()] = 4;
        c[Device.HIDState.DISCONNECTED.ordinal()] = 1;
        c[Device.HIDState.CONNECTING.ordinal()] = 2;
        c[Device.HIDState.CONNECTED.ordinal()] = 3;
        c[Device.HIDState.DISCONNECTING.ordinal()] = 4;
    }
    */
}
