package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import com.fossil.blesdk.obfuscated.hr3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.data.model.ua.UADataSource;
import com.portfolio.platform.data.model.ua.UALink;
import com.portfolio.platform.data.model.ua.UALinks;
import com.portfolio.platform.underamour.UASharePref;
import com.portfolio.platform.underamour.UAValues;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class er3 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public dr3 a;
    @DexIgnore
    public fr3 b;
    @DexIgnore
    public String c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements p94<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ er3 a;
        @DexIgnore
        public /* final */ /* synthetic */ UAActivityTimeSeries b;
        @DexIgnore
        public /* final */ /* synthetic */ hr3.d c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements p94<cs4<yz1>> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(List list, b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void a(cs4<yz1> cs4) {
                if (cs4 != null) {
                    FLogger.INSTANCE.getLocal().d(er3.d + "_postActivityTimeSeries", "onResponse: " + String.valueOf(cs4.a()));
                    hr3.d dVar = this.a.c;
                    if (dVar != null) {
                        dVar.onSuccess();
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.er3$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.er3$b$b  reason: collision with other inner class name */
        public static final class C0084b<T> implements p94<Throwable> {
            @DexIgnore
            public static /* final */ C0084b a; // = new C0084b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(er3.d + "_postActivityTimeSeries", "onFailure: " + th.getMessage());
                }
            }
        }

        @DexIgnore
        public b(er3 er3, UAActivityTimeSeries uAActivityTimeSeries, hr3.d dVar) {
            this.a = er3;
            this.b = uAActivityTimeSeries;
            this.c = dVar;
        }

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) yz1, UAAccessToken.class);
                List<UALink> list = null;
                if (!cg4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                UADataSource c2 = UASharePref.c.a().c();
                if (c2 != null) {
                    UALinks link = c2.getLink();
                    if (link != null) {
                        list = link.getSelf();
                    }
                }
                if (list != null && (!list.isEmpty())) {
                    this.b.setExtraJsonObject((yz1) new Gson().a(new Gson().a((Object) list.get(0), (Type) UALink.class), yz1.class));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(er3.d + "_postActivityTimeSeries", this.b.getJsonData().toString());
                    dr3 b2 = this.a.a;
                    yz1 jsonData = this.b.getJsonData();
                    String a2 = this.a.c;
                    be4 be4 = be4.a;
                    String value = UAValues.Authorization.BEARER.getValue();
                    Object[] objArr = {UASharePref.c.a().a()};
                    String format = String.format(value, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    b2.a(jsonData, a2, format).a(3).a(h94.a()).a(new a(list, this), (p94<? super Throwable>) C0084b.a);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements p94<Throwable> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(er3.d + "_postActivityTimeSeries", th.getMessage());
            }
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = er3.class.getSimpleName();
        wd4.a((Object) simpleName, "UAActivityManager::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public er3(dr3 dr3, fr3 fr3, String str) {
        wd4.b(dr3, "uaApi");
        wd4.b(fr3, "uaAuthorizationManager");
        wd4.b(str, "clientId");
        this.a = dr3;
        this.b = fr3;
        this.c = str;
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void a(UAActivityTimeSeries uAActivityTimeSeries, hr3.d dVar) {
        wd4.b(uAActivityTimeSeries, "uaActivityTimeSeries");
        this.b.a().a(new b(this, uAActivityTimeSeries, dVar), (p94<? super Throwable>) c.a);
    }
}
