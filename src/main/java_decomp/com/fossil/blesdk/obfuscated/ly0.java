package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ly0 extends rx0<ly0> implements Cloneable {
    @DexIgnore
    public static volatile ly0[] i;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";

    @DexIgnore
    public ly0() {
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public static ly0[] f() {
        if (i == null) {
            synchronized (vx0.a) {
                if (i == null) {
                    i = new ly0[0];
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final void a(qx0 qx0) throws IOException {
        String str = this.g;
        if (str != null && !str.equals("")) {
            qx0.a(1, this.g);
        }
        String str2 = this.h;
        if (str2 != null && !str2.equals("")) {
            qx0.a(2, this.h);
        }
        super.a(qx0);
    }

    @DexIgnore
    public final int b() {
        int b = super.b();
        String str = this.g;
        if (str != null && !str.equals("")) {
            b += qx0.b(1, this.g);
        }
        String str2 = this.h;
        return (str2 == null || str2.equals("")) ? b : b + qx0.b(2, this.h);
    }

    @DexIgnore
    public final /* synthetic */ wx0 c() throws CloneNotSupportedException {
        return (ly0) clone();
    }

    @DexIgnore
    public final /* synthetic */ rx0 d() throws CloneNotSupportedException {
        return (ly0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final ly0 clone() {
        try {
            return (ly0) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ly0)) {
            return false;
        }
        ly0 ly0 = (ly0) obj;
        String str = this.g;
        if (str == null) {
            if (ly0.g != null) {
                return false;
            }
        } else if (!str.equals(ly0.g)) {
            return false;
        }
        String str2 = this.h;
        if (str2 == null) {
            if (ly0.h != null) {
                return false;
            }
        } else if (!str2.equals(ly0.h)) {
            return false;
        }
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            return this.f.equals(ly0.f);
        }
        tx0 tx02 = ly0.f;
        return tx02 == null || tx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (ly0.class.getName().hashCode() + 527) * 31;
        String str = this.g;
        int i2 = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.h;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            i2 = this.f.hashCode();
        }
        return hashCode3 + i2;
    }
}
