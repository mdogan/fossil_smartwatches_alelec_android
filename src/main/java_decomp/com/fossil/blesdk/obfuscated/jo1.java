package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jo1<TResult> implements ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public sn1<TResult> c;

    @DexIgnore
    public jo1(Executor executor, sn1<TResult> sn1) {
        this.a = executor;
        this.c = sn1;
    }

    @DexIgnore
    public final void onComplete(xn1<TResult> xn1) {
        synchronized (this.b) {
            if (this.c != null) {
                this.a.execute(new ko1(this, xn1));
            }
        }
    }
}
