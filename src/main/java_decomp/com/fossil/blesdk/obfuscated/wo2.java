package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wo2 {
    @DexIgnore
    public static /* final */ String b; // = "com.fossil.blesdk.obfuscated.wo2";
    @DexIgnore
    public Weather a;

    @DexIgnore
    public void a(yz1 yz1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".parse - json=" + yz1);
        try {
            sz1 sz1 = new sz1();
            sz1.a(DateTime.class, new GsonConvertDateTime());
            sz1.a(Date.class, new GsonConverterShortDate());
            this.a = (Weather) sz1.a().a(yz1.toString(), Weather.class);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b;
            local2.d(str2, "parse mWeather=" + this.a);
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local3.e(str3, "parse error=" + e.toString());
        }
    }

    @DexIgnore
    public Weather a() {
        return this.a;
    }
}
