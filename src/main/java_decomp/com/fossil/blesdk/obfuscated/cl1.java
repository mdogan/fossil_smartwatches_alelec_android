package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cl1 extends ui1 implements wi1 {
    @DexIgnore
    public /* final */ el1 b;

    @DexIgnore
    public cl1(el1 el1) {
        super(el1.B());
        ck0.a(el1);
        this.b = el1;
    }

    @DexIgnore
    public kl1 m() {
        return this.b.j();
    }

    @DexIgnore
    public ul1 n() {
        return this.b.k();
    }

    @DexIgnore
    public bm1 o() {
        return this.b.l();
    }
}
