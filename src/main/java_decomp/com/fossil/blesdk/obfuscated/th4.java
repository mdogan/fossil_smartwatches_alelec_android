package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface th4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static ai4 a(th4 th4, long j, Runnable runnable) {
            wd4.b(runnable, "block");
            return rh4.a().a(j, runnable);
        }
    }

    @DexIgnore
    ai4 a(long j, Runnable runnable);

    @DexIgnore
    void a(long j, pg4<? super cb4> pg4);
}
