package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ct2;
import com.fossil.blesdk.obfuscated.r62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b53 extends as2 implements l53, r62.b {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ur3<ng2> j;
    @DexIgnore
    public k53 k;
    @DexIgnore
    public ct2 l;
    @DexIgnore
    public r62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final b53 a() {
            return new b53();
        }

        @DexIgnore
        public final String b() {
            return b53.o;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ ng2 f;

        @DexIgnore
        public b(View view, ng2 ng2) {
            this.e = view;
            this.f = ng2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.u.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ng2 ng2 = this.f;
                wd4.a((Object) ng2, "binding");
                ng2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = q23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b53 e;
        @DexIgnore
        public /* final */ /* synthetic */ ng2 f;

        @DexIgnore
        public c(b53 b53, ng2 ng2) {
            this.e = b53;
            this.f = ng2;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            r62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    wd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = b53.p.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    if (this.e.l != null && !TextUtils.isEmpty(item.getPlaceId())) {
                        this.f.q.setText("");
                        k53 c = b53.c(this.e);
                        String spannableString = fullText.toString();
                        wd4.a((Object) spannableString, "primaryText.toString()");
                        String placeId = item.getPlaceId();
                        wd4.a((Object) placeId, "item.placeId");
                        r62 a2 = this.e.m;
                        if (a2 != null) {
                            c.a(spannableString, placeId, a2.b());
                            r62 a3 = this.e.m;
                            if (a3 != null) {
                                a3.a();
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ng2 e;

        @DexIgnore
        public d(b53 b53, ng2 ng2) {
            this.e = ng2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.e.s;
            wd4.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ b53 e;

        @DexIgnore
        public e(b53 b53, ng2 ng2) {
            this.e = b53;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ct2.b {
        @DexIgnore
        public /* final */ /* synthetic */ b53 a;

        @DexIgnore
        public f(b53 b53) {
            this.a = b53;
        }

        @DexIgnore
        public void a(int i, boolean z) {
            b53.c(this.a).a(i, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b53 e;

        @DexIgnore
        public g(b53 b53) {
            this.e = b53;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ng2 e;

        @DexIgnore
        public h(ng2 ng2) {
            this.e = ng2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = b53.class.getSimpleName();
        wd4.a((Object) simpleName, "WeatherSettingFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ k53 c(b53 b53) {
        k53 k53 = b53.k;
        if (k53 != null) {
            return k53;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        ur3<ng2> ur3 = this.j;
        if (ur3 != null) {
            ng2 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        ur3<ng2> ur3 = this.j;
        if (ur3 == null) {
            wd4.d("mBinding");
            throw null;
        } else if (ur3.a() == null || this.l == null) {
            return true;
        } else {
            k53 k53 = this.k;
            if (k53 != null) {
                k53.i();
                return true;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T0() {
        ur3<ng2> ur3 = this.j;
        if (ur3 != null) {
            ng2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "it.ftvLocationError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.w;
                wd4.a((Object) flexibleTextView2, "it.tvAdded");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = a2.v;
                wd4.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        ur3<ng2> ur3 = this.j;
        if (ur3 != null) {
            ng2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "it.ftvLocationError");
                be4 be4 = be4.a;
                String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                Object[] objArr = {appCompatAutoCompleteTextView.getText()};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                FlexibleTextView flexibleTextView2 = a2.t;
                wd4.a((Object) flexibleTextView2, "it.ftvLocationError");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.w;
                wd4.a((Object) flexibleTextView3, "it.tvAdded");
                flexibleTextView3.setVisibility(8);
                RecyclerView recyclerView = a2.v;
                wd4.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e0() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.n(childFragmentManager);
        }
    }

    @DexIgnore
    public void o(List<WeatherLocationWrapper> list) {
        wd4.b(list, "locations");
        ct2 ct2 = this.l;
        if (ct2 != null) {
            ct2.a((List<WeatherLocationWrapper>) ce4.c(list));
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ng2 ng2 = (ng2) ra.a(layoutInflater, R.layout.fragment_weather_setting, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = ng2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new c(this, ng2));
        appCompatAutoCompleteTextView.addTextChangedListener(new d(this, ng2));
        appCompatAutoCompleteTextView.setOnKeyListener(new e(this, ng2));
        this.l = new ct2();
        ct2 ct2 = this.l;
        if (ct2 != null) {
            ct2.a((ct2.b) new f(this));
        }
        RecyclerView recyclerView = ng2.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        wd4.a((Object) ng2, "binding");
        View d2 = ng2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, ng2));
        ng2.r.setOnClickListener(new g(this));
        ng2.s.setOnClickListener(new h(ng2));
        this.j = new ur3<>(this, ng2);
        return ng2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        k53 k53 = this.k;
        if (k53 != null) {
            k53.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        k53 k53 = this.k;
        if (k53 != null) {
            k53.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            k53 k53 = this.k;
            if (k53 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", k53.h());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void a(k53 k53) {
        wd4.b(k53, "presenter");
        this.k = k53;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) context, "context!!");
                this.m = new r62(context, placesClient);
                r62 r62 = this.m;
                if (r62 != null) {
                    r62.a((r62.b) this);
                }
                ur3<ng2> ur3 = this.j;
                if (ur3 != null) {
                    ng2 a2 = ur3.a();
                    if (a2 != null) {
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        if (appCompatAutoCompleteTextView != null) {
                            appCompatAutoCompleteTextView.setAdapter(this.m);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
    }
}
