package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ui1 implements wi1 {
    @DexIgnore
    public /* final */ yh1 a;

    @DexIgnore
    public ui1(yh1 yh1) {
        ck0.a(yh1);
        this.a = yh1;
    }

    @DexIgnore
    public uh1 a() {
        return this.a.a();
    }

    @DexIgnore
    public vl1 b() {
        return this.a.b();
    }

    @DexIgnore
    public im0 c() {
        return this.a.c();
    }

    @DexIgnore
    public ug1 d() {
        return this.a.d();
    }

    @DexIgnore
    public void e() {
        this.a.a().e();
    }

    @DexIgnore
    public void f() {
        this.a.i();
    }

    @DexIgnore
    public void g() {
        this.a.a().g();
    }

    @DexIgnore
    public Context getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    public cg1 h() {
        return this.a.q();
    }

    @DexIgnore
    public sg1 i() {
        return this.a.r();
    }

    @DexIgnore
    public ol1 j() {
        return this.a.s();
    }

    @DexIgnore
    public gh1 k() {
        return this.a.t();
    }

    @DexIgnore
    public yl1 l() {
        return this.a.u();
    }
}
