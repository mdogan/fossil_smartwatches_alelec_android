package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wt implements oo<vt> {
    @DexIgnore
    public EncodeStrategy a(mo moVar) {
        return EncodeStrategy.SOURCE;
    }

    @DexIgnore
    public boolean a(bq<vt> bqVar, File file, mo moVar) {
        try {
            lw.a(bqVar.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
