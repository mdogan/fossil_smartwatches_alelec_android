package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nm3 extends bs2 implements tm3 {
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<re2> k;
    @DexIgnore
    public sm3 l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final nm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            nm3 nm3 = new nm3();
            nm3.setArguments(bundle);
            return nm3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nm3 e;

        @DexIgnore
        public b(nm3 nm3) {
            this.e = nm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            nm3.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nm3 e;

        @DexIgnore
        public c(nm3 nm3) {
            this.e = nm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                HomeActivity.a aVar = HomeActivity.C;
                wd4.a((Object) activity, "fragmentActivity");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nm3 e;

        @DexIgnore
        public d(nm3 nm3) {
            this.e = nm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nm3 e;

        @DexIgnore
        public e(nm3 nm3) {
            this.e = nm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                CompatibleModelsActivity.a aVar = CompatibleModelsActivity.B;
                wd4.a((Object) activity, "fragmentActivity");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ sm3 a(nm3 nm3) {
        sm3 sm3 = nm3.l;
        if (sm3 != null) {
            return sm3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        sm3 sm3 = this.l;
        if (sm3 == null) {
            wd4.d("mPresenter");
            throw null;
        } else if (sm3.i()) {
            return false;
        } else {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return false;
            }
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    public void c0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.D;
            wd4.a((Object) activity, "fragmentActivity");
            sm3 sm3 = this.l;
            if (sm3 != null) {
                aVar.a(activity, sm3.i());
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void g() {
        ur3<re2> ur3 = this.k;
        if (ur3 != null) {
            re2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.u;
                if (dashBar != null) {
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.e(dashBar, this.m, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        re2 re2 = (re2) ra.a(layoutInflater, R.layout.fragment_pairing_instructions, viewGroup, false, O0());
        this.k = new ur3<>(this, re2);
        wd4.a((Object) re2, "binding");
        return re2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        sm3 sm3 = this.l;
        if (sm3 != null) {
            sm3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        sm3 sm3 = this.l;
        if (sm3 != null) {
            sm3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<re2> ur3 = this.k;
        if (ur3 != null) {
            re2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.s.setOnClickListener(new d(this));
                a2.t.setOnClickListener(new e(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.m = arguments.getBoolean("IS_ONBOARDING_FLOW");
                sm3 sm3 = this.l;
                if (sm3 != null) {
                    sm3.a(this.m);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void s(boolean z) {
        if (isActive()) {
            ur3<re2> ur3 = this.k;
            if (ur3 != null) {
                re2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.s;
                    if (imageView != null) {
                        wd4.a((Object) imageView, "it");
                        imageView.setVisibility(!z ? 4 : 0);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(sm3 sm3) {
        wd4.b(sm3, "presenter");
        this.l = sm3;
    }
}
