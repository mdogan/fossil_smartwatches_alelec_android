package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v54 {
    @DexIgnore
    public /* final */ e54<String> a; // = new a(this);
    @DexIgnore
    public /* final */ c54<String> b; // = new c54<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements e54<String> {
        @DexIgnore
        public a(v54 v54) {
        }

        @DexIgnore
        public String a(Context context) throws Exception {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    }

    @DexIgnore
    public String a(Context context) {
        try {
            String a2 = this.b.a(context, this.a);
            if ("".equals(a2)) {
                return null;
            }
            return a2;
        } catch (Exception e) {
            r44.g().e("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
