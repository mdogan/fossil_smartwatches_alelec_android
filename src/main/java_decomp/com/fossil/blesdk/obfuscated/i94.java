package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Message;
import com.fossil.blesdk.obfuscated.c94;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i94 extends c94 {
    @DexIgnore
    public /* final */ Handler a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends c94.b {
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public volatile boolean f;

        @DexIgnore
        public a(Handler handler) {
            this.e = handler;
        }

        @DexIgnore
        public j94 a(Runnable runnable, long j, TimeUnit timeUnit) {
            if (runnable == null) {
                throw new NullPointerException("run == null");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else if (this.f) {
                return k94.a();
            } else {
                b bVar = new b(this.e, ta4.a(runnable));
                Message obtain = Message.obtain(this.e, bVar);
                obtain.obj = this;
                this.e.sendMessageDelayed(obtain, Math.max(0, timeUnit.toMillis(j)));
                if (!this.f) {
                    return bVar;
                }
                this.e.removeCallbacks(bVar);
                return k94.a();
            }
        }

        @DexIgnore
        public void dispose() {
            this.f = true;
            this.e.removeCallbacksAndMessages(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable, j94 {
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public /* final */ Runnable f;

        @DexIgnore
        public b(Handler handler, Runnable runnable) {
            this.e = handler;
            this.f = runnable;
        }

        @DexIgnore
        public void dispose() {
            this.e.removeCallbacks(this);
        }

        @DexIgnore
        public void run() {
            try {
                this.f.run();
            } catch (Throwable th) {
                IllegalStateException illegalStateException = new IllegalStateException("Fatal Exception thrown on Scheduler.", th);
                ta4.b(illegalStateException);
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, illegalStateException);
            }
        }
    }

    @DexIgnore
    public i94(Handler handler) {
        this.a = handler;
    }

    @DexIgnore
    public j94 a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (runnable == null) {
            throw new NullPointerException("run == null");
        } else if (timeUnit != null) {
            b bVar = new b(this.a, ta4.a(runnable));
            this.a.postDelayed(bVar, Math.max(0, timeUnit.toMillis(j)));
            return bVar;
        } else {
            throw new NullPointerException("unit == null");
        }
    }

    @DexIgnore
    public c94.b a() {
        return new a(this.a);
    }
}
