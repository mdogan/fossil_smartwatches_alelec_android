package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sr4<F, T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public static Type a(int i, ParameterizedType parameterizedType) {
            return gs4.b(i, parameterizedType);
        }

        @DexIgnore
        public sr4<qm4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
            return null;
        }

        @DexIgnore
        public sr4<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
            return null;
        }

        @DexIgnore
        public sr4<?, String> b(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
            return null;
        }

        @DexIgnore
        public static Class<?> a(Type type) {
            return gs4.b(type);
        }
    }

    @DexIgnore
    T a(F f) throws IOException;
}
