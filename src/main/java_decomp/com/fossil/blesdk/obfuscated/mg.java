package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.util.Pair;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mg implements hg {
    @DexIgnore
    public static /* final */ String[] f; // = new String[0];
    @DexIgnore
    public /* final */ SQLiteDatabase e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ kg a;

        @DexIgnore
        public a(mg mgVar, kg kgVar) {
            this.a = kgVar;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.a(new pg(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    /*
    static {
        new String[]{"", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE "};
    }
    */

    @DexIgnore
    public mg(SQLiteDatabase sQLiteDatabase) {
        this.e = sQLiteDatabase;
    }

    @DexIgnore
    public Cursor a(kg kgVar) {
        return this.e.rawQueryWithFactory(new a(this, kgVar), kgVar.b(), f, (String) null);
    }

    @DexIgnore
    public void b(String str) throws SQLException {
        this.e.execSQL(str);
    }

    @DexIgnore
    public lg c(String str) {
        return new qg(this.e.compileStatement(str));
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public Cursor d(String str) {
        return a((kg) new gg(str));
    }

    @DexIgnore
    public boolean isOpen() {
        return this.e.isOpen();
    }

    @DexIgnore
    public void s() {
        this.e.beginTransaction();
    }

    @DexIgnore
    public List<Pair<String, String>> t() {
        return this.e.getAttachedDbs();
    }

    @DexIgnore
    public void u() {
        this.e.setTransactionSuccessful();
    }

    @DexIgnore
    public void v() {
        this.e.endTransaction();
    }

    @DexIgnore
    public String w() {
        return this.e.getPath();
    }

    @DexIgnore
    public boolean x() {
        return this.e.inTransaction();
    }

    @DexIgnore
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.e == sQLiteDatabase;
    }
}
