package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ph4 {
    @DexIgnore
    public static final String a(kc4<?> kc4) {
        Object obj;
        wd4.b(kc4, "$this$toDebugString");
        if (kc4 instanceof vh4) {
            return kc4.toString();
        }
        try {
            Result.a aVar = Result.Companion;
            obj = Result.m3constructorimpl(kc4 + '@' + b(kc4));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            obj = Result.m3constructorimpl(za4.a(th));
        }
        Throwable r2 = Result.m6exceptionOrNullimpl(obj);
        String str = obj;
        if (r2 != null) {
            str = kc4.getClass().getName() + '@' + b(kc4);
        }
        return (String) str;
    }

    @DexIgnore
    public static final String b(Object obj) {
        wd4.b(obj, "$this$hexAddress");
        String hexString = Integer.toHexString(System.identityHashCode(obj));
        wd4.a((Object) hexString, "Integer.toHexString(System.identityHashCode(this))");
        return hexString;
    }

    @DexIgnore
    public static final String a(Object obj) {
        wd4.b(obj, "$this$classSimpleName");
        String simpleName = obj.getClass().getSimpleName();
        wd4.a((Object) simpleName, "this::class.java.simpleName");
        return simpleName;
    }
}
