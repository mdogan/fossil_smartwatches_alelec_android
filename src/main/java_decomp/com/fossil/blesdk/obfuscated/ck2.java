package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ck2 implements ko {
    @DexIgnore
    public String b;
    @DexIgnore
    public Uri c;
    @DexIgnore
    public String d;

    @DexIgnore
    public ck2(String str, String str2) {
        this.b = str;
        this.d = str2;
    }

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final Uri b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        wd4.b(messageDigest, "messageDigest");
        String str = this.b + this.c + this.d;
        Charset charset = ko.a;
        wd4.a((Object) charset, "Key.CHARSET");
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public ck2(Uri uri, String str) {
        this.c = uri;
        this.d = str;
    }
}
