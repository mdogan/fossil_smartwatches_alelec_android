package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ri4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wi4<J extends ri4> extends dh4 implements ai4, mi4 {
    @DexIgnore
    public /* final */ J h;

    @DexIgnore
    public wi4(J j) {
        wd4.b(j, "job");
        this.h = j;
    }

    @DexIgnore
    public cj4 a() {
        return null;
    }

    @DexIgnore
    public void dispose() {
        J j = this.h;
        if (j != null) {
            ((xi4) j).b((wi4<?>) this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }
}
