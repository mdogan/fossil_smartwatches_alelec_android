package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yi4 {
    @DexIgnore
    public static /* final */ pk4 a; // = new pk4("SEALED");
    @DexIgnore
    public static /* final */ di4 b; // = new di4(false);
    @DexIgnore
    public static /* final */ di4 c; // = new di4(true);

    @DexIgnore
    public static final Object a(Object obj) {
        return obj instanceof mi4 ? new ni4((mi4) obj) : obj;
    }

    @DexIgnore
    public static final Object b(Object obj) {
        ni4 ni4 = (ni4) (!(obj instanceof ni4) ? null : obj);
        if (ni4 == null) {
            return obj;
        }
        mi4 mi4 = ni4.a;
        return mi4 != null ? mi4 : obj;
    }
}
