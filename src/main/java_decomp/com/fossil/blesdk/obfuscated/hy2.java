package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hy2 implements Factory<NotificationWatchRemindersPresenter> {
    @DexIgnore
    public static NotificationWatchRemindersPresenter a(cy2 cy2, fn2 fn2, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new NotificationWatchRemindersPresenter(cy2, fn2, remindersSettingsDatabase);
    }
}
