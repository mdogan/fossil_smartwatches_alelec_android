package com.fossil.blesdk.obfuscated;

import java.nio.charset.Charset;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class em4 {
    @DexIgnore
    public static String a(String str, String str2) {
        return a(str, str2, vm4.j);
    }

    @DexIgnore
    public static String a(String str, String str2, Charset charset) {
        String base64 = ByteString.encodeString(str + ":" + str2, charset).base64();
        return "Basic " + base64;
    }
}
