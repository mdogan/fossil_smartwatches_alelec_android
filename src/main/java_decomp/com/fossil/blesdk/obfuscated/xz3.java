package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xz3 {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(Intent intent, yz3 yz3);

    @DexIgnore
    boolean a(iz3 iz3);

    @DexIgnore
    boolean a(String str);
}
