package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ii4 {
    @DexIgnore
    public static /* final */ pk4 a; // = new pk4("REMOVED_TASK");
    @DexIgnore
    public static /* final */ pk4 b; // = new pk4("CLOSED_EMPTY");

    @DexIgnore
    public static final long a(long j) {
        if (j <= 0) {
            return 0;
        }
        return j >= 9223372036854L ? ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD : 1000000 * j;
    }
}
