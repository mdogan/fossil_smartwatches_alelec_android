package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pf0 extends he0 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public pf0(String str) {
        this.b = str;
    }

    @DexIgnore
    public vd0 a() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public ie0<Status> b() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void c() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public boolean g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(he0.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void b(he0.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }
}
