package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ l04 g;

    @DexIgnore
    public y04(Context context, String str, l04 l04) {
        this.e = context;
        this.f = str;
        this.g = l04;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            k04.f(this.e);
            synchronized (k04.k) {
                l = (Long) k04.k.remove(this.f);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                Long l2 = valueOf;
                String j = k04.j;
                if (j != null && j.equals(this.f)) {
                    j = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                s04 s04 = new s04(this.e, j, this.f, k04.a(this.e, false, this.g), l2, this.g);
                if (!this.f.equals(k04.i)) {
                    k04.m.h("Invalid invocation since previous onResume on diff page.");
                }
                new d14(s04).a();
                String unused = k04.j = this.f;
                return;
            }
            u14 f2 = k04.m;
            f2.c("Starttime for PageID:" + this.f + " not found, lost onResume()?");
        } catch (Throwable th) {
            k04.m.a(th);
            k04.a(this.e, th);
        }
    }
}
