package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m81 {
    @DexIgnore
    public static /* final */ k81<?> a; // = new l81();
    @DexIgnore
    public static /* final */ k81<?> b; // = a();

    @DexIgnore
    public static k81<?> a() {
        try {
            return (k81) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static k81<?> b() {
        return a;
    }

    @DexIgnore
    public static k81<?> c() {
        k81<?> k81 = b;
        if (k81 != null) {
            return k81;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
