package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.xr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wb3 extends as2 implements vb3 {
    @DexIgnore
    public ju3 j;
    @DexIgnore
    public ur3<fc2> k;
    @DexIgnore
    public ub3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wb3 e;

        @DexIgnore
        public b(wb3 wb3, fc2 fc2) {
            this.e = wb3;
        }

        @DexIgnore
        public final void onClick(View view) {
            wb3.a(this.e).c().a(1);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ ju3 a(wb3 wb3) {
        ju3 ju3 = wb3.j;
        if (ju3 != null) {
            return ju3;
        }
        wd4.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void c(boolean z) {
        ur3<fc2> ur3 = this.k;
        if (ur3 != null) {
            fc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                OverviewWeekChart overviewWeekChart = a2.s;
                wd4.a((Object) overviewWeekChart, "binding.weekChart");
                overviewWeekChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                wd4.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewWeekChart overviewWeekChart2 = a2.s;
            wd4.a((Object) overviewWeekChart2, "binding.weekChart");
            overviewWeekChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            wd4.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onCreateView");
        fc2 fc2 = (fc2) ra.a(layoutInflater, R.layout.fragment_goal_tracking_overview_week, viewGroup, false, O0());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ju3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.j = (ju3) a2;
            fc2.r.setOnClickListener(new b(this, fc2));
        }
        this.k = new ur3<>(this, fc2);
        ur3<fc2> ur3 = this.k;
        if (ur3 != null) {
            fc2 a3 = ur3.a();
            if (a3 != null) {
                return a3.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onResume");
        ub3 ub3 = this.l;
        if (ub3 != null) {
            ub3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onStop");
        ub3 ub3 = this.l;
        if (ub3 != null) {
            ub3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(xr2 xr2) {
        wd4.b(xr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "showWeekDetails");
        ur3<fc2> ur3 = this.k;
        if (ur3 != null) {
            fc2 a2 = ur3.a();
            if (a2 != null) {
                OverviewWeekChart overviewWeekChart = a2.s;
                if (overviewWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    xr2.a aVar = xr2.a;
                    wd4.a((Object) overviewWeekChart, "it");
                    Context context = overviewWeekChart.getContext();
                    wd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewWeekChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(ub3 ub3) {
        wd4.b(ub3, "presenter");
        this.l = ub3;
    }
}
