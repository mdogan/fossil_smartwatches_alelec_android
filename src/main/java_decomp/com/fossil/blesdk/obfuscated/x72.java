package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutLocation;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import com.portfolio.platform.enums.WorkoutSourceType;
import com.portfolio.platform.enums.WorkoutType;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x72 {
    @DexIgnore
    public static /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<WorkoutStateChange>> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<List<WorkoutStateChange>> {
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = x72.class.getSimpleName();
        wd4.a((Object) simpleName, "WorkoutTypeConverter::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(WorkoutCalorie workoutCalorie) {
        if (workoutCalorie == null) {
            return null;
        }
        try {
            return new Gson().a((Object) workoutCalorie);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutDistance b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutDistance) new Gson().a(str, WorkoutDistance.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutHeartRate c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutHeartRate) new Gson().a(str, WorkoutHeartRate.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutLocation d(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutLocation) new Gson().a(str, WorkoutLocation.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutLocation ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutSourceType e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        WorkoutSourceType.a aVar = WorkoutSourceType.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSpeed f(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutSpeed) new Gson().a(str, WorkoutSpeed.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final List<WorkoutStateChange> g(String str) {
        wd4.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList();
        }
        try {
            Object a2 = new Gson().a(str, new c().getType());
            wd4.a(a2, "Gson().fromJson(data, type)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final WorkoutStep h(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutStep) new Gson().a(str, WorkoutStep.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStep ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final WorkoutType i(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        WorkoutType.a aVar = WorkoutType.Companion;
        if (str != null) {
            return aVar.a(str);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutCalorie a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (WorkoutCalorie) new Gson().a(str, WorkoutCalorie.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str2, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutDistance workoutDistance) {
        if (workoutDistance == null) {
            return null;
        }
        try {
            return new Gson().a((Object) workoutDistance);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutHeartRate workoutHeartRate) {
        if (workoutHeartRate == null) {
            return null;
        }
        try {
            return new Gson().a((Object) workoutHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutStep workoutStep) {
        if (workoutStep == null) {
            return null;
        }
        try {
            return new Gson().a((Object) workoutStep);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStep ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutSourceType workoutSourceType) {
        if (workoutSourceType != null) {
            return workoutSourceType.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String a(WorkoutType workoutType) {
        if (workoutType != null) {
            return workoutType.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String a(WorkoutSpeed workoutSpeed) {
        try {
            return new Gson().a((Object) workoutSpeed);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(WorkoutLocation workoutLocation) {
        if (workoutLocation == null) {
            return null;
        }
        try {
            return new Gson().a((Object) workoutLocation);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutLocation ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(List<WorkoutStateChange> list) {
        wd4.b(list, "workoutStateChangeList");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = new Gson().a((Object) list, new b().getType());
            wd4.a((Object) a2, "Gson().toJson(workoutStateChangeList, type)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(cb4.a);
            local.d(str, sb.toString());
            return "";
        }
    }
}
