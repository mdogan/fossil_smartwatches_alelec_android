package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class up implements ko {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Class<?> e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ ko g;
    @DexIgnore
    public /* final */ Map<Class<?>, po<?>> h;
    @DexIgnore
    public /* final */ mo i;
    @DexIgnore
    public int j;

    @DexIgnore
    public up(Object obj, ko koVar, int i2, int i3, Map<Class<?>, po<?>> map, Class<?> cls, Class<?> cls2, mo moVar) {
        uw.a(obj);
        this.b = obj;
        uw.a(koVar, "Signature must not be null");
        this.g = koVar;
        this.c = i2;
        this.d = i3;
        uw.a(map);
        this.h = map;
        uw.a(cls, "Resource class must not be null");
        this.e = cls;
        uw.a(cls2, "Transcode class must not be null");
        this.f = cls2;
        uw.a(moVar);
        this.i = moVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof up)) {
            return false;
        }
        up upVar = (up) obj;
        if (!this.b.equals(upVar.b) || !this.g.equals(upVar.g) || this.d != upVar.d || this.c != upVar.c || !this.h.equals(upVar.h) || !this.e.equals(upVar.e) || !this.f.equals(upVar.f) || !this.i.equals(upVar.i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        if (this.j == 0) {
            this.j = this.b.hashCode();
            this.j = (this.j * 31) + this.g.hashCode();
            this.j = (this.j * 31) + this.c;
            this.j = (this.j * 31) + this.d;
            this.j = (this.j * 31) + this.h.hashCode();
            this.j = (this.j * 31) + this.e.hashCode();
            this.j = (this.j * 31) + this.f.hashCode();
            this.j = (this.j * 31) + this.i.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
