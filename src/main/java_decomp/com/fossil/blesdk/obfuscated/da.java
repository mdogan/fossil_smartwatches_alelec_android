package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface da {
    @DexIgnore
    void setSupportButtonTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportButtonTintMode(PorterDuff.Mode mode);
}
