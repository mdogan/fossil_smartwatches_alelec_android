package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hh2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ SwitchCompat s;

    @DexIgnore
    public hh2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, SwitchCompat switchCompat) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = switchCompat;
    }

    @DexIgnore
    public static hh2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, ra.a());
    }

    @DexIgnore
    @Deprecated
    public static hh2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (hh2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_app_notification, viewGroup, z, obj);
    }
}
