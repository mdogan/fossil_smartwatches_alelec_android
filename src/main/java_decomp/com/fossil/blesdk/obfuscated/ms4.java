package com.fossil.blesdk.obfuscated;

import io.reactivex.BackpressureStrategy;
import java.lang.reflect.Type;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ms4<R> implements pr4<R, Object> {
    @DexIgnore
    public /* final */ Type a;
    @DexIgnore
    public /* final */ c94 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public ms4(Type type, c94 c94, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        this.a = type;
        this.b = c94;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = z6;
        this.i = z7;
    }

    @DexIgnore
    public Type a() {
        return this.a;
    }

    @DexIgnore
    public Object a(Call<R> call) {
        z84 z84;
        z84 z842;
        if (this.c) {
            z84 = new is4(call);
        } else {
            z84 = new js4(call);
        }
        if (this.d) {
            z842 = new ls4(z84);
        } else {
            z842 = this.e ? new hs4(z84) : z84;
        }
        c94 c94 = this.b;
        if (c94 != null) {
            z842 = z842.b(c94);
        }
        if (this.f) {
            return z842.a(BackpressureStrategy.LATEST);
        }
        if (this.g) {
            return z842.c();
        }
        if (this.h) {
            return z842.b();
        }
        if (this.i) {
            return z842.a();
        }
        return ta4.a(z842);
    }
}
