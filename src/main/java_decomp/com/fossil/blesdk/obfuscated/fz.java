package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;
import com.zendesk.sdk.attachment.AttachmentHelper;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fz extends f54 implements dz {
    @DexIgnore
    public fz(w44 w44, String str, String str2, a74 a74) {
        super(w44, str, str2, a74, HttpMethod.POST);
    }

    @DexIgnore
    public boolean a(cz czVar) {
        HttpRequest a = a();
        a(a, czVar);
        a(a, czVar.b);
        z44 g = r44.g();
        g.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a.g();
        z44 g3 = r44.g();
        g3.d("CrashlyticsCore", "Create report request ID: " + a.c("X-REQUEST-ID"));
        z44 g4 = r44.g();
        g4.d("CrashlyticsCore", "Result was: " + g2);
        return x54.a(g2) == 0;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, cz czVar) {
        httpRequest.c("X-CRASHLYTICS-API-KEY", czVar.a);
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        for (Map.Entry<String, String> a : czVar.b.a().entrySet()) {
            httpRequest.a(a);
        }
        return httpRequest;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, Report report) {
        httpRequest.e("report[identifier]", report.b());
        if (report.d().length == 1) {
            r44.g().d("CrashlyticsCore", "Adding single file " + report.e() + " to report " + report.b());
            httpRequest.a("report[file]", report.e(), AttachmentHelper.DEFAULT_MIMETYPE, report.c());
            return httpRequest;
        }
        int i = 0;
        for (File file : report.d()) {
            r44.g().d("CrashlyticsCore", "Adding file " + file.getName() + " to report " + report.b());
            StringBuilder sb = new StringBuilder();
            sb.append("report[file");
            sb.append(i);
            sb.append("]");
            httpRequest.a(sb.toString(), file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            i++;
        }
        return httpRequest;
    }
}
