package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ao1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b {
        @DexIgnore
        public /* final */ CountDownLatch a;

        @DexIgnore
        public a() {
            this.a = new CountDownLatch(1);
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.a.await();
        }

        @DexIgnore
        public final void onCanceled() {
            this.a.countDown();
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            this.a.countDown();
        }

        @DexIgnore
        public final void onSuccess(Object obj) {
            this.a.countDown();
        }

        @DexIgnore
        public final boolean a(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        @DexIgnore
        public /* synthetic */ a(wo1 wo1) {
            this();
        }
    }

    @DexIgnore
    public interface b extends rn1, tn1, un1<Object> {
    }

    @DexIgnore
    public static <TResult> xn1<TResult> a(TResult tresult) {
        vo1 vo1 = new vo1();
        vo1.a(tresult);
        return vo1;
    }

    @DexIgnore
    public static <TResult> TResult b(xn1<TResult> xn1) throws ExecutionException {
        if (xn1.e()) {
            return xn1.b();
        }
        if (xn1.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(xn1.a());
    }

    @DexIgnore
    public static <TResult> xn1<TResult> a(Exception exc) {
        vo1 vo1 = new vo1();
        vo1.a(exc);
        return vo1;
    }

    @DexIgnore
    public static <TResult> TResult a(xn1<TResult> xn1) throws ExecutionException, InterruptedException {
        ck0.a();
        ck0.a(xn1, (Object) "Task must not be null");
        if (xn1.d()) {
            return b(xn1);
        }
        a aVar = new a((wo1) null);
        a(xn1, aVar);
        aVar.a();
        return b(xn1);
    }

    @DexIgnore
    public static <TResult> TResult a(xn1<TResult> xn1, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        ck0.a();
        ck0.a(xn1, (Object) "Task must not be null");
        ck0.a(timeUnit, (Object) "TimeUnit must not be null");
        if (xn1.d()) {
            return b(xn1);
        }
        a aVar = new a((wo1) null);
        a(xn1, aVar);
        if (aVar.a(j, timeUnit)) {
            return b(xn1);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static void a(xn1<?> xn1, b bVar) {
        xn1.a(zn1.b, (un1<? super Object>) bVar);
        xn1.a(zn1.b, (tn1) bVar);
        xn1.a(zn1.b, (rn1) bVar);
    }
}
