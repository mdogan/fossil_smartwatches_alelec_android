package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f22 {
    @DexIgnore
    public /* final */ d22 a;
    @DexIgnore
    public /* final */ List<e22> b; // = new ArrayList();

    @DexIgnore
    public f22(d22 d22) {
        this.a = d22;
        this.b.add(new e22(d22, new int[]{1}));
    }

    @DexIgnore
    public final e22 a(int i) {
        if (i >= this.b.size()) {
            List<e22> list = this.b;
            e22 e22 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                d22 d22 = this.a;
                e22 = e22.c(new e22(d22, new int[]{1, d22.a((size - 1) + d22.a())}));
                this.b.add(e22);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                e22 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] a3 = new e22(this.a, iArr2).a(i, 1).b(a2)[1].a();
                int length2 = i - a3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(a3, 0, iArr, length + length2, a3.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
