package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.asyncevent.AppNotificationControlEvent;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent;
import com.fossil.blesdk.device.asyncevent.HeartbeatEvent;
import com.fossil.blesdk.device.asyncevent.JSONRequestEvent;
import com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent;
import com.fossil.blesdk.device.asyncevent.MusicAsyncEvent;
import com.fossil.blesdk.device.asyncevent.ServiceChangeEvent;
import com.fossil.blesdk.device.asyncevent.TimeSyncEvent;
import com.fossil.blesdk.device.data.music.MusicAction;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.device.logic.request.code.AsyncOperationCode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e30 {
    @DexIgnore
    public static /* final */ HashMap<Pair<String, AsyncEventType>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ e30 b; // = new e30();

    @DexIgnore
    public final AsyncEvent a(String str, byte[] bArr) {
        AsyncEvent asyncEvent;
        wd4.b(str, "deviceAddress");
        wd4.b(bArr, "rawData");
        if (bArr.length < 3) {
            u90.c.b("DeviceEventParser", "Invalid rawData.");
            return null;
        }
        AsyncOperationCode a2 = AsyncOperationCode.Companion.a(bArr[0]);
        AsyncEventType a3 = AsyncEventType.Companion.a(bArr[1]);
        byte b2 = (byte) (bArr[2] & -1);
        if (a2 == null) {
            u90.c.b("DeviceEventParser", "Unknown command type.");
        }
        if (a3 == null) {
            u90.c.b("DeviceEventParser", "Invalid event type.");
            return null;
        }
        byte a4 = a(str, a3);
        byte b3 = (byte) ((a4 + 1) % (o90.b((byte) -1) + 1));
        a(str, a3, b2);
        if (a4 == b2) {
            u90 u90 = u90.c;
            u90.b("DeviceEventParser", "Duplicate event sequence number: " + b2 + " - previous: " + a4);
            return null;
        }
        if (b2 != b3) {
            u90 u902 = u90.c;
            u902.b("DeviceEventParser", "WRONG event sequence number: " + b2 + " - expected: " + b3);
        }
        byte[] a5 = kb4.a(bArr, 3, bArr.length);
        switch (d30.a[a3.ordinal()]) {
            case 1:
                asyncEvent = c(b2, a5);
                break;
            case 2:
                asyncEvent = new HeartbeatEvent(b2);
                break;
            case 3:
                return null;
            case 4:
                asyncEvent = a(b2, a5);
                break;
            case 5:
                asyncEvent = e(b2, a5);
                break;
            case 6:
                asyncEvent = b(b2, a5);
                break;
            case 7:
                asyncEvent = new ServiceChangeEvent(b2);
                break;
            case 8:
                asyncEvent = d(b2, a5);
                break;
            case 9:
                asyncEvent = new TimeSyncEvent(b2);
                break;
            default:
                throw new NoWhenBranchMatchedException();
        }
        return asyncEvent;
    }

    @DexIgnore
    public final BackgroundSyncEvent b(byte b2, byte[] bArr) {
        try {
            return BackgroundSyncEvent.CREATOR.a(b2, bArr);
        } catch (IllegalArgumentException e) {
            ea0.l.a(e);
            return null;
        }
    }

    @DexIgnore
    public final JSONRequestEvent c(byte b2, byte[] bArr) {
        try {
            Charset forName = Charset.forName("UTF-8");
            wd4.a((Object) forName, "Charset.forName(\"UTF-8\")");
            JSONObject jSONObject = new JSONObject(new String(bArr, forName)).getJSONObject("req");
            int i = jSONObject.getInt("id");
            wd4.a((Object) jSONObject, "requestJSON");
            return new JSONRequestEvent(b2, i, jSONObject);
        } catch (JSONException e) {
            ea0.l.a(e);
            return null;
        }
    }

    @DexIgnore
    public final MicroAppAsyncEvent d(byte b2, byte[] bArr) {
        try {
            return MicroAppAsyncEvent.CREATOR.a(b2, bArr);
        } catch (IllegalArgumentException e) {
            ea0.l.a(e);
            return null;
        }
    }

    @DexIgnore
    public final MusicAsyncEvent e(byte b2, byte[] bArr) {
        try {
            MusicAction a2 = MusicAction.Companion.a(bArr[0]);
            if (a2 != null) {
                return new MusicAsyncEvent(b2, a2);
            }
            return null;
        } catch (IllegalArgumentException e) {
            ea0.l.a(e);
            return null;
        }
    }

    @DexIgnore
    public final AppNotificationControlEvent a(byte b2, byte[] bArr) {
        try {
            int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
            AppNotificationControlAction a2 = AppNotificationControlAction.Companion.a(bArr[4]);
            if (a2 != null) {
                return new AppNotificationControlEvent(b2, i, a2);
            }
            return null;
        } catch (IllegalArgumentException e) {
            ea0.l.a(e);
            return null;
        }
    }

    @DexIgnore
    public final byte a(String str, AsyncEventType asyncEventType) {
        Byte b2 = a.get(new Pair(str, asyncEventType));
        if (b2 != null) {
            return b2.byteValue();
        }
        return -1;
    }

    @DexIgnore
    public final void a(String str, AsyncEventType asyncEventType, byte b2) {
        a.put(new Pair(str, asyncEventType), Byte.valueOf(b2));
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "macAddress");
        synchronized (a) {
            Set<Map.Entry<Pair<String, AsyncEventType>, Byte>> entrySet = a.entrySet();
            wd4.a((Object) entrySet, "eventSequenceManager.entries");
            for (Map.Entry entry : entrySet) {
                if (wd4.a((Object) str, (Object) (String) ((Pair) entry.getKey()).getFirst())) {
                    HashMap<Pair<String, AsyncEventType>, Byte> hashMap = a;
                    Object key = entry.getKey();
                    wd4.a(key, "entry.key");
                    hashMap.put(key, (byte) -1);
                }
            }
            cb4 cb4 = cb4.a;
        }
    }
}
