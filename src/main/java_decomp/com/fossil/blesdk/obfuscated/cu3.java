package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cu3 extends RecyclerView.q {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public int c; // = 1;
    @DexIgnore
    public /* final */ LinearLayoutManager d;

    @DexIgnore
    public cu3(LinearLayoutManager linearLayoutManager) {
        wd4.b(linearLayoutManager, "mLinearLayoutManager");
        this.d = linearLayoutManager;
    }

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(int i, int i2);

    @DexIgnore
    public final void a(RecyclerView recyclerView) {
        int childCount = recyclerView.getChildCount();
        int j = this.d.j();
        int H = this.d.H();
        if (this.b && j > this.a) {
            this.b = false;
            this.a = j;
        }
        if (!this.b && (j - H) - childCount == 0) {
            this.c++;
            a(this.c);
            this.b = true;
        }
    }

    @DexIgnore
    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        wd4.b(recyclerView, "recyclerView");
        super.onScrollStateChanged(recyclerView, i);
        if (i == 0) {
            a(recyclerView);
            a(this.d.H(), this.d.J());
        }
    }

    @DexIgnore
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        wd4.b(recyclerView, "recyclerView");
        super.onScrolled(recyclerView, i, i2);
        a(recyclerView);
    }

    @DexIgnore
    public final void a() {
        this.c = 1;
        this.a = 0;
        this.b = true;
    }
}
