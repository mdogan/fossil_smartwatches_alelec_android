package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.td;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ld<K, V> extends rd<V> implements td.a {
    @DexIgnore
    public /* final */ kd<K, V> s;
    @DexIgnore
    public int t; // = 0;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public /* final */ boolean y;
    @DexIgnore
    public qd.a<V> z; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qd.a<V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, qd<V> qdVar) {
            if (qdVar.a()) {
                ld.this.c();
            } else if (!ld.this.h()) {
                List<T> list = qdVar.a;
                boolean z = true;
                if (i == 0) {
                    ld ldVar = ld.this;
                    ldVar.i.a(qdVar.b, list, qdVar.c, qdVar.d, ldVar);
                    ld ldVar2 = ld.this;
                    if (ldVar2.j == -1) {
                        ldVar2.j = qdVar.b + qdVar.d + (list.size() / 2);
                    }
                } else {
                    ld ldVar3 = ld.this;
                    boolean z2 = ldVar3.j > ldVar3.i.f();
                    ld ldVar4 = ld.this;
                    boolean z3 = ldVar4.y && ldVar4.i.b(ldVar4.h.d, ldVar4.l, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            ld ldVar5 = ld.this;
                            ldVar5.i.a(list, (td.a) ldVar5);
                        } else {
                            ld ldVar6 = ld.this;
                            ldVar6.w = 0;
                            ldVar6.u = 0;
                        }
                    } else if (i != 2) {
                        throw new IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        ld ldVar7 = ld.this;
                        ldVar7.i.b(list, (td.a) ldVar7);
                    } else {
                        ld ldVar8 = ld.this;
                        ldVar8.v = 0;
                        ldVar8.t = 0;
                    }
                    ld ldVar9 = ld.this;
                    if (ldVar9.y) {
                        if (z2) {
                            if (ldVar9.t != 1 && ldVar9.i.b(ldVar9.x, ldVar9.h.d, ldVar9.l, ldVar9)) {
                                ld.this.t = 0;
                            }
                        } else if (ldVar9.u != 1 && ldVar9.i.a(ldVar9.x, ldVar9.h.d, ldVar9.l, (td.a) ldVar9)) {
                            ld.this.u = 0;
                        }
                    }
                }
                ld ldVar10 = ld.this;
                if (ldVar10.g != null) {
                    boolean z4 = ldVar10.i.size() == 0;
                    boolean z5 = !z4 && i == 2 && qdVar.a.size() == 0;
                    if (!(!z4 && i == 1 && qdVar.a.size() == 0)) {
                        z = false;
                    }
                    ld.this.a(z4, z5, z);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public b(int i, Object obj) {
            this.e = i;
            this.f = obj;
        }

        @DexIgnore
        public void run() {
            if (!ld.this.h()) {
                if (ld.this.s.isInvalid()) {
                    ld.this.c();
                    return;
                }
                ld ldVar = ld.this;
                ldVar.s.dispatchLoadBefore(this.e, this.f, ldVar.h.a, ldVar.e, ldVar.z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public c(int i, Object obj) {
            this.e = i;
            this.f = obj;
        }

        @DexIgnore
        public void run() {
            if (!ld.this.h()) {
                if (ld.this.s.isInvalid()) {
                    ld.this.c();
                    return;
                }
                ld ldVar = ld.this;
                ldVar.s.dispatchLoadAfter(this.e, this.f, ldVar.h.a, ldVar.e, ldVar.z);
            }
        }
    }

    @DexIgnore
    public ld(kd<K, V> kdVar, Executor executor, Executor executor2, rd.c<V> cVar, rd.f fVar, K k, int i) {
        super(new td(), executor, executor2, cVar, fVar);
        boolean z2 = false;
        this.s = kdVar;
        this.j = i;
        if (this.s.isInvalid()) {
            c();
        } else {
            kd<K, V> kdVar2 = this.s;
            rd.f fVar2 = this.h;
            kdVar2.dispatchLoadInitial(k, fVar2.e, fVar2.a, fVar2.c, this.e, this.z);
        }
        if (this.s.supportsPageDropping() && this.h.d != Integer.MAX_VALUE) {
            z2 = true;
        }
        this.y = z2;
    }

    @DexIgnore
    public static int c(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    public static int d(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    public void a(rd<V> rdVar, rd.e eVar) {
        td<T> tdVar = rdVar.i;
        int g = this.i.g() - tdVar.g();
        int h = this.i.h() - tdVar.h();
        int l = tdVar.l();
        int e = tdVar.e();
        if (tdVar.isEmpty() || g < 0 || h < 0 || this.i.l() != Math.max(l - g, 0) || this.i.e() != Math.max(e - h, 0) || this.i.k() != tdVar.k() + g + h) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (g != 0) {
            int min = Math.min(l, g);
            int i = g - min;
            int e2 = tdVar.e() + tdVar.k();
            if (min != 0) {
                eVar.a(e2, min);
            }
            if (i != 0) {
                eVar.b(e2 + min, i);
            }
        }
        if (h != 0) {
            int min2 = Math.min(e, h);
            int i2 = h - min2;
            if (min2 != 0) {
                eVar.a(e, min2);
            }
            if (i2 != 0) {
                eVar.b(0, i2);
            }
        }
    }

    @DexIgnore
    public void b() {
        this.t = 2;
    }

    @DexIgnore
    public void c(int i, int i2) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public md<?, V> d() {
        return this.s;
    }

    @DexIgnore
    public Object e() {
        return this.s.getKey(this.j, this.k);
    }

    @DexIgnore
    public void f(int i) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public boolean g() {
        return true;
    }

    @DexIgnore
    public void h(int i) {
        int d = d(this.h.b, i, this.i.e());
        int c2 = c(this.h.b, i, this.i.e() + this.i.k());
        this.v = Math.max(d, this.v);
        if (this.v > 0) {
            l();
        }
        this.w = Math.max(c2, this.w);
        if (this.w > 0) {
            k();
        }
    }

    @DexIgnore
    public final void k() {
        if (this.u == 0) {
            this.u = 1;
            this.f.execute(new c(((this.i.e() + this.i.k()) - 1) + this.i.j(), this.i.d()));
        }
    }

    @DexIgnore
    public final void l() {
        if (this.t == 0) {
            this.t = 1;
            this.f.execute(new b(this.i.e() + this.i.j(), this.i.c()));
        }
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        this.w = (this.w - i2) - i3;
        this.u = 0;
        if (this.w > 0) {
            k();
        }
        d(i, i2);
        e(i + i2, i3);
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public void a(int i) {
        boolean z2 = false;
        e(0, i);
        if (this.i.e() > 0 || this.i.l() > 0) {
            z2 = true;
        }
        this.x = z2;
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        this.v = (this.v - i2) - i3;
        this.t = 0;
        if (this.v > 0) {
            l();
        }
        d(i, i2);
        e(0, i3);
        i(i3);
    }

    @DexIgnore
    public void a() {
        this.u = 2;
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }
}
