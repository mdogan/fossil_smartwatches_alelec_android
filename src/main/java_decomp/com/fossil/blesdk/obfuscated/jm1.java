package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jm1 {
    @DexIgnore
    public static /* final */ ee0.g<jc1> a; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0.a<jc1, Object> b; // = new pm1();

    /*
    static {
        new ee0("Phenotype.API", b, a);
        new ic1();
    }
    */

    @DexIgnore
    public static Uri a(String str) {
        String valueOf = String.valueOf(Uri.encode(str));
        return Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
    }
}
