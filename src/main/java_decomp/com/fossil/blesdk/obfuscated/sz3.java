package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sz3 extends jz3 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public sz3() {
    }

    @DexIgnore
    public sz3(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public int a() {
        return 1;
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxapi_sendauth_resp_token");
        this.d = bundle.getString("_wxapi_sendauth_resp_state");
        bundle.getString("_wxapi_sendauth_resp_url");
        bundle.getString("_wxapi_sendauth_resp_lang");
        bundle.getString("_wxapi_sendauth_resp_country");
    }
}
