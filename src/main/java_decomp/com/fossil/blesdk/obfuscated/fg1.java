package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg1 extends kk0 implements Iterable<String> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fg1> CREATOR; // = new hg1();
    @DexIgnore
    public /* final */ Bundle e;

    @DexIgnore
    public fg1(Bundle bundle) {
        this.e = bundle;
    }

    @DexIgnore
    public final Bundle H() {
        return new Bundle(this.e);
    }

    @DexIgnore
    public final Object e(String str) {
        return this.e.get(str);
    }

    @DexIgnore
    public final Long f(String str) {
        return Long.valueOf(this.e.getLong(str));
    }

    @DexIgnore
    public final String g(String str) {
        return this.e.getString(str);
    }

    @DexIgnore
    public final Double h(String str) {
        return Double.valueOf(this.e.getDouble(str));
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new gg1(this);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }

    @DexIgnore
    public final String toString() {
        return this.e.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, H(), false);
        lk0.a(parcel, a);
    }
}
