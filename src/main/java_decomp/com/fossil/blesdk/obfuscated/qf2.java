package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qf2 extends pf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public long I;

    /*
    static {
        K.put(R.id.ll_icClose, 1);
        K.put(R.id.iv_close, 2);
        K.put(R.id.progressBar, 3);
        K.put(R.id.tv_title, 4);
        K.put(R.id.tv_desc, 5);
        K.put(R.id.sv_content, 6);
        K.put(R.id.input_email, 7);
        K.put(R.id.et_email, 8);
        K.put(R.id.iv_checked_email, 9);
        K.put(R.id.input_password, 10);
        K.put(R.id.et_password, 11);
        K.put(R.id.tv_error_check_combine, 12);
        K.put(R.id.tv_error_check_character, 13);
        K.put(R.id.br_bottom, 14);
        K.put(R.id.br_top, 15);
        K.put(R.id.tv_signup_via, 16);
        K.put(R.id.iv_facebook, 17);
        K.put(R.id.iv_google, 18);
        K.put(R.id.iv_apple, 19);
        K.put(R.id.iv_weibo, 20);
        K.put(R.id.iv_wechat, 21);
        K.put(R.id.tv_have_account, 22);
        K.put(R.id.tv_login, 23);
        K.put(R.id.bt_continue, 24);
    }
    */

    @DexIgnore
    public qf2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 25, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public qf2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[14], objArr[15], objArr[24], objArr[8], objArr[11], objArr[7], objArr[10], objArr[19], objArr[9], objArr[2], objArr[17], objArr[18], objArr[21], objArr[20], objArr[1], objArr[3], objArr[6], objArr[5], objArr[13], objArr[12], objArr[22], objArr[23], objArr[16], objArr[4]);
        this.I = -1;
        this.H = objArr[0];
        this.H.setTag((Object) null);
        a(view);
        f();
    }
}
