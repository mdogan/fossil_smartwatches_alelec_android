package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class de0 extends ya {
    @DexIgnore
    public Dialog e; // = null;
    @DexIgnore
    public DialogInterface.OnCancelListener f; // = null;

    @DexIgnore
    public static de0 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        de0 de0 = new de0();
        ck0.a(dialog, (Object) "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        de0.e = dialog2;
        if (onCancelListener != null) {
            de0.f = onCancelListener;
        }
        return de0;
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.e == null) {
            setShowsDialog(false);
        }
        return this.e;
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
