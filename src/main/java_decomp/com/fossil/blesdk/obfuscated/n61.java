package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n61 extends ContentObserver {
    @DexIgnore
    public n61(Handler handler) {
        super((Handler) null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        m61.e.set(true);
    }
}
