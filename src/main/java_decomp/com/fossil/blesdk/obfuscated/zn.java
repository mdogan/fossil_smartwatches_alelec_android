package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.zn;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zn<CHILD extends zn<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public gw<? super TranscodeType> e; // = ew.a();

    @DexIgnore
    public final gw<? super TranscodeType> a() {
        return this.e;
    }

    @DexIgnore
    public final CHILD clone() {
        try {
            return (zn) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }
}
