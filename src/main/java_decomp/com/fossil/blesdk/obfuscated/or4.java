package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class or4 extends sr4.a {
    @DexIgnore
    public boolean a; // = true;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements sr4<qm4, qm4> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public qm4 a(qm4 qm4) throws IOException {
            try {
                return gs4.a(qm4);
            } finally {
                qm4.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements sr4<RequestBody, RequestBody> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            RequestBody requestBody = (RequestBody) obj;
            a(requestBody);
            return requestBody;
        }

        @DexIgnore
        public RequestBody a(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sr4<qm4, qm4> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public qm4 a(qm4 qm4) {
            return qm4;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            qm4 qm4 = (qm4) obj;
            a(qm4);
            return qm4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements sr4<Object, String> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        public String a(Object obj) {
            return obj.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements sr4<qm4, cb4> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public cb4 a(qm4 qm4) {
            qm4.close();
            return cb4.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements sr4<qm4, Void> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public Void a(qm4 qm4) {
            qm4.close();
            return null;
        }
    }

    @DexIgnore
    public sr4<qm4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == qm4.class) {
            if (gs4.a(annotationArr, (Class<? extends Annotation>) vt4.class)) {
                return c.a;
            }
            return a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (!this.a || type != cb4.class) {
                return null;
            }
            try {
                return e.a;
            } catch (NoClassDefFoundError unused) {
                this.a = false;
                return null;
            }
        }
    }

    @DexIgnore
    public sr4<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(gs4.b(type))) {
            return b.a;
        }
        return null;
    }
}
