package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class og1 extends qk1 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public int i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;

    @DexIgnore
    public og1(yh1 yh1) {
        super(yh1);
    }

    @DexIgnore
    public final String A() {
        v();
        return this.j;
    }

    @DexIgnore
    public final String B() {
        v();
        return this.c;
    }

    @DexIgnore
    public final String C() {
        v();
        return this.k;
    }

    @DexIgnore
    public final String D() {
        try {
            Class<?> loadClass = getContext().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
            if (loadClass == null) {
                return null;
            }
            try {
                Object invoke = loadClass.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke((Object) null, new Object[]{getContext()});
                if (invoke == null) {
                    return null;
                }
                try {
                    return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                } catch (Exception unused) {
                    d().x().a("Failed to retrieve Firebase Instance Id");
                    return null;
                }
            } catch (Exception unused2) {
                d().w().a("Failed to obtain Firebase Analytics instance");
                return null;
            }
        } catch (ClassNotFoundException unused3) {
            return null;
        }
    }

    @DexIgnore
    public final int E() {
        v();
        return this.e;
    }

    @DexIgnore
    public final int F() {
        v();
        return this.i;
    }

    @DexIgnore
    public final sl1 a(String str) {
        String str2;
        e();
        f();
        String B = B();
        String A = A();
        v();
        String str3 = this.d;
        long E = (long) E();
        v();
        String str4 = this.f;
        long n = l().n();
        v();
        e();
        if (this.g == 0) {
            this.g = this.a.s().a(getContext(), getContext().getPackageName());
        }
        long j2 = this.g;
        boolean e2 = this.a.e();
        boolean z = !k().u;
        e();
        f();
        if (!l().h(this.c) || this.a.e()) {
            str2 = D();
        } else {
            str2 = null;
        }
        v();
        boolean z2 = z;
        String str5 = str2;
        long j3 = this.h;
        long F = this.a.F();
        int F2 = F();
        yl1 l = l();
        l.f();
        Boolean b = l.b("google_analytics_adid_collection_enabled");
        boolean booleanValue = Boolean.valueOf(b == null || b.booleanValue()).booleanValue();
        yl1 l2 = l();
        l2.f();
        Boolean b2 = l2.b("google_analytics_ssaid_collection_enabled");
        return new sl1(B, A, str3, E, str4, n, j2, str, e2, z2, str5, j3, F, F2, booleanValue, Boolean.valueOf(b2 == null || b2.booleanValue()).booleanValue(), k().z(), C());
    }

    @DexIgnore
    public final boolean x() {
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0195 A[Catch:{ IllegalStateException -> 0x01c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0196 A[Catch:{ IllegalStateException -> 0x01c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x019f A[Catch:{ IllegalStateException -> 0x01c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01b2 A[Catch:{ IllegalStateException -> 0x01c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01dc  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01e7  */
    public final void y() {
        boolean z;
        boolean z2;
        String a;
        String str;
        String packageName = getContext().getPackageName();
        PackageManager packageManager = getContext().getPackageManager();
        String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str3 = "";
        String str4 = "unknown";
        int i2 = Integer.MIN_VALUE;
        if (packageManager == null) {
            d().s().a("PackageManager is null, app identity information might be inaccurate. appId", ug1.a(packageName));
        } else {
            try {
                str4 = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException unused) {
                d().s().a("Error retrieving app installer package name. appId", ug1.a(packageName));
            }
            if (str4 == null) {
                str4 = "manual_install";
            } else if ("com.android.vending".equals(str4)) {
                str4 = str3;
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    str = !TextUtils.isEmpty(applicationLabel) ? applicationLabel.toString() : str2;
                    try {
                        str2 = packageInfo.versionName;
                        i2 = packageInfo.versionCode;
                    } catch (PackageManager.NameNotFoundException unused2) {
                        d().s().a("Error retrieving package info. appId, appName", ug1.a(packageName), str);
                        this.c = packageName;
                        this.f = str4;
                        this.d = str2;
                        this.e = i2;
                        this.g = 0;
                        b();
                        Status a2 = xe0.a(getContext());
                        z = true;
                        z2 = (a2 == null && a2.L()) | (TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B()));
                        if (!z2) {
                        }
                        if (z2) {
                        }
                        z = false;
                        this.j = str3;
                        this.k = str3;
                        this.h = 0;
                        b();
                        this.k = this.a.A();
                        a = xe0.a();
                        if (TextUtils.isEmpty(a)) {
                        }
                        this.j = str3;
                        if (!TextUtils.isEmpty(a)) {
                        }
                        if (z) {
                        }
                        if (Build.VERSION.SDK_INT < 16) {
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException unused3) {
                str = str2;
                d().s().a("Error retrieving package info. appId, appName", ug1.a(packageName), str);
                this.c = packageName;
                this.f = str4;
                this.d = str2;
                this.e = i2;
                this.g = 0;
                b();
                Status a22 = xe0.a(getContext());
                z = true;
                z2 = (a22 == null && a22.L()) | (TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B()));
                if (!z2) {
                }
                if (z2) {
                }
                z = false;
                this.j = str3;
                this.k = str3;
                this.h = 0;
                b();
                this.k = this.a.A();
                a = xe0.a();
                if (TextUtils.isEmpty(a)) {
                }
                this.j = str3;
                if (!TextUtils.isEmpty(a)) {
                }
                if (z) {
                }
                if (Build.VERSION.SDK_INT < 16) {
                }
            }
        }
        this.c = packageName;
        this.f = str4;
        this.d = str2;
        this.e = i2;
        this.g = 0;
        b();
        Status a222 = xe0.a(getContext());
        z = true;
        z2 = (a222 == null && a222.L()) | (TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B()));
        if (!z2) {
            if (a222 == null) {
                d().s().a("GoogleService failed to initialize (no status)");
            } else {
                d().s().a("GoogleService failed to initialize, status", Integer.valueOf(a222.I()), a222.J());
            }
        }
        if (z2) {
            Boolean p = l().p();
            if (l().o()) {
                if (this.a.z()) {
                    d().y().a("Collection disabled with firebase_analytics_collection_deactivated=1");
                }
            } else if (p == null || p.booleanValue()) {
                if (p != null || !xe0.b()) {
                    d().A().a("Collection enabled");
                    this.j = str3;
                    this.k = str3;
                    this.h = 0;
                    b();
                    if (!TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B())) {
                        this.k = this.a.A();
                    }
                    a = xe0.a();
                    if (TextUtils.isEmpty(a)) {
                        str3 = a;
                    }
                    this.j = str3;
                    if (!TextUtils.isEmpty(a)) {
                        this.k = new ik0(getContext()).a("admob_app_id");
                    }
                    if (z) {
                        d().A().a("App package, google app id", this.c, this.j);
                    }
                    if (Build.VERSION.SDK_INT < 16) {
                        this.i = an0.a(getContext()) ? 1 : 0;
                        return;
                    } else {
                        this.i = 0;
                        return;
                    }
                } else {
                    d().y().a("Collection disabled with google_app_measurement_enable=0");
                }
            } else if (this.a.z()) {
                d().y().a("Collection disabled with firebase_analytics_collection_enabled=0");
            }
        }
        z = false;
        this.j = str3;
        this.k = str3;
        this.h = 0;
        b();
        this.k = this.a.A();
        try {
            a = xe0.a();
            if (TextUtils.isEmpty(a)) {
            }
            this.j = str3;
            if (!TextUtils.isEmpty(a)) {
            }
            if (z) {
            }
        } catch (IllegalStateException e2) {
            d().s().a("getGoogleAppId or isMeasurementEnabled failed with exception. appId", ug1.a(packageName), e2);
        }
        if (Build.VERSION.SDK_INT < 16) {
        }
    }
}
