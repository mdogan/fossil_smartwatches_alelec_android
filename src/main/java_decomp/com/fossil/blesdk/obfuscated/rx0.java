package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.rx0;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rx0<M extends rx0<M>> extends wx0 {
    @DexIgnore
    public tx0 f;

    @DexIgnore
    public void a(qx0 qx0) throws IOException {
        if (this.f != null) {
            for (int i = 0; i < this.f.b(); i++) {
                this.f.a(i).a(qx0);
            }
        }
    }

    @DexIgnore
    public int b() {
        if (this.f != null) {
            for (int i = 0; i < this.f.b(); i++) {
                this.f.a(i).b();
            }
        }
        return 0;
    }

    @DexIgnore
    public /* synthetic */ wx0 c() throws CloneNotSupportedException {
        return (rx0) clone();
    }

    @DexIgnore
    /* renamed from: d */
    public M clone() throws CloneNotSupportedException {
        M m = (rx0) super.clone();
        vx0.a(this, (rx0) m);
        return m;
    }
}
