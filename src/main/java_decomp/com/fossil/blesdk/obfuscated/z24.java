package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ Context f;
    @DexIgnore
    public /* final */ /* synthetic */ l04 g;

    @DexIgnore
    public z24(String str, Context context, l04 l04) {
        this.e = str;
        this.f = context;
        this.g = l04;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (k04.k) {
                if (k04.k.size() >= i04.h()) {
                    u14 f2 = k04.m;
                    f2.d("The number of page events exceeds the maximum value " + Integer.toString(i04.h()));
                    return;
                }
                String unused = k04.i = this.e;
                if (k04.k.containsKey(k04.i)) {
                    u14 f3 = k04.m;
                    f3.c("Duplicate PageID : " + k04.i + ", onResume() repeated?");
                    return;
                }
                k04.k.put(k04.i, Long.valueOf(System.currentTimeMillis()));
                k04.a(this.f, true, this.g);
            }
        } catch (Throwable th) {
            k04.m.a(th);
            k04.a(this.f, th);
        }
    }
}
