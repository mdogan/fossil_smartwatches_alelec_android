package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.FirebaseApp;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ew1 implements dw1 {
    @DexIgnore
    public static volatile dw1 b;
    @DexIgnore
    public /* final */ AppMeasurement a;

    @DexIgnore
    public ew1(AppMeasurement appMeasurement) {
        ck0.a(appMeasurement);
        this.a = appMeasurement;
        new ConcurrentHashMap();
    }

    @DexIgnore
    public static dw1 a(FirebaseApp firebaseApp, Context context, cx1 cx1) {
        ck0.a(firebaseApp);
        ck0.a(context);
        ck0.a(cx1);
        ck0.a(context.getApplicationContext());
        if (b == null) {
            synchronized (ew1.class) {
                if (b == null) {
                    Bundle bundle = new Bundle(1);
                    if (firebaseApp.d()) {
                        cx1.a(bw1.class, hw1.e, iw1.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", firebaseApp.isDataCollectionDefaultEnabled());
                    }
                    b = new ew1(yh1.a(context, pg1.a(bundle)).y());
                }
            }
        }
        return b;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (gw1.a(str) && gw1.a(str2, bundle) && gw1.a(str, str2, bundle)) {
            this.a.logEventInternal(str, str2, bundle);
        }
    }

    @DexIgnore
    public void a(String str, String str2, Object obj) {
        if (gw1.a(str) && gw1.a(str, str2)) {
            this.a.a(str, str2, obj);
        }
    }

    @DexIgnore
    public static final /* synthetic */ void a(zw1 zw1) {
        boolean z = ((bw1) zw1.a()).a;
        synchronized (ew1.class) {
            ((ew1) b).a.b(z);
        }
    }
}
