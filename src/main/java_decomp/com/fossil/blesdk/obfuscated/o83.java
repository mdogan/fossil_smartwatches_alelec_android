package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o83 implements Factory<v83> {
    @DexIgnore
    public static v83 a(l83 l83) {
        v83 c = l83.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
