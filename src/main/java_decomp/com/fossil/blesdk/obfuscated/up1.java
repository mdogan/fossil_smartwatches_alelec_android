package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class up1 extends cj0 implements gp1 {
    @DexIgnore
    public up1(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    @DexIgnore
    public final String a() {
        return c("asset_key");
    }

    @DexIgnore
    public final String getId() {
        return c("asset_id");
    }
}
