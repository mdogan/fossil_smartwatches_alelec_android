package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s42 implements Factory<Context> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public s42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static s42 a(o42 o42) {
        return new s42(o42);
    }

    @DexIgnore
    public static Context b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static Context c(o42 o42) {
        Context c = o42.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    public Context get() {
        return b(this.a);
    }
}
