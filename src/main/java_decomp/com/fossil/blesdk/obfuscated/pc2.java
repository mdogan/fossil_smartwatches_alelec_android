package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.chart.WeekHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ WeekHeartRateChart q;

    @DexIgnore
    public pc2(Object obj, View view, int i, WeekHeartRateChart weekHeartRateChart) {
        super(obj, view, i);
        this.q = weekHeartRateChart;
    }
}
