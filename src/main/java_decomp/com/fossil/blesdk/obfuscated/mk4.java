package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mk4<T> extends gg4<T> implements rc4 {
    @DexIgnore
    public /* final */ kc4<T> h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mk4(CoroutineContext coroutineContext, kc4<? super T> kc4) {
        super(coroutineContext, true);
        wd4.b(coroutineContext, "context");
        wd4.b(kc4, "uCont");
        this.h = kc4;
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (obj instanceof zg4) {
            Throwable th = ((zg4) obj).a;
            if (i != 4) {
                th = ok4.a(th, (kc4<?>) this.h);
            }
            ij4.a(this.h, th, i);
            return;
        }
        ij4.b(this.h, obj, i);
    }

    @DexIgnore
    public final boolean f() {
        return true;
    }

    @DexIgnore
    public final rc4 getCallerFrame() {
        return (rc4) this.h;
    }

    @DexIgnore
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }
}
