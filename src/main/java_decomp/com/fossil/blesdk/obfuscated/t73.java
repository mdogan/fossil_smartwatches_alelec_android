package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t73 implements Factory<ac3> {
    @DexIgnore
    public static ac3 a(o73 o73) {
        ac3 e = o73.e();
        o44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
