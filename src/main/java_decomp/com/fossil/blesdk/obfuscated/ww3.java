package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ww3 {
    @DexIgnore
    public static boolean a(String str) {
        return str.equals("POST") || str.equals("PATCH") || str.equals("PUT") || str.equals("DELETE");
    }

    @DexIgnore
    public static boolean b(String str) {
        return c(str) || str.equals("DELETE");
    }

    @DexIgnore
    public static boolean c(String str) {
        return str.equals("POST") || str.equals("PUT") || str.equals("PATCH");
    }
}
