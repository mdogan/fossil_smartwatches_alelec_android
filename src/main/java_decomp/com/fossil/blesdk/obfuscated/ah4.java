package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ah4 {
    @DexIgnore
    public static final <T> Object a(Object obj) {
        if (Result.m9isSuccessimpl(obj)) {
            za4.a(obj);
            return obj;
        }
        Throwable r4 = Result.m6exceptionOrNullimpl(obj);
        if (r4 != null) {
            return new zg4(r4, false, 2, (rd4) null);
        }
        wd4.a();
        throw null;
    }
}
