package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j04 {
    @DexIgnore
    public static void a(Context context) {
        k04.b(context, (l04) null);
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties) {
        k04.a(context, str, properties, (l04) null);
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2) {
        return k04.a(context, str, str2, (l04) null);
    }

    @DexIgnore
    public static void b(Context context) {
        k04.c(context, (l04) null);
    }
}
