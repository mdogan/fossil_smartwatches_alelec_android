package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sa1 extends ra1<FieldDescriptorType, Object> {
    @DexIgnore
    public sa1(int i) {
        super(i, (sa1) null);
    }

    @DexIgnore
    public final void b() {
        if (!a()) {
            for (int i = 0; i < c(); i++) {
                Map.Entry a = a(i);
                if (((p81) a.getKey()).g()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : d()) {
                if (((p81) entry.getKey()).g()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.b();
    }
}
