package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.af0;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z31 {
    @DexIgnore
    public /* final */ n41<v31> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<af0.a<sc1>, e41> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<af0.a<Object>, d41> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<af0.a<rc1>, a41> f; // = new HashMap();

    @DexIgnore
    public z31(Context context, n41<v31> n41) {
        this.b = context;
        this.a = n41;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.a.a();
        return this.a.b().b(this.b.getPackageName());
    }

    @DexIgnore
    public final e41 a(af0<sc1> af0) {
        e41 e41;
        synchronized (this.d) {
            e41 = this.d.get(af0.b());
            if (e41 == null) {
                e41 = new e41(af0);
            }
            this.d.put(af0.b(), e41);
        }
        return e41;
    }

    @DexIgnore
    public final void a(af0.a<sc1> aVar, s31 s31) throws RemoteException {
        this.a.a();
        ck0.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.d) {
            e41 remove = this.d.remove(aVar);
            if (remove != null) {
                remove.o();
                this.a.b().a(l41.a((xd1) remove, s31));
            }
        }
    }

    @DexIgnore
    public final void a(j41 j41, af0<rc1> af0, s31 s31) throws RemoteException {
        this.a.a();
        this.a.b().a(new l41(1, j41, (IBinder) null, (PendingIntent) null, b(af0).asBinder(), s31 != null ? s31.asBinder() : null));
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, af0<sc1> af0, s31 s31) throws RemoteException {
        this.a.a();
        this.a.b().a(new l41(1, j41.a(locationRequest), a(af0).asBinder(), (PendingIntent) null, (IBinder) null, s31 != null ? s31.asBinder() : null));
    }

    @DexIgnore
    public final void a(boolean z) throws RemoteException {
        this.a.a();
        this.a.b().e(z);
        this.c = z;
    }

    @DexIgnore
    public final a41 b(af0<rc1> af0) {
        a41 a41;
        synchronized (this.f) {
            a41 = this.f.get(af0.b());
            if (a41 == null) {
                a41 = new a41(af0);
            }
            this.f.put(af0.b(), a41);
        }
        return a41;
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (e41 next : this.d.values()) {
                if (next != null) {
                    this.a.b().a(l41.a((xd1) next, (s31) null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (a41 next2 : this.f.values()) {
                if (next2 != null) {
                    this.a.b().a(l41.a((ud1) next2, (s31) null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (d41 next3 : this.e.values()) {
                if (next3 != null) {
                    this.a.b().a(new w41(2, (u41) null, next3.asBinder(), (IBinder) null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final void b(af0.a<rc1> aVar, s31 s31) throws RemoteException {
        this.a.a();
        ck0.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.f) {
            a41 remove = this.f.remove(aVar);
            if (remove != null) {
                remove.o();
                this.a.b().a(l41.a((ud1) remove, s31));
            }
        }
    }

    @DexIgnore
    public final void c() throws RemoteException {
        if (this.c) {
            a(false);
        }
    }
}
