package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g43 implements Factory<ComplicationSearchPresenter> {
    @DexIgnore
    public static ComplicationSearchPresenter a(c43 c43, ComplicationRepository complicationRepository, fn2 fn2) {
        return new ComplicationSearchPresenter(c43, complicationRepository, fn2);
    }
}
