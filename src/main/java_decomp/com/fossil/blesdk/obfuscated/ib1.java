package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ib1 extends AbstractList<String> implements i91, RandomAccess {
    @DexIgnore
    public /* final */ i91 e;

    @DexIgnore
    public ib1(i91 i91) {
        this.e = i91;
    }

    @DexIgnore
    public final i91 C() {
        return this;
    }

    @DexIgnore
    public final List<?> E() {
        return this.e.E();
    }

    @DexIgnore
    public final void a(zzte zzte) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object d(int i) {
        return this.e.d(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return (String) this.e.get(i);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new kb1(this);
    }

    @DexIgnore
    public final ListIterator<String> listIterator(int i) {
        return new jb1(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }
}
