package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.blesdk.obfuscated.p1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u1 extends n1 implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, p1, View.OnKeyListener {
    @DexIgnore
    public static /* final */ int z; // = x.abc_popup_menu_item_layout;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ h1 g;
    @DexIgnore
    public /* final */ g1 h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ p2 m;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener n; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener o; // = new b();
    @DexIgnore
    public PopupWindow.OnDismissListener p;
    @DexIgnore
    public View q;
    @DexIgnore
    public View r;
    @DexIgnore
    public p1.a s;
    @DexIgnore
    public ViewTreeObserver t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x; // = 0;
    @DexIgnore
    public boolean y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (u1.this.d() && !u1.this.m.l()) {
                View view = u1.this.r;
                if (view == null || !view.isShown()) {
                    u1.this.dismiss();
                } else {
                    u1.this.m.c();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = u1.this.t;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    u1.this.t = view.getViewTreeObserver();
                }
                u1 u1Var = u1.this;
                u1Var.t.removeGlobalOnLayoutListener(u1Var.n);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore
    public u1(Context context, h1 h1Var, View view, int i2, int i3, boolean z2) {
        this.f = context;
        this.g = h1Var;
        this.i = z2;
        this.h = new g1(h1Var, LayoutInflater.from(context), this.i, z);
        this.k = i2;
        this.l = i3;
        Resources resources = context.getResources();
        this.j = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(u.abc_config_prefDialogWidth));
        this.q = view;
        this.m = new p2(this.f, (AttributeSet) null, this.k, this.l);
        h1Var.a((p1) this, context);
    }

    @DexIgnore
    public void a(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    public void a(h1 h1Var) {
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.h.a(z2);
    }

    @DexIgnore
    public void c() {
        if (!h()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @DexIgnore
    public boolean d() {
        return !this.u && this.m.d();
    }

    @DexIgnore
    public void dismiss() {
        if (d()) {
            this.m.dismiss();
        }
    }

    @DexIgnore
    public ListView e() {
        return this.m.e();
    }

    @DexIgnore
    public final boolean h() {
        if (d()) {
            return true;
        }
        if (!this.u) {
            View view = this.q;
            if (view != null) {
                this.r = view;
                this.m.a((PopupWindow.OnDismissListener) this);
                this.m.a((AdapterView.OnItemClickListener) this);
                this.m.a(true);
                View view2 = this.r;
                boolean z2 = this.t == null;
                this.t = view2.getViewTreeObserver();
                if (z2) {
                    this.t.addOnGlobalLayoutListener(this.n);
                }
                view2.addOnAttachStateChangeListener(this.o);
                this.m.a(view2);
                this.m.c(this.x);
                if (!this.v) {
                    this.w = n1.a(this.h, (ViewGroup) null, this.f, this.j);
                    this.v = true;
                }
                this.m.b(this.w);
                this.m.e(2);
                this.m.a(g());
                this.m.c();
                ListView e = this.m.e();
                e.setOnKeyListener(this);
                if (this.y && this.g.h() != null) {
                    FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f).inflate(x.abc_popup_menu_header_item_layout, e, false);
                    TextView textView = (TextView) frameLayout.findViewById(16908310);
                    if (textView != null) {
                        textView.setText(this.g.h());
                    }
                    frameLayout.setEnabled(false);
                    e.addHeaderView(frameLayout, (Object) null, false);
                }
                this.m.a((ListAdapter) this.h);
                this.m.c();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void onDismiss() {
        this.u = true;
        this.g.close();
        ViewTreeObserver viewTreeObserver = this.t;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.t = this.r.getViewTreeObserver();
            }
            this.t.removeGlobalOnLayoutListener(this.n);
            this.t = null;
        }
        this.r.removeOnAttachStateChangeListener(this.o);
        PopupWindow.OnDismissListener onDismissListener = this.p;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.v = false;
        g1 g1Var = this.h;
        if (g1Var != null) {
            g1Var.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void b(int i2) {
        this.m.d(i2);
    }

    @DexIgnore
    public void c(int i2) {
        this.m.h(i2);
    }

    @DexIgnore
    public void c(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void a(p1.a aVar) {
        this.s = aVar;
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        if (v1Var.hasVisibleItems()) {
            o1 o1Var = new o1(this.f, v1Var, this.r, this.i, this.k, this.l);
            o1Var.a(this.s);
            o1Var.a(n1.b((h1) v1Var));
            o1Var.a(this.p);
            this.p = null;
            this.g.a(false);
            int h2 = this.m.h();
            int i2 = this.m.i();
            if ((Gravity.getAbsoluteGravity(this.x, g9.k(this.q)) & 7) == 5) {
                h2 += this.q.getWidth();
            }
            if (o1Var.a(h2, i2)) {
                p1.a aVar = this.s;
                if (aVar == null) {
                    return true;
                }
                aVar.a(v1Var);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z2) {
        if (h1Var == this.g) {
            dismiss();
            p1.a aVar = this.s;
            if (aVar != null) {
                aVar.a(h1Var, z2);
            }
        }
    }

    @DexIgnore
    public void a(View view) {
        this.q = view;
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.p = onDismissListener;
    }
}
