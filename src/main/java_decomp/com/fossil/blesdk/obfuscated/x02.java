package com.fossil.blesdk.obfuscated;

import java.lang.reflect.AccessibleObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class x02 {
    @DexIgnore
    public static /* final */ x02 a; // = (l02.b() < 9 ? new w02() : new y02());

    @DexIgnore
    public static x02 a() {
        return a;
    }

    @DexIgnore
    public abstract void a(AccessibleObject accessibleObject);
}
