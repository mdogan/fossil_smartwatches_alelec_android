package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.km4;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pm4 {
    @DexIgnore
    public /* final */ lm4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ km4 c;
    @DexIgnore
    public /* final */ RequestBody d;
    @DexIgnore
    public /* final */ Map<Class<?>, Object> e;
    @DexIgnore
    public volatile ul4 f;

    @DexIgnore
    public pm4(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c.a();
        this.d = aVar.d;
        this.e = vm4.a(aVar.e);
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }

    @DexIgnore
    public List<String> b(String str) {
        return this.c.b(str);
    }

    @DexIgnore
    public km4 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.h();
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public a f() {
        return new a(this);
    }

    @DexIgnore
    public lm4 g() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.a + ", tags=" + this.e + '}';
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public lm4 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public km4.a c;
        @DexIgnore
        public RequestBody d;
        @DexIgnore
        public Map<Class<?>, Object> e;

        @DexIgnore
        public a() {
            this.e = Collections.emptyMap();
            this.b = "GET";
            this.c = new km4.a();
        }

        @DexIgnore
        public a a(lm4 lm4) {
            if (lm4 != null) {
                this.a = lm4;
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                a(lm4.d(str));
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.c.c(str);
            return this;
        }

        @DexIgnore
        public a(pm4 pm4) {
            Map<Class<?>, Object> map;
            this.e = Collections.emptyMap();
            this.a = pm4.a;
            this.b = pm4.b;
            this.d = pm4.d;
            if (pm4.e.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = new LinkedHashMap<>(pm4.e);
            }
            this.e = map;
            this.c = pm4.c.a();
        }

        @DexIgnore
        public a a(km4 km4) {
            this.c = km4.a();
            return this;
        }

        @DexIgnore
        public a a(RequestBody requestBody) {
            a("POST", requestBody);
            return this;
        }

        @DexIgnore
        public a a(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !on4.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !on4.e(str)) {
                this.b = str;
                this.d = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            this.c.c(str, str2);
            return this;
        }

        @DexIgnore
        public a b() {
            a("GET", (RequestBody) null);
            return this;
        }

        @DexIgnore
        public <T> a a(Class<? super T> cls, T t) {
            if (cls != null) {
                if (t == null) {
                    this.e.remove(cls);
                } else {
                    if (this.e.isEmpty()) {
                        this.e = new LinkedHashMap();
                    }
                    this.e.put(cls, cls.cast(t));
                }
                return this;
            }
            throw new NullPointerException("type == null");
        }

        @DexIgnore
        public pm4 a() {
            if (this.a != null) {
                return new pm4(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    @DexIgnore
    public RequestBody a() {
        return this.d;
    }

    @DexIgnore
    public ul4 b() {
        ul4 ul4 = this.f;
        if (ul4 != null) {
            return ul4;
        }
        ul4 a2 = ul4.a(this.c);
        this.f = a2;
        return a2;
    }

    @DexIgnore
    public <T> T a(Class<? extends T> cls) {
        return cls.cast(this.e.get(cls));
    }
}
