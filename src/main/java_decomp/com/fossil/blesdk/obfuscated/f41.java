package com.fossil.blesdk.obfuscated;

import android.location.Location;
import com.fossil.blesdk.obfuscated.af0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f41 implements af0.b<sc1> {
    @DexIgnore
    public /* final */ /* synthetic */ Location a;

    @DexIgnore
    public f41(e41 e41, Location location) {
        this.a = location;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((sc1) obj).onLocationChanged(this.a);
    }
}
