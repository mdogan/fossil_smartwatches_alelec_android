package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;
import com.fossil.blesdk.obfuscated.l1;
import com.fossil.blesdk.obfuscated.n8;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m1 extends l1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l1.a implements ActionProvider.VisibilityListener {
        @DexIgnore
        public n8.b c;

        @DexIgnore
        public a(m1 m1Var, Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        @DexIgnore
        public boolean isVisible() {
            return this.a.isVisible();
        }

        @DexIgnore
        public void onActionProviderVisibilityChanged(boolean z) {
            n8.b bVar = this.c;
            if (bVar != null) {
                bVar.onActionProviderVisibilityChanged(z);
            }
        }

        @DexIgnore
        public View onCreateActionView(MenuItem menuItem) {
            return this.a.onCreateActionView(menuItem);
        }

        @DexIgnore
        public boolean overridesItemVisibility() {
            return this.a.overridesItemVisibility();
        }

        @DexIgnore
        public void refreshVisibility() {
            this.a.refreshVisibility();
        }

        @DexIgnore
        public void setVisibilityListener(n8.b bVar) {
            this.c = bVar;
            this.a.setVisibilityListener(bVar != null ? this : null);
        }
    }

    @DexIgnore
    public m1(Context context, i7 i7Var) {
        super(context, i7Var);
    }

    @DexIgnore
    public l1.a a(ActionProvider actionProvider) {
        return new a(this, this.b, actionProvider);
    }
}
