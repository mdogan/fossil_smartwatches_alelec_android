package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q82 extends p82 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ RelativeLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.cl_toolbar, 1);
        x.put(R.id.aciv_back, 2);
        x.put(R.id.tv_title, 3);
        x.put(R.id.ln_get_supports, 4);
        x.put(R.id.bt_terms_of_use, 5);
        x.put(R.id.bt_privacy_policy, 6);
        x.put(R.id.bt_open_source_license, 7);
    }
    */

    @DexIgnore
    public q82(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 8, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public q82(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[2], objArr[7], objArr[6], objArr[5], objArr[1], objArr[4], objArr[3]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
