package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.uj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ek0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ek0> CREATOR; // = new ml0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public IBinder f;
    @DexIgnore
    public vd0 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public ek0(int i2, IBinder iBinder, vd0 vd0, boolean z, boolean z2) {
        this.e = i2;
        this.f = iBinder;
        this.g = vd0;
        this.h = z;
        this.i = z2;
    }

    @DexIgnore
    public uj0 H() {
        return uj0.a.a(this.f);
    }

    @DexIgnore
    public vd0 I() {
        return this.g;
    }

    @DexIgnore
    public boolean J() {
        return this.h;
    }

    @DexIgnore
    public boolean K() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ek0)) {
            return false;
        }
        ek0 ek0 = (ek0) obj;
        return this.g.equals(ek0.g) && H().equals(ek0.H());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f, false);
        lk0.a(parcel, 3, (Parcelable) I(), i2, false);
        lk0.a(parcel, 4, J());
        lk0.a(parcel, 5, K());
        lk0.a(parcel, a);
    }
}
