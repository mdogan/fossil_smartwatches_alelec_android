package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ff2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ TextInputLayout B;
    @DexIgnore
    public /* final */ TextInputLayout C;
    @DexIgnore
    public /* final */ ImageView D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ ImageView F;
    @DexIgnore
    public /* final */ ImageView G;
    @DexIgnore
    public /* final */ ImageView H;
    @DexIgnore
    public /* final */ DashBar I;
    @DexIgnore
    public /* final */ AppCompatCheckBox q;
    @DexIgnore
    public /* final */ AppCompatCheckBox r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ TextInputEditText t;
    @DexIgnore
    public /* final */ TextInputEditText u;
    @DexIgnore
    public /* final */ TextInputEditText v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ FlexibleButton x;
    @DexIgnore
    public /* final */ FlexibleButton y;
    @DexIgnore
    public /* final */ FlexibleButton z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ff2(Object obj, View view, int i, AppCompatCheckBox appCompatCheckBox, AppCompatCheckBox appCompatCheckBox2, TextInputEditText textInputEditText, TextInputEditText textInputEditText2, TextInputEditText textInputEditText3, TextInputEditText textInputEditText4, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, TextInputLayout textInputLayout4, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, ImageView imageView5, DashBar dashBar, ScrollView scrollView, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = appCompatCheckBox;
        this.r = appCompatCheckBox2;
        this.s = textInputEditText;
        this.t = textInputEditText2;
        this.u = textInputEditText3;
        this.v = textInputEditText4;
        this.w = flexibleButton;
        this.x = flexibleButton2;
        this.y = flexibleButton3;
        this.z = flexibleButton4;
        this.A = flexibleTextView3;
        this.B = textInputLayout;
        this.C = textInputLayout2;
        this.D = imageView;
        this.E = imageView2;
        this.F = imageView3;
        this.G = imageView4;
        this.H = imageView5;
        this.I = dashBar;
    }
}
