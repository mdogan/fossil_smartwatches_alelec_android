package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ex2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nx2 extends hx2 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public ex2 g;
    @DexIgnore
    public /* final */ ix2 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<ex2.a> {
        @DexIgnore
        public /* final */ /* synthetic */ nx2 a;

        @DexIgnore
        public b(nx2 nx2) {
            this.a = nx2;
        }

        @DexIgnore
        public final void a(ex2.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String i = nx2.i;
            local.d(i, "NotificationSettingChanged value = " + aVar);
            this.a.f = aVar.b();
            this.a.h.m(aVar.a());
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = nx2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public nx2(ix2 ix2) {
        wd4.b(ix2, "mView");
        this.h = ix2;
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "start: isCall = " + this.f);
        ex2 ex2 = this.g;
        if (ex2 != null) {
            MutableLiveData<ex2.a> c = ex2.c();
            ix2 ix2 = this.h;
            if (ix2 != null) {
                c.a((jx2) ix2, new b(this));
                this.h.d(a(this.f));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        wd4.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "stop");
        ex2 ex2 = this.g;
        if (ex2 != null) {
            MutableLiveData<ex2.a> c = ex2.c();
            ix2 ix2 = this.h;
            if (ix2 != null) {
                c.a((LifecycleOwner) (jx2) ix2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        wd4.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(ex2 ex2) {
        wd4.b(ex2, "viewModel");
        this.g = ex2;
    }

    @DexIgnore
    public final String a(boolean z) {
        if (z) {
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_CallsMessagesSettings_Text__AllowCallsFrom);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return a2;
        }
        String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_CallsMessagesSettings_Text__AllowMessagesFrom);
        wd4.a((Object) a3, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return a3;
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i2);
        ex2.a aVar = new ex2.a(i2, this.f);
        ex2 ex2 = this.g;
        if (ex2 != null) {
            ex2.c().a(aVar);
        } else {
            wd4.d("mNotificationSettingViewModel");
            throw null;
        }
    }
}
