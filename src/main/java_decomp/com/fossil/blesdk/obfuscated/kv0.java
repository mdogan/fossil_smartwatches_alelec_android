package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kv0 implements lw0 {
    @DexIgnore
    public static /* final */ sv0 b; // = new lv0();
    @DexIgnore
    public /* final */ sv0 a;

    @DexIgnore
    public kv0() {
        this(new mv0(ru0.a(), a()));
    }

    @DexIgnore
    public kv0(sv0 sv0) {
        uu0.a(sv0, "messageInfoFactory");
        this.a = sv0;
    }

    @DexIgnore
    public static sv0 a() {
        try {
            return (sv0) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    @DexIgnore
    public static boolean a(rv0 rv0) {
        return rv0.a() == su0.e.i;
    }

    @DexIgnore
    public final <T> kw0<T> a(Class<T> cls) {
        Class<su0> cls2 = su0.class;
        mw0.a((Class<?>) cls);
        rv0 zzb = this.a.zzb(cls);
        if (zzb.b()) {
            return cls2.isAssignableFrom(cls) ? yv0.a(mw0.c(), ku0.b(), zzb.c()) : yv0.a(mw0.a(), ku0.c(), zzb.c());
        }
        if (cls2.isAssignableFrom(cls)) {
            boolean a2 = a(zzb);
            aw0 b2 = cw0.b();
            fv0 b3 = fv0.b();
            bx0<?, ?> c = mw0.c();
            if (a2) {
                return xv0.a(cls, zzb, b2, b3, c, ku0.b(), qv0.b());
            }
            return xv0.a(cls, zzb, b2, b3, c, (hu0<?>) null, qv0.b());
        }
        boolean a3 = a(zzb);
        aw0 a4 = cw0.a();
        fv0 a5 = fv0.a();
        if (a3) {
            return xv0.a(cls, zzb, a4, a5, mw0.a(), ku0.c(), qv0.a());
        }
        return xv0.a(cls, zzb, a4, a5, mw0.b(), (hu0<?>) null, qv0.a());
    }
}
