package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ir0 {
    @DexIgnore
    public static ir0 a;

    @DexIgnore
    public static synchronized ir0 a() {
        ir0 ir0;
        synchronized (ir0.class) {
            if (a == null) {
                a = new er0();
            }
            ir0 = a;
        }
        return ir0;
    }

    @DexIgnore
    public abstract jr0<Boolean> a(String str, boolean z);
}
