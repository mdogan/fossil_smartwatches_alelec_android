package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uo<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        uo<T> a(T t);

        @DexIgnore
        Class<T> getDataClass();
    }

    @DexIgnore
    void a();

    @DexIgnore
    T b() throws IOException;
}
