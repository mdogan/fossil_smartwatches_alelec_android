package com.fossil.blesdk.obfuscated;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd4 {
    @DexIgnore
    public ve4 a(Class cls, String str) {
        return new xd4(cls, str);
    }

    @DexIgnore
    public we4 a(FunctionReference functionReference) {
        return functionReference;
    }

    @DexIgnore
    public ye4 a(PropertyReference1 propertyReference1) {
        return propertyReference1;
    }

    @DexIgnore
    public te4 a(Class cls) {
        return new pd4(cls);
    }

    @DexIgnore
    public String a(Lambda lambda) {
        return a((td4) lambda);
    }

    @DexIgnore
    public String a(td4 td4) {
        String obj = td4.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
