package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class to3 extends qo3 {
    @DexIgnore
    public RequestEmailOtp f;
    @DexIgnore
    public VerifyEmailOtp g;
    @DexIgnore
    public SignUpEmailAuth h;
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ro3 j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<RequestEmailOtp.d, RequestEmailOtp.c> {
        @DexIgnore
        public /* final */ /* synthetic */ to3 a;

        @DexIgnore
        public b(to3 to3) {
            this.a = to3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(RequestEmailOtp.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.j.i();
            this.a.j.D0();
        }

        @DexIgnore
        public void a(RequestEmailOtp.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.j.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "resendOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.j.d(cVar.a(), cVar.b());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<VerifyEmailOtp.d, VerifyEmailOtp.c> {
        @DexIgnore
        public /* final */ /* synthetic */ to3 a;

        @DexIgnore
        public c(to3 to3) {
            this.a = to3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(VerifyEmailOtp.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.j.i();
            ro3 a2 = this.a.j;
            SignUpEmailAuth l = this.a.l();
            if (l != null) {
                a2.a(l.getEmail(), 10, 20);
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public void a(VerifyEmailOtp.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.j.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.R.a();
            local.d(a2, "verifyOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.j.I(cVar.a() == 401);
            this.a.j.d(cVar.a(), cVar.b());
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public to3(ro3 ro3) {
        wd4.b(ro3, "mView");
        this.j = ro3;
    }

    @DexIgnore
    public final void b(String[] strArr) {
        this.i = "";
        int length = strArr.length;
        int i2 = 0;
        while (i2 < length) {
            String str = strArr[i2];
            String str2 = this.i;
            if (str != null) {
                this.i = wd4.a(str2, (Object) StringsKt__StringsKt.d(str).toString());
                i2++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }

    @DexIgnore
    public void f() {
        SignUpEmailAuth signUpEmailAuth = this.h;
        if (signUpEmailAuth != null) {
            this.j.t(signUpEmailAuth.getEmail());
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.j.h0();
    }

    @DexIgnore
    public void i() {
        this.j.I(false);
        this.j.k();
        RequestEmailOtp requestEmailOtp = this.f;
        if (requestEmailOtp != null) {
            SignUpEmailAuth signUpEmailAuth = this.h;
            String email = signUpEmailAuth != null ? signUpEmailAuth.getEmail() : null;
            if (email != null) {
                requestEmailOtp.a(new RequestEmailOtp.b(email), new b(this));
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.d("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        ro3 ro3 = this.j;
        SignUpEmailAuth signUpEmailAuth = this.h;
        if (signUpEmailAuth != null) {
            ro3.a(signUpEmailAuth);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void k() {
        this.j.I(false);
        this.j.k();
        VerifyEmailOtp verifyEmailOtp = this.g;
        if (verifyEmailOtp != null) {
            SignUpEmailAuth signUpEmailAuth = this.h;
            if (signUpEmailAuth != null) {
                String email = signUpEmailAuth.getEmail();
                String str = this.i;
                if (str != null) {
                    verifyEmailOtp.a(new VerifyEmailOtp.b(email, str), new c(this));
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.d("mVerifyEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public final SignUpEmailAuth l() {
        return this.h;
    }

    @DexIgnore
    public void m() {
        this.j.a(this);
    }

    @DexIgnore
    public void a(String[] strArr) {
        wd4.b(strArr, "codes");
        b(strArr);
        String str = this.i;
        boolean z = false;
        if (!(str == null || cg4.a(str))) {
            String str2 = this.i;
            if (str2 != null && str2.length() == 4) {
                z = true;
            }
        }
        this.j.v(z);
    }

    @DexIgnore
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "emailAuth");
        this.h = signUpEmailAuth;
    }
}
