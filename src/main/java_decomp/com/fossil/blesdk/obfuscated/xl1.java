package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xl1 implements Parcelable.Creator<wl1> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        ll1 ll1 = null;
        String str3 = null;
        ig1 ig1 = null;
        ig1 ig12 = null;
        ig1 ig13 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 3:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    ll1 = SafeParcelReader.a(parcel2, a, ll1.CREATOR);
                    break;
                case 5:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 6:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                case 7:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 8:
                    ig1 = SafeParcelReader.a(parcel2, a, ig1.CREATOR);
                    break;
                case 9:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 10:
                    ig12 = SafeParcelReader.a(parcel2, a, ig1.CREATOR);
                    break;
                case 11:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 12:
                    ig13 = SafeParcelReader.a(parcel2, a, ig1.CREATOR);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new wl1(str, str2, ll1, j, z, str3, ig1, j2, ig12, j3, ig13);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new wl1[i];
    }
}
