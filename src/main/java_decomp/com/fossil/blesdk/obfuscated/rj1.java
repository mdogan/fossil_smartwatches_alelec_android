package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public rj1(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }
}
