package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m80 extends f80 {
    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ m80(short s, RequestId requestId, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, requestId, peripheral, (i2 & 8) != 0 ? 3 : i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m80(short s, RequestId requestId, Peripheral peripheral, int i) {
        super(FileControlOperationCode.VERIFY_FILE, s, requestId, peripheral, i);
        wd4.b(requestId, "requestId");
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public m80(short s, Peripheral peripheral) {
        this(s, RequestId.VERIFY_FILE, peripheral, 0, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
    }
}
