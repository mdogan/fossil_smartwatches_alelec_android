package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class li4 implements mi4 {
    @DexIgnore
    public /* final */ cj4 e;

    @DexIgnore
    public li4(cj4 cj4) {
        wd4.b(cj4, "list");
        this.e = cj4;
    }

    @DexIgnore
    public cj4 a() {
        return this.e;
    }

    @DexIgnore
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return oh4.c() ? a().a("New") : super.toString();
    }
}
