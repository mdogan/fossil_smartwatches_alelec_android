package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uj1 extends ui1 implements wi1 {
    @DexIgnore
    public uj1(yh1 yh1) {
        super(yh1);
        ck0.a(yh1);
    }

    @DexIgnore
    public void e() {
        this.a.a().e();
    }

    @DexIgnore
    public void f() {
        this.a.i();
    }

    @DexIgnore
    public void g() {
        this.a.a().g();
    }

    @DexIgnore
    public void m() {
        this.a.h();
        throw null;
    }

    @DexIgnore
    public bg1 n() {
        return this.a.j();
    }

    @DexIgnore
    public ej1 o() {
        return this.a.k();
    }

    @DexIgnore
    public og1 p() {
        return this.a.l();
    }

    @DexIgnore
    public wj1 q() {
        return this.a.m();
    }

    @DexIgnore
    public sj1 r() {
        return this.a.n();
    }

    @DexIgnore
    public qg1 s() {
        return this.a.o();
    }

    @DexIgnore
    public uk1 t() {
        return this.a.p();
    }
}
