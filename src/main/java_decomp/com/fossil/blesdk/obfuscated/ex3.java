package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.kv3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ex3 {
    @DexIgnore
    jp4 a(iv3 iv3, long j) throws IOException;

    @DexIgnore
    lv3 a(kv3 kv3) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(bx3 bx3) throws IOException;

    @DexIgnore
    void a(iv3 iv3) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    kv3.b c() throws IOException;

    @DexIgnore
    boolean d();
}
