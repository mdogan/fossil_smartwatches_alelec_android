package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q50 extends SingleRequestPhase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> B; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q50(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.PLAY_ANIMATION, new z70(peripheral));
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.B;
    }
}
