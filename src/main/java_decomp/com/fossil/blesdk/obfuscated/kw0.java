package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kw0<T> {
    @DexIgnore
    int a(T t);

    @DexIgnore
    T a();

    @DexIgnore
    void a(T t, px0 px0) throws IOException;

    @DexIgnore
    void a(T t, byte[] bArr, int i, int i2, qt0 qt0) throws IOException;

    @DexIgnore
    boolean a(T t, T t2);

    @DexIgnore
    int b(T t);

    @DexIgnore
    void b(T t, T t2);

    @DexIgnore
    boolean c(T t);

    @DexIgnore
    void zzc(T t);
}
