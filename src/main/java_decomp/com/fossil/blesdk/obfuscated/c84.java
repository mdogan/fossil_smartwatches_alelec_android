package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c84 {
    @DexIgnore
    public /* final */ n74 a;
    @DexIgnore
    public /* final */ z74 b;
    @DexIgnore
    public /* final */ y74 c;
    @DexIgnore
    public /* final */ w74 d;
    @DexIgnore
    public /* final */ k74 e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public c84(long j, n74 n74, z74 z74, y74 y74, w74 w74, k74 k74, p74 p74, int i, int i2) {
        this.f = j;
        this.a = n74;
        this.b = z74;
        this.c = y74;
        this.d = w74;
        this.e = k74;
    }

    @DexIgnore
    public boolean a(long j) {
        return this.f < j;
    }
}
