package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rl3 implements Factory<UpdateFirmwarePresenter> {
    @DexIgnore
    public static UpdateFirmwarePresenter a(nl3 nl3, DeviceRepository deviceRepository, UserRepository userRepository, wj2 wj2, fn2 fn2, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        return new UpdateFirmwarePresenter(nl3, deviceRepository, userRepository, wj2, fn2, updateFirmwareUsecase, downloadFirmwareByDeviceModelUsecase);
    }
}
