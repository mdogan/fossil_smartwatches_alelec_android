package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class in1 implements Parcelable.Creator<hn1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        vd0 vd0 = null;
        int i = 0;
        ek0 ek0 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                vd0 = (vd0) SafeParcelReader.a(parcel, a, vd0.CREATOR);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                ek0 = (ek0) SafeParcelReader.a(parcel, a, ek0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new hn1(i, vd0, ek0);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new hn1[i];
    }
}
