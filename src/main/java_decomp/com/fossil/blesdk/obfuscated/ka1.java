package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ka1 {
    @DexIgnore
    public static /* final */ ka1 c; // = new ka1();
    @DexIgnore
    public /* final */ pa1 a; // = new o91();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, oa1<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static ka1 a() {
        return c;
    }

    @DexIgnore
    public final <T> oa1<T> a(Class<T> cls) {
        w81.a(cls, "messageType");
        oa1<T> oa1 = (oa1) this.b.get(cls);
        if (oa1 != null) {
            return oa1;
        }
        oa1<T> a2 = this.a.a(cls);
        w81.a(cls, "messageType");
        w81.a(a2, "schema");
        oa1<T> putIfAbsent = this.b.putIfAbsent(cls, a2);
        return putIfAbsent != null ? putIfAbsent : a2;
    }

    @DexIgnore
    public final <T> oa1<T> a(T t) {
        return a(t.getClass());
    }
}
