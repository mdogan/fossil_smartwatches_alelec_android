package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j11 extends oz0 implements h11 {
    @DexIgnore
    public j11(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public final void c(Status status) throws RemoteException {
        Parcel o = o();
        z01.a(o, (Parcelable) status);
        b(1, o);
    }
}
