package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ws0 {
    @DexIgnore
    public static /* final */ ConcurrentHashMap<Uri, ws0> h; // = new ConcurrentHashMap<>();
    @DexIgnore
    public static /* final */ String[] i; // = {"key", "value"};
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentObserver c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Map<String, String> e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public /* final */ List<ys0> g; // = new ArrayList();

    @DexIgnore
    public ws0(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
        this.c = new xs0(this, (Handler) null);
    }

    @DexIgnore
    public static ws0 a(ContentResolver contentResolver, Uri uri) {
        ws0 ws0 = h.get(uri);
        if (ws0 != null) {
            return ws0;
        }
        ws0 ws02 = new ws0(contentResolver, uri);
        ws0 putIfAbsent = h.putIfAbsent(uri, ws02);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        ws02.a.registerContentObserver(ws02.b, false, ws02.c);
        return ws02;
    }

    @DexIgnore
    public final Map<String, String> a() {
        Map<String, String> c2 = zs0.a("gms:phenotype:phenotype_flag:debug_disable_caching", false) ? c() : this.e;
        if (c2 == null) {
            synchronized (this.d) {
                Map<String, String> map = this.e;
                if (map == null) {
                    map = c();
                    this.e = map;
                }
            }
        }
        return c2 != null ? c2 : Collections.emptyMap();
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d) {
            this.e = null;
        }
    }

    @DexIgnore
    public final Map<String, String> c() {
        Cursor query;
        try {
            HashMap hashMap = new HashMap();
            query = this.a.query(this.b, i, (String) null, (String[]) null, (String) null);
            if (query != null) {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(0), query.getString(1));
                }
                query.close();
            }
            return hashMap;
        } catch (SQLiteException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    @DexIgnore
    public final void d() {
        synchronized (this.f) {
            for (ys0 l : this.g) {
                l.l();
            }
        }
    }
}
