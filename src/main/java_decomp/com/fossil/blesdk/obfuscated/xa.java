package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManagerImpl;
import com.fossil.blesdk.obfuscated.wa;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xa implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xa> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int[] e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ CharSequence k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ CharSequence m;
    @DexIgnore
    public /* final */ ArrayList<String> n;
    @DexIgnore
    public /* final */ ArrayList<String> o;
    @DexIgnore
    public /* final */ boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<xa> {
        @DexIgnore
        public xa createFromParcel(Parcel parcel) {
            return new xa(parcel);
        }

        @DexIgnore
        public xa[] newArray(int i) {
            return new xa[i];
        }
    }

    @DexIgnore
    public xa(wa waVar) {
        int size = waVar.b.size();
        this.e = new int[(size * 6)];
        if (waVar.i) {
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                wa.a aVar = waVar.b.get(i3);
                int[] iArr = this.e;
                int i4 = i2 + 1;
                iArr[i2] = aVar.a;
                int i5 = i4 + 1;
                Fragment fragment = aVar.b;
                iArr[i4] = fragment != null ? fragment.mIndex : -1;
                int[] iArr2 = this.e;
                int i6 = i5 + 1;
                iArr2[i5] = aVar.c;
                int i7 = i6 + 1;
                iArr2[i6] = aVar.d;
                int i8 = i7 + 1;
                iArr2[i7] = aVar.e;
                i2 = i8 + 1;
                iArr2[i8] = aVar.f;
            }
            this.f = waVar.g;
            this.g = waVar.h;
            this.h = waVar.k;
            this.i = waVar.m;
            this.j = waVar.n;
            this.k = waVar.o;
            this.l = waVar.p;
            this.m = waVar.q;
            this.n = waVar.r;
            this.o = waVar.s;
            this.p = waVar.t;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    @DexIgnore
    public wa a(FragmentManagerImpl fragmentManagerImpl) {
        wa waVar = new wa(fragmentManagerImpl);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.e.length) {
            wa.a aVar = new wa.a();
            int i4 = i2 + 1;
            aVar.a = this.e[i2];
            if (FragmentManagerImpl.I) {
                Log.v("FragmentManager", "Instantiate " + waVar + " op #" + i3 + " base fragment #" + this.e[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.e[i4];
            if (i6 >= 0) {
                aVar.b = fragmentManagerImpl.i.get(i6);
            } else {
                aVar.b = null;
            }
            int[] iArr = this.e;
            int i7 = i5 + 1;
            aVar.c = iArr[i5];
            int i8 = i7 + 1;
            aVar.d = iArr[i7];
            int i9 = i8 + 1;
            aVar.e = iArr[i8];
            aVar.f = iArr[i9];
            waVar.c = aVar.c;
            waVar.d = aVar.d;
            waVar.e = aVar.e;
            waVar.f = aVar.f;
            waVar.a(aVar);
            i3++;
            i2 = i9 + 1;
        }
        waVar.g = this.f;
        waVar.h = this.g;
        waVar.k = this.h;
        waVar.m = this.i;
        waVar.i = true;
        waVar.n = this.j;
        waVar.o = this.k;
        waVar.p = this.l;
        waVar.q = this.m;
        waVar.r = this.n;
        waVar.s = this.o;
        waVar.t = this.p;
        waVar.a(1);
        return waVar;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.k, parcel, 0);
        parcel.writeInt(this.l);
        TextUtils.writeToParcel(this.m, parcel, 0);
        parcel.writeStringList(this.n);
        parcel.writeStringList(this.o);
        parcel.writeInt(this.p ? 1 : 0);
    }

    @DexIgnore
    public xa(Parcel parcel) {
        this.e = parcel.createIntArray();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
        this.i = parcel.readInt();
        this.j = parcel.readInt();
        this.k = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.l = parcel.readInt();
        this.m = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.n = parcel.createStringArrayList();
        this.o = parcel.createStringArrayList();
        this.p = parcel.readInt() != 0;
    }
}
