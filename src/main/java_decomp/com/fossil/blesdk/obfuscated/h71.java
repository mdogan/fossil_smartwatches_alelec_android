package com.fossil.blesdk.obfuscated;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h71 {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public h71(Uri uri) {
        this((String) null, uri, "", "", false, false, false);
    }

    @DexIgnore
    public final b71<Long> a(String str, long j) {
        return b71.a(this, str, j);
    }

    @DexIgnore
    public h71(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3) {
        this.a = uri;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final b71<Boolean> a(String str, boolean z) {
        return b71.a(this, str, z);
    }

    @DexIgnore
    public final b71<Integer> a(String str, int i) {
        return b71.a(this, str, i);
    }

    @DexIgnore
    public final b71<Double> a(String str, double d) {
        return b71.a(this, str, d);
    }

    @DexIgnore
    public final b71<String> a(String str, String str2) {
        return b71.a(this, str, str2);
    }
}
