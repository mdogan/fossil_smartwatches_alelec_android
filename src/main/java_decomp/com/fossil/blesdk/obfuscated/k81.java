package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.p81;
import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class k81<T extends p81<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract n81<T> a(Object obj);

    @DexIgnore
    public abstract Object a(j81 j81, x91 x91, int i);

    @DexIgnore
    public abstract <UT, UB> UB a(na1 na1, Object obj, j81 j81, n81<T> n81, UB ub, fb1<UT, UB> fb1) throws IOException;

    @DexIgnore
    public abstract void a(na1 na1, Object obj, j81 j81, n81<T> n81) throws IOException;

    @DexIgnore
    public abstract void a(tb1 tb1, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract void a(zzte zzte, Object obj, j81 j81, n81<T> n81) throws IOException;

    @DexIgnore
    public abstract boolean a(x91 x91);

    @DexIgnore
    public abstract n81<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
