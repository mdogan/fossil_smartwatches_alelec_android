package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n23 implements Factory<ComplicationsPresenter> {
    @DexIgnore
    public static ComplicationsPresenter a(j23 j23, CategoryRepository categoryRepository) {
        return new ComplicationsPresenter(j23, categoryRepository);
    }
}
