package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;
import com.google.android.gms.internal.clearcut.zzbb;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iu0 extends hu0<su0.d> {
    @DexIgnore
    public final int a(Map.Entry<?, ?> entry) {
        return ((su0.d) entry.getKey()).e;
    }

    @DexIgnore
    public final lu0<su0.d> a(Object obj) {
        return ((su0.c) obj).zzjv;
    }

    @DexIgnore
    public final void a(px0 px0, Map.Entry<?, ?> entry) throws IOException {
        su0.d dVar = (su0.d) entry.getKey();
        switch (ju0.a[dVar.f.ordinal()]) {
            case 1:
                px0.zza(dVar.e, ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                px0.zza(dVar.e, ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                px0.b(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 4:
                px0.zza(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 5:
                px0.zzc(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                px0.zzc(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 7:
                px0.zzf(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                px0.a(dVar.e, ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                px0.zzd(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                px0.b(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                px0.a(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 12:
                px0.zze(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                px0.zzb(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 14:
                px0.zzc(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                px0.a(dVar.e, (zzbb) entry.getValue());
                return;
            case 16:
                px0.zza(dVar.e, (String) entry.getValue());
                return;
            case 17:
                px0.b(dVar.e, (Object) entry.getValue(), (kw0) fw0.a().a(entry.getValue().getClass()));
                return;
            case 18:
                px0.a(dVar.e, (Object) entry.getValue(), (kw0) fw0.a().a(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void a(Object obj, lu0<su0.d> lu0) {
        ((su0.c) obj).zzjv = lu0;
    }

    @DexIgnore
    public final boolean a(tv0 tv0) {
        return tv0 instanceof su0.c;
    }

    @DexIgnore
    public final lu0<su0.d> b(Object obj) {
        lu0<su0.d> a = a(obj);
        if (!a.c()) {
            return a;
        }
        lu0<su0.d> lu0 = (lu0) a.clone();
        a(obj, lu0);
        return lu0;
    }

    @DexIgnore
    public final void c(Object obj) {
        a(obj).h();
    }
}
