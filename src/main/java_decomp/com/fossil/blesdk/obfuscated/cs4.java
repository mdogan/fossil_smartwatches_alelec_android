package com.fossil.blesdk.obfuscated;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cs4<T> {
    @DexIgnore
    public /* final */ Response a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ qm4 c;

    @DexIgnore
    public cs4(Response response, T t, qm4 qm4) {
        this.a = response;
        this.b = t;
        this.c = qm4;
    }

    @DexIgnore
    public static <T> cs4<T> a(T t, Response response) {
        gs4.a(response, "rawResponse == null");
        if (response.E()) {
            return new cs4<>(response, t, (qm4) null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    @DexIgnore
    public int b() {
        return this.a.B();
    }

    @DexIgnore
    public qm4 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.E();
    }

    @DexIgnore
    public String e() {
        return this.a.F();
    }

    @DexIgnore
    public Response f() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static <T> cs4<T> a(qm4 qm4, Response response) {
        gs4.a(qm4, "body == null");
        gs4.a(response, "rawResponse == null");
        if (!response.E()) {
            return new cs4<>(response, (Object) null, qm4);
        }
        throw new IllegalArgumentException("rawResponse should not be successful response");
    }

    @DexIgnore
    public T a() {
        return this.b;
    }
}
