package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jd1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<jd1> CREATOR; // = new kd1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public jd1(int i, int i2, long j, long j2) {
        this.e = i;
        this.f = i2;
        this.g = j;
        this.h = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && jd1.class == obj.getClass()) {
            jd1 jd1 = (jd1) obj;
            return this.e == jd1.e && this.f == jd1.f && this.g == jd1.g && this.h == jd1.h;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(Integer.valueOf(this.f), Integer.valueOf(this.e), Long.valueOf(this.h), Long.valueOf(this.g));
    }

    @DexIgnore
    public final String toString() {
        return "NetworkLocationStatus:" + " Wifi status: " + this.e + " Cell status: " + this.f + " elapsed time NS: " + this.h + " system time ms: " + this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 4, this.h);
        lk0.a(parcel, a);
    }
}
