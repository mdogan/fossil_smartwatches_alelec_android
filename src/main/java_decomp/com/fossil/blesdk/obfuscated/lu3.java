package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lu3 extends mj {
    @DexIgnore
    public /* final */ Map<Class<? extends ListenableWorker>, Provider<mu3<? extends ListenableWorker>>> b;

    @DexIgnore
    public lu3(Map<Class<? extends ListenableWorker>, Provider<mu3<? extends ListenableWorker>>> map) {
        wd4.b(map, "workerFactoryMap");
        this.b = map;
    }

    @DexIgnore
    public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
        T t;
        wd4.b(context, "appContext");
        wd4.b(str, "workerClassName");
        wd4.b(workerParameters, "workerParameters");
        Iterator<T> it = this.b.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (Class.forName(str).isAssignableFrom((Class) ((Map.Entry) t).getKey())) {
                break;
            }
        }
        Map.Entry entry = (Map.Entry) t;
        if (entry != null) {
            Provider provider = (Provider) entry.getValue();
            if (provider != null) {
                return ((mu3) provider.get()).a(context, workerParameters);
            }
        }
        throw new IllegalArgumentException("could not find worker: " + str);
    }
}
