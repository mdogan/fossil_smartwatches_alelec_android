package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ch4 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ jd4<Throwable, cb4> b;

    @DexIgnore
    public String toString() {
        return "CompletedWithCancellation[" + this.a + ']';
    }
}
