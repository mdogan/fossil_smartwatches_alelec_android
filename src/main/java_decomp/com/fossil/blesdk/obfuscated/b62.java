package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b62 extends me.d<GoalTrackingData> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        wd4.b(goalTrackingData, "oldItem");
        wd4.b(goalTrackingData2, "newItem");
        return wd4.a((Object) goalTrackingData, (Object) goalTrackingData2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        wd4.b(goalTrackingData, "oldItem");
        wd4.b(goalTrackingData2, "newItem");
        return wd4.a((Object) goalTrackingData.getId(), (Object) goalTrackingData2.getId());
    }
}
