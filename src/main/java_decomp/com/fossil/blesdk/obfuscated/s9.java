package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s9 {
    @DexIgnore
    public /* final */ Object a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ s9 a;

        @DexIgnore
        public a(s9 s9Var) {
            this.a = s9Var;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            r9 a2 = this.a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.w();
        }

        @DexIgnore
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<r9> a2 = this.a.a(str, i);
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(a2.get(i2).w());
            }
            return arrayList;
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.a.a(i, i2, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a {
        @DexIgnore
        public b(s9 s9Var) {
            super(s9Var);
        }

        @DexIgnore
        public AccessibilityNodeInfo findFocus(int i) {
            r9 b = this.a.b(i);
            if (b == null) {
                return null;
            }
            return b.w();
        }
    }

    @DexIgnore
    public s9() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.a = new b(this);
        } else if (i >= 16) {
            this.a = new a(this);
        } else {
            this.a = null;
        }
    }

    @DexIgnore
    public r9 a(int i) {
        return null;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public List<r9> a(String str, int i) {
        return null;
    }

    @DexIgnore
    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    @DexIgnore
    public r9 b(int i) {
        return null;
    }

    @DexIgnore
    public s9(Object obj) {
        this.a = obj;
    }
}
