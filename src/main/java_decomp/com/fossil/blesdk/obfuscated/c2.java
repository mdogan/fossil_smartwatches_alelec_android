package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import androidx.collection.SparseArrayCompat;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c2 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode g; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static c2 h;
    @DexIgnore
    public static /* final */ c i; // = new c(6);
    @DexIgnore
    public static /* final */ int[] j; // = {v.abc_textfield_search_default_mtrl_alpha, v.abc_textfield_default_mtrl_alpha, v.abc_ab_share_pack_mtrl_alpha};
    @DexIgnore
    public static /* final */ int[] k; // = {v.abc_ic_commit_search_api_mtrl_alpha, v.abc_seekbar_tick_mark_material, v.abc_ic_menu_share_mtrl_alpha, v.abc_ic_menu_copy_mtrl_am_alpha, v.abc_ic_menu_cut_mtrl_alpha, v.abc_ic_menu_selectall_mtrl_alpha, v.abc_ic_menu_paste_mtrl_am_alpha};
    @DexIgnore
    public static /* final */ int[] l; // = {v.abc_textfield_activated_mtrl_alpha, v.abc_textfield_search_activated_mtrl_alpha, v.abc_cab_background_top_mtrl_alpha, v.abc_text_cursor_material, v.abc_text_select_handle_left_mtrl_dark, v.abc_text_select_handle_middle_mtrl_dark, v.abc_text_select_handle_right_mtrl_dark, v.abc_text_select_handle_left_mtrl_light, v.abc_text_select_handle_middle_mtrl_light, v.abc_text_select_handle_right_mtrl_light};
    @DexIgnore
    public static /* final */ int[] m; // = {v.abc_popup_background_mtrl_mult, v.abc_cab_background_internal_bg, v.abc_menu_hardkey_panel_mtrl_mult};
    @DexIgnore
    public static /* final */ int[] n; // = {v.abc_tab_indicator_material, v.abc_textfield_search_material};
    @DexIgnore
    public static /* final */ int[] o; // = {v.abc_btn_check_material, v.abc_btn_radio_material};
    @DexIgnore
    public WeakHashMap<Context, SparseArrayCompat<ColorStateList>> a;
    @DexIgnore
    public g4<String, d> b;
    @DexIgnore
    public SparseArrayCompat<String> c;
    @DexIgnore
    public /* final */ WeakHashMap<Context, j4<WeakReference<Drawable.ConstantState>>> d; // = new WeakHashMap<>(0);
    @DexIgnore
    public TypedValue e;
    @DexIgnore
    public boolean f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return n0.e(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return ni.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends k4<Integer, PorterDuffColorFilter> {
        @DexIgnore
        public c(int i) {
            super(i);
        }

        @DexIgnore
        public static int b(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) b(Integer.valueOf(b(i, mode)));
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return ti.createFromXmlInner(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    @DexIgnore
    public static synchronized c2 a() {
        c2 c2Var;
        synchronized (c2.class) {
            if (h == null) {
                h = new c2();
                a(h);
            }
            c2Var = h;
        }
        return c2Var;
    }

    @DexIgnore
    public final Drawable b(Context context, int i2) {
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 != null) {
            return a3;
        }
        if (i2 == v.abc_cab_background_top_material) {
            a3 = new LayerDrawable(new Drawable[]{c(context, v.abc_cab_background_internal_bg), c(context, v.abc_cab_background_top_mtrl_alpha)});
        }
        if (a3 != null) {
            a3.setChangingConfigurations(typedValue.changingConfigurations);
            a(context, a2, a3);
        }
        return a3;
    }

    @DexIgnore
    public synchronized Drawable c(Context context, int i2) {
        return a(context, i2, false);
    }

    @DexIgnore
    public synchronized ColorStateList d(Context context, int i2) {
        ColorStateList e2;
        e2 = e(context, i2);
        if (e2 == null) {
            if (i2 == v.abc_edit_text_material) {
                e2 = m0.b(context, t.abc_tint_edittext);
            } else if (i2 == v.abc_switch_track_mtrl_alpha) {
                e2 = m0.b(context, t.abc_tint_switch_track);
            } else if (i2 == v.abc_switch_thumb_material) {
                e2 = e(context);
            } else if (i2 == v.abc_btn_default_mtrl_shape) {
                e2 = d(context);
            } else if (i2 == v.abc_btn_borderless_material) {
                e2 = b(context);
            } else if (i2 == v.abc_btn_colored_material) {
                e2 = c(context);
            } else {
                if (i2 != v.abc_spinner_mtrl_am_alpha) {
                    if (i2 != v.abc_spinner_textfield_background_material) {
                        if (a(k, i2)) {
                            e2 = u2.c(context, r.colorControlNormal);
                        } else if (a(n, i2)) {
                            e2 = m0.b(context, t.abc_tint_default);
                        } else if (a(o, i2)) {
                            e2 = m0.b(context, t.abc_tint_btn_checkable);
                        } else if (i2 == v.abc_seekbar_thumb_material) {
                            e2 = m0.b(context, t.abc_tint_seek_thumb);
                        }
                    }
                }
                e2 = m0.b(context, t.abc_tint_spinner);
            }
            if (e2 != null) {
                a(context, i2, e2);
            }
        }
        return e2;
    }

    @DexIgnore
    public final ColorStateList e(Context context, int i2) {
        WeakHashMap<Context, SparseArrayCompat<ColorStateList>> weakHashMap = this.a;
        if (weakHashMap == null) {
            return null;
        }
        SparseArrayCompat sparseArrayCompat = weakHashMap.get(context);
        if (sparseArrayCompat != null) {
            return (ColorStateList) sparseArrayCompat.b(i2);
        }
        return null;
    }

    @DexIgnore
    public synchronized void f(Context context) {
        j4 j4Var = this.d.get(context);
        if (j4Var != null) {
            j4Var.a();
        }
    }

    @DexIgnore
    public final ColorStateList c(Context context) {
        return a(context, u2.b(context, r.colorAccent));
    }

    @DexIgnore
    public final ColorStateList e(Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList c2 = u2.c(context, r.colorSwitchThumbNormal);
        if (c2 == null || !c2.isStateful()) {
            iArr[0] = u2.b;
            iArr2[0] = u2.a(context, r.colorSwitchThumbNormal);
            iArr[1] = u2.e;
            iArr2[1] = u2.b(context, r.colorControlActivated);
            iArr[2] = u2.f;
            iArr2[2] = u2.b(context, r.colorSwitchThumbNormal);
        } else {
            iArr[0] = u2.b;
            iArr2[0] = c2.getColorForState(iArr[0], 0);
            iArr[1] = u2.e;
            iArr2[1] = u2.b(context, r.colorControlActivated);
            iArr[2] = u2.f;
            iArr2[2] = c2.getDefaultColor();
        }
        return new ColorStateList(iArr, iArr2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073 A[Catch:{ Exception -> 0x00a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[Catch:{ Exception -> 0x00a2 }] */
    public final Drawable f(Context context, int i2) {
        int next;
        g4<String, d> g4Var = this.b;
        if (g4Var == null || g4Var.isEmpty()) {
            return null;
        }
        SparseArrayCompat<String> sparseArrayCompat = this.c;
        if (sparseArrayCompat != null) {
            String b2 = sparseArrayCompat.b(i2);
            if ("appcompat_skip_skip".equals(b2) || (b2 != null && this.b.get(b2) == null)) {
                return null;
            }
        } else {
            this.c = new SparseArrayCompat<>();
        }
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        Resources resources = context.getResources();
        resources.getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 != null) {
            return a3;
        }
        CharSequence charSequence = typedValue.string;
        if (charSequence != null && charSequence.toString().endsWith(".xml")) {
            try {
                XmlResourceParser xml = resources.getXml(i2);
                AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
                while (true) {
                    next = xml.next();
                    if (next == 2 || next == 1) {
                        if (next != 2) {
                            String name = xml.getName();
                            this.c.a(i2, name);
                            d dVar = this.b.get(name);
                            if (dVar != null) {
                                a3 = dVar.a(context, xml, asAttributeSet, context.getTheme());
                            }
                            if (a3 != null) {
                                a3.setChangingConfigurations(typedValue.changingConfigurations);
                                a(context, a2, a3);
                            }
                        } else {
                            throw new XmlPullParserException("No start tag found");
                        }
                    }
                }
                if (next != 2) {
                }
            } catch (Exception e2) {
                Log.e("AppCompatDrawableManag", "Exception while inflating drawable", e2);
            }
        }
        if (a3 == null) {
            this.c.a(i2, "appcompat_skip_skip");
        }
        return a3;
    }

    @DexIgnore
    public static void a(c2 c2Var) {
        if (Build.VERSION.SDK_INT < 24) {
            c2Var.a("vector", (d) new e());
            c2Var.a("animated-vector", (d) new b());
            c2Var.a("animated-selector", (d) new a());
        }
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i2, boolean z) {
        Drawable f2;
        a(context);
        f2 = f(context, i2);
        if (f2 == null) {
            f2 = b(context, i2);
        }
        if (f2 == null) {
            f2 = k6.c(context, i2);
        }
        if (f2 != null) {
            f2 = a(context, i2, z, f2);
        }
        if (f2 != null) {
            k2.b(f2);
        }
        return f2;
    }

    @DexIgnore
    public final ColorStateList b(Context context) {
        return a(context, 0);
    }

    @DexIgnore
    public static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    @DexIgnore
    public final Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList d2 = d(context, i2);
        if (d2 != null) {
            if (k2.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable i3 = c7.i(drawable);
            c7.a(i3, d2);
            PorterDuff.Mode a2 = a(i2);
            if (a2 == null) {
                return i3;
            }
            c7.a(i3, a2);
            return i3;
        } else if (i2 == v.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            a(layerDrawable.findDrawableByLayerId(16908288), u2.b(context, r.colorControlNormal), g);
            a(layerDrawable.findDrawableByLayerId(16908303), u2.b(context, r.colorControlNormal), g);
            a(layerDrawable.findDrawableByLayerId(16908301), u2.b(context, r.colorControlActivated), g);
            return drawable;
        } else if (i2 == v.abc_ratingbar_material || i2 == v.abc_ratingbar_indicator_material || i2 == v.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            a(layerDrawable2.findDrawableByLayerId(16908288), u2.a(context, r.colorControlNormal), g);
            a(layerDrawable2.findDrawableByLayerId(16908303), u2.b(context, r.colorControlActivated), g);
            a(layerDrawable2.findDrawableByLayerId(16908301), u2.b(context, r.colorControlActivated), g);
            return drawable;
        } else if (a(context, i2, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    @DexIgnore
    public final ColorStateList d(Context context) {
        return a(context, u2.b(context, r.colorButtonNormal));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        return null;
     */
    @DexIgnore
    public final synchronized Drawable a(Context context, long j2) {
        j4 j4Var = this.d.get(context);
        if (j4Var == null) {
            return null;
        }
        WeakReference weakReference = (WeakReference) j4Var.b(j2);
        if (weakReference != null) {
            Drawable.ConstantState constantState = (Drawable.ConstantState) weakReference.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            j4Var.a(j2);
        }
    }

    @DexIgnore
    public final synchronized boolean a(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        j4 j4Var = this.d.get(context);
        if (j4Var == null) {
            j4Var = new j4();
            this.d.put(context, j4Var);
        }
        j4Var.c(j2, new WeakReference(constantState));
        return true;
    }

    @DexIgnore
    public synchronized Drawable a(Context context, e3 e3Var, int i2) {
        Drawable f2 = f(context, i2);
        if (f2 == null) {
            f2 = e3Var.a(i2);
        }
        if (f2 == null) {
            return null;
        }
        return a(context, i2, false, f2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061 A[RETURN] */
    public static boolean a(Context context, int i2, Drawable drawable) {
        boolean z;
        int i3;
        PorterDuff.Mode mode = g;
        int i4 = 16842801;
        if (a(j, i2)) {
            i4 = r.colorControlNormal;
        } else if (a(l, i2)) {
            i4 = r.colorControlActivated;
        } else if (a(m, i2)) {
            mode = PorterDuff.Mode.MULTIPLY;
        } else {
            if (i2 == v.abc_list_divider_mtrl_alpha) {
                i4 = 16842800;
                i3 = Math.round(40.8f);
                z = true;
            } else if (i2 != v.abc_dialog_material_background) {
                z = false;
                i3 = -1;
                i4 = 0;
            }
            if (z) {
                return false;
            }
            if (k2.a(drawable)) {
                drawable = drawable.mutate();
            }
            drawable.setColorFilter(a(u2.b(context, i4), mode));
            if (i3 != -1) {
                drawable.setAlpha(i3);
            }
            return true;
        }
        z = true;
        i3 = -1;
        if (z) {
        }
    }

    @DexIgnore
    public final void a(String str, d dVar) {
        if (this.b == null) {
            this.b = new g4<>();
        }
        this.b.put(str, dVar);
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static PorterDuff.Mode a(int i2) {
        if (i2 == v.abc_switch_thumb_material) {
            return PorterDuff.Mode.MULTIPLY;
        }
        return null;
    }

    @DexIgnore
    public final void a(Context context, int i2, ColorStateList colorStateList) {
        if (this.a == null) {
            this.a = new WeakHashMap<>();
        }
        SparseArrayCompat sparseArrayCompat = this.a.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new SparseArrayCompat();
            this.a.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.a(i2, colorStateList);
    }

    @DexIgnore
    public final ColorStateList a(Context context, int i2) {
        int b2 = u2.b(context, r.colorControlHighlight);
        int a2 = u2.a(context, r.colorButtonNormal);
        return new ColorStateList(new int[][]{u2.b, u2.d, u2.c, u2.f}, new int[]{a2, t6.b(b2, i2), t6.b(b2, i2), i2});
    }

    @DexIgnore
    public static void a(Drawable drawable, x2 x2Var, int[] iArr) {
        if (!k2.a(drawable) || drawable.mutate() == drawable) {
            if (x2Var.d || x2Var.c) {
                drawable.setColorFilter(a(x2Var.d ? x2Var.a : null, x2Var.c ? x2Var.b : g, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManag", "Mutated drawable is not the same instance as the input.");
    }

    @DexIgnore
    public static PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    @DexIgnore
    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (c2.class) {
            a2 = i.a(i2, mode);
            if (a2 == null) {
                a2 = new PorterDuffColorFilter(i2, mode);
                i.a(i2, mode, a2);
            }
        }
        return a2;
    }

    @DexIgnore
    public static void a(Drawable drawable, int i2, PorterDuff.Mode mode) {
        if (k2.a(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = g;
        }
        drawable.setColorFilter(a(i2, mode));
    }

    @DexIgnore
    public final void a(Context context) {
        if (!this.f) {
            this.f = true;
            Drawable c2 = c(context, v.abc_vector_test);
            if (c2 == null || !a(c2)) {
                this.f = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    @DexIgnore
    public static boolean a(Drawable drawable) {
        return (drawable instanceof ti) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }
}
