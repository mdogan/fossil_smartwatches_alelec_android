package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hs0 extends pj0<is0> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public hs0(Context context, Looper looper, lj0 lj0, tb0 tb0, he0.b bVar, he0.c cVar) {
        super(context, looper, 16, lj0, bVar, cVar);
        if (tb0 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof is0) {
            return (is0) queryLocalInterface;
        }
        return new js0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final boolean l() {
        lj0 F = F();
        return !TextUtils.isEmpty(F.b()) && !F.a((ee0<?>) sb0.c).isEmpty();
    }

    @DexIgnore
    public final Bundle u() {
        return this.E;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.service.START";
    }
}
