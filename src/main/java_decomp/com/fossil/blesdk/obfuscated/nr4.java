package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nr4 extends kr4<Fragment> {
    @DexIgnore
    public nr4(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        ((Fragment) b()).requestPermissions(strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return ((Fragment) b()).shouldShowRequestPermissionRationale(str);
    }

    @DexIgnore
    public FragmentManager c() {
        return ((Fragment) b()).getChildFragmentManager();
    }

    @DexIgnore
    public Context a() {
        return ((Fragment) b()).getActivity();
    }
}
