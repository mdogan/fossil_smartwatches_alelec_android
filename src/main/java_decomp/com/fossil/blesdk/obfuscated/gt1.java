package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gt1 {
    @DexIgnore
    public float a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public /* final */ List<c> e; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends c {
        @DexIgnore
        public static /* final */ RectF h; // = new RectF();
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;
        @DexIgnore
        public float e;
        @DexIgnore
        public float f;
        @DexIgnore
        public float g;

        @DexIgnore
        public a(float f2, float f3, float f4, float f5) {
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = f5;
        }

        @DexIgnore
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(this.b, this.c, this.d, this.e);
            path.arcTo(h, this.f, this.g, false);
            path.transform(matrix);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends c {
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.b, this.c);
            path.transform(matrix);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public /* final */ Matrix a; // = new Matrix();

        @DexIgnore
        public abstract void a(Matrix matrix, Path path);
    }

    @DexIgnore
    public gt1() {
        b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(float f, float f2) {
        b bVar = new b();
        float unused = bVar.b = f;
        float unused2 = bVar.c = f2;
        this.e.add(bVar);
        this.c = f;
        this.d = f2;
    }

    @DexIgnore
    public void b(float f, float f2) {
        this.a = f;
        this.b = f2;
        this.c = f;
        this.d = f2;
        this.e.clear();
    }

    @DexIgnore
    public void a(float f, float f2, float f3, float f4, float f5, float f6) {
        a aVar = new a(f, f2, f3, f4);
        aVar.f = f5;
        aVar.g = f6;
        this.e.add(aVar);
        double d2 = (double) (f5 + f6);
        this.c = ((f + f3) * 0.5f) + (((f3 - f) / 2.0f) * ((float) Math.cos(Math.toRadians(d2))));
        this.d = ((f2 + f4) * 0.5f) + (((f4 - f2) / 2.0f) * ((float) Math.sin(Math.toRadians(d2))));
    }

    @DexIgnore
    public void a(Matrix matrix, Path path) {
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(matrix, path);
        }
    }
}
