package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dc0 {
    @DexIgnore
    public static /* final */ Lock c; // = new ReentrantLock();
    @DexIgnore
    public static dc0 d;
    @DexIgnore
    public /* final */ Lock a; // = new ReentrantLock();
    @DexIgnore
    public /* final */ SharedPreferences b;

    @DexIgnore
    public dc0(Context context) {
        this.b = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    @DexIgnore
    public static dc0 a(Context context) {
        ck0.a(context);
        c.lock();
        try {
            if (d == null) {
                d = new dc0(context.getApplicationContext());
            }
            return d;
        } finally {
            c.unlock();
        }
    }

    @DexIgnore
    public GoogleSignInAccount b() {
        return a(c("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public GoogleSignInOptions c() {
        return b(c("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public String d() {
        return c("refreshToken");
    }

    @DexIgnore
    public final void e() {
        String c2 = c("defaultGoogleSignInAccount");
        d("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(c2)) {
            d(b("googleSignInAccount", c2));
            d(b("googleSignInOptions", c2));
        }
    }

    @DexIgnore
    public final void d(String str) {
        this.a.lock();
        try {
            this.b.edit().remove(str).apply();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final GoogleSignInOptions b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String c2 = c(b("googleSignInOptions", str));
        if (c2 != null) {
            try {
                return GoogleSignInOptions.e(c2);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String c(String str) {
        this.a.lock();
        try {
            return this.b.getString(str, (String) null);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public void a(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        ck0.a(googleSignInAccount);
        ck0.a(googleSignInOptions);
        a("defaultGoogleSignInAccount", googleSignInAccount.S());
        ck0.a(googleSignInAccount);
        ck0.a(googleSignInOptions);
        String S = googleSignInAccount.S();
        a(b("googleSignInAccount", S), googleSignInAccount.T());
        a(b("googleSignInOptions", S), googleSignInOptions.P());
    }

    @DexIgnore
    public final void a(String str, String str2) {
        this.a.lock();
        try {
            this.b.edit().putString(str, str2).apply();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final GoogleSignInAccount a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String c2 = c(b("googleSignInAccount", str));
        if (c2 != null) {
            try {
                return GoogleSignInAccount.e(c2);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.a.lock();
        try {
            this.b.edit().clear().apply();
        } finally {
            this.a.unlock();
        }
    }
}
