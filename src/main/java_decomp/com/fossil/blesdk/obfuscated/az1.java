package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.Message;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface az1 extends IInterface {
    @DexIgnore
    void a(Message message) throws RemoteException;
}
