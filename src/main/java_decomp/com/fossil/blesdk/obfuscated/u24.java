package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;

    @DexIgnore
    public u24(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void run() {
        u04.a(k04.r).h();
        f24.a(this.e, true);
        h14.b(this.e);
        r24.b(this.e);
        Thread.UncaughtExceptionHandler unused = k04.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new b14());
        if (i04.o() == StatReportStrategy.APP_LAUNCH) {
            k04.a(this.e, -1);
        }
        if (i04.q()) {
            k04.m.a((Object) "Init MTA StatService success.");
        }
    }
}
