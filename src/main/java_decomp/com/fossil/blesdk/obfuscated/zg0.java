package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zg0<O extends ee0.d> extends pf0 {
    @DexIgnore
    public /* final */ ge0<O> c;

    @DexIgnore
    public zg0(ge0<O> ge0) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = ge0;
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T a(T t) {
        this.c.b(t);
        return t;
    }

    @DexIgnore
    public final void a(nh0 nh0) {
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T b(T t) {
        this.c.c(t);
        return t;
    }

    @DexIgnore
    public final Context e() {
        return this.c.e();
    }

    @DexIgnore
    public final Looper f() {
        return this.c.g();
    }
}
