package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r14 {
    @DexIgnore
    public long a;
    @DexIgnore
    public String b;

    @DexIgnore
    public r14(long j, String str, int i, int i2) {
        this.a = j;
        this.b = str;
    }

    @DexIgnore
    public String toString() {
        return this.b;
    }
}
