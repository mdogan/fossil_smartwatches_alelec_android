package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ai1 extends mg1 {
    @DexIgnore
    public /* final */ el1 e;
    @DexIgnore
    public Boolean f;
    @DexIgnore
    public String g;

    @DexIgnore
    public ai1(el1 el1) {
        this(el1, (String) null);
    }

    @DexIgnore
    public final void a(ig1 ig1, sl1 sl1) {
        ck0.a(ig1);
        b(sl1, false);
        a((Runnable) new mi1(this, ig1, sl1));
    }

    @DexIgnore
    public final ig1 b(ig1 ig1, sl1 sl1) {
        boolean z = false;
        if ("_cmp".equals(ig1.e)) {
            fg1 fg1 = ig1.f;
            if (!(fg1 == null || fg1.size() == 0)) {
                String g2 = ig1.f.g("_cis");
                if (!TextUtils.isEmpty(g2) && (("referrer broadcast".equals(g2) || "referrer API".equals(g2)) && this.e.i().m(sl1.e))) {
                    z = true;
                }
            }
        }
        if (!z) {
            return ig1;
        }
        this.e.d().y().a("Event has been filtered ", ig1.toString());
        return new ig1("_cmpx", ig1.f, ig1.g, ig1.h);
    }

    @DexIgnore
    public final void c(sl1 sl1) {
        b(sl1, false);
        a((Runnable) new bi1(this, sl1));
    }

    @DexIgnore
    public final String d(sl1 sl1) {
        b(sl1, false);
        return this.e.e(sl1);
    }

    @DexIgnore
    public ai1(el1 el1, String str) {
        ck0.a(el1);
        this.e = el1;
        this.g = null;
    }

    @DexIgnore
    public final void a(ig1 ig1, String str, String str2) {
        ck0.a(ig1);
        ck0.b(str);
        a(str, true);
        a((Runnable) new ni1(this, ig1, str));
    }

    @DexIgnore
    public final byte[] a(ig1 ig1, String str) {
        ck0.b(str);
        ck0.a(ig1);
        a(str, true);
        this.e.d().z().a("Log and bundle. event", this.e.g().a(ig1.e));
        long a = this.e.c().a() / 1000000;
        try {
            byte[] bArr = (byte[]) this.e.a().b(new oi1(this, ig1, str)).get();
            if (bArr == null) {
                this.e.d().s().a("Log and bundle returned null. appId", ug1.a(str));
                bArr = new byte[0];
            }
            this.e.d().z().a("Log and bundle processed. event, size, time_ms", this.e.g().a(ig1.e), Integer.valueOf(bArr.length), Long.valueOf((this.e.c().a() / 1000000) - a));
            return bArr;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to log and bundle. appId, event, error", ug1.a(str), this.e.g().a(ig1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final void b(sl1 sl1, boolean z) {
        ck0.a(sl1);
        a(sl1.e, false);
        this.e.h().d(sl1.f, sl1.v);
    }

    @DexIgnore
    public final void b(sl1 sl1) {
        a(sl1.e, false);
        a((Runnable) new li1(this, sl1));
    }

    @DexIgnore
    public final void a(ll1 ll1, sl1 sl1) {
        ck0.a(ll1);
        b(sl1, false);
        if (ll1.H() == null) {
            a((Runnable) new pi1(this, ll1, sl1));
        } else {
            a((Runnable) new qi1(this, ll1, sl1));
        }
    }

    @DexIgnore
    public final List<ll1> a(sl1 sl1, boolean z) {
        b(sl1, false);
        try {
            List<nl1> list = (List) this.e.a().a(new ri1(this, sl1)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (nl1 nl1 : list) {
                if (z || !ol1.h(nl1.c)) {
                    arrayList.add(new ll1(nl1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", ug1.a(sl1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final void a(sl1 sl1) {
        b(sl1, false);
        a((Runnable) new si1(this, sl1));
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.f == null) {
                        if (!"com.google.android.gms".equals(this.g) && !tm0.a(this.e.getContext(), Binder.getCallingUid())) {
                            if (!be0.a(this.e.getContext()).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.f = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.f = Boolean.valueOf(z2);
                    }
                    if (this.f.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e2) {
                    this.e.d().s().a("Measurement Service called with invalid calling package. appId", ug1.a(str));
                    throw e2;
                }
            }
            if (this.g == null && ae0.uidHasPackageName(this.e.getContext(), Binder.getCallingUid(), str)) {
                this.g = str;
            }
            if (!str.equals(this.g)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
            return;
        }
        this.e.d().s().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) {
        a((Runnable) new ti1(this, str2, str3, str, j));
    }

    @DexIgnore
    public final void a(wl1 wl1, sl1 sl1) {
        ck0.a(wl1);
        ck0.a(wl1.g);
        b(sl1, false);
        wl1 wl12 = new wl1(wl1);
        wl12.e = sl1.e;
        if (wl1.g.H() == null) {
            a((Runnable) new di1(this, wl12, sl1));
        } else {
            a((Runnable) new ei1(this, wl12, sl1));
        }
    }

    @DexIgnore
    public final void a(wl1 wl1) {
        ck0.a(wl1);
        ck0.a(wl1.g);
        a(wl1.e, true);
        wl1 wl12 = new wl1(wl1);
        if (wl1.g.H() == null) {
            a((Runnable) new fi1(this, wl12));
        } else {
            a((Runnable) new gi1(this, wl12));
        }
    }

    @DexIgnore
    public final List<ll1> a(String str, String str2, boolean z, sl1 sl1) {
        b(sl1, false);
        try {
            List<nl1> list = (List) this.e.a().a(new hi1(this, sl1, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (nl1 nl1 : list) {
                if (z || !ol1.h(nl1.c)) {
                    arrayList.add(new ll1(nl1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", ug1.a(sl1.e), e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<ll1> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<nl1> list = (List) this.e.a().a(new ii1(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (nl1 nl1 : list) {
                if (z || !ol1.h(nl1.c)) {
                    arrayList.add(new ll1(nl1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", ug1.a(str), e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<wl1> a(String str, String str2, sl1 sl1) {
        b(sl1, false);
        try {
            return (List) this.e.a().a(new ji1(this, sl1, str, str2)).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<wl1> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.e.a().a(new ki1(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        ck0.a(runnable);
        if (!kg1.Y.a().booleanValue() || !this.e.a().s()) {
            this.e.a().a(runnable);
        } else {
            runnable.run();
        }
    }
}
