package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.kp1;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kq1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DataHolder e;
    @DexIgnore
    public /* final */ /* synthetic */ kp1.d f;

    @DexIgnore
    public kq1(kp1.d dVar, DataHolder dataHolder) {
        this.f = dVar;
        this.e = dataHolder;
    }

    @DexIgnore
    public final void run() {
        ep1 ep1 = new ep1(this.e);
        try {
            kp1.this.a(ep1);
        } finally {
            ep1.a();
        }
    }
}
