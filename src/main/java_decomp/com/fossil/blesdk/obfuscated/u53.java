package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.bt2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u53 extends as2 implements t53 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<hg2> j;
    @DexIgnore
    public bt2 k;
    @DexIgnore
    public s53 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return u53.n;
        }

        @DexIgnore
        public final u53 b() {
            return new u53();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u53 e;

        @DexIgnore
        public b(u53 u53) {
            this.e = u53;
        }

        @DexIgnore
        public final void onClick(View view) {
            hg2 a = this.e.U0().a();
            if (a != null) {
                a.s.setText("");
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ u53 e;

        @DexIgnore
        public c(u53 u53) {
            this.e = u53;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (TextUtils.isEmpty(charSequence)) {
                hg2 a = this.e.U0().a();
                if (a != null) {
                    ImageView imageView = a.r;
                    if (imageView != null) {
                        imageView.setVisibility(8);
                    }
                }
                this.e.a("");
                this.e.V0().h();
                return;
            }
            hg2 a2 = this.e.U0().a();
            if (a2 != null) {
                ImageView imageView2 = a2.r;
                if (imageView2 != null) {
                    imageView2.setVisibility(0);
                }
            }
            this.e.a(String.valueOf(charSequence));
            this.e.V0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u53 e;

        @DexIgnore
        public d(u53 u53) {
            this.e = u53;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements bt2.d {
        @DexIgnore
        public /* final */ /* synthetic */ u53 a;

        @DexIgnore
        public e(u53 u53) {
            this.a = u53;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            hg2 a2 = this.a.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                wd4.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.u;
                wd4.a((Object) flexibleTextView2, "it.tvNotFound");
                be4 be4 = be4.a;
                String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements bt2.e {
        @DexIgnore
        public /* final */ /* synthetic */ u53 a;

        @DexIgnore
        public f(u53 u53) {
            this.a = u53;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            wd4.b(watchApp, "item");
            this.a.V0().a(watchApp);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ hg2 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(hg2 hg2, long j) {
            this.a = hg2;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.a.q;
            wd4.a((Object) flexibleTextView, "binding.btnCancel");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = u53.class.getSimpleName();
        wd4.a((Object) simpleName, "WatchAppSearchFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void T0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            dl2 dl2 = dl2.a;
            ur3<hg2> ur3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (ur3 != null) {
                hg2 a2 = ur3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    wd4.a((Object) activity, "it");
                    dl2.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final ur3<hg2> U0() {
        ur3<hg2> ur3 = this.j;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final s53 V0() {
        s53 s53 = this.l;
        if (s53 != null) {
            return s53;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(List<Pair<WatchApp, String>> list) {
        wd4.b(list, "results");
        bt2 bt2 = this.k;
        if (bt2 != null) {
            bt2.b(list);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void e(List<Pair<WatchApp, String>> list) {
        wd4.b(list, "recentSearchResult");
        bt2 bt2 = this.k;
        if (bt2 != null) {
            bt2.a(list);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (hg2) ra.a(layoutInflater, R.layout.fragment_watch_app_search, viewGroup, false, O0()));
        ur3<hg2> ur3 = this.j;
        if (ur3 != null) {
            hg2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        s53 s53 = this.l;
        if (s53 != null) {
            s53.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        s53 s53 = this.l;
        if (s53 != null) {
            s53.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "it");
            a(activity, 550);
        }
        this.k = new bt2();
        ur3<hg2> ur3 = this.j;
        if (ur3 != null) {
            hg2 a2 = ur3.a();
            if (a2 != null) {
                hg2 hg2 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = hg2.t;
                wd4.a((Object) recyclerViewEmptySupport, "this.rvResults");
                bt2 bt2 = this.k;
                if (bt2 != null) {
                    recyclerViewEmptySupport.setAdapter(bt2);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = hg2.t;
                    wd4.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = hg2.t;
                    FlexibleTextView flexibleTextView = hg2.u;
                    wd4.a((Object) flexibleTextView, "this.tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = hg2.r;
                    wd4.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    hg2.r.setOnClickListener(new b(this));
                    hg2.s.addTextChangedListener(new c(this));
                    hg2.q.setOnClickListener(new d(this));
                    bt2 bt22 = this.k;
                    if (bt22 != null) {
                        bt22.a((bt2.d) new e(this));
                        bt2 bt23 = this.k;
                        if (bt23 != null) {
                            bt23.a((bt2.e) new f(this));
                        } else {
                            wd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wd4.d("mAdapter");
                        throw null;
                    }
                } else {
                    wd4.d("mAdapter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        bt2 bt2 = this.k;
        if (bt2 != null) {
            bt2.b((List<Pair<WatchApp, String>>) null);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = bq2.a.a(j2);
        Window window = fragmentActivity.getWindow();
        wd4.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        ur3<hg2> ur3 = this.j;
        if (ur3 != null) {
            hg2 a3 = ur3.a();
            if (a3 != null) {
                wd4.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, hg2 hg2) {
        FlexibleTextView flexibleTextView = hg2.q;
        wd4.a((Object) flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener(new g(hg2, j2));
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        bt2 bt2 = this.k;
        if (bt2 != null) {
            bt2.a(str);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(s53 s53) {
        wd4.b(s53, "presenter");
        this.l = s53;
    }

    @DexIgnore
    public void a(WatchApp watchApp) {
        wd4.b(watchApp, "selectedWatchApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            dl2 dl2 = dl2.a;
            ur3<hg2> ur3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (ur3 != null) {
                hg2 a2 = ur3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    wd4.a((Object) activity, "it");
                    dl2.a(flexibleTextView, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_WATCH_APP_RESULT_ID", watchApp.getWatchappId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            wd4.d("mBinding");
            throw null;
        }
    }
}
