package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f13 extends j62<b, c, j62.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.b {
        @DexIgnore
        public /* final */ List<ContactWrapper> a;
        @DexIgnore
        public /* final */ List<AppWrapper> b;

        @DexIgnore
        public b(List<ContactWrapper> list, List<AppWrapper> list2) {
            wd4.b(list, "contactWrapperList");
            wd4.b(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.b;
        }

        @DexIgnore
        public final List<ContactWrapper> b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = f13.class.getSimpleName();
        wd4.a((Object) simpleName, "SaveAllHybridNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public f13(NotificationsRepository notificationsRepository) {
        wd4.b(notificationsRepository, "mNotificationsRepository");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<ContactWrapper> list) {
        if (!list.isEmpty()) {
            ContactProvider b2 = en2.p.a().b();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (ContactWrapper contact : list) {
                Contact contact2 = contact.getContact();
                if (contact2 != null) {
                    Contact contact3 = b2.getContact(contact2.getDbRowId());
                    if (contact3 != null) {
                        arrayList2.add(contact3);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        local.d(str, "Removed contact=" + contact3.getFirstName() + ", rowId=" + contact3.getDbRowId());
                        List<ContactGroup> allContactGroups = b2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        ArrayList arrayList4 = new ArrayList();
                        for (ContactGroup next : allContactGroups) {
                            wd4.a((Object) next, "contactGroupItem");
                            Iterator<Contact> it = next.getContacts().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Contact next2 = it.next();
                                int contactId = contact3.getContactId();
                                wd4.a((Object) next2, "contactItem");
                                if (contactId == next2.getContactId()) {
                                    contact3.setDbRowId(next2.getDbRowId());
                                    arrayList.add(next);
                                    arrayList4.addAll(next2.getPhoneNumbers());
                                    break;
                                }
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = e;
                        local2.d(str2, "Save phone numbers " + arrayList4 + " of contact " + contact3.getDisplayName());
                        Iterator it2 = arrayList4.iterator();
                        while (it2.hasNext()) {
                            PhoneNumber phoneNumber = (PhoneNumber) it2.next();
                            wd4.a((Object) phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                            arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            c(arrayList3);
        }
    }

    @DexIgnore
    public final void c(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact removePhoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(removePhoneFavoritesContact);
        }
    }

    @DexIgnore
    public final void d(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(next.getCurrentHandGroup());
                InstalledApp installedApp = next.getInstalledApp();
                String str = null;
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved App: name=");
                InstalledApp installedApp2 = next.getInstalledApp();
                if (installedApp2 != null) {
                    str = installedApp2.getIdentifier();
                }
                sb.append(str);
                sb.append(", hour=");
                sb.append(next.getCurrentHandGroup());
                local.d(str2, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x028e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017e  */
    public final void e(List<ContactWrapper> list) {
        Iterator<ContactWrapper> it;
        Contact contact;
        if (!list.isEmpty()) {
            List<ContactGroup> allContactGroups = this.d.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ContactProvider b2 = en2.p.a().b();
            Iterator<ContactWrapper> it2 = list.iterator();
            while (it2.hasNext()) {
                ContactWrapper next = it2.next();
                if (allContactGroups != null) {
                    Iterator<ContactGroup> it3 = allContactGroups.iterator();
                    boolean z = false;
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        ContactGroup next2 = it3.next();
                        wd4.a((Object) next2, "contactGroup");
                        Iterator<Contact> it4 = next2.getContacts().iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                it = it2;
                                break;
                            }
                            Contact next3 = it4.next();
                            if (!(next.getContact() == null || next3 == null)) {
                                int contactId = next3.getContactId();
                                Contact contact2 = next.getContact();
                                if (contact2 != null && contactId == contact2.getContactId()) {
                                    Contact contact3 = next.getContact();
                                    Boolean valueOf = contact3 != null ? Boolean.valueOf(contact3.isUseCall()) : null;
                                    if (valueOf != null) {
                                        next3.setUseCall(valueOf.booleanValue());
                                        Contact contact4 = next.getContact();
                                        Boolean valueOf2 = contact4 != null ? Boolean.valueOf(contact4.isUseSms()) : null;
                                        if (valueOf2 != null) {
                                            next3.setUseSms(valueOf2.booleanValue());
                                            Contact contact5 = next.getContact();
                                            next3.setFirstName(contact5 != null ? contact5.getFirstName() : null);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str = e;
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Contact Id = ");
                                            Contact contact6 = next.getContact();
                                            sb.append(contact6 != null ? Integer.valueOf(contact6.getContactId()) : null);
                                            sb.append(", ");
                                            it = it2;
                                            sb.append("Contact name = ");
                                            Contact contact7 = next.getContact();
                                            sb.append(contact7 != null ? contact7.getFirstName() : null);
                                            sb.append(", ");
                                            sb.append("Contact db row = ");
                                            Contact contact8 = next.getContact();
                                            sb.append(contact8 != null ? Integer.valueOf(contact8.getDbRowId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact phone = ");
                                            sb.append(next.getPhoneNumber());
                                            local.d(str, sb.toString());
                                            next.setContact(next3);
                                            b2.removeContactGroup(next2);
                                            allContactGroups.remove(next2);
                                            z = true;
                                        } else {
                                            wd4.a();
                                            throw null;
                                        }
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                }
                            }
                            it2 = it2;
                        }
                        if (z) {
                            break;
                        }
                        it2 = it;
                    }
                    ContactGroup contactGroup = new ContactGroup();
                    contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                    contactGroup.setHour(next.getCurrentHandGroup());
                    arrayList2.add(contactGroup);
                    contact = next.getContact();
                    if (contact == null) {
                        contact.setContactGroup(contactGroup);
                        Contact contact9 = next.getContact();
                        Boolean valueOf3 = contact9 != null ? Boolean.valueOf(contact9.isUseCall()) : null;
                        if (valueOf3 != null) {
                            contact.setUseCall(valueOf3.booleanValue());
                            Contact contact10 = next.getContact();
                            Boolean valueOf4 = contact10 != null ? Boolean.valueOf(contact10.isUseSms()) : null;
                            if (valueOf4 != null) {
                                contact.setUseSms(valueOf4.booleanValue());
                                contact.setUseEmail(false);
                                arrayList.add(contact);
                                ContentResolver contentResolver = PortfolioApp.W.c().getContentResolver();
                                if (next.hasPhoneNumber()) {
                                    Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id=" + contact.getContactId(), (String[]) null, (String) null);
                                    if (query != null) {
                                        while (query.moveToNext()) {
                                            try {
                                                PhoneNumber phoneNumber = new PhoneNumber();
                                                phoneNumber.setNumber(query.getString(query.getColumnIndex("data1")));
                                                phoneNumber.setContact(contact);
                                                arrayList3.add(phoneNumber);
                                                if (next.isFavorites()) {
                                                    arrayList4.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                                }
                                            } catch (Exception e2) {
                                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                                String str2 = e;
                                                local2.e(str2, "Error Inside " + e + ".saveContactToFSL - ex=" + e2);
                                            } catch (Throwable th) {
                                                query.close();
                                                throw th;
                                            }
                                        }
                                        query.close();
                                    }
                                    if (contact.getContactId() == -100) {
                                        PhoneNumber phoneNumber2 = new PhoneNumber();
                                        phoneNumber2.setNumber("-1234");
                                        phoneNumber2.setContact(contact);
                                        arrayList3.add(phoneNumber2);
                                    }
                                    if (contact.getContactId() == -200) {
                                        PhoneNumber phoneNumber3 = new PhoneNumber();
                                        phoneNumber3.setNumber("-5678");
                                        phoneNumber3.setContact(contact);
                                        arrayList3.add(phoneNumber3);
                                    }
                                }
                                it2 = it;
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                it = it2;
                ContactGroup contactGroup2 = new ContactGroup();
                contactGroup2.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                contactGroup2.setHour(next.getCurrentHandGroup());
                arrayList2.add(contactGroup2);
                contact = next.getContact();
                if (contact == null) {
                }
            }
            this.d.saveContactGroupList(arrayList2);
            this.d.saveListContact(arrayList);
            this.d.saveListPhoneNumber(arrayList3);
            f(arrayList4);
        }
    }

    @DexIgnore
    public final void f(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact savePhoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(savePhoneFavoritesContact);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        wd4.b(bVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (ContactWrapper next : bVar.b()) {
            if (!next.isAdded()) {
                arrayList2.add(next);
            } else {
                arrayList.add(next);
            }
        }
        b(arrayList2);
        e(arrayList);
        for (AppWrapper next2 : bVar.a()) {
            InstalledApp installedApp = next2.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                wd4.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList3.add(next2);
            } else {
                arrayList4.add(next2);
            }
        }
        a((List<AppWrapper>) arrayList4);
        d(arrayList3);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAllHybridNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = next.getInstalledApp();
                Integer num = null;
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(next.getCurrentHandGroup());
                    InstalledApp installedApp2 = next.getInstalledApp();
                    appFilter.setType(installedApp2 != null ? installedApp2.getIdentifier() : null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Removed App: name=");
                    InstalledApp installedApp3 = next.getInstalledApp();
                    sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                    sb.append(", rowId=");
                    InstalledApp installedApp4 = next.getInstalledApp();
                    if (installedApp4 != null) {
                        num = Integer.valueOf(installedApp4.getDbRowId());
                    }
                    sb.append(num);
                    sb.append(", hour=");
                    sb.append(next.getCurrentHandGroup());
                    local.d(str, sb.toString());
                    arrayList.add(appFilter);
                } else {
                    wd4.a();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }
}
