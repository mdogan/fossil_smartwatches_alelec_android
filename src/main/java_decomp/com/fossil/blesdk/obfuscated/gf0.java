package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gf0<A extends ee0.b, ResultT> {
    @DexIgnore
    public /* final */ xd0[] a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, yn1<ResultT> yn1) throws RemoteException;

    @DexIgnore
    public boolean a() {
        return this.b;
    }

    @DexIgnore
    public final xd0[] b() {
        return this.a;
    }
}
