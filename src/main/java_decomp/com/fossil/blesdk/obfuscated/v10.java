package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v10 extends GattOperationResult {
    @DexIgnore
    public /* final */ Peripheral.BondState b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v10(GattOperationResult.GattResult gattResult, Peripheral.BondState bondState, Peripheral.BondState bondState2) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        wd4.b(bondState, "previousBondState");
        wd4.b(bondState2, "newBondState");
        this.b = bondState2;
    }

    @DexIgnore
    public final Peripheral.BondState b() {
        return this.b;
    }
}
