package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ts implements no<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ ps a; // = new ps();

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, mo moVar) throws IOException {
        return true;
    }

    @DexIgnore
    public bq<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, mo moVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(byteBuffer), i, i2, moVar);
    }
}
