package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.fossil.blesdk.obfuscated.uo;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ap implements uo<InputStream> {
    @DexIgnore
    public /* final */ RecyclableBufferedInputStream a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements uo.a<InputStream> {
        @DexIgnore
        public /* final */ hq a;

        @DexIgnore
        public a(hq hqVar) {
            this.a = hqVar;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public uo<InputStream> a(InputStream inputStream) {
            return new ap(inputStream, this.a);
        }
    }

    @DexIgnore
    public ap(InputStream inputStream, hq hqVar) {
        this.a = new RecyclableBufferedInputStream(inputStream, hqVar);
        this.a.mark(5242880);
    }

    @DexIgnore
    public void a() {
        this.a.z();
    }

    @DexIgnore
    public InputStream b() throws IOException {
        this.a.reset();
        return this.a;
    }
}
