package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oa {
    @DexIgnore
    public abstract ViewDataBinding a(qa qaVar, View view, int i);

    @DexIgnore
    public abstract ViewDataBinding a(qa qaVar, View[] viewArr, int i);

    @DexIgnore
    public List<oa> a() {
        return Collections.emptyList();
    }
}
