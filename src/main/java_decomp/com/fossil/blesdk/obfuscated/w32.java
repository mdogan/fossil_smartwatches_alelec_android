package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w32 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public w32(byte[] bArr, byte[] bArr2) {
        this.a = bArr;
        this.b = bArr2;
    }

    @DexIgnore
    public byte[] a() {
        return this.a;
    }

    @DexIgnore
    public byte[] b() {
        return this.b;
    }
}
