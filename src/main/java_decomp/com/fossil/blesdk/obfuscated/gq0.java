package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gq0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gq0> CREATOR; // = new hq0();
    @DexIgnore
    public /* final */ DataSet e;
    @DexIgnore
    public /* final */ h11 f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public gq0(DataSet dataSet, IBinder iBinder, boolean z) {
        this.e = dataSet;
        this.f = i11.a(iBinder);
        this.g = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof gq0) && ak0.a(this.e, ((gq0) obj).e);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.e);
    }

    @DexIgnore
    public final String toString() {
        ak0.a a = ak0.a((Object) this);
        a.a("dataSet", this.e);
        return a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) this.e, i, false);
        h11 h11 = this.f;
        lk0.a(parcel, 2, h11 == null ? null : h11.asBinder(), false);
        lk0.a(parcel, 4, this.g);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public gq0(DataSet dataSet, h11 h11, boolean z) {
        this.e = dataSet;
        this.f = h11;
        this.g = z;
    }
}
