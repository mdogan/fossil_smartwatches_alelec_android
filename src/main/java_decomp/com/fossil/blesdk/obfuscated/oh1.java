package com.fossil.blesdk.obfuscated;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oh1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ h81 e;
    @DexIgnore
    public /* final */ /* synthetic */ ServiceConnection f;
    @DexIgnore
    public /* final */ /* synthetic */ nh1 g;

    @DexIgnore
    public oh1(nh1 nh1, h81 h81, ServiceConnection serviceConnection) {
        this.g = nh1;
        this.e = h81;
        this.f = serviceConnection;
    }

    @DexIgnore
    public final void run() {
        nh1 nh1 = this.g;
        mh1 mh1 = nh1.b;
        String a = nh1.a;
        h81 h81 = this.e;
        ServiceConnection serviceConnection = this.f;
        Bundle a2 = mh1.a(a, h81);
        mh1.a.a().e();
        if (a2 != null) {
            long j = a2.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                mh1.a.d().s().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a2.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    mh1.a.d().s().a("No referrer defined in install referrer response");
                } else {
                    mh1.a.d().A().a("InstallReferrer API result", string);
                    ol1 s = mh1.a.s();
                    String valueOf = String.valueOf(string);
                    Bundle a3 = s.a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a3 == null) {
                        mh1.a.d().s().a("No campaign params defined in install referrer result");
                    } else {
                        String string2 = a3.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a2.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                mh1.a.d().s().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a3.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == mh1.a.t().k.a()) {
                            mh1.a.b();
                            mh1.a.d().A().a("Campaign has already been logged");
                        } else {
                            mh1.a.t().k.a(j);
                            mh1.a.b();
                            mh1.a.d().A().a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a3.putString("_cis", "referrer API");
                            mh1.a.k().b("auto", "_cmp", a3);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            em0.a().a(mh1.a.getContext(), serviceConnection);
        }
    }
}
