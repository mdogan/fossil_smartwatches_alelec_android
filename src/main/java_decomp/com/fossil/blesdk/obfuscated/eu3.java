package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eu3 extends RecyclerView.l {
    @DexIgnore
    public int a;

    @DexIgnore
    public eu3() {
        Resources system = Resources.getSystem();
        wd4.a((Object) system, "Resources.getSystem()");
        this.a = system.getDisplayMetrics().widthPixels;
    }

    @DexIgnore
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        wd4.b(rect, "outRect");
        wd4.b(view, "view");
        wd4.b(recyclerView, "parent");
        wd4.b(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        RecyclerView.g adapter = recyclerView.getAdapter();
        if (adapter != null) {
            wd4.a((Object) adapter, "parent.adapter!!");
            int itemCount = adapter.getItemCount();
            int e = recyclerView.e(view);
            if (itemCount > 0) {
                int width = recyclerView.getWidth();
                int width2 = view.getWidth();
                if (width == 0) {
                    width = this.a;
                }
                if (width2 == 0) {
                    width2 = this.a / 2;
                }
                if (itemCount == 1 || e == 0) {
                    rect.set(Math.max(0, (width - width2) / 2), 0, 0, 0);
                } else if (e == itemCount - 1) {
                    rect.set(0, 0, Math.max(0, (width - width2) / 2), 0);
                }
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
