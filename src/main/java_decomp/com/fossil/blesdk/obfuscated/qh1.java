package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qh1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ yh1 e;
    @DexIgnore
    public /* final */ /* synthetic */ ug1 f;

    @DexIgnore
    public qh1(ph1 ph1, yh1 yh1, ug1 ug1) {
        this.e = yh1;
        this.f = ug1;
    }

    @DexIgnore
    public final void run() {
        if (this.e.w() == null) {
            this.f.s().a("Install Referrer Reporter is null");
            return;
        }
        mh1 w = this.e.w();
        w.a.i();
        w.a(w.a.getContext().getPackageName());
    }
}
