package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sc0 extends as0 implements rc0 {
    @DexIgnore
    public sc0() {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            k();
        } else if (i != 2) {
            return false;
        } else {
            l();
        }
        return true;
    }
}
