package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class xw1 implements fz1 {
    @DexIgnore
    public /* final */ lw1 a;
    @DexIgnore
    public /* final */ kw1 b;

    @DexIgnore
    public xw1(lw1 lw1, kw1 kw1) {
        this.a = lw1;
        this.b = kw1;
    }

    @DexIgnore
    public static fz1 a(lw1 lw1, kw1 kw1) {
        return new xw1(lw1, kw1);
    }

    @DexIgnore
    public final Object get() {
        return this.a.a(this.b);
    }
}
