package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pn4 implements Interceptor.Chain {
    @DexIgnore
    public /* final */ List<Interceptor> a;
    @DexIgnore
    public /* final */ in4 b;
    @DexIgnore
    public /* final */ ln4 c;
    @DexIgnore
    public /* final */ fn4 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ pm4 f;
    @DexIgnore
    public /* final */ vl4 g;
    @DexIgnore
    public /* final */ hm4 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public pn4(List<Interceptor> list, in4 in4, ln4 ln4, fn4 fn4, int i2, pm4 pm4, vl4 vl4, hm4 hm4, int i3, int i4, int i5) {
        this.a = list;
        this.d = fn4;
        this.b = in4;
        this.c = ln4;
        this.e = i2;
        this.f = pm4;
        this.g = vl4;
        this.h = hm4;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    public int a() {
        return this.j;
    }

    @DexIgnore
    public int b() {
        return this.k;
    }

    @DexIgnore
    public zl4 c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.i;
    }

    @DexIgnore
    public vl4 e() {
        return this.g;
    }

    @DexIgnore
    public hm4 f() {
        return this.h;
    }

    @DexIgnore
    public ln4 g() {
        return this.c;
    }

    @DexIgnore
    public in4 h() {
        return this.b;
    }

    @DexIgnore
    public pm4 n() {
        return this.f;
    }

    @DexIgnore
    public Response a(pm4 pm4) throws IOException {
        return a(pm4, this.b, this.c, this.d);
    }

    @DexIgnore
    public Response a(pm4 pm4, in4 in4, ln4 ln4, fn4 fn4) throws IOException {
        if (this.e < this.a.size()) {
            this.l++;
            if (this.c != null && !this.d.a(pm4.g())) {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                pn4 pn4 = new pn4(this.a, in4, ln4, fn4, this.e + 1, pm4, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.a.get(this.e);
                Response intercept = interceptor.intercept(pn4);
                if (ln4 != null && this.e + 1 < this.a.size() && pn4.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.y() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }
}
