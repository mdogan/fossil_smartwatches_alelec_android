package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dw2 extends cs2<cw2> {
    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void close();

    @DexIgnore
    void g(List<AppWrapper> list);

    @DexIgnore
    void h(boolean z);

    @DexIgnore
    void j();
}
