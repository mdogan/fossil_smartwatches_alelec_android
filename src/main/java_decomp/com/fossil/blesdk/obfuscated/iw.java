package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iw {
    @DexIgnore
    public static /* final */ ConcurrentMap<String, ko> a; // = new ConcurrentHashMap();

    @DexIgnore
    public static String a(PackageInfo packageInfo) {
        if (packageInfo != null) {
            return String.valueOf(packageInfo.versionCode);
        }
        return UUID.randomUUID().toString();
    }

    @DexIgnore
    public static ko b(Context context) {
        String packageName = context.getPackageName();
        ko koVar = (ko) a.get(packageName);
        if (koVar != null) {
            return koVar;
        }
        ko c = c(context);
        ko putIfAbsent = a.putIfAbsent(packageName, c);
        return putIfAbsent == null ? c : putIfAbsent;
    }

    @DexIgnore
    public static ko c(Context context) {
        return new kw(a(a(context)));
    }

    @DexIgnore
    public static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("AppVersionSignature", "Cannot resolve info for" + context.getPackageName(), e);
            return null;
        }
    }
}
