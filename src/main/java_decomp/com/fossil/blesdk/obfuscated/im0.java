package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface im0 {
    @DexIgnore
    long a();

    @DexIgnore
    long b();

    @DexIgnore
    long c();
}
