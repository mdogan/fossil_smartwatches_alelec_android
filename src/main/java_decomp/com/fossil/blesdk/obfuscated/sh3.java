package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sh3 implements Factory<ProfileEditViewModel> {
    @DexIgnore
    public /* final */ Provider<UpdateUser> a;
    @DexIgnore
    public /* final */ Provider<or2> b;

    @DexIgnore
    public sh3(Provider<UpdateUser> provider, Provider<or2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static sh3 a(Provider<UpdateUser> provider, Provider<or2> provider2) {
        return new sh3(provider, provider2);
    }

    @DexIgnore
    public static ProfileEditViewModel b(Provider<UpdateUser> provider, Provider<or2> provider2) {
        return new ProfileEditViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public ProfileEditViewModel get() {
        return b(this.a, this.b);
    }
}
