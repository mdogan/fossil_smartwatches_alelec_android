package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a33 {
    @DexIgnore
    public /* final */ z23 a;

    @DexIgnore
    public a33(z23 z23) {
        wd4.b(z23, "mCommuteTimeSettingsDefaultAddressContractView");
        this.a = z23;
    }

    @DexIgnore
    public final z23 a() {
        return this.a;
    }
}
