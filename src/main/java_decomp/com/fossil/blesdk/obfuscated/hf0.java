package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hf0 {
    @DexIgnore
    public static void a(Status status, yn1<Void> yn1) {
        a(status, (Object) null, yn1);
    }

    @DexIgnore
    public static <TResult> void a(Status status, TResult tresult, yn1<TResult> yn1) {
        if (status.L()) {
            yn1.a(tresult);
        } else {
            yn1.a((Exception) new ApiException(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static xn1<Void> a(xn1<Boolean> xn1) {
        return xn1.a((qn1<Boolean, TContinuationResult>) new mh0());
    }
}
