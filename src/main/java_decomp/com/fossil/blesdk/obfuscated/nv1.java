package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nv1<T> {
    @DexIgnore
    boolean a(String str) throws IOException;

    @DexIgnore
    T getResult();
}
