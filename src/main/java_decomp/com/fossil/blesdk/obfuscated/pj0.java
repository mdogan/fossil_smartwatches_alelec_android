package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.jj0;
import com.fossil.blesdk.obfuscated.qj0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pj0<T extends IInterface> extends jj0<T> implements ee0.f, qj0.a {
    @DexIgnore
    public /* final */ lj0 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public pj0(Context context, Looper looper, int i, lj0 lj0, he0.b bVar, he0.c cVar) {
        this(context, looper, r3, r4, i, lj0, bVar, cVar);
        rj0 a = rj0.a(context);
        yd0 a2 = yd0.a();
        ck0.a(bVar);
        ck0.a(cVar);
    }

    @DexIgnore
    public static jj0.a a(he0.b bVar) {
        if (bVar == null) {
            return null;
        }
        return new el0(bVar);
    }

    @DexIgnore
    public final lj0 F() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    public final Set<Scope> b(Set<Scope> set) {
        Set<Scope> a = a(set);
        for (Scope contains : a) {
            if (!set.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a;
    }

    @DexIgnore
    public int i() {
        return super.i();
    }

    @DexIgnore
    public final Account r() {
        return this.D;
    }

    @DexIgnore
    public final Set<Scope> w() {
        return this.C;
    }

    @DexIgnore
    public static jj0.b a(he0.c cVar) {
        if (cVar == null) {
            return null;
        }
        return new fl0(cVar);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj0(Context context, Looper looper, rj0 rj0, yd0 yd0, int i, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, rj0, yd0, i, a(bVar), a(cVar), lj0.g());
        this.B = lj0;
        this.D = lj0.a();
        this.C = b(lj0.d());
    }
}
