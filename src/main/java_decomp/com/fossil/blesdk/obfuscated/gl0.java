package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gl0 extends ls0 implements zj0 {
    @DexIgnore
    public gl0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    public final tn0 a(tn0 tn0, fk0 fk0) throws RemoteException {
        Parcel o = o();
        ns0.a(o, (IInterface) tn0);
        ns0.a(o, (Parcelable) fk0);
        Parcel a = a(2, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
