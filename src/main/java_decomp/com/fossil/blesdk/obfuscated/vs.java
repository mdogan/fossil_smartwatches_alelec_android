package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vs extends rs {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(ko.a);

    @DexIgnore
    public Bitmap a(kq kqVar, Bitmap bitmap, int i, int i2) {
        return ht.b(kqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof vs;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterInside".hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
