package com.fossil.blesdk.obfuscated;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.ak0;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.jj0;
import com.fossil.blesdk.obfuscated.te0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class we0 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static we0 q;
    @DexIgnore
    public long a; // = 5000;
    @DexIgnore
    public long b; // = ScanService.BLE_SCAN_TIMEOUT;
    @DexIgnore
    public long c; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ yd0 e;
    @DexIgnore
    public /* final */ tj0 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<zh0<?>, a<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public nf0 j; // = null;
    @DexIgnore
    public /* final */ Set<zh0<?>> k; // = new h4();
    @DexIgnore
    public /* final */ Set<zh0<?>> l; // = new h4();
    @DexIgnore
    public /* final */ Handler m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements lh0, jj0.c {
        @DexIgnore
        public /* final */ ee0.f a;
        @DexIgnore
        public /* final */ zh0<?> b;
        @DexIgnore
        public uj0 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public c(ee0.f fVar, zh0<?> zh0) {
            this.a = fVar;
            this.b = zh0;
        }

        @DexIgnore
        public final void a(vd0 vd0) {
            we0.this.m.post(new yg0(this, vd0));
        }

        @DexIgnore
        public final void b(vd0 vd0) {
            ((a) we0.this.i.get(this.b)).b(vd0);
        }

        @DexIgnore
        public final void a(uj0 uj0, Set<Scope> set) {
            if (uj0 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new vd0(4));
                return;
            }
            this.c = uj0;
            this.d = set;
            a();
        }

        @DexIgnore
        public final void a() {
            if (this.e) {
                uj0 uj0 = this.c;
                if (uj0 != null) {
                    this.a.a(uj0, this.d);
                }
            }
        }
    }

    @DexIgnore
    public we0(Context context, Looper looper, yd0 yd0) {
        this.d = context;
        this.m = new ts0(looper, this);
        this.e = yd0;
        this.f = new tj0(yd0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static we0 a(Context context) {
        we0 we0;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new we0(context.getApplicationContext(), handlerThread.getLooper(), yd0.a());
            }
            we0 = q;
        }
        return we0;
    }

    @DexIgnore
    public static void d() {
        synchronized (p) {
            if (q != null) {
                we0 we0 = q;
                we0.h.incrementAndGet();
                we0.m.sendMessageAtFrontOfQueue(we0.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static we0 e() {
        we0 we0;
        synchronized (p) {
            ck0.a(q, (Object) "Must guarantee manager is non-null before using getInstance");
            we0 = q;
        }
        return we0;
    }

    @DexIgnore
    public final int b() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final void c() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        a aVar;
        int i2 = message.what;
        long j2 = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = ButtonService.CONNECT_TIMEOUT;
                }
                this.c = j2;
                this.m.removeMessages(12);
                for (zh0<?> obtainMessage : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, obtainMessage), this.c);
                }
                break;
            case 2:
                bi0 bi0 = (bi0) message.obj;
                Iterator<zh0<?>> it = bi0.b().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        zh0 next = it.next();
                        a aVar2 = this.i.get(next);
                        if (aVar2 == null) {
                            bi0.a(next, new vd0(13), (String) null);
                            break;
                        } else if (aVar2.c()) {
                            bi0.a(next, vd0.i, aVar2.f().f());
                        } else if (aVar2.n() != null) {
                            bi0.a(next, aVar2.n(), (String) null);
                        } else {
                            aVar2.a(bi0);
                            aVar2.a();
                        }
                    }
                }
            case 3:
                for (a next2 : this.i.values()) {
                    next2.m();
                    next2.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                eh0 eh0 = (eh0) message.obj;
                a aVar3 = this.i.get(eh0.c.h());
                if (aVar3 == null) {
                    b(eh0.c);
                    aVar3 = this.i.get(eh0.c.h());
                }
                if (aVar3.d() && this.h.get() != eh0.b) {
                    eh0.a.a(n);
                    aVar3.k();
                    break;
                } else {
                    aVar3.a(eh0.a);
                    break;
                }
                break;
            case 5:
                int i3 = message.arg1;
                vd0 vd0 = (vd0) message.obj;
                Iterator<a<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = it2.next();
                        if (aVar.b() == i3) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i3);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b2 = this.e.b(vd0.H());
                    String I = vd0.I();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(I).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b2);
                    sb2.append(": ");
                    sb2.append(I);
                    aVar.a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (qm0.a() && (this.d.getApplicationContext() instanceof Application)) {
                    te0.a((Application) this.d.getApplicationContext());
                    te0.b().a((te0.a) new sg0(this));
                    if (!te0.b().b(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                b((ge0<?>) (ge0) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).e();
                    break;
                }
                break;
            case 10:
                for (zh0<?> remove : this.l) {
                    this.i.remove(remove).k();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).q();
                    break;
                }
                break;
            case 14:
                of0 of0 = (of0) message.obj;
                zh0<?> b3 = of0.b();
                if (this.i.containsKey(b3)) {
                    of0.a().a(Boolean.valueOf(this.i.get(b3).a(false)));
                    break;
                } else {
                    of0.a().a(false);
                    break;
                }
            case 15:
                b bVar = (b) message.obj;
                if (this.i.containsKey(bVar.a)) {
                    this.i.get(bVar.a).a(bVar);
                    break;
                }
                break;
            case 16:
                b bVar2 = (b) message.obj;
                if (this.i.containsKey(bVar2.a)) {
                    this.i.get(bVar2.a).b(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(ge0<?> ge0) {
        zh0<?> h2 = ge0.h();
        a aVar = this.i.get(h2);
        if (aVar == null) {
            aVar = new a(ge0);
            this.i.put(h2, aVar);
        }
        if (aVar.d()) {
            this.l.add(h2);
        }
        aVar.a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a<O extends ee0.d> implements he0.b, he0.c, ii0 {
        @DexIgnore
        public /* final */ Queue<jg0> e; // = new LinkedList();
        @DexIgnore
        public /* final */ ee0.f f;
        @DexIgnore
        public /* final */ ee0.b g;
        @DexIgnore
        public /* final */ zh0<O> h;
        @DexIgnore
        public /* final */ kf0 i;
        @DexIgnore
        public /* final */ Set<bi0> j; // = new HashSet();
        @DexIgnore
        public /* final */ Map<af0.a<?>, fh0> k; // = new HashMap();
        @DexIgnore
        public /* final */ int l;
        @DexIgnore
        public /* final */ ih0 m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public /* final */ List<b> o; // = new ArrayList();
        @DexIgnore
        public vd0 p; // = null;

        @DexIgnore
        public a(ge0<O> ge0) {
            this.f = ge0.a(we0.this.m.getLooper(), (a<O>) this);
            ee0.f fVar = this.f;
            if (fVar instanceof hk0) {
                this.g = ((hk0) fVar).G();
            } else {
                this.g = fVar;
            }
            this.h = ge0.h();
            this.i = new kf0();
            this.l = ge0.f();
            if (this.f.l()) {
                this.m = ge0.a(we0.this.d, we0.this.m);
            } else {
                this.m = null;
            }
        }

        @DexIgnore
        public final void a(vd0 vd0, ee0<?> ee0, boolean z) {
            if (Looper.myLooper() == we0.this.m.getLooper()) {
                a(vd0);
            } else {
                we0.this.m.post(new vg0(this, vd0));
            }
        }

        @DexIgnore
        public final void b(vd0 vd0) {
            ck0.a(we0.this.m);
            this.f.a();
            a(vd0);
        }

        @DexIgnore
        public final boolean c(vd0 vd0) {
            synchronized (we0.p) {
                if (we0.this.j == null || !we0.this.k.contains(this.h)) {
                    return false;
                }
                we0.this.j.b(vd0, this.l);
                return true;
            }
        }

        @DexIgnore
        public final void d(vd0 vd0) {
            for (bi0 next : this.j) {
                String str = null;
                if (ak0.a(vd0, vd0.i)) {
                    str = this.f.f();
                }
                next.a(this.h, vd0, str);
            }
            this.j.clear();
        }

        @DexIgnore
        public final void e(Bundle bundle) {
            if (Looper.myLooper() == we0.this.m.getLooper()) {
                h();
            } else {
                we0.this.m.post(new tg0(this));
            }
        }

        @DexIgnore
        public final void f(int i2) {
            if (Looper.myLooper() == we0.this.m.getLooper()) {
                i();
            } else {
                we0.this.m.post(new ug0(this));
            }
        }

        @DexIgnore
        public final void g() {
            Status status;
            ck0.a(we0.this.m);
            if (this.n) {
                o();
                if (we0.this.e.c(we0.this.d) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                a(status);
                this.f.a();
            }
        }

        @DexIgnore
        public final void h() {
            m();
            d(vd0.i);
            o();
            Iterator<fh0> it = this.k.values().iterator();
            while (it.hasNext()) {
                fh0 next = it.next();
                if (a(next.a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.a.a(this.g, new yn1());
                    } catch (DeadObjectException unused) {
                        f(1);
                        this.f.a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            j();
            p();
        }

        @DexIgnore
        public final void i() {
            m();
            this.n = true;
            this.i.c();
            we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 9, this.h), we0.this.a);
            we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 11, this.h), we0.this.b);
            we0.this.f.a();
        }

        @DexIgnore
        public final void j() {
            ArrayList arrayList = new ArrayList(this.e);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                jg0 jg0 = (jg0) obj;
                if (!this.f.c()) {
                    return;
                }
                if (b(jg0)) {
                    this.e.remove(jg0);
                }
            }
        }

        @DexIgnore
        public final void k() {
            ck0.a(we0.this.m);
            a(we0.n);
            this.i.b();
            for (af0.a yh0 : (af0.a[]) this.k.keySet().toArray(new af0.a[this.k.size()])) {
                a((jg0) new yh0(yh0, new yn1()));
            }
            d(new vd0(4));
            if (this.f.c()) {
                this.f.a((jj0.e) new wg0(this));
            }
        }

        @DexIgnore
        public final Map<af0.a<?>, fh0> l() {
            return this.k;
        }

        @DexIgnore
        public final void m() {
            ck0.a(we0.this.m);
            this.p = null;
        }

        @DexIgnore
        public final vd0 n() {
            ck0.a(we0.this.m);
            return this.p;
        }

        @DexIgnore
        public final void o() {
            if (this.n) {
                we0.this.m.removeMessages(11, this.h);
                we0.this.m.removeMessages(9, this.h);
                this.n = false;
            }
        }

        @DexIgnore
        public final void p() {
            we0.this.m.removeMessages(12, this.h);
            we0.this.m.sendMessageDelayed(we0.this.m.obtainMessage(12, this.h), we0.this.c);
        }

        @DexIgnore
        public final boolean q() {
            return a(true);
        }

        @DexIgnore
        public final mn1 r() {
            ih0 ih0 = this.m;
            if (ih0 == null) {
                return null;
            }
            return ih0.o();
        }

        @DexIgnore
        public final void a(vd0 vd0) {
            ck0.a(we0.this.m);
            ih0 ih0 = this.m;
            if (ih0 != null) {
                ih0.p();
            }
            m();
            we0.this.f.a();
            d(vd0);
            if (vd0.H() == 4) {
                a(we0.o);
            } else if (this.e.isEmpty()) {
                this.p = vd0;
            } else if (!c(vd0) && !we0.this.b(vd0, this.l)) {
                if (vd0.H() == 18) {
                    this.n = true;
                }
                if (this.n) {
                    we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 9, this.h), we0.this.a);
                    return;
                }
                String a = this.h.a();
                StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 38);
                sb.append("API: ");
                sb.append(a);
                sb.append(" is not available on this device.");
                a(new Status(17, sb.toString()));
            }
        }

        @DexIgnore
        public final boolean b(jg0 jg0) {
            if (!(jg0 instanceof gh0)) {
                c(jg0);
                return true;
            }
            gh0 gh0 = (gh0) jg0;
            xd0 a = a(gh0.b(this));
            if (a == null) {
                c(jg0);
                return true;
            } else if (gh0.c(this)) {
                b bVar = new b(this.h, a, (sg0) null);
                int indexOf = this.o.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.o.get(indexOf);
                    we0.this.m.removeMessages(15, bVar2);
                    we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 15, bVar2), we0.this.a);
                    return false;
                }
                this.o.add(bVar);
                we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 15, bVar), we0.this.a);
                we0.this.m.sendMessageDelayed(Message.obtain(we0.this.m, 16, bVar), we0.this.b);
                vd0 vd0 = new vd0(2, (PendingIntent) null);
                if (c(vd0)) {
                    return false;
                }
                we0.this.b(vd0, this.l);
                return false;
            } else {
                gh0.a((RuntimeException) new UnsupportedApiCallException(a));
                return false;
            }
        }

        @DexIgnore
        public final void e() {
            ck0.a(we0.this.m);
            if (this.n) {
                a();
            }
        }

        @DexIgnore
        public final ee0.f f() {
            return this.f;
        }

        @DexIgnore
        public final boolean d() {
            return this.f.l();
        }

        @DexIgnore
        public final void c(jg0 jg0) {
            jg0.a(this.i, d());
            try {
                jg0.a((a<?>) this);
            } catch (DeadObjectException unused) {
                f(1);
                this.f.a();
            }
        }

        @DexIgnore
        public final boolean c() {
            return this.f.c();
        }

        @DexIgnore
        public final void a(jg0 jg0) {
            ck0.a(we0.this.m);
            if (!this.f.c()) {
                this.e.add(jg0);
                vd0 vd0 = this.p;
                if (vd0 == null || !vd0.K()) {
                    a();
                } else {
                    a(this.p);
                }
            } else if (b(jg0)) {
                p();
            } else {
                this.e.add(jg0);
            }
        }

        @DexIgnore
        public final int b() {
            return this.l;
        }

        @DexIgnore
        public final void b(b bVar) {
            if (this.o.remove(bVar)) {
                we0.this.m.removeMessages(15, bVar);
                we0.this.m.removeMessages(16, bVar);
                xd0 b = bVar.b;
                ArrayList arrayList = new ArrayList(this.e.size());
                for (jg0 jg0 : this.e) {
                    if (jg0 instanceof gh0) {
                        xd0[] b2 = ((gh0) jg0).b(this);
                        if (b2 != null && gm0.a((T[]) b2, b)) {
                            arrayList.add(jg0);
                        }
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    jg0 jg02 = (jg0) obj;
                    this.e.remove(jg02);
                    jg02.a((RuntimeException) new UnsupportedApiCallException(b));
                }
            }
        }

        @DexIgnore
        public final void a(Status status) {
            ck0.a(we0.this.m);
            for (jg0 a : this.e) {
                a.a(status);
            }
            this.e.clear();
        }

        @DexIgnore
        public final boolean a(boolean z) {
            ck0.a(we0.this.m);
            if (!this.f.c() || this.k.size() != 0) {
                return false;
            }
            if (this.i.a()) {
                if (z) {
                    p();
                }
                return false;
            }
            this.f.a();
            return true;
        }

        @DexIgnore
        public final void a() {
            ck0.a(we0.this.m);
            if (!this.f.c() && !this.f.e()) {
                int a = we0.this.f.a(we0.this.d, this.f);
                if (a != 0) {
                    a(new vd0(a, (PendingIntent) null));
                    return;
                }
                c cVar = new c(this.f, this.h);
                if (this.f.l()) {
                    this.m.a((lh0) cVar);
                }
                this.f.a((jj0.c) cVar);
            }
        }

        @DexIgnore
        public final void a(bi0 bi0) {
            ck0.a(we0.this.m);
            this.j.add(bi0);
        }

        @DexIgnore
        public final xd0 a(xd0[] xd0Arr) {
            if (!(xd0Arr == null || xd0Arr.length == 0)) {
                xd0[] j2 = this.f.j();
                if (j2 == null) {
                    j2 = new xd0[0];
                }
                g4 g4Var = new g4(j2.length);
                for (xd0 xd0 : j2) {
                    g4Var.put(xd0.H(), Long.valueOf(xd0.I()));
                }
                for (xd0 xd02 : xd0Arr) {
                    if (!g4Var.containsKey(xd02.H()) || ((Long) g4Var.get(xd02.H())).longValue() < xd02.I()) {
                        return xd02;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public final void a(b bVar) {
            if (!this.o.contains(bVar) || this.n) {
                return;
            }
            if (!this.f.c()) {
                a();
            } else {
                j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ zh0<?> a;
        @DexIgnore
        public /* final */ xd0 b;

        @DexIgnore
        public b(zh0<?> zh0, xd0 xd0) {
            this.a = zh0;
            this.b = xd0;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                if (!ak0.a(this.a, bVar.a) || !ak0.a(this.b, bVar.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return ak0.a(this.a, this.b);
        }

        @DexIgnore
        public final String toString() {
            ak0.a a2 = ak0.a((Object) this);
            a2.a("key", this.a);
            a2.a("feature", this.b);
            return a2.toString();
        }

        @DexIgnore
        public /* synthetic */ b(zh0 zh0, xd0 xd0, sg0 sg0) {
            this(zh0, xd0);
        }
    }

    @DexIgnore
    public final void b(nf0 nf0) {
        synchronized (p) {
            if (this.j == nf0) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final void a(ge0<?> ge0) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, ge0));
    }

    @DexIgnore
    public final void a(nf0 nf0) {
        synchronized (p) {
            if (this.j != nf0) {
                this.j = nf0;
                this.k.clear();
            }
            this.k.addAll(nf0.h());
        }
    }

    @DexIgnore
    public final boolean b(vd0 vd0, int i2) {
        return this.e.a(this.d, vd0, i2);
    }

    @DexIgnore
    public final xn1<Map<zh0<?>, String>> a(Iterable<? extends ge0<?>> iterable) {
        bi0 bi0 = new bi0(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, bi0));
        return bi0.a();
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final <O extends ee0.d> void a(ge0<O> ge0, int i2, ue0<? extends ne0, ee0.b> ue0) {
        vh0 vh0 = new vh0(i2, ue0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new eh0(vh0, this.h.get(), ge0)));
    }

    @DexIgnore
    public final <O extends ee0.d, ResultT> void a(ge0<O> ge0, int i2, gf0<ee0.b, ResultT> gf0, yn1<ResultT> yn1, ef0 ef0) {
        xh0 xh0 = new xh0(i2, gf0, yn1, ef0);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new eh0(xh0, this.h.get(), ge0)));
    }

    @DexIgnore
    public final <O extends ee0.d> xn1<Void> a(ge0<O> ge0, cf0<ee0.b, ?> cf0, if0<ee0.b, ?> if0) {
        yn1 yn1 = new yn1();
        wh0 wh0 = new wh0(new fh0(cf0, if0), yn1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new eh0(wh0, this.h.get(), ge0)));
        return yn1.a();
    }

    @DexIgnore
    public final <O extends ee0.d> xn1<Boolean> a(ge0<O> ge0, af0.a<?> aVar) {
        yn1 yn1 = new yn1();
        yh0 yh0 = new yh0(aVar, yn1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new eh0(yh0, this.h.get(), ge0)));
        return yn1.a();
    }

    @DexIgnore
    public final PendingIntent a(zh0<?> zh0, int i2) {
        a aVar = this.i.get(zh0);
        if (aVar == null) {
            return null;
        }
        mn1 r = aVar.r();
        if (r == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, r.k(), 134217728);
    }

    @DexIgnore
    public final void a(vd0 vd0, int i2) {
        if (!b(vd0, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, vd0));
        }
    }
}
