package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.google.android.gms.common.api.AvailabilityException;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jf0 implements sn1<Map<zh0<?>, String>> {
    @DexIgnore
    public df0 a;
    @DexIgnore
    public /* final */ /* synthetic */ oi0 b;

    @DexIgnore
    public jf0(oi0 oi0, df0 df0) {
        this.b = oi0;
        this.a = df0;
    }

    @DexIgnore
    public final void a() {
        this.a.onComplete();
    }

    @DexIgnore
    public final void onComplete(xn1<Map<zh0<?>, String>> xn1) {
        this.b.j.lock();
        try {
            if (!this.b.r) {
                this.a.onComplete();
                return;
            }
            if (xn1.e()) {
                Map unused = this.b.t = new g4(this.b.f.size());
                for (ni0 h : this.b.f.values()) {
                    this.b.t.put(h.h(), vd0.i);
                }
            } else if (xn1.a() instanceof AvailabilityException) {
                AvailabilityException availabilityException = (AvailabilityException) xn1.a();
                if (this.b.p) {
                    Map unused2 = this.b.t = new g4(this.b.f.size());
                    for (ni0 ni0 : this.b.f.values()) {
                        zh0 h2 = ni0.h();
                        vd0 connectionResult = availabilityException.getConnectionResult(ni0);
                        if (this.b.a((ni0<?>) ni0, connectionResult)) {
                            this.b.t.put(h2, new vd0(16));
                        } else {
                            this.b.t.put(h2, connectionResult);
                        }
                    }
                } else {
                    Map unused3 = this.b.t = availabilityException.zaj();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", xn1.a());
                Map unused4 = this.b.t = Collections.emptyMap();
            }
            if (this.b.c()) {
                this.b.s.putAll(this.b.t);
                if (this.b.k() == null) {
                    this.b.i();
                    this.b.j();
                    this.b.m.signalAll();
                }
            }
            this.a.onComplete();
            this.b.j.unlock();
        } finally {
            this.b.j.unlock();
        }
    }
}
