package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hi0 implements he0.b, he0.c {
    @DexIgnore
    public /* final */ ee0<?> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public ii0 g;

    @DexIgnore
    public hi0(ee0<?> ee0, boolean z) {
        this.e = ee0;
        this.f = z;
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        a();
        this.g.a(vd0, this.e, this.f);
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        a();
        this.g.e(bundle);
    }

    @DexIgnore
    public final void f(int i) {
        a();
        this.g.f(i);
    }

    @DexIgnore
    public final void a(ii0 ii0) {
        this.g = ii0;
    }

    @DexIgnore
    public final void a() {
        ck0.a(this.g, (Object) "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
