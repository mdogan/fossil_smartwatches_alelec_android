package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xx0 extends qy0 {
    @DexIgnore
    public xx0() {
    }

    @DexIgnore
    public /* synthetic */ xx0(ex0 ex0) {
        this();
    }

    @DexIgnore
    public final void a(Status status, long j) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(Status status, qd0 qd0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(Status status, sd0[] sd0Arr) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(DataHolder dataHolder) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void b(Status status, long j) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void b(Status status, qd0 qd0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void g(Status status) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void h(Status status) {
        throw new UnsupportedOperationException();
    }
}
