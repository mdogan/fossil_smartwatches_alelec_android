package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class om4 implements vl4 {
    @DexIgnore
    public /* final */ OkHttpClient e;
    @DexIgnore
    public /* final */ sn4 f;
    @DexIgnore
    public /* final */ to4 g; // = new a();
    @DexIgnore
    public hm4 h;
    @DexIgnore
    public /* final */ pm4 i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public boolean k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends to4 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void i() {
            om4.this.cancel();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends um4 {
        @DexIgnore
        public /* final */ wl4 f;

        /*
        static {
            Class<om4> cls = om4.class;
        }
        */

        @DexIgnore
        public b(wl4 wl4) {
            super("OkHttp %s", om4.this.c());
            this.f = wl4;
        }

        @DexIgnore
        public void a(ExecutorService executorService) {
            try {
                executorService.execute(this);
            } catch (RejectedExecutionException e) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                interruptedIOException.initCause(e);
                om4.this.h.a((vl4) om4.this, (IOException) interruptedIOException);
                this.f.onFailure(om4.this, interruptedIOException);
                om4.this.e.h().b(this);
            } catch (Throwable th) {
                om4.this.e.h().b(this);
                throw th;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004a A[Catch:{ all -> 0x003d }] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006a A[Catch:{ all -> 0x003d }] */
        public void b() {
            IOException e;
            om4.this.g.g();
            boolean z = true;
            try {
                Response b = om4.this.b();
                if (om4.this.f.b()) {
                    try {
                        this.f.onFailure(om4.this, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                        try {
                            IOException a = om4.this.a(e);
                            if (!z) {
                                mo4 d = mo4.d();
                                d.a(4, "Callback failure for " + om4.this.d(), (Throwable) a);
                            } else {
                                om4.this.h.a((vl4) om4.this, a);
                                this.f.onFailure(om4.this, a);
                            }
                            om4.this.e.h().b(this);
                        } catch (Throwable th) {
                            om4.this.e.h().b(this);
                            throw th;
                        }
                    }
                } else {
                    this.f.onResponse(om4.this, b);
                }
            } catch (IOException e3) {
                e = e3;
                z = false;
                IOException a2 = om4.this.a(e);
                if (!z) {
                }
                om4.this.e.h().b(this);
            }
            om4.this.e.h().b(this);
        }

        @DexIgnore
        public om4 c() {
            return om4.this;
        }

        @DexIgnore
        public String d() {
            return om4.this.i.g().g();
        }
    }

    @DexIgnore
    public om4(OkHttpClient okHttpClient, pm4 pm4, boolean z) {
        this.e = okHttpClient;
        this.i = pm4;
        this.j = z;
        this.f = new sn4(okHttpClient, z);
        this.g.a((long) okHttpClient.b(), TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public Response b() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.e.w());
        arrayList.add(this.f);
        arrayList.add(new jn4(this.e.g()));
        arrayList.add(new xm4(this.e.x()));
        arrayList.add(new dn4(this.e));
        if (!this.j) {
            arrayList.addAll(this.e.y());
        }
        arrayList.add(new kn4(this.j));
        return new pn4(arrayList, (in4) null, (ln4) null, (fn4) null, 0, this.i, this, this.h, this.e.d(), this.e.E(), this.e.I()).a(this.i);
    }

    @DexIgnore
    public String c() {
        return this.i.g().m();
    }

    @DexIgnore
    public void cancel() {
        this.f.a();
    }

    @DexIgnore
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append(o() ? "canceled " : "");
        sb.append(this.j ? "web socket" : "call");
        sb.append(" to ");
        sb.append(c());
        return sb.toString();
    }

    @DexIgnore
    public pm4 n() {
        return this.i;
    }

    @DexIgnore
    public boolean o() {
        return this.f.b();
    }

    @DexIgnore
    public Response r() throws IOException {
        synchronized (this) {
            if (!this.k) {
                this.k = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.g.g();
        this.h.b(this);
        try {
            this.e.h().a(this);
            Response b2 = b();
            if (b2 != null) {
                this.e.h().b(this);
                return b2;
            }
            throw new IOException("Canceled");
        } catch (IOException e2) {
            IOException a2 = a(e2);
            this.h.a((vl4) this, a2);
            throw a2;
        } catch (Throwable th) {
            this.e.h().b(this);
            throw th;
        }
    }

    @DexIgnore
    public static om4 a(OkHttpClient okHttpClient, pm4 pm4, boolean z) {
        om4 om4 = new om4(okHttpClient, pm4, z);
        om4.h = okHttpClient.j().a(om4);
        return om4;
    }

    @DexIgnore
    public om4 clone() {
        return a(this.e, this.i, this.j);
    }

    @DexIgnore
    public IOException a(IOException iOException) {
        if (!this.g.h()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final void a() {
        this.f.a(mo4.d().a("response.body().close()"));
    }

    @DexIgnore
    public void a(wl4 wl4) {
        synchronized (this) {
            if (!this.k) {
                this.k = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.h.b(this);
        this.e.h().a(new b(wl4));
    }
}
