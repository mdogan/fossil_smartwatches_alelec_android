package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g52 implements Factory<sc> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public g52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static g52 a(o42 o42) {
        return new g52(o42);
    }

    @DexIgnore
    public static sc b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static sc c(o42 o42) {
        sc i = o42.i();
        o44.a(i, "Cannot return null from a non-@Nullable @Provides method");
        return i;
    }

    @DexIgnore
    public sc get() {
        return b(this.a);
    }
}
