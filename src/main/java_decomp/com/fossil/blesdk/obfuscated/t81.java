package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t81 implements w91 {
    @DexIgnore
    public static /* final */ t81 a; // = new t81();

    @DexIgnore
    public static t81 a() {
        return a;
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        return u81.class.isAssignableFrom(cls);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final v91 a(Class<?> r5) {
        Class cls = u81.class;
        if (!cls.isAssignableFrom(r5)) {
            String valueOf = String.valueOf(r5.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (v91) u81.a(r5.asSubclass(cls)).a(u81.e.c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(r5.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
