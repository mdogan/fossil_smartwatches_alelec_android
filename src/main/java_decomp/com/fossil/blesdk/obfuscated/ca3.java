package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ca3 implements Factory<DashboardCaloriesPresenter> {
    @DexIgnore
    public static DashboardCaloriesPresenter a(aa3 aa3, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42, yk2 yk2) {
        return new DashboardCaloriesPresenter(aa3, summariesRepository, fitnessDataRepository, activitySummaryDao, fitnessDatabase, userRepository, i42, yk2);
    }
}
