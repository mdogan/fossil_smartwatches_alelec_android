package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zk4 extends al4 {
    @DexIgnore
    public static /* final */ gh4 j;
    @DexIgnore
    public static /* final */ zk4 k;

    /*
    static {
        zk4 zk4 = new zk4();
        k = zk4;
        j = zk4.b(sk4.a("kotlinx.coroutines.io.parallelism", qe4.a(64, qk4.a()), 0, 0, 12, (Object) null));
    }
    */

    @DexIgnore
    public zk4() {
        super(0, 0, (String) null, 7, (rd4) null);
    }

    @DexIgnore
    public final gh4 D() {
        return j;
    }

    @DexIgnore
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    public String toString() {
        return "DefaultDispatcher";
    }
}
