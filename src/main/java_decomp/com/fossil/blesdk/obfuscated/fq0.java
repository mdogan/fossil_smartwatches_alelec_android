package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fq0 implements Parcelable.Creator<bq0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        fp0 fp0 = null;
        ArrayList<DataSet> arrayList = null;
        ArrayList<DataPoint> arrayList2 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                fp0 = (fp0) SafeParcelReader.a(parcel, a, fp0.CREATOR);
            } else if (a2 == 2) {
                arrayList = SafeParcelReader.c(parcel, a, DataSet.CREATOR);
            } else if (a2 == 3) {
                arrayList2 = SafeParcelReader.c(parcel, a, DataPoint.CREATOR);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                iBinder = SafeParcelReader.p(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new bq0(fp0, (List<DataSet>) arrayList, (List<DataPoint>) arrayList2, iBinder);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new bq0[i];
    }
}
