package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.l84;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o84 implements l84.a {
    @DexIgnore
    public /* final */ List<l84> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ j84 c;

    @DexIgnore
    public o84(List<? extends l84> list, int i, j84 j84) {
        wd4.b(list, "interceptors");
        wd4.b(j84, "request");
        this.a = list;
        this.b = i;
        this.c = j84;
    }

    @DexIgnore
    public k84 a(j84 j84) {
        wd4.b(j84, "request");
        if (this.b < this.a.size()) {
            return this.a.get(this.b).intercept(new o84(this.a, this.b + 1, j84));
        }
        throw new AssertionError("no interceptors added to the chain");
    }

    @DexIgnore
    public j84 n() {
        return this.c;
    }
}
