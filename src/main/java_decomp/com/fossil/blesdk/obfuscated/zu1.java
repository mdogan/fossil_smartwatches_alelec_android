package com.fossil.blesdk.obfuscated;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zu1<T> extends Iterable<T> {
    @DexIgnore
    Comparator<? super T> comparator();
}
