package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qn1<TResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(xn1<TResult> xn1) throws Exception;
}
