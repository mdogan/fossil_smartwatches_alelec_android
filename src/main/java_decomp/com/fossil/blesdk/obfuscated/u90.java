package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.log.debuglog.LogLevel;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u90 {
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<v90> a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static LogLevel[] b; // = new LogLevel[0];
    @DexIgnore
    public static /* final */ u90 c; // = new u90();

    @DexIgnore
    public final void a(int i, String str, String str2) {
        wd4.b(str, "tag");
        wd4.b(str2, "logContent");
        long currentTimeMillis = System.currentTimeMillis();
        LogLevel a2 = LogLevel.Companion.a(i);
        if (a2 != null && lb4.b((T[]) b, a2)) {
            for (v90 a3 : a) {
                a3.a(a2, str, str2, currentTimeMillis);
            }
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        wd4.b(str, "tag");
        wd4.b(str2, "logContent");
        a(6, str, str2);
    }

    @DexIgnore
    public final void c(String str, String str2) {
        wd4.b(str, "tag");
        wd4.b(str2, "logContent");
        a(4, str, str2);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wd4.b(str, "tag");
        wd4.b(str2, "logContent");
        a(3, str, str2);
    }
}
