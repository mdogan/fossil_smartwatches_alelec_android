package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ti3 implements Factory<ri3> {
    @DexIgnore
    public static ri3 a(si3 si3) {
        ri3 a = si3.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
