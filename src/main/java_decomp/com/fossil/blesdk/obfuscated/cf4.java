package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cf4<T> implements df4<T> {
    @DexIgnore
    public /* final */ id4<T> a;
    @DexIgnore
    public /* final */ jd4<T, T> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, de4 {
        @DexIgnore
        public T e;
        @DexIgnore
        public int f; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ cf4 g;

        @DexIgnore
        public a(cf4 cf4) {
            this.g = cf4;
        }

        @DexIgnore
        public final void a() {
            T t;
            if (this.f == -2) {
                t = this.g.a.invoke();
            } else {
                jd4 b = this.g.b;
                T t2 = this.e;
                if (t2 != null) {
                    t = b.invoke(t2);
                } else {
                    wd4.a();
                    throw null;
                }
            }
            this.e = t;
            this.f = this.e == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.f < 0) {
                a();
            }
            return this.f == 1;
        }

        @DexIgnore
        public T next() {
            if (this.f < 0) {
                a();
            }
            if (this.f != 0) {
                T t = this.e;
                if (t != null) {
                    this.f = -1;
                    return t;
                }
                throw new TypeCastException("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public cf4(id4<? extends T> id4, jd4<? super T, ? extends T> jd4) {
        wd4.b(id4, "getInitialValue");
        wd4.b(jd4, "getNextValue");
        this.a = id4;
        this.b = jd4;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
