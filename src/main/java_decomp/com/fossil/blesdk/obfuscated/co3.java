package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.xs2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Triple;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class co3 extends wn3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public /* final */ List<xs2.c> f; // = new ArrayList();
    @DexIgnore
    public /* final */ xn3 g;
    @DexIgnore
    public /* final */ ArrayList<Integer> h;
    @DexIgnore
    public /* final */ fn2 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = co3.class.getSimpleName();
        wd4.a((Object) simpleName, "PermissionPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public co3(xn3 xn3, ArrayList<Integer> arrayList, fn2 fn2) {
        wd4.b(xn3, "mView");
        wd4.b(arrayList, "mListPerms");
        wd4.b(fn2, "mSharedPreferencesManager");
        this.g = xn3;
        this.h = arrayList;
        this.i = fn2;
    }

    @DexIgnore
    public void f() {
        Context context;
        FLogger.INSTANCE.getLocal().d(j, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xn3 xn3 = this.g;
        if (xn3 instanceof yn3) {
            context = ((yn3) xn3).getContext();
        } else if (xn3 != null) {
            context = ((tn3) xn3).getContext();
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.AllowNotificationServiceFragment");
        }
        if (this.f.isEmpty()) {
            for (Number intValue : this.h) {
                int intValue2 = intValue.intValue();
                Triple<String, String, String> b = cn2.d.b(intValue2);
                this.f.add(new xs2.c(intValue2, b.component1(), b.component2(), cn2.d.a(intValue2), b.component3(), cn2.d.a(context, intValue2)));
            }
        } else {
            for (xs2.c cVar : this.f) {
                cVar.a(cn2.d.a(context, cVar.d()));
            }
        }
        List<xs2.c> list = this.f;
        int i2 = 0;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (xs2.c b2 : list) {
                if (b2.b()) {
                    i2++;
                    if (i2 < 0) {
                        ob4.b();
                        throw null;
                    }
                }
            }
        }
        if (i2 == this.f.size()) {
            this.g.close();
        } else {
            this.g.t(this.f);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "stop");
    }

    @DexIgnore
    public void h() {
        this.i.q(false);
        this.g.close();
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }
}
