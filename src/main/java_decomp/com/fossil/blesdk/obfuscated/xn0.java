package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.sn0;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xn0 implements wn0<T> {
    @DexIgnore
    public /* final */ /* synthetic */ sn0 a;

    @DexIgnore
    public xn0(sn0 sn0) {
        this.a = sn0;
    }

    @DexIgnore
    public final void a(T t) {
        un0 unused = this.a.a = t;
        Iterator it = this.a.c.iterator();
        while (it.hasNext()) {
            ((sn0.a) it.next()).a(this.a.a);
        }
        this.a.c.clear();
        Bundle unused2 = this.a.b = null;
    }
}
