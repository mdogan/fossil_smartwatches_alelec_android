package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.qf;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wf<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ RoomDatabase k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ pf n;
    @DexIgnore
    public /* final */ qf.c o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public /* final */ Runnable t; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (wf.this.r.compareAndSet(false, true)) {
                wf.this.k.getInvalidationTracker().b(wf.this.o);
            }
            do {
                if (wf.this.q.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (wf.this.p.compareAndSet(true, false)) {
                        try {
                            obj = wf.this.m.call();
                            z = true;
                        } catch (Exception e2) {
                            throw new RuntimeException("Exception while computing database live data.", e2);
                        } catch (Throwable th) {
                            wf.this.q.set(false);
                            throw th;
                        }
                    }
                    if (z) {
                        wf.this.a(obj);
                    }
                    wf.this.q.set(false);
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (wf.this.p.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean c = wf.this.c();
            if (wf.this.p.compareAndSet(false, true) && c) {
                wf.this.f().execute(wf.this.s);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends qf.c {
        @DexIgnore
        public c(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            h3.c().b(wf.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public wf(RoomDatabase roomDatabase, pf pfVar, boolean z, Callable<T> callable, String[] strArr) {
        this.k = roomDatabase;
        this.l = z;
        this.m = callable;
        this.n = pfVar;
        this.o = new c(strArr);
    }

    @DexIgnore
    public void d() {
        super.d();
        this.n.a(this);
        f().execute(this.s);
    }

    @DexIgnore
    public void e() {
        super.e();
        this.n.b(this);
    }

    @DexIgnore
    public Executor f() {
        if (this.l) {
            return this.k.getTransactionExecutor();
        }
        return this.k.getQueryExecutor();
    }
}
