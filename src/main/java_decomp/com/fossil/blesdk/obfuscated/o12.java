package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o12 {
    @DexIgnore
    public static /* final */ String[] a; // = new String[0];

    @DexIgnore
    public static int a(int i) {
        return i >>> 3;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    @DexIgnore
    public static final int a(k12 k12, int i) throws IOException {
        int a2 = k12.a();
        k12.f(i);
        int i2 = 1;
        while (k12.j() == i) {
            k12.f(i);
            i2++;
        }
        k12.e(a2);
        return i2;
    }

    @DexIgnore
    public static int b(int i) {
        return i & 7;
    }

    @DexIgnore
    public static boolean b(k12 k12, int i) throws IOException {
        return k12.f(i);
    }
}
