package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class id3 implements Factory<SleepOverviewDayPresenter> {
    @DexIgnore
    public static SleepOverviewDayPresenter a(gd3 gd3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        return new SleepOverviewDayPresenter(gd3, sleepSummariesRepository, sleepSessionsRepository);
    }
}
