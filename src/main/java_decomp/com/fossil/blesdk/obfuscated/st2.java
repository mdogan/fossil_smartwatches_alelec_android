package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class st2 extends ut2<c> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public /* final */ ArrayList<String> l; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> m; // = new ArrayList<>();
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public b p;
    @DexIgnore
    public AlphabetIndexer q;
    @DexIgnore
    public /* final */ List<ContactWrapper> r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ ph2 h;
        @DexIgnore
        public /* final */ /* synthetic */ st2 i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.e.b();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public b(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.e.b();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(st2 st2, ph2 ph2) {
            super(ph2.d());
            wd4.b(ph2, "binding");
            this.i = st2;
            this.h = ph2;
            this.h.r.setOnClickListener(new a(this));
            this.h.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void b() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.r.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((ContactWrapper) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    this.i.r.remove(i2);
                } else {
                    this.i.r.add(a());
                }
                b c2 = this.i.p;
                if (c2 != null) {
                    c2.a();
                }
                this.i.notifyItemChanged(adapterPosition);
            }
        }

        @DexIgnore
        public final void a(Cursor cursor, int i2) {
            Object obj;
            boolean z;
            wd4.b(cursor, "cursor");
            cursor.moveToPosition(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c2 = st2.s;
            local.d(c2, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            boolean z2 = true;
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            if (this.i.o < i2) {
                this.i.o = i2;
            }
            String b2 = us3.b(this.c);
            if (this.i.m.contains(Integer.valueOf(i2)) || (this.i.o < i2 && this.i.n == this.e && this.i.l.contains(b2))) {
                if (!this.i.m.contains(Integer.valueOf(i2))) {
                    this.i.m.add(Integer.valueOf(i2));
                }
                a(8);
            } else {
                if (i2 > this.i.o) {
                    this.i.o = i2;
                }
                if (this.i.n != this.e) {
                    this.i.l.clear();
                    this.i.n = this.e;
                }
                this.i.l.add(b2);
                a(0);
                if (cursor.moveToPrevious() && cursor.getInt(cursor.getColumnIndex("contact_id")) == this.e) {
                    AppCompatCheckBox appCompatCheckBox = this.h.q;
                    wd4.a((Object) appCompatCheckBox, "binding.accbSelect");
                    appCompatCheckBox.setVisibility(4);
                    FlexibleTextView flexibleTextView = this.h.w;
                    wd4.a((Object) flexibleTextView, "binding.pickContactTitle");
                    flexibleTextView.setVisibility(8);
                }
                cursor.moveToNext();
            }
            FlexibleTextView flexibleTextView2 = this.h.w;
            wd4.a((Object) flexibleTextView2, "binding.pickContactTitle");
            flexibleTextView2.setText(this.a);
            if (this.f == 1) {
                FlexibleTextView flexibleTextView3 = this.h.v;
                wd4.a((Object) flexibleTextView3, "binding.pickContactPhone");
                flexibleTextView3.setText(this.c);
                FlexibleTextView flexibleTextView4 = this.h.v;
                wd4.a((Object) flexibleTextView4, "binding.pickContactPhone");
                flexibleTextView4.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView5 = this.h.v;
                wd4.a((Object) flexibleTextView5, "binding.pickContactPhone");
                flexibleTextView5.setVisibility(8);
            }
            Iterator it = this.i.r.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                Contact contact = ((ContactWrapper) obj).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            ContactWrapper contactWrapper = (ContactWrapper) obj;
            AppCompatCheckBox appCompatCheckBox2 = this.h.q;
            wd4.a((Object) appCompatCheckBox2, "binding.accbSelect");
            if (contactWrapper == null) {
                z2 = false;
            }
            appCompatCheckBox2.setChecked(z2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String c3 = st2.s;
            local2.d(c3, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ',' + " phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                FlexibleTextView flexibleTextView6 = this.h.t;
                wd4.a((Object) flexibleTextView6, "binding.ftvAlphabet");
                flexibleTextView6.setVisibility(0);
                FlexibleTextView flexibleTextView7 = this.h.t;
                wd4.a((Object) flexibleTextView7, "binding.ftvAlphabet");
                flexibleTextView7.setText(Character.toString(ml2.b.a(this.d)));
                return;
            }
            FlexibleTextView flexibleTextView8 = this.h.t;
            wd4.a((Object) flexibleTextView8, "binding.ftvAlphabet");
            flexibleTextView8.setVisibility(8);
        }

        @DexIgnore
        public final void a(int i2) {
            AppCompatCheckBox appCompatCheckBox = this.h.q;
            wd4.a((Object) appCompatCheckBox, "binding.accbSelect");
            appCompatCheckBox.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            wd4.a((Object) constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView = this.h.t;
            wd4.a((Object) flexibleTextView, "binding.ftvAlphabet");
            flexibleTextView.setVisibility(i2);
            LinearLayout linearLayout = this.h.u;
            wd4.a((Object) linearLayout, "binding.llTextContainer");
            linearLayout.setVisibility(i2);
            FlexibleTextView flexibleTextView2 = this.h.v;
            wd4.a((Object) flexibleTextView2, "binding.pickContactPhone");
            flexibleTextView2.setVisibility(i2);
            FlexibleTextView flexibleTextView3 = this.h.w;
            wd4.a((Object) flexibleTextView3, "binding.pickContactTitle");
            flexibleTextView3.setVisibility(i2);
            View view = this.h.x;
            wd4.a((Object) view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.s;
            wd4.a((Object) constraintLayout2, "binding.clRoot");
            constraintLayout2.setVisibility(i2);
        }

        @DexIgnore
        public final ContactWrapper a() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            ContactWrapper contactWrapper = new ContactWrapper(contact, (String) null, 2, (rd4) null);
            if (this.f == 1) {
                contactWrapper.setHasPhoneNumber(true);
                contactWrapper.setPhoneNumber(this.c);
            } else {
                contactWrapper.setHasPhoneNumber(false);
            }
            Contact contact2 = contactWrapper.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
                Contact contact3 = contactWrapper.getContact();
                if (contact3 != null) {
                    contact3.setUseCall(true);
                    contactWrapper.setFavorites(this.g);
                    contactWrapper.setAdded(true);
                    return contactWrapper;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = st2.class.getSimpleName();
        wd4.a((Object) simpleName, "CursorContactSearchAdapter::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public st2(Cursor cursor, List<ContactWrapper> list) {
        super(cursor);
        wd4.b(list, "mContactWrapperList");
        this.r = list;
    }

    @DexIgnore
    public int getPositionForSection(int i) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i);
            }
            wd4.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i);
            }
            wd4.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        ph2 a2 = ph2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wd4.a((Object) a2, "ItemContactBinding.infla\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        this.q = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRTSUVWXYZ");
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.l.clear();
            this.m.clear();
            this.n = -1;
            this.o = -1;
            return super.c(cursor);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(c cVar, Cursor cursor, int i) {
        wd4.b(cVar, "holder");
        if (cursor != null) {
            cVar.a(cursor, i);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "itemClickListener");
        this.p = bVar;
    }
}
