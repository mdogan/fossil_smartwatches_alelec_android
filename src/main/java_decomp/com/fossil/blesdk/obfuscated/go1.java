package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class go1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ fo1 f;

    @DexIgnore
    public go1(fo1 fo1, xn1 xn1) {
        this.f = fo1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        try {
            xn1 xn1 = (xn1) this.f.b.then(this.e);
            if (xn1 == null) {
                this.f.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            xn1.a(zn1.b, this.f);
            xn1.a(zn1.b, (tn1) this.f);
            xn1.a(zn1.b, (rn1) this.f);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.c.a((Exception) e2.getCause());
            } else {
                this.f.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.f.c.a(e3);
        }
    }
}
