package com.fossil.blesdk.obfuscated;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface t1 {
    @DexIgnore
    void c();

    @DexIgnore
    boolean d();

    @DexIgnore
    void dismiss();

    @DexIgnore
    ListView e();
}
