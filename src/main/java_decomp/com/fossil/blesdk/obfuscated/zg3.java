package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zg3 extends ug3 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ vg3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = zg3.class.getSimpleName();
        wd4.a((Object) simpleName, "AboutPresenter::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public zg3(vg3 vg3) {
        wd4.b(vg3, "mView");
        this.f = vg3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(g, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(g, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }
}
