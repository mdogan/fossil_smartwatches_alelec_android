package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g62 {
    @DexIgnore
    public static /* final */ Properties a; // = x.a(PortfolioApp.W.c());
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return g62.b;
        }

        @DexIgnore
        public final String b() {
            return g62.c;
        }

        @DexIgnore
        public final String c() {
            return g62.s;
        }

        @DexIgnore
        public final String d() {
            return g62.w;
        }

        @DexIgnore
        public final String e() {
            return g62.v;
        }

        @DexIgnore
        public final String f() {
            return g62.t;
        }

        @DexIgnore
        public final String g() {
            return g62.k;
        }

        @DexIgnore
        public final String h() {
            return g62.l;
        }

        @DexIgnore
        public final String i() {
            return g62.m;
        }

        @DexIgnore
        public final String j() {
            return g62.g;
        }

        @DexIgnore
        public final String k() {
            return g62.h;
        }

        @DexIgnore
        public final String l() {
            return g62.i;
        }

        @DexIgnore
        public final String m() {
            return g62.n;
        }

        @DexIgnore
        public final String n() {
            return g62.j;
        }

        @DexIgnore
        public final String o() {
            return g62.p;
        }

        @DexIgnore
        public final String p() {
            return g62.o;
        }

        @DexIgnore
        public final String q() {
            return g62.u;
        }

        @DexIgnore
        public final String r() {
            return g62.d;
        }

        @DexIgnore
        public final String s() {
            return g62.q;
        }

        @DexIgnore
        public final String t() {
            return g62.e;
        }

        @DexIgnore
        public final String u() {
            return g62.f;
        }

        @DexIgnore
        public final String v() {
            return g62.r;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final Properties a(Context context) {
            wd4.b(context, "context");
            Properties properties = new Properties();
            int hashCode = "release".hashCode();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open((hashCode == -1897523141 || hashCode != 1090594823) ? "debug.properties" : "production.properties"), "UTF-8"));
                properties.load(bufferedReader);
                bufferedReader.close();
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("XXX", "Exception when load properties=" + e);
            }
            return properties;
        }
    }

    /*
    static {
        String property = a.getProperty("APP_CODE");
        wd4.a((Object) property, "config.getProperty(\"APP_CODE\")");
        b = property;
        String property2 = a.getProperty("BRAND_ID");
        wd4.a((Object) property2, "config.getProperty(\"BRAND_ID\")");
        c = property2;
        String property3 = a.getProperty("UA_REDIRECT");
        wd4.a((Object) property3, "config.getProperty(\"UA_REDIRECT\")");
        d = property3;
        String property4 = a.getProperty("WEIBO_REDIRECT_URL");
        wd4.a((Object) property4, "config.getProperty(\"WEIBO_REDIRECT_URL\")");
        e = property4;
        String property5 = a.getProperty("WEIBO_SCOPE");
        wd4.a((Object) property5, "config.getProperty(\"WEIBO_SCOPE\")");
        f = property5;
        String property6 = a.getProperty("MISFIT_API_BASE_URL_STAGING");
        wd4.a((Object) property6, "config.getProperty(\"MISFIT_API_BASE_URL_STAGING\")");
        g = property6;
        String property7 = a.getProperty("MISFIT_API_BASE_URL_STAGING_V2");
        wd4.a((Object) property7, "config.getProperty(\"MISF\u2026API_BASE_URL_STAGING_V2\")");
        h = property7;
        String property8 = a.getProperty("MISFIT_API_BASE_URL_STAGING_V2DOT1");
        wd4.a((Object) property8, "config.getProperty(\"MISF\u2026BASE_URL_STAGING_V2DOT1\")");
        i = property8;
        String property9 = a.getProperty("MISFIT_WEB_BASE_URL_STAGING");
        wd4.a((Object) property9, "config.getProperty(\"MISFIT_WEB_BASE_URL_STAGING\")");
        j = property9;
        String property10 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION");
        wd4.a((Object) property10, "config.getProperty(\"MISF\u2026API_BASE_URL_PRODUCTION\")");
        k = property10;
        String property11 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2");
        wd4.a((Object) property11, "config.getProperty(\"MISF\u2026_BASE_URL_PRODUCTION_V2\")");
        l = property11;
        String property12 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2DOT1");
        wd4.a((Object) property12, "config.getProperty(\"MISF\u2026E_URL_PRODUCTION_V2DOT1\")");
        m = property12;
        String property13 = a.getProperty("MISFIT_WEB_BASE_URL_PRODUCTION");
        wd4.a((Object) property13, "config.getProperty(\"MISF\u2026WEB_BASE_URL_PRODUCTION\")");
        n = property13;
        wd4.a((Object) a.getProperty("EMAIL_MAGIC_STAGING_STAGING"), "config.getProperty(\"EMAIL_MAGIC_STAGING_STAGING\")");
        wd4.a((Object) a.getProperty("EMAIL_MAGIC_STAGING_PRODUCTION"), "config.getProperty(\"EMAI\u2026AGIC_STAGING_PRODUCTION\")");
        wd4.a((Object) a.getProperty("EMAIL_MAGIC_PRODUCTION_STAGING"), "config.getProperty(\"EMAI\u2026AGIC_PRODUCTION_STAGING\")");
        wd4.a((Object) a.getProperty("EMAIL_MAGIC_PRODUCTION_PRODUCTION"), "config.getProperty(\"EMAI\u2026C_PRODUCTION_PRODUCTION\")");
        String property14 = a.getProperty("SDK_ENDPOINT_STAGING");
        wd4.a((Object) property14, "config.getProperty(\"SDK_ENDPOINT_STAGING\")");
        o = property14;
        String property15 = a.getProperty("SDK_ENDPOINT_PRODUCTION");
        wd4.a((Object) property15, "config.getProperty(\"SDK_ENDPOINT_PRODUCTION\")");
        p = property15;
        String property16 = a.getProperty("LIST_MICRO_APP_NOT_SUPPORTED");
        wd4.a((Object) property16, "config.getProperty(\"LIST_MICRO_APP_NOT_SUPPORTED\")");
        q = property16;
        String property17 = a.getProperty("ZENDESK_URL");
        wd4.a((Object) property17, "config.getProperty(\"ZENDESK_URL\")");
        r = property17;
        String property18 = a.getProperty("CLOUD_LOG_BASE_URL");
        wd4.a((Object) property18, "config.getProperty(\"CLOUD_LOG_BASE_URL\")");
        s = property18;
        String property19 = a.getProperty("LOG_BRAND_NAME");
        wd4.a((Object) property19, "config.getProperty(\"LOG_BRAND_NAME\")");
        t = property19;
        String property20 = a.getProperty("SDK_V2_LOG_END_POINT");
        wd4.a((Object) property20, "config.getProperty(\"SDK_V2_LOG_END_POINT\")");
        u = property20;
        String property21 = a.getProperty("GOOGLE_PROXY_BASE_URL_STAGING");
        wd4.a((Object) property21, "config.getProperty(\"GOOG\u2026_PROXY_BASE_URL_STAGING\")");
        v = property21;
        String property22 = a.getProperty("GOOGLE_PROXY_BASE_URL_PRODUCTION");
        wd4.a((Object) property22, "config.getProperty(\"GOOG\u2026OXY_BASE_URL_PRODUCTION\")");
        w = property22;
    }
    */
}
