package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rl2 {
    @DexIgnore
    public static /* final */ String g; // = "rl2";
    @DexIgnore
    public static rl2 h;
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public CalibrationEnums.HandId e;
    @DexIgnore
    public fn2 f;

    @DexIgnore
    public rl2() {
        PortfolioApp.R.g().a(this);
    }

    @DexIgnore
    public static synchronized rl2 b() {
        rl2 rl2;
        synchronized (rl2.class) {
            if (h == null) {
                h = new rl2();
            }
            rl2 = h;
        }
        return rl2;
    }

    @DexIgnore
    public boolean a(String str) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Cancel Calibration - serial=" + str);
            this.c.set(false);
            PortfolioApp.a0().deviceCancelCalibration(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean c(String str) {
        try {
            this.a = 0;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Enter Calibration - serial=" + str);
            PortfolioApp.a0().deviceStartCalibration(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public void d(String str) {
        try {
            PortfolioApp.a0().resetHandsToZeroDegree(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Error reset hands second timezone of serial=" + str + ", ex=" + e2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0077, code lost:
        return;
     */
    @DexIgnore
    @px3
    public void onMovingHandCompleted(gj2 gj2) {
        synchronized (this.c) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.e(str, "onMovingHandCompleted isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            if (this.c.get()) {
                if (gj2.b()) {
                    int currentTimeMillis = ((int) (((System.currentTimeMillis() - this.b) / 10) * 2)) % 360;
                    int i = currentTimeMillis >= this.a ? currentTimeMillis - this.a : (currentTimeMillis + 360) - this.a;
                    this.a = currentTimeMillis;
                    a(this.d.get(), gj2.a(), i, this.e);
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, int i, CalibrationEnums.HandId handId, CalibrationEnums.MovingType movingType, CalibrationEnums.Direction direction, CalibrationEnums.Speed speed) {
        try {
            PortfolioApp.a0().deviceMovingHand(str, new HandCalibrationObj(handId, movingType, direction, speed, i));
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Complete Calibration - serial=" + str);
            PortfolioApp.a0().deviceCompleteCalibration(str);
            this.c.set(false);
            this.f.a(str, true);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        return a(str, i, handId, CalibrationEnums.MovingType.DISTANCE, z ? CalibrationEnums.Direction.CLOCKWISE : CalibrationEnums.Direction.COUNTER_CLOCKWISE, CalibrationEnums.Speed.FULL);
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            this.c.set(false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "Stop smart movement isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            try {
                PortfolioApp.W.c(b());
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(g, "Exception when unregister receiver");
                e2.printStackTrace();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:5|6|7|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0022 */
    public void b(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        synchronized (this.c) {
            if (!this.c.get()) {
                FLogger.INSTANCE.getLocal().d(g, "Start smart movement");
                PortfolioApp.W.b((Object) b());
                FLogger.INSTANCE.getLocal().e(g, "Exception when register bus events");
                this.c.set(true);
                this.b = System.currentTimeMillis();
                this.a = 0;
                this.d.set(z);
                this.e = handId;
                a(z, str, i, handId);
            }
        }
    }
}
