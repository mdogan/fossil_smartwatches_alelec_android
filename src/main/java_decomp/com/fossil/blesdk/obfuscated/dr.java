package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ww;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dr {
    @DexIgnore
    public /* final */ rw<ko, String> a; // = new rw<>(1000);
    @DexIgnore
    public /* final */ h8<b> b; // = ww.a(10, new a(this));

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ww.d<b> {
        @DexIgnore
        public a(dr drVar) {
        }

        @DexIgnore
        public b a() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ww.f {
        @DexIgnore
        public /* final */ MessageDigest e;
        @DexIgnore
        public /* final */ yw f; // = yw.b();

        @DexIgnore
        public b(MessageDigest messageDigest) {
            this.e = messageDigest;
        }

        @DexIgnore
        public yw i() {
            return this.f;
        }
    }

    @DexIgnore
    public final String a(ko koVar) {
        b a2 = this.b.a();
        uw.a(a2);
        b bVar = a2;
        try {
            koVar.a(bVar.e);
            return vw.a(bVar.e.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    @DexIgnore
    public String b(ko koVar) {
        String a2;
        synchronized (this.a) {
            a2 = this.a.a(koVar);
        }
        if (a2 == null) {
            a2 = a(koVar);
        }
        synchronized (this.a) {
            this.a.b(koVar, a2);
        }
        return a2;
    }
}
