package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PinObject;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class or3 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public ActivitiesRepository a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = or3.class.getSimpleName();
        wd4.a((Object) simpleName, "MigrateDataActivitiesAnd\u2026ps::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public or3() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final void a() {
        if (!TextUtils.isEmpty(ks3.a().f(PortfolioApp.W.c()))) {
            ao2 j = en2.p.a().j();
            List<PinObject> b2 = j.b("FitnessSample");
            ArrayList arrayList = new ArrayList();
            for (PinObject next : b2) {
                Gson gson = new Gson();
                wd4.a((Object) next, "pin");
                SampleRaw sampleRaw = (SampleRaw) gson.a(next.getJsonData(), SampleRaw.class);
                if (sampleRaw != null) {
                    arrayList.add(sampleRaw);
                }
                j.a(next);
            }
            ActivitiesRepository activitiesRepository = this.a;
            if (activitiesRepository != null) {
                activitiesRepository.updateActivityPinType(arrayList, 1);
                List<PinObject> b3 = j.b("MFSleepSessionUpload");
                ArrayList arrayList2 = new ArrayList();
                for (PinObject next2 : b3) {
                    sz1 sz1 = new sz1();
                    sz1.a(DateTime.class, new GsonConvertDateTime());
                    Gson a2 = sz1.a();
                    wd4.a((Object) next2, "pin");
                    MFSleepSession mFSleepSession = (MFSleepSession) a2.a(next2.getJsonData(), MFSleepSession.class);
                    if (mFSleepSession != null) {
                        arrayList2.add(mFSleepSession);
                    }
                    j.a(next2);
                }
                MFSleepSessionProvider m = en2.p.a().m();
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    MFSleepSession mFSleepSession2 = (MFSleepSession) it.next();
                    wd4.a((Object) mFSleepSession2, "sleepSession");
                    mFSleepSession2.setPinType(1);
                    m.updateSleepSessionPinType(mFSleepSession2, 1);
                }
                return;
            }
            wd4.d("mActivityRepository");
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(b, "There is no user here. No need to migrate");
    }
}
