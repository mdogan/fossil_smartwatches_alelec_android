package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import com.portfolio.platform.ui.view.DashBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hs3 {
    @DexIgnore
    public static /* final */ a a; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(DashBar dashBar, long j) {
            wd4.b(dashBar, "dashBar");
            a(dashBar, new tt3(4, 30, 20, 30), j);
        }

        @DexIgnore
        public final void b(DashBar dashBar, long j) {
            wd4.b(dashBar, "dashBar");
            a(dashBar, new tt3(4, 10, 10, 20), j);
        }

        @DexIgnore
        public final void c(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            a(dashBar, new tt3(z ? 4 : 3, 100, z ? 90 : 100, 100), j);
        }

        @DexIgnore
        public final void d(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 50 : 33;
            a(dashBar, new tt3(i, i2, z ? 40 : i2, i2), j);
        }

        @DexIgnore
        public final void e(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 40 : 33;
            a(dashBar, new tt3(i, i2, z ? 30 : 0, i2), j);
        }

        @DexIgnore
        public final void f(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 80 : 66;
            a(dashBar, new tt3(i, i2, z ? 70 : i2, i2), j);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final void a(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 90 : 66;
            a(dashBar, new tt3(i, i2, z ? 80 : i2, i2), j);
        }

        @DexIgnore
        public final void b(DashBar dashBar, boolean z, long j) {
            wd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 60 : 33;
            a(dashBar, new tt3(i, i2, z ? 50 : i2, i2), j);
        }

        @DexIgnore
        public final void a(DashBar dashBar, tt3 tt3, long j) {
            dashBar.setLength(tt3.c());
            dashBar.setProgress(tt3.d());
            if (tt3.e()) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", new int[]{tt3.b(), tt3.a()});
                wd4.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(j);
                ofInt.start();
            }
        }
    }
}
