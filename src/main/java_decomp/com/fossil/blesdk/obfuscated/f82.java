package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class f82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon18 e;
    @DexIgnore
    private /* final */ /* synthetic */ List f;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback g;

    @DexIgnore
    public /* synthetic */ f82(PresetRepository.Anon18 anon18, List list, PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
        this.e = anon18;
        this.f = list;
        this.g = pushPendingSavedPresetsCallback;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
