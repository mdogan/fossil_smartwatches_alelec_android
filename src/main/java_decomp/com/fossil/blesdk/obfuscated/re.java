package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class re implements qe {
    @DexIgnore
    public static /* final */ qe a; // = new re();

    @DexIgnore
    public static float a(RecyclerView recyclerView, View view) {
        int childCount = recyclerView.getChildCount();
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float g = g9.g(childAt);
                if (g > f) {
                    f = g;
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
    }

    @DexIgnore
    public void b(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z && view.getTag(ee.item_touch_helper_previous_elevation) == null) {
            Float valueOf = Float.valueOf(g9.g(view));
            g9.b(view, a(recyclerView, view) + 1.0f);
            view.setTag(ee.item_touch_helper_previous_elevation, valueOf);
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    public void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            Object tag = view.getTag(ee.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof Float)) {
                g9.b(view, ((Float) tag).floatValue());
            }
            view.setTag(ee.item_touch_helper_previous_elevation, (Object) null);
        }
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }
}
