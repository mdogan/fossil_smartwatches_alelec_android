package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.r62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x43 extends as2 implements r62.b {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ur3<ha2> j;
    @DexIgnore
    public CommuteTimeSettingsDetailViewModel k;
    @DexIgnore
    public k42 l;
    @DexIgnore
    public r62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return x43.o;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final x43 a(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
            x43 x43 = new x43();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            x43.setArguments(bundle);
            return x43;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ ha2 f;

        @DexIgnore
        public b(View view, ha2 ha2) {
            this.e = view;
            this.f = ha2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ha2 ha2 = this.f;
                wd4.a((Object) ha2, "binding");
                ha2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = x43.p.a();
                local.d(a, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;

        @DexIgnore
        public c(x43 x43, ha2 ha2) {
            this.e = x43;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            r62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    wd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = x43.p.a();
                    local.i(a2, "Autocomplete item selected: " + fullText);
                    CommuteTimeSettingsDetailViewModel b = x43.b(this.e);
                    String spannableString = fullText.toString();
                    wd4.a((Object) spannableString, "primaryText.toString()");
                    if (spannableString != null) {
                        String obj = StringsKt__StringsKt.d(spannableString).toString();
                        String placeId = item.getPlaceId();
                        r62 a3 = this.e.m;
                        if (a3 != null) {
                            b.a(obj, placeId, a3.b());
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ha2 e;

        @DexIgnore
        public d(x43 x43, ha2 ha2) {
            this.e = ha2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.e.r;
            wd4.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;

        @DexIgnore
        public e(x43 x43, ha2 ha2) {
            this.e = x43;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;

        @DexIgnore
        public f(x43 x43) {
            this.e = x43;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            CommuteTimeSettingsDetailViewModel b = x43.b(this.e);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = StringsKt__StringsKt.d(valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    wd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    b.b(upperCase);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;

        @DexIgnore
        public g(x43 x43) {
            this.e = x43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.r(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;

        @DexIgnore
        public h(x43 x43) {
            this.e = x43;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (x43.b(this.e).e()) {
                es3 es3 = es3.c;
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                es3.p(childFragmentManager);
            } else if (x43.b(this.e).f()) {
                this.e.r(true);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 e;
        @DexIgnore
        public /* final */ /* synthetic */ ha2 f;

        @DexIgnore
        public i(x43 x43, ha2 ha2) {
            this.e = x43;
            this.f = ha2;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsDetailViewModel.a(x43.b(this.e), (String) null, (String) null, (AutocompleteSessionToken) null, 7, (Object) null);
            this.f.q.setText("");
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ x43 a;

        @DexIgnore
        public j(x43 x43) {
            this.a = x43;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            x43.b(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements dc<CommuteTimeSettingsDetailViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ x43 a;
        @DexIgnore
        public /* final */ /* synthetic */ ha2 b;

        @DexIgnore
        public k(x43 x43, ha2 ha2) {
            this.a = x43;
            this.b = ha2;
        }

        @DexIgnore
        public final void a(CommuteTimeSettingsDetailViewModel.b bVar) {
            int i;
            PortfolioApp portfolioApp;
            PlacesClient d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            String c = bVar.c();
            if (c != null) {
                this.b.s.setText(c);
            }
            String a2 = bVar.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = bVar.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                SwitchCompat switchCompat = this.b.w;
                wd4.a((Object) switchCompat, "binding.scAvoidTolls");
                switchCompat.setChecked(booleanValue);
            }
            Boolean f = bVar.f();
            if (f != null) {
                boolean booleanValue2 = f.booleanValue();
                FlexibleEditText flexibleEditText = this.b.s;
                wd4.a((Object) flexibleEditText, "binding.fetAddressName");
                flexibleEditText.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.s.requestFocus();
                }
            }
            Boolean g = bVar.g();
            if (g != null) {
                boolean booleanValue3 = g.booleanValue();
                FlexibleTextView flexibleTextView = this.b.x;
                wd4.a((Object) flexibleTextView, "binding.tvRight");
                flexibleTextView.setEnabled(booleanValue3);
                FlexibleTextView flexibleTextView2 = this.b.x;
                if (booleanValue3) {
                    portfolioApp = PortfolioApp.W.c();
                    i = R.color.primaryColor;
                } else {
                    portfolioApp = PortfolioApp.W.c();
                    i = R.color.disabled;
                }
                flexibleTextView2.setTextColor(k6.a((Context) portfolioApp, i));
            }
            Boolean e = bVar.e();
            if (e == null) {
                return;
            }
            if (e.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    /*
    static {
        String simpleName = x43.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeSettingsDetailViewModel b(x43 x43) {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = x43.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            return commuteTimeSettingsDetailViewModel;
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        ur3<ha2> ur3 = this.j;
        if (ur3 != null) {
            ha2 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void T0() {
        ur3<ha2> ur3 = this.j;
        if (ur3 != null) {
            ha2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        ur3<ha2> ur3 = this.j;
        if (ur3 != null) {
            ha2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.t;
                wd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.W.c().g().a(new d53()).a(this);
        k42 k42 = this.l;
        ArrayList<String> arrayList = null;
        if (k42 != null) {
            jc a2 = mc.a((Fragment) this, (lc.b) k42).a(CommuteTimeSettingsDetailViewModel.class);
            wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.k = (CommuteTimeSettingsDetailViewModel) a2;
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
            if (commuteTimeSettingsDetailViewModel != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS");
                }
                commuteTimeSettingsDetailViewModel.a(addressWrapper, arrayList);
                return;
            }
            wd4.d("mViewModel");
            throw null;
        }
        wd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ha2 ha2 = (ha2) ra.a(layoutInflater, R.layout.fragment_commute_time_settings_detail, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = ha2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new c(this, ha2));
        appCompatAutoCompleteTextView.addTextChangedListener(new d(this, ha2));
        appCompatAutoCompleteTextView.setOnKeyListener(new e(this, ha2));
        ha2.s.addTextChangedListener(new f(this));
        ha2.u.setOnClickListener(new g(this));
        ha2.x.setOnClickListener(new h(this));
        ha2.r.setOnClickListener(new i(this, ha2));
        ha2.w.setOnCheckedChangeListener(new j(this));
        wd4.a((Object) ha2, "binding");
        View d2 = ha2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, ha2));
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.d().a(getViewLifecycleOwner(), new k(this, ha2));
            this.j = new ur3<>(this, ha2);
            return ha2.d();
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.h();
            super.onPause();
            return;
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.g();
        } else {
            wd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        if (z) {
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
            if (commuteTimeSettingsDetailViewModel != null) {
                AddressWrapper c2 = commuteTimeSettingsDetailViewModel.c();
                if (c2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", c2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                wd4.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) context, "context!!");
                this.m = new r62(context, placesClient);
                r62 r62 = this.m;
                if (r62 != null) {
                    r62.a((r62.b) this);
                }
                ur3<ha2> ur3 = this.j;
                if (ur3 != null) {
                    ha2 a2 = ur3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        wd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        T0();
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
    }
}
