package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nw1 {
    @DexIgnore
    public /* final */ Class<?> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public nw1(Class<?> cls, int i, int i2) {
        ck0.a(cls, (Object) "Null dependency anInterface.");
        this.a = cls;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public static nw1 a(Class<?> cls) {
        return new nw1(cls, 1, 0);
    }

    @DexIgnore
    public final boolean b() {
        return this.b == 1;
    }

    @DexIgnore
    public final boolean c() {
        return this.c == 0;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj instanceof nw1) {
            nw1 nw1 = (nw1) obj;
            if (this.a == nw1.a && this.b == nw1.b && this.c == nw1.c) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", required=");
        boolean z = false;
        sb.append(this.b == 1);
        sb.append(", direct=");
        if (this.c == 0) {
            z = true;
        }
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final Class<?> a() {
        return this.a;
    }
}
