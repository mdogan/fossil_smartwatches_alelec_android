package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yq3 extends vq3 {
    @DexIgnore
    public /* final */ wq3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        wd4.a((Object) yq3.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public yq3(wq3 wq3) {
        wd4.b(wq3, "mView");
        this.f = wq3;
    }

    @DexIgnore
    public void f() {
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.f.F0();
    }

    @DexIgnore
    public void i() {
        this.f.C0();
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }
}
