package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ do1 f;

    @DexIgnore
    public eo1(do1 do1, xn1 xn1) {
        this.f = do1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        if (this.e.c()) {
            this.f.c.f();
            return;
        }
        try {
            this.f.c.a(this.f.b.then(this.e));
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.c.a((Exception) e2.getCause());
            } else {
                this.f.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.f.c.a(e3);
        }
    }
}
