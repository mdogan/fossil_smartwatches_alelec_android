package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.af0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pc1 extends ge0<Object> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends t31 {
        @DexIgnore
        public /* final */ yn1<Void> e;

        @DexIgnore
        public a(yn1<Void> yn1) {
            this.e = yn1;
        }

        @DexIgnore
        public final void a(p31 p31) {
            hf0.a(p31.G(), this.e);
        }
    }

    @DexIgnore
    public pc1(Context context) {
        super(context, tc1.c, null, (ef0) new se0());
    }

    @DexIgnore
    public final s31 a(yn1<Boolean> yn1) {
        return new qd1(this, yn1);
    }

    @DexIgnore
    public xn1<Void> a(rc1 rc1) {
        return hf0.a(a((af0.a<?>) bf0.a(rc1, rc1.class.getSimpleName())));
    }

    @DexIgnore
    public xn1<Void> a(LocationRequest locationRequest, rc1 rc1, Looper looper) {
        j41 a2 = j41.a(locationRequest);
        af0 a3 = bf0.a(rc1, q41.a(looper), rc1.class.getSimpleName());
        return a(new od1(this, a3, a2, a3), new pd1(this, a3.b()));
    }

    @DexIgnore
    public xn1<Location> i() {
        return a(new nd1(this));
    }
}
