package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class so2<T> extends ro2<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public so2(T t, boolean z) {
        super((rd4) null);
        this.a = t;
        this.b = z;
    }

    @DexIgnore
    public final T a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof so2) {
                so2 so2 = (so2) obj;
                if (wd4.a((Object) this.a, (Object) so2.a)) {
                    if (this.b == so2.b) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.a;
        int hashCode = (t != null ? t.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + this.a + ", isFromCache=" + this.b + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ so2(Object obj, boolean z, int i, rd4 rd4) {
        this(obj, (i & 2) != 0 ? false : z);
    }
}
