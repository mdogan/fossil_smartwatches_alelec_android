package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.rb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ds0 extends pj0<es0> {
    @DexIgnore
    public /* final */ rb0.a E;

    @DexIgnore
    public ds0(Context context, Looper looper, lj0 lj0, rb0.a aVar, he0.b bVar, he0.c cVar) {
        super(context, looper, 68, lj0, bVar, cVar);
        this.E = aVar;
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof es0) {
            return (es0) queryLocalInterface;
        }
        return new fs0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return 12800000;
    }

    @DexIgnore
    public final Bundle u() {
        rb0.a aVar = this.E;
        return aVar == null ? new Bundle() : aVar.a();
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
