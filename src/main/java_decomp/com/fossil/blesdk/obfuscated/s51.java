package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s51 extends wb1<s51> {
    @DexIgnore
    public static volatile s51[] h;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public w51[] d; // = w51.e();
    @DexIgnore
    public t51[] e; // = t51.e();
    @DexIgnore
    public Boolean f; // = null;
    @DexIgnore
    public Boolean g; // = null;

    @DexIgnore
    public s51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static s51[] e() {
        if (h == null) {
            synchronized (ac1.b) {
                if (h == null) {
                    h = new s51[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        w51[] w51Arr = this.d;
        int i = 0;
        if (w51Arr != null && w51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                w51[] w51Arr2 = this.d;
                if (i2 >= w51Arr2.length) {
                    break;
                }
                w51 w51 = w51Arr2[i2];
                if (w51 != null) {
                    vb1.a(2, (bc1) w51);
                }
                i2++;
            }
        }
        t51[] t51Arr = this.e;
        if (t51Arr != null && t51Arr.length > 0) {
            while (true) {
                t51[] t51Arr2 = this.e;
                if (i >= t51Arr2.length) {
                    break;
                }
                t51 t51 = t51Arr2[i];
                if (t51 != null) {
                    vb1.a(3, (bc1) t51);
                }
                i++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            vb1.a(4, bool.booleanValue());
        }
        Boolean bool2 = this.g;
        if (bool2 != null) {
            vb1.a(5, bool2.booleanValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof s51)) {
            return false;
        }
        s51 s51 = (s51) obj;
        Integer num = this.c;
        if (num == null) {
            if (s51.c != null) {
                return false;
            }
        } else if (!num.equals(s51.c)) {
            return false;
        }
        if (!ac1.a((Object[]) this.d, (Object[]) s51.d) || !ac1.a((Object[]) this.e, (Object[]) s51.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (s51.f != null) {
                return false;
            }
        } else if (!bool.equals(s51.f)) {
            return false;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            if (s51.g != null) {
                return false;
            }
        } else if (!bool2.equals(s51.g)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(s51.b);
        }
        yb1 yb12 = s51.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (s51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (((((hashCode + (num == null ? 0 : num.hashCode())) * 31) + ac1.a((Object[]) this.d)) * 31) + ac1.a((Object[]) this.e)) * 31;
        Boolean bool = this.f;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.g;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        w51[] w51Arr = this.d;
        int i = 0;
        if (w51Arr != null && w51Arr.length > 0) {
            int i2 = a;
            int i3 = 0;
            while (true) {
                w51[] w51Arr2 = this.d;
                if (i3 >= w51Arr2.length) {
                    break;
                }
                w51 w51 = w51Arr2[i3];
                if (w51 != null) {
                    i2 += vb1.b(2, (bc1) w51);
                }
                i3++;
            }
            a = i2;
        }
        t51[] t51Arr = this.e;
        if (t51Arr != null && t51Arr.length > 0) {
            while (true) {
                t51[] t51Arr2 = this.e;
                if (i >= t51Arr2.length) {
                    break;
                }
                t51 t51 = t51Arr2[i];
                if (t51 != null) {
                    a += vb1.b(3, (bc1) t51);
                }
                i++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            bool.booleanValue();
            a += vb1.c(4) + 1;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            return a;
        }
        bool2.booleanValue();
        return a + vb1.c(5) + 1;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(ub1.e());
            } else if (c2 == 18) {
                int a = ec1.a(ub1, 18);
                w51[] w51Arr = this.d;
                int length = w51Arr == null ? 0 : w51Arr.length;
                w51[] w51Arr2 = new w51[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.d, 0, w51Arr2, 0, length);
                }
                while (length < w51Arr2.length - 1) {
                    w51Arr2[length] = new w51();
                    ub1.a((bc1) w51Arr2[length]);
                    ub1.c();
                    length++;
                }
                w51Arr2[length] = new w51();
                ub1.a((bc1) w51Arr2[length]);
                this.d = w51Arr2;
            } else if (c2 == 26) {
                int a2 = ec1.a(ub1, 26);
                t51[] t51Arr = this.e;
                int length2 = t51Arr == null ? 0 : t51Arr.length;
                t51[] t51Arr2 = new t51[(a2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.e, 0, t51Arr2, 0, length2);
                }
                while (length2 < t51Arr2.length - 1) {
                    t51Arr2[length2] = new t51();
                    ub1.a((bc1) t51Arr2[length2]);
                    ub1.c();
                    length2++;
                }
                t51Arr2[length2] = new t51();
                ub1.a((bc1) t51Arr2[length2]);
                this.e = t51Arr2;
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(ub1.d());
            } else if (c2 == 40) {
                this.g = Boolean.valueOf(ub1.d());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
