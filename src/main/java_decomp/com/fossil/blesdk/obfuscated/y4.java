package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y4 extends i5 {
    @DexIgnore
    public int A0; // = 0;
    @DexIgnore
    public int B0; // = 0;
    @DexIgnore
    public int C0; // = 7;
    @DexIgnore
    public boolean D0; // = false;
    @DexIgnore
    public boolean E0; // = false;
    @DexIgnore
    public boolean F0; // = false;
    @DexIgnore
    public boolean l0; // = false;
    @DexIgnore
    public q4 m0; // = new q4();
    @DexIgnore
    public h5 n0;
    @DexIgnore
    public int o0;
    @DexIgnore
    public int p0;
    @DexIgnore
    public int q0;
    @DexIgnore
    public int r0;
    @DexIgnore
    public int s0; // = 0;
    @DexIgnore
    public int t0; // = 0;
    @DexIgnore
    public x4[] u0; // = new x4[4];
    @DexIgnore
    public x4[] v0; // = new x4[4];
    @DexIgnore
    public List<z4> w0; // = new ArrayList();
    @DexIgnore
    public boolean x0; // = false;
    @DexIgnore
    public boolean y0; // = false;
    @DexIgnore
    public boolean z0; // = false;

    @DexIgnore
    public void E() {
        this.m0.i();
        this.o0 = 0;
        this.q0 = 0;
        this.p0 = 0;
        this.r0 = 0;
        this.w0.clear();
        this.D0 = false;
        super.E();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v15, types: [boolean] */
    /* JADX WARNING: type inference failed for: r8v16 */
    /* JADX WARNING: type inference failed for: r8v17 */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0293  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01da  */
    public void L() {
        int i;
        int i2;
        boolean z;
        boolean z2;
        char c;
        int i3;
        boolean z3;
        int max;
        int max2;
        Object r8;
        boolean z4;
        int i4 = this.I;
        int i5 = this.J;
        int max3 = Math.max(0, t());
        int max4 = Math.max(0, j());
        this.E0 = false;
        this.F0 = false;
        if (this.D != null) {
            if (this.n0 == null) {
                this.n0 = new h5(this);
            }
            this.n0.b(this);
            s(this.o0);
            t(this.p0);
            F();
            a(this.m0.e());
        } else {
            this.I = 0;
            this.J = 0;
        }
        int i6 = 32;
        if (this.C0 != 0) {
            if (!u(8)) {
                T();
            }
            if (!u(32)) {
                S();
            }
            this.m0.g = true;
        } else {
            this.m0.g = false;
        }
        ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = this.C;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[1];
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[0];
        V();
        if (this.w0.size() == 0) {
            this.w0.clear();
            this.w0.add(0, new z4(this.k0));
        }
        int size = this.w0.size();
        ArrayList<ConstraintWidget> arrayList = this.k0;
        boolean z5 = k() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || r() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z6 = false;
        int i7 = 0;
        while (i7 < size && !this.D0) {
            if (this.w0.get(i7).d) {
                i = size;
            } else {
                if (u(i6)) {
                    if (k() == ConstraintWidget.DimensionBehaviour.FIXED && r() == ConstraintWidget.DimensionBehaviour.FIXED) {
                        this.k0 = (ArrayList) this.w0.get(i7).a();
                    } else {
                        this.k0 = (ArrayList) this.w0.get(i7).a;
                    }
                }
                V();
                int size2 = this.k0.size();
                for (int i8 = 0; i8 < size2; i8++) {
                    ConstraintWidget constraintWidget = this.k0.get(i8);
                    if (constraintWidget instanceof i5) {
                        ((i5) constraintWidget).L();
                    }
                }
                boolean z7 = z6;
                int i9 = 0;
                boolean z8 = true;
                while (z8) {
                    boolean z9 = z8;
                    int i10 = i9 + 1;
                    try {
                        this.m0.i();
                        V();
                        b(this.m0);
                        int i11 = 0;
                        while (i11 < size2) {
                            z = z7;
                            try {
                                this.k0.get(i11).b(this.m0);
                                i11++;
                                z7 = z;
                            } catch (Exception e) {
                                e = e;
                                z4 = z9;
                                e.printStackTrace();
                                PrintStream printStream = System.out;
                                z2 = z4;
                                StringBuilder sb = new StringBuilder();
                                i2 = size;
                                sb.append("EXCEPTION : ");
                                sb.append(e);
                                printStream.println(sb.toString());
                                if (z2) {
                                }
                                c = 2;
                                if (z5) {
                                }
                                i3 = i10;
                                z7 = z;
                                z3 = false;
                                max = Math.max(this.R, t());
                                if (max > t()) {
                                }
                                max2 = Math.max(this.S, j());
                                if (max2 <= j()) {
                                }
                                if (!z7) {
                                }
                                z8 = z3;
                                i9 = i3;
                                size = i2;
                            }
                        }
                        z = z7;
                        z4 = d(this.m0);
                        if (z4) {
                            try {
                                this.m0.g();
                            } catch (Exception e2) {
                                e = e2;
                            }
                        }
                        z2 = z4;
                        i2 = size;
                    } catch (Exception e3) {
                        e = e3;
                        z = z7;
                        z4 = z9;
                        e.printStackTrace();
                        PrintStream printStream2 = System.out;
                        z2 = z4;
                        StringBuilder sb2 = new StringBuilder();
                        i2 = size;
                        sb2.append("EXCEPTION : ");
                        sb2.append(e);
                        printStream2.println(sb2.toString());
                        if (z2) {
                        }
                        c = 2;
                        if (z5) {
                        }
                        i3 = i10;
                        z7 = z;
                        z3 = false;
                        max = Math.max(this.R, t());
                        if (max > t()) {
                        }
                        max2 = Math.max(this.S, j());
                        if (max2 <= j()) {
                        }
                        if (!z7) {
                        }
                        z8 = z3;
                        i9 = i3;
                        size = i2;
                    }
                    if (z2) {
                        a(this.m0, c5.a);
                    } else {
                        c(this.m0);
                        int i12 = 0;
                        while (true) {
                            if (i12 >= size2) {
                                break;
                            }
                            ConstraintWidget constraintWidget2 = this.k0.get(i12);
                            if (constraintWidget2.C[0] != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget2.t() >= constraintWidget2.v()) {
                                if (constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget2.j() < constraintWidget2.u()) {
                                    c = 2;
                                    c5.a[2] = true;
                                    break;
                                }
                                i12++;
                            } else {
                                c5.a[2] = true;
                                break;
                            }
                        }
                        if (z5 || i10 >= 8 || !c5.a[c]) {
                            i3 = i10;
                            z7 = z;
                            z3 = false;
                        } else {
                            int i13 = 0;
                            int i14 = 0;
                            int i15 = 0;
                            while (i13 < size2) {
                                ConstraintWidget constraintWidget3 = this.k0.get(i13);
                                i14 = Math.max(i14, constraintWidget3.I + constraintWidget3.t());
                                i15 = Math.max(i15, constraintWidget3.J + constraintWidget3.j());
                                i13++;
                                i10 = i10;
                            }
                            i3 = i10;
                            int max5 = Math.max(this.R, i14);
                            int max6 = Math.max(this.S, i15);
                            if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || t() >= max5) {
                                z3 = false;
                            } else {
                                p(max5);
                                this.C[0] = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                z3 = true;
                                z = true;
                            }
                            if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || j() >= max6) {
                                z7 = z;
                            } else {
                                h(max6);
                                this.C[1] = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                z3 = true;
                                z7 = true;
                            }
                        }
                        max = Math.max(this.R, t());
                        if (max > t()) {
                            p(max);
                            this.C[0] = ConstraintWidget.DimensionBehaviour.FIXED;
                            z3 = true;
                            z7 = true;
                        }
                        max2 = Math.max(this.S, j());
                        if (max2 <= j()) {
                            h(max2);
                            r8 = 1;
                            this.C[1] = ConstraintWidget.DimensionBehaviour.FIXED;
                            z3 = true;
                            z7 = true;
                        } else {
                            r8 = 1;
                        }
                        if (!z7) {
                            if (this.C[0] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && max3 > 0 && t() > max3) {
                                this.E0 = r8;
                                this.C[0] = ConstraintWidget.DimensionBehaviour.FIXED;
                                p(max3);
                                z3 = true;
                                z7 = true;
                            }
                            if (this.C[r8] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && max4 > 0 && j() > max4) {
                                this.F0 = r8;
                                this.C[r8] = ConstraintWidget.DimensionBehaviour.FIXED;
                                h(max4);
                                z8 = true;
                                z7 = true;
                                i9 = i3;
                                size = i2;
                            }
                        }
                        z8 = z3;
                        i9 = i3;
                        size = i2;
                    }
                    c = 2;
                    if (z5) {
                    }
                    i3 = i10;
                    z7 = z;
                    z3 = false;
                    max = Math.max(this.R, t());
                    if (max > t()) {
                    }
                    max2 = Math.max(this.S, j());
                    if (max2 <= j()) {
                    }
                    if (!z7) {
                    }
                    z8 = z3;
                    i9 = i3;
                    size = i2;
                }
                i = size;
                this.w0.get(i7).b();
                z6 = z7;
            }
            i7++;
            size = i;
            i6 = 32;
        }
        this.k0 = arrayList;
        if (this.D != null) {
            int max7 = Math.max(this.R, t());
            int max8 = Math.max(this.S, j());
            this.n0.a(this);
            p(max7 + this.o0 + this.q0);
            h(max8 + this.p0 + this.r0);
        } else {
            this.I = i4;
            this.J = i5;
        }
        if (z6) {
            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr2 = this.C;
            dimensionBehaviourArr2[0] = dimensionBehaviour2;
            dimensionBehaviourArr2[1] = dimensionBehaviour;
        }
        a(this.m0.e());
        if (this == K()) {
            I();
        }
    }

    @DexIgnore
    public int N() {
        return this.C0;
    }

    @DexIgnore
    public boolean O() {
        return false;
    }

    @DexIgnore
    public boolean P() {
        return this.F0;
    }

    @DexIgnore
    public boolean Q() {
        return this.l0;
    }

    @DexIgnore
    public boolean R() {
        return this.E0;
    }

    @DexIgnore
    public void S() {
        if (!u(8)) {
            a(this.C0);
        }
        W();
    }

    @DexIgnore
    public void T() {
        int size = this.k0.size();
        G();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).G();
        }
    }

    @DexIgnore
    public void U() {
        T();
        a(this.C0);
    }

    @DexIgnore
    public final void V() {
        this.s0 = 0;
        this.t0 = 0;
    }

    @DexIgnore
    public void W() {
        e5 d = a(ConstraintAnchor.Type.LEFT).d();
        e5 d2 = a(ConstraintAnchor.Type.TOP).d();
        d.a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        d2.a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(q4 q4Var, boolean[] zArr) {
        zArr[2] = false;
        c(q4Var);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.k0.get(i);
            constraintWidget.c(q4Var);
            if (constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.t() < constraintWidget.v()) {
                zArr[2] = true;
            }
            if (constraintWidget.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.j() < constraintWidget.u()) {
                zArr[2] = true;
            }
        }
    }

    @DexIgnore
    public void c(boolean z) {
        this.l0 = z;
    }

    @DexIgnore
    public boolean d(q4 q4Var) {
        a(q4Var);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.k0.get(i);
            if (constraintWidget instanceof y4) {
                ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.C;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[1];
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.a(ConstraintWidget.DimensionBehaviour.FIXED);
                }
                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.b(ConstraintWidget.DimensionBehaviour.FIXED);
                }
                constraintWidget.a(q4Var);
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.a(dimensionBehaviour);
                }
                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.b(dimensionBehaviour2);
                }
            } else {
                c5.a(this, q4Var, constraintWidget);
                constraintWidget.a(q4Var);
            }
        }
        if (this.s0 > 0) {
            w4.a(this, q4Var, 0);
        }
        if (this.t0 > 0) {
            w4.a(this, q4Var, 1);
        }
        return true;
    }

    @DexIgnore
    public final void e(ConstraintWidget constraintWidget) {
        int i = this.t0 + 1;
        x4[] x4VarArr = this.u0;
        if (i >= x4VarArr.length) {
            this.u0 = (x4[]) Arrays.copyOf(x4VarArr, x4VarArr.length * 2);
        }
        this.u0[this.t0] = new x4(constraintWidget, 1, Q());
        this.t0++;
    }

    @DexIgnore
    public void f(int i, int i2) {
        if (this.C[0] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            f5 f5Var = this.c;
            if (f5Var != null) {
                f5Var.a(i);
            }
        }
        if (this.C[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            f5 f5Var2 = this.d;
            if (f5Var2 != null) {
                f5Var2.a(i2);
            }
        }
    }

    @DexIgnore
    public boolean u(int i) {
        return (this.C0 & i) == i;
    }

    @DexIgnore
    public void v(int i) {
        this.C0 = i;
    }

    @DexIgnore
    public void a(int i) {
        super.a(i);
        int size = this.k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k0.get(i2).a(i);
        }
    }

    @DexIgnore
    public void a(ConstraintWidget constraintWidget, int i) {
        if (i == 0) {
            d(constraintWidget);
        } else if (i == 1) {
            e(constraintWidget);
        }
    }

    @DexIgnore
    public final void d(ConstraintWidget constraintWidget) {
        int i = this.s0 + 1;
        x4[] x4VarArr = this.v0;
        if (i >= x4VarArr.length) {
            this.v0 = (x4[]) Arrays.copyOf(x4VarArr, x4VarArr.length * 2);
        }
        this.v0[this.s0] = new x4(constraintWidget, 0, Q());
        this.s0++;
    }
}
