package com.fossil.blesdk.obfuscated;

import android.util.Log;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kr4<T> extends mr4<T> {
    @DexIgnore
    public kr4(T t) {
        super(t);
    }

    @DexIgnore
    public void b(String str, String str2, String str3, int i, int i2, String... strArr) {
        FragmentManager c = c();
        if (c.a("RationaleDialogFragmentCompat") instanceof hr4) {
            Log.d("BSPermissionsHelper", "Found existing fragment, not showing rationale.");
        } else {
            hr4.a(str, str2, str3, i, i2, strArr).a(c, "RationaleDialogFragmentCompat");
        }
    }

    @DexIgnore
    public abstract FragmentManager c();
}
