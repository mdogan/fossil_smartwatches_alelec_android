package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn4 implements Interceptor {
    @DexIgnore
    public /* final */ boolean a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends zo4 {
        @DexIgnore
        public long f;

        @DexIgnore
        public a(jp4 jp4) {
            super(jp4);
        }

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            super.a(vo4, j);
            this.f += j;
        }
    }

    @DexIgnore
    public kn4(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response;
        pn4 pn4 = (pn4) chain;
        ln4 g = pn4.g();
        in4 h = pn4.h();
        fn4 fn4 = (fn4) pn4.c();
        pm4 n = pn4.n();
        long currentTimeMillis = System.currentTimeMillis();
        pn4.f().d(pn4.e());
        g.a(n);
        pn4.f().a(pn4.e(), n);
        Response.a aVar = null;
        if (on4.b(n.e()) && n.a() != null) {
            if ("100-continue".equalsIgnoreCase(n.a("Expect"))) {
                g.b();
                pn4.f().f(pn4.e());
                aVar = g.a(true);
            }
            if (aVar == null) {
                pn4.f().c(pn4.e());
                a aVar2 = new a(g.a(n, n.a().a()));
                wo4 a2 = ep4.a((jp4) aVar2);
                n.a().a(a2);
                a2.close();
                pn4.f().a(pn4.e(), aVar2.f);
            } else if (!fn4.e()) {
                h.e();
            }
        }
        g.a();
        if (aVar == null) {
            pn4.f().f(pn4.e());
            aVar = g.a(false);
        }
        aVar.a(n);
        aVar.a(h.c().d());
        aVar.b(currentTimeMillis);
        aVar.a(System.currentTimeMillis());
        Response a3 = aVar.a();
        int B = a3.B();
        if (B == 100) {
            Response.a a4 = g.a(false);
            a4.a(n);
            a4.a(h.c().d());
            a4.b(currentTimeMillis);
            a4.a(System.currentTimeMillis());
            a3 = a4.a();
            B = a3.B();
        }
        pn4.f().a(pn4.e(), a3);
        if (!this.a || B != 101) {
            Response.a H = a3.H();
            H.a(g.a(a3));
            response = H.a();
        } else {
            Response.a H2 = a3.H();
            H2.a(vm4.c);
            response = H2.a();
        }
        if ("close".equalsIgnoreCase(response.L().a("Connection")) || "close".equalsIgnoreCase(response.e("Connection"))) {
            h.e();
        }
        if ((B != 204 && B != 205) || response.y().C() <= 0) {
            return response;
        }
        throw new ProtocolException("HTTP " + B + " had non-zero Content-Length: " + response.y().C());
    }
}
