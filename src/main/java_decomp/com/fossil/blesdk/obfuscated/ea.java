package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ea {
    @DexIgnore
    ColorStateList getSupportImageTintList();

    @DexIgnore
    PorterDuff.Mode getSupportImageTintMode();

    @DexIgnore
    void setSupportImageTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportImageTintMode(PorterDuff.Mode mode);
}
