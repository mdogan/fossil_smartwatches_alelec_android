package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iw3 {
    @DexIgnore
    public /* final */ dp4 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ xo4 c; // = ep4.a((kp4) this.a);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ap4 {
        @DexIgnore
        public a(kp4 kp4) {
            super(kp4);
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            if (iw3.this.b == 0) {
                return -1;
            }
            long b = super.b(vo4, Math.min(j, (long) iw3.this.b));
            if (b == -1) {
                return -1;
            }
            iw3 iw3 = iw3.this;
            int unused = iw3.b = (int) (((long) iw3.b) - b);
            return b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Inflater {
        @DexIgnore
        public b(iw3 iw3) {
        }

        @DexIgnore
        public int inflate(byte[] bArr, int i, int i2) throws DataFormatException {
            int inflate = super.inflate(bArr, i, i2);
            if (inflate != 0 || !needsDictionary()) {
                return inflate;
            }
            setDictionary(mw3.a);
            return super.inflate(bArr, i, i2);
        }
    }

    @DexIgnore
    public iw3(xo4 xo4) {
        this.a = new dp4((kp4) new a(xo4), (Inflater) new b(this));
    }

    @DexIgnore
    public final void b() throws IOException {
        if (this.b > 0) {
            this.a.c();
            if (this.b != 0) {
                throw new IOException("compressedLimit > 0: " + this.b);
            }
        }
    }

    @DexIgnore
    public final ByteString c() throws IOException {
        return this.c.d((long) this.c.readInt());
    }

    @DexIgnore
    public List<dw3> a(int i) throws IOException {
        this.b += i;
        int readInt = this.c.readInt();
        if (readInt < 0) {
            throw new IOException("numberOfPairs < 0: " + readInt);
        } else if (readInt <= 1024) {
            ArrayList arrayList = new ArrayList(readInt);
            int i2 = 0;
            while (i2 < readInt) {
                ByteString asciiLowercase = c().toAsciiLowercase();
                ByteString c2 = c();
                if (asciiLowercase.size() != 0) {
                    arrayList.add(new dw3(asciiLowercase, c2));
                    i2++;
                } else {
                    throw new IOException("name.size == 0");
                }
            }
            b();
            return arrayList;
        } else {
            throw new IOException("numberOfPairs > 1024: " + readInt);
        }
    }

    @DexIgnore
    public void a() throws IOException {
        this.c.close();
    }
}
