package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.fossil.blesdk.obfuscated.Cdo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ut implements Cdo.a {
    @DexIgnore
    public /* final */ kq a;
    @DexIgnore
    public /* final */ hq b;

    @DexIgnore
    public ut(kq kqVar, hq hqVar) {
        this.a = kqVar;
        this.b = hqVar;
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.b(i, i2, config);
    }

    @DexIgnore
    public byte[] b(int i) {
        hq hqVar = this.b;
        if (hqVar == null) {
            return new byte[i];
        }
        return (byte[]) hqVar.b(i, byte[].class);
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.a(bitmap);
    }

    @DexIgnore
    public void a(byte[] bArr) {
        hq hqVar = this.b;
        if (hqVar != null) {
            hqVar.put(bArr);
        }
    }

    @DexIgnore
    public int[] a(int i) {
        hq hqVar = this.b;
        if (hqVar == null) {
            return new int[i];
        }
        return (int[]) hqVar.b(i, int[].class);
    }

    @DexIgnore
    public void a(int[] iArr) {
        hq hqVar = this.b;
        if (hqVar != null) {
            hqVar.put(iArr);
        }
    }
}
