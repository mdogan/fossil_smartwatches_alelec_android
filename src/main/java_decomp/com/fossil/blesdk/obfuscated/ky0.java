package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzge$zzs;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ky0 extends rx0<ky0> implements Cloneable {
    @DexIgnore
    public long g; // = 0;
    @DexIgnore
    public long h; // = 0;
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public ly0[] l; // = ly0.f();
    @DexIgnore
    public byte[] m;
    @DexIgnore
    public ay0 n;
    @DexIgnore
    public byte[] o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public hy0 r;
    @DexIgnore
    public String s;
    @DexIgnore
    public long t;
    @DexIgnore
    public iy0 u;
    @DexIgnore
    public byte[] v;
    @DexIgnore
    public String w;
    @DexIgnore
    public int[] x;
    @DexIgnore
    public zzge$zzs y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public ky0() {
        byte[] bArr = zx0.e;
        this.m = bArr;
        this.n = null;
        this.o = bArr;
        this.p = "";
        this.q = "";
        this.r = null;
        this.s = "";
        this.t = 180000;
        this.u = null;
        this.v = bArr;
        this.w = "";
        this.x = zx0.a;
        this.y = null;
        this.z = false;
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(qx0 qx0) throws IOException {
        long j2 = this.g;
        if (j2 != 0) {
            qx0.a(1, j2);
        }
        String str = this.i;
        if (str != null && !str.equals("")) {
            qx0.a(2, this.i);
        }
        ly0[] ly0Arr = this.l;
        int i2 = 0;
        if (ly0Arr != null && ly0Arr.length > 0) {
            int i3 = 0;
            while (true) {
                ly0[] ly0Arr2 = this.l;
                if (i3 >= ly0Arr2.length) {
                    break;
                }
                ly0 ly0 = ly0Arr2[i3];
                if (ly0 != null) {
                    qx0.a(3, (wx0) ly0);
                }
                i3++;
            }
        }
        if (!Arrays.equals(this.m, zx0.e)) {
            qx0.a(4, this.m);
        }
        if (!Arrays.equals(this.o, zx0.e)) {
            qx0.a(6, this.o);
        }
        hy0 hy0 = this.r;
        if (hy0 != null) {
            qx0.a(7, (wx0) hy0);
        }
        String str2 = this.p;
        if (str2 != null && !str2.equals("")) {
            qx0.a(8, this.p);
        }
        ay0 ay0 = this.n;
        if (ay0 != null) {
            qx0.a(9, (tv0) ay0);
        }
        int i4 = this.j;
        if (i4 != 0) {
            qx0.b(11, i4);
        }
        String str3 = this.q;
        if (str3 != null && !str3.equals("")) {
            qx0.a(13, this.q);
        }
        String str4 = this.s;
        if (str4 != null && !str4.equals("")) {
            qx0.a(14, this.s);
        }
        long j3 = this.t;
        if (j3 != 180000) {
            qx0.a(15, 0);
            qx0.a(qx0.b(j3));
        }
        iy0 iy0 = this.u;
        if (iy0 != null) {
            qx0.a(16, (wx0) iy0);
        }
        long j4 = this.h;
        if (j4 != 0) {
            qx0.a(17, j4);
        }
        if (!Arrays.equals(this.v, zx0.e)) {
            qx0.a(18, this.v);
        }
        int[] iArr = this.x;
        if (iArr != null && iArr.length > 0) {
            while (true) {
                int[] iArr2 = this.x;
                if (i2 >= iArr2.length) {
                    break;
                }
                qx0.b(20, iArr2[i2]);
                i2++;
            }
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs != null) {
            qx0.a(23, (tv0) zzge_zzs);
        }
        String str5 = this.w;
        if (str5 != null && !str5.equals("")) {
            qx0.a(24, this.w);
        }
        boolean z2 = this.z;
        if (z2) {
            qx0.a(25, z2);
        }
        String str6 = this.k;
        if (str6 != null && !str6.equals("")) {
            qx0.a(26, this.k);
        }
        super.a(qx0);
    }

    @DexIgnore
    public final int b() {
        int[] iArr;
        int b = super.b();
        long j2 = this.g;
        if (j2 != 0) {
            b += qx0.b(1, j2);
        }
        String str = this.i;
        if (str != null && !str.equals("")) {
            b += qx0.b(2, this.i);
        }
        ly0[] ly0Arr = this.l;
        int i2 = 0;
        if (ly0Arr != null && ly0Arr.length > 0) {
            int i3 = b;
            int i4 = 0;
            while (true) {
                ly0[] ly0Arr2 = this.l;
                if (i4 >= ly0Arr2.length) {
                    break;
                }
                ly0 ly0 = ly0Arr2[i4];
                if (ly0 != null) {
                    i3 += qx0.b(3, (wx0) ly0);
                }
                i4++;
            }
            b = i3;
        }
        if (!Arrays.equals(this.m, zx0.e)) {
            b += qx0.b(4, this.m);
        }
        if (!Arrays.equals(this.o, zx0.e)) {
            b += qx0.b(6, this.o);
        }
        hy0 hy0 = this.r;
        if (hy0 != null) {
            b += qx0.b(7, (wx0) hy0);
        }
        String str2 = this.p;
        if (str2 != null && !str2.equals("")) {
            b += qx0.b(8, this.p);
        }
        ay0 ay0 = this.n;
        if (ay0 != null) {
            b += zzbn.c(9, (tv0) ay0);
        }
        int i5 = this.j;
        if (i5 != 0) {
            b += qx0.c(11) + qx0.d(i5);
        }
        String str3 = this.q;
        if (str3 != null && !str3.equals("")) {
            b += qx0.b(13, this.q);
        }
        String str4 = this.s;
        if (str4 != null && !str4.equals("")) {
            b += qx0.b(14, this.s);
        }
        long j3 = this.t;
        if (j3 != 180000) {
            b += qx0.c(15) + qx0.c(qx0.b(j3));
        }
        iy0 iy0 = this.u;
        if (iy0 != null) {
            b += qx0.b(16, (wx0) iy0);
        }
        long j4 = this.h;
        if (j4 != 0) {
            b += qx0.b(17, j4);
        }
        if (!Arrays.equals(this.v, zx0.e)) {
            b += qx0.b(18, this.v);
        }
        int[] iArr2 = this.x;
        if (iArr2 != null && iArr2.length > 0) {
            int i6 = 0;
            while (true) {
                iArr = this.x;
                if (i2 >= iArr.length) {
                    break;
                }
                i6 += qx0.d(iArr[i2]);
                i2++;
            }
            b = b + i6 + (iArr.length * 2);
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs != null) {
            b += zzbn.c(23, (tv0) zzge_zzs);
        }
        String str5 = this.w;
        if (str5 != null && !str5.equals("")) {
            b += qx0.b(24, this.w);
        }
        if (this.z) {
            b += qx0.c(25) + 1;
        }
        String str6 = this.k;
        return (str6 == null || str6.equals("")) ? b : b + qx0.b(26, this.k);
    }

    @DexIgnore
    public final /* synthetic */ wx0 c() throws CloneNotSupportedException {
        return (ky0) clone();
    }

    @DexIgnore
    public final /* synthetic */ rx0 d() throws CloneNotSupportedException {
        return (ky0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final ky0 clone() {
        try {
            ky0 ky0 = (ky0) super.clone();
            ly0[] ly0Arr = this.l;
            if (ly0Arr != null && ly0Arr.length > 0) {
                ky0.l = new ly0[ly0Arr.length];
                int i2 = 0;
                while (true) {
                    ly0[] ly0Arr2 = this.l;
                    if (i2 >= ly0Arr2.length) {
                        break;
                    }
                    if (ly0Arr2[i2] != null) {
                        ky0.l[i2] = (ly0) ly0Arr2[i2].clone();
                    }
                    i2++;
                }
            }
            ay0 ay0 = this.n;
            if (ay0 != null) {
                ky0.n = ay0;
            }
            hy0 hy0 = this.r;
            if (hy0 != null) {
                ky0.r = (hy0) hy0.clone();
            }
            iy0 iy0 = this.u;
            if (iy0 != null) {
                ky0.u = (iy0) iy0.clone();
            }
            int[] iArr = this.x;
            if (iArr != null && iArr.length > 0) {
                ky0.x = (int[]) iArr.clone();
            }
            zzge$zzs zzge_zzs = this.y;
            if (zzge_zzs != null) {
                ky0.y = zzge_zzs;
            }
            return ky0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ky0)) {
            return false;
        }
        ky0 ky0 = (ky0) obj;
        if (this.g != ky0.g || this.h != ky0.h) {
            return false;
        }
        String str = this.i;
        if (str == null) {
            if (ky0.i != null) {
                return false;
            }
        } else if (!str.equals(ky0.i)) {
            return false;
        }
        if (this.j != ky0.j) {
            return false;
        }
        String str2 = this.k;
        if (str2 == null) {
            if (ky0.k != null) {
                return false;
            }
        } else if (!str2.equals(ky0.k)) {
            return false;
        }
        if (!vx0.a((Object[]) this.l, (Object[]) ky0.l) || !Arrays.equals(this.m, ky0.m)) {
            return false;
        }
        ay0 ay0 = this.n;
        if (ay0 == null) {
            if (ky0.n != null) {
                return false;
            }
        } else if (!ay0.equals(ky0.n)) {
            return false;
        }
        if (!Arrays.equals(this.o, ky0.o)) {
            return false;
        }
        String str3 = this.p;
        if (str3 == null) {
            if (ky0.p != null) {
                return false;
            }
        } else if (!str3.equals(ky0.p)) {
            return false;
        }
        String str4 = this.q;
        if (str4 == null) {
            if (ky0.q != null) {
                return false;
            }
        } else if (!str4.equals(ky0.q)) {
            return false;
        }
        hy0 hy0 = this.r;
        if (hy0 == null) {
            if (ky0.r != null) {
                return false;
            }
        } else if (!hy0.equals(ky0.r)) {
            return false;
        }
        String str5 = this.s;
        if (str5 == null) {
            if (ky0.s != null) {
                return false;
            }
        } else if (!str5.equals(ky0.s)) {
            return false;
        }
        if (this.t != ky0.t) {
            return false;
        }
        iy0 iy0 = this.u;
        if (iy0 == null) {
            if (ky0.u != null) {
                return false;
            }
        } else if (!iy0.equals(ky0.u)) {
            return false;
        }
        if (!Arrays.equals(this.v, ky0.v)) {
            return false;
        }
        String str6 = this.w;
        if (str6 == null) {
            if (ky0.w != null) {
                return false;
            }
        } else if (!str6.equals(ky0.w)) {
            return false;
        }
        if (!vx0.a(this.x, ky0.x)) {
            return false;
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs == null) {
            if (ky0.y != null) {
                return false;
            }
        } else if (!zzge_zzs.equals(ky0.y)) {
            return false;
        }
        if (this.z != ky0.z) {
            return false;
        }
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            return this.f.equals(ky0.f);
        }
        tx0 tx02 = ky0.f;
        return tx02 == null || tx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        long j2 = this.g;
        long j3 = this.h;
        int hashCode = (((((ky0.class.getName().hashCode() + 527) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31 * 31;
        String str = this.i;
        int i2 = 0;
        int hashCode2 = (((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.j) * 31;
        String str2 = this.k;
        int hashCode3 = str2 == null ? 0 : str2.hashCode();
        int i3 = 1237;
        int a = ((((((hashCode2 + hashCode3) * 31 * 31) + 1237) * 31) + vx0.a((Object[]) this.l)) * 31) + Arrays.hashCode(this.m);
        ay0 ay0 = this.n;
        int hashCode4 = ((((a * 31) + (ay0 == null ? 0 : ay0.hashCode())) * 31) + Arrays.hashCode(this.o)) * 31;
        String str3 = this.p;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.q;
        int hashCode6 = hashCode5 + (str4 == null ? 0 : str4.hashCode());
        hy0 hy0 = this.r;
        int hashCode7 = ((hashCode6 * 31) + (hy0 == null ? 0 : hy0.hashCode())) * 31;
        String str5 = this.s;
        int hashCode8 = str5 == null ? 0 : str5.hashCode();
        long j4 = this.t;
        iy0 iy0 = this.u;
        int hashCode9 = (((((((hashCode7 + hashCode8) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + (iy0 == null ? 0 : iy0.hashCode())) * 31) + Arrays.hashCode(this.v)) * 31;
        String str6 = this.w;
        int hashCode10 = str6 == null ? 0 : str6.hashCode();
        zzge$zzs zzge_zzs = this.y;
        int a2 = (((((hashCode9 + hashCode10) * 31 * 31) + vx0.a(this.x)) * 31 * 31) + (zzge_zzs == null ? 0 : zzge_zzs.hashCode())) * 31;
        if (this.z) {
            i3 = 1231;
        }
        int i4 = (a2 + i3) * 31;
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            i2 = this.f.hashCode();
        }
        return i4 + i2;
    }
}
