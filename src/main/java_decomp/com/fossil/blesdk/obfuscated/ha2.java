package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ha2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ AppCompatAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ SwitchCompat w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public ha2(Object obj, View view, int i, AppCompatAutoCompleteTextView appCompatAutoCompleteTextView, View view2, ImageView imageView, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, View view3, LinearLayout linearLayout, SwitchCompat switchCompat, FlexibleTextView flexibleTextView2, View view4) {
        super(obj, view, i);
        this.q = appCompatAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleEditText;
        this.t = flexibleTextView;
        this.u = rTLImageView;
        this.v = view3;
        this.w = switchCompat;
        this.x = flexibleTextView2;
    }
}
