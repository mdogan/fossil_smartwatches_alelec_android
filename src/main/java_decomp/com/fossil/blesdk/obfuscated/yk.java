package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yk {
    @DexIgnore
    public static yk e;
    @DexIgnore
    public sk a;
    @DexIgnore
    public tk b;
    @DexIgnore
    public wk c;
    @DexIgnore
    public xk d;

    @DexIgnore
    public yk(Context context, am amVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = new sk(applicationContext, amVar);
        this.b = new tk(applicationContext, amVar);
        this.c = new wk(applicationContext, amVar);
        this.d = new xk(applicationContext, amVar);
    }

    @DexIgnore
    public static synchronized yk a(Context context, am amVar) {
        yk ykVar;
        synchronized (yk.class) {
            if (e == null) {
                e = new yk(context, amVar);
            }
            ykVar = e;
        }
        return ykVar;
    }

    @DexIgnore
    public tk b() {
        return this.b;
    }

    @DexIgnore
    public wk c() {
        return this.c;
    }

    @DexIgnore
    public xk d() {
        return this.d;
    }

    @DexIgnore
    public sk a() {
        return this.a;
    }
}
