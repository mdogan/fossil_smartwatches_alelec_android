package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tm1 extends kc1 implements sm1 {
    @DexIgnore
    public tm1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    public final tn0 a(tn0 tn0, int i, int i2, String str, int i3) throws RemoteException {
        Parcel o = o();
        lc1.a(o, tn0);
        o.writeInt(i);
        o.writeInt(i2);
        o.writeString(str);
        o.writeInt(i3);
        Parcel a = a(1, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
