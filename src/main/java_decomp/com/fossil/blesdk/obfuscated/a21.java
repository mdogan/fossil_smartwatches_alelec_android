package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a21 implements Parcelable.Creator<z11> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ap0 ap0 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                ap0 = (ap0) SafeParcelReader.a(parcel, a, ap0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new z11(ap0);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new z11[i];
    }
}
