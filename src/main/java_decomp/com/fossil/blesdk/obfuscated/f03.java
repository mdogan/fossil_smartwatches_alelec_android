package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f03 implements Factory<b03> {
    @DexIgnore
    public static b03 a(d03 d03) {
        b03 c = d03.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
