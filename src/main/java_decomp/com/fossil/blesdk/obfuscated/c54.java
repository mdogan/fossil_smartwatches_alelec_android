package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c54<T> extends b54<T> {
    @DexIgnore
    public T b;

    @DexIgnore
    public c54() {
        this((d54) null);
    }

    @DexIgnore
    public T a(Context context) {
        return this.b;
    }

    @DexIgnore
    public void b(Context context, T t) {
        this.b = t;
    }

    @DexIgnore
    public c54(d54<T> d54) {
        super(d54);
    }
}
