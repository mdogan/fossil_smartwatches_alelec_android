package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hr2 extends CoroutineUseCase<d, e, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((rd4) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ c f; // = new c();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hr2.g;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements BleCommandResultManager.b {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wd4.b(communicateMode, "communicateMode");
            wd4.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(hr2.h.a(), "Inside .onReceive");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal());
            if (hr2.this.e() && wd4.a((Object) hr2.this.d(), (Object) stringExtra) && intExtra == CommunicateMode.READ_RSSI.ordinal()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = hr2.h.a();
                local.d(a2, "onReceive - blePhase: " + intExtra + " - deviceId: " + stringExtra + " - mIsExecuted: " + hr2.this.e());
                hr2.this.a(false);
                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal() || intent.getExtras() == null) {
                    FLogger.INSTANCE.getLocal().e(hr2.h.a(), "Inside .onReceive return error");
                    hr2.this.a(new b());
                    return;
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int i = extras.getInt("rssi", 0);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = hr2.h.a();
                    local2.d(a3, "Inside .onReceive return rssi=" + i);
                    hr2.this.a(new e(i));
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wd4.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public e(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = hr2.class.getSimpleName();
        wd4.a((Object) simpleName, "GetRssi::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    public final String d() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        wd4.d("mDeviceId");
        throw null;
    }

    @DexIgnore
    public final boolean e() {
        return this.d;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0041 A[SYNTHETIC, Splitter:B:16:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025 A[Catch:{ Exception -> 0x0054 }] */
    public Object a(d dVar, kc4<Object> kc4) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(g, "Inside .run");
            this.d = true;
            if (dVar != null) {
                str = dVar.a();
                if (str != null) {
                    this.e = str;
                    if (PortfolioApp.W.b() == null) {
                        IButtonConnectivity b2 = PortfolioApp.W.b();
                        String str2 = null;
                        if (b2 != null) {
                            if (dVar != null) {
                                str2 = dVar.a();
                            }
                            return pc4.a(b2.deviceGetRssi(str2));
                        }
                        wd4.a();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().e(g, "Inside .run ButtonApi is null");
                    return new b();
                }
            }
            str = "";
            this.e = str;
            if (PortfolioApp.W.b() == null) {
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.e(str3, "Inside .run caught exception=" + e2);
            return new b();
        }
    }
}
