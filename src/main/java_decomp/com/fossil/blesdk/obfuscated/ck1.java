package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ck1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ sl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ wj1 f;

    @DexIgnore
    public ck1(wj1 wj1, sl1 sl1) {
        this.f = wj1;
        this.e = sl1;
    }

    @DexIgnore
    public final void run() {
        lg1 d = this.f.d;
        if (d == null) {
            this.f.d().s().a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            d.c(this.e);
            this.f.C();
        } catch (RemoteException e2) {
            this.f.d().s().a("Failed to send measurementEnabled to the service", e2);
        }
    }
}
