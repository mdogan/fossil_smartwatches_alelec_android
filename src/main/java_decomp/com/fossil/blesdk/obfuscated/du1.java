package com.fossil.blesdk.obfuscated;

import com.google.common.base.Predicates;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class du1 {
    @DexIgnore
    public static /* final */ qt1 a; // = qt1.c(", ").a("null");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements pt1<Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Collection e;

        @DexIgnore
        public a(Collection collection) {
            this.e = collection;
        }

        @DexIgnore
        public Object apply(Object obj) {
            return obj == this.e ? "(this Collection)" : obj;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Object obj) {
        tt1.a(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Collection<?> collection2) {
        return lu1.a(collection2, Predicates.a(collection));
    }

    @DexIgnore
    public static String a(Collection<?> collection) {
        StringBuilder a2 = a(collection.size());
        a2.append('[');
        a.a(a2, (Iterable<?>) lu1.a(collection, new a(collection)));
        a2.append(']');
        return a2.toString();
    }

    @DexIgnore
    public static StringBuilder a(int i) {
        cu1.a(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824));
    }

    @DexIgnore
    public static <T> Collection<T> a(Iterable<T> iterable) {
        return (Collection) iterable;
    }
}
