package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.engine.GlideException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pp<DataType, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Class<DataType> a;
    @DexIgnore
    public /* final */ List<? extends no<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ hu<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ h8<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface a<ResourceType> {
        @DexIgnore
        bq<ResourceType> a(bq<ResourceType> bqVar);
    }

    @DexIgnore
    public pp(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends no<DataType, ResourceType>> list, hu<ResourceType, Transcode> huVar, h8<List<Throwable>> h8Var) {
        this.a = cls;
        this.b = list;
        this.c = huVar;
        this.d = h8Var;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public bq<Transcode> a(uo<DataType> uoVar, int i, int i2, mo moVar, a<ResourceType> aVar) throws GlideException {
        return this.c.a(aVar.a(a(uoVar, i, i2, moVar)), moVar);
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }

    @DexIgnore
    public final bq<ResourceType> a(uo<DataType> uoVar, int i, int i2, mo moVar) throws GlideException {
        List<Throwable> a2 = this.d.a();
        uw.a(a2);
        List list = a2;
        try {
            return a(uoVar, i, i2, moVar, (List<Throwable>) list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final bq<ResourceType> a(uo<DataType> uoVar, int i, int i2, mo moVar, List<Throwable> list) throws GlideException {
        int size = this.b.size();
        bq<ResourceType> bqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            no noVar = (no) this.b.get(i3);
            try {
                if (noVar.a(uoVar.b(), moVar)) {
                    bqVar = noVar.a(uoVar.b(), i, i2, moVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + noVar, e2);
                }
                list.add(e2);
            }
            if (bqVar != null) {
                break;
            }
        }
        if (bqVar != null) {
            return bqVar;
        }
        throw new GlideException(this.e, (List<Throwable>) new ArrayList(list));
    }
}
