package com.fossil.blesdk.obfuscated;

import io.reactivex.BackpressureStrategy;
import io.reactivex.internal.observers.LambdaObserver;
import io.reactivex.internal.operators.observable.ObservableObserveOn;
import io.reactivex.internal.operators.observable.ObservableRetryPredicate;
import io.reactivex.internal.operators.observable.ObservableSubscribeOn;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class z84<T> implements a94<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[BackpressureStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[BackpressureStrategy.DROP.ordinal()] = 1;
            a[BackpressureStrategy.LATEST.ordinal()] = 2;
            a[BackpressureStrategy.MISSING.ordinal()] = 3;
            try {
                a[BackpressureStrategy.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public static <T> z84<T> a(T t) {
        v94.a(t, "The item is null");
        return ta4.a(new ma4(t));
    }

    @DexIgnore
    public static int d() {
        return v84.d();
    }

    @DexIgnore
    public final w84<T> b() {
        return ta4.a(new na4(this));
    }

    @DexIgnore
    public abstract void b(b94<? super T> b94);

    @DexIgnore
    public final d94<T> c() {
        return ta4.a(new oa4(this, null));
    }

    @DexIgnore
    public final z84<T> b(c94 c94) {
        v94.a(c94, "scheduler is null");
        return ta4.a(new ObservableSubscribeOn(this, c94));
    }

    @DexIgnore
    public final s84 a() {
        return ta4.a((s84) new la4(this));
    }

    @DexIgnore
    public final z84<T> a(c94 c94) {
        return a(c94, false, d());
    }

    @DexIgnore
    public final z84<T> a(c94 c94, boolean z, int i) {
        v94.a(c94, "scheduler is null");
        v94.a(i, "bufferSize");
        return ta4.a(new ObservableObserveOn(this, c94, z, i));
    }

    @DexIgnore
    public final z84<T> a(long j) {
        return a(j, (s94<? super Throwable>) u94.a());
    }

    @DexIgnore
    public final z84<T> a(long j, s94<? super Throwable> s94) {
        if (j >= 0) {
            v94.a(s94, "predicate is null");
            return ta4.a(new ObservableRetryPredicate(this, j, s94));
        }
        throw new IllegalArgumentException("times >= 0 required but it was " + j);
    }

    @DexIgnore
    public final j94 a(p94<? super T> p94, p94<? super Throwable> p942) {
        return a(p94, p942, u94.a, u94.b());
    }

    @DexIgnore
    public final j94 a(p94<? super T> p94, p94<? super Throwable> p942, m94 m94, p94<? super j94> p943) {
        v94.a(p94, "onNext is null");
        v94.a(p942, "onError is null");
        v94.a(m94, "onComplete is null");
        v94.a(p943, "onSubscribe is null");
        LambdaObserver lambdaObserver = new LambdaObserver(p94, p942, m94, p943);
        a(lambdaObserver);
        return lambdaObserver;
    }

    @DexIgnore
    public final void a(b94<? super T> b94) {
        v94.a(b94, "observer is null");
        try {
            b94<? super Object> a2 = ta4.a(this, b94);
            v94.a(a2, "Plugin returned null Observer");
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            l94.b(th);
            ta4.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    @DexIgnore
    public final v84<T> a(BackpressureStrategy backpressureStrategy) {
        fa4 fa4 = new fa4(this);
        int i = a.a[backpressureStrategy.ordinal()];
        if (i == 1) {
            return fa4.b();
        }
        if (i == 2) {
            return fa4.c();
        }
        if (i == 3) {
            return fa4;
        }
        if (i != 4) {
            return fa4.a();
        }
        return ta4.a(new ia4(fa4));
    }
}
