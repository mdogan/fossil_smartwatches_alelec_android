package com.fossil.blesdk.obfuscated;

import android.widget.RadioGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class ws3 implements RadioGroup.OnCheckedChangeListener {
    @DexIgnore
    private /* final */ /* synthetic */ xs3 a;
    @DexIgnore
    private /* final */ /* synthetic */ Integer b;

    @DexIgnore
    public /* synthetic */ ws3(xs3 xs3, Integer num) {
        this.a = xs3;
        this.b = num;
    }

    @DexIgnore
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.a.a(this.b, radioGroup, i);
    }
}
