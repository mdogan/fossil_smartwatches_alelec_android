package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qx2 extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public b(String str) {
            wd4.b(str, "message");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        public d(List<? extends ContactGroup> list) {
            wd4.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = qx2.class.getSimpleName();
        wd4.a((Object) simpleName, "GetAllContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return d;
    }

    @DexIgnore
    public Object a(c cVar, kc4<Object> kc4) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = en2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups != null) {
            return new d(allContactGroups);
        }
        return new b("Get all contact group failed");
    }
}
