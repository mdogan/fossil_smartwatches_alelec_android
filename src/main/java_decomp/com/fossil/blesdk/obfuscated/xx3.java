package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.jy3;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xx3 extends jy3 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public xx3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(hy3 hy3) {
        return "content".equals(hy3.d.getScheme());
    }

    @DexIgnore
    public InputStream c(hy3 hy3) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(hy3.d);
    }

    @DexIgnore
    public jy3.a a(hy3 hy3, int i) throws IOException {
        return new jy3.a(c(hy3), Picasso.LoadedFrom.DISK);
    }
}
