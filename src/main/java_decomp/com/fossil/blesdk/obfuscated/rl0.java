package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.blesdk.obfuscated.rj0;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rl0 extends rj0 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<rj0.a, sl0> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ em0 f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public rl0(Context context) {
        this.d = context.getApplicationContext();
        this.e = new cz0(context.getMainLooper(), this);
        this.f = em0.a();
        this.g = 5000;
        this.h = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    public final boolean a(rj0.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d2;
        ck0.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            sl0 sl0 = this.c.get(aVar);
            if (sl0 == null) {
                sl0 = new sl0(this, aVar);
                sl0.a(serviceConnection, str);
                sl0.a(str);
                this.c.put(aVar, sl0);
            } else {
                this.e.removeMessages(0, aVar);
                if (!sl0.a(serviceConnection)) {
                    sl0.a(serviceConnection, str);
                    int c2 = sl0.c();
                    if (c2 == 1) {
                        serviceConnection.onServiceConnected(sl0.b(), sl0.a());
                    } else if (c2 == 2) {
                        sl0.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = sl0.d();
        }
        return d2;
    }

    @DexIgnore
    public final void b(rj0.a aVar, ServiceConnection serviceConnection, String str) {
        ck0.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            sl0 sl0 = this.c.get(aVar);
            if (sl0 == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (sl0.a(serviceConnection)) {
                sl0.b(serviceConnection, str);
                if (sl0.e()) {
                    this.e.sendMessageDelayed(this.e.obtainMessage(0, aVar), this.g);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.c) {
                rj0.a aVar = (rj0.a) message.obj;
                sl0 sl0 = this.c.get(aVar);
                if (sl0 != null && sl0.e()) {
                    if (sl0.d()) {
                        sl0.b("GmsClientSupervisor");
                    }
                    this.c.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.c) {
                rj0.a aVar2 = (rj0.a) message.obj;
                sl0 sl02 = this.c.get(aVar2);
                if (sl02 != null && sl02.c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = sl02.b();
                    if (b == null) {
                        b = aVar2.a();
                    }
                    if (b == null) {
                        b = new ComponentName(aVar2.b(), "unknown");
                    }
                    sl02.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
