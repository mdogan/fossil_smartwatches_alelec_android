package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class af2 extends ze2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public long B;

    /*
    static {
        D.put(R.id.customToolbar, 1);
        D.put(R.id.iv_back, 2);
        D.put(R.id.input_old_pass, 3);
        D.put(R.id.et_old_pass, 4);
        D.put(R.id.input_new_pass, 5);
        D.put(R.id.et_new_pass, 6);
        D.put(R.id.tv_error_check_combine, 7);
        D.put(R.id.tv_error_check_character, 8);
        D.put(R.id.input_repeat_pass, 9);
        D.put(R.id.et_repeat_pass, 10);
        D.put(R.id.save, 11);
    }
    */

    @DexIgnore
    public af2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 12, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[1], objArr[6], objArr[4], objArr[10], objArr[5], objArr[3], objArr[9], objArr[2], objArr[11], objArr[8], objArr[7]);
        this.B = -1;
        this.A = objArr[0];
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
