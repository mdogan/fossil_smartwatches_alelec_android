package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z41 extends n31 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ sc1 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z41(y41 y41, he0 he0, LocationRequest locationRequest, sc1 sc1) {
        super(he0);
        this.s = locationRequest;
        this.t = sc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar) throws RemoteException {
        ((g41) bVar).a(this.s, (af0<sc1>) bf0.a(this.t, q41.a(), sc1.class.getSimpleName()), (s31) new o31(this));
    }
}
