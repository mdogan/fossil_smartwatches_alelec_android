package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sb4 extends rb4 {
    @DexIgnore
    public static final <T> void a(List<T> list, Comparator<? super T> comparator) {
        wd4.b(list, "$this$sortWith");
        wd4.b(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> void c(List<T> list) {
        wd4.b(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }
}
