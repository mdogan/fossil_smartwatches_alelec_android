package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class us2 extends RecyclerView.g<a> {
    @DexIgnore
    public int a;
    @DexIgnore
    public ArrayList<WatchFaceWrapper> b;
    @DexIgnore
    public c c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ us2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.us2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.us2$a$a  reason: collision with other inner class name */
        public static final class C0105a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0105a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    c b = this.e.d.b();
                    if (b != null) {
                        Object obj = this.e.d.b.get(this.e.getAdapterPosition());
                        wd4.a(obj, "mData[adapterPosition]");
                        b.a((WatchFaceWrapper) obj);
                    }
                }
                a aVar = this.e;
                aVar.d.a(aVar.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(us2 us2, View view) {
            super(view);
            wd4.b(view, "view");
            this.d = us2;
            View findViewById = view.findViewById(R.id.iv_background_preview);
            wd4.a((Object) findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_name);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.v_background_selected);
            wd4.a((Object) findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new C0105a(this));
        }

        @DexIgnore
        public final void a(WatchFaceWrapper watchFaceWrapper, int i) {
            wd4.b(watchFaceWrapper, "watchFaceWrapper");
            Drawable combination = watchFaceWrapper.getCombination();
            if (combination != null) {
                this.a.setBackground(combination);
            } else {
                this.a.setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.nothing_bg));
            }
            this.b.setText(watchFaceWrapper.getName());
            if (i == this.d.a) {
                this.b.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor));
                this.c.setVisibility(0);
                return;
            }
            this.b.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.secondaryText));
            this.c.setVisibility(8);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(WatchFaceWrapper watchFaceWrapper);
    }

    /*
    static {
        new b((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ us2(ArrayList arrayList, c cVar, int i, rd4 rd4) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        wd4.b(list, "data");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final c b() {
        return this.c;
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_diana_background, viewGroup, false);
        wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026ackground, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public us2(ArrayList<WatchFaceWrapper> arrayList, c cVar) {
        wd4.b(arrayList, "mData");
        this.b = arrayList;
        this.c = cVar;
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wd4.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchFaceWrapper watchFaceWrapper = this.b.get(i);
            wd4.a((Object) watchFaceWrapper, "mData[position]");
            aVar.a(watchFaceWrapper, i);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wd4.b(cVar, "listener");
        this.c = cVar;
    }
}
