package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wy2 extends CoroutineUseCase<CoroutineUseCase.b, a, CoroutineUseCase.a> {
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Context e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public a(List<AppWrapper> list) {
            wd4.b(list, "apps");
            tt1.a(list, "apps cannot be null!", new Object[0]);
            wd4.a((Object) list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    @DexIgnore
    public wy2(Context context) {
        wd4.b(context, "context");
        String simpleName = wy2.class.getSimpleName();
        wd4.a((Object) simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
        tt1.a(context);
        wd4.a((Object) context, "checkNotNull(context)");
        this.e = context;
    }

    @DexIgnore
    public Object a(CoroutineUseCase.b bVar, kc4<Object> kc4) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List<AppFilter> allAppFilters = en2.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.a> it = AppHelper.f.b(this.e).iterator();
        while (it.hasNext()) {
            AppHelper.a next = it.next();
            if (TextUtils.isEmpty(next.b()) || !cg4.b(next.b(), this.e.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), pc4.a(false));
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    wd4.a((Object) next2, "appFilter");
                    if (wd4.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        sb4.c(linkedList);
        return new a(linkedList);
    }

    @DexIgnore
    public String c() {
        return this.d;
    }
}
