package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k73 extends jc {
    @DexIgnore
    public MutableLiveData<b> c; // = new MutableLiveData<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(Integer num, boolean z) {
            this.a = num;
            this.b = z;
        }

        @DexIgnore
        public final Integer a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (wd4.a((Object) this.a, (Object) bVar.a)) {
                        if (this.b == bVar.b) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.a;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            return hashCode + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(tutorialResource=" + this.a + ", showLoading=" + this.b + ")";
        }
    }

    /*
    static {
        new a((rd4) null);
        wd4.a((Object) k73.class.getSimpleName(), "CustomizeTutorialViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public static /* synthetic */ void a(k73 k73, Integer num, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            z = false;
        }
        k73.a(num, z);
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "watchAppId");
        a(this, ql2.d.c(str), false, 2, (Object) null);
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.c;
    }

    @DexIgnore
    public final void a(Integer num, boolean z) {
        this.c.a(new b(num, z));
    }
}
