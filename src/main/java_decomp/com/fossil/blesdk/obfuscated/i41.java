package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i41 extends y31 {
    @DexIgnore
    public ve0<wc1> e;

    @DexIgnore
    public i41(ve0<wc1> ve0) {
        ck0.a(ve0 != null, (Object) "listener can't be null.");
        this.e = ve0;
    }

    @DexIgnore
    public final void a(wc1 wc1) throws RemoteException {
        this.e.a(wc1);
        this.e = null;
    }
}
