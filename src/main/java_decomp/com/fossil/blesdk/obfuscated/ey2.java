package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ey2 implements MembersInjector<dy2> {
    @DexIgnore
    public static void a(dy2 dy2, InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
        dy2.n = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public static void a(dy2 dy2, RemindTimePresenter remindTimePresenter) {
        dy2.o = remindTimePresenter;
    }
}
