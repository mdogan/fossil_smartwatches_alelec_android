package com.fossil.blesdk.obfuscated;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gj0 implements Parcelable.Creator<DataHolder> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String[] strArr = null;
        CursorWindow[] cursorWindowArr = null;
        Bundle bundle = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                strArr = SafeParcelReader.g(parcel, a);
            } else if (a2 == 2) {
                cursorWindowArr = SafeParcelReader.b(parcel, a, CursorWindow.CREATOR);
            } else if (a2 == 3) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 4) {
                bundle = SafeParcelReader.a(parcel, a);
            } else if (a2 != 1000) {
                SafeParcelReader.v(parcel, a);
            } else {
                i = SafeParcelReader.q(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        DataHolder dataHolder = new DataHolder(i, strArr, cursorWindowArr, i2, bundle);
        dataHolder.J();
        return dataHolder;
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataHolder[i];
    }
}
