package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yi0 implements Parcelable.Creator<Status> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        String str = null;
        PendingIntent pendingIntent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) SafeParcelReader.a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 1000) {
                SafeParcelReader.v(parcel, a);
            } else {
                i = SafeParcelReader.q(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new Status(i, i2, str, pendingIntent);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new Status[i];
    }
}
