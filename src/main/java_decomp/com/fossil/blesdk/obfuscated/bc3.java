package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bc3 extends as2 implements ac3, ot2, ls2 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<db2> j;
    @DexIgnore
    public zb3 k;
    @DexIgnore
    public nt2 l;
    @DexIgnore
    public HeartRateOverviewFragment m;
    @DexIgnore
    public cu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bc3.p;
        }

        @DexIgnore
        public final bc3 b() {
            return new bc3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cu3 {
        @DexIgnore
        public /* final */ /* synthetic */ bc3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bc3 bc3, LinearLayoutManager linearLayoutManager, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager2);
            this.e = bc3;
        }

        @DexIgnore
        public void a(int i) {
            bc3.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = bc3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "DashboardHeartRateFragme\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ zb3 a(bc3 bc3) {
        zb3 zb3 = bc3.k;
        if (zb3 != null) {
            return zb3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (isVisible()) {
            ur3<db2> ur3 = this.j;
            if (ur3 != null) {
                RecyclerView recyclerView = null;
                if (ur3 != null) {
                    db2 a2 = ur3.a();
                    if (a2 != null) {
                        recyclerView = a2.q;
                    }
                    if (recyclerView != null) {
                        RecyclerView.ViewHolder c = recyclerView.c(0);
                        if (c != null) {
                            View view = c.itemView;
                            if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                return;
                            }
                        }
                    }
                    if (recyclerView != null) {
                        recyclerView.j(0);
                    }
                    cu3 cu3 = this.n;
                    if (cu3 != null) {
                        cu3.a();
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            wd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        cu3 cu3 = this.n;
        if (cu3 != null) {
            cu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        db2 db2 = (db2) ra.a(layoutInflater, R.layout.fragment_dashboard_heartrate, viewGroup, false, O0());
        this.m = (HeartRateOverviewFragment) getChildFragmentManager().a("HeartRateOverviewFragment");
        if (this.m == null) {
            this.m = new HeartRateOverviewFragment();
        }
        mt2 mt2 = new mt2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wd4.a((Object) childFragmentManager, "childFragmentManager");
        HeartRateOverviewFragment heartRateOverviewFragment = this.m;
        if (heartRateOverviewFragment != null) {
            this.l = new nt2(mt2, c, this, childFragmentManager, heartRateOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            this.n = new b(this, linearLayoutManager, linearLayoutManager);
            m73 m73 = new m73(linearLayoutManager.M());
            Context context = getContext();
            if (context != null) {
                Drawable c2 = k6.c(context, R.drawable.bg_item_decoration_dashboard_line_1w);
                if (c2 != null) {
                    wd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                    m73.a(c2);
                    RecyclerView recyclerView = db2.q;
                    wd4.a((Object) recyclerView, "this");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    nt2 nt2 = this.l;
                    if (nt2 != null) {
                        recyclerView.setAdapter(nt2);
                        cu3 cu3 = this.n;
                        if (cu3 != null) {
                            recyclerView.a((RecyclerView.q) cu3);
                            recyclerView.setItemViewCacheSize(0);
                            recyclerView.a((RecyclerView.l) m73);
                            if (recyclerView.getItemAnimator() instanceof bf) {
                                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                                if (itemAnimator != null) {
                                    ((bf) itemAnimator).setSupportsChangeAnimations(false);
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator");
                                }
                            }
                            zb3 zb3 = this.k;
                            if (zb3 != null) {
                                zb3.h();
                                this.j = new ur3<>(this, db2);
                                ur3<db2> ur3 = this.j;
                                if (ur3 != null) {
                                    db2 a2 = ur3.a();
                                    if (a2 != null) {
                                        wd4.a((Object) a2, "mBinding.get()!!");
                                        return a2.d();
                                    }
                                    wd4.a();
                                    throw null;
                                }
                                wd4.d("mBinding");
                                throw null;
                            }
                            wd4.d("mPresenter");
                            throw null;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.d("mDashboardHeartRatesAdapter");
                    throw null;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(p, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        zb3 zb3 = this.k;
        if (zb3 != null) {
            zb3.i();
            super.onDestroyView();
            N0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        zb3 zb3 = this.k;
        if (zb3 != null) {
            zb3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        zb3 zb3 = this.k;
        if (zb3 != null) {
            zb3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("heart_rate_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ju3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            ju3 ju3 = (ju3) a2;
        }
    }

    @DexIgnore
    public void a(zb3 zb3) {
        wd4.b(zb3, "presenter");
        this.k = zb3;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wd4.b(date, "startWeekDate");
        wd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    public void b(rd<DailyHeartRateSummary> rdVar) {
        nt2 nt2 = this.l;
        if (nt2 != null) {
            nt2.c(rdVar);
        } else {
            wd4.d("mDashboardHeartRatesAdapter");
            throw null;
        }
    }
}
