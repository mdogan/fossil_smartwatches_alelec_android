package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.CompositeGeneratedAdaptersObserver;
import androidx.lifecycle.FullLifecycleObserverAdapter;
import androidx.lifecycle.ReflectiveGenericLifecycleObserver;
import androidx.lifecycle.SingleGeneratedAdapterObserver;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ac {
    @DexIgnore
    public static Map<Class, Integer> a; // = new HashMap();
    @DexIgnore
    public static Map<Class, List<Constructor<? extends tb>>> b; // = new HashMap();

    @DexIgnore
    public static wb a(Object obj) {
        boolean z = obj instanceof wb;
        boolean z2 = obj instanceof sb;
        if (z && z2) {
            return new FullLifecycleObserverAdapter((sb) obj, (wb) obj);
        }
        if (z2) {
            return new FullLifecycleObserverAdapter((sb) obj, (wb) null);
        }
        if (z) {
            return (wb) obj;
        }
        Class<?> cls = obj.getClass();
        if (b(cls) != 2) {
            return new ReflectiveGenericLifecycleObserver(obj);
        }
        List list = b.get(cls);
        if (list.size() == 1) {
            return new SingleGeneratedAdapterObserver(a((Constructor) list.get(0), obj));
        }
        tb[] tbVarArr = new tb[list.size()];
        for (int i = 0; i < list.size(); i++) {
            tbVarArr[i] = a((Constructor) list.get(i), obj);
        }
        return new CompositeGeneratedAdaptersObserver(tbVarArr);
    }

    @DexIgnore
    public static int b(Class<?> cls) {
        Integer num = a.get(cls);
        if (num != null) {
            return num.intValue();
        }
        int d = d(cls);
        a.put(cls, Integer.valueOf(d));
        return d;
    }

    @DexIgnore
    public static boolean c(Class<?> cls) {
        return cls != null && xb.class.isAssignableFrom(cls);
    }

    @DexIgnore
    public static int d(Class<?> cls) {
        if (cls.getCanonicalName() == null) {
            return 1;
        }
        Constructor<? extends tb> a2 = a(cls);
        if (a2 != null) {
            b.put(cls, Collections.singletonList(a2));
            return 2;
        } else if (ob.c.c(cls)) {
            return 1;
        } else {
            Class<? super Object> superclass = cls.getSuperclass();
            ArrayList arrayList = null;
            if (c(superclass)) {
                if (b(superclass) == 1) {
                    return 1;
                }
                arrayList = new ArrayList(b.get(superclass));
            }
            for (Class cls2 : cls.getInterfaces()) {
                if (c(cls2)) {
                    if (b(cls2) == 1) {
                        return 1;
                    }
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.addAll(b.get(cls2));
                }
            }
            if (arrayList == null) {
                return 1;
            }
            b.put(cls, arrayList);
            return 2;
        }
    }

    @DexIgnore
    public static tb a(Constructor<? extends tb> constructor, Object obj) {
        try {
            return (tb) constructor.newInstance(new Object[]{obj});
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }

    @DexIgnore
    public static Constructor<? extends tb> a(Class<?> cls) {
        try {
            Package packageR = cls.getPackage();
            String canonicalName = cls.getCanonicalName();
            String name = packageR != null ? packageR.getName() : "";
            if (!name.isEmpty()) {
                canonicalName = canonicalName.substring(name.length() + 1);
            }
            String a2 = a(canonicalName);
            if (!name.isEmpty()) {
                a2 = name + CodelessMatcher.CURRENT_CLASS_NAME + a2;
            }
            Constructor<?> declaredConstructor = Class.forName(a2).getDeclaredConstructor(new Class[]{cls});
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return declaredConstructor;
        } catch (ClassNotFoundException unused) {
            return null;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public static String a(String str) {
        return str.replace(CodelessMatcher.CURRENT_CLASS_NAME, "_") + "_LifecycleAdapter";
    }
}
