package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j42 implements Factory<i42> {
    @DexIgnore
    public static /* final */ j42 a; // = new j42();

    @DexIgnore
    public static j42 a() {
        return a;
    }

    @DexIgnore
    public static i42 b() {
        return new i42();
    }

    @DexIgnore
    public i42 get() {
        return b();
    }
}
