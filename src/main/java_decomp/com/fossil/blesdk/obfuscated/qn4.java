package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qn4 extends qm4 {
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ xo4 h;

    @DexIgnore
    public qn4(String str, long j, xo4 xo4) {
        this.f = str;
        this.g = j;
        this.h = xo4;
    }

    @DexIgnore
    public long C() {
        return this.g;
    }

    @DexIgnore
    public mm4 D() {
        String str = this.f;
        if (str != null) {
            return mm4.b(str);
        }
        return null;
    }

    @DexIgnore
    public xo4 E() {
        return this.h;
    }
}
