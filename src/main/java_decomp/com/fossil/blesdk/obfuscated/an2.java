package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.manager.LightAndHapticsManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class an2 implements MembersInjector<LightAndHapticsManager> {
    @DexIgnore
    public static void a(LightAndHapticsManager lightAndHapticsManager, DeviceRepository deviceRepository) {
        lightAndHapticsManager.a = deviceRepository;
    }

    @DexIgnore
    public static void a(LightAndHapticsManager lightAndHapticsManager, fn2 fn2) {
        lightAndHapticsManager.b = fn2;
    }
}
