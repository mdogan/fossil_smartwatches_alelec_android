package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fh4 {
    @DexIgnore
    public static /* final */ boolean a;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r0.equals("on") != false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r0.equals("") != false) goto L_0x0033;
     */
    /*
    static {
        boolean z;
        String a2 = qk4.a("kotlinx.coroutines.scheduler");
        if (a2 != null) {
            int hashCode = a2.hashCode();
            if (hashCode != 0) {
                if (hashCode != 3551) {
                    if (hashCode == 109935 && a2.equals("off")) {
                        z = false;
                        a = z;
                    }
                }
            }
            throw new IllegalStateException(("System property 'kotlinx.coroutines.scheduler' has unrecognized value '" + a2 + '\'').toString());
        }
        z = true;
        a = z;
    }
    */

    @DexIgnore
    public static final gh4 a() {
        return a ? zk4.k : xg4.g;
    }

    @DexIgnore
    public static final CoroutineContext a(lh4 lh4, CoroutineContext coroutineContext) {
        wd4.b(lh4, "$this$newCoroutineContext");
        wd4.b(coroutineContext, "context");
        CoroutineContext plus = lh4.A().plus(coroutineContext);
        CoroutineContext plus2 = oh4.c() ? plus.plus(new jh4(oh4.b().incrementAndGet())) : plus;
        return (plus == zh4.a() || plus.get(lc4.b) != null) ? plus2 : plus2.plus(zh4.a());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        if (r4 != null) goto L_0x002a;
     */
    @DexIgnore
    public static final String a(CoroutineContext coroutineContext) {
        String str;
        wd4.b(coroutineContext, "$this$coroutineName");
        if (!oh4.c()) {
            return null;
        }
        jh4 jh4 = (jh4) coroutineContext.get(jh4.f);
        if (jh4 == null) {
            return null;
        }
        kh4 kh4 = (kh4) coroutineContext.get(kh4.f);
        if (kh4 != null) {
            str = kh4.C();
        }
        str = "coroutine";
        return str + '#' + jh4.C();
    }
}
