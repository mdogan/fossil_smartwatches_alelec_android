package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i81 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a();

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static j81 b() {
        if (a != null) {
            try {
                return a("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return j81.c;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000e  */
    public static j81 c() {
        j81 j81;
        if (a != null) {
            try {
                j81 = a("loadGeneratedRegistry");
            } catch (Exception unused) {
            }
            if (j81 == null) {
                j81 = j81.a();
            }
            return j81 != null ? b() : j81;
        }
        j81 = null;
        if (j81 == null) {
        }
        if (j81 != null) {
        }
    }

    @DexIgnore
    public static final j81 a(String str) throws Exception {
        return (j81) a.getDeclaredMethod(str, new Class[0]).invoke((Object) null, new Object[0]);
    }
}
