package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pv {
    @DexIgnore
    boolean a(pv pvVar);

    @DexIgnore
    void c();

    @DexIgnore
    void clear();

    @DexIgnore
    void d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean isRunning();
}
