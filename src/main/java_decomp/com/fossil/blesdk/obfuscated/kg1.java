package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kg1 {
    @DexIgnore
    public static a<String> A; // = a.a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a");
    @DexIgnore
    public static a<Long> B; // = a.a("measurement.upload.backoff_period", 43200000, 43200000);
    @DexIgnore
    public static a<Long> C; // = a.a("measurement.upload.window_interval", 3600000, 3600000);
    @DexIgnore
    public static a<Long> D; // = a.a("measurement.upload.interval", 3600000, 3600000);
    @DexIgnore
    public static a<Long> E; // = a.a("measurement.upload.realtime_upload_interval", (long) ButtonService.CONNECT_TIMEOUT, (long) ButtonService.CONNECT_TIMEOUT);
    @DexIgnore
    public static a<Long> F; // = a.a("measurement.upload.debug_upload_interval", 1000, 1000);
    @DexIgnore
    public static a<Long> G; // = a.a("measurement.upload.minimum_delay", 500, 500);
    @DexIgnore
    public static a<Long> H; // = a.a("measurement.alarm_manager.minimum_interval", 60000, 60000);
    @DexIgnore
    public static a<Long> I; // = a.a("measurement.upload.stale_data_deletion_interval", (long) LogBuilder.MAX_INTERVAL, (long) LogBuilder.MAX_INTERVAL);
    @DexIgnore
    public static a<Long> J; // = a.a("measurement.upload.refresh_blacklisted_config_interval", 604800000, 604800000);
    @DexIgnore
    public static a<Long> K; // = a.a("measurement.upload.initial_upload_delay_time", 15000, 15000);
    @DexIgnore
    public static a<Long> L; // = a.a("measurement.upload.retry_time", 1800000, 1800000);
    @DexIgnore
    public static a<Integer> M; // = a.a("measurement.upload.retry_count", 6, 6);
    @DexIgnore
    public static a<Long> N; // = a.a("measurement.upload.max_queue_time", 2419200000L, 2419200000L);
    @DexIgnore
    public static a<Integer> O; // = a.a("measurement.lifetimevalue.max_currency_tracked", 4, 4);
    @DexIgnore
    public static a<Integer> P; // = a.a("measurement.audience.filter_result_max_count", 200, 200);
    @DexIgnore
    public static a<Long> Q; // = a.a("measurement.service_client.idle_disconnect_millis", 5000, 5000);
    @DexIgnore
    public static a<Integer> R; // = a.a("measurement.experiment.max_ids", 50, 50);
    @DexIgnore
    public static a<Boolean> S; // = a.a("measurement.lifetimevalue.user_engagement_tracking_enabled", true, true);
    @DexIgnore
    public static a<Boolean> T; // = a.a("measurement.audience.complex_param_evaluation", true, true);
    @DexIgnore
    public static a<Boolean> U; // = a.a("measurement.validation.internal_limits_internal_event_params", false, false);
    @DexIgnore
    public static a<Boolean> V; // = a.a("measurement.quality.unsuccessful_update_retry_counter", true, true);
    @DexIgnore
    public static a<Boolean> W; // = a.a("measurement.iid.disable_on_collection_disabled", true, true);
    @DexIgnore
    public static a<Boolean> X; // = a.a("measurement.app_launch.call_only_when_enabled", true, true);
    @DexIgnore
    public static a<Boolean> Y; // = a.a("measurement.run_on_worker_inline", true, false);
    @DexIgnore
    public static a<Boolean> Z; // = a.a("measurement.audience.dynamic_filters", true, true);
    @DexIgnore
    public static vl1 a;
    @DexIgnore
    public static a<Boolean> a0; // = a.a("measurement.reset_analytics.persist_time", false, false);
    @DexIgnore
    public static List<a<Integer>> b; // = new ArrayList();
    @DexIgnore
    public static a<Boolean> b0; // = a.a("measurement.validation.value_and_currency_params", false, false);
    @DexIgnore
    public static List<a<Long>> c; // = new ArrayList();
    @DexIgnore
    public static a<Boolean> c0; // = a.a("measurement.sampling.time_zone_offset_enabled", false, false);
    @DexIgnore
    public static List<a<Boolean>> d; // = new ArrayList();
    @DexIgnore
    public static a<Boolean> d0; // = a.a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false);
    @DexIgnore
    public static List<a<String>> e; // = new ArrayList();
    @DexIgnore
    public static a<Boolean> e0; // = a.a("measurement.fetch_config_with_admob_app_id", true, true);
    @DexIgnore
    public static List<a<Double>> f; // = new ArrayList();
    @DexIgnore
    public static a<Boolean> f0; // = a.a("measurement.client.sessions.session_id_enabled", false, false);
    @DexIgnore
    public static /* final */ h71 g; // = new h71(a71.a("com.google.android.gms.measurement"));
    @DexIgnore
    public static a<Boolean> g0; // = a.a("measurement.service.sessions.session_number_enabled", false, false);
    @DexIgnore
    public static volatile yh1 h;
    @DexIgnore
    public static a<Boolean> h0; // = a.a("measurement.client.sessions.immediate_start_enabled", false, false);
    @DexIgnore
    public static Boolean i;
    @DexIgnore
    public static a<Boolean> i0; // = a.a("measurement.client.sessions.background_sessions_enabled", false, false);
    @DexIgnore
    public static a<Boolean> j; // = a.a("measurement.upload_dsid_enabled", false, false);
    @DexIgnore
    public static a<Boolean> j0; // = a.a("measurement.client.sessions.remove_expired_session_properties_enabled", false, false);
    @DexIgnore
    public static a<String> k; // = a.a("measurement.log_tag", "FA", "FA-SVC");
    @DexIgnore
    public static a<Boolean> k0; // = a.a("measurement.collection.firebase_global_collection_flag_enabled", true, true);
    @DexIgnore
    public static a<Long> l; // = a.a("measurement.ad_id_cache_time", (long) ButtonService.CONNECT_TIMEOUT, (long) ButtonService.CONNECT_TIMEOUT);
    @DexIgnore
    public static a<Boolean> l0; // = a.a("measurement.collection.efficient_engagement_reporting_enabled", false, false);
    @DexIgnore
    public static a<Long> m; // = a.a("measurement.monitoring.sample_period_millis", (long) LogBuilder.MAX_INTERVAL, (long) LogBuilder.MAX_INTERVAL);
    @DexIgnore
    public static a<Boolean> m0; // = a.a("measurement.collection.redundant_engagement_removal_enabled", false, false);
    @DexIgnore
    public static a<Long> n; // = a.a("measurement.config.cache_time", (long) LogBuilder.MAX_INTERVAL, 3600000);
    @DexIgnore
    public static a<Boolean> n0; // = a.a("measurement.collection.init_params_control_enabled", true, true);
    @DexIgnore
    public static a<String> o; // = a.a("measurement.config.url_scheme", Utility.URL_SCHEME, Utility.URL_SCHEME);
    @DexIgnore
    public static a<Boolean> o0; // = a.a("measurement.upload.disable_is_uploader", false, false);
    @DexIgnore
    public static a<String> p; // = a.a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com");
    @DexIgnore
    public static a<Boolean> p0; // = a.a("measurement.experiment.enable_experiment_reporting", false, false);
    @DexIgnore
    public static a<Integer> q; // = a.a("measurement.upload.max_bundles", 100, 100);
    @DexIgnore
    public static a<Boolean> q0; // = a.a("measurement.collection.log_event_and_bundle_v2", true, true);
    @DexIgnore
    public static a<Integer> r; // = a.a("measurement.upload.max_batch_size", 65536, 65536);
    @DexIgnore
    public static a<Boolean> r0; // = a.a("measurement.collection.null_empty_event_name_fix", true, true);
    @DexIgnore
    public static a<Integer> s; // = a.a("measurement.upload.max_bundle_size", 65536, 65536);
    @DexIgnore
    public static a<Integer> t; // = a.a("measurement.upload.max_events_per_bundle", 1000, 1000);
    @DexIgnore
    public static a<Integer> u; // = a.a("measurement.upload.max_events_per_day", 100000, 100000);
    @DexIgnore
    public static a<Integer> v; // = a.a("measurement.upload.max_error_events_per_day", 1000, 1000);
    @DexIgnore
    public static a<Integer> w; // = a.a("measurement.upload.max_public_events_per_day", 50000, 50000);
    @DexIgnore
    public static a<Integer> x; // = a.a("measurement.upload.max_conversions_per_day", 500, 500);
    @DexIgnore
    public static a<Integer> y; // = a.a("measurement.upload.max_realtime_events_per_day", 10, 10);
    @DexIgnore
    public static a<Integer> z; // = a.a("measurement.store.max_stored_events_per_app", 100000, 100000);

    /*
    static {
        a.a("measurement.log_third_party_store_events_enabled", false, false);
        a.a("measurement.log_installs_enabled", false, false);
        a.a("measurement.log_upgrades_enabled", false, false);
        a.a("measurement.log_androidId_enabled", false, false);
        a.a("measurement.test.boolean_flag", false, false);
        a.a("measurement.test.string_flag", "---", "---");
        a.a("measurement.test.long_flag", -1, -1);
        a.a("measurement.test.int_flag", -2, -2);
        a.a("measurement.test.double_flag", -3.0d, -3.0d);
        a.a("measurement.service.sessions.session_number_backfill_enabled", false, false);
        a.a("measurement.remove_app_instance_id_cache_enabled", true, true);
    }
    */

    @DexIgnore
    public static Map<String, String> a(Context context) {
        return p61.a(context.getContentResolver(), a71.a("com.google.android.gms.measurement")).a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<V> {
        @DexIgnore
        public b71<V> a;
        @DexIgnore
        public /* final */ V b;
        @DexIgnore
        public volatile V c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public a(String str, V v, V v2) {
            this.d = str;
            this.b = v;
        }

        @DexIgnore
        public static a<Boolean> a(String str, boolean z, boolean z2) {
            a<Boolean> aVar = new a<>(str, Boolean.valueOf(z), Boolean.valueOf(z2));
            kg1.d.add(aVar);
            return aVar;
        }

        @DexIgnore
        public static void c() {
            synchronized (a.class) {
                if (!vl1.a()) {
                    vl1 vl1 = kg1.a;
                    try {
                        for (a next : kg1.d) {
                            next.c = next.a.a();
                        }
                        for (a next2 : kg1.e) {
                            next2.c = next2.a.a();
                        }
                        for (a next3 : kg1.c) {
                            next3.c = next3.a.a();
                        }
                        for (a next4 : kg1.b) {
                            next4.c = next4.a.a();
                        }
                        for (a next5 : kg1.f) {
                            next5.c = next5.a.a();
                        }
                    } catch (SecurityException e) {
                        kg1.a((Exception) e);
                    }
                } else {
                    throw new IllegalStateException("Tried to refresh flag cache on main thread or on package side.");
                }
            }
        }

        @DexIgnore
        public static void e() {
            synchronized (a.class) {
                for (a next : kg1.d) {
                    h71 a2 = kg1.g;
                    String str = next.d;
                    vl1 vl1 = kg1.a;
                    next.a = a2.a(str, ((Boolean) next.b).booleanValue());
                }
                for (a next2 : kg1.e) {
                    h71 a3 = kg1.g;
                    String str2 = next2.d;
                    vl1 vl12 = kg1.a;
                    next2.a = a3.a(str2, (String) next2.b);
                }
                for (a next3 : kg1.c) {
                    h71 a4 = kg1.g;
                    String str3 = next3.d;
                    vl1 vl13 = kg1.a;
                    next3.a = a4.a(str3, ((Long) next3.b).longValue());
                }
                for (a next4 : kg1.b) {
                    h71 a5 = kg1.g;
                    String str4 = next4.d;
                    vl1 vl14 = kg1.a;
                    next4.a = a5.a(str4, ((Integer) next4.b).intValue());
                }
                for (a next5 : kg1.f) {
                    h71 a6 = kg1.g;
                    String str5 = next5.d;
                    vl1 vl15 = kg1.a;
                    next5.a = a6.a(str5, ((Double) next5.b).doubleValue());
                }
            }
        }

        @DexIgnore
        public final String b() {
            return this.d;
        }

        @DexIgnore
        public static a<String> a(String str, String str2, String str3) {
            a<String> aVar = new a<>(str, str2, str3);
            kg1.e.add(aVar);
            return aVar;
        }

        @DexIgnore
        public static a<Long> a(String str, long j, long j2) {
            a<Long> aVar = new a<>(str, Long.valueOf(j), Long.valueOf(j2));
            kg1.c.add(aVar);
            return aVar;
        }

        @DexIgnore
        public static a<Integer> a(String str, int i, int i2) {
            a<Integer> aVar = new a<>(str, Integer.valueOf(i), Integer.valueOf(i2));
            kg1.b.add(aVar);
            return aVar;
        }

        @DexIgnore
        public static a<Double> a(String str, double d2, double d3) {
            Double valueOf = Double.valueOf(-3.0d);
            a<Double> aVar = new a<>(str, valueOf, valueOf);
            kg1.f.add(aVar);
            return aVar;
        }

        @DexIgnore
        public final V a() {
            if (kg1.a == null) {
                return this.b;
            }
            if (vl1.a()) {
                return this.c == null ? this.b : this.c;
            }
            c();
            try {
                return this.a.a();
            } catch (SecurityException e) {
                kg1.a((Exception) e);
                return this.a.b();
            }
        }

        @DexIgnore
        public final V a(V v) {
            if (v != null) {
                return v;
            }
            if (kg1.a == null) {
                return this.b;
            }
            if (vl1.a()) {
                return this.c == null ? this.b : this.c;
            }
            c();
            try {
                return this.a.a();
            } catch (SecurityException e) {
                kg1.a((Exception) e);
                return this.a.b();
            }
        }
    }

    @DexIgnore
    public static void a(yh1 yh1) {
        h = yh1;
    }

    @DexIgnore
    public static void a(Exception exc) {
        if (h != null) {
            Context context = h.getContext();
            if (i == null) {
                i = Boolean.valueOf(zd0.a().a(context, (int) ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE) == 0);
            }
            if (i.booleanValue()) {
                h.d().s().a("Got Exception on PhenotypeFlag.get on Play device", exc);
            }
        }
    }

    @DexIgnore
    public static void a(vl1 vl1) {
        a = vl1;
        a.e();
    }
}
