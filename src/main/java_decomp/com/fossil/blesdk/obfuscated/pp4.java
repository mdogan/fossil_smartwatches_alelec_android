package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pp4 extends OutputStream {
    @DexIgnore
    public static /* final */ byte[] j; // = new byte[0];
    @DexIgnore
    public /* final */ List<byte[]> e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public byte[] h;
    @DexIgnore
    public int i;

    @DexIgnore
    public pp4() {
        this(1024);
    }

    @DexIgnore
    public final void b(int i2) {
        if (this.f < this.e.size() - 1) {
            this.g += this.h.length;
            this.f++;
            this.h = this.e.get(this.f);
            return;
        }
        byte[] bArr = this.h;
        if (bArr == null) {
            this.g = 0;
        } else {
            i2 = Math.max(bArr.length << 1, i2 - this.g);
            this.g += this.h.length;
        }
        this.f++;
        this.h = new byte[i2];
        this.e.add(this.h);
    }

    @DexIgnore
    public void close() throws IOException {
    }

    @DexIgnore
    public String toString() {
        return new String(y());
    }

    @DexIgnore
    public void write(byte[] bArr, int i2, int i3) {
        if (i2 >= 0 && i2 <= bArr.length && i3 >= 0) {
            int i4 = i2 + i3;
            if (i4 <= bArr.length && i4 >= 0) {
                if (i3 != 0) {
                    synchronized (this) {
                        int i5 = this.i + i3;
                        int i6 = this.i - this.g;
                        while (i3 > 0) {
                            int min = Math.min(i3, this.h.length - i6);
                            System.arraycopy(bArr, i4 - i3, this.h, i6, min);
                            i3 -= min;
                            if (i3 > 0) {
                                b(i5);
                                i6 = 0;
                            }
                        }
                        this.i = i5;
                    }
                    return;
                }
                return;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return r1;
     */
    @DexIgnore
    public synchronized byte[] y() {
        int i2 = this.i;
        if (i2 != 0) {
            byte[] bArr = new byte[i2];
            int i3 = 0;
            for (byte[] next : this.e) {
                int min = Math.min(next.length, i2);
                System.arraycopy(next, 0, bArr, i3, min);
                i3 += min;
                i2 -= min;
                if (i2 == 0) {
                    break;
                }
            }
        } else {
            return j;
        }
    }

    @DexIgnore
    public pp4(int i2) {
        this.e = new ArrayList();
        if (i2 >= 0) {
            synchronized (this) {
                b(i2);
            }
            return;
        }
        throw new IllegalArgumentException("Negative initial size: " + i2);
    }

    @DexIgnore
    public synchronized void write(int i2) {
        int i3 = this.i - this.g;
        if (i3 == this.h.length) {
            b(this.i + 1);
            i3 = 0;
        }
        this.h[i3] = (byte) i2;
        this.i++;
    }
}
