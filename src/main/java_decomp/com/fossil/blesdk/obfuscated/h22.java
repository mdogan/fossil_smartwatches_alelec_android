package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h22 implements n22 {
    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    public void a(o22 o22) {
        if (q22.a((CharSequence) o22.d(), o22.f) >= 2) {
            o22.a(a(o22.d().charAt(o22.f), o22.d().charAt(o22.f + 1)));
            o22.f += 2;
            return;
        }
        char c = o22.c();
        int a = q22.a(o22.d(), o22.f, a());
        if (a != a()) {
            if (a == 1) {
                o22.a(230);
                o22.b(1);
            } else if (a == 2) {
                o22.a(239);
                o22.b(2);
            } else if (a == 3) {
                o22.a(238);
                o22.b(3);
            } else if (a == 4) {
                o22.a(240);
                o22.b(4);
            } else if (a == 5) {
                o22.a(231);
                o22.b(5);
            } else {
                throw new IllegalStateException("Illegal mode: " + a);
            }
        } else if (q22.c(c)) {
            o22.a(235);
            o22.a((char) ((c - 128) + 1));
            o22.f++;
        } else {
            o22.a((char) (c + 1));
            o22.f++;
        }
    }

    @DexIgnore
    public static char a(char c, char c2) {
        if (q22.b(c) && q22.b(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + 130);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }
}
