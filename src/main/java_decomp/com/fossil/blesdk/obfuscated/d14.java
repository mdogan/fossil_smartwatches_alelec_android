package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d14 {
    @DexIgnore
    public static volatile long f;
    @DexIgnore
    public p04 a;
    @DexIgnore
    public StatReportStrategy b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public d14(p04 p04) {
        this.a = p04;
        this.b = i04.o();
        this.c = p04.e();
        this.d = p04.d();
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            if (i04.J > 0 && this.e >= f) {
                k04.f(this.d);
                f = this.e + i04.K;
                if (i04.q()) {
                    u14 f2 = k04.m;
                    f2.e("nextFlushTime=" + f);
                }
            }
            if (u04.a(this.d).f()) {
                if (i04.q()) {
                    u14 f3 = k04.m;
                    f3.e("sendFailedCount=" + k04.p);
                }
                if (!k04.a()) {
                    b();
                    return;
                }
                h14.b(this.d).a(this.a, (q24) null, this.c, false);
                if (this.e - k04.q > 1800000) {
                    k04.d(this.d);
                    return;
                }
                return;
            }
            h14.b(this.d).a(this.a, (q24) null, this.c, false);
        }
    }

    @DexIgnore
    public final void a(q24 q24) {
        r24.b(k04.r).a(this.a, q24);
    }

    @DexIgnore
    public final void b() {
        if (this.a.c() != null && this.a.c().e()) {
            this.b = StatReportStrategy.INSTANT;
        }
        if (i04.z && u04.a(k04.r).e()) {
            this.b = StatReportStrategy.INSTANT;
        }
        if (i04.q()) {
            u14 f2 = k04.m;
            f2.e("strategy=" + this.b.name());
        }
        switch (x04.a[this.b.ordinal()]) {
            case 1:
                c();
                return;
            case 2:
                h14.b(this.d).a(this.a, (q24) null, this.c, false);
                if (i04.q()) {
                    u14 f3 = k04.m;
                    f3.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + k04.s + ",difftime=" + (k04.s - this.e));
                }
                if (k04.s == 0) {
                    k04.s = j24.a(this.d, "last_period_ts", 0);
                    if (this.e > k04.s) {
                        k04.e(this.d);
                    }
                    long l = this.e + ((long) (i04.l() * 60 * 1000));
                    if (k04.s > l) {
                        k04.s = l;
                    }
                    m24.a(this.d).a();
                }
                if (i04.q()) {
                    u14 f4 = k04.m;
                    f4.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + k04.s + ",difftime=" + (k04.s - this.e));
                }
                if (this.e > k04.s) {
                    k04.e(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                h14.b(this.d).a(this.a, (q24) null, this.c, false);
                return;
            case 5:
                h14.b(this.d).a(this.a, (q24) new e14(this), this.c, true);
                return;
            case 6:
                if (u04.a(k04.r).c() == 1) {
                    c();
                    return;
                } else {
                    h14.b(this.d).a(this.a, (q24) null, this.c, false);
                    return;
                }
            case 7:
                if (f24.h(this.d)) {
                    a((q24) new f14(this));
                    return;
                }
                return;
            default:
                u14 f5 = k04.m;
                f5.d("Invalid stat strategy:" + i04.o());
                return;
        }
    }

    @DexIgnore
    public final void c() {
        if (h14.i().f <= 0 || !i04.I) {
            a((q24) new g14(this));
            return;
        }
        h14.i().a(this.a, (q24) null, this.c, true);
        h14.i().a(-1);
    }

    @DexIgnore
    public final boolean d() {
        if (i04.w <= 0) {
            return false;
        }
        if (this.e > k04.d) {
            k04.c.clear();
            long unused = k04.d = this.e + i04.x;
            if (i04.q()) {
                u14 f2 = k04.m;
                f2.e("clear methodsCalledLimitMap, nextLimitCallClearTime=" + k04.d);
            }
        }
        Integer valueOf = Integer.valueOf(this.a.a().a());
        Integer num = (Integer) k04.c.get(valueOf);
        if (num != null) {
            k04.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
            if (num.intValue() <= i04.w) {
                return false;
            }
            if (i04.q()) {
                u14 f3 = k04.m;
                f3.c("event " + this.a.f() + " was discard, cause of called limit, current:" + num + ", limit:" + i04.w + ", period:" + i04.x + " ms");
            }
            return true;
        }
        k04.c.put(valueOf, 1);
        return false;
    }
}
