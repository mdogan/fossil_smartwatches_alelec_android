package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class el implements dl {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ mf b;
    @DexIgnore
    public /* final */ xf c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf<cl> {
        @DexIgnore
        public a(el elVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(lg lgVar, cl clVar) {
            String str = clVar.a;
            if (str == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, str);
            }
            lgVar.b(2, (long) clVar.b);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo`(`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xf {
        @DexIgnore
        public b(el elVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public el(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
    }

    @DexIgnore
    public void a(cl clVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(clVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public cl a(String str) {
        vf b2 = vf.b("SELECT * FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? new cl(a2.getString(bg.b(a2, "work_spec_id")), a2.getInt(bg.b(a2, "system_id"))) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
