package com.fossil.blesdk.obfuscated;

import android.view.View;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class d23 extends v52 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract CopyOnWriteArrayList<CustomizeRealData> i();

    @DexIgnore
    public abstract void j();
}
