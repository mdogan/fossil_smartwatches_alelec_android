package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.zip.Deflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yo4 implements jp4 {
    @DexIgnore
    public /* final */ wo4 e;
    @DexIgnore
    public /* final */ Deflater f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public yo4(jp4 jp4, Deflater deflater) {
        this(ep4.a(jp4), deflater);
    }

    @DexIgnore
    public void a(vo4 vo4, long j) throws IOException {
        mp4.a(vo4.f, 0, j);
        while (j > 0) {
            hp4 hp4 = vo4.e;
            int min = (int) Math.min(j, (long) (hp4.c - hp4.b));
            this.f.setInput(hp4.a, hp4.b, min);
            a(false);
            long j2 = (long) min;
            vo4.f -= j2;
            hp4.b += min;
            if (hp4.b == hp4.c) {
                vo4.e = hp4.b();
                ip4.a(hp4);
            }
            j -= j2;
        }
    }

    @DexIgnore
    public lp4 b() {
        return this.e.b();
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.g) {
            try {
                f();
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.f.end();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            try {
                this.e.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.g = true;
            if (th != null) {
                mp4.a(th);
                throw null;
            }
        }
    }

    @DexIgnore
    public void f() throws IOException {
        this.f.finish();
        a(false);
    }

    @DexIgnore
    public void flush() throws IOException {
        a(true);
        this.e.flush();
    }

    @DexIgnore
    public String toString() {
        return "DeflaterSink(" + this.e + ")";
    }

    @DexIgnore
    public yo4(wo4 wo4, Deflater deflater) {
        if (wo4 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater != null) {
            this.e = wo4;
            this.f = deflater;
        } else {
            throw new IllegalArgumentException("inflater == null");
        }
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        hp4 b;
        int i;
        vo4 a = this.e.a();
        while (true) {
            b = a.b(1);
            if (z) {
                Deflater deflater = this.f;
                byte[] bArr = b.a;
                int i2 = b.c;
                i = deflater.deflate(bArr, i2, 8192 - i2, 2);
            } else {
                Deflater deflater2 = this.f;
                byte[] bArr2 = b.a;
                int i3 = b.c;
                i = deflater2.deflate(bArr2, i3, 8192 - i3);
            }
            if (i > 0) {
                b.c += i;
                a.f += (long) i;
                this.e.d();
            } else if (this.f.needsInput()) {
                break;
            }
        }
        if (b.b == b.c) {
            a.e = b.b();
            ip4.a(b);
        }
    }
}
