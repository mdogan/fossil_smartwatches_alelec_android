package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fk0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fk0> CREATOR; // = new nl0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    @Deprecated
    public /* final */ Scope[] h;

    @DexIgnore
    public fk0(int i, int i2, int i3, Scope[] scopeArr) {
        this.e = i;
        this.f = i2;
        this.g = i3;
        this.h = scopeArr;
    }

    @DexIgnore
    public int H() {
        return this.f;
    }

    @DexIgnore
    public int I() {
        return this.g;
    }

    @DexIgnore
    @Deprecated
    public Scope[] J() {
        return this.h;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, H());
        lk0.a(parcel, 3, I());
        lk0.a(parcel, 4, (T[]) J(), i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public fk0(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, (Scope[]) null);
    }
}
