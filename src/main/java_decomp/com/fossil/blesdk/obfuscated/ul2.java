package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ul2 {
    @DexIgnore
    public /* final */ Map<String, String> a; // = new HashMap();
    @DexIgnore
    public /* final */ AnalyticsHelper b;

    @DexIgnore
    public ul2(AnalyticsHelper analyticsHelper) {
        wd4.b(analyticsHelper, "mAnalyticsInstance");
        this.b = analyticsHelper;
    }

    @DexIgnore
    public final ul2 a(String str, String str2) {
        wd4.b(str, "propertyName");
        wd4.b(str2, "propertyValue");
        this.a.put(str, str2);
        return this;
    }

    @DexIgnore
    public final void a() {
        this.b.a(this.a);
    }
}
