package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d1<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public d1(T t) {
        if (t != null) {
            this.a = t;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }
}
