package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g74 implements f74 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public g74(w44 w44) {
        if (w44.l() != null) {
            this.a = w44.l();
            w44.q();
            "Android/" + this.a.getPackageName();
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    @DexIgnore
    public File a() {
        return a(this.a.getFilesDir());
    }

    @DexIgnore
    public File a(File file) {
        if (file == null) {
            r44.g().d("Fabric", "Null File");
            return null;
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            r44.g().w("Fabric", "Couldn't create file");
            return null;
        }
    }
}
