package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gs2 extends as2 implements gl3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public fl3 j;
    @DexIgnore
    public ur3<le2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gs2.m;
        }

        @DexIgnore
        public final gs2 b() {
            return new gs2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ gs2 a;

        @DexIgnore
        public c(gs2 gs2) {
            this.a = gs2;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
        }

        @DexIgnore
        public void b(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
            CharSequence d = gVar.d();
            Unit unit = Unit.METRIC;
            if (wd4.a((Object) d, (Object) PortfolioApp.W.c().getString(R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Ft))) {
                unit = Unit.IMPERIAL;
            }
            gs2.a(this.a).a(unit);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ gs2 a;

        @DexIgnore
        public e(gs2 gs2) {
            this.a = gs2;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
        }

        @DexIgnore
        public void b(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
            CharSequence d = gVar.d();
            Unit unit = Unit.METRIC;
            if (wd4.a((Object) d, (Object) PortfolioApp.W.c().getString(R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs))) {
                unit = Unit.IMPERIAL;
            }
            gs2.a(this.a).b(unit);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            wd4.b(gVar, "tab");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gs2 e;

        @DexIgnore
        public f(gs2 gs2) {
            this.e = gs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.j0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gs2 e;

        @DexIgnore
        public g(gs2 gs2) {
            this.e = gs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            gs2.a(this.e).a(true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gs2 e;

        @DexIgnore
        public h(gs2 gs2) {
            this.e = gs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            gs2.a(this.e).a(false);
        }
    }

    /*
    static {
        String simpleName = gs2.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ fl3 a(gs2 gs2) {
        fl3 fl3 = gs2.j;
        if (fl3 != null) {
            return fl3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(int i, Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        int i2 = hs2.a[unit.ordinal()];
        if (i2 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "updateData weight=" + i + " metric");
            ur3<le2> ur3 = this.k;
            if (ur3 != null) {
                le2 a2 = ur3.a();
                if (a2 != null) {
                    TabLayout.g c2 = a2.w.c(0);
                    if (c2 != null) {
                        c2.g();
                    }
                    RulerValuePicker rulerValuePicker = a2.u;
                    wd4.a((Object) rulerValuePicker, "it.rvpWeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.u.setFormatter(new ProfileFormatter(4));
                    a2.u.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.d(str2, "updateData weight=" + i + " imperial");
            ur3<le2> ur32 = this.k;
            if (ur32 != null) {
                le2 a3 = ur32.a();
                if (a3 != null) {
                    TabLayout.g c3 = a3.w.c(1);
                    if (c3 != null) {
                        c3.g();
                    }
                    float f2 = qk2.f((float) i);
                    RulerValuePicker rulerValuePicker2 = a3.u;
                    wd4.a((Object) rulerValuePicker2, "it.rvpWeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.u.setFormatter(new ProfileFormatter(4));
                    a3.u.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        ur3<le2> ur3 = this.k;
        if (ur3 != null) {
            le2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.s;
                if (dashBar != null) {
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.a(dashBar, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity, true);
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.k = new ur3<>(this, (le2) ra.a(layoutInflater, R.layout.fragment_onboarding_height_weight, viewGroup, false, O0()));
        ur3<le2> ur3 = this.k;
        if (ur3 != null) {
            le2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        fl3 fl3 = this.j;
        if (fl3 != null) {
            fl3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        fl3 fl3 = this.j;
        if (fl3 != null) {
            fl3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<le2> ur3 = this.k;
        if (ur3 != null) {
            le2 a2 = ur3.a();
            if (a2 != null) {
                a2.t.setValuePickerListener(new b(a2, this));
                a2.v.a((TabLayout.c) new c(this));
                a2.u.setValuePickerListener(new d(a2, this));
                a2.w.a((TabLayout.c) new e(this));
                a2.r.setOnClickListener(new f(this));
                a2.x.setOnClickListener(new g(this));
                a2.q.setOnClickListener(new h(this));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(fl3 fl3) {
        wd4.b(fl3, "presenter");
        this.j = fl3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements hu3 {
        @DexIgnore
        public /* final */ /* synthetic */ le2 a;
        @DexIgnore
        public /* final */ /* synthetic */ gs2 b;

        @DexIgnore
        public b(le2 le2, gs2 gs2) {
            this.a = le2;
            this.b = gs2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.a.t;
            wd4.a((Object) rulerValuePicker, "it.rvpHeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                gs2.a(this.b).a(i);
                return;
            }
            gs2.a(this.b).a(Math.round(qk2.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.v;
            wd4.a((Object) tabLayout, "it.tlHeightUnit");
            gt3.a(tabLayout, !z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements hu3 {
        @DexIgnore
        public /* final */ /* synthetic */ le2 a;
        @DexIgnore
        public /* final */ /* synthetic */ gs2 b;

        @DexIgnore
        public d(le2 le2, gs2 gs2) {
            this.a = le2;
            this.b = gs2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.a.u;
            wd4.a((Object) rulerValuePicker, "it.rvpWeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                gs2.a(this.b).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            gs2.a(this.b).b(Math.round(qk2.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.w;
            wd4.a((Object) tabLayout, "it.tlWeightUnit");
            gt3.a(tabLayout, !z);
        }
    }

    @DexIgnore
    public void a(int i, Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        int i2 = hs2.b[unit.ordinal()];
        if (i2 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "updateData height=" + i + " metric");
            ur3<le2> ur3 = this.k;
            if (ur3 != null) {
                le2 a2 = ur3.a();
                if (a2 != null) {
                    TabLayout.g c2 = a2.v.c(0);
                    if (c2 != null) {
                        c2.g();
                    }
                    RulerValuePicker rulerValuePicker = a2.t;
                    wd4.a((Object) rulerValuePicker, "it.rvpHeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.t.setFormatter(new ProfileFormatter(-1));
                    a2.t.a(100, 251, i);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.d(str2, "updateData height=" + i + " imperial");
            ur3<le2> ur32 = this.k;
            if (ur32 != null) {
                le2 a3 = ur32.a();
                if (a3 != null) {
                    TabLayout.g c3 = a3.v.c(1);
                    if (c3 != null) {
                        c3.g();
                    }
                    Pair<Integer, Integer> b2 = qk2.b((float) i);
                    RulerValuePicker rulerValuePicker2 = a3.t;
                    wd4.a((Object) rulerValuePicker2, "it.rvpHeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.t.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker3 = a3.t;
                    Integer first = b2.getFirst();
                    wd4.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = qk2.a(first.intValue());
                    Integer second = b2.getSecond();
                    wd4.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker3.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }
}
