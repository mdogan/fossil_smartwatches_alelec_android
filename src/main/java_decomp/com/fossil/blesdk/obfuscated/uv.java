package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.request.RequestCoordinator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uv implements RequestCoordinator, pv {
    @DexIgnore
    public /* final */ RequestCoordinator a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public volatile pv c;
    @DexIgnore
    public volatile pv d;
    @DexIgnore
    public RequestCoordinator.RequestState e;
    @DexIgnore
    public RequestCoordinator.RequestState f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public uv(Object obj, RequestCoordinator requestCoordinator) {
        RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
        this.e = requestState;
        this.f = requestState;
        this.b = obj;
        this.a = requestCoordinator;
    }

    @DexIgnore
    public void a(pv pvVar, pv pvVar2) {
        this.c = pvVar;
        this.d = pvVar2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    @DexIgnore
    public void b(pv pvVar) {
        synchronized (this.b) {
            if (!pvVar.equals(this.c)) {
                this.f = RequestCoordinator.RequestState.FAILED;
                return;
            }
            this.e = RequestCoordinator.RequestState.FAILED;
            if (this.a != null) {
                this.a.b(this);
            }
        }
    }

    @DexIgnore
    public boolean c(pv pvVar) {
        boolean z;
        synchronized (this.b) {
            z = h() && pvVar.equals(this.c) && !b();
        }
        return z;
    }

    @DexIgnore
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            this.e = RequestCoordinator.RequestState.CLEARED;
            this.f = RequestCoordinator.RequestState.CLEARED;
            this.d.clear();
            this.c.clear();
        }
    }

    @DexIgnore
    public boolean d(pv pvVar) {
        boolean z;
        synchronized (this.b) {
            z = i() && (pvVar.equals(this.c) || this.e != RequestCoordinator.RequestState.SUCCESS);
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return;
     */
    @DexIgnore
    public void e(pv pvVar) {
        synchronized (this.b) {
            if (pvVar.equals(this.d)) {
                this.f = RequestCoordinator.RequestState.SUCCESS;
                return;
            }
            this.e = RequestCoordinator.RequestState.SUCCESS;
            if (this.a != null) {
                this.a.e(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @DexIgnore
    public boolean f(pv pvVar) {
        boolean z;
        synchronized (this.b) {
            z = g() && pvVar.equals(this.c) && this.e != RequestCoordinator.RequestState.PAUSED;
        }
        return z;
    }

    @DexIgnore
    public final boolean g() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.f(this);
    }

    @DexIgnore
    public final boolean h() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.c(this);
    }

    @DexIgnore
    public final boolean i() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator == null || requestCoordinator.d(this);
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.RUNNING;
        }
        return z;
    }

    @DexIgnore
    public final boolean j() {
        RequestCoordinator requestCoordinator = this.a;
        return requestCoordinator != null && requestCoordinator.a();
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.b) {
            if (!j()) {
                if (!b()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public void c() {
        synchronized (this.b) {
            this.g = true;
            try {
                if (!(this.e == RequestCoordinator.RequestState.SUCCESS || this.f == RequestCoordinator.RequestState.RUNNING)) {
                    this.f = RequestCoordinator.RequestState.RUNNING;
                    this.d.c();
                }
                if (this.g && this.e != RequestCoordinator.RequestState.RUNNING) {
                    this.e = RequestCoordinator.RequestState.RUNNING;
                    this.c.c();
                }
            } finally {
                this.g = false;
            }
        }
    }

    @DexIgnore
    public void d() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = RequestCoordinator.RequestState.PAUSED;
                this.d.d();
            }
            if (!this.e.isComplete()) {
                this.e = RequestCoordinator.RequestState.PAUSED;
                this.c.d();
            }
        }
    }

    @DexIgnore
    public boolean f() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public boolean a(pv pvVar) {
        if (!(pvVar instanceof uv)) {
            return false;
        }
        uv uvVar = (uv) pvVar;
        if (this.c == null) {
            if (uvVar.c != null) {
                return false;
            }
        } else if (!this.c.a(uvVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (uvVar.d == null) {
                return true;
            }
            return false;
        } else if (!this.d.a(uvVar.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean b() {
        boolean z;
        synchronized (this.b) {
            if (this.e != RequestCoordinator.RequestState.SUCCESS) {
                if (this.f != RequestCoordinator.RequestState.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.b) {
            z = this.e == RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }
}
