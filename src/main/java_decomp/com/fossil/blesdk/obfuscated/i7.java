package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.view.MenuItem;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i7 extends MenuItem {
    @DexIgnore
    i7 a(n8 n8Var);

    @DexIgnore
    n8 a();

    @DexIgnore
    boolean collapseActionView();

    @DexIgnore
    boolean expandActionView();

    @DexIgnore
    View getActionView();

    @DexIgnore
    int getAlphabeticModifiers();

    @DexIgnore
    CharSequence getContentDescription();

    @DexIgnore
    ColorStateList getIconTintList();

    @DexIgnore
    PorterDuff.Mode getIconTintMode();

    @DexIgnore
    int getNumericModifiers();

    @DexIgnore
    CharSequence getTooltipText();

    @DexIgnore
    boolean isActionViewExpanded();

    @DexIgnore
    MenuItem setActionView(int i);

    @DexIgnore
    MenuItem setActionView(View view);

    @DexIgnore
    MenuItem setAlphabeticShortcut(char c, int i);

    @DexIgnore
    i7 setContentDescription(CharSequence charSequence);

    @DexIgnore
    MenuItem setIconTintList(ColorStateList colorStateList);

    @DexIgnore
    MenuItem setIconTintMode(PorterDuff.Mode mode);

    @DexIgnore
    MenuItem setNumericShortcut(char c, int i);

    @DexIgnore
    MenuItem setShortcut(char c, char c2, int i, int i2);

    @DexIgnore
    void setShowAsAction(int i);

    @DexIgnore
    MenuItem setShowAsActionFlags(int i);

    @DexIgnore
    i7 setTooltipText(CharSequence charSequence);
}
