package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ly1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ iy1 e;
    @DexIgnore
    public /* final */ /* synthetic */ ky1 f;

    @DexIgnore
    public ly1(ky1 ky1, iy1 iy1) {
        this.f = ky1;
        this.e = iy1;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f.e.d(this.e.a);
        this.e.a();
    }
}
