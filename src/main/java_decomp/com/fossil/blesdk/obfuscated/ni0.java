package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.d;
import com.fossil.blesdk.obfuscated.we0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ni0<O extends ee0.d> extends ge0<O> {
    @DexIgnore
    public /* final */ ee0.f j;
    @DexIgnore
    public /* final */ hi0 k;
    @DexIgnore
    public /* final */ lj0 l;
    @DexIgnore
    public /* final */ ee0.a<? extends mn1, wm1> m;

    @DexIgnore
    public ni0(Context context, ee0<O> ee0, Looper looper, ee0.f fVar, hi0 hi0, lj0 lj0, ee0.a<? extends mn1, wm1> aVar) {
        super(context, ee0, looper);
        this.j = fVar;
        this.k = hi0;
        this.l = lj0;
        this.m = aVar;
        this.i.a((ge0<?>) this);
    }

    @DexIgnore
    public final ee0.f a(Looper looper, we0.a<O> aVar) {
        this.k.a((ii0) aVar);
        return this.j;
    }

    @DexIgnore
    public final ee0.f i() {
        return this.j;
    }

    @DexIgnore
    public final ih0 a(Context context, Handler handler) {
        return new ih0(context, handler, this.l, this.m);
    }
}
