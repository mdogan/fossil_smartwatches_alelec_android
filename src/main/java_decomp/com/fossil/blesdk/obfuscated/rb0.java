package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rb0 {
    @DexIgnore
    public static /* final */ ee0.g<ds0> a; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0.g<jc0> b; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0.a<ds0, a> c; // = new hd0();
    @DexIgnore
    public static /* final */ ee0.a<jc0, GoogleSignInOptions> d; // = new id0();
    @DexIgnore
    public static /* final */ ee0<GoogleSignInOptions> e; // = new ee0<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ xb0 f; // = new ic0();

    @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a implements ee0.d.e {
        @DexIgnore
        public /* final */ boolean e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rb0$a$a")
        @Deprecated
        /* renamed from: com.fossil.blesdk.obfuscated.rb0$a$a  reason: collision with other inner class name */
        public static class C0030a {
            @DexIgnore
            public Boolean a; // = false;

            @DexIgnore
            public a a() {
                return new a(this);
            }
        }

        /*
        static {
            new C0030a().a();
        }
        */

        @DexIgnore
        public a(C0030a aVar) {
            this.e = aVar.a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", (String) null);
            bundle.putBoolean("force_save_dialog", this.e);
            return bundle;
        }
    }

    /*
    static {
        ee0<tb0> ee0 = sb0.c;
        new ee0("Auth.CREDENTIALS_API", c, a);
        vb0 vb0 = sb0.d;
        new cs0();
    }
    */
}
