package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface n94<T1, T2, R> {
    @DexIgnore
    R a(T1 t1, T2 t2) throws Exception;
}
