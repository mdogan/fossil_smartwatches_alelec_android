package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutinesInternalError;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yh4<T> extends fl4 {
    @DexIgnore
    public int g;

    @DexIgnore
    public yh4(int i) {
        this.g = i;
    }

    @DexIgnore
    public final Throwable a(Object obj) {
        if (!(obj instanceof zg4)) {
            obj = null;
        }
        zg4 zg4 = (zg4) obj;
        if (zg4 != null) {
            return zg4.a;
        }
        return null;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        wd4.b(th, "cause");
    }

    @DexIgnore
    public abstract kc4<T> b();

    @DexIgnore
    public abstract Object c();

    @DexIgnore
    public <T> T c(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void run() {
        Object obj;
        CoroutineContext context;
        Object b;
        Object obj2;
        gl4 gl4 = this.f;
        try {
            kc4 b2 = b();
            if (b2 != null) {
                vh4 vh4 = (vh4) b2;
                kc4<T> kc4 = vh4.l;
                context = kc4.getContext();
                Object c = c();
                b = ThreadContextKt.b(context, vh4.j);
                Throwable a = a(c);
                ri4 ri4 = ij4.a(this.g) ? (ri4) context.get(ri4.d) : null;
                if (a == null && ri4 != null && !ri4.isActive()) {
                    CancellationException y = ri4.y();
                    a(c, (Throwable) y);
                    Result.a aVar = Result.Companion;
                    kc4.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(y, (kc4<?>) kc4))));
                } else if (a != null) {
                    Result.a aVar2 = Result.Companion;
                    kc4.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(a, (kc4<?>) kc4))));
                } else {
                    Object c2 = c(c);
                    Result.a aVar3 = Result.Companion;
                    kc4.resumeWith(Result.m3constructorimpl(c2));
                }
                cb4 cb4 = cb4.a;
                ThreadContextKt.a(context, b);
                try {
                    Result.a aVar4 = Result.Companion;
                    gl4.A();
                    obj2 = Result.m3constructorimpl(cb4.a);
                } catch (Throwable th) {
                    Result.a aVar5 = Result.Companion;
                    obj2 = Result.m3constructorimpl(za4.a(th));
                }
                a((Throwable) null, Result.m6exceptionOrNullimpl(obj2));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        } catch (Throwable th2) {
            try {
                Result.a aVar6 = Result.Companion;
                gl4.A();
                obj = Result.m3constructorimpl(cb4.a);
            } catch (Throwable th3) {
                Result.a aVar7 = Result.Companion;
                obj = Result.m3constructorimpl(za4.a(th3));
            }
            a(th2, Result.m6exceptionOrNullimpl(obj));
        }
    }

    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                ua4.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". " + "Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                ih4.a(b().getContext(), (Throwable) new CoroutinesInternalError(str, th));
                return;
            }
            wd4.a();
            throw null;
        }
    }
}
