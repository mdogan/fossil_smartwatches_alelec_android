package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bm1 extends dl1 {
    @DexIgnore
    public static /* final */ String[] f; // = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;"};
    @DexIgnore
    public static /* final */ String[] g; // = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    @DexIgnore
    public static /* final */ String[] h; // = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;"};
    @DexIgnore
    public static /* final */ String[] i; // = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    @DexIgnore
    public static /* final */ String[] j; // = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] k; // = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    @DexIgnore
    public /* final */ em1 d; // = new em1(this, getContext(), "google_app_measurement.db");
    @DexIgnore
    public /* final */ zk1 e; // = new zk1(c());

    @DexIgnore
    public bm1(el1 el1) {
        super(el1);
    }

    @DexIgnore
    public final long A() {
        return a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @DexIgnore
    public final long B() {
        return a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    @DexIgnore
    public final boolean C() {
        return a("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    @DexIgnore
    public final boolean D() {
        return a("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final long E() {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery("select rowid from raw_events order by rowid desc limit 1;", (String[]) null);
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            long j2 = cursor.getLong(0);
            if (cursor != null) {
                cursor.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            d().s().a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean F() {
        return getContext().getDatabasePath("google_app_measurement.db").exists();
    }

    @DexIgnore
    public final long a(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j2 = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            d().s().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x011f  */
    public final eg1 b(String str, String str2) {
        Cursor cursor;
        Boolean bool;
        String str3 = str2;
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        try {
            boolean z = false;
            Cursor query = v().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp", "last_bundled_timestamp", "last_bundled_day", "last_sampled_complex_event_id", "last_sampling_rate", "last_exempt_from_sampling"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                long j2 = query.getLong(0);
                long j3 = query.getLong(1);
                long j4 = query.getLong(2);
                long j5 = query.isNull(3) ? 0 : query.getLong(3);
                Long valueOf = query.isNull(4) ? null : Long.valueOf(query.getLong(4));
                Long valueOf2 = query.isNull(5) ? null : Long.valueOf(query.getLong(5));
                Long valueOf3 = query.isNull(6) ? null : Long.valueOf(query.getLong(6));
                if (!query.isNull(7)) {
                    if (query.getLong(7) == 1) {
                        z = true;
                    }
                    bool = Boolean.valueOf(z);
                } else {
                    bool = null;
                }
                long j6 = j5;
                cursor = query;
                try {
                    eg1 eg1 = new eg1(str, str2, j2, j3, j4, j6, valueOf, valueOf2, valueOf3, bool);
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for event aggregates, expected one. appId", ug1.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return eg1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying events. appId", ug1.a(str), i().a(str2), e);
                        if (cursor != null) {
                            cursor.close();
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                cursor = query;
                d().s().a("Error querying events. appId", ug1.a(str), i().a(str2), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor = query;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying events. appId", ug1.a(str), i().a(str2), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        try {
            d().A().a("Deleted user attribute rows", Integer.valueOf(v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e2) {
            d().s().a("Error deleting user attribute. appId", ug1.a(str), i().c(str2), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9  */
    public final nl1 d(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        try {
            cursor = v().query("user_attributes", new String[]{"set_timestamp", "value", "origin"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    nl1 nl1 = new nl1(str, cursor.getString(2), str2, cursor.getLong(0), a(cursor, 1));
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for user property, expected one. appId", ug1.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return nl1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying user property. appId", ug1.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying user property. appId", ug1.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying user property. appId", ug1.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0125  */
    public final wl1 e(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        try {
            cursor = v().query("conditional_properties", new String[]{"origin", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                try {
                    Object a = a(cursor, 1);
                    boolean z = cursor.getInt(2) != 0;
                    String str4 = str;
                    wl1 wl1 = new wl1(str4, string, new ll1(str2, cursor.getLong(8), a, string), cursor.getLong(6), z, cursor.getString(3), (ig1) m().a(cursor.getBlob(5), ig1.CREATOR), cursor.getLong(4), (ig1) m().a(cursor.getBlob(7), ig1.CREATOR), cursor.getLong(9), (ig1) m().a(cursor.getBlob(10), ig1.CREATOR));
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for conditional property, expected one", ug1.a(str), i().c(str3));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return wl1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying conditional property", ug1.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying conditional property", ug1.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying conditional property", ug1.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final int f(String str, String str2) {
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        try {
            return v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            d().s().a("Error deleting conditional property", ug1.a(str), i().c(str2), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<t51>> g(String str, String str2) {
        Cursor cursor;
        q();
        e();
        ck0.b(str);
        ck0.b(str2);
        g4 g4Var = new g4();
        try {
            cursor = v().query("event_filters", new String[]{"audience_id", "data"}, "app_id=? AND event_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<t51>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    ub1 a = ub1.a(blob, 0, blob.length);
                    t51 t51 = new t51();
                    try {
                        t51.a(a);
                        int i2 = cursor.getInt(0);
                        List list = (List) g4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            g4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(t51);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter. appId", ug1.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filters. appId", ug1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filters. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<w51>> h(String str, String str2) {
        Cursor cursor;
        q();
        e();
        ck0.b(str);
        ck0.b(str2);
        g4 g4Var = new g4();
        try {
            cursor = v().query("property_filters", new String[]{"audience_id", "data"}, "app_id=? AND property_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<w51>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    ub1 a = ub1.a(blob, 0, blob.length);
                    w51 w51 = new w51();
                    try {
                        w51.a(a);
                        int i2 = cursor.getInt(0);
                        List list = (List) g4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            g4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(w51);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter", ug1.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filters. appId", ug1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filters. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long i(String str, String str2) {
        long j2;
        String str3 = str;
        String str4 = str2;
        ck0.b(str);
        ck0.b(str2);
        e();
        q();
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str4);
            sb.append(" from app2 where app_id=?");
            try {
                j2 = a(sb.toString(), new String[]{str3}, -1);
                if (j2 == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str3);
                    contentValues.put("first_open_count", 0);
                    contentValues.put("previous_install_count", 0);
                    if (v.insertWithOnConflict("app2", (String) null, contentValues, 5) == -1) {
                        d().s().a("Failed to insert column (got -1). appId", ug1.a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    j2 = 0;
                }
            } catch (SQLiteException e2) {
                e = e2;
                j2 = 0;
                try {
                    d().s().a("Error inserting column. appId", ug1.a(str), str4, e);
                    v.endTransaction();
                    return j2;
                } catch (Throwable th) {
                    th = th;
                    v.endTransaction();
                    throw th;
                }
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str3);
                contentValues2.put(str4, Long.valueOf(1 + j2));
                if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str3})) == 0) {
                    d().s().a("Failed to update column (got 0). appId", ug1.a(str), str4);
                    v.endTransaction();
                    return -1;
                }
                v.setTransactionSuccessful();
                v.endTransaction();
                return j2;
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error inserting column. appId", ug1.a(str), str4, e);
                v.endTransaction();
                return j2;
            }
        } catch (SQLiteException e4) {
            e = e4;
            j2 = 0;
            d().s().a("Error inserting column. appId", ug1.a(str), str4, e);
            v.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final void t() {
        q();
        v().beginTransaction();
    }

    @DexIgnore
    public final void u() {
        q();
        v().endTransaction();
    }

    @DexIgnore
    public final SQLiteDatabase v() {
        e();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            d().v().a("Error opening database", e2);
            throw e2;
        }
    }

    @DexIgnore
    public final void w() {
        q();
        v().setTransactionSuccessful();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    public final String x() {
        Cursor cursor;
        try {
            cursor = v().rawQuery("select app_id from queue order by has_realtime desc, rowid asc limit 1;", (String[]) null);
            try {
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Database error getting next bundle app id", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Database error getting next bundle app id", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean y() {
        return a("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final void z() {
        e();
        q();
        if (F()) {
            long a = k().h.a();
            long c = c().c();
            if (Math.abs(c - a) > kg1.I.a().longValue()) {
                k().h.a(c);
                e();
                q();
                if (F()) {
                    int delete = v().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(c().b()), String.valueOf(yl1.t())});
                    if (delete > 0) {
                        d().A().a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    @DexIgnore
    public final long a(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j3 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j3;
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            d().s().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long f(String str) {
        ck0.b(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    @DexIgnore
    public final long c(String str) {
        ck0.b(str);
        e();
        q();
        try {
            return (long) v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, l().b(str, kg1.z))))});
        } catch (SQLiteException e2) {
            d().s().a("Error deleting over the limit events. appId", ug1.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final void a(eg1 eg1) {
        ck0.a(eg1);
        e();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", eg1.a);
        contentValues.put("name", eg1.b);
        contentValues.put("lifetime_count", Long.valueOf(eg1.c));
        contentValues.put("current_bundle_count", Long.valueOf(eg1.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(eg1.e));
        contentValues.put("last_bundled_timestamp", Long.valueOf(eg1.f));
        contentValues.put("last_bundled_day", eg1.g);
        contentValues.put("last_sampled_complex_event_id", eg1.h);
        contentValues.put("last_sampling_rate", eg1.i);
        Boolean bool = eg1.j;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (v().insertWithOnConflict("events", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update event aggregates (got -1). appId", ug1.a(eg1.a));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing event aggregates. appId", ug1.a(eg1.a), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    public final byte[] d(String str) {
        Cursor cursor;
        ck0.b(str);
        e();
        q();
        try {
            cursor = v().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    d().s().a("Got multiple records for app config, expected one. appId", ug1.a(str));
                }
                if (cursor != null) {
                    cursor.close();
                }
                return blob;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error querying remote config. appId", ug1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error querying remote config. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final List<wl1> b(String str, String str2, String str3) {
        ck0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat(Marker.ANY_MARKER));
            sb.append(" and name glob ?");
        }
        return b(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
    public final Map<Integer, h61> e(String str) {
        Cursor cursor;
        q();
        e();
        ck0.b(str);
        try {
            cursor = v().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                g4 g4Var = new g4();
                do {
                    int i2 = cursor.getInt(0);
                    byte[] blob = cursor.getBlob(1);
                    ub1 a = ub1.a(blob, 0, blob.length);
                    h61 h61 = new h61();
                    try {
                        h61.a(a);
                        g4Var.put(Integer.valueOf(i2), h61);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter results. appId, audienceId, error", ug1.a(str), Integer.valueOf(i2), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filter results. appId", ug1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filter results. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(nl1 nl1) {
        ck0.a(nl1);
        e();
        q();
        if (d(nl1.a, nl1.c) == null) {
            if (ol1.e(nl1.c)) {
                if (a("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{nl1.a}) >= 25) {
                    return false;
                }
            } else {
                if (a("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{nl1.a, nl1.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", nl1.a);
        contentValues.put("origin", nl1.b);
        contentValues.put("name", nl1.c);
        contentValues.put("set_timestamp", Long.valueOf(nl1.d));
        a(contentValues, "value", nl1.e);
        try {
            if (v().insertWithOnConflict("user_attributes", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update user property (got -1). appId", ug1.a(nl1.a));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing user property. appId", ug1.a(nl1.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final List<wl1> b(String str, String[] strArr) {
        e();
        q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = v().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, (String) null, (String) null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j2 = cursor.getLong(6);
                    long j3 = cursor.getLong(8);
                    long j4 = cursor.getLong(10);
                    boolean z2 = z;
                    wl1 wl1 = r3;
                    wl1 wl12 = new wl1(string, string2, new ll1(string3, j4, a, string2), j3, z2, string4, (ig1) m().a(cursor.getBlob(7), ig1.CREATOR), j2, (ig1) m().a(cursor.getBlob(9), ig1.CREATOR), cursor.getLong(11), (ig1) m().a(cursor.getBlob(12), ig1.CREATOR));
                    arrayList.add(wl1);
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    d().s().a("Read more than the max allowed conditional properties, ignoring extra", 1000);
                    break;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e2) {
            d().s().a("Error querying conditional user property value", e2);
            List<wl1> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    public final List<nl1> a(String str) {
        Cursor cursor;
        ck0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList();
        try {
            cursor = v().query("user_attributes", new String[]{"name", "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", "1000");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                do {
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    String str2 = string2;
                    long j2 = cursor.getLong(2);
                    Object a = a(cursor, 3);
                    if (a == null) {
                        d().s().a("Read invalid user property value, ignoring it. appId", ug1.a(str));
                    } else {
                        arrayList.add(new nl1(str, str2, string, j2, a));
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error querying user properties. appId", ug1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error querying user properties. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0115 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0119 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x014d A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0150 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x015f A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0174 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0191 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01d5  */
    public final rl1 b(String str) {
        Cursor cursor;
        boolean z;
        boolean z2;
        String str2 = str;
        ck0.b(str);
        e();
        q();
        try {
            boolean z3 = true;
            cursor = v().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count", "daily_realtime_events_count", "health_monitor_sample", "android_id", "adid_reporting_enabled", "ssaid_reporting_enabled", "admob_app_id"}, "app_id=?", new String[]{str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    rl1 rl1 = new rl1(this.b.B(), str2);
                    rl1.b(cursor.getString(0));
                    rl1.c(cursor.getString(1));
                    rl1.e(cursor.getString(2));
                    rl1.i(cursor.getLong(3));
                    rl1.d(cursor.getLong(4));
                    rl1.e(cursor.getLong(5));
                    rl1.a(cursor.getString(6));
                    rl1.g(cursor.getString(7));
                    rl1.g(cursor.getLong(8));
                    rl1.h(cursor.getLong(9));
                    if (!cursor.isNull(10)) {
                        if (cursor.getInt(10) == 0) {
                            z = false;
                            rl1.a(z);
                            rl1.l(cursor.getLong(11));
                            rl1.m(cursor.getLong(12));
                            rl1.n(cursor.getLong(13));
                            rl1.o(cursor.getLong(14));
                            rl1.j(cursor.getLong(15));
                            rl1.k(cursor.getLong(16));
                            rl1.f(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                            rl1.f(cursor.getString(18));
                            rl1.b(cursor.getLong(19));
                            rl1.a(cursor.getLong(20));
                            rl1.h(cursor.getString(21));
                            rl1.c(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                            if (!cursor.isNull(23)) {
                                if (cursor.getInt(23) == 0) {
                                    z2 = false;
                                    rl1.b(z2);
                                    if (!cursor.isNull(24)) {
                                        if (cursor.getInt(24) == 0) {
                                            z3 = false;
                                        }
                                    }
                                    rl1.c(z3);
                                    rl1.d(cursor.getString(25));
                                    rl1.g();
                                    if (cursor.moveToNext()) {
                                        d().s().a("Got multiple records for app, expected one. appId", ug1.a(str));
                                    }
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return rl1;
                                }
                            }
                            z2 = true;
                            rl1.b(z2);
                            if (!cursor.isNull(24)) {
                            }
                            rl1.c(z3);
                            rl1.d(cursor.getString(25));
                            rl1.g();
                            if (cursor.moveToNext()) {
                            }
                            if (cursor != null) {
                            }
                            return rl1;
                        }
                    }
                    z = true;
                    rl1.a(z);
                    rl1.l(cursor.getLong(11));
                    rl1.m(cursor.getLong(12));
                    rl1.n(cursor.getLong(13));
                    rl1.o(cursor.getLong(14));
                    rl1.j(cursor.getLong(15));
                    rl1.k(cursor.getLong(16));
                    rl1.f(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                    rl1.f(cursor.getString(18));
                    rl1.b(cursor.getLong(19));
                    rl1.a(cursor.getLong(20));
                    rl1.h(cursor.getString(21));
                    rl1.c(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                    if (!cursor.isNull(23)) {
                    }
                    z2 = true;
                    rl1.b(z2);
                    if (!cursor.isNull(24)) {
                    }
                    rl1.c(z3);
                    rl1.d(cursor.getString(25));
                    rl1.g();
                    if (cursor.moveToNext()) {
                    }
                    if (cursor != null) {
                    }
                    return rl1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying app. appId", ug1.a(str), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying app. appId", ug1.a(str), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying app. appId", ug1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f9, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0100, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0101, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0104, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0105, code lost:
        r12 = r21;
        r11 = r22;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0100 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0127  */
    public final List<nl1> a(String str, String str2, String str3) {
        Cursor cursor;
        String str4;
        ck0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor2 = null;
        try {
            ArrayList arrayList2 = new ArrayList(3);
            arrayList2.add(str);
            StringBuilder sb = new StringBuilder("app_id=?");
            if (!TextUtils.isEmpty(str2)) {
                str4 = str2;
                arrayList2.add(str4);
                sb.append(" and origin=?");
            } else {
                str4 = str2;
            }
            if (!TextUtils.isEmpty(str3)) {
                arrayList2.add(String.valueOf(str3).concat(Marker.ANY_MARKER));
                sb.append(" and name glob ?");
            }
            cursor = v().query("user_attributes", new String[]{"name", "set_timestamp", "value", "origin"}, sb.toString(), (String[]) arrayList2.toArray(new String[arrayList2.size()]), (String) null, (String) null, "rowid", "1001");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() >= 1000) {
                        d().s().a("Read more than the max allowed user properties, ignoring excess", 1000);
                        break;
                    }
                    String string = cursor.getString(0);
                    long j2 = cursor.getLong(1);
                    try {
                        Object a = a(cursor, 2);
                        String string2 = cursor.getString(3);
                        if (a == null) {
                            try {
                                d().s().a("(2)Read invalid user property value, ignoring it", ug1.a(str), string2, str3);
                            } catch (SQLiteException e2) {
                                e = e2;
                                str4 = string2;
                                try {
                                    d().s().a("(2)Error querying user properties", ug1.a(str), str4, e);
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return null;
                                } catch (Throwable th) {
                                    th = th;
                                    cursor2 = cursor;
                                    if (cursor2 != null) {
                                        cursor2.close();
                                    }
                                    throw th;
                                }
                            }
                        } else {
                            String str5 = str3;
                            arrayList.add(new nl1(str, string2, string, j2, a));
                        }
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        str4 = string2;
                    } catch (SQLiteException e3) {
                        e = e3;
                        d().s().a("(2)Error querying user properties", ug1.a(str), str4, e);
                        if (cursor != null) {
                        }
                        return null;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e4) {
                e = e4;
                d().s().a("(2)Error querying user properties", ug1.a(str), str4, e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            e = e5;
            str4 = str2;
            cursor = null;
            d().s().a("(2)Error querying user properties", ug1.a(str), str4, e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
        }
    }

    @DexIgnore
    public final boolean a(wl1 wl1) {
        ck0.a(wl1);
        e();
        q();
        if (d(wl1.e, wl1.g.f) == null) {
            if (a("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{wl1.e}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", wl1.e);
        contentValues.put("origin", wl1.f);
        contentValues.put("name", wl1.g.f);
        a(contentValues, "value", wl1.g.H());
        contentValues.put("active", Boolean.valueOf(wl1.i));
        contentValues.put("trigger_event_name", wl1.j);
        contentValues.put("trigger_timeout", Long.valueOf(wl1.l));
        j();
        contentValues.put("timed_out_event", ol1.a((Parcelable) wl1.k));
        contentValues.put("creation_timestamp", Long.valueOf(wl1.h));
        j();
        contentValues.put("triggered_event", ol1.a((Parcelable) wl1.m));
        contentValues.put("triggered_timestamp", Long.valueOf(wl1.g.g));
        contentValues.put("time_to_live", Long.valueOf(wl1.n));
        j();
        contentValues.put("expired_event", ol1.a((Parcelable) wl1.o));
        try {
            if (v().insertWithOnConflict("conditional_properties", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update conditional user property (got -1)", ug1.a(wl1.e));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing conditional user property", ug1.a(wl1.e), e2);
        }
        return true;
    }

    @DexIgnore
    public final void a(rl1 rl1) {
        ck0.a(rl1);
        e();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", rl1.f());
        contentValues.put("app_instance_id", rl1.a());
        contentValues.put("gmp_app_id", rl1.c());
        contentValues.put("resettable_device_id_hash", rl1.i());
        contentValues.put("last_bundle_index", Long.valueOf(rl1.p()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(rl1.j()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(rl1.k()));
        contentValues.put("app_version", rl1.e());
        contentValues.put("app_store", rl1.m());
        contentValues.put("gmp_version", Long.valueOf(rl1.n()));
        contentValues.put("dev_cert_hash", Long.valueOf(rl1.o()));
        contentValues.put("measurement_enabled", Boolean.valueOf(rl1.d()));
        contentValues.put("day", Long.valueOf(rl1.t()));
        contentValues.put("daily_public_events_count", Long.valueOf(rl1.u()));
        contentValues.put("daily_events_count", Long.valueOf(rl1.v()));
        contentValues.put("daily_conversions_count", Long.valueOf(rl1.w()));
        contentValues.put("config_fetched_time", Long.valueOf(rl1.q()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(rl1.r()));
        contentValues.put("app_version_int", Long.valueOf(rl1.l()));
        contentValues.put("firebase_instance_id", rl1.b());
        contentValues.put("daily_error_events_count", Long.valueOf(rl1.y()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(rl1.x()));
        contentValues.put("health_monitor_sample", rl1.z());
        contentValues.put("android_id", Long.valueOf(rl1.B()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(rl1.C()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(rl1.D()));
        contentValues.put("admob_app_id", rl1.h());
        try {
            SQLiteDatabase v = v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{rl1.f()})) == 0 && v.insertWithOnConflict("apps", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update app (got -1). appId", ug1.a(rl1.f()));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing app. appId", ug1.a(rl1.f()), e2);
        }
    }

    @DexIgnore
    public final cm1 a(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        ck0.b(str);
        e();
        q();
        String[] strArr = {str};
        cm1 cm1 = new cm1();
        Cursor cursor = null;
        try {
            SQLiteDatabase v = v();
            cursor = v.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            if (!cursor.moveToFirst()) {
                d().v().a("Not updating daily counts, app is not known. appId", ug1.a(str));
                if (cursor != null) {
                    cursor.close();
                }
                return cm1;
            }
            if (cursor.getLong(0) == j2) {
                cm1.b = cursor.getLong(1);
                cm1.a = cursor.getLong(2);
                cm1.c = cursor.getLong(3);
                cm1.d = cursor.getLong(4);
                cm1.e = cursor.getLong(5);
            }
            if (z) {
                cm1.b++;
            }
            if (z2) {
                cm1.a++;
            }
            if (z3) {
                cm1.c++;
            }
            if (z4) {
                cm1.d++;
            }
            if (z5) {
                cm1.e++;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j2));
            contentValues.put("daily_public_events_count", Long.valueOf(cm1.a));
            contentValues.put("daily_events_count", Long.valueOf(cm1.b));
            contentValues.put("daily_conversions_count", Long.valueOf(cm1.c));
            contentValues.put("daily_error_events_count", Long.valueOf(cm1.d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(cm1.e));
            v.update("apps", contentValues, "app_id=?", strArr);
            if (cursor != null) {
                cursor.close();
            }
            return cm1;
        } catch (SQLiteException e2) {
            d().s().a("Error updating daily counts. appId", ug1.a(str), e2);
            if (cursor != null) {
                cursor.close();
            }
            return cm1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(g61 g61, boolean z) {
        e();
        q();
        ck0.a(g61);
        ck0.b(g61.q);
        ck0.a(g61.h);
        z();
        long b = c().b();
        if (g61.h.longValue() < b - yl1.t() || g61.h.longValue() > yl1.t() + b) {
            d().v().a("Storing bundle outside of the max uploading time span. appId, now, timestamp", ug1.a(g61.q), Long.valueOf(b), g61.h);
        }
        try {
            byte[] bArr = new byte[g61.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            g61.a(a);
            a.b();
            byte[] b2 = m().b(bArr);
            d().A().a("Saving bundle, size", Integer.valueOf(b2.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", g61.q);
            contentValues.put("bundle_end_timestamp", g61.h);
            contentValues.put("data", b2);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            Integer num = g61.M;
            if (num != null) {
                contentValues.put("retry_count", num);
            }
            try {
                if (v().insert("queue", (String) null, contentValues) != -1) {
                    return true;
                }
                d().s().a("Failed to insert bundle (got -1). appId", ug1.a(g61.q));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing bundle. appId", ug1.a(g61.q), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize bundle. appId", ug1.a(g61.q), e3);
            return false;
        }
    }

    @DexIgnore
    public final List<Pair<g61, Long>> a(String str, int i2, int i3) {
        e();
        q();
        ck0.a(i2 > 0);
        ck0.a(i3 > 0);
        ck0.b(str);
        Cursor cursor = null;
        try {
            cursor = v().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", String.valueOf(i2));
            if (!cursor.moveToFirst()) {
                List<Pair<g61, Long>> emptyList = Collections.emptyList();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            do {
                long j2 = cursor.getLong(0);
                try {
                    byte[] a = m().a(cursor.getBlob(1));
                    if (!arrayList.isEmpty() && a.length + i4 > i3) {
                        break;
                    }
                    ub1 a2 = ub1.a(a, 0, a.length);
                    g61 g61 = new g61();
                    try {
                        g61.a(a2);
                        if (!cursor.isNull(2)) {
                            g61.M = Integer.valueOf(cursor.getInt(2));
                        }
                        i4 += a.length;
                        arrayList.add(Pair.create(g61, Long.valueOf(j2)));
                    } catch (IOException e2) {
                        d().s().a("Failed to merge queued bundle. appId", ug1.a(str), e2);
                    }
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } catch (IOException e3) {
                    d().s().a("Failed to unzip queued bundle. appId", ug1.a(str), e3);
                }
            } while (i4 <= i3);
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e4) {
            d().s().a("Error querying bundles. appId", ug1.a(str), e4);
            List<Pair<g61, Long>> emptyList2 = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(List<Long> list) {
        e();
        q();
        ck0.a(list);
        ck0.a(list.size());
        if (F()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (a(sb3.toString(), (String[]) null) > 0) {
                d().v().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                d().s().a("Error incrementing retry count. error", e2);
            }
        }
    }

    @DexIgnore
    public final void a(String str, s51[] s51Arr) {
        boolean z;
        String str2 = str;
        s51[] s51Arr2 = s51Arr;
        q();
        e();
        ck0.b(str);
        ck0.a(s51Arr);
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            q();
            e();
            ck0.b(str);
            SQLiteDatabase v2 = v();
            v2.delete("property_filters", "app_id=?", new String[]{str2});
            v2.delete("event_filters", "app_id=?", new String[]{str2});
            for (s51 s51 : s51Arr2) {
                q();
                e();
                ck0.b(str);
                ck0.a(s51);
                ck0.a(s51.e);
                ck0.a(s51.d);
                if (s51.c == null) {
                    d().v().a("Audience with no ID. appId", ug1.a(str));
                } else {
                    int intValue = s51.c.intValue();
                    t51[] t51Arr = s51.e;
                    int length = t51Arr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            w51[] w51Arr = s51.d;
                            int length2 = w51Arr.length;
                            int i3 = 0;
                            while (true) {
                                if (i3 >= length2) {
                                    t51[] t51Arr2 = s51.e;
                                    int length3 = t51Arr2.length;
                                    int i4 = 0;
                                    while (true) {
                                        if (i4 >= length3) {
                                            z = true;
                                            break;
                                        } else if (!a(str2, intValue, t51Arr2[i4])) {
                                            z = false;
                                            break;
                                        } else {
                                            i4++;
                                        }
                                    }
                                    if (z) {
                                        w51[] w51Arr2 = s51.d;
                                        int length4 = w51Arr2.length;
                                        int i5 = 0;
                                        while (true) {
                                            if (i5 >= length4) {
                                                break;
                                            } else if (!a(str2, intValue, w51Arr2[i5])) {
                                                z = false;
                                                break;
                                            } else {
                                                i5++;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        q();
                                        e();
                                        ck0.b(str);
                                        SQLiteDatabase v3 = v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(intValue)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(intValue)});
                                    }
                                } else if (w51Arr[i3].c == null) {
                                    d().v().a("Property filter with no ID. Audience definition ignored. appId, audienceId", ug1.a(str), s51.c);
                                    break;
                                } else {
                                    i3++;
                                }
                            }
                        } else if (t51Arr[i2].c == null) {
                            d().v().a("Event filter with no ID. Audience definition ignored. appId, audienceId", ug1.a(str), s51.c);
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (s51 s512 : s51Arr2) {
                arrayList.add(s512.c);
            }
            a(str2, (List<Integer>) arrayList);
            v.setTransactionSuccessful();
        } finally {
            v.endTransaction();
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, t51 t51) {
        q();
        e();
        ck0.b(str);
        ck0.a(t51);
        if (TextUtils.isEmpty(t51.d)) {
            d().v().a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", ug1.a(str), Integer.valueOf(i2), String.valueOf(t51.c));
            return false;
        }
        try {
            byte[] bArr = new byte[t51.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            t51.a(a);
            a.b();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", t51.c);
            contentValues.put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, t51.d);
            contentValues.put("data", bArr);
            try {
                if (v().insertWithOnConflict("event_filters", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert event filter (got -1). appId", ug1.a(str));
                return true;
            } catch (SQLiteException e2) {
                d().s().a("Error storing event filter. appId", ug1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Configuration loss. Failed to serialize event filter. appId", ug1.a(str), e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, w51 w51) {
        q();
        e();
        ck0.b(str);
        ck0.a(w51);
        if (TextUtils.isEmpty(w51.d)) {
            d().v().a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", ug1.a(str), Integer.valueOf(i2), String.valueOf(w51.c));
            return false;
        }
        try {
            byte[] bArr = new byte[w51.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            w51.a(a);
            a.b();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", w51.c);
            contentValues.put("property_name", w51.d);
            contentValues.put("data", bArr);
            try {
                if (v().insertWithOnConflict("property_filters", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert property filter (got -1). appId", ug1.a(str));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing property filter. appId", ug1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Configuration loss. Failed to serialize property filter. appId", ug1.a(str), e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, List<Integer> list) {
        ck0.b(str);
        q();
        e();
        SQLiteDatabase v = v();
        try {
            long a = a("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION, l().b(str, kg1.P)));
            if (a <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            d().s().a("Database error querying filters. appId", ug1.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public static void a(ContentValues contentValues, String str, Object obj) {
        ck0.b(str);
        ck0.a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @DexIgnore
    public final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            d().s().a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                d().s().a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            d().s().a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    @DexIgnore
    public final long a(g61 g61) throws IOException {
        long j2;
        e();
        q();
        ck0.a(g61);
        ck0.b(g61.q);
        try {
            byte[] bArr = new byte[g61.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            g61.a(a);
            a.b();
            kl1 m = m();
            ck0.a(bArr);
            m.j().e();
            MessageDigest w = ol1.w();
            if (w == null) {
                m.d().s().a("Failed to get MD5");
                j2 = 0;
            } else {
                j2 = ol1.a(w.digest(bArr));
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", g61.q);
            contentValues.put("metadata_fingerprint", Long.valueOf(j2));
            contentValues.put("metadata", bArr);
            try {
                v().insertWithOnConflict("raw_events_metadata", (String) null, contentValues, 4);
                return j2;
            } catch (SQLiteException e2) {
                d().s().a("Error storing raw event metadata. appId", ug1.a(g61.q), e2);
                throw e2;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event metadata. appId", ug1.a(g61.q), e3);
            throw e3;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    public final String a(long j2) {
        Cursor cursor;
        e();
        q();
        try {
            cursor = v().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j2)});
            try {
                if (!cursor.moveToFirst()) {
                    d().A().a("No expired configs for apps with pending events");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                if (cursor != null) {
                    cursor.close();
                }
                return string;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error selecting expired configs", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error selecting expired configs", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008f  */
    public final Pair<d61, Long> a(String str, Long l) {
        Cursor cursor;
        e();
        q();
        try {
            cursor = v().rawQuery("select main_event, children_to_process from main_event_params where app_id=? and event_id=?", new String[]{str, String.valueOf(l)});
            try {
                if (!cursor.moveToFirst()) {
                    d().A().a("Main event not found");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                Long valueOf = Long.valueOf(cursor.getLong(1));
                ub1 a = ub1.a(blob, 0, blob.length);
                d61 d61 = new d61();
                try {
                    d61.a(a);
                    Pair<d61, Long> create = Pair.create(d61, valueOf);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return create;
                } catch (IOException e2) {
                    d().s().a("Failed to merge main event. appId, eventId", ug1.a(str), l, e2);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Error selecting main event", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error selecting main event", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(String str, Long l, long j2, d61 d61) {
        e();
        q();
        ck0.a(d61);
        ck0.b(str);
        ck0.a(l);
        try {
            byte[] bArr = new byte[d61.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            d61.a(a);
            a.b();
            d().A().a("Saving complex main event, appId, data size", i().a(str), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put(LogBuilder.KEY_EVENT_ID, l);
            contentValues.put("children_to_process", Long.valueOf(j2));
            contentValues.put("main_event", bArr);
            try {
                if (v().insertWithOnConflict("main_event_params", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert complex main event (got -1). appId", ug1.a(str));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing complex main event. appId", ug1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event params/data. appId, eventId", ug1.a(str), l, e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(dg1 dg1, long j2, boolean z) {
        e();
        q();
        ck0.a(dg1);
        ck0.b(dg1.a);
        d61 d61 = new d61();
        d61.f = Long.valueOf(dg1.e);
        d61.c = new e61[dg1.f.size()];
        Iterator<String> it = dg1.f.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            String next = it.next();
            e61 e61 = new e61();
            int i3 = i2 + 1;
            d61.c[i2] = e61;
            e61.c = next;
            m().a(e61, dg1.f.e(next));
            i2 = i3;
        }
        try {
            byte[] bArr = new byte[d61.b()];
            vb1 a = vb1.a(bArr, 0, bArr.length);
            d61.a(a);
            a.b();
            d().A().a("Saving event, name, data size", i().a(dg1.b), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", dg1.a);
            contentValues.put("name", dg1.b);
            contentValues.put("timestamp", Long.valueOf(dg1.d));
            contentValues.put("metadata_fingerprint", Long.valueOf(j2));
            contentValues.put("data", bArr);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (v().insert("raw_events", (String) null, contentValues) != -1) {
                    return true;
                }
                d().s().a("Failed to insert raw event (got -1). appId", ug1.a(dg1.a));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing raw event. appId", ug1.a(dg1.a), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event params/data. appId", ug1.a(dg1.a), e3);
            return false;
        }
    }
}
