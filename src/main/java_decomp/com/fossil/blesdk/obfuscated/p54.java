package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p54 {
    @DexIgnore
    public static p54 f;
    @DexIgnore
    public static Object g; // = new Object();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public /* final */ s54 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0066  */
    public p54(Context context) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (context != null) {
            this.a = context.getSharedPreferences("com.google.firebase.crashlytics.prefs", 0);
            this.d = t54.a(context);
            if (this.a.contains("firebase_crashlytics_collection_enabled")) {
                z2 = this.a.getBoolean("firebase_crashlytics_collection_enabled", true);
            } else {
                try {
                    PackageManager packageManager = context.getPackageManager();
                    if (packageManager != null) {
                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
                        z2 = (applicationInfo == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_crashlytics_collection_enabled")) ? applicationInfo.metaData.getBoolean("firebase_crashlytics_collection_enabled") : applicationInfo.metaData.getBoolean("firebase_crashlytics_collection_enabled");
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    r44.g().b("Fabric", "Unable to get PackageManager. Falling through", e2);
                }
                z2 = true;
                z = false;
                this.c = z2;
                this.b = z;
                this.e = CommonUtils.o(context) != null ? true : z3;
                return;
            }
            z = true;
            this.c = z2;
            this.b = z;
            this.e = CommonUtils.o(context) != null ? true : z3;
            return;
        }
        throw new RuntimeException("null context");
    }

    @DexIgnore
    public static p54 a(Context context) {
        p54 p54;
        synchronized (g) {
            if (f == null) {
                f = new p54(context);
            }
            p54 = f;
        }
        return p54;
    }

    @DexIgnore
    public boolean b() {
        return this.c;
    }

    @DexIgnore
    public boolean a() {
        if (this.e && this.b) {
            return this.c;
        }
        s54 s54 = this.d;
        if (s54 != null) {
            return s54.a();
        }
        return true;
    }
}
