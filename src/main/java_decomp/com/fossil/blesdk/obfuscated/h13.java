package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h13 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<i13> c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public h13(String str, String str2, ArrayList<i13> arrayList, boolean z) {
        wd4.b(str, "mPresetId");
        wd4.b(str2, "mPresetName");
        wd4.b(arrayList, "mMicroApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = z;
    }

    @DexIgnore
    public final ArrayList<i13> a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
