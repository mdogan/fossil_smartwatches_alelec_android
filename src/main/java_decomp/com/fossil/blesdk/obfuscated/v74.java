package com.fossil.blesdk.obfuscated;

import com.zendesk.sdk.network.Constants;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v74 extends f54 implements g84 {
    @DexIgnore
    public v74(w44 w44, String str, String str2, a74 a74) {
        this(w44, str, str2, a74, HttpMethod.GET);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    public JSONObject a(f84 f84) {
        HttpRequest httpRequest;
        z44 g;
        StringBuilder sb;
        JSONObject jSONObject = null;
        try {
            Map<String, String> b = b(f84);
            httpRequest = a(b);
            try {
                a(httpRequest, f84);
                r44.g().d("Fabric", "Requesting settings from " + b());
                r44.g().d("Fabric", "Settings query params were: " + b);
                jSONObject = a(httpRequest);
                if (httpRequest != null) {
                    g = r44.g();
                    sb = new StringBuilder();
                    sb.append("Settings request ID: ");
                    sb.append(httpRequest.c("X-REQUEST-ID"));
                    g.d("Fabric", sb.toString());
                }
            } catch (HttpRequest.HttpRequestException e) {
                e = e;
                try {
                    r44.g().e("Fabric", "Settings request failed.", e);
                    if (httpRequest != null) {
                    }
                    return jSONObject;
                } catch (Throwable th) {
                    th = th;
                    if (httpRequest != null) {
                        r44.g().d("Fabric", "Settings request ID: " + httpRequest.c("X-REQUEST-ID"));
                    }
                    throw th;
                }
            }
        } catch (HttpRequest.HttpRequestException e2) {
            e = e2;
            httpRequest = null;
            r44.g().e("Fabric", "Settings request failed.", e);
            if (httpRequest != null) {
                g = r44.g();
                sb = new StringBuilder();
                sb.append("Settings request ID: ");
                sb.append(httpRequest.c("X-REQUEST-ID"));
                g.d("Fabric", sb.toString());
            }
            return jSONObject;
        } catch (Throwable th2) {
            th = th2;
            httpRequest = null;
            if (httpRequest != null) {
            }
            throw th;
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            z44 g = r44.g();
            g.b("Fabric", "Failed to parse settings JSON from " + b(), e);
            z44 g2 = r44.g();
            g2.d("Fabric", "Settings response " + str);
            return null;
        }
    }

    @DexIgnore
    public v74(w44 w44, String str, String str2, a74 a74, HttpMethod httpMethod) {
        super(w44, str, str2, a74, httpMethod);
    }

    @DexIgnore
    public final Map<String, String> b(f84 f84) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", f84.h);
        hashMap.put("display_version", f84.g);
        hashMap.put("source", Integer.toString(f84.i));
        String str = f84.j;
        if (str != null) {
            hashMap.put("icon_hash", str);
        }
        String str2 = f84.f;
        if (!CommonUtils.b(str2)) {
            hashMap.put("instance", str2);
        }
        return hashMap;
    }

    @DexIgnore
    public JSONObject a(HttpRequest httpRequest) {
        int g = httpRequest.g();
        z44 g2 = r44.g();
        g2.d("Fabric", "Settings result was: " + g);
        if (a(g)) {
            return b(httpRequest.a());
        }
        z44 g3 = r44.g();
        g3.e("Fabric", "Failed to retrieve settings from " + b());
        return null;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, f84 f84) {
        a(httpRequest, "X-CRASHLYTICS-API-KEY", f84.a);
        a(httpRequest, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a(httpRequest, "X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        a(httpRequest, Constants.ACCEPT_HEADER, Constants.APPLICATION_JSON);
        a(httpRequest, "X-CRASHLYTICS-DEVICE-MODEL", f84.b);
        a(httpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", f84.c);
        a(httpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", f84.d);
        a(httpRequest, "X-CRASHLYTICS-INSTALLATION-ID", f84.e);
        return httpRequest;
    }

    @DexIgnore
    public final void a(HttpRequest httpRequest, String str, String str2) {
        if (str2 != null) {
            httpRequest.c(str, str2);
        }
    }
}
