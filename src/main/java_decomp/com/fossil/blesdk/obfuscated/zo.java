package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.fossil.blesdk.obfuscated.to;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zo implements to<InputStream> {
    @DexIgnore
    public static /* final */ b k; // = new a();
    @DexIgnore
    public /* final */ mr e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ b g;
    @DexIgnore
    public HttpURLConnection h;
    @DexIgnore
    public InputStream i;
    @DexIgnore
    public volatile boolean j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements b {
        @DexIgnore
        public HttpURLConnection a(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        HttpURLConnection a(URL url) throws IOException;
    }

    @DexIgnore
    public zo(mr mrVar, int i2) {
        this(mrVar, i2, k);
    }

    @DexIgnore
    public static boolean b(int i2) {
        return i2 / 100 == 3;
    }

    @DexIgnore
    public void a(Priority priority, to.a<? super InputStream> aVar) {
        StringBuilder sb;
        long a2 = qw.a();
        try {
            aVar.a(a(this.e.f(), 0, (URL) null, this.e.c()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(qw.a(a2));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            aVar.a((Exception) e2);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + qw.a(a2));
            }
            throw th;
        }
    }

    @DexIgnore
    public void cancel() {
        this.j = true;
    }

    @DexIgnore
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public zo(mr mrVar, int i2, b bVar) {
        this.e = mrVar;
        this.f = i2;
        this.g = bVar;
    }

    @DexIgnore
    public DataSource b() {
        return DataSource.REMOTE;
    }

    @DexIgnore
    public final InputStream a(URL url, int i2, URL url2, Map<String, String> map) throws IOException {
        if (i2 < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new HttpException("In re-direct loop");
                    }
                } catch (URISyntaxException unused) {
                }
            }
            this.h = this.g.a(url);
            for (Map.Entry next : map.entrySet()) {
                this.h.addRequestProperty((String) next.getKey(), (String) next.getValue());
            }
            this.h.setConnectTimeout(this.f);
            this.h.setReadTimeout(this.f);
            this.h.setUseCaches(false);
            this.h.setDoInput(true);
            this.h.setInstanceFollowRedirects(false);
            this.h.connect();
            this.i = this.h.getInputStream();
            if (this.j) {
                return null;
            }
            int responseCode = this.h.getResponseCode();
            if (a(responseCode)) {
                return a(this.h);
            }
            if (b(responseCode)) {
                String headerField = this.h.getHeaderField("Location");
                if (!TextUtils.isEmpty(headerField)) {
                    URL url3 = new URL(url, headerField);
                    a();
                    return a(url3, i2 + 1, url, map);
                }
                throw new HttpException("Received empty or null redirect url");
            } else if (responseCode == -1) {
                throw new HttpException(responseCode);
            } else {
                throw new HttpException(this.h.getResponseMessage(), responseCode);
            }
        } else {
            throw new HttpException("Too many (> 5) redirects!");
        }
    }

    @DexIgnore
    public static boolean a(int i2) {
        return i2 / 100 == 2;
    }

    @DexIgnore
    public final InputStream a(HttpURLConnection httpURLConnection) throws IOException {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.i = nw.a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.i = httpURLConnection.getInputStream();
        }
        return this.i;
    }

    @DexIgnore
    public void a() {
        InputStream inputStream = this.i;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.h;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.h = null;
    }
}
