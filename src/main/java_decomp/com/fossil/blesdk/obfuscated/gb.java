package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gb extends Fragment {
    @DexIgnore
    public static /* final */ int INTERNAL_EMPTY_ID; // = 16711681;
    @DexIgnore
    public static /* final */ int INTERNAL_LIST_CONTAINER_ID; // = 16711683;
    @DexIgnore
    public static /* final */ int INTERNAL_PROGRESS_CONTAINER_ID; // = 16711682;
    @DexIgnore
    public ListAdapter mAdapter;
    @DexIgnore
    public CharSequence mEmptyText;
    @DexIgnore
    public View mEmptyView;
    @DexIgnore
    public /* final */ Handler mHandler; // = new Handler();
    @DexIgnore
    public ListView mList;
    @DexIgnore
    public View mListContainer;
    @DexIgnore
    public boolean mListShown;
    @DexIgnore
    public /* final */ AdapterView.OnItemClickListener mOnClickListener; // = new b();
    @DexIgnore
    public View mProgressContainer;
    @DexIgnore
    public /* final */ Runnable mRequestFocus; // = new a();
    @DexIgnore
    public TextView mStandardEmptyView;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            ListView listView = gb.this.mList;
            listView.focusableViewAvailable(listView);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements AdapterView.OnItemClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            gb.this.onListItemClick((ListView) adapterView, view, i, j);
        }
    }

    @DexIgnore
    private void ensureList() {
        if (this.mList == null) {
            View view = getView();
            if (view != null) {
                if (view instanceof ListView) {
                    this.mList = (ListView) view;
                } else {
                    this.mStandardEmptyView = (TextView) view.findViewById(INTERNAL_EMPTY_ID);
                    TextView textView = this.mStandardEmptyView;
                    if (textView == null) {
                        this.mEmptyView = view.findViewById(16908292);
                    } else {
                        textView.setVisibility(8);
                    }
                    this.mProgressContainer = view.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
                    this.mListContainer = view.findViewById(INTERNAL_LIST_CONTAINER_ID);
                    View findViewById = view.findViewById(16908298);
                    if (findViewById instanceof ListView) {
                        this.mList = (ListView) findViewById;
                        View view2 = this.mEmptyView;
                        if (view2 != null) {
                            this.mList.setEmptyView(view2);
                        } else {
                            CharSequence charSequence = this.mEmptyText;
                            if (charSequence != null) {
                                this.mStandardEmptyView.setText(charSequence);
                                this.mList.setEmptyView(this.mStandardEmptyView);
                            }
                        }
                    } else if (findViewById == null) {
                        throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                    } else {
                        throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                    }
                }
                this.mListShown = true;
                this.mList.setOnItemClickListener(this.mOnClickListener);
                ListAdapter listAdapter = this.mAdapter;
                if (listAdapter != null) {
                    this.mAdapter = null;
                    setListAdapter(listAdapter);
                } else if (this.mProgressContainer != null) {
                    setListShown(false, false);
                }
                this.mHandler.post(this.mRequestFocus);
                return;
            }
            throw new IllegalStateException("Content view not yet created");
        }
    }

    @DexIgnore
    public ListAdapter getListAdapter() {
        return this.mAdapter;
    }

    @DexIgnore
    public ListView getListView() {
        ensureList();
        return this.mList;
    }

    @DexIgnore
    public long getSelectedItemId() {
        ensureList();
        return this.mList.getSelectedItemId();
    }

    @DexIgnore
    public int getSelectedItemPosition() {
        ensureList();
        return this.mList.getSelectedItemPosition();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mList = null;
        this.mListShown = false;
        this.mListContainer = null;
        this.mProgressContainer = null;
        this.mEmptyView = null;
        this.mStandardEmptyView = null;
        super.onDestroyView();
    }

    @DexIgnore
    public void onListItemClick(ListView listView, View view, int i, long j) {
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ensureList();
    }

    @DexIgnore
    public void setEmptyText(CharSequence charSequence) {
        ensureList();
        TextView textView = this.mStandardEmptyView;
        if (textView != null) {
            textView.setText(charSequence);
            if (this.mEmptyText == null) {
                this.mList.setEmptyView(this.mStandardEmptyView);
            }
            this.mEmptyText = charSequence;
            return;
        }
        throw new IllegalStateException("Can't be used with a custom content view");
    }

    @DexIgnore
    public void setListAdapter(ListAdapter listAdapter) {
        boolean z = false;
        boolean z2 = this.mAdapter != null;
        this.mAdapter = listAdapter;
        ListView listView = this.mList;
        if (listView != null) {
            listView.setAdapter(listAdapter);
            if (!this.mListShown && !z2) {
                if (getView().getWindowToken() != null) {
                    z = true;
                }
                setListShown(true, z);
            }
        }
    }

    @DexIgnore
    public void setListShown(boolean z) {
        setListShown(z, true);
    }

    @DexIgnore
    public void setListShownNoAnimation(boolean z) {
        setListShown(z, false);
    }

    @DexIgnore
    public void setSelection(int i) {
        ensureList();
        this.mList.setSelection(i);
    }

    @DexIgnore
    private void setListShown(boolean z, boolean z2) {
        ensureList();
        View view = this.mProgressContainer;
        if (view == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        } else if (this.mListShown != z) {
            this.mListShown = z;
            if (z) {
                if (z2) {
                    view.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
                    this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                } else {
                    view.clearAnimation();
                    this.mListContainer.clearAnimation();
                }
                this.mProgressContainer.setVisibility(8);
                this.mListContainer.setVisibility(0);
                return;
            }
            if (z2) {
                view.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
            } else {
                view.clearAnimation();
                this.mListContainer.clearAnimation();
            }
            this.mProgressContainer.setVisibility(0);
            this.mListContainer.setVisibility(8);
        }
    }
}
