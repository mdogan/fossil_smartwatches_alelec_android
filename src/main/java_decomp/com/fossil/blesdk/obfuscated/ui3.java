package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ui3 implements Factory<DeleteAccountPresenter> {
    @DexIgnore
    public static DeleteAccountPresenter a(ri3 ri3, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, lr2 lr2, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new DeleteAccountPresenter(ri3, deviceRepository, analyticsHelper, lr2, deleteLogoutUserUseCase);
    }
}
