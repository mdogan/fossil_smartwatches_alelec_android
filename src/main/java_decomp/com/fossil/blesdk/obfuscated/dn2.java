package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.telecom.TelecomManager;
import com.facebook.appevents.internal.InAppPurchaseEventManager;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn2 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static dn2 b;
    @DexIgnore
    public static /* final */ a c; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(dn2 dn2) {
            dn2.b = dn2;
        }

        @DexIgnore
        public final dn2 b() {
            return dn2.b;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final synchronized dn2 a() {
            dn2 b;
            if (dn2.c.b() == null) {
                dn2.c.a(new dn2((rd4) null));
            }
            b = dn2.c.b();
            if (b == null) {
                wd4.a();
                throw null;
            }
            return b;
        }
    }

    /*
    static {
        String simpleName = dn2.class.getSimpleName();
        wd4.a((Object) simpleName, "PhoneCallManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public dn2() {
    }

    @DexIgnore
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void b() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.W.c().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            wd4.a((Object) cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            wd4.a((Object) cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            wd4.a((Object) cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", new Class[]{String.class});
            wd4.a((Object) method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod(InAppPurchaseEventManager.AS_INTERFACE, new Class[]{IBinder.class});
            wd4.a((Object) method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface((IInterface) null, "fake");
            Object invoke = method.invoke(method2.invoke((Object) null, new Object[]{binder}), new Object[]{PlaceFields.PHONE});
            if (invoke != null) {
                Method method3 = cls2.getMethod(InAppPurchaseEventManager.AS_INTERFACE, new Class[]{IBinder.class});
                wd4.a((Object) method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke((Object) null, new Object[]{(IBinder) invoke});
                Method method4 = cls.getMethod("endCall", new Class[0]);
                wd4.a((Object) method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when decline call " + e2);
        }
    }

    @DexIgnore
    public /* synthetic */ dn2(rd4 rd4) {
        this();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        try {
            Object systemService = PortfolioApp.W.c().getSystemService("telecom");
            if (systemService != null) {
                ((TelecomManager) systemService).acceptRingingCall();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.telecom.TelecomManager");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when accept call call " + e);
        }
    }
}
