package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v41 implements Parcelable.Creator<u41> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ld1 ld1 = u41.i;
        List<kj0> list = u41.h;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                ld1 = (ld1) SafeParcelReader.a(parcel, a, ld1.CREATOR);
            } else if (a2 == 2) {
                list = SafeParcelReader.c(parcel, a, kj0.CREATOR);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                str = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new u41(ld1, list, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new u41[i];
    }
}
