package com.fossil.blesdk.obfuscated;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gu1<K, V> extends hu1<K, V> implements ConcurrentMap<K, V> {
    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Map delegate();

    @DexIgnore
    public abstract ConcurrentMap<K, V> delegate();

    @DexIgnore
    public V putIfAbsent(K k, V v) {
        return delegate().putIfAbsent(k, v);
    }

    @DexIgnore
    public boolean remove(Object obj, Object obj2) {
        return delegate().remove(obj, obj2);
    }

    @DexIgnore
    public V replace(K k, V v) {
        return delegate().replace(k, v);
    }

    @DexIgnore
    public boolean replace(K k, V v, V v2) {
        return delegate().replace(k, v, v2);
    }
}
