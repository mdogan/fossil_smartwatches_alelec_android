package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vq2 implements Factory<uq2> {
    @DexIgnore
    public static /* final */ vq2 a; // = new vq2();

    @DexIgnore
    public static vq2 a() {
        return a;
    }

    @DexIgnore
    public static uq2 b() {
        return new uq2();
    }

    @DexIgnore
    public uq2 get() {
        return b();
    }
}
