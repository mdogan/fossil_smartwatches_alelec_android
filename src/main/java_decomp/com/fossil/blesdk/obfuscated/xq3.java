package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xq3 extends as2 implements wq3, View.OnClickListener {
    @DexIgnore
    public vq3 j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void C0() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.B;
        Context context = getContext();
        if (context != null) {
            wd4.a((Object) context, "context!!");
            aVar.a(context);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void F0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.G;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.fb_get_started) {
                vq3 vq3 = this.j;
                if (vq3 != null) {
                    vq3.h();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (id == R.id.iv_note) {
                vq3 vq32 = this.j;
                if (vq32 != null) {
                    vq32.i();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_welcome, viewGroup, false);
        ((FlexibleButton) inflate.findViewById(R.id.fb_get_started)).setOnClickListener(this);
        ((ImageView) inflate.findViewById(R.id.iv_note)).setOnClickListener(this);
        R("tutorial_view");
        return inflate;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        vq3 vq3 = this.j;
        if (vq3 != null) {
            vq3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vq3 vq3 = this.j;
        if (vq3 != null) {
            vq3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(vq3 vq3) {
        wd4.b(vq3, "presenter");
        this.j = vq3;
    }
}
