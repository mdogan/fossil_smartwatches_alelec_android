package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tr<Model, Data> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> {
        @DexIgnore
        public /* final */ ko a;
        @DexIgnore
        public /* final */ List<ko> b;
        @DexIgnore
        public /* final */ to<Data> c;

        @DexIgnore
        public a(ko koVar, to<Data> toVar) {
            this(koVar, Collections.emptyList(), toVar);
        }

        @DexIgnore
        public a(ko koVar, List<ko> list, to<Data> toVar) {
            uw.a(koVar);
            this.a = koVar;
            uw.a(list);
            this.b = list;
            uw.a(toVar);
            this.c = toVar;
        }
    }

    @DexIgnore
    a<Data> a(Model model, int i, int i2, mo moVar);

    @DexIgnore
    boolean a(Model model);
}
