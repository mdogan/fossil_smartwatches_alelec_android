package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bb3 extends as2 implements ab3, kt2, ls2 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<bb2> j;
    @DexIgnore
    public za3 k;
    @DexIgnore
    public ht2 l;
    @DexIgnore
    public GoalTrackingOverviewFragment m;
    @DexIgnore
    public cu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bb3.p;
        }

        @DexIgnore
        public final bb3 b() {
            return new bb3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cu3 {
        @DexIgnore
        public /* final */ /* synthetic */ bb3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, bb3 bb3, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = bb3;
        }

        @DexIgnore
        public void a(int i) {
            bb3.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = bb3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "DashboardGoalTrackingFra\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ za3 a(bb3 bb3) {
        za3 za3 = bb3.k;
        if (za3 != null) {
            return za3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (isVisible() && this.j != null) {
            bb2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    RecyclerView.ViewHolder c = recyclerView.c(0);
                    if (c != null) {
                        View view = c.itemView;
                        if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            return;
                        }
                    }
                    recyclerView.j(0);
                    cu3 cu3 = this.n;
                    if (cu3 != null) {
                        cu3.a();
                    }
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final bb2 T0() {
        ur3<bb2> ur3 = this.j;
        if (ur3 != null) {
            return ur3.a();
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.D;
            wd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        cu3 cu3 = this.n;
        if (cu3 != null) {
            cu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (bb2) ra.a(layoutInflater, R.layout.fragment_dashboard_goal_tracking, viewGroup, false, O0()));
        ur3<bb2> ur3 = this.j;
        if (ur3 != null) {
            bb2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        za3 za3 = this.k;
        if (za3 == null) {
            return;
        }
        if (za3 != null) {
            za3.i();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onDestroyView() {
        za3 za3 = this.k;
        if (za3 != null) {
            za3.i();
            super.onDestroyView();
            N0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        za3 za3 = this.k;
        if (za3 != null) {
            za3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        za3 za3 = this.k;
        if (za3 != null) {
            za3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m = (GoalTrackingOverviewFragment) getChildFragmentManager().a("GoalTrackingOverviewFragment");
        if (this.m == null) {
            this.m = new GoalTrackingOverviewFragment();
        }
        jt2 jt2 = new jt2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wd4.a((Object) childFragmentManager, "childFragmentManager");
        GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.m;
        if (goalTrackingOverviewFragment != null) {
            this.l = new ht2(jt2, c, this, childFragmentManager, goalTrackingOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            bb2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    ht2 ht2 = this.l;
                    if (ht2 != null) {
                        recyclerView.setAdapter(ht2);
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            this.n = new b(recyclerView, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                            cu3 cu3 = this.n;
                            if (cu3 != null) {
                                recyclerView.a((RecyclerView.q) cu3);
                                recyclerView.setItemViewCacheSize(0);
                                m73 m73 = new m73(linearLayoutManager.M());
                                Drawable c2 = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_dashboard_line_1w);
                                if (c2 != null) {
                                    wd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                    m73.a(c2);
                                    recyclerView.a((RecyclerView.l) m73);
                                    za3 za3 = this.k;
                                    if (za3 != null) {
                                        za3.h();
                                    } else {
                                        wd4.d("mPresenter");
                                        throw null;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                    } else {
                        wd4.d("mDashboardGoalTrackingAdapter");
                        throw null;
                    }
                }
            }
            bb2 T02 = T0();
            if (T02 != null) {
                RecyclerView recyclerView2 = T02.q;
            }
            bb2 T03 = T0();
            if (T03 != null) {
                RecyclerView recyclerView3 = T03.q;
                if (recyclerView3 != null) {
                    wd4.a((Object) recyclerView3, "recyclerView");
                    RecyclerView.j itemAnimator = recyclerView3.getItemAnimator();
                    if (itemAnimator instanceof bf) {
                        ((bf) itemAnimator).setSupportsChangeAnimations(false);
                    }
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                jc a2 = mc.a(activity).a(ju3.class);
                wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                ju3 ju3 = (ju3) a2;
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(rd<GoalTrackingSummary> rdVar) {
        ht2 ht2 = this.l;
        if (ht2 != null) {
            ht2.c(rdVar);
        } else {
            wd4.d("mDashboardGoalTrackingAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(za3 za3) {
        wd4.b(za3, "presenter");
        this.k = za3;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wd4.b(date, "startWeekDate");
        wd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
