package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cn4 {
    @DexIgnore
    ym4 a(Response response) throws IOException;

    @DexIgnore
    void a();

    @DexIgnore
    void a(pm4 pm4) throws IOException;

    @DexIgnore
    void a(zm4 zm4);

    @DexIgnore
    void a(Response response, Response response2);

    @DexIgnore
    Response b(pm4 pm4) throws IOException;
}
