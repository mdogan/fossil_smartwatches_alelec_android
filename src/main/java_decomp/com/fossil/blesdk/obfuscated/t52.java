package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t52 implements Factory<ln2> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public t52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static t52 a(o42 o42) {
        return new t52(o42);
    }

    @DexIgnore
    public static ln2 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static ln2 c(o42 o42) {
        ln2 q = o42.q();
        o44.a(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    public ln2 get() {
        return b(this.a);
    }
}
