package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tr4 extends pr4.a {
    @DexIgnore
    public /* final */ Executor a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements pr4<Object, Call<?>> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public a(tr4 tr4, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public Call<Object> a(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor e;
        @DexIgnore
        public /* final */ Call<T> f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements qr4<T> {
            @DexIgnore
            public /* final */ /* synthetic */ qr4 e;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tr4$b$a$a")
            /* renamed from: com.fossil.blesdk.obfuscated.tr4$b$a$a  reason: collision with other inner class name */
            public class C0102a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ cs4 e;

                @DexIgnore
                public C0102a(cs4 cs4) {
                    this.e = cs4;
                }

                @DexIgnore
                public void run() {
                    if (b.this.f.o()) {
                        a aVar = a.this;
                        aVar.e.onFailure(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.e.onResponse(b.this, this.e);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tr4$b$a$b")
            /* renamed from: com.fossil.blesdk.obfuscated.tr4$b$a$b  reason: collision with other inner class name */
            public class C0103b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable e;

                @DexIgnore
                public C0103b(Throwable th) {
                    this.e = th;
                }

                @DexIgnore
                public void run() {
                    a aVar = a.this;
                    aVar.e.onFailure(b.this, this.e);
                }
            }

            @DexIgnore
            public a(qr4 qr4) {
                this.e = qr4;
            }

            @DexIgnore
            public void onFailure(Call<T> call, Throwable th) {
                b.this.e.execute(new C0103b(th));
            }

            @DexIgnore
            public void onResponse(Call<T> call, cs4<T> cs4) {
                b.this.e.execute(new C0102a(cs4));
            }
        }

        @DexIgnore
        public b(Executor executor, Call<T> call) {
            this.e = executor;
            this.f = call;
        }

        @DexIgnore
        public void a(qr4<T> qr4) {
            gs4.a(qr4, "callback == null");
            this.f.a(new a(qr4));
        }

        @DexIgnore
        public void cancel() {
            this.f.cancel();
        }

        @DexIgnore
        public pm4 n() {
            return this.f.n();
        }

        @DexIgnore
        public boolean o() {
            return this.f.o();
        }

        @DexIgnore
        public cs4<T> r() throws IOException {
            return this.f.r();
        }

        @DexIgnore
        public Call<T> clone() {
            return new b(this.e, this.f.clone());
        }
    }

    @DexIgnore
    public tr4(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public pr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (pr4.a.a(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = gs4.b(0, (ParameterizedType) type);
            if (!gs4.a(annotationArr, (Class<? extends Annotation>) es4.class)) {
                executor = this.a;
            }
            return new a(this, b2, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
