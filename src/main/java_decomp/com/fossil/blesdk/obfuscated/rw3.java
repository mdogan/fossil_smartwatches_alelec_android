package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.kv3;
import com.squareup.okhttp.Protocol;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rw3 implements ex3 {
    @DexIgnore
    public static /* final */ List<ByteString> d; // = xv3.a((T[]) new ByteString[]{ByteString.encodeUtf8("connection"), ByteString.encodeUtf8("host"), ByteString.encodeUtf8("keep-alive"), ByteString.encodeUtf8("proxy-connection"), ByteString.encodeUtf8("transfer-encoding")});
    @DexIgnore
    public static /* final */ List<ByteString> e; // = xv3.a((T[]) new ByteString[]{ByteString.encodeUtf8("connection"), ByteString.encodeUtf8("host"), ByteString.encodeUtf8("keep-alive"), ByteString.encodeUtf8("proxy-connection"), ByteString.encodeUtf8("te"), ByteString.encodeUtf8("transfer-encoding"), ByteString.encodeUtf8("encoding"), ByteString.encodeUtf8("upgrade")});
    @DexIgnore
    public /* final */ vw3 a;
    @DexIgnore
    public /* final */ bw3 b;
    @DexIgnore
    public cw3 c;

    @DexIgnore
    public rw3(vw3 vw3, bw3 bw3) {
        this.a = vw3;
        this.b = bw3;
    }

    @DexIgnore
    public jp4 a(iv3 iv3, long j) throws IOException {
        return this.c.e();
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public kv3.b c() throws IOException {
        return a(this.c.d(), this.b.z());
    }

    @DexIgnore
    public boolean d() {
        return true;
    }

    @DexIgnore
    public void a(iv3 iv3) throws IOException {
        if (this.c == null) {
            this.a.o();
            boolean j = this.a.j();
            String a2 = ax3.a(this.a.e().d());
            bw3 bw3 = this.b;
            this.c = bw3.a(a(iv3, bw3.z(), a2), j, true);
            this.c.i().a((long) this.a.a.y(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a(bx3 bx3) throws IOException {
        bx3.a(this.c.e());
    }

    @DexIgnore
    public void a() throws IOException {
        this.c.e().close();
    }

    @DexIgnore
    public static List<dw3> a(iv3 iv3, Protocol protocol, String str) {
        dv3 c2 = iv3.c();
        ArrayList arrayList = new ArrayList(c2.b() + 10);
        arrayList.add(new dw3(dw3.e, iv3.f()));
        arrayList.add(new dw3(dw3.f, ax3.a(iv3.d())));
        String a2 = xv3.a(iv3.d());
        if (Protocol.SPDY_3 == protocol) {
            arrayList.add(new dw3(dw3.j, str));
            arrayList.add(new dw3(dw3.i, a2));
        } else if (Protocol.HTTP_2 == protocol) {
            arrayList.add(new dw3(dw3.h, a2));
        } else {
            throw new AssertionError();
        }
        arrayList.add(new dw3(dw3.g, iv3.d().j()));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int b2 = c2.b();
        for (int i = 0; i < b2; i++) {
            ByteString encodeUtf8 = ByteString.encodeUtf8(c2.a(i).toLowerCase(Locale.US));
            String b3 = c2.b(i);
            if (!a(protocol, encodeUtf8) && !encodeUtf8.equals(dw3.e) && !encodeUtf8.equals(dw3.f) && !encodeUtf8.equals(dw3.g) && !encodeUtf8.equals(dw3.h) && !encodeUtf8.equals(dw3.i) && !encodeUtf8.equals(dw3.j)) {
                if (linkedHashSet.add(encodeUtf8)) {
                    arrayList.add(new dw3(encodeUtf8, b3));
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= arrayList.size()) {
                            break;
                        } else if (((dw3) arrayList.get(i2)).a.equals(encodeUtf8)) {
                            arrayList.set(i2, new dw3(encodeUtf8, a(((dw3) arrayList.get(i2)).b.utf8(), b3)));
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(String str, String str2) {
        return str + 0 + str2;
    }

    @DexIgnore
    public static kv3.b a(List<dw3> list, Protocol protocol) throws IOException {
        dv3.b bVar = new dv3.b();
        bVar.d(yw3.e, protocol.toString());
        int size = list.size();
        String str = "HTTP/1.1";
        String str2 = null;
        int i = 0;
        while (i < size) {
            ByteString byteString = list.get(i).a;
            String utf8 = list.get(i).b.utf8();
            String str3 = str;
            String str4 = str2;
            int i2 = 0;
            while (i2 < utf8.length()) {
                int indexOf = utf8.indexOf(0, i2);
                if (indexOf == -1) {
                    indexOf = utf8.length();
                }
                String substring = utf8.substring(i2, indexOf);
                if (byteString.equals(dw3.d)) {
                    str4 = substring;
                } else if (byteString.equals(dw3.j)) {
                    str3 = substring;
                } else if (!a(protocol, byteString)) {
                    bVar.a(byteString.utf8(), substring);
                }
                i2 = indexOf + 1;
            }
            i++;
            str2 = str4;
            str = str3;
        }
        if (str2 != null) {
            dx3 a2 = dx3.a(str + " " + str2);
            kv3.b bVar2 = new kv3.b();
            bVar2.a(protocol);
            bVar2.a(a2.b);
            bVar2.a(a2.c);
            bVar2.a(bVar.a());
            return bVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    public lv3 a(kv3 kv3) throws IOException {
        return new zw3(kv3.g(), ep4.a(this.c.f()));
    }

    @DexIgnore
    public static boolean a(Protocol protocol, ByteString byteString) {
        if (protocol == Protocol.SPDY_3) {
            return d.contains(byteString);
        }
        if (protocol == Protocol.HTTP_2) {
            return e.contains(byteString);
        }
        throw new AssertionError(protocol);
    }
}
