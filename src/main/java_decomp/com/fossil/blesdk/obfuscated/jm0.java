package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jm0 {
    @DexIgnore
    public static boolean a(Collection<?> collection) {
        if (collection == null) {
            return true;
        }
        return collection.isEmpty();
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> a() {
        return Collections.emptyList();
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> a(T t) {
        return Collections.singletonList(t);
    }

    @DexIgnore
    @Deprecated
    public static <T> List<T> a(T... tArr) {
        int length = tArr.length;
        if (length == 0) {
            return a();
        }
        if (length != 1) {
            return Collections.unmodifiableList(Arrays.asList(tArr));
        }
        return a(tArr[0]);
    }
}
