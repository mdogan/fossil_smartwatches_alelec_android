package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pa4<T> implements ca4<T> {
    @DexIgnore
    public static /* final */ int m; // = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
    @DexIgnore
    public static /* final */ Object n; // = new Object();
    @DexIgnore
    public /* final */ AtomicLong e; // = new AtomicLong();
    @DexIgnore
    public int f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public AtomicReferenceArray<Object> i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public AtomicReferenceArray<Object> k;
    @DexIgnore
    public /* final */ AtomicLong l; // = new AtomicLong();

    @DexIgnore
    public pa4(int i2) {
        int a = sa4.a(Math.max(8, i2));
        int i3 = a - 1;
        AtomicReferenceArray<Object> atomicReferenceArray = new AtomicReferenceArray<>(a + 1);
        this.i = atomicReferenceArray;
        this.h = i3;
        a(a);
        this.k = atomicReferenceArray;
        this.j = i3;
        this.g = (long) (i3 - 1);
        b(0);
    }

    @DexIgnore
    public static int b(int i2) {
        return i2;
    }

    @DexIgnore
    public final boolean a(AtomicReferenceArray<Object> atomicReferenceArray, T t, long j2, int i2) {
        a(atomicReferenceArray, i2, (Object) t);
        b(j2 + 1);
        return true;
    }

    @DexIgnore
    public final long b() {
        return this.e.get();
    }

    @DexIgnore
    public final long c() {
        return this.l.get();
    }

    @DexIgnore
    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    @DexIgnore
    public final long d() {
        return this.e.get();
    }

    @DexIgnore
    public boolean isEmpty() {
        return d() == c();
    }

    @DexIgnore
    public boolean offer(T t) {
        if (t != null) {
            AtomicReferenceArray<Object> atomicReferenceArray = this.i;
            long b = b();
            int i2 = this.h;
            int a = a(b, i2);
            if (b < this.g) {
                return a(atomicReferenceArray, t, b, a);
            }
            long j2 = ((long) this.f) + b;
            if (b(atomicReferenceArray, a(j2, i2)) == null) {
                this.g = j2 - 1;
                return a(atomicReferenceArray, t, b, a);
            } else if (b(atomicReferenceArray, a(1 + b, i2)) == null) {
                return a(atomicReferenceArray, t, b, a);
            } else {
                a(atomicReferenceArray, b, a, t, (long) i2);
                return true;
            }
        } else {
            throw new NullPointerException("Null is not a valid element");
        }
    }

    @DexIgnore
    public T poll() {
        AtomicReferenceArray<Object> atomicReferenceArray = this.k;
        long a = a();
        int i2 = this.j;
        int a2 = a(a, i2);
        T b = b(atomicReferenceArray, a2);
        boolean z = b == n;
        if (b != null && !z) {
            a(atomicReferenceArray, a2, (Object) null);
            a(a + 1);
            return b;
        } else if (z) {
            return a(a(atomicReferenceArray, i2 + 1), a, i2);
        } else {
            return null;
        }
    }

    @DexIgnore
    public final void b(long j2) {
        this.e.lazySet(j2);
    }

    @DexIgnore
    public static <E> Object b(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        return atomicReferenceArray.get(i2);
    }

    @DexIgnore
    public final void a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2, T t, long j3) {
        AtomicReferenceArray<Object> atomicReferenceArray2 = new AtomicReferenceArray<>(atomicReferenceArray.length());
        this.i = atomicReferenceArray2;
        this.g = (j3 + j2) - 1;
        a(atomicReferenceArray2, i2, (Object) t);
        a(atomicReferenceArray, atomicReferenceArray2);
        a(atomicReferenceArray, i2, n);
        b(j2 + 1);
    }

    @DexIgnore
    public final void a(AtomicReferenceArray<Object> atomicReferenceArray, AtomicReferenceArray<Object> atomicReferenceArray2) {
        int length = atomicReferenceArray.length() - 1;
        b(length);
        a(atomicReferenceArray, length, (Object) atomicReferenceArray2);
    }

    @DexIgnore
    public final AtomicReferenceArray<Object> a(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        b(i2);
        AtomicReferenceArray<Object> atomicReferenceArray2 = (AtomicReferenceArray) b(atomicReferenceArray, i2);
        a(atomicReferenceArray, i2, (Object) null);
        return atomicReferenceArray2;
    }

    @DexIgnore
    public final T a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2) {
        this.k = atomicReferenceArray;
        int a = a(j2, i2);
        T b = b(atomicReferenceArray, a);
        if (b != null) {
            a(atomicReferenceArray, a, (Object) null);
            a(j2 + 1);
        }
        return b;
    }

    @DexIgnore
    public final void a(int i2) {
        this.f = Math.min(i2 / 4, m);
    }

    @DexIgnore
    public final long a() {
        return this.l.get();
    }

    @DexIgnore
    public final void a(long j2) {
        this.l.lazySet(j2);
    }

    @DexIgnore
    public static int a(long j2, int i2) {
        int i3 = ((int) j2) & i2;
        b(i3);
        return i3;
    }

    @DexIgnore
    public static void a(AtomicReferenceArray<Object> atomicReferenceArray, int i2, Object obj) {
        atomicReferenceArray.lazySet(i2, obj);
    }
}
