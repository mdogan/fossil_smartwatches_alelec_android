package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xv<T> implements cw<T> {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public pv g;

    @DexIgnore
    public xv() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(Drawable drawable) {
    }

    @DexIgnore
    public final void a(bw bwVar) {
    }

    @DexIgnore
    public final void a(pv pvVar) {
        this.g = pvVar;
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Drawable drawable) {
    }

    @DexIgnore
    public final void b(bw bwVar) {
        bwVar.a(this.e, this.f);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public final pv d() {
        return this.g;
    }

    @DexIgnore
    public xv(int i, int i2) {
        if (vw.b(i, i2)) {
            this.e = i;
            this.f = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }
}
