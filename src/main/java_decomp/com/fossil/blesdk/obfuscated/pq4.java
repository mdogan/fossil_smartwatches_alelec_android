package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pq4 {
    @DexIgnore
    public static volatile int a;
    @DexIgnore
    public static /* final */ vq4 b; // = new vq4();
    @DexIgnore
    public static /* final */ tq4 c; // = new tq4();
    @DexIgnore
    public static /* final */ String[] d; // = {"1.6", "1.7"};
    @DexIgnore
    public static String e; // = "org/slf4j/impl/StaticLoggerBinder.class";

    /*
    static {
        wq4.b("slf4j.detectLoggerNameMismatch");
    }
    */

    @DexIgnore
    public static final void a() {
        Set<URL> set = null;
        try {
            if (!f()) {
                set = c();
                c(set);
            }
            xq4.c();
            a = 3;
            b(set);
            d();
            h();
            b.a();
        } catch (NoClassDefFoundError e2) {
            if (b(e2.getMessage())) {
                a = 4;
                wq4.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                wq4.a("Defaulting to no-operation (NOP) logger implementation");
                wq4.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            a((Throwable) e2);
            throw e2;
        } catch (NoSuchMethodError e3) {
            String message = e3.getMessage();
            if (message != null && message.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                a = 2;
                wq4.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                wq4.a("Your binding is version 1.5.5 or earlier.");
                wq4.a("Upgrade your binding to version 1.6.x.");
            }
            throw e3;
        } catch (Exception e4) {
            a((Throwable) e4);
            throw new IllegalStateException("Unexpected initialization failure", e4);
        }
    }

    @DexIgnore
    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        return str.contains("org/slf4j/impl/StaticLoggerBinder") || str.contains("org.slf4j.impl.StaticLoggerBinder");
    }

    @DexIgnore
    public static Set<URL> c() {
        Enumeration<URL> enumeration;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            ClassLoader classLoader = pq4.class.getClassLoader();
            if (classLoader == null) {
                enumeration = ClassLoader.getSystemResources(e);
            } else {
                enumeration = classLoader.getResources(e);
            }
            while (enumeration.hasMoreElements()) {
                linkedHashSet.add(enumeration.nextElement());
            }
        } catch (IOException e2) {
            wq4.a("Error getting resources from path", e2);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static void d() {
        synchronized (b) {
            b.d();
            for (uq4 next : b.c()) {
                next.a(a(next.c()));
            }
        }
    }

    @DexIgnore
    public static nq4 e() {
        if (a == 0) {
            synchronized (pq4.class) {
                if (a == 0) {
                    a = 1;
                    g();
                }
            }
        }
        int i = a;
        if (i == 1) {
            return b;
        }
        if (i == 2) {
            throw new IllegalStateException("org.slf4j.LoggerFactory in failed state. Original exception was thrown EARLIER. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
        } else if (i == 3) {
            return xq4.c().a();
        } else {
            if (i == 4) {
                return c;
            }
            throw new IllegalStateException("Unreachable code");
        }
    }

    @DexIgnore
    public static boolean f() {
        String c2 = wq4.c("java.vendor.url");
        if (c2 == null) {
            return false;
        }
        return c2.toLowerCase().contains("android");
    }

    @DexIgnore
    public static final void g() {
        a();
        if (a == 3) {
            i();
        }
    }

    @DexIgnore
    public static void h() {
        LinkedBlockingQueue<sq4> b2 = b.b();
        int size = b2.size();
        ArrayList<sq4> arrayList = new ArrayList<>(128);
        int i = 0;
        while (b2.drainTo(arrayList, 128) != 0) {
            for (sq4 sq4 : arrayList) {
                a(sq4);
                int i2 = i + 1;
                if (i == 0) {
                    a(sq4, size);
                }
                i = i2;
            }
            arrayList.clear();
        }
    }

    @DexIgnore
    public static final void i() {
        try {
            String str = xq4.c;
            boolean z = false;
            for (String startsWith : d) {
                if (str.startsWith(startsWith)) {
                    z = true;
                }
            }
            if (!z) {
                wq4.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                wq4.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError unused) {
        } catch (Throwable th) {
            wq4.a("Unexpected problem occured during version sanity check", th);
        }
    }

    @DexIgnore
    public static void b() {
        wq4.a("The following set of substitute loggers may have been accessed");
        wq4.a("during the initialization phase. Logging calls during this");
        wq4.a("phase were not honored. However, subsequent logging calls to these");
        wq4.a("loggers will work as normally expected.");
        wq4.a("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    @DexIgnore
    public static void b(Set<URL> set) {
        if (set != null && a(set)) {
            wq4.a("Actual binding is of type [" + xq4.c().b() + "]");
        }
    }

    @DexIgnore
    public static void c(Set<URL> set) {
        if (a(set)) {
            wq4.a("Class path contains multiple SLF4J bindings.");
            for (URL url : set) {
                wq4.a("Found binding in [" + url + "]");
            }
            wq4.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    @DexIgnore
    public static void a(Throwable th) {
        a = 2;
        wq4.a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    @DexIgnore
    public static void a(sq4 sq4, int i) {
        if (sq4.a().d()) {
            a(i);
        } else if (!sq4.a().e()) {
            b();
        }
    }

    @DexIgnore
    public static void a(sq4 sq4) {
        if (sq4 != null) {
            uq4 a2 = sq4.a();
            String c2 = a2.c();
            if (a2.f()) {
                throw new IllegalStateException("Delegate logger cannot be null at this state.");
            } else if (!a2.e()) {
                if (a2.d()) {
                    a2.a((rq4) sq4);
                } else {
                    wq4.a(c2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(int i) {
        wq4.a("A number (" + i + ") of logging calls during the initialization phase have been intercepted and are");
        wq4.a("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        wq4.a("See also http://www.slf4j.org/codes.html#replay");
    }

    @DexIgnore
    public static boolean a(Set<URL> set) {
        return set.size() > 1;
    }

    @DexIgnore
    public static oq4 a(String str) {
        return e().a(str);
    }
}
