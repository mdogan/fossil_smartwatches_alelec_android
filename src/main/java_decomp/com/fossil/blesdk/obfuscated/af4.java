package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class af4<T> implements df4<T> {
    @DexIgnore
    public /* final */ AtomicReference<df4<T>> a;

    @DexIgnore
    public af4(df4<? extends T> df4) {
        wd4.b(df4, "sequence");
        this.a = new AtomicReference<>(df4);
    }

    @DexIgnore
    public Iterator<T> iterator() {
        df4 andSet = this.a.getAndSet((Object) null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
