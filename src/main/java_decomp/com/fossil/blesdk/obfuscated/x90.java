package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.os.Handler;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x90 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ File e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore
        public b(File file, x90 x90, String str) {
            this.e = file;
            this.f = str;
        }

        @DexIgnore
        public final void run() {
            hb0 hb0 = hb0.a;
            hb0.a(this.f + va0.y.w(), this.e);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public x90(String str, int i, Handler handler, String str2, String str3) {
        wd4.b(str, "logDirectory");
        wd4.b(handler, "handler");
        wd4.b(str2, "logFileNamePrefix");
        wd4.b(str3, "logFileNameExtension");
        this.a = str;
        this.b = i;
        this.c = handler;
        this.d = str2;
        this.e = str3;
    }

    @DexIgnore
    public final String a(int i) {
        be4 be4 = be4.a;
        Object[] objArr = {this.d, Integer.valueOf(i), this.e};
        String format = String.format("%s_%010d.%s", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String b(int i) {
        return this.a + File.separatorChar + a(i);
    }

    @DexIgnore
    public final File c() {
        int b2 = b();
        File d2 = d();
        if (d2 == null) {
            return null;
        }
        if (!d2.exists()) {
            d2.mkdirs();
        }
        File a2 = ab0.a.a(b(b2));
        if (a2 == null) {
            return null;
        }
        if (a2.length() < ((long) this.b)) {
            return a2;
        }
        f();
        return c();
    }

    @DexIgnore
    public final File d() {
        return ab0.a.a(this.a);
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public final void f() {
        int c2 = c(b());
        SharedPreferences a2 = bb0.a.a(e());
        if (a2 != null) {
            SharedPreferences.Editor edit = a2.edit();
            if (edit != null) {
                SharedPreferences.Editor putInt = edit.putInt("file_count", c2);
                if (putInt != null) {
                    putInt.apply();
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        wd4.b(str, "line");
        File c2 = c();
        if (c2 != null) {
            return this.c.post(new b(c2, this, str));
        }
        return false;
    }

    @DexIgnore
    public final int b() {
        SharedPreferences a2 = bb0.a.a(e());
        if (a2 != null) {
            return a2.getInt("file_count", 0);
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) com.fossil.blesdk.obfuscated.gd4.a(r6), (java.lang.Object) r12.e) != false) goto L_0x0058;
     */
    @DexIgnore
    public final File[] a() {
        File d2 = d();
        if (d2 == null || !d2.exists()) {
            u90.c.b("LogIO", "Log Folder not exist");
            return new File[0];
        }
        f();
        File[] listFiles = d2.listFiles();
        if (listFiles == null) {
            listFiles = new File[0];
        }
        File c2 = c();
        ArrayList arrayList = new ArrayList();
        int length = listFiles.length;
        for (int i = 0; i < length; i++) {
            File file = listFiles[i];
            boolean z = true;
            if ((!wd4.a((Object) file, (Object) c2)) && file.isFile()) {
                String name = file.getName();
                wd4.a((Object) name, "it.name");
                if (cg4.c(name, this.d, false, 2, (Object) null)) {
                }
            }
            z = false;
            if (z) {
                arrayList.add(file);
            }
        }
        Object[] array = arrayList.toArray(new File[0]);
        if (array != null) {
            return (File[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final int c(int i) {
        long j = (long) Integer.MAX_VALUE;
        return (int) (((((long) i) + 1) + j) % j);
    }
}
