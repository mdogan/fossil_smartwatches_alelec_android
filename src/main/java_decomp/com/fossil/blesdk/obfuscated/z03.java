package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z03 extends as2 implements y03, xs3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public x03 j;
    @DexIgnore
    public ur3<fe2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return z03.m;
        }

        @DexIgnore
        public final z03 b() {
            return new z03();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z03 e;

        @DexIgnore
        public b(z03 z03) {
            this.e = z03;
        }

        @DexIgnore
        public final void onClick(View view) {
            z03.a(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z03 e;

        @DexIgnore
        public c(z03 z03) {
            this.e = z03;
        }

        @DexIgnore
        public final void onClick(View view) {
            z03.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z03 e;

        @DexIgnore
        public d(z03 z03) {
            this.e = z03;
        }

        @DexIgnore
        public final void onClick(View view) {
            z03.a(this.e).j();
        }
    }

    /*
    static {
        String simpleName = z03.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ x03 a(z03 z03) {
        x03 x03 = z03.j;
        if (x03 != null) {
            return x03;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        x03 x03 = this.j;
        if (x03 != null) {
            x03.k();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onPermissionsGranted");
        x03 x03 = this.j;
        if (x03 != null) {
            x03.l();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        fe2 fe2 = (fe2) ra.a(layoutInflater, R.layout.fragment_notification_hybrid_everyone, viewGroup, false, O0());
        fe2.u.setOnClickListener(new b(this));
        fe2.q.setOnClickListener(new c(this));
        fe2.r.setOnClickListener(new d(this));
        this.k = new ur3<>(this, fe2);
        wd4.a((Object) fe2, "binding");
        return fe2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        x03 x03 = this.j;
        if (x03 != null) {
            x03.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        x03 x03 = this.j;
        if (x03 != null) {
            x03.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(x03 x03) {
        wd4.b(x03, "presenter");
        this.j = x03;
    }

    @DexIgnore
    public void b(List<ContactWrapper> list, int i) {
        int i2 = i;
        wd4.b(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (ContactWrapper contactWrapper : list) {
            Contact contact = contactWrapper.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = contactWrapper.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (contactWrapper.getCurrentHandGroup() != i2) {
                        ur3<fe2> ur3 = this.k;
                        if (ur3 != null) {
                            fe2 a2 = ur3.a();
                            if (a2 != null) {
                                FlexibleTextView flexibleTextView = a2.t;
                                if (flexibleTextView != null) {
                                    be4 be4 = be4.a;
                                    String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                                    wd4.a((Object) a3, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                    Object[] objArr = {Integer.valueOf(contactWrapper.getCurrentHandGroup())};
                                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                                    flexibleTextView.setText(format);
                                }
                            }
                            ur3<fe2> ur32 = this.k;
                            if (ur32 != null) {
                                fe2 a4 = ur32.a();
                                if (a4 != null) {
                                    FlexibleTextView flexibleTextView2 = a4.t;
                                    if (flexibleTextView2 != null) {
                                        flexibleTextView2.setVisibility(0);
                                    }
                                }
                                ur3<fe2> ur33 = this.k;
                                if (ur33 != null) {
                                    fe2 a5 = ur33.a();
                                    if (a5 != null) {
                                        AppCompatCheckBox appCompatCheckBox = a5.r;
                                        if (appCompatCheckBox != null) {
                                            appCompatCheckBox.setChecked(false);
                                        }
                                    }
                                } else {
                                    wd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            wd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        ur3<fe2> ur34 = this.k;
                        if (ur34 != null) {
                            fe2 a6 = ur34.a();
                            if (a6 != null) {
                                FlexibleTextView flexibleTextView3 = a6.t;
                                if (flexibleTextView3 != null) {
                                    flexibleTextView3.setText("");
                                }
                            }
                            ur3<fe2> ur35 = this.k;
                            if (ur35 != null) {
                                fe2 a7 = ur35.a();
                                if (a7 != null) {
                                    FlexibleTextView flexibleTextView4 = a7.t;
                                    if (flexibleTextView4 != null) {
                                        flexibleTextView4.setVisibility(8);
                                    }
                                }
                                ur3<fe2> ur36 = this.k;
                                if (ur36 != null) {
                                    fe2 a8 = ur36.a();
                                    if (a8 != null) {
                                        AppCompatCheckBox appCompatCheckBox2 = a8.r;
                                        if (appCompatCheckBox2 != null) {
                                            appCompatCheckBox2.setChecked(true);
                                        }
                                    }
                                } else {
                                    wd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            wd4.d("mBinding");
                            throw null;
                        }
                    }
                    z2 = true;
                }
            } else {
                if (contactWrapper.getCurrentHandGroup() != i2) {
                    ur3<fe2> ur37 = this.k;
                    if (ur37 != null) {
                        fe2 a9 = ur37.a();
                        if (a9 != null) {
                            FlexibleTextView flexibleTextView5 = a9.s;
                            if (flexibleTextView5 != null) {
                                be4 be42 = be4.a;
                                String a10 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                                wd4.a((Object) a10, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                Object[] objArr2 = {Integer.valueOf(contactWrapper.getCurrentHandGroup())};
                                String format2 = String.format(a10, Arrays.copyOf(objArr2, objArr2.length));
                                wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                                flexibleTextView5.setText(format2);
                            }
                        }
                        ur3<fe2> ur38 = this.k;
                        if (ur38 != null) {
                            fe2 a11 = ur38.a();
                            if (a11 != null) {
                                FlexibleTextView flexibleTextView6 = a11.s;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                }
                            }
                            ur3<fe2> ur39 = this.k;
                            if (ur39 != null) {
                                fe2 a12 = ur39.a();
                                if (a12 != null) {
                                    AppCompatCheckBox appCompatCheckBox3 = a12.q;
                                    if (appCompatCheckBox3 != null) {
                                        appCompatCheckBox3.setChecked(false);
                                    }
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            wd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        wd4.d("mBinding");
                        throw null;
                    }
                } else {
                    ur3<fe2> ur310 = this.k;
                    if (ur310 != null) {
                        fe2 a13 = ur310.a();
                        if (a13 != null) {
                            FlexibleTextView flexibleTextView7 = a13.s;
                            if (flexibleTextView7 != null) {
                                flexibleTextView7.setText("");
                            }
                        }
                        ur3<fe2> ur311 = this.k;
                        if (ur311 != null) {
                            fe2 a14 = ur311.a();
                            if (a14 != null) {
                                FlexibleTextView flexibleTextView8 = a14.s;
                                if (flexibleTextView8 != null) {
                                    flexibleTextView8.setVisibility(8);
                                }
                            }
                            ur3<fe2> ur312 = this.k;
                            if (ur312 != null) {
                                fe2 a15 = ur312.a();
                                if (a15 != null) {
                                    AppCompatCheckBox appCompatCheckBox4 = a15.q;
                                    if (appCompatCheckBox4 != null) {
                                        appCompatCheckBox4.setChecked(true);
                                    }
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            wd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        wd4.d("mBinding");
                        throw null;
                    }
                }
                z = true;
            }
        }
        if (!z) {
            ur3<fe2> ur313 = this.k;
            if (ur313 != null) {
                fe2 a16 = ur313.a();
                if (a16 != null) {
                    FlexibleTextView flexibleTextView9 = a16.s;
                    if (flexibleTextView9 != null) {
                        flexibleTextView9.setText("");
                    }
                }
                ur3<fe2> ur314 = this.k;
                if (ur314 != null) {
                    fe2 a17 = ur314.a();
                    if (a17 != null) {
                        FlexibleTextView flexibleTextView10 = a17.s;
                        if (flexibleTextView10 != null) {
                            flexibleTextView10.setVisibility(8);
                        }
                    }
                    ur3<fe2> ur315 = this.k;
                    if (ur315 != null) {
                        fe2 a18 = ur315.a();
                        if (a18 != null) {
                            AppCompatCheckBox appCompatCheckBox5 = a18.q;
                            if (appCompatCheckBox5 != null) {
                                appCompatCheckBox5.setChecked(false);
                            }
                        }
                    } else {
                        wd4.d("mBinding");
                        throw null;
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
        if (!z2) {
            ur3<fe2> ur316 = this.k;
            if (ur316 != null) {
                fe2 a19 = ur316.a();
                if (a19 != null) {
                    FlexibleTextView flexibleTextView11 = a19.t;
                    if (flexibleTextView11 != null) {
                        flexibleTextView11.setText("");
                    }
                }
                ur3<fe2> ur317 = this.k;
                if (ur317 != null) {
                    fe2 a20 = ur317.a();
                    if (a20 != null) {
                        FlexibleTextView flexibleTextView12 = a20.t;
                        if (flexibleTextView12 != null) {
                            flexibleTextView12.setVisibility(8);
                        }
                    }
                    ur3<fe2> ur318 = this.k;
                    if (ur318 != null) {
                        fe2 a21 = ur318.a();
                        if (a21 != null) {
                            AppCompatCheckBox appCompatCheckBox6 = a21.r;
                            if (appCompatCheckBox6 != null) {
                                appCompatCheckBox6.setChecked(false);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ContactWrapper contactWrapper) {
        wd4.b(contactWrapper, "contactWrapper");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            int currentHandGroup = contactWrapper.getCurrentHandGroup();
            x03 x03 = this.j;
            if (x03 != null) {
                es3.a(childFragmentManager, contactWrapper, currentHandGroup, x03.h());
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i == R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactWrapper contactWrapper = (ContactWrapper) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER");
                if (contactWrapper != null) {
                    x03 x03 = this.j;
                    if (x03 != null) {
                        x03.a(contactWrapper);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onPermissionsDenied");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local.d(str2, "Permission Denied : " + str);
        }
        if (br4.a((Fragment) this, list) && isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.z(childFragmentManager);
        }
    }
}
