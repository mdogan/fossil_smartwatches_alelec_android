package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.workers.TimeChangeReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pu3 implements MembersInjector<TimeChangeReceiver> {
    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, AlarmHelper alarmHelper) {
        timeChangeReceiver.a = alarmHelper;
    }

    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, fn2 fn2) {
        timeChangeReceiver.b = fn2;
    }
}
