package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fx2 implements Factory<ex2> {
    @DexIgnore
    public static /* final */ fx2 a; // = new fx2();

    @DexIgnore
    public static fx2 a() {
        return a;
    }

    @DexIgnore
    public static ex2 b() {
        return new ex2();
    }

    @DexIgnore
    public ex2 get() {
        return b();
    }
}
