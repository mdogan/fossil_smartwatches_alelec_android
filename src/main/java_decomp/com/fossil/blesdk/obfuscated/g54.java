package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g54 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public g54(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || g54.class != obj.getClass()) {
            return false;
        }
        g54 g54 = (g54) obj;
        if (this.b != g54.b) {
            return false;
        }
        String str = this.a;
        String str2 = g54.a;
        return str == null ? str2 == null : str.equals(str2);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        return ((str != null ? str.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
