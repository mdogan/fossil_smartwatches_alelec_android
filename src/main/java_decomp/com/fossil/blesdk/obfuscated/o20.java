package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.AppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o20 extends t20<AppNotification, cb4> {
    @DexIgnore
    public static /* final */ l20<AppNotification>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<cb4>[] b; // = new m20[0];
    @DexIgnore
    public static /* final */ o20 c; // = new o20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<AppNotification> {
        @DexIgnore
        public byte[] a(AppNotification appNotification) {
            wd4.b(appNotification, "entries");
            return o20.c.a(appNotification);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<AppNotification> {
        @DexIgnore
        public byte[] a(AppNotification appNotification) {
            wd4.b(appNotification, "entries");
            return o20.c.a(appNotification);
        }
    }

    @DexIgnore
    public m20<cb4>[] b() {
        return b;
    }

    @DexIgnore
    public l20<AppNotification>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(AppNotification appNotification) {
        return appNotification.getData$blesdk_productionRelease();
    }
}
