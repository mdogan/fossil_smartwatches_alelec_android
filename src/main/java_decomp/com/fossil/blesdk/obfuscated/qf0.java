package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qf0 implements ng0 {
    @DexIgnore
    public /* final */ og0 a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public qf0(og0 og0) {
        this.a = og0;
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t) {
        try {
            this.a.r.y.a(t);
            fg0 fg0 = this.a.r;
            ee0.f fVar = fg0.p.get(t.i());
            ck0.a(fVar, (Object) "Appropriate Api was not requested.");
            if (fVar.c() || !this.a.k.containsKey(t.i())) {
                boolean z = fVar instanceof hk0;
                ee0.b bVar = fVar;
                if (z) {
                    bVar = ((hk0) fVar).G();
                }
                t.b(bVar);
                return t;
            }
            t.c(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.a.a((pg0) new rf0(this, this));
        }
    }

    @DexIgnore
    public final void a(vd0 vd0, ee0<?> ee0, boolean z) {
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t) {
        a(t);
        return t;
    }

    @DexIgnore
    public final void c() {
    }

    @DexIgnore
    public final void d() {
        if (this.b) {
            this.b = false;
            this.a.r.y.a();
            a();
        }
    }

    @DexIgnore
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    public final void f(int i) {
        this.a.a((vd0) null);
        this.a.s.a(i, this.b);
    }

    @DexIgnore
    public final void b() {
        if (this.b) {
            this.b = false;
            this.a.a((pg0) new sf0(this, this));
        }
    }

    @DexIgnore
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.a.r.p()) {
            this.b = true;
            for (nh0 a2 : this.a.r.x) {
                a2.a();
            }
            return false;
        }
        this.a.a((vd0) null);
        return true;
    }
}
