package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class in0 extends hn0 {
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public in0(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f = bArr;
    }

    @DexIgnore
    public final byte[] o() {
        return this.f;
    }
}
