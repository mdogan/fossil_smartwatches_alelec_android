package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bc0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bc0> CREATOR; // = new ec0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Bundle g;

    @DexIgnore
    public bc0(int i, int i2, Bundle bundle) {
        this.e = i;
        this.f = i2;
        this.g = bundle;
    }

    @DexIgnore
    public int H() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, H());
        lk0.a(parcel, 3, this.g, false);
        lk0.a(parcel, a);
    }
}
