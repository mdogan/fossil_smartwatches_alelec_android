package com.fossil.blesdk.obfuscated;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yh1 implements wi1 {
    @DexIgnore
    public static volatile yh1 G;
    @DexIgnore
    public volatile Boolean A;
    @DexIgnore
    public Boolean B;
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public int D;
    @DexIgnore
    public AtomicInteger E; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ long F;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ vl1 f;
    @DexIgnore
    public /* final */ yl1 g;
    @DexIgnore
    public /* final */ gh1 h;
    @DexIgnore
    public /* final */ ug1 i;
    @DexIgnore
    public /* final */ uh1 j;
    @DexIgnore
    public /* final */ uk1 k;
    @DexIgnore
    public /* final */ AppMeasurement l;
    @DexIgnore
    public /* final */ ol1 m;
    @DexIgnore
    public /* final */ sg1 n;
    @DexIgnore
    public /* final */ im0 o;
    @DexIgnore
    public /* final */ sj1 p;
    @DexIgnore
    public /* final */ ej1 q;
    @DexIgnore
    public /* final */ bg1 r;
    @DexIgnore
    public qg1 s;
    @DexIgnore
    public wj1 t;
    @DexIgnore
    public cg1 u;
    @DexIgnore
    public og1 v;
    @DexIgnore
    public mh1 w;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public Boolean y;
    @DexIgnore
    public long z;

    @DexIgnore
    public yh1(cj1 cj1) {
        ck0.a(cj1);
        this.f = new vl1(cj1.a);
        kg1.a(this.f);
        this.a = cj1.a;
        this.b = cj1.b;
        this.c = cj1.c;
        this.d = cj1.d;
        this.e = cj1.e;
        this.A = cj1.f;
        pg1 pg1 = cj1.g;
        if (pg1 != null) {
            Bundle bundle = pg1.g;
            if (bundle != null) {
                Object obj = bundle.get("measurementEnabled");
                if (obj instanceof Boolean) {
                    this.B = (Boolean) obj;
                }
                Object obj2 = pg1.g.get("measurementDeactivated");
                if (obj2 instanceof Boolean) {
                    this.C = (Boolean) obj2;
                }
            }
        }
        b71.a(this.a);
        this.o = lm0.d();
        this.F = this.o.b();
        this.g = new yl1(this);
        gh1 gh1 = new gh1(this);
        gh1.r();
        this.h = gh1;
        ug1 ug1 = new ug1(this);
        ug1.r();
        this.i = ug1;
        ol1 ol1 = new ol1(this);
        ol1.r();
        this.m = ol1;
        sg1 sg1 = new sg1(this);
        sg1.r();
        this.n = sg1;
        this.r = new bg1(this);
        sj1 sj1 = new sj1(this);
        sj1.z();
        this.p = sj1;
        ej1 ej1 = new ej1(this);
        ej1.z();
        this.q = ej1;
        this.l = new AppMeasurement(this);
        uk1 uk1 = new uk1(this);
        uk1.z();
        this.k = uk1;
        uh1 uh1 = new uh1(this);
        uh1.r();
        this.j = uh1;
        if (this.a.getApplicationContext() instanceof Application) {
            ej1 k2 = k();
            if (k2.getContext().getApplicationContext() instanceof Application) {
                Application application = (Application) k2.getContext().getApplicationContext();
                if (k2.c == null) {
                    k2.c = new oj1(k2, (fj1) null);
                }
                application.unregisterActivityLifecycleCallbacks(k2.c);
                application.registerActivityLifecycleCallbacks(k2.c);
                k2.d().A().a("Registered activity lifecycle callback");
            }
        } else {
            d().v().a("Application context is not an Application");
        }
        this.j.a((Runnable) new zh1(this, cj1));
    }

    @DexIgnore
    public final String A() {
        return this.b;
    }

    @DexIgnore
    public final String B() {
        return this.c;
    }

    @DexIgnore
    public final String C() {
        return this.d;
    }

    @DexIgnore
    public final boolean D() {
        return this.e;
    }

    @DexIgnore
    public final boolean E() {
        return this.A != null && this.A.booleanValue();
    }

    @DexIgnore
    public final long F() {
        Long valueOf = Long.valueOf(t().j.a());
        if (valueOf.longValue() == 0) {
            return this.F;
        }
        return Math.min(this.F, valueOf.longValue());
    }

    @DexIgnore
    public final void G() {
        this.E.incrementAndGet();
    }

    @DexIgnore
    public final boolean H() {
        g();
        a().e();
        Boolean bool = this.y;
        if (bool == null || this.z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.o.c() - this.z) > 1000)) {
            this.z = this.o.c();
            boolean z2 = true;
            this.y = Boolean.valueOf(s().d("android.permission.INTERNET") && s().d("android.permission.ACCESS_NETWORK_STATE") && (cn0.b(this.a).a() || this.g.r() || (ph1.a(this.a) && ol1.a(this.a, false))));
            if (this.y.booleanValue()) {
                if (!s().d(l().A(), l().C()) && TextUtils.isEmpty(l().C())) {
                    z2 = false;
                }
                this.y = Boolean.valueOf(z2);
            }
        }
        return this.y.booleanValue();
    }

    @DexIgnore
    public final void a(cj1 cj1) {
        String str;
        wg1 wg1;
        a().e();
        yl1.s();
        cg1 cg1 = new cg1(this);
        cg1.r();
        this.u = cg1;
        og1 og1 = new og1(this);
        og1.z();
        this.v = og1;
        qg1 qg1 = new qg1(this);
        qg1.z();
        this.s = qg1;
        wj1 wj1 = new wj1(this);
        wj1.z();
        this.t = wj1;
        this.m.o();
        this.h.o();
        this.w = new mh1(this);
        this.v.w();
        d().y().a("App measurement is starting up, version", Long.valueOf(this.g.n()));
        d().y().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String B2 = og1.B();
        if (TextUtils.isEmpty(this.b)) {
            if (s().c(B2)) {
                wg1 = d().y();
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                wg1 = d().y();
                String valueOf = String.valueOf(B2);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            wg1.a(str);
        }
        d().z().a("Debug-level message logging enabled");
        if (this.D != this.E.get()) {
            d().s().a("Not all components initialized", Integer.valueOf(this.D), Integer.valueOf(this.E.get()));
        }
        this.x = true;
    }

    @DexIgnore
    public final vl1 b() {
        return this.f;
    }

    @DexIgnore
    public final im0 c() {
        return this.o;
    }

    @DexIgnore
    public final ug1 d() {
        b((vi1) this.i);
        return this.i;
    }

    @DexIgnore
    public final boolean e() {
        boolean z2;
        a().e();
        g();
        if (this.g.a(kg1.n0)) {
            if (this.g.o()) {
                return false;
            }
            Boolean bool = this.C;
            if (bool != null && bool.booleanValue()) {
                return false;
            }
            Boolean x2 = t().x();
            if (x2 != null) {
                return x2.booleanValue();
            }
            Boolean p2 = this.g.p();
            if (p2 != null) {
                return p2.booleanValue();
            }
            Boolean bool2 = this.B;
            if (bool2 != null) {
                return bool2.booleanValue();
            }
            if (xe0.b()) {
                return false;
            }
            if (!this.g.a(kg1.k0) || this.A == null) {
                return true;
            }
            return this.A.booleanValue();
        } else if (this.g.o()) {
            return false;
        } else {
            Boolean p3 = this.g.p();
            if (p3 != null) {
                z2 = p3.booleanValue();
            } else {
                z2 = !xe0.b();
                if (z2 && this.A != null && kg1.k0.a().booleanValue()) {
                    z2 = this.A.booleanValue();
                }
            }
            return t().c(z2);
        }
    }

    @DexIgnore
    public final void f() {
        a().e();
        if (t().e.a() == 0) {
            t().e.a(this.o.b());
        }
        if (Long.valueOf(t().j.a()).longValue() == 0) {
            d().A().a("Persisting first open", Long.valueOf(this.F));
            t().j.a(this.F);
        }
        if (H()) {
            if (!TextUtils.isEmpty(l().A()) || !TextUtils.isEmpty(l().C())) {
                s();
                if (ol1.a(l().A(), t().t(), l().C(), t().u())) {
                    d().y().a("Rechecking which service to use due to a GMP App Id change");
                    t().w();
                    o().B();
                    this.t.A();
                    this.t.E();
                    t().j.a(this.F);
                    t().l.a((String) null);
                }
                t().c(l().A());
                t().d(l().C());
                if (this.g.q(l().B())) {
                    this.k.b(this.F);
                }
            }
            k().a(t().l.a());
            if (!TextUtils.isEmpty(l().A()) || !TextUtils.isEmpty(l().C())) {
                boolean e2 = e();
                if (!t().A() && !this.g.o()) {
                    t().d(!e2);
                }
                if (!this.g.i(l().B()) || e2) {
                    k().F();
                }
                m().a((AtomicReference<String>) new AtomicReference());
            }
        } else if (e()) {
            if (!s().d("android.permission.INTERNET")) {
                d().s().a("App is missing INTERNET permission");
            }
            if (!s().d("android.permission.ACCESS_NETWORK_STATE")) {
                d().s().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!cn0.b(this.a).a() && !this.g.r()) {
                if (!ph1.a(this.a)) {
                    d().s().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!ol1.a(this.a, false)) {
                    d().s().a("AppMeasurementService not registered/enabled");
                }
            }
            d().s().a("Uploading is not possible. App measurement disabled");
        }
    }

    @DexIgnore
    public final void g() {
        if (!this.x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    @DexIgnore
    public final Context getContext() {
        return this.a;
    }

    @DexIgnore
    public final void h() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    @DexIgnore
    public final void i() {
    }

    @DexIgnore
    public final bg1 j() {
        bg1 bg1 = this.r;
        if (bg1 != null) {
            return bg1;
        }
        throw new IllegalStateException("Component not created");
    }

    @DexIgnore
    public final ej1 k() {
        b((qk1) this.q);
        return this.q;
    }

    @DexIgnore
    public final og1 l() {
        b((qk1) this.v);
        return this.v;
    }

    @DexIgnore
    public final wj1 m() {
        b((qk1) this.t);
        return this.t;
    }

    @DexIgnore
    public final sj1 n() {
        b((qk1) this.p);
        return this.p;
    }

    @DexIgnore
    public final qg1 o() {
        b((qk1) this.s);
        return this.s;
    }

    @DexIgnore
    public final uk1 p() {
        b((qk1) this.k);
        return this.k;
    }

    @DexIgnore
    public final cg1 q() {
        b((vi1) this.u);
        return this.u;
    }

    @DexIgnore
    public final sg1 r() {
        a((ui1) this.n);
        return this.n;
    }

    @DexIgnore
    public final ol1 s() {
        a((ui1) this.m);
        return this.m;
    }

    @DexIgnore
    public final gh1 t() {
        a((ui1) this.h);
        return this.h;
    }

    @DexIgnore
    public final yl1 u() {
        return this.g;
    }

    @DexIgnore
    public final ug1 v() {
        ug1 ug1 = this.i;
        if (ug1 == null || !ug1.m()) {
            return null;
        }
        return this.i;
    }

    @DexIgnore
    public final mh1 w() {
        return this.w;
    }

    @DexIgnore
    public final uh1 x() {
        return this.j;
    }

    @DexIgnore
    public final AppMeasurement y() {
        return this.l;
    }

    @DexIgnore
    public final boolean z() {
        return TextUtils.isEmpty(this.b);
    }

    @DexIgnore
    public static void b(vi1 vi1) {
        if (vi1 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!vi1.m()) {
            String valueOf = String.valueOf(vi1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static void b(qk1 qk1) {
        if (qk1 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!qk1.u()) {
            String valueOf = String.valueOf(qk1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final uh1 a() {
        b((vi1) this.j);
        return this.j;
    }

    @DexIgnore
    public static yh1 a(Context context, pg1 pg1) {
        if (pg1 != null && (pg1.e == null || pg1.f == null)) {
            pg1 = new pg1(pg1.a, pg1.b, pg1.c, pg1.d, (String) null, (String) null, pg1.g);
        }
        ck0.a(context);
        ck0.a(context.getApplicationContext());
        if (G == null) {
            synchronized (yh1.class) {
                if (G == null) {
                    G = new yh1(new cj1(context, pg1));
                }
            }
        } else if (pg1 != null) {
            Bundle bundle = pg1.g;
            if (bundle != null && bundle.containsKey("dataCollectionDefaultEnabled")) {
                G.a(pg1.g.getBoolean("dataCollectionDefaultEnabled"));
            }
        }
        return G;
    }

    @DexIgnore
    public static void a(ui1 ui1) {
        if (ui1 == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.A = Boolean.valueOf(z2);
    }

    @DexIgnore
    public final void a(vi1 vi1) {
        this.D++;
    }

    @DexIgnore
    public final void a(qk1 qk1) {
        this.D++;
    }
}
