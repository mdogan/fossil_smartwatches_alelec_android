package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ h14 h;

    @DexIgnore
    public j14(h14 h14, List list, boolean z, boolean z2) {
        this.h = h14;
        this.e = list;
        this.f = z;
        this.g = z2;
    }

    @DexIgnore
    public void run() {
        this.h.a((List<r14>) this.e, this.f);
        if (this.g) {
            this.e.clear();
        }
    }
}
