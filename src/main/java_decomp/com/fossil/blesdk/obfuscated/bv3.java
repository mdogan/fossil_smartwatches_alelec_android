package com.fossil.blesdk.obfuscated;

import java.util.ArrayDeque;
import java.util.Deque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bv3 {
    @DexIgnore
    public /* final */ Deque<uu3> a; // = new ArrayDeque();

    @DexIgnore
    public bv3() {
        new ArrayDeque();
        new ArrayDeque();
    }

    @DexIgnore
    public synchronized void a(uu3 uu3) {
        this.a.add(uu3);
    }

    @DexIgnore
    public synchronized void b(uu3 uu3) {
        if (!this.a.remove(uu3)) {
            throw new AssertionError("Call wasn't in-flight!");
        }
    }
}
