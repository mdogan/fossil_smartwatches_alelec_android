package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import com.fossil.blesdk.obfuscated.cs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class as1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ cs1 a;

        @DexIgnore
        public a(cs1 cs1) {
            this.a = cs1;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.a();
        }
    }

    @DexIgnore
    public static Animator a(cs1 cs1, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(cs1, cs1.c.a, cs1.b.b, new cs1.e[]{new cs1.e(f, f2, f3)});
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        cs1.e revealInfo = cs1.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) cs1, (int) f, (int) f2, revealInfo.c, f3);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(new Animator[]{ofObject, createCircularReveal});
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    @DexIgnore
    public static Animator.AnimatorListener a(cs1 cs1) {
        return new a(cs1);
    }
}
