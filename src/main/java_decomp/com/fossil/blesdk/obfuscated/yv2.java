package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yv2 implements Factory<HomeAlertsPresenter> {
    @DexIgnore
    public static HomeAlertsPresenter a(tv2 tv2, k62 k62, AlarmHelper alarmHelper, wy2 wy2, qx2 qx2, jw2 jw2, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, fn2 fn2, DNDSettingsDatabase dNDSettingsDatabase) {
        return new HomeAlertsPresenter(tv2, k62, alarmHelper, wy2, qx2, jw2, notificationSettingsDatabase, setAlarms, alarmsRepository, fn2, dNDSettingsDatabase);
    }
}
