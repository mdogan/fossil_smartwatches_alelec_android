package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qd1 extends t31 {
    @DexIgnore
    public /* final */ /* synthetic */ yn1 e;

    @DexIgnore
    public qd1(pc1 pc1, yn1 yn1) {
        this.e = yn1;
    }

    @DexIgnore
    public final void a(p31 p31) throws RemoteException {
        Status G = p31.G();
        if (G == null) {
            this.e.b((Exception) new ApiException(new Status(8, "Got null status from location service")));
        } else if (G.I() == 0) {
            this.e.a(true);
        } else {
            this.e.b((Exception) ij0.a(G));
        }
    }
}
