package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.utils.Crc32Calculator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class v20<T> extends u20<T> {
    @DexIgnore
    public /* final */ Crc32Calculator.CrcType d; // = Crc32Calculator.CrcType.CRC32C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v20(FileType fileType) {
        super(fileType, new Version((byte) 2, (byte) 0));
        wd4.b(fileType, "fileType");
    }

    @DexIgnore
    public Crc32Calculator.CrcType b() {
        return this.d;
    }
}
