package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cf3 implements MembersInjector<GoalTrackingDetailActivity> {
    @DexIgnore
    public static void a(GoalTrackingDetailActivity goalTrackingDetailActivity, GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        goalTrackingDetailActivity.B = goalTrackingDetailPresenter;
    }
}
