package com.fossil.blesdk.obfuscated;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lm0 implements im0 {
    @DexIgnore
    public static /* final */ lm0 a; // = new lm0();

    @DexIgnore
    public static im0 d() {
        return a;
    }

    @DexIgnore
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
