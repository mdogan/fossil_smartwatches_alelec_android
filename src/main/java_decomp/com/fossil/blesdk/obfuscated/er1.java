package com.fossil.blesdk.obfuscated;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class er1 {
    @DexIgnore
    public static /* final */ TimeInterpolator a; // = new LinearInterpolator();
    @DexIgnore
    public static /* final */ TimeInterpolator b; // = new jb();
    @DexIgnore
    public static /* final */ TimeInterpolator c; // = new ib();
    @DexIgnore
    public static /* final */ TimeInterpolator d; // = new kb();
    @DexIgnore
    public static /* final */ TimeInterpolator e; // = new DecelerateInterpolator();

    @DexIgnore
    public static float a(float f, float f2, float f3) {
        return f + (f3 * (f2 - f));
    }

    @DexIgnore
    public static int a(int i, int i2, float f) {
        return i + Math.round(f * ((float) (i2 - i)));
    }
}
