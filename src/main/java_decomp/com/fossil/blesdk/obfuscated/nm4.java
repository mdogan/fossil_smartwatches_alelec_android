package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;
import okio.ByteString;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nm4 extends RequestBody {
    @DexIgnore
    public static /* final */ mm4 e; // = mm4.a("multipart/mixed");
    @DexIgnore
    public static /* final */ mm4 f; // = mm4.a("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {DateTimeFieldType.HALFDAY_OF_DAY, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {45, 45};
    @DexIgnore
    public /* final */ ByteString a;
    @DexIgnore
    public /* final */ mm4 b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public long d; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ByteString a;
        @DexIgnore
        public mm4 b;
        @DexIgnore
        public /* final */ List<b> c;

        @DexIgnore
        public a() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public a a(mm4 mm4) {
            if (mm4 == null) {
                throw new NullPointerException("type == null");
            } else if (mm4.c().equals("multipart")) {
                this.b = mm4;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + mm4);
            }
        }

        @DexIgnore
        public a(String str) {
            this.b = nm4.e;
            this.c = new ArrayList();
            this.a = ByteString.encodeUtf8(str);
        }

        @DexIgnore
        public a a(km4 km4, RequestBody requestBody) {
            a(b.a(km4, requestBody));
            return this;
        }

        @DexIgnore
        public a a(b bVar) {
            if (bVar != null) {
                this.c.add(bVar);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public nm4 a() {
            if (!this.c.isEmpty()) {
                return new nm4(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ km4 a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public b(km4 km4, RequestBody requestBody) {
            this.a = km4;
            this.b = requestBody;
        }

        @DexIgnore
        public static b a(km4 km4, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (km4 != null && km4.a(GraphRequest.CONTENT_TYPE_HEADER) != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (km4 == null || km4.a("Content-Length") == null) {
                return new b(km4, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    /*
    static {
        mm4.a("multipart/alternative");
        mm4.a("multipart/digest");
        mm4.a("multipart/parallel");
    }
    */

    @DexIgnore
    public nm4(ByteString byteString, mm4 mm4, List<b> list) {
        this.a = byteString;
        this.b = mm4.a(mm4 + "; boundary=" + byteString.utf8());
        this.c = vm4.a(list);
    }

    @DexIgnore
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long a2 = a((wo4) null, true);
        this.d = a2;
        return a2;
    }

    @DexIgnore
    public mm4 b() {
        return this.b;
    }

    @DexIgnore
    public void a(wo4 wo4) throws IOException {
        a(wo4, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: com.fossil.blesdk.obfuscated.wo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.fossil.blesdk.obfuscated.vo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.fossil.blesdk.obfuscated.vo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: com.fossil.blesdk.obfuscated.wo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.fossil.blesdk.obfuscated.vo4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final long a(wo4 wo4, boolean z) throws IOException {
        vo4 vo4;
        if (z) {
            wo4 = new vo4();
            vo4 = wo4;
        } else {
            vo4 = 0;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            km4 km4 = bVar.a;
            RequestBody requestBody = bVar.b;
            wo4.write(i);
            wo4.a(this.a);
            wo4.write(h);
            if (km4 != null) {
                int b2 = km4.b();
                for (int i3 = 0; i3 < b2; i3++) {
                    wo4.a(km4.a(i3)).write(g).a(km4.b(i3)).write(h);
                }
            }
            mm4 b3 = requestBody.b();
            if (b3 != null) {
                wo4.a("Content-Type: ").a(b3.toString()).write(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                wo4.a("Content-Length: ").b(a2).write(h);
            } else if (z) {
                vo4.w();
                return -1;
            }
            wo4.write(h);
            if (z) {
                j += a2;
            } else {
                requestBody.a(wo4);
            }
            wo4.write(h);
        }
        wo4.write(i);
        wo4.a(this.a);
        wo4.write(i);
        wo4.write(h);
        if (!z) {
            return j;
        }
        long B = j + vo4.B();
        vo4.w();
        return B;
    }
}
