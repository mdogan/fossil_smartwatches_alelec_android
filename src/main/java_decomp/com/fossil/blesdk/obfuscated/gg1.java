package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gg1 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> e; // = this.f.e.keySet().iterator();
    @DexIgnore
    public /* final */ /* synthetic */ fg1 f;

    @DexIgnore
    public gg1(fg1 fg1) {
        this.f = fg1;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.e.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
