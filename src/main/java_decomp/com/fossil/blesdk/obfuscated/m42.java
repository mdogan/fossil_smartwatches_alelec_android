package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.util.UserUtils;
import com.portfolio.platform.workers.TimeChangeReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface m42 {
    @DexIgnore
    a43 a(e43 e43);

    @DexIgnore
    a73 a(e73 e73);

    @DexIgnore
    aq3 a(dq3 dq3);

    @DexIgnore
    ay2 a(fy2 fy2);

    @DexIgnore
    be3 a(fe3 fe3);

    @DexIgnore
    bm3 a(em3 em3);

    @DexIgnore
    bn3 a(in3 in3);

    @DexIgnore
    bw2 a(fw2 fw2);

    @DexIgnore
    c53 a(d53 d53);

    @DexIgnore
    d63 a(e63 e63);

    @DexIgnore
    df3 a(hf3 hf3);

    @DexIgnore
    dh3 a(hh3 hh3);

    @DexIgnore
    dk3 a(hk3 hk3);

    @DexIgnore
    e83 a(l83 l83);

    @DexIgnore
    e93 a(l93 l93);

    @DexIgnore
    ea3 a(la3 la3);

    @DexIgnore
    eb3 a(lb3 lb3);

    @DexIgnore
    ec3 a(lc3 lc3);

    @DexIgnore
    ed3 a(ld3 ld3);

    @DexIgnore
    el3 a(hl3 hl3);

    @DexIgnore
    f53 a(g53 g53);

    @DexIgnore
    fz2 a(jz2 jz2);

    @DexIgnore
    gi3 a(ki3 ki3);

    @DexIgnore
    gj3 a(kj3 kj3);

    @DexIgnore
    go3 a(ko3 ko3);

    @DexIgnore
    gv2 a(hv2 hv2);

    @DexIgnore
    gx2 a(lx2 lx2);

    @DexIgnore
    h23 a(m23 m23);

    @DexIgnore
    i43 a(m43 m43);

    @DexIgnore
    i73 a(j73 j73);

    @DexIgnore
    ip3 a(mp3 mp3);

    @DexIgnore
    iu2 a(lu2 lu2);

    @DexIgnore
    j13 a(k13 k13);

    @DexIgnore
    j33 a(n33 n33);

    @DexIgnore
    j53 a(m53 m53);

    @DexIgnore
    k03 a(o03 o03);

    @DexIgnore
    k63 a(p63 p63);

    @DexIgnore
    ke3 a(oe3 oe3);

    @DexIgnore
    kq3 a(nq3 nq3);

    @DexIgnore
    ll3 a(ol3 ol3);

    @DexIgnore
    lw2 a(qw2 qw2);

    @DexIgnore
    mf3 a(qf3 qf3);

    @DexIgnore
    mh3 a(qh3 qh3);

    @DexIgnore
    n73 a(o73 o73);

    @DexIgnore
    nk3 a(qk3 qk3);

    @DexIgnore
    oy2 a(py2 py2);

    @DexIgnore
    p43 a(u43 u43);

    @DexIgnore
    pi3 a(si3 si3);

    @DexIgnore
    pz2 a(tz2 tz2);

    @DexIgnore
    qj3 a(tj3 tj3);

    @DexIgnore
    qu2 a(bv2 bv2);

    @DexIgnore
    r53 a(v53 v53);

    @DexIgnore
    rm3 a(um3 um3);

    @DexIgnore
    rv2 a(wv2 wv2);

    @DexIgnore
    s13 a(x13 x13);

    @DexIgnore
    s33 a(v33 v33);

    @DexIgnore
    t23 a(e33 e33);

    @DexIgnore
    t63 a(w63 w63);

    @DexIgnore
    tg3 a(xg3 xg3);

    @DexIgnore
    tp3 a(wp3 wp3);

    @DexIgnore
    ue3 a(ye3 ye3);

    @DexIgnore
    uh3 a(zh3 zh3);

    @DexIgnore
    ul3 a(xl3 xl3);

    @DexIgnore
    uq3 a(zq3 zq3);

    @DexIgnore
    vf3 a(zf3 zf3);

    @DexIgnore
    vk3 a(yk3 yk3);

    @DexIgnore
    vn3 a(zn3 zn3);

    @DexIgnore
    vw2 a(zw2 zw2);

    @DexIgnore
    w03 a(a13 a13);

    @DexIgnore
    wo3 a(xo3 xo3);

    @DexIgnore
    x23 a(a33 a33);

    @DexIgnore
    xi3 a(bj3 bj3);

    @DexIgnore
    xt2 a(bu2 bu2);

    @DexIgnore
    zo3 a(cp3 cp3);

    @DexIgnore
    zz2 a(d03 d03);

    @DexIgnore
    void a(on2 on2);

    @DexIgnore
    void a(or3 or3);

    @DexIgnore
    void a(rl2 rl2);

    @DexIgnore
    void a(PortfolioApp portfolioApp);

    @DexIgnore
    void a(CloudImageHelper cloudImageHelper);

    @DexIgnore
    void a(URLRequestTaskHelper uRLRequestTaskHelper);

    @DexIgnore
    void a(AppHelper appHelper);

    @DexIgnore
    void a(DeviceHelper deviceHelper);

    @DexIgnore
    void a(LightAndHapticsManager lightAndHapticsManager);

    @DexIgnore
    void a(WeatherManager weatherManager);

    @DexIgnore
    void a(NotificationReceiver notificationReceiver);

    @DexIgnore
    void a(AlarmReceiver alarmReceiver);

    @DexIgnore
    void a(AppPackageRemoveReceiver appPackageRemoveReceiver);

    @DexIgnore
    void a(BootReceiver bootReceiver);

    @DexIgnore
    void a(LocaleChangedReceiver localeChangedReceiver);

    @DexIgnore
    void a(NetworkChangedReceiver networkChangedReceiver);

    @DexIgnore
    void a(FossilNotificationListenerService fossilNotificationListenerService);

    @DexIgnore
    void a(MFDeviceService mFDeviceService);

    @DexIgnore
    void a(ComplicationWeatherService complicationWeatherService);

    @DexIgnore
    void a(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService);

    @DexIgnore
    void a(FossilFirebaseMessagingService fossilFirebaseMessagingService);

    @DexIgnore
    void a(CommuteTimeService commuteTimeService);

    @DexIgnore
    void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager);

    @DexIgnore
    void a(BaseActivity baseActivity);

    @DexIgnore
    void a(DebugActivity debugActivity);

    @DexIgnore
    void a(LoginPresenter loginPresenter);

    @DexIgnore
    void a(ProfileSetupPresenter profileSetupPresenter);

    @DexIgnore
    void a(SignUpPresenter signUpPresenter);

    @DexIgnore
    void a(DeviceUtils deviceUtils);

    @DexIgnore
    void a(UserUtils userUtils);

    @DexIgnore
    void a(TimeChangeReceiver timeChangeReceiver);
}
