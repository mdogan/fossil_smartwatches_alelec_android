package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.model.microapp.MicroAppMapping;
import com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c50 extends h60 {
    @DexIgnore
    public /* final */ MicroAppMapping[] Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c50(Peripheral peripheral, Phase.a aVar, MicroAppMapping[] microAppMappingArr) {
        super(peripheral, aVar, PhaseId.CONFIGURE_MICRO_APP, true, a50.b.b(peripheral.k(), FileType.MICRO_APP), new byte[0], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(microAppMappingArr, "mappings");
        this.Q = microAppMappingArr;
    }

    @DexIgnore
    public byte[] F() {
        Version microAppVersion = e().getDeviceInformation().getMicroAppVersion();
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.MICRO_APP.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = va0.y.g();
        }
        wd4.a((Object) version, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
        return w20.c.a(A(), version, new MicroAppConfiguration(this.Q, microAppVersion).getData());
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.MICRO_APP_MAPPINGS, k00.a(this.Q));
    }
}
