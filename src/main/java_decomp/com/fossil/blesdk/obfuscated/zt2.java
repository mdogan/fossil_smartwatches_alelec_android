package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zt2 extends cs2<yt2> {
    @DexIgnore
    void K();

    @DexIgnore
    void R();

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(SparseIntArray sparseIntArray);

    @DexIgnore
    void a(Alarm alarm, boolean z);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void e(boolean z);

    @DexIgnore
    void k(boolean z);

    @DexIgnore
    void l(boolean z);

    @DexIgnore
    void x();
}
