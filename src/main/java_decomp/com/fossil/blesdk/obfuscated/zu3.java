package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.CipherSuite;
import com.squareup.okhttp.TlsVersion;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zu3 {
    @DexIgnore
    public static /* final */ CipherSuite[] e; // = {CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA, CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA};
    @DexIgnore
    public static /* final */ zu3 f;
    @DexIgnore
    public static /* final */ zu3 g;
    @DexIgnore
    public static /* final */ zu3 h; // = new b(false).a();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String[] b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ boolean d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public b a(CipherSuite... cipherSuiteArr) {
            if (this.a) {
                String[] strArr = new String[cipherSuiteArr.length];
                for (int i = 0; i < cipherSuiteArr.length; i++) {
                    strArr[i] = cipherSuiteArr[i].javaName;
                }
                this.b = strArr;
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public b b(String... strArr) {
            if (this.a) {
                if (strArr == null) {
                    this.c = null;
                } else {
                    this.c = (String[]) strArr.clone();
                }
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        @DexIgnore
        public b(zu3 zu3) {
            this.a = zu3.a;
            this.b = zu3.b;
            this.c = zu3.c;
            this.d = zu3.d;
        }

        @DexIgnore
        public b a(String... strArr) {
            if (this.a) {
                if (strArr == null) {
                    this.b = null;
                } else {
                    this.b = (String[]) strArr.clone();
                }
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public b a(TlsVersion... tlsVersionArr) {
            if (!this.a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (tlsVersionArr.length != 0) {
                String[] strArr = new String[tlsVersionArr.length];
                for (int i = 0; i < tlsVersionArr.length; i++) {
                    strArr[i] = tlsVersionArr[i].javaName;
                }
                this.c = strArr;
                return this;
            } else {
                throw new IllegalArgumentException("At least one TlsVersion is required");
            }
        }

        @DexIgnore
        public b a(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public zu3 a() {
            return new zu3(this);
        }
    }

    /*
    static {
        b bVar = new b(true);
        bVar.a(e);
        bVar.a(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0);
        bVar.a(true);
        f = bVar.a();
        b bVar2 = new b(f);
        bVar2.a(TlsVersion.TLS_1_0);
        bVar2.a(true);
        g = bVar2.a();
    }
    */

    @DexIgnore
    public List<TlsVersion> c() {
        TlsVersion[] tlsVersionArr = new TlsVersion[this.c.length];
        int i = 0;
        while (true) {
            String[] strArr = this.c;
            if (i >= strArr.length) {
                return xv3.a((T[]) tlsVersionArr);
            }
            tlsVersionArr[i] = TlsVersion.forJavaName(strArr[i]);
            i++;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof zu3)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zu3 zu3 = (zu3) obj;
        boolean z = this.a;
        if (z != zu3.a) {
            return false;
        }
        return !z || (Arrays.equals(this.b, zu3.b) && Arrays.equals(this.c, zu3.c) && this.d == zu3.d);
    }

    @DexIgnore
    public int hashCode() {
        if (this.a) {
            return ((((527 + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c)) * 31) + (this.d ^ true ? 1 : 0);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        String str;
        if (!this.a) {
            return "ConnectionSpec()";
        }
        List<CipherSuite> a2 = a();
        if (a2 == null) {
            str = "[use default]";
        } else {
            str = a2.toString();
        }
        return "ConnectionSpec(cipherSuites=" + str + ", tlsVersions=" + c() + ", supportsTlsExtensions=" + this.d + ")";
    }

    @DexIgnore
    public zu3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
    }

    @DexIgnore
    public List<CipherSuite> a() {
        String[] strArr = this.b;
        if (strArr == null) {
            return null;
        }
        CipherSuite[] cipherSuiteArr = new CipherSuite[strArr.length];
        int i = 0;
        while (true) {
            String[] strArr2 = this.b;
            if (i >= strArr2.length) {
                return xv3.a((T[]) cipherSuiteArr);
            }
            cipherSuiteArr[i] = CipherSuite.forJavaName(strArr2[i]);
            i++;
        }
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public final zu3 b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2;
        if (this.b != null) {
            strArr = (String[]) xv3.a(String.class, (T[]) this.b, (T[]) sSLSocket.getEnabledCipherSuites());
        } else {
            strArr = null;
        }
        if (!z || !Arrays.asList(sSLSocket.getSupportedCipherSuites()).contains("TLS_FALLBACK_SCSV")) {
            strArr2 = strArr;
        } else {
            if (strArr == null) {
                strArr = sSLSocket.getEnabledCipherSuites();
            }
            strArr2 = new String[(strArr.length + 1)];
            System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
            strArr2[strArr2.length - 1] = "TLS_FALLBACK_SCSV";
        }
        String[] enabledProtocols = sSLSocket.getEnabledProtocols();
        b bVar = new b(this);
        bVar.a(strArr2);
        bVar.b((String[]) xv3.a(String.class, (T[]) this.c, (T[]) enabledProtocols));
        return bVar.a();
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        zu3 b2 = b(sSLSocket, z);
        sSLSocket.setEnabledProtocols(b2.c);
        String[] strArr = b2.b;
        if (strArr != null) {
            sSLSocket.setEnabledCipherSuites(strArr);
        }
    }

    @DexIgnore
    public boolean a(SSLSocket sSLSocket) {
        if (!this.a) {
            return false;
        }
        if (!a(this.c, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        if (this.b != null) {
            return a(this.b, sSLSocket.getEnabledCipherSuites());
        } else if (sSLSocket.getEnabledCipherSuites().length > 0) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String[] strArr, String[] strArr2) {
        if (!(strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0)) {
            for (String a2 : strArr) {
                if (a((T[]) strArr2, a2)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public static <T> boolean a(T[] tArr, T t) {
        for (T a2 : tArr) {
            if (xv3.a((Object) t, (Object) a2)) {
                return true;
            }
        }
        return false;
    }
}
