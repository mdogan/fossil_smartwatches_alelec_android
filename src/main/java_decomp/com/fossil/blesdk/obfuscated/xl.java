package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.PowerManager;
import java.util.HashMap;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xl {
    @DexIgnore
    public static /* final */ String a; // = ej.a("WakeLocks");
    @DexIgnore
    public static /* final */ WeakHashMap<PowerManager.WakeLock, String> b; // = new WeakHashMap<>();

    @DexIgnore
    public static PowerManager.WakeLock a(Context context, String str) {
        String str2 = "WorkManager: " + str;
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getApplicationContext().getSystemService("power")).newWakeLock(1, str2);
        synchronized (b) {
            b.put(newWakeLock, str2);
        }
        return newWakeLock;
    }

    @DexIgnore
    public static void a() {
        HashMap hashMap = new HashMap();
        synchronized (b) {
            hashMap.putAll(b);
        }
        for (PowerManager.WakeLock wakeLock : hashMap.keySet()) {
            if (wakeLock != null && wakeLock.isHeld()) {
                ej.a().e(a, String.format("WakeLock held for %s", new Object[]{hashMap.get(wakeLock)}), new Throwable[0]);
            }
        }
    }
}
