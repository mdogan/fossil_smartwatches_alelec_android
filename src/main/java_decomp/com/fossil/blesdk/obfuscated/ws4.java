package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ws4 implements sr4<qm4, Integer> {
    @DexIgnore
    public static /* final */ ws4 a; // = new ws4();

    @DexIgnore
    public Integer a(qm4 qm4) throws IOException {
        return Integer.valueOf(qm4.F());
    }
}
