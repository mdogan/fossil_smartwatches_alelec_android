package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class di extends gi {
    @DexIgnore
    public static Method b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Method d;
    @DexIgnore
    public static boolean e;

    @DexIgnore
    public void a(View view) {
    }

    @DexIgnore
    public void a(View view, float f) {
        b();
        Method method = b;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{Float.valueOf(f)});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        } else {
            view.setAlpha(f);
        }
    }

    @DexIgnore
    public float b(View view) {
        a();
        Method method = d;
        if (method != null) {
            try {
                return ((Float) method.invoke(view, new Object[0])).floatValue();
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return super.b(view);
    }

    @DexIgnore
    public void c(View view) {
    }

    @DexIgnore
    public final void a() {
        if (!e) {
            try {
                d = View.class.getDeclaredMethod("getTransitionAlpha", new Class[0]);
                d.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", e2);
            }
            e = true;
        }
    }

    @DexIgnore
    public final void b() {
        if (!c) {
            try {
                b = View.class.getDeclaredMethod("setTransitionAlpha", new Class[]{Float.TYPE});
                b.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", e2);
            }
            c = true;
        }
    }
}
