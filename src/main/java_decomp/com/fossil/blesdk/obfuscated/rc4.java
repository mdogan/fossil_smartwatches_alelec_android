package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rc4 {
    @DexIgnore
    rc4 getCallerFrame();

    @DexIgnore
    StackTraceElement getStackTraceElement();
}
