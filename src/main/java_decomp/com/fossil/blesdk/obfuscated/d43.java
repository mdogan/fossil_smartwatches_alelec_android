package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.rs2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d43 extends as2 implements c43 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<la2> j;
    @DexIgnore
    public rs2 k;
    @DexIgnore
    public b43 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return d43.n;
        }

        @DexIgnore
        public final d43 b() {
            return new d43();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d43 e;

        @DexIgnore
        public b(d43 d43) {
            this.e = d43;
        }

        @DexIgnore
        public final void onClick(View view) {
            la2 a = this.e.U0().a();
            if (a != null) {
                a.s.setText("");
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ d43 e;

        @DexIgnore
        public c(d43 d43) {
            this.e = d43;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (TextUtils.isEmpty(charSequence)) {
                la2 a = this.e.U0().a();
                if (a != null) {
                    ImageView imageView = a.r;
                    if (imageView != null) {
                        imageView.setVisibility(8);
                    }
                }
                this.e.a("");
                this.e.V0().h();
                return;
            }
            la2 a2 = this.e.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                if (flexibleTextView != null) {
                    flexibleTextView.setVisibility(8);
                }
            }
            la2 a3 = this.e.U0().a();
            if (a3 != null) {
                ImageView imageView2 = a3.r;
                if (imageView2 != null) {
                    imageView2.setVisibility(0);
                }
            }
            this.e.a(String.valueOf(charSequence));
            this.e.V0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d43 e;

        @DexIgnore
        public d(d43 d43) {
            this.e = d43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements rs2.d {
        @DexIgnore
        public /* final */ /* synthetic */ d43 a;

        @DexIgnore
        public e(d43 d43) {
            this.a = d43;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            la2 a2 = this.a.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                wd4.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.u;
                wd4.a((Object) flexibleTextView2, "it.tvNotFound");
                be4 be4 = be4.a;
                String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements rs2.e {
        @DexIgnore
        public /* final */ /* synthetic */ d43 a;

        @DexIgnore
        public f(d43 d43) {
            this.a = d43;
        }

        @DexIgnore
        public void a(Complication complication) {
            wd4.b(complication, "item");
            this.a.V0().a(complication);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ la2 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(la2 la2, long j) {
            this.a = la2;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.a.q;
            wd4.a((Object) flexibleTextView, "it");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                flexibleTextView.animate().setDuration(this.b).alpha(1.0f);
            } else {
                flexibleTextView.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = d43.class.getSimpleName();
        wd4.a((Object) simpleName, "ComplicationSearchFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void T0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            dl2 dl2 = dl2.a;
            ur3<la2> ur3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (ur3 != null) {
                la2 a2 = ur3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    wd4.a((Object) activity, "it");
                    dl2.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final ur3<la2> U0() {
        ur3<la2> ur3 = this.j;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final b43 V0() {
        b43 b43 = this.l;
        if (b43 != null) {
            return b43;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(List<Pair<Complication, String>> list) {
        wd4.b(list, "results");
        rs2 rs2 = this.k;
        if (rs2 != null) {
            rs2.b(list);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void e(List<Pair<Complication, String>> list) {
        wd4.b(list, "recentSearchResult");
        rs2 rs2 = this.k;
        if (rs2 != null) {
            rs2.a(list);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (la2) ra.a(layoutInflater, R.layout.fragment_complication_search, viewGroup, false, O0()));
        ur3<la2> ur3 = this.j;
        if (ur3 != null) {
            la2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        b43 b43 = this.l;
        if (b43 != null) {
            b43.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        b43 b43 = this.l;
        if (b43 != null) {
            b43.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "it");
            a(activity, 550);
        }
        this.k = new rs2();
        ur3<la2> ur3 = this.j;
        if (ur3 != null) {
            la2 a2 = ur3.a();
            if (a2 != null) {
                la2 la2 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = la2.t;
                wd4.a((Object) recyclerViewEmptySupport, "this.rvResults");
                rs2 rs2 = this.k;
                if (rs2 != null) {
                    recyclerViewEmptySupport.setAdapter(rs2);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = la2.t;
                    wd4.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = la2.t;
                    FlexibleTextView flexibleTextView = la2.u;
                    wd4.a((Object) flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = la2.r;
                    wd4.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    la2.r.setOnClickListener(new b(this));
                    la2.s.addTextChangedListener(new c(this));
                    la2.q.setOnClickListener(new d(this));
                    rs2 rs22 = this.k;
                    if (rs22 != null) {
                        rs22.a((rs2.d) new e(this));
                        rs2 rs23 = this.k;
                        if (rs23 != null) {
                            rs23.a((rs2.e) new f(this));
                        } else {
                            wd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wd4.d("mAdapter");
                        throw null;
                    }
                } else {
                    wd4.d("mAdapter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        rs2 rs2 = this.k;
        if (rs2 != null) {
            rs2.b((List<Pair<Complication, String>>) null);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = bq2.a.a(j2);
        Window window = fragmentActivity.getWindow();
        wd4.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        ur3<la2> ur3 = this.j;
        if (ur3 != null) {
            la2 a3 = ur3.a();
            if (a3 != null) {
                wd4.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, la2 la2) {
        FlexibleTextView flexibleTextView = la2.q;
        wd4.a((Object) flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener(new g(la2, j2));
    }

    @DexIgnore
    public void a(String str) {
        wd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        rs2 rs2 = this.k;
        if (rs2 != null) {
            rs2.a(str);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(b43 b43) {
        wd4.b(b43, "presenter");
        this.l = b43;
    }

    @DexIgnore
    public void a(Complication complication) {
        wd4.b(complication, "selectedComplication");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, new Intent().putExtra("SEARCH_COMPLICATION_RESULT_ID", complication.getComplicationId()));
            dl2 dl2 = dl2.a;
            ur3<la2> ur3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (ur3 != null) {
                la2 a2 = ur3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    wd4.a((Object) activity, "it");
                    dl2.a(flexibleTextView, activity);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            wd4.d("mBinding");
            throw null;
        }
    }
}
