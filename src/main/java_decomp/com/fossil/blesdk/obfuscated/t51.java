package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t51 extends wb1<t51> {
    @DexIgnore
    public static volatile t51[] j;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public u51[] e; // = u51.e();
    @DexIgnore
    public Boolean f; // = null;
    @DexIgnore
    public v51 g; // = null;
    @DexIgnore
    public Boolean h; // = null;
    @DexIgnore
    public Boolean i; // = null;

    @DexIgnore
    public t51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static t51[] e() {
        if (j == null) {
            synchronized (ac1.b) {
                if (j == null) {
                    j = new t51[0];
                }
            }
        }
        return j;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            vb1.a(2, str);
        }
        u51[] u51Arr = this.e;
        if (u51Arr != null && u51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                u51[] u51Arr2 = this.e;
                if (i2 >= u51Arr2.length) {
                    break;
                }
                u51 u51 = u51Arr2[i2];
                if (u51 != null) {
                    vb1.a(3, (bc1) u51);
                }
                i2++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            vb1.a(4, bool.booleanValue());
        }
        v51 v51 = this.g;
        if (v51 != null) {
            vb1.a(5, (bc1) v51);
        }
        Boolean bool2 = this.h;
        if (bool2 != null) {
            vb1.a(6, bool2.booleanValue());
        }
        Boolean bool3 = this.i;
        if (bool3 != null) {
            vb1.a(7, bool3.booleanValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof t51)) {
            return false;
        }
        t51 t51 = (t51) obj;
        Integer num = this.c;
        if (num == null) {
            if (t51.c != null) {
                return false;
            }
        } else if (!num.equals(t51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (t51.d != null) {
                return false;
            }
        } else if (!str.equals(t51.d)) {
            return false;
        }
        if (!ac1.a((Object[]) this.e, (Object[]) t51.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (t51.f != null) {
                return false;
            }
        } else if (!bool.equals(t51.f)) {
            return false;
        }
        v51 v51 = this.g;
        if (v51 == null) {
            if (t51.g != null) {
                return false;
            }
        } else if (!v51.equals(t51.g)) {
            return false;
        }
        Boolean bool2 = this.h;
        if (bool2 == null) {
            if (t51.h != null) {
                return false;
            }
        } else if (!bool2.equals(t51.h)) {
            return false;
        }
        Boolean bool3 = this.i;
        if (bool3 == null) {
            if (t51.i != null) {
                return false;
            }
        } else if (!bool3.equals(t51.i)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(t51.b);
        }
        yb1 yb12 = t51.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i2;
        int hashCode = (t51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i3 = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (((hashCode2 + (str == null ? 0 : str.hashCode())) * 31) + ac1.a((Object[]) this.e)) * 31;
        Boolean bool = this.f;
        int hashCode4 = hashCode3 + (bool == null ? 0 : bool.hashCode());
        v51 v51 = this.g;
        int i4 = hashCode4 * 31;
        if (v51 == null) {
            i2 = 0;
        } else {
            i2 = v51.hashCode();
        }
        int i5 = (i4 + i2) * 31;
        Boolean bool2 = this.h;
        int hashCode5 = (i5 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Boolean bool3 = this.i;
        int hashCode6 = (hashCode5 + (bool3 == null ? 0 : bool3.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode6 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            a += vb1.b(2, str);
        }
        u51[] u51Arr = this.e;
        if (u51Arr != null && u51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                u51[] u51Arr2 = this.e;
                if (i2 >= u51Arr2.length) {
                    break;
                }
                u51 u51 = u51Arr2[i2];
                if (u51 != null) {
                    a += vb1.b(3, (bc1) u51);
                }
                i2++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            bool.booleanValue();
            a += vb1.c(4) + 1;
        }
        v51 v51 = this.g;
        if (v51 != null) {
            a += vb1.b(5, (bc1) v51);
        }
        Boolean bool2 = this.h;
        if (bool2 != null) {
            bool2.booleanValue();
            a += vb1.c(6) + 1;
        }
        Boolean bool3 = this.i;
        if (bool3 == null) {
            return a;
        }
        bool3.booleanValue();
        return a + vb1.c(7) + 1;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(ub1.e());
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (c2 == 26) {
                int a = ec1.a(ub1, 26);
                u51[] u51Arr = this.e;
                int length = u51Arr == null ? 0 : u51Arr.length;
                u51[] u51Arr2 = new u51[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.e, 0, u51Arr2, 0, length);
                }
                while (length < u51Arr2.length - 1) {
                    u51Arr2[length] = new u51();
                    ub1.a((bc1) u51Arr2[length]);
                    ub1.c();
                    length++;
                }
                u51Arr2[length] = new u51();
                ub1.a((bc1) u51Arr2[length]);
                this.e = u51Arr2;
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(ub1.d());
            } else if (c2 == 42) {
                if (this.g == null) {
                    this.g = new v51();
                }
                ub1.a((bc1) this.g);
            } else if (c2 == 48) {
                this.h = Boolean.valueOf(ub1.d());
            } else if (c2 == 56) {
                this.i = Boolean.valueOf(ub1.d());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
