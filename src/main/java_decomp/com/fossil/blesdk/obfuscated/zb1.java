package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzyh;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zb1 implements Cloneable {
    @DexIgnore
    public xb1<?, ?> e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public List<dc1> g; // = new ArrayList();

    @DexIgnore
    public final void a(dc1 dc1) throws IOException {
        List<dc1> list = this.g;
        if (list != null) {
            list.add(dc1);
            return;
        }
        Object obj = this.f;
        if (obj instanceof bc1) {
            byte[] bArr = dc1.b;
            ub1 a = ub1.a(bArr, 0, bArr.length);
            int e2 = a.e();
            if (e2 == bArr.length - vb1.d(e2)) {
                bc1 a2 = ((bc1) this.f).a(a);
                this.e = this.e;
                this.f = a2;
                this.g = null;
                return;
            }
            throw zzyh.zzzd();
        } else if (obj instanceof bc1[]) {
            this.e.a((List<dc1>) Collections.singletonList(dc1));
            throw null;
        } else if (obj instanceof x91) {
            this.e.a((List<dc1>) Collections.singletonList(dc1));
            throw null;
        } else if (obj instanceof x91[]) {
            this.e.a((List<dc1>) Collections.singletonList(dc1));
            throw null;
        } else {
            this.e.a((List<dc1>) Collections.singletonList(dc1));
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final zb1 clone() {
        zb1 zb1 = new zb1();
        try {
            zb1.e = this.e;
            if (this.g == null) {
                zb1.g = null;
            } else {
                zb1.g.addAll(this.g);
            }
            if (this.f != null) {
                if (this.f instanceof bc1) {
                    zb1.f = (bc1) ((bc1) this.f).clone();
                } else if (this.f instanceof byte[]) {
                    zb1.f = ((byte[]) this.f).clone();
                } else {
                    int i = 0;
                    if (this.f instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zb1.f = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f instanceof boolean[]) {
                        zb1.f = ((boolean[]) this.f).clone();
                    } else if (this.f instanceof int[]) {
                        zb1.f = ((int[]) this.f).clone();
                    } else if (this.f instanceof long[]) {
                        zb1.f = ((long[]) this.f).clone();
                    } else if (this.f instanceof float[]) {
                        zb1.f = ((float[]) this.f).clone();
                    } else if (this.f instanceof double[]) {
                        zb1.f = ((double[]) this.f).clone();
                    } else if (this.f instanceof bc1[]) {
                        bc1[] bc1Arr = (bc1[]) this.f;
                        bc1[] bc1Arr2 = new bc1[bc1Arr.length];
                        zb1.f = bc1Arr2;
                        while (i < bc1Arr.length) {
                            bc1Arr2[i] = (bc1) bc1Arr[i].clone();
                            i++;
                        }
                    }
                }
            }
            return zb1;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zb1)) {
            return false;
        }
        zb1 zb1 = (zb1) obj;
        if (this.f == null || zb1.f == null) {
            List<dc1> list = this.g;
            if (list != null) {
                List<dc1> list2 = zb1.g;
                if (list2 != null) {
                    return list.equals(list2);
                }
            }
            try {
                return Arrays.equals(a(), zb1.a());
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        } else {
            xb1<?, ?> xb1 = this.e;
            if (xb1 != zb1.e) {
                return false;
            }
            if (!xb1.a.isArray()) {
                return this.f.equals(zb1.f);
            }
            Object obj2 = this.f;
            if (obj2 instanceof byte[]) {
                return Arrays.equals((byte[]) obj2, (byte[]) zb1.f);
            }
            if (obj2 instanceof int[]) {
                return Arrays.equals((int[]) obj2, (int[]) zb1.f);
            }
            if (obj2 instanceof long[]) {
                return Arrays.equals((long[]) obj2, (long[]) zb1.f);
            }
            if (obj2 instanceof float[]) {
                return Arrays.equals((float[]) obj2, (float[]) zb1.f);
            }
            if (obj2 instanceof double[]) {
                return Arrays.equals((double[]) obj2, (double[]) zb1.f);
            }
            if (obj2 instanceof boolean[]) {
                return Arrays.equals((boolean[]) obj2, (boolean[]) zb1.f);
            }
            return Arrays.deepEquals((Object[]) obj2, (Object[]) zb1.f);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return Arrays.hashCode(a()) + 527;
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public final int zzf() {
        Object obj = this.f;
        if (obj != null) {
            xb1<?, ?> xb1 = this.e;
            if (xb1.b) {
                int length = Array.getLength(obj);
                int i = 0;
                while (i < length) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 == null) {
                        i++;
                    } else {
                        xb1.a(obj2);
                        throw null;
                    }
                }
                return 0;
            }
            xb1.a(obj);
            throw null;
        }
        int i2 = 0;
        for (dc1 next : this.g) {
            i2 += vb1.e(next.a) + 0 + next.b.length;
        }
        return i2;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Object obj = this.f;
        if (obj != null) {
            xb1<?, ?> xb1 = this.e;
            if (xb1.b) {
                int length = Array.getLength(obj);
                int i = 0;
                while (i < length) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 == null) {
                        i++;
                    } else {
                        xb1.a(obj2, vb1);
                        throw null;
                    }
                }
                return;
            }
            xb1.a(obj, vb1);
            throw null;
        }
        for (dc1 next : this.g) {
            vb1.b(next.a);
            vb1.a(next.b);
        }
    }

    @DexIgnore
    public final byte[] a() throws IOException {
        byte[] bArr = new byte[zzf()];
        a(vb1.b(bArr));
        return bArr;
    }
}
