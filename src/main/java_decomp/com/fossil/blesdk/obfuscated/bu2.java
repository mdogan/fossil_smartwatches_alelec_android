package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bu2 {
    @DexIgnore
    public /* final */ zt2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public bu2(zt2 zt2, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wd4.b(zt2, "mView");
        wd4.b(str, "mDeviceId");
        wd4.b(arrayList, "mAlarms");
        this.a = zt2;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final zt2 d() {
        return this.a;
    }
}
