package com.fossil.blesdk.obfuscated;

import java.net.InetAddress;
import java.net.UnknownHostException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tv3 {
    @DexIgnore
    public static final tv3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tv3 {
        @DexIgnore
        public InetAddress[] a(String str) throws UnknownHostException {
            if (str != null) {
                return InetAddress.getAllByName(str);
            }
            throw new UnknownHostException("host == null");
        }
    }

    @DexIgnore
    InetAddress[] a(String str) throws UnknownHostException;
}
