package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.data.model.ua.UADataSource;
import com.portfolio.platform.data.model.ua.UADevice;
import com.portfolio.platform.data.model.ua.UAEmbedded;
import com.portfolio.platform.data.model.ua.UALink;
import com.portfolio.platform.data.model.ua.UALinks;
import com.portfolio.platform.underamour.UASharePref;
import com.portfolio.platform.underamour.UAValues;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gr3 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public dr3 c;
    @DexIgnore
    public fr3 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements p94<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ gr3 a;
        @DexIgnore
        public /* final */ /* synthetic */ UADataSource b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements p94<yz1> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void a(yz1 yz1) {
                List<UALink> list;
                if (yz1 != null) {
                    FLogger.INSTANCE.getLocal().d(gr3.e, yz1.toString());
                    UADataSource uADataSource = (UADataSource) new Gson().a((JsonElement) yz1, UADataSource.class);
                    UASharePref a2 = UASharePref.c.a();
                    wd4.a((Object) uADataSource, "uaDataSourceResponse");
                    a2.a(uADataSource);
                    UADataSource c = UASharePref.c.a().c();
                    if (c != null) {
                        UALinks link = c.getLink();
                        if (link != null) {
                            list = link.getSelf();
                            if (list != null && (!list.isEmpty())) {
                                yz1 yz12 = (yz1) new Gson().a(new Gson().a((Object) list.get(0), (Type) UALink.class), yz1.class);
                                gr3 gr3 = this.a.a;
                                wd4.a((Object) yz12, "extraJsonObject");
                                gr3.a(yz12);
                                return;
                            }
                        }
                    }
                    list = null;
                    if (list != null) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr3$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.gr3$b$b  reason: collision with other inner class name */
        public static final class C0085b<T> implements p94<Throwable> {
            @DexIgnore
            public static /* final */ C0085b a; // = new C0085b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(gr3.e, th.getMessage());
                }
            }
        }

        @DexIgnore
        public b(gr3 gr3, UADataSource uADataSource) {
            this.a = gr3;
            this.b = uADataSource;
        }

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) yz1, UAAccessToken.class);
                if (!cg4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                dr3 d = this.a.c;
                UADataSource uADataSource = this.b;
                String b2 = this.a.a;
                if (b2 != null) {
                    be4 be4 = be4.a;
                    Locale locale = Locale.US;
                    wd4.a((Object) locale, "Locale.US");
                    String value = UAValues.Authorization.BEARER.getValue();
                    Object[] objArr = {UASharePref.c.a().a()};
                    String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                    d.a(uADataSource, b2, format).a(3).a(h94.a()).a(new a(this), (p94<? super Throwable>) C0085b.a);
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements p94<Throwable> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(gr3.e, th.getMessage());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements p94<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ gr3 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements p94<yz1> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void a(yz1 yz1) {
                if (yz1 != null) {
                    FLogger.INSTANCE.getLocal().d(gr3.e, yz1.toString());
                    UADevice uADevice = (UADevice) new Gson().a((JsonElement) yz1, UADevice.class);
                    UASharePref a2 = UASharePref.c.a();
                    wd4.a((Object) uADevice, "uaDevice");
                    a2.a(uADevice);
                    this.a.a.a();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements p94<Throwable> {
            @DexIgnore
            public static /* final */ b a; // = new b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(gr3.e, th.getMessage());
                }
            }
        }

        @DexIgnore
        public d(gr3 gr3) {
            this.a = gr3;
        }

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) yz1, UAAccessToken.class);
                if (!cg4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                dr3 d = this.a.c;
                String c = this.a.b;
                if (c != null) {
                    String b2 = this.a.a;
                    if (b2 != null) {
                        be4 be4 = be4.a;
                        Locale locale = Locale.US;
                        wd4.a((Object) locale, "Locale.US");
                        String value = UAValues.Authorization.BEARER.getValue();
                        Object[] objArr = {UASharePref.c.a().a()};
                        String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        d.a(c, b2, format).a(3).a(h94.a()).a(new a(this), (p94<? super Throwable>) b.a);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements p94<Throwable> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(gr3.e, th.getMessage());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements p94<yz1> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                FLogger.INSTANCE.getLocal().d(gr3.e, yz1.toString());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements p94<Throwable> {
        @DexIgnore
        public static /* final */ g a; // = new g();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(gr3.e, th.getMessage());
            }
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = gr3.class.getSimpleName();
        wd4.a((Object) simpleName, "UADataSourceManager::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public gr3(dr3 dr3, fr3 fr3) {
        wd4.b(dr3, "uaApi");
        wd4.b(fr3, "uaAuthorizationManager");
        this.c = dr3;
        this.d = fr3;
    }

    @DexIgnore
    public final void b(String str) {
        wd4.b(str, "deviceKey");
        this.b = str;
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "clientId");
        this.a = str;
    }

    @DexIgnore
    public final void b() {
        if (this.b == null) {
            throw new IllegalArgumentException("deviceKey must be set");
        } else if (UASharePref.c.a().d() != null) {
            a();
        } else if (UASharePref.c.a().d() == null) {
            this.d.a().a(new d(this), (p94<? super Throwable>) e.a);
        }
    }

    @DexIgnore
    public final void a(yz1 yz1) {
        yz1 yz12 = new yz1();
        uz1 uz1 = new uz1();
        uz1.a((JsonElement) yz1);
        yz1 yz13 = new yz1();
        yz13.a("data_source", (JsonElement) uz1);
        yz12.a("priority_type", (JsonElement) new a02(Constants.ACTIVITY));
        yz12.a("_links", (JsonElement) yz13);
        dr3 dr3 = this.c;
        String str = this.a;
        if (str != null) {
            be4 be4 = be4.a;
            Locale locale = Locale.US;
            wd4.a((Object) locale, "Locale.US");
            String value = UAValues.Authorization.BEARER.getValue();
            Object[] objArr = {UASharePref.c.a().a()};
            String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            dr3.b(yz12, str, format).a(3).a(h94.a()).a(f.a, (p94<? super Throwable>) g.a);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        UAEmbedded uAEmbedded = new UAEmbedded();
        UADevice[] uADeviceArr = new UADevice[1];
        UADevice d2 = UASharePref.c.a().d();
        if (d2 != null) {
            uADeviceArr[0] = d2;
            uAEmbedded.setDevice(ob4.a((T[]) uADeviceArr));
            UADataSource uADataSource = new UADataSource();
            uADataSource.setEmbedded(uAEmbedded);
            this.d.a().a(new b(this, uADataSource), (p94<? super Throwable>) c.a);
            return;
        }
        wd4.a();
        throw null;
    }
}
