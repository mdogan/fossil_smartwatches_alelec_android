package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oc0 extends fc0 {
    @DexIgnore
    public /* final */ /* synthetic */ nc0 e;

    @DexIgnore
    public oc0(nc0 nc0) {
        this.e = nc0;
    }

    @DexIgnore
    public final void b(Status status) throws RemoteException {
        this.e.a(status);
    }
}
