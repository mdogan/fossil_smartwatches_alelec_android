package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.blesdk.obfuscated.d6;
import com.fossil.blesdk.obfuscated.il2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kp2 extends qp2 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((rd4) null);
    @DexIgnore
    public LocationSource h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kp2.i;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = kp2.class.getSimpleName();
        wd4.a((Object) simpleName, "LocationSupportedService::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public final void a(LocationSource.ErrorState errorState) {
        String str;
        wd4.b(errorState, "errorState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "sendNotificationWeather, errorState=" + errorState);
        Intent intent = new Intent();
        if (errorState == LocationSource.ErrorState.LOCATION_PERMISSION_OFF) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.W.c().getPackageName(), (String) null));
            str = tm2.a(getApplicationContext(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationServicesAreRequiredForLocation);
            wd4.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else if (errorState == LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = tm2.a(getApplicationContext(), (int) R.string.background_location_service_general_explain);
            wd4.a((Object) str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (errorState == LocationSource.ErrorState.LOCATION_SERVICE_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = tm2.a(getApplicationContext(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationServicesAreRequiredForLocation);
            wd4.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else {
            str = "";
        }
        String str3 = str;
        PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 134217728);
        il2.a aVar = il2.a;
        String string = PortfolioApp.W.c().getString(R.string.brand_name);
        wd4.a((Object) string, "PortfolioApp.instance.ge\u2026ring(R.string.brand_name)");
        wd4.a((Object) activity, "pendingIntent");
        aVar.b(this, 8, string, str3, activity, (List<? extends d6.a>) null);
    }

    @DexIgnore
    public final LocationSource e() {
        LocationSource locationSource = this.h;
        if (locationSource != null) {
            return locationSource;
        }
        wd4.d("mLocationSource");
        throw null;
    }
}
