package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l10 extends BluetoothCommand {
    @DexIgnore
    public Peripheral.BondState k; // = Peripheral.BondState.BOND_NONE;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l10(Peripheral.c cVar) {
        super(BluetoothCommandId.REMOVE_BOND, cVar);
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.r();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        if (gattOperationResult instanceof v10) {
            v10 v10 = (v10) gattOperationResult;
            if (v10.b() == Peripheral.BondState.BONDED || v10.b() == Peripheral.BondState.BOND_NONE) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        this.k = ((v10) gattOperationResult).b();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().i();
    }

    @DexIgnore
    public final Peripheral.BondState i() {
        return this.k;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        BluetoothCommand.Result result;
        wd4.b(gattOperationResult, "gattOperationResult");
        d(gattOperationResult);
        if (gattOperationResult.a().getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            BluetoothCommand.Result a = BluetoothCommand.Result.Companion.a(gattOperationResult.a());
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, a.getResultCode(), a.getGattResult(), 1, (Object) null);
        } else if (this.k == Peripheral.BondState.BOND_NONE) {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (Object) null);
        } else {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT, (GattOperationResult.GattResult) null, 5, (Object) null);
        }
        a(result);
    }
}
