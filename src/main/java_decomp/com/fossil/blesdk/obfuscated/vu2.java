package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vu2 implements MembersInjector<tu2> {
    @DexIgnore
    public static void a(tu2 tu2, DashboardActivityPresenter dashboardActivityPresenter) {
        tu2.k = dashboardActivityPresenter;
    }

    @DexIgnore
    public static void a(tu2 tu2, DashboardActiveTimePresenter dashboardActiveTimePresenter) {
        tu2.l = dashboardActiveTimePresenter;
    }

    @DexIgnore
    public static void a(tu2 tu2, DashboardCaloriesPresenter dashboardCaloriesPresenter) {
        tu2.m = dashboardCaloriesPresenter;
    }

    @DexIgnore
    public static void a(tu2 tu2, DashboardHeartRatePresenter dashboardHeartRatePresenter) {
        tu2.n = dashboardHeartRatePresenter;
    }

    @DexIgnore
    public static void a(tu2 tu2, DashboardSleepPresenter dashboardSleepPresenter) {
        tu2.o = dashboardSleepPresenter;
    }

    @DexIgnore
    public static void a(tu2 tu2, DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
        tu2.p = dashboardGoalTrackingPresenter;
    }
}
