package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nt2 extends sd<DailyHeartRateSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ ot2 e;
    @DexIgnore
    public /* final */ FragmentManager f;
    @DexIgnore
    public /* final */ as2 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public String f;
        @DexIgnore
        public int g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4) {
            wd4.b(str, "mDayOfWeek");
            wd4.b(str2, "mDayOfMonth");
            wd4.b(str3, "mDailyRestingUnit");
            wd4.b(str4, "mDailyMaxUnit");
            this.a = date;
            this.b = z;
            this.c = str;
            this.d = str2;
            this.e = i;
            this.f = str3;
            this.g = i2;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(String str) {
            wd4.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void c(String str) {
            wd4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void d(String str) {
            wd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final Date e() {
            return this.a;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final String g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4, int i3, rd4 rd4) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) != 0 ? r5 : str, (r0 & 8) != 0 ? r5 : str2, (r0 & 16) != 0 ? 0 : i, (r0 & 32) != 0 ? r5 : str3, (r0 & 64) == 0 ? i2 : 0, (r0 & 128) == 0 ? str4 : r5);
            int i4 = i3;
            Date date2 = (i4 & 1) != 0 ? null : date;
            String str5 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void b(int i) {
            this.e = i;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final int d() {
            return this.e;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final int b() {
            return this.g;
        }

        @DexIgnore
        public final void a(String str) {
            wd4.b(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.g = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ bi2 b;
        @DexIgnore
        public /* final */ /* synthetic */ nt2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a = this.e.a;
                if (a != null) {
                    this.e.c.e.b(a);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(nt2 nt2, bi2 bi2, View view) {
            super(view);
            wd4.b(bi2, "binding");
            wd4.b(view, "root");
            this.c = nt2;
            this.b = bi2;
            this.b.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            b a2 = this.c.a(dailyHeartRateSummary);
            this.a = a2.e();
            FlexibleTextView flexibleTextView = this.b.t;
            wd4.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.g());
            FlexibleTextView flexibleTextView2 = this.b.s;
            wd4.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.f());
            if (a2.b() == 0 && a2.d() == 0) {
                ConstraintLayout constraintLayout = this.b.q;
                wd4.a((Object) constraintLayout, "binding.clContainer");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.w;
                wd4.a((Object) flexibleTextView3, "binding.ftvNoRecord");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                wd4.a((Object) constraintLayout2, "binding.clContainer");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.w;
                wd4.a((Object) flexibleTextView4, "binding.ftvNoRecord");
                flexibleTextView4.setVisibility(8);
                FlexibleTextView flexibleTextView5 = this.b.y;
                wd4.a((Object) flexibleTextView5, "binding.ftvRestingValue");
                flexibleTextView5.setText(String.valueOf(a2.d()));
                FlexibleTextView flexibleTextView6 = this.b.x;
                wd4.a((Object) flexibleTextView6, "binding.ftvRestingUnit");
                flexibleTextView6.setText(a2.c());
                FlexibleTextView flexibleTextView7 = this.b.v;
                wd4.a((Object) flexibleTextView7, "binding.ftvMaxValue");
                flexibleTextView7.setText(String.valueOf(a2.b()));
                FlexibleTextView flexibleTextView8 = this.b.u;
                wd4.a((Object) flexibleTextView8, "binding.ftvMaxUnit");
                flexibleTextView8.setText(a2.a());
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            wd4.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!a2.h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wd4.b(str, "mWeekly");
            wd4.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, rd4 rd4) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wd4.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ di2 f;
        @DexIgnore
        public /* final */ /* synthetic */ nt2 g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public a(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d != null && this.e.e != null) {
                    ot2 c = this.e.g.e;
                    Date b = this.e.d;
                    if (b != null) {
                        Date a = this.e.e;
                        if (a != null) {
                            c.b(b, a);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(nt2 nt2, di2 di2) {
            super(nt2, r0, r1);
            wd4.b(di2, "binding");
            this.g = nt2;
            bi2 bi2 = di2.r;
            if (bi2 != null) {
                wd4.a((Object) bi2, "binding.dailyItem!!");
                View d2 = di2.d();
                wd4.a((Object) d2, "binding.root");
                this.f = di2;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wd4.a();
            throw null;
        }

        @DexIgnore
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            d b = this.g.b(dailyHeartRateSummary);
            this.e = b.a();
            this.d = b.b();
            FlexibleTextView flexibleTextView = this.f.s;
            wd4.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.f.t;
            wd4.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(dailyHeartRateSummary);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ nt2 e;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public f(nt2 nt2, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.e = nt2;
            this.f = viewHolder;
            this.g = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wd4.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.e.g.getId() + ", isAdded=" + this.e.g.isAdded());
            this.f.itemView.removeOnAttachStateChangeListener(this);
            Fragment a = this.e.f.a(this.e.g.R0());
            if (a == null) {
                FLogger.INSTANCE.getLocal().d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                cb a2 = this.e.f.a();
                a2.a(view.getId(), this.e.g, this.e.g.R0());
                a2.d();
            } else if (this.g) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
                cb a3 = this.e.f.a();
                a3.d(a);
                a3.d();
                cb a4 = this.e.f.a();
                a4.a(view.getId(), this.e.g, this.e.g.R0());
                a4.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.e.g.getId() + ", isAdded2=" + this.e.g.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wd4.b(view, "v");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nt2(mt2 mt2, PortfolioApp portfolioApp, ot2 ot2, FragmentManager fragmentManager, as2 as2) {
        super(mt2);
        wd4.b(mt2, "dailyHeartRateSummaryDifference");
        wd4.b(portfolioApp, "mApp");
        wd4.b(ot2, "mOnItemClick");
        wd4.b(fragmentManager, "mFragmentManager");
        wd4.b(as2, "mFragment");
        this.d = portfolioApp;
        this.e = ot2;
        this.f = fragmentManager;
        this.g = as2;
    }

    @DexIgnore
    public long getItemId(int i) {
        if (getItemViewType(i) != 0) {
            return super.getItemId(i);
        }
        if (this.g.getId() == 0) {
            return 1010101;
        }
        return (long) this.g.getId();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) a(i);
        if (dailyHeartRateSummary == null) {
            return 1;
        }
        Calendar calendar = this.c;
        wd4.a((Object) calendar, "mCalendar");
        calendar.setTime(dailyHeartRateSummary.getDate());
        Calendar calendar2 = this.c;
        wd4.a((Object) calendar2, "mCalendar");
        Boolean s = sk2.s(calendar2.getTime());
        wd4.a((Object) s, "DateHelper.isToday(mCalendar.time)");
        if (s.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardHeartRatesAdapter", "onBindViewHolder - position=" + i);
        int itemViewType = getItemViewType(i);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wd4.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wd4.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardHeartRatesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wd4.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((DailyHeartRateSummary) a(i));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((DailyHeartRateSummary) a(i));
        } else {
            ((e) viewHolder).a((DailyHeartRateSummary) a(i));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i == 1) {
            bi2 a2 = bi2.a(from, viewGroup, false);
            wd4.a((Object) a2, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wd4.a((Object) d2, "itemActivityDayBinding.root");
            return new c(this, a2, d2);
        } else if (i != 2) {
            bi2 a3 = bi2.a(from, viewGroup, false);
            wd4.a((Object) a3, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wd4.a((Object) d3, "itemActivityDayBinding.root");
            return new c(this, a3, d3);
        } else {
            di2 a4 = di2.a(from, viewGroup, false);
            wd4.a((Object) a4, "ItemHeartRateWeekBinding\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(rd<DailyHeartRateSummary> rdVar) {
        if (rdVar != null) {
            List<DailyHeartRateSummary> j = rdVar.j();
            if (j != null) {
                Calendar instance = Calendar.getInstance();
                wd4.a((Object) j, "summaries");
                if (!j.isEmpty()) {
                    wd4.a((Object) instance, "calendar");
                    instance.setTime(((DailyHeartRateSummary) wb4.d(j)).getDate());
                    if (!sk2.s(instance.getTime()).booleanValue()) {
                        instance.setTime(new Date());
                        Date time = instance.getTime();
                        wd4.a((Object) time, "calendar.time");
                        Date time2 = instance.getTime();
                        wd4.a((Object) time2, "calendar.time");
                        long time3 = time2.getTime();
                        Date time4 = instance.getTime();
                        wd4.a((Object) time4, "calendar.time");
                        new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time, time3, time4.getTime(), 0, 0, 0, (Resting) null);
                    } else {
                        Object d2 = wb4.d(j);
                        wd4.a(d2, "summaries.first()");
                        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) d2;
                    }
                } else {
                    wd4.a((Object) instance, "calendar");
                    Date time5 = instance.getTime();
                    wd4.a((Object) time5, "calendar.time");
                    Date time6 = instance.getTime();
                    wd4.a((Object) time6, "calendar.time");
                    long time7 = time6.getTime();
                    Date time8 = instance.getTime();
                    wd4.a((Object) time8, "calendar.time");
                    new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time5, time7, time8.getTime(), 0, 0, 0, (Resting) null);
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(rdVar != null ? Integer.valueOf(rdVar.size()) : null);
        local.d("DashboardHeartRatesAdapter", sb.toString());
        super.b(rdVar);
    }

    @DexIgnore
    public final b a(DailyHeartRateSummary dailyHeartRateSummary) {
        b bVar = new b((Date) null, false, (String) null, (String) null, 0, (String) null, 0, (String) null, 255, (rd4) null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int i = instance.get(7);
            Boolean s = sk2.s(instance.getTime());
            wd4.a((Object) s, "DateHelper.isToday(calendar.time)");
            if (s.booleanValue()) {
                String a2 = tm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_HeartRateToday_Text__Today);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026artRateToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(ml2.b.b(i));
            }
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            Resting resting = dailyHeartRateSummary.getResting();
            boolean z = false;
            bVar.b(resting != null ? resting.getValue() : 0);
            String a3 = tm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_StepsToday_Label__Resting);
            wd4.a((Object) a3, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
            if (a3 != null) {
                String lowerCase = a3.toLowerCase();
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                bVar.b(lowerCase);
                bVar.a(dailyHeartRateSummary.getMax());
                String a4 = tm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_StepsToday_Label__Max);
                wd4.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Label__Max)");
                if (a4 != null) {
                    String lowerCase2 = a4.toLowerCase();
                    wd4.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                    bVar.a(lowerCase2);
                    if (bVar.d() + bVar.b() == 0) {
                        z = true;
                    }
                    bVar.a(z);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(DailyHeartRateSummary dailyHeartRateSummary) {
        String str;
        String str2;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (rd4) null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            Boolean s = sk2.s(instance.getTime());
            int i = instance.get(5);
            int i2 = instance.get(2);
            String b2 = sk2.b(i2);
            int i3 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i4 = instance.get(5);
            int i5 = instance.get(2);
            String b3 = sk2.b(i5);
            int i6 = instance.get(1);
            dVar.b(instance.getTime());
            wd4.a((Object) s, "isToday");
            if (s.booleanValue()) {
                str = tm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_HeartRateToday_Title__ThisWeek);
                wd4.a((Object) str, "LanguageHelper.getString\u2026ateToday_Title__ThisWeek)");
            } else if (i2 == i5) {
                str = b3 + ' ' + i4 + " - " + b3 + ' ' + i;
            } else if (i6 == i3) {
                str = b3 + ' ' + i4 + " - " + b2 + ' ' + i;
            } else {
                str = b3 + ' ' + i4 + ", " + i6 + " - " + b2 + ' ' + i + ", " + i3;
            }
            dVar.a(str);
            if (dailyHeartRateSummary.getAvgRestingHeartRateOfWeek() == null) {
                str2 = "0";
            } else {
                str2 = String.valueOf(dailyHeartRateSummary.getAvgRestingHeartRateOfWeek());
            }
            be4 be4 = be4.a;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_HeartRateToday_Text__NumberRestingBpm);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026y_Text__NumberRestingBpm)");
            Object[] objArr = {str2};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }
}
