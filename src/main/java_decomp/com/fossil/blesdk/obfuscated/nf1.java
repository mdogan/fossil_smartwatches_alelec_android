package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nf1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nf1> CREATOR; // = new uf1();
    @DexIgnore
    public static /* final */ nf1 f; // = new nf1(0);
    @DexIgnore
    public /* final */ int e;

    /*
    static {
        Class<nf1> cls = nf1.class;
        new nf1(1);
    }
    */

    @DexIgnore
    public nf1(int i) {
        this.e = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof nf1) && this.e == ((nf1) obj).e;
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(Integer.valueOf(this.e));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.e;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", new Object[]{Integer.valueOf(i)});
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", new Object[]{str});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e);
        lk0.a(parcel, a);
    }
}
