package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.SingleRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xn<TranscodeType> extends mv<xn<TranscodeType>> implements Cloneable, vn<xn<TranscodeType>> {
    @DexIgnore
    public /* final */ Context E;
    @DexIgnore
    public /* final */ yn F;
    @DexIgnore
    public /* final */ Class<TranscodeType> G;
    @DexIgnore
    public /* final */ un H;
    @DexIgnore
    public zn<?, ? super TranscodeType> I;
    @DexIgnore
    public Object J;
    @DexIgnore
    public List<rv<TranscodeType>> K;
    @DexIgnore
    public xn<TranscodeType> L;
    @DexIgnore
    public xn<TranscodeType> M;
    @DexIgnore
    public Float N;
    @DexIgnore
    public boolean O; // = true;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ImageView.ScaleType.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[Priority.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /*
        static {
            try {
                b[Priority.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[Priority.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[Priority.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[Priority.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            a[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            try {
                a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError unused5) {
            }
        }
        */
    }

    /*
    static {
        sv svVar = (sv) ((sv) ((sv) new sv().a(qp.b)).a(Priority.LOW)).a(true);
    }
    */

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public xn(sn snVar, yn ynVar, Class<TranscodeType> cls, Context context) {
        this.F = ynVar;
        this.G = cls;
        this.E = context;
        this.I = ynVar.b(cls);
        this.H = snVar.f();
        a(ynVar.g());
        a((mv) ynVar.h());
    }

    @DexIgnore
    public ov<TranscodeType> U() {
        return c(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    public xn<TranscodeType> b(rv<TranscodeType> rvVar) {
        this.K = null;
        return a(rvVar);
    }

    @DexIgnore
    public ov<TranscodeType> c(int i, int i2) {
        qv qvVar = new qv(i, i2);
        a(qvVar, qvVar, pw.a());
        return qvVar;
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void a(List<rv<Object>> list) {
        for (rv<Object> a2 : list) {
            a(a2);
        }
    }

    @DexIgnore
    public final xn<TranscodeType> b(Object obj) {
        this.J = obj;
        this.P = true;
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> clone() {
        xn<TranscodeType> xnVar = (xn) super.clone();
        xnVar.I = xnVar.I.clone();
        return xnVar;
    }

    @DexIgnore
    public xn<TranscodeType> a(mv<?> mvVar) {
        uw.a(mvVar);
        return (xn) super.a(mvVar);
    }

    @DexIgnore
    public final <Y extends cw<TranscodeType>> Y b(Y y, rv<TranscodeType> rvVar, mv<?> mvVar, Executor executor) {
        uw.a(y);
        if (this.P) {
            pv a2 = a(y, rvVar, mvVar, executor);
            pv d = y.d();
            if (!a2.a(d) || a(mvVar, d)) {
                this.F.a((cw<?>) y);
                y.a(a2);
                this.F.a(y, a2);
                return y;
            }
            uw.a(d);
            if (!d.isRunning()) {
                d.c();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    @DexIgnore
    public xn<TranscodeType> a(rv<TranscodeType> rvVar) {
        if (rvVar != null) {
            if (this.K == null) {
                this.K = new ArrayList();
            }
            this.K.add(rvVar);
        }
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> a(xn<TranscodeType> xnVar) {
        this.M = xnVar;
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> a(Object obj) {
        b(obj);
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> a(String str) {
        b((Object) str);
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> a(Uri uri) {
        b((Object) uri);
        return this;
    }

    @DexIgnore
    public xn<TranscodeType> a(Integer num) {
        b((Object) num);
        return a((mv) sv.b(hw.a(this.E)));
    }

    @DexIgnore
    public xn<TranscodeType> a(byte[] bArr) {
        b((Object) bArr);
        xn a2 = !F() ? a((mv) sv.b(qp.a)) : this;
        return !a2.J() ? a2.a((mv) sv.c(true)) : a2;
    }

    @DexIgnore
    public final Priority b(Priority priority) {
        int i = a.b[priority.ordinal()];
        if (i == 1) {
            return Priority.NORMAL;
        }
        if (i == 2) {
            return Priority.HIGH;
        }
        if (i == 3 || i == 4) {
            return Priority.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + x());
    }

    @DexIgnore
    public <Y extends cw<TranscodeType>> Y a(Y y) {
        a(y, (rv) null, pw.b());
        return y;
    }

    @DexIgnore
    public <Y extends cw<TranscodeType>> Y a(Y y, rv<TranscodeType> rvVar, Executor executor) {
        b(y, rvVar, this, executor);
        return y;
    }

    @DexIgnore
    public final boolean a(mv<?> mvVar, pv pvVar) {
        return !mvVar.G() && pvVar.f();
    }

    @DexIgnore
    public dw<ImageView, TranscodeType> a(ImageView imageView) {
        mv mvVar;
        vw.b();
        uw.a(imageView);
        if (!M() && K() && imageView.getScaleType() != null) {
            switch (a.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    mvVar = clone().P();
                    break;
                case 2:
                    mvVar = clone().Q();
                    break;
                case 3:
                case 4:
                case 5:
                    mvVar = clone().R();
                    break;
                case 6:
                    mvVar = clone().Q();
                    break;
            }
        }
        mvVar = this;
        dw<ImageView, TranscodeType> a2 = this.H.a(imageView, this.G);
        b(a2, (rv) null, mvVar, pw.b());
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r27v0, types: [com.fossil.blesdk.obfuscated.mv<?>, com.fossil.blesdk.obfuscated.mv] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final pv b(Object obj, cw<TranscodeType> cwVar, rv<TranscodeType> rvVar, RequestCoordinator requestCoordinator, zn<?, ? super TranscodeType> znVar, Priority priority, int i, int i2, mv<?> r27, Executor executor) {
        Priority priority2;
        Object obj2 = obj;
        RequestCoordinator requestCoordinator2 = requestCoordinator;
        Priority priority3 = priority;
        xn<TranscodeType> xnVar = this.L;
        if (xnVar != null) {
            if (!this.Q) {
                zn<?, ? super TranscodeType> znVar2 = xnVar.O ? znVar : xnVar.I;
                if (this.L.H()) {
                    priority2 = this.L.x();
                } else {
                    priority2 = b(priority3);
                }
                Priority priority4 = priority2;
                int l = this.L.l();
                int k = this.L.k();
                if (vw.b(i, i2) && !this.L.N()) {
                    l = r27.l();
                    k = r27.k();
                }
                int i3 = k;
                uv uvVar = new uv(obj2, requestCoordinator2);
                Object obj3 = obj;
                cw<TranscodeType> cwVar2 = cwVar;
                rv<TranscodeType> rvVar2 = rvVar;
                uv uvVar2 = uvVar;
                pv a2 = a(obj3, cwVar2, rvVar2, (mv<?>) r27, (RequestCoordinator) uvVar, znVar, priority, i, i2, executor);
                this.Q = true;
                xn<TranscodeType> xnVar2 = this.L;
                pv a3 = xnVar2.a(obj3, cwVar2, rvVar2, (RequestCoordinator) uvVar2, znVar2, priority4, l, i3, (mv<?>) xnVar2, executor);
                this.Q = false;
                uvVar2.a(a2, a3);
                return uvVar2;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.N == null) {
            return a(obj, cwVar, rvVar, (mv<?>) r27, requestCoordinator, znVar, priority, i, i2, executor);
        } else {
            uv uvVar3 = new uv(obj2, requestCoordinator2);
            cw<TranscodeType> cwVar3 = cwVar;
            rv<TranscodeType> rvVar3 = rvVar;
            uv uvVar4 = uvVar3;
            zn<?, ? super TranscodeType> znVar3 = znVar;
            int i4 = i;
            int i5 = i2;
            Executor executor2 = executor;
            uvVar3.a(a(obj, cwVar3, rvVar3, (mv<?>) r27, (RequestCoordinator) uvVar4, znVar3, priority, i4, i5, executor2), a(obj, cwVar3, rvVar3, (mv<?>) r27.clone().a(this.N.floatValue()), (RequestCoordinator) uvVar4, znVar3, b(priority3), i4, i5, executor2));
            return uvVar3;
        }
    }

    @DexIgnore
    public final pv a(cw<TranscodeType> cwVar, rv<TranscodeType> rvVar, mv<?> mvVar, Executor executor) {
        return a(new Object(), cwVar, rvVar, (RequestCoordinator) null, this.I, mvVar.x(), mvVar.l(), mvVar.k(), mvVar, executor);
    }

    @DexIgnore
    public final pv a(Object obj, cw<TranscodeType> cwVar, rv<TranscodeType> rvVar, RequestCoordinator requestCoordinator, zn<?, ? super TranscodeType> znVar, Priority priority, int i, int i2, mv<?> mvVar, Executor executor) {
        nv nvVar;
        nv nvVar2;
        if (this.M != null) {
            nvVar2 = new nv(obj, requestCoordinator);
            nvVar = nvVar2;
        } else {
            Object obj2 = obj;
            nvVar = null;
            nvVar2 = requestCoordinator;
        }
        pv b = b(obj, cwVar, rvVar, nvVar2, znVar, priority, i, i2, mvVar, executor);
        if (nvVar == null) {
            return b;
        }
        int l = this.M.l();
        int k = this.M.k();
        if (vw.b(i, i2) && !this.M.N()) {
            l = mvVar.l();
            k = mvVar.k();
        }
        xn<TranscodeType> xnVar = this.M;
        nv nvVar3 = nvVar;
        nvVar3.a(b, xnVar.a(obj, cwVar, rvVar, (RequestCoordinator) nvVar3, xnVar.I, xnVar.x(), l, k, (mv<?>) this.M, executor));
        return nvVar3;
    }

    @DexIgnore
    public final pv a(Object obj, cw<TranscodeType> cwVar, rv<TranscodeType> rvVar, mv<?> mvVar, RequestCoordinator requestCoordinator, zn<?, ? super TranscodeType> znVar, Priority priority, int i, int i2, Executor executor) {
        Context context = this.E;
        un unVar = this.H;
        return SingleRequest.a(context, unVar, obj, this.J, this.G, mvVar, i, i2, priority, cwVar, rvVar, this.K, requestCoordinator, unVar.d(), znVar.a(), executor);
    }
}
