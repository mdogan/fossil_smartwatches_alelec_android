package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hy1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ Intent f;
    @DexIgnore
    public /* final */ /* synthetic */ fy1 g;

    @DexIgnore
    public hy1(fy1 fy1, Intent intent, Intent intent2) {
        this.g = fy1;
        this.e = intent;
        this.f = intent2;
    }

    @DexIgnore
    public final void run() {
        this.g.d(this.e);
        this.g.a(this.f);
    }
}
