package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ep3 implements Factory<SplashPresenter> {
    @DexIgnore
    public static SplashPresenter a(bp3 bp3, UserRepository userRepository, MigrationHelper migrationHelper, fn2 fn2) {
        return new SplashPresenter(bp3, userRepository, migrationHelper, fn2);
    }
}
