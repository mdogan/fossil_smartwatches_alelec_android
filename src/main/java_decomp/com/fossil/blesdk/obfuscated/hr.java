package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hr<Data> implements tr<byte[], Data> {
    @DexIgnore
    public /* final */ b<Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<byte[], ByteBuffer> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hr$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.hr$a$a  reason: collision with other inner class name */
        public class C0017a implements b<ByteBuffer> {
            @DexIgnore
            public C0017a(a aVar) {
            }

            @DexIgnore
            public Class<ByteBuffer> getDataClass() {
                return ByteBuffer.class;
            }

            @DexIgnore
            public ByteBuffer a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }
        }

        @DexIgnore
        public tr<byte[], ByteBuffer> a(xr xrVar) {
            return new hr(new C0017a(this));
        }
    }

    @DexIgnore
    public interface b<Data> {
        @DexIgnore
        Data a(byte[] bArr);

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<Data> implements to<Data> {
        @DexIgnore
        public /* final */ byte[] e;
        @DexIgnore
        public /* final */ b<Data> f;

        @DexIgnore
        public c(byte[] bArr, b<Data> bVar) {
            this.e = bArr;
            this.f = bVar;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super Data> aVar) {
            aVar.a(this.f.a(this.e));
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.f.getDataClass();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements ur<byte[], InputStream> {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements b<InputStream> {
            @DexIgnore
            public a(d dVar) {
            }

            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }
        }

        @DexIgnore
        public tr<byte[], InputStream> a(xr xrVar) {
            return new hr(new a(this));
        }
    }

    @DexIgnore
    public hr(b<Data> bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public boolean a(byte[] bArr) {
        return true;
    }

    @DexIgnore
    public tr.a<Data> a(byte[] bArr, int i, int i2, mo moVar) {
        return new tr.a<>(new kw(bArr), new c(bArr, this.a));
    }
}
