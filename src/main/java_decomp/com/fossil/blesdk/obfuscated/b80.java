package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b80 extends q70 {
    @DexIgnore
    public ConnectionParameters L; // = new ConnectionParameters(0, 0, 0);
    @DexIgnore
    public long M; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public ConnectionParametersSet N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b80(ConnectionParametersSet connectionParametersSet, Peripheral peripheral) {
        super(DeviceConfigOperationCode.REQUEST_CONNECTION_PRIORITY, RequestId.SET_CONNECTION_PARAMS, peripheral, 0, 8, (rd4) null);
        wd4.b(connectionParametersSet, "connectionParametersSet");
        wd4.b(peripheral, "peripheral");
        this.N = connectionParametersSet;
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.N.getMinimumInterval$blesdk_productionRelease()).putShort((short) this.N.getMaximumInterval$blesdk_productionRelease()).putShort((short) this.N.getLatency$blesdk_productionRelease()).putShort((short) this.N.getTimeout$blesdk_productionRelease()).array();
        wd4.a((Object) array, "ByteBuffer.allocate(8).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final ConnectionParameters I() {
        return this.L;
    }

    @DexIgnore
    public void a(long j) {
        this.M = j;
    }

    @DexIgnore
    public long m() {
        return this.M;
    }

    @DexIgnore
    public JSONObject t() {
        return n90.a(super.t(), this.N.toJSONObject());
    }

    @DexIgnore
    public JSONObject u() {
        return n90.a(super.u(), this.L.toJSONObject());
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.L = new ConnectionParameters(o90.b(order.getShort(0)), o90.b(order.getShort(2)), o90.b(order.getShort(4)));
        return n90.a(a, this.L.toJSONObject());
    }
}
