package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nc0 extends pc0<Status> {
    @DexIgnore
    public nc0(he0 he0) {
        super(he0);
    }

    @DexIgnore
    public final /* synthetic */ ne0 a(Status status) {
        return status;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar) throws RemoteException {
        jc0 jc0 = (jc0) bVar;
        ((vc0) jc0.x()).b(new oc0(this), jc0.G());
    }
}
