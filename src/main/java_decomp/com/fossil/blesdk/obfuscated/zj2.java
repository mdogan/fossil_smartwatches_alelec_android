package com.fossil.blesdk.obfuscated;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zj2 implements tr<ak2, InputStream> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements to<InputStream> {
        @DexIgnore
        public volatile boolean e;
        @DexIgnore
        public /* final */ ak2 f;

        @DexIgnore
        public a(zj2 zj2, ak2 ak2) {
            wd4.b(ak2, "mAppIconModel");
            this.f = ak2;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super InputStream> aVar) {
            wd4.b(priority, "priority");
            wd4.b(aVar, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.W.c().getPackageManager().getApplicationIcon(this.f.a().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap a = wr3.a(applicationIcon);
                if (a != null) {
                    a.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                aVar.a(this.e ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e2) {
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
            this.e = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ur<ak2, InputStream> {
        @DexIgnore
        public zj2 a(xr xrVar) {
            wd4.b(xrVar, "multiFactory");
            return new zj2();
        }
    }

    @DexIgnore
    public boolean a(ak2 ak2) {
        wd4.b(ak2, "appIconModel");
        return true;
    }

    @DexIgnore
    public tr.a<InputStream> a(ak2 ak2, int i, int i2, mo moVar) {
        wd4.b(ak2, "appIconModel");
        wd4.b(moVar, "options");
        return new tr.a<>(ak2, new a(this, ak2));
    }
}
