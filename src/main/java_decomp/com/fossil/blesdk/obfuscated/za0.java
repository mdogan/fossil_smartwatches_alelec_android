package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class za0 {
    @DexIgnore
    public static final SharedPreferences a(SharedPreferenceFileName sharedPreferenceFileName) {
        wd4.b(sharedPreferenceFileName, "fileName");
        return bb0.a.a(sharedPreferenceFileName.name());
    }
}
