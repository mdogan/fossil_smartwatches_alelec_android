package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hx1 {
    @DexIgnore
    public static hx1 e;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public jx1 c; // = new jx1(this);
    @DexIgnore
    public int d; // = 1;

    @DexIgnore
    public hx1(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static synchronized hx1 a(Context context) {
        hx1 hx1;
        synchronized (hx1.class) {
            if (e == null) {
                e = new hx1(context, ez0.a().a(1, new vm0("MessengerIpcClient"), 9));
            }
            hx1 = e;
        }
        return hx1;
    }

    @DexIgnore
    public final xn1<Bundle> b(int i, Bundle bundle) {
        return a(new rx1(a(), 1, bundle));
    }

    @DexIgnore
    public final xn1<Void> a(int i, Bundle bundle) {
        return a(new px1(a(), 2, bundle));
    }

    @DexIgnore
    public final synchronized <T> xn1<T> a(qx1<T> qx1) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(qx1);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.c.a((qx1) qx1)) {
            this.c = new jx1(this);
            this.c.a((qx1) qx1);
        }
        return qx1.b.a();
    }

    @DexIgnore
    public final synchronized int a() {
        int i;
        i = this.d;
        this.d = i + 1;
        return i;
    }
}
