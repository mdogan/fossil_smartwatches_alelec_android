package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lo3 implements Factory<BaseActivity> {
    @DexIgnore
    public static BaseActivity a(ko3 ko3) {
        BaseActivity a = ko3.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
