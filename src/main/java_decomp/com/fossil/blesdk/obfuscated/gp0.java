package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.fitness.data.MapValue;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gp0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gp0> CREATOR; // = new jp0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public float g;
    @DexIgnore
    public String h;
    @DexIgnore
    public Map<String, MapValue> i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public float[] k;
    @DexIgnore
    public byte[] l;

    @DexIgnore
    public gp0(int i2) {
        this(i2, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, (Bundle) null, (int[]) null, (float[]) null, (byte[]) null);
    }

    @DexIgnore
    public final float H() {
        ck0.b(this.e == 2, "Value is not in float format");
        return this.g;
    }

    @DexIgnore
    public final int I() {
        boolean z = true;
        if (this.e != 1) {
            z = false;
        }
        ck0.b(z, "Value is not in int format");
        return Float.floatToRawIntBits(this.g);
    }

    @DexIgnore
    public final int J() {
        return this.e;
    }

    @DexIgnore
    public final boolean K() {
        return this.f;
    }

    @DexIgnore
    public final void a(float f2) {
        ck0.b(this.e == 2, "Attempting to set an float value to a field that is not in FLOAT format.  Please check the data type definition and use the right format.");
        this.f = true;
        this.g = f2;
    }

    @DexIgnore
    public final void e(String str) {
        f(e21.a(str));
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gp0)) {
            return false;
        }
        gp0 gp0 = (gp0) obj;
        int i2 = this.e;
        if (i2 == gp0.e && this.f == gp0.f) {
            switch (i2) {
                case 1:
                    if (I() == gp0.I()) {
                        return true;
                    }
                    break;
                case 2:
                    return this.g == gp0.g;
                case 3:
                    return ak0.a(this.h, gp0.h);
                case 4:
                    return ak0.a(this.i, gp0.i);
                case 5:
                    return Arrays.equals(this.j, gp0.j);
                case 6:
                    return Arrays.equals(this.k, gp0.k);
                case 7:
                    return Arrays.equals(this.l, gp0.l);
                default:
                    if (this.g == gp0.g) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    @DexIgnore
    public final void f(int i2) {
        ck0.b(this.e == 1, "Attempting to set an int value to a field that is not in INT32 format.  Please check the data type definition and use the right format.");
        this.f = true;
        this.g = Float.intBitsToFloat(i2);
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(Float.valueOf(this.g), this.h, this.i, this.j, this.k, this.l);
    }

    @DexIgnore
    public final String toString() {
        if (!this.f) {
            return "unset";
        }
        switch (this.e) {
            case 1:
                return Integer.toString(I());
            case 2:
                return Float.toString(this.g);
            case 3:
                return this.h;
            case 4:
                return new TreeMap(this.i).toString();
            case 5:
                return Arrays.toString(this.j);
            case 6:
                return Arrays.toString(this.k);
            case 7:
                byte[] bArr = this.l;
                return om0.a(bArr, 0, bArr.length, false);
            default:
                return "unknown";
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        Bundle bundle;
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, J());
        lk0.a(parcel, 2, K());
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 4, this.h, false);
        Map<String, MapValue> map = this.i;
        if (map == null) {
            bundle = null;
        } else {
            Bundle bundle2 = new Bundle(map.size());
            for (Map.Entry next : this.i.entrySet()) {
                bundle2.putParcelable((String) next.getKey(), (Parcelable) next.getValue());
            }
            bundle = bundle2;
        }
        lk0.a(parcel, 5, bundle, false);
        lk0.a(parcel, 6, this.j, false);
        lk0.a(parcel, 7, this.k, false);
        lk0.a(parcel, 8, this.l, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public gp0(int i2, boolean z, float f2, String str, Bundle bundle, int[] iArr, float[] fArr, byte[] bArr) {
        g4 g4Var;
        this.e = i2;
        this.f = z;
        this.g = f2;
        this.h = str;
        if (bundle == null) {
            g4Var = null;
        } else {
            bundle.setClassLoader(MapValue.class.getClassLoader());
            g4Var = new g4(bundle.size());
            for (String str2 : bundle.keySet()) {
                g4Var.put(str2, (MapValue) bundle.getParcelable(str2));
            }
        }
        this.i = g4Var;
        this.j = iArr;
        this.k = fArr;
        this.l = bArr;
    }
}
