package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class di0 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ vd0 b;

    @DexIgnore
    public di0(vd0 vd0, int i) {
        ck0.a(vd0);
        this.b = vd0;
        this.a = i;
    }

    @DexIgnore
    public final vd0 a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }
}
