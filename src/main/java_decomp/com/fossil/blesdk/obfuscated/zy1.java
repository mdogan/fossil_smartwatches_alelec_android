package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zy1 implements qn1<Bundle, String> {
    @DexIgnore
    public /* final */ /* synthetic */ wy1 a;

    @DexIgnore
    public zy1(wy1 wy1) {
        this.a = wy1;
    }

    @DexIgnore
    public final /* synthetic */ Object then(xn1 xn1) throws Exception {
        return wy1.a((Bundle) xn1.a(IOException.class));
    }
}
