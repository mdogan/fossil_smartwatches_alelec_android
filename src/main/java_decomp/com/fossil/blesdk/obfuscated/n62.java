package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n62 extends RecyclerView.g<c> {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public List<Alarm> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Alarm alarm);

        @DexIgnore
        void b(Alarm alarm);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ dh2 a;
        @DexIgnore
        public /* final */ /* synthetic */ n62 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    b b = this.e.b.b;
                    if (b != null) {
                        b.b((Alarm) this.e.b.a.get(adapterPosition));
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public b(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    b b = this.e.b.b;
                    if (b != null) {
                        b.a((Alarm) this.e.b.a.get(adapterPosition));
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(n62 n62, dh2 dh2) {
            super(dh2.d());
            wd4.b(dh2, "binding");
            this.b = n62;
            this.a = dh2;
            this.a.q.setOnClickListener(new a(this));
            this.a.t.setOnClickListener(new b(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Alarm alarm, boolean z) {
            Alarm alarm2 = alarm;
            boolean z2 = z;
            wd4.b(alarm2, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            FLogger.INSTANCE.getLocal().d(n62.c, "Alarm: " + alarm2 + ", isSingleAlarm=" + z2);
            int totalMinutes = alarm.getTotalMinutes();
            int hour = alarm.getHour();
            int minute = alarm.getMinute();
            View d = this.a.d();
            wd4.a((Object) d, "binding.root");
            if (DateFormat.is24HourFormat(d.getContext())) {
                FlexibleTextView flexibleTextView = this.a.s;
                wd4.a((Object) flexibleTextView, "binding.ftvTime");
                StringBuilder sb = new StringBuilder();
                be4 be4 = be4.a;
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                Object[] objArr = {Integer.valueOf(hour)};
                String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                sb.append(format);
                sb.append(':');
                be4 be42 = be4.a;
                Locale locale2 = Locale.US;
                wd4.a((Object) locale2, "Locale.US");
                Object[] objArr2 = {Integer.valueOf(minute)};
                String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
                wd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                sb.append(format2);
                flexibleTextView.setText(sb.toString());
            } else {
                int i = 12;
                if (totalMinutes < 720) {
                    if (hour != 0) {
                        i = hour;
                    }
                    FlexibleTextView flexibleTextView2 = this.a.s;
                    wd4.a((Object) flexibleTextView2, "binding.ftvTime");
                    ml2 ml2 = ml2.b;
                    StringBuilder sb2 = new StringBuilder();
                    be4 be43 = be4.a;
                    Locale locale3 = Locale.US;
                    wd4.a((Object) locale3, "Locale.US");
                    Object[] objArr3 = {Integer.valueOf(i)};
                    String format3 = String.format(locale3, "%02d", Arrays.copyOf(objArr3, objArr3.length));
                    wd4.a((Object) format3, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format3);
                    sb2.append(':');
                    be4 be44 = be4.a;
                    Locale locale4 = Locale.US;
                    wd4.a((Object) locale4, "Locale.US");
                    Object[] objArr4 = {Integer.valueOf(minute)};
                    String format4 = String.format(locale4, "%02d", Arrays.copyOf(objArr4, objArr4.length));
                    wd4.a((Object) format4, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format4);
                    sb2.append(' ');
                    String sb3 = sb2.toString();
                    View d2 = this.a.d();
                    wd4.a((Object) d2, "binding.root");
                    String a2 = tm2.a(d2.getContext(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am);
                    wd4.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    flexibleTextView2.setText(ml2.a(sb3, a2, 1.0f));
                } else {
                    if (hour > 12) {
                        i = hour - 12;
                    }
                    FlexibleTextView flexibleTextView3 = this.a.s;
                    wd4.a((Object) flexibleTextView3, "binding.ftvTime");
                    ml2 ml22 = ml2.b;
                    StringBuilder sb4 = new StringBuilder();
                    be4 be45 = be4.a;
                    Locale locale5 = Locale.US;
                    wd4.a((Object) locale5, "Locale.US");
                    Object[] objArr5 = {Integer.valueOf(i)};
                    String format5 = String.format(locale5, "%02d", Arrays.copyOf(objArr5, objArr5.length));
                    wd4.a((Object) format5, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format5);
                    sb4.append(':');
                    be4 be46 = be4.a;
                    Locale locale6 = Locale.US;
                    wd4.a((Object) locale6, "Locale.US");
                    Object[] objArr6 = {Integer.valueOf(minute)};
                    String format6 = String.format(locale6, "%02d", Arrays.copyOf(objArr6, objArr6.length));
                    wd4.a((Object) format6, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format6);
                    sb4.append(' ');
                    String sb5 = sb4.toString();
                    View d3 = this.a.d();
                    wd4.a((Object) d3, "binding.root");
                    String a3 = tm2.a(d3.getContext(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                    flexibleTextView3.setText(ml22.a(sb5, a3, 1.0f));
                }
            }
            int[] days = alarm.getDays();
            int length = days != null ? days.length : 0;
            StringBuilder sb6 = new StringBuilder("");
            if (length > 0 && alarm.isRepeated()) {
                if (length == 7) {
                    sb6.append(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_Main_Alerts_Label__EveryDay));
                    wd4.a((Object) sb6, "strDays.append(LanguageH\u2026_Alerts_Label__EveryDay))");
                } else {
                    if (length == 2) {
                        n62 n62 = this.b;
                        if (days == null) {
                            wd4.a();
                            throw null;
                        } else if (n62.a(days)) {
                            sb6.append(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_Main_Alerts_Label__EveryWeekend));
                            wd4.a((Object) sb6, "strDays.append(LanguageH\u2026rts_Label__EveryWeekend))");
                        }
                    }
                    if (days != null) {
                        kb4.a(days);
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = 1;
                            while (true) {
                                if (i3 > 7) {
                                    break;
                                } else if (i3 == days[i2]) {
                                    sb6.append(ml2.b.a(i3));
                                    if (i2 < length - 1) {
                                        sb6.append(", ");
                                    }
                                } else {
                                    i3++;
                                }
                            }
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
            FlexibleTextView flexibleTextView4 = this.a.r;
            wd4.a((Object) flexibleTextView4, "binding.ftvRepeatedDays");
            flexibleTextView4.setText(sb6.toString());
            SwitchCompat switchCompat = this.a.t;
            wd4.a((Object) switchCompat, "binding.swEnabled");
            switchCompat.setChecked(alarm.isActive());
            if (z2) {
                CardView cardView = this.a.q;
                wd4.a((Object) cardView, "binding.cvRoot");
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                gl2 b2 = gl2.b();
                wd4.a((Object) b2, "MeasureHelper.getInstance()");
                layoutParams.width = (int) (((float) b2.a()) - us3.a(32.0f));
                return;
            }
            CardView cardView2 = this.a.q;
            wd4.a((Object) cardView2, "binding.cvRoot");
            ViewGroup.LayoutParams layoutParams2 = cardView2.getLayoutParams();
            gl2 b3 = gl2.b();
            wd4.a((Object) b3, "MeasureHelper.getInstance()");
            layoutParams2.width = (int) ((((float) b3.a()) - us3.a(32.0f)) / 2.2f);
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = n62.class.getSimpleName();
        wd4.a((Object) simpleName, "AlarmsAdapter::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        dh2 a2 = dh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wd4.a((Object) a2, "ItemAlarmBinding.inflate\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wd4.b(cVar, "holder");
        Alarm alarm = this.a.get(i);
        boolean z = true;
        if (this.a.size() != 1) {
            z = false;
        }
        cVar.a(alarm, z);
    }

    @DexIgnore
    public final void a(List<Alarm> list) {
        wd4.b(list, "alarms");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        if (iArr.length != 2) {
            return false;
        }
        if ((iArr[0] == 1 && iArr[1] == 7) || (iArr[0] == 7 && iArr[1] == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "listener");
        this.b = bVar;
    }
}
