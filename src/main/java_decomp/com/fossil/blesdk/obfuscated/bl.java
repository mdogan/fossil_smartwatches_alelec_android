package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bl implements al {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ mf b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf<zk> {
        @DexIgnore
        public a(bl blVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(lg lgVar, zk zkVar) {
            String str = zkVar.a;
            if (str == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, str);
            }
            String str2 = zkVar.b;
            if (str2 == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency`(`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public bl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @DexIgnore
    public void a(zk zkVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(zkVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public boolean b(String str) {
        vf b2 = vf.b("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z = false;
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public boolean c(String str) {
        vf b2 = vf.b("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z = false;
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public List<String> a(String str) {
        vf b2 = vf.b("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
