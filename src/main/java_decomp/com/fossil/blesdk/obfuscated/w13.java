package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w13 implements MembersInjector<v13> {
    @DexIgnore
    public static void a(v13 v13, ComplicationsPresenter complicationsPresenter) {
        v13.t = complicationsPresenter;
    }

    @DexIgnore
    public static void a(v13 v13, WatchAppsPresenter watchAppsPresenter) {
        v13.u = watchAppsPresenter;
    }

    @DexIgnore
    public static void a(v13 v13, CustomizeThemePresenter customizeThemePresenter) {
        v13.v = customizeThemePresenter;
    }

    @DexIgnore
    public static void a(v13 v13, k42 k42) {
        v13.w = k42;
    }
}
