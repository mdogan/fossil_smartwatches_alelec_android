package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vc0 extends IInterface {
    @DexIgnore
    void a(tc0 tc0, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    @DexIgnore
    void b(tc0 tc0, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
