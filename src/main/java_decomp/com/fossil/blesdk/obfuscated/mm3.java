package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.lm3;
import com.fossil.blesdk.obfuscated.om3;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mm3 extends bs2 implements hn3, xs3.g, xs3.h {
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public en3 k;
    @DexIgnore
    public zm3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = mm3.class.getSimpleName();
            wd4.a((Object) simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final mm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            mm3 mm3 = new mm3();
            mm3.setArguments(bundle);
            return mm3;
        }
    }

    @DexIgnore
    public void A() {
        zm3 zm3 = this.l;
        if (zm3 == null || !(zm3 instanceof om3)) {
            om3.a aVar = om3.n;
            en3 en3 = this.k;
            if (en3 != null) {
                this.l = aVar.a(en3.i());
                zm3 zm32 = this.l;
                if (zm32 != null) {
                    a(zm32, "", R.id.content);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
        zm3 zm33 = this.l;
        if (zm33 != null) {
            en3 en32 = this.k;
            if (en32 != null) {
                zm33.a(en32);
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void O() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void Q(String str) {
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(String str, boolean z) {
        wd4.b(str, "serial");
        zm3 zm3 = this.l;
        if (zm3 instanceof pm3) {
            ((pm3) zm3).v0();
            return;
        }
        this.l = pm3.r.a(str, z, 1);
        zm3 zm32 = this.l;
        if (zm32 != null) {
            a(zm32, "", R.id.content);
            zm3 zm33 = this.l;
            if (zm33 != null) {
                en3 en3 = this.k;
                if (en3 != null) {
                    zm33.a(en3);
                } else {
                    wd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void c(int i, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = n.a();
        local.d(a2, "showDeviceConnectFail() - errorCode = " + i);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            n();
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                aVar.a(activity, str);
                return;
            }
            wd4.a();
            throw null;
        }
        n();
        TroubleshootingActivity.a aVar2 = TroubleshootingActivity.C;
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            wd4.a((Object) activity2, "activity!!");
            aVar2.a(activity2);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void k(List<Pair<ShineDevice, String>> list) {
        wd4.b(list, "listDevices");
        zm3 zm3 = this.l;
        if (zm3 == null || !(zm3 instanceof lm3)) {
            lm3.a aVar = lm3.q;
            en3 en3 = this.k;
            if (en3 != null) {
                lm3 a2 = aVar.a(en3.i());
                a2.h(list);
                this.l = a2;
                zm3 zm32 = this.l;
                if (zm32 != null) {
                    a(zm32, "", R.id.content);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
        zm3 zm33 = this.l;
        if (zm33 != null) {
            en3 en32 = this.k;
            if (en32 != null) {
                zm33.a(en32);
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void l(String str) {
        wd4.b(str, "serial");
        if (isActive()) {
            es3 es3 = es3.c;
            String e = PortfolioApp.W.c().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.f(e, childFragmentManager);
        }
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    wd4.a((Object) activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_pairing, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        en3 en3 = this.k;
        if (en3 != null) {
            en3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        en3 en3 = this.k;
        if (en3 != null) {
            en3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        en3 en3 = this.k;
        if (en3 != null) {
            if (en3.h() != null) {
                en3 en32 = this.k;
                if (en32 != null) {
                    ShineDevice h = en32.h();
                    if (h != null) {
                        bundle.putParcelable("PAIRING_DEVICE", h);
                    }
                } else {
                    wd4.d("mPairingPresenter");
                    throw null;
                }
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            en3 en3 = this.k;
            if (en3 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    en3.a((ShineDevice) parcelable);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            en3 en32 = this.k;
            if (en32 != null) {
                en32.b(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
        this.l = (zm3) getChildFragmentManager().a((int) R.id.content);
        if (this.l == null) {
            om3.a aVar = om3.n;
            en3 en33 = this.k;
            if (en33 != null) {
                this.l = aVar.a(en33.i());
                zm3 zm3 = this.l;
                if (zm3 != null) {
                    a(zm3, "", R.id.content);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
        zm3 zm32 = this.l;
        if (zm32 != null) {
            en3 en34 = this.k;
            if (en34 != null) {
                zm32.a(en34);
                en3 en35 = this.k;
                if (en35 != null) {
                    zm3 zm33 = this.l;
                    en35.a((zm33 instanceof om3) || (zm33 instanceof lm3));
                    R("scan_device_view");
                    return;
                }
                wd4.d("mPairingPresenter");
                throw null;
            }
            wd4.d("mPairingPresenter");
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void s(String str) {
        wd4.b(str, "serial");
        if (isActive()) {
            es3 es3 = es3.c;
            String e = PortfolioApp.W.c().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.e(e, childFragmentManager);
        }
    }

    @DexIgnore
    public void t() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.C;
            wd4.a((Object) activity, "it");
            en3 en3 = this.k;
            if (en3 != null) {
                aVar.a(activity, en3.i());
            } else {
                wd4.d("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void y() {
        if (isActive()) {
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action_with_title);
            fVar.a((int) R.id.tv_title, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
            fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Text__PleaseCheckYourInternetConnectionAnd));
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionwithActions_CTA__TryAgain));
            fVar.a((int) R.id.tv_ok);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public void a(en3 en3) {
        wd4.b(en3, "presenter");
        this.k = en3;
    }

    @DexIgnore
    public void a(String str, boolean z) {
        wd4.b(str, "serial");
        zm3 zm3 = this.l;
        if (zm3 instanceof pm3) {
            ((pm3) zm3).n0();
            return;
        }
        this.l = pm3.r.a(str, z, 0);
        zm3 zm32 = this.l;
        if (zm32 != null) {
            a(zm32, "", R.id.content);
            zm3 zm33 = this.l;
            if (zm33 != null) {
                en3 en3 = this.k;
                if (en3 != null) {
                    zm33.a(en3);
                } else {
                    wd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void h(List<Pair<ShineDevice, String>> list) {
        wd4.b(list, "deviceList");
        zm3 zm3 = this.l;
        if (zm3 != null && (zm3 instanceof lm3)) {
            ((lm3) zm3).h(list);
        }
    }

    @DexIgnore
    public void a(String str, boolean z, boolean z2) {
        wd4.b(str, "serial");
        zm3 zm3 = this.l;
        if (zm3 instanceof pm3) {
            ((pm3) zm3).b(z2);
            return;
        }
        this.l = pm3.r.a(str, z, z2 ? 2 : 3);
        zm3 zm32 = this.l;
        if (zm32 != null) {
            a(zm32, "", R.id.content);
            zm3 zm33 = this.l;
            if (zm33 != null) {
                en3 en3 = this.k;
                if (en3 != null) {
                    zm33.a(en3);
                } else {
                    wd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001c, code lost:
        if (r6.equals("SERVER_ERROR") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f2, code lost:
        if (r6.equals("SERVER_MAINTENANCE") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f4, code lost:
        n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f8, code lost:
        r0 = getActivity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00fc, code lost:
        if (r0 == null) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00fe, code lost:
        ((com.portfolio.platform.ui.BaseActivity) r0).a(r6, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0109, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        switch (str.hashCode()) {
            case -1636680713:
                break;
            case -879828873:
                if (str.equals("NETWORK_ERROR")) {
                    if (i == R.id.tv_ok) {
                        n();
                        return;
                    }
                    return;
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    if (i == R.id.tv_ok) {
                        en3 en3 = this.k;
                        if (en3 != null) {
                            en3.l();
                            return;
                        } else {
                            wd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i == R.id.tv_cancel) {
                        en3 en32 = this.k;
                        if (en32 != null) {
                            en32.b(stringExtra);
                            return;
                        } else {
                            wd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i == R.id.tv_ok) {
                        en3 en33 = this.k;
                        if (en33 != null) {
                            en33.a(stringExtra);
                            return;
                        } else {
                            wd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 927511079:
                if (str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                    if (i == R.id.fb_try_again) {
                        en3 en34 = this.k;
                        if (en34 != null) {
                            en34.m();
                            return;
                        } else {
                            wd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i != R.id.ftv_contact_cs) {
                        if (i == R.id.iv_close) {
                            n();
                            return;
                        }
                        return;
                    } else if (getActivity() != null) {
                        HelpActivity.a aVar = HelpActivity.C;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wd4.a();
                        throw null;
                    } else {
                        return;
                    }
                }
                break;
            case 1008390942:
                if (str.equals("NO_INTERNET_CONNECTION")) {
                    if (i == R.id.ftv_go_to_setting) {
                        FragmentActivity activity2 = getActivity();
                        if (activity2 != null) {
                            ((BaseActivity) activity2).m();
                            n();
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                    } else if (i == R.id.tv_ok) {
                        n();
                        return;
                    } else {
                        return;
                    }
                }
                break;
            case 1178575340:
                break;
        }
    }

    @DexIgnore
    public void a(int i, String str, String str2) {
        wd4.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = n.a();
        local.d(a2, "showDeviceConnectFail due to network - errorCode = " + i);
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, str2, childFragmentManager);
        }
    }
}
