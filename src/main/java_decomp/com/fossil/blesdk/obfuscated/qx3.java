package com.fossil.blesdk.obfuscated;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qx3 {
    @DexIgnore
    public static final qx3 a = new a();
    @DexIgnore
    public static final qx3 b = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements qx3 {
        @DexIgnore
        public void a(jx3 jx3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements qx3 {
        @DexIgnore
        public void a(jx3 jx3) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + jx3 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(jx3 jx3);
}
