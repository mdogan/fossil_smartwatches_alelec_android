package com.fossil.blesdk.obfuscated;

import com.android.volley.ParseError;
import com.fossil.blesdk.obfuscated.vm;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ln extends mn<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ln(int i, String str, JSONObject jSONObject, vm.b<JSONObject> bVar, vm.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    public vm<JSONObject> parseNetworkResponse(tm tmVar) {
        try {
            return vm.a(new JSONObject(new String(tmVar.b, fn.a(tmVar.c, mn.PROTOCOL_CHARSET))), fn.a(tmVar));
        } catch (UnsupportedEncodingException e) {
            return vm.a(new ParseError((Throwable) e));
        } catch (JSONException e2) {
            return vm.a(new ParseError((Throwable) e2));
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ln(String str, JSONObject jSONObject, vm.b<JSONObject> bVar, vm.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }
}
