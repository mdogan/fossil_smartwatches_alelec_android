package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class es2 extends as2 implements pk3 {
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ok3 j;
    @DexIgnore
    public o62 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public ur3<pb2> m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final es2 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            es2 es2 = new es2();
            es2.setArguments(bundle);
            return es2;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public b(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            es2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ ok3 a(es2 es2) {
        ok3 ok3 = es2.j;
        if (ok3 != null) {
            return ok3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void g() {
        ur3<pb2> ur3 = this.m;
        if (ur3 != null) {
            pb2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.s;
                if (dashBar != null) {
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.a(dashBar, this.l, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.m = new ur3<>(this, (pb2) ra.a(layoutInflater, R.layout.fragment_explore_watch, viewGroup, false, O0()));
        ur3<pb2> ur3 = this.m;
        if (ur3 != null) {
            pb2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        o62 o62 = this.k;
        if (o62 != null) {
            o62.b();
            ok3 ok3 = this.j;
            if (ok3 != null) {
                ok3.g();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ok3 ok3 = this.j;
        if (ok3 != null) {
            ok3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.k = new o62();
        ur3<pb2> ur3 = this.m;
        if (ur3 != null) {
            pb2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerViewPager recyclerViewPager = a2.t;
                wd4.a((Object) recyclerViewPager, "binding.vpExplore");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.t;
                wd4.a((Object) recyclerViewPager2, "binding.vpExplore");
                o62 o62 = this.k;
                if (o62 != null) {
                    recyclerViewPager2.setAdapter(o62);
                    a2.r.a((RecyclerView) a2.t, 0);
                    a2.r.setOnPageChangeListener(new c());
                    a2.q.setOnClickListener(new b(this));
                } else {
                    wd4.d("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.l = arguments.getBoolean("IS_ONBOARDING_FLOW");
                ok3 ok3 = this.j;
                if (ok3 != null) {
                    ok3.a(this.l);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void s0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void u(List<? extends Explore> list) {
        wd4.b(list, "data");
        o62 o62 = this.k;
        if (o62 != null) {
            o62.a(list);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ok3 ok3) {
        wd4.b(ok3, "presenter");
        this.j = ok3;
    }
}
