package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.BitmapFactory;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x74 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public x74(String str, int i, int i2, int i3) {
        this.a = str;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static x74 a(Context context, String str) {
        if (str != null) {
            try {
                int d2 = CommonUtils.d(context);
                z44 g = r44.g();
                g.d("Fabric", "App icon resource ID is " + d2);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), d2, options);
                return new x74(str, d2, options.outWidth, options.outHeight);
            } catch (Exception e) {
                r44.g().e("Fabric", "Failed to load icon", e);
            }
        }
        return null;
    }
}
