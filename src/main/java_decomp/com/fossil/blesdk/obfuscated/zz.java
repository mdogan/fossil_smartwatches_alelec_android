package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zz implements f00 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ f00 b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public String d;

    @DexIgnore
    public zz(Context context, f00 f00) {
        this.a = context;
        this.b = f00;
    }

    @DexIgnore
    public String a() {
        if (!this.c) {
            this.d = CommonUtils.o(this.a);
            this.c = true;
        }
        String str = this.d;
        if (str != null) {
            return str;
        }
        f00 f00 = this.b;
        if (f00 != null) {
            return f00.a();
        }
        return null;
    }
}
