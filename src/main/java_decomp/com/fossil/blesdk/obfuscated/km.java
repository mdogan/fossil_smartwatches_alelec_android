package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class km<TResult> {
    @DexIgnore
    public /* final */ jm<TResult> a; // = new jm<>();

    @DexIgnore
    public jm<TResult> a() {
        return this.a;
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.a(tresult);
    }

    @DexIgnore
    public boolean c() {
        return this.a.g();
    }

    @DexIgnore
    public void a(TResult tresult) {
        if (!b(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.a(exc);
    }

    @DexIgnore
    public void b() {
        if (!c()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    @DexIgnore
    public void a(Exception exc) {
        if (!b(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }
}
