package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xz1 extends JsonElement {
    @DexIgnore
    public static /* final */ xz1 a; // = new xz1();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof xz1);
    }

    @DexIgnore
    public int hashCode() {
        return xz1.class.hashCode();
    }
}
