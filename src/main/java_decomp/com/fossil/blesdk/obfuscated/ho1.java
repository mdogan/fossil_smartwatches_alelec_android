package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ho1<TResult> implements ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public rn1 c;

    @DexIgnore
    public ho1(Executor executor, rn1 rn1) {
        this.a = executor;
        this.c = rn1;
    }

    @DexIgnore
    public final void onComplete(xn1 xn1) {
        if (xn1.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new io1(this));
                }
            }
        }
    }
}
