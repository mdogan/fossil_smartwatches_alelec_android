package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wo4 extends jp4, WritableByteChannel {
    @DexIgnore
    long a(kp4 kp4) throws IOException;

    @DexIgnore
    vo4 a();

    @DexIgnore
    wo4 a(long j) throws IOException;

    @DexIgnore
    wo4 a(String str) throws IOException;

    @DexIgnore
    wo4 a(ByteString byteString) throws IOException;

    @DexIgnore
    wo4 b(long j) throws IOException;

    @DexIgnore
    wo4 c() throws IOException;

    @DexIgnore
    wo4 d() throws IOException;

    @DexIgnore
    OutputStream e();

    @DexIgnore
    void flush() throws IOException;

    @DexIgnore
    wo4 write(byte[] bArr) throws IOException;

    @DexIgnore
    wo4 write(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    wo4 writeByte(int i) throws IOException;

    @DexIgnore
    wo4 writeInt(int i) throws IOException;

    @DexIgnore
    wo4 writeShort(int i) throws IOException;
}
