package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ff1 extends b51 implements ke1 {
    @DexIgnore
    public ff1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    public final void a(tn0 tn0) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        b(4, o);
    }

    @DexIgnore
    public final void b(tn0 tn0) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        b(5, o);
    }

    @DexIgnore
    public final void clear() throws RemoteException {
        b(14, o());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final pe1 m() throws RemoteException {
        pe1 pe1;
        Parcel a = a(25, o());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            pe1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            if (queryLocalInterface instanceof pe1) {
                pe1 = queryLocalInterface;
            } else {
                pe1 = new af1(readStrongBinder);
            }
        }
        a.recycle();
        return pe1;
    }

    @DexIgnore
    public final h51 a(lf1 lf1) throws RemoteException {
        Parcel o = o();
        d51.a(o, (Parcelable) lf1);
        Parcel a = a(11, o);
        h51 a2 = i51.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
