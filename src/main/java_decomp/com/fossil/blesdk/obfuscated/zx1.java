package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zx1 extends hz0 {
    @DexIgnore
    public /* final */ /* synthetic */ yx1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zx1(yx1 yx1, Looper looper) {
        super(looper);
        this.a = yx1;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        this.a.a(message);
    }
}
