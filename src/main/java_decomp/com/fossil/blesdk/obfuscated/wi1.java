package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wi1 {
    @DexIgnore
    uh1 a();

    @DexIgnore
    vl1 b();

    @DexIgnore
    im0 c();

    @DexIgnore
    ug1 d();

    @DexIgnore
    Context getContext();
}
