package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class js4<T> extends z84<cs4<T>> {
    @DexIgnore
    public /* final */ Call<T> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements j94 {
        @DexIgnore
        public /* final */ Call<?> e;
        @DexIgnore
        public volatile boolean f;

        @DexIgnore
        public a(Call<?> call) {
            this.e = call;
        }

        @DexIgnore
        public boolean a() {
            return this.f;
        }

        @DexIgnore
        public void dispose() {
            this.f = true;
            this.e.cancel();
        }
    }

    @DexIgnore
    public js4(Call<T> call) {
        this.e = call;
    }

    @DexIgnore
    public void b(b94<? super cs4<T>> b94) {
        boolean z;
        Call<T> clone = this.e.clone();
        a aVar = new a(clone);
        b94.onSubscribe(aVar);
        if (!aVar.a()) {
            try {
                cs4<T> r = clone.r();
                if (!aVar.a()) {
                    b94.onNext(r);
                }
                if (!aVar.a()) {
                    try {
                        b94.onComplete();
                    } catch (Throwable th) {
                        th = th;
                        z = true;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                z = false;
                l94.b(th);
                if (z) {
                    ta4.b(th);
                } else if (!aVar.a()) {
                    try {
                        b94.onError(th);
                    } catch (Throwable th3) {
                        l94.b(th3);
                        ta4.b(new CompositeException(th, th3));
                    }
                }
            }
        }
    }
}
