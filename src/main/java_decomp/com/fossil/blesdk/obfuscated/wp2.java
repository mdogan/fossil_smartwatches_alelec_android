package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wp2 implements Factory<DianaNotificationComponent> {
    @DexIgnore
    public /* final */ Provider<fn2> a;
    @DexIgnore
    public /* final */ Provider<DNDSettingsDatabase> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;

    @DexIgnore
    public wp2(Provider<fn2> provider, Provider<DNDSettingsDatabase> provider2, Provider<NotificationSettingsDatabase> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static wp2 a(Provider<fn2> provider, Provider<DNDSettingsDatabase> provider2, Provider<NotificationSettingsDatabase> provider3) {
        return new wp2(provider, provider2, provider3);
    }

    @DexIgnore
    public static DianaNotificationComponent b(Provider<fn2> provider, Provider<DNDSettingsDatabase> provider2, Provider<NotificationSettingsDatabase> provider3) {
        return new DianaNotificationComponent(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public DianaNotificationComponent get() {
        return b(this.a, this.b, this.c);
    }
}
