package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface f9 {
    @DexIgnore
    ColorStateList getSupportBackgroundTintList();

    @DexIgnore
    PorterDuff.Mode getSupportBackgroundTintMode();

    @DexIgnore
    void setSupportBackgroundTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportBackgroundTintMode(PorterDuff.Mode mode);
}
