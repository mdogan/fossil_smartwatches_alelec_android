package com.fossil.blesdk.obfuscated;

import android.os.Process;
import com.fossil.blesdk.obfuscated.wp;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jp {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Map<ko, d> b;
    @DexIgnore
    public /* final */ ReferenceQueue<wp<?>> c;
    @DexIgnore
    public wp.a d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile c f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jp$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.jp$a$a  reason: collision with other inner class name */
        public class C0018a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable e;

            @DexIgnore
            public C0018a(a aVar, Runnable runnable) {
                this.e = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.e.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new C0018a(this, runnable), "glide-active-resources");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            jp.this.a();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WeakReference<wp<?>> {
        @DexIgnore
        public /* final */ ko a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public bq<?> c;

        @DexIgnore
        public d(ko koVar, wp<?> wpVar, ReferenceQueue<? super wp<?>> referenceQueue, boolean z) {
            super(wpVar, referenceQueue);
            bq<?> bqVar;
            uw.a(koVar);
            this.a = koVar;
            if (!wpVar.f() || !z) {
                bqVar = null;
            } else {
                bq<?> e = wpVar.e();
                uw.a(e);
                bqVar = e;
            }
            this.c = bqVar;
            this.b = wpVar.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public jp(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new a()));
    }

    @DexIgnore
    public void a(wp.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.d = aVar;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    @DexIgnore
    public synchronized wp<?> b(ko koVar) {
        d dVar = this.b.get(koVar);
        if (dVar == null) {
            return null;
        }
        wp<?> wpVar = (wp) dVar.get();
        if (wpVar == null) {
            a(dVar);
        }
    }

    @DexIgnore
    public jp(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.a = z;
        executor.execute(new b());
    }

    @DexIgnore
    public synchronized void a(ko koVar, wp<?> wpVar) {
        d put = this.b.put(koVar, new d(koVar, wpVar, this.c, this.a));
        if (put != null) {
            put.a();
        }
    }

    @DexIgnore
    public synchronized void a(ko koVar) {
        d remove = this.b.remove(koVar);
        if (remove != null) {
            remove.a();
        }
    }

    @DexIgnore
    public void a(d dVar) {
        synchronized (this) {
            this.b.remove(dVar.a);
            if (dVar.b) {
                if (dVar.c != null) {
                    this.d.a(dVar.a, new wp(dVar.c, true, false, dVar.a, this.d));
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        while (!this.e) {
            try {
                a((d) this.c.remove());
                c cVar = this.f;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
