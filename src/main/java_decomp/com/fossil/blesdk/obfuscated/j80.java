package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j80 extends m80 {
    @DexIgnore
    public long M; // = 5000;
    @DexIgnore
    public boolean N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j80(short s, Peripheral peripheral) {
        super(s, RequestId.VERIFY_FILE, peripheral, 0, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void a(long j) {
        this.M = j;
    }

    @DexIgnore
    public long m() {
        return this.M;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        if (n().getResultCode() != Request.Result.ResultCode.SUCCESS) {
            c(true);
        } else {
            this.N = true;
        }
        return new JSONObject();
    }

    @DexIgnore
    public void a(e20 e20) {
        wd4.b(e20, "connectionStateChangedNotification");
        if (e20.a() != Peripheral.State.DISCONNECTED) {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.UNKNOWN_ERROR, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
        } else if (e20.b() == 19 || this.N) {
            a(n());
        } else {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.CONNECTION_DROPPED, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
        }
    }
}
