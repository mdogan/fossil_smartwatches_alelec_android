package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e00 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ e00 d;

    @DexIgnore
    public e00(Throwable th, d00 d00) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = d00.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new e00(cause, d00) : null;
    }
}
