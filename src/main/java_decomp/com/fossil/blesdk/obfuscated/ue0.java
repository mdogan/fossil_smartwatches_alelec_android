package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.b;
import com.fossil.blesdk.obfuscated.ne0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ue0<R extends ne0, A extends ee0.b> extends BasePendingResult<R> implements ve0<R> {
    @DexIgnore
    public /* final */ ee0.c<A> q;
    @DexIgnore
    public /* final */ ee0<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ue0(ee0<?> ee0, he0 he0) {
        super(he0);
        ck0.a(he0, (Object) "GoogleApiClient must not be null");
        ck0.a(ee0, (Object) "Api must not be null");
        this.q = ee0.a();
        this.r = ee0;
    }

    @DexIgnore
    public final void a(RemoteException remoteException) {
        c(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    @DexIgnore
    public abstract void a(A a) throws RemoteException;

    @DexIgnore
    public final void b(A a) throws DeadObjectException {
        if (a instanceof hk0) {
            a = ((hk0) a).G();
        }
        try {
            a(a);
        } catch (DeadObjectException e) {
            a((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            a(e2);
        }
    }

    @DexIgnore
    public final void c(Status status) {
        ck0.a(!status.L(), (Object) "Failed result must not be success");
        ne0 a = a(status);
        a(a);
        d(a);
    }

    @DexIgnore
    public void d(R r2) {
    }

    @DexIgnore
    public final ee0<?> h() {
        return this.r;
    }

    @DexIgnore
    public final ee0.c<A> i() {
        return this.q;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.a((ne0) obj);
    }
}
