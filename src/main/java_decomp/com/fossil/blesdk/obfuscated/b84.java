package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b84 {
    @DexIgnore
    c84 a();

    @DexIgnore
    c84 a(SettingsCacheBehavior settingsCacheBehavior);
}
