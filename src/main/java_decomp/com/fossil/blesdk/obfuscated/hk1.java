package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ ll1 f;
    @DexIgnore
    public /* final */ /* synthetic */ sl1 g;
    @DexIgnore
    public /* final */ /* synthetic */ wj1 h;

    @DexIgnore
    public hk1(wj1 wj1, boolean z, ll1 ll1, sl1 sl1) {
        this.h = wj1;
        this.e = z;
        this.f = ll1;
        this.g = sl1;
    }

    @DexIgnore
    public final void run() {
        lg1 d = this.h.d;
        if (d == null) {
            this.h.d().s().a("Discarding data. Failed to set user attribute");
            return;
        }
        this.h.a(d, this.e ? null : this.f, this.g);
        this.h.C();
    }
}
