package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b52 implements Factory<al2> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<i42> c;
    @DexIgnore
    public /* final */ Provider<fn2> d;

    @DexIgnore
    public b52(o42 o42, Provider<Context> provider, Provider<i42> provider2, Provider<fn2> provider3) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static b52 a(o42 o42, Provider<Context> provider, Provider<i42> provider2, Provider<fn2> provider3) {
        return new b52(o42, provider, provider2, provider3);
    }

    @DexIgnore
    public static al2 b(o42 o42, Provider<Context> provider, Provider<i42> provider2, Provider<fn2> provider3) {
        return a(o42, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static al2 a(o42 o42, Context context, i42 i42, fn2 fn2) {
        al2 a2 = o42.a(context, i42, fn2);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public al2 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
