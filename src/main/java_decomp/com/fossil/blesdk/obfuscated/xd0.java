package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ak0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xd0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xd0> CREATOR; // = new en0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    @Deprecated
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;

    @DexIgnore
    public xd0(String str, int i, long j) {
        this.e = str;
        this.f = i;
        this.g = j;
    }

    @DexIgnore
    public String H() {
        return this.e;
    }

    @DexIgnore
    public long I() {
        long j = this.g;
        return j == -1 ? (long) this.f : j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof xd0) {
            xd0 xd0 = (xd0) obj;
            if (((H() == null || !H().equals(xd0.H())) && (H() != null || xd0.H() != null)) || I() != xd0.I()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(H(), Long.valueOf(I()));
    }

    @DexIgnore
    public String toString() {
        ak0.a a = ak0.a((Object) this);
        a.a("name", H());
        a.a("version", Long.valueOf(I()));
        return a.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, H(), false);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, I());
        lk0.a(parcel, a);
    }
}
