package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q80 extends r80 {
    @DexIgnore
    public long L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ q80(long j, short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(j, s, peripheral, (i2 & 8) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).array();
        wd4.a((Object) array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean F() {
        return this.M;
    }

    @DexIgnore
    public final long J() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 4) {
            this.L = o90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            xa0.a(a, JSONKey.NEW_SIZE_WRITTEN, Long.valueOf(this.L));
        }
        return a;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.PAGE_OFFSET, Long.valueOf(this.N));
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NEW_SIZE_WRITTEN, Long.valueOf(this.L));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q80(long j, short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_ERASE_SEGMENT, s, RequestId.LEGACY_ERASE_SEGMENT, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.N = j;
        this.M = true;
    }
}
