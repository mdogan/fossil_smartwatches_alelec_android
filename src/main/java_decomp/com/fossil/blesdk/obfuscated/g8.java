package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g8<F, S> {
    @DexIgnore
    public /* final */ F a;
    @DexIgnore
    public /* final */ S b;

    @DexIgnore
    public g8(F f, S s) {
        this.a = f;
        this.b = s;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof g8)) {
            return false;
        }
        g8 g8Var = (g8) obj;
        if (!f8.a(g8Var.a, this.a) || !f8.a(g8Var.b, this.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        F f = this.a;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Pair{" + String.valueOf(this.a) + " " + String.valueOf(this.b) + "}";
    }
}
