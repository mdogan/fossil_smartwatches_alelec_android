package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eu implements hu<Bitmap, BitmapDrawable> {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public eu(Resources resources) {
        uw.a(resources);
        this.a = resources;
    }

    @DexIgnore
    public bq<BitmapDrawable> a(bq<Bitmap> bqVar, mo moVar) {
        return et.a(this.a, bqVar);
    }
}
