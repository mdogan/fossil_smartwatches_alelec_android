package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pr4;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ns4 extends pr4.a {
    @DexIgnore
    public /* final */ c94 a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public ns4(c94 c94, boolean z) {
        this.a = c94;
        this.b = z;
    }

    @DexIgnore
    public static ns4 a() {
        return new ns4((c94) null, true);
    }

    @DexIgnore
    public pr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        boolean z;
        boolean z2;
        Type type2;
        Class<?> a2 = pr4.a.a(type);
        if (a2 == s84.class) {
            return new ms4(Void.class, this.a, this.b, false, true, false, false, false, true);
        }
        boolean z3 = a2 == v84.class;
        boolean z4 = a2 == d94.class;
        boolean z5 = a2 == w84.class;
        if (a2 != z84.class && !z3 && !z4 && !z5) {
            return null;
        }
        if (!(type instanceof ParameterizedType)) {
            String str = !z3 ? !z4 ? z5 ? "Maybe" : "Observable" : "Single" : "Flowable";
            throw new IllegalStateException(str + " return type must be parameterized as " + str + "<Foo> or " + str + "<? extends Foo>");
        }
        Type a3 = pr4.a.a(0, (ParameterizedType) type);
        Class<?> a4 = pr4.a.a(a3);
        if (a4 == cs4.class) {
            if (a3 instanceof ParameterizedType) {
                type2 = pr4.a.a(0, (ParameterizedType) a3);
                z2 = false;
            } else {
                throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
            }
        } else if (a4 != ks4.class) {
            type2 = a3;
            z2 = false;
            z = true;
            return new ms4(type2, this.a, this.b, z2, z, z3, z4, z5, false);
        } else if (a3 instanceof ParameterizedType) {
            type2 = pr4.a.a(0, (ParameterizedType) a3);
            z2 = true;
        } else {
            throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
        }
        z = false;
        return new ms4(type2, this.a, this.b, z2, z, z3, z4, z5, false);
    }
}
