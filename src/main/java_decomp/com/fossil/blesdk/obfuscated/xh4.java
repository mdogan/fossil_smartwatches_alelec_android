package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xh4 {
    @DexIgnore
    public static /* final */ pk4 a; // = new pk4("UNDEFINED");

    @DexIgnore
    public static final <T> void b(kc4<? super T> kc4, T t) {
        wd4.b(kc4, "$this$resumeDirect");
        if (kc4 instanceof vh4) {
            kc4<T> kc42 = ((vh4) kc4).l;
            Result.a aVar = Result.Companion;
            kc42.resumeWith(Result.m3constructorimpl(t));
            return;
        }
        Result.a aVar2 = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(t));
    }

    @DexIgnore
    public static final void a(yh4<?> yh4) {
        ei4 b = nj4.b.b();
        if (b.D()) {
            b.a(yh4);
            return;
        }
        b.c(true);
        try {
            a(yh4, yh4.b(), 3);
            do {
            } while (b.G());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
    }

    @DexIgnore
    public static final <T> void b(kc4<? super T> kc4, Throwable th) {
        wd4.b(kc4, "$this$resumeDirectWithException");
        wd4.b(th, "exception");
        if (kc4 instanceof vh4) {
            kc4<T> kc42 = ((vh4) kc4).l;
            Result.a aVar = Result.Companion;
            kc42.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(th, (kc4<?>) kc42))));
            return;
        }
        Result.a aVar2 = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(th, (kc4<?>) kc4))));
    }

    @DexIgnore
    public static final <T> void a(kc4<? super T> kc4, T t) {
        boolean z;
        CoroutineContext context;
        Object b;
        wd4.b(kc4, "$this$resumeCancellable");
        if (kc4 instanceof vh4) {
            vh4 vh4 = (vh4) kc4;
            if (vh4.k.b(vh4.getContext())) {
                vh4.h = t;
                vh4.g = 1;
                vh4.k.a(vh4.getContext(), vh4);
                return;
            }
            ei4 b2 = nj4.b.b();
            if (b2.D()) {
                vh4.h = t;
                vh4.g = 1;
                b2.a((yh4<?>) vh4);
                return;
            }
            b2.c(true);
            try {
                ri4 ri4 = (ri4) vh4.getContext().get(ri4.d);
                if (ri4 == null || ri4.isActive()) {
                    z = false;
                } else {
                    CancellationException y = ri4.y();
                    Result.a aVar = Result.Companion;
                    vh4.resumeWith(Result.m3constructorimpl(za4.a((Throwable) y)));
                    z = true;
                }
                if (!z) {
                    context = vh4.getContext();
                    b = ThreadContextKt.b(context, vh4.j);
                    kc4<T> kc42 = vh4.l;
                    Result.a aVar2 = Result.Companion;
                    kc42.resumeWith(Result.m3constructorimpl(t));
                    cb4 cb4 = cb4.a;
                    ThreadContextKt.a(context, b);
                }
                do {
                } while (b2.G());
            } catch (Throwable th) {
                try {
                    vh4.a(th, (Throwable) null);
                } catch (Throwable th2) {
                    b2.a(true);
                    throw th2;
                }
            }
            b2.a(true);
            return;
        }
        Result.a aVar3 = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(t));
    }

    @DexIgnore
    public static final <T> void a(kc4<? super T> kc4, Throwable th) {
        CoroutineContext context;
        Object b;
        wd4.b(kc4, "$this$resumeCancellableWithException");
        wd4.b(th, "exception");
        if (kc4 instanceof vh4) {
            vh4 vh4 = (vh4) kc4;
            CoroutineContext context2 = vh4.l.getContext();
            boolean z = false;
            zg4 zg4 = new zg4(th, false, 2, (rd4) null);
            if (vh4.k.b(context2)) {
                vh4.h = new zg4(th, false, 2, (rd4) null);
                vh4.g = 1;
                vh4.k.a(context2, vh4);
                return;
            }
            ei4 b2 = nj4.b.b();
            if (b2.D()) {
                vh4.h = zg4;
                vh4.g = 1;
                b2.a((yh4<?>) vh4);
                return;
            }
            b2.c(true);
            try {
                ri4 ri4 = (ri4) vh4.getContext().get(ri4.d);
                if (ri4 != null && !ri4.isActive()) {
                    CancellationException y = ri4.y();
                    Result.a aVar = Result.Companion;
                    vh4.resumeWith(Result.m3constructorimpl(za4.a((Throwable) y)));
                    z = true;
                }
                if (!z) {
                    context = vh4.getContext();
                    b = ThreadContextKt.b(context, vh4.j);
                    kc4<T> kc42 = vh4.l;
                    Result.a aVar2 = Result.Companion;
                    kc42.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(th, (kc4<?>) kc42))));
                    cb4 cb4 = cb4.a;
                    ThreadContextKt.a(context, b);
                }
                do {
                } while (b2.G());
            } catch (Throwable th2) {
                try {
                    vh4.a(th2, (Throwable) null);
                } catch (Throwable th3) {
                    b2.a(true);
                    throw th3;
                }
            }
            b2.a(true);
            return;
        }
        Result.a aVar3 = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(th, (kc4<?>) kc4))));
    }

    @DexIgnore
    public static final boolean a(vh4<? super cb4> vh4) {
        wd4.b(vh4, "$this$yieldUndispatched");
        cb4 cb4 = cb4.a;
        ei4 b = nj4.b.b();
        if (b.E()) {
            return false;
        }
        if (b.D()) {
            vh4.h = cb4;
            vh4.g = 1;
            b.a((yh4<?>) vh4);
            return true;
        }
        b.c(true);
        try {
            vh4.run();
            do {
            } while (b.G());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
        return false;
    }

    @DexIgnore
    public static final <T> void a(yh4<? super T> yh4, int i) {
        wd4.b(yh4, "$this$dispatch");
        kc4<? super T> b = yh4.b();
        if (!ij4.b(i) || !(b instanceof vh4) || ij4.a(i) != ij4.a(yh4.g)) {
            a(yh4, b, i);
            return;
        }
        gh4 gh4 = ((vh4) b).k;
        CoroutineContext context = b.getContext();
        if (gh4.b(context)) {
            gh4.a(context, yh4);
        } else {
            a((yh4<?>) yh4);
        }
    }

    @DexIgnore
    public static final <T> void a(yh4<? super T> yh4, kc4<? super T> kc4, int i) {
        wd4.b(yh4, "$this$resume");
        wd4.b(kc4, "delegate");
        Object c = yh4.c();
        Throwable a2 = yh4.a(c);
        if (a2 != null) {
            if (!(kc4 instanceof yh4)) {
                a2 = ok4.a(a2, (kc4<?>) kc4);
            }
            ij4.b(kc4, a2, i);
            return;
        }
        ij4.a(kc4, yh4.c(c), i);
    }
}
