package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k13 {
    @DexIgnore
    public /* final */ j23 a;
    @DexIgnore
    public /* final */ r43 b;
    @DexIgnore
    public /* final */ k43 c;

    @DexIgnore
    public k13(j23 j23, r43 r43, k43 k43) {
        wd4.b(j23, "mComplicationsContractView");
        wd4.b(r43, "mWatchAppsContractView");
        wd4.b(k43, "mCustomizeThemeContractView");
        this.a = j23;
        this.b = r43;
        this.c = k43;
    }

    @DexIgnore
    public final j23 a() {
        return this.a;
    }

    @DexIgnore
    public final k43 b() {
        return this.c;
    }

    @DexIgnore
    public final r43 c() {
        return this.b;
    }
}
