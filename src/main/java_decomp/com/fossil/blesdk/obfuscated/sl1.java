package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sl1> CREATOR; // = new tl1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ long i;
    @DexIgnore
    public /* final */ long j;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public /* final */ long n;
    @DexIgnore
    public /* final */ String o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ long q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ boolean s;
    @DexIgnore
    public /* final */ boolean t;
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ String v;

    @DexIgnore
    public sl1(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z, boolean z2, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7) {
        ck0.b(str);
        this.e = str;
        this.f = TextUtils.isEmpty(str2) ? null : str2;
        this.g = str3;
        this.n = j2;
        this.h = str4;
        this.i = j3;
        this.j = j4;
        this.k = str5;
        this.l = z;
        this.m = z2;
        this.o = str6;
        this.p = j5;
        this.q = j6;
        this.r = i2;
        this.s = z3;
        this.t = z4;
        this.u = z5;
        this.v = str7;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e, false);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, 5, this.h, false);
        lk0.a(parcel, 6, this.i);
        lk0.a(parcel, 7, this.j);
        lk0.a(parcel, 8, this.k, false);
        lk0.a(parcel, 9, this.l);
        lk0.a(parcel, 10, this.m);
        lk0.a(parcel, 11, this.n);
        lk0.a(parcel, 12, this.o, false);
        lk0.a(parcel, 13, this.p);
        lk0.a(parcel, 14, this.q);
        lk0.a(parcel, 15, this.r);
        lk0.a(parcel, 16, this.s);
        lk0.a(parcel, 17, this.t);
        lk0.a(parcel, 18, this.u);
        lk0.a(parcel, 19, this.v, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public sl1(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z, boolean z2, long j4, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7) {
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.n = j4;
        this.h = str4;
        this.i = j2;
        this.j = j3;
        this.k = str5;
        this.l = z;
        this.m = z2;
        this.o = str6;
        this.p = j5;
        this.q = j6;
        this.r = i2;
        this.s = z3;
        this.t = z4;
        this.u = z5;
        this.v = str7;
    }
}
