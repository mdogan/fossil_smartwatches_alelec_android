package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.tc1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p41 extends tc1.a<wc1> {
    @DexIgnore
    public /* final */ /* synthetic */ uc1 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p41(o41 o41, he0 he0, uc1 uc1, String str) {
        super(he0);
        this.s = uc1;
    }

    @DexIgnore
    public final /* synthetic */ ne0 a(Status status) {
        return new wc1(status);
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar) throws RemoteException {
        ((g41) bVar).a(this.s, (ve0<wc1>) this, this.t);
    }
}
