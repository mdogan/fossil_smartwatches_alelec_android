package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.debug.DebugViewType;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lq2 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public int a;
    @DexIgnore
    public List<? extends mq2> b;
    @DexIgnore
    public e c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ lq2 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lq2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.lq2$a$a  reason: collision with other inner class name */
        public static final class C0088a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0088a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<mq2> c = this.e.b.c();
                    if (c != null) {
                        mq2 mq2 = c.get(adapterPosition);
                        e b = this.e.b.b();
                        if (b != null) {
                            b.a(mq2.a(), this.e.b.d(), adapterPosition, mq2.b(), (Bundle) null);
                            return;
                        }
                        return;
                    }
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public b(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<mq2> c = this.e.b.c();
                if (c != null) {
                    mq2 mq2 = c.get(adapterPosition);
                    e b = this.e.b.b();
                    if (b == null) {
                        return true;
                    }
                    b.b(mq2.a(), this.e.b.d(), adapterPosition, mq2.b(), (Bundle) null);
                    return true;
                }
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lq2 lq2, View view) {
            super(view);
            wd4.b(view, "itemView");
            this.b = lq2;
            view.setOnClickListener(new C0088a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(R.id.tv_debug_child);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public List<String> a; // = new ArrayList();
        @DexIgnore
        public TextView b;
        @DexIgnore
        public Spinner c;
        @DexIgnore
        public Button d;
        @DexIgnore
        public /* final */ /* synthetic */ lq2 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<mq2> c = this.e.e.c();
                    if (c != null) {
                        mq2 mq2 = c.get(adapterPosition);
                        e b = this.e.e.b();
                        if (b != null) {
                            String a = mq2.a();
                            int d = this.e.e.d();
                            Object b2 = mq2.b();
                            Bundle bundle = new Bundle();
                            bundle.putInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS", this.e.c.getSelectedItemPosition());
                            b.a(a, d, adapterPosition, b2, bundle);
                            return;
                        }
                        return;
                    }
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(lq2 lq2, View view) {
            super(view);
            wd4.b(view, "itemView");
            this.e = lq2;
            View findViewById = view.findViewById(R.id.tv_title_debug_child_with_spinner);
            if (findViewById != null) {
                this.b = (TextView) findViewById;
                View findViewById2 = view.findViewById(R.id.sp_debug_child_with_spinner);
                if (findViewById2 != null) {
                    this.c = (Spinner) findViewById2;
                    View findViewById3 = view.findViewById(R.id.btn_debug_child_with_spinner);
                    if (findViewById3 != null) {
                        Button button = (Button) findViewById3;
                        button.setOnClickListener(new a(this));
                        this.d = button;
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.Spinner");
            }
            wd4.a();
            throw null;
        }

        @DexIgnore
        public final TextView b() {
            return this.b;
        }

        @DexIgnore
        public final void c() {
            View view = this.itemView;
            wd4.a((Object) view, "itemView");
            ArrayAdapter arrayAdapter = new ArrayAdapter(view.getContext(), 17367048, this.a);
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter(arrayAdapter);
        }

        @DexIgnore
        public final void a(List<String> list) {
            wd4.b(list, "value");
            this.a = list;
            c();
        }

        @DexIgnore
        public final Button a() {
            return this.d;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public SwitchCompat b;
        @DexIgnore
        public /* final */ /* synthetic */ lq2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ SwitchCompat e;
            @DexIgnore
            public /* final */ /* synthetic */ c f;

            @DexIgnore
            public a(SwitchCompat switchCompat, c cVar) {
                this.e = switchCompat;
                this.f = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.f.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<mq2> c = this.f.c.c();
                    if (c != null) {
                        mq2 mq2 = c.get(adapterPosition);
                        if (mq2 != null) {
                            ((oq2) mq2).a(this.e.isChecked());
                            e b = this.f.c.b();
                            if (b != null) {
                                String a = mq2.a();
                                int d = this.f.c.d();
                                Object b2 = mq2.b();
                                Bundle bundle = new Bundle();
                                List<mq2> c2 = this.f.c.c();
                                if (c2 != null) {
                                    mq2 mq22 = c2.get(adapterPosition);
                                    if (mq22 != null) {
                                        bundle.putBoolean("DEBUG_BUNDLE_IS_CHECKED", ((oq2) mq22).d());
                                        b.a(a, d, adapterPosition, b2, bundle);
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            }
                            this.f.c.notifyItemChanged(adapterPosition);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                    }
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(lq2 lq2, View view) {
            super(view);
            wd4.b(view, "itemView");
            this.c = lq2;
            View findViewById = view.findViewById(R.id.tv_debug_child_with_switch);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(R.id.sc_debug_child_with_switch);
                if (findViewById2 != null) {
                    SwitchCompat switchCompat = (SwitchCompat) findViewById2;
                    switchCompat.setOnClickListener(new a(switchCompat, this));
                    this.b = switchCompat;
                    return;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }

        @DexIgnore
        public final SwitchCompat a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ lq2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d e;

            @DexIgnore
            public a(d dVar) {
                this.e = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<mq2> c = this.e.c.c();
                    if (c != null) {
                        mq2 mq2 = c.get(adapterPosition);
                        e b = this.e.c.b();
                        if (b != null) {
                            b.a(mq2.a(), this.e.c.d(), adapterPosition, mq2.b(), (Bundle) null);
                            return;
                        }
                        return;
                    }
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d e;

            @DexIgnore
            public b(d dVar) {
                this.e = dVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<mq2> c = this.e.c.c();
                if (c != null) {
                    mq2 mq2 = c.get(adapterPosition);
                    e b = this.e.c.b();
                    if (b == null) {
                        return true;
                    }
                    b.b(mq2.a(), this.e.c.d(), adapterPosition, mq2.b(), (Bundle) null);
                    return true;
                }
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(lq2 lq2, View view) {
            super(view);
            wd4.b(view, "itemView");
            this.c = lq2;
            view.setOnClickListener(new a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(R.id.tv_debug_child_with_text);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(R.id.tv_debug_child_with_text_text);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public final e b() {
        return this.c;
    }

    @DexIgnore
    public final List<mq2> c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public int getItemCount() {
        List<? extends mq2> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.blesdk.obfuscated.mq2} */
    /* JADX WARNING: Multi-variable type inference failed */
    public int getItemViewType(int i) {
        List<? extends mq2> list = this.b;
        mq2 mq2 = null;
        if ((list != null ? (mq2) list.get(i) : null) instanceof oq2) {
            return DebugViewType.CHILD_ITEM_WITH_SWITCH.ordinal();
        }
        List<? extends mq2> list2 = this.b;
        if ((list2 != null ? (mq2) list2.get(i) : null) instanceof pq2) {
            return DebugViewType.CHILD_ITEM_WITH_TEXT.ordinal();
        }
        List list3 = this.b;
        if (list3 != null) {
            mq2 = list3.get(i);
        }
        if (mq2 instanceof nq2) {
            return DebugViewType.CHILD_ITEM_WITH_SPINNER.ordinal();
        }
        return DebugViewType.CHILD.ordinal();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        if (viewHolder instanceof a) {
            TextView a2 = ((a) viewHolder).a();
            List<? extends mq2> list = this.b;
            if (list != null) {
                a2.setText(((mq2) list.get(i)).c());
            } else {
                wd4.a();
                throw null;
            }
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            TextView b2 = cVar.b();
            List<? extends mq2> list2 = this.b;
            if (list2 != null) {
                b2.setText(((mq2) list2.get(i)).c());
                SwitchCompat a3 = cVar.a();
                List<? extends mq2> list3 = this.b;
                if (list3 != null) {
                    Object obj = list3.get(i);
                    if (obj != null) {
                        a3.setChecked(((oq2) obj).d());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        } else if (viewHolder instanceof d) {
            d dVar = (d) viewHolder;
            TextView b3 = dVar.b();
            List<? extends mq2> list4 = this.b;
            if (list4 != null) {
                b3.setText(((mq2) list4.get(i)).c());
                TextView a4 = dVar.a();
                List<? extends mq2> list5 = this.b;
                if (list5 != null) {
                    Object obj2 = list5.get(i);
                    if (obj2 != null) {
                        a4.setText(((pq2) obj2).d());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        } else if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            TextView b4 = bVar.b();
            List<? extends mq2> list6 = this.b;
            if (list6 != null) {
                b4.setText(((mq2) list6.get(i)).c());
                Button a5 = bVar.a();
                List<? extends mq2> list7 = this.b;
                if (list7 != null) {
                    Object obj3 = list7.get(i);
                    if (obj3 != null) {
                        a5.setText(((nq2) obj3).d());
                        List<? extends mq2> list8 = this.b;
                        if (list8 != null) {
                            Object obj4 = list8.get(i);
                            if (obj4 != null) {
                                bVar.a(((nq2) obj4).e());
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                        }
                        wd4.a();
                        throw null;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        if (i == DebugViewType.CHILD.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_debug_child, viewGroup, false);
            wd4.a((Object) inflate, "view");
            return new a(this, inflate);
        } else if (i == DebugViewType.CHILD_ITEM_WITH_SWITCH.ordinal()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_debug_child_with_switch, viewGroup, false);
            wd4.a((Object) inflate2, "view");
            return new c(this, inflate2);
        } else if (i == DebugViewType.CHILD_ITEM_WITH_TEXT.ordinal()) {
            View inflate3 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_debug_child_with_text, viewGroup, false);
            wd4.a((Object) inflate3, "view");
            return new d(this, inflate3);
        } else if (i == DebugViewType.CHILD_ITEM_WITH_SPINNER.ordinal()) {
            View inflate4 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_debug_child_with_spinner, viewGroup, false);
            wd4.a((Object) inflate4, "view");
            return new b(this, inflate4);
        } else {
            throw new IllegalArgumentException("viewType is not appropriate");
        }
    }

    @DexIgnore
    public final void a(List<? extends mq2> list) {
        this.b = list;
    }

    @DexIgnore
    public final void a(e eVar) {
        wd4.b(eVar, "itemClickListener");
        this.c = eVar;
    }
}
