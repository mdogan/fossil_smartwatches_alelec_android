package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class om3 extends zm3 implements fn3 {
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<te2> k;
    @DexIgnore
    public en3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final om3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            om3 om3 = new om3();
            om3.setArguments(bundle);
            return om3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public b(FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.a, R.anim.move_out);
            FlexibleTextView flexibleTextView = (FlexibleTextView) this.a.findViewById(R.id.ftv_title);
            if (flexibleTextView != null) {
                flexibleTextView.startAnimation(loadAnimation);
            }
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) this.a.findViewById(R.id.ftv_desc);
            if (flexibleTextView2 != null) {
                flexibleTextView2.startAnimation(loadAnimation);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ om3 e;

        @DexIgnore
        public c(om3 om3) {
            this.e = om3;
        }

        @DexIgnore
        public final void onClick(View view) {
            om3.a(this.e).a(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ en3 a(om3 om3) {
        en3 en3 = om3.l;
        if (en3 != null) {
            return en3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i, boolean z, int i2) {
        if (!z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(activity, R.anim.fragment_out);
                if (loadAnimation == null) {
                    return loadAnimation;
                }
                loadAnimation.setAnimationListener(new b(activity));
                return loadAnimation;
            }
        }
        return null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        te2 te2 = (te2) ra.a(layoutInflater, R.layout.fragment_pairing_look_for_device, viewGroup, false, O0());
        this.k = new ur3<>(this, te2);
        wd4.a((Object) te2, "binding");
        return te2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<te2> ur3 = this.k;
        if (ur3 != null) {
            te2 a2 = ur3.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.r;
                if (rTLImageView != null) {
                    rTLImageView.setOnClickListener(new c(this));
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                ur3<te2> ur32 = this.k;
                if (ur32 != null) {
                    te2 a3 = ur32.a();
                    if (a3 != null) {
                        DashBar dashBar = a3.s;
                        if (dashBar != null) {
                            hs3.a aVar = hs3.a;
                            wd4.a((Object) dashBar, "this");
                            aVar.d(dashBar, z, 500);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(en3 en3) {
        wd4.b(en3, "presenter");
        this.l = en3;
    }
}
