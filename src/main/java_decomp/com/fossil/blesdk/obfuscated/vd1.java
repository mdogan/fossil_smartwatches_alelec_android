package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vd1 extends h41 implements ud1 {
    @DexIgnore
    public vd1() {
        super("com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    public static ud1 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
        return queryLocalInterface instanceof ud1 ? (ud1) queryLocalInterface : new wd1(iBinder);
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((LocationResult) r41.a(parcel, LocationResult.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            a((LocationAvailability) r41.a(parcel, LocationAvailability.CREATOR));
        }
        return true;
    }
}
