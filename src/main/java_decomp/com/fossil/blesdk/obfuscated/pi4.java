package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pi4 extends si4<ri4> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater j; // = AtomicIntegerFieldUpdater.newUpdater(pi4.class, "_invoked");
    @DexIgnore
    public volatile int _invoked; // = 0;
    @DexIgnore
    public /* final */ jd4<Throwable, cb4> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pi4(ri4 ri4, jd4<? super Throwable, cb4> jd4) {
        super(ri4);
        wd4.b(ri4, "job");
        wd4.b(jd4, "handler");
        this.i = jd4;
    }

    @DexIgnore
    public void b(Throwable th) {
        if (j.compareAndSet(this, 0, 1)) {
            this.i.invoke(th);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancelling[" + ph4.a((Object) this) + '@' + ph4.b(this) + ']';
    }
}
