package com.fossil.blesdk.obfuscated;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface io<T> {
    @DexIgnore
    boolean a(T t, File file, mo moVar);
}
