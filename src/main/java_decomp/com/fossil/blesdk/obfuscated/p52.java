package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.util.UserUtils;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p52 implements Factory<UserUtils> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public p52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static p52 a(o42 o42) {
        return new p52(o42);
    }

    @DexIgnore
    public static UserUtils b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static UserUtils c(o42 o42) {
        UserUtils n = o42.n();
        o44.a(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    public UserUtils get() {
        return b(this.a);
    }
}
