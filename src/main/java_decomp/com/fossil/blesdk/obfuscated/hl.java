package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hl implements gl {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ mf b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf<fl> {
        @DexIgnore
        public a(hl hlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(lg lgVar, fl flVar) {
            String str = flVar.a;
            if (str == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, str);
            }
            String str2 = flVar.b;
            if (str2 == null) {
                lgVar.a(2);
            } else {
                lgVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkName`(`name`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public hl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @DexIgnore
    public void a(fl flVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(flVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }
}
