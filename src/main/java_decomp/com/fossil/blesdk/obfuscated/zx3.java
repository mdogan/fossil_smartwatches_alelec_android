package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zx3 {
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Downloader d;
    @DexIgnore
    public /* final */ Map<String, tx3> e;
    @DexIgnore
    public /* final */ Map<Object, rx3> f;
    @DexIgnore
    public /* final */ Map<Object, rx3> g;
    @DexIgnore
    public /* final */ Set<Object> h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ ux3 k;
    @DexIgnore
    public /* final */ ly3 l;
    @DexIgnore
    public /* final */ List<tx3> m;
    @DexIgnore
    public /* final */ c n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public /* final */ zx3 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zx3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.zx3$a$a  reason: collision with other inner class name */
        public class C0110a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message e;

            @DexIgnore
            public C0110a(a aVar, Message message) {
                this.e = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.e.what);
            }
        }

        @DexIgnore
        public a(Looper looper, zx3 zx3) {
            super(looper);
            this.a = zx3;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            boolean z = false;
            switch (message.what) {
                case 1:
                    this.a.e((rx3) message.obj);
                    return;
                case 2:
                    this.a.d((rx3) message.obj);
                    return;
                case 4:
                    this.a.f((tx3) message.obj);
                    return;
                case 5:
                    this.a.g((tx3) message.obj);
                    return;
                case 6:
                    this.a.a((tx3) message.obj, false);
                    return;
                case 7:
                    this.a.b();
                    return;
                case 9:
                    this.a.b((NetworkInfo) message.obj);
                    return;
                case 10:
                    zx3 zx3 = this.a;
                    if (message.arg1 == 1) {
                        z = true;
                    }
                    zx3.b(z);
                    return;
                case 11:
                    this.a.a(message.obj);
                    return;
                case 12:
                    this.a.b(message.obj);
                    return;
                default:
                    Picasso.p.post(new C0110a(this, message));
                    return;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends HandlerThread {
        @DexIgnore
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ zx3 a;

        @DexIgnore
        public c(zx3 zx3) {
            this.a = zx3;
        }

        @DexIgnore
        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.a.b.registerReceiver(this, intentFilter);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra("state")) {
                        this.a.a(intent.getBooleanExtra("state", false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.a.a(((ConnectivityManager) py3.a(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    @DexIgnore
    public zx3(Context context, ExecutorService executorService, Handler handler, Downloader downloader, ux3 ux3, ly3 ly3) {
        this.a.start();
        py3.a(this.a.getLooper());
        this.b = context;
        this.c = executorService;
        this.e = new LinkedHashMap();
        this.f = new WeakHashMap();
        this.g = new WeakHashMap();
        this.h = new HashSet();
        this.i = new a(this.a.getLooper(), this);
        this.d = downloader;
        this.j = handler;
        this.k = ux3;
        this.l = ly3;
        this.m = new ArrayList(4);
        this.p = py3.d(this.b);
        this.o = py3.b(context, "android.permission.ACCESS_NETWORK_STATE");
        this.n = new c(this);
        this.n.a();
    }

    @DexIgnore
    public void a(rx3 rx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, rx3));
    }

    @DexIgnore
    public void b(rx3 rx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, rx3));
    }

    @DexIgnore
    public void c(tx3 tx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, tx3));
    }

    @DexIgnore
    public void d(tx3 tx3) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, tx3), 500);
    }

    @DexIgnore
    public void e(rx3 rx3) {
        a(rx3, true);
    }

    @DexIgnore
    public void f(tx3 tx3) {
        if (MemoryPolicy.shouldWriteToMemoryCache(tx3.i())) {
            this.k.a(tx3.g(), tx3.l());
        }
        this.e.remove(tx3.g());
        a(tx3);
        if (tx3.j().n) {
            py3.a("Dispatcher", "batched", py3.a(tx3), "for completion");
        }
    }

    @DexIgnore
    public void g(tx3 tx3) {
        if (!tx3.n()) {
            boolean z = false;
            if (this.c.isShutdown()) {
                a(tx3, false);
                return;
            }
            NetworkInfo networkInfo = null;
            if (this.o) {
                networkInfo = ((ConnectivityManager) py3.a(this.b, "connectivity")).getActiveNetworkInfo();
            }
            boolean z2 = networkInfo != null && networkInfo.isConnected();
            boolean a2 = tx3.a(this.p, networkInfo);
            boolean o2 = tx3.o();
            if (!a2) {
                if (this.o && o2) {
                    z = true;
                }
                a(tx3, z);
                if (z) {
                    e(tx3);
                }
            } else if (!this.o || z2) {
                if (tx3.j().n) {
                    py3.a("Dispatcher", "retrying", py3.a(tx3));
                }
                if (tx3.f() instanceof NetworkRequestHandler.ContentLengthException) {
                    tx3.m |= NetworkPolicy.NO_CACHE.index;
                }
                tx3.r = this.c.submit(tx3);
            } else {
                a(tx3, o2);
                if (o2) {
                    e(tx3);
                }
            }
        }
    }

    @DexIgnore
    public void a(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    @DexIgnore
    public void b(tx3 tx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, tx3));
    }

    @DexIgnore
    public final void c(rx3 rx3) {
        Object j2 = rx3.j();
        if (j2 != null) {
            rx3.k = true;
            this.f.put(j2, rx3);
        }
    }

    @DexIgnore
    public void d(rx3 rx3) {
        String c2 = rx3.c();
        tx3 tx3 = this.e.get(c2);
        if (tx3 != null) {
            tx3.b(rx3);
            if (tx3.a()) {
                this.e.remove(c2);
                if (rx3.f().n) {
                    py3.a("Dispatcher", "canceled", rx3.h().d());
                }
            }
        }
        if (this.h.contains(rx3.i())) {
            this.g.remove(rx3.j());
            if (rx3.f().n) {
                py3.a("Dispatcher", "canceled", rx3.h().d(), "because paused request got canceled");
            }
        }
        rx3 remove = this.f.remove(rx3.j());
        if (remove != null && remove.f().n) {
            py3.a("Dispatcher", "canceled", remove.h().d(), "from replaying");
        }
    }

    @DexIgnore
    public final void e(tx3 tx3) {
        rx3 c2 = tx3.c();
        if (c2 != null) {
            c(c2);
        }
        List<rx3> d2 = tx3.d();
        if (d2 != null) {
            int size = d2.size();
            for (int i2 = 0; i2 < size; i2++) {
                c(d2.get(i2));
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    @DexIgnore
    public void b(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<rx3> it = this.g.values().iterator();
            while (it.hasNext()) {
                rx3 next = it.next();
                if (next.i().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @DexIgnore
    public void a(rx3 rx3, boolean z) {
        if (this.h.contains(rx3.i())) {
            this.g.put(rx3.j(), rx3);
            if (rx3.f().n) {
                String d2 = rx3.b.d();
                py3.a("Dispatcher", "paused", d2, "because tag '" + rx3.i() + "' is paused");
                return;
            }
            return;
        }
        tx3 tx3 = this.e.get(rx3.c());
        if (tx3 != null) {
            tx3.a(rx3);
        } else if (!this.c.isShutdown()) {
            tx3 a2 = tx3.a(rx3.f(), this, this.k, this.l, rx3);
            a2.r = this.c.submit(a2);
            this.e.put(rx3.c(), a2);
            if (z) {
                this.f.remove(rx3.j());
            }
            if (rx3.f().n) {
                py3.a("Dispatcher", "enqueued", rx3.b.d());
            }
        } else if (rx3.f().n) {
            py3.a("Dispatcher", "ignored", rx3.b.d(), "because shut down");
        }
    }

    @DexIgnore
    public void b() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        a((List<tx3>) arrayList);
    }

    @DexIgnore
    public void b(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void b(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof gy3) {
            ((gy3) executorService).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            a();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (this.h.add(obj)) {
            Iterator<tx3> it = this.e.values().iterator();
            while (it.hasNext()) {
                tx3 next = it.next();
                boolean z = next.j().n;
                rx3 c2 = next.c();
                List<rx3> d2 = next.d();
                boolean z2 = d2 != null && !d2.isEmpty();
                if (c2 != null || z2) {
                    if (c2 != null && c2.i().equals(obj)) {
                        next.b(c2);
                        this.g.put(c2.j(), c2);
                        if (z) {
                            py3.a("Dispatcher", "paused", c2.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = d2.size() - 1; size >= 0; size--) {
                            rx3 rx3 = d2.get(size);
                            if (rx3.i().equals(obj)) {
                                next.b(rx3);
                                this.g.put(rx3.j(), rx3);
                                if (z) {
                                    py3.a("Dispatcher", "paused", rx3.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.a()) {
                        it.remove();
                        if (z) {
                            py3.a("Dispatcher", "canceled", py3.a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(tx3 tx3, boolean z) {
        if (tx3.j().n) {
            String a2 = py3.a(tx3);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            py3.a("Dispatcher", "batched", a2, sb.toString());
        }
        this.e.remove(tx3.g());
        a(tx3);
    }

    @DexIgnore
    public final void a() {
        if (!this.f.isEmpty()) {
            Iterator<rx3> it = this.f.values().iterator();
            while (it.hasNext()) {
                rx3 next = it.next();
                it.remove();
                if (next.f().n) {
                    py3.a("Dispatcher", "replaying", next.h().d());
                }
                a(next, false);
            }
        }
    }

    @DexIgnore
    public final void a(tx3 tx3) {
        if (!tx3.n()) {
            this.m.add(tx3);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    @DexIgnore
    public final void a(List<tx3> list) {
        if (list != null && !list.isEmpty() && list.get(0).j().n) {
            StringBuilder sb = new StringBuilder();
            for (tx3 next : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(py3.a(next));
            }
            py3.a("Dispatcher", "delivered", sb.toString());
        }
    }
}
