package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ss implements no<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ xs a;

    @DexIgnore
    public ss(xs xsVar) {
        this.a = xsVar;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, mo moVar) {
        return this.a.a(byteBuffer);
    }

    @DexIgnore
    public bq<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, mo moVar) throws IOException {
        return this.a.a(lw.c(byteBuffer), i, i2, moVar);
    }
}
