package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.uj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hj0 extends uj0.a {
    @DexIgnore
    public static Account a(uj0 uj0) {
        if (uj0 != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return uj0.h();
            } catch (RemoteException unused) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return null;
    }
}
