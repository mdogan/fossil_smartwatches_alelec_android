package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb0 {
    @DexIgnore
    public static /* final */ hb0 a; // = new hb0();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b A[SYNTHETIC, Splitter:B:25:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0055 A[SYNTHETIC, Splitter:B:30:0x0055] */
    public final String a(File file) {
        wd4.b(file, "file");
        InputStreamReader inputStreamReader = null;
        if (!file.exists()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader inputStreamReader2 = new InputStreamReader(new FileInputStream(file));
            try {
                char[] cArr = new char[1024];
                for (int read = inputStreamReader2.read(cArr); read != -1; read = inputStreamReader2.read(cArr)) {
                    sb.append(cArr, 0, read);
                }
            } catch (IOException e) {
                e = e;
                inputStreamReader = inputStreamReader2;
                try {
                    ea0.l.a(e);
                    if (inputStreamReader != null) {
                    }
                    return sb.toString();
                } catch (Throwable th) {
                    th = th;
                    if (inputStreamReader != null) {
                        try {
                            inputStreamReader.close();
                        } catch (IOException e2) {
                            ea0.l.a(e2);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                inputStreamReader = inputStreamReader2;
                if (inputStreamReader != null) {
                }
                throw th;
            }
            try {
                inputStreamReader2.close();
            } catch (IOException e3) {
                ea0.l.a(e3);
            }
        } catch (IOException e4) {
            e = e4;
            ea0.l.a(e);
            if (inputStreamReader != null) {
                inputStreamReader.close();
            }
            return sb.toString();
        }
        return sb.toString();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026 A[SYNTHETIC, Splitter:B:11:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002e A[Catch:{ IOException -> 0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    public final boolean a(String str, File file) {
        FileWriter fileWriter;
        wd4.b(str, "text");
        wd4.b(file, "file");
        try {
            fileWriter = new FileWriter(file, true);
            try {
                fileWriter.append(str);
                fileWriter.flush();
                fileWriter.close();
                return true;
            } catch (IOException e) {
                e = e;
                ea0.l.a(e);
                if (fileWriter != null) {
                    try {
                        fileWriter.flush();
                    } catch (IOException e2) {
                        ea0.l.a(e2);
                        return false;
                    }
                }
                if (fileWriter != null) {
                    return false;
                }
                fileWriter.close();
                return false;
            }
        } catch (IOException e3) {
            e = e3;
            fileWriter = null;
            ea0.l.a(e);
            if (fileWriter != null) {
            }
            if (fileWriter != null) {
            }
        }
    }
}
