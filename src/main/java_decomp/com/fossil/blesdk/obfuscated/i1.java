package com.fossil.blesdk.obfuscated;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.fossil.blesdk.obfuscated.c0;
import com.fossil.blesdk.obfuscated.p1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i1 implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, p1.a {
    @DexIgnore
    public h1 e;
    @DexIgnore
    public c0 f;
    @DexIgnore
    public f1 g;
    @DexIgnore
    public p1.a h;

    @DexIgnore
    public i1(h1 h1Var) {
        this.e = h1Var;
    }

    @DexIgnore
    public void a(IBinder iBinder) {
        h1 h1Var = this.e;
        c0.a aVar = new c0.a(h1Var.e());
        this.g = new f1(aVar.b(), x.abc_list_menu_item_layout);
        this.g.a((p1.a) this);
        this.e.a((p1) this.g);
        aVar.a(this.g.c(), (DialogInterface.OnClickListener) this);
        View i = h1Var.i();
        if (i != null) {
            aVar.a(i);
        } else {
            aVar.a(h1Var.g());
            aVar.b(h1Var.h());
        }
        aVar.a((DialogInterface.OnKeyListener) this);
        this.f = aVar.a();
        this.f.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f.show();
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        this.e.a((MenuItem) (k1) this.g.c().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        this.g.a(this.e, true);
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window = this.f.getWindow();
                if (window != null) {
                    View decorView = window.getDecorView();
                    if (decorView != null) {
                        KeyEvent.DispatcherState keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null) {
                            keyDispatcherState.startTracking(keyEvent, this);
                            return true;
                        }
                    }
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled()) {
                Window window2 = this.f.getWindow();
                if (window2 != null) {
                    View decorView2 = window2.getDecorView();
                    if (decorView2 != null) {
                        KeyEvent.DispatcherState keyDispatcherState2 = decorView2.getKeyDispatcherState();
                        if (keyDispatcherState2 != null && keyDispatcherState2.isTracking(keyEvent)) {
                            this.e.a(true);
                            dialogInterface.dismiss();
                            return true;
                        }
                    }
                }
            }
        }
        return this.e.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    public void a() {
        c0 c0Var = this.f;
        if (c0Var != null) {
            c0Var.dismiss();
        }
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z) {
        if (z || h1Var == this.e) {
            a();
        }
        p1.a aVar = this.h;
        if (aVar != null) {
            aVar.a(h1Var, z);
        }
    }

    @DexIgnore
    public boolean a(h1 h1Var) {
        p1.a aVar = this.h;
        if (aVar != null) {
            return aVar.a(h1Var);
        }
        return false;
    }
}
