package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aa0 extends ba0 {
    @DexIgnore
    public static long k; // = 100;
    @DexIgnore
    public static /* final */ aa0 l; // = new aa0();

    @DexIgnore
    public aa0() {
        super("raw_minute_data", 204800, "raw_minute_data", "raw_minute_data", new oa0("", "", ""), 1800, new ca0(), SharedPreferenceFileName.MINUTE_DATA_REFERENCE, false);
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
