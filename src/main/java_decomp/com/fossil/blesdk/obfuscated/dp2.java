package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dp2 implements Factory<cp2> {
    @DexIgnore
    public /* final */ Provider<en2> a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<fn2> c;

    @DexIgnore
    public dp2(Provider<en2> provider, Provider<AuthApiGuestService> provider2, Provider<fn2> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static dp2 a(Provider<en2> provider, Provider<AuthApiGuestService> provider2, Provider<fn2> provider3) {
        return new dp2(provider, provider2, provider3);
    }

    @DexIgnore
    public static cp2 b(Provider<en2> provider, Provider<AuthApiGuestService> provider2, Provider<fn2> provider3) {
        return new cp2(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public cp2 get() {
        return b(this.a, this.b, this.c);
    }
}
