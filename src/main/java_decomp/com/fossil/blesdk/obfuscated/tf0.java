package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.lj0;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tf0 implements ng0 {
    @DexIgnore
    public /* final */ og0 a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ zd0 d;
    @DexIgnore
    public vd0 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Bundle i; // = new Bundle();
    @DexIgnore
    public /* final */ Set<ee0.c> j; // = new HashSet();
    @DexIgnore
    public mn1 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public uj0 o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ lj0 r;
    @DexIgnore
    public /* final */ Map<ee0<?>, Boolean> s;
    @DexIgnore
    public /* final */ ee0.a<? extends mn1, wm1> t;
    @DexIgnore
    public ArrayList<Future<?>> u; // = new ArrayList<>();

    @DexIgnore
    public tf0(og0 og0, lj0 lj0, Map<ee0<?>, Boolean> map, zd0 zd0, ee0.a<? extends mn1, wm1> aVar, Lock lock, Context context) {
        this.a = og0;
        this.r = lj0;
        this.s = map;
        this.d = zd0;
        this.t = aVar;
        this.b = lock;
        this.c = context;
    }

    @DexIgnore
    public static String b(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    @DexIgnore
    public final void a(hn1 hn1) {
        if (a(0)) {
            vd0 H = hn1.H();
            if (H.L()) {
                ek0 I = hn1.I();
                vd0 I2 = I.I();
                if (!I2.L()) {
                    String valueOf = String.valueOf(I2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    b(I2);
                    return;
                }
                this.n = true;
                this.o = I.H();
                this.p = I.J();
                this.q = I.K();
                e();
            } else if (a(H)) {
                g();
                e();
            } else {
                b(H);
            }
        }
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t2) {
        this.a.r.i.add(t2);
        return t2;
    }

    @DexIgnore
    public final void b() {
    }

    @DexIgnore
    public final void c() {
        this.a.k.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (ee0 next : this.s.keySet()) {
            ee0.f fVar = this.a.j.get(next.a());
            z |= next.c().a() == 1;
            boolean booleanValue = this.s.get(next).booleanValue();
            if (fVar.l()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(next.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fVar, new vf0(this, next, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.a(Integer.valueOf(System.identityHashCode(this.a.r)));
            cg0 cg0 = new cg0(this, (uf0) null);
            ee0.a<? extends mn1, wm1> aVar = this.t;
            Context context = this.c;
            Looper f2 = this.a.r.f();
            lj0 lj0 = this.r;
            this.k = (mn1) aVar.a(context, f2, lj0, lj0.j(), cg0, cg0);
        }
        this.h = this.a.j.size();
        this.u.add(rg0.a().submit(new wf0(this, hashMap)));
    }

    @DexIgnore
    public final boolean d() {
        this.h--;
        int i2 = this.h;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GoogleApiClientConnecting", this.a.r.q());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new vd0(8, (PendingIntent) null));
            return false;
        }
        vd0 vd0 = this.e;
        if (vd0 == null) {
            return true;
        }
        this.a.q = this.f;
        b(vd0);
        return false;
    }

    @DexIgnore
    public final void e() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.a.j.size();
                for (ee0.c next : this.a.j.keySet()) {
                    if (!this.a.k.containsKey(next)) {
                        arrayList.add(this.a.j.get(next));
                    } else if (d()) {
                        f();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(rg0.a().submit(new zf0(this, arrayList)));
                }
            }
        }
    }

    @DexIgnore
    public final void f() {
        this.a.i();
        rg0.a().execute(new uf0(this));
        mn1 mn1 = this.k;
        if (mn1 != null) {
            if (this.p) {
                mn1.a(this.o, this.q);
            }
            a(false);
        }
        for (ee0.c<?> cVar : this.a.k.keySet()) {
            this.a.j.get(cVar).a();
        }
        this.a.s.a(this.i.isEmpty() ? null : this.i);
    }

    @DexIgnore
    public final void g() {
        this.m = false;
        this.a.r.q = Collections.emptySet();
        for (ee0.c next : this.j) {
            if (!this.a.k.containsKey(next)) {
                this.a.k.put(next, new vd0(17, (PendingIntent) null));
            }
        }
    }

    @DexIgnore
    public final void h() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    @DexIgnore
    public final Set<Scope> i() {
        lj0 lj0 = this.r;
        if (lj0 == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(lj0.i());
        Map<ee0<?>, lj0.b> f2 = this.r.f();
        for (ee0 next : f2.keySet()) {
            if (!this.a.k.containsKey(next.a())) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    @DexIgnore
    public final void b(vd0 vd0, ee0<?> ee0, boolean z) {
        boolean z2;
        int a2 = ee0.c().a();
        boolean z3 = false;
        if (z) {
            if (!vd0.K() && this.d.a(vd0.H()) == null) {
                z2 = false;
            } else {
                z2 = true;
            }
        }
        if (this.e == null || a2 < this.f) {
            z3 = true;
        }
        if (z3) {
            this.e = vd0;
            this.f = a2;
        }
        this.a.k.put(ee0.a(), vd0);
    }

    @DexIgnore
    public final void b(vd0 vd0) {
        h();
        a(!vd0.K());
        this.a.a(vd0);
        this.a.s.a(vd0);
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        if (a(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final void f(int i2) {
        b(new vd0(8, (PendingIntent) null));
    }

    @DexIgnore
    public final void a(vd0 vd0, ee0<?> ee0, boolean z) {
        if (a(1)) {
            b(vd0, ee0, z);
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final boolean a() {
        h();
        a(true);
        this.a.a((vd0) null);
        return true;
    }

    @DexIgnore
    public final boolean a(vd0 vd0) {
        return this.l && !vd0.K();
    }

    @DexIgnore
    public final void a(boolean z) {
        mn1 mn1 = this.k;
        if (mn1 != null) {
            if (mn1.c() && z) {
                this.k.g();
            }
            this.k.a();
            if (this.r.k()) {
                this.k = null;
            }
            this.o = null;
        }
    }

    @DexIgnore
    public final boolean a(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.a.r.q());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String b2 = b(this.g);
        String b3 = b(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(b2).length() + 70 + String.valueOf(b3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(b2);
        sb3.append(" but received callback for step ");
        sb3.append(b3);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        b(new vd0(8, (PendingIntent) null));
        return false;
    }
}
