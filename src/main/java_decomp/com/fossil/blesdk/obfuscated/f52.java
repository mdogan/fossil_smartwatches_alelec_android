package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f52 implements Factory<LinkStreamingManager> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<wy2> c;
    @DexIgnore
    public /* final */ Provider<qx2> d;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> e;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> f;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> g;
    @DexIgnore
    public /* final */ Provider<fn2> h;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> i;
    @DexIgnore
    public /* final */ Provider<SetNotificationUseCase> j;

    @DexIgnore
    public f52(o42 o42, Provider<HybridPresetRepository> provider, Provider<wy2> provider2, Provider<qx2> provider3, Provider<NotificationSettingsDatabase> provider4, Provider<NotificationsRepository> provider5, Provider<DeviceRepository> provider6, Provider<fn2> provider7, Provider<AlarmsRepository> provider8, Provider<SetNotificationUseCase> provider9) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
    }

    @DexIgnore
    public static f52 a(o42 o42, Provider<HybridPresetRepository> provider, Provider<wy2> provider2, Provider<qx2> provider3, Provider<NotificationSettingsDatabase> provider4, Provider<NotificationsRepository> provider5, Provider<DeviceRepository> provider6, Provider<fn2> provider7, Provider<AlarmsRepository> provider8, Provider<SetNotificationUseCase> provider9) {
        return new f52(o42, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9);
    }

    @DexIgnore
    public static LinkStreamingManager b(o42 o42, Provider<HybridPresetRepository> provider, Provider<wy2> provider2, Provider<qx2> provider3, Provider<NotificationSettingsDatabase> provider4, Provider<NotificationsRepository> provider5, Provider<DeviceRepository> provider6, Provider<fn2> provider7, Provider<AlarmsRepository> provider8, Provider<SetNotificationUseCase> provider9) {
        return a(o42, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get());
    }

    @DexIgnore
    public static LinkStreamingManager a(o42 o42, HybridPresetRepository hybridPresetRepository, wy2 wy2, qx2 qx2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, fn2 fn2, AlarmsRepository alarmsRepository, SetNotificationUseCase setNotificationUseCase) {
        LinkStreamingManager a2 = o42.a(hybridPresetRepository, wy2, qx2, notificationSettingsDatabase, notificationsRepository, deviceRepository, fn2, alarmsRepository, setNotificationUseCase);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public LinkStreamingManager get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j);
    }
}
