package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oh0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ne0 e;
    @DexIgnore
    public /* final */ /* synthetic */ nh0 f;

    @DexIgnore
    public oh0(nh0 nh0, ne0 ne0) {
        this.f = nh0;
        this.e = ne0;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(true);
            this.f.g.sendMessage(this.f.g.obtainMessage(0, this.f.a.a(this.e)));
            BasePendingResult.p.set(false);
            nh0.a(this.e);
            he0 he0 = (he0) this.f.f.get();
            if (he0 != null) {
                he0.a(this.f);
            }
        } catch (RuntimeException e2) {
            this.f.g.sendMessage(this.f.g.obtainMessage(1, e2));
            BasePendingResult.p.set(false);
            nh0.a(this.e);
            he0 he02 = (he0) this.f.f.get();
            if (he02 != null) {
                he02.a(this.f);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(false);
            nh0.a(this.e);
            he0 he03 = (he0) this.f.f.get();
            if (he03 != null) {
                he03.a(this.f);
            }
            throw th;
        }
    }
}
