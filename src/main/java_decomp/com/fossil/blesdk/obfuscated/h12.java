package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.enums.Action;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h12 extends m12 {
    @DexIgnore
    public static volatile h12[] x;
    @DexIgnore
    public j12 a;
    @DexIgnore
    public j12 b;
    @DexIgnore
    public j12 c;
    @DexIgnore
    public j12 d;
    @DexIgnore
    public j12 e;
    @DexIgnore
    public j12 f;
    @DexIgnore
    public j12 g;
    @DexIgnore
    public j12 h;
    @DexIgnore
    public j12 i;
    @DexIgnore
    public j12 j;
    @DexIgnore
    public j12 k;
    @DexIgnore
    public j12 l;
    @DexIgnore
    public j12 m;
    @DexIgnore
    public j12 n;
    @DexIgnore
    public j12 o;
    @DexIgnore
    public j12 p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public g12[] v;
    @DexIgnore
    public g12[] w;

    @DexIgnore
    public h12() {
        a();
    }

    @DexIgnore
    public static h12[] b() {
        if (x == null) {
            synchronized (l12.a) {
                if (x == null) {
                    x = new h12[0];
                }
            }
        }
        return x;
    }

    @DexIgnore
    public h12 a() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = 0;
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = g12.b();
        this.w = g12.b();
        return this;
    }

    @DexIgnore
    public h12 a(k12 k12) throws IOException {
        while (true) {
            int j2 = k12.j();
            switch (j2) {
                case 0:
                    return this;
                case 10:
                    if (this.a == null) {
                        this.a = new j12();
                    }
                    k12.a((m12) this.a);
                    break;
                case 18:
                    if (this.b == null) {
                        this.b = new j12();
                    }
                    k12.a((m12) this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new j12();
                    }
                    k12.a((m12) this.c);
                    break;
                case 34:
                    if (this.d == null) {
                        this.d = new j12();
                    }
                    k12.a((m12) this.d);
                    break;
                case 42:
                    if (this.e == null) {
                        this.e = new j12();
                    }
                    k12.a((m12) this.e);
                    break;
                case 50:
                    if (this.f == null) {
                        this.f = new j12();
                    }
                    k12.a((m12) this.f);
                    break;
                case 58:
                    if (this.g == null) {
                        this.g = new j12();
                    }
                    k12.a((m12) this.g);
                    break;
                case 66:
                    if (this.h == null) {
                        this.h = new j12();
                    }
                    k12.a((m12) this.h);
                    break;
                case 74:
                    k12.i();
                    break;
                case 80:
                    this.q = k12.d();
                    break;
                case 90:
                    this.r = k12.i();
                    break;
                case 98:
                    k12.i();
                    break;
                case 106:
                    this.s = k12.i();
                    break;
                case 122:
                    this.t = k12.i();
                    break;
                case 130:
                    this.u = k12.i();
                    break;
                case 138:
                    k12.i();
                    break;
                case 144:
                    k12.c();
                    break;
                case 154:
                    int a2 = o12.a(k12, 154);
                    g12[] g12Arr = this.v;
                    int length = g12Arr == null ? 0 : g12Arr.length;
                    g12[] g12Arr2 = new g12[(a2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.v, 0, g12Arr2, 0, length);
                    }
                    while (length < g12Arr2.length - 1) {
                        g12Arr2[length] = new g12();
                        k12.a((m12) g12Arr2[length]);
                        k12.j();
                        length++;
                    }
                    g12Arr2[length] = new g12();
                    k12.a((m12) g12Arr2[length]);
                    this.v = g12Arr2;
                    break;
                case 162:
                    int a3 = o12.a(k12, 162);
                    g12[] g12Arr3 = this.w;
                    int length2 = g12Arr3 == null ? 0 : g12Arr3.length;
                    g12[] g12Arr4 = new g12[(a3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.w, 0, g12Arr4, 0, length2);
                    }
                    while (length2 < g12Arr4.length - 1) {
                        g12Arr4[length2] = new g12();
                        k12.a((m12) g12Arr4[length2]);
                        k12.j();
                        length2++;
                    }
                    g12Arr4[length2] = new g12();
                    k12.a((m12) g12Arr4[length2]);
                    this.w = g12Arr4;
                    break;
                case 170:
                    if (this.i == null) {
                        this.i = new j12();
                    }
                    k12.a((m12) this.i);
                    break;
                case 176:
                    k12.c();
                    break;
                case 186:
                    k12.i();
                    break;
                case 194:
                    if (this.p == null) {
                        this.p = new j12();
                    }
                    k12.a((m12) this.p);
                    break;
                case Action.Selfie.TAKE_BURST /*202*/:
                    if (this.j == null) {
                        this.j = new j12();
                    }
                    k12.a((m12) this.j);
                    break;
                case 208:
                    k12.c();
                    break;
                case 218:
                    if (this.k == null) {
                        this.k = new j12();
                    }
                    k12.a((m12) this.k);
                    break;
                case 226:
                    if (this.l == null) {
                        this.l = new j12();
                    }
                    k12.a((m12) this.l);
                    break;
                case 234:
                    if (this.m == null) {
                        this.m = new j12();
                    }
                    k12.a((m12) this.m);
                    break;
                case 242:
                    if (this.n == null) {
                        this.n = new j12();
                    }
                    k12.a((m12) this.n);
                    break;
                case 250:
                    if (this.o == null) {
                        this.o = new j12();
                    }
                    k12.a((m12) this.o);
                    break;
                case 256:
                    k12.c();
                    break;
                default:
                    if (o12.b(k12, j2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
