package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zv3 extends Closeable {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i, int i2, int i3, boolean z);

        @DexIgnore
        void a(int i, int i2, List<dw3> list) throws IOException;

        @DexIgnore
        void a(int i, long j);

        @DexIgnore
        void a(int i, ErrorCode errorCode);

        @DexIgnore
        void a(int i, ErrorCode errorCode, ByteString byteString);

        @DexIgnore
        void a(boolean z, int i, int i2);

        @DexIgnore
        void a(boolean z, int i, xo4 xo4, int i2) throws IOException;

        @DexIgnore
        void a(boolean z, lw3 lw3);

        @DexIgnore
        void a(boolean z, boolean z2, int i, int i2, List<dw3> list, HeadersMode headersMode);
    }

    @DexIgnore
    boolean a(a aVar) throws IOException;

    @DexIgnore
    void q() throws IOException;
}
