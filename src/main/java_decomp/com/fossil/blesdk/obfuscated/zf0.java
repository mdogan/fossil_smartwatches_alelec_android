package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ee0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zf0 extends dg0 {
    @DexIgnore
    public /* final */ ArrayList<ee0.f> f;
    @DexIgnore
    public /* final */ /* synthetic */ tf0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zf0(tf0 tf0, ArrayList<ee0.f> arrayList) {
        super(tf0, (uf0) null);
        this.g = tf0;
        this.f = arrayList;
    }

    @DexIgnore
    public final void a() {
        this.g.a.r.q = this.g.i();
        ArrayList<ee0.f> arrayList = this.f;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            ee0.f fVar = arrayList.get(i);
            i++;
            fVar.a(this.g.o, this.g.a.r.q);
        }
    }
}
