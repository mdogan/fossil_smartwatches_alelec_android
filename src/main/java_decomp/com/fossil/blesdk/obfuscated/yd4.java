package com.fossil.blesdk.obfuscated;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yd4 {
    @DexIgnore
    public static /* final */ zd4 a;

    /*
    static {
        zd4 zd4 = null;
        try {
            zd4 = (zd4) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (zd4 == null) {
            zd4 = new zd4();
        }
        a = zd4;
    }
    */

    @DexIgnore
    public static ve4 a(Class cls, String str) {
        return a.a(cls, str);
    }

    @DexIgnore
    public static te4 a(Class cls) {
        return a.a(cls);
    }

    @DexIgnore
    public static String a(Lambda lambda) {
        return a.a(lambda);
    }

    @DexIgnore
    public static String a(td4 td4) {
        return a.a(td4);
    }

    @DexIgnore
    public static we4 a(FunctionReference functionReference) {
        a.a(functionReference);
        return functionReference;
    }

    @DexIgnore
    public static ye4 a(PropertyReference1 propertyReference1) {
        a.a(propertyReference1);
        return propertyReference1;
    }
}
