package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f62 implements MembersInjector<PortfolioApp> {
    @DexIgnore
    public static void a(PortfolioApp portfolioApp, fn2 fn2) {
        portfolioApp.f = fn2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, UserRepository userRepository) {
        portfolioApp.g = userRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SummariesRepository summariesRepository) {
        portfolioApp.h = summariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SleepSummariesRepository sleepSummariesRepository) {
        portfolioApp.i = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, AlarmHelper alarmHelper) {
        portfolioApp.j = alarmHelper;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        portfolioApp.k = guestApiService;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, i42 i42) {
        portfolioApp.l = i42;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApiServiceV2 apiServiceV2) {
        portfolioApp.m = apiServiceV2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, yr3 yr3) {
        portfolioApp.n = yr3;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, k62 k62) {
        portfolioApp.o = k62;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        portfolioApp.p = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, AnalyticsHelper analyticsHelper) {
        portfolioApp.q = analyticsHelper;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApplicationEventListener applicationEventListener) {
        portfolioApp.r = applicationEventListener;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        portfolioApp.s = deviceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, yk2 yk2) {
        portfolioApp.t = yk2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ShakeFeedbackService shakeFeedbackService) {
        portfolioApp.u = shakeFeedbackService;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository) {
        portfolioApp.v = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, lu3 lu3) {
        portfolioApp.w = lu3;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchLocalizationRepository watchLocalizationRepository) {
        portfolioApp.x = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, hr3 hr3) {
        portfolioApp.y = hr3;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        portfolioApp.z = watchFaceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ko2 ko2) {
        portfolioApp.N = ko2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SmsMmsReceiver smsMmsReceiver) {
        portfolioApp.O = smsMmsReceiver;
    }
}
