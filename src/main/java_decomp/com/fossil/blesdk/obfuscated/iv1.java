package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.nio.CharBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iv1 {
    @DexIgnore
    public static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    @DexIgnore
    public static <T> T a(Readable readable, nv1<T> nv1) throws IOException {
        String a;
        tt1.a(readable);
        tt1.a(nv1);
        ov1 ov1 = new ov1(readable);
        do {
            a = ov1.a();
            if (a == null) {
                break;
            }
        } while (nv1.a(a));
        return nv1.getResult();
    }
}
