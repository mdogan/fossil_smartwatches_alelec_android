package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ce2 extends be2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.iv_back, 1);
        x.put(R.id.ftv_title, 2);
        x.put(R.id.fet_search_apps, 3);
        x.put(R.id.iv_clear, 4);
        x.put(R.id.line, 5);
        x.put(R.id.rv_apps, 6);
    }
    */

    @DexIgnore
    public ce2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 7, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public ce2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[3], objArr[2], objArr[1], objArr[4], objArr[5], objArr[6]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
