package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xb2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ ProgressBar D;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ OverviewDayGoalChart s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xb2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, CoordinatorLayout coordinatorLayout, OverviewDayGoalChart overviewDayGoalChart, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, ImageView imageView, ImageView imageView2, ImageView imageView3, View view2, ProgressBar progressBar, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView8, TextView textView, View view3) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = constraintLayout;
        this.s = overviewDayGoalChart;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = flexibleTextView4;
        this.w = flexibleTextView5;
        this.x = flexibleTextView6;
        this.y = flexibleTextView7;
        this.z = imageView;
        this.A = imageView2;
        this.B = imageView3;
        this.C = view2;
        this.D = progressBar;
        this.E = recyclerViewEmptySupport;
        this.F = flexibleTextView8;
    }
}
