package com.fossil.blesdk.obfuscated;

import androidx.work.impl.utils.futures.AbstractFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zl<V> extends AbstractFuture<V> {
    @DexIgnore
    public static <V> zl<V> e() {
        return new zl<>();
    }

    @DexIgnore
    public boolean a(Throwable th) {
        return super.a(th);
    }

    @DexIgnore
    public boolean b(V v) {
        return super.b(v);
    }

    @DexIgnore
    public boolean a(aw1<? extends V> aw1) {
        return super.a(aw1);
    }
}
