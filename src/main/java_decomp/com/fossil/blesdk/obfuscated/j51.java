package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j51 extends b51 implements h51 {
    @DexIgnore
    public j51(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    @DexIgnore
    public final boolean a(h51 h51) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) h51);
        Parcel a = a(16, o);
        boolean a2 = d51.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int k() throws RemoteException {
        Parcel a = a(17, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
