package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface io3 extends w52<ho3> {
    @DexIgnore
    void B0();

    @DexIgnore
    void F();

    @DexIgnore
    void N(String str);

    @DexIgnore
    void a(boolean z, boolean z2, String str);

    @DexIgnore
    void b(int i, String str);

    @DexIgnore
    void b(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void b(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    void c(boolean z, boolean z2);

    @DexIgnore
    void f(int i, String str);

    @DexIgnore
    void g();

    @DexIgnore
    void h();

    @DexIgnore
    void i();

    @DexIgnore
    void k();

    @DexIgnore
    void k0();

    @DexIgnore
    void z(boolean z);
}
