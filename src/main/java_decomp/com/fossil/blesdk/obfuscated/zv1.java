package com.fossil.blesdk.obfuscated;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zv1 {
    @DexIgnore
    public /* final */ Set<Type> a; // = yu1.a();

    @DexIgnore
    public void a(Class<?> cls) {
    }

    @DexIgnore
    public void a(GenericArrayType genericArrayType) {
    }

    @DexIgnore
    public void a(ParameterizedType parameterizedType) {
    }

    @DexIgnore
    public abstract void a(TypeVariable<?> typeVariable);

    @DexIgnore
    public abstract void a(WildcardType wildcardType);

    @DexIgnore
    public final void a(Type... typeArr) {
        for (TypeVariable typeVariable : typeArr) {
            if (typeVariable != null && this.a.add(typeVariable)) {
                try {
                    if (typeVariable instanceof TypeVariable) {
                        a((TypeVariable<?>) typeVariable);
                    } else if (typeVariable instanceof WildcardType) {
                        a((WildcardType) typeVariable);
                    } else if (typeVariable instanceof ParameterizedType) {
                        a((ParameterizedType) typeVariable);
                    } else if (typeVariable instanceof Class) {
                        a((Class<?>) typeVariable);
                    } else if (typeVariable instanceof GenericArrayType) {
                        a((GenericArrayType) typeVariable);
                    } else {
                        throw new AssertionError("Unknown type: " + typeVariable);
                    }
                } catch (Throwable th) {
                    this.a.remove(typeVariable);
                    throw th;
                }
            }
        }
    }
}
