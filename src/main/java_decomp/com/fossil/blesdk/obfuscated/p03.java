package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p03 implements Factory<ArrayList<ContactWrapper>> {
    @DexIgnore
    public static ArrayList<ContactWrapper> a(o03 o03) {
        ArrayList<ContactWrapper> a = o03.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
