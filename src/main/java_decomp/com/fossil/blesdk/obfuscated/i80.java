package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.device.logic.request.file.FileDataRequest;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i80 extends FileDataRequest {
    @DexIgnore
    public ArrayList<DeviceFile> X;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ i80(short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public void M() {
        g(L());
    }

    @DexIgnore
    public final ArrayList<DeviceFile> N() {
        return this.X;
    }

    @DexIgnore
    public final void g(byte[] bArr) {
        int i = 0;
        while (true) {
            int i2 = i + 10;
            if (i2 <= bArr.length) {
                ByteBuffer order = ByteBuffer.wrap(kb4.a(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
                this.X.add(new DeviceFile(i().k(), order.getShort(0), o90.b(order.getInt(2)), o90.b(order.getInt(6))));
                i = i2;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public JSONObject u() {
        JSONObject u = super.u();
        JSONKey jSONKey = JSONKey.FILE_LIST;
        Object[] array = this.X.toArray(new DeviceFile[0]);
        if (array != null) {
            return xa0.a(u, jSONKey, k00.a((JSONAbleObject[]) array));
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i80(short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.LIST_FILE, s, RequestId.LIST_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.X = new ArrayList<>();
    }
}
