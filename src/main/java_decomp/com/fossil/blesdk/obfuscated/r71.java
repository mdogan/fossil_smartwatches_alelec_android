package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r71 extends t71 {
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public /* final */ int f; // = this.g.size();
    @DexIgnore
    public /* final */ /* synthetic */ zzte g;

    @DexIgnore
    public r71(zzte zzte) {
        this.g = zzte;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e < this.f;
    }

    @DexIgnore
    public final byte nextByte() {
        int i = this.e;
        if (i < this.f) {
            this.e = i + 1;
            return this.g.zzan(i);
        }
        throw new NoSuchElementException();
    }
}
