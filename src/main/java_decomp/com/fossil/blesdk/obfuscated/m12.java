package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m12 {
    @DexIgnore
    public abstract m12 a(k12 k12) throws IOException;

    @DexIgnore
    public String toString() {
        return n12.a(this);
    }

    @DexIgnore
    public m12 clone() throws CloneNotSupportedException {
        return (m12) super.clone();
    }
}
