package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jh1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ gh1 e;

    @DexIgnore
    public jh1(gh1 gh1, String str, long j) {
        this.e = gh1;
        ck0.b(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.s().getLong(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(long j) {
        SharedPreferences.Editor edit = this.e.s().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
