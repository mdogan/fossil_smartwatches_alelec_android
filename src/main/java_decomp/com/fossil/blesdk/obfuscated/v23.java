package com.fossil.blesdk.obfuscated;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface v23 extends w52<u23> {
    @DexIgnore
    void D(String str);

    @DexIgnore
    void L(boolean z);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void a(CommuteTimeSetting commuteTimeSetting);

    @DexIgnore
    void b();

    @DexIgnore
    void d(String str, String str2);

    @DexIgnore
    void j(List<String> list);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    void w(String str);

    @DexIgnore
    void x(String str);
}
