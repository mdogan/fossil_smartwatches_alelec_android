package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class no2 implements Factory<SmsMmsReceiver> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<fn2> b;

    @DexIgnore
    public no2(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static no2 a(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        return new no2(provider, provider2);
    }

    @DexIgnore
    public static SmsMmsReceiver b(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        SmsMmsReceiver smsMmsReceiver = new SmsMmsReceiver();
        oo2.a(smsMmsReceiver, provider.get());
        oo2.a(smsMmsReceiver, provider2.get());
        return smsMmsReceiver;
    }

    @DexIgnore
    public SmsMmsReceiver get() {
        return b(this.a, this.b);
    }
}
