package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u73 implements Factory<ad3> {
    @DexIgnore
    public static ad3 a(o73 o73) {
        ad3 f = o73.f();
        o44.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
