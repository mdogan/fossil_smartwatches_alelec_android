package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lp implements ko {
    @DexIgnore
    public /* final */ ko b;
    @DexIgnore
    public /* final */ ko c;

    @DexIgnore
    public lp(ko koVar, ko koVar2) {
        this.b = koVar;
        this.c = koVar2;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof lp)) {
            return false;
        }
        lp lpVar = (lp) obj;
        if (!this.b.equals(lpVar.b) || !this.c.equals(lpVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
