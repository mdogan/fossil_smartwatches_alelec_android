package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class so0 {
    @DexIgnore
    public static /* final */ ee0<Object> a; // = a01.F;
    @DexIgnore
    public static /* final */ ee0<Object> b; // = uz0.F;
    @DexIgnore
    public static /* final */ vo0 c; // = new o11();
    @DexIgnore
    public static /* final */ ee0<Object> d; // = q21.F;
    @DexIgnore
    public static /* final */ ro0 e; // = new l11();

    /*
    static {
        ee0<Object> ee0 = e01.F;
        new s11();
        new q11();
        ee0<Object> ee02 = i01.F;
        new t11();
        ee0<Object> ee03 = qz0.F;
        new n11();
        ee0<Object> ee04 = m21.F;
        if (Build.VERSION.SDK_INT >= 18) {
            new k11();
        } else {
            new w11();
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static zo0 a(Context context, GoogleSignInAccount googleSignInAccount) {
        ck0.a(googleSignInAccount);
        return new zo0(context, to0.a(googleSignInAccount).a());
    }
}
