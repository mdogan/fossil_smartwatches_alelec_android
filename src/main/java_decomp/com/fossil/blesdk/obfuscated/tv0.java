package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tv0 extends vv0 {
    @DexIgnore
    void a(zzbn zzbn) throws IOException;

    @DexIgnore
    uv0 c();

    @DexIgnore
    zzbb d();

    @DexIgnore
    uv0 e();

    @DexIgnore
    int f();
}
