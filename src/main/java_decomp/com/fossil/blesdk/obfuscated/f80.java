package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.device.logic.request.code.FileControlStatusCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class f80 extends g70 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId I;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId J;
    @DexIgnore
    public /* final */ FileControlOperationCode K;
    @DexIgnore
    public /* final */ short L;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f80(FileControlOperationCode fileControlOperationCode, short s, RequestId requestId, Peripheral peripheral, int i) {
        super(requestId, peripheral, i);
        wd4.b(fileControlOperationCode, "operationCode");
        wd4.b(requestId, "requestId");
        wd4.b(peripheral, "peripheral");
        this.K = fileControlOperationCode;
        this.L = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.getCode()).putShort(this.L).array();
        wd4.a((Object) array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.K.responseCode()).putShort(this.L).array();
        wd4.a((Object) array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.FTC;
        this.I = characteristicId;
        this.J = characteristicId;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId B() {
        return this.J;
    }

    @DexIgnore
    public final byte[] D() {
        return this.G;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId E() {
        return this.I;
    }

    @DexIgnore
    public final byte[] G() {
        return this.H;
    }

    @DexIgnore
    public final short I() {
        return this.L;
    }

    @DexIgnore
    public final long a(d20 d20) {
        wd4.b(d20, "notification");
        if (d20.a() == GattCharacteristic.CharacteristicId.FTC && d20.b().length >= 9) {
            ByteBuffer order = ByteBuffer.wrap(d20.b()).order(ByteOrder.LITTLE_ENDIAN);
            byte b = order.get(0);
            short s = order.getShort(1);
            byte b2 = order.get(3);
            byte b3 = order.get(4);
            long b4 = o90.b(order.getInt(5));
            if (b == FileControlOperationCode.WAITING_REQUEST.responseCode() && this.L == s && b2 == FileControlStatusCode.SUCCESS.getCode() && b3 == this.K.getCode() && b4 > 0) {
                return b4;
            }
            return 0;
        }
        return 0;
    }

    @DexIgnore
    public final void b(byte[] bArr) {
        wd4.b(bArr, "<set-?>");
        this.H = bArr;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.FILE_HANDLE, o90.a(this.L));
    }

    @DexIgnore
    public final p70 b(byte b) {
        return FileControlStatusCode.Companion.a(b);
    }
}
