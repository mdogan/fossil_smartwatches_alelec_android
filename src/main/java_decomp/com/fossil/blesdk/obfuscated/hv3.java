package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hv3 implements Cloneable {
    @DexIgnore
    public static /* final */ List<Protocol> C; // = xv3.a((T[]) new Protocol[]{Protocol.HTTP_2, Protocol.SPDY_3, Protocol.HTTP_1_1});
    @DexIgnore
    public static /* final */ List<zu3> D; // = xv3.a((T[]) new zu3[]{zu3.f, zu3.g, zu3.h});
    @DexIgnore
    public static SSLSocketFactory E;
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ wv3 e;
    @DexIgnore
    public bv3 f;
    @DexIgnore
    public Proxy g;
    @DexIgnore
    public List<Protocol> h;
    @DexIgnore
    public List<zu3> i;
    @DexIgnore
    public /* final */ List<fv3> j;
    @DexIgnore
    public /* final */ List<fv3> k;
    @DexIgnore
    public ProxySelector l;
    @DexIgnore
    public CookieHandler m;
    @DexIgnore
    public rv3 n;
    @DexIgnore
    public su3 o;
    @DexIgnore
    public SocketFactory p;
    @DexIgnore
    public SSLSocketFactory q;
    @DexIgnore
    public HostnameVerifier r;
    @DexIgnore
    public vu3 s;
    @DexIgnore
    public ru3 t;
    @DexIgnore
    public yu3 u;
    @DexIgnore
    public tv3 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends qv3 {
        @DexIgnore
        public ex3 a(xu3 xu3, vw3 vw3) throws IOException {
            return xu3.a(vw3);
        }

        @DexIgnore
        public void b(xu3 xu3, vw3 vw3) {
            xu3.a((Object) vw3);
        }

        @DexIgnore
        public int c(xu3 xu3) {
            return xu3.m();
        }

        @DexIgnore
        public boolean a(xu3 xu3) {
            return xu3.a();
        }

        @DexIgnore
        public boolean b(xu3 xu3) {
            return xu3.l();
        }

        @DexIgnore
        public wv3 c(hv3 hv3) {
            return hv3.G();
        }

        @DexIgnore
        public void a(xu3 xu3, Protocol protocol) {
            xu3.a(protocol);
        }

        @DexIgnore
        public tv3 b(hv3 hv3) {
            return hv3.v;
        }

        @DexIgnore
        public void a(dv3.b bVar, String str) {
            bVar.a(str);
        }

        @DexIgnore
        public rv3 a(hv3 hv3) {
            return hv3.E();
        }

        @DexIgnore
        public void a(yu3 yu3, xu3 xu3) {
            yu3.b(xu3);
        }

        @DexIgnore
        public void a(hv3 hv3, xu3 xu3, vw3 vw3, iv3 iv3) throws RouteException {
            xu3.a(hv3, (Object) vw3, iv3);
        }

        @DexIgnore
        public void a(zu3 zu3, SSLSocket sSLSocket, boolean z) {
            zu3.a(sSLSocket, z);
        }
    }

    /*
    static {
        qv3.b = new a();
    }
    */

    @DexIgnore
    public hv3() {
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.w = true;
        this.x = true;
        this.y = true;
        this.z = 10000;
        this.A = 10000;
        this.B = 10000;
        this.e = new wv3();
        this.f = new bv3();
    }

    @DexIgnore
    public SocketFactory A() {
        return this.p;
    }

    @DexIgnore
    public SSLSocketFactory B() {
        return this.q;
    }

    @DexIgnore
    public int C() {
        return this.B;
    }

    @DexIgnore
    public List<fv3> D() {
        return this.j;
    }

    @DexIgnore
    public rv3 E() {
        return this.n;
    }

    @DexIgnore
    public List<fv3> F() {
        return this.k;
    }

    @DexIgnore
    public wv3 G() {
        return this.e;
    }

    @DexIgnore
    public void b(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.A = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public void c(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.B = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public int d() {
        return this.z;
    }

    @DexIgnore
    public yu3 e() {
        return this.u;
    }

    @DexIgnore
    public List<zu3> f() {
        return this.i;
    }

    @DexIgnore
    public CookieHandler g() {
        return this.m;
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|5|6|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    public final synchronized SSLSocketFactory h() {
        if (E == null) {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init((KeyManager[]) null, (TrustManager[]) null, (SecureRandom) null);
            E = instance.getSocketFactory();
            throw new AssertionError();
        }
        return E;
    }

    @DexIgnore
    public bv3 i() {
        return this.f;
    }

    @DexIgnore
    public boolean j() {
        return this.x;
    }

    @DexIgnore
    public boolean k() {
        return this.w;
    }

    @DexIgnore
    public HostnameVerifier l() {
        return this.r;
    }

    @DexIgnore
    public List<Protocol> m() {
        return this.h;
    }

    @DexIgnore
    public Proxy w() {
        return this.g;
    }

    @DexIgnore
    public ProxySelector x() {
        return this.l;
    }

    @DexIgnore
    public int y() {
        return this.A;
    }

    @DexIgnore
    public boolean z() {
        return this.y;
    }

    @DexIgnore
    public void a(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.z = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public hv3 clone() {
        return new hv3(this);
    }

    @DexIgnore
    public ru3 b() {
        return this.t;
    }

    @DexIgnore
    public vu3 c() {
        return this.s;
    }

    @DexIgnore
    public hv3 a(su3 su3) {
        this.o = su3;
        this.n = null;
        return this;
    }

    @DexIgnore
    public uu3 a(iv3 iv3) {
        return new uu3(this, iv3);
    }

    @DexIgnore
    public hv3 a() {
        hv3 hv3 = new hv3(this);
        if (hv3.l == null) {
            hv3.l = ProxySelector.getDefault();
        }
        if (hv3.m == null) {
            hv3.m = CookieHandler.getDefault();
        }
        if (hv3.p == null) {
            hv3.p = SocketFactory.getDefault();
        }
        if (hv3.q == null) {
            hv3.q = h();
        }
        if (hv3.r == null) {
            hv3.r = hx3.a;
        }
        if (hv3.s == null) {
            hv3.s = vu3.b;
        }
        if (hv3.t == null) {
            hv3.t = ow3.a;
        }
        if (hv3.u == null) {
            hv3.u = yu3.c();
        }
        if (hv3.h == null) {
            hv3.h = C;
        }
        if (hv3.i == null) {
            hv3.i = D;
        }
        if (hv3.v == null) {
            hv3.v = tv3.a;
        }
        return hv3;
    }

    @DexIgnore
    public hv3(hv3 hv3) {
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.w = true;
        this.x = true;
        this.y = true;
        this.z = 10000;
        this.A = 10000;
        this.B = 10000;
        this.e = hv3.e;
        this.f = hv3.f;
        this.g = hv3.g;
        this.h = hv3.h;
        this.i = hv3.i;
        this.j.addAll(hv3.j);
        this.k.addAll(hv3.k);
        this.l = hv3.l;
        this.m = hv3.m;
        this.o = hv3.o;
        su3 su3 = this.o;
        this.n = su3 != null ? su3.a : hv3.n;
        this.p = hv3.p;
        this.q = hv3.q;
        this.r = hv3.r;
        this.s = hv3.s;
        this.t = hv3.t;
        this.u = hv3.u;
        this.v = hv3.v;
        this.w = hv3.w;
        this.x = hv3.x;
        this.y = hv3.y;
        this.z = hv3.z;
        this.A = hv3.A;
        this.B = hv3.B;
    }
}
