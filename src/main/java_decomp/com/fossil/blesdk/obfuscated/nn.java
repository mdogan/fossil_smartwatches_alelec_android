package com.fossil.blesdk.obfuscated;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nn extends ByteArrayOutputStream {
    @DexIgnore
    public /* final */ cn e;

    @DexIgnore
    public nn(cn cnVar, int i) {
        this.e = cnVar;
        this.buf = this.e.a(Math.max(i, 256));
    }

    @DexIgnore
    public final void b(int i) {
        int i2 = this.count;
        if (i2 + i > this.buf.length) {
            byte[] a = this.e.a((i2 + i) * 2);
            System.arraycopy(this.buf, 0, a, 0, this.count);
            this.e.a(this.buf);
            this.buf = a;
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.a(this.buf);
        this.buf = null;
        super.close();
    }

    @DexIgnore
    public void finalize() {
        this.e.a(this.buf);
    }

    @DexIgnore
    public synchronized void write(byte[] bArr, int i, int i2) {
        b(i2);
        super.write(bArr, i, i2);
    }

    @DexIgnore
    public synchronized void write(int i) {
        b(1);
        super.write(i);
    }
}
