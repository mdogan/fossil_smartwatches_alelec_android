package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qq2 {
    @DexIgnore
    public String a;
    @DexIgnore
    public ArrayList<mq2> b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public qq2(String str, String str2, ArrayList<mq2> arrayList, boolean z) {
        wd4.b(str, "tagName");
        wd4.b(str2, "title");
        wd4.b(arrayList, "listItem");
        this.a = str2;
        this.b = arrayList;
        this.c = z;
    }

    @DexIgnore
    public final ArrayList<mq2> a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qq2(String str, String str2, ArrayList arrayList, boolean z, int i, rd4 rd4) {
        this(str, str2, arrayList, (i & 8) != 0 ? false : z);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.c = z;
    }
}
