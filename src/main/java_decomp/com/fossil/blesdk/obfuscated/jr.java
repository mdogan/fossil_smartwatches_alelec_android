package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jr implements tr<File, ByteBuffer> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements to<ByteBuffer> {
        @DexIgnore
        public /* final */ File e;

        @DexIgnore
        public a(File file) {
            this.e = file;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(lw.a(this.e));
            } catch (IOException e2) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e2);
                }
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ur<File, ByteBuffer> {
        @DexIgnore
        public tr<File, ByteBuffer> a(xr xrVar) {
            return new jr();
        }
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public tr.a<ByteBuffer> a(File file, int i, int i2, mo moVar) {
        return new tr.a<>(new kw(file), new a(file));
    }
}
