package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p12 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof p12) {
            p12 p12 = (p12) obj;
            if (this.a == p12.a && this.b == p12.b) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.a + "x" + this.b;
    }
}
