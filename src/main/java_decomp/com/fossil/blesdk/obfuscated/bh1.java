package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bh1 implements Runnable {
    @DexIgnore
    public /* final */ ah1 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Throwable g;
    @DexIgnore
    public /* final */ byte[] h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ Map<String, List<String>> j;

    @DexIgnore
    public bh1(String str, ah1 ah1, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        ck0.a(ah1);
        this.e = ah1;
        this.f = i2;
        this.g = th;
        this.h = bArr;
        this.i = str;
        this.j = map;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.i, this.f, this.g, this.h, this.j);
    }
}
