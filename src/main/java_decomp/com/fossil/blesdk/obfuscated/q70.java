package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigResponseStatusCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class q70 extends g70 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId I;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId J;
    @DexIgnore
    public /* final */ byte[] K;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ q70(DeviceConfigOperationCode deviceConfigOperationCode, RequestId requestId, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(deviceConfigOperationCode, requestId, peripheral, (i2 & 8) != 0 ? 3 : i);
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId B() {
        return this.J;
    }

    @DexIgnore
    public byte[] C() {
        return this.K;
    }

    @DexIgnore
    public final byte[] D() {
        return this.G;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId E() {
        return this.I;
    }

    @DexIgnore
    public final byte[] G() {
        return this.H;
    }

    @DexIgnore
    public final long a(d20 d20) {
        wd4.b(d20, "notification");
        return super.a(d20);
    }

    @DexIgnore
    public final p70 b(byte b) {
        return DeviceConfigResponseStatusCode.Companion.a(b);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q70(DeviceConfigOperationCode deviceConfigOperationCode, RequestId requestId, Peripheral peripheral, int i) {
        super(requestId, peripheral, i);
        wd4.b(deviceConfigOperationCode, "operation");
        wd4.b(requestId, "requestId");
        wd4.b(peripheral, "peripheral");
        this.G = deviceConfigOperationCode.getOperationCode();
        this.H = deviceConfigOperationCode.getResponseOperationCode();
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.DC;
        this.I = characteristicId;
        this.J = characteristicId;
        this.K = new byte[0];
    }
}
