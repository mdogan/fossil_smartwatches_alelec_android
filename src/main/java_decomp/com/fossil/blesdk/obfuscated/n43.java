package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n43 implements Factory<CustomizeThemePresenter> {
    @DexIgnore
    public static CustomizeThemePresenter a(k43 k43, WatchFaceRepository watchFaceRepository) {
        return new CustomizeThemePresenter(k43, watchFaceRepository);
    }
}
