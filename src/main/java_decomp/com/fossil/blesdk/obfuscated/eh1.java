package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eh1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ el1 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    /*
    static {
        Class<eh1> cls = eh1.class;
    }
    */

    @DexIgnore
    public eh1(el1 el1) {
        ck0.a(el1);
        this.a = el1;
    }

    @DexIgnore
    public final void a() {
        this.a.r();
        this.a.a().e();
        this.a.a().e();
        if (this.b) {
            this.a.d().A().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.getContext().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.a.d().s().a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @DexIgnore
    public final void b() {
        this.a.r();
        this.a.a().e();
        if (!this.b) {
            this.a.getContext().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.c = this.a.n().t();
            this.a.d().A().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
            this.b = true;
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        this.a.r();
        String action = intent.getAction();
        this.a.d().A().a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean t = this.a.n().t();
            if (this.c != t) {
                this.c = t;
                this.a.a().a((Runnable) new fh1(this, t));
                return;
            }
            return;
        }
        this.a.d().v().a("NetworkBroadcastReceiver received unknown action", action);
    }
}
