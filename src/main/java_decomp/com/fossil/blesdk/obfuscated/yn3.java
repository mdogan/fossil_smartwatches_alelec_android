package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ar4;
import com.fossil.blesdk.obfuscated.xs2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yn3 extends as2 implements xn3 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<ve2> j;
    @DexIgnore
    public wn3 k;
    @DexIgnore
    public xs2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return yn3.n;
        }

        @DexIgnore
        public final yn3 b() {
            return new yn3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yn3 e;

        @DexIgnore
        public b(yn3 yn3) {
            this.e = yn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements xs2.b {
        @DexIgnore
        public /* final */ /* synthetic */ yn3 a;

        @DexIgnore
        public c(yn3 yn3) {
            this.a = yn3;
        }

        @DexIgnore
        public void a(xs2.c cVar) {
            String[] strArr;
            wd4.b(cVar, "permissionModel");
            if (cVar.d() == 15) {
                if (br4.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_FINE_LOCATION")) {
                    Object[] array = cVar.e().toArray(new String[0]);
                    if (array != null) {
                        strArr = (String[]) array;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else {
                    Object[] array2 = ob4.c("android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_BACKGROUND_LOCATION").toArray(new String[0]);
                    if (array2 != null) {
                        strArr = (String[]) array2;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                br4.a((Fragment) this.a, cVar.c(), cVar.d(), (String[]) Arrays.copyOf(strArr, strArr.length));
                return;
            }
            yn3 yn3 = this.a;
            String c = cVar.c();
            int d = cVar.d();
            Object[] array3 = cVar.e().toArray(new String[0]);
            if (array3 != null) {
                String[] strArr2 = (String[]) array3;
                br4.a((Fragment) yn3, c, d, (String[]) Arrays.copyOf(strArr2, strArr2.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void b(xs2.c cVar) {
            wd4.b(cVar, "permissionModel");
            int d = cVar.d();
            if (d == 1) {
                this.a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
            } else if (d == 3) {
                this.a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }
    }

    /*
    static {
        String simpleName = yn3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "PermissionFragment::class.java.simpleName!!");
            n = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = n;
        local.d(str, "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        this.j = new ur3<>(this, (ve2) ra.a(layoutInflater, R.layout.fragment_permission, viewGroup, false, O0()));
        ur3<ve2> ur3 = this.j;
        if (ur3 != null) {
            ve2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        wn3 wn3 = this.k;
        if (wn3 != null) {
            wn3.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        wn3 wn3 = this.k;
        if (wn3 != null) {
            wn3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<ve2> ur3 = this.j;
        if (ur3 != null) {
            ve2 a2 = ur3.a();
            if (a2 != null) {
                ImageView imageView = a2.r;
                if (imageView != null) {
                    imageView.setOnClickListener(new b(this));
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t(List<xs2.c> list) {
        wd4.b(list, "listPermissionModel");
        if (this.l == null) {
            int i = 0;
            if (list.size() > 1) {
                ur3<ve2> ur3 = this.j;
                if (ur3 != null) {
                    ve2 a2 = ur3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.q;
                        if (flexibleTextView != null) {
                            flexibleTextView.setVisibility(0);
                        }
                    }
                    i = 1;
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else {
                ur3<ve2> ur32 = this.j;
                if (ur32 != null) {
                    ve2 a3 = ur32.a();
                    if (a3 != null) {
                        FlexibleTextView flexibleTextView2 = a3.q;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setVisibility(8);
                        }
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            }
            xs2 xs2 = new xs2(i);
            xs2.a((xs2.b) new c(this));
            this.l = xs2;
            ur3<ve2> ur33 = this.j;
            if (ur33 != null) {
                ve2 a4 = ur33.a();
                RecyclerView recyclerView = a4 != null ? a4.s : null;
                if (recyclerView != null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setAdapter(this.l);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
        xs2 xs22 = this.l;
        if (xs22 != null) {
            xs22.a(list);
        }
    }

    @DexIgnore
    public void a(wn3 wn3) {
        wd4.b(wn3, "presenter");
        this.k = wn3;
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(n, "onPermissionsDenied:" + i + ':' + list.size());
        if (br4.a((Fragment) this, list)) {
            boolean z = !wb4.b(cn2.d.a(100), (ArrayList<String>) list).isEmpty();
            boolean z2 = !wb4.b(cn2.d.a(101), (ArrayList<String>) list).isEmpty();
            if (z) {
                ar4.b bVar = new ar4.b(this);
                bVar.b(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.calls_and_messages_permission));
                bVar.a(cn2.d.b(100).getSecond());
                bVar.a().b();
            } else if (z2) {
                ar4.b bVar2 = new ar4.b(this);
                bVar2.b(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.calls_and_messages_permission));
                bVar2.a(cn2.d.b(101).getSecond());
                bVar2.a().b();
            } else {
                new ar4.b(this).a().b();
            }
        }
    }
}
