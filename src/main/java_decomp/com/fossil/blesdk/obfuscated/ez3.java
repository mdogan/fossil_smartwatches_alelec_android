package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.dz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ez3 implements dz3.a {
    @DexIgnore
    public ez3() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public final int a() {
        return dz3.a;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (dz3.a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        if (dz3.a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        if (dz3.a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    public final void i(String str, String str2) {
        if (dz3.a <= 4) {
            Log.e(str, str2);
        }
    }
}
