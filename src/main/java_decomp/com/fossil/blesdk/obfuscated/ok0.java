package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ok0 extends ee0.a<vk0, Object> {
    @DexIgnore
    public final /* synthetic */ ee0.f a(Context context, Looper looper, lj0 lj0, Object obj, he0.b bVar, he0.c cVar) {
        return new vk0(context, looper, lj0, bVar, cVar);
    }
}
