package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class el1 implements wi1 {
    @DexIgnore
    public static volatile el1 y;
    @DexIgnore
    public th1 a;
    @DexIgnore
    public yg1 b;
    @DexIgnore
    public bm1 c;
    @DexIgnore
    public eh1 d;
    @DexIgnore
    public al1 e;
    @DexIgnore
    public ul1 f;
    @DexIgnore
    public /* final */ kl1 g;
    @DexIgnore
    public pj1 h;
    @DexIgnore
    public /* final */ yh1 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public long m;
    @DexIgnore
    public List<Runnable> n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public FileLock t;
    @DexIgnore
    public FileChannel u;
    @DexIgnore
    public List<Long> v;
    @DexIgnore
    public List<Long> w;
    @DexIgnore
    public long x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements dm1 {
        @DexIgnore
        public g61 a;
        @DexIgnore
        public List<Long> b;
        @DexIgnore
        public List<d61> c;
        @DexIgnore
        public long d;

        @DexIgnore
        public a(el1 el1) {
        }

        @DexIgnore
        public final void a(g61 g61) {
            ck0.a(g61);
            this.a = g61;
        }

        @DexIgnore
        public /* synthetic */ a(el1 el1, fl1 fl1) {
            this(el1);
        }

        @DexIgnore
        public final boolean a(long j, d61 d61) {
            ck0.a(d61);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && a(this.c.get(0)) != a(d61)) {
                return false;
            }
            long b2 = this.d + ((long) d61.b());
            if (b2 >= ((long) Math.max(0, kg1.s.a().intValue()))) {
                return false;
            }
            this.d = b2;
            this.c.add(d61);
            this.b.add(Long.valueOf(j));
            if (this.c.size() >= Math.max(1, kg1.t.a().intValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public static long a(d61 d61) {
            return ((d61.e.longValue() / 1000) / 60) / 60;
        }
    }

    @DexIgnore
    public el1(jl1 jl1) {
        this(jl1, (yh1) null);
    }

    @DexIgnore
    public static el1 a(Context context) {
        ck0.a(context);
        ck0.a(context.getApplicationContext());
        if (y == null) {
            synchronized (el1.class) {
                if (y == null) {
                    y = new el1(new jl1(context));
                }
            }
        }
        return y;
    }

    @DexIgnore
    public final void A() {
        this.p++;
    }

    @DexIgnore
    public final yh1 B() {
        return this.i;
    }

    @DexIgnore
    public final vl1 b() {
        return this.i.b();
    }

    @DexIgnore
    public final im0 c() {
        return this.i.c();
    }

    @DexIgnore
    public final ug1 d() {
        return this.i.d();
    }

    @DexIgnore
    public final void e() {
        this.i.a().e();
        l().z();
        if (this.i.t().e.a() == 0) {
            this.i.t().e.a(this.i.c().b());
        }
        v();
    }

    @DexIgnore
    public final void f() {
        this.i.a().e();
    }

    @DexIgnore
    public final sg1 g() {
        return this.i.r();
    }

    @DexIgnore
    public final Context getContext() {
        return this.i.getContext();
    }

    @DexIgnore
    public final ol1 h() {
        return this.i.s();
    }

    @DexIgnore
    public final yl1 i() {
        return this.i.u();
    }

    @DexIgnore
    public final kl1 j() {
        b((dl1) this.g);
        return this.g;
    }

    @DexIgnore
    public final ul1 k() {
        b((dl1) this.f);
        return this.f;
    }

    @DexIgnore
    public final bm1 l() {
        b((dl1) this.c);
        return this.c;
    }

    @DexIgnore
    public final th1 m() {
        b((dl1) this.a);
        return this.a;
    }

    @DexIgnore
    public final yg1 n() {
        b((dl1) this.b);
        return this.b;
    }

    @DexIgnore
    public final eh1 o() {
        eh1 eh1 = this.d;
        if (eh1 != null) {
            return eh1;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    @DexIgnore
    public final al1 p() {
        b((dl1) this.e);
        return this.e;
    }

    @DexIgnore
    public final pj1 q() {
        b((dl1) this.h);
        return this.h;
    }

    @DexIgnore
    public final void r() {
        if (!this.j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    @DexIgnore
    public final long s() {
        long b2 = this.i.c().b();
        gh1 t2 = this.i.t();
        t2.n();
        t2.e();
        long a2 = t2.i.a();
        if (a2 == 0) {
            a2 = 1 + ((long) t2.j().t().nextInt(DateTimeConstants.MILLIS_PER_DAY));
            t2.i.a(a2);
        }
        return ((((b2 + a2) / 1000) / 60) / 60) / 24;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:92|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r1.i.d().s().a("Failed to parse upload URL. Not uploading. appId", com.fossil.blesdk.obfuscated.ug1.a(r5), r6);
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:92:0x02a0 */
    public final void t() {
        String str;
        f();
        r();
        this.s = true;
        try {
            this.i.b();
            Boolean I = this.i.m().I();
            if (I == null) {
                this.i.d().v().a("Upload data called on the client side before use of service was decided");
            } else if (I.booleanValue()) {
                this.i.d().s().a("Upload called in the client side when service should be used");
                this.s = false;
                w();
            } else if (this.m > 0) {
                v();
                this.s = false;
                w();
            } else {
                f();
                if (this.v != null) {
                    this.i.d().A().a("Uploading requested multiple times");
                    this.s = false;
                    w();
                } else if (!n().t()) {
                    this.i.d().A().a("Network not connected, ignoring upload request");
                    v();
                    this.s = false;
                    w();
                } else {
                    long b2 = this.i.c().b();
                    String str2 = null;
                    a((String) null, b2 - yl1.u());
                    long a2 = this.i.t().e.a();
                    if (a2 != 0) {
                        this.i.d().z().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(b2 - a2)));
                    }
                    String x2 = l().x();
                    if (!TextUtils.isEmpty(x2)) {
                        if (this.x == -1) {
                            this.x = l().E();
                        }
                        List<Pair<g61, Long>> a3 = l().a(x2, this.i.u().b(x2, kg1.q), Math.max(0, this.i.u().b(x2, kg1.r)));
                        if (!a3.isEmpty()) {
                            Iterator<Pair<g61, Long>> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                g61 g61 = (g61) it.next().first;
                                if (!TextUtils.isEmpty(g61.u)) {
                                    str = g61.u;
                                    break;
                                }
                            }
                            if (str != null) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= a3.size()) {
                                        break;
                                    }
                                    g61 g612 = (g61) a3.get(i2).first;
                                    if (!TextUtils.isEmpty(g612.u) && !g612.u.equals(str)) {
                                        a3 = a3.subList(0, i2);
                                        break;
                                    }
                                    i2++;
                                }
                            }
                            f61 f61 = new f61();
                            f61.c = new g61[a3.size()];
                            ArrayList arrayList = new ArrayList(a3.size());
                            boolean z = yl1.v() && this.i.u().c(x2);
                            for (int i3 = 0; i3 < f61.c.length; i3++) {
                                f61.c[i3] = (g61) a3.get(i3).first;
                                arrayList.add((Long) a3.get(i3).second);
                                f61.c[i3].t = Long.valueOf(this.i.u().n());
                                f61.c[i3].f = Long.valueOf(b2);
                                g61 g613 = f61.c[i3];
                                this.i.b();
                                g613.B = false;
                                if (!z) {
                                    f61.c[i3].K = null;
                                }
                            }
                            if (this.i.d().a(2)) {
                                str2 = j().b(f61);
                            }
                            byte[] a4 = j().a(f61);
                            String a5 = kg1.A.a();
                            URL url = new URL(a5);
                            ck0.a(!arrayList.isEmpty());
                            if (this.v != null) {
                                this.i.d().s().a("Set uploading progress before finishing the previous upload");
                            } else {
                                this.v = new ArrayList(arrayList);
                            }
                            this.i.t().f.a(b2);
                            String str3 = "?";
                            if (f61.c.length > 0) {
                                str3 = f61.c[0].q;
                            }
                            this.i.d().A().a("Uploading data. app, uncompressed size, data", str3, Integer.valueOf(a4.length), str2);
                            this.r = true;
                            yg1 n2 = n();
                            gl1 gl1 = new gl1(this, x2);
                            n2.e();
                            n2.q();
                            ck0.a(url);
                            ck0.a(a4);
                            ck0.a(gl1);
                            n2.a().b((Runnable) new dh1(n2, x2, url, a4, (Map<String, String>) null, gl1));
                        }
                    } else {
                        this.x = -1;
                        String a6 = l().a(b2 - yl1.u());
                        if (!TextUtils.isEmpty(a6)) {
                            rl1 b3 = l().b(a6);
                            if (b3 != null) {
                                a(b3);
                            }
                        }
                    }
                    this.s = false;
                    w();
                }
            }
        } finally {
            this.s = false;
            w();
        }
    }

    @DexIgnore
    public final boolean u() {
        f();
        r();
        return l().C() || !TextUtils.isEmpty(l().x());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01be  */
    public final void v() {
        long j2;
        long j3;
        f();
        r();
        if (z() || this.i.u().a(kg1.o0)) {
            if (this.m > 0) {
                long abs = 3600000 - Math.abs(this.i.c().c() - this.m);
                if (abs > 0) {
                    this.i.d().A().a("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    o().a();
                    p().t();
                    return;
                }
                this.m = 0;
            }
            if (!this.i.H() || !u()) {
                this.i.d().A().a("Nothing to upload or uploading impossible");
                o().a();
                p().t();
                return;
            }
            long b2 = this.i.c().b();
            long max = Math.max(0, kg1.K.a().longValue());
            boolean z = l().D() || l().y();
            if (z) {
                String q2 = this.i.u().q();
                if (TextUtils.isEmpty(q2) || ".none.".equals(q2)) {
                    j2 = Math.max(0, kg1.E.a().longValue());
                } else {
                    j2 = Math.max(0, kg1.F.a().longValue());
                }
            } else {
                j2 = Math.max(0, kg1.D.a().longValue());
            }
            long a2 = this.i.t().e.a();
            long a3 = this.i.t().f.a();
            long j4 = j2;
            long j5 = max;
            long max2 = Math.max(l().A(), l().B());
            if (max2 != 0) {
                long abs2 = b2 - Math.abs(max2 - b2);
                long abs3 = b2 - Math.abs(a2 - b2);
                long abs4 = b2 - Math.abs(a3 - b2);
                long max3 = Math.max(abs3, abs4);
                long j6 = abs2 + j5;
                if (z && max3 > 0) {
                    j6 = Math.min(abs2, max3) + j4;
                }
                long j7 = j4;
                j3 = !j().a(max3, j7) ? max3 + j7 : j6;
                if (abs4 != 0 && abs4 >= abs2) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= Math.min(20, Math.max(0, kg1.M.a().intValue()))) {
                            break;
                        }
                        j3 += Math.max(0, kg1.L.a().longValue()) * (1 << i2);
                        if (j3 > abs4) {
                            break;
                        }
                        i2++;
                    }
                }
                if (j3 != 0) {
                    this.i.d().A().a("Next upload time is 0");
                    o().a();
                    p().t();
                    return;
                } else if (!n().t()) {
                    this.i.d().A().a("No network");
                    o().b();
                    p().t();
                    return;
                } else {
                    long a4 = this.i.t().g.a();
                    long max4 = Math.max(0, kg1.B.a().longValue());
                    if (!j().a(a4, max4)) {
                        j3 = Math.max(j3, a4 + max4);
                    }
                    o().a();
                    long b3 = j3 - this.i.c().b();
                    if (b3 <= 0) {
                        b3 = Math.max(0, kg1.G.a().longValue());
                        this.i.t().e.a(this.i.c().b());
                    }
                    this.i.d().A().a("Upload scheduled in approximately ms", Long.valueOf(b3));
                    p().a(b3);
                    return;
                }
            }
            j3 = 0;
            if (j3 != 0) {
            }
        }
    }

    @DexIgnore
    public final void w() {
        f();
        if (this.q || this.r || this.s) {
            this.i.d().A().a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.q), Boolean.valueOf(this.r), Boolean.valueOf(this.s));
            return;
        }
        this.i.d().A().a("Stopping uploading service(s)");
        List<Runnable> list = this.n;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.n.clear();
        }
    }

    @DexIgnore
    public final boolean x() {
        f();
        try {
            this.u = new RandomAccessFile(new File(this.i.getContext().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.t = this.u.tryLock();
            if (this.t != null) {
                this.i.d().A().a("Storage concurrent access okay");
                return true;
            }
            this.i.d().s().a("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e2) {
            this.i.d().s().a("Failed to acquire storage lock", e2);
            return false;
        } catch (IOException e3) {
            this.i.d().s().a("Failed to access storage lock file", e3);
            return false;
        }
    }

    @DexIgnore
    public final void y() {
        f();
        r();
        if (!this.l) {
            this.l = true;
            f();
            r();
            if ((this.i.u().a(kg1.o0) || z()) && x()) {
                int a2 = a(this.u);
                int E = this.i.l().E();
                f();
                if (a2 > E) {
                    this.i.d().s().a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                } else if (a2 < E) {
                    if (a(E, this.u)) {
                        this.i.d().A().a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    } else {
                        this.i.d().s().a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    }
                }
            }
        }
        if (!this.k && !this.i.u().a(kg1.o0)) {
            this.i.d().y().a("This instance being marked as an uploader");
            this.k = true;
            v();
        }
    }

    @DexIgnore
    public final boolean z() {
        f();
        r();
        return this.k;
    }

    @DexIgnore
    public el1(jl1 jl1, yh1 yh1) {
        this.j = false;
        ck0.a(jl1);
        this.i = yh1.a(jl1.a, (pg1) null);
        this.x = -1;
        kl1 kl1 = new kl1(this);
        kl1.s();
        this.g = kl1;
        yg1 yg1 = new yg1(this);
        yg1.s();
        this.b = yg1;
        th1 th1 = new th1(this);
        th1.s();
        this.a = th1;
        this.i.a().a((Runnable) new fl1(this, jl1));
    }

    @DexIgnore
    public static void b(dl1 dl1) {
        if (dl1 == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!dl1.p()) {
            String valueOf = String.valueOf(dl1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0332 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x035f A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03eb A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x041b A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01d2 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01df A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f1 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02d4 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    public final void c(sl1 sl1) {
        rl1 b2;
        int i2;
        eg1 eg1;
        String str;
        long i3;
        PackageInfo packageInfo;
        int i4;
        ApplicationInfo applicationInfo;
        boolean z;
        bm1 l2;
        String f2;
        sl1 sl12 = sl1;
        f();
        r();
        ck0.a(sl1);
        ck0.b(sl12.e);
        if (!TextUtils.isEmpty(sl12.f) || !TextUtils.isEmpty(sl12.v)) {
            rl1 b3 = l().b(sl12.e);
            if (b3 != null && TextUtils.isEmpty(b3.c()) && !TextUtils.isEmpty(sl12.f)) {
                b3.j(0);
                l().a(b3);
                m().e(sl12.e);
            }
            if (!sl12.l) {
                d(sl1);
                return;
            }
            long j2 = sl12.q;
            if (j2 == 0) {
                j2 = this.i.c().b();
            }
            int i5 = sl12.r;
            if (!(i5 == 0 || i5 == 1)) {
                this.i.d().v().a("Incorrect app type, assuming installed app. appId, appType", ug1.a(sl12.e), Integer.valueOf(i5));
                i5 = 0;
            }
            l().t();
            try {
                b2 = l().b(sl12.e);
                if (b2 != null) {
                    this.i.s();
                    if (ol1.a(sl12.f, b2.c(), sl12.v, b2.h())) {
                        this.i.d().v().a("New GMP App Id passed in. Removing cached database data. appId", ug1.a(b2.f()));
                        l2 = l();
                        f2 = b2.f();
                        l2.q();
                        l2.e();
                        ck0.b(f2);
                        SQLiteDatabase v2 = l2.v();
                        String[] strArr = {f2};
                        int delete = v2.delete("events", "app_id=?", strArr) + 0 + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("apps", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("event_filters", "app_id=?", strArr) + v2.delete("property_filters", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr);
                        if (delete > 0) {
                            l2.d().A().a("Deleted application data. app, records", f2, Integer.valueOf(delete));
                        }
                        b2 = null;
                    }
                }
            } catch (SQLiteException e2) {
                l2.d().s().a("Error deleting application data. appId, error", ug1.a(f2), e2);
            } catch (Throwable th) {
                l().u();
                throw th;
            }
            if (b2 != null) {
                if (b2.l() == -2147483648L) {
                    i2 = 1;
                    if (b2.e() != null && !b2.e().equals(sl12.g)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", b2.e());
                        a(new ig1("_au", new fg1(bundle), "auto", j2), sl12);
                    }
                } else if (b2.l() != sl12.n) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", b2.e());
                    fg1 fg1 = new fg1(bundle2);
                    i2 = 1;
                    a(new ig1("_au", fg1, "auto", j2), sl12);
                }
                d(sl1);
                if (i5 != 0) {
                    eg1 = l().b(sl12.e, "_f");
                } else {
                    eg1 = i5 == i2 ? l().b(sl12.e, "_v") : null;
                }
                if (eg1 != null) {
                    long j3 = j2;
                    long j4 = ((j2 / 3600000) + 1) * 3600000;
                    if (i5 == 0) {
                        str = "_et";
                        a(new ll1("_fot", j3, Long.valueOf(j4), "auto"), sl12);
                        if (this.i.u().m(sl12.f)) {
                            f();
                            this.i.w().a(sl12.e);
                        }
                        f();
                        r();
                        Bundle bundle3 = new Bundle();
                        bundle3.putLong("_c", 1);
                        bundle3.putLong("_r", 1);
                        bundle3.putLong("_uwa", 0);
                        bundle3.putLong("_pfo", 0);
                        bundle3.putLong("_sys", 0);
                        bundle3.putLong("_sysu", 0);
                        if (this.i.u().s(sl12.e)) {
                            bundle3.putLong(str, 1);
                        }
                        if (this.i.u().i(sl12.e) && sl12.u) {
                            bundle3.putLong("_dac", 1);
                        }
                        if (this.i.getContext().getPackageManager() == null) {
                            this.i.d().s().a("PackageManager is null, first open report might be inaccurate. appId", ug1.a(sl12.e));
                        } else {
                            try {
                                try {
                                    packageInfo = cn0.b(this.i.getContext()).b(sl12.e, 0);
                                } catch (PackageManager.NameNotFoundException e3) {
                                    e = e3;
                                    this.i.d().s().a("Package info is null, first open report might be inaccurate. appId", ug1.a(sl12.e), e);
                                    packageInfo = null;
                                    if (packageInfo != null) {
                                    }
                                    i4 = 0;
                                    applicationInfo = cn0.b(this.i.getContext()).a(sl12.e, i4);
                                    if (applicationInfo != null) {
                                    }
                                    bm1 l3 = l();
                                    String str2 = sl12.e;
                                    ck0.b(str2);
                                    l3.e();
                                    l3.q();
                                    i3 = l3.i(str2, "first_open_count");
                                    if (i3 >= 0) {
                                    }
                                    a(new ig1("_f", new fg1(bundle3), "auto", j3), sl12);
                                    if (!this.i.u().d(sl12.e, kg1.m0)) {
                                    }
                                    l().w();
                                    l().u();
                                }
                            } catch (PackageManager.NameNotFoundException e4) {
                                e = e4;
                                this.i.d().s().a("Package info is null, first open report might be inaccurate. appId", ug1.a(sl12.e), e);
                                packageInfo = null;
                                if (packageInfo != null) {
                                }
                                i4 = 0;
                                applicationInfo = cn0.b(this.i.getContext()).a(sl12.e, i4);
                                if (applicationInfo != null) {
                                }
                                bm1 l32 = l();
                                String str22 = sl12.e;
                                ck0.b(str22);
                                l32.e();
                                l32.q();
                                i3 = l32.i(str22, "first_open_count");
                                if (i3 >= 0) {
                                }
                                a(new ig1("_f", new fg1(bundle3), "auto", j3), sl12);
                                if (!this.i.u().d(sl12.e, kg1.m0)) {
                                }
                                l().w();
                                l().u();
                            }
                            if (packageInfo != null || packageInfo.firstInstallTime == 0) {
                                i4 = 0;
                            } else {
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle3.putLong("_uwa", 1);
                                    z = false;
                                } else {
                                    z = true;
                                }
                                i4 = 0;
                                a(new ll1("_fi", j3, Long.valueOf(z ? 1 : 0), "auto"), sl12);
                            }
                            try {
                                applicationInfo = cn0.b(this.i.getContext()).a(sl12.e, i4);
                            } catch (PackageManager.NameNotFoundException e5) {
                                this.i.d().s().a("Application info is null, first open report might be inaccurate. appId", ug1.a(sl12.e), e5);
                                applicationInfo = null;
                            }
                            if (applicationInfo != null) {
                                if ((applicationInfo.flags & 1) != 0) {
                                    bundle3.putLong("_sys", 1);
                                }
                                if ((applicationInfo.flags & 128) != 0) {
                                    bundle3.putLong("_sysu", 1);
                                }
                            }
                        }
                        bm1 l322 = l();
                        String str222 = sl12.e;
                        ck0.b(str222);
                        l322.e();
                        l322.q();
                        i3 = l322.i(str222, "first_open_count");
                        if (i3 >= 0) {
                            bundle3.putLong("_pfo", i3);
                        }
                        a(new ig1("_f", new fg1(bundle3), "auto", j3), sl12);
                    } else {
                        str = "_et";
                        if (i5 == 1) {
                            a(new ll1("_fvt", j3, Long.valueOf(j4), "auto"), sl12);
                            f();
                            r();
                            Bundle bundle4 = new Bundle();
                            bundle4.putLong("_c", 1);
                            bundle4.putLong("_r", 1);
                            if (this.i.u().s(sl12.e)) {
                                bundle4.putLong(str, 1);
                            }
                            if (this.i.u().i(sl12.e) && sl12.u) {
                                bundle4.putLong("_dac", 1);
                            }
                            a(new ig1("_v", new fg1(bundle4), "auto", j3), sl12);
                        }
                    }
                    if (!this.i.u().d(sl12.e, kg1.m0)) {
                        Bundle bundle5 = new Bundle();
                        bundle5.putLong(str, 1);
                        if (this.i.u().s(sl12.e)) {
                            bundle5.putLong("_fr", 1);
                        }
                        a(new ig1("_e", new fg1(bundle5), "auto", j3), sl12);
                    }
                } else {
                    long j5 = j2;
                    if (sl12.m) {
                        a(new ig1("_cd", new fg1(new Bundle()), "auto", j5), sl12);
                    }
                }
                l().w();
                l().u();
            }
            i2 = 1;
            d(sl1);
            if (i5 != 0) {
            }
            if (eg1 != null) {
            }
            l().w();
            l().u();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0156  */
    public final rl1 d(sl1 sl1) {
        boolean z;
        f();
        r();
        ck0.a(sl1);
        ck0.b(sl1.e);
        rl1 b2 = l().b(sl1.e);
        String b3 = this.i.t().b(sl1.e);
        if (b2 == null) {
            b2 = new rl1(this.i, sl1.e);
            b2.b(this.i.s().v());
            b2.e(b3);
        } else if (!b3.equals(b2.i())) {
            b2.e(b3);
            b2.b(this.i.s().v());
        } else {
            z = false;
            if (!TextUtils.equals(sl1.f, b2.c())) {
                b2.c(sl1.f);
                z = true;
            }
            if (!TextUtils.equals(sl1.v, b2.h())) {
                b2.d(sl1.v);
                z = true;
            }
            if (!TextUtils.isEmpty(sl1.o) && !sl1.o.equals(b2.b())) {
                b2.f(sl1.o);
                z = true;
            }
            long j2 = sl1.i;
            if (!(j2 == 0 || j2 == b2.n())) {
                b2.g(sl1.i);
                z = true;
            }
            if (!TextUtils.isEmpty(sl1.g) && !sl1.g.equals(b2.e())) {
                b2.a(sl1.g);
                z = true;
            }
            if (sl1.n != b2.l()) {
                b2.f(sl1.n);
                z = true;
            }
            String str = sl1.h;
            if (str != null && !str.equals(b2.m())) {
                b2.g(sl1.h);
                z = true;
            }
            if (sl1.j != b2.o()) {
                b2.h(sl1.j);
                z = true;
            }
            if (sl1.l != b2.d()) {
                b2.a(sl1.l);
                z = true;
            }
            if (!TextUtils.isEmpty(sl1.k) && !sl1.k.equals(b2.z())) {
                b2.h(sl1.k);
                z = true;
            }
            if (sl1.p != b2.B()) {
                b2.c(sl1.p);
                z = true;
            }
            if (sl1.s != b2.C()) {
                b2.b(sl1.s);
                z = true;
            }
            if (sl1.t != b2.D()) {
                b2.c(sl1.t);
                z = true;
            }
            if (z) {
                l().a(b2);
            }
            return b2;
        }
        z = true;
        if (!TextUtils.equals(sl1.f, b2.c())) {
        }
        if (!TextUtils.equals(sl1.v, b2.h())) {
        }
        b2.f(sl1.o);
        z = true;
        long j22 = sl1.i;
        b2.g(sl1.i);
        z = true;
        b2.a(sl1.g);
        z = true;
        if (sl1.n != b2.l()) {
        }
        String str2 = sl1.h;
        b2.g(sl1.h);
        z = true;
        if (sl1.j != b2.o()) {
        }
        if (sl1.l != b2.d()) {
        }
        b2.h(sl1.k);
        z = true;
        if (sl1.p != b2.B()) {
        }
        if (sl1.s != b2.C()) {
        }
        if (sl1.t != b2.D()) {
        }
        if (z) {
        }
        return b2;
    }

    @DexIgnore
    public final void b(sl1 sl1) {
        f();
        r();
        ck0.b(sl1.e);
        d(sl1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:194:0x074d, code lost:
        r2 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0261 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0299 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02e6 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0313  */
    public final void b(ig1 ig1, sl1 sl1) {
        long j2;
        long intValue;
        cm1 cm1;
        eg1 eg1;
        long j3;
        nl1 nl1;
        ig1 ig12 = ig1;
        sl1 sl12 = sl1;
        ck0.a(sl1);
        ck0.b(sl12.e);
        long nanoTime = System.nanoTime();
        f();
        r();
        String str = sl12.e;
        if (j().a(ig12, sl12)) {
            if (!sl12.l) {
                d(sl12);
                return;
            }
            boolean z = false;
            if (m().b(str, ig12.e)) {
                this.i.d().v().a("Dropping blacklisted event. appId", ug1.a(str), this.i.r().a(ig12.e));
                if (m().g(str) || m().h(str)) {
                    z = true;
                }
                if (!z && !"_err".equals(ig12.e)) {
                    this.i.s().a(str, 11, "_ev", ig12.e, 0);
                }
                if (z) {
                    rl1 b2 = l().b(str);
                    if (b2 != null) {
                        if (Math.abs(this.i.c().b() - Math.max(b2.r(), b2.q())) > kg1.J.a().longValue()) {
                            this.i.d().z().a("Fetching config for blacklisted app");
                            a(b2);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            if (this.i.d().a(2)) {
                this.i.d().A().a("Logging event", this.i.r().a(ig12));
            }
            l().t();
            d(sl12);
            if (!"_iap".equals(ig12.e)) {
                if (!"ecommerce_purchase".equals(ig12.e)) {
                    j2 = nanoTime;
                    boolean e2 = ol1.e(ig12.e);
                    boolean equals = "_err".equals(ig12.e);
                    String str2 = str;
                    cm1 a2 = l().a(s(), str, true, e2, false, equals, false);
                    intValue = a2.b - ((long) kg1.u.a().intValue());
                    if (intValue > 0) {
                        if (intValue % 1000 == 1) {
                            this.i.d().s().a("Data loss. Too many events logged. appId, count", ug1.a(str2), Long.valueOf(a2.b));
                        }
                        l().w();
                        l().u();
                        return;
                    }
                    if (e2) {
                        cm1 cm12 = a2;
                        long intValue2 = a2.a - ((long) kg1.w.a().intValue());
                        if (intValue2 > 0) {
                            if (intValue2 % 1000 == 1) {
                                this.i.d().s().a("Data loss. Too many public events logged. appId, count", ug1.a(str2), Long.valueOf(cm12.a));
                            }
                            this.i.s().a(str2, 16, "_ev", ig12.e, 0);
                            l().w();
                            l().u();
                            return;
                        }
                        cm1 = cm12;
                    } else {
                        cm1 = a2;
                    }
                    if (equals) {
                        long max = cm1.d - ((long) Math.max(0, Math.min(1000000, this.i.u().b(sl12.e, kg1.v))));
                        if (max > 0) {
                            if (max == 1) {
                                this.i.d().s().a("Too many error events logged. appId, count", ug1.a(str2), Long.valueOf(cm1.d));
                            }
                            l().w();
                            l().u();
                            return;
                        }
                    }
                    Bundle H = ig12.f.H();
                    this.i.s().a(H, "_o", (Object) ig12.g);
                    String str3 = str2;
                    if (this.i.s().c(str3)) {
                        this.i.s().a(H, "_dbg", (Object) 1L);
                        this.i.s().a(H, "_r", (Object) 1L);
                    }
                    if (this.i.u().p(sl12.e) && "_s".equals(ig12.e)) {
                        nl1 d2 = l().d(sl12.e, "_sno");
                        if (d2 != null && (d2.e instanceof Long)) {
                            this.i.s().a(H, "_sno", d2.e);
                        }
                    }
                    long c2 = l().c(str3);
                    if (c2 > 0) {
                        this.i.d().v().a("Data lost. Too many events stored on disk, deleted. appId", ug1.a(str3), Long.valueOf(c2));
                    }
                    yh1 yh1 = this.i;
                    String str4 = ig12.g;
                    String str5 = ig12.e;
                    long j4 = ig12.h;
                    String str6 = "_r";
                    String str7 = str4;
                    String str8 = str3;
                    dg1 dg1 = new dg1(yh1, str7, str3, str5, j4, 0, H);
                    eg1 b3 = l().b(str8, dg1.b);
                    if (b3 != null) {
                        dg1 = dg1.a(this.i, b3.e);
                        eg1 = b3.a(dg1.d);
                    } else if (l().f(str8) < 500 || !e2) {
                        eg1 = new eg1(str8, dg1.b, 0, 0, dg1.d, 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                    } else {
                        this.i.d().s().a("Too many event names used, ignoring event. appId, name, supported count", ug1.a(str8), this.i.r().a(dg1.b), 500);
                        this.i.s().a(str8, 8, (String) null, (String) null, 0);
                        l().u();
                        return;
                    }
                    l().a(eg1);
                    f();
                    r();
                    ck0.a(dg1);
                    ck0.a(sl1);
                    ck0.b(dg1.a);
                    ck0.a(dg1.a.equals(sl12.e));
                    g61 g61 = new g61();
                    g61.c = 1;
                    g61.k = "android";
                    g61.q = sl12.e;
                    g61.p = sl12.h;
                    g61.r = sl12.g;
                    g61.E = sl12.n == -2147483648L ? null : Integer.valueOf((int) sl12.n);
                    g61.s = Long.valueOf(sl12.i);
                    g61.A = sl12.f;
                    g61.N = sl12.v;
                    g61.x = sl12.j == 0 ? null : Long.valueOf(sl12.j);
                    if (this.i.u().d(sl12.e, kg1.p0)) {
                        g61.P = j().t();
                    }
                    Pair<String, Boolean> a3 = this.i.t().a(sl12.e);
                    if (a3 == null || TextUtils.isEmpty((CharSequence) a3.first)) {
                        if (!this.i.q().a(this.i.getContext()) && sl12.t) {
                            String string = Settings.Secure.getString(this.i.getContext().getContentResolver(), "android_id");
                            if (string == null) {
                                this.i.d().v().a("null secure ID. appId", ug1.a(g61.q));
                                string = "null";
                            } else if (string.isEmpty()) {
                                this.i.d().v().a("empty secure ID. appId", ug1.a(g61.q));
                            }
                            g61.H = string;
                        }
                    } else if (sl12.s) {
                        g61.u = (String) a3.first;
                        g61.v = (Boolean) a3.second;
                    }
                    this.i.q().n();
                    g61.m = Build.MODEL;
                    this.i.q().n();
                    g61.l = Build.VERSION.RELEASE;
                    g61.o = Integer.valueOf((int) this.i.q().s());
                    g61.n = this.i.q().t();
                    g61.t = null;
                    g61.f = null;
                    g61.g = null;
                    g61.h = null;
                    g61.J = Long.valueOf(sl12.p);
                    if (this.i.e() && yl1.v()) {
                        g61.K = null;
                    }
                    rl1 b4 = l().b(sl12.e);
                    if (b4 == null) {
                        b4 = new rl1(this.i, sl12.e);
                        b4.b(this.i.s().v());
                        b4.f(sl12.o);
                        b4.c(sl12.f);
                        b4.e(this.i.t().b(sl12.e));
                        b4.i(0);
                        b4.d(0);
                        b4.e(0);
                        b4.a(sl12.g);
                        b4.f(sl12.n);
                        b4.g(sl12.h);
                        b4.g(sl12.i);
                        b4.h(sl12.j);
                        b4.a(sl12.l);
                        b4.c(sl12.p);
                        l().a(b4);
                    }
                    g61.w = b4.a();
                    g61.D = b4.b();
                    List<nl1> a4 = l().a(sl12.e);
                    g61.e = new j61[a4.size()];
                    for (int i2 = 0; i2 < a4.size(); i2++) {
                        j61 j61 = new j61();
                        g61.e[i2] = j61;
                        j61.d = a4.get(i2).c;
                        j61.c = Long.valueOf(a4.get(i2).d);
                        j().a(j61, a4.get(i2).e);
                    }
                    try {
                        long a5 = l().a(g61);
                        bm1 l2 = l();
                        if (dg1.f != null) {
                            Iterator<String> it = dg1.f.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (str6.equals(it.next())) {
                                        break;
                                    }
                                } else {
                                    boolean c3 = m().c(dg1.a, dg1.b);
                                    cm1 a6 = l().a(s(), dg1.a, false, false, false, false, false);
                                    if (c3 && a6.e < ((long) this.i.u().a(dg1.a))) {
                                    }
                                }
                            }
                        }
                        boolean z2 = false;
                        if (l2.a(dg1, a5, z2)) {
                            this.m = 0;
                        }
                    } catch (IOException e3) {
                        this.i.d().s().a("Data loss. Failed to insert raw event metadata. appId", ug1.a(g61.q), e3);
                    }
                    l().w();
                    if (this.i.d().a(2)) {
                        this.i.d().A().a("Event recorded", this.i.r().a(dg1));
                    }
                    l().u();
                    v();
                    this.i.d().A().a("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / 1000000));
                    return;
                }
            }
            String g2 = ig12.f.g("currency");
            if ("ecommerce_purchase".equals(ig12.e)) {
                double doubleValue = ig12.f.h("value").doubleValue() * 1000000.0d;
                if (doubleValue == 0.0d) {
                    doubleValue = ((double) ig12.f.f("value").longValue()) * 1000000.0d;
                }
                if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                    this.i.d().v().a("Data lost. Currency value is too big. appId", ug1.a(str), Double.valueOf(doubleValue));
                    j2 = nanoTime;
                    if (!z) {
                        l().w();
                        l().u();
                        return;
                    }
                    boolean e22 = ol1.e(ig12.e);
                    boolean equals2 = "_err".equals(ig12.e);
                    String str22 = str;
                    cm1 a22 = l().a(s(), str, true, e22, false, equals2, false);
                    intValue = a22.b - ((long) kg1.u.a().intValue());
                    if (intValue > 0) {
                    }
                } else {
                    j3 = Math.round(doubleValue);
                }
            } else {
                j3 = ig12.f.f("value").longValue();
            }
            if (!TextUtils.isEmpty(g2)) {
                String upperCase = g2.toUpperCase(Locale.US);
                if (upperCase.matches("[A-Z]{3}")) {
                    String valueOf = String.valueOf(upperCase);
                    String concat = valueOf.length() != 0 ? "_ltv_".concat(valueOf) : new String("_ltv_");
                    nl1 d3 = l().d(str, concat);
                    if (d3 != null) {
                        if (d3.e instanceof Long) {
                            j2 = nanoTime;
                            nl1 = new nl1(str, ig12.g, concat, this.i.c().b(), Long.valueOf(((Long) d3.e).longValue() + j3));
                            if (!l().a(nl1)) {
                                this.i.d().s().a("Too many unique user properties are set. Ignoring user property. appId", ug1.a(str), this.i.r().c(nl1.c), nl1.e);
                                this.i.s().a(str, 9, (String) null, (String) null, 0);
                            }
                            z = true;
                            if (!z) {
                            }
                            boolean e222 = ol1.e(ig12.e);
                            boolean equals22 = "_err".equals(ig12.e);
                            String str222 = str;
                            cm1 a222 = l().a(s(), str, true, e222, false, equals22, false);
                            intValue = a222.b - ((long) kg1.u.a().intValue());
                            if (intValue > 0) {
                            }
                        }
                    }
                    j2 = nanoTime;
                    bm1 l3 = l();
                    int b5 = this.i.u().b(str, kg1.O) - 1;
                    ck0.b(str);
                    l3.e();
                    l3.q();
                    try {
                        l3.v().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(b5)});
                    } catch (SQLiteException e4) {
                        l3.d().s().a("Error pruning currencies. appId", ug1.a(str), e4);
                    } catch (Throwable th) {
                        l().u();
                        throw th;
                    }
                    nl1 = new nl1(str, ig12.g, concat, this.i.c().b(), Long.valueOf(j3));
                    if (!l().a(nl1)) {
                    }
                    z = true;
                    if (!z) {
                    }
                    boolean e2222 = ol1.e(ig12.e);
                    boolean equals222 = "_err".equals(ig12.e);
                    String str2222 = str;
                    cm1 a2222 = l().a(s(), str, true, e2222, false, equals222, false);
                    intValue = a2222.b - ((long) kg1.u.a().intValue());
                    if (intValue > 0) {
                    }
                }
            }
            j2 = nanoTime;
            z = true;
            if (!z) {
            }
            boolean e22222 = ol1.e(ig12.e);
            boolean equals2222 = "_err".equals(ig12.e);
            String str22222 = str;
            cm1 a22222 = l().a(s(), str, true, e22222, false, equals2222, false);
            intValue = a22222.b - ((long) kg1.u.a().intValue());
            if (intValue > 0) {
            }
        }
    }

    @DexIgnore
    public final void a(jl1 jl1) {
        this.i.a().e();
        bm1 bm1 = new bm1(this);
        bm1.s();
        this.c = bm1;
        this.i.u().a((am1) this.a);
        ul1 ul1 = new ul1(this);
        ul1.s();
        this.f = ul1;
        pj1 pj1 = new pj1(this);
        pj1.s();
        this.h = pj1;
        al1 al1 = new al1(this);
        al1.s();
        this.e = al1;
        this.d = new eh1(this);
        if (this.o != this.p) {
            this.i.d().s().a("Not all upload components initialized", Integer.valueOf(this.o), Integer.valueOf(this.p));
        }
        this.j = true;
    }

    @DexIgnore
    public final String e(sl1 sl1) {
        try {
            return (String) this.i.a().a(new il1(this, sl1)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.i.d().s().a("Failed to get app instance id. appId", ug1.a(sl1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final uh1 a() {
        return this.i.a();
    }

    @DexIgnore
    public final void a(ig1 ig1, String str) {
        ig1 ig12 = ig1;
        String str2 = str;
        rl1 b2 = l().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.e())) {
            this.i.d().z().a("No app data available; dropping event", str2);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(ig12.e)) {
                this.i.d().v().a("Could not find package. appId", ug1.a(str));
            }
        } else if (!b3.booleanValue()) {
            this.i.d().s().a("App version does not match; dropping event. appId", ug1.a(str));
            return;
        }
        sl1 sl1 = r2;
        rl1 rl1 = b2;
        sl1 sl12 = new sl1(str, b2.c(), b2.e(), b2.l(), b2.m(), b2.n(), b2.o(), (String) null, b2.d(), false, rl1.b(), rl1.B(), 0, 0, rl1.C(), rl1.D(), false, rl1.h());
        a(ig12, sl1);
    }

    @DexIgnore
    public final void a(ig1 ig1, sl1 sl1) {
        List<wl1> list;
        List<wl1> list2;
        List<wl1> list3;
        ig1 ig12 = ig1;
        sl1 sl12 = sl1;
        ck0.a(sl1);
        ck0.b(sl12.e);
        f();
        r();
        String str = sl12.e;
        long j2 = ig12.h;
        if (j().a(ig12, sl12)) {
            if (!sl12.l) {
                d(sl12);
                return;
            }
            l().t();
            try {
                bm1 l2 = l();
                ck0.b(str);
                l2.e();
                l2.q();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    l2.d().v().a("Invalid time querying timed out conditional properties", ug1.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = l2.b("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (wl1 next : list) {
                    if (next != null) {
                        this.i.d().z().a("User property timed out", next.e, this.i.r().c(next.g.f), next.g.H());
                        if (next.k != null) {
                            b(new ig1(next.k, j2), sl12);
                        }
                        l().f(str, next.g.f);
                    }
                }
                bm1 l3 = l();
                ck0.b(str);
                l3.e();
                l3.q();
                if (i2 < 0) {
                    l3.d().v().a("Invalid time querying expired conditional properties", ug1.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = l3.b("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (wl1 next2 : list2) {
                    if (next2 != null) {
                        this.i.d().z().a("User property expired", next2.e, this.i.r().c(next2.g.f), next2.g.H());
                        l().c(str, next2.g.f);
                        if (next2.o != null) {
                            arrayList.add(next2.o);
                        }
                        l().f(str, next2.g.f);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    b(new ig1((ig1) obj, j2), sl12);
                }
                bm1 l4 = l();
                String str2 = ig12.e;
                ck0.b(str);
                ck0.b(str2);
                l4.e();
                l4.q();
                if (i2 < 0) {
                    l4.d().v().a("Invalid time querying triggered conditional properties", ug1.a(str), l4.i().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = l4.b("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (wl1 next3 : list3) {
                    if (next3 != null) {
                        ll1 ll1 = next3.g;
                        nl1 nl1 = r4;
                        nl1 nl12 = new nl1(next3.e, next3.f, ll1.f, j2, ll1.H());
                        if (l().a(nl1)) {
                            this.i.d().z().a("User property triggered", next3.e, this.i.r().c(nl1.c), nl1.e);
                        } else {
                            this.i.d().s().a("Too many active user properties, ignoring", ug1.a(next3.e), this.i.r().c(nl1.c), nl1.e);
                        }
                        if (next3.m != null) {
                            arrayList2.add(next3.m);
                        }
                        next3.g = new ll1(nl1);
                        next3.i = true;
                        l().a(next3);
                    }
                }
                b(ig1, sl1);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    b(new ig1((ig1) obj2, j2), sl12);
                }
                l().w();
            } finally {
                l().u();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0241, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0242, code lost:
        r6 = r4;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0042, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        r5 = r1;
        r8 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0047, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
        r6 = null;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:482:0x0af4, code lost:
        if (r24 != r14) goto L_0x0af6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:579:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042 A[ExcHandler: all (th java.lang.Throwable), PHI: r4 
  PHI: (r4v141 android.database.Cursor) = (r4v136 android.database.Cursor), (r4v136 android.database.Cursor), (r4v136 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v0 android.database.Cursor), (r4v0 android.database.Cursor) binds: [B:46:0x00da, B:52:0x00e7, B:53:?, B:23:0x007c, B:29:0x0089, B:31:0x008d, B:32:?, B:9:0x0033, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0033] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0263 A[SYNTHETIC, Splitter:B:128:0x0263] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x026a A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0278 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x039d A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x03a8 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x03a9 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x0663 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x06e5 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x0834 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:373:0x084c A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:376:0x086c A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:489:0x0b13 A[Catch:{ all -> 0x0d76 }] */
    /* JADX WARNING: Removed duplicated region for block: B:493:0x0b60 A[Catch:{ all -> 0x0d76 }] */
    /* JADX WARNING: Removed duplicated region for block: B:570:0x0d7a  */
    /* JADX WARNING: Removed duplicated region for block: B:578:0x0d91 A[SYNTHETIC, Splitter:B:578:0x0d91] */
    /* JADX WARNING: Removed duplicated region for block: B:614:0x0849 A[SYNTHETIC] */
    public final boolean a(String str, long j2) {
        bm1 l2;
        Cursor cursor;
        Throwable th;
        boolean z;
        boolean z2;
        g61 g61;
        el1 el1;
        bm1 l3;
        SecureRandom secureRandom;
        g61 g612;
        int i2;
        int i3;
        d61[] d61Arr;
        HashMap hashMap;
        long j3;
        boolean z3;
        HashMap hashMap2;
        Long l4;
        boolean z4;
        long j4;
        nl1 nl1;
        int i4;
        boolean z5;
        boolean z6;
        boolean z7;
        d61 d61;
        long j5;
        d61 d612;
        boolean z8;
        char c2;
        boolean z9;
        String str2;
        Cursor cursor2;
        SQLiteException sQLiteException;
        String str3;
        String str4;
        String[] strArr;
        String str5;
        String[] strArr2;
        el1 el12 = this;
        String str6 = "_lte";
        l().t();
        try {
            Cursor cursor3 = null;
            a aVar = new a(el12, (fl1) null);
            l2 = l();
            long j6 = el12.x;
            ck0.a(aVar);
            l2.e();
            l2.q();
            try {
                SQLiteDatabase v2 = l2.v();
                if (TextUtils.isEmpty((CharSequence) null)) {
                    int i5 = (j6 > -1 ? 1 : (j6 == -1 ? 0 : -1));
                    if (i5 != 0) {
                        try {
                            strArr2 = new String[]{String.valueOf(j6), String.valueOf(j2)};
                        } catch (SQLiteException e2) {
                            e = e2;
                            cursor2 = cursor3;
                        } catch (Throwable th2) {
                        }
                    } else {
                        strArr2 = new String[]{String.valueOf(j2)};
                    }
                    String str7 = i5 != 0 ? "rowid <= ? and " : "";
                    StringBuilder sb = new StringBuilder(str7.length() + 148);
                    sb.append("select app_id, metadata_fingerprint from raw_events where ");
                    sb.append(str7);
                    sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                    cursor3 = v2.rawQuery(sb.toString(), strArr2);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (aVar.c != null) {
                            if (!aVar.c.isEmpty()) {
                                z = false;
                                if (z) {
                                    g61 g613 = aVar.a;
                                    g613.d = new d61[aVar.c.size()];
                                    boolean e3 = el12.i.u().e(g613.q);
                                    boolean d2 = el12.i.u().d(aVar.a.q, kg1.m0);
                                    d61 d613 = null;
                                    int i6 = 0;
                                    int i7 = 0;
                                    d61 d614 = null;
                                    boolean z10 = false;
                                    long j7 = 0;
                                    while (true) {
                                        z2 = z10;
                                        if (i6 >= aVar.c.size()) {
                                            break;
                                        }
                                        d61 d615 = aVar.c.get(i6);
                                        String str8 = str6;
                                        int i8 = i6;
                                        int i9 = i7;
                                        if (m().b(aVar.a.q, d615.d)) {
                                            g61 g614 = g613;
                                            el12.i.d().v().a("Dropping blacklisted raw event. appId", ug1.a(aVar.a.q), el12.i.r().a(d615.d));
                                            if (!m().g(aVar.a.q)) {
                                                if (!m().h(aVar.a.q)) {
                                                    z9 = false;
                                                    if (!z9 && !"_err".equals(d615.d)) {
                                                        el12.i.s().a(aVar.a.q, 11, "_ev", d615.d, 0);
                                                    }
                                                    z7 = e3;
                                                    z6 = d2;
                                                    z10 = z2;
                                                    g613 = g614;
                                                }
                                            }
                                            z9 = true;
                                            el12.i.s().a(aVar.a.q, 11, "_ev", d615.d, 0);
                                            z7 = e3;
                                            z6 = d2;
                                            z10 = z2;
                                            g613 = g614;
                                        } else {
                                            g61 g615 = g613;
                                            boolean c3 = m().c(aVar.a.q, d615.d);
                                            if (!c3) {
                                                j();
                                                String str9 = d615.d;
                                                ck0.b(str9);
                                                j5 = j7;
                                                int hashCode = str9.hashCode();
                                                if (hashCode != 94660) {
                                                    if (hashCode != 95025) {
                                                        if (hashCode == 95027) {
                                                            if (str9.equals("_ui")) {
                                                                c2 = 1;
                                                                if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                                    z7 = e3;
                                                                    z6 = d2;
                                                                    d61 = d614;
                                                                    if (!el12.i.u().l(aVar.a.q) && c3) {
                                                                        e61[] e61Arr = d615.c;
                                                                        int i10 = -1;
                                                                        int i11 = -1;
                                                                        for (int i12 = 0; i12 < e61Arr.length; i12++) {
                                                                            if ("value".equals(e61Arr[i12].c)) {
                                                                                i10 = i12;
                                                                            } else if ("currency".equals(e61Arr[i12].c)) {
                                                                                i11 = i12;
                                                                            }
                                                                        }
                                                                        if (i10 != -1) {
                                                                            if (e61Arr[i10].e == null && e61Arr[i10].g == null) {
                                                                                el12.i.d().x().a("Value must be specified with a numeric type.");
                                                                                e61Arr = a(a(a(e61Arr, i10), "_c"), 18, "value");
                                                                            } else {
                                                                                if (i11 != -1) {
                                                                                    String str10 = e61Arr[i11].d;
                                                                                    if (str10 != null) {
                                                                                        if (str10.length() == 3) {
                                                                                            int i13 = 0;
                                                                                            while (i13 < str10.length()) {
                                                                                                int codePointAt = str10.codePointAt(i13);
                                                                                                if (Character.isLetter(codePointAt)) {
                                                                                                    i13 += Character.charCount(codePointAt);
                                                                                                }
                                                                                            }
                                                                                            z8 = false;
                                                                                        }
                                                                                    }
                                                                                    z8 = true;
                                                                                    break;
                                                                                }
                                                                                z8 = true;
                                                                                if (z8) {
                                                                                    el12.i.d().x().a("Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter.");
                                                                                    e61Arr = a(a(a(e61Arr, i10), "_c"), 19, "currency");
                                                                                }
                                                                                d615.c = e61Arr;
                                                                            }
                                                                        }
                                                                        d615.c = e61Arr;
                                                                    }
                                                                    if (el12.i.u().d(aVar.a.q, kg1.l0)) {
                                                                        if ("_e".equals(d615.d)) {
                                                                            j();
                                                                            if (kl1.a(d615, "_fr") == null) {
                                                                                if (d613 == null || Math.abs(d613.e.longValue() - d615.e.longValue()) > 1000 || !el12.a(d615, d613)) {
                                                                                    d612 = d615;
                                                                                    if (z7 && !z6 && "_e".equals(d615.d)) {
                                                                                        if (d615.c != null) {
                                                                                            if (d615.c.length != 0) {
                                                                                                j();
                                                                                                Long l5 = (Long) kl1.b(d615, "_et");
                                                                                                if (l5 == null) {
                                                                                                    el12.i.d().v().a("Engagement event does not include duration. appId", ug1.a(aVar.a.q));
                                                                                                } else {
                                                                                                    j5 += l5.longValue();
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        el12.i.d().v().a("Engagement event does not contain any parameters. appId", ug1.a(aVar.a.q));
                                                                                    }
                                                                                    g613 = g615;
                                                                                    g613.d[i9] = d615;
                                                                                    d614 = d612;
                                                                                    i9++;
                                                                                    z10 = z2;
                                                                                    j7 = j5;
                                                                                }
                                                                            }
                                                                        } else if ("_vs".equals(d615.d)) {
                                                                            j();
                                                                            if (kl1.a(d615, "_et") == null) {
                                                                                if (d61 != null) {
                                                                                    d612 = d61;
                                                                                    if (Math.abs(d612.e.longValue() - d615.e.longValue()) <= 1000 && el12.a(d612, d615)) {
                                                                                    }
                                                                                } else {
                                                                                    d612 = d61;
                                                                                }
                                                                                d613 = d615;
                                                                                if (d615.c != null) {
                                                                                }
                                                                                el12.i.d().v().a("Engagement event does not contain any parameters. appId", ug1.a(aVar.a.q));
                                                                                g613 = g615;
                                                                                g613.d[i9] = d615;
                                                                                d614 = d612;
                                                                                i9++;
                                                                                z10 = z2;
                                                                                j7 = j5;
                                                                            }
                                                                        }
                                                                        d612 = null;
                                                                        d613 = null;
                                                                        if (d615.c != null) {
                                                                        }
                                                                        el12.i.d().v().a("Engagement event does not contain any parameters. appId", ug1.a(aVar.a.q));
                                                                        g613 = g615;
                                                                        g613.d[i9] = d615;
                                                                        d614 = d612;
                                                                        i9++;
                                                                        z10 = z2;
                                                                        j7 = j5;
                                                                    }
                                                                    d612 = d61;
                                                                    if (d615.c != null) {
                                                                    }
                                                                    el12.i.d().v().a("Engagement event does not contain any parameters. appId", ug1.a(aVar.a.q));
                                                                    g613 = g615;
                                                                    g613.d[i9] = d615;
                                                                    d614 = d612;
                                                                    i9++;
                                                                    z10 = z2;
                                                                    j7 = j5;
                                                                }
                                                            }
                                                        }
                                                    } else if (str9.equals("_ug")) {
                                                        c2 = 2;
                                                        if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                        }
                                                    }
                                                } else if (str9.equals("_in")) {
                                                    c2 = 0;
                                                    if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                    }
                                                }
                                                c2 = 65535;
                                                if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                }
                                            } else {
                                                j5 = j7;
                                            }
                                            if (d615.c == null) {
                                                d615.c = new e61[0];
                                            }
                                            e61[] e61Arr2 = d615.c;
                                            int length = e61Arr2.length;
                                            z6 = d2;
                                            int i14 = 0;
                                            boolean z11 = false;
                                            boolean z12 = false;
                                            while (i14 < length) {
                                                int i15 = length;
                                                e61 e61 = e61Arr2[i14];
                                                e61[] e61Arr3 = e61Arr2;
                                                if ("_c".equals(e61.c)) {
                                                    e61.e = 1L;
                                                    z11 = true;
                                                } else if ("_r".equals(e61.c)) {
                                                    e61.e = 1L;
                                                    z12 = true;
                                                }
                                                i14++;
                                                length = i15;
                                                e61Arr2 = e61Arr3;
                                            }
                                            if (z11 || !c3) {
                                                z7 = e3;
                                            } else {
                                                z7 = e3;
                                                el12.i.d().A().a("Marking event as conversion", el12.i.r().a(d615.d));
                                                e61[] e61Arr4 = (e61[]) Arrays.copyOf(d615.c, d615.c.length + 1);
                                                e61 e612 = new e61();
                                                e612.c = "_c";
                                                e612.e = 1L;
                                                e61Arr4[e61Arr4.length - 1] = e612;
                                                d615.c = e61Arr4;
                                            }
                                            if (!z12) {
                                                el12.i.d().A().a("Marking event as real-time", el12.i.r().a(d615.d));
                                                e61[] e61Arr5 = (e61[]) Arrays.copyOf(d615.c, d615.c.length + 1);
                                                e61 e613 = new e61();
                                                e613.c = "_r";
                                                e613.e = 1L;
                                                e61Arr5[e61Arr5.length - 1] = e613;
                                                d615.c = e61Arr5;
                                            }
                                            d61 = d614;
                                            if (l().a(s(), aVar.a.q, false, false, false, false, true).e > ((long) el12.i.u().a(aVar.a.q))) {
                                                int i16 = 0;
                                                while (true) {
                                                    if (i16 >= d615.c.length) {
                                                        break;
                                                    } else if ("_r".equals(d615.c[i16].c)) {
                                                        e61[] e61Arr6 = new e61[(d615.c.length - 1)];
                                                        if (i16 > 0) {
                                                            System.arraycopy(d615.c, 0, e61Arr6, 0, i16);
                                                        }
                                                        if (i16 < e61Arr6.length) {
                                                            System.arraycopy(d615.c, i16 + 1, e61Arr6, i16, e61Arr6.length - i16);
                                                        }
                                                        d615.c = e61Arr6;
                                                    } else {
                                                        i16++;
                                                    }
                                                }
                                            } else {
                                                z2 = true;
                                            }
                                            if (ol1.e(d615.d) && c3 && l().a(s(), aVar.a.q, false, false, true, false, false).c > ((long) el12.i.u().b(aVar.a.q, kg1.x))) {
                                                el12.i.d().v().a("Too many conversions. Not logging as conversion. appId", ug1.a(aVar.a.q));
                                                e61[] e61Arr7 = d615.c;
                                                int length2 = e61Arr7.length;
                                                int i17 = 0;
                                                boolean z13 = false;
                                                e61 e614 = null;
                                                while (i17 < length2) {
                                                    e61 e615 = e61Arr7[i17];
                                                    e61[] e61Arr8 = e61Arr7;
                                                    if ("_c".equals(e615.c)) {
                                                        e614 = e615;
                                                    } else if ("_err".equals(e615.c)) {
                                                        z13 = true;
                                                    }
                                                    i17++;
                                                    e61Arr7 = e61Arr8;
                                                }
                                                if (z13 && e614 != null) {
                                                    d615.c = (e61[]) gm0.a((T[]) d615.c, (T[]) new e61[]{e614});
                                                } else if (e614 != null) {
                                                    e614.c = "_err";
                                                    e614.e = 10L;
                                                } else {
                                                    el12.i.d().s().a("Did not find conversion parameter. appId", ug1.a(aVar.a.q));
                                                }
                                            }
                                            if (!el12.i.u().l(aVar.a.q)) {
                                            }
                                            if (el12.i.u().d(aVar.a.q, kg1.l0)) {
                                            }
                                            d612 = d61;
                                            if (d615.c != null) {
                                            }
                                            el12.i.d().v().a("Engagement event does not contain any parameters. appId", ug1.a(aVar.a.q));
                                            g613 = g615;
                                            g613.d[i9] = d615;
                                            d614 = d612;
                                            i9++;
                                            z10 = z2;
                                            j7 = j5;
                                        }
                                        i6 = i8 + 1;
                                        str6 = str8;
                                        i7 = i9;
                                        e3 = z7;
                                        d2 = z6;
                                    }
                                    String str11 = str6;
                                    boolean z14 = e3;
                                    int i18 = i7;
                                    long j8 = j7;
                                    if (d2) {
                                        long j9 = j8;
                                        int i19 = 0;
                                        while (i19 < i7) {
                                            d61 d616 = g613.d[i19];
                                            if ("_e".equals(d616.d)) {
                                                j();
                                                if (kl1.a(d616, "_fr") != null) {
                                                    System.arraycopy(g613.d, i19 + 1, g613.d, i19, (i7 - i19) - 1);
                                                    i7--;
                                                    i19--;
                                                    i19++;
                                                }
                                            }
                                            if (z14) {
                                                j();
                                                e61 a2 = kl1.a(d616, "_et");
                                                if (a2 != null) {
                                                    Long l6 = a2.e;
                                                    if (l6 != null && l6.longValue() > 0) {
                                                        j9 += l6.longValue();
                                                    }
                                                }
                                            }
                                            i19++;
                                        }
                                        j8 = j9;
                                    }
                                    if (i7 < aVar.c.size()) {
                                        g613.d = (d61[]) Arrays.copyOf(g613.d, i7);
                                    }
                                    if (z14) {
                                        String str12 = str11;
                                        nl1 d3 = l().d(g613.q, str12);
                                        if (d3 != null) {
                                            if (d3.e != null) {
                                                nl1 = new nl1(g613.q, "auto", "_lte", el12.i.c().b(), Long.valueOf(((Long) d3.e).longValue() + j8));
                                                j61 j61 = new j61();
                                                j61.d = str12;
                                                j61.c = Long.valueOf(el12.i.c().b());
                                                j61.f = (Long) nl1.e;
                                                i4 = 0;
                                                while (true) {
                                                    if (i4 < g613.e.length) {
                                                        z5 = false;
                                                        break;
                                                    } else if (str12.equals(g613.e[i4].d)) {
                                                        g613.e[i4] = j61;
                                                        z5 = true;
                                                        break;
                                                    } else {
                                                        i4++;
                                                    }
                                                }
                                                if (!z5) {
                                                    g613.e = (j61[]) Arrays.copyOf(g613.e, g613.e.length + 1);
                                                    g613.e[aVar.a.e.length - 1] = j61;
                                                }
                                                if (j8 > 0) {
                                                    l().a(nl1);
                                                    el12.i.d().z().a("Updated lifetime engagement user property with value. Value", nl1.e);
                                                }
                                            }
                                        }
                                        nl1 = new nl1(g613.q, "auto", "_lte", el12.i.c().b(), Long.valueOf(j8));
                                        j61 j612 = new j61();
                                        j612.d = str12;
                                        j612.c = Long.valueOf(el12.i.c().b());
                                        j612.f = (Long) nl1.e;
                                        i4 = 0;
                                        while (true) {
                                            if (i4 < g613.e.length) {
                                            }
                                            i4++;
                                        }
                                        if (!z5) {
                                        }
                                        if (j8 > 0) {
                                        }
                                    }
                                    String str13 = g613.q;
                                    j61[] j61Arr = g613.e;
                                    d61[] d61Arr2 = g613.d;
                                    ck0.b(str13);
                                    g613.C = k().a(str13, d61Arr2, j61Arr);
                                    if (el12.i.u().d(aVar.a.q)) {
                                        try {
                                            HashMap hashMap3 = new HashMap();
                                            d61[] d61Arr3 = new d61[g613.d.length];
                                            SecureRandom t2 = el12.i.s().t();
                                            d61[] d61Arr4 = g613.d;
                                            int length3 = d61Arr4.length;
                                            int i20 = 0;
                                            int i21 = 0;
                                            while (i20 < length3) {
                                                d61 d617 = d61Arr4[i20];
                                                if (d617.d.equals("_ep")) {
                                                    j();
                                                    String str14 = (String) kl1.b(d617, "_en");
                                                    eg1 eg1 = (eg1) hashMap3.get(str14);
                                                    if (eg1 == null) {
                                                        eg1 = l().b(aVar.a.q, str14);
                                                        hashMap3.put(str14, eg1);
                                                    }
                                                    if (eg1.h == null) {
                                                        if (eg1.i.longValue() > 1) {
                                                            j();
                                                            d617.c = kl1.a(d617.c, "_sr", (Object) eg1.i);
                                                        }
                                                        if (eg1.j != null && eg1.j.booleanValue()) {
                                                            j();
                                                            d617.c = kl1.a(d617.c, "_efs", (Object) 1L);
                                                        }
                                                        int i22 = i21 + 1;
                                                        d61Arr3[i21] = d617;
                                                        g612 = g613;
                                                        secureRandom = t2;
                                                        d61Arr = d61Arr4;
                                                        i3 = length3;
                                                        i2 = i20;
                                                        i21 = i22;
                                                    } else {
                                                        g612 = g613;
                                                        secureRandom = t2;
                                                        d61Arr = d61Arr4;
                                                        i3 = length3;
                                                        i2 = i20;
                                                    }
                                                } else {
                                                    long f2 = m().f(aVar.a.q);
                                                    el12.i.s();
                                                    long a3 = ol1.a(d617.e.longValue(), f2);
                                                    d61Arr = d61Arr4;
                                                    i3 = length3;
                                                    Long l7 = 1L;
                                                    if (!TextUtils.isEmpty("_dbg") && l7 != null) {
                                                        g612 = g613;
                                                        e61[] e61Arr9 = d617.c;
                                                        i2 = i20;
                                                        int length4 = e61Arr9.length;
                                                        j3 = f2;
                                                        int i23 = 0;
                                                        while (true) {
                                                            if (i23 >= length4) {
                                                                break;
                                                            }
                                                            e61 e616 = e61Arr9[i23];
                                                            e61[] e61Arr10 = e61Arr9;
                                                            if (!"_dbg".equals(e616.c)) {
                                                                i23++;
                                                                e61Arr9 = e61Arr10;
                                                            } else if (((l7 instanceof Long) && l7.equals(e616.e)) || (((l7 instanceof String) && l7.equals(e616.d)) || ((l7 instanceof Double) && l7.equals(e616.g)))) {
                                                                z3 = true;
                                                            }
                                                        }
                                                    } else {
                                                        g612 = g613;
                                                        i2 = i20;
                                                        j3 = f2;
                                                    }
                                                    z3 = false;
                                                    int d4 = !z3 ? m().d(aVar.a.q, d617.d) : 1;
                                                    if (d4 <= 0) {
                                                        el12.i.d().v().a("Sample rate must be positive. event, rate", d617.d, Integer.valueOf(d4));
                                                        int i24 = i21 + 1;
                                                        d61Arr3[i21] = d617;
                                                        i21 = i24;
                                                        secureRandom = t2;
                                                    } else {
                                                        eg1 eg12 = (eg1) hashMap3.get(d617.d);
                                                        if (eg12 == null) {
                                                            eg12 = l().b(aVar.a.q, d617.d);
                                                            if (eg12 == null) {
                                                                el12.i.d().v().a("Event being bundled has no eventAggregate. appId, eventName", aVar.a.q, d617.d);
                                                                eg12 = new eg1(aVar.a.q, d617.d, 1, 1, d617.e.longValue(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                                            }
                                                        }
                                                        j();
                                                        Long l8 = (Long) kl1.b(d617, "_eid");
                                                        Boolean valueOf = Boolean.valueOf(l8 != null);
                                                        if (d4 == 1) {
                                                            int i25 = i21 + 1;
                                                            d61Arr3[i21] = d617;
                                                            if (valueOf.booleanValue() && !(eg12.h == null && eg12.i == null && eg12.j == null)) {
                                                                hashMap3.put(d617.d, eg12.a((Long) null, (Long) null, (Boolean) null));
                                                            }
                                                            secureRandom = t2;
                                                            i21 = i25;
                                                        } else if (t2.nextInt(d4) == 0) {
                                                            j();
                                                            long j10 = (long) d4;
                                                            secureRandom = t2;
                                                            d617.c = kl1.a(d617.c, "_sr", (Object) Long.valueOf(j10));
                                                            int i26 = i21 + 1;
                                                            d61Arr3[i21] = d617;
                                                            if (valueOf.booleanValue()) {
                                                                eg12 = eg12.a((Long) null, Long.valueOf(j10), (Boolean) null);
                                                            }
                                                            hashMap3.put(d617.d, eg12.a(d617.e.longValue(), a3));
                                                            i21 = i26;
                                                        } else {
                                                            secureRandom = t2;
                                                            if (!el12.i.u().n(aVar.a.q)) {
                                                                hashMap2 = hashMap3;
                                                                l4 = l8;
                                                                if (Math.abs(d617.e.longValue() - eg12.f) >= LogBuilder.MAX_INTERVAL) {
                                                                }
                                                                z4 = false;
                                                                if (!z4) {
                                                                    j();
                                                                    d617.c = kl1.a(d617.c, "_efs", (Object) 1L);
                                                                    j();
                                                                    long j11 = (long) d4;
                                                                    d617.c = kl1.a(d617.c, "_sr", (Object) Long.valueOf(j11));
                                                                    int i27 = i21 + 1;
                                                                    d61Arr3[i21] = d617;
                                                                    if (valueOf.booleanValue()) {
                                                                        eg12 = eg12.a((Long) null, Long.valueOf(j11), true);
                                                                    }
                                                                    hashMap = hashMap2;
                                                                    hashMap.put(d617.d, eg12.a(d617.e.longValue(), a3));
                                                                    i21 = i27;
                                                                } else {
                                                                    hashMap = hashMap2;
                                                                    if (valueOf.booleanValue()) {
                                                                        hashMap.put(d617.d, eg12.a(l4, (Long) null, (Boolean) null));
                                                                    }
                                                                }
                                                                i20 = i2 + 1;
                                                                el12 = this;
                                                                hashMap3 = hashMap;
                                                                d61Arr4 = d61Arr;
                                                                length3 = i3;
                                                                g613 = g612;
                                                                t2 = secureRandom;
                                                            } else if (eg12.g != null) {
                                                                j4 = eg12.g.longValue();
                                                                hashMap2 = hashMap3;
                                                                l4 = l8;
                                                            } else {
                                                                el12.i.s();
                                                                l4 = l8;
                                                                hashMap2 = hashMap3;
                                                                j4 = ol1.a(d617.f.longValue(), j3);
                                                            }
                                                            z4 = true;
                                                            if (!z4) {
                                                            }
                                                            i20 = i2 + 1;
                                                            el12 = this;
                                                            hashMap3 = hashMap;
                                                            d61Arr4 = d61Arr;
                                                            length3 = i3;
                                                            g613 = g612;
                                                            t2 = secureRandom;
                                                        }
                                                    }
                                                }
                                                hashMap = hashMap3;
                                                i20 = i2 + 1;
                                                el12 = this;
                                                hashMap3 = hashMap;
                                                d61Arr4 = d61Arr;
                                                length3 = i3;
                                                g613 = g612;
                                                t2 = secureRandom;
                                            }
                                            HashMap hashMap4 = hashMap3;
                                            g61 = g613;
                                            if (i21 < g61.d.length) {
                                                g61.d = (d61[]) Arrays.copyOf(d61Arr3, i21);
                                            }
                                            for (Map.Entry value : hashMap4.entrySet()) {
                                                l().a((eg1) value.getValue());
                                            }
                                        } catch (Throwable th3) {
                                            th = th3;
                                            Throwable th4 = th;
                                            l().u();
                                            throw th4;
                                        }
                                    } else {
                                        g61 = g613;
                                    }
                                    g61.g = Long.valueOf(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
                                    g61.h = Long.MIN_VALUE;
                                    for (d61 d618 : g61.d) {
                                        if (d618.e.longValue() < g61.g.longValue()) {
                                            g61.g = d618.e;
                                        }
                                        if (d618.e.longValue() > g61.h.longValue()) {
                                            g61.h = d618.e;
                                        }
                                    }
                                    String str15 = aVar.a.q;
                                    rl1 b2 = l().b(str15);
                                    if (b2 == null) {
                                        el1 = this;
                                        try {
                                            el1.i.d().s().a("Bundling raw events w/o app info. appId", ug1.a(aVar.a.q));
                                        } catch (SQLiteException e4) {
                                            l3.d().s().a("Failed to remove unused event metadata. appId", ug1.a(str15), e4);
                                        } catch (Throwable th5) {
                                            th = th5;
                                            Throwable th42 = th;
                                            l().u();
                                            throw th42;
                                        }
                                    } else {
                                        el1 = this;
                                        if (g61.d.length > 0) {
                                            long k2 = b2.k();
                                            g61.j = k2 != 0 ? Long.valueOf(k2) : null;
                                            long j12 = b2.j();
                                            if (j12 != 0) {
                                                k2 = j12;
                                            }
                                            g61.i = k2 != 0 ? Long.valueOf(k2) : null;
                                            b2.s();
                                            g61.y = Integer.valueOf((int) b2.p());
                                            b2.d(g61.g.longValue());
                                            b2.e(g61.h.longValue());
                                            g61.z = b2.A();
                                            l().a(b2);
                                        }
                                    }
                                    if (g61.d.length > 0) {
                                        el1.i.b();
                                        z51 b3 = m().b(aVar.a.q);
                                        if (b3 != null) {
                                            if (b3.c != null) {
                                                g61.I = b3.c;
                                                l().a(g61, z2);
                                            }
                                        }
                                        if (TextUtils.isEmpty(aVar.a.A)) {
                                            g61.I = -1L;
                                        } else {
                                            el1.i.d().v().a("Did not find measurement config or missing version info. appId", ug1.a(aVar.a.q));
                                        }
                                        l().a(g61, z2);
                                    }
                                    bm1 l9 = l();
                                    List<Long> list = aVar.b;
                                    ck0.a(list);
                                    l9.e();
                                    l9.q();
                                    StringBuilder sb2 = new StringBuilder("rowid in (");
                                    for (int i28 = 0; i28 < list.size(); i28++) {
                                        if (i28 != 0) {
                                            sb2.append(",");
                                        }
                                        sb2.append(list.get(i28).longValue());
                                    }
                                    sb2.append(")");
                                    int delete = l9.v().delete("raw_events", sb2.toString(), (String[]) null);
                                    if (delete != list.size()) {
                                        l9.d().s().a("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                                    }
                                    l3 = l();
                                    l3.v().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str15, str15});
                                    l().w();
                                    l().u();
                                    return true;
                                }
                                el1 el13 = el12;
                                l().w();
                                l().u();
                                return false;
                            }
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        str2 = cursor3.getString(0);
                        str3 = cursor3.getString(1);
                        cursor3.close();
                        String str16 = str2;
                        cursor = cursor3;
                        str4 = str16;
                    }
                } else {
                    int i29 = (j6 > -1 ? 1 : (j6 == -1 ? 0 : -1));
                    String[] strArr3 = i29 != 0 ? new String[]{null, String.valueOf(j6)} : new String[]{null};
                    String str17 = i29 != 0 ? " and rowid <= ?" : "";
                    StringBuilder sb3 = new StringBuilder(str17.length() + 84);
                    sb3.append("select metadata_fingerprint from raw_events where app_id = ?");
                    sb3.append(str17);
                    sb3.append(" order by rowid limit 1;");
                    cursor3 = v2.rawQuery(sb3.toString(), strArr3);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (aVar.c != null) {
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        str3 = cursor3.getString(0);
                        cursor3.close();
                        cursor = cursor3;
                        str4 = null;
                    }
                }
                try {
                    SQLiteDatabase sQLiteDatabase = v2;
                    cursor = v2.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str4, str3}, (String) null, (String) null, "rowid", "2");
                    if (!cursor.moveToFirst()) {
                        l2.d().s().a("Raw event metadata record is missing. appId", ug1.a(str4));
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (aVar.c != null) {
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        byte[] blob = cursor.getBlob(0);
                        ub1 a4 = ub1.a(blob, 0, blob.length);
                        g61 g616 = new g61();
                        g616.a(a4);
                        if (cursor.moveToNext()) {
                            l2.d().v().a("Get multiple raw event metadata records, expected one. appId", ug1.a(str4));
                        }
                        cursor.close();
                        aVar.a(g616);
                        if (j6 != -1) {
                            str5 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                            strArr = new String[]{str4, str3, String.valueOf(j6)};
                        } else {
                            str5 = "app_id = ? and metadata_fingerprint = ?";
                            strArr = new String[]{str4, str3};
                        }
                        cursor2 = sQLiteDatabase.query("raw_events", new String[]{"rowid", "name", "timestamp", "data"}, str5, strArr, (String) null, (String) null, "rowid", (String) null);
                        try {
                            if (!cursor2.moveToFirst()) {
                                l2.d().v().a("Raw event data disappeared while in transaction. appId", ug1.a(str4));
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            } else {
                                do {
                                    long j13 = cursor2.getLong(0);
                                    byte[] blob2 = cursor2.getBlob(3);
                                    ub1 a5 = ub1.a(blob2, 0, blob2.length);
                                    d61 d619 = new d61();
                                    try {
                                        d619.a(a5);
                                        d619.d = cursor2.getString(1);
                                        d619.e = Long.valueOf(cursor2.getLong(2));
                                        if (!aVar.a(j13, d619)) {
                                            if (cursor2 != null) {
                                                cursor2.close();
                                            }
                                            if (aVar.c != null) {
                                            }
                                            z = true;
                                            if (z) {
                                            }
                                        }
                                    } catch (IOException e5) {
                                        l2.d().s().a("Data loss. Failed to merge raw event. appId", ug1.a(str4), e5);
                                    }
                                } while (cursor2.moveToNext());
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            }
                        } catch (SQLiteException e6) {
                            e = e6;
                            str2 = str4;
                            sQLiteException = e;
                            try {
                                l2.d().s().a("Data loss. Error selecting raw event. appId", ug1.a(str2), sQLiteException);
                                if (cursor2 != null) {
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            } catch (Throwable th6) {
                                el1 el14 = el12;
                                th = th6;
                                cursor = cursor2;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        } catch (Throwable th7) {
                            th = th7;
                            el1 el15 = el12;
                            cursor = cursor2;
                            th = th;
                            if (cursor != null) {
                            }
                            throw th;
                        }
                    }
                } catch (SQLiteException e7) {
                    e = e7;
                    cursor2 = cursor;
                    str2 = str4;
                    sQLiteException = e;
                    l2.d().s().a("Data loss. Error selecting raw event. appId", ug1.a(str2), sQLiteException);
                    if (cursor2 != null) {
                    }
                    if (aVar.c != null) {
                    }
                    z = true;
                    if (z) {
                    }
                } catch (Throwable th8) {
                    th = th8;
                    el1 el16 = el12;
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e8) {
                sQLiteException = e8;
                cursor2 = null;
                str2 = null;
                l2.d().s().a("Data loss. Error selecting raw event. appId", ug1.a(str2), sQLiteException);
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (aVar.c != null) {
                }
                z = true;
                if (z) {
                }
            } catch (Throwable th9) {
                th = th9;
                el1 el17 = el12;
                cursor = null;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (IOException e9) {
            l2.d().s().a("Data loss. Failed to merge raw event metadata. appId", ug1.a(str4), e9);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th10) {
            th = th10;
            el1 el18 = el12;
        }
    }

    @DexIgnore
    public final Boolean b(rl1 rl1) {
        try {
            if (rl1.l() != -2147483648L) {
                if (rl1.l() == ((long) cn0.b(this.i.getContext()).b(rl1.f(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = cn0.b(this.i.getContext()).b(rl1.f(), 0).versionName;
                if (rl1.e() != null && rl1.e().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public final void b(ll1 ll1, sl1 sl1) {
        f();
        r();
        if (TextUtils.isEmpty(sl1.f) && TextUtils.isEmpty(sl1.v)) {
            return;
        }
        if (!sl1.l) {
            d(sl1);
            return;
        }
        this.i.d().z().a("Removing user property", this.i.r().c(ll1.f));
        l().t();
        try {
            d(sl1);
            l().c(sl1.e, ll1.f);
            l().w();
            this.i.d().z().a("User property removed", this.i.r().c(ll1.f));
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final void b(wl1 wl1) {
        sl1 a2 = a(wl1.e);
        if (a2 != null) {
            b(wl1, a2);
        }
    }

    @DexIgnore
    public final void b(wl1 wl1, sl1 sl1) {
        ck0.a(wl1);
        ck0.b(wl1.e);
        ck0.a(wl1.g);
        ck0.b(wl1.g.f);
        f();
        r();
        if (TextUtils.isEmpty(sl1.f) && TextUtils.isEmpty(sl1.v)) {
            return;
        }
        if (!sl1.l) {
            d(sl1);
            return;
        }
        l().t();
        try {
            d(sl1);
            wl1 e2 = l().e(wl1.e, wl1.g.f);
            if (e2 != null) {
                this.i.d().z().a("Removing conditional user property", wl1.e, this.i.r().c(wl1.g.f));
                l().f(wl1.e, wl1.g.f);
                if (e2.i) {
                    l().c(wl1.e, wl1.g.f);
                }
                if (wl1.o != null) {
                    Bundle bundle = null;
                    if (wl1.o.f != null) {
                        bundle = wl1.o.f.H();
                    }
                    Bundle bundle2 = bundle;
                    b(this.i.s().a(wl1.e, wl1.o.e, bundle2, e2.f, wl1.o.h, true, false), sl1);
                }
            } else {
                this.i.d().v().a("Conditional user property doesn't exist", ug1.a(wl1.e), this.i.r().c(wl1.g.f));
            }
            l().w();
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final boolean a(d61 d61, d61 d612) {
        String str;
        ck0.a("_e".equals(d61.d));
        j();
        e61 a2 = kl1.a(d61, "_sc");
        String str2 = null;
        if (a2 == null) {
            str = null;
        } else {
            str = a2.d;
        }
        j();
        e61 a3 = kl1.a(d612, "_pc");
        if (a3 != null) {
            str2 = a3.d;
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        j();
        e61 a4 = kl1.a(d61, "_et");
        Long l2 = a4.e;
        if (l2 != null && l2.longValue() > 0) {
            long longValue = a4.e.longValue();
            j();
            e61 a5 = kl1.a(d612, "_et");
            if (a5 != null) {
                Long l3 = a5.e;
                if (l3 != null && l3.longValue() > 0) {
                    longValue += a5.e.longValue();
                }
            }
            j();
            d612.c = kl1.a(d612.c, "_et", (Object) Long.valueOf(longValue));
            j();
            d61.c = kl1.a(d61.c, "_fr", (Object) 1L);
        }
        return true;
    }

    @DexIgnore
    public static e61[] a(e61[] e61Arr, String str) {
        int i2 = 0;
        while (true) {
            if (i2 >= e61Arr.length) {
                i2 = -1;
                break;
            } else if (str.equals(e61Arr[i2].c)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return e61Arr;
        }
        return a(e61Arr, i2);
    }

    @DexIgnore
    public static e61[] a(e61[] e61Arr, int i2) {
        e61[] e61Arr2 = new e61[(e61Arr.length - 1)];
        if (i2 > 0) {
            System.arraycopy(e61Arr, 0, e61Arr2, 0, i2);
        }
        if (i2 < e61Arr2.length) {
            System.arraycopy(e61Arr, i2 + 1, e61Arr2, i2, e61Arr2.length - i2);
        }
        return e61Arr2;
    }

    @DexIgnore
    public static e61[] a(e61[] e61Arr, int i2, String str) {
        for (e61 e61 : e61Arr) {
            if ("_err".equals(e61.c)) {
                return e61Arr;
            }
        }
        e61[] e61Arr2 = new e61[(e61Arr.length + 2)];
        System.arraycopy(e61Arr, 0, e61Arr2, 0, e61Arr.length);
        e61 e612 = new e61();
        e612.c = "_err";
        e612.e = Long.valueOf((long) i2);
        e61 e613 = new e61();
        e613.c = "_ev";
        e613.d = str;
        e61Arr2[e61Arr2.length - 2] = e612;
        e61Arr2[e61Arr2.length - 1] = e613;
        return e61Arr2;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        bm1 l2;
        f();
        r();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.r = false;
                w();
                throw th2;
            }
        }
        List<Long> list = this.v;
        this.v = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.i.t().e.a(this.i.c().b());
                this.i.t().f.a(0);
                v();
                this.i.d().A().a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                l().t();
                try {
                    for (Long next : list) {
                        try {
                            l2 = l();
                            long longValue = next.longValue();
                            l2.e();
                            l2.q();
                            if (l2.v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            l2.d().s().a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.w == null || !this.w.contains(next)) {
                                throw e3;
                            }
                        }
                    }
                    l().w();
                    l().u();
                    this.w = null;
                    if (!n().t() || !u()) {
                        this.x = -1;
                        v();
                    } else {
                        t();
                    }
                    this.m = 0;
                } catch (Throwable th3) {
                    l().u();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.i.d().s().a("Database error while trying to delete uploaded bundles", e4);
                this.m = this.i.c().c();
                this.i.d().A().a("Disable upload, time", Long.valueOf(this.m));
            }
        } else {
            this.i.d().A().a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.i.t().f.a(this.i.c().b());
            if (i2 != 503) {
                if (i2 != 429) {
                    z = false;
                }
            }
            if (z) {
                this.i.t().g.a(this.i.c().b());
            }
            if (this.i.u().g(str)) {
                l().a(list);
            }
            v();
        }
        this.r = false;
        w();
    }

    @DexIgnore
    public final void a(rl1 rl1) {
        g4 g4Var;
        f();
        if (!TextUtils.isEmpty(rl1.c()) || (yl1.w() && !TextUtils.isEmpty(rl1.h()))) {
            yl1 u2 = this.i.u();
            Uri.Builder builder = new Uri.Builder();
            String c2 = rl1.c();
            if (TextUtils.isEmpty(c2) && yl1.w()) {
                c2 = rl1.h();
            }
            Uri.Builder encodedAuthority = builder.scheme(kg1.o.a()).encodedAuthority(kg1.p.a());
            String valueOf = String.valueOf(c2);
            encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", rl1.a()).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", String.valueOf(u2.n()));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.i.d().A().a("Fetching remote configuration", rl1.f());
                z51 b2 = m().b(rl1.f());
                String c3 = m().c(rl1.f());
                if (b2 == null || TextUtils.isEmpty(c3)) {
                    g4Var = null;
                } else {
                    g4 g4Var2 = new g4();
                    g4Var2.put("If-Modified-Since", c3);
                    g4Var = g4Var2;
                }
                this.q = true;
                yg1 n2 = n();
                String f2 = rl1.f();
                hl1 hl1 = new hl1(this);
                n2.e();
                n2.q();
                ck0.a(url);
                ck0.a(hl1);
                n2.a().b((Runnable) new dh1(n2, f2, url, (byte[]) null, g4Var, hl1));
            } catch (MalformedURLException unused) {
                this.i.d().s().a("Failed to parse config URL. Not fetching. appId", ug1.a(rl1.f()), uri);
            }
        } else {
            a(rl1.f(), 204, (Throwable) null, (byte[]) null, (Map<String, List<String>>) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    public final void a(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        f();
        r();
        ck0.b(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.q = false;
                w();
                throw th2;
            }
        }
        this.i.d().A().a("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        l().t();
        rl1 b2 = l().b(str);
        boolean z = true;
        boolean z2 = (i2 == 200 || i2 == 204 || i2 == 304) && th == null;
        if (b2 == null) {
            this.i.d().v().a("App does not exist in onConfigFetched. appId", ug1.a(str));
        } else {
            if (!z2) {
                if (i2 != 404) {
                    b2.k(this.i.c().b());
                    l().a(b2);
                    this.i.d().A().a("Fetching config failed. code, error", Integer.valueOf(i2), th);
                    m().d(str);
                    this.i.t().f.a(this.i.c().b());
                    if (i2 != 503) {
                        if (i2 != 429) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.i.t().g.a(this.i.c().b());
                    }
                    v();
                }
            }
            List list = map != null ? map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i2 != 404) {
                if (i2 != 304) {
                    if (!m().a(str, bArr, str2)) {
                        l().u();
                        this.q = false;
                        w();
                        return;
                    }
                    b2.j(this.i.c().b());
                    l().a(b2);
                    if (i2 != 404) {
                        this.i.d().x().a("Config not found. Using empty config. appId", str);
                    } else {
                        this.i.d().A().a("Successfully fetched config. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                    }
                    if (n().t() || !u()) {
                        v();
                    } else {
                        t();
                    }
                }
            }
            if (m().b(str) == null && !m().a(str, (byte[]) null, (String) null)) {
                l().u();
                this.q = false;
                w();
                return;
            }
            b2.j(this.i.c().b());
            l().a(b2);
            if (i2 != 404) {
            }
            if (n().t()) {
            }
            v();
        }
        l().w();
        l().u();
        this.q = false;
        w();
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        f();
        if (this.n == null) {
            this.n = new ArrayList();
        }
        this.n.add(runnable);
    }

    @DexIgnore
    public final int a(FileChannel fileChannel) {
        f();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().s().a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.i.d().v().a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e2) {
            this.i.d().s().a("Failed to read from channel", e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean a(int i2, FileChannel fileChannel) {
        f();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().s().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.i.d().s().a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e2) {
            this.i.d().s().a("Failed to write to channel", e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(sl1 sl1) {
        if (this.v != null) {
            this.w = new ArrayList();
            this.w.addAll(this.v);
        }
        bm1 l2 = l();
        String str = sl1.e;
        ck0.b(str);
        l2.e();
        l2.q();
        try {
            SQLiteDatabase v2 = l2.v();
            String[] strArr = {str};
            int delete = v2.delete("apps", "app_id=?", strArr) + 0 + v2.delete("events", "app_id=?", strArr) + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("queue", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr) + v2.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                l2.d().A().a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            l2.d().s().a("Error resetting analytics data. appId, error", ug1.a(str), e2);
        }
        sl1 a2 = a(this.i.getContext(), sl1.e, sl1.f, sl1.l, sl1.s, sl1.t, sl1.q, sl1.v);
        if (!this.i.u().i(sl1.e) || sl1.l) {
            c(a2);
        }
    }

    @DexIgnore
    public final sl1 a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j2, String str3) {
        String str4;
        String str5;
        int i2;
        String str6 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.i.d().s().a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str6);
        } catch (IllegalArgumentException unused) {
            this.i.d().s().a("Error retrieving installer package name. appId", ug1.a(str));
            str4 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        String str7 = str4;
        try {
            PackageInfo b2 = cn0.b(context).b(str6, 0);
            if (b2 != null) {
                CharSequence b3 = cn0.b(context).b(str6);
                if (!TextUtils.isEmpty(b3)) {
                    String charSequence = b3.toString();
                }
                String str8 = b2.versionName;
                i2 = b2.versionCode;
                str5 = str8;
            } else {
                i2 = Integer.MIN_VALUE;
                str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            }
            this.i.b();
            return new sl1(str, str2, str5, (long) i2, str7, this.i.u().n(), this.i.s().a(context, str6), (String) null, z, false, "", 0, this.i.u().k(str6) ? j2 : 0, 0, z2, z3, false, str3);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.i.d().s().a("Error retrieving newly installed package info. appId, appName", ug1.a(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    @DexIgnore
    public final void a(ll1 ll1, sl1 sl1) {
        f();
        r();
        if (TextUtils.isEmpty(sl1.f) && TextUtils.isEmpty(sl1.v)) {
            return;
        }
        if (!sl1.l) {
            d(sl1);
            return;
        }
        int b2 = this.i.s().b(ll1.f);
        if (b2 != 0) {
            this.i.s();
            String a2 = ol1.a(ll1.f, 24, true);
            String str = ll1.f;
            this.i.s().a(sl1.e, b2, "_ev", a2, str != null ? str.length() : 0);
            return;
        }
        int b3 = this.i.s().b(ll1.f, ll1.H());
        if (b3 != 0) {
            this.i.s();
            String a3 = ol1.a(ll1.f, 24, true);
            Object H = ll1.H();
            this.i.s().a(sl1.e, b3, "_ev", a3, (H == null || (!(H instanceof String) && !(H instanceof CharSequence))) ? 0 : String.valueOf(H).length());
            return;
        }
        Object c2 = this.i.s().c(ll1.f, ll1.H());
        if (c2 != null) {
            if (this.i.u().p(sl1.e) && "_sno".equals(ll1.f)) {
                long j2 = 0;
                nl1 d2 = l().d(sl1.e, "_sno");
                if (d2 != null) {
                    Object obj = d2.e;
                    if (obj instanceof Long) {
                        j2 = ((Long) obj).longValue();
                        c2 = Long.valueOf(j2 + 1);
                    }
                }
                eg1 b4 = l().b(sl1.e, "_s");
                if (b4 != null) {
                    j2 = b4.c;
                    this.i.d().A().a("Backfill the session number. Last used session number", Long.valueOf(j2));
                }
                c2 = Long.valueOf(j2 + 1);
            }
            nl1 nl1 = new nl1(sl1.e, ll1.j, ll1.f, ll1.g, c2);
            this.i.d().z().a("Setting user property", this.i.r().c(nl1.c), c2);
            l().t();
            try {
                d(sl1);
                boolean a4 = l().a(nl1);
                l().w();
                if (a4) {
                    this.i.d().z().a("User property set", this.i.r().c(nl1.c), nl1.e);
                } else {
                    this.i.d().s().a("Too many unique user properties are set. Ignoring user property", this.i.r().c(nl1.c), nl1.e);
                    this.i.s().a(sl1.e, 9, (String) null, (String) null, 0);
                }
            } finally {
                l().u();
            }
        }
    }

    @DexIgnore
    public final void a(dl1 dl1) {
        this.o++;
    }

    @DexIgnore
    public final sl1 a(String str) {
        String str2 = str;
        rl1 b2 = l().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.e())) {
            this.i.d().z().a("No app data available; dropping", str2);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            rl1 rl1 = b2;
            return new sl1(str, b2.c(), b2.e(), b2.l(), b2.m(), b2.n(), b2.o(), (String) null, b2.d(), false, b2.b(), rl1.B(), 0, 0, rl1.C(), rl1.D(), false, rl1.h());
        }
        this.i.d().s().a("App version does not match; dropping. appId", ug1.a(str));
        return null;
    }

    @DexIgnore
    public final void a(wl1 wl1) {
        sl1 a2 = a(wl1.e);
        if (a2 != null) {
            a(wl1, a2);
        }
    }

    @DexIgnore
    public final void a(wl1 wl1, sl1 sl1) {
        ck0.a(wl1);
        ck0.b(wl1.e);
        ck0.a(wl1.f);
        ck0.a(wl1.g);
        ck0.b(wl1.g.f);
        f();
        r();
        if (TextUtils.isEmpty(sl1.f) && TextUtils.isEmpty(sl1.v)) {
            return;
        }
        if (!sl1.l) {
            d(sl1);
            return;
        }
        wl1 wl12 = new wl1(wl1);
        boolean z = false;
        wl12.i = false;
        l().t();
        try {
            wl1 e2 = l().e(wl12.e, wl12.g.f);
            if (e2 != null && !e2.f.equals(wl12.f)) {
                this.i.d().v().a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.i.r().c(wl12.g.f), wl12.f, e2.f);
            }
            if (e2 != null && e2.i) {
                wl12.f = e2.f;
                wl12.h = e2.h;
                wl12.l = e2.l;
                wl12.j = e2.j;
                wl12.m = e2.m;
                wl12.i = e2.i;
                wl12.g = new ll1(wl12.g.f, e2.g.g, wl12.g.H(), e2.g.j);
            } else if (TextUtils.isEmpty(wl12.j)) {
                wl12.g = new ll1(wl12.g.f, wl12.h, wl12.g.H(), wl12.g.j);
                wl12.i = true;
                z = true;
            }
            if (wl12.i) {
                ll1 ll1 = wl12.g;
                nl1 nl1 = new nl1(wl12.e, wl12.f, ll1.f, ll1.g, ll1.H());
                if (l().a(nl1)) {
                    this.i.d().z().a("User property updated immediately", wl12.e, this.i.r().c(nl1.c), nl1.e);
                } else {
                    this.i.d().s().a("(2)Too many active user properties, ignoring", ug1.a(wl12.e), this.i.r().c(nl1.c), nl1.e);
                }
                if (z && wl12.m != null) {
                    b(new ig1(wl12.m, wl12.h), sl1);
                }
            }
            if (l().a(wl12)) {
                this.i.d().z().a("Conditional property added", wl12.e, this.i.r().c(wl12.g.f), wl12.g.H());
            } else {
                this.i.d().s().a("Too many conditional properties, ignoring", ug1.a(wl12.e), this.i.r().c(wl12.g.f), wl12.g.H());
            }
            l().w();
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        v();
    }
}
