package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.error.Error;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g90<V> extends h90<V> {
    @DexIgnore
    public g90<V> f(jd4<? super cb4, cb4> jd4) {
        wd4.b(jd4, "actionOnFinal");
        h90<V> f = super.f(jd4);
        if (f != null) {
            return (g90) f;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public g90<V> i(jd4<? super Float, cb4> jd4) {
        wd4.b(jd4, "actionOnProgressChange");
        h90<V> d = super.d(jd4);
        if (d != null) {
            return (g90) d;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public g90<V> g(jd4<? super Error, cb4> jd4) {
        wd4.b(jd4, "actionOnError");
        h90<V> g = super.c(jd4);
        if (g != null) {
            return (g90) g;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public g90<V> h(jd4<? super V, cb4> jd4) {
        wd4.b(jd4, "actionOnSuccess");
        h90<V> h = super.e(jd4);
        if (h != null) {
            return (g90) h;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public g90<V> b(jd4<? super Error, cb4> jd4) {
        wd4.b(jd4, "actionOnCancel");
        h90<V> b = super.b(jd4);
        if (b != null) {
            return (g90) b;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }
}
