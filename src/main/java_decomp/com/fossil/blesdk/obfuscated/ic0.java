package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ic0 implements xb0 {
    @DexIgnore
    public final ie0<Status> a(he0 he0) {
        return kc0.b(he0, he0.e(), false);
    }

    @DexIgnore
    public final ac0 a(Intent intent) {
        return kc0.a(intent);
    }
}
