package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.km4;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tm4 {
    @DexIgnore
    public static tm4 a;

    @DexIgnore
    public abstract int a(Response.a aVar);

    @DexIgnore
    public abstract fn4 a(am4 am4, sl4 sl4, in4 in4, rm4 rm4);

    @DexIgnore
    public abstract gn4 a(am4 am4);

    @DexIgnore
    public abstract IOException a(vl4 vl4, IOException iOException);

    @DexIgnore
    public abstract Socket a(am4 am4, sl4 sl4, in4 in4);

    @DexIgnore
    public abstract void a(bm4 bm4, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract void a(km4.a aVar, String str);

    @DexIgnore
    public abstract void a(km4.a aVar, String str, String str2);

    @DexIgnore
    public abstract boolean a(am4 am4, fn4 fn4);

    @DexIgnore
    public abstract boolean a(sl4 sl4, sl4 sl42);

    @DexIgnore
    public abstract void b(am4 am4, fn4 fn4);
}
