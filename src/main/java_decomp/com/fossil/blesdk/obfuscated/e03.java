package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e03 implements Factory<ArrayList<String>> {
    @DexIgnore
    public static ArrayList<String> a(d03 d03) {
        ArrayList<String> b = d03.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
