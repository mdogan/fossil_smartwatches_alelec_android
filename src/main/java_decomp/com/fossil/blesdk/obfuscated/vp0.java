package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vp0 implements Parcelable.Creator<cp0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        Boolean bool = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                bool = SafeParcelReader.j(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new cp0(str, i, bool);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new cp0[i];
    }
}
