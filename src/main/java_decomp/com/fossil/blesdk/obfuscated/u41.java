package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u41 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<u41> CREATOR; // = new v41();
    @DexIgnore
    public static /* final */ List<kj0> h; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ ld1 i; // = new ld1();
    @DexIgnore
    public ld1 e;
    @DexIgnore
    public List<kj0> f;
    @DexIgnore
    public String g;

    @DexIgnore
    public u41(ld1 ld1, List<kj0> list, String str) {
        this.e = ld1;
        this.f = list;
        this.g = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof u41)) {
            return false;
        }
        u41 u41 = (u41) obj;
        return ak0.a(this.e, u41.e) && ak0.a(this.f, u41.f) && ak0.a(this.g, u41.g);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) this.e, i2, false);
        lk0.b(parcel, 2, this.f, false);
        lk0.a(parcel, 3, this.g, false);
        lk0.a(parcel, a);
    }
}
