package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.qj0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg0 extends he0 implements dh0 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ qj0 d;
    @DexIgnore
    public ch0 e; // = null;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ Queue<ue0<?, ?>> i; // = new LinkedList();
    @DexIgnore
    public volatile boolean j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public /* final */ lg0 m;
    @DexIgnore
    public /* final */ yd0 n;
    @DexIgnore
    public ah0 o;
    @DexIgnore
    public /* final */ Map<ee0.c<?>, ee0.f> p;
    @DexIgnore
    public Set<Scope> q;
    @DexIgnore
    public /* final */ lj0 r;
    @DexIgnore
    public /* final */ Map<ee0<?>, Boolean> s;
    @DexIgnore
    public /* final */ ee0.a<? extends mn1, wm1> t;
    @DexIgnore
    public /* final */ bf0 u;
    @DexIgnore
    public /* final */ ArrayList<hi0> v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public Set<nh0> x;
    @DexIgnore
    public /* final */ qh0 y;
    @DexIgnore
    public /* final */ qj0.a z;

    @DexIgnore
    public fg0(Context context, Lock lock, Looper looper, lj0 lj0, yd0 yd0, ee0.a<? extends mn1, wm1> aVar, Map<ee0<?>, Boolean> map, List<he0.b> list, List<he0.c> list2, Map<ee0.c<?>, ee0.f> map2, int i2, int i3, ArrayList<hi0> arrayList, boolean z2) {
        Looper looper2 = looper;
        this.k = hm0.a() ? ButtonService.CONNECT_TIMEOUT : ScanService.BLE_SCAN_TIMEOUT;
        this.l = 5000;
        this.q = new HashSet();
        this.u = new bf0();
        this.w = null;
        this.x = null;
        this.z = new gg0(this);
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new qj0(looper, this.z);
        this.h = looper2;
        this.m = new lg0(this, looper);
        this.n = yd0;
        this.f = i2;
        if (this.f >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new qh0(this.p);
        for (he0.b a : list) {
            this.d.a(a);
        }
        for (he0.c a2 : list2) {
            this.d.a(a2);
        }
        this.r = lj0;
        this.t = aVar;
    }

    @DexIgnore
    public static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T a(T t2) {
        ck0.a(t2.i() != null, (Object) "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        ck0.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                this.i.add(t2);
                return t2;
            }
            T b3 = this.e.b(t2);
            this.b.unlock();
            return b3;
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T b(T t2) {
        ck0.a(t2.i() != null, (Object) "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        ck0.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    ue0 remove = this.i.remove();
                    this.y.a(remove);
                    remove.c(Status.k);
                }
                return t2;
            } else {
                T a = this.e.a(t2);
                this.b.unlock();
                return a;
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void c() {
        this.b.lock();
        try {
            boolean z2 = false;
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                ck0.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<ee0.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.a();
            }
            this.u.a();
            for (ue0 ue0 : this.i) {
                ue0.a((th0) null);
                ue0.a();
            }
            this.i.clear();
            if (this.e != null) {
                o();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final Context e() {
        return this.g;
    }

    @DexIgnore
    public final Looper f() {
        return this.h;
    }

    @DexIgnore
    public final boolean g() {
        ch0 ch0 = this.e;
        return ch0 != null && ch0.c();
    }

    @DexIgnore
    public final void h() {
        ch0 ch0 = this.e;
        if (ch0 != null) {
            ch0.e();
        }
    }

    @DexIgnore
    public final void k() {
        d();
        c();
    }

    @DexIgnore
    public final void l() {
        this.b.lock();
        try {
            if (this.j) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void m() {
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final void n() {
        this.b.lock();
        try {
            if (o()) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final boolean o() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        ah0 ah0 = this.o;
        if (ah0 != null) {
            ah0.a();
            this.o = null;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean p() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean z2 = !this.x.isEmpty();
            this.b.unlock();
            return z2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final String q() {
        StringWriter stringWriter = new StringWriter();
        a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    @DexIgnore
    public final void a(int i2) {
        this.b.lock();
        boolean z2 = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            ck0.a(z2, (Object) sb.toString());
            b(i2);
            m();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final vd0 a() {
        boolean z2 = true;
        ck0.b(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w == null) {
                    z2 = false;
                }
                ck0.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<ee0.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            b(this.w.intValue());
            this.d.b();
            return this.e.f();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final ie0<Status> b() {
        ck0.b(g(), "GoogleApiClient is not connected yet.");
        ck0.b(this.w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        ff0 ff0 = new ff0(this);
        if (this.p.containsKey(mk0.a)) {
            a(this, ff0, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            hg0 hg0 = new hg0(this, atomicReference, ff0);
            ig0 ig0 = new ig0(this, ff0);
            he0.a aVar = new he0.a(this.g);
            aVar.a((ee0<? extends ee0.d.C0011d>) mk0.c);
            aVar.a((he0.b) hg0);
            aVar.a((he0.c) ig0);
            aVar.a((Handler) this.m);
            he0 a = aVar.a();
            atomicReference.set(a);
            a.c();
        }
        return ff0;
    }

    @DexIgnore
    public final void a(he0 he0, ff0 ff0, boolean z2) {
        mk0.d.a(he0).a(new kg0(this, ff0, z2, he0));
    }

    @DexIgnore
    public final void a(he0.c cVar) {
        this.d.a(cVar);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        while (!this.i.isEmpty()) {
            b(this.i.remove());
        }
        this.d.a(bundle);
    }

    @DexIgnore
    public final void b(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (ee0.f next : this.p.values()) {
                if (next.l()) {
                    z2 = true;
                }
                if (next.d()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z2) {
                        if (this.c) {
                            this.e = new oi0(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                            return;
                        }
                        this.e = ji0.a(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new og0(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
                return;
            }
            this.e = new oi0(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
        }
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        if (!this.n.b(this.g, vd0.H())) {
            o();
        }
        if (!this.j) {
            this.d.a(vd0);
            this.d.a();
        }
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !hm0.a()) {
                this.o = this.n.a(this.g.getApplicationContext(), (bh0) new mg0(this));
            }
            lg0 lg0 = this.m;
            lg0.sendMessageDelayed(lg0.obtainMessage(1), this.k);
            lg0 lg02 = this.m;
            lg02.sendMessageDelayed(lg02.obtainMessage(2), this.l);
        }
        this.y.b();
        this.d.a(i2);
        this.d.a();
        if (i2 == 2) {
            m();
        }
    }

    @DexIgnore
    public final void b(he0.c cVar) {
        this.d.b(cVar);
    }

    @DexIgnore
    public final boolean a(df0 df0) {
        ch0 ch0 = this.e;
        return ch0 != null && ch0.a(df0);
    }

    @DexIgnore
    public final void a(nh0 nh0) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(nh0)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!p()) {
                this.e.d();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.g);
        printWriter.append(str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.a.size());
        ch0 ch0 = this.e;
        if (ch0 != null) {
            ch0.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public static int a(Iterable<ee0.f> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (ee0.f next : iterable) {
            if (next.l()) {
                z3 = true;
            }
            if (next.d()) {
                z4 = true;
            }
        }
        if (!z3) {
            return 3;
        }
        if (!z4 || !z2) {
            return 1;
        }
        return 2;
    }
}
