package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sx {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ux b;
    @DexIgnore
    public rx c;

    @DexIgnore
    public sx(Context context) {
        this(context, new ux());
    }

    @DexIgnore
    public rx a() {
        if (this.c == null) {
            this.c = lx.b(this.a);
        }
        return this.c;
    }

    @DexIgnore
    public sx(Context context, ux uxVar) {
        this.a = context;
        this.b = uxVar;
    }

    @DexIgnore
    public void a(SessionEvent sessionEvent) {
        rx a2 = a();
        if (a2 == null) {
            r44.g().d("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        tx a3 = this.b.a(sessionEvent);
        if (a3 == null) {
            z44 g = r44.g();
            g.d("Answers", "Fabric event was not mappable to Firebase event: " + sessionEvent);
            return;
        }
        a2.a(a3.a(), a3.b());
        if ("levelEnd".equals(sessionEvent.g)) {
            a2.a("post_score", a3.b());
        }
    }
}
