package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w80 extends q70 {
    @DexIgnore
    public /* final */ boolean L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w80(Peripheral peripheral) {
        super(DeviceConfigOperationCode.LEGACY_OTA_ENTER, RequestId.LEGACY_OTA_ENTER, peripheral, 0, 8, (rd4) null);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public boolean F() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        c(true);
        return a;
    }
}
