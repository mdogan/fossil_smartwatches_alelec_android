package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lo2 implements Factory<ko2> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<fn2> b;

    @DexIgnore
    public lo2(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static lo2 a(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        return new lo2(provider, provider2);
    }

    @DexIgnore
    public static ko2 b(Provider<DianaNotificationComponent> provider, Provider<fn2> provider2) {
        ko2 ko2 = new ko2();
        mo2.a(ko2, provider.get());
        mo2.a(ko2, provider2.get());
        return ko2;
    }

    @DexIgnore
    public ko2 get() {
        return b(this.a, this.b);
    }
}
