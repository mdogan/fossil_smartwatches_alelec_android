package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ja2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ RecyclerView s;

    @DexIgnore
    public ja2(Object obj, View view, int i, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, RTLImageView rTLImageView2, View view2, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = rTLImageView2;
        this.s = recyclerView;
    }
}
