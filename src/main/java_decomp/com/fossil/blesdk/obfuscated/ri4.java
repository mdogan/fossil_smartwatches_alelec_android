package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ri4 extends CoroutineContext.a {
    @DexIgnore
    public static final b d = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <R> R a(ri4 ri4, R r, kd4<? super R, ? super CoroutineContext.a, ? extends R> kd4) {
            wd4.b(kd4, "operation");
            return CoroutineContext.a.C0169a.a(ri4, r, kd4);
        }

        @DexIgnore
        public static <E extends CoroutineContext.a> E a(ri4 ri4, CoroutineContext.b<E> bVar) {
            wd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) ri4, bVar);
        }

        @DexIgnore
        public static CoroutineContext a(ri4 ri4, CoroutineContext coroutineContext) {
            wd4.b(coroutineContext, "context");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) ri4, coroutineContext);
        }

        @DexIgnore
        public static CoroutineContext b(ri4 ri4, CoroutineContext.b<?> bVar) {
            wd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.b(ri4, bVar);
        }

        @DexIgnore
        public static /* synthetic */ ai4 a(ri4 ri4, boolean z, boolean z2, jd4 jd4, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return ri4.a(z, z2, jd4);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineContext.b<ri4> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.c;
        }
        */
    }

    @DexIgnore
    ai4 a(jd4<? super Throwable, cb4> jd4);

    @DexIgnore
    ai4 a(boolean z, boolean z2, jd4<? super Throwable, cb4> jd4);

    @DexIgnore
    ug4 a(wg4 wg4);

    @DexIgnore
    void a(CancellationException cancellationException);

    @DexIgnore
    /* synthetic */ void cancel();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    boolean start();

    @DexIgnore
    CancellationException y();
}
