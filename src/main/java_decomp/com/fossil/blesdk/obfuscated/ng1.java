package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ng1 extends k61 implements lg1 {
    @DexIgnore
    public ng1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final void a(ig1 ig1, sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) ig1);
        s61.a(o, (Parcelable) sl1);
        b(1, o);
    }

    @DexIgnore
    public final void c(sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) sl1);
        b(6, o);
    }

    @DexIgnore
    public final String d(sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) sl1);
        Parcel a = a(11, o);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    public final void a(ll1 ll1, sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) ll1);
        s61.a(o, (Parcelable) sl1);
        b(2, o);
    }

    @DexIgnore
    public final void a(sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) sl1);
        b(4, o);
    }

    @DexIgnore
    public final void a(ig1 ig1, String str, String str2) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) ig1);
        o.writeString(str);
        o.writeString(str2);
        b(5, o);
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel o = o();
        o.writeLong(j);
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        b(10, o);
    }

    @DexIgnore
    public final void a(wl1 wl1, sl1 sl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) wl1);
        s61.a(o, (Parcelable) sl1);
        b(12, o);
    }

    @DexIgnore
    public final void a(wl1 wl1) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) wl1);
        b(13, o);
    }

    @DexIgnore
    public final List<ll1> a(String str, String str2, boolean z, sl1 sl1) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        s61.a(o, z);
        s61.a(o, (Parcelable) sl1);
        Parcel a = a(14, o);
        ArrayList<ll1> createTypedArrayList = a.createTypedArrayList(ll1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<ll1> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        s61.a(o, z);
        Parcel a = a(15, o);
        ArrayList<ll1> createTypedArrayList = a.createTypedArrayList(ll1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<wl1> a(String str, String str2, sl1 sl1) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        s61.a(o, (Parcelable) sl1);
        Parcel a = a(16, o);
        ArrayList<wl1> createTypedArrayList = a.createTypedArrayList(wl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<wl1> a(String str, String str2, String str3) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        Parcel a = a(17, o);
        ArrayList<wl1> createTypedArrayList = a.createTypedArrayList(wl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }
}
