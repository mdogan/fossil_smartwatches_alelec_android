package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s02 extends JsonReader {
    @DexIgnore
    public static /* final */ Reader y; // = new a();
    @DexIgnore
    public static /* final */ Object z; // = new Object();
    @DexIgnore
    public Object[] u; // = new Object[32];
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public String[] w; // = new String[32];
    @DexIgnore
    public int[] x; // = new int[32];

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Reader {
        @DexIgnore
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public s02(JsonElement jsonElement) {
        super(y);
        a((Object) jsonElement);
    }

    @DexIgnore
    private String H() {
        return " at path " + w();
    }

    @DexIgnore
    public void D() throws IOException {
        a(JsonToken.END_ARRAY);
        Y();
        Y();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public void E() throws IOException {
        a(JsonToken.END_OBJECT);
        Y();
        Y();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public boolean F() throws IOException {
        JsonToken Q = Q();
        return (Q == JsonToken.END_OBJECT || Q == JsonToken.END_ARRAY) ? false : true;
    }

    @DexIgnore
    public boolean I() throws IOException {
        a(JsonToken.BOOLEAN);
        boolean a2 = ((a02) Y()).a();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return a2;
    }

    @DexIgnore
    public double J() throws IOException {
        JsonToken Q = Q();
        if (Q == JsonToken.NUMBER || Q == JsonToken.STRING) {
            double l = ((a02) X()).l();
            if (G() || (!Double.isNaN(l) && !Double.isInfinite(l))) {
                Y();
                int i = this.v;
                if (i > 0) {
                    int[] iArr = this.x;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return l;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + l);
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + Q + H());
    }

    @DexIgnore
    public int K() throws IOException {
        JsonToken Q = Q();
        if (Q == JsonToken.NUMBER || Q == JsonToken.STRING) {
            int b = ((a02) X()).b();
            Y();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return b;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + Q + H());
    }

    @DexIgnore
    public long L() throws IOException {
        JsonToken Q = Q();
        if (Q == JsonToken.NUMBER || Q == JsonToken.STRING) {
            long m = ((a02) X()).m();
            Y();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return m;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + Q + H());
    }

    @DexIgnore
    public String M() throws IOException {
        a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) X()).next();
        String str = (String) entry.getKey();
        this.w[this.v - 1] = str;
        a(entry.getValue());
        return str;
    }

    @DexIgnore
    public void N() throws IOException {
        a(JsonToken.NULL);
        Y();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public String O() throws IOException {
        JsonToken Q = Q();
        if (Q == JsonToken.STRING || Q == JsonToken.NUMBER) {
            String f = ((a02) Y()).f();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return f;
        }
        throw new IllegalStateException("Expected " + JsonToken.STRING + " but was " + Q + H());
    }

    @DexIgnore
    public JsonToken Q() throws IOException {
        if (this.v == 0) {
            return JsonToken.END_DOCUMENT;
        }
        Object X = X();
        if (X instanceof Iterator) {
            boolean z2 = this.u[this.v - 2] instanceof yz1;
            Iterator it = (Iterator) X;
            if (!it.hasNext()) {
                return z2 ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
            }
            if (z2) {
                return JsonToken.NAME;
            }
            a(it.next());
            return Q();
        } else if (X instanceof yz1) {
            return JsonToken.BEGIN_OBJECT;
        } else {
            if (X instanceof uz1) {
                return JsonToken.BEGIN_ARRAY;
            }
            if (X instanceof a02) {
                a02 a02 = (a02) X;
                if (a02.q()) {
                    return JsonToken.STRING;
                }
                if (a02.o()) {
                    return JsonToken.BOOLEAN;
                }
                if (a02.p()) {
                    return JsonToken.NUMBER;
                }
                throw new AssertionError();
            } else if (X instanceof xz1) {
                return JsonToken.NULL;
            } else {
                if (X == z) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    @DexIgnore
    public void W() throws IOException {
        if (Q() == JsonToken.NAME) {
            M();
            this.w[this.v - 2] = "null";
        } else {
            Y();
            int i = this.v;
            if (i > 0) {
                this.w[i - 1] = "null";
            }
        }
        int i2 = this.v;
        if (i2 > 0) {
            int[] iArr = this.x;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    @DexIgnore
    public final Object X() {
        return this.u[this.v - 1];
    }

    @DexIgnore
    public final Object Y() {
        Object[] objArr = this.u;
        int i = this.v - 1;
        this.v = i;
        Object obj = objArr[i];
        objArr[this.v] = null;
        return obj;
    }

    @DexIgnore
    public void Z() throws IOException {
        a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) X()).next();
        a(entry.getValue());
        a((Object) new a02((String) entry.getKey()));
    }

    @DexIgnore
    public final void a(JsonToken jsonToken) throws IOException {
        if (Q() != jsonToken) {
            throw new IllegalStateException("Expected " + jsonToken + " but was " + Q() + H());
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.u = new Object[]{z};
        this.v = 1;
    }

    @DexIgnore
    public String toString() {
        return s02.class.getSimpleName();
    }

    @DexIgnore
    public String w() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.v) {
            Object[] objArr = this.u;
            if (objArr[i] instanceof uz1) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.x[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof yz1) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.w;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    @DexIgnore
    public void y() throws IOException {
        a(JsonToken.BEGIN_ARRAY);
        a((Object) ((uz1) X()).iterator());
        this.x[this.v - 1] = 0;
    }

    @DexIgnore
    public void z() throws IOException {
        a(JsonToken.BEGIN_OBJECT);
        a((Object) ((yz1) X()).entrySet().iterator());
    }

    @DexIgnore
    public final void a(Object obj) {
        int i = this.v;
        Object[] objArr = this.u;
        if (i == objArr.length) {
            Object[] objArr2 = new Object[(i * 2)];
            int[] iArr = new int[(i * 2)];
            String[] strArr = new String[(i * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.x, 0, iArr, 0, this.v);
            System.arraycopy(this.w, 0, strArr, 0, this.v);
            this.u = objArr2;
            this.x = iArr;
            this.w = strArr;
        }
        Object[] objArr3 = this.u;
        int i2 = this.v;
        this.v = i2 + 1;
        objArr3[i2] = obj;
    }
}
