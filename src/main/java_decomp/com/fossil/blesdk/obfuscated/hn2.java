package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.enums.Unit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class hn2 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Unit.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[Unit.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[Unit.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[LocationSource.ErrorState.values().length];

    /*
    static {
        a[Unit.METRIC.ordinal()] = 1;
        a[Unit.IMPERIAL.ordinal()] = 2;
        b[Unit.METRIC.ordinal()] = 1;
        b[Unit.IMPERIAL.ordinal()] = 2;
        c[Unit.METRIC.ordinal()] = 1;
        c[Unit.IMPERIAL.ordinal()] = 2;
        d[LocationSource.ErrorState.LOCATION_PERMISSION_OFF.ordinal()] = 1;
        d[LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF.ordinal()] = 2;
        d[LocationSource.ErrorState.LOCATION_SERVICE_OFF.ordinal()] = 3;
    }
    */
}
