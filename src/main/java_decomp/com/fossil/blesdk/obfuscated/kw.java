package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kw implements ko {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public kw(Object obj) {
        uw.a(obj);
        this.b = obj;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(ko.a));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof kw) {
            return this.b.equals(((kw) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
