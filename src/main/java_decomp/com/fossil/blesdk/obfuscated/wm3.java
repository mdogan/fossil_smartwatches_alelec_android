package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wm3 extends sm3 {
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ tm3 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public wm3(tm3 tm3) {
        wd4.b(tm3, "mPairingInstructionsView");
        this.g = tm3;
    }

    @DexIgnore
    public void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void f() {
        this.g.g();
        this.g.s(!this.f);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        cn2 cn2 = cn2.d;
        tm3 tm3 = this.g;
        if (tm3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (cn2.a(cn2, ((nm3) tm3).getContext(), "PAIR_DEVICE", false, 4, (Object) null)) {
            this.g.c0();
        }
    }

    @DexIgnore
    public boolean i() {
        return this.f;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }
}
