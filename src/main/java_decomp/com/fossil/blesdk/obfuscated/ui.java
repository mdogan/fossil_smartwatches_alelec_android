package com.fossil.blesdk.obfuscated;

import android.os.Parcelable;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ui {
    @DexIgnore
    public static <T extends wi> Class c(T t) throws ClassNotFoundException {
        return a((Class<? extends wi>) t.getClass());
    }

    @DexIgnore
    public int a(int i, int i2) {
        if (!a(i2)) {
            return i;
        }
        return e();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(Parcelable parcelable);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public void a(boolean z, boolean z2) {
    }

    @DexIgnore
    public abstract void a(byte[] bArr);

    @DexIgnore
    public abstract boolean a(int i);

    @DexIgnore
    public abstract ui b();

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public void b(byte[] bArr, int i) {
        b(i);
        a(bArr);
    }

    @DexIgnore
    public abstract void c(int i);

    @DexIgnore
    public boolean c() {
        return false;
    }

    @DexIgnore
    public abstract byte[] d();

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public abstract <T extends Parcelable> T f();

    @DexIgnore
    public abstract String g();

    @DexIgnore
    public <T extends wi> T h() {
        String g = g();
        if (g == null) {
            return null;
        }
        return a(g, b());
    }

    @DexIgnore
    public String a(String str, int i) {
        if (!a(i)) {
            return str;
        }
        return g();
    }

    @DexIgnore
    public void b(int i, int i2) {
        b(i2);
        c(i);
    }

    @DexIgnore
    public byte[] a(byte[] bArr, int i) {
        if (!a(i)) {
            return bArr;
        }
        return d();
    }

    @DexIgnore
    public void b(String str, int i) {
        b(i);
        a(str);
    }

    @DexIgnore
    public <T extends Parcelable> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return f();
    }

    @DexIgnore
    public void b(Parcelable parcelable, int i) {
        b(i);
        a(parcelable);
    }

    @DexIgnore
    public void a(wi wiVar) {
        if (wiVar == null) {
            a((String) null);
            return;
        }
        b(wiVar);
        ui b = b();
        a(wiVar, b);
        b.a();
    }

    @DexIgnore
    public void b(wi wiVar, int i) {
        b(i);
        a(wiVar);
    }

    @DexIgnore
    public final void b(wi wiVar) {
        try {
            a(a((Class<? extends wi>) wiVar.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(wiVar.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    @DexIgnore
    public <T extends wi> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return h();
    }

    @DexIgnore
    public static <T extends wi> T a(String str, ui uiVar) {
        Class<ui> cls = ui.class;
        try {
            return (wi) Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod(HardwareLog.COLUMN_READ, new Class[]{cls}).invoke((Object) null, new Object[]{uiVar});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public static <T extends wi> void a(T t, ui uiVar) {
        try {
            c(t).getDeclaredMethod("write", new Class[]{t.getClass(), ui.class}).invoke((Object) null, new Object[]{t, uiVar});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    public static Class a(Class<? extends wi> cls) throws ClassNotFoundException {
        return Class.forName(String.format("%s.%sParcelizer", new Object[]{cls.getPackage().getName(), cls.getSimpleName()}), false, cls.getClassLoader());
    }
}
