package com.fossil.blesdk.obfuscated;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sz {
    @DexIgnore
    String a();

    @DexIgnore
    InputStream b();

    @DexIgnore
    String[] c();
}
