package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tz1<T> {
    @DexIgnore
    T createInstance(Type type);
}
