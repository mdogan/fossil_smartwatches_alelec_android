package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ms2 extends RecyclerView.g<a> {
    @DexIgnore
    public List<String> a;
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ ms2 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ms2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ms2$a$a  reason: collision with other inner class name */
        public static final class C0090a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0090a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (this.e.getAdapterPosition() != -1) {
                    b b = this.e.b.b;
                    if (b != null) {
                        List a = this.e.b.a;
                        if (a != null) {
                            b.a((String) a.get(adapterPosition));
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ms2 ms2, View view) {
            super(view);
            wd4.b(view, "view");
            this.b = ms2;
            view.setOnClickListener(new C0090a(this));
            View findViewById = view.findViewById(R.id.tv_text);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public int getItemCount() {
        List<String> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wd4.b(aVar, "holder");
        TextView a2 = aVar.a();
        List<String> list = this.a;
        if (list != null) {
            a2.setText(list.get(i));
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address_search, viewGroup, false);
        wd4.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<String> list) {
        wd4.b(list, "addressSearchList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "listener");
        this.b = bVar;
    }
}
