package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jx2 extends ys3 implements ix2 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a((rd4) null);
    @DexIgnore
    public /* final */ qa m; // = new w62(this);
    @DexIgnore
    public ur3<he2> n;
    @DexIgnore
    public hx2 o;
    @DexIgnore
    public k42 p;
    @DexIgnore
    public ex2 q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jx2.s;
        }

        @DexIgnore
        public final jx2 b() {
            return new jx2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jx2 e;

        @DexIgnore
        public b(jx2 jx2) {
            this.e = jx2;
        }

        @DexIgnore
        public final void onClick(View view) {
            jx2.a(this.e).a(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jx2 e;

        @DexIgnore
        public c(jx2 jx2) {
            this.e = jx2;
        }

        @DexIgnore
        public final void onClick(View view) {
            jx2.a(this.e).a(1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jx2 e;

        @DexIgnore
        public d(jx2 jx2) {
            this.e = jx2;
        }

        @DexIgnore
        public final void onClick(View view) {
            jx2.a(this.e).a(2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jx2 e;

        @DexIgnore
        public e(jx2 jx2) {
            this.e = jx2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = jx2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ hx2 a(jx2 jx2) {
        hx2 hx2 = jx2.o;
        if (hx2 != null) {
            return hx2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void P0() {
        ur3<he2> ur3 = this.n;
        if (ur3 != null) {
            he2 a2 = ur3.a();
            if (a2 != null) {
                ImageView imageView = a2.v;
                wd4.a((Object) imageView, "it.ivEveryoneCheck");
                imageView.setVisibility(4);
                ImageView imageView2 = a2.w;
                wd4.a((Object) imageView2, "it.ivFavoriteContactsCheck");
                imageView2.setVisibility(4);
                ImageView imageView3 = a2.x;
                wd4.a((Object) imageView3, "it.ivNoOneCheck");
                imageView3.setVisibility(4);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(String str) {
        wd4.b(str, "title");
        ur3<he2> ur3 = this.n;
        if (ur3 != null) {
            he2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void m(int i) {
        P0();
        ur3<he2> ur3 = this.n;
        if (ur3 != null) {
            he2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                ImageView imageView = a2.v;
                wd4.a((Object) imageView, "it.ivEveryoneCheck");
                imageView.setVisibility(0);
            } else if (i == 1) {
                ImageView imageView2 = a2.w;
                wd4.a((Object) imageView2, "it.ivFavoriteContactsCheck");
                imageView2.setVisibility(0);
            } else if (i == 2) {
                ImageView imageView3 = a2.x;
                wd4.a((Object) imageView3, "it.ivNoOneCheck");
                imageView3.setVisibility(0);
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.W.c().g().a(new lx2(this)).a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
            k42 k42 = this.p;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) notificationCallsAndMessagesActivity, (lc.b) k42).a(ex2.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                this.q = (ex2) a2;
                hx2 hx2 = this.o;
                if (hx2 != null) {
                    ex2 ex2 = this.q;
                    if (ex2 != null) {
                        hx2.a(ex2);
                    } else {
                        wd4.d("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        he2 he2 = (he2) ra.a(layoutInflater, R.layout.fragment_notification_settings_type, viewGroup, false, this.m);
        he2.q.setOnClickListener(new b(this));
        he2.r.setOnClickListener(new c(this));
        he2.s.setOnClickListener(new d(this));
        he2.u.setOnClickListener(new e(this));
        this.n = new ur3<>(this, he2);
        wd4.a((Object) he2, "binding");
        return he2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        hx2 hx2 = this.o;
        if (hx2 != null) {
            hx2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        hx2 hx2 = this.o;
        if (hx2 != null) {
            hx2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(hx2 hx2) {
        wd4.b(hx2, "presenter");
        this.o = hx2;
    }
}
