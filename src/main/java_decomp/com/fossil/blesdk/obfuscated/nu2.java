package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nu2 implements Factory<ConnectedAppsPresenter> {
    @DexIgnore
    public static ConnectedAppsPresenter a(ku2 ku2, UserRepository userRepository, PortfolioApp portfolioApp, al2 al2) {
        return new ConnectedAppsPresenter(ku2, userRepository, portfolioApp, al2);
    }
}
