package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ca0 implements w90 {
    @DexIgnore
    public JSONObject a(File file) {
        wd4.b(file, "logFile");
        JSONObject jSONObject = new JSONObject();
        JSONArray b = b(file);
        if (b.length() > 0) {
            jSONObject.put("data", b);
        }
        return jSONObject;
    }

    @DexIgnore
    public final JSONArray b(File file) {
        JSONArray jSONArray = new JSONArray();
        String a = hb0.a.a(file);
        if (a != null) {
            List a2 = StringsKt__StringsKt.a((CharSequence) a, new String[]{va0.y.w()}, false, 0, 6, (Object) null);
            ArrayList<String> arrayList = new ArrayList<>();
            for (Object next : a2) {
                if (!cg4.a((String) next)) {
                    arrayList.add(next);
                }
            }
            for (String a3 : arrayList) {
                String a4 = lb0.c.a(a3);
                if (a4 != null) {
                    try {
                        jSONArray.put(new JSONObject(a4));
                    } catch (Exception e) {
                        ea0.l.a(e);
                        cb4 cb4 = cb4.a;
                    }
                }
            }
        }
        return jSONArray;
    }
}
