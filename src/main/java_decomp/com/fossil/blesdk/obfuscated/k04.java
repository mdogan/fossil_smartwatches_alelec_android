package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k04 {
    @DexIgnore
    public static z14 a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static u14 m; // = f24.b();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static int a(Context context, boolean z, l04 l04) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = z && currentTimeMillis - e >= ((long) i04.m());
        e = currentTimeMillis;
        if (f == 0) {
            f = f24.c();
        }
        if (currentTimeMillis >= f) {
            f = f24.c();
            if (h14.b(context).a(context).d() != 1) {
                h14.b(context).a(context).a(1);
            }
            i04.b(0);
            p = 0;
            g = f24.a(0);
            z2 = true;
        }
        String str = g;
        if (f24.a(l04)) {
            str = l04.a() + g;
        }
        if (!l.containsKey(str)) {
            z2 = true;
        }
        if (z2) {
            if (f24.a(l04)) {
                a(context, l04);
            } else if (i04.c() < i04.f()) {
                f24.A(context);
                a(context, (l04) null);
            } else {
                m.c("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            h(context);
            o = false;
        }
        return h;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        return;
     */
    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (k04.class) {
            if (context != null) {
                if (a == null) {
                    if (b(context)) {
                        Context applicationContext = context.getApplicationContext();
                        r = applicationContext;
                        a = new z14();
                        g = f24.a(0);
                        d = System.currentTimeMillis() + i04.x;
                        a.a(new u24(applicationContext));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(Context context, int i2) {
        u14 u14;
        String str;
        if (i04.s()) {
            if (i04.q()) {
                u14 u142 = m;
                u142.e("commitEvents, maxNumber=" + i2);
            }
            Context g2 = g(context);
            if (g2 == null) {
                u14 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                u14 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (u04.a(r).f() && c(g2) != null) {
                a.a(new v04(g2, i2));
                return;
            } else {
                return;
            }
            u14.d(str);
        }
    }

    @DexIgnore
    public static void a(Context context, l04 l04) {
        if (c(context) != null) {
            if (i04.q()) {
                m.a((Object) "start new session.");
            }
            if (l04 == null || h == 0) {
                h = f24.a();
            }
            i04.a(0);
            i04.b();
            new d14(new t04(context, h, b(), l04)).a();
        }
    }

    @DexIgnore
    public static void a(Context context, String str, l04 l04) {
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new z24(str2, g2, l04));
            }
        }
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties, l04 l04) {
        u14 u14;
        String str2;
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                u14 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (a(str)) {
                u14 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                n04 n04 = new n04(str, (String[]) null, properties);
                if (c(g2) != null) {
                    a.a(new y24(g2, l04, n04));
                    return;
                }
                return;
            }
            u14.d(str2);
        }
    }

    @DexIgnore
    public static void a(Context context, Throwable th) {
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (c(g2) != null) {
                a.a(new w24(g2, th));
            }
        }
    }

    @DexIgnore
    public static boolean a() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2, l04 l04) {
        try {
            if (!i04.s()) {
                m.d("MTA StatService is disable.");
                return false;
            }
            if (i04.q()) {
                m.a((Object) "MTA SDK version, current: " + "2.0.3" + " ,required: " + str2);
            }
            if (context != null) {
                if (str2 != null) {
                    if (f24.b("2.0.3") < f24.b(str2)) {
                        m.d(("MTA SDK version conflicted, current: " + "2.0.3" + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                        i04.b(false);
                        return false;
                    }
                    String d2 = i04.d(context);
                    if (d2 == null || d2.length() == 0) {
                        i04.b(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    }
                    if (str != null) {
                        i04.b(context, str);
                    }
                    if (c(context) == null) {
                        return true;
                    }
                    a.a(new a14(context, l04));
                    return true;
                }
            }
            m.d("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
            i04.b(false);
            return false;
        } catch (Throwable th) {
            m.a(th);
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (i04.c.d != 0) {
                jSONObject2.put("v", i04.c.d);
            }
            jSONObject.put(Integer.toString(i04.c.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (i04.b.d != 0) {
                jSONObject3.put("v", i04.b.d);
            }
            jSONObject.put(Integer.toString(i04.b.a), jSONObject3);
        } catch (JSONException e2) {
            m.a((Throwable) e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static void b(Context context, l04 l04) {
        if (i04.s() && c(context) != null) {
            a.a(new v24(context, l04));
        }
    }

    @DexIgnore
    public static void b(Context context, String str, l04 l04) {
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new y04(g2, str2, l04));
            }
        }
    }

    @DexIgnore
    public static boolean b(Context context) {
        boolean z;
        long a2 = j24.a(context, i04.n, 0);
        long b2 = f24.b("2.0.3");
        boolean z2 = false;
        if (b2 <= a2) {
            u14 u14 = m;
            u14.d("MTA is disable for current version:" + b2 + ",wakeup version:" + a2);
            z = false;
        } else {
            z = true;
        }
        long a3 = j24.a(context, i04.o, 0);
        if (a3 > System.currentTimeMillis()) {
            u14 u142 = m;
            u142.d("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + a3);
        } else {
            z2 = z;
        }
        i04.b(z2);
        return z2;
    }

    @DexIgnore
    public static z14 c(Context context) {
        if (a == null) {
            synchronized (k04.class) {
                if (a == null) {
                    try {
                        a(context);
                    } catch (Throwable th) {
                        m.b(th);
                        i04.b(false);
                    }
                }
            }
        }
        return a;
    }

    @DexIgnore
    public static void c() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void c(Context context, l04 l04) {
        if (i04.s() && c(context) != null) {
            a.a(new z04(context, l04));
        }
    }

    @DexIgnore
    public static Properties d(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static void d() {
        p++;
        q = System.currentTimeMillis();
        f(r);
    }

    @DexIgnore
    public static void d(Context context) {
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                r24.b(g2).a((p04) new q04(g2), (q24) new x24());
            } catch (Throwable th) {
                m.a(th);
            }
        }
    }

    @DexIgnore
    public static void e(Context context) {
        s = System.currentTimeMillis() + ((long) (i04.l() * 60000));
        j24.b(context, "last_period_ts", s);
        a(context, -1);
    }

    @DexIgnore
    public static void f(Context context) {
        if (i04.s() && i04.J > 0) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else {
                h14.b(g2).b();
            }
        }
    }

    @DexIgnore
    public static Context g(Context context) {
        return context != null ? context : r;
    }

    @DexIgnore
    public static void h(Context context) {
        if (i04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else if (c(g2) != null) {
                a.a(new w04(g2));
            }
        }
    }
}
