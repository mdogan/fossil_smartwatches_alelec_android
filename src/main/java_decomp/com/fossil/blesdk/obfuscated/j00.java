package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.adapter.BluetoothLeAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class j00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[BluetoothLeAdapter.AdvertisingDataType.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[BluetoothLeAdapter.State.values().length];

    /*
    static {
        a[BluetoothLeAdapter.AdvertisingDataType.INCOMPLETE_16_BIT_SERVICE_CLASS_UUID.ordinal()] = 1;
        a[BluetoothLeAdapter.AdvertisingDataType.COMPLETE_16_BIT_SERVICE_CLASS_UUID.ordinal()] = 2;
        a[BluetoothLeAdapter.AdvertisingDataType.INCOMPLETE_32_BIT_SERVICE_CLASS_UUID.ordinal()] = 3;
        a[BluetoothLeAdapter.AdvertisingDataType.COMPLETE_32_BIT_SERVICE_CLASS_UUID.ordinal()] = 4;
        a[BluetoothLeAdapter.AdvertisingDataType.INCOMPLETE_128_BIT_SERVICE_CLASS_UUID.ordinal()] = 5;
        a[BluetoothLeAdapter.AdvertisingDataType.COMPLETE_128_BIT_SERVICE_CLASS_UUID.ordinal()] = 6;
        b[BluetoothLeAdapter.State.ENABLED.ordinal()] = 1;
        b[BluetoothLeAdapter.State.DISABLED.ordinal()] = 2;
        b[BluetoothLeAdapter.State.ENABLING.ordinal()] = 3;
        b[BluetoothLeAdapter.State.DISABLING.ordinal()] = 4;
    }
    */
}
