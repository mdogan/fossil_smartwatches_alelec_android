package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h94 {
    @DexIgnore
    public static /* final */ c94 a; // = g94.b(new a());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Callable<c94> {
        @DexIgnore
        public c94 call() throws Exception {
            return b.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ c94 a; // = new i94(new Handler(Looper.getMainLooper()));
    }

    @DexIgnore
    public static c94 a() {
        return g94.a(a);
    }
}
