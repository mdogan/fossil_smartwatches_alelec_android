package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z90 extends ba0 {
    @DexIgnore
    public static long k; // = 100;
    @DexIgnore
    public static /* final */ z90 l; // = new z90();

    @DexIgnore
    public z90() {
        super("raw_hardware_log", 204800, "raw_hardware_log", "raw_hardware_log", new oa0("", "", ""), 1800, new ca0(), SharedPreferenceFileName.HARDWARE_LOG_REFERENCE, false);
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
