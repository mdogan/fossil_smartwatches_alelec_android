package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j22 implements n22 {
    @DexIgnore
    public static void b(o22 o22, StringBuilder sb) {
        o22.a(a((CharSequence) sb, 0));
        sb.delete(0, 3);
    }

    @DexIgnore
    public int a() {
        return 1;
    }

    @DexIgnore
    public void a(o22 o22) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!o22.i()) {
                break;
            }
            char c = o22.c();
            o22.f++;
            int a = a(c, sb);
            int a2 = o22.a() + ((sb.length() / 3) << 1);
            o22.c(a2);
            int a3 = o22.g().a() - a2;
            if (!o22.i()) {
                StringBuilder sb2 = new StringBuilder();
                if (sb.length() % 3 == 2 && (a3 < 2 || a3 > 2)) {
                    a = a(o22, sb, sb2, a);
                }
                while (sb.length() % 3 == 1 && ((a <= 3 && a3 != 1) || a > 3)) {
                    a = a(o22, sb, sb2, a);
                }
            } else if (sb.length() % 3 == 0) {
                int a4 = q22.a(o22.d(), o22.f, a());
                if (a4 != a()) {
                    o22.b(a4);
                    break;
                }
            }
        }
        a(o22, sb);
    }

    @DexIgnore
    public final int a(o22 o22, StringBuilder sb, StringBuilder sb2, int i) {
        int length = sb.length();
        sb.delete(length - i, length);
        o22.f--;
        int a = a(o22.c(), sb2);
        o22.k();
        return a;
    }

    @DexIgnore
    public void a(o22 o22, StringBuilder sb) {
        int length = sb.length() % 3;
        int a = o22.a() + ((sb.length() / 3) << 1);
        o22.c(a);
        int a2 = o22.g().a() - a;
        if (length == 2) {
            sb.append(0);
            while (sb.length() >= 3) {
                b(o22, sb);
            }
            if (o22.i()) {
                o22.a(254);
            }
        } else if (a2 == 1 && length == 1) {
            while (sb.length() >= 3) {
                b(o22, sb);
            }
            if (o22.i()) {
                o22.a(254);
            }
            o22.f--;
        } else if (length == 0) {
            while (sb.length() >= 3) {
                b(o22, sb);
            }
            if (a2 > 0 || o22.i()) {
                o22.a(254);
            }
        } else {
            throw new IllegalStateException("Unexpected case. Please report!");
        }
        o22.b(0);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == ' ') {
            sb.append(3);
            return 1;
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
            return 1;
        } else if (c >= 'A' && c <= 'Z') {
            sb.append((char) ((c - 'A') + 14));
            return 1;
        } else if (c >= 0 && c <= 31) {
            sb.append(0);
            sb.append(c);
            return 2;
        } else if (c >= '!' && c <= '/') {
            sb.append(1);
            sb.append((char) (c - '!'));
            return 2;
        } else if (c >= ':' && c <= '@') {
            sb.append(1);
            sb.append((char) ((c - ':') + 15));
            return 2;
        } else if (c >= '[' && c <= '_') {
            sb.append(1);
            sb.append((char) ((c - '[') + 22));
            return 2;
        } else if (c >= '`' && c <= 127) {
            sb.append(2);
            sb.append((char) (c - '`'));
            return 2;
        } else if (c >= 128) {
            sb.append("\u0001\u001e");
            return a((char) (c - 128), sb) + 2;
        } else {
            throw new IllegalArgumentException("Illegal character: " + c);
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int charAt = (charSequence.charAt(i) * 1600) + (charSequence.charAt(i + 1) * '(') + charSequence.charAt(i + 2) + 1;
        return new String(new char[]{(char) (charAt / 256), (char) (charAt % 256)});
    }
}
