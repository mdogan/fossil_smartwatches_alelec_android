package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ei4 extends gh4 {
    @DexIgnore
    public long e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public wj4<yh4<?>> g;

    @DexIgnore
    public static /* synthetic */ void b(ei4 ei4, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            ei4.c(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
    }

    @DexIgnore
    public long C() {
        wj4<yh4<?>> wj4 = this.g;
        if (wj4 == null || wj4.b()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return 0;
    }

    @DexIgnore
    public final boolean D() {
        return this.e >= b(true);
    }

    @DexIgnore
    public final boolean E() {
        wj4<yh4<?>> wj4 = this.g;
        if (wj4 != null) {
            return wj4.b();
        }
        return true;
    }

    @DexIgnore
    public long F() {
        if (!G()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return C();
    }

    @DexIgnore
    public final boolean G() {
        wj4<yh4<?>> wj4 = this.g;
        if (wj4 != null) {
            yh4 c = wj4.c();
            if (c != null) {
                c.run();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean H() {
        return false;
    }

    @DexIgnore
    public final void a(yh4<?> yh4) {
        wd4.b(yh4, "task");
        wj4<yh4<?>> wj4 = this.g;
        if (wj4 == null) {
            wj4 = new wj4<>();
            this.g = wj4;
        }
        wj4.a(yh4);
    }

    @DexIgnore
    public final long b(boolean z) {
        return z ? 4294967296L : 1;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.e += b(z);
        if (!z) {
            this.f = true;
        }
    }

    @DexIgnore
    public void shutdown() {
    }

    @DexIgnore
    public static /* synthetic */ void a(ei4 ei4, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            ei4.a(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e -= b(z);
        if (this.e <= 0) {
            if (oh4.a()) {
                if (!(this.e == 0)) {
                    throw new AssertionError();
                }
            }
            if (this.f) {
                shutdown();
            }
        }
    }
}
