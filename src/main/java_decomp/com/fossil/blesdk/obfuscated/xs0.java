package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xs0 extends ContentObserver {
    @DexIgnore
    public /* final */ /* synthetic */ ws0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xs0(ws0 ws0, Handler handler) {
        super((Handler) null);
        this.a = ws0;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.a.b();
        this.a.d();
    }
}
