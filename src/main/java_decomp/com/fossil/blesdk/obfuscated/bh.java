package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bh {
    @DexIgnore
    public static ah a(View view, ViewGroup viewGroup, Matrix matrix) {
        if (Build.VERSION.SDK_INT >= 21) {
            return zg.a(view, viewGroup, matrix);
        }
        return yg.a(view, viewGroup);
    }

    @DexIgnore
    public static void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            zg.a(view);
        } else {
            yg.b(view);
        }
    }
}
