package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ln2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<BaseActivity> a;

        @DexIgnore
        public b(WeakReference<BaseActivity> weakReference) {
            wd4.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<BaseActivity> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, vd0 vd0) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = vr2.class.getSimpleName();
        wd4.a((Object) simpleName, "LoginWeiboUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public vr2(ln2 ln2) {
        wd4.b(ln2, "mLoginWeiboManager");
        this.d = ln2;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements mn2 {
        @DexIgnore
        public /* final */ /* synthetic */ vr2 a;

        @DexIgnore
        public e(vr2 vr2) {
            this.a = vr2;
        }

        @DexIgnore
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vr2.f.a();
            local.d(a2, "Inside .onLoginSuccess with auth=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        public void a(int i, vd0 vd0, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vr2.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + vd0);
            this.a.a(new c(i, vd0));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003d A[Catch:{ Exception -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042 A[Catch:{ Exception -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0045 A[Catch:{ Exception -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061 A[SYNTHETIC, Splitter:B:18:0x0061] */
    public Object a(b bVar, kc4<Object> kc4) {
        String str;
        WeakReference<BaseActivity> a2;
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            ln2 ln2 = this.d;
            Access a3 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
            if (a3 != null) {
                str = a3.getF();
                if (str != null) {
                    ln2.a(str, g62.x.t(), g62.x.u());
                    ln2 ln22 = this.d;
                    a2 = bVar == null ? bVar.a() : null;
                    if (a2 == null) {
                        Object obj = a2.get();
                        if (obj != null) {
                            wd4.a(obj, "requestValues?.activityContext!!.get()!!");
                            ln22.a((Activity) obj, new e(this));
                            return cb4.a;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
            }
            str = "";
            ln2.a(str, g62.x.t(), g62.x.u());
            ln2 ln222 = this.d;
            if (bVar == null) {
            }
            if (a2 == null) {
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "Inside .run failed with exception=" + e2);
            return new c(600, (vd0) null);
        }
    }
}
