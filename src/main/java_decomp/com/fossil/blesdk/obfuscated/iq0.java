package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iq0 extends kk0 implements ne0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<iq0> CREATOR; // = new jq0();
    @DexIgnore
    public /* final */ List<ap0> e;
    @DexIgnore
    public /* final */ Status f;

    @DexIgnore
    public iq0(List<ap0> list, Status status) {
        this.e = Collections.unmodifiableList(list);
        this.f = status;
    }

    @DexIgnore
    public Status G() {
        return this.f;
    }

    @DexIgnore
    public List<ap0> H() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof iq0) {
                iq0 iq0 = (iq0) obj;
                if (this.f.equals(iq0.f) && ak0.a(this.e, iq0.e)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(this.f, this.e);
    }

    @DexIgnore
    public String toString() {
        ak0.a a = ak0.a((Object) this);
        a.a("status", this.f);
        a.a("dataSources", this.e);
        return a.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.b(parcel, 1, H(), false);
        lk0.a(parcel, 2, (Parcelable) G(), i, false);
        lk0.a(parcel, a);
    }
}
