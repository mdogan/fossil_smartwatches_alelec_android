package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ct0 implements ht0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public ct0(String str, boolean z) {
        this.a = str;
    }

    @DexIgnore
    public final Object a() {
        return Boolean.valueOf(xy0.a(zs0.h.getContentResolver(), this.a, this.b));
    }
}
