package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fl4 implements Runnable {
    @DexIgnore
    public long e;
    @DexIgnore
    public gl4 f;

    @DexIgnore
    public fl4(long j, gl4 gl4) {
        wd4.b(gl4, "taskContext");
        this.e = j;
        this.f = gl4;
    }

    @DexIgnore
    public final TaskMode a() {
        return this.f.B();
    }

    @DexIgnore
    public fl4() {
        this(0, el4.f);
    }
}
