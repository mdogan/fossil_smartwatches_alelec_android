package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z51 extends wb1<z51> {
    @DexIgnore
    public Long c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Integer e; // = null;
    @DexIgnore
    public a61[] f; // = a61.e();
    @DexIgnore
    public y51[] g; // = y51.e();
    @DexIgnore
    public s51[] h; // = s51.e();
    @DexIgnore
    public String i; // = null;
    @DexIgnore
    public Boolean j; // = null;

    @DexIgnore
    public z51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Long l = this.c;
        if (l != null) {
            vb1.b(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            vb1.a(2, str);
        }
        Integer num = this.e;
        if (num != null) {
            vb1.b(3, num.intValue());
        }
        a61[] a61Arr = this.f;
        int i2 = 0;
        if (a61Arr != null && a61Arr.length > 0) {
            int i3 = 0;
            while (true) {
                a61[] a61Arr2 = this.f;
                if (i3 >= a61Arr2.length) {
                    break;
                }
                a61 a61 = a61Arr2[i3];
                if (a61 != null) {
                    vb1.a(4, (bc1) a61);
                }
                i3++;
            }
        }
        y51[] y51Arr = this.g;
        if (y51Arr != null && y51Arr.length > 0) {
            int i4 = 0;
            while (true) {
                y51[] y51Arr2 = this.g;
                if (i4 >= y51Arr2.length) {
                    break;
                }
                y51 y51 = y51Arr2[i4];
                if (y51 != null) {
                    vb1.a(5, (bc1) y51);
                }
                i4++;
            }
        }
        s51[] s51Arr = this.h;
        if (s51Arr != null && s51Arr.length > 0) {
            while (true) {
                s51[] s51Arr2 = this.h;
                if (i2 >= s51Arr2.length) {
                    break;
                }
                s51 s51 = s51Arr2[i2];
                if (s51 != null) {
                    vb1.a(6, (bc1) s51);
                }
                i2++;
            }
        }
        String str2 = this.i;
        if (str2 != null) {
            vb1.a(7, str2);
        }
        Boolean bool = this.j;
        if (bool != null) {
            vb1.a(8, bool.booleanValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z51)) {
            return false;
        }
        z51 z51 = (z51) obj;
        Long l = this.c;
        if (l == null) {
            if (z51.c != null) {
                return false;
            }
        } else if (!l.equals(z51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (z51.d != null) {
                return false;
            }
        } else if (!str.equals(z51.d)) {
            return false;
        }
        Integer num = this.e;
        if (num == null) {
            if (z51.e != null) {
                return false;
            }
        } else if (!num.equals(z51.e)) {
            return false;
        }
        if (!ac1.a((Object[]) this.f, (Object[]) z51.f) || !ac1.a((Object[]) this.g, (Object[]) z51.g) || !ac1.a((Object[]) this.h, (Object[]) z51.h)) {
            return false;
        }
        String str2 = this.i;
        if (str2 == null) {
            if (z51.i != null) {
                return false;
            }
        } else if (!str2.equals(z51.i)) {
            return false;
        }
        Boolean bool = this.j;
        if (bool == null) {
            if (z51.j != null) {
                return false;
            }
        } else if (!bool.equals(z51.j)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(z51.b);
        }
        yb1 yb12 = z51.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (z51.class.getName().hashCode() + 527) * 31;
        Long l = this.c;
        int i2 = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        Integer num = this.e;
        int hashCode4 = (((((((hashCode3 + (num == null ? 0 : num.hashCode())) * 31) + ac1.a((Object[]) this.f)) * 31) + ac1.a((Object[]) this.g)) * 31) + ac1.a((Object[]) this.h)) * 31;
        String str2 = this.i;
        int hashCode5 = (hashCode4 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Boolean bool = this.j;
        int hashCode6 = (hashCode5 + (bool == null ? 0 : bool.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i2 = this.b.hashCode();
        }
        return hashCode6 + i2;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Long l = this.c;
        if (l != null) {
            a += vb1.c(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            a += vb1.b(2, str);
        }
        Integer num = this.e;
        if (num != null) {
            a += vb1.c(3, num.intValue());
        }
        a61[] a61Arr = this.f;
        int i2 = 0;
        if (a61Arr != null && a61Arr.length > 0) {
            int i3 = a;
            int i4 = 0;
            while (true) {
                a61[] a61Arr2 = this.f;
                if (i4 >= a61Arr2.length) {
                    break;
                }
                a61 a61 = a61Arr2[i4];
                if (a61 != null) {
                    i3 += vb1.b(4, (bc1) a61);
                }
                i4++;
            }
            a = i3;
        }
        y51[] y51Arr = this.g;
        if (y51Arr != null && y51Arr.length > 0) {
            int i5 = a;
            int i6 = 0;
            while (true) {
                y51[] y51Arr2 = this.g;
                if (i6 >= y51Arr2.length) {
                    break;
                }
                y51 y51 = y51Arr2[i6];
                if (y51 != null) {
                    i5 += vb1.b(5, (bc1) y51);
                }
                i6++;
            }
            a = i5;
        }
        s51[] s51Arr = this.h;
        if (s51Arr != null && s51Arr.length > 0) {
            while (true) {
                s51[] s51Arr2 = this.h;
                if (i2 >= s51Arr2.length) {
                    break;
                }
                s51 s51 = s51Arr2[i2];
                if (s51 != null) {
                    a += vb1.b(6, (bc1) s51);
                }
                i2++;
            }
        }
        String str2 = this.i;
        if (str2 != null) {
            a += vb1.b(7, str2);
        }
        Boolean bool = this.j;
        if (bool == null) {
            return a;
        }
        bool.booleanValue();
        return a + vb1.c(8) + 1;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Long.valueOf(ub1.f());
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (c2 == 24) {
                this.e = Integer.valueOf(ub1.e());
            } else if (c2 == 34) {
                int a = ec1.a(ub1, 34);
                a61[] a61Arr = this.f;
                int length = a61Arr == null ? 0 : a61Arr.length;
                a61[] a61Arr2 = new a61[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.f, 0, a61Arr2, 0, length);
                }
                while (length < a61Arr2.length - 1) {
                    a61Arr2[length] = new a61();
                    ub1.a((bc1) a61Arr2[length]);
                    ub1.c();
                    length++;
                }
                a61Arr2[length] = new a61();
                ub1.a((bc1) a61Arr2[length]);
                this.f = a61Arr2;
            } else if (c2 == 42) {
                int a2 = ec1.a(ub1, 42);
                y51[] y51Arr = this.g;
                int length2 = y51Arr == null ? 0 : y51Arr.length;
                y51[] y51Arr2 = new y51[(a2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.g, 0, y51Arr2, 0, length2);
                }
                while (length2 < y51Arr2.length - 1) {
                    y51Arr2[length2] = new y51();
                    ub1.a((bc1) y51Arr2[length2]);
                    ub1.c();
                    length2++;
                }
                y51Arr2[length2] = new y51();
                ub1.a((bc1) y51Arr2[length2]);
                this.g = y51Arr2;
            } else if (c2 == 50) {
                int a3 = ec1.a(ub1, 50);
                s51[] s51Arr = this.h;
                int length3 = s51Arr == null ? 0 : s51Arr.length;
                s51[] s51Arr2 = new s51[(a3 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.h, 0, s51Arr2, 0, length3);
                }
                while (length3 < s51Arr2.length - 1) {
                    s51Arr2[length3] = new s51();
                    ub1.a((bc1) s51Arr2[length3]);
                    ub1.c();
                    length3++;
                }
                s51Arr2[length3] = new s51();
                ub1.a((bc1) s51Arr2[length3]);
                this.h = s51Arr2;
            } else if (c2 == 58) {
                this.i = ub1.b();
            } else if (c2 == 64) {
                this.j = Boolean.valueOf(ub1.d());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
