package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CirclePageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fg2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ProgressBar A;
    @DexIgnore
    public /* final */ RecyclerViewPager B;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ CirclePageIndicator y;
    @DexIgnore
    public /* final */ DashBar z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fg2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, CirclePageIndicator circlePageIndicator, ImageView imageView, DashBar dashBar, ProgressBar progressBar, RecyclerViewPager recyclerViewPager) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleButton;
        this.t = flexibleButton2;
        this.u = flexibleTextView;
        this.v = flexibleTextView3;
        this.w = flexibleTextView5;
        this.x = flexibleTextView6;
        this.y = circlePageIndicator;
        this.z = dashBar;
        this.A = progressBar;
        this.B = recyclerViewPager;
    }
}
