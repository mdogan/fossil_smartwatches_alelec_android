package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tn0 extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends az0 implements tn0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tn0$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.tn0$a$a  reason: collision with other inner class name */
        public static class C0034a extends zy0 implements tn0 {
            @DexIgnore
            public C0034a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        @DexIgnore
        public a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        @DexIgnore
        public static tn0 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof tn0) {
                return (tn0) queryLocalInterface;
            }
            return new C0034a(iBinder);
        }
    }
}
