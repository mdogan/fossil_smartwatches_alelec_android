package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a74 {
    @DexIgnore
    HttpRequest a(HttpMethod httpMethod, String str, Map<String, String> map);

    @DexIgnore
    void a(c74 c74);
}
