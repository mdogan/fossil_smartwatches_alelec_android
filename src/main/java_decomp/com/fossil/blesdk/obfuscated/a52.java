package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a52 implements Factory<GoogleApiService> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<yo2> b;
    @DexIgnore
    public /* final */ Provider<cp2> c;

    @DexIgnore
    public a52(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static a52 a(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return new a52(o42, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService b(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return a(o42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static GoogleApiService a(o42 o42, yo2 yo2, cp2 cp2) {
        GoogleApiService c2 = o42.c(yo2, cp2);
        o44.a(c2, "Cannot return null from a non-@Nullable @Provides method");
        return c2;
    }

    @DexIgnore
    public GoogleApiService get() {
        return b(this.a, this.b, this.c);
    }
}
