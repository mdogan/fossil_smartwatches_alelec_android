package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ym0 {
    @DexIgnore
    public static int a(int i) {
        if (i == -1) {
            return -1;
        }
        return i / 1000;
    }
}
