package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ie4 implements Iterable<Integer>, de4 {
    @DexIgnore
    public static /* final */ a h; // = new a((rd4) null);
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ie4 a(int i, int i2, int i3) {
            return new ie4(i, i2, i3);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public ie4(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.e = i;
            this.f = zc4.b(i, i2, i3);
            this.g = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.f;
    }

    @DexIgnore
    public final int c() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ie4) {
            if (!isEmpty() || !((ie4) obj).isEmpty()) {
                ie4 ie4 = (ie4) obj;
                if (!(this.e == ie4.e && this.f == ie4.f && this.g == ie4.g)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.e * 31) + this.f) * 31) + this.g;
    }

    @DexIgnore
    public boolean isEmpty() {
        if (this.g > 0) {
            if (this.e > this.f) {
                return true;
            }
        } else if (this.e < this.f) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public String toString() {
        int i;
        StringBuilder sb;
        if (this.g > 0) {
            sb = new StringBuilder();
            sb.append(this.e);
            sb.append("..");
            sb.append(this.f);
            sb.append(" step ");
            i = this.g;
        } else {
            sb = new StringBuilder();
            sb.append(this.e);
            sb.append(" downTo ");
            sb.append(this.f);
            sb.append(" step ");
            i = -this.g;
        }
        sb.append(i);
        return sb.toString();
    }

    @DexIgnore
    public yb4 iterator() {
        return new je4(this.e, this.f, this.g);
    }
}
