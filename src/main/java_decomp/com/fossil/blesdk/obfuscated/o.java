package com.fossil.blesdk.obfuscated;

import android.media.session.PlaybackState;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o {
    @DexIgnore
    public static Bundle a(Object obj) {
        return ((PlaybackState) obj).getExtras();
    }
}
