package com.fossil.blesdk.obfuscated;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.o13;
import com.fossil.blesdk.obfuscated.rm2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v13 extends bs2 implements u13, View.OnClickListener, CustomizeWidget.c, xs3.g, rm2.b {
    @DexIgnore
    public t13 k;
    @DexIgnore
    public ur3<jb2> l;
    @DexIgnore
    public /* final */ ArrayList<Fragment> m; // = new ArrayList<>();
    @DexIgnore
    public k23 n;
    @DexIgnore
    public s43 o;
    @DexIgnore
    public CustomizeThemeFragment p;
    @DexIgnore
    public int q; // = 1;
    @DexIgnore
    public Integer r;
    @DexIgnore
    public ValueAnimator s;
    @DexIgnore
    public ComplicationsPresenter t;
    @DexIgnore
    public WatchAppsPresenter u;
    @DexIgnore
    public CustomizeThemePresenter v;
    @DexIgnore
    public k42 w;
    @DexIgnore
    public DianaCustomizeViewModel x;
    @DexIgnore
    public HashMap y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public b(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                wd4.a((Object) activity, "it");
                int intExtra = activity.getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
                String stringExtra3 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
                wd4.a((Object) stringExtra, "presetId");
                wd4.a((Object) stringExtra2, "complicationPos");
                aVar.a(activity, stringExtra, intExtra, stringExtra2, stringExtra3);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            jb2 jb2 = (jb2) v13.a(this.a).a();
            if (jb2 != null) {
                bq2 bq2 = bq2.a;
                Object a2 = v13.a(this.a).a();
                if (a2 != null) {
                    CardView cardView = ((jb2) a2).t;
                    wd4.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    bq2.a((View) cardView);
                    ViewPropertyAnimator animate = jb2.F.animate();
                    if (animate != null) {
                        ViewPropertyAnimator duration = animate.setDuration(500);
                        if (duration != null) {
                            duration.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate2 = jb2.E.animate();
                    if (animate2 != null) {
                        ViewPropertyAnimator duration2 = animate2.setDuration(500);
                        if (duration2 != null) {
                            duration2.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate3 = jb2.D.animate();
                    if (animate3 != null) {
                        ViewPropertyAnimator duration3 = animate3.setDuration(500);
                        if (duration3 != null) {
                            duration3.alpha(1.0f);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ jb2 a;

        @DexIgnore
        public c(jb2 jb2) {
            this.a = jb2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            wd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    j5Var.a((int) R.id.guideline_complications_holder, ((Float) animatedValue2).floatValue() + ((float) 1));
                    j5Var.a(this.a.r);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ jb2 a;

        @DexIgnore
        public d(jb2 jb2) {
            this.a = jb2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            wd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    j5Var.a((int) R.id.guideline_complications_holder, ((Float) animatedValue2).floatValue() - ((float) 1));
                    j5Var.a(this.a.r);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ jb2 a;

        @DexIgnore
        public e(jb2 jb2) {
            this.a = jb2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            wd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                j5Var.a(this.a.r);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ jb2 a;

        @DexIgnore
        public f(jb2 jb2) {
            this.a = jb2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            wd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                j5Var.a(this.a.r);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public h(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public i(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_LEFT_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_LEFT_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public j(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(false, "right", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(false, "right", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(false, str, "right");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(false, "right", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "right");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public k(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(false, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(false, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(false, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(false, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "bottom");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public l(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(true, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public m(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(true, "middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(true, "middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(true, str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(true, "middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "middle");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ v13 a;

        @DexIgnore
        public n(v13 v13) {
            this.a = v13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(true, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.a(true, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.c(true, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.b(true, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "bottom");
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean S0() {
        t13 t13 = this.k;
        if (t13 != null) {
            t13.h();
            return false;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void T0() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.n = (k23) getChildFragmentManager().a("ComplicationsFragment");
        this.o = (s43) getChildFragmentManager().a("WatchAppsFragment");
        this.p = (CustomizeThemeFragment) getChildFragmentManager().a("CustomizeThemeFragment");
        if (this.n == null) {
            this.n = new k23();
        }
        if (this.o == null) {
            this.o = new s43();
        }
        if (this.p == null) {
            this.p = new CustomizeThemeFragment();
        }
        CustomizeThemeFragment customizeThemeFragment = this.p;
        if (customizeThemeFragment != null) {
            this.m.add(customizeThemeFragment);
        }
        k23 k23 = this.n;
        if (k23 != null) {
            this.m.add(k23);
        }
        s43 s43 = this.o;
        if (s43 != null) {
            this.m.add(s43);
        }
        m42 g2 = PortfolioApp.W.c().g();
        k23 k232 = this.n;
        if (k232 != null) {
            s43 s432 = this.o;
            if (s432 != null) {
                CustomizeThemeFragment customizeThemeFragment2 = this.p;
                if (customizeThemeFragment2 != null) {
                    g2.a(new k13(k232, s432, customizeThemeFragment2)).a(this);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeContract.View");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsContract.View");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsContract.View");
    }

    @DexIgnore
    public final void U(String str) {
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(true);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.setSelectedWc(true);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(true);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(true);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void U0() {
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.M;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                wd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026plicationAppPos.TOP_FACE)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_COMPLICATION", putExtra, new o13(new h(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.L;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_LEFT_KEY);
                wd4.a((Object) putExtra2, "Intent().putExtra(Custom\u2026licationAppPos.LEFT_FACE)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_COMPLICATION", putExtra2, new o13(new i(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.K;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "right");
                wd4.a((Object) putExtra3, "Intent().putExtra(Custom\u2026icationAppPos.RIGHT_FACE)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_COMPLICATION", putExtra3, new o13(new j(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget4 = a2.J;
                Intent putExtra4 = new Intent().putExtra("KEY_POSITION", "bottom");
                wd4.a((Object) putExtra4, "Intent().putExtra(Custom\u2026cationAppPos.BOTTOM_FACE)");
                CustomizeWidget.a(customizeWidget4, "SWAP_PRESET_COMPLICATION", putExtra4, new o13(new k(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget5 = a2.I;
                Intent putExtra5 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                wd4.a((Object) putExtra5, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget5, "SWAP_PRESET_WATCH_APP", putExtra5, new o13(new l(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget6 = a2.H;
                Intent putExtra6 = new Intent().putExtra("KEY_POSITION", "middle");
                wd4.a((Object) putExtra6, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget6, "SWAP_PRESET_WATCH_APP", putExtra6, new o13(new m(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget7 = a2.G;
                Intent putExtra7 = new Intent().putExtra("KEY_POSITION", "bottom");
                wd4.a((Object) putExtra7, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget7, "SWAP_PRESET_WATCH_APP", putExtra7, new o13(new n(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V(String str) {
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.setSelectedWc(true);
                            a2.H.setSelectedWc(false);
                            a2.G.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.I.setSelectedWc(false);
                        a2.H.setSelectedWc(true);
                        a2.G.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.I.setSelectedWc(false);
                    a2.H.setSelectedWc(false);
                    a2.G.setSelectedWc(true);
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    public void c(View view) {
    }

    @DexIgnore
    public void d(View view) {
    }

    @DexIgnore
    public void f(int i2) {
        int i3 = i2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "initTab - initTab=" + i3 + " - mPreTab=" + this.r);
        this.q = i3;
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                j5 j5Var = new j5();
                j5Var.c(a2.r);
                if (i3 == 0) {
                    Integer num = this.r;
                    if (num != null && num.intValue() == 1) {
                        t13 t13 = this.k;
                        if (t13 != null) {
                            t13.i();
                            a2.w.setImageBitmap(bl2.a((View) a2.q));
                            this.s = ValueAnimator.ofFloat(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
                            ValueAnimator valueAnimator = this.s;
                            if (valueAnimator != null) {
                                valueAnimator.addUpdateListener(new c(a2));
                            }
                            ValueAnimator valueAnimator2 = this.s;
                            if (valueAnimator2 != null) {
                                valueAnimator2.start();
                            }
                            this.r = null;
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    } else {
                        j5Var.a((int) R.id.guideline, 1.0f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout = a2.s;
                    wd4.a((Object) constraintLayout, "it.clWatchApps");
                    constraintLayout.setVisibility(4);
                    View view = a2.y;
                    wd4.a((Object) view, "it.lineCenter");
                    view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view2 = a2.x;
                    wd4.a((Object) view2, "it.lineBottom");
                    view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view3 = a2.z;
                    wd4.a((Object) view3, "it.lineTop");
                    view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (i3 == 1) {
                    Integer num2 = this.r;
                    if (num2 != null && num2.intValue() == 0) {
                        a2.w.setImageBitmap(bl2.a((View) a2.q));
                        this.s = ValueAnimator.ofFloat(new float[]{2.0f, 1.0f});
                        ValueAnimator valueAnimator3 = this.s;
                        if (valueAnimator3 != null) {
                            valueAnimator3.addUpdateListener(new d(a2));
                        }
                        ValueAnimator valueAnimator4 = this.s;
                        if (valueAnimator4 != null) {
                            valueAnimator4.start();
                        }
                        this.r = null;
                    } else if (num2 != null && num2.intValue() == 2) {
                        this.s = ValueAnimator.ofFloat(new float[]{0.25f, 1.0f});
                        ValueAnimator valueAnimator5 = this.s;
                        if (valueAnimator5 != null) {
                            valueAnimator5.addUpdateListener(new e(a2));
                        }
                        ValueAnimator valueAnimator6 = this.s;
                        if (valueAnimator6 != null) {
                            valueAnimator6.start();
                        }
                        this.r = null;
                    } else {
                        j5Var.a((int) R.id.guideline, 1.0f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout2 = a2.s;
                    wd4.a((Object) constraintLayout2, "it.clWatchApps");
                    constraintLayout2.setVisibility(4);
                    View view4 = a2.y;
                    wd4.a((Object) view4, "it.lineCenter");
                    view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view5 = a2.x;
                    wd4.a((Object) view5, "it.lineBottom");
                    view5.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view6 = a2.z;
                    wd4.a((Object) view6, "it.lineTop");
                    view6.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        String a3 = dianaCustomizeViewModel.g().a();
                        if (a3 != null) {
                            wd4.a((Object) a3, "pos");
                            U(a3);
                        }
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else if (i3 == 2) {
                    Integer num3 = this.r;
                    if (num3 != null && num3.intValue() == 1) {
                        j5Var.a((int) R.id.guideline_complications_holder, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        j5Var.a(a2.r);
                        this.s = ValueAnimator.ofFloat(new float[]{1.0f, 0.25f});
                        ValueAnimator valueAnimator7 = this.s;
                        if (valueAnimator7 != null) {
                            valueAnimator7.addUpdateListener(new f(a2));
                        }
                        ValueAnimator valueAnimator8 = this.s;
                        if (valueAnimator8 != null) {
                            valueAnimator8.start();
                        }
                        this.r = null;
                    } else {
                        j5Var.a((int) R.id.guideline, 0.25f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout3 = a2.s;
                    wd4.a((Object) constraintLayout3, "it.clWatchApps");
                    constraintLayout3.setVisibility(0);
                    View view7 = a2.y;
                    wd4.a((Object) view7, "it.lineCenter");
                    view7.setAlpha(1.0f);
                    View view8 = a2.x;
                    wd4.a((Object) view8, "it.lineBottom");
                    view8.setAlpha(1.0f);
                    View view9 = a2.z;
                    wd4.a((Object) view9, "it.lineTop");
                    view9.setAlpha(1.0f);
                    DianaCustomizeViewModel dianaCustomizeViewModel2 = this.x;
                    if (dianaCustomizeViewModel2 != null) {
                        String a4 = dianaCustomizeViewModel2.j().a();
                        if (a4 != null) {
                            wd4.a((Object) a4, "pos");
                            V(a4);
                        }
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                }
                p(i2);
                t13 t132 = this.k;
                if (t132 != null) {
                    t132.a(i3);
                    a2.A.i(i3);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g(boolean z) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (z) {
                activity.setResult(-1);
            } else {
                activity.setResult(0);
            }
            activity.finishAfterTransition();
        }
    }

    @DexIgnore
    public void l() {
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        S(a2);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            k42 k42 = this.w;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) dianaCustomizeEditActivity, (lc.b) k42).a(DianaCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.x = (DianaCustomizeViewModel) a2;
                t13 t13 = this.k;
                if (t13 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        t13.a(dianaCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {
                case R.id.ftv_set_to_watch /*2131362293*/:
                    t13 t13 = this.k;
                    if (t13 != null) {
                        t13.j();
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.tv_cancel /*2131362840*/:
                    t13 t132 = this.k;
                    if (t132 != null) {
                        t132.h();
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_bottom /*2131363029*/:
                    t13 t133 = this.k;
                    if (t133 != null) {
                        t133.b("bottom");
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_middle /*2131363030*/:
                    t13 t134 = this.k;
                    if (t134 != null) {
                        t134.b("middle");
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_top /*2131363031*/:
                    t13 t135 = this.k;
                    if (t135 != null) {
                        t135.b(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_bottom /*2131363032*/:
                    t13 t136 = this.k;
                    if (t136 != null) {
                        t136.a("bottom");
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_end /*2131363036*/:
                    t13 t137 = this.k;
                    if (t137 != null) {
                        t137.a("right");
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_start /*2131363039*/:
                    t13 t138 = this.k;
                    if (t138 != null) {
                        t138.a(ViewHierarchy.DIMENSION_LEFT_KEY);
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_top /*2131363040*/:
                    t13 t139 = this.k;
                    if (t139 != null) {
                        t139.a(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        jb2 jb2 = (jb2) ra.a(layoutInflater, R.layout.fragment_diana_customize_edit, viewGroup, false, O0());
        T0();
        this.l = new ur3<>(this, jb2);
        wd4.a((Object) jb2, "binding");
        return jb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        t13 t13 = this.k;
        if (t13 != null) {
            t13.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        t13 t13 = this.k;
        if (t13 != null) {
            t13.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            wd4.a((Object) window, "activity.window");
            window.setEnterTransition(bq2.a.a());
            Window window2 = activity.getWindow();
            wd4.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(bq2.a.a(PortfolioApp.W.c()));
            Intent intent = activity.getIntent();
            wd4.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new aq2(intent, PortfolioApp.W.c()));
            a(activity);
            postponeEnterTransition();
        }
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                a2.M.a(this);
                a2.J.a(this);
                a2.L.a(this);
                a2.K.a(this);
                a2.M.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.L.setOnClickListener(this);
                a2.K.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.H.setOnClickListener(this);
                a2.G.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.u.setOnClickListener(this);
                RecyclerViewPager recyclerViewPager = a2.A;
                wd4.a((Object) recyclerViewPager, "it.rvPreset");
                recyclerViewPager.setAdapter(new du3(getChildFragmentManager(), this.m));
                a2.A.setItemViewCacheSize(2);
                a2.A.a((RecyclerView.p) new g());
                rm2 rm2 = new rm2();
                wd4.a((Object) a2, "it");
                rm2.a(a2.d(), this);
            }
            U0();
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        if (i2 == 1) {
            R("set_complication_view");
        } else if (i2 == 2) {
            R("set_watch_apps_view");
        }
    }

    @DexIgnore
    public void q() {
        xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public void r(String str) {
        wd4.b(str, "buttonsPosition");
        if (this.q == 2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
            V(str);
        }
    }

    @DexIgnore
    public final void b(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.J.e();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.e();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.e();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.K.e();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                t13 t13 = this.k;
                if (t13 != null) {
                    t13.d(str, str2);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                switch (str2.hashCode()) {
                    case -1383228885:
                        if (str2.equals("bottom")) {
                            a2.J.e();
                            break;
                        }
                        break;
                    case 115029:
                        if (str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.e();
                            break;
                        }
                        break;
                    case 3317767:
                        if (str2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.e();
                            break;
                        }
                        break;
                    case 108511772:
                        if (str2.equals("right")) {
                            a2.K.e();
                            break;
                        }
                        break;
                }
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                t13 t132 = this.k;
                if (t132 != null) {
                    t132.c(str, str2);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void p() {
        xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_Text__SaveChangesToThePresetBefore));
        fVar.a((int) R.id.tv_cancel, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Discard));
        fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public static final /* synthetic */ ur3 a(v13 v13) {
        ur3<jb2> ur3 = v13.l;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(View view, Boolean bool) {
        a(view, bool.booleanValue());
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        wd4.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public void a(View view, boolean z) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "onHorizontalFling");
    }

    @DexIgnore
    public void p(String str) {
        wd4.b(str, "complicationsPosition");
        if (this.q == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedComplication data=" + str);
            U(str);
        }
    }

    @DexIgnore
    public final boolean a(boolean z, String str, View view, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return true;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                s43 s43 = this.o;
                if (s43 != null) {
                    s43.T0();
                }
                t13 t13 = this.k;
                if (t13 != null) {
                    t13.b(str2, str);
                    return true;
                }
                wd4.d("mPresenter");
                throw null;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.J.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.L.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.K.e();
                        break;
                    }
                    break;
            }
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            k23 k23 = this.n;
            if (k23 != null) {
                k23.T0();
            }
            t13 t132 = this.k;
            if (t132 != null) {
                t132.a(str2, str);
                return true;
            }
            wd4.d("mPresenter");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void b(WatchFaceWrapper watchFaceWrapper) {
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null && watchFaceWrapper != null) {
                Drawable topComplication = watchFaceWrapper.getTopComplication();
                if (topComplication != null) {
                    a2.M.setBackgroundDrawableCus(topComplication);
                } else {
                    a2.M.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable rightComplication = watchFaceWrapper.getRightComplication();
                if (rightComplication != null) {
                    a2.K.setBackgroundDrawableCus(rightComplication);
                } else {
                    a2.K.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable bottomComplication = watchFaceWrapper.getBottomComplication();
                if (bottomComplication != null) {
                    a2.J.setBackgroundDrawableCus(bottomComplication);
                } else {
                    a2.J.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable leftComplication = watchFaceWrapper.getLeftComplication();
                if (leftComplication != null) {
                    a2.L.setBackgroundDrawableCus(leftComplication);
                } else {
                    a2.L.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                a2.M.setSelectedWc(false);
                a2.J.setSelectedWc(false);
                a2.L.setSelectedWc(false);
                a2.K.setSelectedWc(false);
                startPostponedEnterTransition();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c(boolean z, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                s43 s43 = this.o;
                if (s43 != null) {
                    s43.T0();
                    return;
                }
                return;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.J.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.L.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.K.e();
                        break;
                    }
                    break;
            }
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            k23 k23 = this.n;
            if (k23 != null) {
                k23.T0();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2, String str3, boolean z) {
        wd4.b(str, "message");
        wd4.b(str2, "id");
        wd4.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z);
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneError_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public final void a(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.d();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.J.d();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.d();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.d();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.K.d();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        if (this.q == 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedCustomizeTheme watchFaceWrapper=" + watchFaceWrapper);
            b(watchFaceWrapper);
        }
    }

    @DexIgnore
    public void c(String str) {
        wd4.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            wd4.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void a(g13 g13) {
        wd4.b(g13, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "showCurrentPreset complications=" + g13.a() + " watchApps=" + g13.e());
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(g13.a());
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(g13.e());
                TextView textView = a2.C;
                wd4.a((Object) textView, "it.tvPresetName");
                textView.setText(g13.d());
                WatchFaceWrapper f2 = g13.f();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    i13 i13 = (i13) it.next();
                    String f3 = i13.f();
                    if (f3 != null) {
                        switch (f3.hashCode()) {
                            case -1383228885:
                                if (!f3.equals("bottom")) {
                                    break;
                                } else {
                                    a2.J.b(i13.d());
                                    a2.J.setBottomContent(i13.g());
                                    a2.J.h();
                                    CustomizeWidget customizeWidget = a2.J;
                                    wd4.a((Object) customizeWidget, "it.wcBottom");
                                    a(customizeWidget, i13.d());
                                    break;
                                }
                            case 115029:
                                if (!f3.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    break;
                                } else {
                                    a2.M.b(i13.d());
                                    a2.M.setBottomContent(i13.g());
                                    a2.M.h();
                                    CustomizeWidget customizeWidget2 = a2.M;
                                    wd4.a((Object) customizeWidget2, "it.wcTop");
                                    a(customizeWidget2, i13.d());
                                    break;
                                }
                            case 3317767:
                                if (!f3.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    break;
                                } else {
                                    a2.L.b(i13.d());
                                    a2.L.setBottomContent(i13.g());
                                    a2.L.h();
                                    CustomizeWidget customizeWidget3 = a2.L;
                                    wd4.a((Object) customizeWidget3, "it.wcStart");
                                    a(customizeWidget3, i13.d());
                                    break;
                                }
                            case 108511772:
                                if (!f3.equals("right")) {
                                    break;
                                } else {
                                    a2.K.b(i13.d());
                                    a2.K.setBottomContent(i13.g());
                                    a2.K.h();
                                    CustomizeWidget customizeWidget4 = a2.K;
                                    wd4.a((Object) customizeWidget4, "it.wcEnd");
                                    a(customizeWidget4, i13.d());
                                    break;
                                }
                        }
                    }
                }
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    i13 i132 = (i13) it2.next();
                    String f4 = i132.f();
                    if (f4 != null) {
                        int hashCode = f4.hashCode();
                        if (hashCode != -1383228885) {
                            if (hashCode != -1074341483) {
                                if (hashCode == 115029 && f4.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    FlexibleTextView flexibleTextView = a2.F;
                                    wd4.a((Object) flexibleTextView, "it.tvWaTop");
                                    flexibleTextView.setText(i132.e());
                                    a2.I.d(i132.d());
                                    a2.I.h();
                                    CustomizeWidget customizeWidget5 = a2.I;
                                    wd4.a((Object) customizeWidget5, "it.waTop");
                                    b(customizeWidget5, i132.d());
                                }
                            } else if (f4.equals("middle")) {
                                FlexibleTextView flexibleTextView2 = a2.E;
                                wd4.a((Object) flexibleTextView2, "it.tvWaMiddle");
                                flexibleTextView2.setText(i132.e());
                                a2.H.d(i132.d());
                                a2.H.h();
                                CustomizeWidget customizeWidget6 = a2.H;
                                wd4.a((Object) customizeWidget6, "it.waMiddle");
                                b(customizeWidget6, i132.d());
                            }
                        } else if (f4.equals("bottom")) {
                            FlexibleTextView flexibleTextView3 = a2.D;
                            wd4.a((Object) flexibleTextView3, "it.tvWaBottom");
                            flexibleTextView3.setText(i132.e());
                            a2.G.d(i132.d());
                            a2.G.h();
                            CustomizeWidget customizeWidget7 = a2.G;
                            wd4.a((Object) customizeWidget7, "it.waBottom");
                            b(customizeWidget7, i132.d());
                        }
                    }
                }
                a2.M.g();
                a2.K.g();
                a2.J.g();
                a2.L.g();
                if (f2 != null) {
                    WatchFaceWrapper.MetaData topMetaData = f2.getTopMetaData();
                    if (topMetaData != null) {
                        a2.M.a(topMetaData.getSelectedForegroundColor(), (Integer) null, topMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData rightMetaData = f2.getRightMetaData();
                    if (rightMetaData != null) {
                        a2.K.a(rightMetaData.getSelectedForegroundColor(), (Integer) null, rightMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData bottomMetaData = f2.getBottomMetaData();
                    if (bottomMetaData != null) {
                        a2.J.a(bottomMetaData.getSelectedForegroundColor(), (Integer) null, bottomMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData leftMetaData = f2.getLeftMetaData();
                    if (leftMetaData != null) {
                        a2.L.a(leftMetaData.getSelectedForegroundColor(), (Integer) null, leftMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    Drawable background = f2.getBackground();
                    if (background != null) {
                        a2.v.setImageDrawable(background);
                    }
                }
                if (this.q == 0) {
                    b(g13.f());
                    return;
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData topMetaData2 = f2.getTopMetaData();
                    if (topMetaData2 != null) {
                        a2.M.a((Integer) null, topMetaData2.getSelectedBackgroundColor(), (Integer) null, topMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData rightMetaData2 = f2.getRightMetaData();
                    if (rightMetaData2 != null) {
                        a2.K.a((Integer) null, rightMetaData2.getSelectedBackgroundColor(), (Integer) null, rightMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData bottomMetaData2 = f2.getBottomMetaData();
                    if (bottomMetaData2 != null) {
                        a2.J.a((Integer) null, bottomMetaData2.getSelectedBackgroundColor(), (Integer) null, bottomMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData leftMetaData2 = f2.getLeftMetaData();
                    if (leftMetaData2 != null) {
                        a2.L.a((Integer) null, leftMetaData2.getSelectedBackgroundColor(), (Integer) null, leftMetaData2.getUnselectedBackgroundColor());
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(boolean z) {
        ur3<jb2> ur3 = this.l;
        if (ur3 != null) {
            jb2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.u;
                wd4.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.u;
                wd4.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.u.setBackgroundResource(R.drawable.preset_saved);
                return;
            }
            ImageButton imageButton3 = a2.u;
            wd4.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.u;
            wd4.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.u.setBackgroundResource(R.drawable.preset_unsaved);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(t13 t13) {
        wd4.b(t13, "presenter");
        this.k = t13;
    }

    @DexIgnore
    public void a(String str, String str2, String str3, boolean z) {
        wd4.b(str, "message");
        wd4.b(str2, "id");
        wd4.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z);
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_PERMISSION", bundle);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode == 291193711 && str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING") && i2 == R.id.tv_ok && intent != null) {
                    String stringExtra = intent.getStringExtra("TO_POS");
                    String stringExtra2 = intent.getStringExtra("TO_ID");
                    boolean booleanExtra = intent.getBooleanExtra("TO_COMPLICATION", true);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
                    if (this.q == 1) {
                        if (booleanExtra) {
                            t13 t13 = this.k;
                            if (t13 != null) {
                                wd4.a((Object) stringExtra, "toPos");
                                t13.a(stringExtra);
                                return;
                            }
                            wd4.d("mPresenter");
                            throw null;
                        }
                    } else if (!booleanExtra) {
                        t13 t132 = this.k;
                        if (t132 != null) {
                            wd4.a((Object) stringExtra, "toPos");
                            t132.b(stringExtra);
                            return;
                        }
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
            } else {
                if (i2 == R.id.tv_cancel) {
                    g(false);
                } else if (i2 == R.id.tv_ok) {
                    t13 t133 = this.k;
                    if (t133 != null) {
                        t133.j();
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION") && i2 == R.id.tv_ok && intent != null) {
            String stringExtra3 = intent.getStringExtra("TO_ID");
            cn2 cn2 = cn2.d;
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) stringExtra3, "complicationId");
                cn2.a(context, stringExtra3);
                return;
            }
            wd4.a();
            throw null;
        }
    }
}
