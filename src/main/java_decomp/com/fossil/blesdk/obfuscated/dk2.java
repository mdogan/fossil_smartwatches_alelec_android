package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk2 {
    @DexIgnore
    public static gk2 a(Context context) {
        return (gk2) sn.d(context);
    }

    @DexIgnore
    public static gk2 a(Fragment fragment) {
        return (gk2) sn.a(fragment);
    }

    @DexIgnore
    public static gk2 a(View view) {
        return (gk2) sn.a(view);
    }
}
