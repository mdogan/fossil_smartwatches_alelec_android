package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g51 extends b51 implements e51 {
    @DexIgnore
    public g51(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    public final tn0 zza(Bitmap bitmap) throws RemoteException {
        Parcel o = o();
        d51.a(o, (Parcelable) bitmap);
        Parcel a = a(6, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
