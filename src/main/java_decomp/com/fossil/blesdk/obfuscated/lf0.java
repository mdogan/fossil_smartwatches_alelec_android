package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ie0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lf0 implements ie0.a {
    @DexIgnore
    public /* final */ /* synthetic */ BasePendingResult a;
    @DexIgnore
    public /* final */ /* synthetic */ kf0 b;

    @DexIgnore
    public lf0(kf0 kf0, BasePendingResult basePendingResult) {
        this.b = kf0;
        this.a = basePendingResult;
    }

    @DexIgnore
    public final void a(Status status) {
        this.b.a.remove(this.a);
    }
}
