package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.blesdk.obfuscated.bp1;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kp1 extends Service implements xo1, ap1, cp1, hp1 {
    @DexIgnore
    public ComponentName e;
    @DexIgnore
    public c f;
    @DexIgnore
    public IBinder g;
    @DexIgnore
    public Intent h;
    @DexIgnore
    public Looper i;
    @DexIgnore
    public /* final */ Object j; // = new Object();
    @DexIgnore
    public boolean k;
    @DexIgnore
    public op1 l; // = new op1(new a());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bp1.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(bp1.a aVar) {
            kp1.this.a(aVar);
        }

        @DexIgnore
        public final void b(bp1.a aVar, int i, int i2) {
            kp1.this.b(aVar, i, i2);
        }

        @DexIgnore
        public final void c(bp1.a aVar, int i, int i2) {
            kp1.this.c(aVar, i, i2);
        }

        @DexIgnore
        public final void a(bp1.a aVar, int i, int i2) {
            kp1.this.a(aVar, i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b(kp1 kp1) {
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    @DexIgnore
    public Looper a() {
        if (this.i == null) {
            HandlerThread handlerThread = new HandlerThread("WearableListenerService");
            handlerThread.start();
            this.i = handlerThread.getLooper();
        }
        return this.i;
    }

    @DexIgnore
    public void a(bp1.a aVar) {
    }

    @DexIgnore
    public void a(bp1.a aVar, int i2, int i3) {
    }

    @DexIgnore
    public void a(ep1 ep1) {
    }

    @DexIgnore
    public void a(hq1 hq1) {
    }

    @DexIgnore
    public void a(ip1 ip1) {
    }

    @DexIgnore
    public void a(iq1 iq1) {
    }

    @DexIgnore
    public void a(jp1 jp1) {
    }

    @DexIgnore
    public void a(yo1 yo1) {
    }

    @DexIgnore
    public void a(zo1 zo1) {
    }

    @DexIgnore
    public void a(zo1 zo1, int i2, int i3) {
    }

    @DexIgnore
    public void a(List<jp1> list) {
    }

    @DexIgnore
    public void b(bp1.a aVar, int i2, int i3) {
    }

    @DexIgnore
    public void b(jp1 jp1) {
    }

    @DexIgnore
    public void b(zo1 zo1, int i2, int i3) {
    }

    @DexIgnore
    public void c(bp1.a aVar, int i2, int i3) {
    }

    @DexIgnore
    public void c(zo1 zo1, int i2, int i3) {
    }

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        if ("com.google.android.gms.wearable.BIND_LISTENER".equals(intent.getAction())) {
            return this.g;
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.e = new ComponentName(this, kp1.class.getName());
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 10);
            sb.append("onCreate: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        this.f = new c(a());
        this.h = new Intent("com.google.android.gms.wearable.BIND_LISTENER");
        this.h.setComponent(this.e);
        this.g = new d();
    }

    @DexIgnore
    public void onDestroy() {
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 11);
            sb.append("onDestroy: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        synchronized (this.j) {
            this.k = true;
            if (this.f != null) {
                this.f.a();
            } else {
                String valueOf2 = String.valueOf(this.e);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 111);
                sb2.append("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
        super.onDestroy();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends Handler {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ b b; // = new b();

        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void a() {
            getLooper().quit();
            a("quit");
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void b() {
            if (!this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(kp1.this.e);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
                    sb.append("bindService: ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                kp1.this.bindService(kp1.this.h, this.b, 1);
                this.a = true;
            }
        }

        @DexIgnore
        public final void dispatchMessage(Message message) {
            b();
            try {
                super.dispatchMessage(message);
            } finally {
                if (!hasMessages(0)) {
                    a("dispatch");
                }
            }
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void a(String str) {
            if (this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(kp1.this.e);
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 17 + String.valueOf(valueOf).length());
                    sb.append("unbindService: ");
                    sb.append(str);
                    sb.append(", ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                try {
                    kp1.this.unbindService(this.b);
                } catch (RuntimeException e) {
                    Log.e("WearableLS", "Exception when unbinding from local service", e);
                }
                this.a = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends xp1 {
        @DexIgnore
        public volatile int e;

        @DexIgnore
        public d() {
            this.e = -1;
        }

        @DexIgnore
        public final void a(DataHolder dataHolder) {
            kq1 kq1 = new kq1(this, dataHolder);
            try {
                String valueOf = String.valueOf(dataHolder);
                int count = dataHolder.getCount();
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append(valueOf);
                sb.append(", rows=");
                sb.append(count);
                if (a(kq1, "onDataItemChanged", sb.toString())) {
                }
            } finally {
                dataHolder.close();
            }
        }

        @DexIgnore
        public final void b(aq1 aq1) {
            a(new nq1(this, aq1), "onPeerDisconnected", aq1);
        }

        @DexIgnore
        public final void b(List<aq1> list) {
            a(new oq1(this, list), "onConnectedNodes", list);
        }

        @DexIgnore
        public final void a(yp1 yp1) {
            a(new lq1(this, yp1), "onMessageReceived", yp1);
        }

        @DexIgnore
        public final void a(aq1 aq1) {
            a(new mq1(this, aq1), "onPeerConnected", aq1);
        }

        @DexIgnore
        public final void a(lp1 lp1) {
            a(new pq1(this, lp1), "onConnectedCapabilityChanged", lp1);
        }

        @DexIgnore
        public final void a(fq1 fq1) {
            a(new qq1(this, fq1), "onNotificationReceived", fq1);
        }

        @DexIgnore
        public final void a(dq1 dq1) {
            a(new rq1(this, dq1), "onEntityUpdate", dq1);
        }

        @DexIgnore
        public final void a(pp1 pp1) {
            a(new sq1(this, pp1), "onChannelEvent", pp1);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0074 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
        public final boolean a(Runnable runnable, String str, Object obj) {
            boolean z;
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", String.format("%s: %s %s", new Object[]{str, kp1.this.e.toString(), obj}));
            }
            int callingUid = Binder.getCallingUid();
            if (callingUid != this.e) {
                if (cq1.a((Context) kp1.this).a("com.google.android.wearable.app.cn") && tm0.a(kp1.this, callingUid, "com.google.android.wearable.app.cn")) {
                    this.e = callingUid;
                } else if (tm0.a(kp1.this, callingUid)) {
                    this.e = callingUid;
                } else {
                    StringBuilder sb = new StringBuilder(57);
                    sb.append("Caller is not GooglePlayServices; caller UID: ");
                    sb.append(callingUid);
                    Log.e("WearableLS", sb.toString());
                    z = false;
                    if (z) {
                        return false;
                    }
                    synchronized (kp1.this.j) {
                        if (kp1.this.k) {
                            return false;
                        }
                        kp1.this.f.post(runnable);
                        return true;
                    }
                }
            }
            z = true;
            if (z) {
            }
        }
    }
}
