package com.fossil.blesdk.obfuscated;

import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z12 {
    @DexIgnore
    public static /* final */ z12 e; // = new z12(a22.b, 0, 0, 0);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ a22 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public z12(a22 a22, int i, int i2, int i3) {
        this.b = a22;
        this.a = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public int a() {
        return this.c;
    }

    @DexIgnore
    public int b() {
        return this.d;
    }

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.format("%s bits=%d bytes=%d", new Object[]{x12.b[this.a], Integer.valueOf(this.d), Integer.valueOf(this.c)});
    }

    @DexIgnore
    public z12 a(int i, int i2) {
        int i3 = this.d;
        a22 a22 = this.b;
        int i4 = this.a;
        if (i != i4) {
            int i5 = x12.c[i4][i];
            int i6 = 65535 & i5;
            int i7 = i5 >> 16;
            a22 = a22.a(i6, i7);
            i3 += i7;
        }
        int i8 = i == 2 ? 4 : 5;
        return new z12(a22.a(i2, i8), i, 0, i3 + i8);
    }

    @DexIgnore
    public z12 b(int i, int i2) {
        a22 a22 = this.b;
        int i3 = this.a == 2 ? 4 : 5;
        return new z12(a22.a(x12.e[this.a][i], i3).a(i2, 5), this.a, 0, this.d + i3 + 5);
    }

    @DexIgnore
    public z12 b(int i) {
        int i2 = this.c;
        if (i2 == 0) {
            return this;
        }
        return new z12(this.b.b(i - i2, i2), this.a, 0, this.d);
    }

    @DexIgnore
    public z12 a(int i) {
        a22 a22 = this.b;
        int i2 = this.a;
        int i3 = this.d;
        if (i2 == 4 || i2 == 2) {
            int i4 = x12.c[i2][0];
            int i5 = 65535 & i4;
            int i6 = i4 >> 16;
            a22 = a22.a(i5, i6);
            i3 += i6;
            i2 = 0;
        }
        int i7 = this.c;
        z12 z12 = new z12(a22, i2, this.c + 1, i3 + ((i7 == 0 || i7 == 31) ? 18 : i7 == 62 ? 9 : 8));
        return z12.c == 2078 ? z12.b(i + 1) : z12;
    }

    @DexIgnore
    public boolean a(z12 z12) {
        int i = this.d + (x12.c[this.a][z12.a] >> 16);
        int i2 = z12.c;
        if (i2 > 0) {
            int i3 = this.c;
            if (i3 == 0 || i3 > i2) {
                i += 10;
            }
        }
        return i <= z12.d;
    }

    @DexIgnore
    public b22 a(byte[] bArr) {
        LinkedList<a22> linkedList = new LinkedList<>();
        for (a22 a22 = b(bArr.length).b; a22 != null; a22 = a22.a()) {
            linkedList.addFirst(a22);
        }
        b22 b22 = new b22();
        for (a22 a2 : linkedList) {
            a2.a(b22, bArr);
        }
        return b22;
    }
}
