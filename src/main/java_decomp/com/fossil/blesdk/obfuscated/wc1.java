package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wc1 extends kk0 implements ne0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wc1> CREATOR; // = new hd1();
    @DexIgnore
    public /* final */ Status e;
    @DexIgnore
    public /* final */ xc1 f;

    @DexIgnore
    public wc1(Status status) {
        this(status, (xc1) null);
    }

    @DexIgnore
    public wc1(Status status, xc1 xc1) {
        this.e = status;
        this.f = xc1;
    }

    @DexIgnore
    public final Status G() {
        return this.e;
    }

    @DexIgnore
    public final xc1 H() {
        return this.f;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) G(), i, false);
        lk0.a(parcel, 2, (Parcelable) H(), i, false);
        lk0.a(parcel, a);
    }
}
