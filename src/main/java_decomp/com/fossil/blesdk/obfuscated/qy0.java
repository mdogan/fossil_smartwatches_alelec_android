package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qy0 extends st0 implements py0 {
    @DexIgnore
    public qy0() {
        super("com.google.android.gms.clearcut.internal.IClearcutLoggerCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                f((Status) nu0.a(parcel, Status.CREATOR));
                return true;
            case 2:
                g((Status) nu0.a(parcel, Status.CREATOR));
                throw null;
            case 3:
                a((Status) nu0.a(parcel, Status.CREATOR), parcel.readLong());
                throw null;
            case 4:
                h((Status) nu0.a(parcel, Status.CREATOR));
                throw null;
            case 5:
                b((Status) nu0.a(parcel, Status.CREATOR), parcel.readLong());
                throw null;
            case 6:
                a((Status) nu0.a(parcel, Status.CREATOR), (sd0[]) parcel.createTypedArray(sd0.CREATOR));
                throw null;
            case 7:
                a((DataHolder) nu0.a(parcel, DataHolder.CREATOR));
                throw null;
            case 8:
                a((Status) nu0.a(parcel, Status.CREATOR), (qd0) nu0.a(parcel, qd0.CREATOR));
                throw null;
            case 9:
                b((Status) nu0.a(parcel, Status.CREATOR), (qd0) nu0.a(parcel, qd0.CREATOR));
                throw null;
            default:
                return false;
        }
    }
}
