package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qh4 extends fi4 implements Runnable {
    @DexIgnore
    public static volatile Thread _thread;
    @DexIgnore
    public static volatile int debugStatus;
    @DexIgnore
    public static /* final */ long j;
    @DexIgnore
    public static /* final */ qh4 k;

    /*
    static {
        Long l;
        qh4 qh4 = new qh4();
        k = qh4;
        ei4.b(qh4, false, 1, (Object) null);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        wd4.a((Object) l, "try {\n            java.l\u2026AULT_KEEP_ALIVE\n        }");
        j = timeUnit.toNanos(l.longValue());
    }
    */

    @DexIgnore
    public Thread I() {
        Thread thread = _thread;
        return thread != null ? thread : Q();
    }

    @DexIgnore
    public final synchronized void P() {
        if (R()) {
            debugStatus = 3;
            O();
            notifyAll();
        }
    }

    @DexIgnore
    public final synchronized Thread Q() {
        Thread thread;
        thread = _thread;
        if (thread == null) {
            thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
            _thread = thread;
            thread.setDaemon(true);
            thread.start();
        }
        return thread;
    }

    @DexIgnore
    public final boolean R() {
        int i = debugStatus;
        return i == 2 || i == 3;
    }

    @DexIgnore
    public final synchronized boolean S() {
        if (R()) {
            return false;
        }
        debugStatus = 1;
        notifyAll();
        return true;
    }

    @DexIgnore
    public ai4 a(long j2, Runnable runnable) {
        wd4.b(runnable, "block");
        return b(j2, runnable);
    }

    @DexIgnore
    public void run() {
        nj4.b.a(this);
        oj4 a = pj4.a();
        if (a != null) {
            a.b();
        }
        try {
            if (S()) {
                long j2 = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long F = F();
                    if (F == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                        int i = (j2 > ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? 1 : (j2 == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? 0 : -1));
                        if (i == 0) {
                            oj4 a2 = pj4.a();
                            long a3 = a2 != null ? a2.a() : System.nanoTime();
                            if (i == 0) {
                                j2 = j + a3;
                            }
                            long j3 = j2 - a3;
                            if (j3 <= 0) {
                                _thread = null;
                                P();
                                oj4 a4 = pj4.a();
                                if (a4 != null) {
                                    a4.d();
                                }
                                if (!M()) {
                                    I();
                                    return;
                                }
                                return;
                            }
                            F = qe4.b(F, j3);
                        } else {
                            F = qe4.b(F, j);
                        }
                    }
                    if (F > 0) {
                        if (R()) {
                            _thread = null;
                            P();
                            oj4 a5 = pj4.a();
                            if (a5 != null) {
                                a5.d();
                            }
                            if (!M()) {
                                I();
                                return;
                            }
                            return;
                        }
                        oj4 a6 = pj4.a();
                        if (a6 != null) {
                            a6.a(this, F);
                        } else {
                            LockSupport.parkNanos(this, F);
                        }
                    }
                }
            }
        } finally {
            _thread = null;
            P();
            oj4 a7 = pj4.a();
            if (a7 != null) {
                a7.d();
            }
            if (!M()) {
                I();
            }
        }
    }
}
