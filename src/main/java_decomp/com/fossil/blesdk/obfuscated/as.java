package com.fossil.blesdk.obfuscated;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.tr;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class as<Data> implements tr<String, Data> {
    @DexIgnore
    public /* final */ tr<Uri, Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ur<String, AssetFileDescriptor> {
        @DexIgnore
        public tr<String, AssetFileDescriptor> a(xr xrVar) {
            return new as(xrVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ur<String, ParcelFileDescriptor> {
        @DexIgnore
        public tr<String, ParcelFileDescriptor> a(xr xrVar) {
            return new as(xrVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements ur<String, InputStream> {
        @DexIgnore
        public tr<String, InputStream> a(xr xrVar) {
            return new as(xrVar.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public as(tr<Uri, Data> trVar) {
        this.a = trVar;
    }

    @DexIgnore
    public static Uri b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return c(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? c(str) : parse;
    }

    @DexIgnore
    public static Uri c(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    public boolean a(String str) {
        return true;
    }

    @DexIgnore
    public tr.a<Data> a(String str, int i, int i2, mo moVar) {
        Uri b2 = b(str);
        if (b2 == null || !this.a.a(b2)) {
            return null;
        }
        return this.a.a(b2, i, i2, moVar);
    }
}
