package com.fossil.blesdk.obfuscated;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.blesdk.obfuscated.ap;
import com.fossil.blesdk.obfuscated.as;
import com.fossil.blesdk.obfuscated.bs;
import com.fossil.blesdk.obfuscated.cs;
import com.fossil.blesdk.obfuscated.ds;
import com.fossil.blesdk.obfuscated.es;
import com.fossil.blesdk.obfuscated.fs;
import com.fossil.blesdk.obfuscated.gr;
import com.fossil.blesdk.obfuscated.gs;
import com.fossil.blesdk.obfuscated.hr;
import com.fossil.blesdk.obfuscated.hs;
import com.fossil.blesdk.obfuscated.is;
import com.fossil.blesdk.obfuscated.jr;
import com.fossil.blesdk.obfuscated.kr;
import com.fossil.blesdk.obfuscated.kt;
import com.fossil.blesdk.obfuscated.lr;
import com.fossil.blesdk.obfuscated.qr;
import com.fossil.blesdk.obfuscated.uo;
import com.fossil.blesdk.obfuscated.yr;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sn implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile sn m;
    @DexIgnore
    public static volatile boolean n;
    @DexIgnore
    public /* final */ kq e;
    @DexIgnore
    public /* final */ br f;
    @DexIgnore
    public /* final */ un g;
    @DexIgnore
    public /* final */ Registry h;
    @DexIgnore
    public /* final */ hq i;
    @DexIgnore
    public /* final */ vu j;
    @DexIgnore
    public /* final */ nu k;
    @DexIgnore
    public /* final */ List<yn> l; // = new ArrayList();

    @DexIgnore
    public interface a {
        @DexIgnore
        sv build();
    }

    @DexIgnore
    public sn(Context context, rp rpVar, br brVar, kq kqVar, hq hqVar, vu vuVar, nu nuVar, int i2, a aVar, Map<Class<?>, zn<?, ?>> map, List<rv<Object>> list, boolean z, boolean z2, int i3, int i4) {
        no noVar;
        no noVar2;
        Context context2 = context;
        kq kqVar2 = kqVar;
        hq hqVar2 = hqVar;
        Class<Cdo> cls = Cdo.class;
        Class<byte[]> cls2 = byte[].class;
        MemoryCategory memoryCategory = MemoryCategory.NORMAL;
        this.e = kqVar2;
        this.i = hqVar2;
        this.f = brVar;
        this.j = vuVar;
        this.k = nuVar;
        Resources resources = context.getResources();
        this.h = new Registry();
        this.h.a((ImageHeaderParser) new ws());
        if (Build.VERSION.SDK_INT >= 27) {
            this.h.a((ImageHeaderParser) new at());
        }
        List<ImageHeaderParser> a2 = this.h.a();
        tt ttVar = new tt(context2, a2, kqVar2, hqVar2);
        no<ParcelFileDescriptor, Bitmap> b = jt.b(kqVar);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            xs xsVar = new xs(this.h.a(), resources.getDisplayMetrics(), kqVar2, hqVar2);
            noVar = new ss(xsVar);
            noVar2 = new gt(xsVar, hqVar2);
        } else {
            noVar2 = new dt();
            noVar = new ts();
        }
        pt ptVar = new pt(context2);
        yr.c cVar = new yr.c(resources);
        yr.d dVar = new yr.d(resources);
        yr.b bVar = new yr.b(resources);
        Class<byte[]> cls3 = cls2;
        yr.a aVar2 = new yr.a(resources);
        os osVar = new os(hqVar2);
        yr.a aVar3 = aVar2;
        du duVar = new du();
        gu guVar = new gu();
        ContentResolver contentResolver = context.getContentResolver();
        Registry registry = this.h;
        registry.a(ByteBuffer.class, new ir());
        registry.a(InputStream.class, new zr(hqVar2));
        registry.a("Bitmap", ByteBuffer.class, Bitmap.class, noVar);
        registry.a("Bitmap", InputStream.class, Bitmap.class, noVar2);
        registry.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, b);
        registry.a("Bitmap", AssetFileDescriptor.class, Bitmap.class, jt.a(kqVar));
        registry.a(Bitmap.class, Bitmap.class, bs.a.a());
        registry.a("Bitmap", Bitmap.class, Bitmap.class, new it());
        registry.a(Bitmap.class, osVar);
        registry.a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new ms(resources, noVar));
        registry.a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new ms(resources, noVar2));
        registry.a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new ms(resources, b));
        registry.a(BitmapDrawable.class, new ns(kqVar2, osVar));
        registry.a("Gif", InputStream.class, vt.class, new cu(a2, ttVar, hqVar2));
        registry.a("Gif", ByteBuffer.class, vt.class, ttVar);
        registry.a(vt.class, new wt());
        registry.a(cls, cls, bs.a.a());
        registry.a("Bitmap", cls, Bitmap.class, new au(kqVar2));
        registry.a(Uri.class, Drawable.class, ptVar);
        registry.a(Uri.class, Bitmap.class, new ft(ptVar, kqVar2));
        registry.a((uo.a<?>) new kt.a());
        registry.a(File.class, ByteBuffer.class, new jr.b());
        registry.a(File.class, InputStream.class, new lr.e());
        registry.a(File.class, File.class, new rt());
        registry.a(File.class, ParcelFileDescriptor.class, new lr.b());
        registry.a(File.class, File.class, bs.a.a());
        registry.a((uo.a<?>) new ap.a(hqVar2));
        yr.c cVar2 = cVar;
        registry.a(Integer.TYPE, InputStream.class, cVar2);
        yr.b bVar2 = bVar;
        registry.a(Integer.TYPE, ParcelFileDescriptor.class, bVar2);
        registry.a(Integer.class, InputStream.class, cVar2);
        registry.a(Integer.class, ParcelFileDescriptor.class, bVar2);
        yr.d dVar2 = dVar;
        registry.a(Integer.class, Uri.class, dVar2);
        yr.a aVar4 = aVar3;
        registry.a(Integer.TYPE, AssetFileDescriptor.class, aVar4);
        registry.a(Integer.class, AssetFileDescriptor.class, aVar4);
        registry.a(Integer.TYPE, Uri.class, dVar2);
        registry.a(String.class, InputStream.class, new kr.c());
        registry.a(Uri.class, InputStream.class, new kr.c());
        registry.a(String.class, InputStream.class, new as.c());
        registry.a(String.class, ParcelFileDescriptor.class, new as.b());
        registry.a(String.class, AssetFileDescriptor.class, new as.a());
        registry.a(Uri.class, InputStream.class, new fs.a());
        registry.a(Uri.class, InputStream.class, new gr.c(context.getAssets()));
        registry.a(Uri.class, ParcelFileDescriptor.class, new gr.b(context.getAssets()));
        Context context3 = context;
        registry.a(Uri.class, InputStream.class, new gs.a(context3));
        registry.a(Uri.class, InputStream.class, new hs.a(context3));
        ContentResolver contentResolver2 = contentResolver;
        registry.a(Uri.class, InputStream.class, new cs.d(contentResolver2));
        registry.a(Uri.class, ParcelFileDescriptor.class, new cs.b(contentResolver2));
        registry.a(Uri.class, AssetFileDescriptor.class, new cs.a(contentResolver2));
        registry.a(Uri.class, InputStream.class, new ds.a());
        registry.a(URL.class, InputStream.class, new is.a());
        registry.a(Uri.class, File.class, new qr.a(context3));
        registry.a(mr.class, InputStream.class, new es.a());
        Class<byte[]> cls4 = cls3;
        registry.a(cls4, ByteBuffer.class, new hr.a());
        registry.a(cls4, InputStream.class, new hr.d());
        registry.a(Uri.class, Uri.class, bs.a.a());
        registry.a(Drawable.class, Drawable.class, bs.a.a());
        registry.a(Drawable.class, Drawable.class, new qt());
        registry.a(Bitmap.class, BitmapDrawable.class, new eu(resources));
        du duVar2 = duVar;
        registry.a(Bitmap.class, cls4, duVar2);
        gu guVar2 = guVar;
        registry.a(Drawable.class, cls4, new fu(kqVar2, duVar2, guVar2));
        registry.a(vt.class, cls4, guVar2);
        Context context4 = context;
        hq hqVar3 = hqVar;
        this.g = new un(context4, hqVar3, this.h, new aw(), aVar, map, list, rpVar, z, i2);
    }

    @DexIgnore
    public static sn a(Context context) {
        if (m == null) {
            GeneratedAppGlideModule b = b(context.getApplicationContext());
            synchronized (sn.class) {
                if (m == null) {
                    a(context, b);
                }
            }
        }
        return m;
    }

    @DexIgnore
    public static void b(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        a(context, new tn(), generatedAppGlideModule);
    }

    @DexIgnore
    public kq c() {
        return this.e;
    }

    @DexIgnore
    public nu d() {
        return this.k;
    }

    @DexIgnore
    public Context e() {
        return this.g.getBaseContext();
    }

    @DexIgnore
    public un f() {
        return this.g;
    }

    @DexIgnore
    public Registry g() {
        return this.h;
    }

    @DexIgnore
    public vu h() {
        return this.j;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        a();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        a(i2);
    }

    @DexIgnore
    public static GeneratedAppGlideModule b(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new Class[]{Context.class}).newInstance(new Object[]{context.getApplicationContext()});
        } catch (ClassNotFoundException unused) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e2) {
            a((Exception) e2);
            throw null;
        } catch (IllegalAccessException e3) {
            a((Exception) e3);
            throw null;
        } catch (NoSuchMethodException e4) {
            a((Exception) e4);
            throw null;
        } catch (InvocationTargetException e5) {
            a((Exception) e5);
            throw null;
        }
    }

    @DexIgnore
    public static vu c(Context context) {
        uw.a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).h();
    }

    @DexIgnore
    public static yn d(Context context) {
        return c(context).a(context);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!n) {
            n = true;
            b(context, generatedAppGlideModule);
            n = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public hq b() {
        return this.i;
    }

    @DexIgnore
    public void b(yn ynVar) {
        synchronized (this.l) {
            if (this.l.contains(ynVar)) {
                this.l.remove(ynVar);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    @DexIgnore
    public static void a(Context context, tn tnVar, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<cv> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.a()) {
            emptyList = new ev(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.b().isEmpty()) {
            Set<Class<?>> b = generatedAppGlideModule.b();
            Iterator<cv> it = emptyList.iterator();
            while (it.hasNext()) {
                cv next = it.next();
                if (b.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (cv cvVar : emptyList) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + cvVar.getClass());
            }
        }
        tnVar.a(generatedAppGlideModule != null ? generatedAppGlideModule.c() : null);
        for (cv a2 : emptyList) {
            a2.a(applicationContext, tnVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, tnVar);
        }
        sn a3 = tnVar.a(applicationContext);
        for (cv next2 : emptyList) {
            try {
                next2.a(applicationContext, a3, a3.h);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + next2.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, a3, a3.h);
        }
        applicationContext.registerComponentCallbacks(a3);
        m = a3;
    }

    @DexIgnore
    public static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public void a() {
        vw.b();
        this.f.a();
        this.e.a();
        this.i.a();
    }

    @DexIgnore
    public void a(int i2) {
        vw.b();
        for (yn onTrimMemory : this.l) {
            onTrimMemory.onTrimMemory(i2);
        }
        this.f.a(i2);
        this.e.a(i2);
        this.i.a(i2);
    }

    @DexIgnore
    public static yn a(Fragment fragment) {
        return c(fragment.getContext()).a(fragment);
    }

    @DexIgnore
    public static yn a(View view) {
        return c(view.getContext()).a(view);
    }

    @DexIgnore
    public boolean a(cw<?> cwVar) {
        synchronized (this.l) {
            for (yn b : this.l) {
                if (b.b(cwVar)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void a(yn ynVar) {
        synchronized (this.l) {
            if (!this.l.contains(ynVar)) {
                this.l.add(ynVar);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
