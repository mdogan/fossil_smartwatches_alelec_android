package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class b54<T> implements d54<T> {
    @DexIgnore
    public /* final */ d54<T> a;

    @DexIgnore
    public b54(d54<T> d54) {
        this.a = d54;
    }

    @DexIgnore
    public abstract T a(Context context);

    @DexIgnore
    public final synchronized T a(Context context, e54<T> e54) throws Exception {
        T a2;
        a2 = a(context);
        if (a2 == null) {
            a2 = this.a != null ? this.a.a(context, e54) : e54.a(context);
            a(context, a2);
        }
        return a2;
    }

    @DexIgnore
    public abstract void b(Context context, T t);

    @DexIgnore
    public final void a(Context context, T t) {
        if (t != null) {
            b(context, t);
            return;
        }
        throw new NullPointerException();
    }
}
