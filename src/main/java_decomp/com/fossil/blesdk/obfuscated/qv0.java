package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qv0 {
    @DexIgnore
    public static /* final */ ov0 a; // = c();
    @DexIgnore
    public static /* final */ ov0 b; // = new pv0();

    @DexIgnore
    public static ov0 a() {
        return a;
    }

    @DexIgnore
    public static ov0 b() {
        return b;
    }

    @DexIgnore
    public static ov0 c() {
        try {
            return (ov0) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
