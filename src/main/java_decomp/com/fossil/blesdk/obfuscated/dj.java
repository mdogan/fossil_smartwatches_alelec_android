package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dj {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends dj {
        @DexIgnore
        public cj a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static dj a() {
        return new a();
    }

    @DexIgnore
    public abstract cj a(String str);

    @DexIgnore
    public final cj b(String str) {
        cj a2 = a(str);
        return a2 == null ? cj.a(str) : a2;
    }
}
