package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yx {
    @DexIgnore
    public long a;
    @DexIgnore
    public o64 b;

    @DexIgnore
    public yx(o64 o64) {
        if (o64 != null) {
            this.b = o64;
            return;
        }
        throw new NullPointerException("retryState must not be null");
    }

    @DexIgnore
    public boolean a(long j) {
        return j - this.a >= this.b.a() * 1000000;
    }

    @DexIgnore
    public void b(long j) {
        this.a = j;
        this.b = this.b.c();
    }

    @DexIgnore
    public void a() {
        this.a = 0;
        this.b = this.b.b();
    }
}
