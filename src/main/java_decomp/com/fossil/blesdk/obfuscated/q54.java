package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.common.IdManager;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface q54 {
    @DexIgnore
    Map<IdManager.DeviceIdentifierType, String> j();
}
