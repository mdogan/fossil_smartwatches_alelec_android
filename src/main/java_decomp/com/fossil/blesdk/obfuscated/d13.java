package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d13 implements Factory<NotificationHybridEveryonePresenter> {
    @DexIgnore
    public static NotificationHybridEveryonePresenter a(y03 y03, int i, ArrayList<ContactWrapper> arrayList, k62 k62, u03 u03) {
        return new NotificationHybridEveryonePresenter(y03, i, arrayList, k62, u03);
    }
}
