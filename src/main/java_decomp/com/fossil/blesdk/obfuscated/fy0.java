package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;
import com.google.android.gms.internal.clearcut.zzco;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fy0 extends su0<fy0, a> implements vv0 {
    @DexIgnore
    public static volatile dw0<fy0> zzbg;
    @DexIgnore
    public static /* final */ fy0 zzbir; // = new fy0();
    @DexIgnore
    public xu0<b> zzbiq; // = su0.h();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends su0.a<fy0, a> implements vv0 {
        @DexIgnore
        public a() {
            super(fy0.zzbir);
        }

        @DexIgnore
        public /* synthetic */ a(gy0 gy0) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends su0<b, a> implements vv0 {
        @DexIgnore
        public static volatile dw0<b> zzbg;
        @DexIgnore
        public static /* final */ b zzbiv; // = new b();
        @DexIgnore
        public int zzbb;
        @DexIgnore
        public String zzbis; // = "";
        @DexIgnore
        public long zzbit;
        @DexIgnore
        public long zzbiu;
        @DexIgnore
        public int zzya;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends su0.a<b, a> implements vv0 {
            @DexIgnore
            public a() {
                super(b.zzbiv);
            }

            @DexIgnore
            public /* synthetic */ a(gy0 gy0) {
                this();
            }

            @DexIgnore
            public final a a(long j) {
                g();
                ((b) this.f).a(j);
                return this;
            }

            @DexIgnore
            public final a a(String str) {
                g();
                ((b) this.f).a(str);
                return this;
            }

            @DexIgnore
            public final a b(long j) {
                g();
                ((b) this.f).b(j);
                return this;
            }
        }

        /*
        static {
            su0.a(b.class, zzbiv);
        }
        */

        @DexIgnore
        public static a n() {
            return (a) zzbiv.a(su0.e.e, (Object) null, (Object) null);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.dw0<com.fossil.blesdk.obfuscated.fy0$b>, com.fossil.blesdk.obfuscated.su0$b] */
        public final Object a(int i, Object obj, Object obj2) {
            dw0<b> dw0;
            switch (gy0.a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a((gy0) null);
                case 3:
                    return su0.a((tv0) zzbiv, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0002\u0003", new Object[]{"zzbb", "zzya", "zzbis", "zzbit", "zzbiu"});
                case 4:
                    return zzbiv;
                case 5:
                    dw0<b> dw02 = zzbg;
                    dw0<b> dw03 = dw02;
                    if (dw02 == null) {
                        synchronized (b.class) {
                            dw0<b> dw04 = zzbg;
                            dw0 = dw04;
                            if (dw04 == null) {
                                Object bVar = new su0.b(zzbiv);
                                zzbg = bVar;
                                dw0 = bVar;
                            }
                        }
                        dw03 = dw0;
                    }
                    return dw03;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        @DexIgnore
        public final void a(long j) {
            this.zzbb |= 4;
            this.zzbit = j;
        }

        @DexIgnore
        public final void a(String str) {
            if (str != null) {
                this.zzbb |= 2;
                this.zzbis = str;
                return;
            }
            throw new NullPointerException();
        }

        @DexIgnore
        public final void b(long j) {
            this.zzbb |= 8;
            this.zzbiu = j;
        }

        @DexIgnore
        public final int i() {
            return this.zzya;
        }

        @DexIgnore
        public final boolean j() {
            return (this.zzbb & 1) == 1;
        }

        @DexIgnore
        public final String k() {
            return this.zzbis;
        }

        @DexIgnore
        public final long l() {
            return this.zzbit;
        }

        @DexIgnore
        public final long m() {
            return this.zzbiu;
        }
    }

    /*
    static {
        su0.a(fy0.class, zzbir);
    }
    */

    @DexIgnore
    public static fy0 a(byte[] bArr) throws zzco {
        return (fy0) su0.b(zzbir, bArr);
    }

    @DexIgnore
    public static fy0 j() {
        return zzbir;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.dw0<com.fossil.blesdk.obfuscated.fy0>, com.fossil.blesdk.obfuscated.su0$b] */
    public final Object a(int i, Object obj, Object obj2) {
        dw0<fy0> dw0;
        switch (gy0.a[i - 1]) {
            case 1:
                return new fy0();
            case 2:
                return new a((gy0) null);
            case 3:
                return su0.a((tv0) zzbir, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0002\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzbiq", b.class});
            case 4:
                return zzbir;
            case 5:
                dw0<fy0> dw02 = zzbg;
                dw0<fy0> dw03 = dw02;
                if (dw02 == null) {
                    synchronized (fy0.class) {
                        dw0<fy0> dw04 = zzbg;
                        dw0 = dw04;
                        if (dw04 == null) {
                            Object bVar = new su0.b(zzbir);
                            zzbg = bVar;
                            dw0 = bVar;
                        }
                    }
                    dw03 = dw0;
                }
                return dw03;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final List<b> i() {
        return this.zzbiq;
    }
}
