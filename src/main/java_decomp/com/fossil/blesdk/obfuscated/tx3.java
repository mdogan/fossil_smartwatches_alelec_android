package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.NetworkInfo;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.jy3;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tx3 implements Runnable {
    @DexIgnore
    public static /* final */ jy3 A; // = new b();
    @DexIgnore
    public static /* final */ Object x; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> y; // = new a();
    @DexIgnore
    public static /* final */ AtomicInteger z; // = new AtomicInteger();
    @DexIgnore
    public /* final */ int e; // = z.incrementAndGet();
    @DexIgnore
    public /* final */ Picasso f;
    @DexIgnore
    public /* final */ zx3 g;
    @DexIgnore
    public /* final */ ux3 h;
    @DexIgnore
    public /* final */ ly3 i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ hy3 k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public /* final */ jy3 n;
    @DexIgnore
    public rx3 o;
    @DexIgnore
    public List<rx3> p;
    @DexIgnore
    public Bitmap q;
    @DexIgnore
    public Future<?> r;
    @DexIgnore
    public Picasso.LoadedFrom s;
    @DexIgnore
    public Exception t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public Picasso.Priority w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends jy3 {
        @DexIgnore
        public jy3.a a(hy3 hy3, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + hy3);
        }

        @DexIgnore
        public boolean a(hy3 hy3) {
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation e;
        @DexIgnore
        public /* final */ /* synthetic */ RuntimeException f;

        @DexIgnore
        public c(Transformation transformation, RuntimeException runtimeException) {
            this.e = transformation;
            this.f = runtimeException;
        }

        @DexIgnore
        public void run() {
            throw new RuntimeException("Transformation " + this.e.key() + " crashed with exception.", this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder e;

        @DexIgnore
        public d(StringBuilder sb) {
            this.e = sb;
        }

        @DexIgnore
        public void run() {
            throw new NullPointerException(this.e.toString());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation e;

        @DexIgnore
        public e(Transformation transformation) {
            this.e = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.e.key() + " returned input Bitmap but recycled it.");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation e;

        @DexIgnore
        public f(Transformation transformation) {
            this.e = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.e.key() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    @DexIgnore
    public tx3(Picasso picasso, zx3 zx3, ux3 ux3, ly3 ly3, rx3 rx3, jy3 jy3) {
        this.f = picasso;
        this.g = zx3;
        this.h = ux3;
        this.i = ly3;
        this.o = rx3;
        this.j = rx3.c();
        this.k = rx3.h();
        this.w = rx3.g();
        this.l = rx3.d();
        this.m = rx3.e();
        this.n = jy3;
        this.v = jy3.a();
    }

    @DexIgnore
    public static Bitmap a(InputStream inputStream, hy3 hy3) throws IOException {
        dy3 dy3 = new dy3(inputStream);
        long b2 = dy3.b(65536);
        BitmapFactory.Options b3 = jy3.b(hy3);
        boolean a2 = jy3.a(b3);
        boolean b4 = py3.b((InputStream) dy3);
        dy3.h(b2);
        if (b4) {
            byte[] c2 = py3.c((InputStream) dy3);
            if (a2) {
                BitmapFactory.decodeByteArray(c2, 0, c2.length, b3);
                jy3.a(hy3.h, hy3.i, b3, hy3);
            }
            return BitmapFactory.decodeByteArray(c2, 0, c2.length, b3);
        }
        if (a2) {
            BitmapFactory.decodeStream(dy3, (Rect) null, b3);
            jy3.a(hy3.h, hy3.i, b3, hy3);
            dy3.h(b2);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(dy3, (Rect) null, b3);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    @DexIgnore
    public static boolean a(boolean z2, int i2, int i3, int i4, int i5) {
        return !z2 || i2 > i4 || i3 > i5;
    }

    @DexIgnore
    public void b(rx3 rx3) {
        boolean z2;
        if (this.o == rx3) {
            this.o = null;
            z2 = true;
        } else {
            List<rx3> list = this.p;
            z2 = list != null ? list.remove(rx3) : false;
        }
        if (z2 && rx3.g() == this.w) {
            this.w = b();
        }
        if (this.f.n) {
            py3.a("Hunter", "removed", rx3.b.d(), py3.a(this, "from "));
        }
    }

    @DexIgnore
    public rx3 c() {
        return this.o;
    }

    @DexIgnore
    public List<rx3> d() {
        return this.p;
    }

    @DexIgnore
    public hy3 e() {
        return this.k;
    }

    @DexIgnore
    public Exception f() {
        return this.t;
    }

    @DexIgnore
    public String g() {
        return this.j;
    }

    @DexIgnore
    public Picasso.LoadedFrom h() {
        return this.s;
    }

    @DexIgnore
    public int i() {
        return this.l;
    }

    @DexIgnore
    public Picasso j() {
        return this.f;
    }

    @DexIgnore
    public Picasso.Priority k() {
        return this.w;
    }

    @DexIgnore
    public Bitmap l() {
        return this.q;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public Bitmap m() throws IOException {
        Bitmap bitmap;
        if (MemoryPolicy.shouldReadFromMemoryCache(this.l)) {
            bitmap = this.h.a(this.j);
            if (bitmap != null) {
                this.i.b();
                this.s = Picasso.LoadedFrom.MEMORY;
                if (this.f.n) {
                    py3.a("Hunter", "decoded", this.k.d(), "from cache");
                }
                return bitmap;
            }
        } else {
            bitmap = null;
        }
        this.k.c = this.v == 0 ? NetworkPolicy.OFFLINE.index : this.m;
        jy3.a a2 = this.n.a(this.k, this.m);
        if (a2 != null) {
            this.s = a2.c();
            this.u = a2.b();
            bitmap = a2.a();
            if (bitmap == null) {
                InputStream d2 = a2.d();
                try {
                    Bitmap a3 = a(d2, this.k);
                    py3.a(d2);
                    bitmap = a3;
                } catch (Throwable th) {
                    py3.a(d2);
                    throw th;
                }
            }
        }
        if (bitmap != null) {
            if (this.f.n) {
                py3.a("Hunter", "decoded", this.k.d());
            }
            this.i.a(bitmap);
            if (this.k.f() || this.u != 0) {
                synchronized (x) {
                    if (this.k.e() || this.u != 0) {
                        bitmap = a(this.k, bitmap, this.u);
                        if (this.f.n) {
                            py3.a("Hunter", "transformed", this.k.d());
                        }
                    }
                    if (this.k.b()) {
                        bitmap = a(this.k.g, bitmap);
                        if (this.f.n) {
                            py3.a("Hunter", "transformed", this.k.d(), "from custom transformations");
                        }
                    }
                }
                if (bitmap != null) {
                    this.i.b(bitmap);
                }
            }
        }
        return bitmap;
    }

    @DexIgnore
    public boolean n() {
        Future<?> future = this.r;
        return future != null && future.isCancelled();
    }

    @DexIgnore
    public boolean o() {
        return this.n.b();
    }

    @DexIgnore
    public void run() {
        try {
            a(this.k);
            if (this.f.n) {
                py3.a("Hunter", "executing", py3.a(this));
            }
            this.q = m();
            if (this.q == null) {
                this.g.c(this);
            } else {
                this.g.b(this);
            }
        } catch (Downloader.ResponseException e2) {
            if (!e2.localCacheOnly || e2.responseCode != 504) {
                this.t = e2;
            }
            this.g.c(this);
        } catch (NetworkRequestHandler.ContentLengthException e3) {
            this.t = e3;
            this.g.d(this);
        } catch (IOException e4) {
            this.t = e4;
            this.g.d(this);
        } catch (OutOfMemoryError e5) {
            StringWriter stringWriter = new StringWriter();
            this.i.a().a(new PrintWriter(stringWriter));
            this.t = new RuntimeException(stringWriter.toString(), e5);
            this.g.c(this);
        } catch (Exception e6) {
            this.t = e6;
            this.g.c(this);
        } catch (Throwable th) {
            Thread.currentThread().setName("Picasso-Idle");
            throw th;
        }
        Thread.currentThread().setName("Picasso-Idle");
    }

    @DexIgnore
    public final Picasso.Priority b() {
        Picasso.Priority priority = Picasso.Priority.LOW;
        List<rx3> list = this.p;
        boolean z2 = true;
        boolean z3 = list != null && !list.isEmpty();
        if (this.o == null && !z3) {
            z2 = false;
        }
        if (!z2) {
            return priority;
        }
        rx3 rx3 = this.o;
        if (rx3 != null) {
            priority = rx3.g();
        }
        if (z3) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                Picasso.Priority g2 = this.p.get(i2).g();
                if (g2.ordinal() > priority.ordinal()) {
                    priority = g2;
                }
            }
        }
        return priority;
    }

    @DexIgnore
    public void a(rx3 rx3) {
        boolean z2 = this.f.n;
        hy3 hy3 = rx3.b;
        if (this.o == null) {
            this.o = rx3;
            if (z2) {
                List<rx3> list = this.p;
                if (list == null || list.isEmpty()) {
                    py3.a("Hunter", "joined", hy3.d(), "to empty hunter");
                } else {
                    py3.a("Hunter", "joined", hy3.d(), py3.a(this, "to "));
                }
            }
        } else {
            if (this.p == null) {
                this.p = new ArrayList(3);
            }
            this.p.add(rx3);
            if (z2) {
                py3.a("Hunter", "joined", hy3.d(), py3.a(this, "to "));
            }
            Picasso.Priority g2 = rx3.g();
            if (g2.ordinal() > this.w.ordinal()) {
                this.w = g2;
            }
        }
    }

    @DexIgnore
    public boolean a() {
        if (this.o != null) {
            return false;
        }
        List<rx3> list = this.p;
        if (list != null && !list.isEmpty()) {
            return false;
        }
        Future<?> future = this.r;
        if (future == null || !future.cancel(false)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean a(boolean z2, NetworkInfo networkInfo) {
        if (!(this.v > 0)) {
            return false;
        }
        this.v--;
        return this.n.a(z2, networkInfo);
    }

    @DexIgnore
    public static void a(hy3 hy3) {
        String a2 = hy3.a();
        StringBuilder sb = y.get();
        sb.ensureCapacity(a2.length() + 8);
        sb.replace(8, sb.length(), a2);
        Thread.currentThread().setName(sb.toString());
    }

    @DexIgnore
    public static tx3 a(Picasso picasso, zx3 zx3, ux3 ux3, ly3 ly3, rx3 rx3) {
        hy3 h2 = rx3.h();
        List<jy3> a2 = picasso.a();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            jy3 jy3 = a2.get(i2);
            if (jy3.a(h2)) {
                return new tx3(picasso, zx3, ux3, ly3, rx3, jy3);
            }
        }
        return new tx3(picasso, zx3, ux3, ly3, rx3, A);
    }

    @DexIgnore
    public static Bitmap a(List<Transformation> list, Bitmap bitmap) {
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            Transformation transformation = list.get(i2);
            try {
                Bitmap transform = transformation.transform(bitmap);
                if (transform == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(transformation.key());
                    sb.append(" returned null after ");
                    sb.append(i2);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (Transformation key : list) {
                        sb.append(key.key());
                        sb.append(10);
                    }
                    Picasso.p.post(new d(sb));
                    return null;
                } else if (transform == bitmap && bitmap.isRecycled()) {
                    Picasso.p.post(new e(transformation));
                    return null;
                } else if (transform == bitmap || bitmap.isRecycled()) {
                    i2++;
                    bitmap = transform;
                } else {
                    Picasso.p.post(new f(transformation));
                    return null;
                }
            } catch (RuntimeException e2) {
                Picasso.p.post(new c(transformation, e2));
                return null;
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c4  */
    public static Bitmap a(hy3 hy3, Bitmap bitmap, int i2) {
        int i3;
        int i4;
        int i5;
        Bitmap createBitmap;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        int i6;
        int i7;
        int i8;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        boolean z2 = hy3.l;
        Matrix matrix = new Matrix();
        int i9 = 0;
        if (hy3.e()) {
            int i10 = hy3.h;
            int i11 = hy3.i;
            float f7 = hy3.m;
            if (f7 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (hy3.p) {
                    matrix.setRotate(f7, hy3.n, hy3.o);
                } else {
                    matrix.setRotate(f7);
                }
            }
            if (hy3.j) {
                float f8 = (float) i10;
                float f9 = (float) width;
                float f10 = f8 / f9;
                float f11 = (float) i11;
                float f12 = (float) height;
                float f13 = f11 / f12;
                if (f10 > f13) {
                    int ceil = (int) Math.ceil((double) (f12 * (f13 / f10)));
                    i8 = (height - ceil) / 2;
                    f13 = f11 / ((float) ceil);
                    i6 = ceil;
                    f6 = f10;
                    i7 = width;
                } else {
                    int ceil2 = (int) Math.ceil((double) (f9 * (f10 / f13)));
                    f6 = f8 / ((float) ceil2);
                    i6 = height;
                    i9 = (width - ceil2) / 2;
                    i7 = ceil2;
                    i8 = 0;
                }
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f6, f13);
                }
                i5 = i8;
                i4 = i7;
                i3 = i6;
                if (i2 != 0) {
                    matrix.preRotate((float) i2);
                }
                createBitmap = Bitmap.createBitmap(bitmap, i9, i5, i4, i3, matrix, true);
                if (createBitmap != bitmap) {
                    return bitmap;
                }
                bitmap.recycle();
                return createBitmap;
            } else if (hy3.k) {
                float f14 = ((float) i10) / ((float) width);
                float f15 = ((float) i11) / ((float) height);
                if (f14 >= f15) {
                    f14 = f15;
                }
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f14, f14);
                }
            } else if (!((i10 == 0 && i11 == 0) || (i10 == width && i11 == height))) {
                if (i10 != 0) {
                    f2 = (float) i10;
                    f3 = (float) width;
                } else {
                    f2 = (float) i11;
                    f3 = (float) height;
                }
                float f16 = f2 / f3;
                if (i11 != 0) {
                    f5 = (float) i11;
                    f4 = (float) height;
                } else {
                    f5 = (float) i10;
                    f4 = (float) width;
                }
                float f17 = f5 / f4;
                if (a(z2, width, height, i10, i11)) {
                    matrix.preScale(f16, f17);
                }
            }
        }
        i4 = width;
        i3 = height;
        i5 = 0;
        if (i2 != 0) {
        }
        createBitmap = Bitmap.createBitmap(bitmap, i9, i5, i4, i3, matrix, true);
        if (createBitmap != bitmap) {
        }
    }
}
