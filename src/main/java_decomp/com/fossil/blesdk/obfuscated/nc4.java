package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.CoroutineSingletons;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nc4<T> implements kc4<T>, rc4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<nc4<?>, Object> g; // = AtomicReferenceFieldUpdater.newUpdater(nc4.class, Object.class, "e");
    @DexIgnore
    public volatile Object e;
    @DexIgnore
    public /* final */ kc4<T> f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public nc4(kc4<? super T> kc4, Object obj) {
        wd4.b(kc4, "delegate");
        this.f = kc4;
        this.e = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.e;
        CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
        if (obj == coroutineSingletons) {
            if (g.compareAndSet(this, coroutineSingletons, oc4.a())) {
                return oc4.a();
            }
            obj = this.e;
        }
        if (obj == CoroutineSingletons.RESUMED) {
            return oc4.a();
        }
        if (!(obj instanceof Result.Failure)) {
            return obj;
        }
        throw ((Result.Failure) obj).exception;
    }

    @DexIgnore
    public rc4 getCallerFrame() {
        kc4<T> kc4 = this.f;
        if (!(kc4 instanceof rc4)) {
            kc4 = null;
        }
        return (rc4) kc4;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.f.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.e;
            CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
            if (obj2 == coroutineSingletons) {
                if (g.compareAndSet(this, coroutineSingletons, obj)) {
                    return;
                }
            } else if (obj2 != oc4.a()) {
                throw new IllegalStateException("Already resumed");
            } else if (g.compareAndSet(this, oc4.a(), CoroutineSingletons.RESUMED)) {
                this.f.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.f;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public nc4(kc4<? super T> kc4) {
        this(kc4, CoroutineSingletons.UNDECIDED);
        wd4.b(kc4, "delegate");
    }
}
