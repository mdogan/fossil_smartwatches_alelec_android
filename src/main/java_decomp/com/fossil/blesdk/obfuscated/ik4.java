package com.fossil.blesdk.obfuscated;

import java.util.List;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ik4 {
    @DexIgnore
    public static final bj4 a(MainDispatcherFactory mainDispatcherFactory, List<? extends MainDispatcherFactory> list) {
        wd4.b(mainDispatcherFactory, "$this$tryCreateDispatcher");
        wd4.b(list, "factories");
        try {
            return mainDispatcherFactory.createDispatcher(list);
        } catch (Throwable th) {
            return new jk4(th, mainDispatcherFactory.hintOnError());
        }
    }
}
