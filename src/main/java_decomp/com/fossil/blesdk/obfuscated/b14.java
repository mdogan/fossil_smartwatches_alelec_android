package com.fossil.blesdk.obfuscated;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b14 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (i04.s() && k04.r != null) {
            if (i04.p()) {
                h14.b(k04.r).a((p04) new o04(k04.r, k04.a(k04.r, false, (l04) null), 2, th, thread, (l04) null), (q24) null, false, true);
                k04.m.b((Object) "MTA has caught the following uncaught exception:");
                k04.m.b(th);
            }
            k04.f(k04.r);
            if (k04.n != null) {
                k04.m.a((Object) "Call the original uncaught exception handler.");
                if (!(k04.n instanceof b14)) {
                    k04.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
