package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z14 {
    @DexIgnore
    public ExecutorService a;

    @DexIgnore
    public z14() {
        this.a = null;
        this.a = Executors.newSingleThreadExecutor();
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }
}
