package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oi4 extends ng4 {
    @DexIgnore
    public /* final */ jd4<Throwable, cb4> e;

    @DexIgnore
    public oi4(jd4<? super Throwable, cb4> jd4) {
        wd4.b(jd4, "handler");
        this.e = jd4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.invoke(th);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + ph4.a((Object) this.e) + '@' + ph4.b(this) + ']';
    }
}
