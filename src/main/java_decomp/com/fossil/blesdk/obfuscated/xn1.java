package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xn1<TResult> {
    @DexIgnore
    public xn1<TResult> a(sn1<TResult> sn1) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract xn1<TResult> a(tn1 tn1);

    @DexIgnore
    public abstract xn1<TResult> a(un1<? super TResult> un1);

    @DexIgnore
    public abstract xn1<TResult> a(Executor executor, tn1 tn1);

    @DexIgnore
    public abstract xn1<TResult> a(Executor executor, un1<? super TResult> un1);

    @DexIgnore
    public abstract Exception a();

    @DexIgnore
    public abstract <X extends Throwable> TResult a(Class<X> cls) throws Throwable;

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> b(qn1<TResult, xn1<TContinuationResult>> qn1) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract TResult b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract boolean e();

    @DexIgnore
    public xn1<TResult> a(Executor executor, sn1<TResult> sn1) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> b(Executor executor, qn1<TResult, xn1<TContinuationResult>> qn1) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public xn1<TResult> a(Executor executor, rn1 rn1) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> a(qn1<TResult, TContinuationResult> qn1) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> a(Executor executor, qn1<TResult, TContinuationResult> qn1) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> a(wn1<TResult, TContinuationResult> wn1) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> xn1<TContinuationResult> a(Executor executor, wn1<TResult, TContinuationResult> wn1) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
