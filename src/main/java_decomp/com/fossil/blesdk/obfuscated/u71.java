package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u71 implements v71 {
    @DexIgnore
    public u71() {
    }

    @DexIgnore
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    @DexIgnore
    public /* synthetic */ u71(r71 r71) {
        this();
    }
}
