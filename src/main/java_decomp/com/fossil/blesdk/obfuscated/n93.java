package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n93 implements Factory<q93> {
    @DexIgnore
    public static q93 a(l93 l93) {
        q93 b = l93.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
