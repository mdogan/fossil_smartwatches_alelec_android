package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gk4;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fk4<E> {
    @DexIgnore
    public static /* final */ /* synthetic */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(fk4.class, Object.class, "_cur$internal");
    @DexIgnore
    public volatile /* synthetic */ Object _cur$internal;

    @DexIgnore
    public fk4(boolean z) {
        this._cur$internal = new gk4(8, z);
    }

    @DexIgnore
    public final void a() {
        while (true) {
            gk4 gk4 = (gk4) this._cur$internal;
            if (!gk4.a()) {
                a.compareAndSet(this, gk4, gk4.e());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int b() {
        return ((gk4) this._cur$internal).b();
    }

    @DexIgnore
    public final E c() {
        E e;
        E e2;
        while (true) {
            gk4 gk4 = (gk4) this._cur$internal;
            while (true) {
                long j = gk4._state$internal;
                e = null;
                if ((1152921504606846976L & j) == 0) {
                    gk4.a aVar = gk4.h;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((gk4.a & ((int) ((1152921503533105152L & j) >> 30))) != (gk4.a & i)) {
                        e2 = gk4.b.get(gk4.a & i);
                        if (e2 != null) {
                            if (!(e2 instanceof gk4.b)) {
                                int i2 = (i + 1) & 1073741823;
                                if (!gk4.f.compareAndSet(gk4, j, gk4.h.a(j, i2))) {
                                    if (gk4.d) {
                                        gk4 gk42 = gk4;
                                        do {
                                            gk42 = gk42.a(i, i2);
                                        } while (gk42 != null);
                                        break;
                                    }
                                } else {
                                    gk4.b.set(gk4.a & i, (Object) null);
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (gk4.d) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    e = gk4.g;
                    break;
                }
            }
            e = e2;
            if (e != gk4.g) {
                return e;
            }
            a.compareAndSet(this, gk4, gk4.e());
        }
    }

    @DexIgnore
    public final boolean a(E e) {
        wd4.b(e, "element");
        while (true) {
            gk4 gk4 = (gk4) this._cur$internal;
            int a2 = gk4.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, gk4, gk4.e());
            } else if (a2 == 2) {
                return false;
            }
        }
    }
}
