package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i12 extends m12 {
    @DexIgnore
    public h12[] a;

    @DexIgnore
    public i12() {
        a();
    }

    @DexIgnore
    public i12 a() {
        this.a = h12.b();
        return this;
    }

    @DexIgnore
    public i12 a(k12 k12) throws IOException {
        while (true) {
            int j = k12.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                int a2 = o12.a(k12, 10);
                h12[] h12Arr = this.a;
                int length = h12Arr == null ? 0 : h12Arr.length;
                h12[] h12Arr2 = new h12[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.a, 0, h12Arr2, 0, length);
                }
                while (length < h12Arr2.length - 1) {
                    h12Arr2[length] = new h12();
                    k12.a((m12) h12Arr2[length]);
                    k12.j();
                    length++;
                }
                h12Arr2[length] = new h12();
                k12.a((m12) h12Arr2[length]);
                this.a = h12Arr2;
            } else if (!o12.b(k12, j)) {
                return this;
            }
        }
    }
}
