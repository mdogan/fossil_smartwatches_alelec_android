package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ec1 {
    @DexIgnore
    public static /* final */ int[] a; // = new int[0];
    @DexIgnore
    public static /* final */ long[] b; // = new long[0];
    @DexIgnore
    public static /* final */ String[] c; // = new String[0];
    @DexIgnore
    public static /* final */ byte[] d; // = new byte[0];

    @DexIgnore
    public static final int a(ub1 ub1, int i) throws IOException {
        int a2 = ub1.a();
        ub1.b(i);
        int i2 = 1;
        while (ub1.c() == i) {
            ub1.b(i);
            i2++;
        }
        ub1.b(a2, i);
        return i2;
    }
}
