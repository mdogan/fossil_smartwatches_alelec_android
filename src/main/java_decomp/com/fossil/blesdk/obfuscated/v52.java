package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class v52 {
    @DexIgnore
    public /* final */ yg4 a; // = lj4.a((ri4) null, 1, (Object) null);
    @DexIgnore
    public /* final */ gh4 b; // = zh4.b();
    @DexIgnore
    public /* final */ gh4 c; // = zh4.a();
    @DexIgnore
    public /* final */ bj4 d; // = zh4.c();
    @DexIgnore
    public /* final */ lh4 e; // = mh4.a(this.a.plus(this.d));

    @DexIgnore
    public final gh4 b() {
        return this.c;
    }

    @DexIgnore
    public final gh4 c() {
        return this.b;
    }

    @DexIgnore
    public final bj4 d() {
        return this.d;
    }

    @DexIgnore
    public final lh4 e() {
        return this.e;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public void g() {
    }
}
