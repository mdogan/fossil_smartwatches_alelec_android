package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn3 {
    @DexIgnore
    public static /* final */ String d; // = d;
    @DexIgnore
    public /* final */ b a; // = new b(this);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ pm3 c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ nn3 a;

        @DexIgnore
        public b(nn3 nn3) {
            this.a = nn3;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wd4.b(context, "context");
            wd4.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && cg4.b(otaEvent.getSerial(), this.a.c(), true)) {
                this.a.b().g((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public nn3(String str, pm3 pm3) {
        wd4.b(str, "serial");
        wd4.b(pm3, "mView");
        this.b = str;
        this.c = pm3;
    }

    @DexIgnore
    public final void a() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.b);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "serial=" + this.b + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__NeverMissAStepAndKeep));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_diana_two);
            explore3.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__GlanceableContentReadyOnTheWrist));
            explore4.setBackground(R.drawable.update_fw_diana_four);
        } else {
            explore.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__TrackActivitySleepAndPersonalGoals));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_hybrid_two);
            explore3.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__NoChargingNeededYourWatchIs));
            explore4.setBackground(R.drawable.update_fw_hybrid_four);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.c.i(arrayList);
    }

    @DexIgnore
    public final pm3 b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final void d() {
        PortfolioApp c2 = PortfolioApp.W.c();
        b bVar = this.a;
        c2.registerReceiver(bVar, new IntentFilter(PortfolioApp.W.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        a();
        this.c.g();
    }

    @DexIgnore
    public final void e() {
        PortfolioApp.W.c().unregisterReceiver(this.a);
    }

    @DexIgnore
    public final void f() {
        if (!DeviceIdentityUtils.isDianaDevice(this.b)) {
            this.c.U0();
        }
    }
}
