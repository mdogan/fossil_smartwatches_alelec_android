package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cn0 {
    @DexIgnore
    public static cn0 b; // = new cn0();
    @DexIgnore
    public bn0 a; // = null;

    @DexIgnore
    public static bn0 b(Context context) {
        return b.a(context);
    }

    @DexIgnore
    public final synchronized bn0 a(Context context) {
        if (this.a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.a = new bn0(context);
        }
        return this.a;
    }
}
