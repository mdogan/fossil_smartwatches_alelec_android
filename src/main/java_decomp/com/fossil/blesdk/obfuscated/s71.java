package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s71 implements Comparator<zzte> {
    @DexIgnore
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzte zzte = (zzte) obj;
        zzte zzte2 = (zzte) obj2;
        w71 w71 = (w71) zzte.iterator();
        w71 w712 = (w71) zzte2.iterator();
        while (w71.hasNext() && w712.hasNext()) {
            int compare = Integer.compare(zzte.a(w71.nextByte()), zzte.a(w712.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzte.size(), zzte2.size());
    }
}
