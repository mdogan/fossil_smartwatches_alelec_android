package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Unit;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface we3 extends w52<ve3> {
    @DexIgnore
    void a(xr2 xr2, ArrayList<String> arrayList);

    @DexIgnore
    void a(Unit unit, ActivitySummary activitySummary);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void a(boolean z, Unit unit, rd<WorkoutSession> rdVar);
}
