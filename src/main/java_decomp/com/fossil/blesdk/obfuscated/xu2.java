package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ak3;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xu2 extends bs2 implements su2, xs3.g, ak3.a {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a((rd4) null);
    @DexIgnore
    public HomeDashboardPresenter k;
    @DexIgnore
    public HomeDianaCustomizePresenter l;
    @DexIgnore
    public HomeHybridCustomizePresenter m;
    @DexIgnore
    public HomeProfilePresenter n;
    @DexIgnore
    public HomeAlertsPresenter o;
    @DexIgnore
    public HomeAlertsHybridPresenter p;
    @DexIgnore
    public jg3 q;
    @DexIgnore
    public ru2 r;
    @DexIgnore
    public ur3<xc2> s;
    @DexIgnore
    public /* final */ ArrayList<Fragment> t; // = new ArrayList<>();
    @DexIgnore
    public ju3 u;
    @DexIgnore
    public InAppNotificationUtils v; // = InAppNotificationUtils.b.a();
    @DexIgnore
    public HashMap w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final xu2 a() {
            return new xu2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements dc<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ xu2 a;

        @DexIgnore
        public b(xu2 xu2) {
            this.a = xu2;
        }

        @DexIgnore
        public final void a(Integer num) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String Z0 = xu2.x;
            local.d(Z0, "dashboardTab: " + num);
            int intValue = num != null ? num.intValue() : 0;
            this.a.p(intValue);
            xu2.b(this.a).a(intValue);
            this.a.q(intValue);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements BottomNavigationView.c {
        @DexIgnore
        public /* final */ /* synthetic */ xu2 a;

        @DexIgnore
        public c(xu2 xu2) {
            this.a = xu2;
        }

        @DexIgnore
        public final boolean a(MenuItem menuItem) {
            int i;
            wd4.b(menuItem, "item");
            xc2 xc2 = (xc2) xu2.a(this.a).a();
            BottomNavigationView bottomNavigationView = xc2 != null ? xc2.q : null;
            if (bottomNavigationView != null) {
                wd4.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                wd4.a((Object) menu, "mBinding.get()?.bottomNavigation!!.menu");
                menu.findItem(R.id.dashboardFragment).setIcon(R.drawable.ic_home);
                menu.findItem(R.id.customizeFragment).setIcon(R.drawable.ic_customization);
                menu.findItem(R.id.profileFragment).setIcon(R.drawable.ic_profile);
                menu.findItem(R.id.alertsFragment).setIcon(R.drawable.ic_alerts);
                switch (menuItem.getItemId()) {
                    case R.id.alertsFragment /*2131361845*/:
                        menuItem.setIcon(R.drawable.ic_alerts_active);
                        i = 2;
                        break;
                    case R.id.customizeFragment /*2131362044*/:
                        menuItem.setIcon(R.drawable.ic_customization_active);
                        i = 1;
                        break;
                    case R.id.dashboardFragment /*2131362051*/:
                        Context context = this.a.getContext();
                        if (context != null) {
                            Drawable c = k6.c(context, R.drawable.ic_home_active);
                            if (c != null) {
                                Context context2 = this.a.getContext();
                                if (context2 != null) {
                                    c.setColorFilter(k6.a(context2, (int) R.color.primaryColor), PorterDuff.Mode.SRC_ATOP);
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            }
                            menuItem.setIcon(c);
                            break;
                        } else {
                            wd4.a();
                            throw null;
                        }
                    case R.id.profileFragment /*2131362605*/:
                        menuItem.setIcon(R.drawable.ic_profile_active);
                        i = 3;
                        break;
                }
                i = 0;
                xu2 xu2 = this.a;
                if (xu2.m != null) {
                    xu2.V0().b(i);
                }
                xu2 xu22 = this.a;
                if (xu22.l != null) {
                    xu22.U0().b(i);
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String Z0 = xu2.x;
                local.d(Z0, "show tab with tab=" + i);
                xu2.b(this.a).a(i);
                this.a.q(i);
                return true;
            }
            wd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements dc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ xu2 a;

        @DexIgnore
        public d(xu2 xu2) {
            this.a = xu2;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String Z0 = xu2.x;
            local.d(Z0, "activeDeviceSerialLiveData onChange " + str);
            ru2 b = xu2.b(this.a);
            if (str != null) {
                b.a(str);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
            return true;
        }
    }

    /*
    static {
        String simpleName = xu2.class.getSimpleName();
        wd4.a((Object) simpleName, "HomeFragment::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ur3 a(xu2 xu2) {
        ur3<xc2> ur3 = xu2.s;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ru2 b(xu2 xu2) {
        ru2 ru2 = xu2.r;
        if (ru2 != null) {
            return ru2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void G() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        ru2 ru2 = this.r;
        if (ru2 != null) {
            sb.append(ru2.h());
            local.d(str, sb.toString());
            ru2 ru22 = this.r;
            if (ru22 != null) {
                q(ru22.h());
                HomeDashboardPresenter homeDashboardPresenter = this.k;
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.m();
                } else {
                    wd4.d("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void M() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.m(childFragmentManager);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        Resources resources = getResources();
        wd4.a((Object) resources, "resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 34.0f, resources.getDisplayMetrics());
        ur3<xc2> ur3 = this.s;
        if (ur3 != null) {
            xc2 a2 = ur3.a();
            if (a2 != null) {
                int i = 0;
                View childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount() - 1;
                    if (childCount >= 0) {
                        while (true) {
                            View findViewById = bottomNavigationMenuView.getChildAt(i).findViewById(R.id.icon);
                            wd4.a((Object) findViewById, "icon");
                            findViewById.getLayoutParams().width = applyDimension;
                            findViewById.getLayoutParams().height = applyDimension;
                            if (i != childCount) {
                                i++;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final HomeDianaCustomizePresenter U0() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.l;
        if (homeDianaCustomizePresenter != null) {
            return homeDianaCustomizePresenter;
        }
        wd4.d("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final HomeHybridCustomizePresenter V0() {
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.m;
        if (homeHybridCustomizePresenter != null) {
            return homeHybridCustomizePresenter;
        }
        wd4.d("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final void W0() {
        X0();
        ru2 ru2 = this.r;
        if (ru2 != null) {
            int h = ru2.h();
            if (h == 0) {
                ur3<xc2> ur3 = this.s;
                if (ur3 != null) {
                    xc2 a2 = ur3.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        wd4.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(R.id.dashboardFragment);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            } else if (h == 1) {
                ur3<xc2> ur32 = this.s;
                if (ur32 != null) {
                    xc2 a3 = ur32.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        wd4.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(R.id.customizeFragment);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            } else if (h == 2) {
                ur3<xc2> ur33 = this.s;
                if (ur33 != null) {
                    xc2 a4 = ur33.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        wd4.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(R.id.alertsFragment);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            } else if (h == 3) {
                ur3<xc2> ur34 = this.s;
                if (ur34 != null) {
                    xc2 a5 = ur34.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        wd4.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(R.id.profileFragment);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void X0() {
        ru2 ru2 = this.r;
        if (ru2 == null) {
            return;
        }
        if (ru2 != null) {
            int h = ru2.h();
            int size = this.t.size();
            int i = 0;
            while (i < size) {
                if (this.t.get(i) instanceof ls2) {
                    Fragment fragment = this.t.get(i);
                    if (fragment != null) {
                        ((ls2) fragment).N(i == h);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i++;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void Y0() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showNoActiveDeviceFlow");
        tu2 b2 = tu2.z.b();
        Object a2 = getChildFragmentManager().a("HomeDianaCustomizeFragment");
        Object a3 = getChildFragmentManager().a("HomeHybridCustomizeFragment");
        Object a4 = getChildFragmentManager().a("HomeAlertsFragment");
        LifecycleOwner a5 = getChildFragmentManager().a("HomeAlertsHybridFragment");
        Object a6 = getChildFragmentManager().a("HomeProfileFragment");
        Object a7 = getChildFragmentManager().a("HomeUpdateFirmwareFragment");
        if (a2 == null) {
            a2 = wu2.s.a();
        }
        if (a3 == null) {
            a3 = av2.s.a();
        }
        if (a4 == null) {
            a4 = uv2.q.a();
        }
        if (a5 == null) {
            a5 = az2.o.a();
        }
        if (a6 == null) {
            a6 = fv2.p.a();
        }
        if (a7 == null) {
            a7 = ig3.n.a();
        }
        this.t.clear();
        this.t.add(b2);
        this.t.add(a3);
        this.t.add(a4);
        this.t.add(a6);
        this.t.add(a7);
        ur3<xc2> ur3 = this.s;
        if (ur3 != null) {
            xc2 a8 = ur3.a();
            if (a8 != null) {
                RecyclerViewPager recyclerViewPager = a8.r;
                wd4.a((Object) recyclerViewPager, "binding.rvTabs");
                recyclerViewPager.setAdapter(new du3(getChildFragmentManager(), this.t));
                a8.r.setItemViewCacheSize(3);
                a8.r.a((RecyclerView.p) new f());
            }
            m42 g = PortfolioApp.W.c().g();
            if (a2 != null) {
                wu2 wu2 = (wu2) a2;
                if (a3 != null) {
                    av2 av2 = (av2) a3;
                    if (a6 != null) {
                        fv2 fv2 = (fv2) a6;
                        if (a4 != null) {
                            uv2 uv2 = (uv2) a4;
                            if (a5 != null) {
                                az2 az2 = (az2) a5;
                                if (a7 != null) {
                                    g.a(new hv2(b2, wu2, av2, fv2, uv2, az2, (ig3) a7)).a(this);
                                    return;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onActivityResult " + i + ' ' + i);
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.l;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.a(i, i2, intent);
            } else {
                wd4.d("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.m;
        if (homeHybridCustomizePresenter == null) {
            return;
        }
        if (homeHybridCustomizePresenter != null) {
            homeHybridCustomizePresenter.a(i, i2, intent);
        } else {
            wd4.d("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        xc2 xc2 = (xc2) ra.a(layoutInflater, R.layout.fragment_home, viewGroup, false, O0());
        this.s = new ur3<>(this, xc2);
        wd4.a((Object) xc2, "binding");
        return xc2.d();
    }

    @DexIgnore
    public void onDestroy() {
        ru2 ru2 = this.r;
        if (ru2 != null) {
            ru2.j();
            super.onDestroy();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ru2 ru2 = this.r;
        if (ru2 != null) {
            ru2.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ru2 ru2 = this.r;
        if (ru2 != null) {
            ru2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ju3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            this.u = (ju3) a2;
            ju3 ju3 = this.u;
            if (ju3 != null) {
                ju3.c().a(activity, new b(this));
            } else {
                wd4.d("mHomeDashboardViewModel");
                throw null;
            }
        }
        ur3<xc2> ur3 = this.s;
        if (ur3 != null) {
            xc2 a3 = ur3.a();
            BottomNavigationView bottomNavigationView = a3 != null ? a3.q : null;
            if (bottomNavigationView != null) {
                wd4.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                bottomNavigationView.setItemIconTintList((ColorStateList) null);
                ur3<xc2> ur32 = this.s;
                if (ur32 != null) {
                    xc2 a4 = ur32.a();
                    BottomNavigationView bottomNavigationView2 = a4 != null ? a4.q : null;
                    if (bottomNavigationView2 != null) {
                        bottomNavigationView2.setOnNavigationItemSelectedListener(new c(this));
                        T0();
                        PortfolioApp.W.c().f().a(this, new d(this));
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "showBottomTab currentDashboardTab=" + i);
        if (i == 0) {
            ur3<xc2> ur3 = this.s;
            if (ur3 != null) {
                xc2 a2 = ur3.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    wd4.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(R.id.dashboardFragment);
                    return;
                }
                wd4.a();
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i == 1) {
            ur3<xc2> ur32 = this.s;
            if (ur32 != null) {
                xc2 a3 = ur32.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    wd4.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(R.id.customizeFragment);
                    return;
                }
                wd4.a();
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i == 2) {
            ur3<xc2> ur33 = this.s;
            if (ur33 != null) {
                xc2 a4 = ur33.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    wd4.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(R.id.alertsFragment);
                    return;
                }
                wd4.a();
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i == 3) {
            ur3<xc2> ur34 = this.s;
            if (ur34 != null) {
                xc2 a5 = ur34.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    wd4.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(R.id.profileFragment);
                    return;
                }
                wd4.a();
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void q(int i) {
        ru2 ru2 = this.r;
        if (ru2 != null) {
            boolean i2 = ru2.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "scroll to position=" + i + " fragment " + this.t.get(i) + " isOtaing " + i2);
            if (!i2) {
                ur3<xc2> ur3 = this.s;
                if (ur3 != null) {
                    xc2 a2 = ur3.a();
                    if (a2 != null) {
                        a2.r.i(i);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else if (i > 0) {
                ur3<xc2> ur32 = this.s;
                if (ur32 != null) {
                    xc2 a3 = ur32.a();
                    if (a3 != null) {
                        a3.r.i(4);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else {
                ur3<xc2> ur33 = this.s;
                if (ur33 != null) {
                    xc2 a4 = ur33.a();
                    if (a4 != null) {
                        a4.r.i(i);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            }
            X0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(InAppNotification inAppNotification) {
        ru2 ru2 = this.r;
        if (ru2 == null) {
            wd4.d("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            ru2.b(inAppNotification);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(ru2 ru2) {
        wd4.b(ru2, "presenter");
        this.r = ru2;
    }

    @DexIgnore
    public void b(int i) {
        HomeDashboardPresenter homeDashboardPresenter = this.k;
        if (homeDashboardPresenter != null) {
            homeDashboardPresenter.c(i);
        } else {
            wd4.d("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(FossilDeviceSerialPatternUtil.DEVICE device, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i);
        if (i != 1) {
            a(device);
        } else {
            Y0();
        }
        W0();
    }

    @DexIgnore
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        ru2 ru2 = this.r;
        if (ru2 != null) {
            sb.append(ru2.h());
            local.d(str, sb.toString());
            HomeDashboardPresenter homeDashboardPresenter = this.k;
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.b(z);
                ru2 ru22 = this.r;
                if (ru22 != null) {
                    q(ru22.h());
                    if (!z && isActive()) {
                        es3 es3 = es3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager, "childFragmentManager");
                        es3.J(childFragmentManager);
                        return;
                    }
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            wd4.d("mHomeDashboardPresenter");
            throw null;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(InAppNotification inAppNotification) {
        wd4.b(inAppNotification, "inAppNotification");
        if (isActive()) {
            InAppNotificationUtils inAppNotificationUtils = this.v;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                inAppNotificationUtils.a(activity, inAppNotification);
            } else {
                wd4.a();
                throw null;
            }
        }
        b(inAppNotification);
    }

    @DexIgnore
    public final void a(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showMainFlow");
        tu2 b2 = tu2.z.b();
        Object a2 = getChildFragmentManager().a("HomeDianaCustomizeFragment");
        Object a3 = getChildFragmentManager().a("HomeHybridCustomizeFragment");
        Object a4 = getChildFragmentManager().a("HomeAlertsFragment");
        Object a5 = getChildFragmentManager().a("HomeAlertsHybridFragment");
        Object a6 = getChildFragmentManager().a("HomeProfileFragment");
        Object a7 = getChildFragmentManager().a("HomeUpdateFirmwareFragment");
        if (a2 == null) {
            a2 = wu2.s.a();
        }
        if (a3 == null) {
            a3 = av2.s.a();
        }
        if (a4 == null) {
            a4 = uv2.q.a();
        }
        if (a5 == null) {
            a5 = az2.o.a();
        }
        if (a6 == null) {
            a6 = fv2.p.a();
        }
        if (a7 == null) {
            a7 = ig3.n.a();
        }
        this.t.clear();
        this.t.add(b2);
        if (device != null && yu2.a[device.ordinal()] == 1) {
            this.t.add(a2);
            this.t.add(a4);
        } else {
            this.t.add(a3);
            this.t.add(a5);
        }
        this.t.add(a6);
        this.t.add(a7);
        ur3<xc2> ur3 = this.s;
        if (ur3 != null) {
            xc2 a8 = ur3.a();
            if (a8 != null) {
                RecyclerViewPager recyclerViewPager = a8.r;
                wd4.a((Object) recyclerViewPager, "binding.rvTabs");
                recyclerViewPager.setAdapter(new du3(getChildFragmentManager(), this.t));
                a8.r.setItemViewCacheSize(3);
                a8.r.a((RecyclerView.p) new e());
            }
            m42 g = PortfolioApp.W.c().g();
            if (a2 != null) {
                wu2 wu2 = (wu2) a2;
                if (a3 != null) {
                    av2 av2 = (av2) a3;
                    if (a6 != null) {
                        fv2 fv2 = (fv2) a6;
                        if (a4 != null) {
                            uv2 uv2 = (uv2) a4;
                            if (a5 != null) {
                                az2 az2 = (az2) a5;
                                if (a7 != null) {
                                    g.a(new hv2(b2, wu2, av2, fv2, uv2, az2, (ig3) a7)).a(this);
                                    return;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wd4.b(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i == R.id.tv_ok) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.D;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        wd4.a((Object) activity, "activity!!");
                        aVar.a(activity, false);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Cancel Zendesk feedback");
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Zendesk feedback");
                    ru2 ru2 = this.r;
                    if (ru2 != null) {
                        ru2.b("Feedback - From app [Fossil] - [Android]");
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Send Zendesk feedback");
                    es3 es3 = es3.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    wd4.a((Object) childFragmentManager, "childFragmentManager");
                    es3.i(childFragmentManager);
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Open app rating dialog");
                    es3 es32 = es3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wd4.a((Object) childFragmentManager2, "childFragmentManager");
                    es32.d(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Stay in app");
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Play Store");
                    PortfolioApp c2 = PortfolioApp.W.c();
                    AppHelper.Companion companion = AppHelper.f;
                    String packageName = c2.getPackageName();
                    wd4.a((Object) packageName, "packageName");
                    companion.b(c2, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public void a(Intent intent) {
        wd4.b(intent, "intent");
        HomeDashboardPresenter homeDashboardPresenter = this.k;
        if (homeDashboardPresenter != null) {
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.a(intent);
            } else {
                wd4.d("mHomeDashboardPresenter");
                throw null;
            }
        }
        HomeProfilePresenter homeProfilePresenter = this.n;
        if (homeProfilePresenter == null) {
            return;
        }
        if (homeProfilePresenter != null) {
            homeProfilePresenter.b(intent);
        } else {
            wd4.d("mHomeProfilePresenter");
            throw null;
        }
    }
}
