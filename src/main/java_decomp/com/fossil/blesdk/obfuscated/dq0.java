package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dq0 implements Parcelable.Creator<cq0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                iBinder = SafeParcelReader.p(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new cq0(iBinder);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new cq0[i];
    }
}
