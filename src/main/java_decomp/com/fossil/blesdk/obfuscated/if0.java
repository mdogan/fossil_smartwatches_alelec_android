package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class if0<A extends ee0.b, L> {
    @DexIgnore
    public /* final */ af0.a<L> a;

    @DexIgnore
    public if0(af0.a<L> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public af0.a<L> a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(A a2, yn1<Boolean> yn1) throws RemoteException;
}
