package com.fossil.blesdk.obfuscated;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class vy1 implements ax1 {
    @DexIgnore
    public /* final */ FirebaseInstanceId.a a;

    @DexIgnore
    public vy1(FirebaseInstanceId.a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public final void a(zw1 zw1) {
        FirebaseInstanceId.a aVar = this.a;
        synchronized (aVar) {
            if (aVar.a()) {
                FirebaseInstanceId.this.e();
            }
        }
    }
}
