package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.provider.HourNotificationProviderImp;
import com.portfolio.platform.provider.MicroAppSettingProviderImp;
import com.portfolio.platform.provider.UserProviderImp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class en2 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static en2 o;
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ContactProvider a;
    @DexIgnore
    public LocationProvider b;
    @DexIgnore
    public AppFilterProvider c;
    @DexIgnore
    public DeviceProvider d;
    @DexIgnore
    public xn2 e;
    @DexIgnore
    public MFSleepSessionProvider f;
    @DexIgnore
    public un2 g;
    @DexIgnore
    public ao2 h;
    @DexIgnore
    public vn2 i;
    @DexIgnore
    public eo2 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public yn2 l;
    @DexIgnore
    public co2 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(en2 en2) {
            en2.o = en2;
        }

        @DexIgnore
        public final en2 b() {
            return en2.o;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final en2 a() {
            if (b() == null) {
                a(new en2((rd4) null));
            }
            en2 b = b();
            if (b != null) {
                return b;
            }
            wd4.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = en2.class.getSimpleName();
        wd4.a((Object) simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public en2() {
    }

    @DexIgnore
    public final synchronized ContactProvider b() {
        ContactProvider contactProvider;
        String c2 = c();
        if (this.a == null) {
            this.a = new sn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + ContactProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + ContactProviderImpl.DB_NAME;
            ContactProvider contactProvider2 = this.a;
            if (contactProvider2 != null) {
                String dbPath = contactProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.a = new sn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        contactProvider = this.a;
        if (contactProvider == null) {
            wd4.a();
            throw null;
        }
        return contactProvider;
    }

    @DexIgnore
    public final String c() {
        eo2 eo2 = this.j;
        if (eo2 == null) {
            return "Anonymous";
        }
        if (eo2 != null) {
            MFUser b2 = eo2.b();
            if (b2 == null || TextUtils.isEmpty(b2.getUserId())) {
                return "Anonymous";
            }
            String userId = b2.getUserId();
            wd4.a((Object) userId, "user.userId");
            return userId;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized DeviceProvider d() {
        DeviceProvider deviceProvider;
        DeviceProviderImp deviceProviderImp;
        String c2 = c();
        FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + c2);
        if (this.d == null) {
            this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + DeviceProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + DeviceProviderImp.DB_NAME;
            try {
                DeviceProvider deviceProvider2 = this.d;
                if (deviceProvider2 != null) {
                    String dbPath = deviceProvider2.getDbPath();
                    if (TextUtils.isEmpty(dbPath) || (!wd4.a((Object) str, (Object) dbPath))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                        this.d = deviceProviderImp;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                    if (TextUtils.isEmpty((CharSequence) null) || (!wd4.a((Object) str, (Object) null))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (TextUtils.isEmpty((CharSequence) null) || (!wd4.a((Object) str, (Object) null))) {
                        this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            wd4.a();
            throw null;
        }
        return deviceProvider;
    }

    @DexIgnore
    public final synchronized vn2 e() {
        vn2 vn2;
        String c2 = c();
        if (this.i == null) {
            this.i = new wn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "firmwares.db");
        } else {
            String str = c2 + "_" + "firmwares.db";
            vn2 vn22 = this.i;
            if (vn22 != null) {
                String dbPath = vn22.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.i = new wn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        vn2 = this.i;
        if (vn2 == null) {
            wd4.a();
            throw null;
        }
        return vn2;
    }

    @DexIgnore
    public final synchronized un2 f() {
        un2 un2;
        if (this.g == null) {
            String c2 = c();
            this.g = new HourNotificationProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "hourNotification.db");
        }
        un2 = this.g;
        if (un2 == null) {
            wd4.a();
            throw null;
        }
        return un2;
    }

    @DexIgnore
    public final synchronized LocationProvider g() {
        LocationProvider locationProvider;
        String c2 = c();
        if (this.b == null) {
            this.b = new LocationProviderImpl(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + LocationProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + LocationProviderImpl.DB_NAME;
            LocationProvider locationProvider2 = this.b;
            if (locationProvider2 != null) {
                String dbPath = locationProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.b = new LocationProviderImpl(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        locationProvider = this.b;
        if (locationProvider == null) {
            wd4.a();
            throw null;
        }
        return locationProvider;
    }

    @DexIgnore
    public final synchronized xn2 h() {
        xn2 xn2;
        String c2 = c();
        if (this.e == null) {
            this.e = new MicroAppSettingProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "microAppSetting.db");
        } else {
            String str = c2 + "_" + "microAppSetting.db";
            xn2 xn22 = this.e;
            if (xn22 != null) {
                String dbPath = xn22.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.e = new MicroAppSettingProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        xn2 = this.e;
        if (xn2 == null) {
            wd4.a();
            throw null;
        }
        return xn2;
    }

    @DexIgnore
    public final yn2 i() {
        if (this.l == null) {
            this.l = new zn2(PortfolioApp.W.c().getApplicationContext(), "phone_favorites_contact.db");
        }
        yn2 yn2 = this.l;
        if (yn2 != null) {
            return yn2;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized ao2 j() {
        ao2 ao2;
        bo2 bo2;
        String c2 = c();
        if (this.h == null) {
            this.h = new bo2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "pin.db");
        } else {
            String str = c2 + "_" + "pin.db";
            try {
                ao2 ao22 = this.h;
                if (ao22 != null) {
                    String dbPath = ao22.getDbPath();
                    if (kq4.a(dbPath) || (!wd4.a((Object) str, (Object) dbPath))) {
                        bo2 = new bo2(PortfolioApp.W.c().getApplicationContext(), str);
                        this.h = bo2;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                    if (kq4.a((String) null) || (!wd4.a((Object) str, (Object) null))) {
                        bo2 = new bo2(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (kq4.a((String) null) || (!wd4.a((Object) str, (Object) null))) {
                        this.h = new bo2(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        ao2 = this.h;
        if (ao2 == null) {
            wd4.a();
            throw null;
        }
        return ao2;
    }

    @DexIgnore
    public final synchronized SecondTimezoneProvider k() {
        SecondTimezoneProvider secondTimezoneProvider;
        String c2 = c();
        if (this.k == null) {
            this.k = new SecondTimezoneProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + SecondTimezoneProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + SecondTimezoneProviderImp.DB_NAME;
            SecondTimezoneProvider secondTimezoneProvider2 = this.k;
            if (secondTimezoneProvider2 != null) {
                String dbPath = secondTimezoneProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.k = new SecondTimezoneProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        secondTimezoneProvider = this.k;
        if (secondTimezoneProvider == null) {
            wd4.a();
            throw null;
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    public final co2 l() {
        if (this.m == null) {
            synchronized (en2.class) {
                if (this.m == null) {
                    String str = c() + "_" + "serverSetting.db";
                    FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                    Context applicationContext = PortfolioApp.W.c().getApplicationContext();
                    wd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    this.m = new do2(applicationContext, str);
                }
                cb4 cb4 = cb4.a;
            }
        }
        co2 co2 = this.m;
        if (co2 != null) {
            return co2;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized MFSleepSessionProvider m() {
        MFSleepSessionProvider mFSleepSessionProvider;
        String c2 = c();
        if (this.f == null) {
            this.f = new MFSleepSessionProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + MFSleepSessionProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + MFSleepSessionProviderImp.DB_NAME;
            MFSleepSessionProvider mFSleepSessionProvider2 = this.f;
            if (mFSleepSessionProvider2 != null) {
                String dbPath = mFSleepSessionProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.f = new MFSleepSessionProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        mFSleepSessionProvider = this.f;
        if (mFSleepSessionProvider == null) {
            wd4.a();
            throw null;
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    public final eo2 n() {
        if (this.j == null) {
            Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            wd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            this.j = new UserProviderImp(applicationContext, "user.db");
        }
        eo2 eo2 = this.j;
        if (eo2 != null) {
            return eo2;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void o() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.f = null;
        this.h = null;
        this.d = null;
        this.g = null;
        this.j = null;
        this.e = null;
        this.m = null;
    }

    @DexIgnore
    public /* synthetic */ en2(rd4 rd4) {
        this();
    }

    @DexIgnore
    public final synchronized AppFilterProvider a() {
        AppFilterProvider appFilterProvider;
        String c2 = c();
        if (this.c == null) {
            this.c = new rn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + AppFilterProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + AppFilterProviderImpl.DB_NAME;
            AppFilterProvider appFilterProvider2 = this.c;
            if (appFilterProvider2 != null) {
                String dbPath = appFilterProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!wd4.a((Object) str, (Object) dbPath))) {
                    this.c = new rn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        appFilterProvider = this.c;
        if (appFilterProvider == null) {
            wd4.a();
            throw null;
        }
        return appFilterProvider;
    }

    @DexIgnore
    public final synchronized DeviceProvider a(String str) {
        DeviceProvider deviceProvider;
        wd4.b(str, ButtonService.USER_ID);
        if (this.d == null) {
            String str2 = str + "_" + DeviceProviderImp.DB_NAME;
            FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
            this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str2);
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            wd4.a();
            throw null;
        }
        return deviceProvider;
    }
}
