package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nf0 extends ci0 {
    @DexIgnore
    public /* final */ h4<zh0<?>> j; // = new h4<>();
    @DexIgnore
    public we0 k;

    @DexIgnore
    public nf0(ze0 ze0) {
        super(ze0);
        this.e.a("ConnectionlessLifecycleHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static void a(Activity activity, we0 we0, zh0<?> zh0) {
        ze0 a = LifecycleCallback.a(activity);
        nf0 nf0 = (nf0) a.a("ConnectionlessLifecycleHelper", nf0.class);
        if (nf0 == null) {
            nf0 = new nf0(a);
        }
        nf0.k = we0;
        ck0.a(zh0, (Object) "ApiKey cannot be null");
        nf0.j.add(zh0);
        we0.a(nf0);
    }

    @DexIgnore
    public void c() {
        super.c();
        i();
    }

    @DexIgnore
    public void d() {
        super.d();
        i();
    }

    @DexIgnore
    public void e() {
        super.e();
        this.k.b(this);
    }

    @DexIgnore
    public final void f() {
        this.k.c();
    }

    @DexIgnore
    public final h4<zh0<?>> h() {
        return this.j;
    }

    @DexIgnore
    public final void i() {
        if (!this.j.isEmpty()) {
            this.k.a(this);
        }
    }

    @DexIgnore
    public final void a(vd0 vd0, int i) {
        this.k.a(vd0, i);
    }
}
