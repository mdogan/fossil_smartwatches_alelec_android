package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.message.BasicHeader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class aq4 implements HttpEntity {
    @DexIgnore
    public static /* final */ char[] i; // = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    @DexIgnore
    public /* final */ xp4 e;
    @DexIgnore
    public /* final */ Header f;
    @DexIgnore
    public long g;
    @DexIgnore
    public volatile boolean h;

    @DexIgnore
    public aq4(HttpMultipartMode httpMultipartMode, String str, Charset charset) {
        str = str == null ? a() : str;
        this.e = new xp4("form-data", charset, str, httpMultipartMode == null ? HttpMultipartMode.STRICT : httpMultipartMode);
        this.f = new BasicHeader(GraphRequest.CONTENT_TYPE_HEADER, a(str, charset));
        this.h = true;
    }

    @DexIgnore
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        sb.append(str);
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    @DexIgnore
    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    @DexIgnore
    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    @DexIgnore
    public Header getContentEncoding() {
        return null;
    }

    @DexIgnore
    public long getContentLength() {
        if (this.h) {
            this.g = this.e.c();
            this.h = false;
        }
        return this.g;
    }

    @DexIgnore
    public Header getContentType() {
        return this.f;
    }

    @DexIgnore
    public boolean isChunked() {
        return !isRepeatable();
    }

    @DexIgnore
    public boolean isRepeatable() {
        for (vp4 a : this.e.a()) {
            if (a.a().getContentLength() < 0) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isStreaming() {
        return !isRepeatable();
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        this.e.a(outputStream);
    }

    @DexIgnore
    public aq4() {
        this(HttpMultipartMode.STRICT, (String) null, (Charset) null);
    }

    @DexIgnore
    public String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i2 = 0; i2 < nextInt; i2++) {
            char[] cArr = i;
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    @DexIgnore
    public void a(vp4 vp4) {
        this.e.a(vp4);
        this.h = true;
    }

    @DexIgnore
    public void a(String str, cq4 cq4) {
        a(new vp4(str, cq4));
    }
}
