package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.SupportMapFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zf1 extends ue1 {
    @DexIgnore
    public /* final */ /* synthetic */ ee1 e;

    @DexIgnore
    public zf1(SupportMapFragment.a aVar, ee1 ee1) {
        this.e = ee1;
    }

    @DexIgnore
    public final void a(ke1 ke1) throws RemoteException {
        this.e.a(new ce1(ke1));
    }
}
