package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.nd;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yd<K, A, B> extends nd<K, B> {
    @DexIgnore
    public /* final */ nd<K, A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends nd.c<A> {
        @DexIgnore
        public /* final */ /* synthetic */ nd.c a;

        @DexIgnore
        public a(nd.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(yd.this.a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends nd.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ nd.a a;

        @DexIgnore
        public b(nd.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(yd.this.a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends nd.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ nd.a a;

        @DexIgnore
        public c(nd.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(yd.this.a(list));
        }
    }

    @DexIgnore
    public yd(nd<K, A> ndVar, m3<List<A>, List<B>> m3Var) {
        this.a = ndVar;
        this.b = m3Var;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = md.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    public void addInvalidatedCallback(md.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(nd.f<K> fVar, nd.a<B> aVar) {
        this.a.loadAfter(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadBefore(nd.f<K> fVar, nd.a<B> aVar) {
        this.a.loadBefore(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadInitial(nd.e<K> eVar, nd.c<B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(md.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
