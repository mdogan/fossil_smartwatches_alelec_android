package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gn1 implements Parcelable.Creator<fn1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        dk0 dk0 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                dk0 = (dk0) SafeParcelReader.a(parcel, a, dk0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new fn1(i, dk0);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new fn1[i];
    }
}
