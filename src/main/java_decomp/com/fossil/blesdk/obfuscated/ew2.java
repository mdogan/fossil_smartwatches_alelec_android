package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.pt2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ew2 extends bs2 implements dw2, xs3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public cw2 k;
    @DexIgnore
    public ur3<rd2> l;
    @DexIgnore
    public jx2 m;
    @DexIgnore
    public pt2 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ew2.p;
        }

        @DexIgnore
        public final ew2 b() {
            return new ew2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements pt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ ew2 a;

        @DexIgnore
        public b(ew2 ew2) {
            this.a = ew2;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            wd4.b(appWrapper, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ew2.q.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            ew2.b(this.a).a(appWrapper, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ew2 e;

        @DexIgnore
        public c(ew2 ew2) {
            this.e = ew2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(ew2.q.a(), "press on close button");
            ew2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ew2 a;

        @DexIgnore
        public d(ew2 ew2) {
            this.a = ew2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ew2.b(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ew2 e;
        @DexIgnore
        public /* final */ /* synthetic */ rd2 f;

        @DexIgnore
        public e(ew2 ew2, rd2 rd2) {
            this.e = ew2;
            this.f = rd2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.r;
            wd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            ew2.a(this.e).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rd2 e;

        @DexIgnore
        public f(rd2 rd2) {
            this.e = rd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = ew2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationAppsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pt2 a(ew2 ew2) {
        pt2 pt2 = ew2.n;
        if (pt2 != null) {
            return pt2;
        }
        wd4.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ cw2 b(ew2 ew2) {
        cw2 cw2 = ew2.k;
        if (cw2 != null) {
            return cw2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d(p, "onActivityBackPressed -");
        cw2 cw2 = this.k;
        if (cw2 != null) {
            cw2.h();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void g(List<AppWrapper> list) {
        wd4.b(list, "listAppWrapper");
        pt2 pt2 = this.n;
        if (pt2 != null) {
            pt2.a(list);
        } else {
            wd4.d("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void h(boolean z) {
        ur3<rd2> ur3 = this.l;
        if (ur3 != null) {
            rd2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.u;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) context, "context!!");
                aVar.a(context, PortfolioApp.W.c().e());
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        rd2 rd2 = (rd2) ra.a(layoutInflater, R.layout.fragment_notification_apps, viewGroup, false, O0());
        rd2.s.setOnClickListener(new c(this));
        rd2.u.setOnCheckedChangeListener(new d(this));
        rd2.q.addTextChangedListener(new e(this, rd2));
        rd2.r.setOnClickListener(new f(rd2));
        pt2 pt2 = new pt2();
        pt2.a((pt2.b) new b(this));
        this.n = pt2;
        this.m = (jx2) getChildFragmentManager().a(jx2.t.a());
        if (this.m == null) {
            this.m = jx2.t.b();
        }
        RecyclerView recyclerView = rd2.t;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        pt2 pt22 = this.n;
        if (pt22 != null) {
            recyclerView.setAdapter(pt22);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((le) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                m42 g = PortfolioApp.W.c().g();
                jx2 jx2 = this.m;
                if (jx2 != null) {
                    g.a(new lx2(jx2)).a(this);
                    this.l = new ur3<>(this, rd2);
                    R("app_notification_view");
                    wd4.a((Object) rd2, "binding");
                    return rd2.d();
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        wd4.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        cw2 cw2 = this.k;
        if (cw2 != null) {
            cw2.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        cw2 cw2 = this.k;
        if (cw2 != null) {
            cw2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(cw2 cw2) {
        wd4.b(cw2, "presenter");
        this.k = cw2;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i == R.id.tv_ok) {
                cw2 cw2 = this.k;
                if (cw2 != null) {
                    cw2.i();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i == R.id.fb_try_again) {
                cw2 cw22 = this.k;
                if (cw22 != null) {
                    cw22.h();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (i != R.id.ftv_contact_cs) {
                if (i == R.id.iv_close) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.C;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    wd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }
}
