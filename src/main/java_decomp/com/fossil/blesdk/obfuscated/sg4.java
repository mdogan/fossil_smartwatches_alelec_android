package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sg4 extends zg4 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(sg4.class, "_resumed");
    @DexIgnore
    public volatile int _resumed;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public sg4(kc4<?> kc4, Throwable th, boolean z) {
        super(th, z);
        wd4.b(kc4, "continuation");
        if (th == null) {
            th = new CancellationException("Continuation " + kc4 + " was cancelled normally");
        }
        this._resumed = 0;
    }

    @DexIgnore
    public final boolean c() {
        return c.compareAndSet(this, 0, 1);
    }
}
