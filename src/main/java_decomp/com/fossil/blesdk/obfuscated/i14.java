package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ int f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ h14 i;

    @DexIgnore
    public i14(h14 h14, List list, int i2, boolean z, boolean z2) {
        this.i = h14;
        this.e = list;
        this.f = i2;
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public void run() {
        this.i.a((List<r14>) this.e, this.f, this.g);
        if (this.h) {
            this.e.clear();
        }
    }
}
