package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p32 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public p32(int i) {
        this.a = new byte[i];
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a[i] = z ? (byte) 1 : 0;
    }

    @DexIgnore
    public void a(boolean z, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.b;
            this.b = i3 + 1;
            a(i3, z);
        }
    }

    @DexIgnore
    public byte[] a(int i) {
        byte[] bArr = new byte[(this.a.length * i)];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = this.a[i2 / i];
        }
        return bArr;
    }
}
