package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dg0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ tf0 e;

    @DexIgnore
    public dg0(tf0 tf0) {
        this.e = tf0;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.e.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.e.b.unlock();
            }
        } catch (RuntimeException e2) {
            this.e.a.a(e2);
        } finally {
            this.e.b.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ dg0(tf0 tf0, uf0 uf0) {
        this(tf0);
    }
}
