package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.ao;
import com.fossil.blesdk.obfuscated.uq;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yq implements uq {
    @DexIgnore
    public /* final */ dr a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ wq d; // = new wq();
    @DexIgnore
    public ao e;

    @DexIgnore
    @Deprecated
    public yq(File file, long j) {
        this.b = file;
        this.c = j;
        this.a = new dr();
    }

    @DexIgnore
    public static uq a(File file, long j) {
        return new yq(file, j);
    }

    @DexIgnore
    public final synchronized ao a() throws IOException {
        if (this.e == null) {
            this.e = ao.a(this.b, 1, 1, this.c);
        }
        return this.e;
    }

    @DexIgnore
    public File a(ko koVar) {
        String b2 = this.a.b(koVar);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b2 + " for for Key: " + koVar);
        }
        try {
            ao.e f = a().f(b2);
            if (f != null) {
                return f.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @DexIgnore
    public void a(ko koVar, uq.b bVar) {
        ao.c e2;
        String b2 = this.a.b(koVar);
        this.d.a(b2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b2 + " for for Key: " + koVar);
            }
            try {
                ao a2 = a();
                if (a2.f(b2) == null) {
                    e2 = a2.e(b2);
                    if (e2 != null) {
                        if (bVar.a(e2.a(0))) {
                            e2.c();
                        }
                        e2.b();
                        this.d.b(b2);
                        return;
                    }
                    throw new IllegalStateException("Had two simultaneous puts for: " + b2);
                }
            } catch (IOException e3) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e3);
                }
            } catch (Throwable th) {
                e2.b();
                throw th;
            }
        } finally {
            this.d.b(b2);
        }
    }
}
