package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sd3 implements Factory<SleepOverviewMonthPresenter> {
    @DexIgnore
    public static SleepOverviewMonthPresenter a(qd3 qd3, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository) {
        return new SleepOverviewMonthPresenter(qd3, userRepository, sleepSummariesRepository);
    }
}
