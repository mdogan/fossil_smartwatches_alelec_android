package com.fossil.blesdk.obfuscated;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c12 {
    /*
    static {
        Logger.getLogger(c12.class.getName());
        Collections.synchronizedMap(new HashMap());
        Collections.synchronizedMap(new HashMap());
        z02.a();
        f12.a();
    }
    */

    @DexIgnore
    public static k12 a(ObjectInputStream objectInputStream, int i) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[i];
        while (true) {
            int read = objectInputStream.read(bArr, 0, i);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return k12.a(byteArrayOutputStream.toByteArray());
            }
        }
    }
}
