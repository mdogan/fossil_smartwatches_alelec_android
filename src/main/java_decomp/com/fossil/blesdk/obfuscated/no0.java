package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class no0 extends zy0 implements mo0 {
    @DexIgnore
    public no0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    public final tn0 a(tn0 tn0, String str, int i) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        o.writeInt(i);
        Parcel a = a(2, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int b(tn0 tn0, String str, boolean z) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        bz0.a(o, z);
        Parcel a = a(3, o);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int n() throws RemoteException {
        Parcel a = a(6, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int a(tn0 tn0, String str, boolean z) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        bz0.a(o, z);
        Parcel a = a(5, o);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final tn0 b(tn0 tn0, String str, int i) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        o.writeInt(i);
        Parcel a = a(4, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
