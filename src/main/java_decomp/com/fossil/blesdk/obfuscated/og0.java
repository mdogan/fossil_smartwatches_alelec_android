package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class og0 implements ch0, ii0 {
    @DexIgnore
    public /* final */ Lock e;
    @DexIgnore
    public /* final */ Condition f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ zd0 h;
    @DexIgnore
    public /* final */ qg0 i;
    @DexIgnore
    public /* final */ Map<ee0.c<?>, ee0.f> j;
    @DexIgnore
    public /* final */ Map<ee0.c<?>, vd0> k; // = new HashMap();
    @DexIgnore
    public /* final */ lj0 l;
    @DexIgnore
    public /* final */ Map<ee0<?>, Boolean> m;
    @DexIgnore
    public /* final */ ee0.a<? extends mn1, wm1> n;
    @DexIgnore
    public volatile ng0 o;
    @DexIgnore
    public vd0 p; // = null;
    @DexIgnore
    public int q;
    @DexIgnore
    public /* final */ fg0 r;
    @DexIgnore
    public /* final */ dh0 s;

    @DexIgnore
    public og0(Context context, fg0 fg0, Lock lock, Looper looper, zd0 zd0, Map<ee0.c<?>, ee0.f> map, lj0 lj0, Map<ee0<?>, Boolean> map2, ee0.a<? extends mn1, wm1> aVar, ArrayList<hi0> arrayList, dh0 dh0) {
        this.g = context;
        this.e = lock;
        this.h = zd0;
        this.j = map;
        this.l = lj0;
        this.m = map2;
        this.n = aVar;
        this.r = fg0;
        this.s = dh0;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            hi0 hi0 = arrayList.get(i2);
            i2++;
            hi0.a((ii0) this);
        }
        this.i = new qg0(this, looper);
        this.f = lock.newCondition();
        this.o = new eg0(this);
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t) {
        t.g();
        return this.o.a(t);
    }

    @DexIgnore
    public final boolean a(df0 df0) {
        return false;
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t) {
        t.g();
        return this.o.b(t);
    }

    @DexIgnore
    public final boolean c() {
        return this.o instanceof qf0;
    }

    @DexIgnore
    public final void d() {
        if (c()) {
            ((qf0) this.o).d();
        }
    }

    @DexIgnore
    public final void e() {
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.e.lock();
        try {
            this.o.e(bundle);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final vd0 f() {
        b();
        while (g()) {
            try {
                this.f.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new vd0(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return vd0.i;
        }
        vd0 vd0 = this.p;
        if (vd0 != null) {
            return vd0;
        }
        return new vd0(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        return this.o instanceof tf0;
    }

    @DexIgnore
    public final void h() {
        this.e.lock();
        try {
            this.o = new tf0(this, this.l, this.m, this.h, this.n, this.e, this.g);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        this.e.lock();
        try {
            this.r.o();
            this.o = new qf0(this);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a() {
        if (this.o.a()) {
            this.k.clear();
        }
    }

    @DexIgnore
    public final void b() {
        this.o.b();
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        this.e.lock();
        try {
            this.p = vd0;
            this.o = new eg0(this);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void f(int i2) {
        this.e.lock();
        try {
            this.o.f(i2);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a(vd0 vd0, ee0<?> ee0, boolean z) {
        this.e.lock();
        try {
            this.o.a(vd0, ee0, z);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a(pg0 pg0) {
        this.i.sendMessage(this.i.obtainMessage(1, pg0));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.i.sendMessage(this.i.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append(str).append("mState=").println(this.o);
        for (ee0 next : this.m.keySet()) {
            printWriter.append(str).append(next.b()).println(":");
            this.j.get(next.a()).a(concat, fileDescriptor, printWriter, strArr);
        }
    }
}
