package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ei3 extends as2 implements ri3, View.OnClickListener, xs3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<hb2> j;
    @DexIgnore
    public qi3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ei3 a() {
            return new ei3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ei3 e;

        @DexIgnore
        public b(ei3 ei3) {
            this.e = ei3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ei3 e;

        @DexIgnore
        public c(ei3 ei3) {
            this.e = ei3;
        }

        @DexIgnore
        public final void onClick(View view) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = this.e.getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.f(childFragmentManager);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ei3 e;

        @DexIgnore
        public d(ei3 ei3) {
            this.e = ei3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().i();
            this.e.T0().a("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = ei3.class.getSimpleName();
        wd4.a((Object) simpleName, "DeleteAccountFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void G0() {
        P0().c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(m, "deleteUser - successfully");
                WelcomeActivity.C.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final qi3 T0() {
        qi3 qi3 = this.k;
        if (qi3 != null) {
            return qi3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wd4.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1000) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            qi3 qi3 = this.k;
            if (qi3 != null) {
                qi3.h();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wd4.b(view, "v");
        if (view.getId() == R.id.aciv_back) {
            n();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        hb2 hb2 = (hb2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_delete_account, (ViewGroup) null, false, O0());
        this.j = new ur3<>(this, hb2);
        wd4.a((Object) hb2, "binding");
        return hb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        qi3 qi3 = this.k;
        if (qi3 != null) {
            qi3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        qi3 qi3 = this.k;
        if (qi3 != null) {
            qi3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<hb2> ur3 = this.j;
        if (ur3 != null) {
            hb2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                String a3 = cg4.a(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_Text__IfYouWantToExportYour).toString(), "contact_our_support_team", "", false, 4, (Object) null);
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "binding.tvDescription");
                flexibleTextView.setText(Html.fromHtml(a3));
                a2.s.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, String str) {
        wd4.b(str, "message");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(qi3 qi3) {
        wd4.b(qi3, "presenter");
        tt1.a(qi3);
        wd4.a((Object) qi3, "Preconditions.checkNotNull(presenter)");
        this.k = qi3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((BaseActivity) activity).a(str, i, intent);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i == R.id.tv_ok) {
                qi3 qi3 = this.k;
                if (qi3 != null) {
                    MFUser b2 = en2.p.a().n().b();
                    wd4.a((Object) b2, "ProviderManager.getInsta\u2026serProvider().currentUser");
                    qi3.a(b2);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
        }
    }
}
