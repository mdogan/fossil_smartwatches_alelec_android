package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class po0 extends zy0 implements oo0 {
    @DexIgnore
    public po0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    public final tn0 a(tn0 tn0, String str, int i, tn0 tn02) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        o.writeInt(i);
        bz0.a(o, (IInterface) tn02);
        Parcel a = a(2, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final tn0 b(tn0 tn0, String str, int i, tn0 tn02) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (IInterface) tn0);
        o.writeString(str);
        o.writeInt(i);
        bz0.a(o, (IInterface) tn02);
        Parcel a = a(3, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
