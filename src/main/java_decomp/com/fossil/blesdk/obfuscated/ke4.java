package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ke4 extends ie4 implements he4<Integer> {
    @DexIgnore
    public static /* final */ ke4 i; // = new ke4(1, 0);
    @DexIgnore
    public static /* final */ a j; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ke4 a() {
            return ke4.i;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public ke4(int i2, int i3) {
        super(i2, i3, 1);
    }

    @DexIgnore
    public Integer d() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public Integer e() {
        return Integer.valueOf(a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ke4) {
            if (!isEmpty() || !((ke4) obj).isEmpty()) {
                ke4 ke4 = (ke4) obj;
                if (!(a() == ke4.a() && b() == ke4.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
