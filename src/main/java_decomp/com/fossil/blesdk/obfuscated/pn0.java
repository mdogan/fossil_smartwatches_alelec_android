package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.facebook.internal.Utility;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pn0 {
    @DexIgnore
    public static /* final */ pn0 d; // = new pn0(true, (String) null, (Throwable) null);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Throwable c;

    @DexIgnore
    public pn0(boolean z, String str, Throwable th) {
        this.a = z;
        this.b = str;
        this.c = th;
    }

    @DexIgnore
    public static pn0 a(Callable<String> callable) {
        return new rn0(callable);
    }

    @DexIgnore
    public static pn0 c() {
        return d;
    }

    @DexIgnore
    public final void b() {
        if (!this.a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.c != null) {
                Log.d("GoogleCertificatesRslt", a(), this.c);
            } else {
                Log.d("GoogleCertificatesRslt", a());
            }
        }
    }

    @DexIgnore
    public static pn0 a(String str) {
        return new pn0(false, str, (Throwable) null);
    }

    @DexIgnore
    public static pn0 a(String str, Throwable th) {
        return new pn0(false, str, th);
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public static String a(String str, hn0 hn0, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", new Object[]{z2 ? "debug cert rejected" : "not whitelisted", str, nm0.a(fm0.a(Utility.HASH_ALGORITHM_SHA1).digest(hn0.o())), Boolean.valueOf(z), "12451009.false"});
    }
}
