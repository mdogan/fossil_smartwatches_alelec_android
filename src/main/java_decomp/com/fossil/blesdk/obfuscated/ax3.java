package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.Protocol;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ax3 {
    @DexIgnore
    public static String a(iv3 iv3, Proxy.Type type, Protocol protocol) {
        StringBuilder sb = new StringBuilder();
        sb.append(iv3.f());
        sb.append(' ');
        if (a(iv3, type)) {
            sb.append(iv3.d());
        } else {
            sb.append(a(iv3.d()));
        }
        sb.append(' ');
        sb.append(a(protocol));
        return sb.toString();
    }

    @DexIgnore
    public static boolean a(iv3 iv3, Proxy.Type type) {
        return !iv3.e() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(ev3 ev3) {
        String b = ev3.b();
        String d = ev3.d();
        if (d == null) {
            return b;
        }
        return b + '?' + d;
    }

    @DexIgnore
    public static String a(Protocol protocol) {
        return protocol == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1";
    }
}
