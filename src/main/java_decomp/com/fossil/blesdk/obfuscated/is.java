package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class is implements tr<URL, InputStream> {
    @DexIgnore
    public /* final */ tr<mr, InputStream> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<URL, InputStream> {
        @DexIgnore
        public tr<URL, InputStream> a(xr xrVar) {
            return new is(xrVar.a(mr.class, InputStream.class));
        }
    }

    @DexIgnore
    public is(tr<mr, InputStream> trVar) {
        this.a = trVar;
    }

    @DexIgnore
    public boolean a(URL url) {
        return true;
    }

    @DexIgnore
    public tr.a<InputStream> a(URL url, int i, int i2, mo moVar) {
        return this.a.a(new mr(url), i, i2, moVar);
    }
}
