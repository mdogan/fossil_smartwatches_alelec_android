package com.fossil.blesdk.obfuscated;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.decoder.Mode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b42 {
    @DexIgnore
    public Mode a;
    @DexIgnore
    public ErrorCorrectionLevel b;
    @DexIgnore
    public v32 c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public x32 e;

    @DexIgnore
    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    @DexIgnore
    public x32 a() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }

    @DexIgnore
    public void a(Mode mode) {
        this.a = mode;
    }

    @DexIgnore
    public void a(ErrorCorrectionLevel errorCorrectionLevel) {
        this.b = errorCorrectionLevel;
    }

    @DexIgnore
    public void a(v32 v32) {
        this.c = v32;
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(x32 x32) {
        this.e = x32;
    }
}
