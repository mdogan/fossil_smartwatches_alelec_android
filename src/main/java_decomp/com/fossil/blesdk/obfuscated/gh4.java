package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.lc4;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gh4 extends jc4 implements lc4 {
    @DexIgnore
    public gh4() {
        super(lc4.b);
    }

    @DexIgnore
    public abstract void a(CoroutineContext coroutineContext, Runnable runnable);

    @DexIgnore
    public void b(kc4<?> kc4) {
        wd4.b(kc4, "continuation");
        lc4.a.a((lc4) this, kc4);
    }

    @DexIgnore
    public boolean b(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        return true;
    }

    @DexIgnore
    public final <T> kc4<T> c(kc4<? super T> kc4) {
        wd4.b(kc4, "continuation");
        return new vh4(this, kc4);
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        wd4.b(bVar, "key");
        return lc4.a.a((lc4) this, bVar);
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        wd4.b(bVar, "key");
        return lc4.a.b(this, bVar);
    }

    @DexIgnore
    public String toString() {
        return ph4.a((Object) this) + '@' + ph4.b(this);
    }

    @DexIgnore
    public void b(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        a(coroutineContext, runnable);
    }
}
