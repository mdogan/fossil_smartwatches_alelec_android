package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.st2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yw2 extends as2 implements xw2 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<xd2> j;
    @DexIgnore
    public ww2 k;
    @DexIgnore
    public st2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return yw2.n;
        }

        @DexIgnore
        public final yw2 b() {
            return new yw2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements st2.b {
        @DexIgnore
        public /* final */ /* synthetic */ yw2 a;

        @DexIgnore
        public b(yw2 yw2) {
            this.a = yw2;
        }

        @DexIgnore
        public void a() {
            yw2.b(this.a).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yw2 e;

        @DexIgnore
        public c(yw2 yw2) {
            this.e = yw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            yw2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ yw2 e;

        @DexIgnore
        public d(yw2 yw2) {
            this.e = yw2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            st2 a = this.e.l;
            if (a != null) {
                a.a(String.valueOf(charSequence));
            }
            this.e.O(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yw2 e;

        @DexIgnore
        public e(yw2 yw2) {
            this.e = yw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            yw2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xd2 e;

        @DexIgnore
        public f(xd2 xd2) {
            this.e = xd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.r.setText("");
        }
    }

    /*
    static {
        String simpleName = yw2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ww2 b(yw2 yw2) {
        ww2 ww2 = yw2.k;
        if (ww2 != null) {
            return ww2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void H() {
    }

    @DexIgnore
    public void K(boolean z) {
        int i;
        ur3<xd2> ur3 = this.j;
        if (ur3 != null) {
            xd2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
            }
            ur3<xd2> ur32 = this.j;
            if (ur32 != null) {
                xd2 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleButton flexibleButton2 = a3.q;
                    if (flexibleButton2 != null) {
                        if (z) {
                            i = k6.a((Context) PortfolioApp.W.c(), (int) R.color.colorPrimary);
                        } else {
                            i = k6.a((Context) PortfolioApp.W.c(), (int) R.color.fossilCoolGray);
                        }
                        flexibleButton2.setBackgroundColor(i);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        ur3<xd2> ur3 = this.j;
        if (ur3 != null) {
            xd2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.s;
                wd4.a((Object) imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.s;
            wd4.a((Object) imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        ww2 ww2 = this.k;
        if (ww2 != null) {
            ww2.h();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Typeface typeface;
        wd4.b(layoutInflater, "inflater");
        xd2 xd2 = (xd2) ra.a(layoutInflater, R.layout.fragment_notification_contacts, viewGroup, false, O0());
        xd2.t.setOnClickListener(new c(this));
        xd2.r.addTextChangedListener(new d(this));
        xd2.q.setOnClickListener(new e(this));
        xd2.s.setOnClickListener(new f(xd2));
        xd2.u.setIndexBarVisibility(true);
        xd2.u.setIndexBarHighLateTextVisibility(true);
        xd2.u.setIndexbarHighLateTextColor((int) R.color.greyishBrown);
        xd2.u.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        xd2.u.setIndexTextSize(9);
        xd2.u.setIndexBarTextColor((int) R.color.black);
        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_bold);
            wd4.a((Object) typeface, "resources.getFont(R.font.font_bold)");
        } else {
            typeface = Typeface.DEFAULT_BOLD;
            wd4.a((Object) typeface, "Typeface.DEFAULT_BOLD");
        }
        xd2.u.setTypeface(typeface);
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = xd2.u;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext(), 1, false));
        this.j = new ur3<>(this, xd2);
        wd4.a((Object) xd2, "binding");
        return xd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        ww2 ww2 = this.k;
        if (ww2 != null) {
            ww2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ww2 ww2 = this.k;
        if (ww2 != null) {
            ww2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ww2 ww2) {
        wd4.b(ww2, "presenter");
        this.k = ww2;
    }

    @DexIgnore
    public void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider) {
        wd4.b(list, "listContactWrapper");
        wd4.b(filterQueryProvider, "filterQueryProvider");
        this.l = new st2((Cursor) null, list);
        st2 st2 = this.l;
        if (st2 != null) {
            st2.a((st2.b) new b(this));
            st2 st22 = this.l;
            if (st22 != null) {
                st22.a(filterQueryProvider);
                ur3<xd2> ur3 = this.j;
                if (ur3 != null) {
                    xd2 a2 = ur3.a();
                    if (a2 != null) {
                        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = a2.u;
                        if (alphabetFastScrollRecyclerView != null) {
                            alphabetFastScrollRecyclerView.setAdapter(this.l);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        st2 st2 = this.l;
        if (st2 != null) {
            st2.c(cursor);
        }
    }
}
