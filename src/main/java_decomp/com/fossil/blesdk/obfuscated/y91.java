package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface y91 extends z91, Cloneable {
    @DexIgnore
    y91 a(x91 x91);

    @DexIgnore
    x91 t();

    @DexIgnore
    x91 u();
}
