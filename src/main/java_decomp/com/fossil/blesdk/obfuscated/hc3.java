package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hc3 extends as2 implements gc3 {
    @DexIgnore
    public ur3<lc2> j;
    @DexIgnore
    public fc3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        lc2 lc2 = (lc2) ra.a(layoutInflater, R.layout.fragment_heartrate_overview_day, viewGroup, false, O0());
        lc2.s.setOnClickListener(b.e);
        this.j = new ur3<>(this, lc2);
        ur3<lc2> ur3 = this.j;
        if (ur3 != null) {
            lc2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        fc3 fc3 = this.k;
        if (fc3 != null) {
            fc3.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        fc3 fc3 = this.k;
        if (fc3 != null) {
            fc3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(fc3 fc3) {
        wd4.b(fc3, "presenter");
        this.k = fc3;
    }

    @DexIgnore
    public void a(int i, List<st3> list, List<Triple<Integer, Pair<Integer, Float>, String>> list2) {
        wd4.b(list, "listTodayHeartRateModel");
        wd4.b(list2, "listTimeZoneChange");
        ur3<lc2> ur3 = this.j;
        if (ur3 != null) {
            lc2 a2 = ur3.a();
            if (a2 != null) {
                TodayHeartRateChart todayHeartRateChart = a2.u;
                if (todayHeartRateChart != null) {
                    todayHeartRateChart.setDayInMinuteWithTimeZone(i);
                    todayHeartRateChart.setListTimeZoneChange(list2);
                    todayHeartRateChart.a(list);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        wd4.b(list, "workoutSessions");
        ur3<lc2> ur3 = this.j;
        if (ur3 != null) {
            lc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.t;
                wd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.q, list.get(0));
                if (size == 1) {
                    fi2 fi2 = a2.r;
                    if (fi2 != null) {
                        View d = fi2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                fi2 fi22 = a2.r;
                if (fi22 != null) {
                    View d2 = fi22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.r, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.t;
            wd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(fi2 fi2, WorkoutSession workoutSession) {
        if (fi2 != null) {
            View d = fi2.d();
            wd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = tm2.a(context, a2.getSecond().intValue());
            fi2.u.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = fi2.t;
            wd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = fi2.r;
            wd4.a((Object) flexibleTextView2, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            int i = 0;
            flexibleTextView2.setText(String.valueOf(heartRate != null ? fe4.a(heartRate.getAverage()) : 0));
            FlexibleTextView flexibleTextView3 = fi2.q;
            wd4.a((Object) flexibleTextView3, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i = heartRate2.getMax();
            }
            flexibleTextView3.setText(String.valueOf(i));
            FlexibleTextView flexibleTextView4 = fi2.s;
            wd4.a((Object) flexibleTextView4, "it.ftvWorkoutTime");
            flexibleTextView4.setText(sk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
