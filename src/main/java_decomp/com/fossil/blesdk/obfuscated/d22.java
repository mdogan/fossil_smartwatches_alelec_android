package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.enums.Action;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d22 {
    @DexIgnore
    public static /* final */ d22 g; // = new d22(4201, 4096, 1);
    @DexIgnore
    public static /* final */ d22 h; // = new d22(1033, 1024, 1);
    @DexIgnore
    public static /* final */ d22 i; // = new d22(67, 64, 1);
    @DexIgnore
    public static /* final */ d22 j; // = new d22(19, 16, 1);
    @DexIgnore
    public static /* final */ d22 k; // = new d22(285, 256, 0);
    @DexIgnore
    public static /* final */ d22 l; // = new d22(Action.Presenter.NEXT, 256, 1);
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ e22 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public d22(int i2, int i3, int i4) {
        this.e = i2;
        this.d = i3;
        this.f = i4;
        this.a = new int[i3];
        this.b = new int[i3];
        int i5 = 1;
        for (int i6 = 0; i6 < i3; i6++) {
            this.a[i6] = i5;
            i5 <<= 1;
            if (i5 >= i3) {
                i5 = (i5 ^ i2) & (i3 - 1);
            }
        }
        for (int i7 = 0; i7 < i3 - 1; i7++) {
            this.b[this.a[i7]] = i7;
        }
        this.c = new e22(this, new int[]{0});
        new e22(this, new int[]{1});
    }

    @DexIgnore
    public static int c(int i2, int i3) {
        return i2 ^ i3;
    }

    @DexIgnore
    public e22 a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException();
        } else if (i3 == 0) {
            return this.c;
        } else {
            int[] iArr = new int[(i2 + 1)];
            iArr[0] = i3;
            return new e22(this, iArr);
        }
    }

    @DexIgnore
    public e22 b() {
        return this.c;
    }

    @DexIgnore
    public int c(int i2) {
        if (i2 != 0) {
            return this.b[i2];
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public String toString() {
        return "GF(0x" + Integer.toHexString(this.e) + ',' + this.d + ')';
    }

    @DexIgnore
    public int b(int i2) {
        if (i2 != 0) {
            return this.a[(this.d - this.b[i2]) - 1];
        }
        throw new ArithmeticException();
    }

    @DexIgnore
    public int b(int i2, int i3) {
        if (i2 == 0 || i3 == 0) {
            return 0;
        }
        int[] iArr = this.a;
        int[] iArr2 = this.b;
        return iArr[(iArr2[i2] + iArr2[i3]) % (this.d - 1)];
    }

    @DexIgnore
    public int a(int i2) {
        return this.a[i2];
    }

    @DexIgnore
    public int a() {
        return this.f;
    }
}
