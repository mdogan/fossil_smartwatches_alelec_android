package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vf implements kg, jg {
    @DexIgnore
    public static /* final */ TreeMap<Integer, vf> m; // = new TreeMap<>();
    @DexIgnore
    public volatile String e;
    @DexIgnore
    public /* final */ long[] f;
    @DexIgnore
    public /* final */ double[] g;
    @DexIgnore
    public /* final */ String[] h;
    @DexIgnore
    public /* final */ byte[][] i;
    @DexIgnore
    public /* final */ int[] j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements jg {
        @DexIgnore
        public /* final */ /* synthetic */ vf e;

        @DexIgnore
        public a(vf vfVar) {
            this.e = vfVar;
        }

        @DexIgnore
        public void a(int i) {
            this.e.a(i);
        }

        @DexIgnore
        public void b(int i, long j) {
            this.e.b(i, j);
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public void a(int i, double d) {
            this.e.a(i, d);
        }

        @DexIgnore
        public void a(int i, String str) {
            this.e.a(i, str);
        }

        @DexIgnore
        public void a(int i, byte[] bArr) {
            this.e.a(i, bArr);
        }
    }

    @DexIgnore
    public vf(int i2) {
        this.k = i2;
        int i3 = i2 + 1;
        this.j = new int[i3];
        this.f = new long[i3];
        this.g = new double[i3];
        this.h = new String[i3];
        this.i = new byte[i3][];
    }

    @DexIgnore
    public static vf a(kg kgVar) {
        vf b = b(kgVar.b(), kgVar.a());
        kgVar.a(new a(b));
        return b;
    }

    @DexIgnore
    public static vf b(String str, int i2) {
        synchronized (m) {
            Map.Entry<Integer, vf> ceilingEntry = m.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                m.remove(ceilingEntry.getKey());
                vf value = ceilingEntry.getValue();
                value.a(str, i2);
                return value;
            }
            vf vfVar = new vf(i2);
            vfVar.a(str, i2);
            return vfVar;
        }
    }

    @DexIgnore
    public static void d() {
        if (m.size() > 15) {
            int size = m.size() - 10;
            Iterator<Integer> it = m.descendingKeySet().iterator();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    it.next();
                    it.remove();
                    size = i2;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public void c() {
        synchronized (m) {
            m.put(Integer.valueOf(this.k), this);
            d();
        }
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    public void a(String str, int i2) {
        this.e = str;
        this.l = i2;
    }

    @DexIgnore
    public int a() {
        return this.l;
    }

    @DexIgnore
    public void a(jg jgVar) {
        for (int i2 = 1; i2 <= this.l; i2++) {
            int i3 = this.j[i2];
            if (i3 == 1) {
                jgVar.a(i2);
            } else if (i3 == 2) {
                jgVar.b(i2, this.f[i2]);
            } else if (i3 == 3) {
                jgVar.a(i2, this.g[i2]);
            } else if (i3 == 4) {
                jgVar.a(i2, this.h[i2]);
            } else if (i3 == 5) {
                jgVar.a(i2, this.i[i2]);
            }
        }
    }

    @DexIgnore
    public String b() {
        return this.e;
    }

    @DexIgnore
    public void b(int i2, long j2) {
        this.j[i2] = 2;
        this.f[i2] = j2;
    }

    @DexIgnore
    public void a(int i2) {
        this.j[i2] = 1;
    }

    @DexIgnore
    public void a(int i2, double d) {
        this.j[i2] = 3;
        this.g[i2] = d;
    }

    @DexIgnore
    public void a(int i2, String str) {
        this.j[i2] = 4;
        this.h[i2] = str;
    }

    @DexIgnore
    public void a(int i2, byte[] bArr) {
        this.j[i2] = 5;
        this.i[i2] = bArr;
    }

    @DexIgnore
    public void a(vf vfVar) {
        int a2 = vfVar.a() + 1;
        System.arraycopy(vfVar.j, 0, this.j, 0, a2);
        System.arraycopy(vfVar.f, 0, this.f, 0, a2);
        System.arraycopy(vfVar.h, 0, this.h, 0, a2);
        System.arraycopy(vfVar.i, 0, this.i, 0, a2);
        System.arraycopy(vfVar.g, 0, this.g, 0, a2);
    }
}
