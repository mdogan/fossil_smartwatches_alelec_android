package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.codeless.CodelessMatcher;
import io.fabric.sdk.android.InitializationException;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v44<Result> extends d64<Void, Void, Result> {
    @DexIgnore
    public /* final */ w44<Result> s;

    @DexIgnore
    public v44(w44<Result> w44) {
        this.s = w44;
    }

    @DexIgnore
    public void c(Result result) {
        this.s.a(result);
        this.s.h.a((Exception) new InitializationException(this.s.p() + " Initialization was cancelled"));
    }

    @DexIgnore
    public void d(Result result) {
        this.s.b(result);
        this.s.h.a(result);
    }

    @DexIgnore
    public void f() {
        super.f();
        z54 a = a("onPreExecute");
        try {
            boolean u = this.s.u();
            a.c();
            if (u) {
                return;
            }
        } catch (UnmetDependencyException e) {
            throw e;
        } catch (Exception e2) {
            r44.g().e("Fabric", "Failure onPreExecute()", e2);
            a.c();
        } catch (Throwable th) {
            a.c();
            b(true);
            throw th;
        }
        b(true);
    }

    @DexIgnore
    public Priority getPriority() {
        return Priority.HIGH;
    }

    @DexIgnore
    public Result a(Void... voidArr) {
        z54 a = a("doInBackground");
        Result k = !e() ? this.s.k() : null;
        a.c();
        return k;
    }

    @DexIgnore
    public final z54 a(String str) {
        z54 z54 = new z54(this.s.p() + CodelessMatcher.CURRENT_CLASS_NAME + str, "KitInitialization");
        z54.b();
        return z54;
    }
}
