package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t41 implements n41<v31> {
    @DexIgnore
    public /* final */ /* synthetic */ s41 a;

    @DexIgnore
    public t41(s41 s41) {
        this.a = s41;
    }

    @DexIgnore
    public final void a() {
        this.a.p();
    }

    @DexIgnore
    public final /* synthetic */ IInterface b() throws DeadObjectException {
        return (v31) this.a.x();
    }
}
