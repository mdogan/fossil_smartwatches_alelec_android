package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xw2 extends w52<ww2> {
    @DexIgnore
    void H();

    @DexIgnore
    void K(boolean z);

    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider);

    @DexIgnore
    void close();
}
