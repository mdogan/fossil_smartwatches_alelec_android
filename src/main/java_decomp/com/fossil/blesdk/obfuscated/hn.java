package com.fossil.blesdk.obfuscated;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import java.io.IOException;
import java.util.Map;
import org.apache.http.HttpResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public interface hn {
    @DexIgnore
    HttpResponse a(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError;
}
