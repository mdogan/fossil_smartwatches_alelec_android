package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ih0 extends an1 implements he0.b, he0.c {
    @DexIgnore
    public static ee0.a<? extends mn1, wm1> l; // = jn1.c;
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ ee0.a<? extends mn1, wm1> g;
    @DexIgnore
    public Set<Scope> h;
    @DexIgnore
    public lj0 i;
    @DexIgnore
    public mn1 j;
    @DexIgnore
    public lh0 k;

    @DexIgnore
    public ih0(Context context, Handler handler, lj0 lj0) {
        this(context, handler, lj0, l);
    }

    @DexIgnore
    public final void a(lh0 lh0) {
        mn1 mn1 = this.j;
        if (mn1 != null) {
            mn1.a();
        }
        this.i.a(Integer.valueOf(System.identityHashCode(this)));
        ee0.a<? extends mn1, wm1> aVar = this.g;
        Context context = this.e;
        Looper looper = this.f.getLooper();
        lj0 lj0 = this.i;
        this.j = (mn1) aVar.a(context, looper, lj0, lj0.j(), this, this);
        this.k = lh0;
        Set<Scope> set = this.h;
        if (set == null || set.isEmpty()) {
            this.f.post(new jh0(this));
        } else {
            this.j.b();
        }
    }

    @DexIgnore
    public final void b(hn1 hn1) {
        vd0 H = hn1.H();
        if (H.L()) {
            ek0 I = hn1.I();
            vd0 I2 = I.I();
            if (!I2.L()) {
                String valueOf = String.valueOf(I2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.k.b(I2);
                this.j.a();
                return;
            }
            this.k.a(I.H(), this.h);
        } else {
            this.k.b(H);
        }
        this.j.a();
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.j.a(this);
    }

    @DexIgnore
    public final void f(int i2) {
        this.j.a();
    }

    @DexIgnore
    public final mn1 o() {
        return this.j;
    }

    @DexIgnore
    public final void p() {
        mn1 mn1 = this.j;
        if (mn1 != null) {
            mn1.a();
        }
    }

    @DexIgnore
    public ih0(Context context, Handler handler, lj0 lj0, ee0.a<? extends mn1, wm1> aVar) {
        this.e = context;
        this.f = handler;
        ck0.a(lj0, (Object) "ClientSettings must not be null");
        this.i = lj0;
        this.h = lj0.i();
        this.g = aVar;
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        this.k.b(vd0);
    }

    @DexIgnore
    public final void a(hn1 hn1) {
        this.f.post(new kh0(this, hn1));
    }
}
