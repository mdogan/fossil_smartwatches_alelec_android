package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ku0 {
    @DexIgnore
    public static /* final */ hu0<?> a; // = new iu0();
    @DexIgnore
    public static /* final */ hu0<?> b; // = a();

    @DexIgnore
    public static hu0<?> a() {
        try {
            return (hu0) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static hu0<?> b() {
        return a;
    }

    @DexIgnore
    public static hu0<?> c() {
        hu0<?> hu0 = b;
        if (hu0 != null) {
            return hu0;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
