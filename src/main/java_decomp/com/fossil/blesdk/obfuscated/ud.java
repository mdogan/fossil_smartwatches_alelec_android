package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ud {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends me.b {
        @DexIgnore
        public /* final */ /* synthetic */ td a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ td c;
        @DexIgnore
        public /* final */ /* synthetic */ me.d d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public a(td tdVar, int i, td tdVar2, me.d dVar, int i2, int i3) {
            this.a = tdVar;
            this.b = i;
            this.c = tdVar2;
            this.d = dVar;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public int a() {
            return this.f;
        }

        @DexIgnore
        public int b() {
            return this.e;
        }

        @DexIgnore
        public Object c(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            td tdVar = this.c;
            Object obj2 = tdVar.get(i2 + tdVar.e());
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        public boolean a(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            td tdVar = this.c;
            Object obj2 = tdVar.get(i2 + tdVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        public boolean b(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            td tdVar = this.c;
            Object obj2 = tdVar.get(i2 + tdVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ve {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ve b;

        @DexIgnore
        public b(int i, ve veVar) {
            this.a = i;
            this.b = veVar;
        }

        @DexIgnore
        public void a(int i, int i2) {
            ve veVar = this.b;
            int i3 = this.a;
            veVar.a(i + i3, i2 + i3);
        }

        @DexIgnore
        public void b(int i, int i2) {
            this.b.b(i + this.a, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            this.b.c(i + this.a, i2);
        }

        @DexIgnore
        public void a(int i, int i2, Object obj) {
            this.b.a(i + this.a, i2, obj);
        }
    }

    @DexIgnore
    public static <T> me.c a(td<T> tdVar, td<T> tdVar2, me.d<T> dVar) {
        int a2 = tdVar.a();
        int a3 = tdVar2.a();
        return me.a(new a(tdVar, a2, tdVar2, dVar, (tdVar.size() - a2) - tdVar.b(), (tdVar2.size() - a3) - tdVar2.b()), true);
    }

    @DexIgnore
    public static <T> void a(ve veVar, td<T> tdVar, td<T> tdVar2, me.c cVar) {
        int b2 = tdVar.b();
        int b3 = tdVar2.b();
        int a2 = tdVar.a();
        int a3 = tdVar2.a();
        if (b2 == 0 && b3 == 0 && a2 == 0 && a3 == 0) {
            cVar.a(veVar);
            return;
        }
        if (b2 > b3) {
            int i = b2 - b3;
            veVar.c(tdVar.size() - i, i);
        } else if (b2 < b3) {
            veVar.b(tdVar.size(), b3 - b2);
        }
        if (a2 > a3) {
            veVar.c(0, a2 - a3);
        } else if (a2 < a3) {
            veVar.b(0, a3 - a2);
        }
        if (a3 != 0) {
            cVar.a((ve) new b(a3, veVar));
        } else {
            cVar.a(veVar);
        }
    }

    @DexIgnore
    public static int a(me.c cVar, td tdVar, td tdVar2, int i) {
        int a2 = tdVar.a();
        int i2 = i - a2;
        int size = (tdVar.size() - a2) - tdVar.b();
        if (i2 >= 0 && i2 < size) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 / 2) * (i3 % 2 == 1 ? -1 : 1)) + i2;
                if (i4 >= 0 && i4 < tdVar.k()) {
                    int a3 = cVar.a(i4);
                    if (a3 != -1) {
                        return a3 + tdVar2.e();
                    }
                }
            }
        }
        return Math.max(0, Math.min(i, tdVar2.size() - 1));
    }
}
