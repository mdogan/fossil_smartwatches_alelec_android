package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.enumerate.Weekday;
import java.util.ArrayList;
import java.util.Set;
import kotlin.TypeCastException;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k20 {
    @DexIgnore
    public static final byte a(Set<? extends Weekday> set) {
        wd4.b(set, "$this$combinedData");
        byte b = (byte) 0;
        for (Weekday value$blesdk_productionRelease : set) {
            b = (byte) (b | value$blesdk_productionRelease.getValue$blesdk_productionRelease());
        }
        return b;
    }

    @DexIgnore
    public static final JSONArray b(Set<? extends Weekday> set) {
        wd4.b(set, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (Weekday logName$blesdk_productionRelease : set) {
            jSONArray.put(logName$blesdk_productionRelease.getLogName$blesdk_productionRelease());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final String[] a(Weekday[] weekdayArr) {
        wd4.b(weekdayArr, "$this$toStringArray");
        ArrayList arrayList = new ArrayList(weekdayArr.length);
        for (Weekday name : weekdayArr) {
            arrayList.add(name.name());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
