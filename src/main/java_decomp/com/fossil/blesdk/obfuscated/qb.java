package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qb<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public /* final */ Runnable f; // = new c();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LiveData<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void d() {
            qb qbVar = qb.this;
            qbVar.a.execute(qbVar.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            do {
                if (qb.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (qb.this.c.compareAndSet(true, false)) {
                        try {
                            obj = qb.this.a();
                            z = true;
                        } finally {
                            qb.this.d.set(false);
                        }
                    }
                    if (z) {
                        qb.this.b.a(obj);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (qb.this.c.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            boolean c = qb.this.b.c();
            if (qb.this.c.compareAndSet(false, true) && c) {
                qb qbVar = qb.this;
                qbVar.a.execute(qbVar.e);
            }
        }
    }

    @DexIgnore
    public qb(Executor executor) {
        this.a = executor;
        this.b = new a();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        h3.c().b(this.f);
    }
}
