package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class th extends zh implements vh {
    @DexIgnore
    public th(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static th a(ViewGroup viewGroup) {
        return (th) zh.c(viewGroup);
    }

    @DexIgnore
    public void b(View view) {
        this.a.b(view);
    }

    @DexIgnore
    public void a(View view) {
        this.a.a(view);
    }
}
