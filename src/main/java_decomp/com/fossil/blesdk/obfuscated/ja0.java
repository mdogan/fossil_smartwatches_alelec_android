package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;
import java.util.HashMap;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ja0 {
    @DexIgnore
    public static /* final */ HashMap<Integer, Pair<MicroAppId, MicroAppVariantId>> a; // = dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(3073, new Pair(MicroAppId.RING_PHONE, MicroAppVariantId.STANDARD)), ab4.a(6657, new Pair(MicroAppId.ALARM, MicroAppVariantId.STANDARD)), ab4.a(6658, new Pair(MicroAppId.ALARM, MicroAppVariantId.SEQUENCED)), ab4.a(7169, new Pair(MicroAppId.PROGRESS, MicroAppVariantId.STANDARD)), ab4.a(7170, new Pair(MicroAppId.PROGRESS, MicroAppVariantId.SWEEP)), ab4.a(7681, new Pair(MicroAppId.TWENTY_FOUR_HOUR, MicroAppVariantId.STANDARD)), ab4.a(7682, new Pair(MicroAppId.TWENTY_FOUR_HOUR, MicroAppVariantId.SEQUENCED)), ab4.a(1025, new Pair(MicroAppId.GOAL_TRACKING, MicroAppVariantId.STANDARD)), ab4.a(4097, new Pair(MicroAppId.SELFIE, MicroAppVariantId.STANDARD)), ab4.a(4609, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.PLAY_PAUSE)), ab4.a(4610, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.NEXT)), ab4.a(4611, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.PREVIOUS)), ab4.a(4612, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.VOLUME_UP)), ab4.a(4613, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.VOLUME_DOWN)), ab4.a(4614, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.STANDARD)), ab4.a(5121, new Pair(MicroAppId.DATE, MicroAppVariantId.STANDARD)), ab4.a(5122, new Pair(MicroAppId.DATE, MicroAppVariantId.SEQUENCED)), ab4.a(5633, new Pair(MicroAppId.TIME2, MicroAppVariantId.STANDARD)), ab4.a(5634, new Pair(MicroAppId.TIME2, MicroAppVariantId.SEQUENCED)), ab4.a(6145, new Pair(MicroAppId.ALERT, MicroAppVariantId.STANDARD)), ab4.a(6146, new Pair(MicroAppId.ALERT, MicroAppVariantId.SEQUENCED)), ab4.a(8193, new Pair(MicroAppId.STOPWATCH, MicroAppVariantId.STANDARD)), ab4.a(9217, new Pair(MicroAppId.COMMUTE_TIME, MicroAppVariantId.TRAVEL)), ab4.a(9218, new Pair(MicroAppId.COMMUTE_TIME, MicroAppVariantId.ETA))});
    @DexIgnore
    public static /* final */ ja0 b; // = new ja0();

    @DexIgnore
    public final MicroAppId a(short s) {
        Pair pair = a.get(Integer.valueOf(s));
        if (pair != null) {
            return (MicroAppId) pair.getFirst();
        }
        return MicroAppId.UNDEFINED;
    }

    @DexIgnore
    public final MicroAppVariantId b(short s) {
        Pair pair = a.get(Integer.valueOf(s));
        if (pair != null) {
            return (MicroAppVariantId) pair.getSecond();
        }
        return MicroAppVariantId.UNDEFINED;
    }
}
