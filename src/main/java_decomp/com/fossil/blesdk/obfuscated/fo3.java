package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fo3 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, SignUpPresenter signUpPresenter) {
        signUpActivity.B = signUpPresenter;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, jn2 jn2) {
        signUpActivity.C = jn2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, kn2 kn2) {
        signUpActivity.D = kn2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, ln2 ln2) {
        signUpActivity.E = ln2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, MFLoginWechatManager mFLoginWechatManager) {
        signUpActivity.F = mFLoginWechatManager;
    }
}
