package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m64 implements k64 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public m64(long j, int i) {
        this.a = j;
        this.b = i;
    }

    @DexIgnore
    public long a(int i) {
        return (long) (((double) this.a) * Math.pow((double) this.b, (double) i));
    }
}
