package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ee0;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eg0 implements ng0 {
    @DexIgnore
    public /* final */ og0 a;

    @DexIgnore
    public eg0(og0 og0) {
        this.a = og0;
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final void a(vd0 vd0, ee0<?> ee0, boolean z) {
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t) {
        this.a.r.i.add(t);
        return t;
    }

    @DexIgnore
    public final void c() {
        for (ee0.f a2 : this.a.j.values()) {
            a2.a();
        }
        this.a.r.q = Collections.emptySet();
    }

    @DexIgnore
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    public final void f(int i) {
    }

    @DexIgnore
    public final void b() {
        this.a.h();
    }
}
