package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.ua.UADataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dr3 {
    @DexIgnore
    @nt4("allday_activity_timeseries/")
    z84<cs4<yz1>> a(@bt4 yz1 yz1, @jt4("api-key") String str, @jt4("authorization") String str2);

    @DexIgnore
    @nt4("data_source/")
    z84<yz1> a(@bt4 UADataSource uADataSource, @jt4("api-key") String str, @jt4("authorization") String str2);

    @DexIgnore
    @gt4("device/{pk}/")
    z84<yz1> a(@rt4("pk") String str, @jt4("api-key") String str2, @jt4("authorization") String str3);

    @DexIgnore
    @ct4("oauth2/connection/")
    z84<yz1> a(@st4("user_id") String str, @st4("client_id") String str2, @jt4("api-key") String str3, @jt4("authorization") String str4);

    @DexIgnore
    @nt4("oauth2/access_token/")
    @ft4
    z84<yz1> a(@dt4("grant_type") String str, @dt4("client_id") String str2, @dt4("client_secret") String str3, @dt4("code") String str4, @jt4("Content-Type") String str5, @jt4("Api-Key") String str6);

    @DexIgnore
    @nt4("oauth2/access_token/")
    @ft4
    z84<yz1> a(@dt4("grant_type") String str, @dt4("client_id") String str2, @dt4("client_secret") String str3, @dt4("refresh_token") String str4, @jt4("Content-Type") String str5, @jt4("api-key") String str6, @jt4("authorization") String str7);

    @DexIgnore
    @nt4("data_source_priority/")
    z84<yz1> b(@bt4 yz1 yz1, @jt4("api-key") String str, @jt4("authorization") String str2);
}
