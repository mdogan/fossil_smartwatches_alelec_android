package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b90 {
    @DexIgnore
    public static /* final */ String c; // = b90.class.getSimpleName();
    @DexIgnore
    public /* final */ Hashtable<ResourceType, LinkedHashSet<Phase>> a;
    @DexIgnore
    public /* final */ Hashtable<ResourceType, Integer> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public /* synthetic */ b90(Hashtable hashtable, rd4 rd4) {
        this(hashtable);
    }

    @DexIgnore
    public final boolean a(Phase phase) {
        Object obj;
        wd4.b(phase, "targetPhase");
        synchronized (this.a) {
            synchronized (this.b) {
                u90 u90 = u90.c;
                String str = c;
                wd4.a((Object) str, "TAG");
                u90.a(str, "Before allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                Iterator<ResourceType> it = phase.n().iterator();
                while (it.hasNext()) {
                    ResourceType next = it.next();
                    LinkedHashSet linkedHashSet = this.a.get(next);
                    if (linkedHashSet == null) {
                        linkedHashSet = new LinkedHashSet();
                    }
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    wd4.a((Object) num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator it2 = linkedHashSet.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it2.next();
                        if (((Phase) obj).b(phase)) {
                            break;
                        }
                    }
                    if (obj == null) {
                        if (linkedHashSet.size() < intValue) {
                            linkedHashSet.add(phase);
                            this.a.put(next, linkedHashSet);
                        } else {
                            Set<Map.Entry<ResourceType, LinkedHashSet<Phase>>> entrySet = this.a.entrySet();
                            wd4.a((Object) entrySet, "resourceHolders.entries");
                            for (Map.Entry value : entrySet) {
                                ((LinkedHashSet) value.getValue()).remove(phase);
                            }
                            u90 u902 = u90.c;
                            String str2 = c;
                            wd4.a((Object) str2, "TAG");
                            u902.a(str2, "After allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                            return false;
                        }
                    }
                }
                u90 u903 = u90.c;
                String str3 = c;
                wd4.a((Object) str3, "TAG");
                u903.a(str3, "After allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(Phase phase) {
        wd4.b(phase, "targetPhase");
        synchronized (this.a) {
            synchronized (this.b) {
                u90 u90 = u90.c;
                String str = c;
                wd4.a((Object) str, "TAG");
                u90.a(str, "Before releaseResource for " + phase + ", " + "current resourceHolders=" + this.a);
                Iterator<ResourceType> it = phase.n().iterator();
                while (it.hasNext()) {
                    LinkedHashSet linkedHashSet = this.a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(phase);
                    }
                }
                u90 u902 = u90.c;
                String str2 = c;
                wd4.a((Object) str2, "TAG");
                u902.a(str2, "After releaseResource for " + phase + ", " + "current resourceHolders=" + this.a);
                cb4 cb4 = cb4.a;
            }
            cb4 cb42 = cb4.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Hashtable<ResourceType, Integer> a; // = new Hashtable<>();

        @DexIgnore
        public final b a(ResourceType resourceType, int i) {
            wd4.b(resourceType, "resourceType");
            Integer num = this.a.get(resourceType);
            if (num == null) {
                num = 0;
            }
            wd4.a((Object) num, "resourceQuotas[resourceType] ?: 0");
            this.a.put(resourceType, Integer.valueOf(num.intValue() + i));
            return this;
        }

        @DexIgnore
        public final b b(ResourceType resourceType, int i) {
            wd4.b(resourceType, "resourceType");
            this.a.put(resourceType, Integer.valueOf(i));
            return this;
        }

        @DexIgnore
        public final b90 a() {
            return new b90(this.a, (rd4) null);
        }
    }

    @DexIgnore
    public b90(Hashtable<ResourceType, Integer> hashtable) {
        this.b = hashtable;
        this.a = new Hashtable<>();
        Set<ResourceType> keySet = this.b.keySet();
        wd4.a((Object) keySet, "resourceQuotas.keys");
        for (ResourceType put : keySet) {
            this.a.put(put, new LinkedHashSet());
        }
    }
}
