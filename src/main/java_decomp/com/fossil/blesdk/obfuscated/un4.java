package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.km4;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class un4 implements ln4 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ in4 b;
    @DexIgnore
    public /* final */ xo4 c;
    @DexIgnore
    public /* final */ wo4 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements kp4 {
        @DexIgnore
        public /* final */ bp4 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public long g;

        @DexIgnore
        public b() {
            this.e = new bp4(un4.this.c.b());
            this.g = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            un4 un4 = un4.this;
            int i = un4.e;
            if (i != 6) {
                if (i == 5) {
                    un4.a(this.e);
                    un4 un42 = un4.this;
                    un42.e = 6;
                    in4 in4 = un42.b;
                    if (in4 != null) {
                        in4.a(!z, un42, this.g, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + un4.this.e);
            }
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            try {
                long b = un4.this.c.b(vo4, j);
                if (b > 0) {
                    this.g += b;
                }
                return b;
            } catch (IOException e2) {
                a(false, e2);
                throw e2;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements jp4 {
        @DexIgnore
        public /* final */ bp4 e; // = new bp4(un4.this.d.b());
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            if (this.f) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                un4.this.d.a(j);
                un4.this.d.a("\r\n");
                un4.this.d.a(vo4, j);
                un4.this.d.a("\r\n");
            }
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            if (!this.f) {
                this.f = true;
                un4.this.d.a("0\r\n\r\n");
                un4.this.a(this.e);
                un4.this.e = 3;
            }
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.f) {
                un4.this.d.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public /* final */ lm4 i;
        @DexIgnore
        public long j; // = -1;
        @DexIgnore
        public boolean k; // = true;

        @DexIgnore
        public d(lm4 lm4) {
            super();
            this.i = lm4;
        }

        @DexIgnore
        public long b(vo4 vo4, long j2) throws IOException {
            if (j2 < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j2);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (!this.k) {
                return -1;
            } else {
                long j3 = this.j;
                if (j3 == 0 || j3 == -1) {
                    c();
                    if (!this.k) {
                        return -1;
                    }
                }
                long b = super.b(vo4, Math.min(j2, this.j));
                if (b != -1) {
                    this.j -= b;
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            if (this.j != -1) {
                un4.this.c.i();
            }
            try {
                this.j = un4.this.c.l();
                String trim = un4.this.c.i().trim();
                if (this.j < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.j + trim + "\"");
                } else if (this.j == 0) {
                    this.k = false;
                    nn4.a(un4.this.a.g(), this.i, un4.this.f());
                    a(true, (IOException) null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.k && !vm4.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements jp4 {
        @DexIgnore
        public /* final */ bp4 e; // = new bp4(un4.this.d.b());
        @DexIgnore
        public boolean f;
        @DexIgnore
        public long g;

        @DexIgnore
        public e(long j) {
            this.g = j;
        }

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            if (!this.f) {
                vm4.a(vo4.B(), 0, j);
                if (j <= this.g) {
                    un4.this.d.a(vo4, j);
                    this.g -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.g + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                this.f = true;
                if (this.g <= 0) {
                    un4.this.a(this.e);
                    un4.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            if (!this.f) {
                un4.this.d.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long i;

        @DexIgnore
        public f(un4 un4, long j) throws IOException {
            super();
            this.i = j;
            if (this.i == 0) {
                a(true, (IOException) null);
            }
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!this.f) {
                long j2 = this.i;
                if (j2 == 0) {
                    return -1;
                }
                long b = super.b(vo4, Math.min(j2, j));
                if (b != -1) {
                    this.i -= b;
                    if (this.i == 0) {
                        a(true, (IOException) null);
                    }
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.i != 0 && !vm4.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean i;

        @DexIgnore
        public g(un4 un4) {
            super();
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (this.i) {
                return -1;
            } else {
                long b = super.b(vo4, j);
                if (b != -1) {
                    return b;
                }
                this.i = true;
                a(true, (IOException) null);
                return -1;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (!this.i) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexIgnore
    public un4(OkHttpClient okHttpClient, in4 in4, xo4 xo4, wo4 wo4) {
        this.a = okHttpClient;
        this.b = in4;
        this.c = xo4;
        this.d = wo4;
    }

    @DexIgnore
    public jp4 a(pm4 pm4, long j) {
        if ("chunked".equalsIgnoreCase(pm4.a("Transfer-Encoding"))) {
            return c();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void b() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public jp4 c() {
        if (this.e == 1) {
            this.e = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void cancel() {
        fn4 c2 = this.b.c();
        if (c2 != null) {
            c2.b();
        }
    }

    @DexIgnore
    public kp4 d() throws IOException {
        if (this.e == 4) {
            in4 in4 = this.b;
            if (in4 != null) {
                this.e = 5;
                in4.e();
                return new g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String e() throws IOException {
        String e2 = this.c.e(this.f);
        this.f -= (long) e2.length();
        return e2;
    }

    @DexIgnore
    public km4 f() throws IOException {
        km4.a aVar = new km4.a();
        while (true) {
            String e2 = e();
            if (e2.length() == 0) {
                return aVar.a();
            }
            tm4.a.a(aVar, e2);
        }
    }

    @DexIgnore
    public kp4 b(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new f(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(pm4 pm4) throws IOException {
        a(pm4.c(), rn4.a(pm4, this.b.c().f().b().type()));
    }

    @DexIgnore
    public qm4 a(Response response) throws IOException {
        in4 in4 = this.b;
        in4.f.e(in4.e);
        String e2 = response.e(GraphRequest.CONTENT_TYPE_HEADER);
        if (!nn4.b(response)) {
            return new qn4(e2, 0, ep4.a(b(0)));
        }
        if ("chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return new qn4(e2, -1, ep4.a(a(response.L().g())));
        }
        long a2 = nn4.a(response);
        if (a2 != -1) {
            return new qn4(e2, a2, ep4.a(b(a2)));
        }
        return new qn4(e2, -1, ep4.a(d()));
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public void a(km4 km4, String str) throws IOException {
        if (this.e == 0) {
            this.d.a(str).a("\r\n");
            int b2 = km4.b();
            for (int i = 0; i < b2; i++) {
                this.d.a(km4.a(i)).a(": ").a(km4.b(i)).a("\r\n");
            }
            this.d.a("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                tn4 a2 = tn4.a(e());
                Response.a aVar = new Response.a();
                aVar.a(a2.a);
                aVar.a(a2.b);
                aVar.a(a2.c);
                aVar.a(f());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    public jp4 a(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public kp4 a(lm4 lm4) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new d(lm4);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(bp4 bp4) {
        lp4 g2 = bp4.g();
        bp4.a(lp4.d);
        g2.a();
        g2.b();
    }
}
