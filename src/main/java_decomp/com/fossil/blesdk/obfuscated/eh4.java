package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.eh4;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface eh4<T extends Throwable & eh4<T>> {
    @DexIgnore
    T createCopy();
}
