package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kf4<T, R> implements df4<R> {
    @DexIgnore
    public /* final */ df4<T> a;
    @DexIgnore
    public /* final */ jd4<T, R> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<R>, de4 {
        @DexIgnore
        public /* final */ Iterator<T> e;
        @DexIgnore
        public /* final */ /* synthetic */ kf4 f;

        @DexIgnore
        public a(kf4 kf4) {
            this.f = kf4;
            this.e = kf4.a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.e.hasNext();
        }

        @DexIgnore
        public R next() {
            return this.f.b.invoke(this.e.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public kf4(df4<? extends T> df4, jd4<? super T, ? extends R> jd4) {
        wd4.b(df4, "sequence");
        wd4.b(jd4, "transformer");
        this.a = df4;
        this.b = jd4;
    }

    @DexIgnore
    public Iterator<R> iterator() {
        return new a(this);
    }
}
