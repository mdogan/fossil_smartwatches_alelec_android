package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.util.Base64;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.security.KeyPair;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lr3 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ fn2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wd4.b(str, "aliasName");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            wd4.b(str, "errorMessage");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wd4.b(str, "decryptedValue");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public lr3(fn2 fn2) {
        wd4.b(fn2, "mSharedPreferencesManager");
        this.d = fn2;
    }

    @DexIgnore
    public String c() {
        return "DecryptValueKeyStoreUseCase";
    }

    @DexIgnore
    public Object a(b bVar, kc4<Object> kc4) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                fn2 fn2 = this.d;
                if (bVar != null) {
                    String b2 = fn2.b(bVar.a());
                    String c2 = this.d.c(bVar.a());
                    KeyStore.SecretKeyEntry e = js3.b.e(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(2, e.getSecretKey(), new GCMParameterSpec(128, Base64.decode(b2, 0)));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DecryptValueKeyStoreUseCase", "Start decrypt with iv " + b2 + " encryptedValue " + c2);
                    byte[] doFinal = instance.doFinal(Base64.decode(c2, 0));
                    wd4.a((Object) doFinal, "decryptedByteArray");
                    String str = new String(doFinal, nf4.a);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str);
                    a(new d(str));
                    return new Object();
                }
                wd4.a();
                throw null;
            }
            fn2 fn22 = this.d;
            if (bVar != null) {
                String c3 = fn22.c(bVar.a());
                KeyPair c4 = js3.b.c(bVar.a());
                if (c4 == null) {
                    c4 = js3.b.a(bVar.a());
                }
                Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance2.init(2, c4.getPrivate());
                byte[] doFinal2 = instance2.doFinal(Base64.decode(c3, 0));
                wd4.a((Object) doFinal2, "decryptedByte");
                String str2 = new String(doFinal2, nf4.a);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str2);
                a(new d(str2));
                return new Object();
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("DecryptValueKeyStoreUseCase", "Exception when decrypt value " + e2);
            a(new c(600, ""));
        }
    }
}
