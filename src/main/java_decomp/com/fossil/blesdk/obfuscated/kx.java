package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kx implements u64 {
    @DexIgnore
    public /* final */ ay a;
    @DexIgnore
    public /* final */ yx b;

    @DexIgnore
    public kx(ay ayVar, yx yxVar) {
        this.a = ayVar;
        this.b = yxVar;
    }

    @DexIgnore
    public static kx a(ay ayVar) {
        return new kx(ayVar, new yx(new o64(new xx(new m64(1000, 8), 0.1d), new l64(5))));
    }

    @DexIgnore
    public boolean a(List<File> list) {
        long nanoTime = System.nanoTime();
        if (this.b.a(nanoTime)) {
            if (this.a.a(list)) {
                this.b.a();
                return true;
            }
            this.b.b(nanoTime);
        }
        return false;
    }
}
