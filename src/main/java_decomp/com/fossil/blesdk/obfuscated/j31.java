package com.fossil.blesdk.obfuscated;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j31 extends WeakReference<Throwable> {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public j31(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == j31.class) {
            if (this == obj) {
                return true;
            }
            j31 j31 = (j31) obj;
            return this.a == j31.a && get() == j31.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.a;
    }
}
