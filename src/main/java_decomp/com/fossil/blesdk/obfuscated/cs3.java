package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.util.DeviceUtils;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cs3 implements MembersInjector<DeviceUtils> {
    @DexIgnore
    public static void a(DeviceUtils deviceUtils, DeviceRepository deviceRepository) {
        deviceUtils.a = deviceRepository;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, fn2 fn2) {
        deviceUtils.b = fn2;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, GuestApiService guestApiService) {
        deviceUtils.c = guestApiService;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, FirmwareFileRepository firmwareFileRepository) {
        deviceUtils.d = firmwareFileRepository;
    }
}
