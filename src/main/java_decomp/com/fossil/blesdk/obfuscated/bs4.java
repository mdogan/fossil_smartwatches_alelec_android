package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.km4;
import com.fossil.blesdk.obfuscated.nm4;
import com.fossil.blesdk.obfuscated.pm4;
import com.fossil.blesdk.obfuscated.yr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit.RestMethodInfo;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bs4 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ lm4 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ km4 e;
    @DexIgnore
    public /* final */ mm4 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ yr4<?>[] j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore
    public bs4(a aVar) {
        this.a = aVar.b;
        this.b = aVar.a.c;
        this.c = aVar.n;
        this.d = aVar.r;
        this.e = aVar.s;
        this.f = aVar.t;
        this.g = aVar.o;
        this.h = aVar.p;
        this.i = aVar.q;
        this.j = aVar.v;
        this.k = aVar.w;
    }

    @DexIgnore
    public static bs4 a(Retrofit retrofit3, Method method) {
        return new a(retrofit3, method).a();
    }

    @DexIgnore
    public pm4 a(Object[] objArr) throws IOException {
        yr4<?>[] yr4Arr = this.j;
        int length = objArr.length;
        if (length == yr4Arr.length) {
            as4 as4 = new as4(this.c, this.b, this.d, this.e, this.f, this.g, this.h, this.i);
            if (this.k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i2 = 0; i2 < length; i2++) {
                arrayList.add(objArr[i2]);
                yr4Arr[i2].a(as4, objArr[i2]);
            }
            pm4.a a2 = as4.a();
            a2.a(vr4.class, new vr4(this.a, arrayList));
            return a2.a();
        }
        throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + yr4Arr.length + ")");
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Pattern x; // = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        @DexIgnore
        public static /* final */ Pattern y; // = Pattern.compile(RestMethodInfo.PARAM);
        @DexIgnore
        public /* final */ Retrofit a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Annotation[] c;
        @DexIgnore
        public /* final */ Annotation[][] d;
        @DexIgnore
        public /* final */ Type[] e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q;
        @DexIgnore
        public String r;
        @DexIgnore
        public km4 s;
        @DexIgnore
        public mm4 t;
        @DexIgnore
        public Set<String> u;
        @DexIgnore
        public yr4<?>[] v;
        @DexIgnore
        public boolean w;

        @DexIgnore
        public a(Retrofit retrofit3, Method method) {
            this.a = retrofit3;
            this.b = method;
            this.c = method.getAnnotations();
            this.e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        @DexIgnore
        public bs4 a() {
            for (Annotation a2 : this.c) {
                a(a2);
            }
            if (this.n != null) {
                if (!this.o) {
                    if (this.q) {
                        throw gs4.a(this.b, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    } else if (this.p) {
                        throw gs4.a(this.b, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    }
                }
                int length = this.d.length;
                this.v = new yr4[length];
                int i2 = length - 1;
                int i3 = 0;
                while (true) {
                    boolean z = true;
                    if (i3 >= length) {
                        break;
                    }
                    yr4<?>[] yr4Arr = this.v;
                    Type type = this.e[i3];
                    Annotation[] annotationArr = this.d[i3];
                    if (i3 != i2) {
                        z = false;
                    }
                    yr4Arr[i3] = a(i3, type, annotationArr, z);
                    i3++;
                }
                if (this.r == null && !this.m) {
                    throw gs4.a(this.b, "Missing either @%s URL or @Url parameter.", this.n);
                } else if (!this.p && !this.q && !this.o && this.h) {
                    throw gs4.a(this.b, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                } else if (this.p && !this.f) {
                    throw gs4.a(this.b, "Form-encoded method must contain at least one @Field.", new Object[0]);
                } else if (!this.q || this.g) {
                    return new bs4(this);
                } else {
                    throw gs4.a(this.b, "Multipart method must contain at least one @Part.", new Object[0]);
                }
            } else {
                throw gs4.a(this.b, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(Annotation annotation) {
            if (annotation instanceof ct4) {
                a("DELETE", ((ct4) annotation).value(), false);
            } else if (annotation instanceof gt4) {
                a("GET", ((gt4) annotation).value(), false);
            } else if (annotation instanceof ht4) {
                a("HEAD", ((ht4) annotation).value(), false);
            } else if (annotation instanceof mt4) {
                a("PATCH", ((mt4) annotation).value(), true);
            } else if (annotation instanceof nt4) {
                a("POST", ((nt4) annotation).value(), true);
            } else if (annotation instanceof ot4) {
                a("PUT", ((ot4) annotation).value(), true);
            } else if (annotation instanceof lt4) {
                a("OPTIONS", ((lt4) annotation).value(), false);
            } else if (annotation instanceof it4) {
                it4 it4 = (it4) annotation;
                a(it4.method(), it4.path(), it4.hasBody());
            } else if (annotation instanceof kt4) {
                String[] value = ((kt4) annotation).value();
                if (value.length != 0) {
                    this.s = a(value);
                    return;
                }
                throw gs4.a(this.b, "@Headers annotation is empty.", new Object[0]);
            } else if (!(annotation instanceof ft4)) {
            } else {
                if (!this.q) {
                    this.p = true;
                    return;
                }
                throw gs4.a(this.b, "Only one encoding annotation is allowed.", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(String str, String str2, boolean z) {
            String str3 = this.n;
            if (str3 == null) {
                this.n = str;
                this.o = z;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (x.matcher(substring).find()) {
                            throw gs4.a(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.r = str2;
                    this.u = a(str2);
                    return;
                }
                return;
            }
            throw gs4.a(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        @DexIgnore
        public final km4 a(String[] strArr) {
            km4.a aVar = new km4.a();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw gs4.a(this.b, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if (GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(substring)) {
                    try {
                        this.t = mm4.a(trim);
                    } catch (IllegalArgumentException e2) {
                        throw gs4.a(this.b, (Throwable) e2, "Malformed content type: %s", trim);
                    }
                } else {
                    aVar.a(substring, trim);
                }
            }
            return aVar.a();
        }

        @DexIgnore
        public final yr4<?> a(int i2, Type type, Annotation[] annotationArr, boolean z) {
            yr4<?> yr4;
            if (annotationArr != null) {
                yr4 = null;
                for (Annotation a2 : annotationArr) {
                    yr4<?> a3 = a(i2, type, annotationArr, a2);
                    if (a3 != null) {
                        if (yr4 == null) {
                            yr4 = a3;
                        } else {
                            throw gs4.a(this.b, i2, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                        }
                    }
                }
            } else {
                yr4 = null;
            }
            if (yr4 != null) {
                return yr4;
            }
            if (z) {
                try {
                    if (gs4.b(type) == kc4.class) {
                        this.w = true;
                        return null;
                    }
                } catch (NoClassDefFoundError unused) {
                }
            }
            throw gs4.a(this.b, i2, "No Retrofit annotation found.", new Object[0]);
        }

        @DexIgnore
        public final yr4<?> a(int i2, Type type, Annotation[] annotationArr, Annotation annotation) {
            Class<nm4.b> cls = nm4.b.class;
            if (annotation instanceof wt4) {
                a(i2, type);
                if (this.m) {
                    throw gs4.a(this.b, i2, "Multiple @Url method annotations found.", new Object[0]);
                } else if (this.i) {
                    throw gs4.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.j) {
                    throw gs4.a(this.b, i2, "A @Url parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw gs4.a(this.b, i2, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw gs4.a(this.b, i2, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.r == null) {
                    this.m = true;
                    if (type == lm4.class || type == String.class || type == URI.class || ((type instanceof Class) && "android.net.Uri".equals(((Class) type).getName()))) {
                        return new yr4.n(this.b, i2);
                    }
                    throw gs4.a(this.b, i2, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                } else {
                    throw gs4.a(this.b, i2, "@Url cannot be used with @%s URL", this.n);
                }
            } else if (annotation instanceof rt4) {
                a(i2, type);
                if (this.j) {
                    throw gs4.a(this.b, i2, "A @Path parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw gs4.a(this.b, i2, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw gs4.a(this.b, i2, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.m) {
                    throw gs4.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.r != null) {
                    this.i = true;
                    rt4 rt4 = (rt4) annotation;
                    String value = rt4.value();
                    a(i2, value);
                    return new yr4.i(this.b, i2, value, this.a.c(type, annotationArr), rt4.encoded());
                } else {
                    throw gs4.a(this.b, i2, "@Path can only be used with relative url on @%s", this.n);
                }
            } else if (annotation instanceof st4) {
                a(i2, type);
                st4 st4 = (st4) annotation;
                String value2 = st4.value();
                boolean encoded = st4.encoded();
                Class<?> b2 = gs4.b(type);
                this.j = true;
                if (Iterable.class.isAssignableFrom(b2)) {
                    if (type instanceof ParameterizedType) {
                        return new yr4.j(value2, this.a.c(gs4.b(0, (ParameterizedType) type), annotationArr), encoded).b();
                    }
                    Method method = this.b;
                    throw gs4.a(method, i2, b2.getSimpleName() + " must include generic type (e.g., " + b2.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b2.isArray()) {
                    return new yr4.j(value2, this.a.c(type, annotationArr), encoded);
                } else {
                    return new yr4.j(value2, this.a.c(a(b2.getComponentType()), annotationArr), encoded).a();
                }
            } else if (annotation instanceof ut4) {
                a(i2, type);
                boolean encoded2 = ((ut4) annotation).encoded();
                Class<?> b3 = gs4.b(type);
                this.k = true;
                if (Iterable.class.isAssignableFrom(b3)) {
                    if (type instanceof ParameterizedType) {
                        return new yr4.l(this.a.c(gs4.b(0, (ParameterizedType) type), annotationArr), encoded2).b();
                    }
                    Method method2 = this.b;
                    throw gs4.a(method2, i2, b3.getSimpleName() + " must include generic type (e.g., " + b3.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b3.isArray()) {
                    return new yr4.l(this.a.c(type, annotationArr), encoded2);
                } else {
                    return new yr4.l(this.a.c(a(b3.getComponentType()), annotationArr), encoded2).a();
                }
            } else if (annotation instanceof tt4) {
                a(i2, type);
                Class<?> b4 = gs4.b(type);
                this.l = true;
                if (Map.class.isAssignableFrom(b4)) {
                    Type b5 = gs4.b(type, b4, Map.class);
                    if (b5 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) b5;
                        Type b6 = gs4.b(0, parameterizedType);
                        if (String.class == b6) {
                            return new yr4.k(this.b, i2, this.a.c(gs4.b(1, parameterizedType), annotationArr), ((tt4) annotation).encoded());
                        }
                        Method method3 = this.b;
                        throw gs4.a(method3, i2, "@QueryMap keys must be of type String: " + b6, new Object[0]);
                    }
                    throw gs4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw gs4.a(this.b, i2, "@QueryMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof jt4) {
                a(i2, type);
                String value3 = ((jt4) annotation).value();
                Class<?> b7 = gs4.b(type);
                if (Iterable.class.isAssignableFrom(b7)) {
                    if (type instanceof ParameterizedType) {
                        return new yr4.f(value3, this.a.c(gs4.b(0, (ParameterizedType) type), annotationArr)).b();
                    }
                    Method method4 = this.b;
                    throw gs4.a(method4, i2, b7.getSimpleName() + " must include generic type (e.g., " + b7.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b7.isArray()) {
                    return new yr4.f(value3, this.a.c(type, annotationArr));
                } else {
                    return new yr4.f(value3, this.a.c(a(b7.getComponentType()), annotationArr)).a();
                }
            } else if (annotation instanceof dt4) {
                a(i2, type);
                if (this.p) {
                    dt4 dt4 = (dt4) annotation;
                    String value4 = dt4.value();
                    boolean encoded3 = dt4.encoded();
                    this.f = true;
                    Class<?> b8 = gs4.b(type);
                    if (Iterable.class.isAssignableFrom(b8)) {
                        if (type instanceof ParameterizedType) {
                            return new yr4.d(value4, this.a.c(gs4.b(0, (ParameterizedType) type), annotationArr), encoded3).b();
                        }
                        Method method5 = this.b;
                        throw gs4.a(method5, i2, b8.getSimpleName() + " must include generic type (e.g., " + b8.getSimpleName() + "<String>)", new Object[0]);
                    } else if (!b8.isArray()) {
                        return new yr4.d(value4, this.a.c(type, annotationArr), encoded3);
                    } else {
                        return new yr4.d(value4, this.a.c(a(b8.getComponentType()), annotationArr), encoded3).a();
                    }
                } else {
                    throw gs4.a(this.b, i2, "@Field parameters can only be used with form encoding.", new Object[0]);
                }
            } else if (annotation instanceof et4) {
                a(i2, type);
                if (this.p) {
                    Class<?> b9 = gs4.b(type);
                    if (Map.class.isAssignableFrom(b9)) {
                        Type b10 = gs4.b(type, b9, Map.class);
                        if (b10 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType2 = (ParameterizedType) b10;
                            Type b11 = gs4.b(0, parameterizedType2);
                            if (String.class == b11) {
                                sr4 c2 = this.a.c(gs4.b(1, parameterizedType2), annotationArr);
                                this.f = true;
                                return new yr4.e(this.b, i2, c2, ((et4) annotation).encoded());
                            }
                            Method method6 = this.b;
                            throw gs4.a(method6, i2, "@FieldMap keys must be of type String: " + b11, new Object[0]);
                        }
                        throw gs4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw gs4.a(this.b, i2, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                throw gs4.a(this.b, i2, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof pt4) {
                a(i2, type);
                if (this.q) {
                    pt4 pt4 = (pt4) annotation;
                    this.g = true;
                    String value5 = pt4.value();
                    Class<?> b12 = gs4.b(type);
                    if (!value5.isEmpty()) {
                        km4 a2 = km4.a("Content-Disposition", "form-data; name=\"" + value5 + "\"", "Content-Transfer-Encoding", pt4.encoding());
                        if (Iterable.class.isAssignableFrom(b12)) {
                            if (type instanceof ParameterizedType) {
                                Type b13 = gs4.b(0, (ParameterizedType) type);
                                if (!cls.isAssignableFrom(gs4.b(b13))) {
                                    return new yr4.g(this.b, i2, a2, this.a.a(b13, annotationArr, this.c)).b();
                                }
                                throw gs4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                            }
                            Method method7 = this.b;
                            throw gs4.a(method7, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (b12.isArray()) {
                            Class<?> a3 = a(b12.getComponentType());
                            if (!cls.isAssignableFrom(a3)) {
                                return new yr4.g(this.b, i2, a2, this.a.a((Type) a3, annotationArr, this.c)).a();
                            }
                            throw gs4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        } else if (!cls.isAssignableFrom(b12)) {
                            return new yr4.g(this.b, i2, a2, this.a.a(type, annotationArr, this.c));
                        } else {
                            throw gs4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                    } else if (Iterable.class.isAssignableFrom(b12)) {
                        if (!(type instanceof ParameterizedType)) {
                            Method method8 = this.b;
                            throw gs4.a(method8, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (cls.isAssignableFrom(gs4.b(gs4.b(0, (ParameterizedType) type)))) {
                            return yr4.m.a.b();
                        } else {
                            throw gs4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        }
                    } else if (b12.isArray()) {
                        if (cls.isAssignableFrom(b12.getComponentType())) {
                            return yr4.m.a.a();
                        }
                        throw gs4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    } else if (cls.isAssignableFrom(b12)) {
                        return yr4.m.a;
                    } else {
                        throw gs4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    }
                } else {
                    throw gs4.a(this.b, i2, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                }
            } else if (annotation instanceof qt4) {
                a(i2, type);
                if (this.q) {
                    this.g = true;
                    Class<?> b14 = gs4.b(type);
                    if (Map.class.isAssignableFrom(b14)) {
                        Type b15 = gs4.b(type, b14, Map.class);
                        if (b15 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType3 = (ParameterizedType) b15;
                            Type b16 = gs4.b(0, parameterizedType3);
                            if (String.class == b16) {
                                Type b17 = gs4.b(1, parameterizedType3);
                                if (!cls.isAssignableFrom(gs4.b(b17))) {
                                    return new yr4.h(this.b, i2, this.a.a(b17, annotationArr, this.c), ((qt4) annotation).encoding());
                                }
                                throw gs4.a(this.b, i2, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                            }
                            Method method9 = this.b;
                            throw gs4.a(method9, i2, "@PartMap keys must be of type String: " + b16, new Object[0]);
                        }
                        throw gs4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw gs4.a(this.b, i2, "@PartMap parameter type must be Map.", new Object[0]);
                }
                throw gs4.a(this.b, i2, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
            } else if (!(annotation instanceof bt4)) {
                return null;
            } else {
                a(i2, type);
                if (this.p || this.q) {
                    throw gs4.a(this.b, i2, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                } else if (!this.h) {
                    try {
                        sr4 a4 = this.a.a(type, annotationArr, this.c);
                        this.h = true;
                        return new yr4.c(this.b, i2, a4);
                    } catch (RuntimeException e2) {
                        throw gs4.a(this.b, e2, i2, "Unable to create @Body converter for %s", type);
                    }
                } else {
                    throw gs4.a(this.b, i2, "Multiple @Body method annotations found.", new Object[0]);
                }
            }
        }

        @DexIgnore
        public final void a(int i2, Type type) {
            if (gs4.c(type)) {
                throw gs4.a(this.b, i2, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }

        @DexIgnore
        public final void a(int i2, String str) {
            if (!y.matcher(str).matches()) {
                throw gs4.a(this.b, i2, "@Path parameter name must match %s. Found: %s", x.pattern(), str);
            } else if (!this.u.contains(str)) {
                throw gs4.a(this.b, i2, "URL \"%s\" does not contain \"{%s}\".", this.r, str);
            }
        }

        @DexIgnore
        public static Set<String> a(String str) {
            Matcher matcher = x.matcher(str);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (matcher.find()) {
                linkedHashSet.add(matcher.group(1));
            }
            return linkedHashSet;
        }

        @DexIgnore
        public static Class<?> a(Class<?> cls) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            return Short.TYPE == cls ? Short.class : cls;
        }
    }
}
