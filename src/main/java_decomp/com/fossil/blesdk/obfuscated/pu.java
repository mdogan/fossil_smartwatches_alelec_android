package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import com.fossil.blesdk.obfuscated.mu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pu implements nu {
    @DexIgnore
    public mu a(Context context, mu.a aVar) {
        boolean z = k6.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (Log.isLoggable("ConnectivityMonitor", 3)) {
            Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
        }
        if (z) {
            return new ou(context, aVar);
        }
        return new tu();
    }
}
