package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kl4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater d;
    @DexIgnore
    public /* final */ AtomicReferenceArray<fl4> a; // = new AtomicReferenceArray<>(128);
    @DexIgnore
    public volatile int consumerIndex; // = 0;
    @DexIgnore
    public volatile Object lastScheduledTask; // = null;
    @DexIgnore
    public volatile int producerIndex; // = 0;

    /*
    static {
        Class<kl4> cls = kl4.class;
        b = AtomicReferenceFieldUpdater.newUpdater(cls, Object.class, "lastScheduledTask");
        c = AtomicIntegerFieldUpdater.newUpdater(cls, "producerIndex");
        d = AtomicIntegerFieldUpdater.newUpdater(cls, "consumerIndex");
    }
    */

    @DexIgnore
    public final fl4 b() {
        fl4 fl4 = (fl4) b.getAndSet(this, (Object) null);
        if (fl4 != null) {
            return fl4;
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (((fl4) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                return (fl4) this.a.getAndSet(i2, (Object) null);
            }
        }
    }

    @DexIgnore
    public final int c() {
        return this.lastScheduledTask != null ? a() + 1 : a();
    }

    @DexIgnore
    public final int a() {
        return this.producerIndex - this.consumerIndex;
    }

    @DexIgnore
    public final boolean a(fl4 fl4, bl4 bl4) {
        wd4.b(fl4, "task");
        wd4.b(bl4, "globalQueue");
        fl4 fl42 = (fl4) b.getAndSet(this, fl4);
        if (fl42 != null) {
            return b(fl42, bl4);
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v2, resolved type: com.fossil.blesdk.obfuscated.fl4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final boolean a(kl4 kl4, bl4 bl4) {
        fl4 fl4;
        kl4 kl42 = kl4;
        bl4 bl42 = bl4;
        wd4.b(kl42, "victim");
        wd4.b(bl42, "globalQueue");
        long a2 = il4.f.a();
        int a3 = kl4.a();
        if (a3 == 0) {
            return a(a2, kl42, bl42);
        }
        int a4 = qe4.a(a3 / 2, 1);
        int i = 0;
        boolean z = false;
        while (i < a4) {
            while (true) {
                int i2 = kl42.consumerIndex;
                fl4 = null;
                if (i2 - kl42.producerIndex != 0) {
                    int i3 = i2 & 127;
                    fl4 fl42 = (fl4) kl4.a.get(i3);
                    if (fl42 != null) {
                        if (!(a2 - fl42.e >= il4.a || kl4.a() > il4.b)) {
                            break;
                        } else if (d.compareAndSet(kl42, i2, i2 + 1)) {
                            fl4 = kl4.a.getAndSet(i3, (Object) null);
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            if (fl4 == null) {
                break;
            }
            a(fl4, bl42);
            i++;
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(fl4 fl4, bl4 bl4) {
        wd4.b(fl4, "task");
        wd4.b(bl4, "globalQueue");
        boolean z = true;
        while (!a(fl4)) {
            b(bl4);
            z = false;
        }
        return z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.fossil.blesdk.obfuscated.fl4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void b(bl4 bl4) {
        fl4 fl4;
        int a2 = qe4.a(a() / 2, 1);
        int i = 0;
        while (i < a2) {
            while (true) {
                int i2 = this.consumerIndex;
                fl4 = null;
                if (i2 - this.producerIndex != 0) {
                    int i3 = i2 & 127;
                    if (((fl4) this.a.get(i3)) != null && d.compareAndSet(this, i2, i2 + 1)) {
                        fl4 = this.a.getAndSet(i3, (Object) null);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (fl4 != null) {
                a(bl4, fl4);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(long j, kl4 kl4, bl4 bl4) {
        fl4 fl4 = (fl4) kl4.lastScheduledTask;
        if (fl4 == null || j - fl4.e < il4.a || !b.compareAndSet(kl4, fl4, (Object) null)) {
            return false;
        }
        a(fl4, bl4);
        return true;
    }

    @DexIgnore
    public final void a(bl4 bl4, fl4 fl4) {
        if (!bl4.a(fl4)) {
            throw new IllegalStateException("GlobalQueue could not be closed yet".toString());
        }
    }

    @DexIgnore
    public final void a(bl4 bl4) {
        fl4 fl4;
        wd4.b(bl4, "globalQueue");
        fl4 fl42 = (fl4) b.getAndSet(this, (Object) null);
        if (fl42 != null) {
            a(bl4, fl42);
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                fl4 = null;
            } else {
                int i2 = i & 127;
                if (((fl4) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                    fl4 = (fl4) this.a.getAndSet(i2, (Object) null);
                }
            }
            if (fl4 != null) {
                a(bl4, fl4);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(fl4 fl4) {
        if (a() == 127) {
            return false;
        }
        int i = this.producerIndex & 127;
        if (this.a.get(i) != null) {
            return false;
        }
        this.a.lazySet(i, fl4);
        c.incrementAndGet(this);
        return true;
    }
}
