package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ce1 {
    @DexIgnore
    public /* final */ ke1 a;
    @DexIgnore
    public ie1 b;

    @DexIgnore
    public ce1(ke1 ke1) {
        ck0.a(ke1);
        this.a = ke1;
    }

    @DexIgnore
    public final void a(ae1 ae1) {
        try {
            this.a.b(ae1.a());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final void b(ae1 ae1) {
        try {
            this.a.a(ae1.a());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final kf1 a(lf1 lf1) {
        try {
            h51 a2 = this.a.a(lf1);
            if (a2 != null) {
                return new kf1(a2);
            }
            return null;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final ie1 b() {
        try {
            if (this.b == null) {
                this.b = new ie1(this.a.m());
            }
            return this.b;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final void a() {
        try {
            this.a.clear();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
