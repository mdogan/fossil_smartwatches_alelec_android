package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o73 {
    @DexIgnore
    public /* final */ a93 a;
    @DexIgnore
    public /* final */ a83 b;
    @DexIgnore
    public /* final */ aa3 c;
    @DexIgnore
    public /* final */ ac3 d;
    @DexIgnore
    public /* final */ ad3 e;
    @DexIgnore
    public /* final */ ab3 f;

    @DexIgnore
    public o73(a93 a93, a83 a83, aa3 aa3, ac3 ac3, ad3 ad3, ab3 ab3) {
        wd4.b(a93, "mActivityView");
        wd4.b(a83, "mActiveTimeView");
        wd4.b(aa3, "mCaloriesView");
        wd4.b(ac3, "mHeartrateView");
        wd4.b(ad3, "mSleepView");
        wd4.b(ab3, "mGoalTrackingView");
        this.a = a93;
        this.b = a83;
        this.c = aa3;
        this.d = ac3;
        this.e = ad3;
        this.f = ab3;
    }

    @DexIgnore
    public final a83 a() {
        return this.b;
    }

    @DexIgnore
    public final a93 b() {
        return this.a;
    }

    @DexIgnore
    public final aa3 c() {
        return this.c;
    }

    @DexIgnore
    public final ab3 d() {
        return this.f;
    }

    @DexIgnore
    public final ac3 e() {
        return this.d;
    }

    @DexIgnore
    public final ad3 f() {
        return this.e;
    }
}
