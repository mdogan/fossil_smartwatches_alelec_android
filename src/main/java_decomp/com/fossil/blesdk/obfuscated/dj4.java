package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dj4 implements ai4, ug4 {
    @DexIgnore
    public static /* final */ dj4 e; // = new dj4();

    @DexIgnore
    public boolean a(Throwable th) {
        wd4.b(th, "cause");
        return false;
    }

    @DexIgnore
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
