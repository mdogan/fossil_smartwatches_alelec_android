package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb3 extends as2 implements gb3 {
    @DexIgnore
    public ju3 j;
    @DexIgnore
    public ur3<bc2> k;
    @DexIgnore
    public fb3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hb3 e;

        @DexIgnore
        public b(hb3 hb3, bc2 bc2) {
            this.e = hb3;
        }

        @DexIgnore
        public final void onClick(View view) {
            hb3.a(this.e).c().a(1);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ ju3 a(hb3 hb3) {
        ju3 ju3 = hb3.j;
        if (ju3 != null) {
            return ju3;
        }
        wd4.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void c(boolean z) {
        ur3<bc2> ur3 = this.k;
        if (ur3 != null) {
            bc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                wd4.a((Object) overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                wd4.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            wd4.a((Object) overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            wd4.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        bc2 bc2 = (bc2) ra.a(layoutInflater, R.layout.fragment_goal_tracking_overview_day, viewGroup, false, O0());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ju3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.j = (ju3) a2;
            bc2.s.setOnClickListener(new b(this, bc2));
        }
        this.k = new ur3<>(this, bc2);
        ur3<bc2> ur3 = this.k;
        if (ur3 != null) {
            bc2 a3 = ur3.a();
            if (a3 != null) {
                return a3.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        fb3 fb3 = this.l;
        if (fb3 != null) {
            fb3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        fb3 fb3 = this.l;
        if (fb3 != null) {
            fb3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(xr2 xr2, ArrayList<String> arrayList) {
        wd4.b(xr2, "baseModel");
        wd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + xr2);
        ur3<bc2> ur3 = this.k;
        if (ur3 != null) {
            bc2 a2 = ur3.a();
            if (a2 != null) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                if (overviewDayGoalChart != null) {
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) ml2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayGoalChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(fb3 fb3) {
        wd4.b(fb3, "presenter");
        this.l = fb3;
    }
}
