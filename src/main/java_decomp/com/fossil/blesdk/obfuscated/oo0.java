package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface oo0 extends IInterface {
    @DexIgnore
    tn0 a(tn0 tn0, String str, int i, tn0 tn02) throws RemoteException;

    @DexIgnore
    tn0 b(tn0 tn0, String str, int i, tn0 tn02) throws RemoteException;
}
