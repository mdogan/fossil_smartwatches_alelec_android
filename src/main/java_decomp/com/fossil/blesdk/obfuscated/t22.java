package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t22 extends j22 {
    @DexIgnore
    public int a() {
        return 3;
    }

    @DexIgnore
    public void a(o22 o22) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!o22.i()) {
                break;
            }
            char c = o22.c();
            o22.f++;
            a(c, sb);
            if (sb.length() % 3 == 0) {
                j22.b(o22, sb);
                int a = q22.a(o22.d(), o22.f, a());
                if (a != a()) {
                    o22.b(a);
                    break;
                }
            }
        }
        a(o22, sb);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == 13) {
            sb.append(0);
        } else if (c == '*') {
            sb.append(1);
        } else if (c == '>') {
            sb.append(2);
        } else if (c == ' ') {
            sb.append(3);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            q22.a(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    @DexIgnore
    public void a(o22 o22, StringBuilder sb) {
        o22.l();
        int a = o22.g().a() - o22.a();
        o22.f -= sb.length();
        if (o22.f() > 1 || a > 1 || o22.f() != a) {
            o22.a(254);
        }
        if (o22.e() < 0) {
            o22.b(0);
        }
    }
}
