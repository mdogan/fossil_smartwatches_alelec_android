package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dg1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ fg1 f;

    @DexIgnore
    public dg1(yh1 yh1, String str, String str2, String str3, long j, long j2, fg1 fg1) {
        ck0.b(str2);
        ck0.b(str3);
        ck0.a(fg1);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            yh1.d().v().a("Event created with reverse previous/current timestamps. appId, name", ug1.a(str2), ug1.a(str3));
        }
        this.f = fg1;
    }

    @DexIgnore
    public final dg1 a(yh1 yh1, long j) {
        return new dg1(yh1, this.c, this.a, this.b, this.d, j, this.f);
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public dg1(yh1 yh1, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        fg1 fg1;
        ck0.b(str2);
        ck0.b(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            yh1.d().v().a("Event created with reverse previous/current timestamps. appId", ug1.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            fg1 = new fg1(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String str4 = (String) it.next();
                if (str4 == null) {
                    yh1.d().s().a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = yh1.s().a(str4, bundle2.get(str4));
                    if (a2 == null) {
                        yh1.d().v().a("Param value can't be null", yh1.r().b(str4));
                        it.remove();
                    } else {
                        yh1.s().a(bundle2, str4, a2);
                    }
                }
            }
            fg1 = new fg1(bundle2);
        }
        this.f = fg1;
    }
}
