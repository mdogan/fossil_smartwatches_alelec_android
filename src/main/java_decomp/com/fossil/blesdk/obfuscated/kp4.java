package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kp4 extends Closeable {
    @DexIgnore
    long b(vo4 vo4, long j) throws IOException;

    @DexIgnore
    lp4 b();

    @DexIgnore
    void close() throws IOException;
}
