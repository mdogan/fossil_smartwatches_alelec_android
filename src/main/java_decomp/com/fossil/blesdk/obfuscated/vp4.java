package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vp4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ wp4 b;
    @DexIgnore
    public /* final */ cq4 c;

    @DexIgnore
    public vp4(String str, cq4 cq4) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (cq4 != null) {
            this.a = str;
            this.c = cq4;
            this.b = new wp4();
            a(cq4);
            b(cq4);
            c(cq4);
        } else {
            throw new IllegalArgumentException("Body may not be null");
        }
    }

    @DexIgnore
    public cq4 a() {
        return this.c;
    }

    @DexIgnore
    public wp4 b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (str != null) {
            this.b.a(new zp4(str, str2));
            return;
        }
        throw new IllegalArgumentException("Field name may not be null");
    }

    @DexIgnore
    public void b(cq4 cq4) {
        StringBuilder sb = new StringBuilder();
        sb.append(cq4.c());
        if (cq4.b() != null) {
            sb.append("; charset=");
            sb.append(cq4.b());
        }
        a(GraphRequest.CONTENT_TYPE_HEADER, sb.toString());
    }

    @DexIgnore
    public void c(cq4 cq4) {
        a("Content-Transfer-Encoding", cq4.a());
    }

    @DexIgnore
    public void a(cq4 cq4) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(c());
        sb.append("\"");
        if (cq4.d() != null) {
            sb.append("; filename=\"");
            sb.append(cq4.d());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }
}
