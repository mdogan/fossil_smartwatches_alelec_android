package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zztv;
import com.google.android.gms.internal.measurement.zzyb;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vb1 {
    @DexIgnore
    public /* final */ ByteBuffer a;
    @DexIgnore
    public zztv b;
    @DexIgnore
    public int c;

    @DexIgnore
    public vb1(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    @DexIgnore
    public static vb1 a(byte[] bArr, int i, int i2) {
        return new vb1(bArr, 0, i2);
    }

    @DexIgnore
    public static int b(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (j & Long.MIN_VALUE) == 0 ? 9 : 10;
    }

    @DexIgnore
    public static vb1 b(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int c(int i, long j) {
        return c(i) + b(j);
    }

    @DexIgnore
    public static int d(int i) {
        if (i >= 0) {
            return e(i);
        }
        return 10;
    }

    @DexIgnore
    public static int e(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    @DexIgnore
    public vb1(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
        this.a.order(ByteOrder.LITTLE_ENDIAN);
    }

    @DexIgnore
    public final zztv a() throws IOException {
        if (this.b == null) {
            this.b = zztv.a(this.a);
            this.c = this.a.position();
        } else if (this.c != this.a.position()) {
            this.b.b(this.a.array(), this.c, this.a.position() - this.c);
            this.c = this.a.position();
        }
        return this.b;
    }

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        a(i, 0);
        a(j);
    }

    @DexIgnore
    public static int c(int i, int i2) {
        return c(i) + d(i2);
    }

    @DexIgnore
    public static int c(int i) {
        return e(i << 3);
    }

    @DexIgnore
    public final void b(int i, int i2) throws IOException {
        a(i, 0);
        if (i2 >= 0) {
            b(i2);
        } else {
            a((long) i2);
        }
    }

    @DexIgnore
    public static int b(int i, String str) {
        return c(i) + a(str);
    }

    @DexIgnore
    public static int b(int i, bc1 bc1) {
        int c2 = c(i);
        int b2 = bc1.b();
        return c2 + e(b2) + b2;
    }

    @DexIgnore
    public final void a(int i, double d) throws IOException {
        a(i, 1);
        long doubleToLongBits = Double.doubleToLongBits(d);
        if (this.a.remaining() >= 8) {
            this.a.putLong(doubleToLongBits);
            return;
        }
        throw new zzyb(this.a.position(), this.a.limit());
    }

    @DexIgnore
    public final void b() {
        if (this.a.remaining() != 0) {
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[]{Integer.valueOf(this.a.remaining())}));
        }
    }

    @DexIgnore
    public final void a(int i, float f) throws IOException {
        a(i, 5);
        int floatToIntBits = Float.floatToIntBits(f);
        if (this.a.remaining() >= 4) {
            this.a.putInt(floatToIntBits);
            return;
        }
        throw new zzyb(this.a.position(), this.a.limit());
    }

    @DexIgnore
    public final void b(int i) throws IOException {
        while ((i & -128) != 0) {
            a((i & 127) | 128);
            i >>>= 7;
        }
        a(i);
    }

    @DexIgnore
    public final void a(int i, long j) throws IOException {
        a(i, 0);
        a(j);
    }

    @DexIgnore
    public final void a(int i, boolean z) throws IOException {
        a(i, 0);
        byte b2 = z ? (byte) 1 : 0;
        if (this.a.hasRemaining()) {
            this.a.put(b2);
            return;
        }
        throw new zzyb(this.a.position(), this.a.limit());
    }

    @DexIgnore
    public final void a(int i, String str) throws IOException {
        a(i, 2);
        try {
            int e = e(str.length());
            if (e == e(str.length() * 3)) {
                int position = this.a.position();
                if (this.a.remaining() >= e) {
                    this.a.position(position + e);
                    a((CharSequence) str, this.a);
                    int position2 = this.a.position();
                    this.a.position(position);
                    b((position2 - position) - e);
                    this.a.position(position2);
                    return;
                }
                throw new zzyb(position + e, this.a.limit());
            }
            b(a((CharSequence) str));
            a((CharSequence) str, this.a);
        } catch (BufferOverflowException e2) {
            zzyb zzyb = new zzyb(this.a.position(), this.a.limit());
            zzyb.initCause(e2);
            throw zzyb;
        }
    }

    @DexIgnore
    public final void a(int i, bc1 bc1) throws IOException {
        a(i, 2);
        a(bc1);
    }

    @DexIgnore
    public final void a(int i, x91 x91) throws IOException {
        zztv a2 = a();
        a2.a(i, x91);
        a2.a();
        this.c = this.a.position();
    }

    @DexIgnore
    public static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i2);
                                throw new IllegalArgumentException(sb.toString());
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        StringBuilder sb2 = new StringBuilder(54);
        sb2.append("UTF-8 length does not fit in int: ");
        sb2.append(((long) i3) + 4294967296L);
        throw new IllegalArgumentException(sb2.toString());
    }

    @DexIgnore
    public static void a(CharSequence charSequence, ByteBuffer byteBuffer) {
        int i;
        int i2;
        if (!byteBuffer.isReadOnly()) {
            int i3 = 0;
            if (byteBuffer.hasArray()) {
                try {
                    byte[] array = byteBuffer.array();
                    int arrayOffset = byteBuffer.arrayOffset() + byteBuffer.position();
                    int remaining = byteBuffer.remaining();
                    int length = charSequence.length();
                    int i4 = remaining + arrayOffset;
                    while (i3 < length) {
                        int i5 = i3 + arrayOffset;
                        if (i5 >= i4) {
                            break;
                        }
                        char charAt = charSequence.charAt(i3);
                        if (charAt >= 128) {
                            break;
                        }
                        array[i5] = (byte) charAt;
                        i3++;
                    }
                    if (i3 == length) {
                        i = arrayOffset + length;
                    } else {
                        i = arrayOffset + i3;
                        while (i3 < length) {
                            char charAt2 = charSequence.charAt(i3);
                            if (charAt2 < 128 && i < i4) {
                                i2 = i + 1;
                                array[i] = (byte) charAt2;
                            } else if (charAt2 < 2048 && i <= i4 - 2) {
                                int i6 = i + 1;
                                array[i] = (byte) ((charAt2 >>> 6) | 960);
                                i = i6 + 1;
                                array[i6] = (byte) ((charAt2 & '?') | 128);
                                i3++;
                            } else if ((charAt2 < 55296 || 57343 < charAt2) && i <= i4 - 3) {
                                int i7 = i + 1;
                                array[i] = (byte) ((charAt2 >>> 12) | 480);
                                int i8 = i7 + 1;
                                array[i7] = (byte) (((charAt2 >>> 6) & 63) | 128);
                                i2 = i8 + 1;
                                array[i8] = (byte) ((charAt2 & '?') | 128);
                            } else if (i <= i4 - 4) {
                                int i9 = i3 + 1;
                                if (i9 != charSequence.length()) {
                                    char charAt3 = charSequence.charAt(i9);
                                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                                        int i10 = i + 1;
                                        array[i] = (byte) ((codePoint >>> 18) | 240);
                                        int i11 = i10 + 1;
                                        array[i10] = (byte) (((codePoint >>> 12) & 63) | 128);
                                        int i12 = i11 + 1;
                                        array[i11] = (byte) (((codePoint >>> 6) & 63) | 128);
                                        i = i12 + 1;
                                        array[i12] = (byte) ((codePoint & 63) | 128);
                                        i3 = i9;
                                        i3++;
                                    } else {
                                        i3 = i9;
                                    }
                                }
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i3 - 1);
                                throw new IllegalArgumentException(sb.toString());
                            } else {
                                StringBuilder sb2 = new StringBuilder(37);
                                sb2.append("Failed writing ");
                                sb2.append(charAt2);
                                sb2.append(" at index ");
                                sb2.append(i);
                                throw new ArrayIndexOutOfBoundsException(sb2.toString());
                            }
                            i = i2;
                            i3++;
                        }
                    }
                    byteBuffer.position(i - byteBuffer.arrayOffset());
                } catch (ArrayIndexOutOfBoundsException e) {
                    BufferOverflowException bufferOverflowException = new BufferOverflowException();
                    bufferOverflowException.initCause(e);
                    throw bufferOverflowException;
                }
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt4 = charSequence.charAt(i3);
                    if (charAt4 < 128) {
                        byteBuffer.put((byte) charAt4);
                    } else if (charAt4 < 2048) {
                        byteBuffer.put((byte) ((charAt4 >>> 6) | 960));
                        byteBuffer.put((byte) ((charAt4 & '?') | 128));
                    } else if (charAt4 < 55296 || 57343 < charAt4) {
                        byteBuffer.put((byte) ((charAt4 >>> 12) | 480));
                        byteBuffer.put((byte) (((charAt4 >>> 6) & 63) | 128));
                        byteBuffer.put((byte) ((charAt4 & '?') | 128));
                    } else {
                        int i13 = i3 + 1;
                        if (i13 != charSequence.length()) {
                            char charAt5 = charSequence.charAt(i13);
                            if (Character.isSurrogatePair(charAt4, charAt5)) {
                                int codePoint2 = Character.toCodePoint(charAt4, charAt5);
                                byteBuffer.put((byte) ((codePoint2 >>> 18) | 240));
                                byteBuffer.put((byte) (((codePoint2 >>> 12) & 63) | 128));
                                byteBuffer.put((byte) (((codePoint2 >>> 6) & 63) | 128));
                                byteBuffer.put((byte) ((codePoint2 & 63) | 128));
                                i3 = i13;
                            } else {
                                i3 = i13;
                            }
                        }
                        StringBuilder sb3 = new StringBuilder(39);
                        sb3.append("Unpaired surrogate at index ");
                        sb3.append(i3 - 1);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    i3++;
                }
            }
        } else {
            throw new ReadOnlyBufferException();
        }
    }

    @DexIgnore
    public final void a(bc1 bc1) throws IOException {
        b(bc1.d());
        bc1.a(this);
    }

    @DexIgnore
    public static int a(String str) {
        int a2 = a((CharSequence) str);
        return e(a2) + a2;
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        byte b2 = (byte) i;
        if (this.a.hasRemaining()) {
            this.a.put(b2);
            return;
        }
        throw new zzyb(this.a.position(), this.a.limit());
    }

    @DexIgnore
    public final void a(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (this.a.remaining() >= length) {
            this.a.put(bArr, 0, length);
            return;
        }
        throw new zzyb(this.a.position(), this.a.limit());
    }

    @DexIgnore
    public final void a(int i, int i2) throws IOException {
        b((i << 3) | i2);
    }

    @DexIgnore
    public final void a(long j) throws IOException {
        while ((-128 & j) != 0) {
            a((((int) j) & 127) | 128);
            j >>>= 7;
        }
        a((int) j);
    }
}
