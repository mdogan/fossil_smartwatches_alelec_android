package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kb3 implements MembersInjector<GoalTrackingOverviewFragment> {
    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
        goalTrackingOverviewFragment.k = goalTrackingOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
        goalTrackingOverviewFragment.l = goalTrackingOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        goalTrackingOverviewFragment.m = goalTrackingOverviewMonthPresenter;
    }
}
