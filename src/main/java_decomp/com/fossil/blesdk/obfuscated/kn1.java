package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn1 extends ee0.a<xm1, wm1> {
    @DexIgnore
    public final /* synthetic */ ee0.f a(Context context, Looper looper, lj0 lj0, Object obj, he0.b bVar, he0.c cVar) {
        wm1 wm1 = (wm1) obj;
        if (wm1 == null) {
            wm1 = wm1.m;
        }
        return new xm1(context, looper, true, lj0, wm1, bVar, cVar);
    }
}
