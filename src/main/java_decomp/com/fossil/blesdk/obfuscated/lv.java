package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lv {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ oo<T> b;

        @DexIgnore
        public a(Class<T> cls, oo<T> ooVar) {
            this.a = cls;
            this.b = ooVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <Z> void a(Class<Z> cls, oo<Z> ooVar) {
        this.a.add(new a(cls, ooVar));
    }

    @DexIgnore
    public synchronized <Z> oo<Z> a(Class<Z> cls) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            a aVar = this.a.get(i);
            if (aVar.a(cls)) {
                return aVar.b;
            }
        }
        return null;
    }
}
