package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hv2 {
    @DexIgnore
    public /* final */ w73 a;
    @DexIgnore
    public /* final */ e23 b;
    @DexIgnore
    public /* final */ a63 c;
    @DexIgnore
    public /* final */ pg3 d;
    @DexIgnore
    public /* final */ tv2 e;
    @DexIgnore
    public /* final */ zy2 f;
    @DexIgnore
    public /* final */ hg3 g;

    @DexIgnore
    public hv2(w73 w73, e23 e23, a63 a63, pg3 pg3, tv2 tv2, zy2 zy2, hg3 hg3) {
        wd4.b(w73, "mDashboardView");
        wd4.b(e23, "mDianaCustomizeView");
        wd4.b(a63, "mHybridCustomizeView");
        wd4.b(pg3, "mProfileView");
        wd4.b(tv2, "mAlertsView");
        wd4.b(zy2, "mAlertsHybridView");
        wd4.b(hg3, "mUpdateFirmwareView");
        this.a = w73;
        this.b = e23;
        this.c = a63;
        this.d = pg3;
        this.e = tv2;
        this.f = zy2;
        this.g = hg3;
    }

    @DexIgnore
    public final zy2 a() {
        return this.f;
    }

    @DexIgnore
    public final tv2 b() {
        return this.e;
    }

    @DexIgnore
    public final w73 c() {
        return this.a;
    }

    @DexIgnore
    public final e23 d() {
        return this.b;
    }

    @DexIgnore
    public final a63 e() {
        return this.c;
    }

    @DexIgnore
    public final pg3 f() {
        return this.d;
    }

    @DexIgnore
    public final hg3 g() {
        return this.g;
    }
}
