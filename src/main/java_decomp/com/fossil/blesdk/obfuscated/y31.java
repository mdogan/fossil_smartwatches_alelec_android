package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class y31 extends h41 implements x31 {
    @DexIgnore
    public y31() {
        super("com.google.android.gms.location.internal.ISettingsCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((wc1) r41.a(parcel, wc1.CREATOR));
        return true;
    }
}
