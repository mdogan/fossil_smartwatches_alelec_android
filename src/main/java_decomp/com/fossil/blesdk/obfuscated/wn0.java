package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.un0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wn0<T extends un0> {
    @DexIgnore
    void a(T t);
}
