package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x93 implements Factory<ActivityOverviewWeekPresenter> {
    @DexIgnore
    public static ActivityOverviewWeekPresenter a(v93 v93, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActivityOverviewWeekPresenter(v93, userRepository, summariesRepository);
    }
}
