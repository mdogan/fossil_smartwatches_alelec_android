package com.fossil.blesdk.obfuscated;

import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class il {
    @DexIgnore
    public String a;
    @DexIgnore
    public WorkInfo.State b; // = WorkInfo.State.ENQUEUED;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public bj e;
    @DexIgnore
    public bj f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public zi j;
    @DexIgnore
    public int k;
    @DexIgnore
    public BackoffPolicy l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public long p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements m3<List<c>, List<WorkInfo>> {
        @DexIgnore
        /* renamed from: a */
        public List<WorkInfo> apply(List<c> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (c a : list) {
                arrayList.add(a.a());
            }
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public WorkInfo.State b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b != bVar.b) {
                return false;
            }
            return this.a.equals(bVar.a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public String a;
        @DexIgnore
        public WorkInfo.State b;
        @DexIgnore
        public bj c;
        @DexIgnore
        public int d;
        @DexIgnore
        public List<String> e;
        @DexIgnore
        public List<bj> f;

        @DexIgnore
        public WorkInfo a() {
            List<bj> list = this.f;
            return new WorkInfo(UUID.fromString(this.a), this.b, this.c, this.e, (list == null || list.isEmpty()) ? bj.c : this.f.get(0), this.d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.d != cVar.d) {
                return false;
            }
            String str = this.a;
            if (str == null ? cVar.a != null : !str.equals(cVar.a)) {
                return false;
            }
            if (this.b != cVar.b) {
                return false;
            }
            bj bjVar = this.c;
            if (bjVar == null ? cVar.c != null : !bjVar.equals(cVar.c)) {
                return false;
            }
            List<String> list = this.e;
            if (list == null ? cVar.e != null : !list.equals(cVar.e)) {
                return false;
            }
            List<bj> list2 = this.f;
            List<bj> list3 = cVar.f;
            if (list2 != null) {
                return list2.equals(list3);
            }
            if (list3 == null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            WorkInfo.State state = this.b;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            bj bjVar = this.c;
            int hashCode3 = (((hashCode2 + (bjVar != null ? bjVar.hashCode() : 0)) * 31) + this.d) * 31;
            List<String> list = this.e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            List<bj> list2 = this.f;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode4 + i;
        }
    }

    /*
    static {
        ej.a("WorkSpec");
        new a();
    }
    */

    @DexIgnore
    public il(String str, String str2) {
        bj bjVar = bj.c;
        this.e = bjVar;
        this.f = bjVar;
        this.j = zi.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = str;
        this.c = str2;
    }

    @DexIgnore
    public long a() {
        long j2;
        boolean z = false;
        if (c()) {
            if (this.l == BackoffPolicy.LINEAR) {
                z = true;
            }
            if (z) {
                j2 = this.m * ((long) this.k);
            } else {
                j2 = (long) Math.scalb((float) this.m, this.k - 1);
            }
            return this.n + Math.min(18000000, j2);
        }
        long j3 = 0;
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j4 = this.n;
            if (j4 == 0) {
                j4 = this.g + currentTimeMillis;
            }
            if (this.i != this.h) {
                z = true;
            }
            if (z) {
                if (this.n == 0) {
                    j3 = this.i * -1;
                }
                return j4 + this.h + j3;
            }
            if (this.n != 0) {
                j3 = this.h;
            }
            return j4 + j3;
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.g;
    }

    @DexIgnore
    public boolean b() {
        return !zi.i.equals(this.j);
    }

    @DexIgnore
    public boolean c() {
        return this.b == WorkInfo.State.ENQUEUED && this.k > 0;
    }

    @DexIgnore
    public boolean d() {
        return this.h != 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || il.class != obj.getClass()) {
            return false;
        }
        il ilVar = (il) obj;
        if (this.g != ilVar.g || this.h != ilVar.h || this.i != ilVar.i || this.k != ilVar.k || this.m != ilVar.m || this.n != ilVar.n || this.o != ilVar.o || this.p != ilVar.p || !this.a.equals(ilVar.a) || this.b != ilVar.b || !this.c.equals(ilVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null ? ilVar.d != null : !str.equals(ilVar.d)) {
            return false;
        }
        if (this.e.equals(ilVar.e) && this.f.equals(ilVar.f) && this.j.equals(ilVar.j) && this.l == ilVar.l) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31;
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j2 = this.g;
        long j3 = this.h;
        long j4 = this.i;
        long j5 = this.m;
        long j6 = this.n;
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.j.hashCode()) * 31) + this.k) * 31) + this.l.hashCode()) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "{WorkSpec: " + this.a + "}";
    }

    @DexIgnore
    public il(il ilVar) {
        bj bjVar = bj.c;
        this.e = bjVar;
        this.f = bjVar;
        this.j = zi.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = ilVar.a;
        this.c = ilVar.c;
        this.b = ilVar.b;
        this.d = ilVar.d;
        this.e = new bj(ilVar.e);
        this.f = new bj(ilVar.f);
        this.g = ilVar.g;
        this.h = ilVar.h;
        this.i = ilVar.i;
        this.j = new zi(ilVar.j);
        this.k = ilVar.k;
        this.l = ilVar.l;
        this.m = ilVar.m;
        this.n = ilVar.n;
        this.o = ilVar.o;
        this.p = ilVar.p;
    }
}
