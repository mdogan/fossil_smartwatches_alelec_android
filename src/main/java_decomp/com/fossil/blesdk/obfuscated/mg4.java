package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.VideoUploader;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.DeferredCoroutine;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class mg4 {
    @DexIgnore
    public static /* synthetic */ sh4 a(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4 kd4, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 2) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        return kg4.a(lh4, coroutineContext, coroutineStart, kd4);
    }

    @DexIgnore
    public static /* synthetic */ ri4 b(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4 kd4, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 2) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        return kg4.b(lh4, coroutineContext, coroutineStart, kd4);
    }

    @DexIgnore
    public static final <T> sh4<T> a(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4) {
        DeferredCoroutine deferredCoroutine;
        wd4.b(lh4, "$this$async");
        wd4.b(coroutineContext, "context");
        wd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(kd4, "block");
        CoroutineContext a = fh4.a(lh4, coroutineContext);
        if (coroutineStart.isLazy()) {
            deferredCoroutine = new zi4(a, kd4);
        } else {
            deferredCoroutine = new DeferredCoroutine(a, true);
        }
        deferredCoroutine.a(coroutineStart, deferredCoroutine, kd4);
        return deferredCoroutine;
    }

    @DexIgnore
    public static final ri4 b(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4<? super lh4, ? super kc4<? super cb4>, ? extends Object> kd4) {
        gg4 gg4;
        wd4.b(lh4, "$this$launch");
        wd4.b(coroutineContext, "context");
        wd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(kd4, "block");
        CoroutineContext a = fh4.a(lh4, coroutineContext);
        if (coroutineStart.isLazy()) {
            gg4 = new aj4(a, kd4);
        } else {
            gg4 = new jj4(a, true);
        }
        gg4.a(coroutineStart, gg4, kd4);
        return gg4;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> Object a(CoroutineContext coroutineContext, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4, kc4<? super T> kc4) {
        Object obj;
        CoroutineContext context = kc4.getContext();
        CoroutineContext plus = context.plus(coroutineContext);
        tj4.a(plus);
        if (plus == context) {
            mk4 mk4 = new mk4(plus, kc4);
            obj = xk4.a(mk4, mk4, kd4);
        } else if (wd4.a((Object) (lc4) plus.get(lc4.b), (Object) (lc4) context.get(lc4.b))) {
            sj4 sj4 = new sj4(plus, kc4);
            Object b = ThreadContextKt.b(plus, (Object) null);
            try {
                Object a = xk4.a(sj4, sj4, kd4);
                ThreadContextKt.a(plus, b);
                obj = a;
            } catch (Throwable th) {
                ThreadContextKt.a(plus, b);
                throw th;
            }
        } else {
            wh4 wh4 = new wh4(plus, kc4);
            wh4.k();
            wk4.a(kd4, wh4, wh4);
            obj = wh4.m();
        }
        if (obj == oc4.a()) {
            uc4.c(kc4);
        }
        return obj;
    }
}
