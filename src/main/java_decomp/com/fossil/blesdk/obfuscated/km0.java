package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class km0 {
    /*
    static {
        new String[]{"android.", "com.android.", "dalvik.", "java.", "javax."};
    }
    */

    @DexIgnore
    public static boolean a(Context context, Throwable th) {
        return a(context, th, 536870912);
    }

    @DexIgnore
    public static boolean a(Context context, Throwable th, int i) {
        try {
            ck0.a(context);
            ck0.a(th);
            return false;
        } catch (Exception e) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
            return false;
        }
    }
}
