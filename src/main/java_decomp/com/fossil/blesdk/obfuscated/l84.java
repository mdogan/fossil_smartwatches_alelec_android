package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l84 {

    @DexIgnore
    public interface a {
        @DexIgnore
        k84 a(j84 j84);

        @DexIgnore
        j84 n();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();
    }

    /*
    static {
        b bVar = b.a;
    }
    */

    @DexIgnore
    k84 intercept(a aVar);
}
