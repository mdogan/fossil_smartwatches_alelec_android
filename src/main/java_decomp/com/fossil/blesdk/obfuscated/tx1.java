package com.fossil.blesdk.obfuscated;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class tx1 implements lw1 {
    @DexIgnore
    public static /* final */ lw1 a; // = new tx1();

    @DexIgnore
    public final Object a(kw1 kw1) {
        return new FirebaseInstanceId((FirebaseApp) kw1.a(FirebaseApp.class), (cx1) kw1.a(cx1.class));
    }
}
