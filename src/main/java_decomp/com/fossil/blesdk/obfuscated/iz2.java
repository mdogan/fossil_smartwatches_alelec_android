package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.dz2;
import com.fossil.blesdk.obfuscated.ws2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iz2 extends bs2 implements hz2, xs3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public gz2 k;
    @DexIgnore
    public ur3<vd2> l;
    @DexIgnore
    public ws2 m;
    @DexIgnore
    public dz2 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return iz2.p;
        }

        @DexIgnore
        public final iz2 b() {
            return new iz2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements dz2.b {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 a;

        @DexIgnore
        public c(iz2 iz2) {
            this.a = iz2;
        }

        @DexIgnore
        public void a(int i, boolean z, boolean z2) {
            iz2.b(this.a).a(i, z, z2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 e;

        @DexIgnore
        public d(iz2 iz2) {
            this.e = iz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 e;

        @DexIgnore
        public e(iz2 iz2) {
            this.e = iz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 e;

        @DexIgnore
        public f(iz2 iz2) {
            this.e = iz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            iz2.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 e;

        @DexIgnore
        public g(iz2 iz2) {
            this.e = iz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            iz2.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 e;

        @DexIgnore
        public h(iz2 iz2) {
            this.e = iz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            iz2.b(this.e).l();
        }
    }

    /*
    static {
        String simpleName = iz2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gz2 b(iz2 iz2) {
        gz2 gz2 = iz2.k;
        if (gz2 != null) {
            return gz2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        T0();
        return true;
    }

    @DexIgnore
    public void T0() {
        ur3<vd2> ur3 = this.l;
        if (ur3 == null) {
            wd4.d("mBinding");
            throw null;
        } else if (ur3.a() != null) {
            gz2 gz2 = this.k;
            if (gz2 == null) {
                wd4.d("mPresenter");
                throw null;
            } else if (!gz2.i()) {
                close();
            } else {
                es3 es3 = es3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                es3.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    public final void U0() {
        ws2 ws2 = this.m;
        if (ws2 == null) {
            wd4.d("mAdapter");
            throw null;
        } else if (ws2.b()) {
            gz2 gz2 = this.k;
            if (gz2 != null) {
                gz2.m();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager);
        }
    }

    @DexIgnore
    public void c(int i, ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public void f(List<Object> list) {
        wd4.b(list, "contactAndAppData");
        ws2 ws2 = this.m;
        if (ws2 != null) {
            ws2.a((List<? extends Object>) list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                ur3<vd2> ur3 = this.l;
                if (ur3 != null) {
                    vd2 a2 = ur3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.q;
                        wd4.a((Object) flexibleTextView, "it.ftvAssignSection");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.x;
                        wd4.a((Object) recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            ur3<vd2> ur32 = this.l;
            if (ur32 != null) {
                vd2 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.q;
                    wd4.a((Object) flexibleTextView2, "it.ftvAssignSection");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a3.x;
                    wd4.a((Object) recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public void h(int i) {
        ur3<vd2> ur3 = this.l;
        if (ur3 != null) {
            vd2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    be4 be4 = be4.a;
                    String a3 = tm2.a(getContext(), (int) R.string.AlertsHybrid_AssignNotifications_Select_Title__AssignToNumber);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                    Object[] objArr = {Integer.valueOf(i)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j() {
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                wd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 4567) {
            if (i != 5678) {
                if (i == 6789 && i2 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList arrayList = (ArrayList) serializableExtra;
                        gz2 gz2 = this.k;
                        if (gz2 != null) {
                            gz2.b(arrayList);
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i2 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList arrayList2 = (ArrayList) serializableExtra2;
                    gz2 gz22 = this.k;
                    if (gz22 != null) {
                        gz22.a((ArrayList<ContactWrapper>) arrayList2);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i2 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            gz2 gz23 = this.k;
            if (gz23 != null) {
                wd4.a((Object) stringArrayListExtra, "stringAppsSelected");
                gz23.c(stringArrayListExtra);
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        vd2 vd2 = (vd2) ra.a(layoutInflater, R.layout.fragment_notification_contacts_and_apps_assigned, viewGroup, false, O0());
        this.n = (dz2) getChildFragmentManager().a(dz2.w.a());
        if (this.n == null) {
            this.n = dz2.w.b();
        }
        dz2 dz2 = this.n;
        if (dz2 != null) {
            dz2.a(new c(this));
        }
        FlexibleTextView flexibleTextView = vd2.q;
        wd4.a((Object) flexibleTextView, "binding.ftvAssignSection");
        flexibleTextView.setVisibility(8);
        RecyclerView recyclerView = vd2.x;
        wd4.a((Object) recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = vd2.t;
        wd4.a((Object) imageView, "binding.ivDone");
        Context context = getContext();
        if (context != null) {
            imageView.setBackground(k6.c(context, R.drawable.button_radious_color_grey));
            vd2.s.setOnClickListener(new d(this));
            vd2.t.setOnClickListener(new e(this));
            vd2.w.setOnClickListener(new f(this));
            vd2.v.setOnClickListener(new g(this));
            vd2.u.setOnClickListener(new h(this));
            ws2 ws2 = new ws2();
            ws2.a((ws2.c) new b(this));
            this.m = ws2;
            RecyclerView recyclerView2 = vd2.x;
            recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
            recyclerView2.setHasFixedSize(true);
            ws2 ws22 = this.m;
            if (ws22 != null) {
                recyclerView2.setAdapter(ws22);
                this.l = new ur3<>(this, vd2);
                wd4.a((Object) vd2, "binding");
                return vd2.d();
            }
            wd4.d("mAdapter");
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        gz2 gz2 = this.k;
        if (gz2 != null) {
            gz2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        gz2 gz2 = this.k;
        if (gz2 != null) {
            gz2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void p(boolean z) {
        if (isActive()) {
            ur3<vd2> ur3 = this.l;
            if (ur3 != null) {
                vd2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.t;
                    if (imageView != null) {
                        wd4.a((Object) imageView, "doneButton");
                        imageView.setEnabled(z);
                        imageView.setClickable(z);
                        if (z) {
                            Context context = getContext();
                            if (context != null) {
                                imageView.setBackground(k6.c(context, R.color.primaryColor));
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            Context context2 = getContext();
                            if (context2 != null) {
                                imageView.setBackground(k6.c(context2, R.color.disabledButton));
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(int i, ArrayList<String> arrayList) {
        wd4.b(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void a(gz2 gz2) {
        wd4.b(gz2, "presenter");
        this.k = gz2;
    }

    @DexIgnore
    public void a(int i, ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i == R.id.tv_cancel) {
            close();
        } else if (i == R.id.tv_ok) {
            U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ws2.c {
        @DexIgnore
        public /* final */ /* synthetic */ iz2 a;

        @DexIgnore
        public b(iz2 iz2) {
            this.a = iz2;
        }

        @DexIgnore
        public void a(ContactWrapper contactWrapper) {
            wd4.b(contactWrapper, "contactWrapper");
            dz2 a2 = this.a.n;
            if (a2 != null) {
                Contact contact = contactWrapper.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = contactWrapper.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = contactWrapper.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            a2.a(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            dz2 a3 = this.a.n;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, dz2.w.a());
            }
        }

        @DexIgnore
        public void a() {
            iz2.b(this.a).h();
        }
    }
}
