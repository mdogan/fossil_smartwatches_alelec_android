package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dm4 {
    @DexIgnore
    public static final dm4 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements dm4 {
        @DexIgnore
        public List<cm4> a(lm4 lm4) {
            return Collections.emptyList();
        }

        @DexIgnore
        public void a(lm4 lm4, List<cm4> list) {
        }
    }

    @DexIgnore
    List<cm4> a(lm4 lm4);

    @DexIgnore
    void a(lm4 lm4, List<cm4> list);
}
