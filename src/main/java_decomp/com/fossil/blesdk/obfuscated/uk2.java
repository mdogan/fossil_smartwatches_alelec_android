package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uk2 implements MembersInjector<DeviceHelper> {
    @DexIgnore
    public static void a(DeviceHelper deviceHelper, fn2 fn2) {
        deviceHelper.a = fn2;
    }

    @DexIgnore
    public static void a(DeviceHelper deviceHelper, DeviceRepository deviceRepository) {
        deviceHelper.b = deviceRepository;
    }
}
