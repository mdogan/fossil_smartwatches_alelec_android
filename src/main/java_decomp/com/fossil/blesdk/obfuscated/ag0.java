package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ag0 extends an1 {
    @DexIgnore
    public /* final */ WeakReference<tf0> e;

    @DexIgnore
    public ag0(tf0 tf0) {
        this.e = new WeakReference<>(tf0);
    }

    @DexIgnore
    public final void a(hn1 hn1) {
        tf0 tf0 = (tf0) this.e.get();
        if (tf0 != null) {
            tf0.a.a((pg0) new bg0(this, tf0, tf0, hn1));
        }
    }
}
