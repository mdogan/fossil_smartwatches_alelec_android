package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tt3 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public tt3(int i, int i2, int i3, int i4) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.a = this.d == this.e;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final boolean e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof tt3) {
                tt3 tt3 = (tt3) obj;
                if (this.b == tt3.b) {
                    if (this.c == tt3.c) {
                        if (this.d == tt3.d) {
                            if (this.e == tt3.e) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    public String toString() {
        return "DashBarInformation(length=" + this.b + ", progress=" + this.c + ", animStart=" + this.d + ", animEnd=" + this.e + ")";
    }
}
