package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s03 implements Factory<NotificationHybridContactPresenter> {
    @DexIgnore
    public static NotificationHybridContactPresenter a(m03 m03, int i, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager, k62 k62, u03 u03) {
        return new NotificationHybridContactPresenter(m03, i, arrayList, loaderManager, k62, u03);
    }
}
