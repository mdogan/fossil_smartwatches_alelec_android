package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ik1 e;

    @DexIgnore
    public mk1(ik1 ik1) {
        this.e = ik1;
    }

    @DexIgnore
    public final void run() {
        wj1 wj1 = this.e.c;
        Context context = wj1.getContext();
        this.e.c.b();
        wj1.a(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
