package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ic3 implements Factory<HeartRateOverviewDayPresenter> {
    @DexIgnore
    public static HeartRateOverviewDayPresenter a(gc3 gc3, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new HeartRateOverviewDayPresenter(gc3, heartRateSampleRepository, workoutSessionRepository);
    }
}
