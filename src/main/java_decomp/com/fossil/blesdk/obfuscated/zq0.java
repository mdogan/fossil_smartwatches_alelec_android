package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zq0 extends v21 implements yq0 {
    @DexIgnore
    public zq0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    @DexIgnore
    public final void d(int i) throws RemoteException {
        Parcel o = o();
        o.writeInt(i);
        a(2, o);
    }
}
