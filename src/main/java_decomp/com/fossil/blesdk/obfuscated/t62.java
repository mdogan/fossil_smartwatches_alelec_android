package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t62 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ ArrayList<Explore> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleTextView a;
        @DexIgnore
        public ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(t62 t62, View view) {
            super(view);
            wd4.b(view, "view");
            this.a = (FlexibleTextView) view.findViewById(R.id.ftv_content);
            this.b = (ImageView) view.findViewById(R.id.iv_icon);
        }

        @DexIgnore
        public final void a(Explore explore, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareTutorialAdapter", "build - position=" + i);
            if (explore != null) {
                FlexibleTextView flexibleTextView = this.a;
                wd4.a((Object) flexibleTextView, "ftvContent");
                flexibleTextView.setText(explore.getDescription());
                ImageView imageView = this.b;
                wd4.a((Object) imageView, "ivIcon");
                imageView.setBackground(k6.c(PortfolioApp.W.c(), explore.getBackground()));
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public t62(ArrayList<Explore> arrayList) {
        wd4.b(arrayList, "mData");
        this.a = arrayList;
    }

    @DexIgnore
    public final void a(List<? extends Explore> list) {
        wd4.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        if (getItemCount() > i && i != -1) {
            ((b) viewHolder).a(this.a.get(i), i);
        }
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_firmware_tutorial, viewGroup, false);
        wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026_tutorial, parent, false)");
        return new b(this, inflate);
    }
}
