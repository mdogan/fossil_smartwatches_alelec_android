package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ly2 extends ys3 implements ky2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public /* final */ qa m; // = new w62(this);
    @DexIgnore
    public ur3<hf2> n;
    @DexIgnore
    public jy2 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ly2.q;
        }

        @DexIgnore
        public final ly2 b() {
            return new ly2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ly2 a;
        @DexIgnore
        public /* final */ /* synthetic */ hf2 b;

        @DexIgnore
        public b(ly2 ly2, hf2 hf2) {
            this.a = ly2;
            this.b = hf2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            jy2 a2 = ly2.a(this.a);
            NumberPicker numberPicker2 = this.b.s;
            wd4.a((Object) numberPicker2, "binding.numberPicker");
            a2.a(numberPicker2.getValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ly2 e;

        @DexIgnore
        public c(ly2 ly2) {
            this.e = ly2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ly2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ hf2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public d(hf2 hf2, Ref$ObjectRef ref$ObjectRef) {
            this.e = hf2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.e.q;
            wd4.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.c(3);
                        hf2 hf2 = this.e;
                        wd4.a((Object) hf2, "it");
                        View d = hf2.d();
                        wd4.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = ly2.class.getSimpleName();
        wd4.a((Object) simpleName, "RemindTimeFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ jy2 a(ly2 ly2) {
        jy2 jy2 = ly2.o;
        if (jy2 != null) {
            return jy2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        hf2 hf2 = (hf2) ra.a(layoutInflater, R.layout.fragment_remind_time, viewGroup, false, this.m);
        hf2.r.setOnClickListener(new c(this));
        wd4.a((Object) hf2, "binding");
        a(hf2);
        this.n = new ur3<>(this, hf2);
        return hf2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        jy2 jy2 = this.o;
        if (jy2 != null) {
            jy2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        jy2 jy2 = this.o;
        if (jy2 != null) {
            jy2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<hf2> ur3 = this.n;
        if (ur3 != null) {
            hf2 a2 = ur3.a();
            if (a2 != null) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                ref$ObjectRef.element = new d(a2, ref$ObjectRef);
                wd4.a((Object) a2, "it");
                View d2 = a2.d();
                wd4.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(jy2 jy2) {
        wd4.b(jy2, "presenter");
        this.o = jy2;
    }

    @DexIgnore
    public final void a(hf2 hf2) {
        ArrayList arrayList = new ArrayList();
        ie4 a2 = qe4.a((ie4) new ke4(20, 120), 20);
        int a3 = a2.a();
        int b2 = a2.b();
        int c2 = a2.c();
        if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
            while (true) {
                String d2 = ol2.d(a3);
                wd4.a((Object) d2, "timeString");
                arrayList.add(d2);
                if (a3 == b2) {
                    break;
                }
                a3 += c2;
            }
        }
        NumberPicker numberPicker = hf2.s;
        wd4.a((Object) numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = hf2.s;
        wd4.a((Object) numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = hf2.s;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            hf2.s.setOnValueChangedListener(new b(this, hf2));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(int i) {
        ur3<hf2> ur3 = this.n;
        if (ur3 != null) {
            hf2 a2 = ur3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.s;
                if (numberPicker != null) {
                    numberPicker.setValue(i / 20);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
