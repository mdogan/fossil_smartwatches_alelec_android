package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ny0 extends pj0<ry0> {
    @DexIgnore
    public ny0(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 40, lj0, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.clearcut.internal.IClearcutLoggerService");
        return queryLocalInterface instanceof ry0 ? (ry0) queryLocalInterface : new sy0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return 11925000;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.clearcut.service.START";
    }
}
