package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.eg3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hd3 extends as2 implements gd3 {
    @DexIgnore
    public ur3<vf2> j;
    @DexIgnore
    public fd3 k;
    @DexIgnore
    public eg3 l;
    @DexIgnore
    public int m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public static /* final */ c e; // = new c();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ hd3 a;

        @DexIgnore
        public d(hd3 hd3) {
            this.a = hd3;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            wd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            View childAt = recyclerView.getChildAt(0);
            wd4.a((Object) childAt, "it.getChildAt(0)");
            int measuredWidth = childAt.getMeasuredWidth();
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "initUI - offset=" + computeHorizontalScrollOffset);
            if (measuredWidth > 0 && computeHorizontalScrollOffset % measuredWidth == 0) {
                int i3 = computeHorizontalScrollOffset / measuredWidth;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepDetailFragment", "initUI - position=" + i3);
                if (i3 != -1) {
                    this.a.m = i3;
                }
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        vf2 vf2 = (vf2) ra.a(layoutInflater, R.layout.fragment_sleep_overview_day, viewGroup, false, O0());
        wd4.a((Object) vf2, "binding");
        a(vf2);
        this.j = new ur3<>(this, vf2);
        ur3<vf2> ur3 = this.j;
        if (ur3 != null) {
            vf2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        fd3 fd3 = this.k;
        if (fd3 != null) {
            fd3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        fd3 fd3 = this.k;
        if (fd3 != null) {
            fd3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void s(List<eg3.b> list) {
        wd4.b(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            ur3<vf2> ur3 = this.j;
            if (ur3 != null) {
                vf2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.u;
                    if (flexibleTextView != null) {
                        flexibleTextView.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        ur3<vf2> ur32 = this.j;
        if (ur32 != null) {
            vf2 a3 = ur32.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView2 = a3.u;
                if (flexibleTextView2 != null) {
                    flexibleTextView2.setVisibility(8);
                }
            }
        }
        if (list.size() > 1) {
            ur3<vf2> ur33 = this.j;
            if (ur33 != null) {
                vf2 a4 = ur33.a();
                if (a4 != null) {
                    CustomPageIndicator customPageIndicator = a4.q;
                    if (customPageIndicator != null) {
                        customPageIndicator.setVisibility(0);
                    }
                }
            }
        } else {
            ur3<vf2> ur34 = this.j;
            if (ur34 != null) {
                vf2 a5 = ur34.a();
                if (a5 != null) {
                    CustomPageIndicator customPageIndicator2 = a5.q;
                    if (customPageIndicator2 != null) {
                        customPageIndicator2.setVisibility(8);
                    }
                }
            }
        }
        eg3 eg3 = this.l;
        if (eg3 != null) {
            eg3.a(list);
        }
    }

    @DexIgnore
    public void a(fd3 fd3) {
        wd4.b(fd3, "presenter");
        this.k = fd3;
    }

    @DexIgnore
    public final void a(vf2 vf2) {
        vf2.r.setOnClickListener(b.e);
        vf2.s.setOnClickListener(c.e);
        this.l = new eg3(new ArrayList());
        RecyclerViewPager recyclerViewPager = vf2.t;
        wd4.a((Object) recyclerViewPager, "binding.rvSleeps");
        recyclerViewPager.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        RecyclerViewPager recyclerViewPager2 = vf2.t;
        wd4.a((Object) recyclerViewPager2, "binding.rvSleeps");
        recyclerViewPager2.setAdapter(this.l);
        vf2.t.a((RecyclerView.q) new d(this));
        vf2.t.i(this.m);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CustomPageIndicator.a(1, 0, 2, (rd4) null));
        vf2.q.a((RecyclerView) vf2.t, this.m, (List<CustomPageIndicator.a>) arrayList);
    }
}
