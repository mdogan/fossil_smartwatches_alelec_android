package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import java.io.IOException;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zx extends q64<SessionEvent> {
    @DexIgnore
    public k74 g;

    @DexIgnore
    public zx(Context context, ey eyVar, o54 o54, r64 r64) throws IOException {
        super(context, eyVar, o54, r64, 100);
    }

    @DexIgnore
    public void a(k74 k74) {
        this.g = k74;
    }

    @DexIgnore
    public String c() {
        UUID randomUUID = UUID.randomUUID();
        return "sa" + "_" + randomUUID.toString() + "_" + this.c.a() + ".tap";
    }

    @DexIgnore
    public int e() {
        k74 k74 = this.g;
        return k74 == null ? super.e() : k74.c;
    }

    @DexIgnore
    public int f() {
        k74 k74 = this.g;
        return k74 == null ? super.f() : k74.d;
    }
}
