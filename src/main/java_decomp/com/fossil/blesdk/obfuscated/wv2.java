package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wv2 {
    @DexIgnore
    public /* final */ ty2 a;

    @DexIgnore
    public wv2(ty2 ty2) {
        wd4.b(ty2, "mDNDScheduledTimeView");
        this.a = ty2;
    }

    @DexIgnore
    public final ty2 a() {
        return this.a;
    }
}
