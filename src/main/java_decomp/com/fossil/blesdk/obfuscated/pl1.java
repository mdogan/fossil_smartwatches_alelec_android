package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pl1 extends SSLSocketFactory {
    @DexIgnore
    public /* final */ SSLSocketFactory a;

    @DexIgnore
    public pl1() {
        this(HttpsURLConnection.getDefaultSSLSocketFactory());
    }

    @DexIgnore
    public final SSLSocket a(SSLSocket sSLSocket) {
        return new ql1(this, sSLSocket);
    }

    @DexIgnore
    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a((SSLSocket) this.a.createSocket(socket, str, i, z));
    }

    @DexIgnore
    public final String[] getDefaultCipherSuites() {
        return this.a.getDefaultCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.a.getSupportedCipherSuites();
    }

    @DexIgnore
    public pl1(SSLSocketFactory sSLSocketFactory) {
        this.a = sSLSocketFactory;
    }

    @DexIgnore
    public final Socket createSocket(String str, int i) throws IOException {
        return a((SSLSocket) this.a.createSocket(str, i));
    }

    @DexIgnore
    public final Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a((SSLSocket) this.a.createSocket(inetAddress, i));
    }

    @DexIgnore
    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a((SSLSocket) this.a.createSocket(str, i, inetAddress, i2));
    }

    @DexIgnore
    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a((SSLSocket) this.a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    @DexIgnore
    public final Socket createSocket() throws IOException {
        return a((SSLSocket) this.a.createSocket());
    }
}
