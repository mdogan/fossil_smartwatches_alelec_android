package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.p81;
import com.google.android.gms.internal.measurement.zzxs;
import com.google.android.gms.internal.measurement.zzxx;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface p81<T extends p81<T>> extends Comparable<T> {
    @DexIgnore
    ea1 a(ea1 ea1, ea1 ea12);

    @DexIgnore
    y91 a(y91 y91, x91 x91);

    @DexIgnore
    boolean e();

    @DexIgnore
    zzxs f();

    @DexIgnore
    boolean g();

    @DexIgnore
    zzxx h();

    @DexIgnore
    int zzc();
}
