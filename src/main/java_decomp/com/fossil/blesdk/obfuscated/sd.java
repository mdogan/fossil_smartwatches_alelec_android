package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.blesdk.obfuscated.jd;
import com.fossil.blesdk.obfuscated.me;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sd<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ jd<T> a;
    @DexIgnore
    public /* final */ jd.c<T> b; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jd.c<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(rd<T> rdVar, rd<T> rdVar2) {
            sd.this.a(rdVar2);
            sd.this.a(rdVar, rdVar2);
        }
    }

    @DexIgnore
    public sd(me.d<T> dVar) {
        this.a = new jd<>(this, dVar);
        this.a.a(this.b);
    }

    @DexIgnore
    public T a(int i) {
        return this.a.a(i);
    }

    @DexIgnore
    @Deprecated
    public void a(rd<T> rdVar) {
    }

    @DexIgnore
    public void a(rd<T> rdVar, rd<T> rdVar2) {
    }

    @DexIgnore
    public void b(rd<T> rdVar) {
        this.a.a(rdVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.a();
    }
}
