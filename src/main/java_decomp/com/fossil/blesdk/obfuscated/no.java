package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface no<T, Z> {
    @DexIgnore
    bq<Z> a(T t, int i, int i2, mo moVar) throws IOException;

    @DexIgnore
    boolean a(T t, mo moVar) throws IOException;
}
