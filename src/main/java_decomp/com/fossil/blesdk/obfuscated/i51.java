package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i51 extends c51 implements h51 {
    @DexIgnore
    public static h51 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IMarkerDelegate");
        if (queryLocalInterface instanceof h51) {
            return (h51) queryLocalInterface;
        }
        return new j51(iBinder);
    }
}
