package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bk implements nj {
    @DexIgnore
    public static /* final */ String o; // = ej.a("SystemAlarmDispatcher");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ am f;
    @DexIgnore
    public /* final */ dk g;
    @DexIgnore
    public /* final */ pj h;
    @DexIgnore
    public /* final */ uj i;
    @DexIgnore
    public /* final */ yj j;
    @DexIgnore
    public /* final */ Handler k;
    @DexIgnore
    public /* final */ List<Intent> l;
    @DexIgnore
    public Intent m;
    @DexIgnore
    public c n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            d dVar;
            bk bkVar;
            synchronized (bk.this.l) {
                bk.this.m = bk.this.l.get(0);
            }
            Intent intent = bk.this.m;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = bk.this.m.getIntExtra("KEY_START_ID", 0);
                ej.a().a(bk.o, String.format("Processing command %s, %s", new Object[]{bk.this.m, Integer.valueOf(intExtra)}), new Throwable[0]);
                PowerManager.WakeLock a = xl.a(bk.this.e, String.format("%s (%s)", new Object[]{action, Integer.valueOf(intExtra)}));
                try {
                    ej.a().a(bk.o, String.format("Acquiring operation wake lock (%s) %s", new Object[]{action, a}), new Throwable[0]);
                    a.acquire();
                    bk.this.j.g(bk.this.m, intExtra, bk.this);
                    ej.a().a(bk.o, String.format("Releasing operation wake lock (%s) %s", new Object[]{action, a}), new Throwable[0]);
                    a.release();
                    bkVar = bk.this;
                    dVar = new d(bkVar);
                } catch (Throwable th) {
                    ej.a().a(bk.o, String.format("Releasing operation wake lock (%s) %s", new Object[]{action, a}), new Throwable[0]);
                    a.release();
                    bk bkVar2 = bk.this;
                    bkVar2.a((Runnable) new d(bkVar2));
                    throw th;
                }
                bkVar.a((Runnable) dVar);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ bk e;
        @DexIgnore
        public /* final */ Intent f;
        @DexIgnore
        public /* final */ int g;

        @DexIgnore
        public b(bk bkVar, Intent intent, int i) {
            this.e = bkVar;
            this.f = intent;
            this.g = i;
        }

        @DexIgnore
        public void run() {
            this.e.a(this.f, this.g);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ bk e;

        @DexIgnore
        public d(bk bkVar) {
            this.e = bkVar;
        }

        @DexIgnore
        public void run() {
            this.e.b();
        }
    }

    @DexIgnore
    public bk(Context context) {
        this(context, (pj) null, (uj) null);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        a((Runnable) new b(this, yj.a(this.e, str, z), 0));
    }

    @DexIgnore
    public void b() {
        ej.a().a(o, "Checking if commands are complete.", new Throwable[0]);
        a();
        synchronized (this.l) {
            if (this.m != null) {
                ej.a().a(o, String.format("Removing command %s", new Object[]{this.m}), new Throwable[0]);
                if (this.l.remove(0).equals(this.m)) {
                    this.m = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            ul b2 = this.f.b();
            if (!this.j.a() && this.l.isEmpty() && !b2.a()) {
                ej.a().a(o, "No more commands & intents.", new Throwable[0]);
                if (this.n != null) {
                    this.n.a();
                }
            } else if (!this.l.isEmpty()) {
                h();
            }
        }
    }

    @DexIgnore
    public pj c() {
        return this.h;
    }

    @DexIgnore
    public am d() {
        return this.f;
    }

    @DexIgnore
    public uj e() {
        return this.i;
    }

    @DexIgnore
    public dk f() {
        return this.g;
    }

    @DexIgnore
    public void g() {
        ej.a().a(o, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.h.b((nj) this);
        this.g.a();
        this.n = null;
    }

    @DexIgnore
    public final void h() {
        a();
        PowerManager.WakeLock a2 = xl.a(this.e, "ProcessCommand");
        try {
            a2.acquire();
            this.i.h().a(new a());
        } finally {
            a2.release();
        }
    }

    @DexIgnore
    public bk(Context context, pj pjVar, uj ujVar) {
        this.e = context.getApplicationContext();
        this.j = new yj(this.e);
        this.g = new dk();
        this.i = ujVar == null ? uj.a(context) : ujVar;
        this.h = pjVar == null ? this.i.e() : pjVar;
        this.f = this.i.h();
        this.h.a((nj) this);
        this.l = new ArrayList();
        this.m = null;
        this.k = new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public boolean a(Intent intent, int i2) {
        boolean z = false;
        ej.a().a(o, String.format("Adding command %s (%s)", new Object[]{intent, Integer.valueOf(i2)}), new Throwable[0]);
        a();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            ej.a().e(o, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && a("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.l) {
                if (!this.l.isEmpty()) {
                    z = true;
                }
                this.l.add(intent);
                if (!z) {
                    h();
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        if (this.n != null) {
            ej.a().b(o, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.n = cVar;
        }
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.k.post(runnable);
    }

    @DexIgnore
    public final boolean a(String str) {
        a();
        synchronized (this.l) {
            for (Intent action : this.l) {
                if (str.equals(action.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public final void a() {
        if (this.k.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }
}
