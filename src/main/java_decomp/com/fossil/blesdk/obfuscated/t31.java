package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class t31 extends h41 implements s31 {
    @DexIgnore
    public t31() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((p31) r41.a(parcel, p31.CREATOR));
        return true;
    }
}
