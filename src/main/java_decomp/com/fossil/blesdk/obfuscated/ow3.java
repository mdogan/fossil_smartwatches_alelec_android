package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.iv3;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ow3 implements ru3 {
    @DexIgnore
    public static /* final */ ru3 a; // = new ow3();

    @DexIgnore
    public iv3 a(Proxy proxy, kv3 kv3) throws IOException {
        List<wu3> d = kv3.d();
        iv3 l = kv3.l();
        ev3 d2 = l.d();
        int size = d.size();
        for (int i = 0; i < size; i++) {
            wu3 wu3 = d.get(i);
            if ("Basic".equalsIgnoreCase(wu3.b())) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) proxy.address();
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(inetSocketAddress.getHostName(), a(proxy, d2), inetSocketAddress.getPort(), d2.j(), wu3.a(), wu3.b(), d2.l(), Authenticator.RequestorType.PROXY);
                if (requestPasswordAuthentication != null) {
                    String a2 = av3.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()));
                    iv3.b g = l.g();
                    g.b("Proxy-Authorization", a2);
                    return g.a();
                }
            }
        }
        return null;
    }

    @DexIgnore
    public iv3 b(Proxy proxy, kv3 kv3) throws IOException {
        List<wu3> d = kv3.d();
        iv3 l = kv3.l();
        ev3 d2 = l.d();
        int size = d.size();
        for (int i = 0; i < size; i++) {
            wu3 wu3 = d.get(i);
            if (!"Basic".equalsIgnoreCase(wu3.b())) {
                Proxy proxy2 = proxy;
            } else {
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(d2.f(), a(proxy, d2), d2.h(), d2.j(), wu3.a(), wu3.b(), d2.l(), Authenticator.RequestorType.SERVER);
                if (requestPasswordAuthentication != null) {
                    String a2 = av3.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()));
                    iv3.b g = l.g();
                    g.b("Authorization", a2);
                    return g.a();
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final InetAddress a(Proxy proxy, ev3 ev3) throws IOException {
        if (proxy == null || proxy.type() == Proxy.Type.DIRECT) {
            return InetAddress.getByName(ev3.f());
        }
        return ((InetSocketAddress) proxy.address()).getAddress();
    }
}
