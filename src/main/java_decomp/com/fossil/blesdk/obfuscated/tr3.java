package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tr3<T> extends LiveData<T> {
    @DexIgnore
    public static /* final */ a k; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> LiveData<T> a() {
            return new tr3((rd4) null);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public tr3() {
        a(null);
    }

    @DexIgnore
    public /* synthetic */ tr3(rd4 rd4) {
        this();
    }
}
