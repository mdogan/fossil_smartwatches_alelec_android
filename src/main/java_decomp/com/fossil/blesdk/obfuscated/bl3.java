package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bl3 implements Factory<al3> {
    @DexIgnore
    public static al3 a(xk3 xk3, ResetPasswordUseCase resetPasswordUseCase) {
        return new al3(xk3, resetPasswordUseCase);
    }
}
