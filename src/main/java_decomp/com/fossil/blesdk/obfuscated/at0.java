package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class at0 implements ht0 {
    @DexIgnore
    public /* final */ zs0 a;
    @DexIgnore
    public /* final */ ws0 b;

    @DexIgnore
    public at0(zs0 zs0, ws0 ws0) {
        this.a = zs0;
        this.b = ws0;
    }

    @DexIgnore
    public final Object a() {
        return this.b.a().get(this.a.b);
    }
}
