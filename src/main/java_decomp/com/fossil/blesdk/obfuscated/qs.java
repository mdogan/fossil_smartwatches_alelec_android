package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qs implements bq<Bitmap>, xp {
    @DexIgnore
    public /* final */ Bitmap e;
    @DexIgnore
    public /* final */ kq f;

    @DexIgnore
    public qs(Bitmap bitmap, kq kqVar) {
        uw.a(bitmap, "Bitmap must not be null");
        this.e = bitmap;
        uw.a(kqVar, "BitmapPool must not be null");
        this.f = kqVar;
    }

    @DexIgnore
    public static qs a(Bitmap bitmap, kq kqVar) {
        if (bitmap == null) {
            return null;
        }
        return new qs(bitmap, kqVar);
    }

    @DexIgnore
    public int b() {
        return vw.a(this.e);
    }

    @DexIgnore
    public Class<Bitmap> c() {
        return Bitmap.class;
    }

    @DexIgnore
    public void d() {
        this.e.prepareToDraw();
    }

    @DexIgnore
    public void a() {
        this.f.a(this.e);
    }

    @DexIgnore
    public Bitmap get() {
        return this.e;
    }
}
