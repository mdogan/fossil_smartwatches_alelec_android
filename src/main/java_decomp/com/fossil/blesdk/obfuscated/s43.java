package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qs2;
import com.fossil.blesdk.obfuscated.u62;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s43 extends as2 implements r43, xs3.g {
    @DexIgnore
    public ur3<jg2> j;
    @DexIgnore
    public q43 k;
    @DexIgnore
    public qs2 l;
    @DexIgnore
    public u62 m;
    @DexIgnore
    public k42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements u62.b {
        @DexIgnore
        public /* final */ /* synthetic */ s43 a;

        @DexIgnore
        public b(s43 s43) {
            this.a = s43;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            wd4.b(watchApp, "watchApp");
            s43.a(this.a).a(watchApp.getWatchappId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements qs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ s43 a;

        @DexIgnore
        public c(s43 s43) {
            this.a = s43;
        }

        @DexIgnore
        public void a() {
            s43.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wd4.b(category, "category");
            s43.a(this.a).a(category);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s43 e;

        @DexIgnore
        public d(s43 s43) {
            this.e = s43;
        }

        @DexIgnore
        public final void onClick(View view) {
            s43.a(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s43 e;

        @DexIgnore
        public e(s43 s43) {
            this.e = s43;
        }

        @DexIgnore
        public final void onClick(View view) {
            s43.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s43 e;

        @DexIgnore
        public f(s43 s43) {
            this.e = s43;
        }

        @DexIgnore
        public final void onClick(View view) {
            s43.a(this.e).j();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ q43 a(s43 s43) {
        q43 q43 = s43.k;
        if (q43 != null) {
            return q43;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        es3 es3 = es3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager, "childFragmentManager");
                        es3.u(childFragmentManager);
                        return;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        es3 es32 = es3.c;
                        FragmentManager childFragmentManager2 = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager2, "childFragmentManager");
                        es32.A(childFragmentManager2);
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        es3 es33 = es3.c;
                        FragmentManager childFragmentManager3 = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager3, "childFragmentManager");
                        es33.B(childFragmentManager3);
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        es3 es34 = es3.c;
                        FragmentManager childFragmentManager4 = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager4, "childFragmentManager");
                        es34.t(childFragmentManager4);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void E(String str) {
        wd4.b(str, "content");
        ur3<jg2> ur3 = this.j;
        if (ur3 != null) {
            jg2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                wd4.a((Object) flexibleTextView, "it.tvWatchappsDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void M(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.C.a(this, str);
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            u62 u62 = this.m;
            if (u62 != null) {
                u62.b();
            } else {
                wd4.d("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(WatchApp watchApp) {
        if (watchApp != null) {
            u62 u62 = this.m;
            if (u62 != null) {
                u62.b(watchApp.getWatchappId());
                c(watchApp);
                return;
            }
            wd4.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void c(String str) {
        wd4.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            wd4.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void e(String str) {
        wd4.b(str, "category");
        ur3<jg2> ur3 = this.j;
        if (ur3 != null) {
            jg2 a2 = ur3.a();
            if (a2 != null) {
                qs2 qs2 = this.l;
                if (qs2 != null) {
                    int a3 = qs2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        qs2 qs22 = this.l;
                        if (qs22 != null) {
                            qs22.a(a3);
                            a2.q.j(a3);
                            return;
                        }
                        wd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        os3.a aVar = os3.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        os3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            k42 k42 = this.n;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) dianaCustomizeEditActivity, (lc.b) k42).a(DianaCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                q43 q43 = this.k;
                if (q43 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        q43.a(dianaCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 101) {
            if (i != 105) {
                if (i == 106 && i2 == -1 && intent != null) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING");
                    if (commuteTimeWatchAppSetting != null) {
                        q43 q43 = this.k;
                        if (q43 != null) {
                            q43.a("commute-time", commuteTimeWatchAppSetting);
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (i2 == -1 && intent != null) {
                WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING");
                if (weatherWatchAppSetting != null) {
                    q43 q432 = this.k;
                    if (q432 != null) {
                        q432.a("weather", weatherWatchAppSetting);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                q43 q433 = this.k;
                if (q433 != null) {
                    wd4.a((Object) stringExtra, "selectedWatchAppId");
                    q433.a(stringExtra);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        jg2 jg2 = (jg2) ra.a(layoutInflater, R.layout.fragment_watch_apps, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new u43(this)).a(this);
        this.j = new ur3<>(this, jg2);
        wd4.a((Object) jg2, "binding");
        return jg2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        q43 q43 = this.k;
        if (q43 != null) {
            q43.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        q43 q43 = this.k;
        if (q43 != null) {
            q43.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        u62 u62 = new u62((ArrayList) null, (u62.b) null, 3, (rd4) null);
        u62.a((u62.b) new b(this));
        this.m = u62;
        qs2 qs2 = new qs2((ArrayList) null, (qs2.c) null, 3, (rd4) null);
        qs2.a((qs2.c) new c(this));
        this.l = qs2;
        ur3<jg2> ur3 = this.j;
        if (ur3 != null) {
            jg2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                qs2 qs22 = this.l;
                if (qs22 != null) {
                    recyclerView.setAdapter(qs22);
                    RecyclerView recyclerView2 = a2.r;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    u62 u622 = this.m;
                    if (u622 != null) {
                        recyclerView2.setAdapter(u622);
                        a2.v.setOnClickListener(new d(this));
                        a2.w.setOnClickListener(new e(this));
                        a2.t.setOnClickListener(new f(this));
                        return;
                    }
                    wd4.d("mWatchAppAdapter");
                    throw null;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t(boolean z) {
        if (isActive()) {
            ur3<jg2> ur3 = this.j;
            if (ur3 != null) {
                jg2 a2 = ur3.a();
                if (a2 != null) {
                    a2.t.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? R.drawable.ic_help : 0, 0);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void v(List<WatchApp> list) {
        wd4.b(list, "watchApps");
        u62 u62 = this.m;
        if (u62 != null) {
            u62.a(list);
        } else {
            wd4.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(q43 q43) {
        wd4.b(q43, "presenter");
        this.k = q43;
    }

    @DexIgnore
    public final void c(WatchApp watchApp) {
        ur3<jg2> ur3 = this.j;
        if (ur3 != null) {
            jg2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "binding.tvSelectedWatchapps");
                flexibleTextView.setText(tm2.a(PortfolioApp.W.c(), watchApp.getNameKey(), watchApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.u;
                wd4.a((Object) flexibleTextView2, "binding.tvWatchappsDetail");
                flexibleTextView2.setText(tm2.a(PortfolioApp.W.c(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                u62 u62 = this.m;
                if (u62 != null) {
                    int a3 = u62.a(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.r.i(a3);
                        return;
                    }
                    return;
                }
                wd4.d("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(List<Category> list) {
        wd4.b(list, "categories");
        qs2 qs2 = this.l;
        if (qs2 != null) {
            qs2.a(list);
        } else {
            wd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.B.a(this, str);
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, String str3) {
        wd4.b(str, "watchAppId");
        wd4.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        ur3<jg2> ur3 = this.j;
        if (ur3 != null) {
            jg2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.v;
                wd4.a((Object) flexibleTextView2, "it.tvWatchappsPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.w;
                    wd4.a((Object) flexibleTextView3, "it.tvWatchappsSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.w;
                        wd4.a((Object) flexibleTextView4, "it.tvWatchappsSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.w;
                    wd4.a((Object) flexibleTextView5, "it.tvWatchappsSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.w;
                wd4.a((Object) flexibleTextView6, "it.tvWatchappsSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wd4.b(str, "topWatchApp");
        wd4.b(str2, "middleWatchApp");
        wd4.b(str3, "bottomWatchApp");
        WatchAppSearchActivity.C.a(this, str, str2, str3);
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (br4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        wd4.b(str, "title");
        wd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            ur3<jg2> ur3 = this.j;
            if (ur3 != null) {
                jg2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    wd4.a((Object) flexibleTextView, "it.tvWatchappsSetting");
                    flexibleTextView.setVisibility(8);
                    if (i2 == 0 || i == i2) {
                        FlexibleTextView flexibleTextView2 = a2.v;
                        wd4.a((Object) flexibleTextView2, "it.tvWatchappsPermission");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.s;
                        wd4.a((Object) flexibleTextView3, "it.tvPermissionOrder");
                        flexibleTextView3.setVisibility(8);
                        return;
                    }
                    FlexibleTextView flexibleTextView4 = a2.v;
                    wd4.a((Object) flexibleTextView4, "it.tvWatchappsPermission");
                    flexibleTextView4.setVisibility(0);
                    FlexibleTextView flexibleTextView5 = a2.v;
                    wd4.a((Object) flexibleTextView5, "it.tvWatchappsPermission");
                    flexibleTextView5.setText(str);
                    if (str2.length() > 0) {
                        FlexibleTextView flexibleTextView6 = a2.u;
                        wd4.a((Object) flexibleTextView6, "it.tvWatchappsDetail");
                        flexibleTextView6.setText(str2);
                    }
                    if (i2 > 1) {
                        FlexibleTextView flexibleTextView7 = a2.s;
                        wd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                        flexibleTextView7.setVisibility(0);
                        FlexibleTextView flexibleTextView8 = a2.s;
                        wd4.a((Object) flexibleTextView8, "it.tvPermissionOrder");
                        be4 be4 = be4.a;
                        String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                        wd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView8.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (wd4.a((Object) str, (Object) "REQUEST_NOTIFICATION_ACCESS")) {
            if (i == R.id.tv_cancel) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (wd4.a((Object) str, (Object) es3.c.a())) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (!br4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity != null) {
                    ((BaseActivity) activity).l();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (wd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION") && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (activity2 != null) {
                ((BaseActivity) activity2).l();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
