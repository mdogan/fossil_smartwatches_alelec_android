package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class es implements tr<mr, InputStream> {
    @DexIgnore
    public static /* final */ lo<Integer> b; // = lo.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    @DexIgnore
    public /* final */ sr<mr, mr> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<mr, InputStream> {
        @DexIgnore
        public /* final */ sr<mr, mr> a; // = new sr<>(500);

        @DexIgnore
        public tr<mr, InputStream> a(xr xrVar) {
            return new es(this.a);
        }
    }

    @DexIgnore
    public es(sr<mr, mr> srVar) {
        this.a = srVar;
    }

    @DexIgnore
    public boolean a(mr mrVar) {
        return true;
    }

    @DexIgnore
    public tr.a<InputStream> a(mr mrVar, int i, int i2, mo moVar) {
        sr<mr, mr> srVar = this.a;
        if (srVar != null) {
            mr a2 = srVar.a(mrVar, 0, 0);
            if (a2 == null) {
                this.a.a(mrVar, 0, 0, mrVar);
            } else {
                mrVar = a2;
            }
        }
        return new tr.a<>(mrVar, new zo(mrVar, ((Integer) moVar.a(b)).intValue()));
    }
}
