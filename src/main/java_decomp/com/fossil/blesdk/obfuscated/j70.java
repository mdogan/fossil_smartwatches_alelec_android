package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j70 extends h70 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId A;
    @DexIgnore
    public /* final */ boolean B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j70(GattCharacteristic.CharacteristicId characteristicId, boolean z, Peripheral peripheral) {
        super(RequestId.SUBSCRIBE_CHARACTERISTIC, peripheral);
        wd4.b(characteristicId, "characteristicId");
        wd4.b(peripheral, "peripheral");
        this.A = characteristicId;
        this.B = z;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new n10(this.A, this.B, i().h());
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(super.t(), JSONKey.CHANNEL_ID, this.A.getLogName$blesdk_productionRelease()), JSONKey.ENABLE, Boolean.valueOf(this.B));
    }
}
