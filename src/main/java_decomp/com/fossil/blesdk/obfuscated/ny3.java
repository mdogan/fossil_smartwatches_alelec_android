package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ny3 extends rx3<Target> {
    @DexIgnore
    public ny3(Picasso picasso, Target target, hy3 hy3, int i, int i2, Drawable drawable, String str, Object obj, int i3) {
        super(picasso, target, hy3, i, i2, i3, drawable, str, obj, false);
    }

    @DexIgnore
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            Target target = (Target) j();
            if (target != null) {
                target.onBitmapLoaded(bitmap, loadedFrom);
                if (bitmap.isRecycled()) {
                    throw new IllegalStateException("Target callback must not recycle bitmap!");
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[]{this}));
    }

    @DexIgnore
    public void b() {
        Target target = (Target) j();
        if (target == null) {
            return;
        }
        if (this.g != 0) {
            target.onBitmapFailed(this.a.e.getResources().getDrawable(this.g));
        } else {
            target.onBitmapFailed(this.h);
        }
    }
}
