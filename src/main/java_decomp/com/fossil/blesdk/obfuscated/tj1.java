package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ rj1 f;
    @DexIgnore
    public /* final */ /* synthetic */ rj1 g;
    @DexIgnore
    public /* final */ /* synthetic */ sj1 h;

    @DexIgnore
    public tj1(sj1 sj1, boolean z, rj1 rj1, rj1 rj12) {
        this.h = sj1;
        this.e = z;
        this.f = rj1;
        this.g = rj12;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0061, code lost:
        if (com.fossil.blesdk.obfuscated.ol1.e(r10.f.a, r10.g.a) != false) goto L_0x0064;
     */
    @DexIgnore
    public final void run() {
        boolean z;
        boolean z2 = false;
        if (this.h.l().s(this.h.p().B())) {
            z = this.e && this.h.c != null;
            if (z) {
                sj1 sj1 = this.h;
                sj1.a(sj1.c, true);
            }
        } else {
            if (this.e) {
                sj1 sj12 = this.h;
                rj1 rj1 = sj12.c;
                if (rj1 != null) {
                    sj12.a(rj1, true);
                }
            }
            z = false;
        }
        rj1 rj12 = this.f;
        if (rj12 != null) {
            long j = rj12.c;
            rj1 rj13 = this.g;
            if (j == rj13.c) {
                if (ol1.e(rj12.b, rj13.b)) {
                }
            }
        }
        z2 = true;
        if (z2) {
            Bundle bundle = new Bundle();
            sj1.a(this.g, bundle, true);
            rj1 rj14 = this.f;
            if (rj14 != null) {
                String str = rj14.a;
                if (str != null) {
                    bundle.putString("_pn", str);
                }
                bundle.putString("_pc", this.f.b);
                bundle.putLong("_pi", this.f.c);
            }
            if (this.h.l().s(this.h.p().B()) && z) {
                long C = this.h.t().C();
                if (C > 0) {
                    this.h.j().a(bundle, C);
                }
            }
            this.h.o().c("auto", "_vs", bundle);
        }
        sj1 sj13 = this.h;
        sj13.c = this.g;
        sj13.q().a(this.g);
    }
}
