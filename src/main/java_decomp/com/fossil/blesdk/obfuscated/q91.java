package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q91 implements w91 {
    @DexIgnore
    public w91[] a;

    @DexIgnore
    public q91(w91... w91Arr) {
        this.a = w91Arr;
    }

    @DexIgnore
    public final v91 a(Class<?> cls) {
        for (w91 w91 : this.a) {
            if (w91.b(cls)) {
                return w91.a(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        for (w91 b : this.a) {
            if (b.b(cls)) {
                return true;
            }
        }
        return false;
    }
}
