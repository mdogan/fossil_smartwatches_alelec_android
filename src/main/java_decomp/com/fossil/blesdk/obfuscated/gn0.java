package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class gn0 implements Callable {
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ hn0 g;

    @DexIgnore
    public gn0(boolean z, String str, hn0 hn0) {
        this.e = z;
        this.f = str;
        this.g = hn0;
    }

    @DexIgnore
    public final Object call() {
        return fn0.a(this.e, this.f, this.g);
    }
}
