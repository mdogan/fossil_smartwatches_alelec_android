package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jr2 implements Factory<ir2> {
    @DexIgnore
    public static ir2 a(NotificationsRepository notificationsRepository) {
        return new ir2(notificationsRepository);
    }
}
