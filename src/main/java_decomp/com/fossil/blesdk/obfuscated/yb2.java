package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yb2 extends xb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout G;
    @DexIgnore
    public long H;

    /*
    static {
        J.put(R.id.cl_top, 1);
        J.put(R.id.iv_back, 2);
        J.put(R.id.tv_title, 3);
        J.put(R.id.cl_overview_day, 4);
        J.put(R.id.iv_back_date, 5);
        J.put(R.id.ftv_day_of_week, 6);
        J.put(R.id.ftv_day_of_month, 7);
        J.put(R.id.line, 8);
        J.put(R.id.ftv_daily_value, 9);
        J.put(R.id.ftv_daily_unit, 10);
        J.put(R.id.iv_next_date, 11);
        J.put(R.id.coordinatorLayout, 12);
        J.put(R.id.cl_pb_goal, 13);
        J.put(R.id.pb_goal, 14);
        J.put(R.id.ftv_progress_value, 15);
        J.put(R.id.ftv_goal_value, 16);
        J.put(R.id.dayChart, 17);
        J.put(R.id.v_elevation, 18);
        J.put(R.id.cl_recorded_goal_tracking, 19);
        J.put(R.id.add, 20);
        J.put(R.id.tv_not_found, 21);
        J.put(R.id.rv_recorded_goal_tracking, 22);
    }
    */

    @DexIgnore
    public yb2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 23, I, J));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public yb2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[20], objArr[4], objArr[13], objArr[19], objArr[1], objArr[12], objArr[17], objArr[10], objArr[9], objArr[7], objArr[6], objArr[16], objArr[15], objArr[2], objArr[5], objArr[11], objArr[8], objArr[14], objArr[22], objArr[21], objArr[3], objArr[18]);
        this.H = -1;
        this.G = objArr[0];
        this.G.setTag((Object) null);
        a(view);
        f();
    }
}
