package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ck implements qj {
    @DexIgnore
    public static /* final */ String f; // = ej.a("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context e;

    @DexIgnore
    public ck(Context context) {
        this.e = context.getApplicationContext();
    }

    @DexIgnore
    public void a(il... ilVarArr) {
        for (il a : ilVarArr) {
            a(a);
        }
    }

    @DexIgnore
    public void a(String str) {
        this.e.startService(yj.c(this.e, str));
    }

    @DexIgnore
    public final void a(il ilVar) {
        ej.a().a(f, String.format("Scheduling work with workSpecId %s", new Object[]{ilVar.a}), new Throwable[0]);
        this.e.startService(yj.b(this.e, ilVar.a));
    }
}
