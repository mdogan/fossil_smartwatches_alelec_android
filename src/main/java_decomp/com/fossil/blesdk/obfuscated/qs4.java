package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qs4<T> implements sr4<T, RequestBody> {
    @DexIgnore
    public static /* final */ qs4<Object> a; // = new qs4<>();
    @DexIgnore
    public static /* final */ mm4 b; // = mm4.b("text/plain; charset=UTF-8");

    @DexIgnore
    public RequestBody a(T t) throws IOException {
        return RequestBody.a(b, String.valueOf(t));
    }
}
