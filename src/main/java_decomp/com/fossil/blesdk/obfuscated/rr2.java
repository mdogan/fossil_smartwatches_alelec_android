package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rr2 extends j62<a.b, a.c, a.C0097a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.rr2$a$a  reason: collision with other inner class name */
        public static final class C0097a implements j62.a {
            @DexIgnore
            public C0097a(int i) {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements j62.b {
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements j62.c {
            @DexIgnore
            public c(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ rr2 a;

        @DexIgnore
        public b(rr2 rr2) {
            this.a = rr2;
        }

        @DexIgnore
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rr2.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.a.a().a(new a.C0097a(i));
        }

        @DexIgnore
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(rr2.f.a(), "executeUseCase - onSuccess");
            this.a.a().onSuccess(new a.c(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = rr2.class.getSimpleName();
        wd4.a((Object) simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public rr2(ServerSettingRepository serverSettingRepository) {
        wd4.b(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    public void a(a.b bVar) {
        this.d.getServerSettingList(new b(this));
    }
}
