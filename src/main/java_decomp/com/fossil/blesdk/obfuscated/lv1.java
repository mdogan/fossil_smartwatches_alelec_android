package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements nv1<List<String>> {
        @DexIgnore
        public /* final */ List<String> a; // = Lists.a();

        @DexIgnore
        public boolean a(String str) {
            this.a.add(str);
            return true;
        }

        @DexIgnore
        public List<String> getResult() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends dv1<File> {
        @DexIgnore
        public String toString() {
            return "Files.fileTreeTraverser()";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends gv1 {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public /* synthetic */ c(File file, a aVar) {
            this(file);
        }

        @DexIgnore
        public String toString() {
            return "Files.asByteSource(" + this.a + ")";
        }

        @DexIgnore
        public c(File file) {
            tt1.a(file);
            this.a = file;
        }

        @DexIgnore
        public FileInputStream a() throws IOException {
            return new FileInputStream(this.a);
        }
    }

    /*
    static {
        new b();
    }
    */

    @DexIgnore
    public static gv1 a(File file) {
        return new c(file, (a) null);
    }

    @DexIgnore
    public static List<String> b(File file, Charset charset) throws IOException {
        return (List) a(file, charset, new a());
    }

    @DexIgnore
    public static hv1 a(File file, Charset charset) {
        return a(file).a(charset);
    }

    @DexIgnore
    public static <T> T a(File file, Charset charset, nv1<T> nv1) throws IOException {
        return a(file, charset).a(nv1);
    }
}
