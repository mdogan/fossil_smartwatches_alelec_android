package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uy {
    @DexIgnore
    public /* final */ ExecutorService a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexIgnore
        public a(uy uyVar, Runnable runnable) {
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.e.run();
            } catch (Exception e2) {
                r44.g().e("CrashlyticsCore", "Failed to execute task.", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Callable e;

        @DexIgnore
        public b(uy uyVar, Callable callable) {
            this.e = callable;
        }

        @DexIgnore
        public T call() throws Exception {
            try {
                return this.e.call();
            } catch (Exception e2) {
                r44.g().e("CrashlyticsCore", "Failed to execute task.", e2);
                return null;
            }
        }
    }

    @DexIgnore
    public uy(ExecutorService executorService) {
        this.a = executorService;
    }

    @DexIgnore
    public Future<?> a(Runnable runnable) {
        try {
            return this.a.submit(new a(this, runnable));
        } catch (RejectedExecutionException unused) {
            r44.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }

    @DexIgnore
    public <T> T b(Callable<T> callable) {
        try {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                return this.a.submit(callable).get(4, TimeUnit.SECONDS);
            }
            return this.a.submit(callable).get();
        } catch (RejectedExecutionException unused) {
            r44.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        } catch (Exception e) {
            r44.g().e("CrashlyticsCore", "Failed to execute task.", e);
            return null;
        }
    }

    @DexIgnore
    public <T> Future<T> a(Callable<T> callable) {
        try {
            return this.a.submit(new b(this, callable));
        } catch (RejectedExecutionException unused) {
            r44.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }
}
