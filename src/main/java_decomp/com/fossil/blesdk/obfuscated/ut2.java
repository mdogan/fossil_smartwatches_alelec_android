package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.blesdk.obfuscated.tt2;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ut2<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, tt2.a {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Cursor f;
    @DexIgnore
    public ut2<VH>.a g;
    @DexIgnore
    public DataSetObserver h;
    @DexIgnore
    public tt2 i;
    @DexIgnore
    public FilterQueryProvider j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            ut2.this.b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends DataSetObserver {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onChanged() {
            ut2.this.e = true;
            ut2.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            ut2.this.e = false;
            ut2 ut2 = ut2.this;
            ut2.notifyItemRangeRemoved(0, ut2.getItemCount());
        }
    }

    /*
    static {
        new b((rd4) null);
        String simpleName = ut2.class.getSimpleName();
        wd4.a((Object) simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public ut2(Cursor cursor) {
        boolean z = cursor != null;
        this.f = cursor;
        this.e = z;
        this.g = new a();
        this.h = new c();
        if (z) {
            ut2<VH>.a aVar = this.g;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    wd4.a();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public abstract void a(VH vh, Cursor cursor, int i2);

    @DexIgnore
    public CharSequence b(Cursor cursor) {
        if (cursor != null) {
            String obj = cursor.toString();
            if (obj != null) {
                return obj;
            }
        }
        return "";
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (wd4.a((Object) cursor, (Object) this.f)) {
            return null;
        }
        Cursor cursor2 = this.f;
        if (cursor2 != null) {
            ut2<VH>.a aVar = this.g;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f = cursor;
        if (cursor != null) {
            ut2<VH>.a aVar2 = this.g;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.h;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.e = true;
            notifyDataSetChanged();
        } else {
            this.e = false;
            notifyItemRangeRemoved(0, getItemCount());
        }
        return cursor2;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.i == null) {
            this.i = new tt2(this);
        }
        tt2 tt2 = this.i;
        if (tt2 != null) {
            return tt2;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public int getItemCount() {
        if (this.e) {
            Cursor cursor = this.f;
            if (cursor != null) {
                if (cursor != null) {
                    return cursor.getCount();
                }
                wd4.a();
                throw null;
            }
        }
        return 0;
    }

    @DexIgnore
    public void onBindViewHolder(VH vh, int i2) {
        wd4.b(vh, "holder");
        if (!this.e) {
            FLogger.INSTANCE.getLocal().d(k, ".Inside onBindViewHolder the cursor is invalid");
        }
        a(vh, this.f, i2);
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(k, ".Inside onContentChanged");
    }

    @DexIgnore
    public Cursor a() {
        return this.f;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        wd4.b(cursor, "cursor");
        Cursor c2 = c(cursor);
        if (c2 != null) {
            c2.close();
        }
    }

    @DexIgnore
    public Cursor a(CharSequence charSequence) {
        wd4.b(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.j;
        if (filterQueryProvider == null) {
            return this.f;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(FilterQueryProvider filterQueryProvider) {
        wd4.b(filterQueryProvider, "filterQueryProvider");
        this.j = filterQueryProvider;
    }
}
