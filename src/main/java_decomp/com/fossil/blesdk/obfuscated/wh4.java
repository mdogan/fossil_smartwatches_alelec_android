package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wh4<T> extends mk4<T> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater i; // = AtomicIntegerFieldUpdater.newUpdater(wh4.class, "_decision");
    @DexIgnore
    public volatile int _decision; // = 0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wh4(CoroutineContext coroutineContext, kc4<? super T> kc4) {
        super(coroutineContext, kc4);
        wd4.b(coroutineContext, "context");
        wd4.b(kc4, "uCont");
    }

    @DexIgnore
    public void a(Object obj, int i2) {
        if (!n()) {
            super.a(obj, i2);
        }
    }

    @DexIgnore
    public int j() {
        return 1;
    }

    @DexIgnore
    public final Object m() {
        if (o()) {
            return oc4.a();
        }
        Object b = yi4.b(d());
        if (!(b instanceof zg4)) {
            return b;
        }
        throw ((zg4) b).a;
    }

    @DexIgnore
    public final boolean n() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!i.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean o() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!i.compareAndSet(this, 0, 1));
        return true;
    }
}
