package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dl0 extends nj0 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ ze0 f;
    @DexIgnore
    public /* final */ /* synthetic */ int g;

    @DexIgnore
    public dl0(Intent intent, ze0 ze0, int i) {
        this.e = intent;
        this.f = ze0;
        this.g = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.e;
        if (intent != null) {
            this.f.startActivityForResult(intent, this.g);
        }
    }
}
