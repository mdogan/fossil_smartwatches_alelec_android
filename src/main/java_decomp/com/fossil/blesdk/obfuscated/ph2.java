package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ph2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ AppCompatCheckBox q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ View x;

    @DexIgnore
    public ph2(Object obj, View view, int i, AppCompatCheckBox appCompatCheckBox, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, LinearLayout linearLayout, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2) {
        super(obj, view, i);
        this.q = appCompatCheckBox;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleTextView;
        this.u = linearLayout;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = view2;
    }

    @DexIgnore
    public static ph2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, ra.a());
    }

    @DexIgnore
    @Deprecated
    public static ph2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ph2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_contact, viewGroup, z, obj);
    }
}
