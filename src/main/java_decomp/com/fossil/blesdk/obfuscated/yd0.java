package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.blesdk.obfuscated.d6;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.misfit.frameworks.buttonservice.ble.ScanService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yd0 extends zd0 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static /* final */ yd0 e; // = new yd0();
    @DexIgnore
    public String c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"HandlerLeak"})
    public class a extends ts0 {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int c = yd0.this.c(this.a);
            if (yd0.this.c(c)) {
                yd0.this.c(this.a, c);
            }
        }
    }

    @DexIgnore
    public static yd0 a() {
        return e;
    }

    @DexIgnore
    public boolean b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, i2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @DexIgnore
    public void c(Context context, int i) {
        a(context, i, (String) null, a(context, i, 0, "n"));
    }

    @DexIgnore
    public final void d(Context context) {
        new a(context).sendEmptyMessageDelayed(1, ScanService.BLE_SCAN_TIMEOUT);
    }

    @DexIgnore
    public Dialog a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return a((Context) activity, i, nj0.a(activity, a((Context) activity, i, "d"), i2), onCancelListener);
    }

    @DexIgnore
    public final String b() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    @DexIgnore
    public int c(Context context) {
        return super.c(context);
    }

    @DexIgnore
    public final boolean c(int i) {
        return super.c(i);
    }

    @DexIgnore
    public final boolean a(Activity activity, ze0 ze0, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a((Context) activity, i, nj0.a(ze0, a((Context) activity, i, "d"), 2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @DexIgnore
    public final String b(int i) {
        return super.b(i);
    }

    @DexIgnore
    public final boolean a(Context context, vd0 vd0, int i) {
        PendingIntent a2 = a(context, vd0);
        if (a2 == null) {
            return false;
        }
        a(context, vd0.H(), (String) null, GoogleApiActivity.a(context, a2, i));
        return true;
    }

    @DexIgnore
    public static Dialog a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, (AttributeSet) null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(mj0.b(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        a(activity, (Dialog) create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    @DexIgnore
    public final ah0 a(Context context, bh0 bh0) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        ah0 ah0 = new ah0(bh0);
        context.registerReceiver(ah0, intentFilter);
        ah0.a(context);
        if (a(context, "com.google.android.gms")) {
            return ah0;
        }
        bh0.a();
        ah0.a();
        return null;
    }

    @DexIgnore
    public int a(Context context, int i) {
        return super.a(context, i);
    }

    @DexIgnore
    public Intent a(Context context, int i, String str) {
        return super.a(context, i, str);
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    @DexIgnore
    public PendingIntent a(Context context, vd0 vd0) {
        if (vd0.K()) {
            return vd0.J();
        }
        return a(context, vd0.H(), 0);
    }

    @DexIgnore
    public static Dialog a(Context context, int i, nj0 nj0, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(mj0.b(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String a2 = mj0.a(context, i);
        if (a2 != null) {
            builder.setPositiveButton(a2, nj0);
        }
        String e2 = mj0.e(context, i);
        if (e2 != null) {
            builder.setTitle(e2);
        }
        return builder.create();
    }

    @DexIgnore
    public static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            de0.a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        wd0.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @DexIgnore
    @TargetApi(20)
    public final void a(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        if (i == 18) {
            d(context);
        } else if (pendingIntent != null) {
            String d2 = mj0.d(context, i);
            String c2 = mj0.c(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            d6.c cVar = new d6.c(context);
            cVar.b(true);
            cVar.a(true);
            cVar.b((CharSequence) d2);
            d6.b bVar = new d6.b();
            bVar.a((CharSequence) c2);
            cVar.a((d6.d) bVar);
            if (mm0.b(context)) {
                ck0.b(qm0.f());
                cVar.c(context.getApplicationInfo().icon);
                cVar.b(2);
                if (mm0.c(context)) {
                    cVar.a(kd0.common_full_open_on_phone, resources.getString(ld0.common_open_on_phone), pendingIntent);
                } else {
                    cVar.a(pendingIntent);
                }
            } else {
                cVar.c(17301642);
                cVar.c((CharSequence) resources.getString(ld0.common_google_play_services_notification_ticker));
                cVar.a(System.currentTimeMillis());
                cVar.a(pendingIntent);
                cVar.a((CharSequence) c2);
            }
            if (qm0.i()) {
                ck0.b(qm0.i());
                String b = b();
                if (b == null) {
                    b = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(b);
                    String b2 = mj0.b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(b, b2, 4));
                    } else if (!b2.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(b2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                cVar.b(b);
            }
            Notification a2 = cVar.a();
            if (i == 1 || i == 2 || i == 3) {
                i2 = ae0.GMS_AVAILABILITY_NOTIFICATION_ID;
                ae0.sCanceledAvailabilityNotification.set(false);
            } else {
                i2 = ae0.GMS_GENERAL_ERROR_NOTIFICATION_ID;
            }
            notificationManager.notify(i2, a2);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
