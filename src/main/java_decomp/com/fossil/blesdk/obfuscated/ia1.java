package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzuv;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ia1<MessageType> {
    @DexIgnore
    MessageType a(z71 z71, j81 j81) throws zzuv;
}
