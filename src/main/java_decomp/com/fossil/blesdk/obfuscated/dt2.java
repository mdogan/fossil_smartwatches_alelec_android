package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dt2 extends me.d<ActivitySummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        wd4.b(activitySummary, "oldItem");
        wd4.b(activitySummary2, "newItem");
        return wd4.a((Object) activitySummary, (Object) activitySummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        wd4.b(activitySummary, "oldItem");
        wd4.b(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
