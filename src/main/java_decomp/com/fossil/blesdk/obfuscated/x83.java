package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x83 implements Factory<ActiveTimeOverviewWeekPresenter> {
    @DexIgnore
    public static ActiveTimeOverviewWeekPresenter a(v83 v83, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActiveTimeOverviewWeekPresenter(v83, userRepository, summariesRepository);
    }
}
