package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bs<Model> implements tr<Model, Model> {
    @DexIgnore
    public static /* final */ bs<?> a; // = new bs<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Model> implements ur<Model, Model> {
        @DexIgnore
        public static /* final */ a<?> a; // = new a<>();

        @DexIgnore
        public static <T> a<T> a() {
            return a;
        }

        @DexIgnore
        public tr<Model, Model> a(xr xrVar) {
            return bs.a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model> implements to<Model> {
        @DexIgnore
        public /* final */ Model e;

        @DexIgnore
        public b(Model model) {
            this.e = model;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super Model> aVar) {
            aVar.a(this.e);
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Model> getDataClass() {
            return this.e.getClass();
        }
    }

    @DexIgnore
    public static <T> bs<T> a() {
        return a;
    }

    @DexIgnore
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    public tr.a<Model> a(Model model, int i, int i2, mo moVar) {
        return new tr.a<>(new kw(model), new b(model));
    }
}
