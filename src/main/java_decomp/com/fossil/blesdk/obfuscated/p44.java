package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p44 {
    @DexIgnore
    public /* final */ Application a;
    @DexIgnore
    public a b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public void a(Activity activity) {
        }

        @DexIgnore
        public abstract void a(Activity activity, Bundle bundle);

        @DexIgnore
        public void b(Activity activity) {
        }

        @DexIgnore
        public void b(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public abstract void c(Activity activity);

        @DexIgnore
        public abstract void d(Activity activity);

        @DexIgnore
        public void e(Activity activity) {
        }
    }

    @DexIgnore
    public p44(Context context) {
        this.a = (Application) context.getApplicationContext();
        if (Build.VERSION.SDK_INT >= 14) {
            this.b = new a(this.a);
        }
    }

    @DexIgnore
    public boolean a(b bVar) {
        a aVar = this.b;
        return aVar != null && aVar.a(bVar);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Set<Application.ActivityLifecycleCallbacks> a; // = new HashSet();
        @DexIgnore
        public /* final */ Application b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p44$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.p44$a$a  reason: collision with other inner class name */
        public class C0093a implements Application.ActivityLifecycleCallbacks {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public C0093a(a aVar, b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public void onActivityCreated(Activity activity, Bundle bundle) {
                this.e.a(activity, bundle);
            }

            @DexIgnore
            public void onActivityDestroyed(Activity activity) {
                this.e.a(activity);
            }

            @DexIgnore
            public void onActivityPaused(Activity activity) {
                this.e.b(activity);
            }

            @DexIgnore
            public void onActivityResumed(Activity activity) {
                this.e.c(activity);
            }

            @DexIgnore
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                this.e.b(activity, bundle);
            }

            @DexIgnore
            public void onActivityStarted(Activity activity) {
                this.e.d(activity);
            }

            @DexIgnore
            public void onActivityStopped(Activity activity) {
                this.e.e(activity);
            }
        }

        @DexIgnore
        public a(Application application) {
            this.b = application;
        }

        @DexIgnore
        @TargetApi(14)
        public final void a() {
            for (Application.ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : this.a) {
                this.b.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }

        @DexIgnore
        @TargetApi(14)
        public final boolean a(b bVar) {
            if (this.b == null) {
                return false;
            }
            C0093a aVar = new C0093a(this, bVar);
            this.b.registerActivityLifecycleCallbacks(aVar);
            this.a.add(aVar);
            return true;
        }
    }

    @DexIgnore
    public void a() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.a();
        }
    }
}
