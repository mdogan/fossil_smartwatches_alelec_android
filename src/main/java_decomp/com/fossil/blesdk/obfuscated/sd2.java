package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sd2 extends rd2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.iv_close, 1);
        y.put(R.id.ftv_title, 2);
        y.put(R.id.fet_search_apps, 3);
        y.put(R.id.iv_clear, 4);
        y.put(R.id.cl_all_apps, 5);
        y.put(R.id.ftv_all_apps, 6);
        y.put(R.id.sw_all_apps_enabled, 7);
        y.put(R.id.rv_notification_apps, 8);
    }
    */

    @DexIgnore
    public sd2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 9, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sd2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[5], objArr[3], objArr[6], objArr[2], objArr[4], objArr[1], objArr[8], objArr[7]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
