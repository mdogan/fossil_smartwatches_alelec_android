package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ sl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ wj1 f;

    @DexIgnore
    public zj1(wj1 wj1, sl1 sl1) {
        this.f = wj1;
        this.e = sl1;
    }

    @DexIgnore
    public final void run() {
        lg1 d = this.f.d;
        if (d == null) {
            this.f.d().s().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            d.a(this.e);
            this.f.a(d, (kk0) null, this.e);
            this.f.C();
        } catch (RemoteException e2) {
            this.f.d().s().a("Failed to send app launch to the service", e2);
        }
    }
}
