package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o53 implements Factory<WeatherSettingPresenter> {
    @DexIgnore
    public static WeatherSettingPresenter a(l53 l53, GoogleApiService googleApiService) {
        return new WeatherSettingPresenter(l53, googleApiService);
    }
}
