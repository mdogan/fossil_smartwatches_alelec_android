package com.fossil.blesdk.obfuscated;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o34 implements ErrorResponse {
    @DexIgnore
    public Throwable a;
    @DexIgnore
    public cs4 b;

    @DexIgnore
    public o34(Throwable th) {
        this.a = th;
    }

    @DexIgnore
    public static o34 a(Throwable th) {
        return new o34(th);
    }

    @DexIgnore
    public int G() {
        cs4 cs4 = this.b;
        if (cs4 != null) {
            return cs4.b();
        }
        return -1;
    }

    @DexIgnore
    public boolean b() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }

    @DexIgnore
    public static o34 a(cs4 cs4) {
        return new o34(cs4);
    }

    @DexIgnore
    public o34(cs4 cs4) {
        this.b = cs4;
    }

    @DexIgnore
    public String a() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        cs4 cs4 = this.b;
        if (cs4 != null) {
            if (s34.b(cs4.e())) {
                sb.append(this.b.e());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }
}
