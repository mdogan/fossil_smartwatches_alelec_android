package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c92 extends b92 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public long J;

    /*
    static {
        L.put(R.id.cl_top, 1);
        L.put(R.id.iv_back, 2);
        L.put(R.id.tv_title, 3);
        L.put(R.id.cl_overview_day, 4);
        L.put(R.id.iv_back_date, 5);
        L.put(R.id.ftv_day_of_week, 6);
        L.put(R.id.ftv_day_of_month, 7);
        L.put(R.id.line, 8);
        L.put(R.id.ftv_daily_value, 9);
        L.put(R.id.ftv_daily_unit, 10);
        L.put(R.id.ftv_est, 11);
        L.put(R.id.iv_next_date, 12);
        L.put(R.id.appBarLayout, 13);
        L.put(R.id.cl_pb_goal, 14);
        L.put(R.id.pb_goal, 15);
        L.put(R.id.ftv_progress_value, 16);
        L.put(R.id.ftv_goal_value, 17);
        L.put(R.id.dayChart, 18);
        L.put(R.id.llWorkout, 19);
        L.put(R.id.v_elevation, 20);
        L.put(R.id.ftv_no_workout_recorded, 21);
        L.put(R.id.rvWorkout, 22);
    }
    */

    @DexIgnore
    public c92(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 23, K, L));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.J != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.J = 1;
        }
        g();
    }

    @DexIgnore
    public c92(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[13], objArr[4], objArr[14], objArr[1], objArr[18], objArr[10], objArr[9], objArr[7], objArr[6], objArr[11], objArr[17], objArr[21], objArr[16], objArr[2], objArr[5], objArr[12], objArr[8], objArr[19], objArr[15], objArr[22], objArr[3], objArr[20]);
        this.J = -1;
        this.I = objArr[0];
        this.I.setTag((Object) null);
        a(view);
        f();
    }
}
