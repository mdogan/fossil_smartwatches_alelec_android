package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r63 implements Factory<HybridCustomizeEditPresenter> {
    @DexIgnore
    public static HybridCustomizeEditPresenter a(m63 m63, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, fn2 fn2) {
        return new HybridCustomizeEditPresenter(m63, setHybridPresetToWatchUseCase, fn2);
    }
}
