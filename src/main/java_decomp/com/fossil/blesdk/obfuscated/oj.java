package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.gj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oj implements gj {
    @DexIgnore
    public /* final */ MutableLiveData<gj.b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ zl<gj.b.c> d; // = zl.e();

    @DexIgnore
    public oj() {
        a(gj.b);
    }

    @DexIgnore
    public void a(gj.b bVar) {
        this.c.a(bVar);
        if (bVar instanceof gj.b.c) {
            this.d.b((gj.b.c) bVar);
        } else if (bVar instanceof gj.b.a) {
            this.d.a(((gj.b.a) bVar).a());
        }
    }
}
