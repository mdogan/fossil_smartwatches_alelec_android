package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ LinearLayout C;
    @DexIgnore
    public /* final */ RecyclerView D;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ TodayHeartRateChart s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hc2(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, TodayHeartRateChart todayHeartRateChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, Guideline guideline, ImageView imageView, ImageView imageView2, ImageView imageView3, View view2, LinearLayout linearLayout, RecyclerView recyclerView, View view3, TextView textView, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = todayHeartRateChart;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView4;
        this.w = flexibleTextView5;
        this.x = flexibleTextView6;
        this.y = flexibleTextView8;
        this.z = imageView;
        this.A = imageView2;
        this.B = imageView3;
        this.C = linearLayout;
        this.D = recyclerView;
    }
}
