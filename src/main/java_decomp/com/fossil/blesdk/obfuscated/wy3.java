package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wy3 {
    @DexIgnore
    public static wy3 c;
    @DexIgnore
    public Map<Integer, vy3> a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public wy3(Context context) {
        this.b = context.getApplicationContext();
        this.a = new HashMap(3);
        this.a.put(1, new uy3(context));
        this.a.put(2, new ry3(context));
        this.a.put(4, new ty3(context));
    }

    @DexIgnore
    public static synchronized wy3 a(Context context) {
        wy3 wy3;
        synchronized (wy3.class) {
            if (c == null) {
                c = new wy3(context);
            }
            wy3 = c;
        }
        return wy3;
    }

    @DexIgnore
    public final sy3 a() {
        return a((List<Integer>) new ArrayList(Arrays.asList(new Integer[]{1, 2, 4})));
    }

    @DexIgnore
    public final sy3 a(List<Integer> list) {
        if (list.size() >= 0) {
            for (Integer num : list) {
                vy3 vy3 = this.a.get(num);
                if (vy3 != null) {
                    sy3 c2 = vy3.c();
                    if (c2 != null && xy3.b(c2.c)) {
                        return c2;
                    }
                }
            }
        }
        return new sy3();
    }

    @DexIgnore
    public final void a(String str) {
        sy3 a2 = a();
        a2.c = str;
        if (!xy3.a(a2.a)) {
            a2.a = xy3.a(this.b);
        }
        if (!xy3.a(a2.b)) {
            a2.b = xy3.b(this.b);
        }
        a2.d = System.currentTimeMillis();
        for (Map.Entry<Integer, vy3> value : this.a.entrySet()) {
            ((vy3) value.getValue()).a(a2);
        }
    }
}
