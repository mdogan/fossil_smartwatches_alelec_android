package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u51 extends wb1<u51> {
    @DexIgnore
    public static volatile u51[] g;
    @DexIgnore
    public x51 c; // = null;
    @DexIgnore
    public v51 d; // = null;
    @DexIgnore
    public Boolean e; // = null;
    @DexIgnore
    public String f; // = null;

    @DexIgnore
    public u51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static u51[] e() {
        if (g == null) {
            synchronized (ac1.b) {
                if (g == null) {
                    g = new u51[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        x51 x51 = this.c;
        if (x51 != null) {
            vb1.a(1, (bc1) x51);
        }
        v51 v51 = this.d;
        if (v51 != null) {
            vb1.a(2, (bc1) v51);
        }
        Boolean bool = this.e;
        if (bool != null) {
            vb1.a(3, bool.booleanValue());
        }
        String str = this.f;
        if (str != null) {
            vb1.a(4, str);
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof u51)) {
            return false;
        }
        u51 u51 = (u51) obj;
        x51 x51 = this.c;
        if (x51 == null) {
            if (u51.c != null) {
                return false;
            }
        } else if (!x51.equals(u51.c)) {
            return false;
        }
        v51 v51 = this.d;
        if (v51 == null) {
            if (u51.d != null) {
                return false;
            }
        } else if (!v51.equals(u51.d)) {
            return false;
        }
        Boolean bool = this.e;
        if (bool == null) {
            if (u51.e != null) {
                return false;
            }
        } else if (!bool.equals(u51.e)) {
            return false;
        }
        String str = this.f;
        if (str == null) {
            if (u51.f != null) {
                return false;
            }
        } else if (!str.equals(u51.f)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(u51.b);
        }
        yb1 yb12 = u51.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i;
        int i2;
        x51 x51 = this.c;
        int hashCode = (u51.class.getName().hashCode() + 527) * 31;
        int i3 = 0;
        if (x51 == null) {
            i = 0;
        } else {
            i = x51.hashCode();
        }
        int i4 = hashCode + i;
        v51 v51 = this.d;
        int i5 = i4 * 31;
        if (v51 == null) {
            i2 = 0;
        } else {
            i2 = v51.hashCode();
        }
        int i6 = (i5 + i2) * 31;
        Boolean bool = this.e;
        int hashCode2 = (i6 + (bool == null ? 0 : bool.hashCode())) * 31;
        String str = this.f;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode3 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        x51 x51 = this.c;
        if (x51 != null) {
            a += vb1.b(1, (bc1) x51);
        }
        v51 v51 = this.d;
        if (v51 != null) {
            a += vb1.b(2, (bc1) v51);
        }
        Boolean bool = this.e;
        if (bool != null) {
            bool.booleanValue();
            a += vb1.c(3) + 1;
        }
        String str = this.f;
        return str != null ? a + vb1.b(4, str) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                if (this.c == null) {
                    this.c = new x51();
                }
                ub1.a((bc1) this.c);
            } else if (c2 == 18) {
                if (this.d == null) {
                    this.d = new v51();
                }
                ub1.a((bc1) this.d);
            } else if (c2 == 24) {
                this.e = Boolean.valueOf(ub1.d());
            } else if (c2 == 34) {
                this.f = ub1.b();
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
