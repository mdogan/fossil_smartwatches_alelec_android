package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.n62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class az2 extends bs2 implements zy2 {
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<vc2> k;
    @DexIgnore
    public yy2 l;
    @DexIgnore
    public n62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final az2 a() {
            return new az2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n62.b {
        @DexIgnore
        public /* final */ /* synthetic */ az2 a;

        @DexIgnore
        public b(az2 az2) {
            this.a = az2;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            az2.a(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            az2.a(this.a).a(alarm);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az2 e;

        @DexIgnore
        public c(az2 az2) {
            this.e = az2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az2 e;

        @DexIgnore
        public d(az2 az2) {
            this.e = az2;
        }

        @DexIgnore
        public final void onClick(View view) {
            az2.a(this.e).a((Alarm) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az2 e;

        @DexIgnore
        public e(az2 az2) {
            this.e = az2;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (cn2.a(cn2.d, this.e.getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null) && cn2.a(cn2.d, this.e.getContext(), "NOTIFICATION_APPS", false, 4, (Object) null)) {
                NotificationDialLandingActivity.C.a(this.e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az2 e;

        @DexIgnore
        public f(az2 az2) {
            this.e = az2;
        }

        @DexIgnore
        public final void onClick(View view) {
            az2.a(this.e).h();
        }
    }

    @DexIgnore
    public static final /* synthetic */ yy2 a(az2 az2) {
        yy2 yy2 = az2.l;
        if (yy2 != null) {
            return yy2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void d(List<Alarm> list) {
        wd4.b(list, "alarms");
        n62 n62 = this.m;
        if (n62 != null) {
            n62.a(list);
        } else {
            wd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void i(boolean z) {
        Context context;
        int i;
        ur3<vc2> ur3 = this.k;
        if (ur3 != null) {
            vc2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.x;
                wd4.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                FlexibleTextView flexibleTextView = a2.t;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i = R.color.primaryText;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i = R.color.warmGrey;
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                flexibleTextView.setTextColor(k6.a(context, i));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        ur3<vc2> ur3 = this.k;
        if (ur3 != null) {
            vc2 a2 = ur3.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.v;
                if (linearLayout != null) {
                    wd4.a((Object) linearLayout, "it");
                    linearLayout.setAlpha(z ? 0.5f : 1.0f);
                    linearLayout.setClickable(!z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        vc2 vc2 = (vc2) ra.a(layoutInflater, R.layout.fragment_home_alerts_hybrid, viewGroup, false, O0());
        R("alert_view");
        vc2.r.setOnClickListener(new d(this));
        vc2.v.setOnClickListener(new e(this));
        vc2.x.setOnClickListener(new f(this));
        vi2 vi2 = vc2.u;
        if (vi2 != null) {
            ConstraintLayout constraintLayout = vi2.q;
            wd4.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            vi2.t.setImageResource(R.drawable.alerts_no_device);
            FlexibleTextView flexibleTextView = vi2.r;
            wd4.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(tm2.a(getContext(), (int) R.string.Onboarding_WithoutDevice_Alerts_Text__SetAlarmsAndGetNotificationUpdates));
            vi2.s.setOnClickListener(new c(this));
        }
        n62 n62 = new n62();
        n62.a((n62.b) new b(this));
        this.m = n62;
        RecyclerView recyclerView = vc2.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        n62 n622 = this.m;
        if (n622 != null) {
            recyclerView.setAdapter(n622);
            this.k = new ur3<>(this, vc2);
            ur3<vc2> ur3 = this.k;
            if (ur3 != null) {
                vc2 a2 = ur3.a();
                if (a2 != null) {
                    wd4.a((Object) a2, "mBinding.get()!!");
                    return a2.d();
                }
                wd4.a();
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        yy2 yy2 = this.l;
        if (yy2 != null) {
            if (yy2 != null) {
                yy2.g();
                wl2 Q0 = Q0();
                if (Q0 != null) {
                    Q0.a("");
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        super.onPause();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        yy2 yy2 = this.l;
        if (yy2 == null) {
            return;
        }
        if (yy2 != null) {
            yy2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void q(boolean z) {
        ur3<vc2> ur3 = this.k;
        if (ur3 != null) {
            vc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.w;
                wd4.a((Object) recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "binding.ftvAlarmsSection");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.r;
                wd4.a((Object) flexibleTextView2, "binding.ftvAdd");
                flexibleTextView2.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.w;
            wd4.a((Object) recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            FlexibleTextView flexibleTextView3 = a2.s;
            wd4.a((Object) flexibleTextView3, "binding.ftvAlarmsSection");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = a2.r;
            wd4.a((Object) flexibleTextView4, "binding.ftvAdd");
            flexibleTextView4.setVisibility(8);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.y(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        n62 n62 = this.m;
        if (n62 != null) {
            n62.notifyDataSetChanged();
        } else {
            wd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(yy2 yy2) {
        wd4.b(yy2, "presenter");
        this.l = yy2;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wd4.b(str, "deviceId");
        wd4.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            wd4.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        ur3<vc2> ur3 = this.k;
        if (ur3 != null) {
            vc2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
