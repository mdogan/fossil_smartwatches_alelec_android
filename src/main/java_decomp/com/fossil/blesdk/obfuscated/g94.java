package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g94 {
    @DexIgnore
    public static volatile q94<Callable<c94>, c94> a;
    @DexIgnore
    public static volatile q94<c94, c94> b;

    @DexIgnore
    public static c94 a(c94 c94) {
        if (c94 != null) {
            q94<c94, c94> q94 = b;
            if (q94 == null) {
                return c94;
            }
            a(q94, c94);
            return c94;
        }
        throw new NullPointerException("scheduler == null");
    }

    @DexIgnore
    public static c94 b(Callable<c94> callable) {
        if (callable != null) {
            q94<Callable<c94>, c94> q94 = a;
            if (q94 == null) {
                return a(callable);
            }
            return a(q94, callable);
        }
        throw new NullPointerException("scheduler == null");
    }

    @DexIgnore
    public static c94 a(Callable<c94> callable) {
        try {
            c94 call = callable.call();
            if (call != null) {
                return call;
            }
            throw new NullPointerException("Scheduler Callable returned null");
        } catch (Throwable th) {
            l94.a(th);
            throw null;
        }
    }

    @DexIgnore
    public static c94 a(q94<Callable<c94>, c94> q94, Callable<c94> callable) {
        a(q94, callable);
        c94 c94 = (c94) callable;
        if (c94 != null) {
            return c94;
        }
        throw new NullPointerException("Scheduler Callable returned null");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [R, T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <T, R> R a(q94<T, R> q94, T r1) {
        try {
            q94.apply(r1);
            return r1;
        } catch (Throwable th) {
            l94.a(th);
            throw null;
        }
    }
}
