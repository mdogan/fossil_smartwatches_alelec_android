package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gt0 extends zs0<T> {
    @DexIgnore
    public /* final */ Object k; // = new Object();
    @DexIgnore
    public String l;
    @DexIgnore
    public T m;
    @DexIgnore
    public /* final */ /* synthetic */ it0 n;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gt0(jt0 jt0, String str, Object obj, it0 it0) {
        super(jt0, str, obj, (dt0) null);
        this.n = it0;
    }

    @DexIgnore
    public final T a(SharedPreferences sharedPreferences) {
        try {
            return a(sharedPreferences.getString(this.b, ""));
        } catch (ClassCastException e) {
            String valueOf = String.valueOf(this.b);
            Log.e("PhenotypeFlag", valueOf.length() != 0 ? "Invalid byte[] value in SharedPreferences for ".concat(valueOf) : new String("Invalid byte[] value in SharedPreferences for "), e);
            return null;
        }
    }

    @DexIgnore
    public final T a(String str) {
        T t;
        try {
            synchronized (this.k) {
                if (!str.equals(this.l)) {
                    T a = this.n.a(Base64.decode(str, 3));
                    this.l = str;
                    this.m = a;
                }
                t = this.m;
            }
            return t;
        } catch (IOException | IllegalArgumentException unused) {
            String str2 = this.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 27 + String.valueOf(str).length());
            sb.append("Invalid byte[] value for ");
            sb.append(str2);
            sb.append(": ");
            sb.append(str);
            Log.e("PhenotypeFlag", sb.toString());
            return null;
        }
    }
}
