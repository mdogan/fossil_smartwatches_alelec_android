package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ek2 extends sv implements Cloneable {
    @DexIgnore
    public ek2 O() {
        super.O();
        return this;
    }

    @DexIgnore
    public ek2 P() {
        return (ek2) super.P();
    }

    @DexIgnore
    public ek2 Q() {
        return (ek2) super.Q();
    }

    @DexIgnore
    public ek2 R() {
        return (ek2) super.R();
    }

    @DexIgnore
    public ek2 c() {
        return (ek2) super.c();
    }

    @DexIgnore
    public ek2 b(boolean z) {
        return (ek2) super.b(z);
    }

    @DexIgnore
    public ek2 clone() {
        return (ek2) super.clone();
    }

    @DexIgnore
    public ek2 b(int i) {
        return (ek2) super.b(i);
    }

    @DexIgnore
    public ek2 a(float f) {
        return (ek2) super.a(f);
    }

    @DexIgnore
    public ek2 a(qp qpVar) {
        return (ek2) super.a(qpVar);
    }

    @DexIgnore
    public ek2 a(Priority priority) {
        return (ek2) super.a(priority);
    }

    @DexIgnore
    public ek2 a(boolean z) {
        return (ek2) super.a(z);
    }

    @DexIgnore
    public ek2 a(int i, int i2) {
        return (ek2) super.a(i, i2);
    }

    @DexIgnore
    public ek2 a(ko koVar) {
        return (ek2) super.a(koVar);
    }

    @DexIgnore
    public <Y> ek2 a(lo<Y> loVar, Y y) {
        return (ek2) super.a(loVar, y);
    }

    @DexIgnore
    public ek2 a(Class<?> cls) {
        return (ek2) super.a(cls);
    }

    @DexIgnore
    public ek2 a(DownsampleStrategy downsampleStrategy) {
        return (ek2) super.a(downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public ek2 a(po<Bitmap> r1) {
        return (ek2) super.a((po<Bitmap>) r1);
    }

    @DexIgnore
    public ek2 a(mv<?> mvVar) {
        return (ek2) super.a(mvVar);
    }

    @DexIgnore
    public ek2 a() {
        return (ek2) super.a();
    }
}
