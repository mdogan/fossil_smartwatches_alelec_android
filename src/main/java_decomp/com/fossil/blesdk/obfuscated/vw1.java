package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class vw1 implements Runnable {
    @DexIgnore
    public /* final */ Map.Entry e;
    @DexIgnore
    public /* final */ zw1 f;

    @DexIgnore
    public vw1(Map.Entry entry, zw1 zw1) {
        this.e = entry;
        this.f = zw1;
    }

    @DexIgnore
    public static Runnable a(Map.Entry entry, zw1 zw1) {
        return new vw1(entry, zw1);
    }

    @DexIgnore
    public final void run() {
        ((ax1) this.e.getKey()).a(this.f);
    }
}
