package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.WorkoutPagedAdapter;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xe3 extends as2 implements we3, View.OnClickListener {
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public WorkoutPagedAdapter j;
    @DexIgnore
    public Date k; // = new Date();
    @DexIgnore
    public /* final */ Calendar l; // = Calendar.getInstance();
    @DexIgnore
    public ur3<t92> m;
    @DexIgnore
    public ve3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final xe3 a(Date date) {
            wd4.b(date, "date");
            xe3 xe3 = new xe3();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            xe3.setArguments(bundle);
            return xe3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ rd b;

        @DexIgnore
        public b(xe3 xe3, boolean z, rd rdVar, Unit unit) {
            this.a = z;
            this.b = rdVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            wd4.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesDetailFragment";
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("CaloriesDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case R.id.iv_back /*2131362398*/:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case R.id.iv_back_date /*2131362399*/:
                    ve3 ve3 = this.n;
                    if (ve3 != null) {
                        ve3.j();
                        return;
                    }
                    return;
                case R.id.iv_next_date /*2131362447*/:
                    ve3 ve32 = this.n;
                    if (ve32 != null) {
                        ve32.i();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        t92 t92 = (t92) ra.a(layoutInflater, R.layout.fragment_calories_detail, viewGroup, false, O0());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.k = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.k = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        wd4.a((Object) t92, "binding");
        a(t92);
        ve3 ve3 = this.n;
        if (ve3 != null) {
            ve3.a(this.k);
        }
        this.m = new ur3<>(this, t92);
        ur3<t92> ur3 = this.m;
        if (ur3 != null) {
            t92 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public void onDestroyView() {
        ve3 ve3 = this.n;
        if (ve3 != null) {
            ve3.h();
        }
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ve3 ve3 = this.n;
        if (ve3 != null) {
            ve3.g();
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ve3 ve3 = this.n;
        if (ve3 != null) {
            ve3.b(this.k);
        }
        ve3 ve32 = this.n;
        if (ve32 != null) {
            ve32.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        ve3 ve3 = this.n;
        if (ve3 != null) {
            ve3.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(t92 t92) {
        t92.A.setOnClickListener(this);
        t92.B.setOnClickListener(this);
        t92.C.setOnClickListener(this);
        this.j = new WorkoutPagedAdapter(WorkoutPagedAdapter.WorkoutItem.CALORIES, Unit.IMPERIAL, new WorkoutSessionDifference());
        RecyclerView recyclerView = t92.G;
        wd4.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.j);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_eggshell_80a_line_1w);
        if (c != null) {
            zd3 zd3 = new zd3(linearLayoutManager.M(), false, false, 6, (rd4) null);
            wd4.a((Object) c, ResourceManager.DRAWABLE);
            zd3.a(c);
            recyclerView.a((RecyclerView.l) zd3);
        }
    }

    @DexIgnore
    public void a(ve3 ve3) {
        wd4.b(ve3, "presenter");
        this.n = ve3;
    }

    @DexIgnore
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        wd4.b(date, "date");
        this.k = date;
        Calendar calendar = this.l;
        wd4.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i = this.l.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.l);
        ur3<t92> ur3 = this.m;
        if (ur3 != null) {
            t92 a2 = ur3.a();
            if (a2 != null) {
                a2.q.a(true, true);
                FlexibleTextView flexibleTextView = a2.v;
                wd4.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.l.get(5)));
                if (z) {
                    ImageView imageView = a2.B;
                    wd4.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.B;
                    wd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.C;
                    wd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.w;
                        wd4.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(tm2.a(getContext(), (int) R.string.DashboardDiana_Main_ActiveCaloriesToday_Text__Today));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.w;
                    wd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(ml2.b.b(i));
                    return;
                }
                ImageView imageView4 = a2.C;
                wd4.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.w;
                wd4.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(ml2.b.b(i));
            }
        }
    }

    @DexIgnore
    public void a(Unit unit, ActivitySummary activitySummary) {
        int i;
        int i2;
        Unit unit2 = unit;
        ActivitySummary activitySummary2 = activitySummary;
        wd4.b(unit2, MFUser.DISTANCE_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDayDetail - distanceUnit=" + unit2 + ", activitySummary=" + activitySummary2);
        ur3<t92> ur3 = this.m;
        if (ur3 != null) {
            t92 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "binding");
                View d = a2.d();
                wd4.a((Object) d, "binding.root");
                Context context = d.getContext();
                if (activitySummary2 != null) {
                    i = Math.round((float) activitySummary.getCalories());
                    i2 = activitySummary.getCaloriesGoal();
                } else {
                    i2 = 0;
                    i = 0;
                }
                if (i > 0) {
                    FlexibleTextView flexibleTextView = a2.u;
                    wd4.a((Object) flexibleTextView, "binding.ftvDailyValue");
                    flexibleTextView.setText(pl2.a.a(Float.valueOf((float) i)));
                    FlexibleTextView flexibleTextView2 = a2.t;
                    wd4.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                    String a3 = tm2.a(context, (int) R.string.DashboardDiana_Main_ActiveCaloriesMonth_Label__Cals);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026aloriesMonth_Label__Cals)");
                    if (a3 != null) {
                        String lowerCase = a3.toLowerCase();
                        wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        flexibleTextView2.setText(lowerCase);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    FlexibleTextView flexibleTextView3 = a2.u;
                    wd4.a((Object) flexibleTextView3, "binding.ftvDailyValue");
                    flexibleTextView3.setText("");
                    FlexibleTextView flexibleTextView4 = a2.t;
                    wd4.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
                    String a4 = tm2.a(context, (int) R.string.DashboardDiana_Main_ActiveCaloriesToday_Text__NoRecord);
                    wd4.a((Object) a4, "LanguageHelper.getString\u2026riesToday_Text__NoRecord)");
                    if (a4 != null) {
                        String upperCase = a4.toUpperCase();
                        wd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        flexibleTextView4.setText(upperCase);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                int i3 = i2 > 0 ? (i * 100) / i2 : -1;
                if (i3 >= 100) {
                    ImageView imageView = a2.C;
                    wd4.a((Object) imageView, "binding.ivNextDate");
                    imageView.setSelected(true);
                    ImageView imageView2 = a2.B;
                    wd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setSelected(true);
                    ConstraintLayout constraintLayout = a2.r;
                    wd4.a((Object) constraintLayout, "binding.clOverviewDay");
                    constraintLayout.setSelected(true);
                    FlexibleTextView flexibleTextView5 = a2.w;
                    wd4.a((Object) flexibleTextView5, "binding.ftvDayOfWeek");
                    flexibleTextView5.setSelected(true);
                    FlexibleTextView flexibleTextView6 = a2.v;
                    wd4.a((Object) flexibleTextView6, "binding.ftvDayOfMonth");
                    flexibleTextView6.setSelected(true);
                    View view = a2.D;
                    wd4.a((Object) view, "binding.line");
                    view.setSelected(true);
                    FlexibleTextView flexibleTextView7 = a2.u;
                    wd4.a((Object) flexibleTextView7, "binding.ftvDailyValue");
                    flexibleTextView7.setSelected(true);
                    FlexibleTextView flexibleTextView8 = a2.t;
                    wd4.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
                    flexibleTextView8.setSelected(true);
                } else {
                    ImageView imageView3 = a2.C;
                    wd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setSelected(false);
                    ImageView imageView4 = a2.B;
                    wd4.a((Object) imageView4, "binding.ivBackDate");
                    imageView4.setSelected(false);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wd4.a((Object) constraintLayout2, "binding.clOverviewDay");
                    constraintLayout2.setSelected(false);
                    FlexibleTextView flexibleTextView9 = a2.w;
                    wd4.a((Object) flexibleTextView9, "binding.ftvDayOfWeek");
                    flexibleTextView9.setSelected(false);
                    FlexibleTextView flexibleTextView10 = a2.v;
                    wd4.a((Object) flexibleTextView10, "binding.ftvDayOfMonth");
                    flexibleTextView10.setSelected(false);
                    View view2 = a2.D;
                    wd4.a((Object) view2, "binding.line");
                    view2.setSelected(false);
                    FlexibleTextView flexibleTextView11 = a2.u;
                    wd4.a((Object) flexibleTextView11, "binding.ftvDailyValue");
                    flexibleTextView11.setSelected(false);
                    FlexibleTextView flexibleTextView12 = a2.t;
                    wd4.a((Object) flexibleTextView12, "binding.ftvDailyUnit");
                    flexibleTextView12.setSelected(false);
                }
                if (i3 == -1) {
                    ProgressBar progressBar = a2.F;
                    wd4.a((Object) progressBar, "binding.pbGoal");
                    progressBar.setProgress(0);
                    FlexibleTextView flexibleTextView13 = a2.z;
                    wd4.a((Object) flexibleTextView13, "binding.ftvProgressValue");
                    flexibleTextView13.setText(tm2.a(context, (int) R.string.character_dash_double));
                } else {
                    ProgressBar progressBar2 = a2.F;
                    wd4.a((Object) progressBar2, "binding.pbGoal");
                    progressBar2.setProgress(i3);
                    FlexibleTextView flexibleTextView14 = a2.z;
                    wd4.a((Object) flexibleTextView14, "binding.ftvProgressValue");
                    flexibleTextView14.setText(i3 + "%");
                }
                FlexibleTextView flexibleTextView15 = a2.x;
                wd4.a((Object) flexibleTextView15, "binding.ftvGoalValue");
                be4 be4 = be4.a;
                String a5 = tm2.a(context, (int) R.string.DashboardDiana_ActiveCalories_DetailPage_Title__OfNumberCals);
                wd4.a((Object) a5, "LanguageHelper.getString\u2026Page_Title__OfNumberCals)");
                Object[] objArr = {pl2.a.a(Float.valueOf((float) i2))};
                String format = String.format(a5, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView15.setText(format);
            }
        }
    }

    @DexIgnore
    public void a(xr2 xr2, ArrayList<String> arrayList) {
        wd4.b(xr2, "baseModel");
        wd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDayDetailChart - baseModel=" + xr2);
        ur3<t92> ur3 = this.m;
        if (ur3 != null) {
            t92 a2 = ur3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.s;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ml2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(boolean z, Unit unit, rd<WorkoutSession> rdVar) {
        wd4.b(unit, MFUser.DISTANCE_UNIT);
        wd4.b(rdVar, "workoutSessions");
        ur3<t92> ur3 = this.m;
        if (ur3 != null) {
            t92 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.E;
                    wd4.a((Object) linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!rdVar.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.y;
                        wd4.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.G;
                        wd4.a((Object) recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        WorkoutPagedAdapter workoutPagedAdapter = this.j;
                        if (workoutPagedAdapter != null) {
                            workoutPagedAdapter.a(unit, rdVar);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.y;
                        wd4.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.G;
                        wd4.a((Object) recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        WorkoutPagedAdapter workoutPagedAdapter2 = this.j;
                        if (workoutPagedAdapter2 != null) {
                            workoutPagedAdapter2.a(unit, rdVar);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.E;
                    wd4.a((Object) linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                wd4.a((Object) appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new b(this, z, rdVar, unit));
                    eVar.a((CoordinatorLayout.Behavior) behavior);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
        }
    }
}
