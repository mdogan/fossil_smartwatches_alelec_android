package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.alarm.Alarm;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n20 extends t20<Alarm[], Alarm[]> {
    @DexIgnore
    public static /* final */ l20<Alarm[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<Alarm[]>[] b; // = {new c(FileType.ALARM), new d(FileType.ALARM)};
    @DexIgnore
    public static /* final */ n20 c; // = new n20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<Alarm[]> {
        @DexIgnore
        public byte[] a(Alarm[] alarmArr) {
            wd4.b(alarmArr, "entries");
            return n20.c.a(alarmArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<Alarm[]> {
        @DexIgnore
        public byte[] a(Alarm[] alarmArr) {
            wd4.b(alarmArr, "entries");
            return n20.c.a(alarmArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends u20<Alarm[]> {
        @DexIgnore
        public c(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public Alarm[] b(byte[] bArr) {
            wd4.b(bArr, "data");
            return n20.c.b(bArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends v20<Alarm[]> {
        @DexIgnore
        public d(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public Alarm[] b(byte[] bArr) {
            wd4.b(bArr, "data");
            return n20.c.b(bArr);
        }
    }

    @DexIgnore
    public m20<Alarm[]>[] b() {
        return b;
    }

    @DexIgnore
    public final Alarm[] b(byte[] bArr) throws FileFormatException {
        byte[] a2 = kb4.a(bArr, 12, bArr.length - 4);
        if (a2.length % 3 == 0) {
            ArrayList arrayList = new ArrayList();
            ie4 a3 = qe4.a((ie4) qe4.d(0, a2.length), 3);
            int a4 = a3.a();
            int b2 = a3.b();
            int c2 = a3.c();
            if (c2 < 0 ? a4 >= b2 : a4 <= b2) {
                while (true) {
                    arrayList.add(Alarm.CREATOR.a(kb4.a(a2, a4, a4 + 3)));
                    if (a4 == b2) {
                        break;
                    }
                    a4 += c2;
                }
            }
            Object[] array = arrayList.toArray(new Alarm[0]);
            if (array != null) {
                return (Alarm[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        FileFormatException.FileFormatErrorCode fileFormatErrorCode = FileFormatException.FileFormatErrorCode.INVALID_FILE_DATA;
        throw new FileFormatException(fileFormatErrorCode, "Size(" + a2.length + ") not divide to 3.", (Throwable) null, 4, (rd4) null);
    }

    @DexIgnore
    public l20<Alarm[]>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(Alarm[] alarmArr) {
        ByteBuffer allocate = ByteBuffer.allocate(alarmArr.length * 3);
        for (Alarm data$blesdk_productionRelease : alarmArr) {
            allocate.put(data$blesdk_productionRelease.getData$blesdk_productionRelease());
        }
        byte[] array = allocate.array();
        wd4.a((Object) array, "entryDataBuffer.array()");
        return array;
    }
}
