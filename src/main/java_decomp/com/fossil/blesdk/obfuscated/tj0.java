package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tj0 {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public zd0 b;

    @DexIgnore
    public tj0(zd0 zd0) {
        ck0.a(zd0);
        this.b = zd0;
    }

    @DexIgnore
    public int a(Context context, ee0.f fVar) {
        ck0.a(context);
        ck0.a(fVar);
        if (!fVar.h()) {
            return 0;
        }
        int i = fVar.i();
        int i2 = this.a.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.a.size()) {
                break;
            }
            int keyAt = this.a.keyAt(i3);
            if (keyAt > i && this.a.get(keyAt) == 0) {
                i2 = 0;
                break;
            }
            i3++;
        }
        if (i2 == -1) {
            i2 = this.b.a(context, i);
        }
        this.a.put(i, i2);
        return i2;
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }
}
