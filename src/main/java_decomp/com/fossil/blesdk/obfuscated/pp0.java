package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pp0 implements Parcelable.Creator<DataSet> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        ap0 ap0 = null;
        ArrayList<ap0> arrayList2 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                ap0 = SafeParcelReader.a(parcel, a, ap0.CREATOR);
            } else if (a2 == 1000) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                SafeParcelReader.a(parcel, a, (List) arrayList, pp0.class.getClassLoader());
            } else if (a2 == 4) {
                arrayList2 = SafeParcelReader.c(parcel, a, ap0.CREATOR);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                z = SafeParcelReader.i(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new DataSet(i, ap0, arrayList, arrayList2, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataSet[i];
    }
}
