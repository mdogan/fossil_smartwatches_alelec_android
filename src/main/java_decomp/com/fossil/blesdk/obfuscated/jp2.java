package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.ShakeFeedbackService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jp2 implements Factory<ShakeFeedbackService> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public jp2(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static jp2 a(Provider<UserRepository> provider) {
        return new jp2(provider);
    }

    @DexIgnore
    public static ShakeFeedbackService b(Provider<UserRepository> provider) {
        return new ShakeFeedbackService(provider.get());
    }

    @DexIgnore
    public ShakeFeedbackService get() {
        return b(this.a);
    }
}
