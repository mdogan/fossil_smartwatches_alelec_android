package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p60 extends h70 {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p60(Peripheral peripheral, byte[] bArr, GattCharacteristic.CharacteristicId characteristicId) {
        super(RequestId.CUSTOM_COMMAND, peripheral);
        wd4.b(peripheral, "peripheral");
        wd4.b(bArr, "commandData");
        wd4.b(characteristicId, "characteristicId");
        this.A = bArr;
        this.B = characteristicId;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new o10(this.B, this.A, i().h());
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId B() {
        return this.B;
    }
}
