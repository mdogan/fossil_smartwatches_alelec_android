package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bb0 {
    @DexIgnore
    public static /* final */ bb0 a; // = new bb0();

    @DexIgnore
    public final SharedPreferences a(String str) {
        wd4.b(str, "name");
        Context a2 = wa0.f.a();
        if (a2 != null) {
            return a2.getSharedPreferences(str, 0);
        }
        return null;
    }
}
