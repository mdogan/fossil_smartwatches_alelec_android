package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ml0 implements Parcelable.Creator<ek0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        IBinder iBinder = null;
        vd0 vd0 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                iBinder = SafeParcelReader.p(parcel, a);
            } else if (a2 == 3) {
                vd0 = SafeParcelReader.a(parcel, a, vd0.CREATOR);
            } else if (a2 == 4) {
                z = SafeParcelReader.i(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                z2 = SafeParcelReader.i(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ek0(i, iBinder, vd0, z, z2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ek0[i];
    }
}
