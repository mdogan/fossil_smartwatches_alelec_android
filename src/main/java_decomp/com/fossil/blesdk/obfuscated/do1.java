package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class do1<TResult, TContinuationResult> implements ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ qn1<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ vo1<TContinuationResult> c;

    @DexIgnore
    public do1(Executor executor, qn1<TResult, TContinuationResult> qn1, vo1<TContinuationResult> vo1) {
        this.a = executor;
        this.b = qn1;
        this.c = vo1;
    }

    @DexIgnore
    public final void onComplete(xn1<TResult> xn1) {
        this.a.execute(new eo1(this, xn1));
    }
}
