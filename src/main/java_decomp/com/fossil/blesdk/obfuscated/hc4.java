package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Set;
import kotlin.collections.EmptySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hc4 extends gc4 {
    @DexIgnore
    public static final <T> Set<T> a() {
        return EmptySet.INSTANCE;
    }

    @DexIgnore
    public static final <T> Set<T> a(T... tArr) {
        wd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length > 0 ? lb4.h(tArr) : a();
    }

    @DexIgnore
    public static final <T> Set<T> a(Set<? extends T> set) {
        wd4.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return set;
        }
        return gc4.a(set.iterator().next());
    }
}
