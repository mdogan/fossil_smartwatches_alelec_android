package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xd4 implements od4 {
    @DexIgnore
    public /* final */ Class<?> e;

    @DexIgnore
    public xd4(Class<?> cls, String str) {
        wd4.b(cls, "jClass");
        wd4.b(str, "moduleName");
        this.e = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof xd4) && wd4.a((Object) a(), (Object) ((xd4) obj).a());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
