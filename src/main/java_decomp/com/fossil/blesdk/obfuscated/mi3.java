package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mi3 implements Factory<HelpPresenter> {
    @DexIgnore
    public static HelpPresenter a(ii3 ii3, DeviceRepository deviceRepository, lr2 lr2, AnalyticsHelper analyticsHelper) {
        return new HelpPresenter(ii3, deviceRepository, lr2, analyticsHelper);
    }
}
