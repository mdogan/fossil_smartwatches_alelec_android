package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ms<DataType> implements no<DataType, BitmapDrawable> {
    @DexIgnore
    public /* final */ no<DataType, Bitmap> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public ms(Resources resources, no<DataType, Bitmap> noVar) {
        uw.a(resources);
        this.b = resources;
        uw.a(noVar);
        this.a = noVar;
    }

    @DexIgnore
    public boolean a(DataType datatype, mo moVar) throws IOException {
        return this.a.a(datatype, moVar);
    }

    @DexIgnore
    public bq<BitmapDrawable> a(DataType datatype, int i, int i2, mo moVar) throws IOException {
        return et.a(this.b, this.a.a(datatype, i, i2, moVar));
    }
}
