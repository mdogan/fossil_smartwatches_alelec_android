package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qv3 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(hv3.class.getName());
    @DexIgnore
    public static qv3 b;

    @DexIgnore
    public abstract ex3 a(xu3 xu3, vw3 vw3) throws IOException;

    @DexIgnore
    public abstract rv3 a(hv3 hv3);

    @DexIgnore
    public abstract void a(dv3.b bVar, String str);

    @DexIgnore
    public abstract void a(hv3 hv3, xu3 xu3, vw3 vw3, iv3 iv3) throws RouteException;

    @DexIgnore
    public abstract void a(xu3 xu3, Protocol protocol);

    @DexIgnore
    public abstract void a(yu3 yu3, xu3 xu3);

    @DexIgnore
    public abstract void a(zu3 zu3, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract boolean a(xu3 xu3);

    @DexIgnore
    public abstract tv3 b(hv3 hv3);

    @DexIgnore
    public abstract void b(xu3 xu3, vw3 vw3);

    @DexIgnore
    public abstract boolean b(xu3 xu3);

    @DexIgnore
    public abstract int c(xu3 xu3);

    @DexIgnore
    public abstract wv3 c(hv3 hv3);
}
