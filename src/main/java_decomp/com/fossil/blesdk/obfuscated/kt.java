package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uo;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kt implements uo<ByteBuffer> {
    @DexIgnore
    public /* final */ ByteBuffer a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements uo.a<ByteBuffer> {
        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }

        @DexIgnore
        public uo<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new kt(byteBuffer);
        }
    }

    @DexIgnore
    public kt(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public ByteBuffer b() {
        this.a.position(0);
        return this.a;
    }
}
