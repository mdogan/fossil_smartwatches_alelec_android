package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class no3 implements Factory<SignUpPresenter> {
    @DexIgnore
    public static SignUpPresenter a(io3 io3, BaseActivity baseActivity) {
        return new SignUpPresenter(io3, baseActivity);
    }
}
