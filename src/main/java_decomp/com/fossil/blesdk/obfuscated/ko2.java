package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ko2 extends ho2 {
    @DexIgnore
    public static /* final */ String i; // = ko2.class.getSimpleName();
    @DexIgnore
    public /* final */ HashMap<String, Long> f; // = new HashMap<>();
    @DexIgnore
    public DianaNotificationComponent g;
    @DexIgnore
    public fn2 h;

    @DexIgnore
    public void a(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onMissedCall : " + str);
        if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.R.e()) && !TextUtils.isEmpty(str)) {
            this.g.a(str, date, DianaNotificationComponent.PhoneStateEnum.PICKED);
        }
    }

    @DexIgnore
    public void a(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    public void b(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onIncomingCallStarted : " + str);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.R.e())) {
            a(str);
        } else {
            a(str, date);
        }
    }

    @DexIgnore
    public void b(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    public void c(Context context, String str, Date date) {
        if (Build.VERSION.SDK_INT < 28) {
            this.g.a(str, date, DianaNotificationComponent.PhoneStateEnum.MISSED);
        }
    }

    @DexIgnore
    public void d(Context context, String str, Date date) {
    }

    @DexIgnore
    public final void a(String str, Date date) {
        if (!TextUtils.isEmpty(str)) {
            this.g.a(str, date, DianaNotificationComponent.PhoneStateEnum.RINGING);
        }
    }

    @DexIgnore
    public final void a(String str) {
        String b = us3.b(str);
        boolean D = this.h.D();
        synchronized (this.f) {
            this.f.put(b, Long.valueOf(System.currentTimeMillis()));
        }
        if (D) {
            FLogger.INSTANCE.getLocal().d(i, "Phone Receiver - blocked by DND mode");
            return;
        }
        LightAndHapticsManager.i.a().a(new NotificationInfo(NotificationSource.CALL, b, "", ""));
        a();
    }

    @DexIgnore
    public final void a() {
        synchronized (this.f) {
            long currentTimeMillis = System.currentTimeMillis();
            int size = this.f.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Clean past call. Size = " + size);
            LinkedList<String> linkedList = new LinkedList<>();
            for (Map.Entry next : this.f.entrySet()) {
                String str2 = (String) next.getKey();
                if (currentTimeMillis - ((Long) next.getValue()).longValue() > 900000) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = i;
                    local2.d(str3, "Adding key to remove - key = " + str2);
                    linkedList.add(str2);
                }
            }
            for (String remove : linkedList) {
                FLogger.INSTANCE.getLocal().d(i, "Dumping old call");
                this.f.remove(remove);
            }
        }
    }
}
