package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l52 implements Factory<en2> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public l52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static l52 a(o42 o42) {
        return new l52(o42);
    }

    @DexIgnore
    public static en2 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static en2 c(o42 o42) {
        en2 l = o42.l();
        o44.a(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    public en2 get() {
        return b(this.a);
    }
}
