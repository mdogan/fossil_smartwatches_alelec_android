package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class x92 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ nh2 s;
    @DexIgnore
    public /* final */ nh2 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public x92(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, nh2 nh2, nh2 nh22, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = nh2;
        a((ViewDataBinding) this.s);
        this.t = nh22;
        a((ViewDataBinding) this.t);
        this.u = linearLayout;
    }
}
