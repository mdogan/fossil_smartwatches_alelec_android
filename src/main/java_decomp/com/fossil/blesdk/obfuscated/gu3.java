package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.HorizontalScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"ViewConstructor"})
public final class gu3 extends HorizontalScrollView {
    @DexIgnore
    public long e; // = -1;
    @DexIgnore
    public b f;
    @DexIgnore
    public Runnable g; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            gu3 gu3 = gu3.this;
            if (currentTimeMillis - gu3.e > 100) {
                gu3.e = -1;
                gu3.f.a();
                return;
            }
            gu3.postDelayed(this, 100);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void onScrollChanged();
    }

    @DexIgnore
    public gu3(Context context, b bVar) {
        super(context);
        this.f = bVar;
    }

    @DexIgnore
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        b bVar = this.f;
        if (bVar != null) {
            bVar.onScrollChanged();
            if (this.e == -1) {
                postDelayed(this.g, 100);
            }
            this.e = System.currentTimeMillis();
        }
    }
}
