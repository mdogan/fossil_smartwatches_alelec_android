package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g91 {
    @DexIgnore
    public volatile x91 a;
    @DexIgnore
    public volatile zzte b;

    /*
    static {
        j81.c();
    }
    */

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    public final x91 a(x91 x91) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = x91;
                    this.b = zzte.zzbts;
                    this.a = x91;
                    this.b = zzte.zzbts;
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public final x91 b(x91 x91) {
        x91 x912 = this.a;
        this.b = null;
        this.a = x91;
        return x912;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g91)) {
            return false;
        }
        g91 g91 = (g91) obj;
        x91 x91 = this.a;
        x91 x912 = g91.a;
        if (x91 == null && x912 == null) {
            return a().equals(g91.a());
        }
        if (x91 != null && x912 != null) {
            return x91.equals(x912);
        }
        if (x91 != null) {
            return x91.equals(g91.a(x91.b()));
        }
        return a(x912.b()).equals(x912);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final int b() {
        if (this.b != null) {
            return this.b.size();
        }
        if (this.a != null) {
            return this.a.e();
        }
        return 0;
    }

    @DexIgnore
    public final zzte a() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                zzte zzte = this.b;
                return zzte;
            }
            if (this.a == null) {
                this.b = zzte.zzbts;
            } else {
                this.b = this.a.d();
            }
            zzte zzte2 = this.b;
            return zzte2;
        }
    }
}
