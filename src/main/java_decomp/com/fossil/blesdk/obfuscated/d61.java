package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d61 extends wb1<d61> {
    @DexIgnore
    public static volatile d61[] h;
    @DexIgnore
    public e61[] c; // = e61.e();
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Long e; // = null;
    @DexIgnore
    public Long f; // = null;
    @DexIgnore
    public Integer g; // = null;

    @DexIgnore
    public d61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static d61[] e() {
        if (h == null) {
            synchronized (ac1.b) {
                if (h == null) {
                    h = new d61[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        e61[] e61Arr = this.c;
        if (e61Arr != null && e61Arr.length > 0) {
            int i = 0;
            while (true) {
                e61[] e61Arr2 = this.c;
                if (i >= e61Arr2.length) {
                    break;
                }
                e61 e61 = e61Arr2[i];
                if (e61 != null) {
                    vb1.a(1, (bc1) e61);
                }
                i++;
            }
        }
        String str = this.d;
        if (str != null) {
            vb1.a(2, str);
        }
        Long l = this.e;
        if (l != null) {
            vb1.b(3, l.longValue());
        }
        Long l2 = this.f;
        if (l2 != null) {
            vb1.b(4, l2.longValue());
        }
        Integer num = this.g;
        if (num != null) {
            vb1.b(5, num.intValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d61)) {
            return false;
        }
        d61 d61 = (d61) obj;
        if (!ac1.a((Object[]) this.c, (Object[]) d61.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (d61.d != null) {
                return false;
            }
        } else if (!str.equals(d61.d)) {
            return false;
        }
        Long l = this.e;
        if (l == null) {
            if (d61.e != null) {
                return false;
            }
        } else if (!l.equals(d61.e)) {
            return false;
        }
        Long l2 = this.f;
        if (l2 == null) {
            if (d61.f != null) {
                return false;
            }
        } else if (!l2.equals(d61.f)) {
            return false;
        }
        Integer num = this.g;
        if (num == null) {
            if (d61.g != null) {
                return false;
            }
        } else if (!num.equals(d61.g)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(d61.b);
        }
        yb1 yb12 = d61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((d61.class.getName().hashCode() + 527) * 31) + ac1.a((Object[]) this.c)) * 31;
        String str = this.d;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Long l = this.e;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        Long l2 = this.f;
        int hashCode4 = (hashCode3 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Integer num = this.g;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        e61[] e61Arr = this.c;
        if (e61Arr != null && e61Arr.length > 0) {
            int i = 0;
            while (true) {
                e61[] e61Arr2 = this.c;
                if (i >= e61Arr2.length) {
                    break;
                }
                e61 e61 = e61Arr2[i];
                if (e61 != null) {
                    a += vb1.b(1, (bc1) e61);
                }
                i++;
            }
        }
        String str = this.d;
        if (str != null) {
            a += vb1.b(2, str);
        }
        Long l = this.e;
        if (l != null) {
            a += vb1.c(3, l.longValue());
        }
        Long l2 = this.f;
        if (l2 != null) {
            a += vb1.c(4, l2.longValue());
        }
        Integer num = this.g;
        return num != null ? a + vb1.c(5, num.intValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                int a = ec1.a(ub1, 10);
                e61[] e61Arr = this.c;
                int length = e61Arr == null ? 0 : e61Arr.length;
                e61[] e61Arr2 = new e61[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, e61Arr2, 0, length);
                }
                while (length < e61Arr2.length - 1) {
                    e61Arr2[length] = new e61();
                    ub1.a((bc1) e61Arr2[length]);
                    ub1.c();
                    length++;
                }
                e61Arr2[length] = new e61();
                ub1.a((bc1) e61Arr2[length]);
                this.c = e61Arr2;
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (c2 == 24) {
                this.e = Long.valueOf(ub1.f());
            } else if (c2 == 32) {
                this.f = Long.valueOf(ub1.f());
            } else if (c2 == 40) {
                this.g = Integer.valueOf(ub1.e());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
