package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ct {
    @DexIgnore
    public static /* final */ File d; // = new File("/proc/self/fd");
    @DexIgnore
    public static volatile int e; // = 700;
    @DexIgnore
    public static volatile int f; // = 128;
    @DexIgnore
    public static volatile ct g;
    @DexIgnore
    public /* final */ boolean a; // = c();
    @DexIgnore
    public int b;
    @DexIgnore
    public boolean c; // = true;

    @DexIgnore
    public static ct b() {
        if (g == null) {
            synchronized (ct.class) {
                if (g == null) {
                    g = new ct();
                }
            }
        }
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static boolean c() {
        char c2;
        String str = Build.MODEL;
        if (str == null || str.length() < 7) {
            return true;
        }
        String substring = Build.MODEL.substring(0, 7);
        switch (substring.hashCode()) {
            case -1398613787:
                if (substring.equals("SM-A520")) {
                    c2 = 6;
                    break;
                }
            case -1398431166:
                if (substring.equals("SM-G930")) {
                    c2 = 5;
                    break;
                }
            case -1398431161:
                if (substring.equals("SM-G935")) {
                    c2 = 4;
                    break;
                }
            case -1398431073:
                if (substring.equals("SM-G960")) {
                    c2 = 2;
                    break;
                }
            case -1398431068:
                if (substring.equals("SM-G965")) {
                    c2 = 3;
                    break;
                }
            case -1398343746:
                if (substring.equals("SM-J720")) {
                    c2 = 1;
                    break;
                }
            case -1398222624:
                if (substring.equals("SM-N935")) {
                    c2 = 0;
                    break;
                }
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                if (Build.VERSION.SDK_INT != 26) {
                    return true;
                }
                return false;
            default:
                return true;
        }
    }

    @DexIgnore
    public boolean a(int i, int i2, boolean z, boolean z2) {
        if (!z || !this.a || Build.VERSION.SDK_INT < 26 || z2 || i < f || i2 < f || !a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @TargetApi(26)
    public boolean a(int i, int i2, BitmapFactory.Options options, boolean z, boolean z2) {
        boolean a2 = a(i, i2, z, z2);
        if (a2) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return a2;
    }

    @DexIgnore
    public final synchronized boolean a() {
        int i = this.b + 1;
        this.b = i;
        if (i >= 50) {
            boolean z = false;
            this.b = 0;
            int length = d.list().length;
            if (length < e) {
                z = true;
            }
            this.c = z;
            if (!this.c && Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + e);
            }
        }
        return this.c;
    }
}
