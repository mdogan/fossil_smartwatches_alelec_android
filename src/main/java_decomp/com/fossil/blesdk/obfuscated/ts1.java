package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ts1 extends Transition {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ TextView a;

        @DexIgnore
        public a(ts1 ts1, TextView textView) {
            this.a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public void a(qh qhVar) {
        d(qhVar);
    }

    @DexIgnore
    public void c(qh qhVar) {
        d(qhVar);
    }

    @DexIgnore
    public final void d(qh qhVar) {
        View view = qhVar.b;
        if (view instanceof TextView) {
            qhVar.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    public Animator a(ViewGroup viewGroup, qh qhVar, qh qhVar2) {
        if (qhVar == null || qhVar2 == null || !(qhVar.b instanceof TextView)) {
            return null;
        }
        View view = qhVar2.b;
        if (!(view instanceof TextView)) {
            return null;
        }
        TextView textView = (TextView) view;
        Map<String, Object> map = qhVar.a;
        Map<String, Object> map2 = qhVar2.a;
        float f = 1.0f;
        float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
        if (map2.get("android:textscale:scale") != null) {
            f = ((Float) map2.get("android:textscale:scale")).floatValue();
        }
        if (floatValue == f) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{floatValue, f});
        ofFloat.addUpdateListener(new a(this, textView));
        return ofFloat;
    }
}
