package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ve1 extends b51 implements je1 {
    @DexIgnore
    public ve1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    public final tn0 a(float f) throws RemoteException {
        Parcel o = o();
        o.writeFloat(f);
        Parcel a = a(4, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final tn0 a(LatLng latLng, float f) throws RemoteException {
        Parcel o = o();
        d51.a(o, (Parcelable) latLng);
        o.writeFloat(f);
        Parcel a = a(9, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
