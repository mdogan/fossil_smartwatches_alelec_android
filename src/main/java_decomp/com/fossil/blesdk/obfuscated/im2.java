package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.mms.InvalidHeaderValueException;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class im2 {
    @DexIgnore
    public HashMap<Integer, Object> a;

    @DexIgnore
    public im2() {
        this.a = null;
        this.a = new HashMap<>();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x010c, code lost:
        if (r9 < 192) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x010e, code lost:
        if (r9 > 255) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x011d, code lost:
        if (r9 <= 255) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0124, code lost:
        if (r9 < 192) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0126, code lost:
        if (r9 > 255) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x007a, code lost:
        if (r9 <= 255) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0082, code lost:
        if (r9 < 192) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0084, code lost:
        if (r9 > 255) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0105, code lost:
        if (r9 <= 255) goto L_0x0135;
     */
    @DexIgnore
    public void a(int i, int i2) throws InvalidHeaderValueException {
        int i3 = 224;
        if (i2 != 134) {
            if (i2 != 153) {
                if (i2 != 165) {
                    if (!(i2 == 167 || i2 == 169 || i2 == 171 || i2 == 177)) {
                        if (i2 != 180) {
                            if (i2 != 191) {
                                if (i2 != 140) {
                                    if (i2 != 141) {
                                        if (i2 != 148) {
                                            if (i2 != 149) {
                                                if (i2 != 155) {
                                                    if (i2 != 156) {
                                                        if (i2 != 162) {
                                                            if (i2 != 163) {
                                                                switch (i2) {
                                                                    case 143:
                                                                        if (i < 128 || i > 130) {
                                                                            throw new InvalidHeaderValueException("Invalid Octet value!");
                                                                        }
                                                                    case 144:
                                                                    case 145:
                                                                        break;
                                                                    case 146:
                                                                        if (i <= 196 || i >= 224) {
                                                                            if (i > 235) {
                                                                            }
                                                                            if (i >= 128) {
                                                                                if (i > 136) {
                                                                                }
                                                                            }
                                                                        }
                                                                        break;
                                                                    default:
                                                                        switch (i2) {
                                                                            case 186:
                                                                                if (i < 128 || i > 135) {
                                                                                    throw new InvalidHeaderValueException("Invalid Octet value!");
                                                                                }
                                                                            case 187:
                                                                            case 188:
                                                                                break;
                                                                            default:
                                                                                throw new RuntimeException("Invalid header field!");
                                                                        }
                                                                        break;
                                                                }
                                                            } else if (i < 128 || i > 132) {
                                                                throw new InvalidHeaderValueException("Invalid Octet value!");
                                                            }
                                                        }
                                                    } else if (i < 128 || i > 131) {
                                                        throw new InvalidHeaderValueException("Invalid Octet value!");
                                                    }
                                                } else if (!(128 == i || 129 == i)) {
                                                    throw new InvalidHeaderValueException("Invalid Octet value!");
                                                }
                                            } else if (i < 128 || i > 135) {
                                                throw new InvalidHeaderValueException("Invalid Octet value!");
                                            }
                                        }
                                    } else if (i < 16 || i > 19) {
                                        i3 = 18;
                                        this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
                                    }
                                } else if (i < 128 || i > 151) {
                                    throw new InvalidHeaderValueException("Invalid Octet value!");
                                }
                            } else if (!(128 == i || 129 == i)) {
                                throw new InvalidHeaderValueException("Invalid Octet value!");
                            }
                        } else if (128 != i) {
                            throw new InvalidHeaderValueException("Invalid Octet value!");
                        }
                        i3 = i;
                        this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
                    }
                } else if (i <= 193 || i >= 224) {
                    if (i > 228) {
                    }
                    if (i >= 128) {
                        if (i > 128) {
                        }
                    }
                    this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
                }
            } else if (i <= 194 || i >= 224) {
                if (i > 227) {
                }
                if (i >= 128) {
                    if (i > 128) {
                    }
                }
                this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
            }
            i3 = 192;
            this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
        }
        if (!(128 == i || 129 == i)) {
            throw new InvalidHeaderValueException("Invalid Octet value!");
        }
        i3 = i;
        this.a.put(Integer.valueOf(i2), Integer.valueOf(i3));
    }

    @DexIgnore
    public bm2[] b(int i) {
        ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
        if (arrayList == null) {
            return null;
        }
        return (bm2[]) arrayList.toArray(new bm2[arrayList.size()]);
    }

    @DexIgnore
    public long c(int i) {
        Long l = (Long) this.a.get(Integer.valueOf(i));
        if (l == null) {
            return -1;
        }
        return l.longValue();
    }

    @DexIgnore
    public int d(int i) {
        Integer num = (Integer) this.a.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    @DexIgnore
    public byte[] e(int i) {
        return (byte[]) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void b(bm2 bm2, int i) {
        if (bm2 == null) {
            throw new NullPointerException();
        } else if (i == 137 || i == 147 || i == 150 || i == 154 || i == 160 || i == 164 || i == 166 || i == 181 || i == 182) {
            this.a.put(Integer.valueOf(i), bm2);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public void a(byte[] bArr, int i) {
        if (bArr != null) {
            if (!(i == 131 || i == 132 || i == 138 || i == 139 || i == 152 || i == 158 || i == 189 || i == 190)) {
                switch (i) {
                    case 183:
                    case 184:
                    case 185:
                        break;
                    default:
                        throw new RuntimeException("Invalid header field!");
                }
            }
            this.a.put(Integer.valueOf(i), bArr);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public bm2 a(int i) {
        return (bm2) this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(bm2 bm2, int i) {
        if (bm2 == null) {
            throw new NullPointerException();
        } else if (i == 129 || i == 130 || i == 151) {
            ArrayList arrayList = (ArrayList) this.a.get(Integer.valueOf(i));
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(bm2);
            this.a.put(Integer.valueOf(i), arrayList);
        } else {
            throw new RuntimeException("Invalid header field!");
        }
    }

    @DexIgnore
    public void a(long j, int i) {
        if (i == 133 || i == 142 || i == 157 || i == 159 || i == 161 || i == 173 || i == 175 || i == 179 || i == 135 || i == 136) {
            this.a.put(Integer.valueOf(i), Long.valueOf(j));
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }
}
