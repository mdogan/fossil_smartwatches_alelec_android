package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class li0 implements dh0 {
    @DexIgnore
    public /* final */ /* synthetic */ ji0 a;

    @DexIgnore
    public li0(ji0 ji0) {
        this.a = ji0;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        this.a.q.lock();
        try {
            this.a.a(bundle);
            vd0 unused = this.a.n = vd0.i;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ li0(ji0 ji0, ki0 ki0) {
        this(ji0);
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        this.a.q.lock();
        try {
            vd0 unused = this.a.n = vd0;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a.q.lock();
        try {
            if (!this.a.p && this.a.o != null) {
                if (this.a.o.L()) {
                    boolean unused = this.a.p = true;
                    this.a.i.f(i);
                    this.a.q.unlock();
                    return;
                }
            }
            boolean unused2 = this.a.p = false;
            this.a.a(i, z);
        } finally {
            this.a.q.unlock();
        }
    }
}
