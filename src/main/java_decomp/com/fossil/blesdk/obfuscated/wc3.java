package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wc3 extends as2 implements vc3 {
    @DexIgnore
    public ur3<pc2> j;
    @DexIgnore
    public uc3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        this.j = new ur3<>(this, (pc2) ra.a(layoutInflater, R.layout.fragment_heartrate_overview_week, viewGroup, false, O0()));
        ur3<pc2> ur3 = this.j;
        if (ur3 != null) {
            pc2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        uc3 uc3 = this.k;
        if (uc3 != null) {
            uc3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        uc3 uc3 = this.k;
        if (uc3 != null) {
            uc3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(uc3 uc3) {
        wd4.b(uc3, "presenter");
        this.k = uc3;
    }

    @DexIgnore
    public void a(List<Integer> list, List<String> list2) {
        wd4.b(list, "data");
        wd4.b(list2, "listWeekDays");
        ur3<pc2> ur3 = this.j;
        if (ur3 != null) {
            pc2 a2 = ur3.a();
            if (a2 != null) {
                WeekHeartRateChart weekHeartRateChart = a2.q;
                if (weekHeartRateChart != null) {
                    weekHeartRateChart.setListWeekDays(list2);
                    weekHeartRateChart.a(list);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
