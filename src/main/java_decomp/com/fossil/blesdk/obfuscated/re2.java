package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class re2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ DashBar u;

    @DexIgnore
    public re2(Object obj, View view, int i, Barrier barrier, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, ImageView imageView2, ImageView imageView3, DashBar dashBar, RelativeLayout relativeLayout) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleButton2;
        this.s = imageView;
        this.t = imageView3;
        this.u = dashBar;
    }
}
