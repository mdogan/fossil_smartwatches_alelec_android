package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qu0 extends nt0<Float> implements xu0<Float>, RandomAccess {
    @DexIgnore
    public float[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new qu0().z();
    }
    */

    @DexIgnore
    public qu0() {
        this(new float[10], 0);
    }

    @DexIgnore
    public qu0(float[] fArr, int i) {
        this.f = fArr;
        this.g = i;
    }

    @DexIgnore
    public final void a(float f2) {
        a(this.g, f2);
    }

    @DexIgnore
    public final void a(int i) {
        if (i < 0 || i >= this.g) {
            throw new IndexOutOfBoundsException(f(i));
        }
    }

    @DexIgnore
    public final void a(int i, float f2) {
        a();
        if (i >= 0) {
            int i2 = this.g;
            if (i <= i2) {
                float[] fArr = this.f;
                if (i2 < fArr.length) {
                    System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
                } else {
                    float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
                    System.arraycopy(fArr, 0, fArr2, 0, i);
                    System.arraycopy(this.f, i, fArr2, i + 1, this.g - i);
                    this.f = fArr2;
                }
                this.f[i] = f2;
                this.g++;
                this.modCount++;
                return;
            }
        }
        throw new IndexOutOfBoundsException(f(i));
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Float) obj).floatValue());
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Float> collection) {
        a();
        uu0.a(collection);
        if (!(collection instanceof qu0)) {
            return super.addAll(collection);
        }
        qu0 qu0 = (qu0) collection;
        int i = qu0.g;
        if (i == 0) {
            return false;
        }
        int i2 = this.g;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.f;
            if (i3 > fArr.length) {
                this.f = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(qu0.f, 0, this.f, this.g, qu0.g);
            this.g = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final /* synthetic */ xu0 c(int i) {
        if (i >= this.g) {
            return new qu0(Arrays.copyOf(this.f, i), this.g);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qu0)) {
            return super.equals(obj);
        }
        qu0 qu0 = (qu0) obj;
        if (this.g != qu0.g) {
            return false;
        }
        float[] fArr = qu0.f;
        for (int i = 0; i < this.g; i++) {
            if (this.f[i] != fArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final String f(int i) {
        int i2 = this.g;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        a(i);
        return Float.valueOf(this.f[i]);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f[i2]);
        }
        return i;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        a(i);
        float[] fArr = this.f;
        float f2 = fArr[i];
        int i2 = this.g;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, i2 - i);
        }
        this.g--;
        this.modCount++;
        return Float.valueOf(f2);
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.g; i++) {
            if (obj.equals(Float.valueOf(this.f[i]))) {
                float[] fArr = this.f;
                System.arraycopy(fArr, i + 1, fArr, i, this.g - i);
                this.g--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            float[] fArr = this.f;
            System.arraycopy(fArr, i2, fArr, i, this.g - i2);
            this.g -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        a();
        a(i);
        float[] fArr = this.f;
        float f2 = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f2);
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }
}
