package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dv3 {
    @DexIgnore
    public /* final */ String[] a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ List<String> a; // = new ArrayList(20);

        @DexIgnore
        public b b(String str, String str2) {
            this.a.add(str);
            this.a.add(str2.trim());
            return this;
        }

        @DexIgnore
        public final void c(String str, String str2) {
            if (str == null) {
                throw new IllegalArgumentException("name == null");
            } else if (!str.isEmpty()) {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= 31 || charAt >= 127) {
                        throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in header name: %s", new Object[]{Integer.valueOf(charAt), Integer.valueOf(i), str}));
                    }
                }
                if (str2 != null) {
                    int length2 = str2.length();
                    for (int i2 = 0; i2 < length2; i2++) {
                        char charAt2 = str2.charAt(i2);
                        if (charAt2 <= 31 || charAt2 >= 127) {
                            throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in header value: %s", new Object[]{Integer.valueOf(charAt2), Integer.valueOf(i2), str2}));
                        }
                    }
                    return;
                }
                throw new IllegalArgumentException("value == null");
            } else {
                throw new IllegalArgumentException("name is empty");
            }
        }

        @DexIgnore
        public b d(String str, String str2) {
            c(str, str2);
            b(str);
            b(str, str2);
            return this;
        }

        @DexIgnore
        public b a(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                b(str.substring(0, indexOf), str.substring(indexOf + 1));
                return this;
            } else if (str.startsWith(":")) {
                b("", str.substring(1));
                return this;
            } else {
                b("", str);
                return this;
            }
        }

        @DexIgnore
        public b b(String str) {
            int i = 0;
            while (i < this.a.size()) {
                if (str.equalsIgnoreCase(this.a.get(i))) {
                    this.a.remove(i);
                    this.a.remove(i);
                    i -= 2;
                }
                i += 2;
            }
            return this;
        }

        @DexIgnore
        public b a(String str, String str2) {
            c(str, str2);
            b(str, str2);
            return this;
        }

        @DexIgnore
        public dv3 a() {
            return new dv3(this);
        }
    }

    @DexIgnore
    public String a(String str) {
        return a(this.a, str);
    }

    @DexIgnore
    public Date b(String str) {
        String a2 = a(str);
        if (a2 != null) {
            return uw3.a(a2);
        }
        return null;
    }

    @DexIgnore
    public List<String> c(String str) {
        int b2 = b();
        ArrayList arrayList = null;
        for (int i = 0; i < b2; i++) {
            if (str.equalsIgnoreCase(a(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(b(i));
            }
        }
        if (arrayList != null) {
            return Collections.unmodifiableList(arrayList);
        }
        return Collections.emptyList();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int b2 = b();
        for (int i = 0; i < b2; i++) {
            sb.append(a(i));
            sb.append(": ");
            sb.append(b(i));
            sb.append("\n");
        }
        return sb.toString();
    }

    @DexIgnore
    public dv3(b bVar) {
        this.a = (String[]) bVar.a.toArray(new String[bVar.a.size()]);
    }

    @DexIgnore
    public String a(int i) {
        int i2 = i * 2;
        if (i2 < 0) {
            return null;
        }
        String[] strArr = this.a;
        if (i2 >= strArr.length) {
            return null;
        }
        return strArr[i2];
    }

    @DexIgnore
    public int b() {
        return this.a.length / 2;
    }

    @DexIgnore
    public b a() {
        b bVar = new b();
        Collections.addAll(bVar.a, this.a);
        return bVar;
    }

    @DexIgnore
    public String b(int i) {
        int i2 = (i * 2) + 1;
        if (i2 < 0) {
            return null;
        }
        String[] strArr = this.a;
        if (i2 >= strArr.length) {
            return null;
        }
        return strArr[i2];
    }

    @DexIgnore
    public static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }
}
