package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pb implements Closeable, lh4 {
    @DexIgnore
    public /* final */ CoroutineContext e;

    @DexIgnore
    public pb(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        this.e = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.e;
    }

    @DexIgnore
    public void close() {
        vi4.a(A(), (CancellationException) null, 1, (Object) null);
    }
}
