package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ys3 extends ur1 implements DialogInterface.OnKeyListener, View.OnKeyListener {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g; // = 80;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public float i; // = 0.3f;
    @DexIgnore
    public DialogInterface.OnDismissListener j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = ys3.class.getSimpleName();
        wd4.a((Object) simpleName, "BaseBottomSheetDialogFra\u2026nt::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean O0() {
        dismiss();
        return true;
    }

    @DexIgnore
    public void dismiss() {
        FLogger.INSTANCE.getLocal().d(l, "dismiss");
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            cb a2 = fragmentManager.a();
            if (a2 != null) {
                a2.d(this);
                if (a2 != null) {
                    a2.b();
                }
            }
        }
        super.dismiss();
    }

    @DexIgnore
    public boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    public void onAttach(Context context) {
        wd4.b(context, "context");
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof DialogInterface.OnDismissListener)) {
            parentFragment = null;
        }
        DialogInterface.OnDismissListener onDismissListener = (DialogInterface.OnDismissListener) parentFragment;
        if (onDismissListener == null) {
            if (!(context instanceof DialogInterface.OnDismissListener)) {
                context = null;
            }
            onDismissListener = (DialogInterface.OnDismissListener) context;
        }
        this.j = onDismissListener;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AnalyticsHelper.f.c();
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(l, "onDestroy");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        FLogger.INSTANCE.getLocal().d(l, "onDetach");
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        FLogger.INSTANCE.getLocal().d(l, "onDismiss");
        DialogInterface.OnDismissListener onDismissListener = this.j;
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialogInterface);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        wd4.a((Object) simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return O0();
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i2) {
        if (this.f) {
            if (dialog != null) {
                Window window = dialog.getWindow();
                if (window != null) {
                    window.setDimAmount(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
            }
        } else if (dialog != null) {
            Window window2 = dialog.getWindow();
            if (window2 != null) {
                window2.setDimAmount(this.i);
            }
        }
        if (dialog != null) {
            Window window3 = dialog.getWindow();
            if (window3 != null) {
                window3.setGravity(this.g);
            }
        }
        if (dialog != null) {
            Window window4 = dialog.getWindow();
            if (window4 != null) {
                window4.setLayout(-1, this.e);
            }
        }
        if (dialog != null) {
            Window window5 = dialog.getWindow();
            if (window5 != null) {
                window5.setBackgroundDrawableResource(R.color.transparent);
            }
        }
        if (this.h && dialog != null) {
            Window window6 = dialog.getWindow();
            if (window6 != null) {
                window6.setFlags(32, 32);
            }
        }
        if (dialog != null) {
            dialog.setOnKeyListener(this);
        }
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        wd4.b(fragmentManager, "fragmentManager");
        wd4.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(str, "show");
        if (!isActive()) {
            cb a2 = fragmentManager.a();
            wd4.a((Object) a2, "fragmentManager.beginTransaction()");
            Fragment a3 = fragmentManager.a(str);
            if (a3 != null) {
                a2.d(a3);
            }
            a2.a((String) null);
            show(a2, str);
            fragmentManager.b();
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        wd4.a((Object) simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return O0();
    }
}
