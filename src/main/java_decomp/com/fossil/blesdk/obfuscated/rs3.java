package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tg;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rs3 implements tg {
    @DexIgnore
    public /* final */ AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public volatile tg.a b;

    @DexIgnore
    public rs3(String str) {
    }

    @DexIgnore
    public boolean a() {
        return this.a.get() == 0;
    }

    @DexIgnore
    public void b() {
        int decrementAndGet = this.a.decrementAndGet();
        if (decrementAndGet == 0 && this.b != null) {
            this.b.a();
        }
        if (decrementAndGet < 0) {
            throw new IllegalArgumentException("Counter has been corrupted!");
        }
    }

    @DexIgnore
    public void c() {
        this.a.getAndIncrement();
    }
}
