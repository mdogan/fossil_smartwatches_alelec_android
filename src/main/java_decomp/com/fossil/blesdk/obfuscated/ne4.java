package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ne4 extends le4 implements he4<Long> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        new ne4(1, 0);
    }
    */

    @DexIgnore
    public ne4(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean a(long j) {
        return a() <= j && j <= b();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ne4) {
            if (!isEmpty() || !((ne4) obj).isEmpty()) {
                ne4 ne4 = (ne4) obj;
                if (!(a() == ne4.a() && b() == ne4.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (a() ^ (a() >>> 32))) + (b() ^ (b() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
