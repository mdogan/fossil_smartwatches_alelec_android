package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pd1 extends if0<g41, rc1> {
    @DexIgnore
    public /* final */ /* synthetic */ pc1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pd1(pc1 pc1, af0.a aVar) {
        super(aVar);
        this.b = pc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar, yn1 yn1) throws RemoteException {
        try {
            ((g41) bVar).b(a(), this.b.a((yn1<Boolean>) yn1));
        } catch (RuntimeException e) {
            yn1.b((Exception) e);
        }
    }
}
