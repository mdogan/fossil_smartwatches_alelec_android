package com.fossil.blesdk.obfuscated;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gr<Data> implements tr<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;
    @DexIgnore
    public /* final */ a<Data> b;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        to<Data> a(AssetManager assetManager, String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ur<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public tr<Uri, ParcelFileDescriptor> a(xr xrVar) {
            return new gr(this.a, this);
        }

        @DexIgnore
        public to<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new xo(assetManager, str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements ur<Uri, InputStream>, a<InputStream> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public tr<Uri, InputStream> a(xr xrVar) {
            return new gr(this.a, this);
        }

        @DexIgnore
        public to<InputStream> a(AssetManager assetManager, String str) {
            return new cp(assetManager, str);
        }
    }

    @DexIgnore
    public gr(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    @DexIgnore
    public tr.a<Data> a(Uri uri, int i, int i2, mo moVar) {
        return new tr.a<>(new kw(uri), this.b.a(this.a, uri.toString().substring(c)));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
