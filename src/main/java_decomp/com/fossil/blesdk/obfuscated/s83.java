package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s83 implements Factory<ActiveTimeOverviewMonthPresenter> {
    @DexIgnore
    public static ActiveTimeOverviewMonthPresenter a(q83 q83, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActiveTimeOverviewMonthPresenter(q83, userRepository, summariesRepository);
    }
}
