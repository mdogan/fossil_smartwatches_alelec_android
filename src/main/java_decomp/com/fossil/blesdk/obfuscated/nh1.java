package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh1 implements ServiceConnection {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ mh1 b;

    @DexIgnore
    public nh1(mh1 mh1, String str) {
        this.b = mh1;
        this.a = str;
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.a.d().v().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            h81 a2 = d91.a(iBinder);
            if (a2 == null) {
                this.b.a.d().v().a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.a.d().y().a("Install Referrer Service connected");
            this.b.a.a().a((Runnable) new oh1(this, a2, this));
        } catch (Exception e) {
            this.b.a.d().v().a("Exception occurred while calling Install Referrer API", e);
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.a.d().y().a("Install Referrer Service disconnected");
    }
}
