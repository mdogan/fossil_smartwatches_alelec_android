package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ds2 extends ys3 implements dm3 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public cm3 m;
    @DexIgnore
    public ur3<p92> n;
    @DexIgnore
    public ku3 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ds2.q;
        }

        @DexIgnore
        public final ds2 b() {
            return new ds2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 a;

        @DexIgnore
        public b(ds2 ds2) {
            this.a = ds2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ds2.r.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            ds2.a(this.a).b(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 a;

        @DexIgnore
        public c(ds2 ds2) {
            this.a = ds2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ds2.r.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            ds2.a(this.a).a(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 a;

        @DexIgnore
        public d(ds2 ds2) {
            this.a = ds2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ds2.r.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            ds2.a(this.a).c(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 e;

        @DexIgnore
        public e(ds2 ds2) {
            this.e = ds2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ds2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 e;

        @DexIgnore
        public f(ds2 ds2) {
            this.e = ds2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.dismiss();
        }
    }

    /*
    static {
        String simpleName = ds2.class.getSimpleName();
        wd4.a((Object) simpleName, "BirthdayFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ cm3 a(ds2 ds2) {
        cm3 cm3 = ds2.m;
        if (cm3 != null) {
            return cm3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final p92 P0() {
        ur3<p92> ur3 = this.n;
        if (ur3 != null) {
            return ur3.a();
        }
        return null;
    }

    @DexIgnore
    public void b(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "Update day range: from " + i + " to " + i2);
        p92 P0 = P0();
        if (P0 != null) {
            NumberPicker numberPicker = P0.s;
            if (numberPicker != null) {
                wd4.a((Object) numberPicker, "npDay");
                numberPicker.setMinValue(i);
                numberPicker.setMaxValue(i2);
            }
        }
    }

    @DexIgnore
    public void c(Date date) {
        wd4.b(date, "birthday");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        ku3 ku3 = this.o;
        if (ku3 != null) {
            ku3.c().a(date);
            dismiss();
            return;
        }
        wd4.d("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.BottomDialog);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(q, "onCreateDialog");
        View inflate = View.inflate(getContext(), R.layout.fragment_birthday, (ViewGroup) null);
        wd4.a((Object) inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        Context context = getContext();
        if (context != null) {
            tr1 tr1 = new tr1(context, getTheme());
            tr1.setContentView(inflate);
            tr1.setCanceledOnTouchOutside(true);
            return tr1;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ViewDataBinding a2 = ra.a(layoutInflater, (int) R.layout.fragment_birthday, viewGroup, false);
        wd4.a((Object) a2, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        p92 p92 = (p92) a2;
        this.n = new ur3<>(this, p92);
        return p92.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        cm3 cm3 = this.m;
        if (cm3 != null) {
            cm3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        cm3 cm3 = this.m;
        if (cm3 != null) {
            cm3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ku3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.o = (ku3) a2;
            p92 P0 = P0();
            if (P0 != null) {
                NumberPicker numberPicker = P0.t;
                if (numberPicker != null) {
                    DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
                    wd4.a((Object) instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
                    numberPicker.setDisplayedValues(instance.getShortMonths());
                    numberPicker.setOnValueChangedListener(new b(this));
                }
            }
            p92 P02 = P0();
            if (P02 != null) {
                NumberPicker numberPicker2 = P02.s;
                if (numberPicker2 != null) {
                    numberPicker2.setOnValueChangedListener(new c(this));
                }
            }
            p92 P03 = P0();
            if (P03 != null) {
                NumberPicker numberPicker3 = P03.u;
                if (numberPicker3 != null) {
                    numberPicker3.setOnValueChangedListener(new d(this));
                }
            }
            p92 P04 = P0();
            if (P04 != null) {
                FlexibleButton flexibleButton = P04.r;
                if (flexibleButton != null) {
                    flexibleButton.setOnClickListener(new e(this));
                }
            }
            p92 P05 = P0();
            if (P05 != null) {
                FlexibleButton flexibleButton2 = P05.q;
                if (flexibleButton2 != null) {
                    flexibleButton2.setOnClickListener(new f(this));
                    return;
                }
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(cm3 cm3) {
        wd4.b(cm3, "presenter");
        this.m = cm3;
    }

    @DexIgnore
    public void a(Pair<Integer, Integer> pair, Pair<Integer, Integer> pair2, Pair<Integer, Integer> pair3) {
        wd4.b(pair, "dayRange");
        wd4.b(pair2, "monthRange");
        wd4.b(pair3, "yearRange");
        p92 P0 = P0();
        if (P0 != null) {
            NumberPicker numberPicker = P0.u;
            if (numberPicker != null) {
                wd4.a((Object) numberPicker, "npYear");
                numberPicker.setMinValue(pair3.getFirst().intValue());
                numberPicker.setMaxValue(pair3.getSecond().intValue());
            }
        }
        p92 P02 = P0();
        if (P02 != null) {
            NumberPicker numberPicker2 = P02.t;
            if (numberPicker2 != null) {
                wd4.a((Object) numberPicker2, "npMonth");
                numberPicker2.setMinValue(pair2.getFirst().intValue());
                numberPicker2.setMaxValue(pair2.getSecond().intValue());
            }
        }
        p92 P03 = P0();
        if (P03 != null) {
            NumberPicker numberPicker3 = P03.s;
            if (numberPicker3 != null) {
                wd4.a((Object) numberPicker3, "npDay");
                numberPicker3.setMinValue(pair.getFirst().intValue());
                numberPicker3.setMaxValue(pair.getSecond().intValue());
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            p92 P04 = P0();
            if (P04 != null) {
                NumberPicker numberPicker4 = P04.s;
                if (numberPicker4 != null) {
                    numberPicker4.setValue(i);
                }
            }
            p92 P05 = P0();
            if (P05 != null) {
                NumberPicker numberPicker5 = P05.t;
                if (numberPicker5 != null) {
                    numberPicker5.setValue(i2);
                }
            }
            p92 P06 = P0();
            if (P06 != null) {
                NumberPicker numberPicker6 = P06.u;
                if (numberPicker6 != null) {
                    numberPicker6.setValue(i3);
                }
            }
            cm3 cm3 = this.m;
            if (cm3 != null) {
                cm3.a(i);
                cm3 cm32 = this.m;
                if (cm32 != null) {
                    cm32.b(i2);
                    cm3 cm33 = this.m;
                    if (cm33 != null) {
                        cm33.c(i3);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }
}
