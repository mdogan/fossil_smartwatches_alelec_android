package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.obfuscated.jy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ay3 extends xx3 {
    @DexIgnore
    public ay3(Context context) {
        super(context);
    }

    @DexIgnore
    public boolean a(hy3 hy3) {
        return "file".equals(hy3.d.getScheme());
    }

    @DexIgnore
    public jy3.a a(hy3 hy3, int i) throws IOException {
        return new jy3.a((Bitmap) null, c(hy3), Picasso.LoadedFrom.DISK, a(hy3.d));
    }

    @DexIgnore
    public static int a(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE;
        }
        if (attributeInt == 6) {
            return 90;
        }
        if (attributeInt != 8) {
            return 0;
        }
        return BackgroundImageConfig.LEFT_BACKGROUND_ANGLE;
    }
}
