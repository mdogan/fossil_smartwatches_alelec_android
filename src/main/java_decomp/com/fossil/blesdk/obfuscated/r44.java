package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.p44;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r44 {
    @DexIgnore
    public static volatile r44 l;
    @DexIgnore
    public static /* final */ z44 m; // = new q44();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Map<Class<? extends w44>, w44> b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ u44<r44> d;
    @DexIgnore
    public /* final */ u44<?> e;
    @DexIgnore
    public /* final */ IdManager f;
    @DexIgnore
    public p44 g;
    @DexIgnore
    public WeakReference<Activity> h;
    @DexIgnore
    public AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ z44 j;
    @DexIgnore
    public /* final */ boolean k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends p44.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Activity activity, Bundle bundle) {
            r44.this.a(activity);
        }

        @DexIgnore
        public void c(Activity activity) {
            r44.this.a(activity);
        }

        @DexIgnore
        public void d(Activity activity) {
            r44.this.a(activity);
        }
    }

    @DexIgnore
    public r44(Context context, Map<Class<? extends w44>, w44> map, i64 i64, Handler handler, z44 z44, boolean z, u44 u44, IdManager idManager, Activity activity) {
        this.a = context;
        this.b = map;
        this.c = i64;
        this.j = z44;
        this.k = z;
        this.d = u44;
        this.e = a(map.size());
        this.f = idManager;
        a(activity);
    }

    @DexIgnore
    public static r44 d(r44 r44) {
        if (l == null) {
            synchronized (r44.class) {
                if (l == null) {
                    c(r44);
                }
            }
        }
        return l;
    }

    @DexIgnore
    public static z44 g() {
        if (l == null) {
            return m;
        }
        return l.j;
    }

    @DexIgnore
    public static boolean h() {
        if (l == null) {
            return false;
        }
        return l.k;
    }

    @DexIgnore
    public static r44 i() {
        if (l != null) {
            return l;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    @DexIgnore
    public String c() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String e() {
        return "1.4.8.32";
    }

    @DexIgnore
    public final void f() {
        this.g = new p44(this.a);
        this.g.a(new a());
        b(this.a);
    }

    @DexIgnore
    public static void c(r44 r44) {
        l = r44;
        r44.f();
    }

    @DexIgnore
    public void b(Context context) {
        StringBuilder sb;
        Future<Map<String, y44>> a2 = a(context);
        Collection<w44> d2 = d();
        a54 a54 = new a54(a2, d2);
        ArrayList<w44> arrayList = new ArrayList<>(d2);
        Collections.sort(arrayList);
        a54.a(context, this, u44.a, this.f);
        for (w44 a3 : arrayList) {
            a3.a(context, this, this.e, this.f);
        }
        a54.t();
        if (g().a("Fabric", 3)) {
            sb = new StringBuilder("Initializing ");
            sb.append(c());
            sb.append(" [Version: ");
            sb.append(e());
            sb.append("], with the following kits:\n");
        } else {
            sb = null;
        }
        for (w44 w44 : arrayList) {
            w44.f.a((j64) a54.f);
            a(this.b, w44);
            w44.t();
            if (sb != null) {
                sb.append(w44.p());
                sb.append(" [Version: ");
                sb.append(w44.r());
                sb.append("]\n");
            }
        }
        if (sb != null) {
            g().d("Fabric", sb.toString());
        }
    }

    @DexIgnore
    public static r44 a(Context context, w44... w44Arr) {
        if (l == null) {
            synchronized (r44.class) {
                if (l == null) {
                    c cVar = new c(context);
                    cVar.a(w44Arr);
                    c(cVar.a());
                }
            }
        }
        return l;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements u44 {
        @DexIgnore
        public /* final */ CountDownLatch b; // = new CountDownLatch(this.c);
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public b(int i) {
            this.c = i;
        }

        @DexIgnore
        public void a(Object obj) {
            this.b.countDown();
            if (this.b.getCount() == 0) {
                r44.this.i.set(true);
                r44.this.d.a(r44.this);
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            r44.this.d.a(exc);
        }
    }

    @DexIgnore
    public static Activity d(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    @DexIgnore
    public r44 a(Activity activity) {
        this.h = new WeakReference<>(activity);
        return this;
    }

    @DexIgnore
    public Collection<w44> d() {
        return this.b.values();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public w44[] b;
        @DexIgnore
        public i64 c;
        @DexIgnore
        public Handler d;
        @DexIgnore
        public z44 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public u44<r44> i;

        @DexIgnore
        public c(Context context) {
            if (context != null) {
                this.a = context;
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        @DexIgnore
        public c a(w44... w44Arr) {
            if (this.b == null) {
                if (!p54.a(this.a).a()) {
                    ArrayList arrayList = new ArrayList();
                    boolean z = false;
                    for (w44 w44 : w44Arr) {
                        String p = w44.p();
                        char c2 = 65535;
                        int hashCode = p.hashCode();
                        if (hashCode != 607220212) {
                            if (hashCode == 1830452504 && p.equals("com.crashlytics.sdk.android:crashlytics")) {
                                c2 = 0;
                            }
                        } else if (p.equals("com.crashlytics.sdk.android:answers")) {
                            c2 = 1;
                        }
                        if (c2 == 0 || c2 == 1) {
                            arrayList.add(w44);
                        } else if (!z) {
                            r44.g().w("Fabric", "Fabric will not initialize any kits when Firebase automatic data collection is disabled; to use Third-party kits with automatic data collection disabled, initialize these kits via non-Fabric means.");
                            z = true;
                        }
                    }
                    w44Arr = (w44[]) arrayList.toArray(new w44[0]);
                }
                this.b = w44Arr;
                return this;
            }
            throw new IllegalStateException("Kits already set.");
        }

        @DexIgnore
        public c a(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public r44 a() {
            Map map;
            if (this.c == null) {
                this.c = i64.a();
            }
            if (this.d == null) {
                this.d = new Handler(Looper.getMainLooper());
            }
            if (this.e == null) {
                if (this.f) {
                    this.e = new q44(3);
                } else {
                    this.e = new q44();
                }
            }
            if (this.h == null) {
                this.h = this.a.getPackageName();
            }
            if (this.i == null) {
                this.i = u44.a;
            }
            w44[] w44Arr = this.b;
            if (w44Arr == null) {
                map = new HashMap();
            } else {
                map = r44.b((Collection<? extends w44>) Arrays.asList(w44Arr));
            }
            Map map2 = map;
            Context applicationContext = this.a.getApplicationContext();
            return new r44(applicationContext, map2, this.c, this.d, this.e, this.f, this.i, new IdManager(applicationContext, this.h, this.g, map2.values()), r44.d(this.a));
        }
    }

    @DexIgnore
    public Activity a() {
        WeakReference<Activity> weakReference = this.h;
        if (weakReference != null) {
            return (Activity) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public void a(Map<Class<? extends w44>, w44> map, w44 w44) {
        c64 c64 = w44.j;
        if (c64 != null) {
            for (Class cls : c64.value()) {
                if (cls.isInterface()) {
                    for (w44 next : map.values()) {
                        if (cls.isAssignableFrom(next.getClass())) {
                            w44.f.a((j64) next.f);
                        }
                    }
                } else if (map.get(cls) != null) {
                    w44.f.a((j64) map.get(cls).f);
                } else {
                    throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                }
            }
        }
    }

    @DexIgnore
    public static <T extends w44> T a(Class<T> cls) {
        return (w44) i().b.get(cls);
    }

    @DexIgnore
    public static void a(Map<Class<? extends w44>, w44> map, Collection<? extends w44> collection) {
        for (w44 w44 : collection) {
            map.put(w44.getClass(), w44);
            if (w44 instanceof x44) {
                a(map, ((x44) w44).i());
            }
        }
    }

    @DexIgnore
    public ExecutorService b() {
        return this.c;
    }

    @DexIgnore
    public static Map<Class<? extends w44>, w44> b(Collection<? extends w44> collection) {
        HashMap hashMap = new HashMap(collection.size());
        a((Map<Class<? extends w44>, w44>) hashMap, collection);
        return hashMap;
    }

    @DexIgnore
    public u44<?> a(int i2) {
        return new b(i2);
    }

    @DexIgnore
    public Future<Map<String, y44>> a(Context context) {
        return b().submit(new t44(context.getPackageCodePath()));
    }
}
