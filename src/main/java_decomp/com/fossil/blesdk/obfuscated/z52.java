package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z52 extends oa {
    @DexIgnore
    public static /* final */ SparseIntArray a; // = new SparseIntArray(137);

    /*
    static {
        a.put(R.layout.activity_search_ringphone, 1);
        a.put(R.layout.activity_webview, 2);
        a.put(R.layout.fragment_about, 3);
        a.put(R.layout.fragment_active_time_detail, 4);
        a.put(R.layout.fragment_active_time_overview, 5);
        a.put(R.layout.fragment_active_time_overview_day, 6);
        a.put(R.layout.fragment_active_time_overview_month, 7);
        a.put(R.layout.fragment_active_time_overview_week, 8);
        a.put(R.layout.fragment_activity_detail, 9);
        a.put(R.layout.fragment_activity_overview, 10);
        a.put(R.layout.fragment_activity_overview_day, 11);
        a.put(R.layout.fragment_activity_overview_month, 12);
        a.put(R.layout.fragment_activity_overview_week, 13);
        a.put(R.layout.fragment_alarm, 14);
        a.put(R.layout.fragment_allow_notification_service, 15);
        a.put(R.layout.fragment_birthday, 16);
        a.put(R.layout.fragment_calibration, 17);
        a.put(R.layout.fragment_calories_detail, 18);
        a.put(R.layout.fragment_calories_overview, 19);
        a.put(R.layout.fragment_calories_overview_day, 20);
        a.put(R.layout.fragment_calories_overview_month, 21);
        a.put(R.layout.fragment_calories_overview_week, 22);
        a.put(R.layout.fragment_commute_time_settings, 23);
        a.put(R.layout.fragment_commute_time_settings_default_address, 24);
        a.put(R.layout.fragment_commute_time_settings_detail, 25);
        a.put(R.layout.fragment_commute_time_watch_app_settings, 26);
        a.put(R.layout.fragment_complication_search, 27);
        a.put(R.layout.fragment_complications, 28);
        a.put(R.layout.fragment_connected_apps, 29);
        a.put(R.layout.fragment_customize_theme, 30);
        a.put(R.layout.fragment_customize_tutorial, 31);
        a.put(R.layout.fragment_dashboard_active_time, 32);
        a.put(R.layout.fragment_dashboard_activity, 33);
        a.put(R.layout.fragment_dashboard_calories, 34);
        a.put(R.layout.fragment_dashboard_goal_tracking, 35);
        a.put(R.layout.fragment_dashboard_heartrate, 36);
        a.put(R.layout.fragment_dashboard_sleep, 37);
        a.put(R.layout.fragment_delete_account, 38);
        a.put(R.layout.fragment_diana_customize_edit, 39);
        a.put(R.layout.fragment_do_not_disturb_scheduled_time, 40);
        a.put(R.layout.fragment_email_verification, 41);
        a.put(R.layout.fragment_explore_watch, 42);
        a.put(R.layout.fragment_find_device, 43);
        a.put(R.layout.fragment_forgot_password, 44);
        a.put(R.layout.fragment_getting_started, 45);
        a.put(R.layout.fragment_goal_tracking_detail, 46);
        a.put(R.layout.fragment_goal_tracking_overview, 47);
        a.put(R.layout.fragment_goal_tracking_overview_day, 48);
        a.put(R.layout.fragment_goal_tracking_overview_month, 49);
        a.put(R.layout.fragment_goal_tracking_overview_week, 50);
        a.put(R.layout.fragment_heartrate_detail, 51);
        a.put(R.layout.fragment_heartrate_overview, 52);
        a.put(R.layout.fragment_heartrate_overview_day, 53);
        a.put(R.layout.fragment_heartrate_overview_month, 54);
        a.put(R.layout.fragment_heartrate_overview_week, 55);
        a.put(R.layout.fragment_help, 56);
        a.put(R.layout.fragment_home, 57);
        a.put(R.layout.fragment_home_alerts, 58);
        a.put(R.layout.fragment_home_alerts_hybrid, 59);
        a.put(R.layout.fragment_home_dashboard, 60);
        a.put(R.layout.fragment_home_diana_customize, 61);
        a.put(R.layout.fragment_home_hybrid_customize, 62);
        a.put(R.layout.fragment_home_profile, 63);
        a.put(R.layout.fragment_home_update_firmware, 64);
        a.put(R.layout.fragment_hybrid_customize_edit, 65);
        a.put(R.layout.fragment_micro_app, 66);
        a.put(R.layout.fragment_micro_app_search, 67);
        a.put(R.layout.fragment_notification_allow_calls_and_messages, 68);
        a.put(R.layout.fragment_notification_apps, 69);
        a.put(R.layout.fragment_notification_calls_and_messages, 70);
        a.put(R.layout.fragment_notification_contacts, 71);
        a.put(R.layout.fragment_notification_contacts_and_apps_assigned, 72);
        a.put(R.layout.fragment_notification_dial_landing, 73);
        a.put(R.layout.fragment_notification_hybrid_app, 74);
        a.put(R.layout.fragment_notification_hybrid_contact, 75);
        a.put(R.layout.fragment_notification_hybrid_everyone, 76);
        a.put(R.layout.fragment_notification_settings_type, 77);
        a.put(R.layout.fragment_notification_watch_reminders, 78);
        a.put(R.layout.fragment_onboarding_height_weight, 79);
        a.put(R.layout.fragment_opt_in, 80);
        a.put(R.layout.fragment_pairing_device_found, 81);
        a.put(R.layout.fragment_pairing_instructions, 82);
        a.put(R.layout.fragment_pairing_look_for_device, 83);
        a.put(R.layout.fragment_permission, 84);
        a.put(R.layout.fragment_preferred_unit, 85);
        a.put(R.layout.fragment_profile_change_pass, 86);
        a.put(R.layout.fragment_profile_edit, 87);
        a.put(R.layout.fragment_profile_goals_edit, 88);
        a.put(R.layout.fragment_profile_setup, 89);
        a.put(R.layout.fragment_remind_time, 90);
        a.put(R.layout.fragment_replace_battery, 91);
        a.put(R.layout.fragment_search_second_timezone, 92);
        a.put(R.layout.fragment_signin, 93);
        a.put(R.layout.fragment_signup, 94);
        a.put(R.layout.fragment_sleep_detail, 95);
        a.put(R.layout.fragment_sleep_overview, 96);
        a.put(R.layout.fragment_sleep_overview_day, 97);
        a.put(R.layout.fragment_sleep_overview_month, 98);
        a.put(R.layout.fragment_sleep_overview_week, 99);
        a.put(R.layout.fragment_troubleshooting, 100);
        a.put(R.layout.fragment_update_failed_troubleshooting, 101);
        a.put(R.layout.fragment_update_firmware, 102);
        a.put(R.layout.fragment_watch_app_search, 103);
        a.put(R.layout.fragment_watch_apps, 104);
        a.put(R.layout.fragment_watch_setting, 105);
        a.put(R.layout.fragment_weather_setting, 106);
        a.put(R.layout.item_active_time_day, 107);
        a.put(R.layout.item_active_time_week, 108);
        a.put(R.layout.item_active_time_workout_day, 109);
        a.put(R.layout.item_activity_day, 110);
        a.put(R.layout.item_activity_week, 111);
        a.put(R.layout.item_activity_workout_day, 112);
        a.put(R.layout.item_address_commute_time, 113);
        a.put(R.layout.item_alarm, 114);
        a.put(R.layout.item_app_hybrid_notification, 115);
        a.put(R.layout.item_app_notification, 116);
        a.put(R.layout.item_calories_day, 117);
        a.put(R.layout.item_calories_week, 118);
        a.put(R.layout.item_calories_workout_day, 119);
        a.put(R.layout.item_contact, 120);
        a.put(R.layout.item_contact_hybrid, 121);
        a.put(R.layout.item_default_place_commute_time, 122);
        a.put(R.layout.item_favorite_contact_notification, 123);
        a.put(R.layout.item_goal_tracking_day, 124);
        a.put(R.layout.item_goal_tracking_week, 125);
        a.put(R.layout.item_heart_rate_day, 126);
        a.put(R.layout.item_heart_rate_week, 127);
        a.put(R.layout.item_heartrate_workout_day, 128);
        a.put(R.layout.item_notification_hybrid, 129);
        a.put(R.layout.item_permission, 130);
        a.put(R.layout.item_recorded_goal_tracking, 131);
        a.put(R.layout.item_single_permission, 132);
        a.put(R.layout.item_sleep_day, 133);
        a.put(R.layout.item_sleep_week, 134);
        a.put(R.layout.item_workout, 135);
        a.put(R.layout.view_no_device, 136);
        a.put(R.layout.view_tab_custom, 137);
    }
    */

    @DexIgnore
    public final ViewDataBinding a(qa qaVar, View view, int i, Object obj) {
        switch (i) {
            case 1:
                if ("layout/activity_search_ringphone_0".equals(obj)) {
                    return new m82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for activity_search_ringphone is invalid. Received: " + obj);
            case 2:
                if ("layout/activity_webview_0".equals(obj)) {
                    return new o82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for activity_webview is invalid. Received: " + obj);
            case 3:
                if ("layout/fragment_about_0".equals(obj)) {
                    return new q82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_about is invalid. Received: " + obj);
            case 4:
                if ("layout/fragment_active_time_detail_0".equals(obj)) {
                    return new s82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_detail is invalid. Received: " + obj);
            case 5:
                if ("layout/fragment_active_time_overview_0".equals(obj)) {
                    return new u82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview is invalid. Received: " + obj);
            case 6:
                if ("layout/fragment_active_time_overview_day_0".equals(obj)) {
                    return new w82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_day is invalid. Received: " + obj);
            case 7:
                if ("layout/fragment_active_time_overview_month_0".equals(obj)) {
                    return new y82(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_month is invalid. Received: " + obj);
            case 8:
                if ("layout/fragment_active_time_overview_week_0".equals(obj)) {
                    return new a92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_week is invalid. Received: " + obj);
            case 9:
                if ("layout/fragment_activity_detail_0".equals(obj)) {
                    return new c92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_detail is invalid. Received: " + obj);
            case 10:
                if ("layout/fragment_activity_overview_0".equals(obj)) {
                    return new e92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview is invalid. Received: " + obj);
            case 11:
                if ("layout/fragment_activity_overview_day_0".equals(obj)) {
                    return new g92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_day is invalid. Received: " + obj);
            case 12:
                if ("layout/fragment_activity_overview_month_0".equals(obj)) {
                    return new i92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_month is invalid. Received: " + obj);
            case 13:
                if ("layout/fragment_activity_overview_week_0".equals(obj)) {
                    return new k92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_week is invalid. Received: " + obj);
            case 14:
                if ("layout/fragment_alarm_0".equals(obj)) {
                    return new m92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_alarm is invalid. Received: " + obj);
            case 15:
                if ("layout/fragment_allow_notification_service_0".equals(obj)) {
                    return new o92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_allow_notification_service is invalid. Received: " + obj);
            case 16:
                if ("layout/fragment_birthday_0".equals(obj)) {
                    return new q92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_birthday is invalid. Received: " + obj);
            case 17:
                if ("layout/fragment_calibration_0".equals(obj)) {
                    return new s92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calibration is invalid. Received: " + obj);
            case 18:
                if ("layout/fragment_calories_detail_0".equals(obj)) {
                    return new u92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_detail is invalid. Received: " + obj);
            case 19:
                if ("layout/fragment_calories_overview_0".equals(obj)) {
                    return new w92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview is invalid. Received: " + obj);
            case 20:
                if ("layout/fragment_calories_overview_day_0".equals(obj)) {
                    return new y92(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_day is invalid. Received: " + obj);
            case 21:
                if ("layout/fragment_calories_overview_month_0".equals(obj)) {
                    return new aa2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_month is invalid. Received: " + obj);
            case 22:
                if ("layout/fragment_calories_overview_week_0".equals(obj)) {
                    return new ca2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_week is invalid. Received: " + obj);
            case 23:
                if ("layout/fragment_commute_time_settings_0".equals(obj)) {
                    return new ea2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings is invalid. Received: " + obj);
            case 24:
                if ("layout/fragment_commute_time_settings_default_address_0".equals(obj)) {
                    return new ga2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_default_address is invalid. Received: " + obj);
            case 25:
                if ("layout/fragment_commute_time_settings_detail_0".equals(obj)) {
                    return new ia2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_detail is invalid. Received: " + obj);
            case 26:
                if ("layout/fragment_commute_time_watch_app_settings_0".equals(obj)) {
                    return new ka2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_watch_app_settings is invalid. Received: " + obj);
            case 27:
                if ("layout/fragment_complication_search_0".equals(obj)) {
                    return new ma2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complication_search is invalid. Received: " + obj);
            case 28:
                if ("layout/fragment_complications_0".equals(obj)) {
                    return new oa2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complications is invalid. Received: " + obj);
            case 29:
                if ("layout/fragment_connected_apps_0".equals(obj)) {
                    return new qa2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_connected_apps is invalid. Received: " + obj);
            case 30:
                if ("layout/fragment_customize_theme_0".equals(obj)) {
                    return new sa2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_theme is invalid. Received: " + obj);
            case 31:
                if ("layout/fragment_customize_tutorial_0".equals(obj)) {
                    return new ua2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_tutorial is invalid. Received: " + obj);
            case 32:
                if ("layout/fragment_dashboard_active_time_0".equals(obj)) {
                    return new wa2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_active_time is invalid. Received: " + obj);
            case 33:
                if ("layout/fragment_dashboard_activity_0".equals(obj)) {
                    return new ya2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_activity is invalid. Received: " + obj);
            case 34:
                if ("layout/fragment_dashboard_calories_0".equals(obj)) {
                    return new ab2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_calories is invalid. Received: " + obj);
            case 35:
                if ("layout/fragment_dashboard_goal_tracking_0".equals(obj)) {
                    return new cb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_goal_tracking is invalid. Received: " + obj);
            case 36:
                if ("layout/fragment_dashboard_heartrate_0".equals(obj)) {
                    return new eb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_heartrate is invalid. Received: " + obj);
            case 37:
                if ("layout/fragment_dashboard_sleep_0".equals(obj)) {
                    return new gb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_sleep is invalid. Received: " + obj);
            case 38:
                if ("layout/fragment_delete_account_0".equals(obj)) {
                    return new ib2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_delete_account is invalid. Received: " + obj);
            case 39:
                if ("layout/fragment_diana_customize_edit_0".equals(obj)) {
                    return new kb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_diana_customize_edit is invalid. Received: " + obj);
            case 40:
                if ("layout/fragment_do_not_disturb_scheduled_time_0".equals(obj)) {
                    return new mb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_do_not_disturb_scheduled_time is invalid. Received: " + obj);
            case 41:
                if ("layout/fragment_email_verification_0".equals(obj)) {
                    return new ob2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_email_verification is invalid. Received: " + obj);
            case 42:
                if ("layout/fragment_explore_watch_0".equals(obj)) {
                    return new qb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_explore_watch is invalid. Received: " + obj);
            case 43:
                if ("layout/fragment_find_device_0".equals(obj)) {
                    return new sb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_find_device is invalid. Received: " + obj);
            case 44:
                if ("layout/fragment_forgot_password_0".equals(obj)) {
                    return new ub2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_forgot_password is invalid. Received: " + obj);
            case 45:
                if ("layout/fragment_getting_started_0".equals(obj)) {
                    return new wb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_getting_started is invalid. Received: " + obj);
            case 46:
                if ("layout/fragment_goal_tracking_detail_0".equals(obj)) {
                    return new yb2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_detail is invalid. Received: " + obj);
            case 47:
                if ("layout/fragment_goal_tracking_overview_0".equals(obj)) {
                    return new ac2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview is invalid. Received: " + obj);
            case 48:
                if ("layout/fragment_goal_tracking_overview_day_0".equals(obj)) {
                    return new cc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_day is invalid. Received: " + obj);
            case 49:
                if ("layout/fragment_goal_tracking_overview_month_0".equals(obj)) {
                    return new ec2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_month is invalid. Received: " + obj);
            case 50:
                if ("layout/fragment_goal_tracking_overview_week_0".equals(obj)) {
                    return new gc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_week is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding b(qa qaVar, View view, int i, Object obj) {
        switch (i) {
            case 51:
                if ("layout/fragment_heartrate_detail_0".equals(obj)) {
                    return new ic2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_detail is invalid. Received: " + obj);
            case 52:
                if ("layout/fragment_heartrate_overview_0".equals(obj)) {
                    return new kc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview is invalid. Received: " + obj);
            case 53:
                if ("layout/fragment_heartrate_overview_day_0".equals(obj)) {
                    return new mc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_day is invalid. Received: " + obj);
            case 54:
                if ("layout/fragment_heartrate_overview_month_0".equals(obj)) {
                    return new oc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_month is invalid. Received: " + obj);
            case 55:
                if ("layout/fragment_heartrate_overview_week_0".equals(obj)) {
                    return new qc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_week is invalid. Received: " + obj);
            case 56:
                if ("layout/fragment_help_0".equals(obj)) {
                    return new sc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_help is invalid. Received: " + obj);
            case 57:
                if ("layout/fragment_home_0".equals(obj)) {
                    return new yc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + obj);
            case 58:
                if ("layout/fragment_home_alerts_0".equals(obj)) {
                    return new uc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts is invalid. Received: " + obj);
            case 59:
                if ("layout/fragment_home_alerts_hybrid_0".equals(obj)) {
                    return new wc2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts_hybrid is invalid. Received: " + obj);
            case 60:
                if ("layout/fragment_home_dashboard_0".equals(obj)) {
                    return new ad2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_dashboard is invalid. Received: " + obj);
            case 61:
                if ("layout/fragment_home_diana_customize_0".equals(obj)) {
                    return new cd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_diana_customize is invalid. Received: " + obj);
            case 62:
                if ("layout/fragment_home_hybrid_customize_0".equals(obj)) {
                    return new ed2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_hybrid_customize is invalid. Received: " + obj);
            case 63:
                if ("layout/fragment_home_profile_0".equals(obj)) {
                    return new gd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_profile is invalid. Received: " + obj);
            case 64:
                if ("layout/fragment_home_update_firmware_0".equals(obj)) {
                    return new id2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_update_firmware is invalid. Received: " + obj);
            case 65:
                if ("layout/fragment_hybrid_customize_edit_0".equals(obj)) {
                    return new kd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_hybrid_customize_edit is invalid. Received: " + obj);
            case 66:
                if ("layout/fragment_micro_app_0".equals(obj)) {
                    return new md2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app is invalid. Received: " + obj);
            case 67:
                if ("layout/fragment_micro_app_search_0".equals(obj)) {
                    return new od2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app_search is invalid. Received: " + obj);
            case 68:
                if ("layout/fragment_notification_allow_calls_and_messages_0".equals(obj)) {
                    return new qd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_allow_calls_and_messages is invalid. Received: " + obj);
            case 69:
                if ("layout/fragment_notification_apps_0".equals(obj)) {
                    return new sd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_apps is invalid. Received: " + obj);
            case 70:
                if ("layout/fragment_notification_calls_and_messages_0".equals(obj)) {
                    return new ud2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_calls_and_messages is invalid. Received: " + obj);
            case 71:
                if ("layout/fragment_notification_contacts_0".equals(obj)) {
                    return new yd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts is invalid. Received: " + obj);
            case 72:
                if ("layout/fragment_notification_contacts_and_apps_assigned_0".equals(obj)) {
                    return new wd2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts_and_apps_assigned is invalid. Received: " + obj);
            case 73:
                if ("layout/fragment_notification_dial_landing_0".equals(obj)) {
                    return new ae2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_dial_landing is invalid. Received: " + obj);
            case 74:
                if ("layout/fragment_notification_hybrid_app_0".equals(obj)) {
                    return new ce2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_app is invalid. Received: " + obj);
            case 75:
                if ("layout/fragment_notification_hybrid_contact_0".equals(obj)) {
                    return new ee2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_contact is invalid. Received: " + obj);
            case 76:
                if ("layout/fragment_notification_hybrid_everyone_0".equals(obj)) {
                    return new ge2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_everyone is invalid. Received: " + obj);
            case 77:
                if ("layout/fragment_notification_settings_type_0".equals(obj)) {
                    return new ie2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_settings_type is invalid. Received: " + obj);
            case 78:
                if ("layout/fragment_notification_watch_reminders_0".equals(obj)) {
                    return new ke2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_watch_reminders is invalid. Received: " + obj);
            case 79:
                if ("layout/fragment_onboarding_height_weight_0".equals(obj)) {
                    return new me2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_onboarding_height_weight is invalid. Received: " + obj);
            case 80:
                if ("layout/fragment_opt_in_0".equals(obj)) {
                    return new oe2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_opt_in is invalid. Received: " + obj);
            case 81:
                if ("layout/fragment_pairing_device_found_0".equals(obj)) {
                    return new qe2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_device_found is invalid. Received: " + obj);
            case 82:
                if ("layout/fragment_pairing_instructions_0".equals(obj)) {
                    return new se2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_instructions is invalid. Received: " + obj);
            case 83:
                if ("layout/fragment_pairing_look_for_device_0".equals(obj)) {
                    return new ue2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_look_for_device is invalid. Received: " + obj);
            case 84:
                if ("layout/fragment_permission_0".equals(obj)) {
                    return new we2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_permission is invalid. Received: " + obj);
            case 85:
                if ("layout/fragment_preferred_unit_0".equals(obj)) {
                    return new ye2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_preferred_unit is invalid. Received: " + obj);
            case 86:
                if ("layout/fragment_profile_change_pass_0".equals(obj)) {
                    return new af2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_change_pass is invalid. Received: " + obj);
            case 87:
                if ("layout/fragment_profile_edit_0".equals(obj)) {
                    return new cf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_edit is invalid. Received: " + obj);
            case 88:
                if ("layout/fragment_profile_goals_edit_0".equals(obj)) {
                    return new ef2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_goals_edit is invalid. Received: " + obj);
            case 89:
                if ("layout/fragment_profile_setup_0".equals(obj)) {
                    return new gf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_setup is invalid. Received: " + obj);
            case 90:
                if ("layout/fragment_remind_time_0".equals(obj)) {
                    return new if2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_remind_time is invalid. Received: " + obj);
            case 91:
                if ("layout/fragment_replace_battery_0".equals(obj)) {
                    return new kf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_replace_battery is invalid. Received: " + obj);
            case 92:
                if ("layout/fragment_search_second_timezone_0".equals(obj)) {
                    return new mf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_search_second_timezone is invalid. Received: " + obj);
            case 93:
                if ("layout/fragment_signin_0".equals(obj)) {
                    return new of2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signin is invalid. Received: " + obj);
            case 94:
                if ("layout/fragment_signup_0".equals(obj)) {
                    return new qf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signup is invalid. Received: " + obj);
            case 95:
                if ("layout/fragment_sleep_detail_0".equals(obj)) {
                    return new sf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_detail is invalid. Received: " + obj);
            case 96:
                if ("layout/fragment_sleep_overview_0".equals(obj)) {
                    return new uf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview is invalid. Received: " + obj);
            case 97:
                if ("layout/fragment_sleep_overview_day_0".equals(obj)) {
                    return new wf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_day is invalid. Received: " + obj);
            case 98:
                if ("layout/fragment_sleep_overview_month_0".equals(obj)) {
                    return new yf2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_month is invalid. Received: " + obj);
            case 99:
                if ("layout/fragment_sleep_overview_week_0".equals(obj)) {
                    return new ag2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_week is invalid. Received: " + obj);
            case 100:
                if ("layout/fragment_troubleshooting_0".equals(obj)) {
                    return new cg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_troubleshooting is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding c(qa qaVar, View view, int i, Object obj) {
        switch (i) {
            case 101:
                if ("layout/fragment_update_failed_troubleshooting_0".equals(obj)) {
                    return new eg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_update_failed_troubleshooting is invalid. Received: " + obj);
            case 102:
                if ("layout/fragment_update_firmware_0".equals(obj)) {
                    return new gg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_update_firmware is invalid. Received: " + obj);
            case 103:
                if ("layout/fragment_watch_app_search_0".equals(obj)) {
                    return new ig2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_app_search is invalid. Received: " + obj);
            case 104:
                if ("layout/fragment_watch_apps_0".equals(obj)) {
                    return new kg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_apps is invalid. Received: " + obj);
            case 105:
                if ("layout/fragment_watch_setting_0".equals(obj)) {
                    return new mg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_setting is invalid. Received: " + obj);
            case 106:
                if ("layout/fragment_weather_setting_0".equals(obj)) {
                    return new og2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_weather_setting is invalid. Received: " + obj);
            case 107:
                if ("layout/item_active_time_day_0".equals(obj)) {
                    return new qg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_day is invalid. Received: " + obj);
            case 108:
                if ("layout/item_active_time_week_0".equals(obj)) {
                    return new sg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_week is invalid. Received: " + obj);
            case 109:
                if ("layout/item_active_time_workout_day_0".equals(obj)) {
                    return new ug2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_workout_day is invalid. Received: " + obj);
            case 110:
                if ("layout/item_activity_day_0".equals(obj)) {
                    return new wg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_day is invalid. Received: " + obj);
            case 111:
                if ("layout/item_activity_week_0".equals(obj)) {
                    return new yg2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_week is invalid. Received: " + obj);
            case 112:
                if ("layout/item_activity_workout_day_0".equals(obj)) {
                    return new ah2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_workout_day is invalid. Received: " + obj);
            case 113:
                if ("layout/item_address_commute_time_0".equals(obj)) {
                    return new ch2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_address_commute_time is invalid. Received: " + obj);
            case 114:
                if ("layout/item_alarm_0".equals(obj)) {
                    return new eh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_alarm is invalid. Received: " + obj);
            case 115:
                if ("layout/item_app_hybrid_notification_0".equals(obj)) {
                    return new gh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_app_hybrid_notification is invalid. Received: " + obj);
            case 116:
                if ("layout/item_app_notification_0".equals(obj)) {
                    return new ih2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_app_notification is invalid. Received: " + obj);
            case 117:
                if ("layout/item_calories_day_0".equals(obj)) {
                    return new kh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_day is invalid. Received: " + obj);
            case 118:
                if ("layout/item_calories_week_0".equals(obj)) {
                    return new mh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_week is invalid. Received: " + obj);
            case 119:
                if ("layout/item_calories_workout_day_0".equals(obj)) {
                    return new oh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_workout_day is invalid. Received: " + obj);
            case 120:
                if ("layout/item_contact_0".equals(obj)) {
                    return new qh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_contact is invalid. Received: " + obj);
            case 121:
                if ("layout/item_contact_hybrid_0".equals(obj)) {
                    return new sh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_contact_hybrid is invalid. Received: " + obj);
            case 122:
                if ("layout/item_default_place_commute_time_0".equals(obj)) {
                    return new uh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_default_place_commute_time is invalid. Received: " + obj);
            case 123:
                if ("layout/item_favorite_contact_notification_0".equals(obj)) {
                    return new wh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_favorite_contact_notification is invalid. Received: " + obj);
            case 124:
                if ("layout/item_goal_tracking_day_0".equals(obj)) {
                    return new yh2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_day is invalid. Received: " + obj);
            case 125:
                if ("layout/item_goal_tracking_week_0".equals(obj)) {
                    return new ai2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_week is invalid. Received: " + obj);
            case 126:
                if ("layout/item_heart_rate_day_0".equals(obj)) {
                    return new ci2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_day is invalid. Received: " + obj);
            case 127:
                if ("layout/item_heart_rate_week_0".equals(obj)) {
                    return new ei2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_week is invalid. Received: " + obj);
            case 128:
                if ("layout/item_heartrate_workout_day_0".equals(obj)) {
                    return new gi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heartrate_workout_day is invalid. Received: " + obj);
            case 129:
                if ("layout/item_notification_hybrid_0".equals(obj)) {
                    return new ii2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_notification_hybrid is invalid. Received: " + obj);
            case 130:
                if ("layout/item_permission_0".equals(obj)) {
                    return new ki2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_permission is invalid. Received: " + obj);
            case 131:
                if ("layout/item_recorded_goal_tracking_0".equals(obj)) {
                    return new mi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_recorded_goal_tracking is invalid. Received: " + obj);
            case 132:
                if ("layout/item_single_permission_0".equals(obj)) {
                    return new oi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_single_permission is invalid. Received: " + obj);
            case 133:
                if ("layout/item_sleep_day_0".equals(obj)) {
                    return new qi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_day is invalid. Received: " + obj);
            case 134:
                if ("layout/item_sleep_week_0".equals(obj)) {
                    return new si2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_week is invalid. Received: " + obj);
            case 135:
                if ("layout/item_workout_0".equals(obj)) {
                    return new ui2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for item_workout is invalid. Received: " + obj);
            case 136:
                if ("layout/view_no_device_0".equals(obj)) {
                    return new wi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for view_no_device is invalid. Received: " + obj);
            case 137:
                if ("layout/view_tab_custom_0".equals(obj)) {
                    return new yi2(qaVar, view);
                }
                throw new IllegalArgumentException("The tag for view_tab_custom is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public ViewDataBinding a(qa qaVar, View view, int i) {
        int i2 = a.get(i);
        if (i2 <= 0) {
            return null;
        }
        Object tag = view.getTag();
        if (tag != null) {
            int i3 = (i2 - 1) / 50;
            if (i3 == 0) {
                return a(qaVar, view, i2, tag);
            }
            if (i3 == 1) {
                return b(qaVar, view, i2, tag);
            }
            if (i3 != 2) {
                return null;
            }
            return c(qaVar, view, i2, tag);
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    public ViewDataBinding a(qa qaVar, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    public List<oa> a() {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new va());
        return arrayList;
    }
}
