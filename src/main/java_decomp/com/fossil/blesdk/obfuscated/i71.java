package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i71 implements u61 {
    @DexIgnore
    public static /* final */ Map<String, i71> f; // = new HashMap();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new j71(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<t61> e; // = new ArrayList();

    @DexIgnore
    public i71(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        this.a.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static i71 a(Context context, String str) {
        i71 i71;
        SharedPreferences sharedPreferences;
        if (!((!o61.a() || str.startsWith("direct_boot:")) ? true : o61.a(context))) {
            return null;
        }
        synchronized (i71.class) {
            i71 = f.get(str);
            if (i71 == null) {
                if (str.startsWith("direct_boot:")) {
                    if (o61.a()) {
                        context = context.createDeviceProtectedStorageContext();
                    }
                    sharedPreferences = context.getSharedPreferences(str.substring(12), 0);
                } else {
                    sharedPreferences = context.getSharedPreferences(str, 0);
                }
                i71 = new i71(sharedPreferences);
                f.put(str, i71);
            }
        }
        return i71;
    }

    @DexIgnore
    public final Object a(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    map = this.a.getAll();
                    this.d = map;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    @DexIgnore
    public final /* synthetic */ void a(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            b71.f();
        }
        synchronized (this) {
            for (t61 a2 : this.e) {
                a2.a();
            }
        }
    }
}
