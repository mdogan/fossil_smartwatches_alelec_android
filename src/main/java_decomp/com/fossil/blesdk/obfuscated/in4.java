package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.hn4;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.StreamResetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class in4 {
    @DexIgnore
    public /* final */ sl4 a;
    @DexIgnore
    public hn4.a b;
    @DexIgnore
    public rm4 c;
    @DexIgnore
    public /* final */ am4 d;
    @DexIgnore
    public /* final */ vl4 e;
    @DexIgnore
    public /* final */ hm4 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ hn4 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public fn4 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ln4 n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends WeakReference<in4> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public a(in4 in4, Object obj) {
            super(in4);
            this.a = obj;
        }
    }

    @DexIgnore
    public in4(am4 am4, sl4 sl4, vl4 vl4, hm4 hm4, Object obj) {
        this.d = am4;
        this.a = sl4;
        this.e = vl4;
        this.f = hm4;
        this.h = new hn4(sl4, i(), vl4, hm4);
        this.g = obj;
    }

    @DexIgnore
    public ln4 a(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            ln4 a2 = a(chain.d(), chain.a(), chain.b(), okHttpClient.z(), okHttpClient.F(), z).a(okHttpClient, chain, this);
            synchronized (this.d) {
                this.n = a2;
            }
            return a2;
        } catch (IOException e2) {
            throw new RouteException(e2);
        }
    }

    @DexIgnore
    public ln4 b() {
        ln4 ln4;
        synchronized (this.d) {
            ln4 = this.n;
        }
        return ln4;
    }

    @DexIgnore
    public synchronized fn4 c() {
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        if (this.c == null) {
            hn4.a aVar = this.b;
            if ((aVar == null || !aVar.b()) && !this.h.a()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public void e() {
        fn4 fn4;
        Socket a2;
        synchronized (this.d) {
            fn4 = this.j;
            a2 = a(true, false, false);
            if (this.j != null) {
                fn4 = null;
            }
        }
        vm4.a(a2);
        if (fn4 != null) {
            this.f.b(this.e, (zl4) fn4);
        }
    }

    @DexIgnore
    public void f() {
        fn4 fn4;
        Socket a2;
        synchronized (this.d) {
            fn4 = this.j;
            a2 = a(false, true, false);
            if (this.j != null) {
                fn4 = null;
            }
        }
        vm4.a(a2);
        if (fn4 != null) {
            tm4.a.a(this.e, (IOException) null);
            this.f.b(this.e, (zl4) fn4);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket g() {
        fn4 fn4 = this.j;
        if (fn4 == null || !fn4.k) {
            return null;
        }
        return a(false, false, true);
    }

    @DexIgnore
    public rm4 h() {
        return this.c;
    }

    @DexIgnore
    public final gn4 i() {
        return tm4.a.a(this.d);
    }

    @DexIgnore
    public String toString() {
        fn4 c2 = c();
        return c2 != null ? c2.toString() : this.a.toString();
    }

    @DexIgnore
    public Socket b(fn4 fn4) {
        if (this.n == null && this.j.n.size() == 1) {
            Socket a2 = a(true, false, false);
            this.j = fn4;
            fn4.n.add(this.j.n.get(0));
            return a2;
        }
        throw new IllegalStateException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0.a(r9) != false) goto L_0x0018;
     */
    @DexIgnore
    public final fn4 a(int i2, int i3, int i4, int i5, boolean z, boolean z2) throws IOException {
        while (true) {
            fn4 a2 = a(i2, i3, i4, i5, z);
            synchronized (this.d) {
                if (a2.l == 0) {
                    return a2;
                }
            }
            e();
        }
        while (true) {
        }
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:695)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    @DexIgnore
    public final com.fossil.blesdk.obfuscated.fn4 a(int r19, int r20, int r21, int r22, boolean r23) throws java.io.IOException {
        /*
            r18 = this;
            r1 = r18
            com.fossil.blesdk.obfuscated.am4 r2 = r1.d
            monitor-enter(r2)
            boolean r0 = r1.l     // Catch:{ all -> 0x013c }
            if (r0 != 0) goto L_0x0134
            com.fossil.blesdk.obfuscated.ln4 r0 = r1.n     // Catch:{ all -> 0x013c }
            if (r0 != 0) goto L_0x012c
            boolean r0 = r1.m     // Catch:{ all -> 0x013c }
            if (r0 != 0) goto L_0x0124
            com.fossil.blesdk.obfuscated.fn4 r0 = r1.j     // Catch:{ all -> 0x013c }
            java.net.Socket r3 = r18.g()     // Catch:{ all -> 0x013c }
            com.fossil.blesdk.obfuscated.fn4 r4 = r1.j     // Catch:{ all -> 0x013c }
            r5 = 0
            if (r4 == 0) goto L_0x0020
            com.fossil.blesdk.obfuscated.fn4 r0 = r1.j     // Catch:{ all -> 0x013c }
            r4 = r5
            goto L_0x0022
        L_0x0020:
            r4 = r0
            r0 = r5
        L_0x0022:
            boolean r6 = r1.k     // Catch:{ all -> 0x013c }
            if (r6 != 0) goto L_0x0027
            r4 = r5
        L_0x0027:
            r6 = 1
            r7 = 0
            if (r0 != 0) goto L_0x0043
            com.fossil.blesdk.obfuscated.tm4 r8 = com.fossil.blesdk.obfuscated.tm4.a     // Catch:{ all -> 0x013c }
            com.fossil.blesdk.obfuscated.am4 r9 = r1.d     // Catch:{ all -> 0x013c }
            com.fossil.blesdk.obfuscated.sl4 r10 = r1.a     // Catch:{ all -> 0x013c }
            r8.a(r9, r10, r1, r5)     // Catch:{ all -> 0x013c }
            com.fossil.blesdk.obfuscated.fn4 r8 = r1.j     // Catch:{ all -> 0x013c }
            if (r8 == 0) goto L_0x003e
            com.fossil.blesdk.obfuscated.fn4 r0 = r1.j     // Catch:{ all -> 0x013c }
            r8 = r0
            r9 = r5
            r0 = 1
            goto L_0x0046
        L_0x003e:
            com.fossil.blesdk.obfuscated.rm4 r8 = r1.c     // Catch:{ all -> 0x013c }
            r9 = r8
            r8 = r0
            goto L_0x0045
        L_0x0043:
            r8 = r0
            r9 = r5
        L_0x0045:
            r0 = 0
        L_0x0046:
            monitor-exit(r2)     // Catch:{ all -> 0x013c }
            com.fossil.blesdk.obfuscated.vm4.a((java.net.Socket) r3)
            if (r4 == 0) goto L_0x0053
            com.fossil.blesdk.obfuscated.hm4 r2 = r1.f
            com.fossil.blesdk.obfuscated.vl4 r3 = r1.e
            r2.b((com.fossil.blesdk.obfuscated.vl4) r3, (com.fossil.blesdk.obfuscated.zl4) r4)
        L_0x0053:
            if (r0 == 0) goto L_0x005c
            com.fossil.blesdk.obfuscated.hm4 r2 = r1.f
            com.fossil.blesdk.obfuscated.vl4 r3 = r1.e
            r2.a((com.fossil.blesdk.obfuscated.vl4) r3, (com.fossil.blesdk.obfuscated.zl4) r8)
        L_0x005c:
            if (r8 == 0) goto L_0x005f
            return r8
        L_0x005f:
            if (r9 != 0) goto L_0x0075
            com.fossil.blesdk.obfuscated.hn4$a r2 = r1.b
            if (r2 == 0) goto L_0x006b
            boolean r2 = r2.b()
            if (r2 != 0) goto L_0x0075
        L_0x006b:
            com.fossil.blesdk.obfuscated.hn4 r2 = r1.h
            com.fossil.blesdk.obfuscated.hn4$a r2 = r2.c()
            r1.b = r2
            r2 = 1
            goto L_0x0076
        L_0x0075:
            r2 = 0
        L_0x0076:
            com.fossil.blesdk.obfuscated.am4 r3 = r1.d
            monitor-enter(r3)
            boolean r4 = r1.m     // Catch:{ all -> 0x0121 }
            if (r4 != 0) goto L_0x0119
            if (r2 == 0) goto L_0x00a8
            com.fossil.blesdk.obfuscated.hn4$a r2 = r1.b     // Catch:{ all -> 0x0121 }
            java.util.List r2 = r2.a()     // Catch:{ all -> 0x0121 }
            int r4 = r2.size()     // Catch:{ all -> 0x0121 }
            r10 = 0
        L_0x008a:
            if (r10 >= r4) goto L_0x00a8
            java.lang.Object r11 = r2.get(r10)     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.rm4 r11 = (com.fossil.blesdk.obfuscated.rm4) r11     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.tm4 r12 = com.fossil.blesdk.obfuscated.tm4.a     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.am4 r13 = r1.d     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.sl4 r14 = r1.a     // Catch:{ all -> 0x0121 }
            r12.a(r13, r14, r1, r11)     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.fn4 r12 = r1.j     // Catch:{ all -> 0x0121 }
            if (r12 == 0) goto L_0x00a5
            com.fossil.blesdk.obfuscated.fn4 r8 = r1.j     // Catch:{ all -> 0x0121 }
            r1.c = r11     // Catch:{ all -> 0x0121 }
            r0 = 1
            goto L_0x00a8
        L_0x00a5:
            int r10 = r10 + 1
            goto L_0x008a
        L_0x00a8:
            if (r0 != 0) goto L_0x00c0
            if (r9 != 0) goto L_0x00b2
            com.fossil.blesdk.obfuscated.hn4$a r2 = r1.b     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.rm4 r9 = r2.c()     // Catch:{ all -> 0x0121 }
        L_0x00b2:
            r1.c = r9     // Catch:{ all -> 0x0121 }
            r1.i = r7     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.fn4 r8 = new com.fossil.blesdk.obfuscated.fn4     // Catch:{ all -> 0x0121 }
            com.fossil.blesdk.obfuscated.am4 r2 = r1.d     // Catch:{ all -> 0x0121 }
            r8.<init>(r2, r9)     // Catch:{ all -> 0x0121 }
            r1.a(r8, r7)     // Catch:{ all -> 0x0121 }
        L_0x00c0:
            monitor-exit(r3)     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x00cb
            com.fossil.blesdk.obfuscated.hm4 r0 = r1.f
            com.fossil.blesdk.obfuscated.vl4 r2 = r1.e
            r0.a((com.fossil.blesdk.obfuscated.vl4) r2, (com.fossil.blesdk.obfuscated.zl4) r8)
            return r8
        L_0x00cb:
            com.fossil.blesdk.obfuscated.vl4 r0 = r1.e
            com.fossil.blesdk.obfuscated.hm4 r2 = r1.f
            r10 = r8
            r11 = r19
            r12 = r20
            r13 = r21
            r14 = r22
            r15 = r23
            r16 = r0
            r17 = r2
            r10.a(r11, r12, r13, r14, r15, r16, r17)
            com.fossil.blesdk.obfuscated.gn4 r0 = r18.i()
            com.fossil.blesdk.obfuscated.rm4 r2 = r8.f()
            r0.a(r2)
            com.fossil.blesdk.obfuscated.am4 r2 = r1.d
            monitor-enter(r2)
            r1.k = r6     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.tm4 r0 = com.fossil.blesdk.obfuscated.tm4.a     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.am4 r3 = r1.d     // Catch:{ all -> 0x0116 }
            r0.b(r3, r8)     // Catch:{ all -> 0x0116 }
            boolean r0 = r8.e()     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x010a
            com.fossil.blesdk.obfuscated.tm4 r0 = com.fossil.blesdk.obfuscated.tm4.a     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.am4 r3 = r1.d     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.sl4 r4 = r1.a     // Catch:{ all -> 0x0116 }
            java.net.Socket r5 = r0.a((com.fossil.blesdk.obfuscated.am4) r3, (com.fossil.blesdk.obfuscated.sl4) r4, (com.fossil.blesdk.obfuscated.in4) r1)     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.fn4 r8 = r1.j     // Catch:{ all -> 0x0116 }
        L_0x010a:
            monitor-exit(r2)     // Catch:{ all -> 0x0116 }
            com.fossil.blesdk.obfuscated.vm4.a((java.net.Socket) r5)
            com.fossil.blesdk.obfuscated.hm4 r0 = r1.f
            com.fossil.blesdk.obfuscated.vl4 r2 = r1.e
            r0.a((com.fossil.blesdk.obfuscated.vl4) r2, (com.fossil.blesdk.obfuscated.zl4) r8)
            return r8
        L_0x0116:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0116 }
            throw r0
        L_0x0119:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0121 }
            java.lang.String r2 = "Canceled"
            r0.<init>(r2)     // Catch:{ all -> 0x0121 }
            throw r0     // Catch:{ all -> 0x0121 }
        L_0x0121:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0121 }
            throw r0
        L_0x0124:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x013c }
            java.lang.String r3 = "Canceled"
            r0.<init>(r3)     // Catch:{ all -> 0x013c }
            throw r0     // Catch:{ all -> 0x013c }
        L_0x012c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x013c }
            java.lang.String r3 = "codec != null"
            r0.<init>(r3)     // Catch:{ all -> 0x013c }
            throw r0     // Catch:{ all -> 0x013c }
        L_0x0134:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x013c }
            java.lang.String r3 = "released"
            r0.<init>(r3)     // Catch:{ all -> 0x013c }
            throw r0     // Catch:{ all -> 0x013c }
        L_0x013c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x013c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.obfuscated.in4.a(int, int, int, int, boolean):com.fossil.blesdk.obfuscated.fn4");
    }

    @DexIgnore
    public void a(boolean z, ln4 ln4, long j2, IOException iOException) {
        fn4 fn4;
        Socket a2;
        boolean z2;
        this.f.b(this.e, j2);
        synchronized (this.d) {
            if (ln4 != null) {
                if (ln4 == this.n) {
                    if (!z) {
                        this.j.l++;
                    }
                    fn4 = this.j;
                    a2 = a(z, false, true);
                    if (this.j != null) {
                        fn4 = null;
                    }
                    z2 = this.l;
                }
            }
            throw new IllegalStateException("expected " + this.n + " but was " + ln4);
        }
        vm4.a(a2);
        if (fn4 != null) {
            this.f.b(this.e, (zl4) fn4);
        }
        if (iOException != null) {
            this.f.a(this.e, tm4.a.a(this.e, iOException));
        } else if (z2) {
            tm4.a.a(this.e, (IOException) null);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket a(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.n = null;
        }
        if (z2) {
            this.l = true;
        }
        fn4 fn4 = this.j;
        if (fn4 != null) {
            if (z) {
                fn4.k = true;
            }
            if (this.n == null && (this.l || this.j.k)) {
                a(this.j);
                if (this.j.n.isEmpty()) {
                    this.j.o = System.nanoTime();
                    if (tm4.a.a(this.d, this.j)) {
                        socket = this.j.g();
                        this.j = null;
                        return socket;
                    }
                }
                socket = null;
                this.j = null;
                return socket;
            }
        }
        return null;
    }

    @DexIgnore
    public void a() {
        ln4 ln4;
        fn4 fn4;
        synchronized (this.d) {
            this.m = true;
            ln4 = this.n;
            fn4 = this.j;
        }
        if (ln4 != null) {
            ln4.cancel();
        } else if (fn4 != null) {
            fn4.b();
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        boolean z;
        fn4 fn4;
        Socket a2;
        synchronized (this.d) {
            if (iOException instanceof StreamResetException) {
                ErrorCode errorCode = ((StreamResetException) iOException).errorCode;
                if (errorCode == ErrorCode.REFUSED_STREAM) {
                    this.i++;
                    if (this.i > 1) {
                        this.c = null;
                    }
                    z = false;
                    fn4 = this.j;
                    a2 = a(z, false, true);
                    if (this.j != null || !this.k) {
                        fn4 = null;
                    }
                } else {
                    if (errorCode != ErrorCode.CANCEL) {
                        this.c = null;
                    }
                    z = false;
                    fn4 = this.j;
                    a2 = a(z, false, true);
                    fn4 = null;
                }
            } else {
                if (this.j != null && (!this.j.e() || (iOException instanceof ConnectionShutdownException))) {
                    if (this.j.l == 0) {
                        if (!(this.c == null || iOException == null)) {
                            this.h.a(this.c, iOException);
                        }
                        this.c = null;
                    }
                }
                z = false;
                fn4 = this.j;
                a2 = a(z, false, true);
                fn4 = null;
            }
            z = true;
            fn4 = this.j;
            a2 = a(z, false, true);
            fn4 = null;
        }
        vm4.a(a2);
        if (fn4 != null) {
            this.f.b(this.e, (zl4) fn4);
        }
    }

    @DexIgnore
    public void a(fn4 fn4, boolean z) {
        if (this.j == null) {
            this.j = fn4;
            this.k = z;
            fn4.n.add(new a(this, this.g));
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final void a(fn4 fn4) {
        int size = fn4.n.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (fn4.n.get(i2).get() == this) {
                fn4.n.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }
}
