package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk3 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ fk3 b;

    @DexIgnore
    public hk3(BaseActivity baseActivity, fk3 fk3) {
        wd4.b(baseActivity, "baseActivity");
        wd4.b(fk3, "mView");
        this.a = baseActivity;
        this.b = fk3;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final fk3 b() {
        return this.b;
    }
}
