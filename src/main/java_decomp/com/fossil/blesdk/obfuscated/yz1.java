package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.internal.LinkedTreeMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yz1 extends JsonElement {
    @DexIgnore
    public /* final */ LinkedTreeMap<String, JsonElement> a; // = new LinkedTreeMap<>();

    @DexIgnore
    public void a(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = xz1.a;
        }
        this.a.put(str, jsonElement);
    }

    @DexIgnore
    public uz1 b(String str) {
        return (uz1) this.a.get(str);
    }

    @DexIgnore
    public yz1 c(String str) {
        return (yz1) this.a.get(str);
    }

    @DexIgnore
    public boolean d(String str) {
        return this.a.containsKey(str);
    }

    @DexIgnore
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.a.entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof yz1) && ((yz1) obj).a.equals(this.a));
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public void a(String str, String str2) {
        a(str, a((Object) str2));
    }

    @DexIgnore
    public void a(String str, Number number) {
        a(str, a((Object) number));
    }

    @DexIgnore
    public void a(String str, Boolean bool) {
        a(str, a((Object) bool));
    }

    @DexIgnore
    public final JsonElement a(Object obj) {
        return obj == null ? xz1.a : new a02(obj);
    }

    @DexIgnore
    public JsonElement a(String str) {
        return this.a.get(str);
    }
}
