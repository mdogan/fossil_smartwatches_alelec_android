package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.kt0;
import com.fossil.blesdk.obfuscated.lt0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lt0<MessageType extends kt0<MessageType, BuilderType>, BuilderType extends lt0<MessageType, BuilderType>> implements uv0 {
    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public final /* synthetic */ uv0 a(tv0 tv0) {
        if (b().getClass().isInstance(tv0)) {
            a((kt0) tv0);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
