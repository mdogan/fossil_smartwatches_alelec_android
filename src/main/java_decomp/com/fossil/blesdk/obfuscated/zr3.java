package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zr3 implements Factory<yr3> {
    @DexIgnore
    public static yr3 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, i42 i42, ContentResolver contentResolver, ir2 ir2, fn2 fn2) {
        return new yr3(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, i42, contentResolver, ir2, fn2);
    }
}
