package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wd1 extends m31 implements ud1 {
    @DexIgnore
    public wd1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }
}
