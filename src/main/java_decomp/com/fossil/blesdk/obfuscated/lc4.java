package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lc4 extends CoroutineContext.a {
    @DexIgnore
    public static final b b = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends CoroutineContext.a> E a(lc4 lc4, CoroutineContext.b<E> bVar) {
            wd4.b(bVar, "key");
            if (bVar != lc4.b) {
                return null;
            }
            if (lc4 != null) {
                return lc4;
            }
            throw new TypeCastException("null cannot be cast to non-null type E");
        }

        @DexIgnore
        public static void a(lc4 lc4, kc4<?> kc4) {
            wd4.b(kc4, "continuation");
        }

        @DexIgnore
        public static CoroutineContext b(lc4 lc4, CoroutineContext.b<?> bVar) {
            wd4.b(bVar, "key");
            return bVar == lc4.b ? EmptyCoroutineContext.INSTANCE : lc4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineContext.b<lc4> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();
    }

    @DexIgnore
    void b(kc4<?> kc4);

    @DexIgnore
    <T> kc4<T> c(kc4<? super T> kc4);
}
