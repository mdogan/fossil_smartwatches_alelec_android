package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l80 extends f80 {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public /* final */ long P;
    @DexIgnore
    public /* final */ long Q;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ l80(long j, long j2, short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(j, j2, s, peripheral, (i2 & 16) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.P).putInt((int) this.Q).array();
        wd4.a((Object) array, "ByteBuffer.allocate(8)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final long J() {
        return this.O;
    }

    @DexIgnore
    public final long K() {
        return this.N;
    }

    @DexIgnore
    public final long L() {
        return this.M;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = o90.b(order.getInt(0));
            this.N = o90.b(order.getInt(4));
            this.O = o90.b(order.getInt(8));
            xa0.a(xa0.a(xa0.a(a, JSONKey.VERIFIED_DATA_OFFSET, Long.valueOf(this.M)), JSONKey.VERIFIED_DATA_LENGTH, Long.valueOf(this.N)), JSONKey.VERIFIED_DATA_CRC, Long.valueOf(this.O));
        }
        return a;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(super.t(), JSONKey.OFFSET, Long.valueOf(this.P)), JSONKey.LENGTH, Long.valueOf(this.Q));
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(xa0.a(super.u(), JSONKey.VERIFIED_DATA_OFFSET, Long.valueOf(this.M)), JSONKey.VERIFIED_DATA_LENGTH, Long.valueOf(this.N)), JSONKey.VERIFIED_DATA_CRC, Long.valueOf(this.O));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l80(long j, long j2, short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.VERIFY_DATA, s, RequestId.VERIFY_DATA, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.P = j;
        this.Q = j2;
    }
}
