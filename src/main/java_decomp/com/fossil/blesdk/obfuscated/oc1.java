package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public interface oc1 {
    @DexIgnore
    ie0<Status> a(he0 he0, sc1 sc1);

    @DexIgnore
    ie0<Status> a(he0 he0, LocationRequest locationRequest, sc1 sc1);
}
