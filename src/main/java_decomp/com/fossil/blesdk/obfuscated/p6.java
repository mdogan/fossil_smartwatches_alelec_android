package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p6 {
    @DexIgnore
    public static Shader a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            Resources.Theme theme2 = theme;
            TypedArray a2 = s6.a(resources, theme2, attributeSet, u5.GradientColor);
            float a3 = s6.a(a2, xmlPullParser2, "startX", u5.GradientColor_android_startX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a4 = s6.a(a2, xmlPullParser2, "startY", u5.GradientColor_android_startY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a5 = s6.a(a2, xmlPullParser2, "endX", u5.GradientColor_android_endX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a6 = s6.a(a2, xmlPullParser2, "endY", u5.GradientColor_android_endY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a7 = s6.a(a2, xmlPullParser2, "centerX", u5.GradientColor_android_centerX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a8 = s6.a(a2, xmlPullParser2, "centerY", u5.GradientColor_android_centerY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            int b = s6.b(a2, xmlPullParser2, "type", u5.GradientColor_android_type, 0);
            int a9 = s6.a(a2, xmlPullParser2, "startColor", u5.GradientColor_android_startColor, 0);
            boolean a10 = s6.a(xmlPullParser2, "centerColor");
            int a11 = s6.a(a2, xmlPullParser2, "centerColor", u5.GradientColor_android_centerColor, 0);
            int a12 = s6.a(a2, xmlPullParser2, "endColor", u5.GradientColor_android_endColor, 0);
            int b2 = s6.b(a2, xmlPullParser2, "tileMode", u5.GradientColor_android_tileMode, 0);
            float f = a7;
            float a13 = s6.a(a2, xmlPullParser2, "gradientRadius", u5.GradientColor_android_gradientRadius, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.recycle();
            a a14 = a(b(resources, xmlPullParser, attributeSet, theme), a9, a12, a10, a11);
            if (b == 1) {
                float f2 = f;
                if (a13 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    int[] iArr = a14.a;
                    return new RadialGradient(f2, a8, a13, iArr, a14.b, a(b2));
                }
                throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            } else if (b != 2) {
                return new LinearGradient(a3, a4, a5, a6, a14.a, a14.b, a(b2));
            } else {
                return new SweepGradient(f, a8, a14.a, a14.b);
            }
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0089, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r9.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' " + "attribute!");
     */
    @DexIgnore
    public static a b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth = xmlPullParser.getDepth() + 1;
        ArrayList arrayList = new ArrayList(20);
        ArrayList arrayList2 = new ArrayList(20);
        while (true) {
            int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            int depth2 = xmlPullParser.getDepth();
            if (depth2 < depth && next == 3) {
                break;
            } else if (next == 2 && depth2 <= depth && xmlPullParser.getName().equals("item")) {
                TypedArray a2 = s6.a(resources, theme, attributeSet, u5.GradientColorItem);
                boolean hasValue = a2.hasValue(u5.GradientColorItem_android_color);
                boolean hasValue2 = a2.hasValue(u5.GradientColorItem_android_offset);
                if (!hasValue || !hasValue2) {
                } else {
                    int color = a2.getColor(u5.GradientColorItem_android_color, 0);
                    float f = a2.getFloat(u5.GradientColorItem_android_offset, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    a2.recycle();
                    arrayList2.add(Integer.valueOf(color));
                    arrayList.add(Float.valueOf(f));
                }
            }
        }
        if (arrayList2.size() > 0) {
            return new a((List<Integer>) arrayList2, (List<Float>) arrayList);
        }
        return null;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ float[] b;

        @DexIgnore
        public a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; i++) {
                this.a[i] = list.get(i).intValue();
                this.b[i] = list2.get(i).floatValue();
            }
        }

        @DexIgnore
        public a(int i, int i2) {
            this.a = new int[]{i, i2};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f};
        }

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.a = new int[]{i, i2, i3};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
        }
    }

    @DexIgnore
    public static a a(a aVar, int i, int i2, boolean z, int i3) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new a(i, i3, i2);
        }
        return new a(i, i2);
    }

    @DexIgnore
    public static Shader.TileMode a(int i) {
        if (i == 1) {
            return Shader.TileMode.REPEAT;
        }
        if (i != 2) {
            return Shader.TileMode.CLAMP;
        }
        return Shader.TileMode.MIRROR;
    }
}
