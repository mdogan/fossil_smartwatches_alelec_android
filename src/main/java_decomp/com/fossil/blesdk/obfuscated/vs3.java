package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vs3 implements yz3 {
    @DexIgnore
    public static volatile vs3 h;
    @DexIgnore
    public a e;
    @DexIgnore
    public xz3 f;
    @DexIgnore
    public String g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);

        @DexIgnore
        void b();

        @DexIgnore
        void c();
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:690)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    @DexIgnore
    public static synchronized com.fossil.blesdk.obfuscated.vs3 a() {
        /*
            java.lang.Class<com.fossil.blesdk.obfuscated.vs3> r0 = com.fossil.blesdk.obfuscated.vs3.class
            monitor-enter(r0)
            com.fossil.blesdk.obfuscated.vs3 r1 = h     // Catch:{ all -> 0x001c }
            if (r1 != 0) goto L_0x0018
            monitor-enter(r0)     // Catch:{ all -> 0x001c }
            com.fossil.blesdk.obfuscated.vs3 r1 = h     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0013
            com.fossil.blesdk.obfuscated.vs3 r1 = new com.fossil.blesdk.obfuscated.vs3     // Catch:{ all -> 0x0015 }
            r1.<init>()     // Catch:{ all -> 0x0015 }
            h = r1     // Catch:{ all -> 0x0015 }
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            goto L_0x0018
        L_0x0015:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            throw r1     // Catch:{ all -> 0x001c }
        L_0x0018:
            com.fossil.blesdk.obfuscated.vs3 r1 = h     // Catch:{ all -> 0x001c }
            monitor-exit(r0)
            return r1
        L_0x001c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.obfuscated.vs3.a():com.fossil.blesdk.obfuscated.vs3");
    }

    @DexIgnore
    public void a(iz3 iz3) {
    }

    @DexIgnore
    public final void b(jz3 jz3) {
        if (jz3.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize canceled!");
            a aVar = this.e;
            if (aVar != null) {
                aVar.c();
            }
        }
    }

    @DexIgnore
    public final void c(jz3 jz3) {
        if (jz3.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize denied!");
            a aVar = this.e;
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    @DexIgnore
    public final void d(jz3 jz3) {
        if (jz3.a() == 1) {
            sz3 sz3 = (sz3) jz3;
            if (sz3.d.equals("com.fossil.wearables.fossil.tag_wechat_login")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GraphRequest.TAG;
                local.d(str, "Wechat authorize succeed, data = " + sz3.c + ", openid: " + sz3.b);
                a aVar = this.e;
                if (aVar != null) {
                    aVar.a(sz3.c);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        this.g = str;
    }

    @DexIgnore
    public void a(Context context) {
        this.f = a04.a(context, this.g, false);
        this.f.a(this.g);
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, initialize");
    }

    @DexIgnore
    public void a(a aVar) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, login...");
        this.e = aVar;
        if (!this.f.a()) {
            this.e.b();
            return;
        }
        rz3 rz3 = new rz3();
        rz3.c = "snsapi_userinfo";
        rz3.d = "com.fossil.wearables.fossil.tag_wechat_login";
        if (!this.f.a((iz3) rz3)) {
            this.e.c();
        }
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, start authorize...");
    }

    @DexIgnore
    public void a(jz3 jz3) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, onResp...");
        int i = jz3.a;
        if (i == -4) {
            c(jz3);
        } else if (i == -2) {
            b(jz3);
        } else if (i != 0) {
            a aVar = this.e;
            if (aVar != null) {
                aVar.a();
            }
        } else {
            d(jz3);
        }
    }

    @DexIgnore
    public void a(Intent intent, yz3 yz3) {
        xz3 xz3 = this.f;
        if (xz3 != null) {
            xz3.a(intent, yz3);
        }
    }
}
