package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.request.RequestCoordinator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nv implements RequestCoordinator, pv {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ RequestCoordinator b;
    @DexIgnore
    public volatile pv c;
    @DexIgnore
    public volatile pv d;
    @DexIgnore
    public RequestCoordinator.RequestState e;
    @DexIgnore
    public RequestCoordinator.RequestState f;

    @DexIgnore
    public nv(Object obj, RequestCoordinator requestCoordinator) {
        RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
        this.e = requestState;
        this.f = requestState;
        this.a = obj;
        this.b = requestCoordinator;
    }

    @DexIgnore
    public void a(pv pvVar, pv pvVar2) {
        this.c = pvVar;
        this.d = pvVar2;
    }

    @DexIgnore
    public final boolean b() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.f(this);
    }

    @DexIgnore
    public void c() {
        synchronized (this.a) {
            if (this.e != RequestCoordinator.RequestState.RUNNING) {
                this.e = RequestCoordinator.RequestState.RUNNING;
                this.c.c();
            }
        }
    }

    @DexIgnore
    public void clear() {
        synchronized (this.a) {
            this.e = RequestCoordinator.RequestState.CLEARED;
            this.c.clear();
            if (this.f != RequestCoordinator.RequestState.CLEARED) {
                this.f = RequestCoordinator.RequestState.CLEARED;
                this.d.clear();
            }
        }
    }

    @DexIgnore
    public void d() {
        synchronized (this.a) {
            if (this.e == RequestCoordinator.RequestState.RUNNING) {
                this.e = RequestCoordinator.RequestState.PAUSED;
                this.c.d();
            }
            if (this.f == RequestCoordinator.RequestState.RUNNING) {
                this.f = RequestCoordinator.RequestState.PAUSED;
                this.d.d();
            }
        }
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.e == RequestCoordinator.RequestState.CLEARED && this.f == RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }

    @DexIgnore
    public boolean f() {
        boolean z;
        synchronized (this.a) {
            if (this.e != RequestCoordinator.RequestState.SUCCESS) {
                if (this.f != RequestCoordinator.RequestState.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean g() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.c(this);
    }

    @DexIgnore
    public final boolean h() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.d(this);
    }

    @DexIgnore
    public final boolean i() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator != null && requestCoordinator.a();
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.a) {
            if (this.e != RequestCoordinator.RequestState.RUNNING) {
                if (this.f != RequestCoordinator.RequestState.RUNNING) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        return;
     */
    @DexIgnore
    public void b(pv pvVar) {
        synchronized (this.a) {
            if (!pvVar.equals(this.d)) {
                this.e = RequestCoordinator.RequestState.FAILED;
                if (this.f != RequestCoordinator.RequestState.RUNNING) {
                    this.f = RequestCoordinator.RequestState.RUNNING;
                    this.d.c();
                }
            } else {
                this.f = RequestCoordinator.RequestState.FAILED;
                if (this.b != null) {
                    this.b.b(this);
                }
            }
        }
    }

    @DexIgnore
    public final boolean g(pv pvVar) {
        return pvVar.equals(this.c) || (this.e == RequestCoordinator.RequestState.FAILED && pvVar.equals(this.d));
    }

    @DexIgnore
    public boolean a(pv pvVar) {
        if (!(pvVar instanceof nv)) {
            return false;
        }
        nv nvVar = (nv) pvVar;
        if (!this.c.a(nvVar.c) || !this.d.a(nvVar.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void e(pv pvVar) {
        synchronized (this.a) {
            if (pvVar.equals(this.c)) {
                this.e = RequestCoordinator.RequestState.SUCCESS;
            } else if (pvVar.equals(this.d)) {
                this.f = RequestCoordinator.RequestState.SUCCESS;
            }
            if (this.b != null) {
                this.b.e(this);
            }
        }
    }

    @DexIgnore
    public boolean f(pv pvVar) {
        boolean z;
        synchronized (this.a) {
            z = b() && g(pvVar);
        }
        return z;
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.a) {
            if (!i()) {
                if (!f()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public boolean c(pv pvVar) {
        boolean z;
        synchronized (this.a) {
            z = g() && g(pvVar);
        }
        return z;
    }

    @DexIgnore
    public boolean d(pv pvVar) {
        boolean z;
        synchronized (this.a) {
            z = h() && g(pvVar);
        }
        return z;
    }
}
