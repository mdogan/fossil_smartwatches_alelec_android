package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.in4;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class am4 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), vm4.a("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<fn4> d;
    @DexIgnore
    public /* final */ gn4 e;
    @DexIgnore
    public boolean f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
        public void run() {
            while (true) {
                long a = am4.this.a(System.nanoTime());
                if (a != -1) {
                    if (a > 0) {
                        long j = a / 1000000;
                        long j2 = a - (1000000 * j);
                        synchronized (am4.this) {
                            am4.this.wait(j, (int) j2);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public am4() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public fn4 a(sl4 sl4, in4 in4, rm4 rm4) {
        for (fn4 next : this.d) {
            if (next.a(sl4, rm4)) {
                in4.a(next, true);
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public void b(fn4 fn4) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(fn4);
    }

    @DexIgnore
    public am4(int i, long j, TimeUnit timeUnit) {
        this.c = new a();
        this.d = new ArrayDeque();
        this.e = new gn4();
        this.a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public Socket a(sl4 sl4, in4 in4) {
        for (fn4 next : this.d) {
            if (next.a(sl4, (rm4) null) && next.e() && next != in4.c()) {
                return in4.b(next);
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(fn4 fn4) {
        if (fn4.k || this.a == 0) {
            this.d.remove(fn4);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            long j2 = Long.MIN_VALUE;
            fn4 fn4 = null;
            int i = 0;
            int i2 = 0;
            for (fn4 next : this.d) {
                if (a(next, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - next.o;
                    if (j3 > j2) {
                        fn4 = next;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.b) {
                if (i <= this.a) {
                    if (i > 0) {
                        long j4 = this.b - j2;
                        return j4;
                    } else if (i2 > 0) {
                        long j5 = this.b;
                        return j5;
                    } else {
                        this.f = false;
                        return -1;
                    }
                }
            }
            this.d.remove(fn4);
            vm4.a(fn4.g());
            return 0;
        }
    }

    @DexIgnore
    public final int a(fn4 fn4, long j) {
        List<Reference<in4>> list = fn4.n;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                mo4.d().a("A connection to " + fn4.f().a().k() + " was leaked. Did you forget to close a response body?", ((in4.a) reference).a);
                list.remove(i);
                fn4.k = true;
                if (list.isEmpty()) {
                    fn4.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
