package com.fossil.blesdk.obfuscated;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sy3 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = "0";
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public static sy3 a(String str) {
        sy3 sy3 = new sy3();
        if (xy3.a(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    sy3.a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    sy3.b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    sy3.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    sy3.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return sy3;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            xy3.a(jSONObject, "ui", this.a);
            xy3.a(jSONObject, "mc", this.b);
            xy3.a(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final String toString() {
        return b().toString();
    }
}
