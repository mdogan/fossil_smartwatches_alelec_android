package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uz1 extends JsonElement implements Iterable<JsonElement> {
    @DexIgnore
    public /* final */ List<JsonElement> e; // = new ArrayList();

    @DexIgnore
    public void a(String str) {
        this.e.add(str == null ? xz1.a : new a02(str));
    }

    @DexIgnore
    public int b() {
        if (this.e.size() == 1) {
            return this.e.get(0).b();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof uz1) && ((uz1) obj).e.equals(this.e));
    }

    @DexIgnore
    public String f() {
        if (this.e.size() == 1) {
            return this.e.get(0).f();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public JsonElement get(int i) {
        return this.e.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public Iterator<JsonElement> iterator() {
        return this.e.iterator();
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    public void a(JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = xz1.a;
        }
        this.e.add(jsonElement);
    }

    @DexIgnore
    public boolean a() {
        if (this.e.size() == 1) {
            return this.e.get(0).a();
        }
        throw new IllegalStateException();
    }
}
