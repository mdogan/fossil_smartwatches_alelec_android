package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.wb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wb1<M extends wb1<M>> extends bc1 {
    @DexIgnore
    public yb1 b;

    @DexIgnore
    public int a() {
        if (this.b == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.b.b(); i2++) {
            i += this.b.b(i2).zzf();
        }
        return i;
    }

    @DexIgnore
    public final /* synthetic */ bc1 c() throws CloneNotSupportedException {
        return (wb1) clone();
    }

    @DexIgnore
    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        wb1 wb1 = (wb1) super.clone();
        ac1.a(this, wb1);
        return wb1;
    }

    @DexIgnore
    public void a(vb1 vb1) throws IOException {
        if (this.b != null) {
            for (int i = 0; i < this.b.b(); i++) {
                this.b.b(i).a(vb1);
            }
        }
    }

    @DexIgnore
    public final boolean a(ub1 ub1, int i) throws IOException {
        int a = ub1.a();
        if (!ub1.b(i)) {
            return false;
        }
        int i2 = i >>> 3;
        dc1 dc1 = new dc1(i, ub1.a(a, ub1.a() - a));
        zb1 zb1 = null;
        yb1 yb1 = this.b;
        if (yb1 == null) {
            this.b = new yb1();
        } else {
            zb1 = yb1.a(i2);
        }
        if (zb1 == null) {
            zb1 = new zb1();
            this.b.a(i2, zb1);
        }
        zb1.a(dc1);
        return true;
    }
}
