package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import android.os.Message;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qg0 extends ts0 {
    @DexIgnore
    public /* final */ /* synthetic */ og0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qg0(og0 og0, Looper looper) {
        super(looper);
        this.a = og0;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((pg0) message.obj).a(this.a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GACStateManager", sb.toString());
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
