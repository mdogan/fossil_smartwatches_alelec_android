package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wj3 implements Factory<vj3> {
    @DexIgnore
    public static vj3 a(zj3 zj3, UpdateUser updateUser, or2 or2) {
        return new vj3(zj3, updateUser, or2);
    }
}
