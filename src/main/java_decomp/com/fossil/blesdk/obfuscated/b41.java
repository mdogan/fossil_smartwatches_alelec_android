package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.af0;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b41 implements af0.b<rc1> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationResult a;

    @DexIgnore
    public b41(a41 a41, LocationResult locationResult) {
        this.a = locationResult;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((rc1) obj).onLocationResult(this.a);
    }
}
