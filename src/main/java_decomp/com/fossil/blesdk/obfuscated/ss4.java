package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ss4 implements sr4<qm4, Byte> {
    @DexIgnore
    public static /* final */ ss4 a; // = new ss4();

    @DexIgnore
    public Byte a(qm4 qm4) throws IOException {
        return Byte.valueOf(qm4.F());
    }
}
