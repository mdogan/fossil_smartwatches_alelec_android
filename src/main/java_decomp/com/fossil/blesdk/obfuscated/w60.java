package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w60 extends h70 {
    @DexIgnore
    public String A; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w60(Peripheral peripheral) {
        super(RequestId.READ_MODEL_NUMBER, peripheral);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new j10(GattCharacteristic.CharacteristicId.MODEL_NUMBER, i().h());
    }

    @DexIgnore
    public final String B() {
        return this.A;
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = new String(((j10) bluetoothCommand).j(), nf4.a);
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.MODEL_NUMBER, this.A), 7, (rd4) null));
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.MODEL_NUMBER, this.A);
    }
}
