package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m10 extends BluetoothCommand {
    @DexIgnore
    public int k; // = 23;
    @DexIgnore
    public /* final */ int l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m10(int i, Peripheral.c cVar) {
        super(BluetoothCommandId.REQUEST_MTU, cVar);
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.l = i;
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.a(this.l);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return gattOperationResult instanceof a20;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        this.k = ((a20) gattOperationResult).b();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().j();
    }

    @DexIgnore
    public final int i() {
        return this.k;
    }
}
