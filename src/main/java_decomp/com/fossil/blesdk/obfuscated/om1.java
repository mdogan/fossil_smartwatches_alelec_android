package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class om1 implements Parcelable.Creator<im1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        byte[] bArr = null;
        byte[][] bArr2 = null;
        byte[][] bArr3 = null;
        byte[][] bArr4 = null;
        byte[][] bArr5 = null;
        int[] iArr = null;
        byte[][] bArr6 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 3:
                    bArr = SafeParcelReader.b(parcel, a);
                    break;
                case 4:
                    bArr2 = SafeParcelReader.c(parcel, a);
                    break;
                case 5:
                    bArr3 = SafeParcelReader.c(parcel, a);
                    break;
                case 6:
                    bArr4 = SafeParcelReader.c(parcel, a);
                    break;
                case 7:
                    bArr5 = SafeParcelReader.c(parcel, a);
                    break;
                case 8:
                    iArr = SafeParcelReader.e(parcel, a);
                    break;
                case 9:
                    bArr6 = SafeParcelReader.c(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new im1(str, bArr, bArr2, bArr3, bArr4, bArr5, iArr, bArr6);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new im1[i];
    }
}
