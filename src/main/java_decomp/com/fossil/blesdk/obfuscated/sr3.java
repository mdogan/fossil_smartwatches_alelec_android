package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class sr3 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ AppNotificationFilterSettings e;
    @DexIgnore
    private /* final */ /* synthetic */ String f;

    @DexIgnore
    public /* synthetic */ sr3(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        this.e = appNotificationFilterSettings;
        this.f = str;
    }

    @DexIgnore
    public final void run() {
        PortfolioApp.R.a(this.e, this.f);
    }
}
