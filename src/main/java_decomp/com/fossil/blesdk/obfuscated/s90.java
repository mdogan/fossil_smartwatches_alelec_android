package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.gattserver.command.CommandId;
import com.fossil.blesdk.gattserver.request.ClientRequest;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s90 extends Command {
    @DexIgnore
    public /* final */ sa0<GattOperationResult> n;
    @DexIgnore
    public /* final */ ClientRequest o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ byte[] r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s90(ClientRequest clientRequest, int i, int i2, byte[] bArr, GattServer.a aVar) {
        super(CommandId.SEND_RESPONSE, clientRequest.getDevice$blesdk_productionRelease(), aVar);
        wd4.b(clientRequest, "request");
        wd4.b(aVar, "gattServerOperationCallbackProvider");
        this.o = clientRequest;
        this.p = i;
        this.q = i2;
        this.r = bArr;
    }

    @DexIgnore
    public void a(GattServer gattServer) {
        Command.Result result;
        wd4.b(gattServer, "gattServer");
        if (gattServer.a(this.o.getDevice$blesdk_productionRelease(), this.o.getRequestId$blesdk_productionRelease(), this.p, this.q, this.r)) {
            result = Command.Result.copy$default(f(), (CommandId) null, Command.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (Object) null);
        } else {
            result = Command.Result.copy$default(f(), (CommandId) null, Command.Result.ResultCode.GATT_ERROR, new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (rd4) null), 1, (Object) null);
        }
        a(result);
        a();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return false;
    }

    @DexIgnore
    public sa0<GattOperationResult> g() {
        return this.n;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.REQUEST, this.o.toJSONObject()), JSONKey.STATUS, Integer.valueOf(this.p)), JSONKey.OFFSET, Integer.valueOf(this.q));
        JSONKey jSONKey = JSONKey.VALUE;
        byte[] bArr = this.r;
        String str = null;
        if (bArr != null) {
            str = l90.a(bArr, (String) null, 1, (Object) null);
        }
        return xa0.a(a, jSONKey, str);
    }
}
