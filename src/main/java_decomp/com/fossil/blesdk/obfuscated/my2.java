package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class my2 implements Factory<RemindTimePresenter> {
    @DexIgnore
    public static RemindTimePresenter a(ky2 ky2, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new RemindTimePresenter(ky2, remindersSettingsDatabase);
    }
}
