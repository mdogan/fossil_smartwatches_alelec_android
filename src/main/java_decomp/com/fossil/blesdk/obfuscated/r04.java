package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r04 extends p04 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public r04(Context context, int i, l04 l04) {
        super(context, i, l04);
        this.m = u04.a(context).b();
        if (o == null) {
            o = f24.l(context);
        }
    }

    @DexIgnore
    public f a() {
        return f.NETWORK_MONITOR;
    }

    @DexIgnore
    public void a(String str) {
        this.n = str;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        k24.a(jSONObject, "op", o);
        k24.a(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }
}
