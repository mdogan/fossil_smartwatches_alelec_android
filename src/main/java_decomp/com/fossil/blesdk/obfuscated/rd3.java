package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rd3 extends as2 implements qd3 {
    @DexIgnore
    public ur3<xf2> j;
    @DexIgnore
    public pd3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.e {
        @DexIgnore
        public /* final */ /* synthetic */ rd3 a;

        @DexIgnore
        public b(rd3 rd3) {
            this.a = rd3;
        }

        @DexIgnore
        public final void a(Calendar calendar) {
            pd3 a2 = this.a.k;
            if (a2 != null) {
                wd4.a((Object) calendar, "calendar");
                Date time = calendar.getTime();
                wd4.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.d {
        @DexIgnore
        public /* final */ /* synthetic */ rd3 e;

        @DexIgnore
        public c(rd3 rd3) {
            this.e = rd3;
        }

        @DexIgnore
        public final void a(int i, Calendar calendar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null && calendar != null) {
                SleepDetailActivity.a aVar = SleepDetailActivity.D;
                Date time = calendar.getTime();
                wd4.a((Object) time, "it.time");
                wd4.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onCreateView");
        xf2 xf2 = (xf2) ra.a(layoutInflater, R.layout.fragment_sleep_overview_month, viewGroup, false, O0());
        xf2.q.setEndDate(Calendar.getInstance());
        xf2.q.setOnCalendarMonthChanged(new b(this));
        xf2.q.setOnCalendarItemClickListener(new c(this));
        this.j = new ur3<>(this, xf2);
        ur3<xf2> ur3 = this.j;
        if (ur3 != null) {
            xf2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onResume");
        pd3 pd3 = this.k;
        if (pd3 != null) {
            pd3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onStop");
        pd3 pd3 = this.k;
        if (pd3 != null) {
            pd3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    public View p(int i) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        View view = (View) this.l.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.l.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(TreeMap<Long, Float> treeMap) {
        wd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ur3<xf2> ur3 = this.j;
        if (ur3 != null) {
            xf2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                if (recyclerViewCalendar != null) {
                    recyclerViewCalendar.setTintColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.steps));
                }
            }
        }
        ur3<xf2> ur32 = this.j;
        if (ur32 != null) {
            xf2 a3 = ur32.a();
            if (a3 != null) {
                RecyclerViewCalendar recyclerViewCalendar2 = a3.q;
                if (recyclerViewCalendar2 != null) {
                    recyclerViewCalendar2.setData(treeMap);
                }
            }
        }
        ((RecyclerViewCalendar) p(h62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        wd4.b(date, "selectDate");
        wd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ur3<xf2> ur3 = this.j;
        if (ur3 != null) {
            xf2 a2 = ur3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                wd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                wd4.a((Object) instance2, "startCalendar");
                instance2.setTime(sk2.n(date2));
                wd4.a((Object) instance3, "endCalendar");
                instance3.setTime(sk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
            }
        }
    }

    @DexIgnore
    public void a(pd3 pd3) {
        wd4.b(pd3, "presenter");
        this.k = pd3;
    }
}
