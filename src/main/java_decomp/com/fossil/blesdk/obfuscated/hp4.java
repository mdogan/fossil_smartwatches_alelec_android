package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hp4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public hp4 f;
    @DexIgnore
    public hp4 g;

    @DexIgnore
    public hp4() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    @DexIgnore
    public final hp4 a(hp4 hp4) {
        hp4.g = this;
        hp4.f = this.f;
        this.f.g = hp4;
        this.f = hp4;
        return hp4;
    }

    @DexIgnore
    public final hp4 b() {
        hp4 hp4 = this.f;
        if (hp4 == this) {
            hp4 = null;
        }
        hp4 hp42 = this.g;
        hp42.f = this.f;
        this.f.g = hp42;
        this.f = null;
        this.g = null;
        return hp4;
    }

    @DexIgnore
    public final hp4 c() {
        this.d = true;
        return new hp4(this.a, this.b, this.c, true, false);
    }

    @DexIgnore
    public hp4(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public final hp4 a(int i) {
        hp4 hp4;
        if (i <= 0 || i > this.c - this.b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            hp4 = c();
        } else {
            hp4 = ip4.a();
            System.arraycopy(this.a, this.b, hp4.a, 0, i);
        }
        hp4.c = hp4.b + i;
        this.b += i;
        this.g.a(hp4);
        return hp4;
    }

    @DexIgnore
    public final void a() {
        hp4 hp4 = this.g;
        if (hp4 == this) {
            throw new IllegalStateException();
        } else if (hp4.e) {
            int i = this.c - this.b;
            if (i <= (8192 - hp4.c) + (hp4.d ? 0 : hp4.b)) {
                a(this.g, i);
                b();
                ip4.a(this);
            }
        }
    }

    @DexIgnore
    public final void a(hp4 hp4, int i) {
        if (hp4.e) {
            int i2 = hp4.c;
            if (i2 + i > 8192) {
                if (!hp4.d) {
                    int i3 = hp4.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = hp4.a;
                        System.arraycopy(bArr, i3, bArr, 0, i2 - i3);
                        hp4.c -= hp4.b;
                        hp4.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            System.arraycopy(this.a, this.b, hp4.a, hp4.c, i);
            hp4.c += i;
            this.b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
