package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ks4<T> {
    @DexIgnore
    public ks4(cs4<T> cs4, Throwable th) {
    }

    @DexIgnore
    public static <T> ks4<T> a(Throwable th) {
        if (th != null) {
            return new ks4<>((cs4) null, th);
        }
        throw new NullPointerException("error == null");
    }

    @DexIgnore
    public static <T> ks4<T> a(cs4<T> cs4) {
        if (cs4 != null) {
            return new ks4<>(cs4, (Throwable) null);
        }
        throw new NullPointerException("response == null");
    }
}
