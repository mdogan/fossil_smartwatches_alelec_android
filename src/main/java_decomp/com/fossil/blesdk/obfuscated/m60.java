package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m60 extends h70 {
    @DexIgnore
    public long A;
    @DexIgnore
    public Peripheral.HIDState B; // = Peripheral.HIDState.DISCONNECTED;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m60(Peripheral peripheral, long j) {
        super(RequestId.CONNECT_HID, peripheral);
        wd4.b(peripheral, "peripheral");
        this.A = j;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new e10(i().h());
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public void a(e20 e20) {
        wd4.b(e20, "connectionStateChangedNotification");
    }

    @DexIgnore
    public long m() {
        return this.A;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(super.t(), JSONKey.CURRENT_HID_STATE, i().j().getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, i().k());
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NEW_HID_STATE, this.B.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.B = ((e10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.NEW_HID_STATE, this.B.getLogName$blesdk_productionRelease()), 7, (rd4) null));
    }
}
