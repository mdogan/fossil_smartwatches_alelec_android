package com.fossil.blesdk.obfuscated;

import android.location.Location;
import com.fossil.blesdk.obfuscated.g42;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ g42 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public d(DeviceLocation deviceLocation) {
            wd4.b(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements g42.b {
        @DexIgnore
        public /* final */ /* synthetic */ gr2 a;

        @DexIgnore
        public e(gr2 gr2) {
            this.a = gr2;
        }

        @DexIgnore
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.W.c().e(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            en2.p.a().g().saveDeviceLocation(deviceLocation);
                            this.a.a(new d(deviceLocation));
                        } catch (Exception unused) {
                            this.a.a(new c());
                        }
                        this.a.d().b((g42.b) this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.a.d().b((g42.b) this);
            this.a.a(new c());
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public gr2(g42 g42) {
        wd4.b(g42, "mfLocationService");
        tt1.a(g42, "mfLocationService cannot be null!", new Object[0]);
        wd4.a((Object) g42, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = g42;
    }

    @DexIgnore
    public String c() {
        return "LoadLocation";
    }

    @DexIgnore
    public final g42 d() {
        return this.d;
    }

    @DexIgnore
    public Object a(b bVar, kc4<Object> kc4) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.a((g42.b) new e(this));
        return new Object();
    }
}
