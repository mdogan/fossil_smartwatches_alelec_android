package com.fossil.blesdk.obfuscated;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vh1 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ uh1 b;

    @DexIgnore
    public vh1(uh1 uh1, String str) {
        this.b = uh1;
        ck0.a(str);
        this.a = str;
    }

    @DexIgnore
    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.b.d().s().a(this.a, th);
    }
}
