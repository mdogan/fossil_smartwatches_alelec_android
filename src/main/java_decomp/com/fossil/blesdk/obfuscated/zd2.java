package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NotificationSummaryDialView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zd2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ NotificationSummaryDialView r;

    @DexIgnore
    public zd2(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ImageView imageView, NotificationSummaryDialView notificationSummaryDialView) {
        super(obj, view, i);
        this.q = imageView;
        this.r = notificationSummaryDialView;
    }
}
