package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bq0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bq0> CREATOR; // = new fq0();
    @DexIgnore
    public static /* final */ TimeUnit i; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ fp0 e;
    @DexIgnore
    public /* final */ List<DataSet> f;
    @DexIgnore
    public /* final */ List<DataPoint> g;
    @DexIgnore
    public /* final */ h11 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public fp0 a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<ap0> d; // = new ArrayList();

        @DexIgnore
        public a a(fp0 fp0) {
            this.a = fp0;
            return this;
        }

        @DexIgnore
        public a a(DataSet dataSet) {
            ck0.a(dataSet != null, (Object) "Must specify a valid data set.");
            ap0 J = dataSet.J();
            ck0.b(!this.d.contains(J), "Data set for this data source %s is already added.", J);
            ck0.a(!dataSet.I().isEmpty(), (Object) "No data points specified in the input data set.");
            this.d.add(J);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public bq0 a() {
            boolean z = true;
            ck0.b(this.a != null, "Must specify a valid session.");
            if (this.a.a(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            ck0.b(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet I : this.b) {
                for (DataPoint a2 : I.I()) {
                    a(a2);
                }
            }
            for (DataPoint a3 : this.c) {
                a(a3);
            }
            return new bq0(this);
        }

        @DexIgnore
        public final void a(DataPoint dataPoint) {
            DataPoint dataPoint2 = dataPoint;
            long b2 = this.a.b(TimeUnit.NANOSECONDS);
            long a2 = this.a.a(TimeUnit.NANOSECONDS);
            long c2 = dataPoint2.c(TimeUnit.NANOSECONDS);
            if (c2 != 0) {
                if (c2 < b2 || c2 > a2) {
                    c2 = r11.a(c2, TimeUnit.NANOSECONDS, bq0.i);
                }
                ck0.b(c2 >= b2 && c2 <= a2, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint2, Long.valueOf(b2), Long.valueOf(a2));
                if (dataPoint2.c(TimeUnit.NANOSECONDS) != c2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.c(TimeUnit.NANOSECONDS)), Long.valueOf(c2), bq0.i}));
                    dataPoint2.a(c2, TimeUnit.NANOSECONDS);
                }
            }
            long b3 = this.a.b(TimeUnit.NANOSECONDS);
            long a3 = this.a.a(TimeUnit.NANOSECONDS);
            long b4 = dataPoint2.b(TimeUnit.NANOSECONDS);
            long a4 = dataPoint2.a(TimeUnit.NANOSECONDS);
            if (b4 != 0 && a4 != 0) {
                if (a4 > a3) {
                    a4 = r11.a(a4, TimeUnit.NANOSECONDS, bq0.i);
                }
                ck0.b(b4 >= b3 && a4 <= a3, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint2, Long.valueOf(b3), Long.valueOf(a3));
                if (a4 != dataPoint2.a(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.a(TimeUnit.NANOSECONDS)), Long.valueOf(a4), bq0.i}));
                    dataPoint.a(b4, a4, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public bq0(fp0 fp0, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.e = fp0;
        this.f = Collections.unmodifiableList(list);
        this.g = Collections.unmodifiableList(list2);
        this.h = i11.a(iBinder);
    }

    @DexIgnore
    public List<DataPoint> H() {
        return this.g;
    }

    @DexIgnore
    public List<DataSet> I() {
        return this.f;
    }

    @DexIgnore
    public fp0 J() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof bq0) {
                bq0 bq0 = (bq0) obj;
                if (ak0.a(this.e, bq0.e) && ak0.a(this.f, bq0.f) && ak0.a(this.g, bq0.g)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        ak0.a a2 = ak0.a((Object) this);
        a2.a(Constants.SESSION, this.e);
        a2.a("dataSets", this.f);
        a2.a("aggregateDataPoints", this.g);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) J(), i2, false);
        lk0.b(parcel, 2, I(), false);
        lk0.b(parcel, 3, H(), false);
        h11 h11 = this.h;
        lk0.a(parcel, 4, h11 == null ? null : h11.asBinder(), false);
        lk0.a(parcel, a2);
    }

    @DexIgnore
    public bq0(a aVar) {
        this(aVar.a, (List<DataSet>) aVar.b, (List<DataPoint>) aVar.c, (h11) null);
    }

    @DexIgnore
    public bq0(bq0 bq0, h11 h11) {
        this(bq0.e, bq0.f, bq0.g, h11);
    }

    @DexIgnore
    public bq0(fp0 fp0, List<DataSet> list, List<DataPoint> list2, h11 h11) {
        this.e = fp0;
        this.f = Collections.unmodifiableList(list);
        this.g = Collections.unmodifiableList(list2);
        this.h = h11;
    }
}
