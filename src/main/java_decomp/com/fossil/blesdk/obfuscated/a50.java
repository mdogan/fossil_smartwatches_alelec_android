package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import java.util.HashMap;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a50 {
    @DexIgnore
    public static /* final */ HashMap<Pair<String, FileType>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ a50 b; // = new a50();

    @DexIgnore
    public final short a(String str, FileType fileType) {
        wd4.b(str, "macAddress");
        wd4.b(fileType, "fileType");
        return fileType.getFileHandleMask$blesdk_productionRelease();
    }

    @DexIgnore
    public final short b(String str, FileType fileType) {
        wd4.b(str, "macAddress");
        wd4.b(fileType, "fileType");
        Byte b2 = a.get(new Pair(str, fileType));
        byte byteValue = b2 != null ? b2.byteValue() : 0;
        switch (z40.b[fileType.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return fileType.getFileHandleMask$blesdk_productionRelease();
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                return fileType.getFileHandle$blesdk_productionRelease((byte) 0);
            case 14:
                return fileType.getFileHandle$blesdk_productionRelease((byte) 254);
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                a.put(new Pair(str, fileType), Byte.valueOf((byte) ((o90.b(byteValue) + 1) % o90.b((byte) -1))));
                return fileType.getFileHandle$blesdk_productionRelease(byteValue);
            default:
                throw new NoWhenBranchMatchedException();
        }
    }
}
