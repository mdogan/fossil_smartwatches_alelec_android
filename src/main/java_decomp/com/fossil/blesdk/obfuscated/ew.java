package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.fw;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ew<R> implements fw<R> {
    @DexIgnore
    public static /* final */ ew<?> a; // = new ew<>();
    @DexIgnore
    public static /* final */ gw<?> b; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements gw<R> {
        @DexIgnore
        public fw<R> a(DataSource dataSource, boolean z) {
            return ew.a;
        }
    }

    @DexIgnore
    public static <R> gw<R> a() {
        return b;
    }

    @DexIgnore
    public boolean a(Object obj, fw.a aVar) {
        return false;
    }
}
