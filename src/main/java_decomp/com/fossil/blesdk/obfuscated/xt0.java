package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbi;
import com.google.android.gms.internal.clearcut.zzbn;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xt0 {
    @DexIgnore
    public /* final */ zzbn a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public xt0(int i) {
        this.b = new byte[i];
        this.a = zzbn.a(this.b);
    }

    @DexIgnore
    public /* synthetic */ xt0(int i, ut0 ut0) {
        this(i);
    }

    @DexIgnore
    public final zzbb a() {
        if (this.a.b() == 0) {
            return new zzbi(this.b);
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }

    @DexIgnore
    public final zzbn b() {
        return this.a;
    }
}
