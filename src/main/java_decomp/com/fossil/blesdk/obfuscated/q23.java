package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ms2;
import com.fossil.blesdk.obfuscated.r62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.enums.DirectionBy;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q23 extends as2 implements v23, r62.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<da2> j;
    @DexIgnore
    public u23 k;
    @DexIgnore
    public ms2 l;
    @DexIgnore
    public r62 m;
    @DexIgnore
    public String n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final q23 a() {
            return new q23();
        }

        @DexIgnore
        public final String b() {
            return q23.p;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ms2.b {
        @DexIgnore
        public /* final */ /* synthetic */ q23 a;
        @DexIgnore
        public /* final */ /* synthetic */ da2 b;

        @DexIgnore
        public b(q23 q23, da2 da2) {
            this.a = q23;
            this.b = da2;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "address");
            this.b.q.setText(str, false);
            this.a.U(str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public c(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public d(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public e(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public f(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).m();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ da2 f;

        @DexIgnore
        public g(View view, da2 da2) {
            this.e = view;
            this.f = da2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.y.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                da2 da2 = this.f;
                wd4.a((Object) da2, "binding");
                da2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = q23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;
        @DexIgnore
        public /* final */ /* synthetic */ da2 f;

        @DexIgnore
        public h(q23 q23, da2 da2) {
            this.e = q23;
            this.f = da2;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            r62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    wd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = q23.q.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    String spannableString = fullText.toString();
                    wd4.a((Object) spannableString, "primaryText.toString()");
                    if (!TextUtils.isEmpty(spannableString)) {
                        this.e.U(spannableString);
                        this.f.q.setText(spannableString, false);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;
        @DexIgnore
        public /* final */ /* synthetic */ da2 f;

        @DexIgnore
        public i(q23 q23, da2 da2) {
            this.e = q23;
            this.f = da2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.f.r;
            if (!wd4.a((Object) this.e.T0(), (Object) String.valueOf(editable))) {
                this.e.U((String) null);
            }
            ImageView imageView2 = this.f.r;
            wd4.a((Object) imageView2, "binding.closeIv");
            imageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public j(q23 q23, da2 da2) {
            this.e = q23;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public k(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).b("travel");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public l(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;
        @DexIgnore
        public /* final */ /* synthetic */ da2 f;

        @DexIgnore
        public m(q23 q23, da2 da2) {
            this.e = q23;
            this.f = da2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.f.q.setText("", false);
            q23.b(this.e).h();
            this.e.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public n(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            q23.b(this.e).b("eta");
        }
    }

    /*
    static {
        String simpleName = q23.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ u23 b(q23 q23) {
        u23 u23 = q23.k;
        if (u23 != null) {
            return u23;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void D(String str) {
        wd4.b(str, "userWorkDefaultAddress");
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                th2 th2 = a2.v;
                if (th2 != null) {
                    FlexibleTextView flexibleTextView = th2.r;
                    wd4.a((Object) flexibleTextView, "workButton.ftvContent");
                    flexibleTextView.setText(str);
                    Context context = getContext();
                    if (context != null) {
                        int a3 = (int) us3.a(12, context);
                        th2.t.setPadding(a3, a3, a3, a3);
                        if (!TextUtils.isEmpty(str)) {
                            ImageButton imageButton = th2.t;
                            wd4.a((Object) imageButton, "workButton.ivEditButton");
                            imageButton.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.activeColorPrimary)));
                            th2.t.setImageResource(R.drawable.ic_caret_right);
                            return;
                        }
                        ImageButton imageButton2 = th2.t;
                        wd4.a((Object) imageButton2, "workButton.ivEditButton");
                        imageButton2.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.dianaInactiveTab)));
                        th2.t.setImageResource(R.drawable.ic_caret_right);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L(boolean z) {
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.B;
                if (switchCompat != null) {
                    wd4.a((Object) switchCompat, "it");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        this.n = null;
                        V0();
                        return;
                    }
                }
                U0();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2 != null) goto L_0x0028;
     */
    @DexIgnore
    public boolean S0() {
        String str;
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 == null) {
                return true;
            }
            String str2 = this.n;
            if (str2 != null) {
                if (str2 != null) {
                    str = StringsKt__StringsKt.d(str2).toString();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            str = "";
            String str3 = str;
            ImageView imageView = a2.w;
            wd4.a((Object) imageView, "it.ivArrivalTime");
            String str4 = imageView.getAlpha() == 1.0f ? "eta" : "travel";
            u23 u23 = this.k;
            if (u23 != null) {
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                String obj = appCompatAutoCompleteTextView.getText().toString();
                if (obj != null) {
                    String obj2 = StringsKt__StringsKt.d(obj).toString();
                    DirectionBy directionBy = DirectionBy.CAR;
                    SwitchCompat switchCompat = a2.B;
                    wd4.a((Object) switchCompat, "it.scAvoidTolls");
                    u23.a(str3, obj2, directionBy, switchCompat.isChecked(), str4);
                    return true;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            wd4.d("mPresenter");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final String T0() {
        return this.n;
    }

    @DexIgnore
    public final void U(String str) {
        this.n = str;
    }

    @DexIgnore
    public void U0() {
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                LinearLayout linearLayout = a2.z;
                wd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                th2 th2 = a2.u;
                wd4.a((Object) th2, "it.icHome");
                View d2 = th2.d();
                wd4.a((Object) d2, "it.icHome.root");
                d2.setVisibility(0);
                th2 th22 = a2.v;
                wd4.a((Object) th22, "it.icWork");
                View d3 = th22.d();
                wd4.a((Object) d3, "it.icWork.root");
                d3.setVisibility(0);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void V0() {
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                wd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.z;
                wd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                th2 th2 = a2.u;
                wd4.a((Object) th2, "it.icHome");
                View d2 = th2.d();
                wd4.a((Object) d2, "it.icHome.root");
                d2.setVisibility(8);
                th2 th22 = a2.v;
                wd4.a((Object) th22, "it.icWork");
                View d3 = th22.d();
                wd4.a((Object) d3, "it.icWork.root");
                d3.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(String str, String str2) {
        wd4.b(str, "addressType");
        wd4.b(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                r62 r62 = this.m;
                if (r62 != null) {
                    r62.a((r62.b) null);
                }
                CommuteTimeSettingsDefaultAddressActivity.C.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            r62 r622 = this.m;
            if (r622 != null) {
                r622.a((r62.b) null);
            }
            CommuteTimeSettingsDefaultAddressActivity.C.a(this, bundle);
        }
    }

    @DexIgnore
    public void j(List<String> list) {
        wd4.b(list, "recentSearchedList");
        U0();
        if (!list.isEmpty()) {
            ms2 ms2 = this.l;
            if (ms2 != null) {
                ms2.a((List<String>) wb4.d(list));
            }
            ur3<da2> ur3 = this.j;
            if (ur3 != null) {
                da2 a2 = ur3.a();
                if (a2 != null) {
                    LinearLayout linearLayout = a2.z;
                    if (linearLayout != null) {
                        linearLayout.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        ur3<da2> ur32 = this.j;
        if (ur32 != null) {
            da2 a3 = ur32.a();
            if (a3 != null) {
                LinearLayout linearLayout2 = a3.z;
                if (linearLayout2 != null) {
                    linearLayout2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent == null) {
            return;
        }
        if (i2 == 111) {
            String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
            wd4.a((Object) stringExtra, "homeAddress");
            w(stringExtra);
            u23 u23 = this.k;
            if (u23 != null) {
                u23.a(stringExtra);
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else if (i2 == 112) {
            String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
            wd4.a((Object) stringExtra2, "workAddress");
            D(stringExtra2);
            u23 u232 = this.k;
            if (u232 != null) {
                u232.a(stringExtra2);
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        da2 da2 = (da2) ra.a(layoutInflater, R.layout.fragment_commute_time_settings, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = da2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new h(this, da2));
        appCompatAutoCompleteTextView.addTextChangedListener(new i(this, da2));
        appCompatAutoCompleteTextView.setOnKeyListener(new j(this, da2));
        da2.t.setOnClickListener(new l(this));
        th2 th2 = da2.u;
        th2.u.setImageResource(R.drawable.ic_destination_home);
        FlexibleTextView flexibleTextView = th2.s;
        wd4.a((Object) flexibleTextView, "it.ftvTitle");
        flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Home));
        th2.q.setOnClickListener(new c(this));
        th2.t.setOnClickListener(new d(this));
        th2 th22 = da2.v;
        th22.u.setImageResource(R.drawable.ic_destination_other);
        FlexibleTextView flexibleTextView2 = th22.s;
        wd4.a((Object) flexibleTextView2, "it.ftvTitle");
        flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Other));
        th22.q.setOnClickListener(new e(this));
        th22.t.setOnClickListener(new f(this));
        da2.r.setOnClickListener(new m(this, da2));
        ms2 ms2 = new ms2();
        ms2.a((ms2.b) new b(this, da2));
        this.l = ms2;
        RecyclerView recyclerView = da2.A;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        wd4.a((Object) da2, "binding");
        View d2 = da2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new g(d2, da2));
        da2.w.setOnClickListener(new n(this));
        da2.x.setOnClickListener(new k(this));
        this.j = new ur3<>(this, da2);
        return da2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        u23 u23 = this.k;
        if (u23 != null) {
            u23.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        u23 u23 = this.k;
        if (u23 != null) {
            u23.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            u23 u23 = this.k;
            if (u23 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", u23.i());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void w(String str) {
        wd4.b(str, "userHomeDefaultAddress");
        ur3<da2> ur3 = this.j;
        if (ur3 != null) {
            da2 a2 = ur3.a();
            if (a2 != null) {
                th2 th2 = a2.u;
                if (th2 != null) {
                    FlexibleTextView flexibleTextView = th2.r;
                    wd4.a((Object) flexibleTextView, "homeButton.ftvContent");
                    flexibleTextView.setText(str);
                    Context context = getContext();
                    if (context != null) {
                        int a3 = (int) us3.a(12, context);
                        th2.t.setPadding(a3, a3, a3, a3);
                        if (!TextUtils.isEmpty(str)) {
                            ImageButton imageButton = th2.t;
                            wd4.a((Object) imageButton, "homeButton.ivEditButton");
                            imageButton.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor)));
                            th2.t.setImageResource(R.drawable.ic_caret_right);
                            return;
                        }
                        ImageButton imageButton2 = th2.t;
                        wd4.a((Object) imageButton2, "homeButton.ivEditButton");
                        imageButton2.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.dianaInactiveTab)));
                        th2.t.setImageResource(R.drawable.ic_caret_right);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void x(String str) {
        wd4.b(str, "userCurrentAddress");
        if (isActive()) {
            ur3<da2> ur3 = this.j;
            if (ur3 != null) {
                da2 a2 = ur3.a();
                if (a2 != null) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = appCompatAutoCompleteTextView.getText();
                    wd4.a((Object) text, "it.autocompletePlaces.text");
                    boolean z = true;
                    if (StringsKt__StringsKt.d(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText(str, false);
                            this.n = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(u23 u23) {
        wd4.b(u23, "presenter");
        this.k = u23;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) context, "context!!");
                this.m = new r62(context, placesClient);
                r62 r62 = this.m;
                if (r62 != null) {
                    r62.a((r62.b) this);
                }
                ur3<da2> ur3 = this.j;
                if (ur3 != null) {
                    da2 a2 = ur3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        wd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2, false);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        U0();
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(p, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    ur3<da2> ur3 = this.j;
                    if (ur3 != null) {
                        da2 a2 = ur3.a();
                        if (a2 != null) {
                            a2.w.setAlpha(1.0f);
                            a2.x.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                ur3<da2> ur32 = this.j;
                if (ur32 != null) {
                    da2 a3 = ur32.a();
                    if (a3 != null) {
                        a3.x.setAlpha(1.0f);
                        a3.w.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
        }
    }
}
