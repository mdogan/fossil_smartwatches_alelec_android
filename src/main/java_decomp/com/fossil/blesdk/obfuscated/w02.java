package com.fossil.blesdk.obfuscated;

import java.lang.reflect.AccessibleObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w02 extends x02 {
    @DexIgnore
    public void a(AccessibleObject accessibleObject) {
        accessibleObject.setAccessible(true);
    }
}
