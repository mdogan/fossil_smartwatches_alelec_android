package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ue0;
import com.fossil.blesdk.obfuscated.we0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vh0<A extends ue0<? extends ne0, ee0.b>> extends jg0 {
    @DexIgnore
    public /* final */ A a;

    @DexIgnore
    public vh0(int i, A a2) {
        super(i);
        this.a = a2;
    }

    @DexIgnore
    public final void a(we0.a<?> aVar) throws DeadObjectException {
        try {
            this.a.b(aVar.f());
        } catch (RuntimeException e) {
            a(e);
        }
    }

    @DexIgnore
    public final void a(Status status) {
        this.a.c(status);
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        String simpleName = runtimeException.getClass().getSimpleName();
        String localizedMessage = runtimeException.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.a.c(new Status(10, sb.toString()));
    }

    @DexIgnore
    public final void a(kf0 kf0, boolean z) {
        kf0.a((BasePendingResult<? extends ne0>) this.a, z);
    }
}
