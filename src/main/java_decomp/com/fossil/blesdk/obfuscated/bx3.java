package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.ProtocolException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bx3 implements jp4 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ vo4 g;

    @DexIgnore
    public bx3(int i) {
        this.g = new vo4();
        this.f = i;
    }

    @DexIgnore
    public void a(vo4 vo4, long j) throws IOException {
        if (!this.e) {
            xv3.a(vo4.B(), 0, j);
            if (this.f == -1 || this.g.B() <= ((long) this.f) - j) {
                this.g.a(vo4, j);
                return;
            }
            throw new ProtocolException("exceeded content-length limit of " + this.f + " bytes");
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public lp4 b() {
        return lp4.d;
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.e) {
            this.e = true;
            if (this.g.B() < ((long) this.f)) {
                throw new ProtocolException("content-length promised " + this.f + " bytes, but received " + this.g.B());
            }
        }
    }

    @DexIgnore
    public long f() throws IOException {
        return this.g.B();
    }

    @DexIgnore
    public void flush() throws IOException {
    }

    @DexIgnore
    public bx3() {
        this(-1);
    }

    @DexIgnore
    public void a(jp4 jp4) throws IOException {
        vo4 vo4 = new vo4();
        vo4 vo42 = this.g;
        vo42.a(vo4, 0, vo42.B());
        jp4.a(vo4, vo4.B());
    }
}
