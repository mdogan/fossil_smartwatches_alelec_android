package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g73 implements Factory<SearchMicroAppPresenter> {
    @DexIgnore
    public static SearchMicroAppPresenter a(c73 c73, MicroAppRepository microAppRepository, fn2 fn2, PortfolioApp portfolioApp) {
        return new SearchMicroAppPresenter(c73, microAppRepository, fn2, portfolioApp);
    }
}
