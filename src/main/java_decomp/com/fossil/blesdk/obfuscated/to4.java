package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class to4 extends lp4 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static to4 j;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public to4 f;
    @DexIgnore
    public long g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jp4 {
        @DexIgnore
        public /* final */ /* synthetic */ jp4 e;

        @DexIgnore
        public a(jp4 jp4) {
            this.e = jp4;
        }

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            mp4.a(vo4.f, 0, j);
            while (true) {
                long j2 = 0;
                if (j > 0) {
                    hp4 hp4 = vo4.e;
                    while (true) {
                        if (j2 >= 65536) {
                            break;
                        }
                        j2 += (long) (hp4.c - hp4.b);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        hp4 = hp4.f;
                    }
                    to4.this.g();
                    try {
                        this.e.a(vo4, j2);
                        j -= j2;
                        to4.this.a(true);
                    } catch (IOException e2) {
                        throw to4.this.a(e2);
                    } catch (Throwable th) {
                        to4.this.a(false);
                        throw th;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public lp4 b() {
            return to4.this;
        }

        @DexIgnore
        public void close() throws IOException {
            to4.this.g();
            try {
                this.e.close();
                to4.this.a(true);
            } catch (IOException e2) {
                throw to4.this.a(e2);
            } catch (Throwable th) {
                to4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            to4.this.g();
            try {
                this.e.flush();
                to4.this.a(true);
            } catch (IOException e2) {
                throw to4.this.a(e2);
            } catch (Throwable th) {
                to4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.e + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Thread {
        @DexIgnore
        public c() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1.i();
         */
        @DexIgnore
        public void run() {
            while (true) {
                try {
                    synchronized (to4.class) {
                        to4 j = to4.j();
                        if (j != null) {
                            if (j == to4.j) {
                                to4.j = null;
                                return;
                            }
                        }
                    }
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    @DexIgnore
    public static synchronized void a(to4 to4, long j2, boolean z) {
        Class<to4> cls = to4.class;
        synchronized (cls) {
            if (j == null) {
                j = new to4();
                new c().start();
            }
            long nanoTime = System.nanoTime();
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (i2 != 0 && z) {
                to4.g = Math.min(j2, to4.c() - nanoTime) + nanoTime;
            } else if (i2 != 0) {
                to4.g = j2 + nanoTime;
            } else if (z) {
                to4.g = to4.c();
            } else {
                throw new AssertionError();
            }
            long b2 = to4.b(nanoTime);
            to4 to42 = j;
            while (true) {
                if (to42.f == null) {
                    break;
                } else if (b2 < to42.f.b(nanoTime)) {
                    break;
                } else {
                    to42 = to42.f;
                }
            }
            to4.f = to42.f;
            to42.f = to4;
            if (to42 == j) {
                cls.notify();
            }
        }
    }

    @DexIgnore
    public static to4 j() throws InterruptedException {
        Class<to4> cls = to4.class;
        to4 to4 = j.f;
        if (to4 == null) {
            long nanoTime = System.nanoTime();
            cls.wait(h);
            if (j.f != null || System.nanoTime() - nanoTime < i) {
                return null;
            }
            return j;
        }
        long b2 = to4.b(System.nanoTime());
        if (b2 > 0) {
            long j2 = b2 / 1000000;
            cls.wait(j2, (int) (b2 - (1000000 * j2)));
            return null;
        }
        j.f = to4.f;
        to4.f = null;
        return to4;
    }

    @DexIgnore
    public final long b(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final void g() {
        if (!this.e) {
            long f2 = f();
            boolean d = d();
            if (f2 != 0 || d) {
                this.e = true;
                a(this, f2, d);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit");
    }

    @DexIgnore
    public final boolean h() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return a(this);
    }

    @DexIgnore
    public void i() {
    }

    @DexIgnore
    public IOException b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements kp4 {
        @DexIgnore
        public /* final */ /* synthetic */ kp4 e;

        @DexIgnore
        public b(kp4 kp4) {
            this.e = kp4;
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            to4.this.g();
            try {
                long b = this.e.b(vo4, j);
                to4.this.a(true);
                return b;
            } catch (IOException e2) {
                throw to4.this.a(e2);
            } catch (Throwable th) {
                to4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            try {
                this.e.close();
                to4.this.a(true);
            } catch (IOException e2) {
                throw to4.this.a(e2);
            } catch (Throwable th) {
                to4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.e + ")";
        }

        @DexIgnore
        public lp4 b() {
            return to4.this;
        }
    }

    @DexIgnore
    public static synchronized boolean a(to4 to4) {
        synchronized (to4.class) {
            for (to4 to42 = j; to42 != null; to42 = to42.f) {
                if (to42.f == to4) {
                    to42.f = to4.f;
                    to4.f = null;
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final jp4 a(jp4 jp4) {
        return new a(jp4);
    }

    @DexIgnore
    public final kp4 a(kp4 kp4) {
        return new b(kp4);
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        if (h() && z) {
            throw b((IOException) null);
        }
    }

    @DexIgnore
    public final IOException a(IOException iOException) throws IOException {
        if (!h()) {
            return iOException;
        }
        return b(iOException);
    }
}
