package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wy0 implements Parcelable.Creator<vy0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i = 0;
        int i2 = 0;
        boolean z = true;
        boolean z2 = false;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 3:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 4:
                    i2 = SafeParcelReader.q(parcel, a);
                    break;
                case 5:
                    str2 = SafeParcelReader.f(parcel, a);
                    break;
                case 6:
                    str3 = SafeParcelReader.f(parcel, a);
                    break;
                case 7:
                    z = SafeParcelReader.i(parcel, a);
                    break;
                case 8:
                    str4 = SafeParcelReader.f(parcel, a);
                    break;
                case 9:
                    z2 = SafeParcelReader.i(parcel, a);
                    break;
                case 10:
                    i3 = SafeParcelReader.q(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new vy0(str, i, i2, str2, str3, z, str4, z2, i3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new vy0[i];
    }
}
