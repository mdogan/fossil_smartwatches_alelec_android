package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.fossil.blesdk.obfuscated.p1;
import com.fossil.blesdk.obfuscated.q1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f1 implements p1, AdapterView.OnItemClickListener {
    @DexIgnore
    public Context e;
    @DexIgnore
    public LayoutInflater f;
    @DexIgnore
    public h1 g;
    @DexIgnore
    public ExpandedMenuView h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public p1.a l;
    @DexIgnore
    public a m;
    @DexIgnore
    public int n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BaseAdapter {
        @DexIgnore
        public int e; // = -1;

        @DexIgnore
        public a() {
            a();
        }

        @DexIgnore
        public void a() {
            k1 f2 = f1.this.g.f();
            if (f2 != null) {
                ArrayList<k1> j = f1.this.g.j();
                int size = j.size();
                for (int i = 0; i < size; i++) {
                    if (j.get(i) == f2) {
                        this.e = i;
                        return;
                    }
                }
            }
            this.e = -1;
        }

        @DexIgnore
        public int getCount() {
            int size = f1.this.g.j().size() - f1.this.i;
            return this.e < 0 ? size : size - 1;
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                f1 f1Var = f1.this;
                view = f1Var.f.inflate(f1Var.k, viewGroup, false);
            }
            ((q1.a) view).a(getItem(i), 0);
            return view;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        @DexIgnore
        public k1 getItem(int i) {
            ArrayList<k1> j = f1.this.g.j();
            int i2 = i + f1.this.i;
            int i3 = this.e;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return j.get(i2);
        }
    }

    @DexIgnore
    public f1(Context context, int i2) {
        this(i2, 0);
        this.e = context;
        this.f = LayoutInflater.from(this.e);
    }

    @DexIgnore
    public void a(Context context, h1 h1Var) {
        int i2 = this.j;
        if (i2 != 0) {
            this.e = new ContextThemeWrapper(context, i2);
            this.f = LayoutInflater.from(this.e);
        } else if (this.e != null) {
            this.e = context;
            if (this.f == null) {
                this.f = LayoutInflater.from(this.e);
            }
        }
        this.g = h1Var;
        a aVar = this.m;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        ExpandedMenuView expandedMenuView = this.h;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    public boolean b(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public ListAdapter c() {
        if (this.m == null) {
            this.m = new a();
        }
        return this.m;
    }

    @DexIgnore
    public int getId() {
        return this.n;
    }

    @DexIgnore
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.g.a((MenuItem) this.m.getItem(i2), (p1) this, 0);
    }

    @DexIgnore
    public f1(int i2, int i3) {
        this.k = i2;
        this.j = i3;
    }

    @DexIgnore
    public Parcelable b() {
        if (this.h == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        b(bundle);
        return bundle;
    }

    @DexIgnore
    public q1 a(ViewGroup viewGroup) {
        if (this.h == null) {
            this.h = (ExpandedMenuView) this.f.inflate(x.abc_expanded_menu_layout, viewGroup, false);
            if (this.m == null) {
                this.m = new a();
            }
            this.h.setAdapter(this.m);
            this.h.setOnItemClickListener(this);
        }
        return this.h;
    }

    @DexIgnore
    public void a(boolean z) {
        a aVar = this.m;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(p1.a aVar) {
        this.l = aVar;
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        new i1(v1Var).a((IBinder) null);
        p1.a aVar = this.l;
        if (aVar == null) {
            return true;
        }
        aVar.a(v1Var);
        return true;
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z) {
        p1.a aVar = this.l;
        if (aVar != null) {
            aVar.a(h1Var, z);
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.h.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        a((Bundle) parcelable);
    }
}
