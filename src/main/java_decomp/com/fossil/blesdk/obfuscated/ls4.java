package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ls4<T> extends z84<ks4<T>> {
    @DexIgnore
    public /* final */ z84<cs4<T>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements b94<cs4<R>> {
        @DexIgnore
        public /* final */ b94<? super ks4<R>> e;

        @DexIgnore
        public a(b94<? super ks4<R>> b94) {
            this.e = b94;
        }

        @DexIgnore
        /* renamed from: a */
        public void onNext(cs4<R> cs4) {
            this.e.onNext(ks4.a(cs4));
        }

        @DexIgnore
        public void onComplete() {
            this.e.onComplete();
        }

        @DexIgnore
        public void onError(Throwable th) {
            try {
                this.e.onNext(ks4.a(th));
                this.e.onComplete();
            } catch (Throwable th2) {
                l94.b(th2);
                ta4.b(new CompositeException(th, th2));
            }
        }

        @DexIgnore
        public void onSubscribe(j94 j94) {
            this.e.onSubscribe(j94);
        }
    }

    @DexIgnore
    public ls4(z84<cs4<T>> z84) {
        this.e = z84;
    }

    @DexIgnore
    public void b(b94<? super ks4<T>> b94) {
        this.e.a(new a(b94));
    }
}
