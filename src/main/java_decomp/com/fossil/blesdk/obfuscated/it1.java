package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface it1 {
    @DexIgnore
    void a(int i, int i2);

    @DexIgnore
    void b(int i, int i2);
}
