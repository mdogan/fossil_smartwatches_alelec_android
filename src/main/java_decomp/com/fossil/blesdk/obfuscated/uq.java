package com.fossil.blesdk.obfuscated;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uq {

    @DexIgnore
    public interface a {
        @DexIgnore
        uq build();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(File file);
    }

    @DexIgnore
    File a(ko koVar);

    @DexIgnore
    void a(ko koVar, b bVar);
}
