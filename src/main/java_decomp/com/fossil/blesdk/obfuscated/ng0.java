package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ng0 {
    @DexIgnore
    <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t);

    @DexIgnore
    void a(vd0 vd0, ee0<?> ee0, boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void e(Bundle bundle);

    @DexIgnore
    void f(int i);
}
