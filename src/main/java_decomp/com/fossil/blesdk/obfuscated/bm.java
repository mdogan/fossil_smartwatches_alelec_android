package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bm implements am {
    @DexIgnore
    public /* final */ ul a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            bm.this.b(runnable);
        }
    }

    @DexIgnore
    public bm(Executor executor) {
        this.a = new ul(executor);
    }

    @DexIgnore
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    public void b(Runnable runnable) {
        this.b.post(runnable);
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    public ul b() {
        return this.a;
    }
}
