package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo2 implements MembersInjector<ko2> {
    @DexIgnore
    public static void a(ko2 ko2, DianaNotificationComponent dianaNotificationComponent) {
        ko2.g = dianaNotificationComponent;
    }

    @DexIgnore
    public static void a(ko2 ko2, fn2 fn2) {
        ko2.h = fn2;
    }
}
