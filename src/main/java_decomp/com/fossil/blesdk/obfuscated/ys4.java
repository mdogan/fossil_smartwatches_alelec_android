package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ys4 implements sr4<qm4, Short> {
    @DexIgnore
    public static /* final */ ys4 a; // = new ys4();

    @DexIgnore
    public Short a(qm4 qm4) throws IOException {
        return Short.valueOf(qm4.F());
    }
}
