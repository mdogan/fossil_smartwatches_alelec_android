package com.fossil.blesdk.obfuscated;

import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d90 {
    @DexIgnore
    public static /* final */ Hashtable<String, c90> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ d90 b; // = new d90();

    @DexIgnore
    public final void a(String str, byte[] bArr) {
        wd4.b(str, "macAddress");
        synchronized (a) {
            b.a(str).a(bArr);
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public final void a(String str, byte[] bArr, byte[] bArr2) {
        wd4.b(str, "macAddress");
        wd4.b(bArr, "phoneRandomNumber");
        wd4.b(bArr2, "deviceRandomNumber");
        synchronized (a) {
            b.a(str).a(bArr, bArr2);
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public final c90 a(String str) {
        c90 c90;
        wd4.b(str, "macAddress");
        synchronized (a) {
            c90 = a.get(str);
            if (c90 == null) {
                c90 = new c90();
            }
            a.put(str, c90);
        }
        return c90;
    }
}
