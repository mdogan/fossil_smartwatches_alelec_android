package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.mv;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mv<T extends mv<T>> implements Cloneable {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C; // = true;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f; // = 1.0f;
    @DexIgnore
    public qp g; // = qp.d;
    @DexIgnore
    public Priority h; // = Priority.NORMAL;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public int j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public ko p; // = jw.a();
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public Drawable s;
    @DexIgnore
    public int t;
    @DexIgnore
    public mo u; // = new mo();
    @DexIgnore
    public Map<Class<?>, po<?>> v; // = new mw();
    @DexIgnore
    public Class<?> w; // = Object.class;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public Resources.Theme y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @DexIgnore
    public final float A() {
        return this.f;
    }

    @DexIgnore
    public final Resources.Theme B() {
        return this.y;
    }

    @DexIgnore
    public final Map<Class<?>, po<?>> C() {
        return this.v;
    }

    @DexIgnore
    public final boolean D() {
        return this.D;
    }

    @DexIgnore
    public final boolean E() {
        return this.A;
    }

    @DexIgnore
    public final boolean F() {
        return a(4);
    }

    @DexIgnore
    public final boolean G() {
        return this.m;
    }

    @DexIgnore
    public final boolean H() {
        return a(8);
    }

    @DexIgnore
    public boolean I() {
        return this.C;
    }

    @DexIgnore
    public final boolean J() {
        return a(256);
    }

    @DexIgnore
    public final boolean K() {
        return this.r;
    }

    @DexIgnore
    public final boolean L() {
        return this.q;
    }

    @DexIgnore
    public final boolean M() {
        return a(2048);
    }

    @DexIgnore
    public final boolean N() {
        return vw.b(this.o, this.n);
    }

    @DexIgnore
    public T O() {
        this.x = true;
        S();
        return this;
    }

    @DexIgnore
    public T P() {
        return b(DownsampleStrategy.c, (po<Bitmap>) new us());
    }

    @DexIgnore
    public T Q() {
        return a(DownsampleStrategy.b, (po<Bitmap>) new vs());
    }

    @DexIgnore
    public T R() {
        return a(DownsampleStrategy.a, (po<Bitmap>) new bt());
    }

    @DexIgnore
    public final T S() {
        return this;
    }

    @DexIgnore
    public final T T() {
        if (!this.x) {
            S();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    public T a(float f2) {
        if (this.z) {
            return clone().a(f2);
        }
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.f = f2;
        this.e |= 2;
        T();
        return this;
    }

    @DexIgnore
    public T b(boolean z2) {
        if (this.z) {
            return clone().b(z2);
        }
        this.D = z2;
        this.e |= 1048576;
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T c(DownsampleStrategy downsampleStrategy, po<Bitmap> r3) {
        return a(downsampleStrategy, (po<Bitmap>) r3, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T d(DownsampleStrategy downsampleStrategy, po<Bitmap> r3) {
        if (this.z) {
            return clone().d(downsampleStrategy, r3);
        }
        a(downsampleStrategy);
        return a((po<Bitmap>) r3);
    }

    @DexIgnore
    public final int e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof mv)) {
            return false;
        }
        mv mvVar = (mv) obj;
        if (Float.compare(mvVar.f, this.f) == 0 && this.j == mvVar.j && vw.b((Object) this.i, (Object) mvVar.i) && this.l == mvVar.l && vw.b((Object) this.k, (Object) mvVar.k) && this.t == mvVar.t && vw.b((Object) this.s, (Object) mvVar.s) && this.m == mvVar.m && this.n == mvVar.n && this.o == mvVar.o && this.q == mvVar.q && this.r == mvVar.r && this.A == mvVar.A && this.B == mvVar.B && this.g.equals(mvVar.g) && this.h == mvVar.h && this.u.equals(mvVar.u) && this.v.equals(mvVar.v) && this.w.equals(mvVar.w) && vw.b((Object) this.p, (Object) mvVar.p) && vw.b((Object) this.y, (Object) mvVar.y)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final Drawable f() {
        return this.i;
    }

    @DexIgnore
    public final Drawable g() {
        return this.s;
    }

    @DexIgnore
    public final int h() {
        return this.t;
    }

    @DexIgnore
    public int hashCode() {
        return vw.a((Object) this.y, vw.a((Object) this.p, vw.a((Object) this.w, vw.a((Object) this.v, vw.a((Object) this.u, vw.a((Object) this.h, vw.a((Object) this.g, vw.a(this.B, vw.a(this.A, vw.a(this.r, vw.a(this.q, vw.a(this.o, vw.a(this.n, vw.a(this.m, vw.a((Object) this.s, vw.a(this.t, vw.a((Object) this.k, vw.a(this.l, vw.a((Object) this.i, vw.a(this.j, vw.a(this.f)))))))))))))))))))));
    }

    @DexIgnore
    public final boolean i() {
        return this.B;
    }

    @DexIgnore
    public final mo j() {
        return this.u;
    }

    @DexIgnore
    public final int k() {
        return this.n;
    }

    @DexIgnore
    public final int l() {
        return this.o;
    }

    @DexIgnore
    public final Drawable m() {
        return this.k;
    }

    @DexIgnore
    public final int w() {
        return this.l;
    }

    @DexIgnore
    public final Priority x() {
        return this.h;
    }

    @DexIgnore
    public final Class<?> y() {
        return this.w;
    }

    @DexIgnore
    public final ko z() {
        return this.p;
    }

    @DexIgnore
    public T c() {
        return a(bu.b, true);
    }

    @DexIgnore
    public T clone() {
        try {
            T t2 = (mv) super.clone();
            t2.u = new mo();
            t2.u.a(this.u);
            t2.v = new mw();
            t2.v.putAll(this.v);
            t2.x = false;
            t2.z = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final qp d() {
        return this.g;
    }

    @DexIgnore
    public T b(int i2) {
        if (this.z) {
            return clone().b(i2);
        }
        this.l = i2;
        this.e |= 128;
        this.k = null;
        this.e &= -65;
        T();
        return this;
    }

    @DexIgnore
    public T a(qp qpVar) {
        if (this.z) {
            return clone().a(qpVar);
        }
        uw.a(qpVar);
        this.g = qpVar;
        this.e |= 4;
        T();
        return this;
    }

    @DexIgnore
    public T a(Priority priority) {
        if (this.z) {
            return clone().a(priority);
        }
        uw.a(priority);
        this.h = priority;
        this.e |= 8;
        T();
        return this;
    }

    @DexIgnore
    public T b() {
        return c(DownsampleStrategy.b, new vs());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T b(DownsampleStrategy downsampleStrategy, po<Bitmap> r3) {
        if (this.z) {
            return clone().b(downsampleStrategy, (po<Bitmap>) r3);
        }
        a(downsampleStrategy);
        return a((po<Bitmap>) r3, false);
    }

    @DexIgnore
    public T a(boolean z2) {
        if (this.z) {
            return clone().a(true);
        }
        this.m = !z2;
        this.e |= 256;
        T();
        return this;
    }

    @DexIgnore
    public T a(int i2, int i3) {
        if (this.z) {
            return clone().a(i2, i3);
        }
        this.o = i2;
        this.n = i3;
        this.e |= RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN;
        T();
        return this;
    }

    @DexIgnore
    public T a(ko koVar) {
        if (this.z) {
            return clone().a(koVar);
        }
        uw.a(koVar);
        this.p = koVar;
        this.e |= 1024;
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.lo<Y>, java.lang.Object, com.fossil.blesdk.obfuscated.lo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public <Y> T a(lo<Y> r2, Y y2) {
        if (this.z) {
            return clone().a(r2, y2);
        }
        uw.a(r2);
        uw.a(y2);
        this.u.a(r2, y2);
        T();
        return this;
    }

    @DexIgnore
    public T a(Class<?> cls) {
        if (this.z) {
            return clone().a(cls);
        }
        uw.a(cls);
        this.w = cls;
        this.e |= 4096;
        T();
        return this;
    }

    @DexIgnore
    public T a(DownsampleStrategy downsampleStrategy) {
        lo loVar = DownsampleStrategy.f;
        uw.a(downsampleStrategy);
        return a(loVar, downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(DownsampleStrategy downsampleStrategy, po<Bitmap> r3) {
        return a(downsampleStrategy, (po<Bitmap>) r3, false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(DownsampleStrategy downsampleStrategy, po<Bitmap> r2, boolean z2) {
        T t2;
        if (z2) {
            t2 = d(downsampleStrategy, r2);
        } else {
            t2 = b(downsampleStrategy, (po<Bitmap>) r2);
        }
        t2.C = true;
        return t2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(po<Bitmap> r2) {
        return a((po<Bitmap>) r2, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(po<Bitmap> r3, boolean z2) {
        if (this.z) {
            return clone().a((po<Bitmap>) r3, z2);
        }
        zs zsVar = new zs(r3, z2);
        a(Bitmap.class, r3, z2);
        a(Drawable.class, zsVar, z2);
        zsVar.a();
        a(BitmapDrawable.class, zsVar, z2);
        a(vt.class, new yt(r3), z2);
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<Y>, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 2 */
    public <Y> T a(Class<Y> r2, po<Y> r3, boolean z2) {
        if (this.z) {
            return clone().a(r2, r3, z2);
        }
        uw.a(r2);
        uw.a(r3);
        this.v.put(r2, r3);
        this.e |= 2048;
        this.r = true;
        this.e |= 65536;
        this.C = false;
        if (z2) {
            this.e |= 131072;
            this.q = true;
        }
        T();
        return this;
    }

    @DexIgnore
    public T a(mv<?> mvVar) {
        if (this.z) {
            return clone().a(mvVar);
        }
        if (b(mvVar.e, 2)) {
            this.f = mvVar.f;
        }
        if (b(mvVar.e, 262144)) {
            this.A = mvVar.A;
        }
        if (b(mvVar.e, 1048576)) {
            this.D = mvVar.D;
        }
        if (b(mvVar.e, 4)) {
            this.g = mvVar.g;
        }
        if (b(mvVar.e, 8)) {
            this.h = mvVar.h;
        }
        if (b(mvVar.e, 16)) {
            this.i = mvVar.i;
            this.j = 0;
            this.e &= -33;
        }
        if (b(mvVar.e, 32)) {
            this.j = mvVar.j;
            this.i = null;
            this.e &= -17;
        }
        if (b(mvVar.e, 64)) {
            this.k = mvVar.k;
            this.l = 0;
            this.e &= -129;
        }
        if (b(mvVar.e, 128)) {
            this.l = mvVar.l;
            this.k = null;
            this.e &= -65;
        }
        if (b(mvVar.e, 256)) {
            this.m = mvVar.m;
        }
        if (b(mvVar.e, (int) RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN)) {
            this.o = mvVar.o;
            this.n = mvVar.n;
        }
        if (b(mvVar.e, 1024)) {
            this.p = mvVar.p;
        }
        if (b(mvVar.e, 4096)) {
            this.w = mvVar.w;
        }
        if (b(mvVar.e, 8192)) {
            this.s = mvVar.s;
            this.t = 0;
            this.e &= -16385;
        }
        if (b(mvVar.e, (int) RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE)) {
            this.t = mvVar.t;
            this.s = null;
            this.e &= -8193;
        }
        if (b(mvVar.e, 32768)) {
            this.y = mvVar.y;
        }
        if (b(mvVar.e, 65536)) {
            this.r = mvVar.r;
        }
        if (b(mvVar.e, 131072)) {
            this.q = mvVar.q;
        }
        if (b(mvVar.e, 2048)) {
            this.v.putAll(mvVar.v);
            this.C = mvVar.C;
        }
        if (b(mvVar.e, 524288)) {
            this.B = mvVar.B;
        }
        if (!this.r) {
            this.v.clear();
            this.e &= -2049;
            this.q = false;
            this.e &= -131073;
            this.C = true;
        }
        this.e |= mvVar.e;
        this.u.a(mvVar.u);
        T();
        return this;
    }

    @DexIgnore
    public T a() {
        if (!this.x || this.z) {
            this.z = true;
            return O();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    public final boolean a(int i2) {
        return b(this.e, i2);
    }
}
