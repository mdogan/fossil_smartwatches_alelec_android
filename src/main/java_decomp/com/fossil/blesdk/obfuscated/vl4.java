package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vl4 extends Cloneable {

    @DexIgnore
    public interface a {
        @DexIgnore
        vl4 a(pm4 pm4);
    }

    @DexIgnore
    void a(wl4 wl4);

    @DexIgnore
    void cancel();

    @DexIgnore
    pm4 n();

    @DexIgnore
    boolean o();

    @DexIgnore
    Response r() throws IOException;
}
