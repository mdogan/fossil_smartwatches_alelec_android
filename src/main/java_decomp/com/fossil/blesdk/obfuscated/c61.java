package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c61 extends wb1<c61> {
    @DexIgnore
    public static volatile c61[] e;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public Long d; // = null;

    @DexIgnore
    public c61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static c61[] e() {
        if (e == null) {
            synchronized (ac1.b) {
                if (e == null) {
                    e = new c61[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        Long l = this.d;
        if (l != null) {
            vb1.b(2, l.longValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c61)) {
            return false;
        }
        c61 c61 = (c61) obj;
        Integer num = this.c;
        if (num == null) {
            if (c61.c != null) {
                return false;
            }
        } else if (!num.equals(c61.c)) {
            return false;
        }
        Long l = this.d;
        if (l == null) {
            if (c61.d != null) {
                return false;
            }
        } else if (!l.equals(c61.d)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(c61.b);
        }
        yb1 yb12 = c61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (c61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        Long l = this.d;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        Long l = this.d;
        return l != null ? a + vb1.c(2, l.longValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(ub1.e());
            } else if (c2 == 16) {
                this.d = Long.valueOf(ub1.f());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
