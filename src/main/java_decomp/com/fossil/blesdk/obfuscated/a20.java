package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a20 extends GattOperationResult {
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a20(GattOperationResult.GattResult gattResult, int i) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        this.b = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }
}
