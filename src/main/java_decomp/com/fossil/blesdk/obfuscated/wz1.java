package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wz1<T> {
    @DexIgnore
    T deserialize(JsonElement jsonElement, Type type, vz1 vz1) throws JsonParseException;
}
