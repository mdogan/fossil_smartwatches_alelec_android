package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wl4 {
    @DexIgnore
    void onFailure(vl4 vl4, IOException iOException);

    @DexIgnore
    void onResponse(vl4 vl4, Response response) throws IOException;
}
