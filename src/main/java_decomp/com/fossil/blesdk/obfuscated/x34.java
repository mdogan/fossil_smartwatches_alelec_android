package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class x34 extends Application implements d44, g44, h44, e44, f44 {
    @DexIgnore
    public c44<Activity> e;
    @DexIgnore
    public c44<BroadcastReceiver> f;
    @DexIgnore
    public c44<Service> g;
    @DexIgnore
    public c44<ContentProvider> h;
    @DexIgnore
    public volatile boolean i; // = true;

    @DexIgnore
    public v34<ContentProvider> c() {
        f();
        return this.h;
    }

    @DexIgnore
    public abstract v34<? extends x34> e();

    @DexIgnore
    public final void f() {
        if (this.i) {
            synchronized (this) {
                if (this.i) {
                    e().a(this);
                    if (this.i) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        f();
    }

    @DexIgnore
    public c44<BroadcastReceiver> a() {
        return this.f;
    }

    @DexIgnore
    public c44<Service> b() {
        return this.g;
    }

    @DexIgnore
    public c44<Activity> d() {
        return this.e;
    }
}
