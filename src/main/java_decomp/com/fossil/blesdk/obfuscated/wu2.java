package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.b23;
import com.fossil.blesdk.obfuscated.pv2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wu2 extends bs2 implements e23, View.OnClickListener, b23.d, xs3.g, ls2 {
    @DexIgnore
    public static /* final */ a s; // = new a((rd4) null);
    @DexIgnore
    public d23 k;
    @DexIgnore
    public ConstraintLayout l;
    @DexIgnore
    public RecyclerViewPager m;
    @DexIgnore
    public int n;
    @DexIgnore
    public CustomPageIndicator o;
    @DexIgnore
    public b23 p;
    @DexIgnore
    public ur3<bd2> q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wu2 a() {
            return new wu2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ wu2 a;

        @DexIgnore
        public b(wu2 wu2) {
            this.a = wu2;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            wd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            View childAt = recyclerView.getChildAt(0);
            wd4.a((Object) childAt, "recyclerView.getChildAt(0)");
            int measuredWidth = childAt.getMeasuredWidth();
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "initUI - offset=" + computeHorizontalScrollOffset);
            if (computeHorizontalScrollOffset % measuredWidth == 0) {
                int i3 = computeHorizontalScrollOffset / measuredWidth;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDianaCustomizeFragment", "initUI - position=" + i3);
                if (i3 != -1) {
                    this.a.p(i3);
                    this.a.T0().a(i3);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements pv2.b {
        @DexIgnore
        public /* final */ /* synthetic */ wu2 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(wu2 wu2, String str) {
            this.a = wu2;
            this.b = str;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.T0().a(str, this.b);
            }
        }

        @DexIgnore
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final d23 T0() {
        d23 d23 = this.k;
        if (d23 != null) {
            return d23;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2, String str, int i) {
        List<? extends g8<View, String>> list3 = list;
        String str2 = str;
        wd4.b(list, "views");
        List<? extends g8<CustomizeWidget, String>> list4 = list2;
        wd4.b(list2, "customizeWidgetViews");
        wd4.b(str2, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetComplicationClick preset=");
        sb.append(g13 != null ? g13.a() : null);
        sb.append(" complicationPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (g13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                String c2 = g13.c();
                ArrayList arrayList = new ArrayList(list);
                d23 d23 = this.k;
                if (d23 != null) {
                    aVar.a(activity, c2, arrayList, list2, d23.i(), 1, str, ViewHierarchy.DIMENSION_TOP_KEY);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void c(String str, String str2) {
        wd4.b(str, "presetName");
        wd4.b(str2, "presetId");
        if (getChildFragmentManager().a("RenamePresetDialogFragment") == null) {
            pv2 a2 = pv2.k.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    public void d(int i) {
        if (isActive()) {
            b23 b23 = this.p;
            if (b23 == null) {
                wd4.d("mAdapterDiana");
                throw null;
            } else if (b23.getItemCount() > i) {
                RecyclerViewPager recyclerViewPager = this.m;
                if (recyclerViewPager != null) {
                    recyclerViewPager.i(i);
                } else {
                    wd4.d("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void e(int i) {
        this.n = i;
        b23 b23 = this.p;
        if (b23 == null) {
            wd4.d("mAdapterDiana");
            throw null;
        } else if (b23.getItemCount() > i) {
            RecyclerViewPager recyclerViewPager = this.m;
            if (recyclerViewPager != null) {
                recyclerViewPager.i(i);
            } else {
                wd4.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public int getItemCount() {
        RecyclerViewPager recyclerViewPager = this.m;
        if (recyclerViewPager != null) {
            RecyclerView.g adapter = recyclerViewPager.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        wd4.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public void j() {
        xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_CreateNew_NewPreset_CTA__Set));
        fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public void l() {
        String string = getString(R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        wd4.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        S(string);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void onClick(View view) {
        wd4.b(view, "v");
        if (view.getId() == R.id.ftv_pair_watch) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        bd2 bd2 = (bd2) ra.a(layoutInflater, R.layout.fragment_home_diana_customize, viewGroup, false, O0());
        wd4.a((Object) bd2, "binding");
        a(bd2);
        this.q = new ur3<>(this, bd2);
        ur3<bd2> ur3 = this.q;
        if (ur3 != null) {
            bd2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        d23 d23 = this.k;
        if (d23 != null) {
            if (d23 != null) {
                d23.g();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        wl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        d23 d23 = this.k;
        if (d23 != null) {
            if (d23 != null) {
                d23.f();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        wl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("customize_view");
    }

    @DexIgnore
    public final void p(int i) {
        this.n = i;
    }

    @DexIgnore
    public void v() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showCreateNewSuccessfully");
        CustomPageIndicator customPageIndicator = this.o;
        if (customPageIndicator != null) {
            customPageIndicator.invalidate();
        } else {
            wd4.d("indicator");
            throw null;
        }
    }

    @DexIgnore
    public void y0() {
        d23 d23 = this.k;
        if (d23 != null) {
            d23.j();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void z0() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onAddPresetClick");
        d23 d23 = this.k;
        if (d23 != null) {
            d23.h();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(bd2 bd2) {
        this.p = new b23(new ArrayList(), this);
        ConstraintLayout constraintLayout = bd2.q;
        wd4.a((Object) constraintLayout, "binding.clNoDevice");
        this.l = constraintLayout;
        bd2.u.setImageResource(R.drawable.customization_no_device);
        bd2.s.setOnClickListener(this);
        RecyclerViewPager recyclerViewPager = bd2.v;
        wd4.a((Object) recyclerViewPager, "binding.rvPreset");
        this.m = recyclerViewPager;
        RecyclerViewPager recyclerViewPager2 = this.m;
        if (recyclerViewPager2 != null) {
            recyclerViewPager2.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
            RecyclerViewPager recyclerViewPager3 = this.m;
            if (recyclerViewPager3 != null) {
                b23 b23 = this.p;
                if (b23 != null) {
                    recyclerViewPager3.setAdapter(b23);
                    RecyclerViewPager recyclerViewPager4 = this.m;
                    if (recyclerViewPager4 != null) {
                        recyclerViewPager4.a((RecyclerView.q) new b(this));
                        RecyclerViewPager recyclerViewPager5 = this.m;
                        if (recyclerViewPager5 != null) {
                            recyclerViewPager5.i(this.n);
                            CustomPageIndicator customPageIndicator = bd2.r;
                            wd4.a((Object) customPageIndicator, "binding.cpiPreset");
                            this.o = customPageIndicator;
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(new CustomPageIndicator.a(2, R.drawable.current));
                            arrayList.add(new CustomPageIndicator.a(1, 0, 2, (rd4) null));
                            arrayList.add(new CustomPageIndicator.a(0, R.drawable.add));
                            CustomPageIndicator customPageIndicator2 = this.o;
                            if (customPageIndicator2 != null) {
                                RecyclerViewPager recyclerViewPager6 = this.m;
                                if (recyclerViewPager6 != null) {
                                    customPageIndicator2.a((RecyclerView) recyclerViewPager6, this.n, (List<CustomPageIndicator.a>) arrayList);
                                    bd2.t.setOnClickListener(this);
                                    return;
                                }
                                wd4.d("rvCustomize");
                                throw null;
                            }
                            wd4.d("indicator");
                            throw null;
                        }
                        wd4.d("rvCustomize");
                        throw null;
                    }
                    wd4.d("rvCustomize");
                    throw null;
                }
                wd4.d("mAdapterDiana");
                throw null;
            }
            wd4.d("rvCustomize");
            throw null;
        }
        wd4.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public void b(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2) {
        wd4.b(list, "views");
        wd4.b(list2, "customizeWidgetViews");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onEditThemeClick preset=");
        sb.append(g13 != null ? g13.a() : null);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        d23 d23 = this.k;
        if (d23 != null) {
            d23.a(g13, list, list2);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d(boolean z) {
        if (z) {
            ur3<bd2> ur3 = this.q;
            if (ur3 != null) {
                bd2 a2 = ur3.a();
                if (a2 != null) {
                    TextView textView = a2.w;
                    wd4.a((Object) textView, "tvTapIconToCustomize");
                    textView.setVisibility(0);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        ur3<bd2> ur32 = this.q;
        if (ur32 != null) {
            bd2 a3 = ur32.a();
            if (a3 != null) {
                TextView textView2 = a3.w;
                wd4.a((Object) textView2, "tvTapIconToCustomize");
                textView2.setVisibility(4);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(boolean z, String str, String str2, String str3) {
        wd4.b(str, "currentPresetName");
        wd4.b(str2, "nextPresetName");
        wd4.b(str3, "nextPresetId");
        isActive();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String string = activity.getString(R.string.Customization_Delete_Confirm_Text__DeletingAPresetIsPermanentAnd);
            wd4.a((Object) string, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    String string2 = activity2.getString(R.string.Customization_Delete_CurrentPreset_Text__DeletingAPresetIsPermanentAnd);
                    wd4.a((Object) string2, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
                    be4 be4 = be4.a;
                    Object[] objArr = {cg4.d(str2)};
                    string = String.format(string2, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) string, "java.lang.String.format(format, *args)");
                } else {
                    wd4.a();
                    throw null;
                }
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_two_action_with_title);
            be4 be42 = be4.a;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_CurrentPreset_Title__DeletePresetName);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            Object[] objArr2 = {str};
            String format = String.format(a2, Arrays.copyOf(objArr2, objArr2.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a((int) R.id.tv_title, format);
            fVar.a((int) R.id.tv_description, string);
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_Confirm_CTA__Delete));
            fVar.a((int) R.id.tv_cancel, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_Confirm_CTA__Cancel));
            fVar.a((int) R.id.tv_ok);
            fVar.a((int) R.id.tv_cancel);
            fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void c(List<g13> list) {
        wd4.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size());
        b23 b23 = this.p;
        if (b23 != null) {
            b23.a(list);
            RecyclerViewPager recyclerViewPager = this.m;
            if (recyclerViewPager != null) {
                recyclerViewPager.i(this.n);
            } else {
                wd4.d("rvCustomize");
                throw null;
            }
        } else {
            wd4.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    public void c(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i);
        this.n = i;
        b23 b23 = this.p;
        if (b23 != null) {
            if (b23.getItemCount() > i) {
                RecyclerViewPager recyclerViewPager = this.m;
                if (recyclerViewPager != null) {
                    recyclerViewPager.i(i);
                } else {
                    wd4.d("rvCustomize");
                    throw null;
                }
            }
            CustomPageIndicator customPageIndicator = this.o;
            if (customPageIndicator != null) {
                customPageIndicator.invalidate();
            } else {
                wd4.d("indicator");
                throw null;
            }
        } else {
            wd4.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    public void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2) {
        wd4.b(list, "views");
        wd4.b(list2, "customizeWidgetViews");
        if (g13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                String c2 = g13.c();
                ArrayList arrayList = new ArrayList(list);
                d23 d23 = this.k;
                if (d23 != null) {
                    aVar.a(activity, c2, arrayList, list2, d23.i(), 0, ViewHierarchy.DIMENSION_TOP_KEY, ViewHierarchy.DIMENSION_TOP_KEY);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2, String str, int i) {
        List<? extends g8<View, String>> list3 = list;
        String str2 = str;
        wd4.b(list, "views");
        List<? extends g8<CustomizeWidget, String>> list4 = list2;
        wd4.b(list2, "customizeWidgetViews");
        wd4.b(str2, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(g13 != null ? g13.a() : null);
        sb.append(" watchAppPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (g13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                String c2 = g13.c();
                ArrayList arrayList = new ArrayList(list);
                d23 d23 = this.k;
                if (d23 != null) {
                    aVar.a(activity, c2, arrayList, list2, d23.i(), 2, ViewHierarchy.DIMENSION_TOP_KEY, str);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        String str2;
        wd4.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i == R.id.tv_ok) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                wd4.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
            } else {
                str2 = "";
            }
            d23 d23 = this.k;
            if (d23 != null) {
                d23.a(str2);
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            ConstraintLayout constraintLayout = this.l;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                wd4.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.l;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                wd4.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(d23 d23) {
        wd4.b(d23, "presenter");
        this.k = d23;
    }
}
