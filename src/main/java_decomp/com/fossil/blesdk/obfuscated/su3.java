package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.iv3;
import com.fossil.blesdk.obfuscated.kv3;
import com.fossil.blesdk.obfuscated.ov3;
import com.squareup.okhttp.Protocol;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class su3 {
    @DexIgnore
    public /* final */ rv3 a;
    @DexIgnore
    public /* final */ ov3 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements rv3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public kv3 a(iv3 iv3) throws IOException {
            return su3.this.a(iv3);
        }

        @DexIgnore
        public void b(iv3 iv3) throws IOException {
            su3.this.b(iv3);
        }

        @DexIgnore
        public pw3 a(kv3 kv3) throws IOException {
            return su3.this.a(kv3);
        }

        @DexIgnore
        public void a(kv3 kv3, kv3 kv32) throws IOException {
            su3.this.a(kv3, kv32);
        }

        @DexIgnore
        public void a() {
            su3.this.a();
        }

        @DexIgnore
        public void a(qw3 qw3) {
            su3.this.a(qw3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends lv3 {
        @DexIgnore
        public /* final */ ov3.f e;
        @DexIgnore
        public /* final */ xo4 f;
        @DexIgnore
        public /* final */ String g;
        @DexIgnore
        public /* final */ String h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ap4 {
            @DexIgnore
            public /* final */ /* synthetic */ ov3.f f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, kp4 kp4, ov3.f fVar) {
                super(kp4);
                this.f = fVar;
            }

            @DexIgnore
            public void close() throws IOException {
                this.f.close();
                super.close();
            }
        }

        @DexIgnore
        public c(ov3.f fVar, String str, String str2) {
            this.e = fVar;
            this.g = str;
            this.h = str2;
            this.f = ep4.a((kp4) new a(this, fVar.b(1), fVar));
        }

        @DexIgnore
        public gv3 A() {
            String str = this.g;
            if (str != null) {
                return gv3.a(str);
            }
            return null;
        }

        @DexIgnore
        public xo4 B() {
            return this.f;
        }

        @DexIgnore
        public long z() {
            try {
                if (this.h != null) {
                    return Long.parseLong(this.h);
                }
                return -1;
            } catch (NumberFormatException unused) {
                return -1;
            }
        }
    }

    @DexIgnore
    public su3(File file, long j) {
        this(file, j, fx3.a);
    }

    @DexIgnore
    public static /* synthetic */ int b(su3 su3) {
        int i = su3.c;
        su3.c = i + 1;
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int c(su3 su3) {
        int i = su3.d;
        su3.d = i + 1;
        return i;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements pw3 {
        @DexIgnore
        public /* final */ ov3.d a;
        @DexIgnore
        public jp4 b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public jp4 d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends zo4 {
            @DexIgnore
            public /* final */ /* synthetic */ ov3.d f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(jp4 jp4, su3 su3, ov3.d dVar) {
                super(jp4);
                this.f = dVar;
            }

            @DexIgnore
            public void close() throws IOException {
                synchronized (su3.this) {
                    if (!b.this.c) {
                        boolean unused = b.this.c = true;
                        su3.b(su3.this);
                        super.close();
                        this.f.b();
                    }
                }
            }
        }

        @DexIgnore
        public b(ov3.d dVar) throws IOException {
            this.a = dVar;
            this.b = dVar.a(1);
            this.d = new a(this.b, su3.this, dVar);
        }

        @DexIgnore
        public void abort() {
            synchronized (su3.this) {
                if (!this.c) {
                    this.c = true;
                    su3.c(su3.this);
                    xv3.a((Closeable) this.b);
                    try {
                        this.a.a();
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexIgnore
        public jp4 a() {
            return this.d;
        }
    }

    @DexIgnore
    public su3(File file, long j, fx3 fx3) {
        this.a = new a();
        this.b = ov3.a(fx3, file, 201105, 2, j);
    }

    @DexIgnore
    public static String c(iv3 iv3) {
        return xv3.a(iv3.i());
    }

    @DexIgnore
    public final void b(iv3 iv3) throws IOException {
        this.b.h(c(iv3));
    }

    @DexIgnore
    public static int b(xo4 xo4) throws IOException {
        try {
            long h = xo4.h();
            String i = xo4.i();
            if (h >= 0 && h <= 2147483647L && i.isEmpty()) {
                return (int) h;
            }
            throw new IOException("expected an int but was \"" + h + i + "\"");
        } catch (NumberFormatException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    @DexIgnore
    public kv3 a(iv3 iv3) {
        try {
            ov3.f f2 = this.b.f(c(iv3));
            if (f2 == null) {
                return null;
            }
            try {
                d dVar = new d(f2.b(0));
                kv3 a2 = dVar.a(iv3, f2);
                if (dVar.a(iv3, a2)) {
                    return a2;
                }
                xv3.a((Closeable) a2.a());
                return null;
            } catch (IOException unused) {
                xv3.a((Closeable) f2);
                return null;
            }
        } catch (IOException unused2) {
            return null;
        }
    }

    @DexIgnore
    public final pw3 a(kv3 kv3) throws IOException {
        ov3.d dVar;
        String f2 = kv3.l().f();
        if (ww3.a(kv3.l().f())) {
            try {
                b(kv3.l());
            } catch (IOException unused) {
            }
            return null;
        } else if (!f2.equals("GET") || yw3.b(kv3)) {
            return null;
        } else {
            d dVar2 = new d(kv3);
            try {
                dVar = this.b.e(c(kv3.l()));
                if (dVar == null) {
                    return null;
                }
                try {
                    dVar2.a(dVar);
                    return new b(dVar);
                } catch (IOException unused2) {
                    a(dVar);
                    return null;
                }
            } catch (IOException unused3) {
                dVar = null;
                a(dVar);
                return null;
            }
        }
    }

    @DexIgnore
    public final void a(kv3 kv3, kv3 kv32) {
        ov3.d dVar;
        d dVar2 = new d(kv32);
        try {
            dVar = ((c) kv3.a()).e.y();
            if (dVar != null) {
                try {
                    dVar2.a(dVar);
                    dVar.b();
                } catch (IOException unused) {
                }
            }
        } catch (IOException unused2) {
            dVar = null;
            a(dVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ dv3 b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ Protocol d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ dv3 g;
        @DexIgnore
        public /* final */ cv3 h;

        @DexIgnore
        public d(kp4 kp4) throws IOException {
            try {
                xo4 a2 = ep4.a(kp4);
                this.a = a2.i();
                this.c = a2.i();
                dv3.b bVar = new dv3.b();
                int a3 = su3.b(a2);
                for (int i = 0; i < a3; i++) {
                    bVar.a(a2.i());
                }
                this.b = bVar.a();
                dx3 a4 = dx3.a(a2.i());
                this.d = a4.a;
                this.e = a4.b;
                this.f = a4.c;
                dv3.b bVar2 = new dv3.b();
                int a5 = su3.b(a2);
                for (int i2 = 0; i2 < a5; i2++) {
                    bVar2.a(a2.i());
                }
                this.g = bVar2.a();
                if (a()) {
                    String i3 = a2.i();
                    if (i3.length() <= 0) {
                        this.h = cv3.a(a2.i(), a(a2), a(a2));
                    } else {
                        throw new IOException("expected \"\" but was \"" + i3 + "\"");
                    }
                } else {
                    this.h = null;
                }
            } finally {
                kp4.close();
            }
        }

        @DexIgnore
        public void a(ov3.d dVar) throws IOException {
            wo4 a2 = ep4.a(dVar.a(0));
            a2.a(this.a);
            a2.writeByte(10);
            a2.a(this.c);
            a2.writeByte(10);
            a2.b((long) this.b.b());
            a2.writeByte(10);
            int b2 = this.b.b();
            for (int i = 0; i < b2; i++) {
                a2.a(this.b.a(i));
                a2.a(": ");
                a2.a(this.b.b(i));
                a2.writeByte(10);
            }
            a2.a(new dx3(this.d, this.e, this.f).toString());
            a2.writeByte(10);
            a2.b((long) this.g.b());
            a2.writeByte(10);
            int b3 = this.g.b();
            for (int i2 = 0; i2 < b3; i2++) {
                a2.a(this.g.a(i2));
                a2.a(": ");
                a2.a(this.g.b(i2));
                a2.writeByte(10);
            }
            if (a()) {
                a2.writeByte(10);
                a2.a(this.h.a());
                a2.writeByte(10);
                a(a2, this.h.c());
                a(a2, this.h.b());
            }
            a2.close();
        }

        @DexIgnore
        public d(kv3 kv3) {
            this.a = kv3.l().i();
            this.b = yw3.d(kv3);
            this.c = kv3.l().f();
            this.d = kv3.k();
            this.e = kv3.e();
            this.f = kv3.h();
            this.g = kv3.g();
            this.h = kv3.f();
        }

        @DexIgnore
        public final boolean a() {
            return this.a.startsWith("https://");
        }

        @DexIgnore
        public final List<Certificate> a(xo4 xo4) throws IOException {
            int a2 = su3.b(xo4);
            if (a2 == -1) {
                return Collections.emptyList();
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(a2);
                for (int i = 0; i < a2; i++) {
                    String i2 = xo4.i();
                    vo4 vo4 = new vo4();
                    vo4.a(ByteString.decodeBase64(i2));
                    arrayList.add(instance.generateCertificate(vo4.m()));
                }
                return arrayList;
            } catch (CertificateException e2) {
                throw new IOException(e2.getMessage());
            }
        }

        @DexIgnore
        public final void a(wo4 wo4, List<Certificate> list) throws IOException {
            try {
                wo4.b((long) list.size());
                wo4.writeByte(10);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    wo4.a(ByteString.of(list.get(i).getEncoded()).base64());
                    wo4.writeByte(10);
                }
            } catch (CertificateEncodingException e2) {
                throw new IOException(e2.getMessage());
            }
        }

        @DexIgnore
        public boolean a(iv3 iv3, kv3 kv3) {
            return this.a.equals(iv3.i()) && this.c.equals(iv3.f()) && yw3.a(kv3, this.b, iv3);
        }

        @DexIgnore
        public kv3 a(iv3 iv3, ov3.f fVar) {
            String a2 = this.g.a(GraphRequest.CONTENT_TYPE_HEADER);
            String a3 = this.g.a("Content-Length");
            iv3.b bVar = new iv3.b();
            bVar.b(this.a);
            bVar.a(this.c, (jv3) null);
            bVar.a(this.b);
            iv3 a4 = bVar.a();
            kv3.b bVar2 = new kv3.b();
            bVar2.a(a4);
            bVar2.a(this.d);
            bVar2.a(this.e);
            bVar2.a(this.f);
            bVar2.a(this.g);
            bVar2.a((lv3) new c(fVar, a2, a3));
            bVar2.a(this.h);
            return bVar2.a();
        }
    }

    @DexIgnore
    public final void a(ov3.d dVar) {
        if (dVar != null) {
            try {
                dVar.a();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public final synchronized void a(qw3 qw3) {
        this.g++;
        if (qw3.a != null) {
            this.e++;
        } else if (qw3.b != null) {
            this.f++;
        }
    }

    @DexIgnore
    public final synchronized void a() {
        this.f++;
    }
}
