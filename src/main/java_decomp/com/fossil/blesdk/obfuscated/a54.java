package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import retrofit.mime.MultipartTypedOutput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a54 extends w44<Boolean> {
    @DexIgnore
    public /* final */ a74 k; // = new z64();
    @DexIgnore
    public PackageManager l;
    @DexIgnore
    public String m;
    @DexIgnore
    public PackageInfo n;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ Future<Map<String, y44>> t;
    @DexIgnore
    public /* final */ Collection<w44> u;

    @DexIgnore
    public a54(Future<Map<String, y44>> future, Collection<w44> collection) {
        this.t = future;
        this.u = collection;
    }

    @DexIgnore
    public Map<String, y44> a(Map<String, y44> map, Collection<w44> collection) {
        for (w44 next : collection) {
            if (!map.containsKey(next.p())) {
                map.put(next.p(), new y44(next.p(), next.r(), MultipartTypedOutput.DEFAULT_TRANSFER_ENCODING));
            }
        }
        return map;
    }

    @DexIgnore
    public final boolean b(String str, n74 n74, Collection<y44> collection) {
        return new r74(this, v(), n74.b, this.k).a(a(x74.a(l(), str), collection));
    }

    @DexIgnore
    public final boolean c(String str, n74 n74, Collection<y44> collection) {
        return a(n74, x74.a(l(), str), collection);
    }

    @DexIgnore
    public String p() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String r() {
        return "1.4.8.32";
    }

    @DexIgnore
    public boolean u() {
        try {
            this.q = o().g();
            this.l = l().getPackageManager();
            this.m = l().getPackageName();
            this.n = this.l.getPackageInfo(this.m, 0);
            this.o = Integer.toString(this.n.versionCode);
            this.p = this.n.versionName == null ? "0.0" : this.n.versionName;
            this.r = this.l.getApplicationLabel(l().getApplicationInfo()).toString();
            this.s = Integer.toString(l().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            r44.g().e("Fabric", "Failed init", e);
            return false;
        }
    }

    @DexIgnore
    public String v() {
        return CommonUtils.b(l(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public final c84 w() {
        try {
            a84 d = a84.d();
            d.a(this, this.i, this.k, this.o, this.p, v(), p54.a(l()));
            d.b();
            return a84.d().a();
        } catch (Exception e) {
            r44.g().e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    @DexIgnore
    public Boolean k() {
        boolean z;
        Map map;
        String c = CommonUtils.c(l());
        c84 w = w();
        if (w != null) {
            try {
                if (this.t != null) {
                    map = this.t.get();
                } else {
                    map = new HashMap();
                }
                a((Map<String, y44>) map, this.u);
                z = a(c, w.a, (Collection<y44>) map.values());
            } catch (Exception e) {
                r44.g().e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final boolean a(String str, n74 n74, Collection<y44> collection) {
        if ("new".equals(n74.a)) {
            if (b(str, n74, collection)) {
                return a84.d().c();
            }
            r44.g().e("Fabric", "Failed to create app with Crashlytics service.", (Throwable) null);
            return false;
        } else if ("configured".equals(n74.a)) {
            return a84.d().c();
        } else {
            if (n74.e) {
                r44.g().d("Fabric", "Server says an update is required - forcing a full App update.");
                c(str, n74, collection);
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean a(n74 n74, x74 x74, Collection<y44> collection) {
        return new h84(this, v(), n74.b, this.k).a(a(x74, collection));
    }

    @DexIgnore
    public final m74 a(x74 x74, Collection<y44> collection) {
        Context l2 = l();
        return new m74(new l54().d(l2), o().d(), this.p, this.o, CommonUtils.a(CommonUtils.n(l2)), this.r, DeliveryMechanism.determineFrom(this.q).getId(), this.s, "0", x74, collection);
    }
}
