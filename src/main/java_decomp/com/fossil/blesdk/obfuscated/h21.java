package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h21 {
    @DexIgnore
    public static <DP, DT> String a(DP dp, f21<DP, DT> f21) {
        double d;
        g21<DT> zzb = f21.zzb();
        if (!zzb.a(f21.zzb(dp))) {
            return null;
        }
        DT zza = f21.zza(dp);
        for (int i = 0; i < zzb.zzc(zza); i++) {
            String b = zzb.b(zza, i);
            if (f21.c(dp, i)) {
                double c = (double) zzb.c(zza, i);
                if (c == 1.0d) {
                    d = (double) f21.b(dp, i);
                } else if (c == 2.0d) {
                    d = f21.a(dp, i);
                } else {
                    continue;
                }
                k21 a = i21.a().a(b);
                if (a != null && !a.a(d)) {
                    return "Field out of range";
                }
                k21 a2 = i21.a().a(zzb.zzd(zza), b);
                if (a2 != null) {
                    long a3 = f21.a(dp, TimeUnit.NANOSECONDS);
                    if (a3 == 0) {
                        if (d == 0.0d) {
                            return null;
                        }
                        return "DataPoint out of range";
                    } else if (!a2.a(d / ((double) a3))) {
                        return "DataPoint out of range";
                    }
                } else {
                    continue;
                }
            } else if (!zzb.a(zza, i) && !i21.g.contains(b)) {
                return String.valueOf(b).concat(" not set");
            }
        }
        return null;
    }
}
