package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class le2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ DashBar s;
    @DexIgnore
    public /* final */ RulerValuePicker t;
    @DexIgnore
    public /* final */ RulerValuePicker u;
    @DexIgnore
    public /* final */ TabLayout v;
    @DexIgnore
    public /* final */ TabLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public le2(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, DashBar dashBar, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ScrollView scrollView, TabLayout tabLayout, TabLayout tabLayout2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = dashBar;
        this.t = rulerValuePicker;
        this.u = rulerValuePicker2;
        this.v = tabLayout;
        this.w = tabLayout2;
        this.x = flexibleTextView4;
    }
}
