package com.fossil.blesdk.obfuscated;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g22 implements s12 {
    @DexIgnore
    public c22 a(String str, BarcodeFormat barcodeFormat, int i, int i2, Map<EncodeHintType, ?> map) {
        p12 p12;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (barcodeFormat != BarcodeFormat.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + barcodeFormat);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            SymbolShapeHint symbolShapeHint = SymbolShapeHint.FORCE_NONE;
            p12 p122 = null;
            if (map != null) {
                SymbolShapeHint symbolShapeHint2 = (SymbolShapeHint) map.get(EncodeHintType.DATA_MATRIX_SHAPE);
                if (symbolShapeHint2 != null) {
                    symbolShapeHint = symbolShapeHint2;
                }
                p12 = (p12) map.get(EncodeHintType.MIN_SIZE);
                if (p12 == null) {
                    p12 = null;
                }
                p12 p123 = (p12) map.get(EncodeHintType.MAX_SIZE);
                if (p123 != null) {
                    p122 = p123;
                }
            } else {
                p12 = null;
            }
            String a = q22.a(str, symbolShapeHint, p12, p122);
            r22 a2 = r22.a(a.length(), symbolShapeHint, p12, p122, true);
            l22 l22 = new l22(p22.a(a, a2), a2.f(), a2.e());
            l22.a();
            return a(l22, a2);
        }
    }

    @DexIgnore
    public static c22 a(l22 l22, r22 r22) {
        int f = r22.f();
        int e = r22.e();
        x32 x32 = new x32(r22.h(), r22.g());
        int i = 0;
        for (int i2 = 0; i2 < e; i2++) {
            if (i2 % r22.e == 0) {
                int i3 = 0;
                for (int i4 = 0; i4 < r22.h(); i4++) {
                    x32.a(i3, i, i4 % 2 == 0);
                    i3++;
                }
                i++;
            }
            int i5 = 0;
            for (int i6 = 0; i6 < f; i6++) {
                if (i6 % r22.d == 0) {
                    x32.a(i5, i, true);
                    i5++;
                }
                x32.a(i5, i, l22.a(i6, i2));
                i5++;
                int i7 = r22.d;
                if (i6 % i7 == i7 - 1) {
                    x32.a(i5, i, i2 % 2 == 0);
                    i5++;
                }
            }
            i++;
            int i8 = r22.e;
            if (i2 % i8 == i8 - 1) {
                int i9 = 0;
                for (int i10 = 0; i10 < r22.h(); i10++) {
                    x32.a(i9, i, true);
                    i9++;
                }
                i++;
            }
        }
        return a(x32);
    }

    @DexIgnore
    public static c22 a(x32 x32) {
        int c = x32.c();
        int b = x32.b();
        c22 c22 = new c22(c, b);
        c22.a();
        for (int i = 0; i < c; i++) {
            for (int i2 = 0; i2 < b; i2++) {
                if (x32.a(i, i2) == 1) {
                    c22.b(i, i2);
                }
            }
        }
        return c22;
    }
}
