package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.fossil.blesdk.obfuscated.ig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ng implements ig {
    @DexIgnore
    public /* final */ a a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ mg[] e;
        @DexIgnore
        public /* final */ ig.a f;
        @DexIgnore
        public boolean g;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ng$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ng$a$a  reason: collision with other inner class name */
        public class C0022a implements DatabaseErrorHandler {
            @DexIgnore
            public /* final */ /* synthetic */ ig.a a;
            @DexIgnore
            public /* final */ /* synthetic */ mg[] b;

            @DexIgnore
            public C0022a(ig.a aVar, mg[] mgVarArr) {
                this.a = aVar;
                this.b = mgVarArr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.b(a.a(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public a(Context context, String str, mg[] mgVarArr, ig.a aVar) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, aVar.a, new C0022a(aVar, mgVarArr));
            this.f = aVar;
            this.e = mgVarArr;
        }

        @DexIgnore
        public mg a(SQLiteDatabase sQLiteDatabase) {
            return a(this.e, sQLiteDatabase);
        }

        @DexIgnore
        public synchronized void close() {
            super.close();
            this.e[0] = null;
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.f.a((hg) a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.f.c(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g = true;
            this.f.a(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.g) {
                this.f.d(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g = true;
            this.f.b(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public synchronized hg y() {
            this.g = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.g) {
                close();
                return y();
            }
            return a(writableDatabase);
        }

        @DexIgnore
        public static mg a(mg[] mgVarArr, SQLiteDatabase sQLiteDatabase) {
            mg mgVar = mgVarArr[0];
            if (mgVar == null || !mgVar.a(sQLiteDatabase)) {
                mgVarArr[0] = new mg(sQLiteDatabase);
            }
            return mgVarArr[0];
        }
    }

    @DexIgnore
    public ng(Context context, String str, ig.a aVar) {
        this.a = a(context, str, aVar);
    }

    @DexIgnore
    public final a a(Context context, String str, ig.a aVar) {
        return new a(context, str, new mg[1], aVar);
    }

    @DexIgnore
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }

    @DexIgnore
    public void a(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }

    @DexIgnore
    public hg a() {
        return this.a.y();
    }
}
