package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.np;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fq implements np, to.a<Object>, np.a {
    @DexIgnore
    public /* final */ op<?> e;
    @DexIgnore
    public /* final */ np.a f;
    @DexIgnore
    public int g;
    @DexIgnore
    public kp h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public volatile tr.a<?> j;
    @DexIgnore
    public lp k;

    @DexIgnore
    public fq(op<?> opVar, np.a aVar) {
        this.e = opVar;
        this.f = aVar;
    }

    @DexIgnore
    public boolean a() {
        Object obj = this.i;
        if (obj != null) {
            this.i = null;
            b(obj);
        }
        kp kpVar = this.h;
        if (kpVar != null && kpVar.a()) {
            return true;
        }
        this.h = null;
        this.j = null;
        boolean z = false;
        while (!z && b()) {
            List<tr.a<?>> g2 = this.e.g();
            int i2 = this.g;
            this.g = i2 + 1;
            this.j = g2.get(i2);
            if (this.j != null && (this.e.e().a(this.j.c.b()) || this.e.c(this.j.c.getDataClass()))) {
                this.j.c.a(this.e.j(), this);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.e.g().size();
    }

    @DexIgnore
    public void cancel() {
        tr.a<?> aVar = this.j;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public void j() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void b(Object obj) {
        long a = qw.a();
        try {
            io<X> a2 = this.e.a(obj);
            mp mpVar = new mp(a2, obj, this.e.i());
            this.k = new lp(this.j.a, this.e.l());
            this.e.d().a(this.k, mpVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.k + ", data: " + obj + ", encoder: " + a2 + ", duration: " + qw.a(a));
            }
            this.j.c.a();
            this.h = new kp(Collections.singletonList(this.j.a), this.e, this);
        } catch (Throwable th) {
            this.j.c.a();
            throw th;
        }
    }

    @DexIgnore
    public void a(Object obj) {
        qp e2 = this.e.e();
        if (obj == null || !e2.a(this.j.c.b())) {
            this.f.a(this.j.a, obj, this.j.c, this.j.c.b(), this.k);
            return;
        }
        this.i = obj;
        this.f.j();
    }

    @DexIgnore
    public void a(Exception exc) {
        this.f.a(this.k, exc, this.j.c, this.j.c.b());
    }

    @DexIgnore
    public void a(ko koVar, Object obj, to<?> toVar, DataSource dataSource, ko koVar2) {
        this.f.a(koVar, obj, toVar, this.j.c.b(), koVar);
    }

    @DexIgnore
    public void a(ko koVar, Exception exc, to<?> toVar, DataSource dataSource) {
        this.f.a(koVar, exc, toVar, this.j.c.b());
    }
}
