package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.rj0;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl0 implements ServiceConnection {
    @DexIgnore
    public /* final */ Set<ServiceConnection> a; // = new HashSet();
    @DexIgnore
    public int b; // = 2;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public IBinder d;
    @DexIgnore
    public /* final */ rj0.a e;
    @DexIgnore
    public ComponentName f;
    @DexIgnore
    public /* final */ /* synthetic */ rl0 g;

    @DexIgnore
    public sl0(rl0 rl0, rj0.a aVar) {
        this.g = rl0;
        this.e = aVar;
    }

    @DexIgnore
    public final void a(String str) {
        this.b = 3;
        this.c = this.g.f.a(this.g.d, str, this.e.a(this.g.d), this, this.e.c());
        if (this.c) {
            this.g.e.sendMessageDelayed(this.g.e.obtainMessage(1, this.e), this.g.h);
            return;
        }
        this.b = 2;
        try {
            this.g.f.a(this.g.d, this);
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final void b(String str) {
        this.g.e.removeMessages(1, this.e);
        this.g.f.a(this.g.d, this);
        this.c = false;
        this.b = 2;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }

    @DexIgnore
    public final boolean e() {
        return this.a.isEmpty();
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.c) {
            this.g.e.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection onServiceConnected : this.a) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.b = 1;
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.c) {
            this.g.e.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection onServiceDisconnected : this.a) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.b = 2;
        }
    }

    @DexIgnore
    public final void b(ServiceConnection serviceConnection, String str) {
        em0 unused = this.g.f;
        Context unused2 = this.g.d;
        this.a.remove(serviceConnection);
    }

    @DexIgnore
    public final ComponentName b() {
        return this.f;
    }

    @DexIgnore
    public final void a(ServiceConnection serviceConnection, String str) {
        em0 unused = this.g.f;
        Context unused2 = this.g.d;
        this.e.a(this.g.d);
        this.a.add(serviceConnection);
    }

    @DexIgnore
    public final boolean a(ServiceConnection serviceConnection) {
        return this.a.contains(serviceConnection);
    }

    @DexIgnore
    public final IBinder a() {
        return this.d;
    }
}
