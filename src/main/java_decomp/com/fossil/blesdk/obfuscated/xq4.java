package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xq4 implements yq4 {
    @DexIgnore
    public static /* final */ xq4 b; // = new xq4();
    @DexIgnore
    public static String c; // = "1.6.99";
    @DexIgnore
    public static /* final */ String d; // = tq4.class.getName();
    @DexIgnore
    public /* final */ nq4 a; // = new tq4();

    @DexIgnore
    public static final xq4 c() {
        return b;
    }

    @DexIgnore
    public nq4 a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return d;
    }
}
