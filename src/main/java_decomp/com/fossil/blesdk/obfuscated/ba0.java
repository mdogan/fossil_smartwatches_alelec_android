package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.log.generic.LogEntry;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ba0 extends y90 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ba0(String str, int i, String str2, String str3, oa0 oa0, int i2, w90 w90, SharedPreferenceFileName sharedPreferenceFileName, boolean z) {
        super(str, i, str2, str3, oa0, i2, w90, sharedPreferenceFileName, z);
        wd4.b(str, "logDir");
        wd4.b(str2, "logFileNamePrefix");
        wd4.b(str3, "logFileNameExtension");
        wd4.b(oa0, "endpoint");
        wd4.b(w90, "logFileParser");
        wd4.b(sharedPreferenceFileName, "sharedPreferenceFileName");
    }

    @DexIgnore
    public void a(LogEntry logEntry) {
        wd4.b(logEntry, "logEntry");
        super.a(logEntry);
        if (logEntry instanceof SdkLogEntry) {
            SdkLogEntry sdkLogEntry = (SdkLogEntry) logEntry;
            sdkLogEntry.setSystemInformation(da0.e.a());
            DeviceInformation a = da0.e.a(sdkLogEntry.getMacAddress());
            if (a != null) {
                sdkLogEntry.setDeviceInformation(a);
            }
            sdkLogEntry.setSessionUuid(da0.e.b(sdkLogEntry.getMacAddress()));
        }
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public boolean b(LogEntry logEntry) {
        wd4.b(logEntry, "logEntry");
        boolean b = super.b(logEntry);
        if (b() > 0) {
            a(b());
        }
        return b;
    }
}
