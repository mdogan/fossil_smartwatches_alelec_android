package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j41 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<j41> CREATOR; // = new k41();
    @DexIgnore
    public static /* final */ List<kj0> l; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest e;
    @DexIgnore
    public List<kj0> f;
    @DexIgnore
    public String g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public String k;

    @DexIgnore
    public j41(LocationRequest locationRequest, List<kj0> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.e = locationRequest;
        this.f = list;
        this.g = str;
        this.h = z;
        this.i = z2;
        this.j = z3;
        this.k = str2;
    }

    @DexIgnore
    @Deprecated
    public static j41 a(LocationRequest locationRequest) {
        return new j41(locationRequest, l, (String) null, false, false, false, (String) null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof j41)) {
            return false;
        }
        j41 j41 = (j41) obj;
        return ak0.a(this.e, j41.e) && ak0.a(this.f, j41.f) && ak0.a(this.g, j41.g) && this.h == j41.h && this.i == j41.i && this.j == j41.j && ak0.a(this.k, j41.k);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        if (this.g != null) {
            sb.append(" tag=");
            sb.append(this.g);
        }
        if (this.k != null) {
            sb.append(" moduleId=");
            sb.append(this.k);
        }
        sb.append(" hideAppOps=");
        sb.append(this.h);
        sb.append(" clients=");
        sb.append(this.f);
        sb.append(" forceCoarseLocation=");
        sb.append(this.i);
        if (this.j) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) this.e, i2, false);
        lk0.b(parcel, 5, this.f, false);
        lk0.a(parcel, 6, this.g, false);
        lk0.a(parcel, 7, this.h);
        lk0.a(parcel, 8, this.i);
        lk0.a(parcel, 9, this.j);
        lk0.a(parcel, 10, this.k, false);
        lk0.a(parcel, a);
    }
}
