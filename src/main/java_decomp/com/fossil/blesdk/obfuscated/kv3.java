package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.squareup.okhttp.Protocol;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kv3 {
    @DexIgnore
    public /* final */ iv3 a;
    @DexIgnore
    public /* final */ Protocol b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ cv3 e;
    @DexIgnore
    public /* final */ dv3 f;
    @DexIgnore
    public /* final */ lv3 g;
    @DexIgnore
    public kv3 h;
    @DexIgnore
    public kv3 i;
    @DexIgnore
    public /* final */ kv3 j;
    @DexIgnore
    public volatile tu3 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public iv3 a;
        @DexIgnore
        public Protocol b;
        @DexIgnore
        public int c;
        @DexIgnore
        public String d;
        @DexIgnore
        public cv3 e;
        @DexIgnore
        public dv3.b f;
        @DexIgnore
        public lv3 g;
        @DexIgnore
        public kv3 h;
        @DexIgnore
        public kv3 i;
        @DexIgnore
        public kv3 j;

        @DexIgnore
        public b() {
            this.c = -1;
            this.f = new dv3.b();
        }

        @DexIgnore
        public b a(iv3 iv3) {
            this.a = iv3;
            return this;
        }

        @DexIgnore
        public b b(String str, String str2) {
            this.f.d(str, str2);
            return this;
        }

        @DexIgnore
        public b c(kv3 kv3) {
            if (kv3 != null) {
                a("networkResponse", kv3);
            }
            this.h = kv3;
            return this;
        }

        @DexIgnore
        public b d(kv3 kv3) {
            if (kv3 != null) {
                b(kv3);
            }
            this.j = kv3;
            return this;
        }

        @DexIgnore
        public b a(Protocol protocol) {
            this.b = protocol;
            return this;
        }

        @DexIgnore
        public final void b(kv3 kv3) {
            if (kv3.g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        @DexIgnore
        public b a(int i2) {
            this.c = i2;
            return this;
        }

        @DexIgnore
        public b(kv3 kv3) {
            this.c = -1;
            this.a = kv3.a;
            this.b = kv3.b;
            this.c = kv3.c;
            this.d = kv3.d;
            this.e = kv3.e;
            this.f = kv3.f.a();
            this.g = kv3.g;
            this.h = kv3.h;
            this.i = kv3.i;
            this.j = kv3.j;
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b a(cv3 cv3) {
            this.e = cv3;
            return this;
        }

        @DexIgnore
        public b a(String str, String str2) {
            this.f.a(str, str2);
            return this;
        }

        @DexIgnore
        public b a(dv3 dv3) {
            this.f = dv3.a();
            return this;
        }

        @DexIgnore
        public b a(lv3 lv3) {
            this.g = lv3;
            return this;
        }

        @DexIgnore
        public b a(kv3 kv3) {
            if (kv3 != null) {
                a("cacheResponse", kv3);
            }
            this.i = kv3;
            return this;
        }

        @DexIgnore
        public final void a(String str, kv3 kv3) {
            if (kv3.g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (kv3.h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (kv3.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (kv3.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        @DexIgnore
        public kv3 a() {
            if (this.a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.c >= 0) {
                return new kv3(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.c);
            }
        }
    }

    @DexIgnore
    public Protocol k() {
        return this.b;
    }

    @DexIgnore
    public iv3 l() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Response{protocol=" + this.b + ", code=" + this.c + ", message=" + this.d + ", url=" + this.a.i() + '}';
    }

    @DexIgnore
    public kv3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f.a();
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
    }

    @DexIgnore
    public String a(String str) {
        return a(str, (String) null);
    }

    @DexIgnore
    public tu3 b() {
        tu3 tu3 = this.k;
        if (tu3 != null) {
            return tu3;
        }
        tu3 a2 = tu3.a(this.f);
        this.k = a2;
        return a2;
    }

    @DexIgnore
    public kv3 c() {
        return this.i;
    }

    @DexIgnore
    public List<wu3> d() {
        String str;
        int i2 = this.c;
        if (i2 == 401) {
            str = "WWW-Authenticate";
        } else if (i2 != 407) {
            return Collections.emptyList();
        } else {
            str = "Proxy-Authenticate";
        }
        return yw3.a(g(), str);
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public cv3 f() {
        return this.e;
    }

    @DexIgnore
    public dv3 g() {
        return this.f;
    }

    @DexIgnore
    public String h() {
        return this.d;
    }

    @DexIgnore
    public kv3 i() {
        return this.h;
    }

    @DexIgnore
    public b j() {
        return new b();
    }

    @DexIgnore
    public String a(String str, String str2) {
        String a2 = this.f.a(str);
        return a2 != null ? a2 : str2;
    }

    @DexIgnore
    public lv3 a() {
        return this.g;
    }
}
