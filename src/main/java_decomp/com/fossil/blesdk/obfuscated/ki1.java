package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ki1 implements Callable<List<wl1>> {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ ai1 h;

    @DexIgnore
    public ki1(ai1 ai1, String str, String str2, String str3) {
        this.h = ai1;
        this.e = str;
        this.f = str2;
        this.g = str3;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.h.e.y();
        return this.h.e.l().b(this.e, this.f, this.g);
    }
}
