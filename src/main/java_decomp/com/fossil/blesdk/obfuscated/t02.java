package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t02 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer s; // = new a();
    @DexIgnore
    public static /* final */ a02 t; // = new a02("closed");
    @DexIgnore
    public /* final */ List<JsonElement> p; // = new ArrayList();
    @DexIgnore
    public String q;
    @DexIgnore
    public JsonElement r; // = xz1.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Writer {
        @DexIgnore
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public t02() {
        super(s);
    }

    @DexIgnore
    public JsonWriter A() throws IOException {
        uz1 uz1 = new uz1();
        a((JsonElement) uz1);
        this.p.add(uz1);
        return this;
    }

    @DexIgnore
    public JsonWriter B() throws IOException {
        yz1 yz1 = new yz1();
        a((JsonElement) yz1);
        this.p.add(yz1);
        return this;
    }

    @DexIgnore
    public JsonWriter C() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof uz1) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter D() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof yz1) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter I() throws IOException {
        a((JsonElement) xz1.a);
        return this;
    }

    @DexIgnore
    public final JsonElement J() {
        List<JsonElement> list = this.p;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public JsonElement L() {
        if (this.p.isEmpty()) {
            return this.r;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.p);
    }

    @DexIgnore
    public final void a(JsonElement jsonElement) {
        if (this.q != null) {
            if (!jsonElement.h() || E()) {
                ((yz1) J()).a(this.q, jsonElement);
            }
            this.q = null;
        } else if (this.p.isEmpty()) {
            this.r = jsonElement;
        } else {
            JsonElement J = J();
            if (J instanceof uz1) {
                ((uz1) J).a(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (this.p.isEmpty()) {
            this.p.add(t);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    public JsonWriter d(boolean z) throws IOException {
        a((JsonElement) new a02(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    public JsonWriter e(String str) throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (J() instanceof yz1) {
            this.q = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void flush() throws IOException {
    }

    @DexIgnore
    public JsonWriter h(String str) throws IOException {
        if (str == null) {
            I();
            return this;
        }
        a((JsonElement) new a02(str));
        return this;
    }

    @DexIgnore
    public JsonWriter h(long j) throws IOException {
        a((JsonElement) new a02((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    public JsonWriter a(Boolean bool) throws IOException {
        if (bool == null) {
            I();
            return this;
        }
        a((JsonElement) new a02(bool));
        return this;
    }

    @DexIgnore
    public JsonWriter a(Number number) throws IOException {
        if (number == null) {
            I();
            return this;
        }
        if (!G()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a((JsonElement) new a02(number));
        return this;
    }
}
