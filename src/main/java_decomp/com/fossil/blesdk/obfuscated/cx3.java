package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cx3 {
    @DexIgnore
    public /* final */ qu3 a;
    @DexIgnore
    public /* final */ ev3 b;
    @DexIgnore
    public /* final */ tv3 c;
    @DexIgnore
    public /* final */ hv3 d;
    @DexIgnore
    public /* final */ wv3 e;
    @DexIgnore
    public Proxy f;
    @DexIgnore
    public InetSocketAddress g;
    @DexIgnore
    public List<Proxy> h; // = Collections.emptyList();
    @DexIgnore
    public int i;
    @DexIgnore
    public List<InetSocketAddress> j; // = Collections.emptyList();
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ List<mv3> l; // = new ArrayList();

    @DexIgnore
    public cx3(qu3 qu3, ev3 ev3, hv3 hv3) {
        this.a = qu3;
        this.b = ev3;
        this.d = hv3;
        this.e = qv3.b.c(hv3);
        this.c = qv3.b.b(hv3);
        a(ev3, qu3.f());
    }

    @DexIgnore
    public static cx3 a(qu3 qu3, iv3 iv3, hv3 hv3) throws IOException {
        return new cx3(qu3, iv3.d(), hv3);
    }

    @DexIgnore
    public final boolean b() {
        return this.k < this.j.size();
    }

    @DexIgnore
    public final boolean c() {
        return !this.l.isEmpty();
    }

    @DexIgnore
    public final boolean d() {
        return this.i < this.h.size();
    }

    @DexIgnore
    public mv3 e() throws IOException {
        if (!b()) {
            if (d()) {
                this.f = h();
            } else if (c()) {
                return g();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.g = f();
        mv3 mv3 = new mv3(this.a, this.f, this.g);
        if (!this.e.c(mv3)) {
            return mv3;
        }
        this.l.add(mv3);
        return e();
    }

    @DexIgnore
    public final InetSocketAddress f() throws IOException {
        if (b()) {
            List<InetSocketAddress> list = this.j;
            int i2 = this.k;
            this.k = i2 + 1;
            return list.get(i2);
        }
        throw new SocketException("No route to " + this.a.j() + "; exhausted inet socket addresses: " + this.j);
    }

    @DexIgnore
    public final mv3 g() {
        return this.l.remove(0);
    }

    @DexIgnore
    public final Proxy h() throws IOException {
        if (d()) {
            List<Proxy> list = this.h;
            int i2 = this.i;
            this.i = i2 + 1;
            Proxy proxy = list.get(i2);
            a(proxy);
            return proxy;
        }
        throw new SocketException("No route to " + this.a.j() + "; exhausted proxy configurations: " + this.h);
    }

    @DexIgnore
    public boolean a() {
        return b() || d() || c();
    }

    @DexIgnore
    public void a(mv3 mv3, IOException iOException) {
        if (!(mv3.b().type() == Proxy.Type.DIRECT || this.a.g() == null)) {
            this.a.g().connectFailed(this.b.k(), mv3.b().address(), iOException);
        }
        this.e.b(mv3);
    }

    @DexIgnore
    public final void a(ev3 ev3, Proxy proxy) {
        if (proxy != null) {
            this.h = Collections.singletonList(proxy);
        } else {
            this.h = new ArrayList();
            List<Proxy> select = this.d.x().select(ev3.k());
            if (select != null) {
                this.h.addAll(select);
            }
            this.h.removeAll(Collections.singleton(Proxy.NO_PROXY));
            this.h.add(Proxy.NO_PROXY);
        }
        this.i = 0;
    }

    @DexIgnore
    public final void a(Proxy proxy) throws IOException {
        int i2;
        String str;
        this.j = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.a.j();
            i2 = this.a.k();
        } else {
            SocketAddress address = proxy.address();
            if (address instanceof InetSocketAddress) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                str = a(inetSocketAddress);
                i2 = inetSocketAddress.getPort();
            } else {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        for (InetAddress inetSocketAddress2 : this.c.a(str)) {
            this.j.add(new InetSocketAddress(inetSocketAddress2, i2));
        }
        this.k = 0;
    }

    @DexIgnore
    public static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }
}
