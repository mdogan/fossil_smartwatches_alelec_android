package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lg1 extends IInterface {
    @DexIgnore
    List<ll1> a(sl1 sl1, boolean z) throws RemoteException;

    @DexIgnore
    List<wl1> a(String str, String str2, sl1 sl1) throws RemoteException;

    @DexIgnore
    List<wl1> a(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<ll1> a(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    List<ll1> a(String str, String str2, boolean z, sl1 sl1) throws RemoteException;

    @DexIgnore
    void a(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void a(ig1 ig1, sl1 sl1) throws RemoteException;

    @DexIgnore
    void a(ig1 ig1, String str, String str2) throws RemoteException;

    @DexIgnore
    void a(ll1 ll1, sl1 sl1) throws RemoteException;

    @DexIgnore
    void a(sl1 sl1) throws RemoteException;

    @DexIgnore
    void a(wl1 wl1) throws RemoteException;

    @DexIgnore
    void a(wl1 wl1, sl1 sl1) throws RemoteException;

    @DexIgnore
    byte[] a(ig1 ig1, String str) throws RemoteException;

    @DexIgnore
    void b(sl1 sl1) throws RemoteException;

    @DexIgnore
    void c(sl1 sl1) throws RemoteException;

    @DexIgnore
    String d(sl1 sl1) throws RemoteException;
}
