package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import com.fossil.blesdk.obfuscated.bj;
import com.fossil.blesdk.obfuscated.gj;
import com.fossil.blesdk.obfuscated.il;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ql implements Runnable {
    @DexIgnore
    public static /* final */ String g; // = ej.a("EnqueueRunnable");
    @DexIgnore
    public /* final */ sj e;
    @DexIgnore
    public /* final */ oj f; // = new oj();

    @DexIgnore
    public ql(sj sjVar) {
        this.e = sjVar;
    }

    @DexIgnore
    public boolean a() {
        WorkDatabase g2 = this.e.g().g();
        g2.beginTransaction();
        try {
            boolean b = b(this.e);
            g2.setTransactionSuccessful();
            return b;
        } finally {
            g2.endTransaction();
        }
    }

    @DexIgnore
    public gj b() {
        return this.f;
    }

    @DexIgnore
    public void c() {
        uj g2 = this.e.g();
        rj.a(g2.c(), g2.g(), g2.f());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.e.h()) {
                if (a()) {
                    sl.a(this.e.g().b(), RescheduleReceiver.class, true);
                    c();
                }
                this.f.a(gj.a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", new Object[]{this.e}));
        } catch (Throwable th) {
            this.f.a(new gj.b.a(th));
        }
    }

    @DexIgnore
    public static boolean b(sj sjVar) {
        List<sj> e2 = sjVar.e();
        boolean z = false;
        if (e2 != null) {
            boolean z2 = false;
            for (sj next : e2) {
                if (!next.i()) {
                    z2 |= b(next);
                } else {
                    ej.a().e(g, String.format("Already enqueued work ids (%s).", new Object[]{TextUtils.join(", ", next.c())}), new Throwable[0]);
                }
            }
            z = z2;
        }
        return a(sjVar) | z;
    }

    @DexIgnore
    public static boolean a(sj sjVar) {
        boolean a = a(sjVar.g(), sjVar.f(), (String[]) sj.a(sjVar).toArray(new String[0]), sjVar.d(), sjVar.b());
        sjVar.j();
        return a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01a8 A[LOOP:6: B:107:0x01a2->B:109:0x01a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011f  */
    public static boolean a(uj ujVar, List<? extends lj> list, String[] strArr, String str, ExistingWorkPolicy existingWorkPolicy) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j;
        int i;
        uj ujVar2 = ujVar;
        String[] strArr2 = strArr;
        String str2 = str;
        ExistingWorkPolicy existingWorkPolicy2 = existingWorkPolicy;
        long currentTimeMillis = System.currentTimeMillis();
        WorkDatabase g2 = ujVar.g();
        boolean z5 = strArr2 != null && strArr2.length > 0;
        if (z5) {
            z3 = true;
            z2 = false;
            z = false;
            for (String str3 : strArr2) {
                il e2 = g2.d().e(str3);
                if (e2 == null) {
                    ej.a().b(g, String.format("Prerequisite %s doesn't exist; not enqueuing", new Object[]{str3}), new Throwable[0]);
                    return false;
                }
                WorkInfo.State state = e2.b;
                z3 &= state == WorkInfo.State.SUCCEEDED;
                if (state == WorkInfo.State.FAILED) {
                    z2 = true;
                } else if (state == WorkInfo.State.CANCELLED) {
                    z = true;
                }
            }
        } else {
            z3 = true;
            z2 = false;
            z = false;
        }
        boolean z6 = !TextUtils.isEmpty(str);
        if (z6 && !z5) {
            List<il.b> a = g2.d().a(str2);
            if (!a.isEmpty()) {
                if (existingWorkPolicy2 == ExistingWorkPolicy.APPEND) {
                    al a2 = g2.a();
                    ArrayList arrayList = new ArrayList();
                    for (il.b next : a) {
                        if (!a2.c(next.a)) {
                            boolean z7 = (next.b == WorkInfo.State.SUCCEEDED) & z3;
                            WorkInfo.State state2 = next.b;
                            if (state2 == WorkInfo.State.FAILED) {
                                z2 = true;
                            } else if (state2 == WorkInfo.State.CANCELLED) {
                                z = true;
                            }
                            arrayList.add(next.a);
                            z3 = z7;
                        }
                    }
                    strArr2 = (String[]) arrayList.toArray(strArr2);
                    z5 = strArr2.length > 0;
                } else {
                    if (existingWorkPolicy2 == ExistingWorkPolicy.KEEP) {
                        for (il.b bVar : a) {
                            WorkInfo.State state3 = bVar.b;
                            if (state3 == WorkInfo.State.ENQUEUED) {
                                return false;
                            }
                            if (state3 == WorkInfo.State.RUNNING) {
                                return false;
                            }
                        }
                    }
                    pl.a(str2, ujVar2, false).run();
                    jl d = g2.d();
                    for (il.b bVar2 : a) {
                        d.b(bVar2.a);
                    }
                    z4 = true;
                    for (lj ljVar : list) {
                        il d2 = ljVar.d();
                        if (!z5 || z3) {
                            if (!d2.d()) {
                                d2.n = currentTimeMillis;
                            } else {
                                j = currentTimeMillis;
                                d2.n = 0;
                                i = Build.VERSION.SDK_INT;
                                if (i < 23 && i <= 25) {
                                    a(d2);
                                } else if (Build.VERSION.SDK_INT <= 22 && a(ujVar2, "androidx.work.impl.background.gcm.GcmScheduler")) {
                                    a(d2);
                                }
                                if (d2.b == WorkInfo.State.ENQUEUED) {
                                    z4 = true;
                                }
                                g2.d().a(d2);
                                if (z5) {
                                    for (String zkVar : strArr2) {
                                        g2.a().a(new zk(ljVar.b(), zkVar));
                                    }
                                }
                                for (String llVar : ljVar.c()) {
                                    g2.e().a(new ll(llVar, ljVar.b()));
                                }
                                if (!z6) {
                                    g2.c().a(new fl(str2, ljVar.b()));
                                }
                                currentTimeMillis = j;
                            }
                        } else if (z2) {
                            d2.b = WorkInfo.State.FAILED;
                        } else if (z) {
                            d2.b = WorkInfo.State.CANCELLED;
                        } else {
                            d2.b = WorkInfo.State.BLOCKED;
                        }
                        j = currentTimeMillis;
                        i = Build.VERSION.SDK_INT;
                        if (i < 23) {
                        }
                        a(d2);
                        if (d2.b == WorkInfo.State.ENQUEUED) {
                        }
                        g2.d().a(d2);
                        if (z5) {
                        }
                        while (r3.hasNext()) {
                        }
                        if (!z6) {
                        }
                        currentTimeMillis = j;
                    }
                    return z4;
                }
            }
        }
        z4 = false;
        while (r8.hasNext()) {
        }
        return z4;
    }

    @DexIgnore
    public static void a(il ilVar) {
        zi ziVar = ilVar.j;
        if (ziVar.f() || ziVar.i()) {
            String str = ilVar.c;
            bj.a aVar = new bj.a();
            aVar.a(ilVar.e);
            aVar.a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            ilVar.c = ConstraintTrackingWorker.class.getName();
            ilVar.e = aVar.a();
        }
    }

    @DexIgnore
    public static boolean a(uj ujVar, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (qj qjVar : ujVar.f()) {
                if (cls.isAssignableFrom(qjVar.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }
}
