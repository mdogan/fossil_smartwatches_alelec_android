package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mo4 {
    @DexIgnore
    public static /* final */ mo4 a; // = c();
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(OkHttpClient.class.getName());

    @DexIgnore
    public static byte[] b(List<Protocol> list) {
        vo4 vo4 = new vo4();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                vo4.writeByte(protocol.toString().length());
                vo4.a(protocol.toString());
            }
        }
        return vo4.f();
    }

    @DexIgnore
    public static mo4 c() {
        mo4 f = ho4.f();
        if (f != null) {
            return f;
        }
        if (e()) {
            io4 g = io4.g();
            if (g != null) {
                return g;
            }
        }
        jo4 f2 = jo4.f();
        if (f2 != null) {
            return f2;
        }
        mo4 f3 = ko4.f();
        if (f3 != null) {
            return f3;
        }
        return new mo4();
    }

    @DexIgnore
    public static mo4 d() {
        return a;
    }

    @DexIgnore
    public static boolean e() {
        if ("conscrypt".equals(System.getProperty("okhttp.platform"))) {
            return true;
        }
        return "Conscrypt".equals(Security.getProviders()[0].getName());
    }

    @DexIgnore
    public String a() {
        return "OkHttp";
    }

    @DexIgnore
    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket) {
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
    }

    @DexIgnore
    public void a(SSLSocketFactory sSLSocketFactory) {
    }

    @DexIgnore
    public String b(SSLSocket sSLSocket) {
        return null;
    }

    @DexIgnore
    public boolean b(String str) {
        return true;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName();
    }

    @DexIgnore
    public void a(int i, String str, Throwable th) {
        b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    @DexIgnore
    public Object a(String str) {
        if (b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    @DexIgnore
    public void a(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        a(5, str, (Throwable) obj);
    }

    @DexIgnore
    public static List<String> a(List<Protocol> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                arrayList.add(protocol.toString());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public SSLContext b() {
        if ("1.7".equals(System.getProperty("java.specification.version"))) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }

    @DexIgnore
    public qo4 a(X509TrustManager x509TrustManager) {
        return new oo4(b(x509TrustManager));
    }

    @DexIgnore
    public so4 b(X509TrustManager x509TrustManager) {
        return new po4(x509TrustManager.getAcceptedIssuers());
    }
}
