package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Ordering;
import java.util.Comparator;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class av1 {
    @DexIgnore
    public static boolean a(Comparator<?> comparator, Iterable<?> iterable) {
        Comparator comparator2;
        tt1.a(comparator);
        tt1.a(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = a((SortedSet) iterable);
        } else if (!(iterable instanceof zu1)) {
            return false;
        } else {
            comparator2 = ((zu1) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }

    @DexIgnore
    public static <E> Comparator<? super E> a(SortedSet<E> sortedSet) {
        Comparator<? super E> comparator = sortedSet.comparator();
        return comparator == null ? Ordering.natural() : comparator;
    }
}
