package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kd4<P1, P2, R> extends va4<R> {
    @DexIgnore
    R invoke(P1 p1, P2 p2);
}
