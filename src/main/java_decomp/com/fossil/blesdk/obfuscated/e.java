package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e {

    @DexIgnore
    public interface a {
        @DexIgnore
        void d();

        @DexIgnore
        void e();

        @DexIgnore
        void f();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends MediaBrowser.ConnectionCallback {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public b(T t) {
            this.a = t;
        }

        @DexIgnore
        public void onConnected() {
            this.a.e();
        }

        @DexIgnore
        public void onConnectionFailed() {
            this.a.f();
        }

        @DexIgnore
        public void onConnectionSuspended() {
            this.a.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public static Object a(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getDescription();
        }

        @DexIgnore
        public static int b(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getFlags();
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void a(String str, List<?> list);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e$e")
    /* renamed from: com.fossil.blesdk.obfuscated.e$e  reason: collision with other inner class name */
    public static class C0010e<T extends d> extends MediaBrowser.SubscriptionCallback {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public C0010e(T t) {
            this.a = t;
        }

        @DexIgnore
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list) {
            this.a.a(str, list);
        }

        @DexIgnore
        public void onError(String str) {
            this.a.a(str);
        }
    }

    @DexIgnore
    public static Object a(a aVar) {
        return new b(aVar);
    }

    @DexIgnore
    public static void b(Object obj) {
        ((MediaBrowser) obj).disconnect();
    }

    @DexIgnore
    public static Bundle c(Object obj) {
        return ((MediaBrowser) obj).getExtras();
    }

    @DexIgnore
    public static Object d(Object obj) {
        return ((MediaBrowser) obj).getSessionToken();
    }

    @DexIgnore
    public static boolean e(Object obj) {
        return ((MediaBrowser) obj).isConnected();
    }

    @DexIgnore
    public static Object a(Context context, ComponentName componentName, Object obj, Bundle bundle) {
        return new MediaBrowser(context, componentName, (MediaBrowser.ConnectionCallback) obj, bundle);
    }

    @DexIgnore
    public static void a(Object obj) {
        ((MediaBrowser) obj).connect();
    }

    @DexIgnore
    public static Object a(d dVar) {
        return new C0010e(dVar);
    }
}
