package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface k43 extends w52<j43> {
    @DexIgnore
    void n(int i);

    @DexIgnore
    void n(List<WatchFaceWrapper> list);
}
