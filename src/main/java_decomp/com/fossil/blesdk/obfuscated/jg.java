package com.fossil.blesdk.obfuscated;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface jg extends Closeable {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(int i, double d);

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(int i, byte[] bArr);

    @DexIgnore
    void b(int i, long j);
}
