package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rs4 implements sr4<qm4, Boolean> {
    @DexIgnore
    public static /* final */ rs4 a; // = new rs4();

    @DexIgnore
    public Boolean a(qm4 qm4) throws IOException {
        return Boolean.valueOf(qm4.F());
    }
}
