package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kp0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kp0> CREATOR; // = new lp0();
    @DexIgnore
    public static /* final */ kp0 g; // = new kp0("com.google.android.gms", (String) null);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public kp0(String str, String str2) {
        ck0.a(str);
        this.e = str;
        this.f = str2;
    }

    @DexIgnore
    public static kp0 e(String str) {
        if ("com.google.android.gms".equals(str)) {
            return g;
        }
        return new kp0(str, (String) null);
    }

    @DexIgnore
    public final String H() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof kp0)) {
            return false;
        }
        kp0 kp0 = (kp0) obj;
        return this.e.equals(kp0.e) && ak0.a(this.f, kp0.f);
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.e, this.f);
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s:%s}", new Object[]{this.e, this.f});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e, false);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, a);
    }
}
