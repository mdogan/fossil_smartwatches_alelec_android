package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.bk0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yb0 extends ge0<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bk0.a<ac0, GoogleSignInAccount> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final /* synthetic */ Object a(ne0 ne0) {
            return ((ac0) ne0).a();
        }

        @DexIgnore
        public /* synthetic */ a(ed0 ed0) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class b {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {a, b, c, d};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    /*
    static {
        new a((ed0) null);
    }
    */

    @DexIgnore
    public yb0(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, rb0.e, googleSignInOptions, (ef0) new se0());
    }

    @DexIgnore
    public Intent i() {
        Context e = e();
        int i = ed0.a[k() - 1];
        if (i == 1) {
            return kc0.b(e, (GoogleSignInOptions) d());
        }
        if (i != 2) {
            return kc0.c(e, (GoogleSignInOptions) d());
        }
        return kc0.a(e, (GoogleSignInOptions) d());
    }

    @DexIgnore
    public xn1<Void> j() {
        return bk0.a(kc0.a(a(), e(), k() == b.c));
    }

    @DexIgnore
    public final synchronized int k() {
        if (j == b.a) {
            Context e = e();
            yd0 a2 = yd0.a();
            int a3 = a2.a(e, (int) ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE);
            if (a3 == 0) {
                j = b.d;
            } else if (a2.a(e, a3, (String) null) != null || DynamiteModule.a(e, "com.google.android.gms.auth.api.fallback") == 0) {
                j = b.b;
            } else {
                j = b.c;
            }
        }
        return j;
    }

    @DexIgnore
    public yb0(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, rb0.e, googleSignInOptions, (ef0) new se0());
    }
}
