package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.os.Trace;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class br0 implements Closeable {
    @DexIgnore
    public static /* final */ jr0<Boolean> f; // = ir0.a().a("nts.enable_tracing", true);
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    @TargetApi(18)
    public br0(String str) {
        this.e = qm0.d() && f.get().booleanValue();
        if (this.e) {
            Trace.beginSection(str.length() > 127 ? str.substring(0, 127) : str);
        }
    }

    @DexIgnore
    @TargetApi(18)
    public final void close() {
        if (this.e) {
            Trace.endSection();
        }
    }
}
