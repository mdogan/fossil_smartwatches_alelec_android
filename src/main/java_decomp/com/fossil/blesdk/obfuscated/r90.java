package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.gattserver.command.CommandId;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r90 extends Command {
    @DexIgnore
    public boolean n;
    @DexIgnore
    public /* final */ sa0<GattOperationResult> o; // = c().a();
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic p;
    @DexIgnore
    public /* final */ boolean q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r90(BluetoothDevice bluetoothDevice, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, GattServer.a aVar) {
        super(CommandId.NOTIFY_CHARACTERISTIC_CHANGED, bluetoothDevice, aVar);
        wd4.b(bluetoothDevice, "device");
        wd4.b(bluetoothGattCharacteristic, "characteristic");
        wd4.b(aVar, "gattServerOperationCallbackProvider");
        this.p = bluetoothGattCharacteristic;
        this.q = z;
    }

    @DexIgnore
    public void a(GattServer gattServer) {
        wd4.b(gattServer, "gattServer");
        if (gattServer.a(b(), this.p, this.q)) {
            this.n = true;
            return;
        }
        a(Command.Result.copy$default(f(), (CommandId) null, Command.Result.ResultCode.GATT_ERROR, new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (rd4) null), 1, (Object) null));
        a();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return (gattOperationResult instanceof t90) && wd4.a((Object) ((t90) gattOperationResult).b(), (Object) b());
    }

    @DexIgnore
    public sa0<GattOperationResult> g() {
        return this.o;
    }

    @DexIgnore
    public boolean h() {
        return (f().getResultCode() == Command.Result.ResultCode.TIMEOUT || f().getResultCode() == Command.Result.ResultCode.INTERRUPTED) && this.n;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        super.a(gattOperationResult);
        this.n = false;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return xa0.a(xa0.a(xa0.a(new JSONObject(), JSONKey.DEVICE, b().getAddress()), JSONKey.CHARACTERISTIC, this.p.getUuid().toString()), JSONKey.NEED_CONFIRM, Boolean.valueOf(this.q));
    }
}
