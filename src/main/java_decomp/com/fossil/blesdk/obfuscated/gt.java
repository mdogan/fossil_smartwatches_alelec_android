package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.fossil.blesdk.obfuscated.xs;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gt implements no<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ xs a;
    @DexIgnore
    public /* final */ hq b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements xs.b {
        @DexIgnore
        public /* final */ RecyclableBufferedInputStream a;
        @DexIgnore
        public /* final */ ow b;

        @DexIgnore
        public a(RecyclableBufferedInputStream recyclableBufferedInputStream, ow owVar) {
            this.a = recyclableBufferedInputStream;
            this.b = owVar;
        }

        @DexIgnore
        public void a() {
            this.a.y();
        }

        @DexIgnore
        public void a(kq kqVar, Bitmap bitmap) throws IOException {
            IOException y = this.b.y();
            if (y != null) {
                if (bitmap != null) {
                    kqVar.a(bitmap);
                }
                throw y;
            }
        }
    }

    @DexIgnore
    public gt(xs xsVar, hq hqVar) {
        this.a = xsVar;
        this.b = hqVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, mo moVar) {
        return this.a.a(inputStream);
    }

    @DexIgnore
    public bq<Bitmap> a(InputStream inputStream, int i, int i2, mo moVar) throws IOException {
        RecyclableBufferedInputStream recyclableBufferedInputStream;
        boolean z;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, this.b);
            z = true;
        }
        ow b2 = ow.b(recyclableBufferedInputStream);
        try {
            return this.a.a((InputStream) new sw(b2), i, i2, moVar, (xs.b) new a(recyclableBufferedInputStream, b2));
        } finally {
            b2.z();
            if (z) {
                recyclableBufferedInputStream.z();
            }
        }
    }
}
