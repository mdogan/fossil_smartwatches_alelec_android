package com.fossil.blesdk.obfuscated;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jt0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore
    public jt0(Uri uri) {
        this((String) null, uri, "", "", false, false);
    }

    @DexIgnore
    public jt0(String str, Uri uri, String str2, String str3, boolean z, boolean z2) {
        this.a = str;
        this.b = uri;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = z2;
    }

    @DexIgnore
    public final jt0 a(String str) {
        boolean z = this.e;
        if (!z) {
            return new jt0(this.a, this.b, str, this.d, z, this.f);
        }
        throw new IllegalStateException("Cannot set GServices prefix and skip GServices");
    }

    @DexIgnore
    public final <T> zs0<T> a(String str, T t, it0<T> it0) {
        return zs0.a(this, str, t, it0);
    }

    @DexIgnore
    public final zs0<String> a(String str, String str2) {
        return zs0.a(this, str, (String) null);
    }

    @DexIgnore
    public final zs0<Boolean> a(String str, boolean z) {
        return zs0.a(this, str, false);
    }

    @DexIgnore
    public final jt0 b(String str) {
        return new jt0(this.a, this.b, this.c, str, this.e, this.f);
    }
}
