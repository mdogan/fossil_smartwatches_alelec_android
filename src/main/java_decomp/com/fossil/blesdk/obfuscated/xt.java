package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xt extends nt<vt> implements xp {
    @DexIgnore
    public xt(vt vtVar) {
        super(vtVar);
    }

    @DexIgnore
    public void a() {
        ((vt) this.e).stop();
        ((vt) this.e).k();
    }

    @DexIgnore
    public int b() {
        return ((vt) this.e).i();
    }

    @DexIgnore
    public Class<vt> c() {
        return vt.class;
    }

    @DexIgnore
    public void d() {
        ((vt) this.e).e().prepareToDraw();
    }
}
