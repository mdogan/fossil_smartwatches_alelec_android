package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class io1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ho1 e;

    @DexIgnore
    public io1(ho1 ho1) {
        this.e = ho1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.e.b) {
            if (this.e.c != null) {
                this.e.c.onCanceled();
            }
        }
    }
}
