package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class is2 extends as2 implements wl3 {
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public vl3 j;
    @DexIgnore
    public ur3<ff2> k;
    @DexIgnore
    public ds2 l;
    @DexIgnore
    public gm3 m;
    @DexIgnore
    public ku3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final is2 a() {
            return new is2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 a;

        @DexIgnore
        public b(is2 is2) {
            this.a = is2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            is2.a(this.a).b(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public c(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            is2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public d(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            is2.a(this.e).a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public e(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            is2.a(this.e).b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public f(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            is2.a(this.e).c(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public g(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ff2 e;
        @DexIgnore
        public /* final */ /* synthetic */ is2 f;

        @DexIgnore
        public h(ff2 ff2, is2 is2) {
            this.e = ff2;
            this.f = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputEditText textInputEditText = this.e.s;
            wd4.a((Object) textInputEditText, "binding.etBirthday");
            Editable text = textInputEditText.getText();
            if (text == null || text.length() == 0) {
                is2.a(this.f).k();
            } else {
                is2.a(this.f).j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public i(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            is2.a(this.e).a(Gender.MALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public j(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            is2.a(this.e).a(Gender.FEMALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 e;

        @DexIgnore
        public k(is2 is2) {
            this.e = is2;
        }

        @DexIgnore
        public final void onClick(View view) {
            is2.a(this.e).a(Gender.OTHER);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ is2 a;

        @DexIgnore
        public l(is2 is2) {
            this.a = is2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            is2.a(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements dc<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ is2 a;

        @DexIgnore
        public m(is2 is2) {
            this.a = is2;
        }

        @DexIgnore
        public final void a(Date date) {
            if (date != null) {
                Calendar i = is2.a(this.a).i();
                i.setTime(date);
                is2.a(this.a).a(date, i);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ vl3 a(is2 is2) {
        vl3 vl3 = is2.j;
        if (vl3 != null) {
            return vl3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(boolean z) {
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.G;
                    if (imageView != null) {
                        wd4.a((Object) imageView, "it");
                        imageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void B(boolean z) {
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.F;
                    if (imageView != null) {
                        wd4.a((Object) imageView, "it");
                        imageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void H0() {
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    wd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.w;
                    wd4.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.w;
                    wd4.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(false);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void K(String str) {
        wd4.b(str, "birthdate");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputEditText textInputEditText = a2.s;
                    if (textInputEditText != null) {
                        textInputEditText.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(boolean z, String str) {
        wd4.b(str, "message");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.B;
                    wd4.a((Object) textInputLayout, "it.inputBirthday");
                    textInputLayout.setErrorEnabled(z);
                    TextInputLayout textInputLayout2 = a2.B;
                    wd4.a((Object) textInputLayout2, "it.inputBirthday");
                    textInputLayout2.setError(str);
                    ImageView imageView = a2.D;
                    wd4.a((Object) imageView, "it.ivCheckedBirthday");
                    imageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void c(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "auth");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    wd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_ProfileSetup_CTA__CreateAccount));
                    TextInputLayout textInputLayout = a2.C;
                    wd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setVisibility(8);
                    ImageView imageView = a2.E;
                    wd4.a((Object) imageView, "it.ivCheckedEmail");
                    imageView.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.t.setText(signUpEmailAuth.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        wd4.a((Object) textInputEditText, "it.etEmail");
                        textInputEditText.setKeyListener((KeyListener) null);
                        TextInputLayout textInputLayout2 = a2.C;
                        wd4.a((Object) textInputLayout2, "it.inputEmail");
                        textInputLayout2.setFocusable(false);
                        TextInputLayout textInputLayout3 = a2.C;
                        wd4.a((Object) textInputLayout3, "it.inputEmail");
                        textInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        ur3<ff2> ur3 = this.k;
        if (ur3 != null) {
            ff2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.I;
                if (dashBar != null) {
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.b(dashBar, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g0() {
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.C;
                wd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public void k() {
        if (isActive()) {
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AgreedTerms_Text__PleaseWait);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            S(a2);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.k = new ur3<>(this, (ff2) ra.a(layoutInflater, R.layout.fragment_profile_setup, viewGroup, false, O0()));
        ur3<ff2> ur3 = this.k;
        if (ur3 != null) {
            ff2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        vl3 vl3 = this.j;
        if (vl3 != null) {
            vl3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vl3 vl3 = this.j;
        if (vl3 != null) {
            vl3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ku3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.n = (ku3) a2;
            this.l = (ds2) getChildFragmentManager().a(ds2.r.a());
            if (this.l == null) {
                this.l = ds2.r.b();
            }
            m42 g2 = PortfolioApp.W.c().g();
            ds2 ds2 = this.l;
            if (ds2 != null) {
                g2.a(new em3(ds2)).a(this);
                ku3 ku3 = this.n;
                if (ku3 != null) {
                    ku3.c().a(this, new m(this));
                    ur3<ff2> ur3 = this.k;
                    if (ur3 != null) {
                        ff2 a3 = ur3.a();
                        if (a3 != null) {
                            a3.t.addTextChangedListener(new d(this));
                            a3.u.addTextChangedListener(new e(this));
                            a3.v.addTextChangedListener(new f(this));
                            a3.H.setOnClickListener(new g(this));
                            a3.s.setOnClickListener(new h(a3, this));
                            a3.y.setOnClickListener(new i(this));
                            a3.x.setOnClickListener(new j(this));
                            a3.z.setOnClickListener(new k(this));
                            a3.q.setOnCheckedChangeListener(new l(this));
                            a3.r.setOnCheckedChangeListener(new b(this));
                            a3.w.setOnClickListener(new c(this));
                            FlexibleTextView flexibleTextView = a3.A;
                            wd4.a((Object) flexibleTextView, "binding.ftvTermsService");
                            flexibleTextView.setMovementMethod(new LinkMovementMethod());
                        }
                        R("profile_setup_view");
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
                wd4.d("mUserBirthDayViewModel");
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void t0() {
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    wd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.w;
                    wd4.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.w;
                    wd4.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(true);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(vl3 vl3) {
        wd4.b(vl3, "presenter");
        this.j = vl3;
    }

    @DexIgnore
    public void a(boolean z, boolean z2, String str) {
        int i2;
        wd4.b(str, "errorMessage");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.C;
                    wd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setErrorEnabled(z2);
                    TextInputLayout textInputLayout2 = a2.C;
                    wd4.a((Object) textInputLayout2, "it.inputEmail");
                    textInputLayout2.setError(str);
                    ImageView imageView = a2.E;
                    wd4.a((Object) imageView, "it.ivCheckedEmail");
                    if (z) {
                        TextInputLayout textInputLayout3 = a2.C;
                        wd4.a((Object) textInputLayout3, "it.inputEmail");
                        if (textInputLayout3.getVisibility() == 0) {
                            i2 = 0;
                            imageView.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    imageView.setVisibility(i2);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g(int i2, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void b(MFUser mFUser) {
        Date date;
        wd4.b(mFUser, "user");
        if (isActive()) {
            H0();
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    wd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_CTA__Continue));
                    Gender gender = mFUser.getGender();
                    wd4.a((Object) gender, "user.gender");
                    a(gender);
                    String email = mFUser.getEmail();
                    boolean z = false;
                    if (!(email == null || cg4.a(email))) {
                        a2.t.setText(mFUser.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        wd4.a((Object) textInputEditText, "it.etEmail");
                        a((TextView) textInputEditText);
                    }
                    String firstName = mFUser.getFirstName();
                    wd4.a((Object) firstName, "user.firstName");
                    if (!cg4.a(firstName)) {
                        a2.u.setText(mFUser.getFirstName());
                        TextInputEditText textInputEditText2 = a2.u;
                        wd4.a((Object) textInputEditText2, "it.etFirstName");
                        a((TextView) textInputEditText2);
                    }
                    String lastName = mFUser.getLastName();
                    wd4.a((Object) lastName, "user.lastName");
                    if (!cg4.a(lastName)) {
                        a2.v.setText(mFUser.getLastName());
                        TextInputEditText textInputEditText3 = a2.v;
                        wd4.a((Object) textInputEditText3, "it.etLastName");
                        a((TextView) textInputEditText3);
                    }
                    String birthday = mFUser.getBirthday();
                    if (birthday == null || cg4.a(birthday)) {
                        z = true;
                    }
                    if (!z) {
                        TextInputEditText textInputEditText4 = a2.s;
                        wd4.a((Object) textInputEditText4, "it.etBirthday");
                        a((TextView) textInputEditText4);
                        try {
                            SimpleDateFormat simpleDateFormat = sk2.a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                vl3 vl3 = this.j;
                                if (vl3 != null) {
                                    Calendar i2 = vl3.i();
                                    i2.setTime(date);
                                    vl3 vl32 = this.j;
                                    if (vl32 == null) {
                                        wd4.d("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        vl32.a(date, i2);
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                } else {
                                    wd4.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e2);
                            date = new Date();
                        }
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wd4.b(bundle, "data");
        ds2 ds2 = this.l;
        if (ds2 != null) {
            ds2.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            ds2.show(childFragmentManager, ds2.r.a());
        }
    }

    @DexIgnore
    public void a(Spanned spanned) {
        wd4.b(spanned, "message");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.A;
                    wd4.a((Object) flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "auth");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    wd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_ProfileSetup_CTA__CreateAccount));
                    a2.u.setText(signUpSocialAuth.getFirstName());
                    a2.v.setText(signUpSocialAuth.getLastName());
                    TextInputLayout textInputLayout = a2.C;
                    wd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.t.setText(signUpSocialAuth.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        wd4.a((Object) textInputEditText, "it.etEmail");
                        textInputEditText.setKeyListener((KeyListener) null);
                        TextInputLayout textInputLayout2 = a2.C;
                        wd4.a((Object) textInputLayout2, "it.inputEmail");
                        textInputLayout2.setFocusable(false);
                        TextInputLayout textInputLayout3 = a2.C;
                        wd4.a((Object) textInputLayout3, "it.inputEmail");
                        textInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(TextView textView) {
        Drawable c2 = k6.c(PortfolioApp.W.c(), R.drawable.bg_border_disable_color_primary);
        textView.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.blueGrey50));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(c2);
    }

    @DexIgnore
    public void a(Gender gender) {
        wd4.b(gender, "gender");
        if (isActive()) {
            ur3<ff2> ur3 = this.k;
            if (ur3 != null) {
                ff2 a2 = ur3.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.W.c();
                    int a3 = k6.a((Context) c2, (int) R.color.activeColorPrimary);
                    int a4 = k6.a((Context) c2, (int) R.color.white);
                    Drawable c3 = k6.c(c2, R.drawable.bg_border_disable_color_primary);
                    Drawable c4 = k6.c(c2, R.drawable.bg_border_active_color_primary);
                    a2.z.setTextColor(a3);
                    a2.x.setTextColor(a3);
                    a2.y.setTextColor(a3);
                    FlexibleButton flexibleButton = a2.z;
                    wd4.a((Object) flexibleButton, "it.fbOther");
                    flexibleButton.setBackground(c3);
                    FlexibleButton flexibleButton2 = a2.x;
                    wd4.a((Object) flexibleButton2, "it.fbFemale");
                    flexibleButton2.setBackground(c3);
                    FlexibleButton flexibleButton3 = a2.y;
                    wd4.a((Object) flexibleButton3, "it.fbMale");
                    flexibleButton3.setBackground(c3);
                    int i2 = js2.a[gender.ordinal()];
                    if (i2 == 1) {
                        a2.y.setTextColor(a4);
                        FlexibleButton flexibleButton4 = a2.y;
                        wd4.a((Object) flexibleButton4, "it.fbMale");
                        flexibleButton4.setBackground(c4);
                    } else if (i2 == 2) {
                        a2.x.setTextColor(a4);
                        FlexibleButton flexibleButton5 = a2.x;
                        wd4.a((Object) flexibleButton5, "it.fbFemale");
                        flexibleButton5.setBackground(c4);
                    } else if (i2 == 3) {
                        a2.z.setTextColor(a4);
                        FlexibleButton flexibleButton6 = a2.z;
                        wd4.a((Object) flexibleButton6, "it.fbOther");
                        flexibleButton6.setBackground(c4);
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
    }
}
