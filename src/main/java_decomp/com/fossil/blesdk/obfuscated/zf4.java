package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zf4 extends yf4 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, jd4<? super T, ? extends CharSequence> jd4) {
        wd4.b(appendable, "$this$appendElement");
        if (jd4 != null) {
            appendable.append((CharSequence) jd4.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
