package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.to0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class al2 {
    @DexIgnore
    public static /* final */ String c; // = "al2";
    @DexIgnore
    public static he0 d;
    @DexIgnore
    public static d e;
    @DexIgnore
    public fn2 a;
    @DexIgnore
    public /* final */ Executor b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements he0.c {
        @DexIgnore
        public a(al2 al2) {
        }

        @DexIgnore
        public void a(vd0 vd0) {
            d dVar = al2.e;
            if (dVar != null) {
                dVar.a(vd0);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements he0.b {
        @DexIgnore
        public b(al2 al2) {
        }

        @DexIgnore
        public void e(Bundle bundle) {
            d dVar = al2.e;
            if (dVar != null) {
                dVar.a();
            }
        }

        @DexIgnore
        public void f(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(al2.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(al2.c, "Connection lost.  Reason: Service Disconnected");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            ie0<Status> a = so0.e.a(al2.d);
            d dVar = al2.e;
            if (dVar != null) {
                dVar.a(a);
            }
            al2.this.a.h(false);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ie0<Status> ie0);

        @DexIgnore
        void a(vd0 vd0);
    }

    @DexIgnore
    public al2(Context context, Executor executor, fn2 fn2) {
        if (d == null) {
            he0.a aVar = new he0.a(context);
            aVar.a((ee0<? extends ee0.d.C0011d>) so0.b, new Scope[0]);
            aVar.a((ee0<? extends ee0.d.C0011d>) so0.a, new Scope[0]);
            aVar.a((ee0<? extends ee0.d.C0011d>) so0.d, new Scope[0]);
            aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            aVar.a((he0.b) new b(this));
            aVar.a((he0.c) new a(this));
            d = aVar.a();
        }
        this.a = fn2;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.c();
        this.a.h(true);
    }

    @DexIgnore
    public void b() {
        d.d();
        this.a.h(false);
    }

    @DexIgnore
    public final to0 c() {
        to0.a c2 = to0.c();
        c2.a(DataType.p, 1);
        return c2.a();
    }

    @DexIgnore
    public boolean d() {
        return wb0.a(wb0.a((Context) PortfolioApp.R), (zb0) c());
    }

    @DexIgnore
    public boolean e() {
        return d.g() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.a.g();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new c());
    }

    @DexIgnore
    public void h() {
        try {
            b();
            wb0.a((Context) PortfolioApp.R, new GoogleSignInOptions.a(GoogleSignInOptions.s).a()).j();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void i() {
        this.a.h(false);
    }

    @DexIgnore
    public void a(d dVar) {
        e = dVar;
    }

    @DexIgnore
    public void a(Fragment fragment) {
        wb0.a(fragment, 1, wb0.a((Context) PortfolioApp.R), (zb0) c());
    }
}
