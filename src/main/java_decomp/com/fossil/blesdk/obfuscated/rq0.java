package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rq0 {
    @DexIgnore
    public static rq0 b;
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> a; // = new g4();

    @DexIgnore
    public rq0(Context context) {
    }

    @DexIgnore
    public static rq0 a(Context context) {
        rq0 rq0;
        synchronized (rq0.class) {
            if (b == null) {
                b = new rq0(context.getApplicationContext());
            }
            rq0 = b;
        }
        return rq0;
    }

    @DexIgnore
    public final synchronized void b(String str, String str2) {
        Map map = this.a.get(str2);
        if (map != null) {
            if ((map.remove(str) != null) && map.isEmpty()) {
                this.a.remove(str2);
            }
        }
    }

    @DexIgnore
    public final synchronized boolean c(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final synchronized boolean a(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            map = new g4();
            this.a.put(str2, map);
        }
        if (map.put(str, false) == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final synchronized boolean a(String str) {
        return this.a.containsKey(str);
    }
}
