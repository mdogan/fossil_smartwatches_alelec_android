package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.kv3;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tw3 {
    @DexIgnore
    public /* final */ yu3 a;
    @DexIgnore
    public /* final */ xu3 b;
    @DexIgnore
    public /* final */ Socket c;
    @DexIgnore
    public /* final */ xo4 d;
    @DexIgnore
    public /* final */ wo4 e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int g; // = 0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements kp4 {
        @DexIgnore
        public /* final */ bp4 e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b() {
            this.e = new bp4(tw3.this.d.b());
        }

        @DexIgnore
        public final void a(boolean z) throws IOException {
            if (tw3.this.f == 5) {
                tw3.this.a(this.e);
                int unused = tw3.this.f = 0;
                if (z && tw3.this.g == 1) {
                    int unused2 = tw3.this.g = 0;
                    qv3.b.a(tw3.this.a, tw3.this.b);
                } else if (tw3.this.g == 2) {
                    int unused3 = tw3.this.f = 6;
                    tw3.this.b.f().close();
                }
            } else {
                throw new IllegalStateException("state: " + tw3.this.f);
            }
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public final void c() {
            xv3.a(tw3.this.b.f());
            int unused = tw3.this.f = 6;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements jp4 {
        @DexIgnore
        public /* final */ bp4 e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
            this.e = new bp4(tw3.this.e.b());
        }

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            if (this.f) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                tw3.this.e.a(j);
                tw3.this.e.a("\r\n");
                tw3.this.e.a(vo4, j);
                tw3.this.e.a("\r\n");
            }
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            if (!this.f) {
                this.f = true;
                tw3.this.e.a("0\r\n\r\n");
                tw3.this.a(this.e);
                int unused = tw3.this.f = 3;
            }
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.f) {
                tw3.this.e.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public long h; // = -1;
        @DexIgnore
        public boolean i; // = true;
        @DexIgnore
        public /* final */ vw3 j;

        @DexIgnore
        public d(vw3 vw3) throws IOException {
            super();
            this.j = vw3;
        }

        @DexIgnore
        public long b(vo4 vo4, long j2) throws IOException {
            if (j2 < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j2);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (!this.i) {
                return -1;
            } else {
                long j3 = this.h;
                if (j3 == 0 || j3 == -1) {
                    d();
                    if (!this.i) {
                        return -1;
                    }
                }
                long b = tw3.this.d.b(vo4, Math.min(j2, this.h));
                if (b != -1) {
                    this.h -= b;
                    return b;
                }
                c();
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.i && !xv3.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                    c();
                }
                this.f = true;
            }
        }

        @DexIgnore
        public final void d() throws IOException {
            if (this.h != -1) {
                tw3.this.d.i();
            }
            try {
                this.h = tw3.this.d.l();
                String trim = tw3.this.d.i().trim();
                if (this.h < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.h + trim + "\"");
                } else if (this.h == 0) {
                    this.i = false;
                    dv3.b bVar = new dv3.b();
                    tw3.this.a(bVar);
                    this.j.a(bVar.a());
                    a(true);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements jp4 {
        @DexIgnore
        public /* final */ bp4 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public long g;

        @DexIgnore
        public void a(vo4 vo4, long j) throws IOException {
            if (!this.f) {
                xv3.a(vo4.B(), 0, j);
                if (j <= this.g) {
                    tw3.this.e.a(vo4, j);
                    this.g -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.g + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        public lp4 b() {
            return this.e;
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                this.f = true;
                if (this.g <= 0) {
                    tw3.this.a(this.e);
                    int unused = tw3.this.f = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            if (!this.f) {
                tw3.this.e.flush();
            }
        }

        @DexIgnore
        public e(long j) {
            this.e = new bp4(tw3.this.e.b());
            this.g = j;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long h;

        @DexIgnore
        public f(long j) throws IOException {
            super();
            this.h = j;
            if (this.h == 0) {
                a(true);
            }
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (this.h == 0) {
                return -1;
            } else {
                long b = tw3.this.d.b(vo4, Math.min(this.h, j));
                if (b != -1) {
                    this.h -= b;
                    if (this.h == 0) {
                        a(true);
                    }
                    return b;
                }
                c();
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.h != 0 && !xv3.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                    c();
                }
                this.f = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean h;

        @DexIgnore
        public g() {
            super();
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (this.h) {
                return -1;
            } else {
                long b = tw3.this.d.b(vo4, j);
                if (b != -1) {
                    return b;
                }
                this.h = true;
                a(false);
                return -1;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (!this.h) {
                    c();
                }
                this.f = true;
            }
        }
    }

    @DexIgnore
    public tw3(yu3 yu3, xu3 xu3, Socket socket) throws IOException {
        this.a = yu3;
        this.b = xu3;
        this.c = socket;
        this.d = ep4.a(ep4.b(socket));
        this.e = ep4.a(ep4.a(socket));
    }

    @DexIgnore
    public kp4 g() throws IOException {
        if (this.f == 4) {
            this.f = 5;
            return new g();
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public void h() {
        this.g = 1;
        if (this.f == 0) {
            this.g = 0;
            qv3.b.a(this.a, this.b);
        }
    }

    @DexIgnore
    public kv3.b i() throws IOException {
        dx3 a2;
        kv3.b bVar;
        int i = this.f;
        if (i == 1 || i == 3) {
            do {
                try {
                    a2 = dx3.a(this.d.i());
                    bVar = new kv3.b();
                    bVar.a(a2.a);
                    bVar.a(a2.b);
                    bVar.a(a2.c);
                    dv3.b bVar2 = new dv3.b();
                    a(bVar2);
                    bVar2.a(yw3.e, a2.a.toString());
                    bVar.a(bVar2.a());
                } catch (EOFException e2) {
                    IOException iOException = new IOException("unexpected end of stream on " + this.b + " (recycle count=" + qv3.b.c(this.b) + ")");
                    iOException.initCause(e2);
                    throw iOException;
                }
            } while (a2.b == 100);
            this.f = 4;
            return bVar;
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public void c() throws IOException {
        this.e.flush();
    }

    @DexIgnore
    public boolean d() {
        return this.f == 6;
    }

    @DexIgnore
    public boolean e() {
        int soTimeout;
        try {
            soTimeout = this.c.getSoTimeout();
            this.c.setSoTimeout(1);
            if (this.d.g()) {
                this.c.setSoTimeout(soTimeout);
                return false;
            }
            this.c.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException unused) {
            return true;
        } catch (IOException unused2) {
            return false;
        } catch (Throwable th) {
            this.c.setSoTimeout(soTimeout);
            throw th;
        }
    }

    @DexIgnore
    public jp4 f() {
        if (this.f == 1) {
            this.f = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public void b() throws IOException {
        this.g = 2;
        if (this.f == 0) {
            this.f = 6;
            this.b.f().close();
        }
    }

    @DexIgnore
    public void a(int i, int i2) {
        if (i != 0) {
            this.d.b().a((long) i, TimeUnit.MILLISECONDS);
        }
        if (i2 != 0) {
            this.e.b().a((long) i2, TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public long a() {
        return this.d.a().B();
    }

    @DexIgnore
    public void a(dv3 dv3, String str) throws IOException {
        if (this.f == 0) {
            this.e.a(str).a("\r\n");
            int b2 = dv3.b();
            for (int i = 0; i < b2; i++) {
                this.e.a(dv3.a(i)).a(": ").a(dv3.b(i)).a("\r\n");
            }
            this.e.a("\r\n");
            this.f = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public kp4 b(long j) throws IOException {
        if (this.f == 4) {
            this.f = 5;
            return new f(j);
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public void a(dv3.b bVar) throws IOException {
        while (true) {
            String i = this.d.i();
            if (i.length() != 0) {
                qv3.b.a(bVar, i);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public jp4 a(long j) {
        if (this.f == 1) {
            this.f = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public void a(bx3 bx3) throws IOException {
        if (this.f == 1) {
            this.f = 3;
            bx3.a(this.e);
            return;
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public kp4 a(vw3 vw3) throws IOException {
        if (this.f == 4) {
            this.f = 5;
            return new d(vw3);
        }
        throw new IllegalStateException("state: " + this.f);
    }

    @DexIgnore
    public final void a(bp4 bp4) {
        lp4 g2 = bp4.g();
        bp4.a(lp4.d);
        g2.a();
        g2.b();
    }
}
