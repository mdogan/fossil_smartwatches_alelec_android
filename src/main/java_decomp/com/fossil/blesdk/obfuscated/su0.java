package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;
import com.fossil.blesdk.obfuscated.su0.a;
import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzco;
import com.google.android.gms.internal.clearcut.zzew;
import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class su0<MessageType extends su0<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends kt0<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, su0<?, ?>> zzjr; // = new ConcurrentHashMap();
    @DexIgnore
    public cx0 zzjp; // = cx0.d();
    @DexIgnore
    public int zzjq; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<MessageType extends su0<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends lt0<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType e;
        @DexIgnore
        public MessageType f;
        @DexIgnore
        public boolean g; // = false;

        @DexIgnore
        public a(MessageType messagetype) {
            this.e = messagetype;
            this.f = (su0) messagetype.a(e.d, (Object) null, (Object) null);
        }

        @DexIgnore
        public static void a(MessageType messagetype, MessageType messagetype2) {
            fw0.a().a(messagetype).b(messagetype, messagetype2);
        }

        @DexIgnore
        public final /* synthetic */ lt0 a(kt0 kt0) {
            a((su0) kt0);
            return this;
        }

        @DexIgnore
        public final BuilderType a(MessageType messagetype) {
            g();
            a(this.f, messagetype);
            return this;
        }

        @DexIgnore
        public final /* synthetic */ tv0 b() {
            return this.e;
        }

        @DexIgnore
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) this.e.a(e.e, (Object) null, (Object) null);
            aVar.a((su0) v());
            return aVar;
        }

        @DexIgnore
        public void g() {
            if (this.g) {
                MessageType messagetype = (su0) this.f.a(e.d, (Object) null, (Object) null);
                a(messagetype, this.f);
                this.f = messagetype;
                this.g = false;
            }
        }

        @DexIgnore
        /* renamed from: h */
        public MessageType v() {
            if (this.g) {
                return this.f;
            }
            MessageType messagetype = this.f;
            fw0.a().a(messagetype).zzc(messagetype);
            this.g = true;
            return this.f;
        }

        @DexIgnore
        public final MessageType i() {
            MessageType messagetype = (su0) v();
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) messagetype.a(e.a, (Object) null, (Object) null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = fw0.a().a(messagetype).c(messagetype);
                    if (booleanValue) {
                        messagetype.a(e.b, (Object) z ? messagetype : null, (Object) null);
                    }
                }
            }
            if (z) {
                return messagetype;
            }
            throw new zzew(messagetype);
        }

        @DexIgnore
        public final /* synthetic */ tv0 s() {
            su0 su0 = (su0) v();
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) su0.a(e.a, (Object) null, (Object) null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = fw0.a().a(su0).c(su0);
                    if (booleanValue) {
                        su0.a(e.b, (Object) z ? su0 : null, (Object) null);
                    }
                }
            }
            if (z) {
                return su0;
            }
            throw new zzew(su0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends su0<T, ?>> extends mt0<T> {
        @DexIgnore
        public b(T t) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType extends Object<MessageType, BuilderType>> extends su0<MessageType, BuilderType> implements vv0 {
        @DexIgnore
        public lu0<d> zzjv; // = lu0.i();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ou0<d> {
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ zzfl f;

        @DexIgnore
        public final uv0 a(uv0 uv0, tv0 tv0) {
            return ((a) uv0).a((su0) tv0);
        }

        @DexIgnore
        public final zv0 a(zv0 zv0, zv0 zv02) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public final boolean a() {
            return false;
        }

        @DexIgnore
        public final zzfq b() {
            return this.f.zzek();
        }

        @DexIgnore
        public final boolean c() {
            return false;
        }

        @DexIgnore
        public final /* synthetic */ int compareTo(Object obj) {
            return this.e - ((d) obj).e;
        }

        @DexIgnore
        public final zzfl d() {
            return this.f;
        }

        @DexIgnore
        public final int zzc() {
            return this.e;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class e {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {a, b, c, d, e, f, g};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        /*
        static {
            int[] iArr = {i, j};
            int[] iArr2 = {k, l};
        }
        */

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore
    public static <T extends su0<T, ?>> T a(T t, byte[] bArr) throws zzco {
        T t2 = (su0) t.a(e.d, (Object) null, (Object) null);
        try {
            fw0.a().a(t2).a(t2, bArr, 0, bArr.length, new qt0());
            fw0.a().a(t2).zzc(t2);
            if (t2.zzex == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e2) {
            if (e2.getCause() instanceof zzco) {
                throw ((zzco) e2.getCause());
            }
            throw new zzco(e2.getMessage()).zzg(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzco.zzbl().zzg(t2);
        }
    }

    @DexIgnore
    public static <T extends su0<?, ?>> T a(Class<T> cls) {
        T t = (su0) zzjr.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (su0) zzjr.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t != null) {
            return t;
        }
        String valueOf = String.valueOf(cls.getName());
        throw new IllegalStateException(valueOf.length() != 0 ? "Unable to get default instance for: ".concat(valueOf) : new String("Unable to get default instance for: "));
    }

    @DexIgnore
    public static Object a(tv0 tv0, String str, Object[] objArr) {
        return new hw0(tv0, str, objArr);
    }

    @DexIgnore
    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static <T extends su0<?, ?>> void a(Class<T> cls, T t) {
        zzjr.put(cls, t);
    }

    @DexIgnore
    public static <T extends su0<T, ?>> T b(T t, byte[] bArr) throws zzco {
        T a2 = a(t, bArr);
        if (a2 != null) {
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) a2.a(e.a, (Object) null, (Object) null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = fw0.a().a(a2).c(a2);
                    if (booleanValue) {
                        a2.a(e.b, (Object) z ? a2 : null, (Object) null);
                    }
                }
            }
            if (!z) {
                throw new zzco(new zzew(a2).getMessage()).zzg(a2);
            }
        }
        return a2;
    }

    @DexIgnore
    public static <E> xu0<E> h() {
        return gw0.b();
    }

    @DexIgnore
    public abstract Object a(int i, Object obj, Object obj2);

    @DexIgnore
    public final void a(int i) {
        this.zzjq = i;
    }

    @DexIgnore
    public final void a(zzbn zzbn) throws IOException {
        fw0.a().a(getClass()).a(this, (px0) du0.a(zzbn));
    }

    @DexIgnore
    public final boolean a() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) a(e.a, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean c2 = fw0.a().a(this).c(this);
        if (booleanValue) {
            a(e.b, (Object) c2 ? this : null, (Object) null);
        }
        return c2;
    }

    @DexIgnore
    public final /* synthetic */ tv0 b() {
        return (su0) a(e.f, (Object) null, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ uv0 c() {
        a aVar = (a) a(e.e, (Object) null, (Object) null);
        aVar.a(this);
        return aVar;
    }

    @DexIgnore
    public final /* synthetic */ uv0 e() {
        return (a) a(e.e, (Object) null, (Object) null);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((su0) a(e.f, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return fw0.a().a(this).a(this, (su0) obj);
    }

    @DexIgnore
    public final int f() {
        if (this.zzjq == -1) {
            this.zzjq = fw0.a().a(this).b(this);
        }
        return this.zzjq;
    }

    @DexIgnore
    public final int g() {
        return this.zzjq;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.zzex;
        if (i != 0) {
            return i;
        }
        this.zzex = fw0.a().a(this).a(this);
        return this.zzex;
    }

    @DexIgnore
    public String toString() {
        return wv0.a(this, super.toString());
    }
}
