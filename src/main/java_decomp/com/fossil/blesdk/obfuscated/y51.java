package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y51 extends wb1<y51> {
    @DexIgnore
    public static volatile y51[] g;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public Boolean d; // = null;
    @DexIgnore
    public Boolean e; // = null;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public y51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static y51[] e() {
        if (g == null) {
            synchronized (ac1.b) {
                if (g == null) {
                    g = new y51[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        String str = this.c;
        if (str != null) {
            vb1.a(1, str);
        }
        Boolean bool = this.d;
        if (bool != null) {
            vb1.a(2, bool.booleanValue());
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            vb1.a(3, bool2.booleanValue());
        }
        Integer num = this.f;
        if (num != null) {
            vb1.b(4, num.intValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof y51)) {
            return false;
        }
        y51 y51 = (y51) obj;
        String str = this.c;
        if (str == null) {
            if (y51.c != null) {
                return false;
            }
        } else if (!str.equals(y51.c)) {
            return false;
        }
        Boolean bool = this.d;
        if (bool == null) {
            if (y51.d != null) {
                return false;
            }
        } else if (!bool.equals(y51.d)) {
            return false;
        }
        Boolean bool2 = this.e;
        if (bool2 == null) {
            if (y51.e != null) {
                return false;
            }
        } else if (!bool2.equals(y51.e)) {
            return false;
        }
        Integer num = this.f;
        if (num == null) {
            if (y51.f != null) {
                return false;
            }
        } else if (!num.equals(y51.f)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(y51.b);
        }
        yb1 yb12 = y51.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (y51.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Boolean bool = this.d;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.e;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Integer num = this.f;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += vb1.b(1, str);
        }
        Boolean bool = this.d;
        if (bool != null) {
            bool.booleanValue();
            a += vb1.c(2) + 1;
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            bool2.booleanValue();
            a += vb1.c(3) + 1;
        }
        Integer num = this.f;
        return num != null ? a + vb1.c(4, num.intValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = ub1.b();
            } else if (c2 == 16) {
                this.d = Boolean.valueOf(ub1.d());
            } else if (c2 == 24) {
                this.e = Boolean.valueOf(ub1.d());
            } else if (c2 == 32) {
                this.f = Integer.valueOf(ub1.e());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
