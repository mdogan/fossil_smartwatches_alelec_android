package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.xs2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tn3 extends as2 implements xn3 {
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public ur3<n92> j;
    @DexIgnore
    public wn3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final tn3 a() {
            return new tn3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tn3 e;

        @DexIgnore
        public b(tn3 tn3) {
            this.e = tn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            tn3.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tn3 e;

        @DexIgnore
        public c(tn3 tn3) {
            this.e = tn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    public static final /* synthetic */ wn3 a(tn3 tn3) {
        wn3 wn3 = tn3.k;
        if (wn3 != null) {
            return wn3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        wn3 wn3 = this.k;
        if (wn3 != null) {
            wn3.h();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        n92 n92 = (n92) ra.a(layoutInflater, R.layout.fragment_allow_notification_service, viewGroup, false, O0());
        FlexibleTextView flexibleTextView = n92.q;
        wd4.a((Object) flexibleTextView, "binding.ftvDescription");
        be4 be4 = be4.a;
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidInAppNotication_Text__BrandNeedsAccessToYourNotification);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026AccessToYourNotification)");
        Object[] objArr = {PortfolioApp.W.c().i()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        n92.s.setOnClickListener(new b(this));
        n92.r.setOnClickListener(new c(this));
        this.j = new ur3<>(this, n92);
        ur3<n92> ur3 = this.j;
        if (ur3 != null) {
            n92 a3 = ur3.a();
            if (a3 != null) {
                wd4.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        wn3 wn3 = this.k;
        if (wn3 != null) {
            wn3.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        wn3 wn3 = this.k;
        if (wn3 != null) {
            wn3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void t(List<xs2.c> list) {
        wd4.b(list, "listPermissionModel");
    }

    @DexIgnore
    public void a(wn3 wn3) {
        wd4.b(wn3, "presenter");
        this.k = wn3;
    }
}
