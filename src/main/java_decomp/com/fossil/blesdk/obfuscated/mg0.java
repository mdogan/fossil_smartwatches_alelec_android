package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mg0 extends bh0 {
    @DexIgnore
    public WeakReference<fg0> a;

    @DexIgnore
    public mg0(fg0 fg0) {
        this.a = new WeakReference<>(fg0);
    }

    @DexIgnore
    public final void a() {
        fg0 fg0 = (fg0) this.a.get();
        if (fg0 != null) {
            fg0.l();
        }
    }
}
