package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ l04 f;

    @DexIgnore
    public v24(Context context, l04 l04) {
        this.e = context;
        this.f = l04;
    }

    @DexIgnore
    public final void run() {
        Context context = this.e;
        if (context == null) {
            k04.m.d("The Context of StatService.onPause() can not be null!");
        } else {
            k04.b(context, f24.k(context), this.f);
        }
    }
}
