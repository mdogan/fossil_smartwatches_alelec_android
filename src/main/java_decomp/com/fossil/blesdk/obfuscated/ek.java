package com.fossil.blesdk.obfuscated;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.BackoffPolicy;
import androidx.work.NetworkType;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.blesdk.obfuscated.aj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ek {
    @DexIgnore
    public static /* final */ String b; // = ej.a("SystemJobInfoConverter");
    @DexIgnore
    public /* final */ ComponentName a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[NetworkType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[NetworkType.NOT_REQUIRED.ordinal()] = 1;
            a[NetworkType.CONNECTED.ordinal()] = 2;
            a[NetworkType.UNMETERED.ordinal()] = 3;
            a[NetworkType.NOT_ROAMING.ordinal()] = 4;
            a[NetworkType.METERED.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    public ek(Context context) {
        this.a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    @DexIgnore
    public JobInfo a(il ilVar, int i) {
        zi ziVar = ilVar.j;
        int a2 = a(ziVar.b());
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", ilVar.a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", ilVar.d());
        JobInfo.Builder extras = new JobInfo.Builder(i, this.a).setRequiredNetworkType(a2).setRequiresCharging(ziVar.g()).setRequiresDeviceIdle(ziVar.h()).setExtras(persistableBundle);
        if (!ziVar.h()) {
            extras.setBackoffCriteria(ilVar.m, ilVar.l == BackoffPolicy.LINEAR ? 0 : 1);
        }
        long max = Math.max(ilVar.a() - System.currentTimeMillis(), 0);
        if (Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (Build.VERSION.SDK_INT >= 24 && ziVar.e()) {
            for (aj.a a3 : ziVar.a().a()) {
                extras.addTriggerContentUri(a(a3));
            }
            extras.setTriggerContentUpdateDelay(ziVar.c());
            extras.setTriggerContentMaxDelay(ziVar.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(ziVar.f());
            extras.setRequiresStorageNotLow(ziVar.i());
        }
        return extras.build();
    }

    @DexIgnore
    public static JobInfo.TriggerContentUri a(aj.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    @DexIgnore
    public static int a(NetworkType networkType) {
        int i = a.a[networkType.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i != 4) {
            if (i == 5 && Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        ej.a().a(b, String.format("API version too low. Cannot convert network type value %s", new Object[]{networkType}), new Throwable[0]);
        return 1;
    }
}
