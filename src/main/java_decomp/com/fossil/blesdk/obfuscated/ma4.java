package com.fossil.blesdk.obfuscated;

import io.reactivex.internal.operators.observable.ObservableScalarXMap$ScalarDisposable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ma4<T> extends z84<T> implements ba4<T> {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public ma4(T t) {
        this.e = t;
    }

    @DexIgnore
    public void b(b94<? super T> b94) {
        ObservableScalarXMap$ScalarDisposable observableScalarXMap$ScalarDisposable = new ObservableScalarXMap$ScalarDisposable(b94, this.e);
        b94.onSubscribe(observableScalarXMap$ScalarDisposable);
        observableScalarXMap$ScalarDisposable.run();
    }

    @DexIgnore
    public T call() {
        return this.e;
    }
}
