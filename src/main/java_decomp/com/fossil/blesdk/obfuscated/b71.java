package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class b71<T> {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context g;
    @DexIgnore
    public static /* final */ AtomicInteger h; // = new AtomicInteger();
    @DexIgnore
    public /* final */ h71 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;

    @DexIgnore
    public b71(h71 h71, String str, T t) {
        this.d = -1;
        if (h71.a != null) {
            this.a = h71;
            this.b = str;
            this.c = t;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    public static void a(Context context) {
        synchronized (f) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (g != context) {
                synchronized (p61.class) {
                    p61.f.clear();
                }
                synchronized (i71.class) {
                    i71.f.clear();
                }
                synchronized (x61.class) {
                    x61.b = null;
                }
                h.incrementAndGet();
                g = context;
            }
        }
    }

    @DexIgnore
    public static void f() {
        h.incrementAndGet();
    }

    @DexIgnore
    public abstract T a(Object obj);

    @DexIgnore
    public final T b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return a(this.a.c);
    }

    @DexIgnore
    public final T d() {
        u61 u61;
        String str = (String) x61.a(g).a("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
        if (!(str != null && m61.c.matcher(str).matches())) {
            if (this.a.a != null) {
                u61 = p61.a(g.getContentResolver(), this.a.a);
            } else {
                u61 = i71.a(g, (String) null);
            }
            if (u61 != null) {
                Object a2 = u61.a(c());
                if (a2 != null) {
                    return a(a2);
                }
            }
        } else {
            String valueOf = String.valueOf(c());
            Log.w("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
        }
        return null;
    }

    @DexIgnore
    public final T e() {
        Object a2 = x61.a(g).a(a(this.a.b));
        if (a2 != null) {
            return a(a2);
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ b71(h71 h71, String str, Object obj, c71 c71) {
        this(h71, str, obj);
    }

    @DexIgnore
    public final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final T a() {
        int i = h.get();
        if (this.d < i) {
            synchronized (this) {
                if (this.d < i) {
                    if (g != null) {
                        T d2 = d();
                        if (d2 == null) {
                            d2 = e();
                            if (d2 == null) {
                                d2 = this.c;
                            }
                        }
                        this.e = d2;
                        this.d = i;
                    } else {
                        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
                    }
                }
            }
        }
        return this.e;
    }

    @DexIgnore
    public static b71<Long> a(h71 h71, String str, long j) {
        return new c71(h71, str, Long.valueOf(j));
    }

    @DexIgnore
    public static b71<Integer> a(h71 h71, String str, int i) {
        return new d71(h71, str, Integer.valueOf(i));
    }

    @DexIgnore
    public static b71<Boolean> a(h71 h71, String str, boolean z) {
        return new e71(h71, str, Boolean.valueOf(z));
    }

    @DexIgnore
    public static b71<Double> a(h71 h71, String str, double d2) {
        return new f71(h71, str, Double.valueOf(d2));
    }

    @DexIgnore
    public static b71<String> a(h71 h71, String str, String str2) {
        return new g71(h71, str, str2);
    }
}
