package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.fitness.FitnessData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface u40 {
    @DexIgnore
    g90<FitnessData[]> a(BiometricProfile biometricProfile);
}
