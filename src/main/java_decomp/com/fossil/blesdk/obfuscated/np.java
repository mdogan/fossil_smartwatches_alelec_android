package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface np {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(ko koVar, Exception exc, to<?> toVar, DataSource dataSource);

        @DexIgnore
        void a(ko koVar, Object obj, to<?> toVar, DataSource dataSource, ko koVar2);

        @DexIgnore
        void j();
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    void cancel();
}
