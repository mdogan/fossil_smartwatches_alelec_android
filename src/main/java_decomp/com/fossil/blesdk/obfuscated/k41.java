package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.location.LocationRequest;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k41 implements Parcelable.Creator<j41> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        List<kj0> list = j41.l;
        LocationRequest locationRequest = null;
        String str = null;
        String str2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 != 1) {
                switch (a2) {
                    case 5:
                        list = SafeParcelReader.c(parcel, a, kj0.CREATOR);
                        break;
                    case 6:
                        str = SafeParcelReader.f(parcel, a);
                        break;
                    case 7:
                        z = SafeParcelReader.i(parcel, a);
                        break;
                    case 8:
                        z2 = SafeParcelReader.i(parcel, a);
                        break;
                    case 9:
                        z3 = SafeParcelReader.i(parcel, a);
                        break;
                    case 10:
                        str2 = SafeParcelReader.f(parcel, a);
                        break;
                    default:
                        SafeParcelReader.v(parcel, a);
                        break;
                }
            } else {
                locationRequest = SafeParcelReader.a(parcel, a, LocationRequest.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new j41(locationRequest, list, str, z, z2, z3, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new j41[i];
    }
}
