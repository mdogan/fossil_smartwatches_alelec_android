package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.r6;
import java.lang.ref.WeakReference;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zr1 extends Drawable implements d7, Drawable.Callback {
    @DexIgnore
    public static /* final */ int[] l0; // = {16842910};
    @DexIgnore
    public lr1 A;
    @DexIgnore
    public lr1 B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public /* final */ Context K;
    @DexIgnore
    public /* final */ TextPaint L; // = new TextPaint(1);
    @DexIgnore
    public /* final */ Paint M; // = new Paint(1);
    @DexIgnore
    public /* final */ Paint N;
    @DexIgnore
    public /* final */ Paint.FontMetrics O; // = new Paint.FontMetrics();
    @DexIgnore
    public /* final */ RectF P; // = new RectF();
    @DexIgnore
    public /* final */ PointF Q; // = new PointF();
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public int W;
    @DexIgnore
    public int X; // = 255;
    @DexIgnore
    public ColorFilter Y;
    @DexIgnore
    public PorterDuffColorFilter Z;
    @DexIgnore
    public ColorStateList a0;
    @DexIgnore
    public PorterDuff.Mode b0; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public int[] c0;
    @DexIgnore
    public boolean d0;
    @DexIgnore
    public ColorStateList e;
    @DexIgnore
    public ColorStateList e0;
    @DexIgnore
    public float f;
    @DexIgnore
    public WeakReference<b> f0; // = new WeakReference<>((Object) null);
    @DexIgnore
    public float g;
    @DexIgnore
    public boolean g0; // = true;
    @DexIgnore
    public ColorStateList h;
    @DexIgnore
    public float h0;
    @DexIgnore
    public float i;
    @DexIgnore
    public TextUtils.TruncateAt i0;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public boolean j0;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public int k0;
    @DexIgnore
    public CharSequence l;
    @DexIgnore
    public ys1 m;
    @DexIgnore
    public /* final */ r6.a n; // = new a();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public Drawable p;
    @DexIgnore
    public ColorStateList q;
    @DexIgnore
    public float r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public Drawable t;
    @DexIgnore
    public ColorStateList u;
    @DexIgnore
    public float v;
    @DexIgnore
    public CharSequence w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public Drawable z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends r6.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(Typeface typeface) {
            boolean unused = zr1.this.g0 = true;
            zr1.this.N();
            zr1.this.invalidateSelf();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public zr1(Context context) {
        this.K = context;
        this.k = "";
        this.L.density = context.getResources().getDisplayMetrics().density;
        this.N = null;
        Paint paint = this.N;
        if (paint != null) {
            paint.setStyle(Paint.Style.STROKE);
        }
        setState(l0);
        a(l0);
        this.j0 = true;
    }

    @DexIgnore
    public ColorStateList A() {
        return this.j;
    }

    @DexIgnore
    public lr1 B() {
        return this.A;
    }

    @DexIgnore
    public CharSequence C() {
        return this.k;
    }

    @DexIgnore
    public ys1 D() {
        return this.m;
    }

    @DexIgnore
    public float E() {
        return this.G;
    }

    @DexIgnore
    public float F() {
        return this.F;
    }

    @DexIgnore
    public final float G() {
        if (!this.g0) {
            return this.h0;
        }
        this.h0 = a(this.l);
        this.g0 = false;
        return this.h0;
    }

    @DexIgnore
    public final ColorFilter H() {
        ColorFilter colorFilter = this.Y;
        return colorFilter != null ? colorFilter : this.Z;
    }

    @DexIgnore
    public boolean I() {
        return this.x;
    }

    @DexIgnore
    public boolean J() {
        return this.y;
    }

    @DexIgnore
    public boolean K() {
        return this.o;
    }

    @DexIgnore
    public boolean L() {
        return f(this.t);
    }

    @DexIgnore
    public boolean M() {
        return this.s;
    }

    @DexIgnore
    public void N() {
        b bVar = (b) this.f0.get();
        if (bVar != null) {
            bVar.a();
        }
    }

    @DexIgnore
    public boolean O() {
        return this.j0;
    }

    @DexIgnore
    public final boolean P() {
        return this.y && this.z != null && this.V;
    }

    @DexIgnore
    public final boolean Q() {
        return this.o && this.p != null;
    }

    @DexIgnore
    public final boolean R() {
        return this.s && this.t != null;
    }

    @DexIgnore
    public final void S() {
        this.e0 = this.d0 ? at1.a(this.j) : null;
    }

    @DexIgnore
    public final float b() {
        return R() ? this.H + this.v + this.I : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void c(Canvas canvas, Rect rect) {
        if (Q()) {
            a(rect, this.P);
            RectF rectF = this.P;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.p.setBounds(0, 0, (int) this.P.width(), (int) this.P.height());
            this.p.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.y && this.z != null && this.x;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && getAlpha() != 0) {
            int i2 = 0;
            int i3 = this.X;
            if (i3 < 255) {
                i2 = xr1.a(canvas, (float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, i3);
            }
            b(canvas, bounds);
            d(canvas, bounds);
            f(canvas, bounds);
            c(canvas, bounds);
            a(canvas, bounds);
            if (this.j0) {
                h(canvas, bounds);
            }
            e(canvas, bounds);
            g(canvas, bounds);
            if (this.X < 255) {
                canvas.restoreToCount(i2);
            }
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, Rect rect) {
        if (R()) {
            c(rect, this.P);
            RectF rectF = this.P;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.t.setBounds(0, 0, (int) this.P.width(), (int) this.P.height());
            this.t.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    @DexIgnore
    public void f(boolean z2) {
        if (this.d0 != z2) {
            this.d0 = z2;
            S();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void g(Canvas canvas, Rect rect) {
        Paint paint = this.N;
        if (paint != null) {
            paint.setColor(t6.c(-16777216, 127));
            canvas.drawRect(rect, this.N);
            if (Q() || P()) {
                a(rect, this.P);
                canvas.drawRect(this.P, this.N);
            }
            if (this.l != null) {
                canvas.drawLine((float) rect.left, rect.exactCenterY(), (float) rect.right, rect.exactCenterY(), this.N);
            }
            if (R()) {
                c(rect, this.P);
                canvas.drawRect(this.P, this.N);
            }
            this.N.setColor(t6.c(-65536, 127));
            b(rect, this.P);
            canvas.drawRect(this.P, this.N);
            this.N.setColor(t6.c(-16711936, 127));
            d(rect, this.P);
            canvas.drawRect(this.P, this.N);
        }
    }

    @DexIgnore
    public int getAlpha() {
        return this.X;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.Y;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return (int) this.f;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.C + a() + this.F + G() + this.G + b() + this.J), this.k0);
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.g);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.g);
        }
        outline.setAlpha(((float) getAlpha()) / 255.0f);
    }

    @DexIgnore
    public final void h(Canvas canvas, Rect rect) {
        if (this.l != null) {
            Paint.Align a2 = a(rect, this.Q);
            e(rect, this.P);
            if (this.m != null) {
                this.L.drawableState = getState();
                this.m.b(this.K, this.L, this.n);
            }
            this.L.setTextAlign(a2);
            int i2 = 0;
            boolean z2 = Math.round(G()) > Math.round(this.P.width());
            if (z2) {
                i2 = canvas.save();
                canvas.clipRect(this.P);
            }
            CharSequence charSequence = this.l;
            if (z2 && this.i0 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.L, this.P.width(), this.i0);
            }
            CharSequence charSequence2 = charSequence;
            int length = charSequence2.length();
            PointF pointF = this.Q;
            canvas.drawText(charSequence2, 0, length, pointF.x, pointF.y, this.L);
            if (z2) {
                canvas.restoreToCount(i2);
            }
        }
    }

    @DexIgnore
    public Drawable i() {
        Drawable drawable = this.p;
        if (drawable != null) {
            return c7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @DexIgnore
    public boolean isStateful() {
        return f(this.e) || f(this.h) || (this.d0 && f(this.e0)) || b(this.m) || d() || f(this.p) || f(this.z) || f(this.a0);
    }

    @DexIgnore
    public void j(int i2) {
        c(this.K.getResources().getBoolean(i2));
    }

    @DexIgnore
    public void k(int i2) {
        d(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public float l() {
        return this.f;
    }

    @DexIgnore
    public void m(int i2) {
        c(m0.b(this.K, i2));
    }

    @DexIgnore
    public ColorStateList n() {
        return this.h;
    }

    @DexIgnore
    public float o() {
        return this.i;
    }

    @DexIgnore
    @TargetApi(23)
    public boolean onLayoutDirectionChanged(int i2) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i2);
        if (Q()) {
            onLayoutDirectionChanged |= this.p.setLayoutDirection(i2);
        }
        if (P()) {
            onLayoutDirectionChanged |= this.z.setLayoutDirection(i2);
        }
        if (R()) {
            onLayoutDirectionChanged |= this.t.setLayoutDirection(i2);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public boolean onLevelChange(int i2) {
        boolean onLevelChange = super.onLevelChange(i2);
        if (Q()) {
            onLevelChange |= this.p.setLevel(i2);
        }
        if (P()) {
            onLevelChange |= this.z.setLevel(i2);
        }
        if (R()) {
            onLevelChange |= this.t.setLevel(i2);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        return a(iArr, u());
    }

    @DexIgnore
    public Drawable p() {
        Drawable drawable = this.t;
        if (drawable != null) {
            return c7.h(drawable);
        }
        return null;
    }

    @DexIgnore
    public void q(int i2) {
        h(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void r(int i2) {
        i(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void s(int i2) {
        d(m0.b(this.K, i2));
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        if (this.X != i2) {
            this.X = i2;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.Y != colorFilter) {
            this.Y = colorFilter;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        if (this.a0 != colorStateList) {
            this.a0 = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        if (this.b0 != mode) {
            this.b0 = mode;
            this.Z = ds1.a(this, this.a0, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z2, boolean z3) {
        boolean visible = super.setVisible(z2, z3);
        if (Q()) {
            visible |= this.p.setVisible(z2, z3);
        }
        if (P()) {
            visible |= this.z.setVisible(z2, z3);
        }
        if (R()) {
            visible |= this.t.setVisible(z2, z3);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    @DexIgnore
    public void t(int i2) {
        d(this.K.getResources().getBoolean(i2));
    }

    @DexIgnore
    public int[] u() {
        return this.c0;
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    @DexIgnore
    public ColorStateList v() {
        return this.u;
    }

    @DexIgnore
    public TextUtils.TruncateAt w() {
        return this.i0;
    }

    @DexIgnore
    public lr1 x() {
        return this.B;
    }

    @DexIgnore
    public void y(int i2) {
        e(m0.b(this.K, i2));
    }

    @DexIgnore
    public void z(int i2) {
        b(lr1.a(this.K, i2));
    }

    @DexIgnore
    public static zr1 a(Context context, AttributeSet attributeSet, int i2, int i3) {
        zr1 zr1 = new zr1(context);
        zr1.a(attributeSet, i2, i3);
        return zr1;
    }

    @DexIgnore
    public void A(int i2) {
        a(new ys1(this.K, i2));
    }

    @DexIgnore
    public void B(int i2) {
        l(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void C(int i2) {
        m(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public final void d(Canvas canvas, Rect rect) {
        if (this.i > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.M.setColor(this.S);
            this.M.setStyle(Paint.Style.STROKE);
            this.M.setColorFilter(H());
            RectF rectF = this.P;
            float f2 = this.i;
            rectF.set(((float) rect.left) + (f2 / 2.0f), ((float) rect.top) + (f2 / 2.0f), ((float) rect.right) - (f2 / 2.0f), ((float) rect.bottom) - (f2 / 2.0f));
            float f3 = this.g - (this.i / 2.0f);
            canvas.drawRoundRect(this.P, f3, f3, this.M);
        }
    }

    @DexIgnore
    public void i(int i2) {
        b(m0.b(this.K, i2));
    }

    @DexIgnore
    public float j() {
        return this.r;
    }

    @DexIgnore
    public ColorStateList k() {
        return this.q;
    }

    @DexIgnore
    public void l(int i2) {
        e(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public float m() {
        return this.C;
    }

    @DexIgnore
    public void n(int i2) {
        f(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void o(int i2) {
        g(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void p(int i2) {
        d(m0.c(this.K, i2));
    }

    @DexIgnore
    public CharSequence q() {
        return this.w;
    }

    @DexIgnore
    public float r() {
        return this.I;
    }

    @DexIgnore
    public float s() {
        return this.v;
    }

    @DexIgnore
    public float t() {
        return this.H;
    }

    @DexIgnore
    public void u(int i2) {
        a(lr1.a(this.K, i2));
    }

    @DexIgnore
    public void v(int i2) {
        j(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void w(int i2) {
        k(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void x(int i2) {
        this.k0 = i2;
    }

    @DexIgnore
    public float y() {
        return this.E;
    }

    @DexIgnore
    public float z() {
        return this.D;
    }

    @DexIgnore
    public final void b(Canvas canvas, Rect rect) {
        this.M.setColor(this.R);
        this.M.setStyle(Paint.Style.FILL);
        this.M.setColorFilter(H());
        this.P.set(rect);
        RectF rectF = this.P;
        float f2 = this.g;
        canvas.drawRoundRect(rectF, f2, f2, this.M);
    }

    @DexIgnore
    public void i(float f2) {
        if (this.H != f2) {
            this.H = f2;
            invalidateSelf();
            if (R()) {
                N();
            }
        }
    }

    @DexIgnore
    public void j(float f2) {
        if (this.E != f2) {
            float a2 = a();
            this.E = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void k(float f2) {
        if (this.D != f2) {
            float a2 = a();
            this.D = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void l(float f2) {
        if (this.G != f2) {
            this.G = f2;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public void m(float f2) {
        if (this.F != f2) {
            this.F = f2;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet, int i2, int i3) {
        TypedArray c = us1.c(this.K, attributeSet, dr1.Chip, i2, i3, new int[0]);
        a(xs1.a(this.K, c, dr1.Chip_chipBackgroundColor));
        d(c.getDimension(dr1.Chip_chipMinHeight, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        a(c.getDimension(dr1.Chip_chipCornerRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        c(xs1.a(this.K, c, dr1.Chip_chipStrokeColor));
        f(c.getDimension(dr1.Chip_chipStrokeWidth, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        e(xs1.a(this.K, c, dr1.Chip_rippleColor));
        c(c.getText(dr1.Chip_android_text));
        a(xs1.c(this.K, c, dr1.Chip_android_textAppearance));
        int i4 = c.getInt(dr1.Chip_android_ellipsize, 0);
        if (i4 == 1) {
            a(TextUtils.TruncateAt.START);
        } else if (i4 == 2) {
            a(TextUtils.TruncateAt.MIDDLE);
        } else if (i4 == 3) {
            a(TextUtils.TruncateAt.END);
        }
        c(c.getBoolean(dr1.Chip_chipIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") != null)) {
            c(c.getBoolean(dr1.Chip_chipIconEnabled, false));
        }
        c(xs1.b(this.K, c, dr1.Chip_chipIcon));
        b(xs1.a(this.K, c, dr1.Chip_chipIconTint));
        c(c.getDimension(dr1.Chip_chipIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        d(c.getBoolean(dr1.Chip_closeIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") != null)) {
            d(c.getBoolean(dr1.Chip_closeIconEnabled, false));
        }
        d(xs1.b(this.K, c, dr1.Chip_closeIcon));
        d(xs1.a(this.K, c, dr1.Chip_closeIconTint));
        h(c.getDimension(dr1.Chip_closeIconSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        a(c.getBoolean(dr1.Chip_android_checkable, false));
        b(c.getBoolean(dr1.Chip_checkedIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") != null)) {
            b(c.getBoolean(dr1.Chip_checkedIconEnabled, false));
        }
        b(xs1.b(this.K, c, dr1.Chip_checkedIcon));
        b(lr1.a(this.K, c, dr1.Chip_showMotionSpec));
        a(lr1.a(this.K, c, dr1.Chip_hideMotionSpec));
        e(c.getDimension(dr1.Chip_chipStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        k(c.getDimension(dr1.Chip_iconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        j(c.getDimension(dr1.Chip_iconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        m(c.getDimension(dr1.Chip_textStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        l(c.getDimension(dr1.Chip_textEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        i(c.getDimension(dr1.Chip_closeIconStartPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        g(c.getDimension(dr1.Chip_closeIconEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        b(c.getDimension(dr1.Chip_chipEndPadding, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        x(c.getDimensionPixelSize(dr1.Chip_android_maxWidth, Integer.MAX_VALUE));
        c.recycle();
    }

    @DexIgnore
    public final void f(Canvas canvas, Rect rect) {
        this.M.setColor(this.T);
        this.M.setStyle(Paint.Style.FILL);
        this.P.set(rect);
        RectF rectF = this.P;
        float f2 = this.g;
        canvas.drawRoundRect(rectF, f2, f2, this.M);
    }

    @DexIgnore
    public final void b(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (R()) {
            float f2 = this.J + this.I + this.v + this.H + this.G;
            if (c7.e(this) == 0) {
                rectF.right = ((float) rect.right) - f2;
            } else {
                rectF.left = ((float) rect.left) + f2;
            }
        }
    }

    @DexIgnore
    public static boolean f(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    @DexIgnore
    public final float c() {
        this.L.getFontMetrics(this.O);
        Paint.FontMetrics fontMetrics = this.O;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    @DexIgnore
    public final void d(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (R()) {
            float f2 = this.J + this.I + this.v + this.H + this.G;
            if (c7.e(this) == 0) {
                rectF.right = (float) rect.right;
                rectF.left = rectF.right - f2;
            } else {
                int i2 = rect.left;
                rectF.left = (float) i2;
                rectF.right = ((float) i2) + f2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public final void e(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.l != null) {
            float a2 = this.C + a() + this.F;
            float b2 = this.J + b() + this.G;
            if (c7.e(this) == 0) {
                rectF.left = ((float) rect.left) + a2;
                rectF.right = ((float) rect.right) - b2;
            } else {
                rectF.left = ((float) rect.left) + b2;
                rectF.right = ((float) rect.right) - a2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    @DexIgnore
    public static boolean f(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    @DexIgnore
    public final void c(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (R()) {
            float f2 = this.J + this.I;
            if (c7.e(this) == 0) {
                rectF.right = ((float) rect.right) - f2;
                rectF.left = rectF.right - this.v;
            } else {
                rectF.left = ((float) rect.left) + f2;
                rectF.right = rectF.left + this.v;
            }
            float exactCenterY = rect.exactCenterY();
            float f3 = this.v;
            rectF.top = exactCenterY - (f3 / 2.0f);
            rectF.bottom = rectF.top + f3;
        }
    }

    @DexIgnore
    public ColorStateList f() {
        return this.e;
    }

    @DexIgnore
    public void f(float f2) {
        if (this.i != f2) {
            this.i = f2;
            this.M.setStrokeWidth(f2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public static boolean b(ys1 ys1) {
        if (ys1 != null) {
            ColorStateList colorStateList = ys1.b;
            return colorStateList != null && colorStateList.isStateful();
        }
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.q != colorStateList) {
            this.q = colorStateList;
            if (Q()) {
                c7.a(this.p, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void f(int i2) {
        b(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void h(int i2) {
        c(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void h(float f2) {
        if (this.v != f2) {
            this.v = f2;
            invalidateSelf();
            if (R()) {
                N();
            }
        }
    }

    @DexIgnore
    public void d(int i2) {
        a(m0.b(this.K, i2));
    }

    @DexIgnore
    public void d(float f2) {
        if (this.f != f2) {
            this.f = f2;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public final void e(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback((Drawable.Callback) null);
        }
    }

    @DexIgnore
    public float g() {
        return this.g;
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        if (this.w != charSequence) {
            this.w = x7.b().a(charSequence);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.h != colorStateList) {
            this.h = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void e(int i2) {
        a(this.K.getResources().getDimension(i2));
    }

    @DexIgnore
    public void g(int i2) {
        c(m0.c(this.K, i2));
    }

    @DexIgnore
    public void e(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            S();
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void g(float f2) {
        if (this.I != f2) {
            this.I = f2;
            invalidateSelf();
            if (R()) {
                N();
            }
        }
    }

    @DexIgnore
    public float h() {
        return this.J;
    }

    @DexIgnore
    public void b(boolean z2) {
        if (this.y != z2) {
            boolean P2 = P();
            this.y = z2;
            boolean P3 = P();
            if (P2 != P3) {
                if (P3) {
                    a(this.z);
                } else {
                    e(this.z);
                }
                invalidateSelf();
                N();
            }
        }
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (this.k != charSequence) {
            this.k = charSequence;
            this.l = x7.b().a(charSequence);
            this.g0 = true;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public void d(boolean z2) {
        if (this.s != z2) {
            boolean R2 = R();
            this.s = z2;
            boolean R3 = R();
            if (R2 != R3) {
                if (R3) {
                    a(this.t);
                } else {
                    e(this.t);
                }
                invalidateSelf();
                N();
            }
        }
    }

    @DexIgnore
    public Drawable e() {
        return this.z;
    }

    @DexIgnore
    public void e(float f2) {
        if (this.C != f2) {
            this.C = f2;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public void c(boolean z2) {
        if (this.o != z2) {
            boolean Q2 = Q();
            this.o = z2;
            boolean Q3 = Q();
            if (Q2 != Q3) {
                if (Q3) {
                    a(this.p);
                } else {
                    e(this.p);
                }
                invalidateSelf();
                N();
            }
        }
    }

    @DexIgnore
    public void e(boolean z2) {
        this.j0 = z2;
    }

    @DexIgnore
    public void b(int i2) {
        b(m0.c(this.K, i2));
    }

    @DexIgnore
    public void d(Drawable drawable) {
        Drawable p2 = p();
        if (p2 != drawable) {
            float b2 = b();
            this.t = drawable != null ? c7.i(drawable).mutate() : null;
            float b3 = b();
            e(p2);
            if (R()) {
                a(this.t);
            }
            invalidateSelf();
            if (b2 != b3) {
                N();
            }
        }
    }

    @DexIgnore
    public void b(Drawable drawable) {
        if (this.z != drawable) {
            float a2 = a();
            this.z = drawable;
            float a3 = a();
            e(this.z);
            a(this.z);
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void c(Drawable drawable) {
        Drawable i2 = i();
        if (i2 != drawable) {
            float a2 = a();
            this.p = drawable != null ? c7.i(drawable).mutate() : null;
            float a3 = a();
            e(i2);
            if (Q()) {
                a(this.p);
            }
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void b(lr1 lr1) {
        this.A = lr1;
    }

    @DexIgnore
    public void d(ColorStateList colorStateList) {
        if (this.u != colorStateList) {
            this.u = colorStateList;
            if (R()) {
                c7.a(this.t, colorStateList);
            }
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void b(float f2) {
        if (this.J != f2) {
            this.J = f2;
            invalidateSelf();
            N();
        }
    }

    @DexIgnore
    public void c(float f2) {
        if (this.r != f2) {
            float a2 = a();
            this.r = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void c(int i2) {
        b(this.K.getResources().getBoolean(i2));
    }

    @DexIgnore
    public void a(b bVar) {
        this.f0 = new WeakReference<>(bVar);
    }

    @DexIgnore
    public void a(RectF rectF) {
        d(getBounds(), rectF);
    }

    @DexIgnore
    public float a() {
        if (Q() || P()) {
            return this.D + this.r + this.E;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float a(CharSequence charSequence) {
        return charSequence == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.L.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public final void a(Canvas canvas, Rect rect) {
        if (P()) {
            a(rect, this.P);
            RectF rectF = this.P;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.z.setBounds(0, 0, (int) this.P.width(), (int) this.P.height());
            this.z.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    @DexIgnore
    public final void a(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (Q() || P()) {
            float f2 = this.C + this.D;
            if (c7.e(this) == 0) {
                rectF.left = ((float) rect.left) + f2;
                rectF.right = rectF.left + this.r;
            } else {
                rectF.right = ((float) rect.right) - f2;
                rectF.left = rectF.right - this.r;
            }
            float exactCenterY = rect.exactCenterY();
            float f3 = this.r;
            rectF.top = exactCenterY - (f3 / 2.0f);
            rectF.bottom = rectF.top + f3;
        }
    }

    @DexIgnore
    public Paint.Align a(Rect rect, PointF pointF) {
        pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        Paint.Align align = Paint.Align.LEFT;
        if (this.l != null) {
            float a2 = this.C + a() + this.F;
            if (c7.e(this) == 0) {
                pointF.x = ((float) rect.left) + a2;
                align = Paint.Align.LEFT;
            } else {
                pointF.x = ((float) rect.right) - a2;
                align = Paint.Align.RIGHT;
            }
            pointF.y = ((float) rect.centerY()) - c();
        }
        return align;
    }

    @DexIgnore
    public boolean a(int[] iArr) {
        if (Arrays.equals(this.c0, iArr)) {
            return false;
        }
        this.c0 = iArr;
        if (R()) {
            return a(getState(), iArr);
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d8  */
    public final boolean a(int[] iArr, int[] iArr2) {
        int i2;
        boolean z2;
        boolean z3;
        ColorStateList colorStateList;
        float a2;
        boolean onStateChange = super.onStateChange(iArr);
        ColorStateList colorStateList2 = this.e;
        int i3 = 0;
        int colorForState = colorStateList2 != null ? colorStateList2.getColorForState(iArr, this.R) : 0;
        if (this.R != colorForState) {
            this.R = colorForState;
            onStateChange = true;
        }
        ColorStateList colorStateList3 = this.h;
        int colorForState2 = colorStateList3 != null ? colorStateList3.getColorForState(iArr, this.S) : 0;
        if (this.S != colorForState2) {
            this.S = colorForState2;
            onStateChange = true;
        }
        ColorStateList colorStateList4 = this.e0;
        int colorForState3 = colorStateList4 != null ? colorStateList4.getColorForState(iArr, this.T) : 0;
        if (this.T != colorForState3) {
            this.T = colorForState3;
            if (this.d0) {
                onStateChange = true;
            }
        }
        ys1 ys1 = this.m;
        if (ys1 != null) {
            ColorStateList colorStateList5 = ys1.b;
            if (colorStateList5 != null) {
                i2 = colorStateList5.getColorForState(iArr, this.U);
                if (this.U != i2) {
                    this.U = i2;
                    onStateChange = true;
                }
                z2 = !a(getState(), 16842912) && this.x;
                if (!(this.V == z2 || this.z == null)) {
                    a2 = a();
                    this.V = z2;
                    if (a2 == a()) {
                        onStateChange = true;
                        z3 = true;
                        colorStateList = this.a0;
                        if (colorStateList != null) {
                            i3 = colorStateList.getColorForState(iArr, this.W);
                        }
                        if (this.W != i3) {
                            this.W = i3;
                            this.Z = ds1.a(this, this.a0, this.b0);
                            onStateChange = true;
                        }
                        if (f(this.p)) {
                            onStateChange |= this.p.setState(iArr);
                        }
                        if (f(this.z)) {
                            onStateChange |= this.z.setState(iArr);
                        }
                        if (f(this.t)) {
                            onStateChange |= this.t.setState(iArr2);
                        }
                        if (onStateChange) {
                            invalidateSelf();
                        }
                        if (z3) {
                            N();
                        }
                        return onStateChange;
                    }
                    onStateChange = true;
                }
                z3 = false;
                colorStateList = this.a0;
                if (colorStateList != null) {
                }
                if (this.W != i3) {
                }
                if (f(this.p)) {
                }
                if (f(this.z)) {
                }
                if (f(this.t)) {
                }
                if (onStateChange) {
                }
                if (z3) {
                }
                return onStateChange;
            }
        }
        i2 = 0;
        if (this.U != i2) {
        }
        if (!a(getState(), 16842912)) {
        }
        a2 = a();
        this.V = z2;
        if (a2 == a()) {
        }
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            c7.a(drawable, c7.e(this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.t) {
                if (drawable.isStateful()) {
                    drawable.setState(u());
                }
                c7.a(drawable, this.u);
            } else if (drawable.isStateful()) {
                drawable.setState(getState());
            }
        }
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i2) {
        if (iArr == null) {
            return false;
        }
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.e != colorStateList) {
            this.e = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public void a(float f2) {
        if (this.g != f2) {
            this.g = f2;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void a(ys1 ys1) {
        if (this.m != ys1) {
            this.m = ys1;
            if (ys1 != null) {
                ys1.c(this.K, this.L, this.n);
                this.g0 = true;
            }
            onStateChange(getState());
            N();
        }
    }

    @DexIgnore
    public void a(TextUtils.TruncateAt truncateAt) {
        this.i0 = truncateAt;
    }

    @DexIgnore
    public void a(int i2) {
        a(this.K.getResources().getBoolean(i2));
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.x != z2) {
            this.x = z2;
            float a2 = a();
            if (!z2 && this.V) {
                this.V = false;
            }
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                N();
            }
        }
    }

    @DexIgnore
    public void a(lr1 lr1) {
        this.B = lr1;
    }
}
