package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u42 implements Factory<PortfolioApp> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public u42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static u42 a(o42 o42) {
        return new u42(o42);
    }

    @DexIgnore
    public static PortfolioApp b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static PortfolioApp c(o42 o42) {
        PortfolioApp b = o42.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    public PortfolioApp get() {
        return b(this.a);
    }
}
