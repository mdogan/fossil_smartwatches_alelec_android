package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l60 extends k60 {
    @DexIgnore
    public /* final */ int g; // = 1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l60(byte[] bArr, int i, GattCharacteristic.CharacteristicId characteristicId) {
        super(bArr, i, characteristicId);
        wd4.b(bArr, "data");
        wd4.b(characteristicId, "characteristicId");
    }

    @DexIgnore
    public byte[] c() {
        return new byte[]{(byte) (h() % 256)};
    }

    @DexIgnore
    public int f() {
        return this.g;
    }
}
