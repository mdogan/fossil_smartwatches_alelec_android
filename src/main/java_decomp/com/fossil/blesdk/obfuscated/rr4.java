package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pr4;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import retrofit2.Call;
import retrofit2.HttpException;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rr4 extends pr4.a {
    @DexIgnore
    public static /* final */ pr4.a a; // = new rr4();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R> implements pr4<R, CompletableFuture<R>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr4$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.rr4$a$a  reason: collision with other inner class name */
        public class C0098a extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call e;

            @DexIgnore
            public C0098a(a aVar, Call call) {
                this.e = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.e.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements qr4<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture e;

            @DexIgnore
            public b(a aVar, CompletableFuture completableFuture) {
                this.e = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.e.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, cs4<R> cs4) {
                if (cs4.d()) {
                    this.e.complete(cs4.a());
                } else {
                    this.e.completeExceptionally(new HttpException(cs4));
                }
            }
        }

        @DexIgnore
        public a(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<R> a(Call<R> call) {
            C0098a aVar = new C0098a(this, call);
            call.a(new b(this, aVar));
            return aVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<R> implements pr4<R, CompletableFuture<cs4<R>>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends CompletableFuture<cs4<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call e;

            @DexIgnore
            public a(b bVar, Call call) {
                this.e = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.e.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr4$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.rr4$b$b  reason: collision with other inner class name */
        public class C0099b implements qr4<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture e;

            @DexIgnore
            public C0099b(b bVar, CompletableFuture completableFuture) {
                this.e = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.e.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, cs4<R> cs4) {
                this.e.complete(cs4);
            }
        }

        @DexIgnore
        public b(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<cs4<R>> a(Call<R> call) {
            a aVar = new a(this, call);
            call.a(new C0099b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore
    public pr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (pr4.a.a(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = pr4.a.a(0, (ParameterizedType) type);
            if (pr4.a.a(a2) != cs4.class) {
                return new a(a2);
            }
            if (a2 instanceof ParameterizedType) {
                return new b(pr4.a.a(0, (ParameterizedType) a2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
