package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jv2 implements Factory<tv2> {
    @DexIgnore
    public static tv2 a(hv2 hv2) {
        tv2 b = hv2.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
