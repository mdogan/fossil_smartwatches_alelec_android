package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class u9 implements View.OnTouchListener {
    @DexIgnore
    public static /* final */ int v; // = ViewConfiguration.getTapTimeout();
    @DexIgnore
    public /* final */ a e; // = new a();
    @DexIgnore
    public /* final */ Interpolator f; // = new AccelerateInterpolator();
    @DexIgnore
    public /* final */ View g;
    @DexIgnore
    public Runnable h;
    @DexIgnore
    public float[] i; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] j; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float[] m; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] n; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] o; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;
        @DexIgnore
        public long e; // = Long.MIN_VALUE;
        @DexIgnore
        public long f; // = 0;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public long i; // = -1;
        @DexIgnore
        public float j;
        @DexIgnore
        public int k;

        @DexIgnore
        public final float a(float f2) {
            return (-4.0f * f2 * f2) + (f2 * 4.0f);
        }

        @DexIgnore
        public void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public void b(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public int c() {
            return this.h;
        }

        @DexIgnore
        public int d() {
            float f2 = this.c;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public int e() {
            float f2 = this.d;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public boolean f() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        @DexIgnore
        public void g() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = u9.a((int) (currentAnimationTimeMillis - this.e), 0, this.b);
            this.j = a(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        @DexIgnore
        public void h() {
            this.e = AnimationUtils.currentAnimationTimeMillis();
            this.i = -1;
            this.f = this.e;
            this.j = 0.5f;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public final float a(long j2) {
            if (j2 < this.e) {
                return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            long j3 = this.i;
            if (j3 < 0 || j2 < j3) {
                return u9.a(((float) (j2 - this.e)) / ((float) this.a), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f) * 0.5f;
            }
            long j4 = j2 - j3;
            float f2 = this.j;
            return (1.0f - f2) + (f2 * u9.a(((float) j4) / ((float) this.k), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f));
        }

        @DexIgnore
        public int b() {
            return this.g;
        }

        @DexIgnore
        public void a() {
            if (this.f != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float a2 = a(a(currentAnimationTimeMillis));
                this.f = currentAnimationTimeMillis;
                float f2 = ((float) (currentAnimationTimeMillis - this.f)) * a2;
                this.g = (int) (this.c * f2);
                this.h = (int) (f2 * this.d);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        @DexIgnore
        public void a(float f2, float f3) {
            this.c = f2;
            this.d = f3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            u9 u9Var = u9.this;
            if (u9Var.s) {
                if (u9Var.q) {
                    u9Var.q = false;
                    u9Var.e.h();
                }
                a aVar = u9.this.e;
                if (aVar.f() || !u9.this.c()) {
                    u9.this.s = false;
                    return;
                }
                u9 u9Var2 = u9.this;
                if (u9Var2.r) {
                    u9Var2.r = false;
                    u9Var2.a();
                }
                aVar.a();
                u9.this.a(aVar.b(), aVar.c());
                g9.a(u9.this.g, (Runnable) this);
            }
        }
    }

    @DexIgnore
    public u9(View view) {
        this.g = view;
        float f2 = Resources.getSystem().getDisplayMetrics().density;
        float f3 = (float) ((int) ((1575.0f * f2) + 0.5f));
        c(f3, f3);
        float f4 = (float) ((int) ((f2 * 315.0f) + 0.5f));
        d(f4, f4);
        d(1);
        b(Float.MAX_VALUE, Float.MAX_VALUE);
        e(0.2f, 0.2f);
        f(1.0f, 1.0f);
        c(v);
        f(500);
        e(500);
    }

    @DexIgnore
    public static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    @DexIgnore
    public static int a(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    @DexIgnore
    public u9 a(boolean z) {
        if (this.t && !z) {
            b();
        }
        this.t = z;
        return this;
    }

    @DexIgnore
    public abstract void a(int i2, int i3);

    @DexIgnore
    public abstract boolean a(int i2);

    @DexIgnore
    public u9 b(float f2, float f3) {
        float[] fArr = this.j;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public abstract boolean b(int i2);

    @DexIgnore
    public u9 c(float f2, float f3) {
        float[] fArr = this.o;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public u9 d(float f2, float f3) {
        float[] fArr = this.n;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public u9 e(float f2, float f3) {
        float[] fArr = this.i;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public u9 f(float f2, float f3) {
        float[] fArr = this.m;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x0058;
     */
    @DexIgnore
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.t) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                }
            }
            b();
            if (!this.u && this.s) {
                return true;
            }
        }
        this.r = true;
        this.p = false;
        this.e.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.g.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.g.getHeight()));
        if (!this.s && c()) {
            d();
        }
        return !this.u ? false : false;
    }

    @DexIgnore
    public final void b() {
        if (this.q) {
            this.s = false;
        } else {
            this.e.g();
        }
    }

    @DexIgnore
    public u9 c(int i2) {
        this.l = i2;
        return this;
    }

    @DexIgnore
    public u9 d(int i2) {
        this.k = i2;
        return this;
    }

    @DexIgnore
    public u9 e(int i2) {
        this.e.a(i2);
        return this;
    }

    @DexIgnore
    public u9 f(int i2) {
        this.e.b(i2);
        return this;
    }

    @DexIgnore
    public final float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.i[i2], f3, this.j[i2], f2);
        int i3 = (a2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (a2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
        if (i3 == 0) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f5 = this.m[i2];
        float f6 = this.n[i2];
        float f7 = this.o[i2];
        float f8 = f5 * f4;
        if (i3 > 0) {
            return a(a2 * f8, f6, f7);
        }
        return -a((-a2) * f8, f6, f7);
    }

    @DexIgnore
    public boolean c() {
        a aVar = this.e;
        int e2 = aVar.e();
        int d = aVar.d();
        return (e2 != 0 && b(e2)) || (d != 0 && a(d));
    }

    @DexIgnore
    public final void d() {
        if (this.h == null) {
            this.h = new b();
        }
        this.s = true;
        this.q = true;
        if (!this.p) {
            int i2 = this.l;
            if (i2 > 0) {
                g9.a(this.g, this.h, (long) i2);
                this.p = true;
            }
        }
        this.h.run();
        this.p = true;
    }

    @DexIgnore
    public final float a(float f2, float f3, float f4, float f5) {
        float f6;
        float a2 = a(f2 * f3, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4);
        float a3 = a(f3 - f5, a2) - a(f5, a2);
        if (a3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f6 = -this.f.getInterpolation(-a3);
        } else if (a3 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f6 = this.f.getInterpolation(a3);
        }
        return a(f6, -1.0f, 1.0f);
    }

    @DexIgnore
    public final float a(float f2, float f3) {
        if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i2 = this.k;
        if (i2 == 0 || i2 == 1) {
            if (f2 < f3) {
                if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return 1.0f - (f2 / f3);
                }
                if (!this.s || this.k != 1) {
                    return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                return 1.0f;
            }
        } else if (i2 == 2 && f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return f2 / (-f3);
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void a() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        this.g.onTouchEvent(obtain);
        obtain.recycle();
    }
}
