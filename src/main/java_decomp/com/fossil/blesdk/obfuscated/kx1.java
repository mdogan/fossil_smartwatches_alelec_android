package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class kx1 implements Handler.Callback {
    @DexIgnore
    public /* final */ jx1 a;

    @DexIgnore
    public kx1(jx1 jx1) {
        this.a = jx1;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.a.a(message);
    }
}
