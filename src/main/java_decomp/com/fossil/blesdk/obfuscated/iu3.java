package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.Arrays;
import java.util.Calendar;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iu3 extends LinearLayout implements CustomSwipeRefreshLayout.c {
    @DexIgnore
    public ViewGroup e;
    @DexIgnore
    public ImageView f;
    @DexIgnore
    public FlexibleTextView g;
    @DexIgnore
    public FlexibleTextView h;
    @DexIgnore
    public FlexibleTextView i;
    @DexIgnore
    public FlexibleTextView j;
    @DexIgnore
    public String k;
    @DexIgnore
    public String l;
    @DexIgnore
    public yn m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ iu3 a;

        @DexIgnore
        public b(iu3 iu3) {
            this.a = iu3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            xn<Drawable> a2 = this.a.getMRequestManager$app_fossilRelease().a(str2);
            ImageView ivDevice$app_fossilRelease = this.a.getIvDevice$app_fossilRelease();
            if (ivDevice$app_fossilRelease != null) {
                a2.a(ivDevice$app_fossilRelease);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public iu3(Context context) {
        super(context);
        wd4.b(context, "context");
        yn d = sn.d(PortfolioApp.W.c());
        wd4.a((Object) d, "Glide.with(PortfolioApp.instance)");
        this.m = d;
        setWillNotDraw(false);
        a();
    }

    @DexIgnore
    public final void a() {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_head_card, (ViewGroup) null);
        if (inflate != null) {
            this.e = (ViewGroup) inflate;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            ViewGroup viewGroup = this.e;
            if (viewGroup != null) {
                this.g = (FlexibleTextView) viewGroup.findViewById(R.id.ftvDeviceName);
                this.h = (FlexibleTextView) viewGroup.findViewById(R.id.ftvStatus);
                this.i = (FlexibleTextView) viewGroup.findViewById(R.id.ftvLastSynced);
                this.j = (FlexibleTextView) viewGroup.findViewById(R.id.ftvNoDevice);
                this.f = (ImageView) viewGroup.findViewById(R.id.ivDevice);
                addView(viewGroup, layoutParams);
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @DexIgnore
    public final ImageView getIvDevice$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final yn getMRequestManager$app_fossilRelease() {
        return this.m;
    }

    @DexIgnore
    public final void setIvDevice$app_fossilRelease(ImageView imageView) {
        this.f = imageView;
    }

    @DexIgnore
    public final void setMRequestManager$app_fossilRelease(yn ynVar) {
        wd4.b(ynVar, "<set-?>");
        this.m = ynVar;
    }

    @DexIgnore
    public void a(CustomSwipeRefreshLayout.e eVar, CustomSwipeRefreshLayout.e eVar2) {
        wd4.b(eVar, "currentState");
        wd4.b(eVar2, "lastState");
        int a2 = eVar.a();
        int a3 = eVar2.a();
        if (a2 != a3) {
            if (a2 == 0) {
                FlexibleTextView flexibleTextView = this.h;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(tm2.a(getContext(), (int) R.string.DesignPatterns_Pulldown_SyncHold_Text__PullDownToSync));
                }
            } else if (a2 != 1) {
                if (a2 == 2) {
                    FlexibleTextView flexibleTextView2 = this.h;
                    if (flexibleTextView2 != null) {
                        flexibleTextView2.setText(tm2.a(getContext(), (int) R.string.DesignPatterns_Pulldown_SyncSyncingPulldown_Text__Syncing));
                    }
                } else if (a2 == 3) {
                    FlexibleTextView flexibleTextView3 = this.h;
                    if (flexibleTextView3 != null) {
                        flexibleTextView3.setText(tm2.a(getContext(), (int) R.string.Dashboard_Refresh_Updated__Updated));
                    }
                }
            } else if (a3 != 1) {
                FlexibleTextView flexibleTextView4 = this.h;
                if (flexibleTextView4 != null) {
                    flexibleTextView4.setText(tm2.a(getContext(), (int) R.string.DesignPatterns_Pulldown_SyncHold_Text__ReleaseToSync));
                }
            }
            a(this.k, this.l);
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        String str3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeadViewCard", "updateStaticInfo - deviceSerial=" + str);
        this.k = str;
        this.l = str2;
        PortfolioApp c = PortfolioApp.W.c();
        if (TextUtils.isEmpty(str)) {
            FlexibleTextView flexibleTextView = this.j;
            if (flexibleTextView != null) {
                flexibleTextView.setVisibility(0);
                return;
            }
            return;
        }
        FlexibleTextView flexibleTextView2 = this.j;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setVisibility(8);
        }
        FlexibleTextView flexibleTextView3 = this.g;
        if (flexibleTextView3 != null) {
            flexibleTextView3.setText(str2);
        }
        long g2 = new fn2(c).g(c.e());
        if (g2 <= 0) {
            str3 = tm2.a((Context) c, (int) R.string.empty_hour_min);
            wd4.a((Object) str3, "LanguageHelper.getString\u2026 R.string.empty_hour_min)");
        } else {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "calendar");
            instance.setTimeInMillis(g2);
            Boolean s = sk2.s(instance.getTime());
            wd4.a((Object) s, "DateHelper.isToday(calendar.time)");
            if (s.booleanValue()) {
                be4 be4 = be4.a;
                String a2 = tm2.a((Context) c, (int) R.string.DashboardDiana_Main_StepsToday_Title__TodayMonthDate);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026ay_Title__TodayMonthDate)");
                Object[] objArr = {sk2.g(instance.getTime())};
                str3 = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) str3, "java.lang.String.format(format, *args)");
            } else {
                Calendar instance2 = Calendar.getInstance();
                wd4.a((Object) instance2, "Calendar.getInstance()");
                long timeInMillis = (instance2.getTimeInMillis() - g2) / 3600000;
                long j2 = (long) 24;
                if (timeInMillis < j2) {
                    be4 be42 = be4.a;
                    String a3 = tm2.a((Context) c, (int) R.string.s_h_ago);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    Object[] objArr2 = {String.valueOf(timeInMillis)};
                    str3 = String.format(a3, Arrays.copyOf(objArr2, objArr2.length));
                    wd4.a((Object) str3, "java.lang.String.format(format, *args)");
                } else {
                    be4 be43 = be4.a;
                    String a4 = tm2.a((Context) c, (int) R.string.s_h_ago);
                    wd4.a((Object) a4, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    Object[] objArr3 = {String.valueOf(timeInMillis / j2)};
                    str3 = String.format(a4, Arrays.copyOf(objArr3, objArr3.length));
                    wd4.a((Object) str3, "java.lang.String.format(format, *args)");
                }
            }
        }
        FlexibleTextView flexibleTextView4 = this.i;
        if (flexibleTextView4 != null) {
            be4 be44 = be4.a;
            String a5 = tm2.a((Context) c, (int) R.string.Profile_MyWatch_HybridProfile_Text__LastSyncedDayTime);
            wd4.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
            Object[] objArr4 = {str3};
            String format = String.format(a5, Arrays.copyOf(objArr4, objArr4.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView4.setText(format);
        }
        FlexibleTextView flexibleTextView5 = this.i;
        if (flexibleTextView5 != null) {
            flexibleTextView5.setSelected(true);
        }
        CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
        if (str != null) {
            CloudImageHelper.ItemImage type = with.setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.f;
            if (imageView != null) {
                type.setPlaceHolder(imageView, DeviceHelper.o.b(str, DeviceHelper.ImageStyle.SMALL)).setImageCallback(new b(this)).download();
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
