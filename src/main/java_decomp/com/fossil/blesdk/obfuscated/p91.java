package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p91 implements w91 {
    @DexIgnore
    public final v91 a(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        return false;
    }
}
