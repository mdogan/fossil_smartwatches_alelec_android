package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wv extends zv<Bitmap> {
    @DexIgnore
    public wv(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void c(Bitmap bitmap) {
        ((ImageView) this.e).setImageBitmap(bitmap);
    }
}
