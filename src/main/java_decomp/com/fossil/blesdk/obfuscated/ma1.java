package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ma1 implements v91 {
    @DexIgnore
    public /* final */ x91 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public ma1(x91 x91, String str, Object[] objArr) {
        this.a = x91;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        char c2 = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c2 |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.d = c2 | (charAt2 << i);
                return;
            }
        }
    }

    @DexIgnore
    public final x91 a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return (this.d & 2) == 2;
    }

    @DexIgnore
    public final int c() {
        return (this.d & 1) == 1 ? u81.e.i : u81.e.j;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final Object[] e() {
        return this.c;
    }
}
