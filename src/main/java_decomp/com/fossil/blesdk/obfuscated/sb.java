package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sb extends xb {
    @DexIgnore
    void a(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void b(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void c(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void d(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void e(LifecycleOwner lifecycleOwner);

    @DexIgnore
    void f(LifecycleOwner lifecycleOwner);
}
