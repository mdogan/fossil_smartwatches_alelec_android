package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class y61 implements w61 {
    @DexIgnore
    public /* final */ x61 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public y61(x61 x61, String str) {
        this.a = x61;
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        return this.a.c(this.b);
    }
}
