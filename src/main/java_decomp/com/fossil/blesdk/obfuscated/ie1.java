package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ie1 {
    @DexIgnore
    public /* final */ pe1 a;

    @DexIgnore
    public ie1(pe1 pe1) {
        this.a = pe1;
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.d(z);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
