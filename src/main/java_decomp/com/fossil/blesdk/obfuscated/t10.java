package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t10 extends GattOperationResult {
    @DexIgnore
    public /* final */ Peripheral.HIDState b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t10(GattOperationResult.GattResult gattResult, Peripheral.HIDState hIDState, Peripheral.HIDState hIDState2) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        wd4.b(hIDState, "previousHIDState");
        wd4.b(hIDState2, "newState");
        this.b = hIDState2;
    }

    @DexIgnore
    public final Peripheral.HIDState b() {
        return this.b;
    }
}
