package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n13 implements Factory<r43> {
    @DexIgnore
    public static r43 a(k13 k13) {
        r43 c = k13.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
