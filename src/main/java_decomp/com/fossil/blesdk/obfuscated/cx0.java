package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzco;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cx0 {
    @DexIgnore
    public static /* final */ cx0 f; // = new cx0(0, new int[0], new Object[0], false);
    @DexIgnore
    public int a;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public cx0() {
        this(0, new int[8], new Object[8], true);
    }

    @DexIgnore
    public cx0(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    @DexIgnore
    public static cx0 a(cx0 cx0, cx0 cx02) {
        int i = cx0.a + cx02.a;
        int[] copyOf = Arrays.copyOf(cx0.b, i);
        System.arraycopy(cx02.b, 0, copyOf, cx0.a, cx02.a);
        Object[] copyOf2 = Arrays.copyOf(cx0.c, i);
        System.arraycopy(cx02.c, 0, copyOf2, cx0.a, cx02.a);
        return new cx0(i, copyOf, copyOf2, true);
    }

    @DexIgnore
    public static void a(int i, Object obj, px0 px0) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            px0.b(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            px0.zzc(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            px0.a(i2, (zzbb) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                px0.zzf(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzco.zzbn());
        } else if (px0.a() == su0.e.k) {
            px0.b(i2);
            ((cx0) obj).b(px0);
            px0.a(i2);
        } else {
            px0.a(i2);
            ((cx0) obj).b(px0);
            px0.b(i2);
        }
    }

    @DexIgnore
    public static cx0 d() {
        return f;
    }

    @DexIgnore
    public static cx0 e() {
        return new cx0();
    }

    @DexIgnore
    public final int a() {
        int i;
        int i2 = this.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.a; i4++) {
            int i5 = this.b[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zzbn.e(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 1) {
                i = zzbn.g(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 2) {
                i = zzbn.c(i6, (zzbb) this.c[i4]);
            } else if (i7 == 3) {
                i = (zzbn.e(i6) << 1) + ((cx0) this.c[i4]).a();
            } else if (i7 == 5) {
                i = zzbn.i(i6, ((Integer) this.c[i4]).intValue());
            } else {
                throw new IllegalStateException(zzco.zzbn());
            }
            i3 += i;
        }
        this.d = i3;
        return i3;
    }

    @DexIgnore
    public final void a(int i, Object obj) {
        if (this.e) {
            int i2 = this.a;
            if (i2 == this.b.length) {
                int i3 = this.a + (i2 < 4 ? 8 : i2 >> 1);
                this.b = Arrays.copyOf(this.b, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr = this.b;
            int i4 = this.a;
            iArr[i4] = i;
            this.c[i4] = obj;
            this.a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        if (px0.a() == su0.e.l) {
            for (int i = this.a - 1; i >= 0; i--) {
                px0.zza(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            px0.zza(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            wv0.a(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    @DexIgnore
    public final int b() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += zzbn.d(this.b[i3] >>> 3, (zzbb) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    @DexIgnore
    public final void b(px0 px0) throws IOException {
        if (this.a != 0) {
            if (px0.a() == su0.e.k) {
                for (int i = 0; i < this.a; i++) {
                    a(this.b[i], this.c[i], px0);
                }
                return;
            }
            for (int i2 = this.a - 1; i2 >= 0; i2--) {
                a(this.b[i2], this.c[i2], px0);
            }
        }
    }

    @DexIgnore
    public final void c() {
        this.e = false;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof cx0)) {
            return false;
        }
        cx0 cx0 = (cx0) obj;
        int i = this.a;
        if (i == cx0.a) {
            int[] iArr = this.b;
            int[] iArr2 = cx0.b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = cx0.c;
                int i3 = this.a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }
}
