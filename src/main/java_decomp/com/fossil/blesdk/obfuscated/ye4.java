package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.xe4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ye4<T, R> extends xe4<R>, jd4<T, R> {

    @DexIgnore
    public interface a<T, R> extends xe4.b<R>, jd4<T, R> {
    }

    @DexIgnore
    R get(T t);

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    a<T, R> getGetter();
}
