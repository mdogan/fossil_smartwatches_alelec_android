package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ys2 extends sd<GoalTrackingData, a> {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public b d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ ys2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ys2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ys2$a$a  reason: collision with other inner class name */
        public static final class C0108a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0108a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    a aVar = this.e;
                    GoalTrackingData a = ys2.a(aVar.d, aVar.getAdapterPosition());
                    if (a != null) {
                        b a2 = this.e.d.d;
                        if (a2 != null) {
                            wd4.a((Object) a, "it1");
                            a2.a(a);
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ys2 ys2, View view) {
            super(view);
            wd4.b(view, "view");
            this.d = ys2;
            this.a = (FlexibleTextView) view.findViewById(R.id.ftv_time);
            this.b = (FlexibleTextView) view.findViewById(R.id.ftv_no_time);
            this.c = (FlexibleTextView) view.findViewById(R.id.ftv_delete);
            this.c.setOnClickListener(new C0108a(this));
        }

        @DexIgnore
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            wd4.b(goalTrackingData, "item");
            if (this.d.c == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + jl2.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = jl2.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                wd4.a((Object) str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            FlexibleTextView flexibleTextView = this.a;
            wd4.a((Object) flexibleTextView, "mTvTime");
            be4 be4 = be4.a;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.s_time_zone);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            Object[] objArr = {sk2.a(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView.setText(format);
            FlexibleTextView flexibleTextView2 = this.b;
            wd4.a((Object) flexibleTextView2, "mTvNoTime");
            flexibleTextView2.setVisibility(8);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ys2(b bVar, b62 b62) {
        super(b62);
        wd4.b(b62, "goalTrackingDataDiff");
        this.d = bVar;
        TimeZone timeZone = TimeZone.getDefault();
        wd4.a((Object) timeZone, "TimeZone.getDefault()");
        this.c = sk2.a(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData a(ys2 ys2, int i) {
        return (GoalTrackingData) ys2.a(i);
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recorded_goal_tracking, viewGroup, false);
        wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wd4.b(aVar, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) a(i);
        if (goalTrackingData != null) {
            aVar.a(goalTrackingData);
        }
    }
}
