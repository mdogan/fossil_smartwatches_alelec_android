package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qs2;
import com.fossil.blesdk.obfuscated.vs2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h63 extends as2 implements v63, xs3.g {
    @DexIgnore
    public ur3<ld2> j;
    @DexIgnore
    public u63 k;
    @DexIgnore
    public qs2 l;
    @DexIgnore
    public vs2 m;
    @DexIgnore
    public k42 n;
    @DexIgnore
    public HybridCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ h63 a;

        @DexIgnore
        public b(h63 h63) {
            this.a = h63;
        }

        @DexIgnore
        public void a(MicroApp microApp) {
            wd4.b(microApp, "microApp");
            h63.a(this.a).a(microApp.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements qs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ h63 a;

        @DexIgnore
        public c(h63 h63) {
            this.a = h63;
        }

        @DexIgnore
        public void a() {
            h63.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wd4.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "onItemClicked category=" + category);
            h63.a(this.a).a(category);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ h63 e;

        @DexIgnore
        public d(h63 h63) {
            this.e = h63;
        }

        @DexIgnore
        public final void onClick(View view) {
            h63.a(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ h63 e;

        @DexIgnore
        public e(h63 h63) {
            this.e = h63;
        }

        @DexIgnore
        public final void onClick(View view) {
            h63.a(this.e).i();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ u63 a(h63 h63) {
        u63 u63 = h63.k;
        if (u63 != null) {
            return u63;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        es3 es3 = es3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager, "childFragmentManager");
                        es3.B(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    es3 es32 = es3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wd4.a((Object) childFragmentManager2, "childFragmentManager");
                    es32.A(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                es3 es33 = es3.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                wd4.a((Object) childFragmentManager3, "childFragmentManager");
                es33.u(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    public void F(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "MicroAppsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            vs2 vs2 = this.m;
            if (vs2 != null) {
                vs2.b();
            } else {
                wd4.d("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(MicroApp microApp) {
        if (microApp != null) {
            vs2 vs2 = this.m;
            if (vs2 != null) {
                vs2.b(microApp.getId());
                c(microApp);
                return;
            }
            wd4.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(MicroApp microApp) {
        ur3<ld2> ur3 = this.j;
        if (ur3 != null) {
            ld2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                wd4.a((Object) flexibleTextView, "binding.tvSelectedMicroApp");
                flexibleTextView.setText(tm2.a(PortfolioApp.W.c(), microApp.getNameKey(), microApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.s;
                wd4.a((Object) flexibleTextView2, "binding.tvMicroAppDetail");
                flexibleTextView2.setText(tm2.a(PortfolioApp.W.c(), microApp.getDescriptionKey(), microApp.getDescription()));
                vs2 vs2 = this.m;
                if (vs2 != null) {
                    int a3 = vs2.a(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.r.i(a3);
                        return;
                    }
                    return;
                }
                wd4.d("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(String str) {
        wd4.b(str, "category");
        ur3<ld2> ur3 = this.j;
        if (ur3 != null) {
            ld2 a2 = ur3.a();
            if (a2 != null) {
                qs2 qs2 = this.l;
                if (qs2 != null) {
                    int a3 = qs2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        qs2 qs22 = this.l;
                        if (qs22 != null) {
                            qs22.a(a3);
                            a2.q.j(a3);
                            return;
                        }
                        wd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        os3.a aVar = os3.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            wd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        os3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        PermissionActivity.a aVar2 = PermissionActivity.C;
                        Context context = getContext();
                        if (context != null) {
                            wd4.a((Object) context, "context!!");
                            aVar2.a(context, ob4.a((T[]) new Integer[]{10}));
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            k42 k42 = this.n;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) hybridCustomizeEditActivity, (lc.b) k42).a(HybridCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (HybridCustomizeViewModel) a2;
                u63 u63 = this.k;
                if (u63 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.o;
                    if (hybridCustomizeViewModel != null) {
                        u63.a(hybridCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onActivityResult requestCode " + i + " resultCode " + i2);
        if (i != 100) {
            if (i != 102) {
                if (i != 104) {
                    if (i == 106 && i2 == -1 && intent != null) {
                        CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING");
                        if (commuteTimeSetting != null) {
                            u63 u63 = this.k;
                            if (u63 != null) {
                                u63.a(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), sj2.a(commuteTimeSetting));
                            } else {
                                wd4.d("mPresenter");
                                throw null;
                            }
                        }
                    }
                } else if (intent != null && i2 == -1) {
                    Ringtone ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE");
                    if (ringtone != null) {
                        u63 u632 = this.k;
                        if (u632 != null) {
                            u632.a(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), sj2.a(ringtone));
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    u63 u633 = this.k;
                    if (u633 != null) {
                        wd4.a((Object) stringExtra, "selectedMicroAppId");
                        u633.a(stringExtra);
                        return;
                    }
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (i2 == -1 && intent != null) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE");
            if (secondTimezoneSetting != null) {
                u63 u634 = this.k;
                if (u634 != null) {
                    u634.a(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), sj2.a(secondTimezoneSetting));
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ld2 ld2 = (ld2) ra.a(layoutInflater, R.layout.fragment_micro_app, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new w63(this)).a(this);
        this.j = new ur3<>(this, ld2);
        wd4.a((Object) ld2, "binding");
        return ld2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        u63 u63 = this.k;
        if (u63 != null) {
            u63.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        u63 u63 = this.k;
        if (u63 != null) {
            u63.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        vs2 vs2 = new vs2((ArrayList) null, (vs2.c) null, 3, (rd4) null);
        vs2.a((vs2.c) new b(this));
        this.m = vs2;
        qs2 qs2 = new qs2((ArrayList) null, (qs2.c) null, 3, (rd4) null);
        qs2.a((qs2.c) new c(this));
        this.l = qs2;
        ur3<ld2> ur3 = this.j;
        if (ur3 != null) {
            ld2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                qs2 qs22 = this.l;
                if (qs22 != null) {
                    recyclerView.setAdapter(qs22);
                    RecyclerView recyclerView2 = a2.r;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    vs2 vs22 = this.m;
                    if (vs22 != null) {
                        recyclerView2.setAdapter(vs22);
                        a2.t.setOnClickListener(new d(this));
                        a2.u.setOnClickListener(new e(this));
                        return;
                    }
                    wd4.d("mMicroAppAdapter");
                    throw null;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void w(List<MicroApp> list) {
        wd4.b(list, "microApps");
        vs2 vs2 = this.m;
        if (vs2 != null) {
            vs2.a(list);
        } else {
            wd4.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void z(String str) {
        wd4.b(str, "content");
        ur3<ld2> ur3 = this.j;
        if (ur3 != null) {
            ld2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.tvMicroAppDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(u63 u63) {
        wd4.b(u63, "presenter");
        this.k = u63;
    }

    @DexIgnore
    public void a(List<Category> list) {
        wd4.b(list, "categories");
        qs2 qs2 = this.l;
        if (qs2 != null) {
            qs2.a(list);
        } else {
            wd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, String str3) {
        wd4.b(str, "microAppId");
        wd4.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        ur3<ld2> ur3 = this.j;
        if (ur3 != null) {
            ld2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                wd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.t;
                wd4.a((Object) flexibleTextView2, "it.tvMicroAppPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.u;
                    wd4.a((Object) flexibleTextView3, "it.tvMicroAppSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.u;
                        wd4.a((Object) flexibleTextView4, "it.tvMicroAppSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.u;
                    wd4.a((Object) flexibleTextView5, "it.tvMicroAppSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.u;
                wd4.a((Object) flexibleTextView6, "it.tvMicroAppSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (br4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        wd4.b(str, "title");
        wd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            ur3<ld2> ur3 = this.j;
            if (ur3 != null) {
                ld2 a2 = ur3.a();
                if (a2 == null) {
                    return;
                }
                if (i2 == 0 || i == i2) {
                    FlexibleTextView flexibleTextView = a2.t;
                    wd4.a((Object) flexibleTextView, "it.tvMicroAppPermission");
                    flexibleTextView.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.v;
                    wd4.a((Object) flexibleTextView2, "it.tvPermissionOrder");
                    flexibleTextView2.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.u;
                wd4.a((Object) flexibleTextView3, "it.tvMicroAppSetting");
                flexibleTextView3.setVisibility(8);
                FlexibleTextView flexibleTextView4 = a2.t;
                wd4.a((Object) flexibleTextView4, "it.tvMicroAppPermission");
                flexibleTextView4.setVisibility(0);
                FlexibleTextView flexibleTextView5 = a2.t;
                wd4.a((Object) flexibleTextView5, "it.tvMicroAppPermission");
                flexibleTextView5.setText(str);
                if (str2.length() > 0) {
                    FlexibleTextView flexibleTextView6 = a2.s;
                    wd4.a((Object) flexibleTextView6, "it.tvMicroAppDetail");
                    flexibleTextView6.setText(str2);
                }
                if (i2 > 1) {
                    FlexibleTextView flexibleTextView7 = a2.v;
                    wd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                    flexibleTextView7.setVisibility(0);
                    FlexibleTextView flexibleTextView8 = a2.v;
                    wd4.a((Object) flexibleTextView8, "it.tvPermissionOrder");
                    be4 be4 = be4.a;
                    String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                    Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView8.setText(format);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (wd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (activity != null) {
                    ((BaseActivity) activity).l();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (wd4.a((Object) str, (Object) es3.c.a()) && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (!br4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
            } else if (activity2 != null) {
                ((BaseActivity) activity2).l();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        wd4.b(str, "topMicroApp");
        wd4.b(str2, "middleMicroApp");
        wd4.b(str3, "bottomMicroApp");
        SearchMicroAppActivity.C.a(this, str, str2, str3);
    }
}
