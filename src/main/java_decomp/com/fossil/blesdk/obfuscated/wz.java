package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.w54;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wz implements iz {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public w54 c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements w54.d {
        @DexIgnore
        public /* final */ /* synthetic */ byte[] a;
        @DexIgnore
        public /* final */ /* synthetic */ int[] b;

        @DexIgnore
        public a(wz wzVar, byte[] bArr, int[] iArr) {
            this.a = bArr;
            this.b = iArr;
        }

        @DexIgnore
        public void a(InputStream inputStream, int i) throws IOException {
            try {
                inputStream.read(this.a, this.b[0], i);
                int[] iArr = this.b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public /* final */ byte[] a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(wz wzVar, byte[] bArr, int i) {
            this.a = bArr;
            this.b = i;
        }
    }

    @DexIgnore
    public wz(File file, int i) {
        this.a = file;
        this.b = i;
    }

    @DexIgnore
    public void a(long j, String str) {
        f();
        b(j, str);
    }

    @DexIgnore
    public py b() {
        b e = e();
        if (e == null) {
            return null;
        }
        return py.a(e.a, 0, e.b);
    }

    @DexIgnore
    public byte[] c() {
        b e = e();
        if (e == null) {
            return null;
        }
        return e.a;
    }

    @DexIgnore
    public void d() {
        a();
        this.a.delete();
    }

    @DexIgnore
    public final b e() {
        if (!this.a.exists()) {
            return null;
        }
        f();
        w54 w54 = this.c;
        if (w54 == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[w54.D()];
        try {
            this.c.a((w54.d) new a(this, bArr, iArr));
        } catch (IOException e) {
            r44.g().e("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", e);
        }
        return new b(this, bArr, iArr[0]);
    }

    @DexIgnore
    public final void f() {
        if (this.c == null) {
            try {
                this.c = new w54(this.a);
            } catch (IOException e) {
                z44 g = r44.g();
                g.e("CrashlyticsCore", "Could not open log file: " + this.a, e);
            }
        }
    }

    @DexIgnore
    public void a() {
        CommonUtils.a((Closeable) this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    @DexIgnore
    public final void b(long j, String str) {
        if (this.c != null) {
            if (str == null) {
                str = "null";
            }
            try {
                int i = this.b / 4;
                if (str.length() > i) {
                    str = "..." + str.substring(str.length() - i);
                }
                this.c.a(String.format(Locale.US, "%d %s%n", new Object[]{Long.valueOf(j), str.replaceAll("\r", " ").replaceAll("\n", " ")}).getBytes("UTF-8"));
                while (!this.c.z() && this.c.D() > this.b) {
                    this.c.C();
                }
            } catch (IOException e) {
                r44.g().e("CrashlyticsCore", "There was a problem writing to the Crashlytics log.", e);
            }
        }
    }
}
