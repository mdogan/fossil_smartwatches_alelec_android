package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.kv3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xw3 implements ex3 {
    @DexIgnore
    public /* final */ vw3 a;
    @DexIgnore
    public /* final */ tw3 b;

    @DexIgnore
    public xw3(vw3 vw3, tw3 tw3) {
        this.a = vw3;
        this.b = tw3;
    }

    @DexIgnore
    public jp4 a(iv3 iv3, long j) throws IOException {
        if ("chunked".equalsIgnoreCase(iv3.a("Transfer-Encoding"))) {
            return this.b.f();
        }
        if (j != -1) {
            return this.b.a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void b() throws IOException {
        if (d()) {
            this.b.h();
        } else {
            this.b.b();
        }
    }

    @DexIgnore
    public kv3.b c() throws IOException {
        return this.b.i();
    }

    @DexIgnore
    public boolean d() {
        if (!"close".equalsIgnoreCase(this.a.f().a("Connection")) && !"close".equalsIgnoreCase(this.a.g().a("Connection")) && !this.b.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final kp4 b(kv3 kv3) throws IOException {
        if (!vw3.b(kv3)) {
            return this.b.b(0);
        }
        if ("chunked".equalsIgnoreCase(kv3.a("Transfer-Encoding"))) {
            return this.b.a(this.a);
        }
        long a2 = yw3.a(kv3);
        if (a2 != -1) {
            return this.b.b(a2);
        }
        return this.b.g();
    }

    @DexIgnore
    public void a() throws IOException {
        this.b.c();
    }

    @DexIgnore
    public void a(bx3 bx3) throws IOException {
        this.b.a(bx3);
    }

    @DexIgnore
    public void a(iv3 iv3) throws IOException {
        this.a.o();
        this.b.a(iv3.c(), ax3.a(iv3, this.a.e().e().b().type(), this.a.e().d()));
    }

    @DexIgnore
    public lv3 a(kv3 kv3) throws IOException {
        return new zw3(kv3.g(), ep4.a(b(kv3)));
    }
}
