package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rn0 extends pn0 {
    @DexIgnore
    public /* final */ Callable<String> e;

    @DexIgnore
    public rn0(Callable<String> callable) {
        super(false, (String) null, (Throwable) null);
        this.e = callable;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.e.call();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
