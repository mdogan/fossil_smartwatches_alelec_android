package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import androidx.work.WorkInfo;
import com.fossil.blesdk.obfuscated.il;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kl implements jl {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ mf b;
    @DexIgnore
    public /* final */ xf c;
    @DexIgnore
    public /* final */ xf d;
    @DexIgnore
    public /* final */ xf e;
    @DexIgnore
    public /* final */ xf f;
    @DexIgnore
    public /* final */ xf g;
    @DexIgnore
    public /* final */ xf h;
    @DexIgnore
    public /* final */ xf i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf<il> {
        @DexIgnore
        public a(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(lg lgVar, il ilVar) {
            String str = ilVar.a;
            if (str == null) {
                lgVar.a(1);
            } else {
                lgVar.a(1, str);
            }
            lgVar.b(2, (long) ol.a(ilVar.b));
            String str2 = ilVar.c;
            if (str2 == null) {
                lgVar.a(3);
            } else {
                lgVar.a(3, str2);
            }
            String str3 = ilVar.d;
            if (str3 == null) {
                lgVar.a(4);
            } else {
                lgVar.a(4, str3);
            }
            byte[] a = bj.a(ilVar.e);
            if (a == null) {
                lgVar.a(5);
            } else {
                lgVar.a(5, a);
            }
            byte[] a2 = bj.a(ilVar.f);
            if (a2 == null) {
                lgVar.a(6);
            } else {
                lgVar.a(6, a2);
            }
            lgVar.b(7, ilVar.g);
            lgVar.b(8, ilVar.h);
            lgVar.b(9, ilVar.i);
            lgVar.b(10, (long) ilVar.k);
            lgVar.b(11, (long) ol.a(ilVar.l));
            lgVar.b(12, ilVar.m);
            lgVar.b(13, ilVar.n);
            lgVar.b(14, ilVar.o);
            lgVar.b(15, ilVar.p);
            zi ziVar = ilVar.j;
            if (ziVar != null) {
                lgVar.b(16, (long) ol.a(ziVar.b()));
                lgVar.b(17, ziVar.g() ? 1 : 0);
                lgVar.b(18, ziVar.h() ? 1 : 0);
                lgVar.b(19, ziVar.f() ? 1 : 0);
                lgVar.b(20, ziVar.i() ? 1 : 0);
                lgVar.b(21, ziVar.c());
                lgVar.b(22, ziVar.d());
                byte[] a3 = ol.a(ziVar.a());
                if (a3 == null) {
                    lgVar.a(23);
                } else {
                    lgVar.a(23, a3);
                }
            } else {
                lgVar.a(16);
                lgVar.a(17);
                lgVar.a(18);
                lgVar.a(19);
                lgVar.a(20);
                lgVar.a(21);
                lgVar.a(22);
                lgVar.a(23);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec`(`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xf {
        @DexIgnore
        public b(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends xf {
        @DexIgnore
        public c(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends xf {
        @DexIgnore
        public d(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends xf {
        @DexIgnore
        public e(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends xf {
        @DexIgnore
        public f(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends xf {
        @DexIgnore
        public g(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends xf {
        @DexIgnore
        public h(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends xf {
        @DexIgnore
        public i(kl klVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public kl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
        this.e = new d(this, roomDatabase);
        this.f = new e(this, roomDatabase);
        this.g = new f(this, roomDatabase);
        this.h = new g(this, roomDatabase);
        this.i = new h(this, roomDatabase);
        new i(this, roomDatabase);
    }

    @DexIgnore
    public void a(il ilVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(ilVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c(String str) {
        vf b2 = vf.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int d() {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.i.acquire();
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.i.release(acquire);
        }
    }

    @DexIgnore
    public il e(String str) {
        vf vfVar;
        il ilVar;
        String str2 = str;
        vf b2 = vf.b("SELECT * FROM workspec WHERE id=?", 1);
        if (str2 == null) {
            b2.a(1);
        } else {
            b2.a(1, str2);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            int b3 = bg.b(a2, "id");
            int b4 = bg.b(a2, "state");
            int b5 = bg.b(a2, "worker_class_name");
            int b6 = bg.b(a2, "input_merger_class_name");
            int b7 = bg.b(a2, "input");
            int b8 = bg.b(a2, "output");
            int b9 = bg.b(a2, "initial_delay");
            int b10 = bg.b(a2, "interval_duration");
            int b11 = bg.b(a2, "flex_duration");
            int b12 = bg.b(a2, "run_attempt_count");
            int b13 = bg.b(a2, "backoff_policy");
            int b14 = bg.b(a2, "backoff_delay_duration");
            int b15 = bg.b(a2, "period_start_time");
            int b16 = bg.b(a2, "minimum_retention_duration");
            vfVar = b2;
            try {
                int b17 = bg.b(a2, "schedule_requested_at");
                int b18 = bg.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = bg.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = bg.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = bg.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = bg.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = bg.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = bg.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = bg.b(a2, "content_uri_triggers");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b3);
                    String string2 = a2.getString(b5);
                    int i9 = b9;
                    zi ziVar = new zi();
                    ziVar.a(ol.b(a2.getInt(b18)));
                    ziVar.b(a2.getInt(b19) != 0);
                    ziVar.c(a2.getInt(b20) != 0);
                    ziVar.a(a2.getInt(b21) != 0);
                    ziVar.d(a2.getInt(b22) != 0);
                    ziVar.a(a2.getLong(b23));
                    ziVar.b(a2.getLong(b24));
                    ziVar.a(ol.a(a2.getBlob(b25)));
                    ilVar = new il(string, string2);
                    ilVar.b = ol.c(a2.getInt(b4));
                    ilVar.d = a2.getString(b6);
                    ilVar.e = bj.b(a2.getBlob(b7));
                    ilVar.f = bj.b(a2.getBlob(b8));
                    ilVar.g = a2.getLong(i9);
                    ilVar.h = a2.getLong(i8);
                    ilVar.i = a2.getLong(i7);
                    ilVar.k = a2.getInt(i6);
                    ilVar.l = ol.a(a2.getInt(i5));
                    ilVar.m = a2.getLong(i4);
                    ilVar.n = a2.getLong(i3);
                    ilVar.o = a2.getLong(i2);
                    ilVar.p = a2.getLong(b17);
                    ilVar.j = ziVar;
                } else {
                    ilVar = null;
                }
                a2.close();
                vfVar.c();
                return ilVar;
            } catch (Throwable th) {
                th = th;
                a2.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b2;
            a2.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int f(String str) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.g.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    public List<bj> g(String str) {
        vf b2 = vf.b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(bj.b(a2.getBlob(0)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int h(String str) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.f.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    public void a(String str, bj bjVar) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.d.acquire();
        byte[] a2 = bj.a(bjVar);
        if (a2 == null) {
            acquire.a(1);
        } else {
            acquire.a(1, a2);
        }
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    public WorkInfo.State d(String str) {
        vf b2 = vf.b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? ol.c(a2.getInt(0)) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public void b(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.e.acquire();
        acquire.b(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c() {
        vf b2 = vf.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int a(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        lg acquire = this.h.acquire();
        acquire.b(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    public List<il> b() {
        vf vfVar;
        vf b2 = vf.b("SELECT * FROM workspec WHERE state=1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            int b3 = bg.b(a2, "id");
            int b4 = bg.b(a2, "state");
            int b5 = bg.b(a2, "worker_class_name");
            int b6 = bg.b(a2, "input_merger_class_name");
            int b7 = bg.b(a2, "input");
            int b8 = bg.b(a2, "output");
            int b9 = bg.b(a2, "initial_delay");
            int b10 = bg.b(a2, "interval_duration");
            int b11 = bg.b(a2, "flex_duration");
            int b12 = bg.b(a2, "run_attempt_count");
            int b13 = bg.b(a2, "backoff_policy");
            int b14 = bg.b(a2, "backoff_delay_duration");
            int b15 = bg.b(a2, "period_start_time");
            int b16 = bg.b(a2, "minimum_retention_duration");
            vfVar = b2;
            try {
                int b17 = bg.b(a2, "schedule_requested_at");
                int b18 = bg.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = bg.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = bg.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = bg.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = bg.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = bg.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = bg.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = bg.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    zi ziVar = new zi();
                    int i13 = b18;
                    ziVar.a(ol.b(a2.getInt(b18)));
                    ziVar.b(a2.getInt(b19) != 0);
                    ziVar.c(a2.getInt(b20) != 0);
                    ziVar.a(a2.getInt(b21) != 0);
                    ziVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    ziVar.a(a2.getLong(b23));
                    ziVar.b(a2.getLong(b24));
                    ziVar.a(ol.a(a2.getBlob(b25)));
                    il ilVar = new il(string, string2);
                    ilVar.b = ol.c(a2.getInt(b4));
                    ilVar.d = a2.getString(b6);
                    ilVar.e = bj.b(a2.getBlob(b7));
                    int i16 = i10;
                    ilVar.f = bj.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    ilVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    ilVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    ilVar.i = a2.getLong(i20);
                    int i21 = i6;
                    ilVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    ilVar.l = ol.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    ilVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    ilVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    ilVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    ilVar.p = a2.getLong(i29);
                    ilVar.j = ziVar;
                    arrayList.add(ilVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b2;
            a2.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<il.b> a(String str) {
        vf b2 = vf.b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            int b3 = bg.b(a2, "id");
            int b4 = bg.b(a2, "state");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                il.b bVar = new il.b();
                bVar.a = a2.getString(b3);
                bVar.b = ol.c(a2.getInt(b4));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public List<il> a(int i2) {
        vf vfVar;
        vf b2 = vf.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b2.b(1, (long) i2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            int b3 = bg.b(a2, "id");
            int b4 = bg.b(a2, "state");
            int b5 = bg.b(a2, "worker_class_name");
            int b6 = bg.b(a2, "input_merger_class_name");
            int b7 = bg.b(a2, "input");
            int b8 = bg.b(a2, "output");
            int b9 = bg.b(a2, "initial_delay");
            int b10 = bg.b(a2, "interval_duration");
            int b11 = bg.b(a2, "flex_duration");
            int b12 = bg.b(a2, "run_attempt_count");
            int b13 = bg.b(a2, "backoff_policy");
            int b14 = bg.b(a2, "backoff_delay_duration");
            int b15 = bg.b(a2, "period_start_time");
            int b16 = bg.b(a2, "minimum_retention_duration");
            vfVar = b2;
            try {
                int b17 = bg.b(a2, "schedule_requested_at");
                int b18 = bg.b(a2, "required_network_type");
                int i3 = b16;
                int b19 = bg.b(a2, "requires_charging");
                int i4 = b15;
                int b20 = bg.b(a2, "requires_device_idle");
                int i5 = b14;
                int b21 = bg.b(a2, "requires_battery_not_low");
                int i6 = b13;
                int b22 = bg.b(a2, "requires_storage_not_low");
                int i7 = b12;
                int b23 = bg.b(a2, "trigger_content_update_delay");
                int i8 = b11;
                int b24 = bg.b(a2, "trigger_max_content_delay");
                int i9 = b10;
                int b25 = bg.b(a2, "content_uri_triggers");
                int i10 = b9;
                int i11 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i12 = b3;
                    String string2 = a2.getString(b5);
                    int i13 = b5;
                    zi ziVar = new zi();
                    int i14 = b18;
                    ziVar.a(ol.b(a2.getInt(b18)));
                    ziVar.b(a2.getInt(b19) != 0);
                    ziVar.c(a2.getInt(b20) != 0);
                    ziVar.a(a2.getInt(b21) != 0);
                    ziVar.d(a2.getInt(b22) != 0);
                    int i15 = b21;
                    int i16 = b19;
                    ziVar.a(a2.getLong(b23));
                    ziVar.b(a2.getLong(b24));
                    ziVar.a(ol.a(a2.getBlob(b25)));
                    il ilVar = new il(string, string2);
                    ilVar.b = ol.c(a2.getInt(b4));
                    ilVar.d = a2.getString(b6);
                    ilVar.e = bj.b(a2.getBlob(b7));
                    int i17 = i11;
                    ilVar.f = bj.b(a2.getBlob(i17));
                    i11 = i17;
                    int i18 = i16;
                    int i19 = i10;
                    ilVar.g = a2.getLong(i19);
                    i10 = i19;
                    int i20 = i9;
                    ilVar.h = a2.getLong(i20);
                    i9 = i20;
                    int i21 = b20;
                    int i22 = i8;
                    ilVar.i = a2.getLong(i22);
                    int i23 = i7;
                    ilVar.k = a2.getInt(i23);
                    int i24 = i6;
                    i7 = i23;
                    ilVar.l = ol.a(a2.getInt(i24));
                    i8 = i22;
                    int i25 = i5;
                    int i26 = i21;
                    ilVar.m = a2.getLong(i25);
                    int i27 = i25;
                    i6 = i24;
                    int i28 = i4;
                    ilVar.n = a2.getLong(i28);
                    i4 = i28;
                    int i29 = i3;
                    ilVar.o = a2.getLong(i29);
                    i3 = i29;
                    int i30 = i27;
                    int i31 = b17;
                    ilVar.p = a2.getLong(i31);
                    ilVar.j = ziVar;
                    arrayList.add(ilVar);
                    b17 = i31;
                    b19 = i18;
                    b20 = i26;
                    b5 = i13;
                    b21 = i15;
                    b18 = i14;
                    i5 = i30;
                    b3 = i12;
                }
                a2.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b2;
            a2.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<il> a() {
        vf vfVar;
        vf b2 = vf.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = cg.a(this.a, b2, false);
        try {
            int b3 = bg.b(a2, "id");
            int b4 = bg.b(a2, "state");
            int b5 = bg.b(a2, "worker_class_name");
            int b6 = bg.b(a2, "input_merger_class_name");
            int b7 = bg.b(a2, "input");
            int b8 = bg.b(a2, "output");
            int b9 = bg.b(a2, "initial_delay");
            int b10 = bg.b(a2, "interval_duration");
            int b11 = bg.b(a2, "flex_duration");
            int b12 = bg.b(a2, "run_attempt_count");
            int b13 = bg.b(a2, "backoff_policy");
            int b14 = bg.b(a2, "backoff_delay_duration");
            int b15 = bg.b(a2, "period_start_time");
            int b16 = bg.b(a2, "minimum_retention_duration");
            vfVar = b2;
            try {
                int b17 = bg.b(a2, "schedule_requested_at");
                int b18 = bg.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = bg.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = bg.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = bg.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = bg.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = bg.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = bg.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = bg.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    zi ziVar = new zi();
                    int i13 = b18;
                    ziVar.a(ol.b(a2.getInt(b18)));
                    ziVar.b(a2.getInt(b19) != 0);
                    ziVar.c(a2.getInt(b20) != 0);
                    ziVar.a(a2.getInt(b21) != 0);
                    ziVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    ziVar.a(a2.getLong(b23));
                    ziVar.b(a2.getLong(b24));
                    ziVar.a(ol.a(a2.getBlob(b25)));
                    il ilVar = new il(string, string2);
                    ilVar.b = ol.c(a2.getInt(b4));
                    ilVar.d = a2.getString(b6);
                    ilVar.e = bj.b(a2.getBlob(b7));
                    int i16 = i10;
                    ilVar.f = bj.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    ilVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    ilVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    ilVar.i = a2.getLong(i20);
                    int i21 = i6;
                    ilVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    ilVar.l = ol.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    ilVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    ilVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    ilVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    ilVar.p = a2.getLong(i29);
                    ilVar.j = ziVar;
                    arrayList.add(ilVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                vfVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                vfVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            vfVar = b2;
            a2.close();
            vfVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int a(WorkInfo.State state, String... strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = eg.a();
        a2.append("UPDATE workspec SET state=");
        a2.append("?");
        a2.append(" WHERE id IN (");
        eg.a(a2, strArr.length);
        a2.append(")");
        lg compileStatement = this.a.compileStatement(a2.toString());
        compileStatement.b(1, (long) ol.a(state));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.a(i2);
            } else {
                compileStatement.a(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            int n = compileStatement.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
        }
    }
}
