package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ar2 implements CoroutineUseCase.b {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public ar2(int i, String str, boolean z) {
        wd4.b(str, "serial");
        this.a = i;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }
}
