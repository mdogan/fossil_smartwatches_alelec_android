package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dk {
    @DexIgnore
    public static /* final */ String f; // = ej.a("WorkTimer");
    @DexIgnore
    public /* final */ ThreadFactory a; // = new a(this);
    @DexIgnore
    public /* final */ ScheduledExecutorService b; // = Executors.newSingleThreadScheduledExecutor(this.a);
    @DexIgnore
    public /* final */ Map<String, c> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<String, b> d; // = new HashMap();
    @DexIgnore
    public /* final */ Object e; // = new Object();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {
        @DexIgnore
        public int a; // = 0;

        @DexIgnore
        public a(dk dkVar) {
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.a);
            this.a = this.a + 1;
            return newThread;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ dk e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public c(dk dkVar, String str) {
            this.e = dkVar;
            this.f = str;
        }

        @DexIgnore
        public void run() {
            synchronized (this.e.e) {
                if (this.e.c.remove(this.f) != null) {
                    b remove = this.e.d.remove(this.f);
                    if (remove != null) {
                        remove.a(this.f);
                    }
                } else {
                    ej.a().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", new Object[]{this.f}), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, long j, b bVar) {
        synchronized (this.e) {
            ej.a().a(f, String.format("Starting timer for %s", new Object[]{str}), new Throwable[0]);
            a(str);
            c cVar = new c(this, str);
            this.c.put(str, cVar);
            this.d.put(str, bVar);
            this.b.schedule(cVar, j, TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a(String str) {
        synchronized (this.e) {
            if (this.c.remove(str) != null) {
                ej.a().a(f, String.format("Stopping timer for %s", new Object[]{str}), new Throwable[0]);
                this.d.remove(str);
            }
        }
    }

    @DexIgnore
    public void a() {
        if (!this.b.isShutdown()) {
            this.b.shutdownNow();
        }
    }
}
