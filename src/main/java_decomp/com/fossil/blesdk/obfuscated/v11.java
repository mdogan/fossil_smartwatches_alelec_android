package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v11 extends i11 {
    @DexIgnore
    public /* final */ ve0<Status> e;

    @DexIgnore
    public v11(ve0<Status> ve0) {
        this.e = ve0;
    }

    @DexIgnore
    public final void c(Status status) {
        this.e.a(status);
    }
}
