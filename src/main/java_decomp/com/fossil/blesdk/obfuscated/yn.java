package com.fossil.blesdk.obfuscated;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.bumptech.glide.Priority;
import com.fossil.blesdk.obfuscated.mu;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yn implements ComponentCallbacks2, su, vn<xn<Drawable>> {
    @DexIgnore
    public static /* final */ sv q; // = ((sv) sv.b((Class<?>) Bitmap.class).O());
    @DexIgnore
    public /* final */ sn e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ ru g;
    @DexIgnore
    public /* final */ xu h;
    @DexIgnore
    public /* final */ wu i;
    @DexIgnore
    public /* final */ zu j;
    @DexIgnore
    public /* final */ Runnable k;
    @DexIgnore
    public /* final */ Handler l;
    @DexIgnore
    public /* final */ mu m;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<rv<Object>> n;
    @DexIgnore
    public sv o;
    @DexIgnore
    public boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            yn ynVar = yn.this;
            ynVar.g.a(ynVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements mu.a {
        @DexIgnore
        public /* final */ xu a;

        @DexIgnore
        public b(xu xuVar) {
            this.a = xuVar;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                synchronized (yn.this) {
                    this.a.d();
                }
            }
        }
    }

    /*
    static {
        sv svVar = (sv) sv.b((Class<?>) vt.class).O();
        sv svVar2 = (sv) ((sv) sv.b(qp.b).a(Priority.LOW)).a(true);
    }
    */

    @DexIgnore
    public yn(sn snVar, ru ruVar, wu wuVar, Context context) {
        this(snVar, ruVar, wuVar, new xu(), snVar.d(), context);
    }

    @DexIgnore
    public synchronized void a(sv svVar) {
        this.o = (sv) ((sv) svVar.clone()).a();
    }

    @DexIgnore
    public synchronized void b() {
        this.j.b();
        for (cw<?> a2 : this.j.f()) {
            a(a2);
        }
        this.j.e();
        this.h.a();
        this.g.b(this);
        this.g.b(this.m);
        this.l.removeCallbacks(this.k);
        this.e.b(this);
    }

    @DexIgnore
    public synchronized void c() {
        k();
        this.j.c();
    }

    @DexIgnore
    public xn<Bitmap> e() {
        return a(Bitmap.class).a((mv) q);
    }

    @DexIgnore
    public xn<Drawable> f() {
        return a(Drawable.class);
    }

    @DexIgnore
    public List<rv<Object>> g() {
        return this.n;
    }

    @DexIgnore
    public synchronized sv h() {
        return this.o;
    }

    @DexIgnore
    public synchronized void i() {
        this.h.b();
    }

    @DexIgnore
    public synchronized void j() {
        i();
        for (yn i2 : this.i.a()) {
            i2.i();
        }
    }

    @DexIgnore
    public synchronized void k() {
        this.h.c();
    }

    @DexIgnore
    public synchronized void l() {
        this.h.e();
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.p) {
            j();
        }
    }

    @DexIgnore
    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.h + ", treeNode=" + this.i + "}";
    }

    @DexIgnore
    public synchronized void a() {
        l();
        this.j.a();
    }

    @DexIgnore
    public yn(sn snVar, ru ruVar, wu wuVar, xu xuVar, nu nuVar, Context context) {
        this.j = new zu();
        this.k = new a();
        this.l = new Handler(Looper.getMainLooper());
        this.e = snVar;
        this.g = ruVar;
        this.i = wuVar;
        this.h = xuVar;
        this.f = context;
        this.m = nuVar.a(context.getApplicationContext(), new b(xuVar));
        if (vw.c()) {
            this.l.post(this.k);
        } else {
            ruVar.a(this);
        }
        ruVar.a(this.m);
        this.n = new CopyOnWriteArrayList<>(snVar.f().b());
        a(snVar.f().c());
        snVar.a(this);
    }

    @DexIgnore
    public final void c(cw<?> cwVar) {
        boolean b2 = b(cwVar);
        pv d = cwVar.d();
        if (!b2 && !this.e.a(cwVar) && d != null) {
            cwVar.a((pv) null);
            d.clear();
        }
    }

    @DexIgnore
    public xn<Drawable> a(String str) {
        return f().a(str);
    }

    @DexIgnore
    public xn<Drawable> a(Uri uri) {
        return f().a(uri);
    }

    @DexIgnore
    public xn<Drawable> a(Integer num) {
        return f().a(num);
    }

    @DexIgnore
    public xn<Drawable> a(byte[] bArr) {
        return f().a(bArr);
    }

    @DexIgnore
    public xn<Drawable> a(Object obj) {
        return f().a(obj);
    }

    @DexIgnore
    public <ResourceType> xn<ResourceType> a(Class<ResourceType> cls) {
        return new xn<>(this.e, this, cls, this.f);
    }

    @DexIgnore
    public synchronized boolean b(cw<?> cwVar) {
        pv d = cwVar.d();
        if (d == null) {
            return true;
        }
        if (!this.h.a(d)) {
            return false;
        }
        this.j.b(cwVar);
        cwVar.a((pv) null);
        return true;
    }

    @DexIgnore
    public void a(cw<?> cwVar) {
        if (cwVar != null) {
            c(cwVar);
        }
    }

    @DexIgnore
    public synchronized void a(cw<?> cwVar, pv pvVar) {
        this.j.a(cwVar);
        this.h.b(pvVar);
    }

    @DexIgnore
    public <T> zn<?, T> b(Class<T> cls) {
        return this.e.f().a(cls);
    }
}
