package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ps4<T> implements sr4<qm4, T> {
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public ps4(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    public T a(qm4 qm4) throws IOException {
        JsonReader a2 = this.a.a(qm4.A());
        try {
            T read = this.b.read(a2);
            if (a2.Q() == JsonToken.END_DOCUMENT) {
                return read;
            }
            throw new JsonIOException("JSON document was not fully consumed.");
        } finally {
            qm4.close();
        }
    }
}
