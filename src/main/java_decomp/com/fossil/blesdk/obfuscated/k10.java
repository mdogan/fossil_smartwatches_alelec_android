package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k10 extends BluetoothCommand {
    @DexIgnore
    public int k;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k10(Peripheral.c cVar) {
        super(BluetoothCommandId.READ_RSSI, cVar);
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.n();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return gattOperationResult instanceof z10;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        this.k = ((z10) gattOperationResult).b();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().h();
    }

    @DexIgnore
    public final int i() {
        return this.k;
    }
}
