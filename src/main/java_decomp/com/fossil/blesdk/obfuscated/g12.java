package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g12 extends m12 {
    @DexIgnore
    public static volatile g12[] f;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public g12() {
        a();
    }

    @DexIgnore
    public static g12[] b() {
        if (f == null) {
            synchronized (l12.a) {
                if (f == null) {
                    f = new g12[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    public g12 a() {
        this.a = "";
        this.b = "";
        this.c = o12.a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    public g12 a(k12 k12) throws IOException {
        while (true) {
            int j = k12.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                this.a = k12.i();
            } else if (j == 18) {
                this.b = k12.i();
            } else if (j == 26) {
                int a2 = o12.a(k12, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                String[] strArr2 = new String[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < strArr2.length - 1) {
                    strArr2[length] = k12.i();
                    k12.j();
                    length++;
                }
                strArr2[length] = k12.i();
                this.c = strArr2;
            } else if (j == 34) {
                this.d = k12.i();
            } else if (j == 42) {
                this.e = k12.i();
            } else if (j == 48) {
                k12.c();
            } else if (!o12.b(k12, j)) {
                return this;
            }
        }
    }
}
