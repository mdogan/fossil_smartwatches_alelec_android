package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z11 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<z11> CREATOR; // = new a21();
    @DexIgnore
    public /* final */ ap0 e;

    @DexIgnore
    public z11(ap0 ap0) {
        this.e = ap0;
    }

    @DexIgnore
    public final ap0 H() {
        return this.e;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", new Object[]{this.e});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) this.e, i, false);
        lk0.a(parcel, a);
    }
}
