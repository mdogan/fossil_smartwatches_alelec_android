package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xy2 implements Factory<wy2> {
    @DexIgnore
    public /* final */ Provider<Context> a;

    @DexIgnore
    public xy2(Provider<Context> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static xy2 a(Provider<Context> provider) {
        return new xy2(provider);
    }

    @DexIgnore
    public static wy2 b(Provider<Context> provider) {
        return new wy2(provider.get());
    }

    @DexIgnore
    public wy2 get() {
        return b(this.a);
    }
}
