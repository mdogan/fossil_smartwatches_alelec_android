package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l91 extends j91 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public l91() {
        super();
    }

    @DexIgnore
    public static <E> List<E> c(Object obj, long j) {
        return (List) lb1.f(obj, j);
    }

    @DexIgnore
    public final <L> List<L> a(Object obj, long j) {
        return a(obj, j, 10);
    }

    @DexIgnore
    public final void b(Object obj, long j) {
        Object obj2;
        List list = (List) lb1.f(obj, j);
        if (list instanceof i91) {
            obj2 = ((i91) list).C();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof ja1) || !(list instanceof a91)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                a91 a91 = (a91) list;
                if (a91.A()) {
                    a91.B();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        lb1.a(obj, j, obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: com.fossil.blesdk.obfuscated.h91} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: com.fossil.blesdk.obfuscated.h91} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: com.fossil.blesdk.obfuscated.h91} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static <L> List<L> a(Object obj, long j, int i) {
        h91 h91;
        List<L> list;
        List<L> c2 = c(obj, j);
        if (c2.isEmpty()) {
            if (c2 instanceof i91) {
                list = new h91(i);
            } else if (!(c2 instanceof ja1) || !(c2 instanceof a91)) {
                list = new ArrayList<>(i);
            } else {
                list = ((a91) c2).b(i);
            }
            lb1.a(obj, j, (Object) list);
            return list;
        }
        if (c.isAssignableFrom(c2.getClass())) {
            ArrayList arrayList = new ArrayList(c2.size() + i);
            arrayList.addAll(c2);
            lb1.a(obj, j, (Object) arrayList);
            h91 = arrayList;
        } else if (c2 instanceof ib1) {
            h91 h912 = new h91(c2.size() + i);
            h912.addAll((ib1) c2);
            lb1.a(obj, j, (Object) h912);
            h91 = h912;
        } else if (!(c2 instanceof ja1) || !(c2 instanceof a91)) {
            return c2;
        } else {
            a91 a91 = (a91) c2;
            if (a91.A()) {
                return c2;
            }
            a91 b = a91.b(c2.size() + i);
            lb1.a(obj, j, (Object) b);
            return b;
        }
        return h91;
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        List c2 = c(obj2, j);
        List a = a(obj, j, c2.size());
        int size = a.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            a.addAll(c2);
        }
        if (size > 0) {
            c2 = a;
        }
        lb1.a(obj, j, (Object) c2);
    }
}
