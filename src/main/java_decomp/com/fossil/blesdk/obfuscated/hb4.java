package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb4<T> implements Collection<T>, de4 {
    @DexIgnore
    public /* final */ T[] e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore
    public hb4(T[] tArr, boolean z) {
        wd4.b(tArr, "values");
        this.e = tArr;
        this.f = z;
    }

    @DexIgnore
    public int a() {
        return this.e.length;
    }

    @DexIgnore
    public boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return lb4.b(this.e, obj);
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        wd4.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.e.length == 0;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return md4.a(this.e);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public final Object[] toArray() {
        return nb4.a(this.e, this.f);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return qd4.a(this, tArr);
    }
}
