package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qm4 implements Closeable {
    @DexIgnore
    public Reader e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qm4 {
        @DexIgnore
        public /* final */ /* synthetic */ mm4 f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 h;

        @DexIgnore
        public a(mm4 mm4, long j, xo4 xo4) {
            this.f = mm4;
            this.g = j;
            this.h = xo4;
        }

        @DexIgnore
        public long C() {
            return this.g;
        }

        @DexIgnore
        public mm4 D() {
            return this.f;
        }

        @DexIgnore
        public xo4 E() {
            return this.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Reader {
        @DexIgnore
        public /* final */ xo4 e;
        @DexIgnore
        public /* final */ Charset f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Reader h;

        @DexIgnore
        public b(xo4 xo4, Charset charset) {
            this.e = xo4;
            this.f = charset;
        }

        @DexIgnore
        public void close() throws IOException {
            this.g = true;
            Reader reader = this.h;
            if (reader != null) {
                reader.close();
            } else {
                this.e.close();
            }
        }

        @DexIgnore
        public int read(char[] cArr, int i, int i2) throws IOException {
            if (!this.g) {
                Reader reader = this.h;
                if (reader == null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(this.e.m(), vm4.a(this.e, this.f));
                    this.h = inputStreamReader;
                    reader = inputStreamReader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    @DexIgnore
    public static qm4 a(mm4 mm4, String str) {
        Charset charset = vm4.i;
        if (mm4 != null) {
            charset = mm4.a();
            if (charset == null) {
                charset = vm4.i;
                mm4 = mm4.b(mm4 + "; charset=utf-8");
            }
        }
        vo4 vo4 = new vo4();
        vo4.a(str, charset);
        return a(mm4, vo4.B(), vo4);
    }

    @DexIgnore
    public final Reader A() {
        Reader reader = this.e;
        if (reader != null) {
            return reader;
        }
        b bVar = new b(E(), B());
        this.e = bVar;
        return bVar;
    }

    @DexIgnore
    public final Charset B() {
        mm4 D = D();
        return D != null ? D.a(vm4.i) : vm4.i;
    }

    @DexIgnore
    public abstract long C();

    @DexIgnore
    public abstract mm4 D();

    @DexIgnore
    public abstract xo4 E();

    @DexIgnore
    public final String F() throws IOException {
        xo4 E = E();
        try {
            return E.a(vm4.a(E, B()));
        } finally {
            vm4.a((Closeable) E);
        }
    }

    @DexIgnore
    public void close() {
        vm4.a((Closeable) E());
    }

    @DexIgnore
    public final InputStream y() {
        return E().m();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final byte[] z() throws IOException {
        long C = C();
        if (C <= 2147483647L) {
            xo4 E = E();
            try {
                byte[] f = E.f();
                vm4.a((Closeable) E);
                if (C == -1 || C == ((long) f.length)) {
                    return f;
                }
                throw new IOException("Content-Length (" + C + ") and stream length (" + f.length + ") disagree");
            } catch (Throwable th) {
                vm4.a((Closeable) E);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + C);
        }
    }

    @DexIgnore
    public static qm4 a(mm4 mm4, byte[] bArr) {
        vo4 vo4 = new vo4();
        vo4.write(bArr);
        return a(mm4, (long) bArr.length, vo4);
    }

    @DexIgnore
    public static qm4 a(mm4 mm4, long j, xo4 xo4) {
        if (xo4 != null) {
            return new a(mm4, j, xo4);
        }
        throw new NullPointerException("source == null");
    }
}
