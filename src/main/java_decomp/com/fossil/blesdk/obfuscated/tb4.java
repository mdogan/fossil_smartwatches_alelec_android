package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tb4 extends sb4 {
    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, Iterable<? extends T> iterable) {
        wd4.b(collection, "$this$addAll");
        wd4.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        for (Object add : iterable) {
            if (collection.add(add)) {
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static final <T> boolean b(Collection<? super T> collection, Iterable<? extends T> iterable) {
        wd4.b(collection, "$this$retainAll");
        wd4.b(iterable, MessengerShareContentUtility.ELEMENTS);
        return ce4.a((Object) collection).retainAll(pb4.a(iterable, (Iterable<? extends T>) collection));
    }

    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, T[] tArr) {
        wd4.b(collection, "$this$addAll");
        wd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return collection.addAll(kb4.a(tArr));
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, jd4<? super T, Boolean> jd4) {
        wd4.b(iterable, "$this$retainAll");
        wd4.b(jd4, "predicate");
        return a(iterable, jd4, false);
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, jd4<? super T, Boolean> jd4, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (jd4.invoke(it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, jd4<? super T, Boolean> jd4) {
        wd4.b(list, "$this$removeAll");
        wd4.b(jd4, "predicate");
        return a(list, jd4, true);
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, jd4<? super T, Boolean> jd4, boolean z) {
        int i;
        if (list instanceof RandomAccess) {
            int a = ob4.a(list);
            if (a >= 0) {
                int i2 = 0;
                i = 0;
                while (true) {
                    T t = list.get(i2);
                    if (jd4.invoke(t).booleanValue() != z) {
                        if (i != i2) {
                            list.set(i, t);
                        }
                        i++;
                    }
                    if (i2 == a) {
                        break;
                    }
                    i2++;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int a2 = ob4.a(list);
            if (a2 < i) {
                return true;
            }
            while (true) {
                list.remove(a2);
                if (a2 == i) {
                    return true;
                }
                a2--;
            }
        } else if (list != null) {
            return a(ce4.b(list), jd4, z);
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }
}
