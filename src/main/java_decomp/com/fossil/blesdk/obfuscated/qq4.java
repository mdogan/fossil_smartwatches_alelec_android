package com.fossil.blesdk.obfuscated;

import java.util.Queue;
import org.slf4j.Marker;
import org.slf4j.event.Level;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qq4 implements oq4 {
    @DexIgnore
    public String e;
    @DexIgnore
    public uq4 f;
    @DexIgnore
    public Queue<sq4> g;

    @DexIgnore
    public qq4(uq4 uq4, Queue<sq4> queue) {
        this.f = uq4;
        this.e = uq4.c();
        this.g = queue;
    }

    @DexIgnore
    public final void a(Level level, String str, Object[] objArr, Throwable th) {
        a(level, (Marker) null, str, objArr, th);
    }

    @DexIgnore
    public void debug(String str) {
        a(Level.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void error(String str) {
        a(Level.ERROR, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void info(String str) {
        a(Level.INFO, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    public void trace(String str) {
        a(Level.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void warn(String str) {
        a(Level.WARN, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public final void a(Level level, Marker marker, String str, Object[] objArr, Throwable th) {
        sq4 sq4 = new sq4();
        sq4.a(System.currentTimeMillis());
        sq4.a(level);
        sq4.a(this.f);
        sq4.a(this.e);
        sq4.a(marker);
        sq4.b(str);
        sq4.a(objArr);
        sq4.a(th);
        sq4.c(Thread.currentThread().getName());
        this.g.add(sq4);
    }

    @DexIgnore
    public void debug(String str, Throwable th) {
        a(Level.DEBUG, str, (Object[]) null, th);
    }

    @DexIgnore
    public void error(String str, Object... objArr) {
        a(Level.ERROR, str, objArr, (Throwable) null);
    }

    @DexIgnore
    public void info(String str, Object obj) {
        a(Level.INFO, str, new Object[]{obj}, (Throwable) null);
    }

    @DexIgnore
    public void trace(String str, Throwable th) {
        a(Level.TRACE, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Object obj, Object obj2) {
        a(Level.WARN, str, new Object[]{obj, obj2}, (Throwable) null);
    }

    @DexIgnore
    public void error(String str, Throwable th) {
        a(Level.ERROR, str, (Object[]) null, th);
    }

    @DexIgnore
    public void info(String str, Throwable th) {
        a(Level.INFO, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Throwable th) {
        a(Level.WARN, str, (Object[]) null, th);
    }
}
