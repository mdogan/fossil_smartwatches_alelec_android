package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface e94<T> {
    @DexIgnore
    void onError(Throwable th);

    @DexIgnore
    void onSubscribe(j94 j94);
}
