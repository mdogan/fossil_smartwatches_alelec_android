package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.rt2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c03 extends as2 implements b03, xs3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public a03 j;
    @DexIgnore
    public rt2 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return c03.m;
        }

        @DexIgnore
        public final c03 b() {
            return new c03();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements rt2.a {
        @DexIgnore
        public /* final */ /* synthetic */ c03 a;

        @DexIgnore
        public b(c03 c03) {
            this.a = c03;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            wd4.b(appWrapper, "appWrapper");
            if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == c03.b(this.a).h()) {
                c03.b(this.a).a(appWrapper, z);
                return;
            }
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                es3.a(childFragmentManager, title, appWrapper.getCurrentHandGroup(), c03.b(this.a).h(), appWrapper);
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ c03 e;

        @DexIgnore
        public c(c03 c03) {
            this.e = c03;
        }

        @DexIgnore
        public final void onClick(View view) {
            c03.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ c03 e;
        @DexIgnore
        public /* final */ /* synthetic */ be2 f;

        @DexIgnore
        public d(c03 c03, be2 be2) {
            this.e = c03;
            this.f = be2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.s;
            wd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            c03.a(this.e).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ be2 e;

        @DexIgnore
        public e(be2 be2) {
            this.e = be2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = c03.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ rt2 a(c03 c03) {
        rt2 rt2 = c03.k;
        if (rt2 != null) {
            return rt2;
        }
        wd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ a03 b(c03 c03) {
        a03 a03 = c03.j;
        if (a03 != null) {
            return a03;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        a03 a03 = this.j;
        if (a03 != null) {
            a03.i();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        be2 be2 = (be2) ra.a(layoutInflater, R.layout.fragment_notification_hybrid_app, viewGroup, false, O0());
        be2.r.setOnClickListener(new c(this));
        be2.q.addTextChangedListener(new d(this, be2));
        be2.s.setOnClickListener(new e(be2));
        rt2 rt2 = new rt2();
        rt2.a((rt2.a) new b(this));
        this.k = rt2;
        RecyclerView recyclerView = be2.t;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        rt2 rt22 = this.k;
        if (rt22 != null) {
            recyclerView.setAdapter(rt22);
            new ur3(this, be2);
            wd4.a((Object) be2, "binding");
            return be2.d();
        }
        wd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        a03 a03 = this.j;
        if (a03 != null) {
            a03.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a03 a03 = this.j;
        if (a03 != null) {
            a03.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(a03 a03) {
        wd4.b(a03, "presenter");
        this.j = a03;
    }

    @DexIgnore
    public void a(List<AppWrapper> list, int i) {
        wd4.b(list, "listAppWrapper");
        rt2 rt2 = this.k;
        if (rt2 != null) {
            rt2.a(list, i);
        } else {
            wd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<String> arrayList) {
        wd4.b(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i != R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                AppWrapper appWrapper = (AppWrapper) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER");
                if (appWrapper != null) {
                    a03 a03 = this.j;
                    if (a03 != null) {
                        a03.a(appWrapper, false);
                        rt2 rt2 = this.k;
                        if (rt2 != null) {
                            rt2.notifyDataSetChanged();
                        } else {
                            wd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null) {
                AppWrapper appWrapper2 = (AppWrapper) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER");
                if (appWrapper2 != null) {
                    a03 a032 = this.j;
                    if (a032 != null) {
                        a032.a(appWrapper2, true);
                        rt2 rt22 = this.k;
                        if (rt22 != null) {
                            rt22.notifyDataSetChanged();
                        } else {
                            wd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
