package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kf1 {
    @DexIgnore
    public /* final */ h51 a;

    @DexIgnore
    public kf1(h51 h51) {
        ck0.a(h51);
        this.a = h51;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof kf1)) {
            return false;
        }
        try {
            return this.a.a(((kf1) obj).a);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.k();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
