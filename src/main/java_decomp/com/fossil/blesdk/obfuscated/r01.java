package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r01 extends oz0 implements p01 {
    @DexIgnore
    public r01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    public final void a(iq0 iq0) throws RemoteException {
        Parcel o = o();
        z01.a(o, (Parcelable) iq0);
        b(1, o);
    }
}
