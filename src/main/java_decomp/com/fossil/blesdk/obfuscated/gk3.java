package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gk3 extends as2 implements fk3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ek3 j;
    @DexIgnore
    public ur3<nf2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gk3.m;
        }

        @DexIgnore
        public final gk3 b() {
            return new gk3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ gk3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ nf2 e;

            @DexIgnore
            public a(nf2 nf2) {
                this.e = nf2;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.e.C;
                wd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.D;
                wd4.a((Object) flexibleTextView2, "it.tvSignup");
                flexibleTextView2.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, gk3 gk3) {
            this.e = view;
            this.f = gk3;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                nf2 nf2 = (nf2) gk3.a(this.f).a();
                if (nf2 != null) {
                    try {
                        FlexibleTextView flexibleTextView = nf2.C;
                        wd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = nf2.D;
                        wd4.a((Object) flexibleTextView2, "it.tvSignup");
                        flexibleTextView2.setVisibility(8);
                        cb4 cb4 = cb4.a;
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = gk3.n.a();
                        local.d(a2, "onCreateView - e=" + e2);
                        cb4 cb42 = cb4.a;
                    }
                }
            } else {
                nf2 nf22 = (nf2) gk3.a(this.f).a();
                if (nf22 != null) {
                    try {
                        wd4.a((Object) nf22, "it");
                        Boolean.valueOf(nf22.d().postDelayed(new a(nf22), 100));
                    } catch (Exception e3) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = gk3.n.a();
                        local2.d(a3, "onCreateView - e=" + e3);
                        cb4 cb43 = cb4.a;
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public c(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().m();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public d(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.G;
                wd4.a((Object) activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public e(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 a;

        @DexIgnore
        public f(gk3 gk3) {
            this.a = gk3;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(gk3.n.a(), "Password DONE key, trigger login flow");
            this.a.U0();
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nf2 e;
        @DexIgnore
        public /* final */ /* synthetic */ gk3 f;

        @DexIgnore
        public g(nf2 nf2, gk3 gk3) {
            this.e = nf2;
            this.f = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputLayout textInputLayout = this.e.u;
            wd4.a((Object) textInputLayout, "binding.inputPassword");
            textInputLayout.setErrorEnabled(false);
            TextInputLayout textInputLayout2 = this.e.t;
            wd4.a((Object) textInputLayout2, "binding.inputEmail");
            textInputLayout2.setErrorEnabled(false);
            this.f.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public h(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.e.T0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public i(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.e.T0().a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public j(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.e.T0().b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public k(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public l(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public m(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public n(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gk3 e;

        @DexIgnore
        public o(gk3 gk3) {
            this.e = gk3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().l();
        }
    }

    /*
    static {
        String simpleName = gk3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "LoginFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ur3 a(gk3 gk3) {
        ur3<nf2> ur3 = gk3.k;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void F(boolean z) {
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FloatingActionButton floatingActionButton = a2.A;
                wd4.a((Object) floatingActionButton, "it.ivWeibo");
                floatingActionButton.setVisibility(0);
                FloatingActionButton floatingActionButton2 = a2.z;
                wd4.a((Object) floatingActionButton2, "it.ivWechat");
                floatingActionButton2.setVisibility(0);
                ImageView imageView = a2.y;
                wd4.a((Object) imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.x;
                wd4.a((Object) imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            FloatingActionButton floatingActionButton3 = a2.A;
            wd4.a((Object) floatingActionButton3, "it.ivWeibo");
            floatingActionButton3.setVisibility(8);
            FloatingActionButton floatingActionButton4 = a2.z;
            wd4.a((Object) floatingActionButton4, "it.ivWechat");
            floatingActionButton4.setVisibility(8);
            ImageView imageView3 = a2.y;
            wd4.a((Object) imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.x;
            wd4.a((Object) imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void J(String str) {
        wd4.b(str, "email");
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.C;
            wd4.a((Object) context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final ek3 T0() {
        ek3 ek3 = this.j;
        if (ek3 != null) {
            return ek3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 != null) {
                ek3 ek3 = this.j;
                if (ek3 != null) {
                    TextInputEditText textInputEditText = a2.r;
                    wd4.a((Object) textInputEditText, "etEmail");
                    String valueOf = String.valueOf(textInputEditText.getText());
                    TextInputEditText textInputEditText2 = a2.s;
                    wd4.a((Object) textInputEditText2, "etPassword");
                    ek3.a(valueOf, String.valueOf(textInputEditText2.getText()));
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Y() {
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(false);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void i0() {
        if (isActive()) {
            ur3<nf2> ur3 = this.k;
            if (ur3 != null) {
                nf2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    wd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setError(" ");
                    TextInputLayout textInputLayout2 = a2.u;
                    wd4.a((Object) textInputLayout2, "it.inputPassword");
                    textInputLayout2.setError(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Login_WrongEmailPasswordError_Text__WrongEmailOrPassword));
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void k() {
        String string = getString(R.string.Onboarding_Login_LoggingIn_Text__PleaseWait);
        wd4.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(m, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            ek3 ek3 = this.j;
            if (ek3 != null) {
                wd4.a((Object) signUpSocialAuth, "authCode");
                ek3.a(signUpSocialAuth);
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        nf2 nf2 = (nf2) ra.a(layoutInflater, R.layout.fragment_signin, viewGroup, false, O0());
        wd4.a((Object) nf2, "binding");
        View d2 = nf2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.k = new ur3<>(this, nf2);
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ek3 ek3 = this.j;
        if (ek3 != null) {
            ek3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ek3 ek3 = this.j;
        if (ek3 != null) {
            ek3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new g(a2, this));
                a2.r.addTextChangedListener(new h(this));
                a2.r.setOnFocusChangeListener(new i(this));
                a2.s.addTextChangedListener(new j(this));
                a2.w.setOnClickListener(new k(this));
                a2.x.setOnClickListener(new l(this));
                a2.y.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.z.setOnClickListener(new o(this));
                a2.A.setOnClickListener(new c(this));
                a2.D.setOnClickListener(new d(this));
                a2.B.setOnClickListener(new e(this));
                a2.s.setOnEditorActionListener(new f(this));
            }
            R("login_view");
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void q0() {
        ur3<nf2> ur3 = this.k;
        if (ur3 != null) {
            nf2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(true);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i2, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(boolean z, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            ur3<nf2> ur3 = this.k;
            if (ur3 != null) {
                nf2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    if (textInputLayout != null) {
                        wd4.a((Object) textInputLayout, "it");
                        textInputLayout.setErrorEnabled(z);
                        textInputLayout.setError(str);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ek3 ek3) {
        wd4.b(ek3, "presenter");
        this.j = ek3;
    }

    @DexIgnore
    public void F() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            wd4.a((Object) activity, "it");
            aVar.a(activity, "file:///android_asset/signinwithapple.html");
        }
    }
}
