package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn1 {
    @DexIgnore
    public static /* final */ Executor a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new uo1();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler e; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public final void execute(Runnable runnable) {
            this.e.post(runnable);
        }
    }
}
