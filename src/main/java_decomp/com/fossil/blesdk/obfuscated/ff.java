package com.fossil.blesdk.obfuscated;

import android.renderscript.BaseObj;
import androidx.renderscript.RSIllegalArgumentException;
import androidx.renderscript.RSInvalidStateException;
import androidx.renderscript.RSRuntimeException;
import androidx.renderscript.RenderScript;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ff {
    @DexIgnore
    public long a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public RenderScript c;

    @DexIgnore
    public ff(long j, RenderScript renderScript) {
        renderScript.j();
        this.c = renderScript;
        this.a = j;
    }

    @DexIgnore
    public long a(RenderScript renderScript) {
        this.c.j();
        if (this.b) {
            throw new RSInvalidStateException("using a destroyed object.");
        } else if (this.a == 0) {
            throw new RSRuntimeException("Internal error: Object id 0.");
        } else if (renderScript == null || renderScript == this.c) {
            return this.a;
        } else {
            throw new RSInvalidStateException("using object with mismatched context.");
        }
    }

    @DexIgnore
    public void b() {
        if (!this.b) {
            d();
            return;
        }
        throw new RSInvalidStateException("Object already destroyed.");
    }

    @DexIgnore
    public BaseObj c() {
        return null;
    }

    @DexIgnore
    public final void d() {
        boolean z;
        synchronized (this) {
            z = true;
            if (!this.b) {
                this.b = true;
            } else {
                z = false;
            }
        }
        if (z) {
            ReentrantReadWriteLock.ReadLock readLock = this.c.k.readLock();
            readLock.lock();
            if (this.c.c()) {
                this.c.b(this.a);
            }
            readLock.unlock();
            this.c = null;
            this.a = 0;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.a == ((ff) obj).a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        d();
        super.finalize();
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return (int) ((j >> 32) ^ (268435455 & j));
    }

    @DexIgnore
    public void a() {
        if (this.a == 0 && c() == null) {
            throw new RSIllegalArgumentException("Invalid object.");
        }
    }
}
