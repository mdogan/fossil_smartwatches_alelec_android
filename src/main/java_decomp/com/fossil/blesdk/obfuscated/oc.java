package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.rc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oc extends LoaderManager {
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public /* final */ LifecycleOwner a;
    @DexIgnore
    public /* final */ c b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends jc {
        @DexIgnore
        public static /* final */ lc.b e; // = new a();
        @DexIgnore
        public SparseArrayCompat<a> c; // = new SparseArrayCompat<>();
        @DexIgnore
        public boolean d; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements lc.b {
            @DexIgnore
            public <T extends jc> T a(Class<T> cls) {
                return new c();
            }
        }

        @DexIgnore
        public static c a(ViewModelStore viewModelStore) {
            return (c) new lc(viewModelStore, e).a(c.class);
        }

        @DexIgnore
        public void b(int i) {
            this.c.e(i);
        }

        @DexIgnore
        public void c() {
            this.d = false;
        }

        @DexIgnore
        public boolean d() {
            return this.d;
        }

        @DexIgnore
        public void e() {
            int c2 = this.c.c();
            for (int i = 0; i < c2; i++) {
                this.c.f(i).g();
            }
        }

        @DexIgnore
        public void f() {
            this.d = true;
        }

        @DexIgnore
        public void a(int i, a aVar) {
            this.c.c(i, aVar);
        }

        @DexIgnore
        public void b() {
            super.b();
            int c2 = this.c.c();
            for (int i = 0; i < c2; i++) {
                this.c.f(i).a(true);
            }
            this.c.a();
        }

        @DexIgnore
        public <D> a<D> a(int i) {
            return this.c.b(i);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.c.c() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.c.c(); i++) {
                    a f = this.c.f(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.c.d(i));
                    printWriter.print(": ");
                    printWriter.println(f.toString());
                    f.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    @DexIgnore
    public oc(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.a = lifecycleOwner;
        this.b = c.a(viewModelStore);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final <D> rc<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar, rc<D> rcVar) {
        try {
            this.b.f();
            rc<D> a2 = aVar.a(i, bundle);
            if (a2 != null) {
                if (a2.getClass().isMemberClass()) {
                    if (!Modifier.isStatic(a2.getClass().getModifiers())) {
                        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + a2);
                    }
                }
                a aVar2 = new a(i, bundle, a2, rcVar);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.b.a(i, aVar2);
                this.b.c();
                return aVar2.a(this.a, aVar);
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.b.c();
            throw th;
        }
    }

    @DexIgnore
    public <D> rc<D> b(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            a a2 = this.b.a(i);
            rc rcVar = null;
            if (a2 != null) {
                rcVar = a2.a(false);
            }
            return a(i, bundle, aVar, rcVar);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        d8.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<D> extends MutableLiveData<D> implements rc.c<D> {
        @DexIgnore
        public /* final */ int k;
        @DexIgnore
        public /* final */ Bundle l;
        @DexIgnore
        public /* final */ rc<D> m;
        @DexIgnore
        public LifecycleOwner n;
        @DexIgnore
        public b<D> o;
        @DexIgnore
        public rc<D> p;

        @DexIgnore
        public a(int i, Bundle bundle, rc<D> rcVar, rc<D> rcVar2) {
            this.k = i;
            this.l = bundle;
            this.m = rcVar;
            this.p = rcVar2;
            this.m.registerListener(i, this);
        }

        @DexIgnore
        public rc<D> a(LifecycleOwner lifecycleOwner, LoaderManager.a<D> aVar) {
            b<D> bVar = new b<>(this.m, aVar);
            a(lifecycleOwner, bVar);
            b<D> bVar2 = this.o;
            if (bVar2 != null) {
                b(bVar2);
            }
            this.n = lifecycleOwner;
            this.o = bVar;
            return this.m;
        }

        @DexIgnore
        public void b(dc<? super D> dcVar) {
            super.b(dcVar);
            this.n = null;
            this.o = null;
        }

        @DexIgnore
        public void d() {
            if (oc.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        @DexIgnore
        public void e() {
            if (oc.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        @DexIgnore
        public rc<D> f() {
            return this.m;
        }

        @DexIgnore
        public void g() {
            LifecycleOwner lifecycleOwner = this.n;
            b<D> bVar = this.o;
            if (lifecycleOwner != null && bVar != null) {
                super.b(bVar);
                a(lifecycleOwner, bVar);
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            d8.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }

        @DexIgnore
        public void b(D d) {
            super.b(d);
            rc<D> rcVar = this.p;
            if (rcVar != null) {
                rcVar.reset();
                this.p = null;
            }
        }

        @DexIgnore
        public rc<D> a(boolean z) {
            if (oc.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            b<D> bVar = this.o;
            if (bVar != null) {
                b(bVar);
                if (z) {
                    bVar.b();
                }
            }
            this.m.unregisterListener(this);
            if ((bVar == null || bVar.a()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        @DexIgnore
        public void a(rc<D> rcVar, D d) {
            if (oc.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                b(d);
                return;
            }
            if (oc.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            a(d);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            rc<D> rcVar = this.m;
            rcVar.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                b<D> bVar = this.o;
                bVar.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(f().dataToString(a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<D> implements dc<D> {
        @DexIgnore
        public /* final */ rc<D> a;
        @DexIgnore
        public /* final */ LoaderManager.a<D> b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(rc<D> rcVar, LoaderManager.a<D> aVar) {
            this.a = rcVar;
            this.b = aVar;
        }

        @DexIgnore
        public void a(D d) {
            if (oc.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.a + ": " + this.a.dataToString(d));
            }
            this.b.a(this.a, d);
            this.c = true;
        }

        @DexIgnore
        public void b() {
            if (this.c) {
                if (oc.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.a);
                }
                this.b.a(this.a);
            }
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }

        @DexIgnore
        public boolean a() {
            return this.c;
        }

        @DexIgnore
        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }
    }

    @DexIgnore
    public <D> rc<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            a a2 = this.b.a(i);
            if (c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (a2 == null) {
                return a(i, bundle, aVar, (rc) null);
            }
            if (c) {
                Log.v("LoaderManager", "  Re-using existing loader " + a2);
            }
            return a2.a(this.a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public void a(int i) {
        if (this.b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            a a2 = this.b.a(i);
            if (a2 != null) {
                a2.a(true);
                this.b.b(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public void a() {
        this.b.e();
    }

    @DexIgnore
    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }
}
