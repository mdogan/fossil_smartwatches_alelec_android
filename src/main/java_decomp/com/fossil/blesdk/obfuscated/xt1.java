package com.fossil.blesdk.obfuscated;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xt1<E> extends fv1<E> {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;

    @DexIgnore
    public xt1(int i, int i2) {
        tt1.b(i2, i);
        this.e = i;
        this.f = i2;
    }

    @DexIgnore
    public abstract E a(int i);

    @DexIgnore
    public final boolean hasNext() {
        return this.f < this.e;
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.f > 0;
    }

    @DexIgnore
    public final E next() {
        if (hasNext()) {
            int i = this.f;
            this.f = i + 1;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.f;
    }

    @DexIgnore
    public final E previous() {
        if (hasPrevious()) {
            int i = this.f - 1;
            this.f = i;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.f - 1;
    }
}
