package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rn3 implements Factory<GetDianaDeviceSettingUseCase> {
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<wy2> f;
    @DexIgnore
    public /* final */ Provider<qx2> g;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> h;
    @DexIgnore
    public /* final */ Provider<fn2> i;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> j;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> k;

    @DexIgnore
    public rn3(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchFaceRepository> provider5, Provider<wy2> provider6, Provider<qx2> provider7, Provider<NotificationSettingsDatabase> provider8, Provider<fn2> provider9, Provider<WatchLocalizationRepository> provider10, Provider<AlarmsRepository> provider11) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
    }

    @DexIgnore
    public static rn3 a(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchFaceRepository> provider5, Provider<wy2> provider6, Provider<qx2> provider7, Provider<NotificationSettingsDatabase> provider8, Provider<fn2> provider9, Provider<WatchLocalizationRepository> provider10, Provider<AlarmsRepository> provider11) {
        return new rn3(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11);
    }

    @DexIgnore
    public static GetDianaDeviceSettingUseCase b(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchFaceRepository> provider5, Provider<wy2> provider6, Provider<qx2> provider7, Provider<NotificationSettingsDatabase> provider8, Provider<fn2> provider9, Provider<WatchLocalizationRepository> provider10, Provider<AlarmsRepository> provider11) {
        return new GetDianaDeviceSettingUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get());
    }

    @DexIgnore
    public GetDianaDeviceSettingUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k);
    }
}
