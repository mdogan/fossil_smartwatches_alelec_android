package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dv3;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.IOException;
import java.net.URI;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iv3 {
    @DexIgnore
    public /* final */ ev3 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ dv3 c;
    @DexIgnore
    public /* final */ jv3 d;
    @DexIgnore
    public /* final */ Object e;
    @DexIgnore
    public volatile URI f;
    @DexIgnore
    public volatile tu3 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public ev3 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public dv3.b c;
        @DexIgnore
        public jv3 d;
        @DexIgnore
        public Object e;

        @DexIgnore
        public b() {
            this.b = "GET";
            this.c = new dv3.b();
        }

        @DexIgnore
        public b a(ev3 ev3) {
            if (ev3 != null) {
                this.a = ev3;
                return this;
            }
            throw new IllegalArgumentException("url == null");
        }

        @DexIgnore
        public b b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                ev3 c2 = ev3.c(str);
                if (c2 != null) {
                    a(c2);
                    return this;
                }
                throw new IllegalArgumentException("unexpected url: " + str);
            }
            throw new IllegalArgumentException("url == null");
        }

        @DexIgnore
        public b a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public b(iv3 iv3) {
            this.a = iv3.a;
            this.b = iv3.b;
            this.d = iv3.d;
            this.e = iv3.e;
            this.c = iv3.c.a();
        }

        @DexIgnore
        public b a(String str) {
            this.c.b(str);
            return this;
        }

        @DexIgnore
        public b a(dv3 dv3) {
            this.c = dv3.a();
            return this;
        }

        @DexIgnore
        public b a(tu3 tu3) {
            String tu32 = tu3.toString();
            if (tu32.isEmpty()) {
                a(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
                return this;
            }
            b(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, tu32);
            return this;
        }

        @DexIgnore
        public b a(String str, jv3 jv3) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("method == null || method.length() == 0");
            } else if (jv3 != null && !ww3.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (jv3 != null || !ww3.c(str)) {
                this.b = str;
                this.d = jv3;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public b b(String str, String str2) {
            this.c.d(str, str2);
            return this;
        }

        @DexIgnore
        public iv3 a() {
            if (this.a != null) {
                return new iv3(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    @DexIgnore
    public String f() {
        return this.b;
    }

    @DexIgnore
    public b g() {
        return new b();
    }

    @DexIgnore
    public URI h() throws IOException {
        try {
            URI uri = this.f;
            if (uri != null) {
                return uri;
            }
            URI k = this.a.k();
            this.f = k;
            return k;
        } catch (IllegalStateException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    @DexIgnore
    public String i() {
        return this.a.toString();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request{method=");
        sb.append(this.b);
        sb.append(", url=");
        sb.append(this.a);
        sb.append(", tag=");
        Object obj = this.e;
        if (obj == this) {
            obj = null;
        }
        sb.append(obj);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public iv3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c.a();
        this.d = bVar.d;
        this.e = bVar.e != null ? bVar.e : this;
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }

    @DexIgnore
    public List<String> b(String str) {
        return this.c.c(str);
    }

    @DexIgnore
    public dv3 c() {
        return this.c;
    }

    @DexIgnore
    public ev3 d() {
        return this.a;
    }

    @DexIgnore
    public boolean e() {
        return this.a.g();
    }

    @DexIgnore
    public jv3 a() {
        return this.d;
    }

    @DexIgnore
    public tu3 b() {
        tu3 tu3 = this.g;
        if (tu3 != null) {
            return tu3;
        }
        tu3 a2 = tu3.a(this.c);
        this.g = a2;
        return a2;
    }
}
