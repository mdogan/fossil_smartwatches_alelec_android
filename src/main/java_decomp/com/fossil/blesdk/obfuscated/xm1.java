package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.jj0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xm1 extends pj0<dn1> implements mn1 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ lj0 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public xm1(Context context, Looper looper, boolean z, lj0 lj0, Bundle bundle, he0.b bVar, he0.c cVar) {
        super(context, looper, 44, lj0, bVar, cVar);
        this.E = true;
        this.F = lj0;
        this.G = bundle;
        this.H = lj0.e();
    }

    @DexIgnore
    public final void a(uj0 uj0, boolean z) {
        try {
            ((dn1) x()).a(uj0, this.H.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    public final void b() {
        a((jj0.c) new jj0.d());
    }

    @DexIgnore
    public final void g() {
        try {
            ((dn1) x()).c(this.H.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    public int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public boolean l() {
        return this.E;
    }

    @DexIgnore
    public Bundle u() {
        if (!t().getPackageName().equals(this.F.h())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.h());
        }
        return this.G;
    }

    @DexIgnore
    public String y() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    public String z() {
        return "com.google.android.gms.signin.service.START";
    }

    @DexIgnore
    public final void a(bn1 bn1) {
        ck0.a(bn1, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = dc0.a(t()).b();
            }
            ((dn1) x()).a(new fn1(new dk0(c, this.H.intValue(), googleSignInAccount)), bn1);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                bn1.a(new hn1(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    public xm1(Context context, Looper looper, boolean z, lj0 lj0, wm1 wm1, he0.b bVar, he0.c cVar) {
        this(context, looper, true, lj0, a(lj0), bVar, cVar);
    }

    @DexIgnore
    public static Bundle a(lj0 lj0) {
        wm1 j = lj0.j();
        Integer e = lj0.e();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", lj0.a());
        if (e != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", e.intValue());
        }
        if (j != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", j.g());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", j.f());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", j.d());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", j.e());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", j.b());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", j.h());
            if (j.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", j.a().longValue());
            }
            if (j.c() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", j.c().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof dn1) {
            return (dn1) queryLocalInterface;
        }
        return new en1(iBinder);
    }
}
