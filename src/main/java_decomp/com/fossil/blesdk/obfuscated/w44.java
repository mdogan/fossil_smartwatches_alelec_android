package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.IdManager;
import java.io.File;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class w44<Result> implements Comparable<w44> {
    @DexIgnore
    public r44 e;
    @DexIgnore
    public v44<Result> f; // = new v44<>(this);
    @DexIgnore
    public Context g;
    @DexIgnore
    public u44<Result> h;
    @DexIgnore
    public IdManager i;
    @DexIgnore
    public /* final */ c64 j; // = ((c64) getClass().getAnnotation(c64.class));

    @DexIgnore
    public void a(Context context, r44 r44, u44<Result> u44, IdManager idManager) {
        this.e = r44;
        this.g = new s44(context, p(), q());
        this.h = u44;
        this.i = idManager;
    }

    @DexIgnore
    public void a(Result result) {
    }

    @DexIgnore
    public void b(Result result) {
    }

    @DexIgnore
    public boolean b(w44 w44) {
        if (s()) {
            for (Class isAssignableFrom : this.j.value()) {
                if (isAssignableFrom.isAssignableFrom(w44.getClass())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Result k();

    @DexIgnore
    public Context l() {
        return this.g;
    }

    @DexIgnore
    public Collection<j64> m() {
        return this.f.c();
    }

    @DexIgnore
    public r44 n() {
        return this.e;
    }

    @DexIgnore
    public IdManager o() {
        return this.i;
    }

    @DexIgnore
    public abstract String p();

    @DexIgnore
    public String q() {
        return ".Fabric" + File.separator + p();
    }

    @DexIgnore
    public abstract String r();

    @DexIgnore
    public boolean s() {
        return this.j != null;
    }

    @DexIgnore
    public final void t() {
        this.f.a(this.e.b(), null);
    }

    @DexIgnore
    public boolean u() {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(w44 w44) {
        if (b(w44)) {
            return 1;
        }
        if (w44.b(this)) {
            return -1;
        }
        if (s() && !w44.s()) {
            return 1;
        }
        if (s() || !w44.s()) {
            return 0;
        }
        return -1;
    }
}
