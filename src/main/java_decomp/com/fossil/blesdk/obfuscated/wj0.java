package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wj0 extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends az0 implements wj0 {
        @DexIgnore
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @DexIgnore
        public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) bz0.a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                b(parcel.readInt(), (Bundle) bz0.a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                a(parcel.readInt(), parcel.readStrongBinder(), (ol0) bz0.a(parcel, ol0.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    @DexIgnore
    void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(int i, IBinder iBinder, ol0 ol0) throws RemoteException;

    @DexIgnore
    void b(int i, Bundle bundle) throws RemoteException;
}
