package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kr2 extends j62<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            wd4.b(str, "oldPass");
            wd4.b(str2, "newPass");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements j62.c {
    }

    /*
    static {
        String simpleName = kr2.class.getSimpleName();
        wd4.a((Object) simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public kr2(AuthApiUserService authApiUserService) {
        wd4.b(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ap2<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ kr2 e;

        @DexIgnore
        public e(kr2 kr2) {
            this.e = kr2;
        }

        @DexIgnore
        public void a(Call<yz1> call, cs4<yz1> cs4) {
            wd4.b(call, "call");
            wd4.b(cs4, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = kr2.f.a();
            local.d(a, "changePassword onSuccessResponse response=" + cs4);
            this.e.a().onSuccess(new d());
        }

        @DexIgnore
        public void a(Call<yz1> call, ServerError serverError) {
            wd4.b(call, "call");
            wd4.b(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = kr2.f.a();
            local.d(a, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            j62.d a2 = this.e.a();
            Integer code = serverError.getCode();
            wd4.a((Object) code, "response.code");
            a2.a(new c(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        public void a(Call<yz1> call, Throwable th) {
            wd4.b(call, "call");
            wd4.b(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = kr2.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(cb4.a);
            local.d(a, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.e.a().a(new c(MFNetworkReturnCode.CLIENT_TIMEOUT, (String) null));
            } else {
                this.e.a().a(new c(601, (String) null));
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        wd4.b(bVar, "requestValues");
        if (!bs3.b(PortfolioApp.W.c())) {
            a().a(new c(601, ""));
            return;
        }
        yz1 yz1 = new yz1();
        try {
            yz1.a("oldPassword", bVar.b());
            yz1.a("newPassword", bVar.a());
        } catch (Exception unused) {
        }
        this.d.changePassword(yz1).a(new e(this));
    }
}
