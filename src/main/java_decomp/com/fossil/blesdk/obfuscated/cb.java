package com.fossil.blesdk.obfuscated;

import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cb {
    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract cb a(int i, Fragment fragment);

    @DexIgnore
    public abstract cb a(int i, Fragment fragment, String str);

    @DexIgnore
    public abstract cb a(Fragment fragment);

    @DexIgnore
    public abstract cb a(Fragment fragment, String str);

    @DexIgnore
    public abstract cb a(String str);

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract cb b(int i, Fragment fragment, String str);

    @DexIgnore
    public abstract cb b(Fragment fragment);

    @DexIgnore
    public abstract cb c(Fragment fragment);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract cb d(Fragment fragment);

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public abstract cb e(Fragment fragment);
}
