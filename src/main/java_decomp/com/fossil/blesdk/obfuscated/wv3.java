package com.fossil.blesdk.obfuscated;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wv3 {
    @DexIgnore
    public /* final */ Set<mv3> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(mv3 mv3) {
        this.a.remove(mv3);
    }

    @DexIgnore
    public synchronized void b(mv3 mv3) {
        this.a.add(mv3);
    }

    @DexIgnore
    public synchronized boolean c(mv3 mv3) {
        return this.a.contains(mv3);
    }
}
