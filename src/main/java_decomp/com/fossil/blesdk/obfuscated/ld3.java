package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ld3 {
    @DexIgnore
    public /* final */ gd3 a;
    @DexIgnore
    public /* final */ vd3 b;
    @DexIgnore
    public /* final */ qd3 c;

    @DexIgnore
    public ld3(gd3 gd3, vd3 vd3, qd3 qd3) {
        wd4.b(gd3, "mSleepOverviewDayView");
        wd4.b(vd3, "mSleepOverviewWeekView");
        wd4.b(qd3, "mSleepOverviewMonthView");
        this.a = gd3;
        this.b = vd3;
        this.c = qd3;
    }

    @DexIgnore
    public final gd3 a() {
        return this.a;
    }

    @DexIgnore
    public final qd3 b() {
        return this.c;
    }

    @DexIgnore
    public final vd3 c() {
        return this.b;
    }
}
