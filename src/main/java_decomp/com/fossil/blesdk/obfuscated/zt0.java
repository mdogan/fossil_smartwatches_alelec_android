package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzco;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zt0 {
    @DexIgnore
    public zt0() {
    }

    @DexIgnore
    public static int a(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    @DexIgnore
    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    @DexIgnore
    public static zt0 a(byte[] bArr, int i, int i2, boolean z) {
        bu0 bu0 = new bu0(bArr, 0, i2, false);
        try {
            bu0.b(i2);
            return bu0;
        } catch (zzco e) {
            throw new IllegalArgumentException(e);
        }
    }
}
