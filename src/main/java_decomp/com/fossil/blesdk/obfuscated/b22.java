package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b22 implements Cloneable {
    @DexIgnore
    public int[] e;
    @DexIgnore
    public int f;

    @DexIgnore
    public b22() {
        this.f = 0;
        this.e = new int[1];
    }

    @DexIgnore
    public static int[] c(int i) {
        return new int[((i + 31) / 32)];
    }

    @DexIgnore
    public int a() {
        return this.f;
    }

    @DexIgnore
    public int b() {
        return (this.f + 7) / 8;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof b22)) {
            return false;
        }
        b22 b22 = (b22) obj;
        if (this.f != b22.f || !Arrays.equals(this.e, b22.e)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f * 31) + Arrays.hashCode(this.e);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(this.f);
        for (int i = 0; i < this.f; i++) {
            if ((i & 7) == 0) {
                sb.append(' ');
            }
            sb.append(b(i) ? 'X' : '.');
        }
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i) {
        if (i > (this.e.length << 5)) {
            int[] c = c(i);
            int[] iArr = this.e;
            System.arraycopy(iArr, 0, c, 0, iArr.length);
            this.e = c;
        }
    }

    @DexIgnore
    public boolean b(int i) {
        return ((1 << (i & 31)) & this.e[i / 32]) != 0;
    }

    @DexIgnore
    public b22 clone() {
        return new b22((int[]) this.e.clone(), this.f);
    }

    @DexIgnore
    public void b(b22 b22) {
        if (this.f == b22.f) {
            int i = 0;
            while (true) {
                int[] iArr = this.e;
                if (i < iArr.length) {
                    iArr[i] = iArr[i] ^ b22.e[i];
                    i++;
                } else {
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Sizes don't match");
        }
    }

    @DexIgnore
    public b22(int[] iArr, int i) {
        this.e = iArr;
        this.f = i;
    }

    @DexIgnore
    public void a(boolean z) {
        a(this.f + 1);
        if (z) {
            int[] iArr = this.e;
            int i = this.f;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.f++;
    }

    @DexIgnore
    public void a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        a(this.f + i2);
        while (i2 > 0) {
            boolean z = true;
            if (((i >> (i2 - 1)) & 1) != 1) {
                z = false;
            }
            a(z);
            i2--;
        }
    }

    @DexIgnore
    public void a(b22 b22) {
        int i = b22.f;
        a(this.f + i);
        for (int i2 = 0; i2 < i; i2++) {
            a(b22.b(i2));
        }
    }

    @DexIgnore
    public void a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i4;
            int i7 = 0;
            for (int i8 = 0; i8 < 8; i8++) {
                if (b(i6)) {
                    i7 |= 1 << (7 - i8);
                }
                i6++;
            }
            bArr[i2 + i5] = (byte) i7;
            i5++;
            i4 = i6;
        }
    }
}
