package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c14 implements Runnable {
    @DexIgnore
    public Context e; // = null;
    @DexIgnore
    public Map<String, Integer> f; // = null;
    @DexIgnore
    public l04 g; // = null;

    @DexIgnore
    public c14(Context context, Map<String, Integer> map, l04 l04) {
        this.e = context;
        this.g = l04;
        if (map != null) {
            this.f = map;
        }
    }

    @DexIgnore
    public final h04 a(String str, int i) {
        int i2;
        h04 h04 = new h04();
        Socket socket = new Socket();
        try {
            h04.a(str);
            h04.a(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            h04.a(System.currentTimeMillis() - currentTimeMillis);
            h04.b(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                k04.m.a(th);
            }
            i2 = 0;
        } catch (IOException e2) {
            try {
                k04.m.a((Throwable) e2);
                socket.close();
            } catch (Throwable th2) {
                k04.m.a(th2);
            }
        } catch (Throwable th3) {
            k04.m.a(th3);
        }
        h04.b(i2);
        return h04;
        i2 = -1;
        h04.b(i2);
        return h04;
        throw th;
    }

    @DexIgnore
    public final Map<String, Integer> a() {
        HashMap hashMap = new HashMap();
        String a = i04.a("__MTA_TEST_SPEED__", (String) null);
        if (!(a == null || a.trim().length() == 0)) {
            for (String split : a.split(";")) {
                String[] split2 = split.split(",");
                if (split2 != null && split2.length == 2) {
                    String str = split2[0];
                    if (!(str == null || str.trim().length() == 0)) {
                        try {
                            hashMap.put(str, Integer.valueOf(Integer.valueOf(split2[1]).intValue()));
                        } catch (NumberFormatException e2) {
                            k04.m.a((Throwable) e2);
                        }
                    }
                }
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void run() {
        u14 f2;
        String str;
        try {
            if (this.f == null) {
                this.f = a();
            }
            if (this.f != null) {
                if (this.f.size() != 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (Map.Entry next : this.f.entrySet()) {
                        String str2 = (String) next.getKey();
                        if (str2 != null) {
                            if (str2.length() != 0) {
                                if (((Integer) next.getValue()) == null) {
                                    f2 = k04.m;
                                    str = "port is null for " + str2;
                                    f2.g(str);
                                } else {
                                    jSONArray.put(a((String) next.getKey(), ((Integer) next.getValue()).intValue()).a());
                                }
                            }
                        }
                        f2 = k04.m;
                        str = "empty domain name.";
                        f2.g(str);
                    }
                    if (jSONArray.length() != 0) {
                        r04 r04 = new r04(this.e, k04.a(this.e, false, this.g), this.g);
                        r04.a(jSONArray.toString());
                        new d14(r04).a();
                        return;
                    }
                    return;
                }
            }
            k04.m.e("empty domain list.");
        } catch (Throwable th) {
            k04.m.a(th);
        }
    }
}
