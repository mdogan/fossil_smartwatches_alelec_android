package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i62 {
    @DexIgnore
    public static /* final */ int[] ActionBar; // = {R.attr.background, R.attr.backgroundSplit, R.attr.backgroundStacked, R.attr.contentInsetEnd, R.attr.contentInsetEndWithActions, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.contentInsetStart, R.attr.contentInsetStartWithNavigation, R.attr.customNavigationLayout, R.attr.displayOptions, R.attr.divider, R.attr.elevation, R.attr.height, R.attr.hideOnContentScroll, R.attr.homeAsUpIndicator, R.attr.homeLayout, R.attr.icon, R.attr.indeterminateProgressStyle, R.attr.itemPadding, R.attr.logo, R.attr.navigationMode, R.attr.popupTheme, R.attr.progressBarPadding, R.attr.progressBarStyle, R.attr.subtitle, R.attr.subtitleTextStyle, R.attr.title, R.attr.titleTextStyle};
    @DexIgnore
    public static /* final */ int[] ActionBarLayout; // = {16842931};
    @DexIgnore
    public static /* final */ int ActionBarLayout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundStacked; // = 2;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEnd; // = 3;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEndWithActions; // = 4;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetLeft; // = 5;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetRight; // = 6;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStart; // = 7;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStartWithNavigation; // = 8;
    @DexIgnore
    public static /* final */ int ActionBar_customNavigationLayout; // = 9;
    @DexIgnore
    public static /* final */ int ActionBar_displayOptions; // = 10;
    @DexIgnore
    public static /* final */ int ActionBar_divider; // = 11;
    @DexIgnore
    public static /* final */ int ActionBar_elevation; // = 12;
    @DexIgnore
    public static /* final */ int ActionBar_height; // = 13;
    @DexIgnore
    public static /* final */ int ActionBar_hideOnContentScroll; // = 14;
    @DexIgnore
    public static /* final */ int ActionBar_homeAsUpIndicator; // = 15;
    @DexIgnore
    public static /* final */ int ActionBar_homeLayout; // = 16;
    @DexIgnore
    public static /* final */ int ActionBar_icon; // = 17;
    @DexIgnore
    public static /* final */ int ActionBar_indeterminateProgressStyle; // = 18;
    @DexIgnore
    public static /* final */ int ActionBar_itemPadding; // = 19;
    @DexIgnore
    public static /* final */ int ActionBar_logo; // = 20;
    @DexIgnore
    public static /* final */ int ActionBar_navigationMode; // = 21;
    @DexIgnore
    public static /* final */ int ActionBar_popupTheme; // = 22;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarPadding; // = 23;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarStyle; // = 24;
    @DexIgnore
    public static /* final */ int ActionBar_subtitle; // = 25;
    @DexIgnore
    public static /* final */ int ActionBar_subtitleTextStyle; // = 26;
    @DexIgnore
    public static /* final */ int ActionBar_title; // = 27;
    @DexIgnore
    public static /* final */ int ActionBar_titleTextStyle; // = 28;
    @DexIgnore
    public static /* final */ int[] ActionMenuItemView; // = {16843071};
    @DexIgnore
    public static /* final */ int ActionMenuItemView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int[] ActionMenuView; // = new int[0];
    @DexIgnore
    public static /* final */ int[] ActionMode; // = {R.attr.background, R.attr.backgroundSplit, R.attr.closeItemLayout, R.attr.height, R.attr.subtitleTextStyle, R.attr.titleTextStyle};
    @DexIgnore
    public static /* final */ int ActionMode_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionMode_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionMode_closeItemLayout; // = 2;
    @DexIgnore
    public static /* final */ int ActionMode_height; // = 3;
    @DexIgnore
    public static /* final */ int ActionMode_subtitleTextStyle; // = 4;
    @DexIgnore
    public static /* final */ int ActionMode_titleTextStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ActivityChooserView; // = {R.attr.expandActivityOverflowButtonDrawable, R.attr.initialActivityCount};
    @DexIgnore
    public static /* final */ int ActivityChooserView_expandActivityOverflowButtonDrawable; // = 0;
    @DexIgnore
    public static /* final */ int ActivityChooserView_initialActivityCount; // = 1;
    @DexIgnore
    public static /* final */ int[] ActivityDayDetailsChart; // = {R.attr.activity_day_bar_active_time_color, R.attr.activity_day_bar_calories_color, R.attr.activity_day_bar_color, R.attr.activity_day_bar_steps_intense_color, R.attr.activity_day_bar_steps_light_color, R.attr.activity_day_bar_steps_moderate_color, R.attr.activity_day_dash_line_color, R.attr.activity_day_line_color, R.attr.activity_day_text_color, R.attr.activity_day_text_size};
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_active_time_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_calories_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_intense_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_light_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_moderate_color; // = 5;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_dash_line_color; // = 6;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_line_color; // = 7;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_text_color; // = 8;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_text_size; // = 9;
    @DexIgnore
    public static /* final */ int[] ActivityHorizontalBar; // = {R.attr.activity_background_color, R.attr.activity_fontFamily, R.attr.activity_progress_color, R.attr.activity_progress_margin_end, R.attr.activity_progress_radius, R.attr.activity_progress_width, R.attr.activity_star_alpha, R.attr.activity_star_res, R.attr.activity_star_size, R.attr.activity_text, R.attr.activity_text_color, R.attr.activity_text_size};
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_background_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_fontFamily; // = 1;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_margin_end; // = 3;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_radius; // = 4;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_width; // = 5;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_alpha; // = 6;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_res; // = 7;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_size; // = 8;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text; // = 9;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text_color; // = 10;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text_size; // = 11;
    @DexIgnore
    public static /* final */ int[] ActivityMonthDetailsChart; // = {R.attr.activity_month_bar_active_color, R.attr.activity_month_bar_color, R.attr.activity_month_dash_line_color, R.attr.activity_month_line_color, R.attr.activity_month_text_color, R.attr.activity_month_text_font, R.attr.activity_month_text_size};
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_bar_active_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_bar_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_dash_line_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_line_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_font; // = 5;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_size; // = 6;
    @DexIgnore
    public static /* final */ int[] ActivityWeekDetailsChart; // = {R.attr.activity_week_bar_active_color, R.attr.activity_week_bar_color, R.attr.activity_week_dash_line_color, R.attr.activity_week_line_color, R.attr.activity_week_text_color, R.attr.activity_week_text_font, R.attr.activity_week_text_size};
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_bar_active_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_bar_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_dash_line_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_line_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_font; // = 5;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_size; // = 6;
    @DexIgnore
    public static /* final */ int[] AlertDialog; // = {16842994, R.attr.buttonIconDimen, R.attr.buttonPanelSideLayout, R.attr.listItemLayout, R.attr.listLayout, R.attr.multiChoiceItemLayout, R.attr.showTitle, R.attr.singleChoiceItemLayout};
    @DexIgnore
    public static /* final */ int AlertDialog_android_layout; // = 0;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonIconDimen; // = 1;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonPanelSideLayout; // = 2;
    @DexIgnore
    public static /* final */ int AlertDialog_listItemLayout; // = 3;
    @DexIgnore
    public static /* final */ int AlertDialog_listLayout; // = 4;
    @DexIgnore
    public static /* final */ int AlertDialog_multiChoiceItemLayout; // = 5;
    @DexIgnore
    public static /* final */ int AlertDialog_showTitle; // = 6;
    @DexIgnore
    public static /* final */ int AlertDialog_singleChoiceItemLayout; // = 7;
    @DexIgnore
    public static /* final */ int[] AlphabetFastScrollRecyclerView; // = {R.attr.setIndexBarColor, R.attr.setIndexBarColorRes, R.attr.setIndexBarCornerRadius, R.attr.setIndexBarHighlightTextColor, R.attr.setIndexBarHighlightTextColorRes, R.attr.setIndexBarTextColor, R.attr.setIndexBarTextColorRes, R.attr.setIndexBarTransparentValue, R.attr.setIndexTextSize, R.attr.setIndexbarMargin, R.attr.setIndexbarWidth, R.attr.setPreviewPadding};
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarColor; // = 0;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarColorRes; // = 1;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarCornerRadius; // = 2;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarHighlightTextColor; // = 3;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarHighlightTextColorRes; // = 4;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTextColor; // = 5;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTextColorRes; // = 6;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTransparentValue; // = 7;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexTextSize; // = 8;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexbarMargin; // = 9;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexbarWidth; // = 10;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setPreviewPadding; // = 11;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableCompat; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableItem; // = {16842960, 16843161};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_drawable; // = 1;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_id; // = 0;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableTransition; // = {16843161, 16843849, 16843850, 16843851};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_fromId; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_reversible; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_toId; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimationImageView; // = {R.attr.fps, R.attr.imagesAmount, R.attr.imagesPathFromAssetsWithoutFormatted, R.attr.startImageIndex};
    @DexIgnore
    public static /* final */ int AnimationImageView_fps; // = 0;
    @DexIgnore
    public static /* final */ int AnimationImageView_imagesAmount; // = 1;
    @DexIgnore
    public static /* final */ int AnimationImageView_imagesPathFromAssetsWithoutFormatted; // = 2;
    @DexIgnore
    public static /* final */ int AnimationImageView_startImageIndex; // = 3;
    @DexIgnore
    public static /* final */ int[] AnnularView; // = {R.attr.colorArray, R.attr.handColor, R.attr.strokeWidth};
    @DexIgnore
    public static /* final */ int AnnularView_colorArray; // = 0;
    @DexIgnore
    public static /* final */ int AnnularView_handColor; // = 1;
    @DexIgnore
    public static /* final */ int AnnularView_strokeWidth; // = 2;
    @DexIgnore
    public static /* final */ int[] AppBarLayout; // = {16842964, 16843919, 16844096, R.attr.elevation, R.attr.expanded, R.attr.liftOnScroll};
    @DexIgnore
    public static /* final */ int[] AppBarLayoutStates; // = {R.attr.state_collapsed, R.attr.state_collapsible, R.attr.state_liftable, R.attr.state_lifted};
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_collapsed; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_collapsible; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_liftable; // = 2;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_lifted; // = 3;
    @DexIgnore
    public static /* final */ int[] AppBarLayout_Layout; // = {R.attr.layout_scrollFlags, R.attr.layout_scrollInterpolator};
    @DexIgnore
    public static /* final */ int AppBarLayout_Layout_layout_scrollFlags; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayout_Layout_layout_scrollInterpolator; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_background; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_keyboardNavigationCluster; // = 2;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_touchscreenBlocksFocus; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayout_elevation; // = 3;
    @DexIgnore
    public static /* final */ int AppBarLayout_expanded; // = 4;
    @DexIgnore
    public static /* final */ int AppBarLayout_liftOnScroll; // = 5;
    @DexIgnore
    public static /* final */ int[] AppCompatImageView; // = {16843033, R.attr.srcCompat, R.attr.tint, R.attr.tintMode};
    @DexIgnore
    public static /* final */ int AppCompatImageView_android_src; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatImageView_srcCompat; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatSeekBar; // = {16843074, R.attr.tickMark, R.attr.tickMarkTint, R.attr.tickMarkTintMode};
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_android_thumb; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMark; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatTextHelper; // = {16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667};
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableBottom; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableEnd; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableLeft; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableRight; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableStart; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableTop; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int[] AppCompatTextView; // = {16842804, R.attr.autoSizeMaxTextSize, R.attr.autoSizeMinTextSize, R.attr.autoSizePresetSizes, R.attr.autoSizeStepGranularity, R.attr.autoSizeTextType, R.attr.firstBaselineToTopHeight, R.attr.fontFamily, R.attr.lastBaselineToBottomHeight, R.attr.lineHeight, R.attr.textAllCaps};
    @DexIgnore
    public static /* final */ int AppCompatTextView_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMaxTextSize; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMinTextSize; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizePresetSizes; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeStepGranularity; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeTextType; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextView_firstBaselineToTopHeight; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontFamily; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lastBaselineToBottomHeight; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lineHeight; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textAllCaps; // = 10;
    @DexIgnore
    public static /* final */ int[] AppCompatTheme; // = {16842839, 16842926, R.attr.actionBarDivider, R.attr.actionBarItemBackground, R.attr.actionBarPopupTheme, R.attr.actionBarSize, R.attr.actionBarSplitStyle, R.attr.actionBarStyle, R.attr.actionBarTabBarStyle, R.attr.actionBarTabStyle, R.attr.actionBarTabTextStyle, R.attr.actionBarTheme, R.attr.actionBarWidgetTheme, R.attr.actionButtonStyle, R.attr.actionDropDownStyle, R.attr.actionMenuTextAppearance, R.attr.actionMenuTextColor, R.attr.actionModeBackground, R.attr.actionModeCloseButtonStyle, R.attr.actionModeCloseDrawable, R.attr.actionModeCopyDrawable, R.attr.actionModeCutDrawable, R.attr.actionModeFindDrawable, R.attr.actionModePasteDrawable, R.attr.actionModePopupWindowStyle, R.attr.actionModeSelectAllDrawable, R.attr.actionModeShareDrawable, R.attr.actionModeSplitBackground, R.attr.actionModeStyle, R.attr.actionModeWebSearchDrawable, R.attr.actionOverflowButtonStyle, R.attr.actionOverflowMenuStyle, R.attr.activityChooserViewStyle, R.attr.alertDialogButtonGroupStyle, R.attr.alertDialogCenterButtons, R.attr.alertDialogStyle, R.attr.alertDialogTheme, R.attr.autoCompleteTextViewStyle, R.attr.borderlessButtonStyle, R.attr.buttonBarButtonStyle, R.attr.buttonBarNegativeButtonStyle, R.attr.buttonBarNeutralButtonStyle, R.attr.buttonBarPositiveButtonStyle, R.attr.buttonBarStyle, R.attr.buttonStyle, R.attr.buttonStyleSmall, R.attr.checkboxStyle, R.attr.checkedTextViewStyle, R.attr.colorAccent, R.attr.colorBackgroundFloating, R.attr.colorButtonNormal, R.attr.colorControlActivated, R.attr.colorControlHighlight, R.attr.colorControlNormal, R.attr.colorError, R.attr.colorPrimary, R.attr.colorPrimaryDark, R.attr.colorSwitchThumbNormal, R.attr.controlBackground, R.attr.dialogCornerRadius, R.attr.dialogPreferredPadding, R.attr.dialogTheme, R.attr.dividerHorizontal, R.attr.dividerVertical, R.attr.dropDownListViewStyle, R.attr.dropdownListPreferredItemHeight, R.attr.editTextBackground, R.attr.editTextColor, R.attr.editTextStyle, R.attr.homeAsUpIndicator, R.attr.imageButtonStyle, R.attr.listChoiceBackgroundIndicator, R.attr.listDividerAlertDialog, R.attr.listMenuViewStyle, R.attr.listPopupWindowStyle, R.attr.listPreferredItemHeight, R.attr.listPreferredItemHeightLarge, R.attr.listPreferredItemHeightSmall, R.attr.listPreferredItemPaddingLeft, R.attr.listPreferredItemPaddingRight, R.attr.panelBackground, R.attr.panelMenuListTheme, R.attr.panelMenuListWidth, R.attr.popupMenuStyle, R.attr.popupWindowStyle, R.attr.radioButtonStyle, R.attr.ratingBarStyle, R.attr.ratingBarStyleIndicator, R.attr.ratingBarStyleSmall, R.attr.searchViewStyle, R.attr.seekBarStyle, R.attr.selectableItemBackground, R.attr.selectableItemBackgroundBorderless, R.attr.spinnerDropDownItemStyle, R.attr.spinnerStyle, R.attr.switchStyle, R.attr.textAppearanceLargePopupMenu, R.attr.textAppearanceListItem, R.attr.textAppearanceListItemSecondary, R.attr.textAppearanceListItemSmall, R.attr.textAppearancePopupMenuHeader, R.attr.textAppearanceSearchResultSubtitle, R.attr.textAppearanceSearchResultTitle, R.attr.textAppearanceSmallPopupMenu, R.attr.textColorAlertDialogListItem, R.attr.textColorSearchUrl, R.attr.toolbarNavigationButtonStyle, R.attr.toolbarStyle, R.attr.tooltipForegroundColor, R.attr.tooltipFrameBackground, R.attr.viewInflaterClass, R.attr.windowActionBar, R.attr.windowActionBarOverlay, R.attr.windowActionModeOverlay, R.attr.windowFixedHeightMajor, R.attr.windowFixedHeightMinor, R.attr.windowFixedWidthMajor, R.attr.windowFixedWidthMinor, R.attr.windowMinWidthMajor, R.attr.windowMinWidthMinor, R.attr.windowNoTitle};
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarDivider; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarItemBackground; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarPopupTheme; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSize; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSplitStyle; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarStyle; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabBarStyle; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabStyle; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabTextStyle; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTheme; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarWidgetTheme; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionButtonStyle; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionDropDownStyle; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextAppearance; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextColor; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeBackground; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseButtonStyle; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseDrawable; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCopyDrawable; // = 20;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCutDrawable; // = 21;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeFindDrawable; // = 22;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePasteDrawable; // = 23;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePopupWindowStyle; // = 24;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSelectAllDrawable; // = 25;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeShareDrawable; // = 26;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSplitBackground; // = 27;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeStyle; // = 28;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeWebSearchDrawable; // = 29;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowButtonStyle; // = 30;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowMenuStyle; // = 31;
    @DexIgnore
    public static /* final */ int AppCompatTheme_activityChooserViewStyle; // = 32;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogButtonGroupStyle; // = 33;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogCenterButtons; // = 34;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogStyle; // = 35;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogTheme; // = 36;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowIsFloating; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTheme_autoCompleteTextViewStyle; // = 37;
    @DexIgnore
    public static /* final */ int AppCompatTheme_borderlessButtonStyle; // = 38;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarButtonStyle; // = 39;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNegativeButtonStyle; // = 40;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNeutralButtonStyle; // = 41;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarPositiveButtonStyle; // = 42;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarStyle; // = 43;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyle; // = 44;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyleSmall; // = 45;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkboxStyle; // = 46;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkedTextViewStyle; // = 47;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorAccent; // = 48;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorBackgroundFloating; // = 49;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorButtonNormal; // = 50;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlActivated; // = 51;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlHighlight; // = 52;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlNormal; // = 53;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorError; // = 54;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimary; // = 55;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimaryDark; // = 56;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorSwitchThumbNormal; // = 57;
    @DexIgnore
    public static /* final */ int AppCompatTheme_controlBackground; // = 58;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogCornerRadius; // = 59;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogPreferredPadding; // = 60;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogTheme; // = 61;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerHorizontal; // = 62;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerVertical; // = 63;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropDownListViewStyle; // = 64;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropdownListPreferredItemHeight; // = 65;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextBackground; // = 66;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextColor; // = 67;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextStyle; // = 68;
    @DexIgnore
    public static /* final */ int AppCompatTheme_homeAsUpIndicator; // = 69;
    @DexIgnore
    public static /* final */ int AppCompatTheme_imageButtonStyle; // = 70;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceBackgroundIndicator; // = 71;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listDividerAlertDialog; // = 72;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listMenuViewStyle; // = 73;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPopupWindowStyle; // = 74;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeight; // = 75;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightLarge; // = 76;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightSmall; // = 77;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingLeft; // = 78;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingRight; // = 79;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelBackground; // = 80;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListTheme; // = 81;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListWidth; // = 82;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupMenuStyle; // = 83;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupWindowStyle; // = 84;
    @DexIgnore
    public static /* final */ int AppCompatTheme_radioButtonStyle; // = 85;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyle; // = 86;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleIndicator; // = 87;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleSmall; // = 88;
    @DexIgnore
    public static /* final */ int AppCompatTheme_searchViewStyle; // = 89;
    @DexIgnore
    public static /* final */ int AppCompatTheme_seekBarStyle; // = 90;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackground; // = 91;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackgroundBorderless; // = 92;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerDropDownItemStyle; // = 93;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerStyle; // = 94;
    @DexIgnore
    public static /* final */ int AppCompatTheme_switchStyle; // = 95;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceLargePopupMenu; // = 96;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItem; // = 97;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSecondary; // = 98;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSmall; // = 99;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearancePopupMenuHeader; // = 100;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultSubtitle; // = 101;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultTitle; // = 102;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSmallPopupMenu; // = 103;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorAlertDialogListItem; // = 104;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorSearchUrl; // = 105;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarNavigationButtonStyle; // = 106;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarStyle; // = 107;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipForegroundColor; // = 108;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipFrameBackground; // = 109;
    @DexIgnore
    public static /* final */ int AppCompatTheme_viewInflaterClass; // = 110;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBar; // = 111;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBarOverlay; // = 112;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionModeOverlay; // = 113;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMajor; // = 114;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMinor; // = 115;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMajor; // = 116;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMinor; // = 117;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMajor; // = 118;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMinor; // = 119;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowNoTitle; // = 120;
    @DexIgnore
    public static /* final */ int[] AutoResizeTextView; // = {R.attr.minTextSize};
    @DexIgnore
    public static /* final */ int AutoResizeTextView_minTextSize; // = 0;
    @DexIgnore
    public static /* final */ int[] BarChart; // = {R.attr.bar_end_margin, R.attr.bar_goal_line_color, R.attr.bar_goal_line_dash_gap, R.attr.bar_goal_line_dash_width, R.attr.bar_goal_line_width, R.attr.bar_icon_drawable, R.attr.bar_icon_padding, R.attr.bar_icon_size, R.attr.bar_start_margin, R.attr.bar_text_color, R.attr.bar_text_font, R.attr.bar_text_height, R.attr.bar_text_margin, R.attr.bar_text_size, R.attr.bar_width};
    @DexIgnore
    public static /* final */ int BarChart_bar_end_margin; // = 0;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_color; // = 1;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_dash_gap; // = 2;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_dash_width; // = 3;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_width; // = 4;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_drawable; // = 5;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_padding; // = 6;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_size; // = 7;
    @DexIgnore
    public static /* final */ int BarChart_bar_start_margin; // = 8;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_color; // = 9;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_font; // = 10;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_height; // = 11;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_margin; // = 12;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_size; // = 13;
    @DexIgnore
    public static /* final */ int BarChart_bar_width; // = 14;
    @DexIgnore
    public static /* final */ int[] BaseChart; // = {R.attr.bc_active_color, R.attr.bc_default_color, R.attr.bc_flexible_size, R.attr.bc_fontFamily, R.attr.bc_goal_icon_res, R.attr.bc_goal_icon_size, R.attr.bc_goal_line_color, R.attr.bc_goal_line_dash_gap, R.attr.bc_goal_line_dash_width, R.attr.bc_goal_line_width, R.attr.bc_goal_text_format, R.attr.bc_graph_legend_margin, R.attr.bc_highest_color, R.attr.bc_inactive_color, R.attr.bc_legend_height, R.attr.bc_legend_icon_res, R.attr.bc_legend_line_color, R.attr.bc_legend_line_width, R.attr.bc_legend_texts, R.attr.bc_lowest_color, R.attr.bc_margin, R.attr.bc_margin_end, R.attr.bc_radius, R.attr.bc_safe_area_height, R.attr.bc_safe_area_width, R.attr.bc_space, R.attr.bc_star_icon_res, R.attr.bc_star_icon_size, R.attr.bc_text_color, R.attr.bc_text_margin, R.attr.bc_text_size, R.attr.bc_touch_padding, R.attr.bc_width};
    @DexIgnore
    public static /* final */ int BaseChart_bc_active_color; // = 0;
    @DexIgnore
    public static /* final */ int BaseChart_bc_default_color; // = 1;
    @DexIgnore
    public static /* final */ int BaseChart_bc_flexible_size; // = 2;
    @DexIgnore
    public static /* final */ int BaseChart_bc_fontFamily; // = 3;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_icon_res; // = 4;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_icon_size; // = 5;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_color; // = 6;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_dash_gap; // = 7;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_dash_width; // = 8;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_width; // = 9;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_text_format; // = 10;
    @DexIgnore
    public static /* final */ int BaseChart_bc_graph_legend_margin; // = 11;
    @DexIgnore
    public static /* final */ int BaseChart_bc_highest_color; // = 12;
    @DexIgnore
    public static /* final */ int BaseChart_bc_inactive_color; // = 13;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_height; // = 14;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_icon_res; // = 15;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_line_color; // = 16;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_line_width; // = 17;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_texts; // = 18;
    @DexIgnore
    public static /* final */ int BaseChart_bc_lowest_color; // = 19;
    @DexIgnore
    public static /* final */ int BaseChart_bc_margin; // = 20;
    @DexIgnore
    public static /* final */ int BaseChart_bc_margin_end; // = 21;
    @DexIgnore
    public static /* final */ int BaseChart_bc_radius; // = 22;
    @DexIgnore
    public static /* final */ int BaseChart_bc_safe_area_height; // = 23;
    @DexIgnore
    public static /* final */ int BaseChart_bc_safe_area_width; // = 24;
    @DexIgnore
    public static /* final */ int BaseChart_bc_space; // = 25;
    @DexIgnore
    public static /* final */ int BaseChart_bc_star_icon_res; // = 26;
    @DexIgnore
    public static /* final */ int BaseChart_bc_star_icon_size; // = 27;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_color; // = 28;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_margin; // = 29;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_size; // = 30;
    @DexIgnore
    public static /* final */ int BaseChart_bc_touch_padding; // = 31;
    @DexIgnore
    public static /* final */ int BaseChart_bc_width; // = 32;
    @DexIgnore
    public static /* final */ int[] BatteryIndicator; // = {R.attr.batteryAlertPercentage, R.attr.batteryPercentage, R.attr.isDeviceConnected};
    @DexIgnore
    public static /* final */ int[] BatteryIndicatorAsset; // = {R.attr.batteryPercentage_0, R.attr.batteryPercentage_100, R.attr.batteryPercentage_25, R.attr.batteryPercentage_50, R.attr.batteryPercentage_75, R.attr.batteryRealPercentage};
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_0; // = 0;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_100; // = 1;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_25; // = 2;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_50; // = 3;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_75; // = 4;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryRealPercentage; // = 5;
    @DexIgnore
    public static /* final */ int BatteryIndicator_batteryAlertPercentage; // = 0;
    @DexIgnore
    public static /* final */ int BatteryIndicator_batteryPercentage; // = 1;
    @DexIgnore
    public static /* final */ int BatteryIndicator_isDeviceConnected; // = 2;
    @DexIgnore
    public static /* final */ int[] BlurView; // = {R.attr.blurDownSampleFactor, R.attr.blurOverlayColor, R.attr.blurRadius};
    @DexIgnore
    public static /* final */ int BlurView_blurDownSampleFactor; // = 0;
    @DexIgnore
    public static /* final */ int BlurView_blurOverlayColor; // = 1;
    @DexIgnore
    public static /* final */ int BlurView_blurRadius; // = 2;
    @DexIgnore
    public static /* final */ int[] BottomAppBar; // = {R.attr.backgroundTint, R.attr.fabAlignmentMode, R.attr.fabCradleMargin, R.attr.fabCradleRoundedCornerRadius, R.attr.fabCradleVerticalOffset, R.attr.hideOnScroll};
    @DexIgnore
    public static /* final */ int BottomAppBar_backgroundTint; // = 0;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabAlignmentMode; // = 1;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleMargin; // = 2;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleRoundedCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleVerticalOffset; // = 4;
    @DexIgnore
    public static /* final */ int BottomAppBar_hideOnScroll; // = 5;
    @DexIgnore
    public static /* final */ int[] BottomNavigationView; // = {R.attr.elevation, R.attr.itemBackground, R.attr.itemHorizontalTranslationEnabled, R.attr.itemIconSize, R.attr.itemIconTint, R.attr.itemTextAppearanceActive, R.attr.itemTextAppearanceInactive, R.attr.itemTextColor, R.attr.labelVisibilityMode, R.attr.menu};
    @DexIgnore
    public static /* final */ int BottomNavigationView_elevation; // = 0;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemBackground; // = 1;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemHorizontalTranslationEnabled; // = 2;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemIconSize; // = 3;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemIconTint; // = 4;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextAppearanceActive; // = 5;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextAppearanceInactive; // = 6;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextColor; // = 7;
    @DexIgnore
    public static /* final */ int BottomNavigationView_labelVisibilityMode; // = 8;
    @DexIgnore
    public static /* final */ int BottomNavigationView_menu; // = 9;
    @DexIgnore
    public static /* final */ int[] BottomSheetBehavior_Layout; // = {R.attr.behavior_fitToContents, R.attr.behavior_hideable, R.attr.behavior_peekHeight, R.attr.behavior_skipCollapsed};
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_fitToContents; // = 0;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_hideable; // = 1;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_peekHeight; // = 2;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_skipCollapsed; // = 3;
    @DexIgnore
    public static /* final */ int[] ButtonBarContainerTheme; // = {R.attr.metaButtonBarButtonStyle, R.attr.metaButtonBarStyle};
    @DexIgnore
    public static /* final */ int ButtonBarContainerTheme_metaButtonBarButtonStyle; // = 0;
    @DexIgnore
    public static /* final */ int ButtonBarContainerTheme_metaButtonBarStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] ButtonBarLayout; // = {R.attr.allowStacking};
    @DexIgnore
    public static /* final */ int ButtonBarLayout_allowStacking; // = 0;
    @DexIgnore
    public static /* final */ int[] ButtonFeatureView; // = {R.attr.belongs_to_tracker, R.attr.feature_description, R.attr.feature_name, R.attr.icon_drawable, R.attr.is_locked, R.attr.sub_icon_drawable};
    @DexIgnore
    public static /* final */ int ButtonFeatureView_belongs_to_tracker; // = 0;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_feature_description; // = 1;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_feature_name; // = 2;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_icon_drawable; // = 3;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_is_locked; // = 4;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_sub_icon_drawable; // = 5;
    @DexIgnore
    public static /* final */ int[] CardStackView; // = {R.attr.bottomOverlay, R.attr.elevationEnabled, R.attr.leftOverlay, R.attr.rightOverlay, R.attr.scaleDiff, R.attr.stackFrom, R.attr.swipeDirection, R.attr.swipeEnabled, R.attr.swipeThreshold, R.attr.topOverlay, R.attr.translationDiff, R.attr.visibleCount};
    @DexIgnore
    public static /* final */ int CardStackView_bottomOverlay; // = 0;
    @DexIgnore
    public static /* final */ int CardStackView_elevationEnabled; // = 1;
    @DexIgnore
    public static /* final */ int CardStackView_leftOverlay; // = 2;
    @DexIgnore
    public static /* final */ int CardStackView_rightOverlay; // = 3;
    @DexIgnore
    public static /* final */ int CardStackView_scaleDiff; // = 4;
    @DexIgnore
    public static /* final */ int CardStackView_stackFrom; // = 5;
    @DexIgnore
    public static /* final */ int CardStackView_swipeDirection; // = 6;
    @DexIgnore
    public static /* final */ int CardStackView_swipeEnabled; // = 7;
    @DexIgnore
    public static /* final */ int CardStackView_swipeThreshold; // = 8;
    @DexIgnore
    public static /* final */ int CardStackView_topOverlay; // = 9;
    @DexIgnore
    public static /* final */ int CardStackView_translationDiff; // = 10;
    @DexIgnore
    public static /* final */ int CardStackView_visibleCount; // = 11;
    @DexIgnore
    public static /* final */ int[] CardView; // = {16843071, 16843072, R.attr.cardBackgroundColor, R.attr.cardCornerRadius, R.attr.cardElevation, R.attr.cardMaxElevation, R.attr.cardPreventCornerOverlap, R.attr.cardUseCompatPadding, R.attr.contentPadding, R.attr.contentPaddingBottom, R.attr.contentPaddingLeft, R.attr.contentPaddingRight, R.attr.contentPaddingTop};
    @DexIgnore
    public static /* final */ int CardView_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int CardView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int CardView_cardBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int CardView_cardCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int CardView_cardElevation; // = 4;
    @DexIgnore
    public static /* final */ int CardView_cardMaxElevation; // = 5;
    @DexIgnore
    public static /* final */ int CardView_cardPreventCornerOverlap; // = 6;
    @DexIgnore
    public static /* final */ int CardView_cardUseCompatPadding; // = 7;
    @DexIgnore
    public static /* final */ int CardView_contentPadding; // = 8;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingBottom; // = 9;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingLeft; // = 10;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingRight; // = 11;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingTop; // = 12;
    @DexIgnore
    public static /* final */ int[] CenterDrawableGravityRadioButton; // = {16843015};
    @DexIgnore
    public static /* final */ int CenterDrawableGravityRadioButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int[] Chip; // = {16842804, 16842923, 16843039, 16843087, 16843237, R.attr.checkedIcon, R.attr.checkedIconEnabled, R.attr.checkedIconVisible, R.attr.chipBackgroundColor, R.attr.chipCornerRadius, R.attr.chipEndPadding, R.attr.chipIcon, R.attr.chipIconEnabled, R.attr.chipIconSize, R.attr.chipIconTint, R.attr.chipIconVisible, R.attr.chipMinHeight, R.attr.chipStartPadding, R.attr.chipStrokeColor, R.attr.chipStrokeWidth, R.attr.closeIcon, R.attr.closeIconEnabled, R.attr.closeIconEndPadding, R.attr.closeIconSize, R.attr.closeIconStartPadding, R.attr.closeIconTint, R.attr.closeIconVisible, R.attr.hideMotionSpec, R.attr.iconEndPadding, R.attr.iconStartPadding, R.attr.rippleColor, R.attr.showMotionSpec, R.attr.textEndPadding, R.attr.textStartPadding};
    @DexIgnore
    public static /* final */ int[] ChipGroup; // = {R.attr.checkedChip, R.attr.chipSpacing, R.attr.chipSpacingHorizontal, R.attr.chipSpacingVertical, R.attr.singleLine, R.attr.singleSelection};
    @DexIgnore
    public static /* final */ int ChipGroup_checkedChip; // = 0;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacing; // = 1;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacingHorizontal; // = 2;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacingVertical; // = 3;
    @DexIgnore
    public static /* final */ int ChipGroup_singleLine; // = 4;
    @DexIgnore
    public static /* final */ int ChipGroup_singleSelection; // = 5;
    @DexIgnore
    public static /* final */ int Chip_android_checkable; // = 4;
    @DexIgnore
    public static /* final */ int Chip_android_ellipsize; // = 1;
    @DexIgnore
    public static /* final */ int Chip_android_maxWidth; // = 2;
    @DexIgnore
    public static /* final */ int Chip_android_text; // = 3;
    @DexIgnore
    public static /* final */ int Chip_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int Chip_checkedIcon; // = 5;
    @DexIgnore
    public static /* final */ int Chip_checkedIconEnabled; // = 6;
    @DexIgnore
    public static /* final */ int Chip_checkedIconVisible; // = 7;
    @DexIgnore
    public static /* final */ int Chip_chipBackgroundColor; // = 8;
    @DexIgnore
    public static /* final */ int Chip_chipCornerRadius; // = 9;
    @DexIgnore
    public static /* final */ int Chip_chipEndPadding; // = 10;
    @DexIgnore
    public static /* final */ int Chip_chipIcon; // = 11;
    @DexIgnore
    public static /* final */ int Chip_chipIconEnabled; // = 12;
    @DexIgnore
    public static /* final */ int Chip_chipIconSize; // = 13;
    @DexIgnore
    public static /* final */ int Chip_chipIconTint; // = 14;
    @DexIgnore
    public static /* final */ int Chip_chipIconVisible; // = 15;
    @DexIgnore
    public static /* final */ int Chip_chipMinHeight; // = 16;
    @DexIgnore
    public static /* final */ int Chip_chipStartPadding; // = 17;
    @DexIgnore
    public static /* final */ int Chip_chipStrokeColor; // = 18;
    @DexIgnore
    public static /* final */ int Chip_chipStrokeWidth; // = 19;
    @DexIgnore
    public static /* final */ int Chip_closeIcon; // = 20;
    @DexIgnore
    public static /* final */ int Chip_closeIconEnabled; // = 21;
    @DexIgnore
    public static /* final */ int Chip_closeIconEndPadding; // = 22;
    @DexIgnore
    public static /* final */ int Chip_closeIconSize; // = 23;
    @DexIgnore
    public static /* final */ int Chip_closeIconStartPadding; // = 24;
    @DexIgnore
    public static /* final */ int Chip_closeIconTint; // = 25;
    @DexIgnore
    public static /* final */ int Chip_closeIconVisible; // = 26;
    @DexIgnore
    public static /* final */ int Chip_hideMotionSpec; // = 27;
    @DexIgnore
    public static /* final */ int Chip_iconEndPadding; // = 28;
    @DexIgnore
    public static /* final */ int Chip_iconStartPadding; // = 29;
    @DexIgnore
    public static /* final */ int Chip_rippleColor; // = 30;
    @DexIgnore
    public static /* final */ int Chip_showMotionSpec; // = 31;
    @DexIgnore
    public static /* final */ int Chip_textEndPadding; // = 32;
    @DexIgnore
    public static /* final */ int Chip_textStartPadding; // = 33;
    @DexIgnore
    public static /* final */ int[] ChooseDeviceTypeButton; // = {R.attr.choose_device_type_btn_icon, R.attr.choose_device_type_btn_prefix, R.attr.choose_device_type_btn_title};
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_icon; // = 0;
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_prefix; // = 1;
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_title; // = 2;
    @DexIgnore
    public static /* final */ int[] Circle; // = {R.attr.mActiveColor, R.attr.mInactiveColor};
    @DexIgnore
    public static /* final */ int[] CircleImageViewProgressBar; // = {R.attr.centercircle_diammterer, R.attr.draw_anticlockwise, R.attr.normal_border_width, R.attr.progress_border_color, R.attr.progress_border_overlay, R.attr.progress_border_width, R.attr.progress_color, R.attr.progress_fill_color, R.attr.progress_startAngle};
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_centercircle_diammterer; // = 0;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_draw_anticlockwise; // = 1;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_normal_border_width; // = 2;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_color; // = 3;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_overlay; // = 4;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_width; // = 5;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_color; // = 6;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_fill_color; // = 7;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_startAngle; // = 8;
    @DexIgnore
    public static /* final */ int[] CirclePageIndicator; // = {16842948, 16842964, R.attr.fillColor, R.attr.indicator_centered, R.attr.pageColor, R.attr.radius, R.attr.snap, R.attr.strokeColor, R.attr.strokeWidth};
    @DexIgnore
    public static /* final */ int CirclePageIndicator_android_background; // = 1;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_fillColor; // = 2;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_indicator_centered; // = 3;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_pageColor; // = 4;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_radius; // = 5;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_snap; // = 6;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_strokeColor; // = 7;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_strokeWidth; // = 8;
    @DexIgnore
    public static /* final */ int[] CircleTextWidget; // = {R.attr.circle_box_bg_deselected, R.attr.circle_box_bg_selected, R.attr.circle_content, R.attr.circle_content_color_deselected, R.attr.circle_content_color_selected, R.attr.circle_content_size, R.attr.circle_state_enabled};
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_box_bg_deselected; // = 0;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_box_bg_selected; // = 1;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content; // = 2;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_color_deselected; // = 3;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_color_selected; // = 4;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_size; // = 5;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_state_enabled; // = 6;
    @DexIgnore
    public static /* final */ int Circle_mActiveColor; // = 0;
    @DexIgnore
    public static /* final */ int Circle_mInactiveColor; // = 1;
    @DexIgnore
    public static /* final */ int[] ClearableEditText; // = {R.attr.clearableEditBackground, R.attr.clearablesBackground, R.attr.editTextHint, R.attr.rightIcon};
    @DexIgnore
    public static /* final */ int ClearableEditText_clearableEditBackground; // = 0;
    @DexIgnore
    public static /* final */ int ClearableEditText_clearablesBackground; // = 1;
    @DexIgnore
    public static /* final */ int ClearableEditText_editTextHint; // = 2;
    @DexIgnore
    public static /* final */ int ClearableEditText_rightIcon; // = 3;
    @DexIgnore
    public static /* final */ int[] ClockView; // = {R.attr.cv_finalNumeralHeight, R.attr.cv_finalPadding, R.attr.cv_fontColor, R.attr.cv_fontName, R.attr.cv_fontSelectedColor, R.attr.cv_fontSize, R.attr.cv_maxHeight, R.attr.cv_minHeight, R.attr.cv_numeralSelectedBackgroundColor};
    @DexIgnore
    public static /* final */ int ClockView_cv_finalNumeralHeight; // = 0;
    @DexIgnore
    public static /* final */ int ClockView_cv_finalPadding; // = 1;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontColor; // = 2;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontName; // = 3;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontSelectedColor; // = 4;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontSize; // = 5;
    @DexIgnore
    public static /* final */ int ClockView_cv_maxHeight; // = 6;
    @DexIgnore
    public static /* final */ int ClockView_cv_minHeight; // = 7;
    @DexIgnore
    public static /* final */ int ClockView_cv_numeralSelectedBackgroundColor; // = 8;
    @DexIgnore
    public static /* final */ int[] CollapsingToolbarLayout; // = {R.attr.collapsedTitleGravity, R.attr.collapsedTitleTextAppearance, R.attr.contentScrim, R.attr.expandedTitleGravity, R.attr.expandedTitleMargin, R.attr.expandedTitleMarginBottom, R.attr.expandedTitleMarginEnd, R.attr.expandedTitleMarginStart, R.attr.expandedTitleMarginTop, R.attr.expandedTitleTextAppearance, R.attr.scrimAnimationDuration, R.attr.scrimVisibleHeightTrigger, R.attr.statusBarScrim, R.attr.title, R.attr.titleEnabled, R.attr.toolbarId};
    @DexIgnore
    public static /* final */ int[] CollapsingToolbarLayout_Layout; // = {R.attr.layout_collapseMode, R.attr.layout_collapseParallaxMultiplier};
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_Layout_layout_collapseMode; // = 0;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier; // = 1;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_collapsedTitleGravity; // = 0;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_collapsedTitleTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_contentScrim; // = 2;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleGravity; // = 3;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMargin; // = 4;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginBottom; // = 5;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginEnd; // = 6;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginStart; // = 7;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginTop; // = 8;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleTextAppearance; // = 9;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_scrimAnimationDuration; // = 10;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_scrimVisibleHeightTrigger; // = 11;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_statusBarScrim; // = 12;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_title; // = 13;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_titleEnabled; // = 14;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_toolbarId; // = 15;
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, R.attr.alpha};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] CompoundButton; // = {16843015, R.attr.buttonTint, R.attr.buttonTintMode};
    @DexIgnore
    public static /* final */ int CompoundButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTint; // = 1;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_Layout; // = {16842948, 16843039, 16843040, 16843071, 16843072, R.attr.barrierAllowsGoneWidgets, R.attr.barrierDirection, R.attr.chainUseRtl, R.attr.constraintSet, R.attr.constraint_referenced_ids, R.attr.layout_constrainedHeight, R.attr.layout_constrainedWidth, R.attr.layout_constraintBaseline_creator, R.attr.layout_constraintBaseline_toBaselineOf, R.attr.layout_constraintBottom_creator, R.attr.layout_constraintBottom_toBottomOf, R.attr.layout_constraintBottom_toTopOf, R.attr.layout_constraintCircle, R.attr.layout_constraintCircleAngle, R.attr.layout_constraintCircleRadius, R.attr.layout_constraintDimensionRatio, R.attr.layout_constraintEnd_toEndOf, R.attr.layout_constraintEnd_toStartOf, R.attr.layout_constraintGuide_begin, R.attr.layout_constraintGuide_end, R.attr.layout_constraintGuide_percent, R.attr.layout_constraintHeight_default, R.attr.layout_constraintHeight_max, R.attr.layout_constraintHeight_min, R.attr.layout_constraintHeight_percent, R.attr.layout_constraintHorizontal_bias, R.attr.layout_constraintHorizontal_chainStyle, R.attr.layout_constraintHorizontal_weight, R.attr.layout_constraintLeft_creator, R.attr.layout_constraintLeft_toLeftOf, R.attr.layout_constraintLeft_toRightOf, R.attr.layout_constraintRight_creator, R.attr.layout_constraintRight_toLeftOf, R.attr.layout_constraintRight_toRightOf, R.attr.layout_constraintStart_toEndOf, R.attr.layout_constraintStart_toStartOf, R.attr.layout_constraintTop_creator, R.attr.layout_constraintTop_toBottomOf, R.attr.layout_constraintTop_toTopOf, R.attr.layout_constraintVertical_bias, R.attr.layout_constraintVertical_chainStyle, R.attr.layout_constraintVertical_weight, R.attr.layout_constraintWidth_default, R.attr.layout_constraintWidth_max, R.attr.layout_constraintWidth_min, R.attr.layout_constraintWidth_percent, R.attr.layout_editor_absoluteX, R.attr.layout_editor_absoluteY, R.attr.layout_goneMarginBottom, R.attr.layout_goneMarginEnd, R.attr.layout_goneMarginLeft, R.attr.layout_goneMarginRight, R.attr.layout_goneMarginStart, R.attr.layout_goneMarginTop, R.attr.layout_optimizationLevel};
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxHeight; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minHeight; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minWidth; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierAllowsGoneWidgets; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierDirection; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_chainUseRtl; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraintSet; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraint_referenced_ids; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_creator; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_creator; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toTopOf; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircle; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleAngle; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleRadius; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintDimensionRatio; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toEndOf; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toStartOf; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_begin; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_end; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_percent; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_default; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_max; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_min; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_percent; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_bias; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_weight; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toRightOf; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_creator; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toLeftOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toRightOf; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toEndOf; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toStartOf; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_creator; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toBottomOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toTopOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_bias; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_chainStyle; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_weight; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteX; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteY; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginBottom; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginEnd; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginLeft; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginRight; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginStart; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginTop; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_optimizationLevel; // = 59;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_placeholder; // = {R.attr.content, R.attr.emptyVisibility};
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_content; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_emptyVisibility; // = 1;
    @DexIgnore
    public static /* final */ int[] ConstraintSet; // = {16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, R.attr.barrierAllowsGoneWidgets, R.attr.barrierDirection, R.attr.chainUseRtl, R.attr.constraint_referenced_ids, R.attr.layout_constrainedHeight, R.attr.layout_constrainedWidth, R.attr.layout_constraintBaseline_creator, R.attr.layout_constraintBaseline_toBaselineOf, R.attr.layout_constraintBottom_creator, R.attr.layout_constraintBottom_toBottomOf, R.attr.layout_constraintBottom_toTopOf, R.attr.layout_constraintCircle, R.attr.layout_constraintCircleAngle, R.attr.layout_constraintCircleRadius, R.attr.layout_constraintDimensionRatio, R.attr.layout_constraintEnd_toEndOf, R.attr.layout_constraintEnd_toStartOf, R.attr.layout_constraintGuide_begin, R.attr.layout_constraintGuide_end, R.attr.layout_constraintGuide_percent, R.attr.layout_constraintHeight_default, R.attr.layout_constraintHeight_max, R.attr.layout_constraintHeight_min, R.attr.layout_constraintHeight_percent, R.attr.layout_constraintHorizontal_bias, R.attr.layout_constraintHorizontal_chainStyle, R.attr.layout_constraintHorizontal_weight, R.attr.layout_constraintLeft_creator, R.attr.layout_constraintLeft_toLeftOf, R.attr.layout_constraintLeft_toRightOf, R.attr.layout_constraintRight_creator, R.attr.layout_constraintRight_toLeftOf, R.attr.layout_constraintRight_toRightOf, R.attr.layout_constraintStart_toEndOf, R.attr.layout_constraintStart_toStartOf, R.attr.layout_constraintTop_creator, R.attr.layout_constraintTop_toBottomOf, R.attr.layout_constraintTop_toTopOf, R.attr.layout_constraintVertical_bias, R.attr.layout_constraintVertical_chainStyle, R.attr.layout_constraintVertical_weight, R.attr.layout_constraintWidth_default, R.attr.layout_constraintWidth_max, R.attr.layout_constraintWidth_min, R.attr.layout_constraintWidth_percent, R.attr.layout_editor_absoluteX, R.attr.layout_editor_absoluteY, R.attr.layout_goneMarginBottom, R.attr.layout_goneMarginEnd, R.attr.layout_goneMarginLeft, R.attr.layout_goneMarginRight, R.attr.layout_goneMarginStart, R.attr.layout_goneMarginTop};
    @DexIgnore
    public static /* final */ int ConstraintSet_android_alpha; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_elevation; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_id; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_height; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginBottom; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginEnd; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginLeft; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginRight; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginStart; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginTop; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_width; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxWidth; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minHeight; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotation; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationX; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationY; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleX; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleY; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotX; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotY; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationX; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationY; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationZ; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_visibility; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierAllowsGoneWidgets; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierDirection; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintSet_chainUseRtl; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintSet_constraint_referenced_ids; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedHeight; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedWidth; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_toBaselineOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_creator; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toBottomOf; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toTopOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircle; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleAngle; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleRadius; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintDimensionRatio; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toEndOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toStartOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_begin; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_end; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_percent; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_bias; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_chainStyle; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_weight; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_creator; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toLeftOf; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toRightOf; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_creator; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toLeftOf; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toRightOf; // = 59;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toEndOf; // = 60;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toStartOf; // = 61;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_creator; // = 62;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toBottomOf; // = 63;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toTopOf; // = 64;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_bias; // = 65;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_chainStyle; // = 66;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_weight; // = 67;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_default; // = 68;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_max; // = 69;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_min; // = 70;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_percent; // = 71;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteX; // = 72;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteY; // = 73;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginBottom; // = 74;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginEnd; // = 75;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginLeft; // = 76;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginRight; // = 77;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginStart; // = 78;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginTop; // = 79;
    @DexIgnore
    public static /* final */ int[] ContactDetailWidget; // = {R.attr.contact_detail_icon, R.attr.contact_detail_title};
    @DexIgnore
    public static /* final */ int ContactDetailWidget_contact_detail_icon; // = 0;
    @DexIgnore
    public static /* final */ int ContactDetailWidget_contact_detail_title; // = 1;
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout; // = {R.attr.keylines, R.attr.statusBarBackground};
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout_Layout; // = {16842931, R.attr.layout_anchor, R.attr.layout_anchorGravity, R.attr.layout_behavior, R.attr.layout_dodgeInsetEdges, R.attr.layout_insetEdge, R.attr.layout_keyline};
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchor; // = 1;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchorGravity; // = 2;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_behavior; // = 3;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_dodgeInsetEdges; // = 4;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_insetEdge; // = 5;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_keyline; // = 6;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_keylines; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_statusBarBackground; // = 1;
    @DexIgnore
    public static /* final */ int[] CuriosityDrawerLayout; // = {R.attr.bottomBarHeight};
    @DexIgnore
    public static /* final */ int CuriosityDrawerLayout_bottomBarHeight; // = 0;
    @DexIgnore
    public static /* final */ int[] CustomClockView; // = {R.attr.backgroundColor, R.attr.backgroundStrokeWidth, R.attr.defaultMarkerColor, R.attr.defaultMarkerColorInactive, R.attr.fontName, R.attr.handLength, R.attr.handWidth, R.attr.isRoundedPaint, R.attr.markerColor, R.attr.markerColorInactive, R.attr.markerLength, R.attr.markerWidth, R.attr.paddingInside, R.attr.style, R.attr.textSize, R.attr.textValueColor, R.attr.textValueColorInactive};
    @DexIgnore
    public static /* final */ int CustomClockView_backgroundColor; // = 0;
    @DexIgnore
    public static /* final */ int CustomClockView_backgroundStrokeWidth; // = 1;
    @DexIgnore
    public static /* final */ int CustomClockView_defaultMarkerColor; // = 2;
    @DexIgnore
    public static /* final */ int CustomClockView_defaultMarkerColorInactive; // = 3;
    @DexIgnore
    public static /* final */ int CustomClockView_fontName; // = 4;
    @DexIgnore
    public static /* final */ int CustomClockView_handLength; // = 5;
    @DexIgnore
    public static /* final */ int CustomClockView_handWidth; // = 6;
    @DexIgnore
    public static /* final */ int CustomClockView_isRoundedPaint; // = 7;
    @DexIgnore
    public static /* final */ int CustomClockView_markerColor; // = 8;
    @DexIgnore
    public static /* final */ int CustomClockView_markerColorInactive; // = 9;
    @DexIgnore
    public static /* final */ int CustomClockView_markerLength; // = 10;
    @DexIgnore
    public static /* final */ int CustomClockView_markerWidth; // = 11;
    @DexIgnore
    public static /* final */ int CustomClockView_paddingInside; // = 12;
    @DexIgnore
    public static /* final */ int CustomClockView_style; // = 13;
    @DexIgnore
    public static /* final */ int CustomClockView_textSize; // = 14;
    @DexIgnore
    public static /* final */ int CustomClockView_textValueColor; // = 15;
    @DexIgnore
    public static /* final */ int CustomClockView_textValueColorInactive; // = 16;
    @DexIgnore
    public static /* final */ int[] CustomDrawerLayout; // = {R.attr.minDrawerMargin};
    @DexIgnore
    public static /* final */ int CustomDrawerLayout_minDrawerMargin; // = 0;
    @DexIgnore
    public static /* final */ int[] DashBar; // = {R.attr.dash_bar_background_color, R.attr.dash_bar_color_end, R.attr.dash_bar_color_start, R.attr.dash_bar_dash_width, R.attr.dash_bar_length, R.attr.dash_bar_progress, R.attr.dash_bar_stroke_width};
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_background_color; // = 0;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_color_end; // = 1;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_color_start; // = 2;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_dash_width; // = 3;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_length; // = 4;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_progress; // = 5;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_stroke_width; // = 6;
    @DexIgnore
    public static /* final */ int[] DashboardVisualizationRings; // = {R.attr.visualization_active_time_color, R.attr.visualization_active_time_second_color, R.attr.visualization_background_color, R.attr.visualization_calories_color, R.attr.visualization_calories_second_color, R.attr.visualization_ring_bg_color, R.attr.visualization_sleep_first_color, R.attr.visualization_sleep_second_color, R.attr.visualization_sleep_third_color, R.attr.visualization_steps_color, R.attr.visualization_steps_second_color};
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_active_time_color; // = 0;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_active_time_second_color; // = 1;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_background_color; // = 2;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_calories_color; // = 3;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_calories_second_color; // = 4;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_ring_bg_color; // = 5;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_first_color; // = 6;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_second_color; // = 7;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_third_color; // = 8;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_steps_color; // = 9;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_steps_second_color; // = 10;
    @DexIgnore
    public static /* final */ int[] DesignTheme; // = {R.attr.bottomSheetDialogTheme, R.attr.bottomSheetStyle};
    @DexIgnore
    public static /* final */ int DesignTheme_bottomSheetDialogTheme; // = 0;
    @DexIgnore
    public static /* final */ int DesignTheme_bottomSheetStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] DeviceInfoField; // = {R.attr.device_info_field_label};
    @DexIgnore
    public static /* final */ int DeviceInfoField_device_info_field_label; // = 0;
    @DexIgnore
    public static /* final */ int[] DialView; // = {R.attr.dialViewBackgroundImage, R.attr.dialViewDialImage};
    @DexIgnore
    public static /* final */ int DialView_dialViewBackgroundImage; // = 0;
    @DexIgnore
    public static /* final */ int DialView_dialViewDialImage; // = 1;
    @DexIgnore
    public static /* final */ int[] DrawerArrowToggle; // = {R.attr.arrowHeadLength, R.attr.arrowShaftLength, R.attr.barLength, R.attr.color, R.attr.drawableSize, R.attr.gapBetweenBars, R.attr.spinBars, R.attr.thickness};
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowHeadLength; // = 0;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowShaftLength; // = 1;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_barLength; // = 2;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_color; // = 3;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_drawableSize; // = 4;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_gapBetweenBars; // = 5;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_spinBars; // = 6;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_thickness; // = 7;
    @DexIgnore
    public static /* final */ int[] EditListItem; // = {R.attr.editlistitem_autocorrect, R.attr.editlistitem_editable, R.attr.editlistitem_icon, R.attr.editlistitem_title};
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_autocorrect; // = 0;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_editable; // = 1;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_icon; // = 2;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_title; // = 3;
    @DexIgnore
    public static /* final */ int[] EmptyMessageWidget; // = {R.attr.empty_image, R.attr.empty_sub_title, R.attr.empty_subtitle_text_style, R.attr.empty_text_font_color, R.attr.empty_title};
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_image; // = 0;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_sub_title; // = 1;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_subtitle_text_style; // = 2;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_text_font_color; // = 3;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_title; // = 4;
    @DexIgnore
    public static /* final */ int[] ErrorStep; // = {R.attr.error_step_message, R.attr.error_step_title};
    @DexIgnore
    public static /* final */ int ErrorStep_error_step_message; // = 0;
    @DexIgnore
    public static /* final */ int ErrorStep_error_step_title; // = 1;
    @DexIgnore
    public static /* final */ int[] FlexibleButton; // = {R.attr.textType};
    @DexIgnore
    public static /* final */ int FlexibleButton_textType; // = 0;
    @DexIgnore
    public static /* final */ int[] FlexibleEditText; // = {R.attr.cursorEnable, R.attr.hintType, R.attr.textType, R.attr.tintType};
    @DexIgnore
    public static /* final */ int FlexibleEditText_cursorEnable; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleEditText_hintType; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleEditText_textType; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleEditText_tintType; // = 3;
    @DexIgnore
    public static /* final */ int[] FlexibleTextView; // = {R.attr.hintType, R.attr.textType, R.attr.tintType};
    @DexIgnore
    public static /* final */ int FlexibleTextView_hintType; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleTextView_textType; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleTextView_tintType; // = 2;
    @DexIgnore
    public static /* final */ int[] FloatingActionButton; // = {R.attr.backgroundTint, R.attr.backgroundTintMode, R.attr.borderWidth, R.attr.elevation, R.attr.fabCustomSize, R.attr.fabSize, R.attr.hideMotionSpec, R.attr.hoveredFocusedTranslationZ, R.attr.maxImageSize, R.attr.pressedTranslationZ, R.attr.rippleColor, R.attr.showMotionSpec, R.attr.useCompatPadding};
    @DexIgnore
    public static /* final */ int[] FloatingActionButton_Behavior_Layout; // = {R.attr.behavior_autoHide};
    @DexIgnore
    public static /* final */ int FloatingActionButton_Behavior_Layout_behavior_autoHide; // = 0;
    @DexIgnore
    public static /* final */ int FloatingActionButton_backgroundTint; // = 0;
    @DexIgnore
    public static /* final */ int FloatingActionButton_backgroundTintMode; // = 1;
    @DexIgnore
    public static /* final */ int FloatingActionButton_borderWidth; // = 2;
    @DexIgnore
    public static /* final */ int FloatingActionButton_elevation; // = 3;
    @DexIgnore
    public static /* final */ int FloatingActionButton_fabCustomSize; // = 4;
    @DexIgnore
    public static /* final */ int FloatingActionButton_fabSize; // = 5;
    @DexIgnore
    public static /* final */ int FloatingActionButton_hideMotionSpec; // = 6;
    @DexIgnore
    public static /* final */ int FloatingActionButton_hoveredFocusedTranslationZ; // = 7;
    @DexIgnore
    public static /* final */ int FloatingActionButton_maxImageSize; // = 8;
    @DexIgnore
    public static /* final */ int FloatingActionButton_pressedTranslationZ; // = 9;
    @DexIgnore
    public static /* final */ int FloatingActionButton_rippleColor; // = 10;
    @DexIgnore
    public static /* final */ int FloatingActionButton_showMotionSpec; // = 11;
    @DexIgnore
    public static /* final */ int FloatingActionButton_useCompatPadding; // = 12;
    @DexIgnore
    public static /* final */ int[] FlowLayout; // = {R.attr.itemSpacing, R.attr.lineSpacing};
    @DexIgnore
    public static /* final */ int FlowLayout_itemSpacing; // = 0;
    @DexIgnore
    public static /* final */ int FlowLayout_lineSpacing; // = 1;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] ForegroundLinearLayout; // = {16843017, 16843264, R.attr.foregroundInsidePadding};
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_android_foreground; // = 0;
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_android_foregroundGravity; // = 1;
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_foregroundInsidePadding; // = 2;
    @DexIgnore
    public static /* final */ int[] FossilButton; // = {R.attr.button_background, R.attr.button_icon, R.attr.button_title, R.attr.button_title_color};
    @DexIgnore
    public static /* final */ int FossilButton_button_background; // = 0;
    @DexIgnore
    public static /* final */ int FossilButton_button_icon; // = 1;
    @DexIgnore
    public static /* final */ int FossilButton_button_title; // = 2;
    @DexIgnore
    public static /* final */ int FossilButton_button_title_color; // = 3;
    @DexIgnore
    public static /* final */ int[] FossilCircleImageView; // = {R.attr.border_color, R.attr.border_overlay, R.attr.border_width, R.attr.fill_color, R.attr.hand_number};
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_color; // = 0;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_overlay; // = 1;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_width; // = 2;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_fill_color; // = 3;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_hand_number; // = 4;
    @DexIgnore
    public static /* final */ int[] FossilTopHeaderLayout; // = {R.attr.toolbarId};
    @DexIgnore
    public static /* final */ int[] FossilTopHeaderLayout_Layout; // = {R.attr.collapseMode, R.attr.collapseParallaxMultiplier};
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_Layout_collapseMode; // = 0;
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_Layout_collapseParallaxMultiplier; // = 1;
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_toolbarId; // = 0;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
    @DexIgnore
    public static /* final */ int[] HeartRateSleepSessionChart; // = {R.attr.HRSS_AwakeColor, R.attr.HRSS_AxesColor, R.attr.HRSS_DeepColor, R.attr.HRSS_HeartRateFontSize, R.attr.HRSS_HeartRateTextFont, R.attr.HRSS_LightColor, R.attr.HRSS_TextColor, R.attr.HRSS_TimeFontSize, R.attr.HRSS_TimeTextFont};
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_AwakeColor; // = 0;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_AxesColor; // = 1;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_DeepColor; // = 2;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_HeartRateFontSize; // = 3;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_HeartRateTextFont; // = 4;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_LightColor; // = 5;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TextColor; // = 6;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TimeFontSize; // = 7;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TimeTextFont; // = 8;
    @DexIgnore
    public static /* final */ int[] ImageButton; // = {R.attr.fossil_button_background, R.attr.fossil_button_icon, R.attr.fossil_button_title, R.attr.fossil_button_title_color};
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_background; // = 0;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_icon; // = 1;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_title; // = 2;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_title_color; // = 3;
    @DexIgnore
    public static /* final */ int[] LeftDrawableButton; // = {R.attr.background, R.attr.enabled, R.attr.icon, R.attr.icon_disable, R.attr.isSingleLine, R.attr.text, R.attr.text_color, R.attr.text_color_disable, R.attr.text_id, R.attr.text_size};
    @DexIgnore
    public static /* final */ int LeftDrawableButton_background; // = 0;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_enabled; // = 1;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_icon; // = 2;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_icon_disable; // = 3;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_isSingleLine; // = 4;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_text; // = 5;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_text_color; // = 6;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_text_color_disable; // = 7;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_text_id; // = 8;
    @DexIgnore
    public static /* final */ int LeftDrawableButton_text_size; // = 9;
    @DexIgnore
    public static /* final */ int[] LetterFastScroller; // = {R.attr.alphabetColor, R.attr.alphabetFont};
    @DexIgnore
    public static /* final */ int LetterFastScroller_alphabetColor; // = 0;
    @DexIgnore
    public static /* final */ int LetterFastScroller_alphabetFont; // = 1;
    @DexIgnore
    public static /* final */ int[] LinePageIndicator; // = {16842964, R.attr.gapWidth, R.attr.indicator_centered, R.attr.indicator_selectedColor, R.attr.indicator_strokeWidth, R.attr.indicator_unselectedBorderColor, R.attr.indicator_unselectedBorderWidth, R.attr.indicator_unselectedColor, R.attr.lineWidth};
    @DexIgnore
    public static /* final */ int LinePageIndicator_android_background; // = 0;
    @DexIgnore
    public static /* final */ int LinePageIndicator_gapWidth; // = 1;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_centered; // = 2;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_selectedColor; // = 3;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_strokeWidth; // = 4;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedBorderColor; // = 5;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedBorderWidth; // = 6;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedColor; // = 7;
    @DexIgnore
    public static /* final */ int LinePageIndicator_lineWidth; // = 8;
    @DexIgnore
    public static /* final */ int[] LinearConstraintLayout; // = {16842948};
    @DexIgnore
    public static /* final */ int LinearConstraintLayout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat; // = {16842927, 16842948, 16843046, 16843047, 16843048, R.attr.divider, R.attr.dividerPadding, R.attr.measureWithLargestChild, R.attr.showDividers};
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat_Layout; // = {16842931, 16842996, 16842997, 16843137};
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_height; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_weight; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_width; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAligned; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAlignedChildIndex; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_orientation; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_weightSum; // = 4;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_divider; // = 5;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_dividerPadding; // = 6;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_measureWithLargestChild; // = 7;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_showDividers; // = 8;
    @DexIgnore
    public static /* final */ int[] ListPopupWindow; // = {16843436, 16843437};
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownHorizontalOffset; // = 0;
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownVerticalOffset; // = 1;
    @DexIgnore
    public static /* final */ int[] LoadingImageView; // = {R.attr.circleCrop, R.attr.imageAspectRatio, R.attr.imageAspectRatioAdjust};
    @DexIgnore
    public static /* final */ int LoadingImageView_circleCrop; // = 0;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatio; // = 1;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatioAdjust; // = 2;
    @DexIgnore
    public static /* final */ int[] MapAttrs; // = {R.attr.ambientEnabled, R.attr.cameraBearing, R.attr.cameraMaxZoomPreference, R.attr.cameraMinZoomPreference, R.attr.cameraTargetLat, R.attr.cameraTargetLng, R.attr.cameraTilt, R.attr.cameraZoom, R.attr.latLngBoundsNorthEastLatitude, R.attr.latLngBoundsNorthEastLongitude, R.attr.latLngBoundsSouthWestLatitude, R.attr.latLngBoundsSouthWestLongitude, R.attr.liteMode, R.attr.mapType, R.attr.uiCompass, R.attr.uiMapToolbar, R.attr.uiRotateGestures, R.attr.uiScrollGestures, R.attr.uiScrollGesturesDuringRotateOrZoom, R.attr.uiTiltGestures, R.attr.uiZoomControls, R.attr.uiZoomGestures, R.attr.useViewLifecycle, R.attr.zOrderOnTop};
    @DexIgnore
    public static /* final */ int MapAttrs_ambientEnabled; // = 0;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraBearing; // = 1;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMaxZoomPreference; // = 2;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMinZoomPreference; // = 3;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLat; // = 4;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLng; // = 5;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTilt; // = 6;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraZoom; // = 7;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLatitude; // = 8;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLongitude; // = 9;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLatitude; // = 10;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLongitude; // = 11;
    @DexIgnore
    public static /* final */ int MapAttrs_liteMode; // = 12;
    @DexIgnore
    public static /* final */ int MapAttrs_mapType; // = 13;
    @DexIgnore
    public static /* final */ int MapAttrs_uiCompass; // = 14;
    @DexIgnore
    public static /* final */ int MapAttrs_uiMapToolbar; // = 15;
    @DexIgnore
    public static /* final */ int MapAttrs_uiRotateGestures; // = 16;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGestures; // = 17;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGesturesDuringRotateOrZoom; // = 18;
    @DexIgnore
    public static /* final */ int MapAttrs_uiTiltGestures; // = 19;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomControls; // = 20;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomGestures; // = 21;
    @DexIgnore
    public static /* final */ int MapAttrs_useViewLifecycle; // = 22;
    @DexIgnore
    public static /* final */ int MapAttrs_zOrderOnTop; // = 23;
    @DexIgnore
    public static /* final */ int[] MaterialButton; // = {16843191, 16843192, 16843193, 16843194, R.attr.backgroundTint, R.attr.backgroundTintMode, R.attr.cornerRadius, R.attr.icon, R.attr.iconGravity, R.attr.iconPadding, R.attr.iconSize, R.attr.iconTint, R.attr.iconTintMode, R.attr.rippleColor, R.attr.strokeColor, R.attr.strokeWidth};
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetBottom; // = 3;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetLeft; // = 0;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetRight; // = 1;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetTop; // = 2;
    @DexIgnore
    public static /* final */ int MaterialButton_backgroundTint; // = 4;
    @DexIgnore
    public static /* final */ int MaterialButton_backgroundTintMode; // = 5;
    @DexIgnore
    public static /* final */ int MaterialButton_cornerRadius; // = 6;
    @DexIgnore
    public static /* final */ int MaterialButton_icon; // = 7;
    @DexIgnore
    public static /* final */ int MaterialButton_iconGravity; // = 8;
    @DexIgnore
    public static /* final */ int MaterialButton_iconPadding; // = 9;
    @DexIgnore
    public static /* final */ int MaterialButton_iconSize; // = 10;
    @DexIgnore
    public static /* final */ int MaterialButton_iconTint; // = 11;
    @DexIgnore
    public static /* final */ int MaterialButton_iconTintMode; // = 12;
    @DexIgnore
    public static /* final */ int MaterialButton_rippleColor; // = 13;
    @DexIgnore
    public static /* final */ int MaterialButton_strokeColor; // = 14;
    @DexIgnore
    public static /* final */ int MaterialButton_strokeWidth; // = 15;
    @DexIgnore
    public static /* final */ int[] MaterialCardView; // = {R.attr.strokeColor, R.attr.strokeWidth};
    @DexIgnore
    public static /* final */ int MaterialCardView_strokeColor; // = 0;
    @DexIgnore
    public static /* final */ int MaterialCardView_strokeWidth; // = 1;
    @DexIgnore
    public static /* final */ int[] MaterialComponentsTheme; // = {R.attr.bottomSheetDialogTheme, R.attr.bottomSheetStyle, R.attr.chipGroupStyle, R.attr.chipStandaloneStyle, R.attr.chipStyle, R.attr.colorAccent, R.attr.colorBackgroundFloating, R.attr.colorPrimary, R.attr.colorPrimaryDark, R.attr.colorSecondary, R.attr.editTextStyle, R.attr.floatingActionButtonStyle, R.attr.materialButtonStyle, R.attr.materialCardViewStyle, R.attr.navigationViewStyle, R.attr.scrimBackground, R.attr.snackbarButtonStyle, R.attr.tabStyle, R.attr.textAppearanceBody1, R.attr.textAppearanceBody2, R.attr.textAppearanceButton, R.attr.textAppearanceCaption, R.attr.textAppearanceHeadline1, R.attr.textAppearanceHeadline2, R.attr.textAppearanceHeadline3, R.attr.textAppearanceHeadline4, R.attr.textAppearanceHeadline5, R.attr.textAppearanceHeadline6, R.attr.textAppearanceOverline, R.attr.textAppearanceSubtitle1, R.attr.textAppearanceSubtitle2, R.attr.textInputStyle};
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_bottomSheetDialogTheme; // = 0;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_bottomSheetStyle; // = 1;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_chipGroupStyle; // = 2;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_chipStandaloneStyle; // = 3;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_chipStyle; // = 4;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_colorAccent; // = 5;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_colorBackgroundFloating; // = 6;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_colorPrimary; // = 7;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_colorPrimaryDark; // = 8;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_colorSecondary; // = 9;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_editTextStyle; // = 10;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_floatingActionButtonStyle; // = 11;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_materialButtonStyle; // = 12;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_materialCardViewStyle; // = 13;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_navigationViewStyle; // = 14;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_scrimBackground; // = 15;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_snackbarButtonStyle; // = 16;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_tabStyle; // = 17;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceBody1; // = 18;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceBody2; // = 19;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceButton; // = 20;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceCaption; // = 21;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline1; // = 22;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline2; // = 23;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline3; // = 24;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline4; // = 25;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline5; // = 26;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceHeadline6; // = 27;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceOverline; // = 28;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceSubtitle1; // = 29;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textAppearanceSubtitle2; // = 30;
    @DexIgnore
    public static /* final */ int MaterialComponentsTheme_textInputStyle; // = 31;
    @DexIgnore
    public static /* final */ int[] MenuGroup; // = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    @DexIgnore
    public static /* final */ int MenuGroup_android_checkableBehavior; // = 5;
    @DexIgnore
    public static /* final */ int MenuGroup_android_enabled; // = 0;
    @DexIgnore
    public static /* final */ int MenuGroup_android_id; // = 1;
    @DexIgnore
    public static /* final */ int MenuGroup_android_menuCategory; // = 3;
    @DexIgnore
    public static /* final */ int MenuGroup_android_orderInCategory; // = 4;
    @DexIgnore
    public static /* final */ int MenuGroup_android_visible; // = 2;
    @DexIgnore
    public static /* final */ int[] MenuItem; // = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, R.attr.actionLayout, R.attr.actionProviderClass, R.attr.actionViewClass, R.attr.alphabeticModifiers, R.attr.contentDescription, R.attr.iconTint, R.attr.iconTintMode, R.attr.numericModifiers, R.attr.showAsAction, R.attr.tooltipText};
    @DexIgnore
    public static /* final */ int MenuItem_actionLayout; // = 13;
    @DexIgnore
    public static /* final */ int MenuItem_actionProviderClass; // = 14;
    @DexIgnore
    public static /* final */ int MenuItem_actionViewClass; // = 15;
    @DexIgnore
    public static /* final */ int MenuItem_alphabeticModifiers; // = 16;
    @DexIgnore
    public static /* final */ int MenuItem_android_alphabeticShortcut; // = 9;
    @DexIgnore
    public static /* final */ int MenuItem_android_checkable; // = 11;
    @DexIgnore
    public static /* final */ int MenuItem_android_checked; // = 3;
    @DexIgnore
    public static /* final */ int MenuItem_android_enabled; // = 1;
    @DexIgnore
    public static /* final */ int MenuItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int MenuItem_android_id; // = 2;
    @DexIgnore
    public static /* final */ int MenuItem_android_menuCategory; // = 5;
    @DexIgnore
    public static /* final */ int MenuItem_android_numericShortcut; // = 10;
    @DexIgnore
    public static /* final */ int MenuItem_android_onClick; // = 12;
    @DexIgnore
    public static /* final */ int MenuItem_android_orderInCategory; // = 6;
    @DexIgnore
    public static /* final */ int MenuItem_android_title; // = 7;
    @DexIgnore
    public static /* final */ int MenuItem_android_titleCondensed; // = 8;
    @DexIgnore
    public static /* final */ int MenuItem_android_visible; // = 4;
    @DexIgnore
    public static /* final */ int MenuItem_contentDescription; // = 17;
    @DexIgnore
    public static /* final */ int MenuItem_iconTint; // = 18;
    @DexIgnore
    public static /* final */ int MenuItem_iconTintMode; // = 19;
    @DexIgnore
    public static /* final */ int MenuItem_numericModifiers; // = 20;
    @DexIgnore
    public static /* final */ int MenuItem_showAsAction; // = 21;
    @DexIgnore
    public static /* final */ int MenuItem_tooltipText; // = 22;
    @DexIgnore
    public static /* final */ int[] MenuView; // = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, R.attr.preserveIconSpacing, R.attr.subMenuArrow};
    @DexIgnore
    public static /* final */ int MenuView_android_headerBackground; // = 4;
    @DexIgnore
    public static /* final */ int MenuView_android_horizontalDivider; // = 2;
    @DexIgnore
    public static /* final */ int MenuView_android_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int MenuView_android_itemIconDisabledAlpha; // = 6;
    @DexIgnore
    public static /* final */ int MenuView_android_itemTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int MenuView_android_verticalDivider; // = 3;
    @DexIgnore
    public static /* final */ int MenuView_android_windowAnimationStyle; // = 0;
    @DexIgnore
    public static /* final */ int MenuView_preserveIconSpacing; // = 7;
    @DexIgnore
    public static /* final */ int MenuView_subMenuArrow; // = 8;
    @DexIgnore
    public static /* final */ int[] MessageBlackbgWidget; // = {R.attr.message_bb_image, R.attr.message_bb_sub_title, R.attr.message_bb_subtitle_text_style, R.attr.message_bb_text_font_color, R.attr.message_bb_title};
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_image; // = 0;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_sub_title; // = 1;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_subtitle_text_style; // = 2;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_text_font_color; // = 3;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_title; // = 4;
    @DexIgnore
    public static /* final */ int[] MockFitnessField; // = {R.attr.mock_fitness_field_label};
    @DexIgnore
    public static /* final */ int MockFitnessField_mock_fitness_field_label; // = 0;
    @DexIgnore
    public static /* final */ int[] MyLetterListView; // = {R.attr.fontPaths, R.attr.textColors, R.attr.textSizes};
    @DexIgnore
    public static /* final */ int MyLetterListView_fontPaths; // = 0;
    @DexIgnore
    public static /* final */ int MyLetterListView_textColors; // = 1;
    @DexIgnore
    public static /* final */ int MyLetterListView_textSizes; // = 2;
    @DexIgnore
    public static /* final */ int[] NavigationView; // = {16842964, 16842973, 16843039, R.attr.elevation, R.attr.headerLayout, R.attr.itemBackground, R.attr.itemHorizontalPadding, R.attr.itemIconPadding, R.attr.itemIconTint, R.attr.itemTextAppearance, R.attr.itemTextColor, R.attr.menu};
    @DexIgnore
    public static /* final */ int NavigationView_android_background; // = 0;
    @DexIgnore
    public static /* final */ int NavigationView_android_fitsSystemWindows; // = 1;
    @DexIgnore
    public static /* final */ int NavigationView_android_maxWidth; // = 2;
    @DexIgnore
    public static /* final */ int NavigationView_elevation; // = 3;
    @DexIgnore
    public static /* final */ int NavigationView_headerLayout; // = 4;
    @DexIgnore
    public static /* final */ int NavigationView_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int NavigationView_itemHorizontalPadding; // = 6;
    @DexIgnore
    public static /* final */ int NavigationView_itemIconPadding; // = 7;
    @DexIgnore
    public static /* final */ int NavigationView_itemIconTint; // = 8;
    @DexIgnore
    public static /* final */ int NavigationView_itemTextAppearance; // = 9;
    @DexIgnore
    public static /* final */ int NavigationView_itemTextColor; // = 10;
    @DexIgnore
    public static /* final */ int NavigationView_menu; // = 11;
    @DexIgnore
    public static /* final */ int[] NotificationConfigurationSummaryView; // = {R.attr.circleColor, R.attr.numeralColor, R.attr.numeralFontName, R.attr.numeralSize};
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_circleColor; // = 0;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralColor; // = 1;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralFontName; // = 2;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralSize; // = 3;
    @DexIgnore
    public static /* final */ int[] NotificationSummaryDialView; // = {R.attr.NSDV_ClockImageSrc, R.attr.NSDV_DialItemBackground, R.attr.NSDV_DialItemPadding, R.attr.NSDV_DialItemSrc, R.attr.NSDV_DialItemTintColor};
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_ClockImageSrc; // = 0;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemBackground; // = 1;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemPadding; // = 2;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemSrc; // = 3;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemTintColor; // = 4;
    @DexIgnore
    public static /* final */ int[] NumberPicker; // = {R.attr.internalLayout, R.attr.internalMaxHeight, R.attr.internalMaxWidth, R.attr.internalMinHeight, R.attr.internalMinWidth, R.attr.np_fontFamily, R.attr.number_picker_font, R.attr.selectedColor, R.attr.selectedTextColor, R.attr.selectionDivider, R.attr.selectionDividerHeight, R.attr.selectionDividersDistance, R.attr.solidColor, R.attr.unselectedColor, R.attr.virtualButtonPressedDrawable};
    @DexIgnore
    public static /* final */ int NumberPicker_internalLayout; // = 0;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMaxHeight; // = 1;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMaxWidth; // = 2;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMinHeight; // = 3;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMinWidth; // = 4;
    @DexIgnore
    public static /* final */ int NumberPicker_np_fontFamily; // = 5;
    @DexIgnore
    public static /* final */ int NumberPicker_number_picker_font; // = 6;
    @DexIgnore
    public static /* final */ int NumberPicker_selectedColor; // = 7;
    @DexIgnore
    public static /* final */ int NumberPicker_selectedTextColor; // = 8;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDivider; // = 9;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDividerHeight; // = 10;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDividersDistance; // = 11;
    @DexIgnore
    public static /* final */ int NumberPicker_solidColor; // = 12;
    @DexIgnore
    public static /* final */ int NumberPicker_unselectedColor; // = 13;
    @DexIgnore
    public static /* final */ int NumberPicker_virtualButtonPressedDrawable; // = 14;
    @DexIgnore
    public static /* final */ int[] OverviewSleepDayChart; // = {R.attr.awake_bar_height_percent, R.attr.deep_bar_height_percent, R.attr.light_bar_height_percent};
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_awake_bar_height_percent; // = 0;
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_deep_bar_height_percent; // = 1;
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_light_bar_height_percent; // = 2;
    @DexIgnore
    public static /* final */ int[] OverviewSleepDaySummary; // = {R.attr.awake_color, R.attr.bar_margin, R.attr.bar_radius, R.attr.deep_color, R.attr.light_color};
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_bar_margin; // = 1;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_bar_radius; // = 2;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_deep_color; // = 3;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_light_color; // = 4;
    @DexIgnore
    public static /* final */ int[] PaddingView; // = {R.attr.heightByPercentageWithScreen, R.attr.widthByPercentageWithScreen};
    @DexIgnore
    public static /* final */ int PaddingView_heightByPercentageWithScreen; // = 0;
    @DexIgnore
    public static /* final */ int PaddingView_widthByPercentageWithScreen; // = 1;
    @DexIgnore
    public static /* final */ int[] PercentLayout_Layout; // = {R.attr.layout_aspectRatio, R.attr.layout_heightPercent, R.attr.layout_marginBottomPercent, R.attr.layout_marginEndPercent, R.attr.layout_marginLeftPercent, R.attr.layout_marginPercent, R.attr.layout_marginRightPercent, R.attr.layout_marginStartPercent, R.attr.layout_marginTopPercent, R.attr.layout_widthPercent};
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_aspectRatio; // = 0;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_heightPercent; // = 1;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginBottomPercent; // = 2;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginEndPercent; // = 3;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginLeftPercent; // = 4;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginPercent; // = 5;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginRightPercent; // = 6;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginStartPercent; // = 7;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginTopPercent; // = 8;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_widthPercent; // = 9;
    @DexIgnore
    public static /* final */ int[] PopupWindow; // = {16843126, 16843465, R.attr.overlapAnchor};
    @DexIgnore
    public static /* final */ int[] PopupWindowBackgroundState; // = {R.attr.state_above_anchor};
    @DexIgnore
    public static /* final */ int PopupWindowBackgroundState_state_above_anchor; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupBackground; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_overlapAnchor; // = 2;
    @DexIgnore
    public static /* final */ int[] ProfileUnit; // = {R.attr.is_selected};
    @DexIgnore
    public static /* final */ int ProfileUnit_is_selected; // = 0;
    @DexIgnore
    public static /* final */ int[] ProgressButton; // = {R.attr.autoDisableClickable, R.attr.loading, R.attr.loadingDrawable, R.attr.loadingText, R.attr.selectedText, R.attr.unselectedText};
    @DexIgnore
    public static /* final */ int ProgressButton_autoDisableClickable; // = 0;
    @DexIgnore
    public static /* final */ int ProgressButton_loading; // = 1;
    @DexIgnore
    public static /* final */ int ProgressButton_loadingDrawable; // = 2;
    @DexIgnore
    public static /* final */ int ProgressButton_loadingText; // = 3;
    @DexIgnore
    public static /* final */ int ProgressButton_selectedText; // = 4;
    @DexIgnore
    public static /* final */ int ProgressButton_unselectedText; // = 5;
    @DexIgnore
    public static /* final */ int[] ProgressImageView; // = {R.attr.done_color, R.attr.normal_color, R.attr.progress_width};
    @DexIgnore
    public static /* final */ int ProgressImageView_done_color; // = 0;
    @DexIgnore
    public static /* final */ int ProgressImageView_normal_color; // = 1;
    @DexIgnore
    public static /* final */ int ProgressImageView_progress_width; // = 2;
    @DexIgnore
    public static /* final */ int[] ProximityCircleView; // = {R.attr.strokeColorInRange, R.attr.strokeColorOutOfRange, R.attr.strokeSize, R.attr.type};
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeColorInRange; // = 0;
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeColorOutOfRange; // = 1;
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeSize; // = 2;
    @DexIgnore
    public static /* final */ int ProximityCircleView_type; // = 3;
    @DexIgnore
    public static /* final */ int[] QBaseBarChart; // = {R.attr.QegBarMargin, R.attr.QegBarTextSize, R.attr.QegBarWidth, R.attr.QegBottomFont, R.attr.QegFixedBarWidth, R.attr.QegFont, R.attr.QegLineBottomText, R.attr.QegLineBottomTextColor, R.attr.QegLineBottomTextSize, R.attr.QegLineColor, R.attr.QegLineEnable, R.attr.QegLineMargin, R.attr.QegLineStrokeWidth, R.attr.QegLineTextColor, R.attr.QegLineTextSize, R.attr.QegLineTopText, R.attr.QegSelectedColor};
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarMargin; // = 0;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarTextSize; // = 1;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarWidth; // = 2;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBottomFont; // = 3;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegFixedBarWidth; // = 4;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegFont; // = 5;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomText; // = 6;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomTextColor; // = 7;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomTextSize; // = 8;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineColor; // = 9;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineEnable; // = 10;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineMargin; // = 11;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineStrokeWidth; // = 12;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTextColor; // = 13;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTextSize; // = 14;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTopText; // = 15;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegSelectedColor; // = 16;
    @DexIgnore
    public static /* final */ int[] QBaseChart; // = {R.attr.QegLegendColor, R.attr.QegLegendEnable, R.attr.QegLegendHeight, R.attr.QegLegendLetterSpacing, R.attr.QegLegendTextSize};
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendColor; // = 0;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendEnable; // = 1;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendHeight; // = 2;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendLetterSpacing; // = 3;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendTextSize; // = 4;
    @DexIgnore
    public static /* final */ int[] RecycleListView; // = {R.attr.paddingBottomNoButtons, R.attr.paddingTopNoTitle};
    @DexIgnore
    public static /* final */ int RecycleListView_paddingBottomNoButtons; // = 0;
    @DexIgnore
    public static /* final */ int RecycleListView_paddingTopNoTitle; // = 1;
    @DexIgnore
    public static /* final */ int[] RecyclerView; // = {16842948, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
    @DexIgnore
    public static /* final */ int[] RecyclerViewAlphabetIndex; // = {R.attr.rvaiCustomizable, R.attr.rvaiFontSize, R.attr.rvaiItemColor, R.attr.rvaiStyle, R.attr.rvaiWidth};
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiCustomizable; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiFontSize; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiItemColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiStyle; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiWidth; // = 4;
    @DexIgnore
    public static /* final */ int[] RecyclerViewCalendar; // = {R.attr.cv_completedTextColor, R.attr.cv_incompleteTextColor, R.attr.cv_noValueTextColor, R.attr.cv_progressColor, R.attr.cv_selectedTextColor};
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_completedTextColor; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_incompleteTextColor; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_noValueTextColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_progressColor; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_selectedTextColor; // = 4;
    @DexIgnore
    public static /* final */ int[] RecyclerViewHeartRateCalendar; // = {R.attr.rvhrdc_circleEndColor, R.attr.rvhrdc_circleOutlineColor, R.attr.rvhrdc_circleStartColor, R.attr.rvhrdc_hasValueTextColor, R.attr.rvhrdc_noValueTextColor, R.attr.rvhrdc_selectedTextColor};
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_circleEndColor; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_circleOutlineColor; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_circleStartColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_hasValueTextColor; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_noValueTextColor; // = 4;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_selectedTextColor; // = 5;
    @DexIgnore
    public static /* final */ int RecyclerView_android_descendantFocusability; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerView_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollEnabled; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollHorizontalThumbDrawable; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollHorizontalTrackDrawable; // = 4;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollVerticalThumbDrawable; // = 5;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollVerticalTrackDrawable; // = 6;
    @DexIgnore
    public static /* final */ int RecyclerView_layoutManager; // = 7;
    @DexIgnore
    public static /* final */ int RecyclerView_reverseLayout; // = 8;
    @DexIgnore
    public static /* final */ int RecyclerView_spanCount; // = 9;
    @DexIgnore
    public static /* final */ int RecyclerView_stackFromEnd; // = 10;
    @DexIgnore
    public static /* final */ int[] RingChart; // = {R.attr.color, R.attr.goal, R.attr.width};
    @DexIgnore
    public static /* final */ int RingChart_color; // = 0;
    @DexIgnore
    public static /* final */ int RingChart_goal; // = 1;
    @DexIgnore
    public static /* final */ int RingChart_width; // = 2;
    @DexIgnore
    public static /* final */ int[] RingProgressBar; // = {R.attr.icon_background, R.attr.icon_size, R.attr.icon_source, R.attr.max_progress, R.attr.progress, R.attr.ring_progress_background_color, R.attr.ring_progress_color, R.attr.ring_progress_index, R.attr.space_width, R.attr.stroke_width};
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_background; // = 0;
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_size; // = 1;
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_source; // = 2;
    @DexIgnore
    public static /* final */ int RingProgressBar_max_progress; // = 3;
    @DexIgnore
    public static /* final */ int RingProgressBar_progress; // = 4;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_background_color; // = 5;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_color; // = 6;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_index; // = 7;
    @DexIgnore
    public static /* final */ int RingProgressBar_space_width; // = 8;
    @DexIgnore
    public static /* final */ int RingProgressBar_stroke_width; // = 9;
    @DexIgnore
    public static /* final */ int[] RoundRectangleSeekBar; // = {R.attr.recommended_line_color, R.attr.recommended_text_color, R.attr.recommended_text_size, R.attr.recommended_text_typeface, R.attr.thumb_text_background_color, R.attr.thumb_text_color, R.attr.thumb_text_size, R.attr.thumb_text_typeface, R.attr.unit};
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_line_color; // = 0;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_color; // = 1;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_size; // = 2;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_typeface; // = 3;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_background_color; // = 4;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_color; // = 5;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_size; // = 6;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_typeface; // = 7;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_unit; // = 8;
    @DexIgnore
    public static /* final */ int[] RulerValuePicker; // = {R.attr.indicator_color, R.attr.indicator_interval, R.attr.indicator_width, R.attr.long_height_height_ratio, R.attr.max_value, R.attr.min_value, R.attr.notch_color, R.attr.ruler_fontFamily, R.attr.ruler_selected_text_size, R.attr.ruler_text_color, R.attr.ruler_text_size, R.attr.ruler_text_style, R.attr.short_height_height_ratio};
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_color; // = 0;
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_interval; // = 1;
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_width; // = 2;
    @DexIgnore
    public static /* final */ int RulerValuePicker_long_height_height_ratio; // = 3;
    @DexIgnore
    public static /* final */ int RulerValuePicker_max_value; // = 4;
    @DexIgnore
    public static /* final */ int RulerValuePicker_min_value; // = 5;
    @DexIgnore
    public static /* final */ int RulerValuePicker_notch_color; // = 6;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_fontFamily; // = 7;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_selected_text_size; // = 8;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_color; // = 9;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_size; // = 10;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_style; // = 11;
    @DexIgnore
    public static /* final */ int RulerValuePicker_short_height_height_ratio; // = 12;
    @DexIgnore
    public static /* final */ int[] RulerView; // = {R.attr.indicator_color, R.attr.indicator_interval, R.attr.indicator_width, R.attr.long_height_height_ratio, R.attr.max_value, R.attr.min_value, R.attr.ruler_text_color, R.attr.ruler_text_size, R.attr.short_height_height_ratio};
    @DexIgnore
    public static /* final */ int RulerView_indicator_color; // = 0;
    @DexIgnore
    public static /* final */ int RulerView_indicator_interval; // = 1;
    @DexIgnore
    public static /* final */ int RulerView_indicator_width; // = 2;
    @DexIgnore
    public static /* final */ int RulerView_long_height_height_ratio; // = 3;
    @DexIgnore
    public static /* final */ int RulerView_max_value; // = 4;
    @DexIgnore
    public static /* final */ int RulerView_min_value; // = 5;
    @DexIgnore
    public static /* final */ int RulerView_ruler_text_color; // = 6;
    @DexIgnore
    public static /* final */ int RulerView_ruler_text_size; // = 7;
    @DexIgnore
    public static /* final */ int RulerView_short_height_height_ratio; // = 8;
    @DexIgnore
    public static /* final */ int[] ScrimInsetsFrameLayout; // = {R.attr.insetForeground};
    @DexIgnore
    public static /* final */ int ScrimInsetsFrameLayout_insetForeground; // = 0;
    @DexIgnore
    public static /* final */ int[] ScrollingViewBehavior_Layout; // = {R.attr.behavior_overlapTop};
    @DexIgnore
    public static /* final */ int ScrollingViewBehavior_Layout_behavior_overlapTop; // = 0;
    @DexIgnore
    public static /* final */ int[] SearchView; // = {16842970, 16843039, 16843296, 16843364, R.attr.closeIcon, R.attr.commitIcon, R.attr.defaultQueryHint, R.attr.goIcon, R.attr.iconifiedByDefault, R.attr.layout, R.attr.queryBackground, R.attr.queryHint, R.attr.searchHintIcon, R.attr.searchIcon, R.attr.submitBackground, R.attr.suggestionRowLayout, R.attr.voiceIcon};
    @DexIgnore
    public static /* final */ int SearchView_android_focusable; // = 0;
    @DexIgnore
    public static /* final */ int SearchView_android_imeOptions; // = 3;
    @DexIgnore
    public static /* final */ int SearchView_android_inputType; // = 2;
    @DexIgnore
    public static /* final */ int SearchView_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int SearchView_closeIcon; // = 4;
    @DexIgnore
    public static /* final */ int SearchView_commitIcon; // = 5;
    @DexIgnore
    public static /* final */ int SearchView_defaultQueryHint; // = 6;
    @DexIgnore
    public static /* final */ int SearchView_goIcon; // = 7;
    @DexIgnore
    public static /* final */ int SearchView_iconifiedByDefault; // = 8;
    @DexIgnore
    public static /* final */ int SearchView_layout; // = 9;
    @DexIgnore
    public static /* final */ int SearchView_queryBackground; // = 10;
    @DexIgnore
    public static /* final */ int SearchView_queryHint; // = 11;
    @DexIgnore
    public static /* final */ int SearchView_searchHintIcon; // = 12;
    @DexIgnore
    public static /* final */ int SearchView_searchIcon; // = 13;
    @DexIgnore
    public static /* final */ int SearchView_submitBackground; // = 14;
    @DexIgnore
    public static /* final */ int SearchView_suggestionRowLayout; // = 15;
    @DexIgnore
    public static /* final */ int SearchView_voiceIcon; // = 16;
    @DexIgnore
    public static /* final */ int[] ShimmerView; // = {R.attr.reflectionColor};
    @DexIgnore
    public static /* final */ int ShimmerView_reflectionColor; // = 0;
    @DexIgnore
    public static /* final */ int[] SignInButton; // = {R.attr.buttonSize, R.attr.colorScheme, R.attr.scopeUris};
    @DexIgnore
    public static /* final */ int SignInButton_buttonSize; // = 0;
    @DexIgnore
    public static /* final */ int SignInButton_colorScheme; // = 1;
    @DexIgnore
    public static /* final */ int SignInButton_scopeUris; // = 2;
    @DexIgnore
    public static /* final */ int[] SleepDayDetailsChart; // = {R.attr.sleep_day_awake_color, R.attr.sleep_day_dash_line_color, R.attr.sleep_day_light_color, R.attr.sleep_day_line_color, R.attr.sleep_day_restful_color, R.attr.sleep_day_text_color, R.attr.sleep_day_text_font};
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_dash_line_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_light_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_restful_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_text_font; // = 6;
    @DexIgnore
    public static /* final */ int[] SleepHorizontalBar; // = {R.attr.sleep_background_color, R.attr.sleep_fontFamily, R.attr.sleep_progress_awake_color, R.attr.sleep_progress_color, R.attr.sleep_progress_deep_color, R.attr.sleep_progress_margin_end, R.attr.sleep_progress_radius, R.attr.sleep_progress_sleep_color, R.attr.sleep_progress_space, R.attr.sleep_progress_width, R.attr.sleep_star_alpha, R.attr.sleep_star_res, R.attr.sleep_star_size, R.attr.sleep_text, R.attr.sleep_text_color, R.attr.sleep_text_size};
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_background_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_fontFamily; // = 1;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_awake_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_deep_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_margin_end; // = 5;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_radius; // = 6;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_sleep_color; // = 7;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_space; // = 8;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_width; // = 9;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_alpha; // = 10;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_res; // = 11;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_size; // = 12;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text; // = 13;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text_color; // = 14;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text_size; // = 15;
    @DexIgnore
    public static /* final */ int[] SleepMonthDetailsChart; // = {R.attr.sleep_month_bar_awake_color, R.attr.sleep_month_bar_light_color, R.attr.sleep_month_bar_restful_color, R.attr.sleep_month_dash_line_color, R.attr.sleep_month_line_color, R.attr.sleep_month_text_color, R.attr.sleep_month_text_font, R.attr.sleep_month_text_size};
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_light_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_restful_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_dash_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_line_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_font; // = 6;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_size; // = 7;
    @DexIgnore
    public static /* final */ int[] SleepQualityChart; // = {R.attr.sleep_color_end, R.attr.sleep_color_start, R.attr.sleep_percent, R.attr.sleep_percent_image, R.attr.sleep_width};
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_color_end; // = 0;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_color_start; // = 1;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_percent; // = 2;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_percent_image; // = 3;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_width; // = 4;
    @DexIgnore
    public static /* final */ int[] SleepWeekDetailsChart; // = {R.attr.sleep_week_awake_color, R.attr.sleep_week_dash_line_color, R.attr.sleep_week_light_color, R.attr.sleep_week_line_color, R.attr.sleep_week_restful_color, R.attr.sleep_week_text_color, R.attr.sleep_week_text_font, R.attr.sleep_week_text_size};
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_dash_line_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_light_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_restful_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_font; // = 6;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_size; // = 7;
    @DexIgnore
    public static /* final */ int[] Snackbar; // = {R.attr.snackbarButtonStyle, R.attr.snackbarStyle};
    @DexIgnore
    public static /* final */ int[] SnackbarLayout; // = {16843039, R.attr.elevation, R.attr.maxActionInlineWidth};
    @DexIgnore
    public static /* final */ int SnackbarLayout_android_maxWidth; // = 0;
    @DexIgnore
    public static /* final */ int SnackbarLayout_elevation; // = 1;
    @DexIgnore
    public static /* final */ int SnackbarLayout_maxActionInlineWidth; // = 2;
    @DexIgnore
    public static /* final */ int Snackbar_snackbarButtonStyle; // = 0;
    @DexIgnore
    public static /* final */ int Snackbar_snackbarStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] SnappyHorizontalRecyclerView; // = {R.attr.numOfDisplayingItem};
    @DexIgnore
    public static /* final */ int SnappyHorizontalRecyclerView_numOfDisplayingItem; // = 0;
    @DexIgnore
    public static /* final */ int[] Spinner; // = {16842930, 16843126, 16843131, 16843362, R.attr.popupTheme};
    @DexIgnore
    public static /* final */ int Spinner_android_dropDownWidth; // = 3;
    @DexIgnore
    public static /* final */ int Spinner_android_entries; // = 0;
    @DexIgnore
    public static /* final */ int Spinner_android_popupBackground; // = 1;
    @DexIgnore
    public static /* final */ int Spinner_android_prompt; // = 2;
    @DexIgnore
    public static /* final */ int Spinner_popupTheme; // = 4;
    @DexIgnore
    public static /* final */ int[] StateListDrawable; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int[] StateListDrawableItem; // = {16843161};
    @DexIgnore
    public static /* final */ int StateListDrawableItem_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] SwitchCompat; // = {16843044, 16843045, 16843074, R.attr.showText, R.attr.splitTrack, R.attr.switchMinWidth, R.attr.switchPadding, R.attr.switchTextAppearance, R.attr.thumbTextPadding, R.attr.thumbTint, R.attr.thumbTintMode, R.attr.track, R.attr.trackTint, R.attr.trackTintMode};
    @DexIgnore
    public static /* final */ int[] SwitchCompatCustom; // = {R.attr.switch_background, R.attr.switch_font, R.attr.switch_isChecked, R.attr.switch_textSize, R.attr.switch_title};
    @DexIgnore
    public static /* final */ int SwitchCompatCustom_switch_background; // = 0;
    @DexIgnore
    public static /* final */ int SwitchCompatCustom_switch_font; // = 1;
    @DexIgnore
    public static /* final */ int SwitchCompatCustom_switch_isChecked; // = 2;
    @DexIgnore
    public static /* final */ int SwitchCompatCustom_switch_textSize; // = 3;
    @DexIgnore
    public static /* final */ int SwitchCompatCustom_switch_title; // = 4;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOff; // = 1;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOn; // = 0;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_thumb; // = 2;
    @DexIgnore
    public static /* final */ int SwitchCompat_showText; // = 3;
    @DexIgnore
    public static /* final */ int SwitchCompat_splitTrack; // = 4;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchMinWidth; // = 5;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchPadding; // = 6;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchTextAppearance; // = 7;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTextPadding; // = 8;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTint; // = 9;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTintMode; // = 10;
    @DexIgnore
    public static /* final */ int SwitchCompat_track; // = 11;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTint; // = 12;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTintMode; // = 13;
    @DexIgnore
    public static /* final */ int[] TabItem; // = {16842754, 16842994, 16843087};
    @DexIgnore
    public static /* final */ int TabItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int TabItem_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int TabItem_android_text; // = 2;
    @DexIgnore
    public static /* final */ int[] TabLayout; // = {R.attr.tabBackground, R.attr.tabContentStart, R.attr.tabGravity, R.attr.tabIconTint, R.attr.tabIconTintMode, R.attr.tabIndicator, R.attr.tabIndicatorAnimationDuration, R.attr.tabIndicatorColor, R.attr.tabIndicatorFullWidth, R.attr.tabIndicatorGravity, R.attr.tabIndicatorHeight, R.attr.tabInlineLabel, R.attr.tabMaxWidth, R.attr.tabMinWidth, R.attr.tabMode, R.attr.tabPadding, R.attr.tabPaddingBottom, R.attr.tabPaddingEnd, R.attr.tabPaddingStart, R.attr.tabPaddingTop, R.attr.tabRippleColor, R.attr.tabSelectedTextColor, R.attr.tabTextAppearance, R.attr.tabTextColor, R.attr.tabUnboundedRipple};
    @DexIgnore
    public static /* final */ int TabLayout_tabBackground; // = 0;
    @DexIgnore
    public static /* final */ int TabLayout_tabContentStart; // = 1;
    @DexIgnore
    public static /* final */ int TabLayout_tabGravity; // = 2;
    @DexIgnore
    public static /* final */ int TabLayout_tabIconTint; // = 3;
    @DexIgnore
    public static /* final */ int TabLayout_tabIconTintMode; // = 4;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicator; // = 5;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorAnimationDuration; // = 6;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorColor; // = 7;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorFullWidth; // = 8;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorGravity; // = 9;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorHeight; // = 10;
    @DexIgnore
    public static /* final */ int TabLayout_tabInlineLabel; // = 11;
    @DexIgnore
    public static /* final */ int TabLayout_tabMaxWidth; // = 12;
    @DexIgnore
    public static /* final */ int TabLayout_tabMinWidth; // = 13;
    @DexIgnore
    public static /* final */ int TabLayout_tabMode; // = 14;
    @DexIgnore
    public static /* final */ int TabLayout_tabPadding; // = 15;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingBottom; // = 16;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingEnd; // = 17;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingStart; // = 18;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingTop; // = 19;
    @DexIgnore
    public static /* final */ int TabLayout_tabRippleColor; // = 20;
    @DexIgnore
    public static /* final */ int TabLayout_tabSelectedTextColor; // = 21;
    @DexIgnore
    public static /* final */ int TabLayout_tabTextAppearance; // = 22;
    @DexIgnore
    public static /* final */ int TabLayout_tabTextColor; // = 23;
    @DexIgnore
    public static /* final */ int TabLayout_tabUnboundedRipple; // = 24;
    @DexIgnore
    public static /* final */ int[] TextAppearance; // = {16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, R.attr.fontFamily, R.attr.textAllCaps};
    @DexIgnore
    public static /* final */ int TextAppearance_android_fontFamily; // = 10;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowColor; // = 6;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDx; // = 7;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDy; // = 8;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowRadius; // = 9;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColor; // = 3;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorHint; // = 4;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorLink; // = 5;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textSize; // = 0;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textStyle; // = 2;
    @DexIgnore
    public static /* final */ int TextAppearance_android_typeface; // = 1;
    @DexIgnore
    public static /* final */ int TextAppearance_fontFamily; // = 11;
    @DexIgnore
    public static /* final */ int TextAppearance_textAllCaps; // = 12;
    @DexIgnore
    public static /* final */ int[] TextInputLayout; // = {16842906, 16843088, R.attr.boxBackgroundColor, R.attr.boxBackgroundMode, R.attr.boxCollapsedPaddingTop, R.attr.boxCornerRadiusBottomEnd, R.attr.boxCornerRadiusBottomStart, R.attr.boxCornerRadiusTopEnd, R.attr.boxCornerRadiusTopStart, R.attr.boxStrokeColor, R.attr.boxStrokeWidth, R.attr.counterEnabled, R.attr.counterMaxLength, R.attr.counterOverflowTextAppearance, R.attr.counterTextAppearance, R.attr.errorEnabled, R.attr.errorTextAppearance, R.attr.helperText, R.attr.helperTextEnabled, R.attr.helperTextTextAppearance, R.attr.hintAnimationEnabled, R.attr.hintEnabled, R.attr.hintTextAppearance, R.attr.passwordToggleContentDescription, R.attr.passwordToggleDrawable, R.attr.passwordToggleEnabled, R.attr.passwordToggleTint, R.attr.passwordToggleTintMode};
    @DexIgnore
    public static /* final */ int TextInputLayout_android_hint; // = 1;
    @DexIgnore
    public static /* final */ int TextInputLayout_android_textColorHint; // = 0;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxBackgroundMode; // = 3;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCollapsedPaddingTop; // = 4;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusBottomEnd; // = 5;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusBottomStart; // = 6;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusTopEnd; // = 7;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusTopStart; // = 8;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxStrokeColor; // = 9;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxStrokeWidth; // = 10;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterEnabled; // = 11;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterMaxLength; // = 12;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterOverflowTextAppearance; // = 13;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterTextAppearance; // = 14;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorEnabled; // = 15;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorTextAppearance; // = 16;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperText; // = 17;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperTextEnabled; // = 18;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperTextTextAppearance; // = 19;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintAnimationEnabled; // = 20;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintEnabled; // = 21;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintTextAppearance; // = 22;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleContentDescription; // = 23;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleDrawable; // = 24;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleEnabled; // = 25;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleTint; // = 26;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleTintMode; // = 27;
    @DexIgnore
    public static /* final */ int[] ThemeEnforcement; // = {16842804, R.attr.enforceMaterialTheme, R.attr.enforceTextAppearance};
    @DexIgnore
    public static /* final */ int ThemeEnforcement_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int ThemeEnforcement_enforceMaterialTheme; // = 1;
    @DexIgnore
    public static /* final */ int ThemeEnforcement_enforceTextAppearance; // = 2;
    @DexIgnore
    public static /* final */ int[] TitleMessageWidget; // = {R.attr.titlemessage_image, R.attr.titlemessage_image_max_height, R.attr.titlemessage_image_max_width, R.attr.titlemessage_image_padding, R.attr.titlemessage_message, R.attr.titlemessage_title};
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image; // = 0;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_max_height; // = 1;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_max_width; // = 2;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_padding; // = 3;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_message; // = 4;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_title; // = 5;
    @DexIgnore
    public static /* final */ int[] TitlePunchCircleWidget; // = {R.attr.circle_text, R.attr.circle_text_color};
    @DexIgnore
    public static /* final */ int TitlePunchCircleWidget_circle_text; // = 0;
    @DexIgnore
    public static /* final */ int TitlePunchCircleWidget_circle_text_color; // = 1;
    @DexIgnore
    public static /* final */ int[] TodayHeartRateChart; // = {R.attr.THR_AxesColor, R.attr.THR_HeartRateFontSize, R.attr.THR_HeartRateTextFont, R.attr.THR_LineEndColor, R.attr.THR_LineStartColor, R.attr.THR_TextColor, R.attr.THR_TimeFontSize, R.attr.THR_TimeTextFont};
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_AxesColor; // = 0;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_HeartRateFontSize; // = 1;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_HeartRateTextFont; // = 2;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_LineEndColor; // = 3;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_LineStartColor; // = 4;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TextColor; // = 5;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TimeFontSize; // = 6;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TimeTextFont; // = 7;
    @DexIgnore
    public static /* final */ int[] Toolbar; // = {16842927, 16843072, R.attr.buttonGravity, R.attr.collapseContentDescription, R.attr.collapseIcon, R.attr.contentInsetEnd, R.attr.contentInsetEndWithActions, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.contentInsetStart, R.attr.contentInsetStartWithNavigation, R.attr.logo, R.attr.logoDescription, R.attr.maxButtonHeight, R.attr.navigationContentDescription, R.attr.navigationIcon, R.attr.popupTheme, R.attr.subtitle, R.attr.subtitleTextAppearance, R.attr.subtitleTextColor, R.attr.title, R.attr.titleMargin, R.attr.titleMarginBottom, R.attr.titleMarginEnd, R.attr.titleMarginStart, R.attr.titleMarginTop, R.attr.titleMargins, R.attr.titleTextAppearance, R.attr.titleTextColor};
    @DexIgnore
    public static /* final */ int Toolbar_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int Toolbar_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int Toolbar_buttonGravity; // = 2;
    @DexIgnore
    public static /* final */ int Toolbar_collapseContentDescription; // = 3;
    @DexIgnore
    public static /* final */ int Toolbar_collapseIcon; // = 4;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEnd; // = 5;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEndWithActions; // = 6;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetLeft; // = 7;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetRight; // = 8;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStart; // = 9;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStartWithNavigation; // = 10;
    @DexIgnore
    public static /* final */ int Toolbar_logo; // = 11;
    @DexIgnore
    public static /* final */ int Toolbar_logoDescription; // = 12;
    @DexIgnore
    public static /* final */ int Toolbar_maxButtonHeight; // = 13;
    @DexIgnore
    public static /* final */ int Toolbar_navigationContentDescription; // = 14;
    @DexIgnore
    public static /* final */ int Toolbar_navigationIcon; // = 15;
    @DexIgnore
    public static /* final */ int Toolbar_popupTheme; // = 16;
    @DexIgnore
    public static /* final */ int Toolbar_subtitle; // = 17;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextAppearance; // = 18;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextColor; // = 19;
    @DexIgnore
    public static /* final */ int Toolbar_title; // = 20;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargin; // = 21;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginBottom; // = 22;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginEnd; // = 23;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginStart; // = 24;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginTop; // = 25;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargins; // = 26;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextAppearance; // = 27;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextColor; // = 28;
    @DexIgnore
    public static /* final */ int[] UnderLine; // = {R.attr.distance, R.attr.size};
    @DexIgnore
    public static /* final */ int UnderLine_distance; // = 0;
    @DexIgnore
    public static /* final */ int UnderLine_size; // = 1;
    @DexIgnore
    public static /* final */ int[] UnderlinedTextView; // = {R.attr.underlineColor, R.attr.underlineDistance, R.attr.underlineStrokeWidth, R.attr.underlineWidth};
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineColor; // = 0;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineDistance; // = 1;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineStrokeWidth; // = 2;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineWidth; // = 3;
    @DexIgnore
    public static /* final */ int[] View; // = {16842752, 16842970, R.attr.paddingEnd, R.attr.paddingStart, R.attr.theme};
    @DexIgnore
    public static /* final */ int[] ViewBackgroundHelper; // = {16842964, R.attr.backgroundTint, R.attr.backgroundTintMode};
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_android_background; // = 0;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTint; // = 1;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] ViewPagerIndicator; // = {R.attr.vpiCirclePageIndicatorStyle, R.attr.vpiIconPageIndicatorStyle, R.attr.vpiLinePageIndicatorStyle, R.attr.vpiTabPageIndicatorStyle, R.attr.vpiTitlePageIndicatorStyle, R.attr.vpiUnderlinePageIndicatorStyle};
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiCirclePageIndicatorStyle; // = 0;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiIconPageIndicatorStyle; // = 1;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiLinePageIndicatorStyle; // = 2;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiTabPageIndicatorStyle; // = 3;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiTitlePageIndicatorStyle; // = 4;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ViewStubCompat; // = {16842960, 16842994, 16842995};
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_id; // = 0;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_inflatedId; // = 2;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int View_android_focusable; // = 1;
    @DexIgnore
    public static /* final */ int View_android_theme; // = 0;
    @DexIgnore
    public static /* final */ int View_paddingEnd; // = 2;
    @DexIgnore
    public static /* final */ int View_paddingStart; // = 3;
    @DexIgnore
    public static /* final */ int View_theme; // = 4;
    @DexIgnore
    public static /* final */ int[] WLHeartRateChart; // = {R.attr.WLHR_AxesColor, R.attr.WLHR_BarColor, R.attr.WLHR_GridColor, R.attr.WLHR_HeartRateFontSize, R.attr.WLHR_HeartRateTextFont, R.attr.WLHR_LineColor, R.attr.WLHR_RoundRadius, R.attr.WLHR_TextColor, R.attr.WLHR_TimeFontSize, R.attr.WLHR_TimeTextFont, R.attr.WLHR_Type};
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_AxesColor; // = 0;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_BarColor; // = 1;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_GridColor; // = 2;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_HeartRateFontSize; // = 3;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_HeartRateTextFont; // = 4;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_LineColor; // = 5;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_RoundRadius; // = 6;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TextColor; // = 7;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TimeFontSize; // = 8;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TimeTextFont; // = 9;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_Type; // = 10;
    @DexIgnore
    public static /* final */ int[] WatchAppControl; // = {R.attr.watch_app_bottom_content, R.attr.watch_app_bottom_content_color, R.attr.watch_app_bottom_content_size, R.attr.watch_app_default_color, R.attr.watch_app_default_color_selected, R.attr.watch_app_end_content, R.attr.watch_app_end_content_color, R.attr.watch_app_end_content_size, R.attr.watch_app_icon_background, R.attr.watch_app_icon_background_selected, R.attr.watch_app_icon_src, R.attr.watch_app_remove_mode, R.attr.watch_app_selected};
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content; // = 0;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content_color; // = 1;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content_size; // = 2;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_default_color; // = 3;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_default_color_selected; // = 4;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content; // = 5;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content_color; // = 6;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content_size; // = 7;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_background; // = 8;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_background_selected; // = 9;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_src; // = 10;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_remove_mode; // = 11;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_selected; // = 12;
    @DexIgnore
    public static /* final */ int[] WaveView; // = {R.attr.wave_color, R.attr.wave_number_wave, R.attr.wave_period, R.attr.wave_period_change, R.attr.wave_period_delay, R.attr.wave_radius_end, R.attr.wave_radius_start, R.attr.wave_space_wave, R.attr.wave_stroke_width};
    @DexIgnore
    public static /* final */ int WaveView_wave_color; // = 0;
    @DexIgnore
    public static /* final */ int WaveView_wave_number_wave; // = 1;
    @DexIgnore
    public static /* final */ int WaveView_wave_period; // = 2;
    @DexIgnore
    public static /* final */ int WaveView_wave_period_change; // = 3;
    @DexIgnore
    public static /* final */ int WaveView_wave_period_delay; // = 4;
    @DexIgnore
    public static /* final */ int WaveView_wave_radius_end; // = 5;
    @DexIgnore
    public static /* final */ int WaveView_wave_radius_start; // = 6;
    @DexIgnore
    public static /* final */ int WaveView_wave_space_wave; // = 7;
    @DexIgnore
    public static /* final */ int WaveView_wave_stroke_width; // = 8;
    @DexIgnore
    public static /* final */ int[] WeekHeartRateChart; // = {R.attr.WHR_AxesColor, R.attr.WHR_AxesFontSize, R.attr.WHR_AxesTextColor, R.attr.WHR_AxesTextFont, R.attr.WHR_CircleEndColor, R.attr.WHR_CircleOutlineColor, R.attr.WHR_CircleRadius, R.attr.WHR_CircleStartColor, R.attr.WHR_HeartRateTextColor, R.attr.WHR_HeartRateTextFont, R.attr.WHR_TimeFontSize, R.attr.WHR_TimeTextColor, R.attr.WHR_TimeTextFont};
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesColor; // = 0;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesFontSize; // = 1;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesTextColor; // = 2;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesTextFont; // = 3;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleEndColor; // = 4;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleOutlineColor; // = 5;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleRadius; // = 6;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleStartColor; // = 7;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_HeartRateTextColor; // = 8;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_HeartRateTextFont; // = 9;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeFontSize; // = 10;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeTextColor; // = 11;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeTextFont; // = 12;
    @DexIgnore
    public static /* final */ int[] WidgetControl; // = {R.attr.widget_background, R.attr.widget_background_selected, R.attr.widget_bottom_content, R.attr.widget_bottom_content_color, R.attr.widget_bottom_content_color_selected, R.attr.widget_bottom_content_remove, R.attr.widget_bottom_content_size, R.attr.widget_bottom_content_size_selected, R.attr.widget_default_color, R.attr.widget_default_color_selected, R.attr.widget_default_content_size, R.attr.widget_default_content_size_selected, R.attr.widget_remove_mode, R.attr.widget_selected, R.attr.widget_top_content, R.attr.widget_top_content_color, R.attr.widget_top_content_color_selected, R.attr.widget_top_content_size, R.attr.widget_top_content_size_selected, R.attr.widget_top_icon_src, R.attr.widget_top_icon_src_remove, R.attr.widget_top_icon_src_selected};
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background; // = 0;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background_selected; // = 1;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content; // = 2;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_color; // = 3;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_color_selected; // = 4;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_remove; // = 5;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_size; // = 6;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_size_selected; // = 7;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_color; // = 8;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_color_selected; // = 9;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_content_size; // = 10;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_content_size_selected; // = 11;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_remove_mode; // = 12;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_selected; // = 13;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content; // = 14;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_color; // = 15;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_color_selected; // = 16;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_size; // = 17;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_size_selected; // = 18;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src; // = 19;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src_remove; // = 20;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src_selected; // = 21;
    @DexIgnore
    public static /* final */ int[] com_facebook_like_view; // = {R.attr.com_facebook_auxiliary_view_position, R.attr.com_facebook_foreground_color, R.attr.com_facebook_horizontal_alignment, R.attr.com_facebook_object_id, R.attr.com_facebook_object_type, R.attr.com_facebook_style};
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_auxiliary_view_position; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_foreground_color; // = 1;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_horizontal_alignment; // = 2;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_object_id; // = 3;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_object_type; // = 4;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_style; // = 5;
    @DexIgnore
    public static /* final */ int[] com_facebook_login_view; // = {R.attr.com_facebook_confirm_logout, R.attr.com_facebook_login_text, R.attr.com_facebook_logout_text, R.attr.com_facebook_tooltip_mode};
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_confirm_logout; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_login_text; // = 1;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_logout_text; // = 2;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_tooltip_mode; // = 3;
    @DexIgnore
    public static /* final */ int[] com_facebook_profile_picture_view; // = {R.attr.com_facebook_is_cropped, R.attr.com_facebook_preset_size};
    @DexIgnore
    public static /* final */ int com_facebook_profile_picture_view_com_facebook_is_cropped; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_profile_picture_view_com_facebook_preset_size; // = 1;
}
