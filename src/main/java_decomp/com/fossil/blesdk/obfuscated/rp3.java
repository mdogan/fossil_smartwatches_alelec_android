package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.rm2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rp3 extends fq2 implements cq3, xs3.g, rm2.b, xs3.h {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((rd4) null);
    @DexIgnore
    public bq3 m;
    @DexIgnore
    public ur3<r92> n;
    @DexIgnore
    public gk2 o;
    @DexIgnore
    public List<DeviceHelper.ImageStyle> p; // = new ArrayList();
    @DexIgnore
    public HashMap q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final rp3 a() {
            return new rp3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rp3 e;

        @DexIgnore
        public b(rp3 rp3) {
            this.e = rp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rp3.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rp3 e;

        @DexIgnore
        public c(rp3 rp3) {
            this.e = rp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rp3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r92 a;
        @DexIgnore
        public /* final */ /* synthetic */ rp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements rv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, cw<Drawable> cwVar, boolean z) {
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = rp3.r;
                local.d(j1, "showHour onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, cw<Drawable> cwVar, DataSource dataSource, boolean z) {
                wd4.b(drawable, "resource");
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                wd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(rp3.r, "showHour onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public d(r92 r92, rp3 rp3, String str) {
            this.a = r92;
            this.b = rp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = rp3.r;
            local.d(j1, "showHour onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                gk2 a2 = this.b.o;
                if (a2 != null) {
                    fk2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        fk2<Drawable> b2 = a3.b((rv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r92 a;
        @DexIgnore
        public /* final */ /* synthetic */ rp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements rv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, cw<Drawable> cwVar, boolean z) {
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = rp3.r;
                local.d(j1, "showMinute onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, cw<Drawable> cwVar, DataSource dataSource, boolean z) {
                wd4.b(drawable, "resource");
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                wd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(rp3.r, "showMinute onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public e(r92 r92, rp3 rp3, String str) {
            this.a = r92;
            this.b = rp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = rp3.r;
            local.d(j1, "showMinute onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                gk2 a2 = this.b.o;
                if (a2 != null) {
                    fk2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        fk2<Drawable> b2 = a3.b((rv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r92 a;
        @DexIgnore
        public /* final */ /* synthetic */ rp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements rv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, cw<Drawable> cwVar, boolean z) {
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = rp3.r;
                local.d(j1, "showSubeye onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, cw<Drawable> cwVar, DataSource dataSource, boolean z) {
                wd4.b(drawable, "resource");
                wd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                wd4.b(cwVar, "target");
                wd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(rp3.r, "showSubeye onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public f(r92 r92, rp3 rp3, String str) {
            this.a = r92;
            this.b = rp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = rp3.r;
            local.d(j1, "showSubeye onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                gk2 a2 = this.b.o;
                if (a2 != null) {
                    fk2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        fk2<Drawable> b2 = a3.b((rv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String simpleName = rp3.class.getSimpleName();
        wd4.a((Object) simpleName, "CalibrationFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ bq3 b(rp3 rp3) {
        bq3 bq3 = rp3.m;
        if (bq3 != null) {
            return bq3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B() {
        FLogger.INSTANCE.getLocal().d(r, "showBluetoothError");
        if (isActive()) {
            bq3 bq3 = this.m;
            if (bq3 != null) {
                bq3.a(true);
                es3 es3 = es3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                es3.e(childFragmentManager);
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void Q(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            bq3 bq3 = this.m;
            if (bq3 != null) {
                bq3.a(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public boolean S0() {
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.h();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(View view, Boolean bool) {
    }

    @DexIgnore
    public void a1() {
        n();
    }

    @DexIgnore
    public void b1() {
        n();
    }

    @DexIgnore
    public void c(View view) {
        wd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    public void c1() {
        n();
    }

    @DexIgnore
    public void d(View view) {
        wd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }

    @DexIgnore
    public void d1() {
    }

    @DexIgnore
    public void e1() {
    }

    @DexIgnore
    public void j(String str) {
        wd4.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.p.size() > 2) {
                ur3<r92> ur3 = this.n;
                if (ur3 != null) {
                    r92 a2 = ur3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.w;
                        wd4.a((Object) flexibleTextView, "binding.ftvTitle");
                        flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchSubeyeHybrid_Title__AlignTheWatchsSubeyeHandTo));
                        FlexibleTextView flexibleTextView2 = a2.u;
                        wd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                        flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchSubeyeHybrid_Text__FinallyUseTheArrowsToMatch));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        wd4.a((Object) appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(2))).setImageCallback(new f(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void k(String str) {
        wd4.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            ur3<r92> ur3 = this.n;
            if (ur3 != null) {
                r92 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    wd4.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_Title__AlignTheWatchsHourHandTo));
                    FlexibleTextView flexibleTextView2 = a2.u;
                    wd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_Text__UseTheArrowsToMoveThe));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    wd4.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(0))).setImageCallback(new d(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void n() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void o(String str) {
        wd4.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showMinute: deviceId = " + str);
            ur3<r92> ur3 = this.n;
            if (ur3 != null) {
                r92 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    wd4.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHand_Title__AlignTheWatchsMinuteHandTo));
                    FlexibleTextView flexibleTextView2 = a2.u;
                    wd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHand_Text__NowUseTheArrowsToMatch));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    wd4.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(1))).setImageCallback(new e(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onClick");
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.b(view.getId() == R.id.aciv_right);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        bq3 bq3 = this.m;
        if (bq3 != null) {
            this.p = bq3.i();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        r92 r92 = (r92) ra.a(layoutInflater, R.layout.fragment_calibration, viewGroup, false, O0());
        r92.q.setOnClickListener(new b(this));
        r92.v.setOnClickListener(new c(this));
        new rm2().a(r92.s, this);
        new rm2().a(r92.t, this);
        this.n = new ur3<>(this, r92);
        this.o = dk2.a((Fragment) this);
        wd4.a((Object) r92, "binding");
        return r92.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(View view) {
        wd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEnded");
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.k();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(bq3 bq3) {
        wd4.b(bq3, "presenter");
        this.m = bq3;
    }

    @DexIgnore
    public void a(int i, int i2) {
        String str;
        if (isActive()) {
            ur3<r92> ur3 = this.n;
            if (ur3 != null) {
                r92 a2 = ur3.a();
                if (a2 != null) {
                    a2.x.setLength(i2);
                    int i3 = i + 1;
                    a2.x.setProgress((int) ((((float) i3) / ((float) i2)) * ((float) 100)));
                    if (i3 < i2) {
                        str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_CTA__Next);
                    } else {
                        str = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHandDiana_CTA__Done);
                    }
                    FlexibleButton flexibleButton = a2.v;
                    wd4.a((Object) flexibleButton, "it.ftvNext");
                    flexibleButton.setText(str);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
        wd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEvent");
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.c(view.getId() == R.id.aciv_right);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        super.a(str, i, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        n();
    }

    @DexIgnore
    public void o() {
        FLogger.INSTANCE.getLocal().d(r, "showGeneralError");
        bq3 bq3 = this.m;
        if (bq3 != null) {
            bq3.a(true);
            if (isActive()) {
                es3 es3 = es3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                es3.w(childFragmentManager);
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }
}
