package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u10 extends GattOperationResult {
    @DexIgnore
    public /* final */ Peripheral.State b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u10(GattOperationResult.GattResult gattResult, Peripheral.State state) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        wd4.b(state, "newState");
        this.b = state;
    }

    @DexIgnore
    public final Peripheral.State b() {
        return this.b;
    }
}
