package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj2 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<T> {
    }

    @DexIgnore
    public static final <T> String a(T t) {
        String a2 = new Gson().a((Object) t, new a().getType());
        wd4.a((Object) a2, "Gson().toJson(this, type)");
        return a2;
    }
}
