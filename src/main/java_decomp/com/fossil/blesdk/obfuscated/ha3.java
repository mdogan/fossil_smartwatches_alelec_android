package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ha3 extends as2 implements ga3 {
    @DexIgnore
    public ur3<x92> j;
    @DexIgnore
    public fa3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void b(xr2 xr2, ArrayList<String> arrayList) {
        wd4.b(xr2, "baseModel");
        wd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayFragment", "showDayDetails - baseModel=" + xr2);
        ur3<x92> ur3 = this.j;
        if (ur3 != null) {
            x92 a2 = ur3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.q;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ml2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onCreateView");
        x92 x92 = (x92) ra.a(layoutInflater, R.layout.fragment_calories_overview_day, viewGroup, false, O0());
        x92.r.setOnClickListener(b.e);
        this.j = new ur3<>(this, x92);
        ur3<x92> ur3 = this.j;
        if (ur3 != null) {
            x92 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onResume");
        fa3 fa3 = this.k;
        if (fa3 != null) {
            fa3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onStop");
        fa3 fa3 = this.k;
        if (fa3 != null) {
            fa3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        wd4.b(list, "workoutSessions");
        ur3<x92> ur3 = this.j;
        if (ur3 != null) {
            x92 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.u;
                wd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    nh2 nh2 = a2.t;
                    if (nh2 != null) {
                        View d = nh2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                nh2 nh22 = a2.t;
                if (nh22 != null) {
                    View d2 = nh22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            wd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(fa3 fa3) {
        wd4.b(fa3, "presenter");
        this.k = fa3;
    }

    @DexIgnore
    public final void a(nh2 nh2, WorkoutSession workoutSession) {
        if (nh2 != null) {
            View d = nh2.d();
            wd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = tm2.a(context, a2.getSecond().intValue());
            nh2.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = nh2.r;
            wd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = nh2.s;
            wd4.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            be4 be4 = be4.a;
            String a4 = tm2.a(context, (int) R.string.DashboardDiana_Main_ActiveCaloriesToday_Text__NumberCals);
            wd4.a((Object) a4, "LanguageHelper.getString\u2026esToday_Text__NumberCals)");
            Object[] objArr = new Object[1];
            Float totalCalorie = workoutSession.getTotalCalorie();
            objArr[0] = jl2.b(totalCalorie != null ? totalCalorie.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1);
            String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = nh2.q;
            wd4.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(sk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
