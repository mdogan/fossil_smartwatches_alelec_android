package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class de1 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static synchronized int a(Context context) {
        synchronized (de1.class) {
            ck0.a(context, (Object) "Context is null");
            if (a) {
                return 0;
            }
            try {
                df1 a2 = cf1.a(context);
                try {
                    be1.a(a2.zze());
                    jf1.a(a2.zzf());
                    a = true;
                    return 0;
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            } catch (GooglePlayServicesNotAvailableException e2) {
                return e2.errorCode;
            }
        }
    }
}
