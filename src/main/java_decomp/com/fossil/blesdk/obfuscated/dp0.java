package com.fossil.blesdk.obfuscated;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dp0 {
    @DexIgnore
    public static /* final */ DataType a; // = new DataType("com.google.blood_pressure", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", ep0.a, ep0.e, ep0.i, ep0.j);
    @DexIgnore
    public static /* final */ DataType b; // = new DataType("com.google.blood_glucose", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", ep0.k, ep0.l, cp0.K, ep0.m, ep0.n);
    @DexIgnore
    public static /* final */ DataType c; // = new DataType("com.google.oxygen_saturation", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", ep0.o, ep0.s, ep0.w, ep0.x, ep0.y);
    @DexIgnore
    public static /* final */ DataType d; // = new DataType("com.google.body.temperature", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", ep0.z, ep0.A);
    @DexIgnore
    public static /* final */ DataType e; // = new DataType("com.google.body.temperature.basal", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ep0.z, ep0.A);
    @DexIgnore
    public static /* final */ DataType f; // = new DataType("com.google.cervical_mucus", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ep0.B, ep0.C);
    @DexIgnore
    public static /* final */ DataType g; // = new DataType("com.google.cervical_position", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ep0.D, ep0.E, ep0.F);
    @DexIgnore
    public static /* final */ DataType h; // = new DataType("com.google.menstruation", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ep0.G);
    @DexIgnore
    public static /* final */ DataType i; // = new DataType("com.google.ovulation_test", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", ep0.H);
    @DexIgnore
    public static /* final */ DataType j; // = new DataType("com.google.vaginal_spotting", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", cp0.f0);
    @DexIgnore
    public static /* final */ DataType k; // = new DataType("com.google.blood_pressure.summary", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", ep0.b, ep0.d, ep0.c, ep0.f, ep0.h, ep0.g, ep0.i, ep0.j);
    @DexIgnore
    public static /* final */ DataType l; // = new DataType("com.google.blood_glucose.summary", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", cp0.Y, cp0.Z, cp0.a0, ep0.l, cp0.K, ep0.m, ep0.n);
    @DexIgnore
    public static /* final */ DataType m; // = new DataType("com.google.oxygen_saturation.summary", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", ep0.p, ep0.r, ep0.q, ep0.t, ep0.v, ep0.u, ep0.w, ep0.x, ep0.y);
    @DexIgnore
    public static /* final */ DataType n; // = new DataType("com.google.body.temperature.summary", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", cp0.Y, cp0.Z, cp0.a0, ep0.A);
    @DexIgnore
    public static /* final */ DataType o; // = new DataType("com.google.body.temperature.basal.summary", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", cp0.Y, cp0.Z, cp0.a0, ep0.A);
}
