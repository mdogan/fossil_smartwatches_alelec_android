package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i93 implements Factory<ActivityOverviewDayPresenter> {
    @DexIgnore
    public static ActivityOverviewDayPresenter a(g93 g93, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new ActivityOverviewDayPresenter(g93, summariesRepository, activitiesRepository, workoutSessionRepository);
    }
}
