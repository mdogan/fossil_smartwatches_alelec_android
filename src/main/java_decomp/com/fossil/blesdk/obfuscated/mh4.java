package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mh4 {
    @DexIgnore
    public static final lh4 a(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        if (coroutineContext.get(ri4.d) == null) {
            coroutineContext = coroutineContext.plus(ui4.a((ri4) null, 1, (Object) null));
        }
        return new ak4(coroutineContext);
    }
}
