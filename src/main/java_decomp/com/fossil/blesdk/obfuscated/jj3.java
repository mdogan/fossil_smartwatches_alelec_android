package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jj3 extends as2 implements ij3 {
    @DexIgnore
    public static /* final */ Pattern x;
    @DexIgnore
    public static /* final */ a y; // = new a((rd4) null);
    @DexIgnore
    public hj3 j;
    @DexIgnore
    public TextInputEditText k;
    @DexIgnore
    public TextInputEditText l;
    @DexIgnore
    public TextInputEditText m;
    @DexIgnore
    public TextInputLayout n;
    @DexIgnore
    public TextInputLayout o;
    @DexIgnore
    public TextInputLayout p;
    @DexIgnore
    public ProgressButton q;
    @DexIgnore
    public FlexibleTextView r;
    @DexIgnore
    public FlexibleTextView s;
    @DexIgnore
    public RTLImageView t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public HashMap w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final jj3 a() {
            return new jj3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public b(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jj3.c(this.e).clearFocus();
            jj3.b(this.e).clearFocus();
            jj3.a(this.e).clearFocus();
            wd4.a((Object) view, "it");
            view.setFocusable(true);
            if (this.e.T0()) {
                jj3.d(this.e).a(jj3.b(this.e).getEditableText().toString(), jj3.a(this.e).getEditableText().toString());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public c(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    wd4.a((Object) activity, "activity!!");
                    if (!activity.isFinishing()) {
                        FragmentActivity activity2 = this.e.getActivity();
                        if (activity2 != null) {
                            wd4.a((Object) activity2, "activity!!");
                            if (!activity2.isDestroyed()) {
                                FragmentActivity activity3 = this.e.getActivity();
                                if (activity3 != null) {
                                    activity3.finish();
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public d(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wd4.b(editable, "s");
            jj3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public e(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wd4.b(editable, "s");
            jj3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public f(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                jj3.f(this.e).setVisibility(0);
                jj3.g(this.e).setVisibility(0);
                return;
            }
            Editable editableText = jj3.a(this.e).getEditableText();
            wd4.a((Object) editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.e.v) {
                jj3.f(this.e).setVisibility(8);
                jj3.g(this.e).setVisibility(8);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jj3 e;

        @DexIgnore
        public g(jj3 jj3) {
            this.e = jj3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wd4.b(editable, "s");
            jj3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "s");
        }
    }

    /*
    static {
        wd4.a((Object) jj3.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            x = compile;
        } else {
            wd4.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText a(jj3 jj3) {
        TextInputEditText textInputEditText = jj3.l;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wd4.d("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText b(jj3 jj3) {
        TextInputEditText textInputEditText = jj3.k;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wd4.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText c(jj3 jj3) {
        TextInputEditText textInputEditText = jj3.m;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        wd4.d("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ hj3 d(jj3 jj3) {
        hj3 hj3 = jj3.j;
        if (hj3 != null) {
            return hj3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton e(jj3 jj3) {
        ProgressButton progressButton = jj3.q;
        if (progressButton != null) {
            return progressButton;
        }
        wd4.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView f(jj3 jj3) {
        FlexibleTextView flexibleTextView = jj3.r;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wd4.d("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView g(jj3 jj3) {
        FlexibleTextView flexibleTextView = jj3.s;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wd4.d("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    public void A0() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.setEnabled(false);
                TextInputEditText textInputEditText = this.k;
                if (textInputEditText != null) {
                    this.u = textInputEditText.getEditableText().toString();
                    O(true);
                    return;
                }
                wd4.d("mEdtOld");
                throw null;
            }
            wd4.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public void J0() {
        if (isActive()) {
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_PasswordUpdated_Text__YourPasswordHasBeenChanged);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            T(a2);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        TextInputLayout textInputLayout = this.n;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z);
            if (z) {
                TextInputLayout textInputLayout2 = this.n;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_IncorrectPasswordError_Text__YouEnteredAWrongPassword));
                } else {
                    wd4.d("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.n;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    wd4.d("mTvOldError");
                    throw null;
                }
            }
        } else {
            wd4.d("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    public boolean S0() {
        if (getActivity() == null) {
            return false;
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "activity!!");
            if (activity.isFinishing()) {
                return false;
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                wd4.a((Object) activity2, "activity!!");
                if (activity2.isDestroyed()) {
                    return false;
                }
                FragmentActivity activity3 = getActivity();
                if (activity3 != null) {
                    activity3.finish();
                    return true;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean T0() {
        boolean z;
        TextInputEditText textInputEditText = this.k;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.l;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.m;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    boolean z2 = true;
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.r;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_check_text_ok), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = true;
                        } else {
                            wd4.d("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.r;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            wd4.d("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (x.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.s;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_check_text_ok), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            wd4.d("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.s;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            wd4.d("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.n;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.n;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                wd4.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            wd4.d("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (wd4.a((Object) this.u, (Object) obj)) {
                            TextInputLayout textInputLayout3 = this.n;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.n;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_IncorrectPasswordError_Text__YouEnteredAWrongPassword));
                                } else {
                                    wd4.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.n;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.n;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    wd4.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !wd4.a((Object) obj2, (Object) obj)) {
                            TextInputLayout textInputLayout7 = this.o;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.o;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    wd4.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.o;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.o;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_SamePassword_Text__YourNewPasswordCannotBeThe));
                                } else {
                                    wd4.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.o;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.o;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.p;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.p;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        wd4.d("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    wd4.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            wd4.d("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || wd4.a((Object) obj3, (Object) obj2)) {
                        TextInputLayout textInputLayout15 = this.p;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.p;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                wd4.d("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            wd4.d("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!wd4.a((Object) obj3, (Object) obj2))) {
                            TextInputLayout textInputLayout17 = this.p;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.p;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_PasswordsdonotMatchError_Text__PasswordsDontMatch));
                                } else {
                                    wd4.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.v = z;
                    if (this.v) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!wd4.a((Object) obj, (Object) obj2)) && wd4.a((Object) obj2, (Object) obj3) && (!wd4.a((Object) obj, (Object) this.u))) {
                                TextInputLayout textInputLayout19 = this.o;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.o;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.p;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.p;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            wd4.d("mTvRepeatError");
                                            throw null;
                                        }
                                        wd4.d("mTvRepeatError");
                                        throw null;
                                    }
                                    wd4.d("mTvNewError");
                                    throw null;
                                }
                                wd4.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.v) {
                        if (obj2.length() != 0) {
                            z2 = false;
                        }
                        if (!z2) {
                            FlexibleTextView flexibleTextView5 = this.r;
                            if (flexibleTextView5 != null) {
                                flexibleTextView5.setVisibility(0);
                                FlexibleTextView flexibleTextView6 = this.s;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                } else {
                                    wd4.d("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                wd4.d("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                wd4.d("mEdtRepeat");
                throw null;
            }
            wd4.d("mEdtNew");
            throw null;
        }
        wd4.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public void m0() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                wd4.a((Object) fragmentManager, "fragmentManager!!");
                es3.x(fragmentManager);
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ze2 ze2 = (ze2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_profile_change_pass, (ViewGroup) null, false, O0());
        wd4.a((Object) ze2, "binding");
        a(ze2);
        new ur3(this, ze2);
        ProgressButton progressButton = this.q;
        if (progressButton != null) {
            progressButton.setEnabled(false);
            return ze2.d();
        }
        wd4.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        hj3 hj3 = this.j;
        if (hj3 != null) {
            hj3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        hj3 hj3 = this.j;
        if (hj3 != null) {
            hj3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.b();
            } else {
                wd4.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void e() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.c();
            } else {
                wd4.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(hj3 hj3) {
        wd4.b(hj3, "presenter");
        tt1.a(hj3);
        wd4.a((Object) hj3, "checkNotNull(presenter)");
        this.j = hj3;
    }

    @DexIgnore
    public final void a(ze2 ze2) {
        TextInputEditText textInputEditText = ze2.r;
        wd4.a((Object) textInputEditText, "binding.etOldPass");
        this.k = textInputEditText;
        TextInputEditText textInputEditText2 = ze2.q;
        wd4.a((Object) textInputEditText2, "binding.etNewPass");
        this.l = textInputEditText2;
        TextInputEditText textInputEditText3 = ze2.s;
        wd4.a((Object) textInputEditText3, "binding.etRepeatPass");
        this.m = textInputEditText3;
        TextInputLayout textInputLayout = ze2.u;
        wd4.a((Object) textInputLayout, "binding.inputOldPass");
        this.n = textInputLayout;
        TextInputLayout textInputLayout2 = ze2.t;
        wd4.a((Object) textInputLayout2, "binding.inputNewPass");
        this.o = textInputLayout2;
        TextInputLayout textInputLayout3 = ze2.v;
        wd4.a((Object) textInputLayout3, "binding.inputRepeatPass");
        this.p = textInputLayout3;
        ProgressButton progressButton = ze2.x;
        wd4.a((Object) progressButton, "binding.save");
        this.q = progressButton;
        FlexibleTextView flexibleTextView = ze2.y;
        wd4.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
        this.r = flexibleTextView;
        FlexibleTextView flexibleTextView2 = ze2.z;
        wd4.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
        this.s = flexibleTextView2;
        RTLImageView rTLImageView = ze2.w;
        wd4.a((Object) rTLImageView, "binding.ivBack");
        this.t = rTLImageView;
        ProgressButton progressButton2 = this.q;
        if (progressButton2 != null) {
            progressButton2.setOnClickListener(new b(this));
            RTLImageView rTLImageView2 = this.t;
            if (rTLImageView2 != null) {
                rTLImageView2.setOnClickListener(new c(this));
                TextInputEditText textInputEditText4 = this.k;
                if (textInputEditText4 != null) {
                    textInputEditText4.addTextChangedListener(new d(this));
                    TextInputEditText textInputEditText5 = this.l;
                    if (textInputEditText5 != null) {
                        textInputEditText5.addTextChangedListener(new e(this));
                        TextInputEditText textInputEditText6 = this.l;
                        if (textInputEditText6 != null) {
                            textInputEditText6.setOnFocusChangeListener(new f(this));
                            TextInputEditText textInputEditText7 = this.m;
                            if (textInputEditText7 != null) {
                                textInputEditText7.addTextChangedListener(new g(this));
                            } else {
                                wd4.d("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            wd4.d("mEdtNew");
                            throw null;
                        }
                    } else {
                        wd4.d("mEdtNew");
                        throw null;
                    }
                } else {
                    wd4.d("mEdtOld");
                    throw null;
                }
            } else {
                wd4.d("mBackButton");
                throw null;
            }
        } else {
            wd4.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public void e(int i, String str) {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, str, childFragmentManager);
        }
    }
}
