package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f61 extends wb1<f61> {
    @DexIgnore
    public g61[] c; // = g61.e();

    @DexIgnore
    public f61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        g61[] g61Arr = this.c;
        if (g61Arr != null && g61Arr.length > 0) {
            int i = 0;
            while (true) {
                g61[] g61Arr2 = this.c;
                if (i >= g61Arr2.length) {
                    break;
                }
                g61 g61 = g61Arr2[i];
                if (g61 != null) {
                    vb1.a(1, (bc1) g61);
                }
                i++;
            }
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f61)) {
            return false;
        }
        f61 f61 = (f61) obj;
        if (!ac1.a((Object[]) this.c, (Object[]) f61.c)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(f61.b);
        }
        yb1 yb12 = f61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((f61.class.getName().hashCode() + 527) * 31) + ac1.a((Object[]) this.c)) * 31;
        yb1 yb1 = this.b;
        return hashCode + ((yb1 == null || yb1.a()) ? 0 : this.b.hashCode());
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        g61[] g61Arr = this.c;
        if (g61Arr != null && g61Arr.length > 0) {
            int i = 0;
            while (true) {
                g61[] g61Arr2 = this.c;
                if (i >= g61Arr2.length) {
                    break;
                }
                g61 g61 = g61Arr2[i];
                if (g61 != null) {
                    a += vb1.b(1, (bc1) g61);
                }
                i++;
            }
        }
        return a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                int a = ec1.a(ub1, 10);
                g61[] g61Arr = this.c;
                int length = g61Arr == null ? 0 : g61Arr.length;
                g61[] g61Arr2 = new g61[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, g61Arr2, 0, length);
                }
                while (length < g61Arr2.length - 1) {
                    g61Arr2[length] = new g61();
                    ub1.a((bc1) g61Arr2[length]);
                    ub1.c();
                    length++;
                }
                g61Arr2[length] = new g61();
                ub1.a((bc1) g61Arr2[length]);
                this.c = g61Arr2;
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
