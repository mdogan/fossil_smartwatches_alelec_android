package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ q24 f;
    @DexIgnore
    public /* final */ /* synthetic */ r24 g;

    @DexIgnore
    public t24(r24 r24, List list, q24 q24) {
        this.g = r24;
        this.e = list;
        this.f = q24;
    }

    @DexIgnore
    public void run() {
        this.g.a((List<?>) this.e, this.f);
    }
}
