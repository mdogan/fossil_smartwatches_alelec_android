package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.lc;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k42 implements lc.b {
    @DexIgnore
    public /* final */ Map<Class<? extends jc>, Provider<jc>> a;

    @DexIgnore
    public k42(Map<Class<? extends jc>, Provider<jc>> map) {
        wd4.b(map, "creators");
        this.a = map;
    }

    @DexIgnore
    public <T extends jc> T a(Class<T> cls) {
        T t;
        wd4.b(cls, "modelClass");
        Provider provider = this.a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (cls.isAssignableFrom((Class) ((Map.Entry) t).getKey())) {
                    break;
                }
            }
            Map.Entry entry = (Map.Entry) t;
            provider = entry != null ? (Provider) entry.getValue() : null;
        }
        if (provider != null) {
            try {
                T t2 = provider.get();
                if (t2 != null) {
                    return (jc) t2;
                }
                throw new TypeCastException("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
