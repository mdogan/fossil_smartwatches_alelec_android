package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v43 implements Factory<WatchAppsPresenter> {
    @DexIgnore
    public static WatchAppsPresenter a(r43 r43, CategoryRepository categoryRepository, fn2 fn2) {
        return new WatchAppsPresenter(r43, categoryRepository, fn2);
    }
}
