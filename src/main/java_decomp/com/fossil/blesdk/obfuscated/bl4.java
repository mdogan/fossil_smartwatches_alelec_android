package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gk4;
import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bl4 extends fk4<fl4> {
    @DexIgnore
    public bl4() {
        super(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008f, code lost:
        r7 = r9;
     */
    @DexIgnore
    public final fl4 a(TaskMode taskMode) {
        Object obj;
        wd4.b(taskMode, "mode");
        while (true) {
            gk4 gk4 = (gk4) this._cur$internal;
            while (true) {
                long j = gk4._state$internal;
                obj = null;
                if ((1152921504606846976L & j) == 0) {
                    gk4.a aVar = gk4.h;
                    boolean z = false;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((gk4.a & ((int) ((1152921503533105152L & j) >> 30))) != (gk4.a & i)) {
                        Object obj2 = gk4.b.get(gk4.a & i);
                        if (obj2 != null) {
                            if (obj2 instanceof gk4.b) {
                                break;
                            }
                            if (((fl4) obj2).a() == taskMode) {
                                z = true;
                            }
                            if (z) {
                                int i2 = (i + 1) & 1073741823;
                                if (!gk4.f.compareAndSet(gk4, j, gk4.h.a(j, i2))) {
                                    if (gk4.d) {
                                        gk4 gk42 = gk4;
                                        do {
                                            gk42 = gk42.a(i, i2);
                                        } while (gk42 != null);
                                        break;
                                    }
                                } else {
                                    gk4.b.set(gk4.a & i, (Object) null);
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (gk4.d) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    obj = gk4.g;
                    break;
                }
            }
            if (obj != gk4.g) {
                return (fl4) obj;
            }
            fk4.a.compareAndSet(this, gk4, gk4.e());
        }
    }
}
