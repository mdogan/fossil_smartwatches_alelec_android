package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class d64<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements b64<j64>, g64, j64, a64 {
    @DexIgnore
    public /* final */ h64 r; // = new h64();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Result> implements Executor {
        @DexIgnore
        public /* final */ Executor e;
        @DexIgnore
        public /* final */ d64 f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d64$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.d64$a$a  reason: collision with other inner class name */
        public class C0083a extends f64<Result> {
            @DexIgnore
            public C0083a(Runnable runnable, Object obj) {
                super(runnable, obj);
            }

            @DexIgnore
            public <T extends b64<j64> & g64 & j64> T d() {
                return a.this.f;
            }
        }

        @DexIgnore
        public a(Executor executor, d64 d64) {
            this.e = executor;
            this.f = d64;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.e.execute(new C0083a(runnable, (Object) null));
        }
    }

    @DexIgnore
    public boolean b() {
        return ((b64) ((g64) g())).b();
    }

    @DexIgnore
    public Collection<j64> c() {
        return ((b64) ((g64) g())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return Priority.compareTo(this, obj);
    }

    @DexIgnore
    public <T extends b64<j64> & g64 & j64> T g() {
        return this.r;
    }

    @DexIgnore
    public final void a(ExecutorService executorService, Params... paramsArr) {
        super.a((Executor) new a(executorService, this), paramsArr);
    }

    @DexIgnore
    public void a(j64 j64) {
        if (d() == AsyncTask.Status.PENDING) {
            ((b64) ((g64) g())).a(j64);
            return;
        }
        throw new IllegalStateException("Must not add Dependency after task is running");
    }

    @DexIgnore
    public void a(boolean z) {
        ((j64) ((g64) g())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((j64) ((g64) g())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((j64) ((g64) g())).a(th);
    }
}
