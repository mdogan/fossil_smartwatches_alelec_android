package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ci4 extends wi4<ri4> {
    @DexIgnore
    public /* final */ ai4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ci4(ri4 ri4, ai4 ai4) {
        super(ri4);
        wd4.b(ri4, "job");
        wd4.b(ai4, "handle");
        this.i = ai4;
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCompletion[" + this.i + ']';
    }
}
