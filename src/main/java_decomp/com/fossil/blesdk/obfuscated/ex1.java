package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ex1 {
    @DexIgnore
    xn1<Void> a(String str);

    @DexIgnore
    xn1<Void> a(String str, String str2, String str3);

    @DexIgnore
    xn1<String> a(String str, String str2, String str3, String str4);

    @DexIgnore
    boolean a();

    @DexIgnore
    xn1<Void> b(String str, String str2, String str3);

    @DexIgnore
    boolean b();
}
