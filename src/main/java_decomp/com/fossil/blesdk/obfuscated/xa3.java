package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xa3 implements Factory<CaloriesOverviewWeekPresenter> {
    @DexIgnore
    public static CaloriesOverviewWeekPresenter a(va3 va3, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new CaloriesOverviewWeekPresenter(va3, userRepository, summariesRepository);
    }
}
