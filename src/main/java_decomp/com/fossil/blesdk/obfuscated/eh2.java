package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eh2 extends dh2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public long u;

    /*
    static {
        w.put(R.id.ftv_time, 1);
        w.put(R.id.ftv_repeated_days, 2);
        w.put(R.id.sw_enabled, 3);
    }
    */

    @DexIgnore
    public eh2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 4, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public eh2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[0], objArr[2], objArr[1], objArr[3]);
        this.u = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
