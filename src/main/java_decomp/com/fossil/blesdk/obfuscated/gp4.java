package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gp4 implements xo4 {
    @DexIgnore
    public /* final */ vo4 e; // = new vo4();
    @DexIgnore
    public /* final */ kp4 f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public gp4(kp4 kp4) {
        if (kp4 != null) {
            this.f = kp4;
            return;
        }
        throw new NullPointerException("source == null");
    }

    @DexIgnore
    public vo4 a() {
        return this.e;
    }

    @DexIgnore
    public long b(vo4 vo4, long j) throws IOException {
        if (vo4 == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.g) {
            vo4 vo42 = this.e;
            if (vo42.f == 0 && this.f.b(vo42, 8192) == -1) {
                return -1;
            }
            return this.e.b(vo4, Math.min(j, this.e.f));
        } else {
            throw new IllegalStateException("closed");
        }
    }

    @DexIgnore
    public boolean c(long j) throws IOException {
        vo4 vo4;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.g) {
            do {
                vo4 = this.e;
                if (vo4.f >= j) {
                    return true;
                }
            } while (this.f.b(vo4, 8192) != -1);
            return false;
        } else {
            throw new IllegalStateException("closed");
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.g) {
            this.g = true;
            this.f.close();
            this.e.w();
        }
    }

    @DexIgnore
    public ByteString d(long j) throws IOException {
        g(j);
        return this.e.d(j);
    }

    @DexIgnore
    public String e(long j) throws IOException {
        if (j >= 0) {
            long j2 = j == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? Long.MAX_VALUE : j + 1;
            long a2 = a((byte) 10, 0, j2);
            if (a2 != -1) {
                return this.e.j(a2);
            }
            if (j2 < ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD && c(j2) && this.e.h(j2 - 1) == 13 && c(1 + j2) && this.e.h(j2) == 10) {
                return this.e.j(j2);
            }
            vo4 vo4 = new vo4();
            vo4 vo42 = this.e;
            vo42.a(vo4, 0, Math.min(32, vo42.B()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.e.B(), j) + " content=" + vo4.y().hex() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    @DexIgnore
    public byte[] f() throws IOException {
        this.e.a(this.f);
        return this.e.f();
    }

    @DexIgnore
    public boolean g() throws IOException {
        if (!this.g) {
            return this.e.g() && this.f.b(this.e, 8192) == -1;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    public long h() throws IOException {
        g(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!c((long) i2)) {
                break;
            }
            byte h = this.e.h((long) i);
            if ((h >= 48 && h <= 57) || (i == 0 && h == 45)) {
                i = i2;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9] or '-' character but was %#x", new Object[]{Byte.valueOf(h)}));
            }
        }
        if (i == 0) {
        }
        return this.e.h();
    }

    @DexIgnore
    public String i() throws IOException {
        return e(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.g;
    }

    @DexIgnore
    public int j() throws IOException {
        g(4);
        return this.e.j();
    }

    @DexIgnore
    public short k() throws IOException {
        g(2);
        return this.e.k();
    }

    @DexIgnore
    public long l() throws IOException {
        g(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!c((long) i2)) {
                break;
            }
            byte h = this.e.h((long) i);
            if ((h >= 48 && h <= 57) || ((h >= 97 && h <= 102) || (h >= 65 && h <= 70))) {
                i = i2;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(h)}));
            }
        }
        return this.e.l();
    }

    @DexIgnore
    public InputStream m() {
        return new a();
    }

    @DexIgnore
    public int read(ByteBuffer byteBuffer) throws IOException {
        vo4 vo4 = this.e;
        if (vo4.f == 0 && this.f.b(vo4, 8192) == -1) {
            return -1;
        }
        return this.e.read(byteBuffer);
    }

    @DexIgnore
    public byte readByte() throws IOException {
        g(1);
        return this.e.readByte();
    }

    @DexIgnore
    public void readFully(byte[] bArr) throws IOException {
        try {
            g((long) bArr.length);
            this.e.readFully(bArr);
        } catch (EOFException e2) {
            int i = 0;
            while (true) {
                vo4 vo4 = this.e;
                long j = vo4.f;
                if (j > 0) {
                    int a2 = vo4.a(bArr, i, (int) j);
                    if (a2 != -1) {
                        i += a2;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw e2;
                }
            }
        }
    }

    @DexIgnore
    public int readInt() throws IOException {
        g(4);
        return this.e.readInt();
    }

    @DexIgnore
    public short readShort() throws IOException {
        g(2);
        return this.e.readShort();
    }

    @DexIgnore
    public void skip(long j) throws IOException {
        if (!this.g) {
            while (j > 0) {
                vo4 vo4 = this.e;
                if (vo4.f == 0 && this.f.b(vo4, 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.e.B());
                this.e.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.f + ")";
    }

    @DexIgnore
    public long a(jp4 jp4) throws IOException {
        if (jp4 != null) {
            long j = 0;
            while (this.f.b(this.e, 8192) != -1) {
                long x = this.e.x();
                if (x > 0) {
                    j += x;
                    jp4.a(this.e, x);
                }
            }
            if (this.e.B() <= 0) {
                return j;
            }
            long B = j + this.e.B();
            vo4 vo4 = this.e;
            jp4.a(vo4, vo4.B());
            return B;
        }
        throw new IllegalArgumentException("sink == null");
    }

    @DexIgnore
    public byte[] f(long j) throws IOException {
        g(j);
        return this.e.f(j);
    }

    @DexIgnore
    public void g(long j) throws IOException {
        if (!c(j)) {
            throw new EOFException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends InputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public int available() throws IOException {
            gp4 gp4 = gp4.this;
            if (!gp4.g) {
                return (int) Math.min(gp4.e.f, 2147483647L);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public void close() throws IOException {
            gp4.this.close();
        }

        @DexIgnore
        public int read() throws IOException {
            gp4 gp4 = gp4.this;
            if (!gp4.g) {
                vo4 vo4 = gp4.e;
                if (vo4.f == 0 && gp4.f.b(vo4, 8192) == -1) {
                    return -1;
                }
                return gp4.this.e.readByte() & FileType.MASKED_INDEX;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public String toString() {
            return gp4.this + ".inputStream()";
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws IOException {
            if (!gp4.this.g) {
                mp4.a((long) bArr.length, (long) i, (long) i2);
                gp4 gp4 = gp4.this;
                vo4 vo4 = gp4.e;
                if (vo4.f == 0 && gp4.f.b(vo4, 8192) == -1) {
                    return -1;
                }
                return gp4.this.e.a(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public String a(Charset charset) throws IOException {
        if (charset != null) {
            this.e.a(this.f);
            return this.e.a(charset);
        }
        throw new IllegalArgumentException("charset == null");
    }

    @DexIgnore
    public lp4 b() {
        return this.f.b();
    }

    @DexIgnore
    public long a(byte b) throws IOException {
        return a(b, 0, ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public long a(byte b, long j, long j2) throws IOException {
        if (this.g) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2)}));
        } else {
            while (j < j2) {
                long a2 = this.e.a(b, j, j2);
                if (a2 == -1) {
                    vo4 vo4 = this.e;
                    long j3 = vo4.f;
                    if (j3 >= j2 || this.f.b(vo4, 8192) == -1) {
                        break;
                    }
                    j = Math.max(j, j3);
                } else {
                    return a2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public boolean a(long j, ByteString byteString) throws IOException {
        return a(j, byteString, 0, byteString.size());
    }

    @DexIgnore
    public boolean a(long j, ByteString byteString, int i, int i2) throws IOException {
        if (this.g) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || byteString.size() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!c(1 + j2) || this.e.h(j2) != byteString.getByte(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }
}
