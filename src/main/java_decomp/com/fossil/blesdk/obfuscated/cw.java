package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cw<R> extends su {
    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    void a(bw bwVar);

    @DexIgnore
    void a(pv pvVar);

    @DexIgnore
    void a(R r, fw<? super R> fwVar);

    @DexIgnore
    void b(Drawable drawable);

    @DexIgnore
    void b(bw bwVar);

    @DexIgnore
    void c(Drawable drawable);

    @DexIgnore
    pv d();
}
