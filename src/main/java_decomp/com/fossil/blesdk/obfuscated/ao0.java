package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ao0 implements sn0.a {
    @DexIgnore
    public /* final */ /* synthetic */ FrameLayout a;
    @DexIgnore
    public /* final */ /* synthetic */ LayoutInflater b;
    @DexIgnore
    public /* final */ /* synthetic */ ViewGroup c;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ sn0 e;

    @DexIgnore
    public ao0(sn0 sn0, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.e = sn0;
        this.a = frameLayout;
        this.b = layoutInflater;
        this.c = viewGroup;
        this.d = bundle;
    }

    @DexIgnore
    public final void a(un0 un0) {
        this.a.removeAllViews();
        this.a.addView(this.e.a.a(this.b, this.c, this.d));
    }

    @DexIgnore
    public final int getState() {
        return 2;
    }
}
