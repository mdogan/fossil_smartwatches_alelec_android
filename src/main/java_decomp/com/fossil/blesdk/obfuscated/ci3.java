package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ci3 implements Factory<ProfileGoalEditPresenter> {
    @DexIgnore
    public static ProfileGoalEditPresenter a(wh3 wh3, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        return new ProfileGoalEditPresenter(wh3, summariesRepository, sleepSummariesRepository, goalTrackingRepository, userRepository);
    }
}
