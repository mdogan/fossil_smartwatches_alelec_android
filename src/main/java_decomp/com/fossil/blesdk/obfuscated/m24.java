package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m24 {
    @DexIgnore
    public static volatile m24 c;
    @DexIgnore
    public Timer a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public m24(Context context) {
        this.b = context.getApplicationContext();
        this.a = new Timer(false);
    }

    @DexIgnore
    public static m24 a(Context context) {
        if (c == null) {
            synchronized (m24.class) {
                if (c == null) {
                    c = new m24(context);
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void a() {
        if (i04.o() == StatReportStrategy.PERIOD) {
            long l = (long) (i04.l() * 60 * 1000);
            if (i04.q()) {
                u14 b2 = f24.b();
                b2.e("setupPeriodTimer delay:" + l);
            }
            a(new n24(this), l);
        }
    }

    @DexIgnore
    public void a(TimerTask timerTask, long j) {
        if (this.a != null) {
            if (i04.q()) {
                u14 b2 = f24.b();
                b2.e("setupPeriodTimer schedule delay:" + j);
            }
            this.a.schedule(timerTask, j);
        } else if (i04.q()) {
            f24.b().g("setupPeriodTimer schedule timer == null");
        }
    }
}
