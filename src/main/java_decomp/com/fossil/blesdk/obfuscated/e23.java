package com.fossil.blesdk.obfuscated;

import android.view.View;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface e23 extends cs2<d23> {
    @DexIgnore
    void a(g13 g13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void c(int i);

    @DexIgnore
    void c(List<g13> list);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void v();
}
