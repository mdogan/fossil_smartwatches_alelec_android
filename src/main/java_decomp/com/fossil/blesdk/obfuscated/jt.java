package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.fossil.blesdk.obfuscated.lo;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jt<T> implements no<T, Bitmap> {
    @DexIgnore
    public static /* final */ lo<Long> d; // = lo.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new a());
    @DexIgnore
    public static /* final */ lo<Integer> e; // = lo.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new b());
    @DexIgnore
    public static /* final */ d f; // = new d();
    @DexIgnore
    public /* final */ e<T> a;
    @DexIgnore
    public /* final */ kq b;
    @DexIgnore
    public /* final */ d c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements lo.b<Long> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(8);

        @DexIgnore
        public void a(byte[] bArr, Long l, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.a) {
                this.a.position(0);
                messageDigest.update(this.a.putLong(l.longValue()).array());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements lo.b<Integer> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(4);

        @DexIgnore
        public void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.a) {
                    this.a.position(0);
                    messageDigest.update(this.a.putInt(num.intValue()).array());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements e<AssetFileDescriptor> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(a aVar) {
            this();
        }

        @DexIgnore
        public void a(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }

    @DexIgnore
    public interface e<T> {
        @DexIgnore
        void a(MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements e<ParcelFileDescriptor> {
        @DexIgnore
        public void a(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    @DexIgnore
    public jt(kq kqVar, e<T> eVar) {
        this(kqVar, eVar, f);
    }

    @DexIgnore
    public static no<AssetFileDescriptor, Bitmap> a(kq kqVar) {
        return new jt(kqVar, new c((a) null));
    }

    @DexIgnore
    public static no<ParcelFileDescriptor, Bitmap> b(kq kqVar) {
        return new jt(kqVar, new f());
    }

    @DexIgnore
    public boolean a(T t, mo moVar) {
        return true;
    }

    @DexIgnore
    public jt(kq kqVar, e<T> eVar, d dVar) {
        this.b = kqVar;
        this.a = eVar;
        this.c = dVar;
    }

    @DexIgnore
    @TargetApi(27)
    public static Bitmap b(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, DownsampleStrategy downsampleStrategy) {
        try {
            int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                int i4 = parseInt2;
                parseInt2 = parseInt;
                parseInt = i4;
            }
            float b2 = downsampleStrategy.b(parseInt, parseInt2, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, Math.round(((float) parseInt) * b2), Math.round(b2 * ((float) parseInt2)));
        } catch (Throwable th) {
            if (!Log.isLoggable("VideoDecoder", 3)) {
                return null;
            }
            Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            return null;
        }
    }

    @DexIgnore
    public bq<Bitmap> a(T t, int i, int i2, mo moVar) throws IOException {
        long longValue = ((Long) moVar.a(d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) moVar.a(e);
            if (num == null) {
                num = 2;
            }
            DownsampleStrategy downsampleStrategy = (DownsampleStrategy) moVar.a(DownsampleStrategy.f);
            if (downsampleStrategy == null) {
                downsampleStrategy = DownsampleStrategy.e;
            }
            DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
            MediaMetadataRetriever a2 = this.c.a();
            try {
                this.a.a(a2, t);
                Bitmap a3 = a(a2, longValue, num.intValue(), i, i2, downsampleStrategy2);
                a2.release();
                return qs.a(a3, this.b);
            } catch (RuntimeException e2) {
                throw new IOException(e2);
            } catch (Throwable th) {
                a2.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    @DexIgnore
    public static Bitmap a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, DownsampleStrategy downsampleStrategy) {
        Bitmap b2 = (Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || downsampleStrategy == DownsampleStrategy.d) ? null : b(mediaMetadataRetriever, j, i, i2, i3, downsampleStrategy);
        return b2 == null ? a(mediaMetadataRetriever, j, i) : b2;
    }

    @DexIgnore
    public static Bitmap a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }
}
