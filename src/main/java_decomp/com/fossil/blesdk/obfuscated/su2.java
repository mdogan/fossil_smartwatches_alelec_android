package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.InAppNotification;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface su2 extends hq2<ru2> {
    @DexIgnore
    void G();

    @DexIgnore
    void M();

    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    void a(FossilDeviceSerialPatternUtil.DEVICE device, int i);

    @DexIgnore
    void a(InAppNotification inAppNotification);

    @DexIgnore
    void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration);

    @DexIgnore
    void b(int i);

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    void onActivityResult(int i, int i2, Intent intent);
}
