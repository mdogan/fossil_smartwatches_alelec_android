package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sv0 {
    @DexIgnore
    boolean zza(Class<?> cls);

    @DexIgnore
    rv0 zzb(Class<?> cls);
}
