package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lr1 {
    @DexIgnore
    public /* final */ SimpleArrayMap<String, mr1> a; // = new SimpleArrayMap<>();

    @DexIgnore
    public mr1 a(String str) {
        if (b(str)) {
            return this.a.get(str);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public boolean b(String str) {
        return this.a.get(str) != null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || lr1.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((lr1) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return 10 + lr1.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " timings: " + this.a + "}\n";
    }

    @DexIgnore
    public void a(String str, mr1 mr1) {
        this.a.put(str, mr1);
    }

    @DexIgnore
    public long a() {
        int size = this.a.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            mr1 e = this.a.e(i);
            j = Math.max(j, e.a() + e.b());
        }
        return j;
    }

    @DexIgnore
    public static lr1 a(Context context, TypedArray typedArray, int i) {
        if (!typedArray.hasValue(i)) {
            return null;
        }
        int resourceId = typedArray.getResourceId(i, 0);
        if (resourceId != 0) {
            return a(context, resourceId);
        }
        return null;
    }

    @DexIgnore
    public static lr1 a(Context context, int i) {
        try {
            Animator loadAnimator = AnimatorInflater.loadAnimator(context, i);
            if (loadAnimator instanceof AnimatorSet) {
                return a((List<Animator>) ((AnimatorSet) loadAnimator).getChildAnimations());
            }
            if (loadAnimator == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(loadAnimator);
            return a((List<Animator>) arrayList);
        } catch (Exception e) {
            Log.w("MotionSpec", "Can't load animation resource ID #0x" + Integer.toHexString(i), e);
            return null;
        }
    }

    @DexIgnore
    public static lr1 a(List<Animator> list) {
        lr1 lr1 = new lr1();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(lr1, list.get(i));
        }
        return lr1;
    }

    @DexIgnore
    public static void a(lr1 lr1, Animator animator) {
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            lr1.a(objectAnimator.getPropertyName(), mr1.a((ValueAnimator) objectAnimator));
            return;
        }
        throw new IllegalArgumentException("Animator must be an ObjectAnimator: " + animator);
    }
}
