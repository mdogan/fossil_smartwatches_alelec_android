package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s63 implements MembersInjector<HybridCustomizeEditPresenter> {
    @DexIgnore
    public static void a(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        hybridCustomizeEditPresenter.j();
    }
}
