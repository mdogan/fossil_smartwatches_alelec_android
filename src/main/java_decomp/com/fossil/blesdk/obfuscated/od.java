package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.rd;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class od<Key, Value> {
    @DexIgnore
    public Key a;
    @DexIgnore
    public rd.f b;
    @DexIgnore
    public md.b<Key, Value> c;
    @DexIgnore
    public rd.c d;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Executor e; // = h3.b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends qb<rd<Value>> {
        @DexIgnore
        public rd<Value> g;
        @DexIgnore
        public md<Key, Value> h;
        @DexIgnore
        public /* final */ md.c i; // = new C0024a();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ md.b k;
        @DexIgnore
        public /* final */ /* synthetic */ rd.f l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ rd.c o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.od$a$a  reason: collision with other inner class name */
        public class C0024a implements md.c {
            @DexIgnore
            public C0024a() {
            }

            @DexIgnore
            public void a() {
                a.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Executor executor, Object obj, md.b bVar, rd.f fVar, Executor executor2, Executor executor3, rd.c cVar) {
            super(executor);
            this.j = obj;
            this.k = bVar;
            this.l = fVar;
            this.m = executor2;
            this.n = executor3;
            this.o = cVar;
        }

        @DexIgnore
        public rd<Value> a() {
            Object obj = this.j;
            rd<Value> rdVar = this.g;
            if (rdVar != null) {
                obj = rdVar.e();
            }
            do {
                md<Key, Value> mdVar = this.h;
                if (mdVar != null) {
                    mdVar.removeInvalidatedCallback(this.i);
                }
                this.h = this.k.create();
                this.h.addInvalidatedCallback(this.i);
                rd.d dVar = new rd.d(this.h, this.l);
                dVar.b(this.m);
                dVar.a(this.n);
                dVar.a(this.o);
                dVar.a(obj);
                this.g = dVar.a();
            } while (this.g.h());
            return this.g;
        }
    }

    @DexIgnore
    public od(md.b<Key, Value> bVar, rd.f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.c = bVar;
            this.b = fVar;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public LiveData<rd<Value>> a() {
        return a(this.a, this.b, this.d, this.c, h3.d(), this.e);
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public static <Key, Value> LiveData<rd<Value>> a(Key key, rd.f fVar, rd.c cVar, md.b<Key, Value> bVar, Executor executor, Executor executor2) {
        return new a(executor2, key, bVar, fVar, executor, executor2, cVar).b();
    }
}
