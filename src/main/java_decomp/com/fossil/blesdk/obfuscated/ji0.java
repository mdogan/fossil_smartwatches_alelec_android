package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ji0 implements ch0 {
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ fg0 f;
    @DexIgnore
    public /* final */ Looper g;
    @DexIgnore
    public /* final */ og0 h;
    @DexIgnore
    public /* final */ og0 i;
    @DexIgnore
    public /* final */ Map<ee0.c<?>, og0> j;
    @DexIgnore
    public /* final */ Set<df0> k; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ ee0.f l;
    @DexIgnore
    public Bundle m;
    @DexIgnore
    public vd0 n; // = null;
    @DexIgnore
    public vd0 o; // = null;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public /* final */ Lock q;
    @DexIgnore
    public int r; // = 0;

    @DexIgnore
    public ji0(Context context, fg0 fg0, Lock lock, Looper looper, zd0 zd0, Map<ee0.c<?>, ee0.f> map, Map<ee0.c<?>, ee0.f> map2, lj0 lj0, ee0.a<? extends mn1, wm1> aVar, ee0.f fVar, ArrayList<hi0> arrayList, ArrayList<hi0> arrayList2, Map<ee0<?>, Boolean> map3, Map<ee0<?>, Boolean> map4) {
        this.e = context;
        this.f = fg0;
        this.q = lock;
        this.g = looper;
        this.l = fVar;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        zd0 zd02 = zd0;
        og0 og0 = r3;
        og0 og02 = new og0(context2, this.f, lock2, looper2, zd02, map2, (lj0) null, map4, (ee0.a<? extends mn1, wm1>) null, arrayList2, new li0(this, (ki0) null));
        this.h = og0;
        this.i = new og0(context2, this.f, lock2, looper2, zd02, map, lj0, map3, aVar, arrayList, new mi0(this, (ki0) null));
        g4 g4Var = new g4();
        for (ee0.c<?> put : map2.keySet()) {
            g4Var.put(put, this.h);
        }
        for (ee0.c<?> put2 : map.keySet()) {
            g4Var.put(put2, this.i);
        }
        this.j = Collections.unmodifiableMap(g4Var);
    }

    @DexIgnore
    public static ji0 a(Context context, fg0 fg0, Lock lock, Looper looper, zd0 zd0, Map<ee0.c<?>, ee0.f> map, lj0 lj0, Map<ee0<?>, Boolean> map2, ee0.a<? extends mn1, wm1> aVar, ArrayList<hi0> arrayList) {
        Map<ee0<?>, Boolean> map3 = map2;
        g4 g4Var = new g4();
        g4 g4Var2 = new g4();
        ee0.f fVar = null;
        for (Map.Entry next : map.entrySet()) {
            ee0.f fVar2 = (ee0.f) next.getValue();
            if (fVar2.d()) {
                fVar = fVar2;
            }
            if (fVar2.l()) {
                g4Var.put((ee0.c) next.getKey(), fVar2);
            } else {
                g4Var2.put((ee0.c) next.getKey(), fVar2);
            }
        }
        ck0.b(!g4Var.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        g4 g4Var3 = new g4();
        g4 g4Var4 = new g4();
        for (ee0 next2 : map2.keySet()) {
            ee0.c<?> a = next2.a();
            if (g4Var.containsKey(a)) {
                g4Var3.put(next2, map3.get(next2));
            } else if (g4Var2.containsKey(a)) {
                g4Var4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            hi0 hi0 = arrayList.get(i2);
            i2++;
            hi0 hi02 = hi0;
            if (g4Var3.containsKey(hi02.e)) {
                arrayList2.add(hi02);
            } else if (g4Var4.containsKey(hi02.e)) {
                arrayList3.add(hi02);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new ji0(context, fg0, lock, looper, zd0, g4Var, g4Var2, lj0, aVar, fVar, arrayList2, arrayList3, g4Var3, g4Var4);
    }

    @DexIgnore
    public final <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t) {
        if (!c((ue0<? extends ne0, ? extends ee0.b>) t)) {
            return this.h.b(t);
        }
        if (!k()) {
            return this.i.b(t);
        }
        t.c(new Status(4, (String) null, h()));
        return t;
    }

    @DexIgnore
    public final boolean c() {
        this.q.lock();
        try {
            boolean z = true;
            if (!this.h.c() || (!this.i.c() && !k() && this.r != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.h.d();
        this.i.d();
    }

    @DexIgnore
    public final void e() {
        this.q.lock();
        try {
            boolean g2 = g();
            this.i.a();
            this.o = new vd0(4);
            if (g2) {
                new ts0(this.g).post(new ki0(this));
            } else {
                j();
            }
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final vd0 f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean g() {
        this.q.lock();
        try {
            return this.r == 2;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final PendingIntent h() {
        if (this.l == null) {
            return null;
        }
        return PendingIntent.getActivity(this.e, System.identityHashCode(this.f), this.l.k(), 134217728);
    }

    @DexIgnore
    public final void i() {
        if (b(this.n)) {
            if (b(this.o) || k()) {
                int i2 = this.r;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.r = 0;
                        return;
                    }
                    this.f.a(this.m);
                }
                j();
                this.r = 0;
                return;
            }
            vd0 vd0 = this.o;
            if (vd0 == null) {
                return;
            }
            if (this.r == 1) {
                j();
                return;
            }
            a(vd0);
            this.h.a();
        } else if (this.n == null || !b(this.o)) {
            vd0 vd02 = this.n;
            if (vd02 != null) {
                vd0 vd03 = this.o;
                if (vd03 != null) {
                    if (this.i.q < this.h.q) {
                        vd02 = vd03;
                    }
                    a(vd02);
                }
            }
        } else {
            this.i.a();
            a(this.n);
        }
    }

    @DexIgnore
    public final void j() {
        for (df0 onComplete : this.k) {
            onComplete.onComplete();
        }
        this.k.clear();
    }

    @DexIgnore
    public final boolean k() {
        vd0 vd0 = this.o;
        return vd0 != null && vd0.H() == 4;
    }

    @DexIgnore
    public final boolean c(ue0<? extends ne0, ? extends ee0.b> ue0) {
        ee0.c<? extends ee0.b> i2 = ue0.i();
        ck0.a(this.j.containsKey(i2), (Object) "GoogleApiClient is not configured to use the API required for this call.");
        return this.j.get(i2).equals(this.i);
    }

    @DexIgnore
    public final void b() {
        this.r = 2;
        this.p = false;
        this.o = null;
        this.n = null;
        this.h.b();
        this.i.b();
    }

    @DexIgnore
    public static boolean b(vd0 vd0) {
        return vd0 != null && vd0.L();
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t) {
        if (!c((ue0<? extends ne0, ? extends ee0.b>) t)) {
            return this.h.a(t);
        }
        if (!k()) {
            return this.i.a(t);
        }
        t.c(new Status(4, (String) null, h()));
        return t;
    }

    @DexIgnore
    public final void a() {
        this.o = null;
        this.n = null;
        this.r = 0;
        this.h.a();
        this.i.a();
        j();
    }

    @DexIgnore
    public final boolean a(df0 df0) {
        this.q.lock();
        try {
            if ((g() || c()) && !this.i.c()) {
                this.k.add(df0);
                if (this.r == 0) {
                    this.r = 1;
                }
                this.o = null;
                this.i.b();
                return true;
            }
            this.q.unlock();
            return false;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        int i2 = this.r;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.r = 0;
            }
            this.f.a(vd0);
        }
        j();
        this.r = 0;
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        this.f.a(i2, z);
        this.o = null;
        this.n = null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = this.m;
        if (bundle2 == null) {
            this.m = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.i.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.h.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }
}
