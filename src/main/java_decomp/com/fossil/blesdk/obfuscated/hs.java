package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hs implements tr<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public tr<Uri, InputStream> a(xr xrVar) {
            return new hs(this.a);
        }
    }

    @DexIgnore
    public hs(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public tr.a<InputStream> a(Uri uri, int i, int i2, mo moVar) {
        if (!fp.a(i, i2) || !a(moVar)) {
            return null;
        }
        return new tr.a<>(new kw(uri), gp.b(this.a, uri));
    }

    @DexIgnore
    public final boolean a(mo moVar) {
        Long l = (Long) moVar.a(jt.d);
        return l != null && l.longValue() == -1;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return fp.c(uri);
    }
}
