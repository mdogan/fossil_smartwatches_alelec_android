package com.fossil.blesdk.obfuscated;

import org.slf4j.Marker;
import org.slf4j.event.Level;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sq4 implements rq4 {
    @DexIgnore
    public uq4 a;

    @DexIgnore
    public uq4 a() {
        return this.a;
    }

    @DexIgnore
    public void a(long j) {
    }

    @DexIgnore
    public void a(String str) {
    }

    @DexIgnore
    public void a(Throwable th) {
    }

    @DexIgnore
    public void a(Marker marker) {
    }

    @DexIgnore
    public void a(Level level) {
    }

    @DexIgnore
    public void a(Object[] objArr) {
    }

    @DexIgnore
    public void b(String str) {
    }

    @DexIgnore
    public void c(String str) {
    }

    @DexIgnore
    public void a(uq4 uq4) {
        this.a = uq4;
    }
}
