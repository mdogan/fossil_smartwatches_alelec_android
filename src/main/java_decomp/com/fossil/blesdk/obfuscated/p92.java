package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class p92 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ NumberPicker s;
    @DexIgnore
    public /* final */ NumberPicker t;
    @DexIgnore
    public /* final */ NumberPicker u;

    @DexIgnore
    public p92(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleButton2;
        this.s = numberPicker;
        this.t = numberPicker2;
        this.u = numberPicker3;
    }
}
