package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s52 implements Factory<MFLoginWechatManager> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public s52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static s52 a(o42 o42) {
        return new s52(o42);
    }

    @DexIgnore
    public static MFLoginWechatManager b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static MFLoginWechatManager c(o42 o42) {
        MFLoginWechatManager p = o42.p();
        o44.a(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    public MFLoginWechatManager get() {
        return b(this.a);
    }
}
