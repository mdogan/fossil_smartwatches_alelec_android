package com.fossil.blesdk.obfuscated;

import androidx.work.WorkInfo;
import com.fossil.blesdk.obfuscated.il;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface jl {
    @DexIgnore
    int a(WorkInfo.State state, String... strArr);

    @DexIgnore
    int a(String str, long j);

    @DexIgnore
    List<il> a();

    @DexIgnore
    List<il> a(int i);

    @DexIgnore
    List<il.b> a(String str);

    @DexIgnore
    void a(il ilVar);

    @DexIgnore
    void a(String str, bj bjVar);

    @DexIgnore
    List<il> b();

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(String str, long j);

    @DexIgnore
    List<String> c();

    @DexIgnore
    List<String> c(String str);

    @DexIgnore
    int d();

    @DexIgnore
    WorkInfo.State d(String str);

    @DexIgnore
    il e(String str);

    @DexIgnore
    int f(String str);

    @DexIgnore
    List<bj> g(String str);

    @DexIgnore
    int h(String str);
}
