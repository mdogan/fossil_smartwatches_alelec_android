package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv2 implements Factory<e23> {
    @DexIgnore
    public static e23 a(hv2 hv2) {
        e23 d = hv2.d();
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
