package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tr1 extends f0 {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public BottomSheetBehavior.c k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            tr1 tr1 = tr1.this;
            if (tr1.h && tr1.isShowing() && tr1.this.b()) {
                tr1.this.cancel();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnTouchListener {
        @DexIgnore
        public c(tr1 tr1) {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends BottomSheetBehavior.c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(View view, float f) {
        }

        @DexIgnore
        public void a(View view, int i) {
            if (i == 5) {
                tr1.this.cancel();
            }
        }
    }

    @DexIgnore
    public tr1(Context context) {
        this(context, 0);
    }

    @DexIgnore
    public final View a(int i2, View view, ViewGroup.LayoutParams layoutParams) {
        FrameLayout frameLayout = (FrameLayout) View.inflate(getContext(), ar1.design_bottom_sheet_dialog, (ViewGroup) null);
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) frameLayout.findViewById(yq1.coordinator);
        if (i2 != 0 && view == null) {
            view = getLayoutInflater().inflate(i2, coordinatorLayout, false);
        }
        FrameLayout frameLayout2 = (FrameLayout) coordinatorLayout.findViewById(yq1.design_bottom_sheet);
        this.g = BottomSheetBehavior.b(frameLayout2);
        this.g.a(this.k);
        this.g.b(this.h);
        if (layoutParams == null) {
            frameLayout2.addView(view);
        } else {
            frameLayout2.addView(view, layoutParams);
        }
        coordinatorLayout.findViewById(yq1.touch_outside).setOnClickListener(new a());
        g9.a((View) frameLayout2, (m8) new b());
        frameLayout2.setOnTouchListener(new c(this));
        return frameLayout;
    }

    @DexIgnore
    public boolean b() {
        if (!this.j) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{16843611});
            this.i = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
            this.j = true;
        }
        return this.i;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
                window.addFlags(Integer.MIN_VALUE);
            }
            window.setLayout(-1, -1);
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.g;
        if (bottomSheetBehavior != null && bottomSheetBehavior.c() == 5) {
            this.g.c(4);
        }
    }

    @DexIgnore
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.h != z) {
            this.h = z;
            BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.g;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.b(z);
            }
        }
    }

    @DexIgnore
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.h) {
            this.h = true;
        }
        this.i = z;
        this.j = true;
    }

    @DexIgnore
    public void setContentView(int i2) {
        super.setContentView(a(i2, (View) null, (ViewGroup.LayoutParams) null));
    }

    @DexIgnore
    public tr1(Context context, int i2) {
        super(context, a(context, i2));
        this.h = true;
        this.i = true;
        this.k = new d();
        a(1);
    }

    @DexIgnore
    public void setContentView(View view) {
        super.setContentView(a(0, view, (ViewGroup.LayoutParams) null));
    }

    @DexIgnore
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(a(0, view, layoutParams));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends m8 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(View view, r9 r9Var) {
            super.a(view, r9Var);
            if (tr1.this.h) {
                r9Var.a(1048576);
                r9Var.g(true);
                return;
            }
            r9Var.g(false);
        }

        @DexIgnore
        public boolean a(View view, int i, Bundle bundle) {
            if (i == 1048576) {
                tr1 tr1 = tr1.this;
                if (tr1.h) {
                    tr1.cancel();
                    return true;
                }
            }
            return super.a(view, i, bundle);
        }
    }

    @DexIgnore
    public static int a(Context context, int i2) {
        if (i2 != 0) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(uq1.bottomSheetDialogTheme, typedValue, true)) {
            return typedValue.resourceId;
        }
        return cr1.Theme_Design_Light_BottomSheetDialog;
    }
}
