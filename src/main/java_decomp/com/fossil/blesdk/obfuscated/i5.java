package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i5 extends ConstraintWidget {
    @DexIgnore
    public ArrayList<ConstraintWidget> k0; // = new ArrayList<>();

    @DexIgnore
    public void E() {
        this.k0.clear();
        super.E();
    }

    @DexIgnore
    public void I() {
        super.I();
        ArrayList<ConstraintWidget> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ConstraintWidget constraintWidget = this.k0.get(i);
                constraintWidget.b(h(), i());
                if (!(constraintWidget instanceof y4)) {
                    constraintWidget.I();
                }
            }
        }
    }

    @DexIgnore
    public y4 K() {
        ConstraintWidget l = l();
        y4 y4Var = this instanceof y4 ? (y4) this : null;
        while (l != null) {
            ConstraintWidget l2 = l.l();
            if (l instanceof y4) {
                y4Var = (y4) l;
            }
            l = l2;
        }
        return y4Var;
    }

    @DexIgnore
    public void L() {
        I();
        ArrayList<ConstraintWidget> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ConstraintWidget constraintWidget = this.k0.get(i);
                if (constraintWidget instanceof i5) {
                    ((i5) constraintWidget).L();
                }
            }
        }
    }

    @DexIgnore
    public void M() {
        this.k0.clear();
    }

    @DexIgnore
    public void a(o4 o4Var) {
        super.a(o4Var);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).a(o4Var);
        }
    }

    @DexIgnore
    public void b(ConstraintWidget constraintWidget) {
        this.k0.add(constraintWidget);
        if (constraintWidget.l() != null) {
            ((i5) constraintWidget.l()).c(constraintWidget);
        }
        constraintWidget.a((ConstraintWidget) this);
    }

    @DexIgnore
    public void c(ConstraintWidget constraintWidget) {
        this.k0.remove(constraintWidget);
        constraintWidget.a((ConstraintWidget) null);
    }

    @DexIgnore
    public void b(int i, int i2) {
        super.b(i, i2);
        int size = this.k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.k0.get(i3).b(p(), q());
        }
    }
}
