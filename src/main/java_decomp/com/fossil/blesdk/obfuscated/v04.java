package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ int f;

    @DexIgnore
    public v04(Context context, int i) {
        this.e = context;
        this.f = i;
    }

    @DexIgnore
    public final void run() {
        try {
            k04.f(this.e);
            h14.b(this.e).a(this.f);
        } catch (Throwable th) {
            k04.m.a(th);
            k04.a(this.e, th);
        }
    }
}
