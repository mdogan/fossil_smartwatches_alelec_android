package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sy0 extends us0 implements ry0 {
    @DexIgnore
    public sy0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.clearcut.internal.IClearcutLoggerService");
    }

    @DexIgnore
    public final void a(py0 py0, sd0 sd0) throws RemoteException {
        Parcel o = o();
        nu0.a(o, (IInterface) py0);
        nu0.a(o, (Parcelable) sd0);
        a(1, o);
    }
}
