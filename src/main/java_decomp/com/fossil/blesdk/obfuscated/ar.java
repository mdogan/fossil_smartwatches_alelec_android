package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import com.fossil.blesdk.obfuscated.br;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ar extends rw<ko, bq<?>> implements br {
    @DexIgnore
    public br.a d;

    @DexIgnore
    public ar(long j) {
        super(j);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ bq a(ko koVar, bq bqVar) {
        return (bq) super.b(koVar, bqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void a(ko koVar, bq<?> bqVar) {
        br.a aVar = this.d;
        if (aVar != null && bqVar != null) {
            aVar.a(bqVar);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ bq a(ko koVar) {
        return (bq) super.c(koVar);
    }

    @DexIgnore
    public void a(br.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int b(bq<?> bqVar) {
        if (bqVar == null) {
            return super.b(null);
        }
        return bqVar.b();
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20 || i == 15) {
            a(c() / 2);
        }
    }
}
