package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g14 implements q24 {
    @DexIgnore
    public /* final */ /* synthetic */ d14 a;

    @DexIgnore
    public g14(d14 d14) {
        this.a = d14;
    }

    @DexIgnore
    public void a() {
        k04.c();
        if (h14.i().f > 0) {
            k04.a(this.a.d, -1);
        }
    }

    @DexIgnore
    public void b() {
        h14.i().a(this.a.a, (q24) null, this.a.c, true);
        k04.d();
    }
}
