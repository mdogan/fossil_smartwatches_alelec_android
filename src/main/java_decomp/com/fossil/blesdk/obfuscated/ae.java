package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.vd;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ae<A, B> extends vd<B> {
    @DexIgnore
    public /* final */ vd<A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vd.b<A> {
        @DexIgnore
        public /* final */ /* synthetic */ vd.b a;

        @DexIgnore
        public a(vd.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(List<A> list, int i, int i2) {
            this.a.a(md.convert(ae.this.b, list), i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends vd.e<A> {
        @DexIgnore
        public /* final */ /* synthetic */ vd.e a;

        @DexIgnore
        public b(vd.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(md.convert(ae.this.b, list));
        }
    }

    @DexIgnore
    public ae(vd<A> vdVar, m3<List<A>, List<B>> m3Var) {
        this.a = vdVar;
        this.b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(md.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadInitial(vd.d dVar, vd.b<B> bVar) {
        this.a.loadInitial(dVar, new a(bVar));
    }

    @DexIgnore
    public void loadRange(vd.g gVar, vd.e<B> eVar) {
        this.a.loadRange(gVar, new b(eVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(md.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
