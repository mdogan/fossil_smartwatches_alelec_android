package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c52 implements Factory<GuestApiService> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public c52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static c52 a(o42 o42) {
        return new c52(o42);
    }

    @DexIgnore
    public static GuestApiService b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static GuestApiService c(o42 o42) {
        GuestApiService h = o42.h();
        o44.a(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    public GuestApiService get() {
        return b(this.a);
    }
}
