package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.TransmitDataPhase;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w50 extends TransmitDataPhase {
    @DexIgnore
    public /* final */ DeviceData P;
    @DexIgnore
    public /* final */ FileType Q;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ w50(Peripheral peripheral, Phase.a aVar, DeviceData deviceData, FileType fileType, String str, int i, rd4 rd4) {
        this(peripheral, aVar, deviceData, fileType, str);
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public byte[] F() {
        DeviceData deviceData = this.P;
        short b = a50.b.b(j().k(), this.Q);
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(this.Q.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = va0.y.g();
        }
        return deviceData.getResponseData$blesdk_productionRelease(b, version);
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.DEVICE_DATA, this.P.toJSONObject());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public w50(Peripheral peripheral, Phase.a aVar, DeviceData deviceData, FileType fileType, String str) {
        super(peripheral, aVar, PhaseId.SEND_DEVICE_DATA, true, a50.b.b(peripheral.k(), r12), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r7, 32, (rd4) null);
        DeviceData deviceData2 = deviceData;
        FileType fileType2 = fileType;
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(deviceData2, "deviceResponse");
        wd4.b(fileType2, "fileType");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        this.P = deviceData2;
        this.Q = fileType2;
    }
}
