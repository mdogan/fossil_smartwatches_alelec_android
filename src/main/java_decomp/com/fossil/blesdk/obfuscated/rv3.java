package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rv3 {
    @DexIgnore
    kv3 a(iv3 iv3) throws IOException;

    @DexIgnore
    pw3 a(kv3 kv3) throws IOException;

    @DexIgnore
    void a();

    @DexIgnore
    void a(kv3 kv3, kv3 kv32) throws IOException;

    @DexIgnore
    void a(qw3 qw3);

    @DexIgnore
    void b(iv3 iv3) throws IOException;
}
