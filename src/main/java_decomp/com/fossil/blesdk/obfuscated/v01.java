package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v01 extends oz0 implements u01 {
    @DexIgnore
    public v01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
    }

    @DexIgnore
    public final void a(cq0 cq0) throws RemoteException {
        Parcel o = o();
        z01.a(o, (Parcelable) cq0);
        a(22, o);
    }
}
