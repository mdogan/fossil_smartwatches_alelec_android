package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nb0 {
    @DexIgnore
    public static /* final */ nb0 a; // = new nb0();

    @DexIgnore
    public final void a(BroadcastReceiver broadcastReceiver, String... strArr) {
        wd4.b(broadcastReceiver, "receiver");
        wd4.b(strArr, "actions");
        Context a2 = wa0.f.a();
        if (a2 != null) {
            IntentFilter intentFilter = new IntentFilter();
            for (String addAction : strArr) {
                intentFilter.addAction(addAction);
            }
            sc.a(a2).a(broadcastReceiver, intentFilter);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        wd4.b(intent, "intent");
        Context a2 = wa0.f.a();
        if (a2 != null) {
            return sc.a(a2).a(intent);
        }
        return false;
    }
}
