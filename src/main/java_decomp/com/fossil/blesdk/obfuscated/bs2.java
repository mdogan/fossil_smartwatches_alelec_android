package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.xs3;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bs2 extends as2 implements xs3.g {
    @DexIgnore
    public HashMap j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        wd4.a((Object) bs2.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a(PermissionCodes... permissionCodesArr) {
        wd4.b(permissionCodesArr, "permissionCodes");
        ArrayList arrayList = new ArrayList();
        for (PermissionCodes add : permissionCodesArr) {
            arrayList.add(add);
        }
        cn2.d.a(getContext(), (ArrayList<PermissionCodes>) arrayList);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (getActivity() instanceof BaseActivity) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
