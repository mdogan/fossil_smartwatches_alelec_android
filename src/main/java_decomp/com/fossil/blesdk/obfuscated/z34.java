package com.fossil.blesdk.obfuscated;

import android.content.ContentProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class z34 extends ContentProvider {
    @DexIgnore
    public boolean onCreate() {
        u34.a((ContentProvider) this);
        return true;
    }
}
