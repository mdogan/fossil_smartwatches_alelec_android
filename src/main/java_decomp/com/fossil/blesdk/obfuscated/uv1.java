package com.fossil.blesdk.obfuscated;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uv1<T, R> extends tv1 implements GenericDeclaration {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T> extends uv1<T, T> {
        @DexIgnore
        public /* final */ Constructor<?> g;

        @DexIgnore
        public a(Constructor<?> constructor) {
            super(constructor);
            this.g = constructor;
        }

        @DexIgnore
        public Type[] b() {
            Type[] genericParameterTypes = this.g.getGenericParameterTypes();
            if (genericParameterTypes.length <= 0 || !c()) {
                return genericParameterTypes;
            }
            Class<?>[] parameterTypes = this.g.getParameterTypes();
            return (genericParameterTypes.length == parameterTypes.length && parameterTypes[0] == getDeclaringClass().getEnclosingClass()) ? (Type[]) Arrays.copyOfRange(genericParameterTypes, 1, genericParameterTypes.length) : genericParameterTypes;
        }

        @DexIgnore
        public final boolean c() {
            Class<?> declaringClass = this.g.getDeclaringClass();
            if (declaringClass.getEnclosingConstructor() != null) {
                return true;
            }
            Method enclosingMethod = declaringClass.getEnclosingMethod();
            if (enclosingMethod != null) {
                return !Modifier.isStatic(enclosingMethod.getModifiers());
            }
            if (declaringClass.getEnclosingClass() == null || Modifier.isStatic(declaringClass.getModifiers())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final TypeVariable<?>[] getTypeParameters() {
            TypeVariable[] typeParameters = getDeclaringClass().getTypeParameters();
            TypeVariable[] typeParameters2 = this.g.getTypeParameters();
            TypeVariable<?>[] typeVariableArr = new TypeVariable[(typeParameters.length + typeParameters2.length)];
            System.arraycopy(typeParameters, 0, typeVariableArr, 0, typeParameters.length);
            System.arraycopy(typeParameters2, 0, typeVariableArr, typeParameters.length, typeParameters2.length);
            return typeVariableArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> extends uv1<T, Object> {
        @DexIgnore
        public /* final */ Method g;

        @DexIgnore
        public b(Method method) {
            super(method);
            this.g = method;
        }

        @DexIgnore
        public final TypeVariable<?>[] getTypeParameters() {
            return this.g.getTypeParameters();
        }
    }

    @DexIgnore
    public <M extends AccessibleObject & Member> uv1(M m) {
        super(m);
    }

    @DexIgnore
    public final Class<? super T> getDeclaringClass() {
        return super.getDeclaringClass();
    }
}
