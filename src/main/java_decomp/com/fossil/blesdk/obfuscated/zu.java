package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zu implements su {
    @DexIgnore
    public /* final */ Set<cw<?>> e; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void a(cw<?> cwVar) {
        this.e.add(cwVar);
    }

    @DexIgnore
    public void b(cw<?> cwVar) {
        this.e.remove(cwVar);
    }

    @DexIgnore
    public void c() {
        for (T c : vw.a(this.e)) {
            c.c();
        }
    }

    @DexIgnore
    public void e() {
        this.e.clear();
    }

    @DexIgnore
    public List<cw<?>> f() {
        return vw.a(this.e);
    }

    @DexIgnore
    public void a() {
        for (T a : vw.a(this.e)) {
            a.a();
        }
    }

    @DexIgnore
    public void b() {
        for (T b : vw.a(this.e)) {
            b.b();
        }
    }
}
