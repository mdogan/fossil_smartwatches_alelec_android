package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface w30 {
    @DexIgnore
    h90<cb4> a(HandMovingType handMovingType, HandMovingConfig[] handMovingConfigArr);
}
