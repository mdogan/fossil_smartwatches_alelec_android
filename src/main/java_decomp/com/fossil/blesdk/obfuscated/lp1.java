package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lp1 extends kk0 implements yo1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lp1> CREATOR; // = new mp1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ List<aq1> f;

    @DexIgnore
    public lp1(String str, List<aq1> list) {
        this.e = str;
        this.f = list;
        ck0.a(this.e);
        ck0.a(this.f);
    }

    @DexIgnore
    public final String H() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || lp1.class != obj.getClass()) {
            return false;
        }
        lp1 lp1 = (lp1) obj;
        String str = this.e;
        if (str == null ? lp1.e != null : !str.equals(lp1.e)) {
            return false;
        }
        List<aq1> list = this.f;
        List<aq1> list2 = lp1.f;
        return list == null ? list2 == null : list.equals(list2);
    }

    @DexIgnore
    public final int hashCode() {
        String str = this.e;
        int i = 0;
        int hashCode = ((str != null ? str.hashCode() : 0) + 31) * 31;
        List<aq1> list = this.f;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final String toString() {
        String str = this.e;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, H(), false);
        lk0.b(parcel, 3, this.f, false);
        lk0.a(parcel, a);
    }
}
