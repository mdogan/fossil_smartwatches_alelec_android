package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xj0 implements yj0 {
    @DexIgnore
    public /* final */ IBinder e;

    @DexIgnore
    public xj0(IBinder iBinder) {
        this.e = iBinder;
    }

    @DexIgnore
    public final void a(wj0 wj0, oj0 oj0) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            obtain.writeStrongBinder(wj0 != null ? wj0.asBinder() : null);
            if (oj0 != null) {
                obtain.writeInt(1);
                oj0.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.e.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.e;
    }
}
