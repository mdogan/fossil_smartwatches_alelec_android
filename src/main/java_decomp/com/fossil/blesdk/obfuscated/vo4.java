package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import okio.ByteString;
import okio.SegmentedByteString;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vo4 implements xo4, wo4, Cloneable, ByteChannel {
    @DexIgnore
    public static /* final */ byte[] g; // = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    @DexIgnore
    public hp4 e;
    @DexIgnore
    public long f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OutputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public void flush() {
        }

        @DexIgnore
        public String toString() {
            return vo4.this + ".outputStream()";
        }

        @DexIgnore
        public void write(int i) {
            vo4.this.writeByte((int) (byte) i);
        }

        @DexIgnore
        public void write(byte[] bArr, int i, int i2) {
            vo4.this.write(bArr, i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends InputStream {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int available() {
            return (int) Math.min(vo4.this.f, 2147483647L);
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public int read() {
            vo4 vo4 = vo4.this;
            if (vo4.f > 0) {
                return vo4.readByte() & FileType.MASKED_INDEX;
            }
            return -1;
        }

        @DexIgnore
        public String toString() {
            return vo4.this + ".inputStream()";
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) {
            return vo4.this.a(bArr, i, i2);
        }
    }

    @DexIgnore
    public int A() throws EOFException {
        byte b2;
        int i;
        byte b3;
        if (this.f != 0) {
            byte h = h(0);
            int i2 = 1;
            if ((h & 128) == 0) {
                b3 = h & Byte.MAX_VALUE;
                i = 1;
                b2 = 0;
            } else if ((h & 224) == 192) {
                b3 = h & 31;
                i = 2;
                b2 = 128;
            } else if ((h & 240) == 224) {
                b3 = h & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY;
                i = 3;
                b2 = 2048;
            } else if ((h & 248) == 240) {
                b3 = h & 7;
                i = 4;
                b2 = 65536;
            } else {
                skip(1);
                return 65533;
            }
            long j = (long) i;
            if (this.f >= j) {
                while (i2 < i) {
                    long j2 = (long) i2;
                    byte h2 = h(j2);
                    if ((h2 & 192) == 128) {
                        b3 = (b3 << 6) | (h2 & 63);
                        i2++;
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (b3 > 1114111) {
                    return 65533;
                }
                if ((b3 < 55296 || b3 > 57343) && b3 >= b2) {
                    return b3;
                }
                return 65533;
            }
            throw new EOFException("size < " + i + ": " + this.f + " (to read code point prefixed 0x" + Integer.toHexString(h) + ")");
        }
        throw new EOFException();
    }

    @DexIgnore
    public final long B() {
        return this.f;
    }

    @DexIgnore
    public final ByteString C() {
        long j = this.f;
        if (j <= 2147483647L) {
            return a((int) j);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f);
    }

    @DexIgnore
    public vo4 a() {
        return this;
    }

    @DexIgnore
    public wo4 c() {
        return this;
    }

    @DexIgnore
    public boolean c(long j) {
        return this.f >= j;
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    public vo4 d() {
        return this;
    }

    @DexIgnore
    public OutputStream e() {
        return new a();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof vo4)) {
            return false;
        }
        vo4 vo4 = (vo4) obj;
        long j = this.f;
        if (j != vo4.f) {
            return false;
        }
        long j2 = 0;
        if (j == 0) {
            return true;
        }
        hp4 hp4 = this.e;
        hp4 hp42 = vo4.e;
        int i = hp4.b;
        int i2 = hp42.b;
        while (j2 < this.f) {
            long min = (long) Math.min(hp4.c - i, hp42.c - i2);
            int i3 = i2;
            int i4 = i;
            int i5 = 0;
            while (((long) i5) < min) {
                int i6 = i4 + 1;
                int i7 = i3 + 1;
                if (hp4.a[i4] != hp42.a[i3]) {
                    return false;
                }
                i5++;
                i4 = i6;
                i3 = i7;
            }
            if (i4 == hp4.c) {
                hp4 = hp4.f;
                i = hp4.b;
            } else {
                i = i4;
            }
            if (i3 == hp42.c) {
                hp42 = hp42.f;
                i2 = hp42.b;
            } else {
                i2 = i3;
            }
            j2 += min;
        }
        return true;
    }

    @DexIgnore
    public byte[] f() {
        try {
            return f(this.f);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public void flush() {
    }

    @DexIgnore
    public boolean g() {
        return this.f == 0;
    }

    @DexIgnore
    public final byte h(long j) {
        int i;
        mp4.a(this.f, j, 1);
        long j2 = this.f;
        if (j2 - j > j) {
            hp4 hp4 = this.e;
            while (true) {
                int i2 = hp4.c;
                int i3 = hp4.b;
                long j3 = (long) (i2 - i3);
                if (j < j3) {
                    return hp4.a[i3 + ((int) j)];
                }
                j -= j3;
                hp4 = hp4.f;
            }
        } else {
            long j4 = j - j2;
            hp4 hp42 = this.e;
            do {
                hp42 = hp42.g;
                int i4 = hp42.c;
                i = hp42.b;
                j4 += (long) (i4 - i);
            } while (j4 < 0);
            return hp42.a[i + ((int) j4)];
        }
    }

    @DexIgnore
    public int hashCode() {
        hp4 hp4 = this.e;
        if (hp4 == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = hp4.c;
            for (int i3 = hp4.b; i3 < i2; i3++) {
                i = (i * 31) + hp4.a[i3];
            }
            hp4 = hp4.f;
        } while (hp4 != this.e);
        return i;
    }

    @DexIgnore
    public String i(long j) throws EOFException {
        return a(j, mp4.a);
    }

    @DexIgnore
    public boolean isOpen() {
        return true;
    }

    @DexIgnore
    public int j() {
        return mp4.a(readInt());
    }

    @DexIgnore
    public short k() {
        return mp4.a(readShort());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        if (r8 != r9) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
        r15.e = r6.b();
        com.fossil.blesdk.obfuscated.ip4.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009b, code lost:
        r6.b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009d, code lost:
        if (r0 != false) goto L_0x00a3;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0074 A[SYNTHETIC] */
    public long l() {
        int i;
        int i2;
        if (this.f != 0) {
            boolean z = false;
            long j = 0;
            int i3 = 0;
            do {
                hp4 hp4 = this.e;
                byte[] bArr = hp4.a;
                int i4 = hp4.b;
                int i5 = hp4.c;
                while (true) {
                    if (i4 >= i5) {
                        break;
                    }
                    byte b2 = bArr[i4];
                    if (b2 < 48 || b2 > 57) {
                        if (b2 >= 97 && b2 <= 102) {
                            i2 = b2 - 97;
                        } else if (b2 >= 65 && b2 <= 70) {
                            i2 = b2 - 65;
                        } else if (i3 == 0) {
                            z = true;
                        } else {
                            throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(b2));
                        }
                        i = i2 + 10;
                    } else {
                        i = b2 - 48;
                    }
                    if ((-1152921504606846976L & j) == 0) {
                        j = (j << 4) | ((long) i);
                        i4++;
                        i3++;
                    } else {
                        vo4 vo4 = new vo4();
                        vo4.a(j);
                        vo4.writeByte((int) b2);
                        throw new NumberFormatException("Number too large: " + vo4.z());
                    }
                }
                if (i3 == 0) {
                }
            } while (this.e != null);
            this.f -= (long) i3;
            return j;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public InputStream m() {
        return new b();
    }

    @DexIgnore
    public int read(ByteBuffer byteBuffer) throws IOException {
        hp4 hp4 = this.e;
        if (hp4 == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), hp4.c - hp4.b);
        byteBuffer.put(hp4.a, hp4.b, min);
        hp4.b += min;
        this.f -= (long) min;
        if (hp4.b == hp4.c) {
            this.e = hp4.b();
            ip4.a(hp4);
        }
        return min;
    }

    @DexIgnore
    public byte readByte() {
        long j = this.f;
        if (j != 0) {
            hp4 hp4 = this.e;
            int i = hp4.b;
            int i2 = hp4.c;
            int i3 = i + 1;
            byte b2 = hp4.a[i];
            this.f = j - 1;
            if (i3 == i2) {
                this.e = hp4.b();
                ip4.a(hp4);
            } else {
                hp4.b = i3;
            }
            return b2;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public void readFully(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 != -1) {
                i += a2;
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    public int readInt() {
        long j = this.f;
        if (j >= 4) {
            hp4 hp4 = this.e;
            int i = hp4.b;
            int i2 = hp4.c;
            if (i2 - i < 4) {
                return ((readByte() & FileType.MASKED_INDEX) << 24) | ((readByte() & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((readByte() & FileType.MASKED_INDEX) << 8) | (readByte() & FileType.MASKED_INDEX);
            }
            byte[] bArr = hp4.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & FileType.MASKED_INDEX) << 24) | ((bArr[i3] & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
            int i5 = i4 + 1;
            byte b3 = b2 | ((bArr[i4] & FileType.MASKED_INDEX) << 8);
            int i6 = i5 + 1;
            byte b4 = b3 | (bArr[i5] & FileType.MASKED_INDEX);
            this.f = j - 4;
            if (i6 == i2) {
                this.e = hp4.b();
                ip4.a(hp4);
            } else {
                hp4.b = i6;
            }
            return b4;
        }
        throw new IllegalStateException("size < 4: " + this.f);
    }

    @DexIgnore
    public short readShort() {
        long j = this.f;
        if (j >= 2) {
            hp4 hp4 = this.e;
            int i = hp4.b;
            int i2 = hp4.c;
            if (i2 - i < 2) {
                return (short) (((readByte() & FileType.MASKED_INDEX) << 8) | (readByte() & FileType.MASKED_INDEX));
            }
            byte[] bArr = hp4.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & FileType.MASKED_INDEX) << 8) | (bArr[i3] & FileType.MASKED_INDEX);
            this.f = j - 2;
            if (i4 == i2) {
                this.e = hp4.b();
                ip4.a(hp4);
            } else {
                hp4.b = i4;
            }
            return (short) b2;
        }
        throw new IllegalStateException("size < 2: " + this.f);
    }

    @DexIgnore
    public void skip(long j) throws EOFException {
        while (j > 0) {
            hp4 hp4 = this.e;
            if (hp4 != null) {
                int min = (int) Math.min(j, (long) (hp4.c - hp4.b));
                long j2 = (long) min;
                this.f -= j2;
                j -= j2;
                hp4 hp42 = this.e;
                hp42.b += min;
                if (hp42.b == hp42.c) {
                    this.e = hp42.b();
                    ip4.a(hp42);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @DexIgnore
    public String toString() {
        return C().toString();
    }

    @DexIgnore
    public final void w() {
        try {
            skip(this.f);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public final long x() {
        long j = this.f;
        if (j == 0) {
            return 0;
        }
        hp4 hp4 = this.e.g;
        int i = hp4.c;
        return (i >= 8192 || !hp4.e) ? j : j - ((long) (i - hp4.b));
    }

    @DexIgnore
    public ByteString y() {
        return new ByteString(f());
    }

    @DexIgnore
    public String z() {
        try {
            return a(this.f, mp4.a);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public vo4 b(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            writeByte(48);
            return this;
        }
        boolean z = false;
        int i2 = 1;
        if (i < 0) {
            j = -j;
            if (j < 0) {
                a("-9223372036854775808");
                return this;
            }
            z = true;
        }
        if (j >= 100000000) {
            i2 = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= ButtonService.CONNECT_TIMEOUT) {
            i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i2 = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i2 = 2;
        }
        if (z) {
            i2++;
        }
        hp4 b2 = b(i2);
        byte[] bArr = b2.a;
        int i3 = b2.c + i2;
        while (j != 0) {
            i3--;
            bArr[i3] = g[(int) (j % 10)];
            j /= 10;
        }
        if (z) {
            bArr[i3 - 1] = 45;
        }
        b2.c += i2;
        this.f += (long) i2;
        return this;
    }

    @DexIgnore
    public vo4 c(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            writeByte((i >> 6) | 192);
            writeByte((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                writeByte((i >> 12) | 224);
                writeByte(((i >> 6) & 63) | 128);
                writeByte((i & 63) | 128);
            } else {
                writeByte(63);
            }
        } else if (i <= 1114111) {
            writeByte((i >> 18) | 240);
            writeByte(((i >> 12) & 63) | 128);
            writeByte(((i >> 6) & 63) | 128);
            writeByte((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    @DexIgnore
    public vo4 clone() {
        vo4 vo4 = new vo4();
        if (this.f == 0) {
            return vo4;
        }
        vo4.e = this.e.c();
        hp4 hp4 = vo4.e;
        hp4.g = hp4;
        hp4.f = hp4;
        hp4 hp42 = this.e;
        while (true) {
            hp42 = hp42.f;
            if (hp42 != this.e) {
                vo4.e.g.a(hp42.c());
            } else {
                vo4.f = this.f;
                return vo4;
            }
        }
    }

    @DexIgnore
    public ByteString d(long j) throws EOFException {
        return new ByteString(f(j));
    }

    @DexIgnore
    public String e(long j) throws EOFException {
        if (j >= 0) {
            long j2 = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            if (j != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                j2 = j + 1;
            }
            long a2 = a((byte) 10, 0, j2);
            if (a2 != -1) {
                return j(a2);
            }
            if (j2 < B() && h(j2 - 1) == 13 && h(j2) == 10) {
                return j(j2);
            }
            vo4 vo4 = new vo4();
            a(vo4, 0, Math.min(32, B()));
            throw new EOFException("\\n not found: limit=" + Math.min(B(), j) + " content=" + vo4.y().hex() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    @DexIgnore
    public void g(long j) throws EOFException {
        if (this.f < j) {
            throw new EOFException();
        }
    }

    @DexIgnore
    public String i() throws EOFException {
        return e(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public String j(long j) throws EOFException {
        if (j > 0) {
            long j2 = j - 1;
            if (h(j2) == 13) {
                String i = i(j2);
                skip(2);
                return i;
            }
        }
        String i2 = i(j);
        skip(1);
        return i2;
    }

    @DexIgnore
    public vo4 writeByte(int i) {
        hp4 b2 = b(1);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        b2.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f++;
        return this;
    }

    @DexIgnore
    public vo4 writeInt(int i) {
        hp4 b2 = b(4);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        b2.c = i5 + 1;
        this.f += 4;
        return this;
    }

    @DexIgnore
    public vo4 writeShort(int i) {
        hp4 b2 = b(2);
        byte[] bArr = b2.a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        b2.c = i3 + 1;
        this.f += 2;
        return this;
    }

    @DexIgnore
    public byte[] f(long j) throws EOFException {
        mp4.a(this.f, 0, j);
        if (j <= 2147483647L) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        }
        throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
    }

    @DexIgnore
    public vo4 write(byte[] bArr) {
        if (bArr != null) {
            write(bArr, 0, bArr.length);
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public final vo4 a(vo4 vo4, long j, long j2) {
        if (vo4 != null) {
            mp4.a(this.f, j, j2);
            if (j2 == 0) {
                return this;
            }
            vo4.f += j2;
            hp4 hp4 = this.e;
            while (true) {
                int i = hp4.c;
                int i2 = hp4.b;
                if (j < ((long) (i - i2))) {
                    break;
                }
                j -= (long) (i - i2);
                hp4 = hp4.f;
            }
            while (j2 > 0) {
                hp4 c = hp4.c();
                c.b = (int) (((long) c.b) + j);
                c.c = Math.min(c.b + ((int) j2), c.c);
                hp4 hp42 = vo4.e;
                if (hp42 == null) {
                    c.g = c;
                    c.f = c;
                    vo4.e = c;
                } else {
                    hp42.g.a(c);
                }
                j2 -= (long) (c.c - c.b);
                hp4 = hp4.f;
                j = 0;
            }
            return this;
        }
        throw new IllegalArgumentException("out == null");
    }

    @DexIgnore
    public vo4 write(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            long j = (long) i2;
            mp4.a((long) bArr.length, (long) i, j);
            int i3 = i2 + i;
            while (i < i3) {
                hp4 b2 = b(1);
                int min = Math.min(i3 - i, 8192 - b2.c);
                System.arraycopy(bArr, i, b2.a, b2.c, min);
                i += min;
                b2.c += min;
            }
            this.f += j;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (r5 != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        r1.readByte();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        throw new java.lang.NumberFormatException("Number too large: " + r1.z());
     */
    @DexIgnore
    public long h() {
        long j = 0;
        if (this.f != 0) {
            int i = 0;
            long j2 = -7;
            boolean z = false;
            boolean z2 = false;
            loop0:
            do {
                hp4 hp4 = this.e;
                byte[] bArr = hp4.a;
                int i2 = hp4.b;
                int i3 = hp4.c;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    byte b2 = bArr[i2];
                    if (b2 >= 48 && b2 <= 57) {
                        int i4 = 48 - b2;
                        int i5 = (j > -922337203685477580L ? 1 : (j == -922337203685477580L ? 0 : -1));
                        if (i5 < 0 || (i5 == 0 && ((long) i4) < j2)) {
                            vo4 vo4 = new vo4();
                            vo4.b(j);
                            vo4.writeByte((int) b2);
                        } else {
                            j = (j * 10) + ((long) i4);
                        }
                    } else if (b2 == 45 && i == 0) {
                        j2--;
                        z = true;
                    } else if (i != 0) {
                        z2 = true;
                    } else {
                        throw new NumberFormatException("Expected leading [0-9] or '-' character but was 0x" + Integer.toHexString(b2));
                    }
                    i2++;
                    i++;
                }
                if (i2 == i3) {
                    this.e = hp4.b();
                    ip4.a(hp4);
                } else {
                    hp4.b = i2;
                }
                if (z2) {
                    break;
                }
            } while (this.e != null);
            this.f -= (long) i;
            return z ? j : -j;
        }
        throw new IllegalStateException("size == 0");
    }

    @DexIgnore
    public int write(ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer != null) {
            int remaining = byteBuffer.remaining();
            int i = remaining;
            while (i > 0) {
                hp4 b2 = b(1);
                int min = Math.min(i, 8192 - b2.c);
                byteBuffer.get(b2.a, b2.c, min);
                i -= min;
                b2.c += min;
            }
            this.f += (long) remaining;
            return remaining;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public hp4 b(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        }
        hp4 hp4 = this.e;
        if (hp4 == null) {
            this.e = ip4.a();
            hp4 hp42 = this.e;
            hp42.g = hp42;
            hp42.f = hp42;
            return hp42;
        }
        hp4 hp43 = hp4.g;
        if (hp43.c + i <= 8192 && hp43.e) {
            return hp43;
        }
        hp4 a2 = ip4.a();
        hp43.a(a2);
        return a2;
    }

    @DexIgnore
    public long a(jp4 jp4) throws IOException {
        long j = this.f;
        if (j > 0) {
            jp4.a(this, j);
        }
        return j;
    }

    @DexIgnore
    public String a(Charset charset) {
        try {
            return a(this.f, charset);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public long b(vo4 vo4, long j) {
        if (vo4 == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j >= 0) {
            long j2 = this.f;
            if (j2 == 0) {
                return -1;
            }
            if (j > j2) {
                j = j2;
            }
            vo4.a(this, j);
            return j;
        } else {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        }
    }

    @DexIgnore
    public String a(long j, Charset charset) throws EOFException {
        mp4.a(this.f, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            hp4 hp4 = this.e;
            int i = hp4.b;
            if (((long) i) + j > ((long) hp4.c)) {
                return new String(f(j), charset);
            }
            String str = new String(hp4.a, i, (int) j, charset);
            hp4.b = (int) (((long) hp4.b) + j);
            this.f -= j;
            if (hp4.b == hp4.c) {
                this.e = hp4.b();
                ip4.a(hp4);
            }
            return str;
        }
    }

    @DexIgnore
    public lp4 b() {
        return lp4.d;
    }

    @DexIgnore
    public int a(byte[] bArr, int i, int i2) {
        mp4.a((long) bArr.length, (long) i, (long) i2);
        hp4 hp4 = this.e;
        if (hp4 == null) {
            return -1;
        }
        int min = Math.min(i2, hp4.c - hp4.b);
        System.arraycopy(hp4.a, hp4.b, bArr, i, min);
        hp4.b += min;
        this.f -= (long) min;
        if (hp4.b == hp4.c) {
            this.e = hp4.b();
            ip4.a(hp4);
        }
        return min;
    }

    @DexIgnore
    public vo4 a(ByteString byteString) {
        if (byteString != null) {
            byteString.write(this);
            return this;
        }
        throw new IllegalArgumentException("byteString == null");
    }

    @DexIgnore
    public vo4 a(String str) {
        a(str, 0, str.length());
        return this;
    }

    @DexIgnore
    public vo4 a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 <= str.length()) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    hp4 b2 = b(1);
                    byte[] bArr = b2.a;
                    int i3 = b2.c - i;
                    int min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        char charAt2 = str.charAt(i4);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i4 + i3] = (byte) charAt2;
                        i4++;
                    }
                    int i5 = b2.c;
                    int i6 = (i3 + i4) - i5;
                    b2.c = i5 + i6;
                    this.f += (long) i6;
                    i = i4;
                } else {
                    if (charAt < 2048) {
                        writeByte((charAt >> 6) | 192);
                        writeByte((int) (charAt & '?') | 128);
                    } else if (charAt < 55296 || charAt > 57343) {
                        writeByte((charAt >> 12) | 224);
                        writeByte(((charAt >> 6) & 63) | 128);
                        writeByte((int) (charAt & '?') | 128);
                    } else {
                        int i7 = i + 1;
                        char charAt3 = i7 < i2 ? str.charAt(i7) : 0;
                        if (charAt > 56319 || charAt3 < 56320 || charAt3 > 57343) {
                            writeByte(63);
                            i = i7;
                        } else {
                            int i8 = (((charAt & 10239) << 10) | (9215 & charAt3)) + 0;
                            writeByte((i8 >> 18) | 240);
                            writeByte(((i8 >> 12) & 63) | 128);
                            writeByte(((i8 >> 6) & 63) | 128);
                            writeByte((i8 & 63) | 128);
                            i += 2;
                        }
                    }
                    i++;
                }
            }
            return this;
        } else {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        }
    }

    @DexIgnore
    public vo4 a(String str, Charset charset) {
        a(str, 0, str.length(), charset);
        return this;
    }

    @DexIgnore
    public vo4 a(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(mp4.a)) {
            a(str, i, i2);
            return this;
        } else {
            byte[] bytes = str.substring(i, i2).getBytes(charset);
            write(bytes, 0, bytes.length);
            return this;
        }
    }

    @DexIgnore
    public long a(kp4 kp4) throws IOException {
        if (kp4 != null) {
            long j = 0;
            while (true) {
                long b2 = kp4.b(this, 8192);
                if (b2 == -1) {
                    return j;
                }
                j += b2;
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }

    @DexIgnore
    public vo4 a(long j) {
        if (j == 0) {
            writeByte(48);
            return this;
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        hp4 b2 = b(numberOfTrailingZeros);
        byte[] bArr = b2.a;
        int i = b2.c;
        for (int i2 = (i + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = g[(int) (15 & j)];
            j >>>= 4;
        }
        b2.c += numberOfTrailingZeros;
        this.f += (long) numberOfTrailingZeros;
        return this;
    }

    @DexIgnore
    public void a(vo4 vo4, long j) {
        int i;
        if (vo4 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (vo4 != this) {
            mp4.a(vo4.f, 0, j);
            while (j > 0) {
                hp4 hp4 = vo4.e;
                if (j < ((long) (hp4.c - hp4.b))) {
                    hp4 hp42 = this.e;
                    hp4 hp43 = hp42 != null ? hp42.g : null;
                    if (hp43 != null && hp43.e) {
                        long j2 = ((long) hp43.c) + j;
                        if (hp43.d) {
                            i = 0;
                        } else {
                            i = hp43.b;
                        }
                        if (j2 - ((long) i) <= 8192) {
                            vo4.e.a(hp43, (int) j);
                            vo4.f -= j;
                            this.f += j;
                            return;
                        }
                    }
                    vo4.e = vo4.e.a((int) j);
                }
                hp4 hp44 = vo4.e;
                long j3 = (long) (hp44.c - hp44.b);
                vo4.e = hp44.b();
                hp4 hp45 = this.e;
                if (hp45 == null) {
                    this.e = hp44;
                    hp4 hp46 = this.e;
                    hp46.g = hp46;
                    hp46.f = hp46;
                } else {
                    hp45.g.a(hp44);
                    hp44.a();
                }
                vo4.f -= j3;
                this.f += j3;
                j -= j3;
            }
        } else {
            throw new IllegalArgumentException("source == this");
        }
    }

    @DexIgnore
    public long a(byte b2) {
        return a(b2, 0, (long) ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public long a(byte b2, long j, long j2) {
        long j3 = 0;
        if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(this.f), Long.valueOf(j), Long.valueOf(j2)}));
        }
        long j4 = this.f;
        if (j2 <= j4) {
            j4 = j2;
        }
        if (j == j4) {
            return -1;
        }
        hp4 hp4 = this.e;
        if (hp4 == null) {
            return -1;
        }
        long j5 = this.f;
        if (j5 - j >= j) {
            while (true) {
                j5 = j3;
                j3 = ((long) (hp4.c - hp4.b)) + j5;
                if (j3 >= j) {
                    break;
                }
                hp4 = hp4.f;
            }
        } else {
            while (j5 > j) {
                hp4 = hp4.g;
                j5 -= (long) (hp4.c - hp4.b);
            }
        }
        long j6 = j;
        while (j5 < j4) {
            byte[] bArr = hp4.a;
            int min = (int) Math.min((long) hp4.c, (((long) hp4.b) + j4) - j5);
            for (int i = (int) ((((long) hp4.b) + j6) - j5); i < min; i++) {
                if (bArr[i] == b2) {
                    return ((long) (i - hp4.b)) + j5;
                }
            }
            byte b3 = b2;
            j6 = ((long) (hp4.c - hp4.b)) + j5;
            hp4 = hp4.f;
            j5 = j6;
        }
        return -1;
    }

    @DexIgnore
    public boolean a(long j, ByteString byteString) {
        return a(j, byteString, 0, byteString.size());
    }

    @DexIgnore
    public boolean a(long j, ByteString byteString, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.f - j < ((long) i2) || byteString.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (h(((long) i3) + j) != byteString.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final ByteString a(int i) {
        if (i == 0) {
            return ByteString.EMPTY;
        }
        return new SegmentedByteString(this, i);
    }
}
