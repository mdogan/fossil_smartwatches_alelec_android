package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.bp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class op1 implements ap1 {
    @DexIgnore
    public /* final */ bp1.b e;

    @DexIgnore
    public op1(bp1.b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public final void a(zo1 zo1) {
        this.e.a(np1.a(zo1));
    }

    @DexIgnore
    public final void b(zo1 zo1, int i, int i2) {
        this.e.b(np1.a(zo1), i, i2);
    }

    @DexIgnore
    public final void c(zo1 zo1, int i, int i2) {
        this.e.c(np1.a(zo1), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || op1.class != obj.getClass()) {
            return false;
        }
        return this.e.equals(((op1) obj).e);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final void a(zo1 zo1, int i, int i2) {
        this.e.a(np1.a(zo1), i, i2);
    }
}
