package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.br4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hr4 extends g0 {
    @DexIgnore
    public br4.a e;
    @DexIgnore
    public br4.b f;

    @DexIgnore
    public static hr4 a(String str, String str2, String str3, int i, int i2, String[] strArr) {
        hr4 hr4 = new hr4();
        hr4.setArguments(new fr4(str2, str3, str, i, i2, strArr).a());
        return hr4;
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() != null) {
            if (getParentFragment() instanceof br4.a) {
                this.e = (br4.a) getParentFragment();
            }
            if (getParentFragment() instanceof br4.b) {
                this.f = (br4.b) getParentFragment();
            }
        }
        if (context instanceof br4.a) {
            this.e = (br4.a) context;
        }
        if (context instanceof br4.b) {
            this.f = (br4.b) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        setCancelable(false);
        fr4 fr4 = new fr4(getArguments());
        return fr4.b(getContext(), new er4(this, fr4, this.e, this.f));
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.e = null;
        this.f = null;
    }

    @DexIgnore
    public void a(FragmentManager fragmentManager, String str) {
        if (!fragmentManager.e()) {
            show(fragmentManager, str);
        }
    }
}
