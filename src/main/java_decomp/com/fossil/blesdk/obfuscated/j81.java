package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j81 {
    @DexIgnore
    public static volatile j81 b;
    @DexIgnore
    public static /* final */ j81 c; // = new j81(true);
    @DexIgnore
    public /* final */ Map<a, u81.d<?, ?>> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    /*
    static {
        b();
    }
    */

    @DexIgnore
    public j81() {
        this.a = new HashMap();
    }

    @DexIgnore
    public static j81 a() {
        return s81.a(j81.class);
    }

    @DexIgnore
    public static Class<?> b() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static j81 c() {
        return i81.b();
    }

    @DexIgnore
    public static j81 d() {
        j81 j81 = b;
        if (j81 == null) {
            synchronized (j81.class) {
                j81 = b;
                if (j81 == null) {
                    j81 = i81.c();
                    b = j81;
                }
            }
        }
        return j81;
    }

    @DexIgnore
    public final <ContainingType extends x91> u81.d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return this.a.get(new a(containingtype, i));
    }

    @DexIgnore
    public j81(boolean z) {
        this.a = Collections.emptyMap();
    }
}
