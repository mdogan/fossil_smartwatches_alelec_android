package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface e51 extends IInterface {
    @DexIgnore
    tn0 zza(Bitmap bitmap) throws RemoteException;
}
