package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;
import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l81 extends k81<Object> {
    @DexIgnore
    public final boolean a(x91 x91) {
        return x91 instanceof u81.c;
    }

    @DexIgnore
    public final n81<Object> b(Object obj) {
        u81.c cVar = (u81.c) obj;
        if (cVar.zzbyl.c()) {
            cVar.zzbyl = (n81) cVar.zzbyl.clone();
        }
        return cVar.zzbyl;
    }

    @DexIgnore
    public final void c(Object obj) {
        a(obj).f();
    }

    @DexIgnore
    public final n81<Object> a(Object obj) {
        return ((u81.c) obj).zzbyl;
    }

    @DexIgnore
    public final <UT, UB> UB a(na1 na1, Object obj, j81 j81, n81<Object> n81, UB ub, fb1<UT, UB> fb1) throws IOException {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final int a(Map.Entry<?, ?> entry) {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final void a(tb1 tb1, Map.Entry<?, ?> entry) throws IOException {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Object a(j81 j81, x91 x91, int i) {
        return j81.a(x91, i);
    }

    @DexIgnore
    public final void a(na1 na1, Object obj, j81 j81, n81<Object> n81) throws IOException {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final void a(zzte zzte, Object obj, j81 j81, n81<Object> n81) throws IOException {
        throw new NoSuchMethodError();
    }
}
