package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uy2 implements Factory<DoNotDisturbScheduledTimePresenter> {
    @DexIgnore
    public static DoNotDisturbScheduledTimePresenter a(ty2 ty2, DNDSettingsDatabase dNDSettingsDatabase) {
        return new DoNotDisturbScheduledTimePresenter(ty2, dNDSettingsDatabase);
    }
}
