package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hy0 extends rx0<hy0> implements Cloneable {
    @DexIgnore
    public String[] g;
    @DexIgnore
    public String[] h;
    @DexIgnore
    public int[] i; // = zx0.a;
    @DexIgnore
    public long[] j;
    @DexIgnore
    public long[] k;

    @DexIgnore
    public hy0() {
        String[] strArr = zx0.c;
        this.g = strArr;
        this.h = strArr;
        long[] jArr = zx0.b;
        this.j = jArr;
        this.k = jArr;
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(qx0 qx0) throws IOException {
        String[] strArr = this.g;
        int i2 = 0;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr2 = this.g;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i3];
                if (str != null) {
                    qx0.a(1, str);
                }
                i3++;
            }
        }
        String[] strArr3 = this.h;
        if (strArr3 != null && strArr3.length > 0) {
            int i4 = 0;
            while (true) {
                String[] strArr4 = this.h;
                if (i4 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i4];
                if (str2 != null) {
                    qx0.a(2, str2);
                }
                i4++;
            }
        }
        int[] iArr = this.i;
        if (iArr != null && iArr.length > 0) {
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.i;
                if (i5 >= iArr2.length) {
                    break;
                }
                qx0.b(3, iArr2[i5]);
                i5++;
            }
        }
        long[] jArr = this.j;
        if (jArr != null && jArr.length > 0) {
            int i6 = 0;
            while (true) {
                long[] jArr2 = this.j;
                if (i6 >= jArr2.length) {
                    break;
                }
                qx0.a(4, jArr2[i6]);
                i6++;
            }
        }
        long[] jArr3 = this.k;
        if (jArr3 != null && jArr3.length > 0) {
            while (true) {
                long[] jArr4 = this.k;
                if (i2 >= jArr4.length) {
                    break;
                }
                qx0.a(5, jArr4[i2]);
                i2++;
            }
        }
        super.a(qx0);
    }

    @DexIgnore
    public final int b() {
        long[] jArr;
        int[] iArr;
        int b = super.b();
        String[] strArr = this.g;
        int i2 = 0;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (true) {
                String[] strArr2 = this.g;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i3];
                if (str != null) {
                    i5++;
                    i4 += qx0.a(str);
                }
                i3++;
            }
            b = b + i4 + (i5 * 1);
        }
        String[] strArr3 = this.h;
        if (strArr3 != null && strArr3.length > 0) {
            int i6 = 0;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                String[] strArr4 = this.h;
                if (i6 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i6];
                if (str2 != null) {
                    i8++;
                    i7 += qx0.a(str2);
                }
                i6++;
            }
            b = b + i7 + (i8 * 1);
        }
        int[] iArr2 = this.i;
        if (iArr2 != null && iArr2.length > 0) {
            int i9 = 0;
            int i10 = 0;
            while (true) {
                iArr = this.i;
                if (i9 >= iArr.length) {
                    break;
                }
                i10 += qx0.d(iArr[i9]);
                i9++;
            }
            b = b + i10 + (iArr.length * 1);
        }
        long[] jArr2 = this.j;
        if (jArr2 != null && jArr2.length > 0) {
            int i11 = 0;
            int i12 = 0;
            while (true) {
                jArr = this.j;
                if (i11 >= jArr.length) {
                    break;
                }
                i12 += qx0.c(jArr[i11]);
                i11++;
            }
            b = b + i12 + (jArr.length * 1);
        }
        long[] jArr3 = this.k;
        if (jArr3 == null || jArr3.length <= 0) {
            return b;
        }
        int i13 = 0;
        while (true) {
            long[] jArr4 = this.k;
            if (i2 >= jArr4.length) {
                return b + i13 + (jArr4.length * 1);
            }
            i13 += qx0.c(jArr4[i2]);
            i2++;
        }
    }

    @DexIgnore
    public final /* synthetic */ wx0 c() throws CloneNotSupportedException {
        return (hy0) clone();
    }

    @DexIgnore
    public final /* synthetic */ rx0 d() throws CloneNotSupportedException {
        return (hy0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final hy0 clone() {
        try {
            hy0 hy0 = (hy0) super.clone();
            String[] strArr = this.g;
            if (strArr != null && strArr.length > 0) {
                hy0.g = (String[]) strArr.clone();
            }
            String[] strArr2 = this.h;
            if (strArr2 != null && strArr2.length > 0) {
                hy0.h = (String[]) strArr2.clone();
            }
            int[] iArr = this.i;
            if (iArr != null && iArr.length > 0) {
                hy0.i = (int[]) iArr.clone();
            }
            long[] jArr = this.j;
            if (jArr != null && jArr.length > 0) {
                hy0.j = (long[]) jArr.clone();
            }
            long[] jArr2 = this.k;
            if (jArr2 != null && jArr2.length > 0) {
                hy0.k = (long[]) jArr2.clone();
            }
            return hy0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof hy0)) {
            return false;
        }
        hy0 hy0 = (hy0) obj;
        if (!vx0.a((Object[]) this.g, (Object[]) hy0.g) || !vx0.a((Object[]) this.h, (Object[]) hy0.h) || !vx0.a(this.i, hy0.i) || !vx0.a(this.j, hy0.j) || !vx0.a(this.k, hy0.k)) {
            return false;
        }
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            return this.f.equals(hy0.f);
        }
        tx0 tx02 = hy0.f;
        return tx02 == null || tx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((((((((((hy0.class.getName().hashCode() + 527) * 31) + vx0.a((Object[]) this.g)) * 31) + vx0.a((Object[]) this.h)) * 31) + vx0.a(this.i)) * 31) + vx0.a(this.j)) * 31) + vx0.a(this.k)) * 31;
        tx0 tx0 = this.f;
        return hashCode + ((tx0 == null || tx0.a()) ? 0 : this.f.hashCode());
    }
}
