package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fc1 implements IInterface {
    @DexIgnore
    public /* final */ IBinder e;

    @DexIgnore
    public fc1(IBinder iBinder, String str) {
        this.e = iBinder;
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.e;
    }
}
