package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wi3 implements MembersInjector<ProfileOptInActivity> {
    @DexIgnore
    public static void a(ProfileOptInActivity profileOptInActivity, ProfileOptInPresenter profileOptInPresenter) {
        profileOptInActivity.B = profileOptInPresenter;
    }
}
