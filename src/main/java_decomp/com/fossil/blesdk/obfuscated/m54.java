package com.fossil.blesdk.obfuscated;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m54 implements Runnable {
    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(10);
        a();
    }
}
