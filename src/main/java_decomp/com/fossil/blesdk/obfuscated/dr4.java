package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dr4 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131821371;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131821372;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131821373;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131821374;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131821375;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131821376;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131821377;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131821378;
    @DexIgnore
    public static /* final */ int abc_font_family_body_1_material; // = 2131821379;
    @DexIgnore
    public static /* final */ int abc_font_family_body_2_material; // = 2131821380;
    @DexIgnore
    public static /* final */ int abc_font_family_button_material; // = 2131821381;
    @DexIgnore
    public static /* final */ int abc_font_family_caption_material; // = 2131821382;
    @DexIgnore
    public static /* final */ int abc_font_family_display_1_material; // = 2131821383;
    @DexIgnore
    public static /* final */ int abc_font_family_display_2_material; // = 2131821384;
    @DexIgnore
    public static /* final */ int abc_font_family_display_3_material; // = 2131821385;
    @DexIgnore
    public static /* final */ int abc_font_family_display_4_material; // = 2131821386;
    @DexIgnore
    public static /* final */ int abc_font_family_headline_material; // = 2131821387;
    @DexIgnore
    public static /* final */ int abc_font_family_menu_material; // = 2131821388;
    @DexIgnore
    public static /* final */ int abc_font_family_subhead_material; // = 2131821389;
    @DexIgnore
    public static /* final */ int abc_font_family_title_material; // = 2131821390;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131821391;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131821392;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131821393;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131821394;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131821395;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131821396;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131821397;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131821398;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131821399;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131821400;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131821401;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131821402;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131821403;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131821404;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131821405;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131821406;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131821407;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131821408;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131821409;
    @DexIgnore
    public static /* final */ int rationale_ask; // = 2131821618;
    @DexIgnore
    public static /* final */ int rationale_ask_again; // = 2131821619;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131821639;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131821651;
    @DexIgnore
    public static /* final */ int title_settings_dialog; // = 2131821659;
}
