package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ak4 implements lh4 {
    @DexIgnore
    public /* final */ CoroutineContext e;

    @DexIgnore
    public ak4(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        this.e = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.e;
    }
}
