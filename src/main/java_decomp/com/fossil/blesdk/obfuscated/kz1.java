package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kz1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kz1> CREATOR; // = new nz1();
    @DexIgnore
    public Bundle e;
    @DexIgnore
    public Map<String, String> f;
    @DexIgnore
    public a g;

    @DexIgnore
    public kz1(Bundle bundle) {
        this.e = bundle;
    }

    @DexIgnore
    public final Map<String, String> H() {
        if (this.f == null) {
            Bundle bundle = this.e;
            g4 g4Var = new g4();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                        g4Var.put(str, str2);
                    }
                }
            }
            this.f = g4Var;
        }
        return this.f;
    }

    @DexIgnore
    public final String I() {
        return this.e.getString("from");
    }

    @DexIgnore
    public final a J() {
        if (this.g == null && lz1.b(this.e)) {
            this.g = new a(this.e);
        }
        return this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = lk0.a(parcel);
        lk0.a(parcel, 2, this.e, false);
        lk0.a(parcel, a2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(Bundle bundle) {
            this.a = lz1.b(bundle, "gcm.n.title");
            lz1.c(bundle, "gcm.n.title");
            a(bundle, "gcm.n.title");
            this.b = lz1.b(bundle, "gcm.n.body");
            lz1.c(bundle, "gcm.n.body");
            a(bundle, "gcm.n.body");
            lz1.b(bundle, "gcm.n.icon");
            lz1.d(bundle);
            lz1.b(bundle, "gcm.n.tag");
            lz1.b(bundle, "gcm.n.color");
            lz1.b(bundle, "gcm.n.click_action");
            lz1.b(bundle, "gcm.n.android_channel_id");
            lz1.c(bundle);
        }

        @DexIgnore
        public static String[] a(Bundle bundle, String str) {
            Object[] d = lz1.d(bundle, str);
            if (d == null) {
                return null;
            }
            String[] strArr = new String[d.length];
            for (int i = 0; i < d.length; i++) {
                strArr[i] = String.valueOf(d[i]);
            }
            return strArr;
        }

        @DexIgnore
        public String b() {
            return this.a;
        }

        @DexIgnore
        public String a() {
            return this.b;
        }
    }
}
