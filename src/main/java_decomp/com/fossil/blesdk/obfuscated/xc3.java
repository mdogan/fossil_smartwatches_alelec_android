package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xc3 implements Factory<HeartRateOverviewWeekPresenter> {
    @DexIgnore
    public static HeartRateOverviewWeekPresenter a(vc3 vc3, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new HeartRateOverviewWeekPresenter(vc3, userRepository, heartRateSummaryRepository);
    }
}
