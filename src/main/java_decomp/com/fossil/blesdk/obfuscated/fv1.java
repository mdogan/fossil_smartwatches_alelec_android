package com.fossil.blesdk.obfuscated;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fv1<E> extends ev1<E> implements ListIterator<E> {
    @DexIgnore
    @Deprecated
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }
}
