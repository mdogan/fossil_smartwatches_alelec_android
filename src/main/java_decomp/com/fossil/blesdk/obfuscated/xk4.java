package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.TimeoutCancellationException;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xk4 {
    @DexIgnore
    public static final <T, R> Object a(gg4<? super T> gg4, R r, kd4<? super R, ? super kc4<? super T>, ? extends Object> kd4) {
        Object obj;
        wd4.b(gg4, "$this$startUndispatchedOrReturn");
        wd4.b(kd4, "block");
        gg4.k();
        try {
            ce4.a((Object) kd4, 2);
            obj = kd4.invoke(r, gg4);
        } catch (Throwable th) {
            obj = new zg4(th, false, 2, (rd4) null);
        }
        if (obj == oc4.a()) {
            return oc4.a();
        }
        if (!gg4.b(obj, 4)) {
            return oc4.a();
        }
        Object d = gg4.d();
        if (!(d instanceof zg4)) {
            return yi4.b(d);
        }
        throw nk4.a(gg4, ((zg4) d).a);
    }

    @DexIgnore
    public static final <T, R> Object b(gg4<? super T> gg4, R r, kd4<? super R, ? super kc4<? super T>, ? extends Object> kd4) {
        Object obj;
        wd4.b(gg4, "$this$startUndispatchedOrReturnIgnoreTimeout");
        wd4.b(kd4, "block");
        gg4.k();
        boolean z = false;
        try {
            ce4.a((Object) kd4, 2);
            obj = kd4.invoke(r, gg4);
        } catch (Throwable th) {
            obj = new zg4(th, false, 2, (rd4) null);
        }
        if (obj == oc4.a()) {
            return oc4.a();
        }
        if (!gg4.b(obj, 4)) {
            return oc4.a();
        }
        Object d = gg4.d();
        if (!(d instanceof zg4)) {
            return yi4.b(d);
        }
        zg4 zg4 = (zg4) d;
        Throwable th2 = zg4.a;
        if (!(th2 instanceof TimeoutCancellationException) || ((TimeoutCancellationException) th2).coroutine != gg4) {
            z = true;
        }
        if (z) {
            throw nk4.a(gg4, zg4.a);
        } else if (!(obj instanceof zg4)) {
            return obj;
        } else {
            throw nk4.a(gg4, ((zg4) obj).a);
        }
    }

    @DexIgnore
    public static final <T> void a(jd4<? super kc4<? super T>, ? extends Object> jd4, kc4<? super T> kc4) {
        CoroutineContext context;
        Object b;
        wd4.b(jd4, "$this$startCoroutineUndispatched");
        wd4.b(kc4, "completion");
        uc4.a(kc4);
        try {
            context = kc4.getContext();
            b = ThreadContextKt.b(context, (Object) null);
            ce4.a((Object) jd4, 1);
            Object invoke = jd4.invoke(kc4);
            ThreadContextKt.a(context, b);
            if (invoke != oc4.a()) {
                Result.a aVar = Result.Companion;
                kc4.resumeWith(Result.m3constructorimpl(invoke));
            }
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(kd4<? super R, ? super kc4<? super T>, ? extends Object> kd4, R r, kc4<? super T> kc4) {
        CoroutineContext context;
        Object b;
        wd4.b(kd4, "$this$startCoroutineUndispatched");
        wd4.b(kc4, "completion");
        uc4.a(kc4);
        try {
            context = kc4.getContext();
            b = ThreadContextKt.b(context, (Object) null);
            ce4.a((Object) kd4, 2);
            Object invoke = kd4.invoke(r, kc4);
            ThreadContextKt.a(context, b);
            if (invoke != oc4.a()) {
                Result.a aVar = Result.Companion;
                kc4.resumeWith(Result.m3constructorimpl(invoke));
            }
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        }
    }
}
