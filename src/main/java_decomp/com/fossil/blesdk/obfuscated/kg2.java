package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kg2 extends jg2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ScrollView x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.cl_container, 1);
        A.put(R.id.rv_categories, 2);
        A.put(R.id.rv_watch_apps, 3);
        A.put(R.id.tv_selected_watchapps, 4);
        A.put(R.id.tv_watchapps_setting, 5);
        A.put(R.id.tv_permission_order, 6);
        A.put(R.id.tv_watchapps_permission, 7);
        A.put(R.id.tv_watchapps_detail, 8);
    }
    */

    @DexIgnore
    public kg2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 9, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kg2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[1], objArr[2], objArr[3], objArr[6], objArr[4], objArr[8], objArr[7], objArr[5]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
