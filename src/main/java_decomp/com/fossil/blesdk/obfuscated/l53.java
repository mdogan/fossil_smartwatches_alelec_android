package com.fossil.blesdk.obfuscated;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l53 extends w52<k53> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void e0();

    @DexIgnore
    void o(List<WeatherLocationWrapper> list);

    @DexIgnore
    void r(boolean z);
}
