package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qo2<T> extends ro2<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public qo2(int i, ServerError serverError, Throwable th, String str) {
        super((rd4) null);
        this.a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof qo2) {
                qo2 qo2 = (qo2) obj;
                if (!(this.a == qo2.a) || !wd4.a((Object) this.b, (Object) qo2.b) || !wd4.a((Object) this.c, (Object) qo2.c) || !wd4.a((Object) this.d, (Object) qo2.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a * 31;
        ServerError serverError = this.b;
        int i2 = 0;
        int hashCode = (i + (serverError != null ? serverError.hashCode() : 0)) * 31;
        Throwable th = this.c;
        int hashCode2 = (hashCode + (th != null ? th.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode2 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qo2(int i, ServerError serverError, Throwable th, String str, int i2, rd4 rd4) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str);
    }
}
