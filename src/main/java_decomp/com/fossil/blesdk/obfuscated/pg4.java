package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pg4<T> extends kc4<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ Object a(pg4 pg4, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return pg4.a(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    @DexIgnore
    Object a(T t, Object obj);

    @DexIgnore
    void a(gh4 gh4, T t);

    @DexIgnore
    void b(jd4<? super Throwable, cb4> jd4);

    @DexIgnore
    void b(Object obj);

    @DexIgnore
    boolean isActive();
}
