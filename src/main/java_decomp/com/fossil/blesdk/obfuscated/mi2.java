package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mi2 extends li2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public long r;

    /*
    static {
        t.put(R.id.iv_goal, 1);
        t.put(R.id.ftv_time, 2);
        t.put(R.id.ftv_no_time, 3);
        t.put(R.id.ftv_delete, 4);
        t.put(R.id.line, 5);
    }
    */

    @DexIgnore
    public mi2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 6, s, t));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    public mi2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[4], objArr[3], objArr[2], objArr[1], objArr[5]);
        this.r = -1;
        this.q = objArr[0];
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
