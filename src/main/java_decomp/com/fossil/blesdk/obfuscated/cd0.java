package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cd0 implements Comparator<Scope> {
    @DexIgnore
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((Scope) obj).H().compareTo(((Scope) obj2).H());
    }
}
