package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zo4 implements jp4 {
    @DexIgnore
    public /* final */ jp4 e;

    @DexIgnore
    public zo4(jp4 jp4) {
        if (jp4 != null) {
            this.e = jp4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public void a(vo4 vo4, long j) throws IOException {
        this.e.a(vo4, j);
    }

    @DexIgnore
    public lp4 b() {
        return this.e.b();
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public void flush() throws IOException {
        this.e.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.e.toString() + ")";
    }
}
