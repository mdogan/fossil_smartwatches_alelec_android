package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jm3 implements MembersInjector<UpdateFirmwareActivity> {
    @DexIgnore
    public static void a(UpdateFirmwareActivity updateFirmwareActivity, UpdateFirmwarePresenter updateFirmwarePresenter) {
        updateFirmwareActivity.B = updateFirmwarePresenter;
    }
}
