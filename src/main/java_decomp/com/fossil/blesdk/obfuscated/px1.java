package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.google.firebase.iid.zzal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class px1 extends qx1<Void> {
    @DexIgnore
    public px1(int i, int i2, Bundle bundle) {
        super(i, 2, bundle);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            a(null);
        } else {
            a(new zzal(4, "Invalid response to one way request"));
        }
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }
}
