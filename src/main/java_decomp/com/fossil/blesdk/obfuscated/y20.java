package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.music.TrackInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y20 extends t20<TrackInfo, cb4> {
    @DexIgnore
    public static /* final */ l20<TrackInfo>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<cb4>[] b; // = new m20[0];
    @DexIgnore
    public static /* final */ y20 c; // = new y20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<TrackInfo> {
        @DexIgnore
        public byte[] a(TrackInfo trackInfo) {
            wd4.b(trackInfo, "entries");
            return y20.c.a(trackInfo);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<TrackInfo> {
        @DexIgnore
        public byte[] a(TrackInfo trackInfo) {
            wd4.b(trackInfo, "entries");
            return y20.c.a(trackInfo);
        }
    }

    @DexIgnore
    public m20<cb4>[] b() {
        return b;
    }

    @DexIgnore
    public l20<TrackInfo>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(TrackInfo trackInfo) {
        return trackInfo.getData$blesdk_productionRelease();
    }
}
