package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fv0 {
    @DexIgnore
    public static /* final */ fv0 a; // = new hv0();
    @DexIgnore
    public static /* final */ fv0 b; // = new iv0();

    @DexIgnore
    public fv0() {
    }

    @DexIgnore
    public static fv0 a() {
        return a;
    }

    @DexIgnore
    public static fv0 b() {
        return b;
    }

    @DexIgnore
    public abstract void a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);
}
