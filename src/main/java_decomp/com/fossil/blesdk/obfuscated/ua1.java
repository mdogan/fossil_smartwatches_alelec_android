package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ua1 extends ab1 {
    @DexIgnore
    public /* final */ /* synthetic */ ra1 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ua1(ra1 ra1) {
        super(ra1, (sa1) null);
        this.f = ra1;
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new ta1(this.f, (sa1) null);
    }

    @DexIgnore
    public /* synthetic */ ua1(ra1 ra1, sa1 sa1) {
        this(ra1);
    }
}
