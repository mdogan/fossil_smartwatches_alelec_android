package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cv0 {
    @DexIgnore
    public volatile tv0 a;
    @DexIgnore
    public volatile zzbb b;

    /*
    static {
        gu0.b();
    }
    */

    @DexIgnore
    public final int a() {
        if (this.b != null) {
            return this.b.size();
        }
        if (this.a != null) {
            return this.a.f();
        }
        return 0;
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(4:7|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    public final tv0 a(tv0 tv0) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = tv0;
                    this.b = zzbb.zzfi;
                    this.a = tv0;
                    this.b = zzbb.zzfi;
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public final tv0 b(tv0 tv0) {
        tv0 tv02 = this.a;
        this.b = null;
        this.a = tv0;
        return tv02;
    }

    @DexIgnore
    public final zzbb b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                zzbb zzbb = this.b;
                return zzbb;
            }
            this.b = this.a == null ? zzbb.zzfi : this.a.d();
            zzbb zzbb2 = this.b;
            return zzbb2;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cv0)) {
            return false;
        }
        cv0 cv0 = (cv0) obj;
        tv0 tv0 = this.a;
        tv0 tv02 = cv0.a;
        return (tv0 == null && tv02 == null) ? b().equals(cv0.b()) : (tv0 == null || tv02 == null) ? tv0 != null ? tv0.equals(cv0.a(tv0.b())) : a(tv02.b()).equals(tv02) : tv0.equals(tv02);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }
}
