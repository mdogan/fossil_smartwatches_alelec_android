package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.sn;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class un extends ContextWrapper {
    @DexIgnore
    public static /* final */ zn<?, ?> k; // = new rn();
    @DexIgnore
    public /* final */ hq a;
    @DexIgnore
    public /* final */ Registry b;
    @DexIgnore
    public /* final */ aw c;
    @DexIgnore
    public /* final */ sn.a d;
    @DexIgnore
    public /* final */ List<rv<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, zn<?, ?>> f;
    @DexIgnore
    public /* final */ rp g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public sv j;

    @DexIgnore
    public un(Context context, hq hqVar, Registry registry, aw awVar, sn.a aVar, Map<Class<?>, zn<?, ?>> map, List<rv<Object>> list, rp rpVar, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = hqVar;
        this.b = registry;
        this.c = awVar;
        this.d = aVar;
        this.e = list;
        this.f = map;
        this.g = rpVar;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <T> zn<?, T> a(Class<T> cls) {
        zn<?, T> znVar = this.f.get(cls);
        if (znVar == null) {
            for (Map.Entry next : this.f.entrySet()) {
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    znVar = (zn) next.getValue();
                }
            }
        }
        return znVar == null ? k : znVar;
    }

    @DexIgnore
    public List<rv<Object>> b() {
        return this.e;
    }

    @DexIgnore
    public synchronized sv c() {
        if (this.j == null) {
            this.j = (sv) this.d.build().O();
        }
        return this.j;
    }

    @DexIgnore
    public rp d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        return this.i;
    }

    @DexIgnore
    public Registry f() {
        return this.b;
    }

    @DexIgnore
    public boolean g() {
        return this.h;
    }

    @DexIgnore
    public <X> dw<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public hq a() {
        return this.a;
    }
}
