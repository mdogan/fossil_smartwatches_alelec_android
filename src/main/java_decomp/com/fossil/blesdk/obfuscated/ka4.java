package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ka4<T, U> extends z84<U> implements x94<T> {
    @DexIgnore
    public /* final */ a94<T> e;

    @DexIgnore
    public ka4(a94<T> a94) {
        this.e = a94;
    }
}
