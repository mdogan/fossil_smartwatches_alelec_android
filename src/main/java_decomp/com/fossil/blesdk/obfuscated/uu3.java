package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.fv3;
import com.fossil.blesdk.obfuscated.iv3;
import com.squareup.okhttp.internal.http.RequestException;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.net.ProtocolException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uu3 {
    @DexIgnore
    public /* final */ hv3 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public iv3 d;
    @DexIgnore
    public vw3 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements fv3.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(int i, iv3 iv3, boolean z) {
            this.a = i;
            this.b = z;
        }

        @DexIgnore
        public kv3 a(iv3 iv3) throws IOException {
            if (this.a >= uu3.this.a.D().size()) {
                return uu3.this.a(iv3, this.b);
            }
            return uu3.this.a.D().get(this.a).a(new a(this.a + 1, iv3, this.b));
        }
    }

    @DexIgnore
    public uu3(hv3 hv3, iv3 iv3) {
        this.a = hv3.a();
        this.d = iv3;
    }

    @DexIgnore
    public kv3 a() throws IOException {
        synchronized (this) {
            if (!this.b) {
                this.b = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        try {
            this.a.i().a(this);
            kv3 a2 = a(false);
            if (a2 != null) {
                return a2;
            }
            throw new IOException("Canceled");
        } finally {
            this.a.i().b(this);
        }
    }

    @DexIgnore
    public final kv3 a(boolean z) throws IOException {
        return new a(0, this.d, z).a(this.d);
    }

    @DexIgnore
    public kv3 a(iv3 iv3, boolean z) throws IOException {
        jv3 a2 = iv3.a();
        if (a2 != null) {
            iv3.b g = iv3.g();
            gv3 contentType = a2.contentType();
            if (contentType != null) {
                g.b(GraphRequest.CONTENT_TYPE_HEADER, contentType.toString());
            }
            long contentLength = a2.contentLength();
            if (contentLength != -1) {
                g.b("Content-Length", Long.toString(contentLength));
                g.a("Transfer-Encoding");
            } else {
                g.b("Transfer-Encoding", "chunked");
                g.a("Content-Length");
            }
            iv3 = g.a();
        }
        this.e = new vw3(this.a, iv3, false, false, z, (xu3) null, (cx3) null, (bx3) null, (kv3) null);
        int i = 0;
        while (!this.c) {
            try {
                this.e.n();
                this.e.l();
                kv3 g2 = this.e.g();
                iv3 d2 = this.e.d();
                if (d2 == null) {
                    if (!z) {
                        this.e.m();
                    }
                    return g2;
                }
                i++;
                if (i <= 20) {
                    if (!this.e.a(d2.d())) {
                        this.e.m();
                    }
                    this.e = new vw3(this.a, d2, false, false, z, this.e.a(), (cx3) null, (bx3) null, g2);
                } else {
                    throw new ProtocolException("Too many follow-up requests: " + i);
                }
            } catch (RequestException e2) {
                throw e2.getCause();
            } catch (RouteException e3) {
                vw3 b2 = this.e.b(e3);
                if (b2 != null) {
                    this.e = b2;
                } else {
                    throw e3.getLastConnectException();
                }
            } catch (IOException e4) {
                vw3 a3 = this.e.a(e4, (jp4) null);
                if (a3 != null) {
                    this.e = a3;
                } else {
                    throw e4;
                }
            }
        }
        this.e.m();
        throw new IOException("Canceled");
    }
}
