package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface h11 extends IInterface {
    @DexIgnore
    void c(Status status) throws RemoteException;
}
