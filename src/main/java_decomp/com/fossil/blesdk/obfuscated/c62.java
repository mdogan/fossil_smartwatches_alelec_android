package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c62 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public c62(fn2 fn2, UserRepository userRepository, HybridPresetDao hybridPresetDao, DeviceDao deviceDao, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp) {
        wd4.b(fn2, "mSharedPrefs");
        wd4.b(userRepository, "mUserRepository");
        wd4.b(hybridPresetDao, "mHybridPresetDao");
        wd4.b(deviceDao, "mDeviceDao");
        wd4.b(notificationsRepository, "mNotificationsRepository");
        wd4.b(portfolioApp, "mApp");
    }
}
