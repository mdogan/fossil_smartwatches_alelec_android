package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zh4 {
    @DexIgnore
    public static /* final */ gh4 a; // = fh4.a();
    @DexIgnore
    public static /* final */ gh4 b; // = zk4.k.D();

    /*
    static {
        new zh4();
        rj4 rj4 = rj4.e;
    }
    */

    @DexIgnore
    public static final gh4 a() {
        return a;
    }

    @DexIgnore
    public static final gh4 b() {
        return b;
    }

    @DexIgnore
    public static final bj4 c() {
        return hk4.b;
    }
}
