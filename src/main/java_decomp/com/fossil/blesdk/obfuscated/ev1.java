package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ev1<E> implements Iterator<E> {
    @DexIgnore
    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
