package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h64 implements b64<j64>, g64, j64 {
    @DexIgnore
    public /* final */ List<j64> e; // = new ArrayList();
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicReference<Throwable> g; // = new AtomicReference<>((Object) null);

    @DexIgnore
    public boolean b() {
        for (j64 a : c()) {
            if (!a.a()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public synchronized Collection<j64> c() {
        return Collections.unmodifiableCollection(this.e);
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return Priority.compareTo(this, obj);
    }

    @DexIgnore
    public Priority getPriority() {
        return Priority.NORMAL;
    }

    @DexIgnore
    public synchronized void a(j64 j64) {
        this.e.add(j64);
    }

    @DexIgnore
    public static boolean b(Object obj) {
        try {
            b64 b64 = (b64) obj;
            j64 j64 = (j64) obj;
            g64 g64 = (g64) obj;
            if (b64 == null || j64 == null || g64 == null) {
                return false;
            }
            return true;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public synchronized void a(boolean z) {
        this.f.set(z);
    }

    @DexIgnore
    public boolean a() {
        return this.f.get();
    }

    @DexIgnore
    public void a(Throwable th) {
        this.g.set(th);
    }
}
