package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dk0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dk0> CREATOR; // = new ll0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ Account f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ GoogleSignInAccount h;

    @DexIgnore
    public dk0(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.e = i;
        this.f = account;
        this.g = i2;
        this.h = googleSignInAccount;
    }

    @DexIgnore
    public Account H() {
        return this.f;
    }

    @DexIgnore
    public int I() {
        return this.g;
    }

    @DexIgnore
    public GoogleSignInAccount J() {
        return this.h;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, (Parcelable) H(), i, false);
        lk0.a(parcel, 3, I());
        lk0.a(parcel, 4, (Parcelable) J(), i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public dk0(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
