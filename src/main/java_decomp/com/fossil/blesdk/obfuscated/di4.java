package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class di4 implements mi4 {
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    public di4(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public cj4 a() {
        return null;
    }

    @DexIgnore
    public boolean isActive() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(isActive() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
