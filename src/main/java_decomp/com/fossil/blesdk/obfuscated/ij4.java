package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ij4 {
    @DexIgnore
    public static final <T> void a(kc4<? super T> kc4, T t, int i) {
        wd4.b(kc4, "$this$resumeMode");
        if (i == 0) {
            Result.a aVar = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 1) {
            xh4.a(kc4, t);
        } else if (i == 2) {
            xh4.b(kc4, t);
        } else if (i == 3) {
            vh4 vh4 = (vh4) kc4;
            CoroutineContext context = vh4.getContext();
            Object b = ThreadContextKt.b(context, vh4.j);
            try {
                kc4<T> kc42 = vh4.l;
                Result.a aVar2 = Result.Companion;
                kc42.resumeWith(Result.m3constructorimpl(t));
                cb4 cb4 = cb4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean a(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final <T> void b(kc4<? super T> kc4, Throwable th, int i) {
        wd4.b(kc4, "$this$resumeWithExceptionMode");
        wd4.b(th, "exception");
        if (i == 0) {
            Result.a aVar = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        } else if (i == 1) {
            xh4.a(kc4, th);
        } else if (i == 2) {
            xh4.b(kc4, th);
        } else if (i == 3) {
            vh4 vh4 = (vh4) kc4;
            CoroutineContext context = vh4.getContext();
            Object b = ThreadContextKt.b(context, vh4.j);
            try {
                kc4<T> kc42 = vh4.l;
                Result.a aVar2 = Result.Companion;
                kc42.resumeWith(Result.m3constructorimpl(za4.a(ok4.a(th, (kc4<?>) kc42))));
                cb4 cb4 = cb4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void a(kc4<? super T> kc4, Throwable th, int i) {
        wd4.b(kc4, "$this$resumeUninterceptedWithExceptionMode");
        wd4.b(th, "exception");
        if (i == 0) {
            kc4<? super T> a = IntrinsicsKt__IntrinsicsJvmKt.a(kc4);
            Result.a aVar = Result.Companion;
            a.resumeWith(Result.m3constructorimpl(za4.a(th)));
        } else if (i == 1) {
            xh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), th);
        } else if (i == 2) {
            Result.a aVar2 = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        } else if (i == 3) {
            CoroutineContext context = kc4.getContext();
            Object b = ThreadContextKt.b(context, (Object) null);
            try {
                Result.a aVar3 = Result.Companion;
                kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
                cb4 cb4 = cb4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final <T> void b(kc4<? super T> kc4, T t, int i) {
        wd4.b(kc4, "$this$resumeUninterceptedMode");
        if (i == 0) {
            kc4<? super T> a = IntrinsicsKt__IntrinsicsJvmKt.a(kc4);
            Result.a aVar = Result.Companion;
            a.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 1) {
            xh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), t);
        } else if (i == 2) {
            Result.a aVar2 = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 3) {
            CoroutineContext context = kc4.getContext();
            Object b = ThreadContextKt.b(context, (Object) null);
            try {
                Result.a aVar3 = Result.Companion;
                kc4.resumeWith(Result.m3constructorimpl(t));
                cb4 cb4 = cb4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }
}
