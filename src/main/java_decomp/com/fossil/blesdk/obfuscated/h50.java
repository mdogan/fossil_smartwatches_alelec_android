package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.DeviceConfigFileFormat;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import kotlin.Pair;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h50 extends GetFilePhase {
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public HashMap<DeviceConfigKey, DeviceConfigItem> S;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h50(Peripheral peripheral, Phase.a aVar, short s, int i, rd4 rd4) {
        this(peripheral, aVar, (i & 4) != 0 ? a50.b.a(peripheral.k(), FileType.DEVICE_CONFIG) : s);
    }

    @DexIgnore
    public void a(ArrayList<DeviceFile> arrayList) {
        wd4.b(arrayList, "filesData");
        a(k());
    }

    @DexIgnore
    public boolean c() {
        return this.R;
    }

    @DexIgnore
    public JSONObject x() {
        JSONArray jSONArray;
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.CONFIGS;
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = this.S;
        if (hashMap != null) {
            Collection<DeviceConfigItem> values = hashMap.values();
            if (values != null) {
                Object[] array = values.toArray(new DeviceConfigItem[0]);
                if (array != null) {
                    DeviceConfigItem[] deviceConfigItemArr = (DeviceConfigItem[]) array;
                    if (deviceConfigItemArr != null) {
                        jSONArray = k00.a(deviceConfigItemArr);
                        return xa0.a(x, jSONKey, jSONArray);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        }
        jSONArray = null;
        return xa0.a(x, jSONKey, jSONArray);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public h50(Peripheral peripheral, Phase.a aVar, short s) {
        super(r2, r3, PhaseId.GET_DEVICE_CONFIGS, r5, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, true), ab4.a(GetFilePhase.GetFileOption.NUMBER_OF_FILE_REQUIRED, 1), ab4.a(GetFilePhase.GetFileOption.ERASE_CACHE_FILE_BEFORE_GET, true)}), 1.0f, (String) null, 64, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        Peripheral peripheral2 = peripheral;
        Phase.a aVar2 = aVar;
        short s2 = s;
        this.R = true;
    }

    @DexIgnore
    public void c(DeviceFile deviceFile) {
        Phase.Result.ResultCode resultCode;
        wd4.b(deviceFile, "deviceFile");
        super.c(deviceFile);
        try {
            this.S = (HashMap) DeviceConfigFileFormat.e.a(deviceFile.getRawData());
            resultCode = Phase.Result.ResultCode.SUCCESS;
        } catch (FileFormatException e) {
            ea0.l.a(e);
            resultCode = Phase.Result.ResultCode.INCORRECT_FILE_DATA;
        }
        b(Phase.Result.copy$default(k(), (PhaseId) null, resultCode, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public HashMap<DeviceConfigKey, DeviceConfigItem> i() {
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = this.S;
        return hashMap != null ? hashMap : new HashMap<>();
    }
}
