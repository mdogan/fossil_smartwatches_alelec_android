package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class um3 {
    @DexIgnore
    public tm3 a;

    @DexIgnore
    public um3(tm3 tm3) {
        wd4.b(tm3, "mPairingInstructionsView");
        this.a = tm3;
    }

    @DexIgnore
    public final tm3 a() {
        return this.a;
    }
}
