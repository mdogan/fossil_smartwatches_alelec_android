package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zt1<K, V> implements qu1<K, V> {
    @DexIgnore
    public transient Collection<Map.Entry<K, V>> e;
    @DexIgnore
    public transient Set<K> f;
    @DexIgnore
    public transient su1<K> g;
    @DexIgnore
    public transient Collection<V> h;
    @DexIgnore
    public transient Map<K, Collection<V>> i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Multimaps.a<K, V> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public qu1<K, V> a() {
            return zt1.this;
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return zt1.this.entryIterator();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends zt1<K, V>.b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public c(zt1 zt1) {
            super();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return yu1.a((Set<?>) this, obj);
        }

        @DexIgnore
        public int hashCode() {
            return yu1.a((Set<?>) this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AbstractCollection<V> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void clear() {
            zt1.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return zt1.this.containsValue(obj);
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return zt1.this.valueIterator();
        }

        @DexIgnore
        public int size() {
            return zt1.this.size();
        }
    }

    @DexIgnore
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> map = this.i;
        if (map != null) {
            return map;
        }
        Map<K, Collection<V>> createAsMap = createAsMap();
        this.i = createAsMap;
        return createAsMap;
    }

    @DexIgnore
    public boolean containsEntry(Object obj, Object obj2) {
        Collection collection = (Collection) asMap().get(obj);
        return collection != null && collection.contains(obj2);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        for (Collection contains : asMap().values()) {
            if (contains.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> createAsMap();

    @DexIgnore
    public Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof xu1) {
            return new c();
        }
        return new b();
    }

    @DexIgnore
    public Set<K> createKeySet() {
        return new Maps.d(asMap());
    }

    @DexIgnore
    public su1<K> createKeys() {
        return new Multimaps.b(this);
    }

    @DexIgnore
    public Collection<V> createValues() {
        return new d();
    }

    @DexIgnore
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> collection = this.e;
        if (collection != null) {
            return collection;
        }
        Collection<Map.Entry<K, V>> createEntries = createEntries();
        this.e = createEntries;
        return createEntries;
    }

    @DexIgnore
    public abstract Iterator<Map.Entry<K, V>> entryIterator();

    @DexIgnore
    public boolean equals(Object obj) {
        return Multimaps.a((qu1<?, ?>) this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return asMap().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public Set<K> keySet() {
        Set<K> set = this.f;
        if (set != null) {
            return set;
        }
        Set<K> createKeySet = createKeySet();
        this.f = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    public su1<K> keys() {
        su1<K> su1 = this.g;
        if (su1 != null) {
            return su1;
        }
        su1<K> createKeys = createKeys();
        this.g = createKeys;
        return createKeys;
    }

    @DexIgnore
    public abstract boolean put(K k, V v);

    @DexIgnore
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        tt1.a(iterable);
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.isEmpty() || !get(k).addAll(collection)) {
                return false;
            }
            return true;
        }
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext() || !mu1.a(get(k), it)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean remove(Object obj, Object obj2) {
        Collection collection = (Collection) asMap().get(obj);
        return collection != null && collection.remove(obj2);
    }

    @DexIgnore
    public abstract Collection<V> replaceValues(K k, Iterable<? extends V> iterable);

    @DexIgnore
    public String toString() {
        return asMap().toString();
    }

    @DexIgnore
    public abstract Iterator<V> valueIterator();

    @DexIgnore
    public Collection<V> values() {
        Collection<V> collection = this.h;
        if (collection != null) {
            return collection;
        }
        Collection<V> createValues = createValues();
        this.h = createValues;
        return createValues;
    }

    @DexIgnore
    public boolean putAll(qu1<? extends K, ? extends V> qu1) {
        boolean z = false;
        for (Map.Entry next : qu1.entries()) {
            z |= put(next.getKey(), next.getValue());
        }
        return z;
    }
}
