package com.fossil.blesdk.obfuscated;

import android.app.job.JobParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class rk1 implements Runnable {
    @DexIgnore
    public /* final */ ok1 e;
    @DexIgnore
    public /* final */ ug1 f;
    @DexIgnore
    public /* final */ JobParameters g;

    @DexIgnore
    public rk1(ok1 ok1, ug1 ug1, JobParameters jobParameters) {
        this.e = ok1;
        this.f = ug1;
        this.g = jobParameters;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
