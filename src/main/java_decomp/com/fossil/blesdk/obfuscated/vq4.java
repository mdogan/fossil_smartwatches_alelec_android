package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vq4 implements nq4 {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ Map<String, uq4> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<sq4> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    public synchronized oq4 a(String str) {
        uq4 uq4;
        uq4 = this.b.get(str);
        if (uq4 == null) {
            uq4 = new uq4(str, this.c, this.a);
            this.b.put(str, uq4);
        }
        return uq4;
    }

    @DexIgnore
    public LinkedBlockingQueue<sq4> b() {
        return this.c;
    }

    @DexIgnore
    public List<uq4> c() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void d() {
        this.a = true;
    }

    @DexIgnore
    public void a() {
        this.b.clear();
        this.c.clear();
    }
}
