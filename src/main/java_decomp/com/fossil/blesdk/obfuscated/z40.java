package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileAction;
import com.fossil.blesdk.device.data.file.FileType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class z40 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[FileAction.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[FileType.values().length];

    /*
    static {
        a[FileAction.GET.ordinal()] = 1;
        a[FileAction.PUT.ordinal()] = 2;
        b[FileType.OTA.ordinal()] = 1;
        b[FileType.ACTIVITY_FILE.ordinal()] = 2;
        b[FileType.DEVICE_INFO.ordinal()] = 3;
        b[FileType.HARDWARE_LOG.ordinal()] = 4;
        b[FileType.FONT.ordinal()] = 5;
        b[FileType.ALL_FILE.ordinal()] = 6;
        b[FileType.ASSET.ordinal()] = 7;
        b[FileType.DEVICE_CONFIG.ordinal()] = 8;
        b[FileType.ALARM.ordinal()] = 9;
        b[FileType.NOTIFICATION_FILTER.ordinal()] = 10;
        b[FileType.MUSIC_CONTROL.ordinal()] = 11;
        b[FileType.MICRO_APP.ordinal()] = 12;
        b[FileType.WATCH_PARAMETERS_FILE.ordinal()] = 13;
        b[FileType.UI_PACKAGE_FILE.ordinal()] = 14;
        b[FileType.NOTIFICATION.ordinal()] = 15;
        b[FileType.UI_SCRIPT.ordinal()] = 16;
        b[FileType.LUTS_FILE.ordinal()] = 17;
        b[FileType.RATE_FILE.ordinal()] = 18;
        b[FileType.DATA_COLLECTION_FILE.ordinal()] = 19;
    }
    */
}
