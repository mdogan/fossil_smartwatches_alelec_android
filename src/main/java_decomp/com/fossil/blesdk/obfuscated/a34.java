package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.zendesk.belvedere.Belvedere;
import com.zendesk.belvedere.BelvedereSource;
import java.util.Arrays;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a34 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public e34 g;
    @DexIgnore
    public TreeSet<BelvedereSource> h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public Context a;
        @DexIgnore
        public String b; // = "belvedere-data";
        @DexIgnore
        public int c; // = FailureCode.USER_CANCELLED_BUT_USER_DID_NOT_SELECT_ANY_DEVICE;
        @DexIgnore
        public int d; // = FailureCode.USER_CANCELLED;
        @DexIgnore
        public int e; // = 1653;
        @DexIgnore
        public boolean f; // = true;
        @DexIgnore
        public String g; // = "*/*";
        @DexIgnore
        public e34 h; // = new h34();
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public TreeSet<BelvedereSource> j; // = new TreeSet<>(Arrays.asList(new BelvedereSource[]{BelvedereSource.Camera, BelvedereSource.Gallery}));

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public a a(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public a b(boolean z) {
            this.i = z;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public a a(e34 e34) {
            if (e34 != null) {
                this.h = e34;
                return this;
            }
            throw new IllegalArgumentException("Invalid logger provided");
        }

        @DexIgnore
        public Belvedere a() {
            this.h.setLoggable(this.i);
            return new Belvedere(this.a, new a34(this));
        }
    }

    @DexIgnore
    public a34(a aVar) {
        this.a = aVar.b;
        this.b = aVar.c;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.j;
    }

    @DexIgnore
    public boolean a() {
        return this.e;
    }

    @DexIgnore
    public e34 b() {
        return this.g;
    }

    @DexIgnore
    public TreeSet<BelvedereSource> c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public String f() {
        return this.f;
    }

    @DexIgnore
    public String g() {
        return this.a;
    }

    @DexIgnore
    public int h() {
        return this.b;
    }
}
