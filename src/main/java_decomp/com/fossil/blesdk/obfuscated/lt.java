package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lt implements bq<byte[]> {
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore
    public lt(byte[] bArr) {
        uw.a(bArr);
        this.e = bArr;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public int b() {
        return this.e.length;
    }

    @DexIgnore
    public Class<byte[]> c() {
        return byte[].class;
    }

    @DexIgnore
    public byte[] get() {
        return this.e;
    }
}
