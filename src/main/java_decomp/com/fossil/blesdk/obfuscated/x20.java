package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.NotificationFilter;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x20 extends t20<NotificationFilter[], NotificationFilter[]> {
    @DexIgnore
    public static /* final */ l20<NotificationFilter[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<NotificationFilter[]>[] b; // = new m20[0];
    @DexIgnore
    public static /* final */ x20 c; // = new x20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<NotificationFilter[]> {
        @DexIgnore
        public byte[] a(NotificationFilter[] notificationFilterArr) {
            wd4.b(notificationFilterArr, "entries");
            return x20.c.a(notificationFilterArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<NotificationFilter[]> {
        @DexIgnore
        public byte[] a(NotificationFilter[] notificationFilterArr) {
            wd4.b(notificationFilterArr, "entries");
            return x20.c.a(notificationFilterArr);
        }
    }

    @DexIgnore
    public m20<NotificationFilter[]>[] b() {
        return b;
    }

    @DexIgnore
    public l20<NotificationFilter[]>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(NotificationFilter[] notificationFilterArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (NotificationFilter buildNotificationFilterData$blesdk_productionRelease : notificationFilterArr) {
            byteArrayOutputStream.write(buildNotificationFilterData$blesdk_productionRelease.buildNotificationFilterData$blesdk_productionRelease());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wd4.a((Object) byteArray, "entriesData.toByteArray()");
        return byteArray;
    }
}
