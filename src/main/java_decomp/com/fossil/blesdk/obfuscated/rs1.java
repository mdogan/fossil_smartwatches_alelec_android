package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rs1 extends SparseArray<Parcelable> implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rs1> CREATOR; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.ClassLoaderCreator<rs1> {
        @DexIgnore
        public rs1[] newArray(int i) {
            return new rs1[i];
        }

        @DexIgnore
        public rs1 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new rs1(parcel, classLoader);
        }

        @DexIgnore
        public rs1 createFromParcel(Parcel parcel) {
            return new rs1(parcel, (ClassLoader) null);
        }
    }

    @DexIgnore
    public rs1() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = keyAt(i2);
            parcelableArr[i2] = (Parcelable) valueAt(i2);
        }
        parcel.writeInt(size);
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }

    @DexIgnore
    public rs1(Parcel parcel, ClassLoader classLoader) {
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }
}
