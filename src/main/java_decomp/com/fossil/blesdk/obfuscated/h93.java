package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h93 extends as2 implements g93 {
    @DexIgnore
    public ur3<f92> j;
    @DexIgnore
    public f93 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void b(xr2 xr2, ArrayList<String> arrayList) {
        wd4.b(xr2, "baseModel");
        wd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewDayFragment", "showDayDetails - baseModel=" + xr2);
        ur3<f92> ur3 = this.j;
        if (ur3 != null) {
            f92 a2 = ur3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.q;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ml2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onCreateView");
        f92 f92 = (f92) ra.a(layoutInflater, R.layout.fragment_activity_overview_day, viewGroup, false, O0());
        f92.r.setOnClickListener(b.e);
        this.j = new ur3<>(this, f92);
        ur3<f92> ur3 = this.j;
        if (ur3 != null) {
            f92 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onResume");
        f93 f93 = this.k;
        if (f93 != null) {
            f93.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onStop");
        f93 f93 = this.k;
        if (f93 != null) {
            f93.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        wd4.b(list, "workoutSessions");
        ur3<f92> ur3 = this.j;
        if (ur3 != null) {
            f92 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.u;
                wd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    zg2 zg2 = a2.t;
                    if (zg2 != null) {
                        View d = zg2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                zg2 zg22 = a2.t;
                if (zg22 != null) {
                    View d2 = zg22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            wd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(f93 f93) {
        wd4.b(f93, "presenter");
        this.k = f93;
    }

    @DexIgnore
    public final void a(zg2 zg2, WorkoutSession workoutSession) {
        if (zg2 != null) {
            View d = zg2.d();
            wd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = tm2.a(context, a2.getSecond().intValue());
            zg2.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = zg2.r;
            wd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = zg2.s;
            wd4.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            be4 be4 = be4.a;
            String a4 = tm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Text__NumberSteps);
            wd4.a((Object) a4, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
            Object[] objArr = new Object[1];
            Integer totalSteps = workoutSession.getTotalSteps();
            objArr[0] = jl2.b(totalSteps != null ? (float) totalSteps.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1);
            String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = zg2.q;
            wd4.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(sk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
