package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bh4 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ ej4 c;

    @DexIgnore
    public bh4(Object obj, Object obj2, ej4 ej4) {
        wd4.b(ej4, "token");
        this.a = obj;
        this.b = obj2;
        this.c = ej4;
    }

    @DexIgnore
    public String toString() {
        return "CompletedIdempotentResult[" + this.b + ']';
    }
}
