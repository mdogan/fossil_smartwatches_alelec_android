package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o63 implements MembersInjector<n63> {
    @DexIgnore
    public static void a(n63 n63, MicroAppPresenter microAppPresenter) {
        n63.o = microAppPresenter;
    }

    @DexIgnore
    public static void a(n63 n63, k42 k42) {
        n63.p = k42;
    }
}
