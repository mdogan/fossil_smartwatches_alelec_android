package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.mj4;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jh4 extends jc4 implements mj4<String> {
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ long e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineContext.b<jh4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public jh4(long j) {
        super(f);
        this.e = j;
    }

    @DexIgnore
    public final long C() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof jh4) {
                if (this.e == ((jh4) obj).e) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public <R> R fold(R r, kd4<? super R, ? super CoroutineContext.a, ? extends R> kd4) {
        wd4.b(kd4, "operation");
        return mj4.a.a(this, r, kd4);
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        wd4.b(bVar, "key");
        return mj4.a.a(this, bVar);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.e;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        wd4.b(bVar, "key");
        return mj4.a.b(this, bVar);
    }

    @DexIgnore
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        return mj4.a.a(this, coroutineContext);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.e + ')';
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r9 != null) goto L_0x0018;
     */
    @DexIgnore
    public String a(CoroutineContext coroutineContext) {
        String str;
        wd4.b(coroutineContext, "context");
        kh4 kh4 = (kh4) coroutineContext.get(kh4.f);
        if (kh4 != null) {
            str = kh4.C();
        }
        str = "coroutine";
        Thread currentThread = Thread.currentThread();
        wd4.a((Object) currentThread, "currentThread");
        String name = currentThread.getName();
        wd4.a((Object) name, "oldName");
        int b = StringsKt__StringsKt.b((CharSequence) name, " @", 0, false, 6, (Object) null);
        if (b < 0) {
            b = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + b + 10);
        String substring = name.substring(0, b);
        wd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb.append(substring);
        sb.append(" @");
        sb.append(str);
        sb.append('#');
        sb.append(this.e);
        String sb2 = sb.toString();
        wd4.a((Object) sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
        currentThread.setName(sb2);
        return name;
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, String str) {
        wd4.b(coroutineContext, "context");
        wd4.b(str, "oldState");
        Thread currentThread = Thread.currentThread();
        wd4.a((Object) currentThread, "Thread.currentThread()");
        currentThread.setName(str);
    }
}
