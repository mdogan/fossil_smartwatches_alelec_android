package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ze extends m8 {
    @DexIgnore
    public /* final */ RecyclerView c;
    @DexIgnore
    public /* final */ m8 d; // = new a(this);

    @DexIgnore
    public ze(RecyclerView recyclerView) {
        this.c = recyclerView;
    }

    @DexIgnore
    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (c() || this.c.getLayoutManager() == null) {
            return false;
        }
        return this.c.getLayoutManager().a(i, bundle);
    }

    @DexIgnore
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !c()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    @DexIgnore
    public boolean c() {
        return this.c.p();
    }

    @DexIgnore
    public void a(View view, r9 r9Var) {
        super.a(view, r9Var);
        r9Var.a((CharSequence) RecyclerView.class.getName());
        if (!c() && this.c.getLayoutManager() != null) {
            this.c.getLayoutManager().a(r9Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends m8 {
        @DexIgnore
        public /* final */ ze c;

        @DexIgnore
        public a(ze zeVar) {
            this.c = zeVar;
        }

        @DexIgnore
        public void a(View view, r9 r9Var) {
            super.a(view, r9Var);
            if (!this.c.c() && this.c.c.getLayoutManager() != null) {
                this.c.c.getLayoutManager().a(view, r9Var);
            }
        }

        @DexIgnore
        public boolean a(View view, int i, Bundle bundle) {
            if (super.a(view, i, bundle)) {
                return true;
            }
            if (this.c.c() || this.c.c.getLayoutManager() == null) {
                return false;
            }
            return this.c.c.getLayoutManager().a(view, i, bundle);
        }
    }

    @DexIgnore
    public m8 b() {
        return this.d;
    }
}
