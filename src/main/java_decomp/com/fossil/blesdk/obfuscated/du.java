package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class du implements hu<Bitmap, byte[]> {
    @DexIgnore
    public /* final */ Bitmap.CompressFormat a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public du() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    public bq<byte[]> a(bq<Bitmap> bqVar, mo moVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bqVar.get().compress(this.a, this.b, byteArrayOutputStream);
        bqVar.a();
        return new lt(byteArrayOutputStream.toByteArray());
    }

    @DexIgnore
    public du(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }
}
