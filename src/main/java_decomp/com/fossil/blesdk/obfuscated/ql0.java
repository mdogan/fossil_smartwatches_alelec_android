package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ql0 implements Parcelable.Creator<oj0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v6, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        xd0[] xd0Arr = null;
        xd0[] xd0Arr2 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 2:
                    i2 = SafeParcelReader.q(parcel2, a);
                    break;
                case 3:
                    i3 = SafeParcelReader.q(parcel2, a);
                    break;
                case 4:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 5:
                    iBinder = SafeParcelReader.p(parcel2, a);
                    break;
                case 6:
                    scopeArr = SafeParcelReader.b(parcel2, a, Scope.CREATOR);
                    break;
                case 7:
                    bundle = SafeParcelReader.a(parcel2, a);
                    break;
                case 8:
                    account = SafeParcelReader.a(parcel2, a, Account.CREATOR);
                    break;
                case 10:
                    xd0Arr = SafeParcelReader.b(parcel2, a, xd0.CREATOR);
                    break;
                case 11:
                    xd0Arr2 = SafeParcelReader.b(parcel2, a, xd0.CREATOR);
                    break;
                case 12:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new oj0(i, i2, i3, str, iBinder, scopeArr, bundle, account, xd0Arr, xd0Arr2, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new oj0[i];
    }
}
