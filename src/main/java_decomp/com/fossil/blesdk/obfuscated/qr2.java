package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qr2 implements Factory<UpdateUser> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public qr2(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static qr2 a(Provider<UserRepository> provider) {
        return new qr2(provider);
    }

    @DexIgnore
    public static UpdateUser b(Provider<UserRepository> provider) {
        return new UpdateUser(provider.get());
    }

    @DexIgnore
    public UpdateUser get() {
        return b(this.a);
    }
}
