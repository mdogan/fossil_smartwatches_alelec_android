package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z33 implements MembersInjector<ComplicationSearchActivity> {
    @DexIgnore
    public static void a(ComplicationSearchActivity complicationSearchActivity, ComplicationSearchPresenter complicationSearchPresenter) {
        complicationSearchActivity.B = complicationSearchPresenter;
    }
}
