package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xp2 implements Factory<HybridMessageNotificationComponent> {
    @DexIgnore
    public static /* final */ xp2 a; // = new xp2();

    @DexIgnore
    public static xp2 a() {
        return a;
    }

    @DexIgnore
    public static HybridMessageNotificationComponent b() {
        return new HybridMessageNotificationComponent();
    }

    @DexIgnore
    public HybridMessageNotificationComponent get() {
        return b();
    }
}
