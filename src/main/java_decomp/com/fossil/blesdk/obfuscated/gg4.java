package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.VideoUploader;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gg4<T> extends xi4 implements ri4, kc4<T>, lh4 {
    @DexIgnore
    public /* final */ CoroutineContext f; // = this.g.plus(this);
    @DexIgnore
    public /* final */ CoroutineContext g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gg4(CoroutineContext coroutineContext, boolean z) {
        super(z);
        wd4.b(coroutineContext, "parentContext");
        this.g = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.f;
    }

    @DexIgnore
    public void a(Throwable th, boolean z) {
        wd4.b(th, "cause");
    }

    @DexIgnore
    public final <R> void a(CoroutineStart coroutineStart, R r, kd4<? super R, ? super kc4<? super T>, ? extends Object> kd4) {
        wd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wd4.b(kd4, "block");
        k();
        coroutineStart.invoke(kd4, r, this);
    }

    @DexIgnore
    public final void f(Throwable th) {
        wd4.b(th, "exception");
        ih4.a(this.f, th);
    }

    @DexIgnore
    public final void g(Object obj) {
        if (obj instanceof zg4) {
            zg4 zg4 = (zg4) obj;
            a(zg4.a, zg4.a());
            return;
        }
        j(obj);
    }

    @DexIgnore
    public final CoroutineContext getContext() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        l();
    }

    @DexIgnore
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    public int j() {
        return 0;
    }

    @DexIgnore
    public void j(T t) {
    }

    @DexIgnore
    public final void k() {
        a((ri4) this.g.get(ri4.d));
    }

    @DexIgnore
    public void l() {
    }

    @DexIgnore
    public final void resumeWith(Object obj) {
        b(ah4.a(obj), j());
    }

    @DexIgnore
    public String g() {
        String a = fh4.a(this.f);
        if (a == null) {
            return super.g();
        }
        return '\"' + a + "\":" + super.g();
    }
}
