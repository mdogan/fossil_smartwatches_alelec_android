package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.ne0;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh0<R extends ne0> extends re0<R> implements oe0<R> {
    @DexIgnore
    public qe0<? super R, ? extends ne0> a;
    @DexIgnore
    public nh0<? extends ne0> b;
    @DexIgnore
    public volatile pe0<? super R> c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Status e;
    @DexIgnore
    public /* final */ WeakReference<he0> f;
    @DexIgnore
    public /* final */ ph0 g;

    @DexIgnore
    public final void a(Status status) {
        synchronized (this.d) {
            this.e = status;
            b(this.e);
        }
    }

    @DexIgnore
    public final void b(Status status) {
        synchronized (this.d) {
            if (this.a != null) {
                Status a2 = this.a.a(status);
                ck0.a(a2, (Object) "onFailure must not return null");
                this.b.a(a2);
            } else if (b()) {
                this.c.a(status);
            }
        }
    }

    @DexIgnore
    public final void onResult(R r) {
        synchronized (this.d) {
            if (!r.G().L()) {
                a(r.G());
                a((ne0) r);
            } else if (this.a != null) {
                hh0.a().submit(new oh0(this, r));
            } else if (b()) {
                this.c.a(r);
            }
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }

    @DexIgnore
    public static void a(ne0 ne0) {
        if (ne0 instanceof ke0) {
            try {
                ((ke0) ne0).a();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(ne0);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return (this.c == null || ((he0) this.f.get()) == null) ? false : true;
    }
}
