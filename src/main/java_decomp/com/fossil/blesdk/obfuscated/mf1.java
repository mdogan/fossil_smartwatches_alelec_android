package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ak0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mf1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mf1> CREATOR; // = new tf1();
    @DexIgnore
    public /* final */ float e;
    @DexIgnore
    public /* final */ float f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final a a(float f) {
            this.a = f;
            return this;
        }

        @DexIgnore
        public final a b(float f) {
            this.b = f;
            return this;
        }

        @DexIgnore
        public final mf1 a() {
            return new mf1(this.b, this.a);
        }
    }

    @DexIgnore
    public mf1(float f2, float f3) {
        boolean z = -90.0f <= f2 && f2 <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f2);
        ck0.a(z, (Object) sb.toString());
        this.e = f2 + LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = (((double) f3) <= 0.0d ? (f3 % 360.0f) + 360.0f : f3) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof mf1)) {
            return false;
        }
        mf1 mf1 = (mf1) obj;
        return Float.floatToIntBits(this.e) == Float.floatToIntBits(mf1.e) && Float.floatToIntBits(this.f) == Float.floatToIntBits(mf1.f);
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(Float.valueOf(this.e), Float.valueOf(this.f));
    }

    @DexIgnore
    public String toString() {
        ak0.a a2 = ak0.a((Object) this);
        a2.a("tilt", Float.valueOf(this.e));
        a2.a("bearing", Float.valueOf(this.f));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = lk0.a(parcel);
        lk0.a(parcel, 2, this.e);
        lk0.a(parcel, 3, this.f);
        lk0.a(parcel, a2);
    }
}
