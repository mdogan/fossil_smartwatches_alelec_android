package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dn1 extends IInterface {
    @DexIgnore
    void a(fn1 fn1, bn1 bn1) throws RemoteException;

    @DexIgnore
    void a(uj0 uj0, int i, boolean z) throws RemoteException;

    @DexIgnore
    void c(int i) throws RemoteException;
}
