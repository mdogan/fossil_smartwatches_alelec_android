package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m91 extends j91 {
    @DexIgnore
    public m91() {
        super();
    }

    @DexIgnore
    public static <E> a91<E> c(Object obj, long j) {
        return (a91) lb1.f(obj, j);
    }

    @DexIgnore
    public final <L> List<L> a(Object obj, long j) {
        a91 c = c(obj, j);
        if (c.A()) {
            return c;
        }
        int size = c.size();
        a91 b = c.b(size == 0 ? 10 : size << 1);
        lb1.a(obj, j, (Object) b);
        return b;
    }

    @DexIgnore
    public final void b(Object obj, long j) {
        c(obj, j).B();
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        a91 c = c(obj, j);
        a91 c2 = c(obj2, j);
        int size = c.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            if (!c.A()) {
                c = c.b(size2 + size);
            }
            c.addAll(c2);
        }
        if (size > 0) {
            c2 = c;
        }
        lb1.a(obj, j, (Object) c2);
    }
}
