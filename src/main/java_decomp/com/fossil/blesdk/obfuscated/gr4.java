package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.br4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gr4 extends DialogFragment {
    @DexIgnore
    public br4.a e;
    @DexIgnore
    public br4.b f;
    @DexIgnore
    public boolean g; // = false;

    @DexIgnore
    public static gr4 a(String str, String str2, String str3, int i, int i2, String[] strArr) {
        gr4 gr4 = new gr4();
        gr4.setArguments(new fr4(str, str2, str3, i, i2, strArr).a());
        return gr4;
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (Build.VERSION.SDK_INT >= 17 && getParentFragment() != null) {
            if (getParentFragment() instanceof br4.a) {
                this.e = (br4.a) getParentFragment();
            }
            if (getParentFragment() instanceof br4.b) {
                this.f = (br4.b) getParentFragment();
            }
        }
        if (context instanceof br4.a) {
            this.e = (br4.a) context;
        }
        if (context instanceof br4.b) {
            this.f = (br4.b) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        setCancelable(false);
        fr4 fr4 = new fr4(getArguments());
        return fr4.a(getActivity(), new er4(this, fr4, this.e, this.f));
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.e = null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        this.g = true;
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void a(FragmentManager fragmentManager, String str) {
        if ((Build.VERSION.SDK_INT < 26 || !fragmentManager.isStateSaved()) && !this.g) {
            show(fragmentManager, str);
        }
    }
}
