package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.blesdk.obfuscated.bk;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zj {
    @DexIgnore
    public static /* final */ String e; // = ej.a("ConstraintsCmdHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ bk c;
    @DexIgnore
    public /* final */ jk d; // = new jk(this.a, this.c.d(), (ik) null);

    @DexIgnore
    public zj(Context context, int i, bk bkVar) {
        this.a = context;
        this.b = i;
        this.c = bkVar;
    }

    @DexIgnore
    public void a() {
        List<il> a2 = this.c.e().g().d().a();
        ConstraintProxy.a(this.a, a2);
        this.d.c(a2);
        ArrayList<il> arrayList = new ArrayList<>(a2.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (il next : a2) {
            String str = next.a;
            if (currentTimeMillis >= next.a() && (!next.b() || this.d.a(str))) {
                arrayList.add(next);
            }
        }
        for (il ilVar : arrayList) {
            String str2 = ilVar.a;
            Intent a3 = yj.a(this.a, str2);
            ej.a().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", new Object[]{str2}), new Throwable[0]);
            bk bkVar = this.c;
            bkVar.a((Runnable) new bk.b(bkVar, a3, this.b));
        }
        this.d.a();
    }
}
