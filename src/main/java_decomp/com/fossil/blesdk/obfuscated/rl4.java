package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rl4 {
    @DexIgnore
    public static /* final */ pk4 a; // = new pk4("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ pk4 b; // = new pk4("LOCKED");
    @DexIgnore
    public static /* final */ pk4 c; // = new pk4("UNLOCKED");
    @DexIgnore
    public static /* final */ ol4 d; // = new ol4(b);
    @DexIgnore
    public static /* final */ ol4 e; // = new ol4(c);

    /*
    static {
        new pk4("LOCK_FAIL");
        new pk4("ENQUEUE_FAIL");
        new pk4("SELECT_SUCCESS");
    }
    */

    @DexIgnore
    public static /* synthetic */ pl4 a(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }

    @DexIgnore
    public static final pl4 a(boolean z) {
        return new ql4(z);
    }
}
