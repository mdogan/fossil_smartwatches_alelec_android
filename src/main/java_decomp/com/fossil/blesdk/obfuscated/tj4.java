package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tj4 {
    @DexIgnore
    public static final Object a(kc4<? super cb4> kc4) {
        Object obj;
        CoroutineContext context = kc4.getContext();
        a(context);
        kc4<? super cb4> a = IntrinsicsKt__IntrinsicsJvmKt.a(kc4);
        if (!(a instanceof vh4)) {
            a = null;
        }
        vh4 vh4 = (vh4) a;
        if (vh4 == null) {
            obj = cb4.a;
        } else if (!vh4.k.b(context)) {
            obj = xh4.a((vh4<? super cb4>) vh4) ? oc4.a() : cb4.a;
        } else {
            vh4.d(cb4.a);
            obj = oc4.a();
        }
        if (obj == oc4.a()) {
            uc4.c(kc4);
        }
        return obj;
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "$this$checkCompletion");
        ri4 ri4 = (ri4) coroutineContext.get(ri4.d);
        if (ri4 != null && !ri4.isActive()) {
            throw ri4.y();
        }
    }
}
