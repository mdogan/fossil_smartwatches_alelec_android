package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xl0 extends zy0 implements vl0 {
    @DexIgnore
    public xl0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    public final tn0 zzb() throws RemoteException {
        Parcel a = a(1, o());
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int zzc() throws RemoteException {
        Parcel a = a(2, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
