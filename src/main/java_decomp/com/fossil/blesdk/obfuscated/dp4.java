package com.fossil.blesdk.obfuscated;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dp4 implements kp4 {
    @DexIgnore
    public /* final */ xo4 e;
    @DexIgnore
    public /* final */ Inflater f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;

    @DexIgnore
    public dp4(kp4 kp4, Inflater inflater) {
        this(ep4.a(kp4), inflater);
    }

    @DexIgnore
    public long b(vo4 vo4, long j) throws IOException {
        hp4 b;
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.h) {
            throw new IllegalStateException("closed");
        } else if (i == 0) {
            return 0;
        } else {
            while (true) {
                boolean c = c();
                try {
                    b = vo4.b(1);
                    int inflate = this.f.inflate(b.a, b.c, (int) Math.min(j, (long) (8192 - b.c)));
                    if (inflate > 0) {
                        b.c += inflate;
                        long j2 = (long) inflate;
                        vo4.f += j2;
                        return j2;
                    } else if (this.f.finished()) {
                        break;
                    } else if (this.f.needsDictionary()) {
                        break;
                    } else if (c) {
                        throw new EOFException("source exhausted prematurely");
                    }
                } catch (DataFormatException e2) {
                    throw new IOException(e2);
                }
            }
            d();
            if (b.b != b.c) {
                return -1;
            }
            vo4.e = b.b();
            ip4.a(b);
            return -1;
        }
    }

    @DexIgnore
    public final boolean c() throws IOException {
        if (!this.f.needsInput()) {
            return false;
        }
        d();
        if (this.f.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.e.g()) {
            return true;
        } else {
            hp4 hp4 = this.e.a().e;
            int i = hp4.c;
            int i2 = hp4.b;
            this.g = i - i2;
            this.f.setInput(hp4.a, i2, this.g);
            return false;
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.h) {
            this.f.end();
            this.h = true;
            this.e.close();
        }
    }

    @DexIgnore
    public final void d() throws IOException {
        int i = this.g;
        if (i != 0) {
            int remaining = i - this.f.getRemaining();
            this.g -= remaining;
            this.e.skip((long) remaining);
        }
    }

    @DexIgnore
    public dp4(xo4 xo4, Inflater inflater) {
        if (xo4 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater != null) {
            this.e = xo4;
            this.f = inflater;
        } else {
            throw new IllegalArgumentException("inflater == null");
        }
    }

    @DexIgnore
    public lp4 b() {
        return this.e.b();
    }
}
