package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uq1 {
    @DexIgnore
    public static /* final */ int actionBarDivider; // = 2130968645;
    @DexIgnore
    public static /* final */ int actionBarItemBackground; // = 2130968646;
    @DexIgnore
    public static /* final */ int actionBarPopupTheme; // = 2130968647;
    @DexIgnore
    public static /* final */ int actionBarSize; // = 2130968648;
    @DexIgnore
    public static /* final */ int actionBarSplitStyle; // = 2130968649;
    @DexIgnore
    public static /* final */ int actionBarStyle; // = 2130968650;
    @DexIgnore
    public static /* final */ int actionBarTabBarStyle; // = 2130968651;
    @DexIgnore
    public static /* final */ int actionBarTabStyle; // = 2130968652;
    @DexIgnore
    public static /* final */ int actionBarTabTextStyle; // = 2130968653;
    @DexIgnore
    public static /* final */ int actionBarTheme; // = 2130968654;
    @DexIgnore
    public static /* final */ int actionBarWidgetTheme; // = 2130968655;
    @DexIgnore
    public static /* final */ int actionButtonStyle; // = 2130968656;
    @DexIgnore
    public static /* final */ int actionDropDownStyle; // = 2130968657;
    @DexIgnore
    public static /* final */ int actionLayout; // = 2130968658;
    @DexIgnore
    public static /* final */ int actionMenuTextAppearance; // = 2130968659;
    @DexIgnore
    public static /* final */ int actionMenuTextColor; // = 2130968660;
    @DexIgnore
    public static /* final */ int actionModeBackground; // = 2130968661;
    @DexIgnore
    public static /* final */ int actionModeCloseButtonStyle; // = 2130968662;
    @DexIgnore
    public static /* final */ int actionModeCloseDrawable; // = 2130968663;
    @DexIgnore
    public static /* final */ int actionModeCopyDrawable; // = 2130968664;
    @DexIgnore
    public static /* final */ int actionModeCutDrawable; // = 2130968665;
    @DexIgnore
    public static /* final */ int actionModeFindDrawable; // = 2130968666;
    @DexIgnore
    public static /* final */ int actionModePasteDrawable; // = 2130968667;
    @DexIgnore
    public static /* final */ int actionModePopupWindowStyle; // = 2130968668;
    @DexIgnore
    public static /* final */ int actionModeSelectAllDrawable; // = 2130968669;
    @DexIgnore
    public static /* final */ int actionModeShareDrawable; // = 2130968670;
    @DexIgnore
    public static /* final */ int actionModeSplitBackground; // = 2130968671;
    @DexIgnore
    public static /* final */ int actionModeStyle; // = 2130968672;
    @DexIgnore
    public static /* final */ int actionModeWebSearchDrawable; // = 2130968673;
    @DexIgnore
    public static /* final */ int actionOverflowButtonStyle; // = 2130968674;
    @DexIgnore
    public static /* final */ int actionOverflowMenuStyle; // = 2130968675;
    @DexIgnore
    public static /* final */ int actionProviderClass; // = 2130968676;
    @DexIgnore
    public static /* final */ int actionViewClass; // = 2130968677;
    @DexIgnore
    public static /* final */ int activityChooserViewStyle; // = 2130968686;
    @DexIgnore
    public static /* final */ int alertDialogButtonGroupStyle; // = 2130968736;
    @DexIgnore
    public static /* final */ int alertDialogCenterButtons; // = 2130968737;
    @DexIgnore
    public static /* final */ int alertDialogStyle; // = 2130968738;
    @DexIgnore
    public static /* final */ int alertDialogTheme; // = 2130968739;
    @DexIgnore
    public static /* final */ int allowStacking; // = 2130968742;
    @DexIgnore
    public static /* final */ int alpha; // = 2130968743;
    @DexIgnore
    public static /* final */ int alphabeticModifiers; // = 2130968747;
    @DexIgnore
    public static /* final */ int arrowHeadLength; // = 2130968749;
    @DexIgnore
    public static /* final */ int arrowShaftLength; // = 2130968750;
    @DexIgnore
    public static /* final */ int autoCompleteTextViewStyle; // = 2130968751;
    @DexIgnore
    public static /* final */ int autoSizeMaxTextSize; // = 2130968753;
    @DexIgnore
    public static /* final */ int autoSizeMinTextSize; // = 2130968754;
    @DexIgnore
    public static /* final */ int autoSizePresetSizes; // = 2130968755;
    @DexIgnore
    public static /* final */ int autoSizeStepGranularity; // = 2130968756;
    @DexIgnore
    public static /* final */ int autoSizeTextType; // = 2130968757;
    @DexIgnore
    public static /* final */ int background; // = 2130968761;
    @DexIgnore
    public static /* final */ int backgroundSplit; // = 2130968763;
    @DexIgnore
    public static /* final */ int backgroundStacked; // = 2130968764;
    @DexIgnore
    public static /* final */ int backgroundTint; // = 2130968766;
    @DexIgnore
    public static /* final */ int backgroundTintMode; // = 2130968767;
    @DexIgnore
    public static /* final */ int barLength; // = 2130968768;
    @DexIgnore
    public static /* final */ int behavior_autoHide; // = 2130968829;
    @DexIgnore
    public static /* final */ int behavior_fitToContents; // = 2130968830;
    @DexIgnore
    public static /* final */ int behavior_hideable; // = 2130968831;
    @DexIgnore
    public static /* final */ int behavior_overlapTop; // = 2130968832;
    @DexIgnore
    public static /* final */ int behavior_peekHeight; // = 2130968833;
    @DexIgnore
    public static /* final */ int behavior_skipCollapsed; // = 2130968834;
    @DexIgnore
    public static /* final */ int borderWidth; // = 2130968839;
    @DexIgnore
    public static /* final */ int borderlessButtonStyle; // = 2130968843;
    @DexIgnore
    public static /* final */ int bottomAppBarStyle; // = 2130968844;
    @DexIgnore
    public static /* final */ int bottomNavigationStyle; // = 2130968846;
    @DexIgnore
    public static /* final */ int bottomSheetDialogTheme; // = 2130968848;
    @DexIgnore
    public static /* final */ int bottomSheetStyle; // = 2130968849;
    @DexIgnore
    public static /* final */ int boxBackgroundColor; // = 2130968850;
    @DexIgnore
    public static /* final */ int boxBackgroundMode; // = 2130968851;
    @DexIgnore
    public static /* final */ int boxCollapsedPaddingTop; // = 2130968852;
    @DexIgnore
    public static /* final */ int boxCornerRadiusBottomEnd; // = 2130968853;
    @DexIgnore
    public static /* final */ int boxCornerRadiusBottomStart; // = 2130968854;
    @DexIgnore
    public static /* final */ int boxCornerRadiusTopEnd; // = 2130968855;
    @DexIgnore
    public static /* final */ int boxCornerRadiusTopStart; // = 2130968856;
    @DexIgnore
    public static /* final */ int boxStrokeColor; // = 2130968857;
    @DexIgnore
    public static /* final */ int boxStrokeWidth; // = 2130968858;
    @DexIgnore
    public static /* final */ int buttonBarButtonStyle; // = 2130968859;
    @DexIgnore
    public static /* final */ int buttonBarNegativeButtonStyle; // = 2130968860;
    @DexIgnore
    public static /* final */ int buttonBarNeutralButtonStyle; // = 2130968861;
    @DexIgnore
    public static /* final */ int buttonBarPositiveButtonStyle; // = 2130968862;
    @DexIgnore
    public static /* final */ int buttonBarStyle; // = 2130968863;
    @DexIgnore
    public static /* final */ int buttonGravity; // = 2130968864;
    @DexIgnore
    public static /* final */ int buttonIconDimen; // = 2130968865;
    @DexIgnore
    public static /* final */ int buttonPanelSideLayout; // = 2130968866;
    @DexIgnore
    public static /* final */ int buttonStyle; // = 2130968874;
    @DexIgnore
    public static /* final */ int buttonStyleSmall; // = 2130968875;
    @DexIgnore
    public static /* final */ int buttonTint; // = 2130968876;
    @DexIgnore
    public static /* final */ int buttonTintMode; // = 2130968877;
    @DexIgnore
    public static /* final */ int cardBackgroundColor; // = 2130968898;
    @DexIgnore
    public static /* final */ int cardCornerRadius; // = 2130968899;
    @DexIgnore
    public static /* final */ int cardElevation; // = 2130968900;
    @DexIgnore
    public static /* final */ int cardMaxElevation; // = 2130968901;
    @DexIgnore
    public static /* final */ int cardPreventCornerOverlap; // = 2130968902;
    @DexIgnore
    public static /* final */ int cardUseCompatPadding; // = 2130968903;
    @DexIgnore
    public static /* final */ int cardViewStyle; // = 2130968904;
    @DexIgnore
    public static /* final */ int checkboxStyle; // = 2130968913;
    @DexIgnore
    public static /* final */ int checkedChip; // = 2130968914;
    @DexIgnore
    public static /* final */ int checkedIcon; // = 2130968915;
    @DexIgnore
    public static /* final */ int checkedIconEnabled; // = 2130968916;
    @DexIgnore
    public static /* final */ int checkedIconVisible; // = 2130968917;
    @DexIgnore
    public static /* final */ int checkedTextViewStyle; // = 2130968918;
    @DexIgnore
    public static /* final */ int chipBackgroundColor; // = 2130968919;
    @DexIgnore
    public static /* final */ int chipCornerRadius; // = 2130968920;
    @DexIgnore
    public static /* final */ int chipEndPadding; // = 2130968921;
    @DexIgnore
    public static /* final */ int chipGroupStyle; // = 2130968922;
    @DexIgnore
    public static /* final */ int chipIcon; // = 2130968923;
    @DexIgnore
    public static /* final */ int chipIconEnabled; // = 2130968924;
    @DexIgnore
    public static /* final */ int chipIconSize; // = 2130968925;
    @DexIgnore
    public static /* final */ int chipIconTint; // = 2130968926;
    @DexIgnore
    public static /* final */ int chipIconVisible; // = 2130968927;
    @DexIgnore
    public static /* final */ int chipMinHeight; // = 2130968928;
    @DexIgnore
    public static /* final */ int chipSpacing; // = 2130968929;
    @DexIgnore
    public static /* final */ int chipSpacingHorizontal; // = 2130968930;
    @DexIgnore
    public static /* final */ int chipSpacingVertical; // = 2130968931;
    @DexIgnore
    public static /* final */ int chipStandaloneStyle; // = 2130968932;
    @DexIgnore
    public static /* final */ int chipStartPadding; // = 2130968933;
    @DexIgnore
    public static /* final */ int chipStrokeColor; // = 2130968934;
    @DexIgnore
    public static /* final */ int chipStrokeWidth; // = 2130968935;
    @DexIgnore
    public static /* final */ int chipStyle; // = 2130968936;
    @DexIgnore
    public static /* final */ int closeIcon; // = 2130968953;
    @DexIgnore
    public static /* final */ int closeIconEnabled; // = 2130968954;
    @DexIgnore
    public static /* final */ int closeIconEndPadding; // = 2130968955;
    @DexIgnore
    public static /* final */ int closeIconSize; // = 2130968956;
    @DexIgnore
    public static /* final */ int closeIconStartPadding; // = 2130968957;
    @DexIgnore
    public static /* final */ int closeIconTint; // = 2130968958;
    @DexIgnore
    public static /* final */ int closeIconVisible; // = 2130968959;
    @DexIgnore
    public static /* final */ int closeItemLayout; // = 2130968960;
    @DexIgnore
    public static /* final */ int collapseContentDescription; // = 2130968962;
    @DexIgnore
    public static /* final */ int collapseIcon; // = 2130968963;
    @DexIgnore
    public static /* final */ int collapsedTitleGravity; // = 2130968966;
    @DexIgnore
    public static /* final */ int collapsedTitleTextAppearance; // = 2130968967;
    @DexIgnore
    public static /* final */ int color; // = 2130968968;
    @DexIgnore
    public static /* final */ int colorAccent; // = 2130968969;
    @DexIgnore
    public static /* final */ int colorBackgroundFloating; // = 2130968971;
    @DexIgnore
    public static /* final */ int colorButtonNormal; // = 2130968972;
    @DexIgnore
    public static /* final */ int colorControlActivated; // = 2130968973;
    @DexIgnore
    public static /* final */ int colorControlHighlight; // = 2130968974;
    @DexIgnore
    public static /* final */ int colorControlNormal; // = 2130968975;
    @DexIgnore
    public static /* final */ int colorError; // = 2130968976;
    @DexIgnore
    public static /* final */ int colorPrimary; // = 2130968977;
    @DexIgnore
    public static /* final */ int colorPrimaryDark; // = 2130968978;
    @DexIgnore
    public static /* final */ int colorSecondary; // = 2130968980;
    @DexIgnore
    public static /* final */ int colorSwitchThumbNormal; // = 2130968981;
    @DexIgnore
    public static /* final */ int commitIcon; // = 2130968994;
    @DexIgnore
    public static /* final */ int contentDescription; // = 2130969009;
    @DexIgnore
    public static /* final */ int contentInsetEnd; // = 2130969010;
    @DexIgnore
    public static /* final */ int contentInsetEndWithActions; // = 2130969011;
    @DexIgnore
    public static /* final */ int contentInsetLeft; // = 2130969012;
    @DexIgnore
    public static /* final */ int contentInsetRight; // = 2130969013;
    @DexIgnore
    public static /* final */ int contentInsetStart; // = 2130969014;
    @DexIgnore
    public static /* final */ int contentInsetStartWithNavigation; // = 2130969015;
    @DexIgnore
    public static /* final */ int contentPadding; // = 2130969016;
    @DexIgnore
    public static /* final */ int contentPaddingBottom; // = 2130969017;
    @DexIgnore
    public static /* final */ int contentPaddingLeft; // = 2130969018;
    @DexIgnore
    public static /* final */ int contentPaddingRight; // = 2130969019;
    @DexIgnore
    public static /* final */ int contentPaddingTop; // = 2130969020;
    @DexIgnore
    public static /* final */ int contentScrim; // = 2130969021;
    @DexIgnore
    public static /* final */ int controlBackground; // = 2130969022;
    @DexIgnore
    public static /* final */ int coordinatorLayoutStyle; // = 2130969039;
    @DexIgnore
    public static /* final */ int cornerRadius; // = 2130969040;
    @DexIgnore
    public static /* final */ int counterEnabled; // = 2130969041;
    @DexIgnore
    public static /* final */ int counterMaxLength; // = 2130969042;
    @DexIgnore
    public static /* final */ int counterOverflowTextAppearance; // = 2130969043;
    @DexIgnore
    public static /* final */ int counterTextAppearance; // = 2130969044;
    @DexIgnore
    public static /* final */ int customNavigationLayout; // = 2130969047;
    @DexIgnore
    public static /* final */ int defaultQueryHint; // = 2130969089;
    @DexIgnore
    public static /* final */ int dialogCornerRadius; // = 2130969113;
    @DexIgnore
    public static /* final */ int dialogPreferredPadding; // = 2130969114;
    @DexIgnore
    public static /* final */ int dialogTheme; // = 2130969115;
    @DexIgnore
    public static /* final */ int displayOptions; // = 2130969116;
    @DexIgnore
    public static /* final */ int divider; // = 2130969118;
    @DexIgnore
    public static /* final */ int dividerHorizontal; // = 2130969119;
    @DexIgnore
    public static /* final */ int dividerPadding; // = 2130969120;
    @DexIgnore
    public static /* final */ int dividerVertical; // = 2130969121;
    @DexIgnore
    public static /* final */ int drawableSize; // = 2130969124;
    @DexIgnore
    public static /* final */ int drawerArrowStyle; // = 2130969125;
    @DexIgnore
    public static /* final */ int dropDownListViewStyle; // = 2130969126;
    @DexIgnore
    public static /* final */ int dropdownListPreferredItemHeight; // = 2130969127;
    @DexIgnore
    public static /* final */ int editTextBackground; // = 2130969128;
    @DexIgnore
    public static /* final */ int editTextColor; // = 2130969129;
    @DexIgnore
    public static /* final */ int editTextStyle; // = 2130969131;
    @DexIgnore
    public static /* final */ int elevation; // = 2130969139;
    @DexIgnore
    public static /* final */ int enforceMaterialTheme; // = 2130969148;
    @DexIgnore
    public static /* final */ int enforceTextAppearance; // = 2130969149;
    @DexIgnore
    public static /* final */ int errorEnabled; // = 2130969150;
    @DexIgnore
    public static /* final */ int errorTextAppearance; // = 2130969152;
    @DexIgnore
    public static /* final */ int expandActivityOverflowButtonDrawable; // = 2130969156;
    @DexIgnore
    public static /* final */ int expanded; // = 2130969157;
    @DexIgnore
    public static /* final */ int expandedTitleGravity; // = 2130969158;
    @DexIgnore
    public static /* final */ int expandedTitleMargin; // = 2130969159;
    @DexIgnore
    public static /* final */ int expandedTitleMarginBottom; // = 2130969160;
    @DexIgnore
    public static /* final */ int expandedTitleMarginEnd; // = 2130969161;
    @DexIgnore
    public static /* final */ int expandedTitleMarginStart; // = 2130969162;
    @DexIgnore
    public static /* final */ int expandedTitleMarginTop; // = 2130969163;
    @DexIgnore
    public static /* final */ int expandedTitleTextAppearance; // = 2130969164;
    @DexIgnore
    public static /* final */ int fabAlignmentMode; // = 2130969165;
    @DexIgnore
    public static /* final */ int fabCradleMargin; // = 2130969166;
    @DexIgnore
    public static /* final */ int fabCradleRoundedCornerRadius; // = 2130969167;
    @DexIgnore
    public static /* final */ int fabCradleVerticalOffset; // = 2130969168;
    @DexIgnore
    public static /* final */ int fabCustomSize; // = 2130969169;
    @DexIgnore
    public static /* final */ int fabSize; // = 2130969170;
    @DexIgnore
    public static /* final */ int fastScrollEnabled; // = 2130969171;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalThumbDrawable; // = 2130969172;
    @DexIgnore
    public static /* final */ int fastScrollHorizontalTrackDrawable; // = 2130969173;
    @DexIgnore
    public static /* final */ int fastScrollVerticalThumbDrawable; // = 2130969174;
    @DexIgnore
    public static /* final */ int fastScrollVerticalTrackDrawable; // = 2130969175;
    @DexIgnore
    public static /* final */ int firstBaselineToTopHeight; // = 2130969180;
    @DexIgnore
    public static /* final */ int floatingActionButtonStyle; // = 2130969181;
    @DexIgnore
    public static /* final */ int font; // = 2130969182;
    @DexIgnore
    public static /* final */ int fontFamily; // = 2130969183;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969187;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969188;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969189;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969190;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969191;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969192;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969193;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969194;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969195;
    @DexIgnore
    public static /* final */ int foregroundInsidePadding; // = 2130969196;
    @DexIgnore
    public static /* final */ int gapBetweenBars; // = 2130969202;
    @DexIgnore
    public static /* final */ int goIcon; // = 2130969204;
    @DexIgnore
    public static /* final */ int headerLayout; // = 2130969220;
    @DexIgnore
    public static /* final */ int height; // = 2130969235;
    @DexIgnore
    public static /* final */ int helperText; // = 2130969238;
    @DexIgnore
    public static /* final */ int helperTextEnabled; // = 2130969239;
    @DexIgnore
    public static /* final */ int helperTextTextAppearance; // = 2130969240;
    @DexIgnore
    public static /* final */ int hideMotionSpec; // = 2130969241;
    @DexIgnore
    public static /* final */ int hideOnContentScroll; // = 2130969242;
    @DexIgnore
    public static /* final */ int hideOnScroll; // = 2130969243;
    @DexIgnore
    public static /* final */ int hintAnimationEnabled; // = 2130969244;
    @DexIgnore
    public static /* final */ int hintEnabled; // = 2130969245;
    @DexIgnore
    public static /* final */ int hintTextAppearance; // = 2130969246;
    @DexIgnore
    public static /* final */ int homeAsUpIndicator; // = 2130969248;
    @DexIgnore
    public static /* final */ int homeLayout; // = 2130969249;
    @DexIgnore
    public static /* final */ int hoveredFocusedTranslationZ; // = 2130969250;
    @DexIgnore
    public static /* final */ int icon; // = 2130969251;
    @DexIgnore
    public static /* final */ int iconEndPadding; // = 2130969252;
    @DexIgnore
    public static /* final */ int iconGravity; // = 2130969253;
    @DexIgnore
    public static /* final */ int iconPadding; // = 2130969254;
    @DexIgnore
    public static /* final */ int iconSize; // = 2130969255;
    @DexIgnore
    public static /* final */ int iconStartPadding; // = 2130969256;
    @DexIgnore
    public static /* final */ int iconTint; // = 2130969257;
    @DexIgnore
    public static /* final */ int iconTintMode; // = 2130969258;
    @DexIgnore
    public static /* final */ int iconifiedByDefault; // = 2130969264;
    @DexIgnore
    public static /* final */ int imageButtonStyle; // = 2130969268;
    @DexIgnore
    public static /* final */ int indeterminateProgressStyle; // = 2130969272;
    @DexIgnore
    public static /* final */ int initialActivityCount; // = 2130969283;
    @DexIgnore
    public static /* final */ int insetForeground; // = 2130969284;
    @DexIgnore
    public static /* final */ int isLightTheme; // = 2130969291;
    @DexIgnore
    public static /* final */ int itemBackground; // = 2130969296;
    @DexIgnore
    public static /* final */ int itemHorizontalPadding; // = 2130969297;
    @DexIgnore
    public static /* final */ int itemHorizontalTranslationEnabled; // = 2130969298;
    @DexIgnore
    public static /* final */ int itemIconPadding; // = 2130969299;
    @DexIgnore
    public static /* final */ int itemIconSize; // = 2130969300;
    @DexIgnore
    public static /* final */ int itemIconTint; // = 2130969301;
    @DexIgnore
    public static /* final */ int itemPadding; // = 2130969302;
    @DexIgnore
    public static /* final */ int itemSpacing; // = 2130969303;
    @DexIgnore
    public static /* final */ int itemTextAppearance; // = 2130969304;
    @DexIgnore
    public static /* final */ int itemTextAppearanceActive; // = 2130969305;
    @DexIgnore
    public static /* final */ int itemTextAppearanceInactive; // = 2130969306;
    @DexIgnore
    public static /* final */ int itemTextColor; // = 2130969307;
    @DexIgnore
    public static /* final */ int keylines; // = 2130969308;
    @DexIgnore
    public static /* final */ int labelVisibilityMode; // = 2130969309;
    @DexIgnore
    public static /* final */ int lastBaselineToBottomHeight; // = 2130969310;
    @DexIgnore
    public static /* final */ int layout; // = 2130969315;
    @DexIgnore
    public static /* final */ int layoutManager; // = 2130969316;
    @DexIgnore
    public static /* final */ int layout_anchor; // = 2130969317;
    @DexIgnore
    public static /* final */ int layout_anchorGravity; // = 2130969318;
    @DexIgnore
    public static /* final */ int layout_behavior; // = 2130969320;
    @DexIgnore
    public static /* final */ int layout_collapseMode; // = 2130969321;
    @DexIgnore
    public static /* final */ int layout_collapseParallaxMultiplier; // = 2130969322;
    @DexIgnore
    public static /* final */ int layout_dodgeInsetEdges; // = 2130969364;
    @DexIgnore
    public static /* final */ int layout_insetEdge; // = 2130969374;
    @DexIgnore
    public static /* final */ int layout_keyline; // = 2130969375;
    @DexIgnore
    public static /* final */ int layout_scrollFlags; // = 2130969384;
    @DexIgnore
    public static /* final */ int layout_scrollInterpolator; // = 2130969385;
    @DexIgnore
    public static /* final */ int liftOnScroll; // = 2130969388;
    @DexIgnore
    public static /* final */ int lineHeight; // = 2130969391;
    @DexIgnore
    public static /* final */ int lineSpacing; // = 2130969392;
    @DexIgnore
    public static /* final */ int listChoiceBackgroundIndicator; // = 2130969394;
    @DexIgnore
    public static /* final */ int listDividerAlertDialog; // = 2130969395;
    @DexIgnore
    public static /* final */ int listItemLayout; // = 2130969396;
    @DexIgnore
    public static /* final */ int listLayout; // = 2130969397;
    @DexIgnore
    public static /* final */ int listMenuViewStyle; // = 2130969398;
    @DexIgnore
    public static /* final */ int listPopupWindowStyle; // = 2130969399;
    @DexIgnore
    public static /* final */ int listPreferredItemHeight; // = 2130969400;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightLarge; // = 2130969401;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightSmall; // = 2130969402;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingLeft; // = 2130969403;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingRight; // = 2130969404;
    @DexIgnore
    public static /* final */ int logo; // = 2130969409;
    @DexIgnore
    public static /* final */ int logoDescription; // = 2130969410;
    @DexIgnore
    public static /* final */ int materialButtonStyle; // = 2130969423;
    @DexIgnore
    public static /* final */ int materialCardViewStyle; // = 2130969424;
    @DexIgnore
    public static /* final */ int maxActionInlineWidth; // = 2130969425;
    @DexIgnore
    public static /* final */ int maxButtonHeight; // = 2130969426;
    @DexIgnore
    public static /* final */ int maxImageSize; // = 2130969427;
    @DexIgnore
    public static /* final */ int measureWithLargestChild; // = 2130969430;
    @DexIgnore
    public static /* final */ int menu; // = 2130969431;
    @DexIgnore
    public static /* final */ int multiChoiceItemLayout; // = 2130969443;
    @DexIgnore
    public static /* final */ int navigationContentDescription; // = 2130969448;
    @DexIgnore
    public static /* final */ int navigationIcon; // = 2130969449;
    @DexIgnore
    public static /* final */ int navigationMode; // = 2130969450;
    @DexIgnore
    public static /* final */ int navigationViewStyle; // = 2130969452;
    @DexIgnore
    public static /* final */ int numericModifiers; // = 2130969466;
    @DexIgnore
    public static /* final */ int overlapAnchor; // = 2130969467;
    @DexIgnore
    public static /* final */ int paddingBottomNoButtons; // = 2130969468;
    @DexIgnore
    public static /* final */ int paddingEnd; // = 2130969469;
    @DexIgnore
    public static /* final */ int paddingStart; // = 2130969471;
    @DexIgnore
    public static /* final */ int paddingTopNoTitle; // = 2130969472;
    @DexIgnore
    public static /* final */ int panelBackground; // = 2130969474;
    @DexIgnore
    public static /* final */ int panelMenuListTheme; // = 2130969475;
    @DexIgnore
    public static /* final */ int panelMenuListWidth; // = 2130969476;
    @DexIgnore
    public static /* final */ int passwordToggleContentDescription; // = 2130969477;
    @DexIgnore
    public static /* final */ int passwordToggleDrawable; // = 2130969478;
    @DexIgnore
    public static /* final */ int passwordToggleEnabled; // = 2130969479;
    @DexIgnore
    public static /* final */ int passwordToggleTint; // = 2130969480;
    @DexIgnore
    public static /* final */ int passwordToggleTintMode; // = 2130969481;
    @DexIgnore
    public static /* final */ int popupMenuStyle; // = 2130969483;
    @DexIgnore
    public static /* final */ int popupTheme; // = 2130969484;
    @DexIgnore
    public static /* final */ int popupWindowStyle; // = 2130969485;
    @DexIgnore
    public static /* final */ int preserveIconSpacing; // = 2130969486;
    @DexIgnore
    public static /* final */ int pressedTranslationZ; // = 2130969487;
    @DexIgnore
    public static /* final */ int progressBarPadding; // = 2130969496;
    @DexIgnore
    public static /* final */ int progressBarStyle; // = 2130969497;
    @DexIgnore
    public static /* final */ int queryBackground; // = 2130969510;
    @DexIgnore
    public static /* final */ int queryHint; // = 2130969511;
    @DexIgnore
    public static /* final */ int radioButtonStyle; // = 2130969512;
    @DexIgnore
    public static /* final */ int ratingBarStyle; // = 2130969514;
    @DexIgnore
    public static /* final */ int ratingBarStyleIndicator; // = 2130969515;
    @DexIgnore
    public static /* final */ int ratingBarStyleSmall; // = 2130969516;
    @DexIgnore
    public static /* final */ int reverseLayout; // = 2130969525;
    @DexIgnore
    public static /* final */ int rippleColor; // = 2130969532;
    @DexIgnore
    public static /* final */ int scrimAnimationDuration; // = 2130969551;
    @DexIgnore
    public static /* final */ int scrimBackground; // = 2130969552;
    @DexIgnore
    public static /* final */ int scrimVisibleHeightTrigger; // = 2130969553;
    @DexIgnore
    public static /* final */ int searchHintIcon; // = 2130969555;
    @DexIgnore
    public static /* final */ int searchIcon; // = 2130969556;
    @DexIgnore
    public static /* final */ int searchViewStyle; // = 2130969558;
    @DexIgnore
    public static /* final */ int seekBarStyle; // = 2130969559;
    @DexIgnore
    public static /* final */ int selectableItemBackground; // = 2130969560;
    @DexIgnore
    public static /* final */ int selectableItemBackgroundBorderless; // = 2130969561;
    @DexIgnore
    public static /* final */ int showAsAction; // = 2130969583;
    @DexIgnore
    public static /* final */ int showDividers; // = 2130969584;
    @DexIgnore
    public static /* final */ int showMotionSpec; // = 2130969585;
    @DexIgnore
    public static /* final */ int showText; // = 2130969586;
    @DexIgnore
    public static /* final */ int showTitle; // = 2130969587;
    @DexIgnore
    public static /* final */ int singleChoiceItemLayout; // = 2130969588;
    @DexIgnore
    public static /* final */ int singleLine; // = 2130969589;
    @DexIgnore
    public static /* final */ int singleSelection; // = 2130969590;
    @DexIgnore
    public static /* final */ int snackbarButtonStyle; // = 2130969648;
    @DexIgnore
    public static /* final */ int snackbarStyle; // = 2130969649;
    @DexIgnore
    public static /* final */ int spanCount; // = 2130969653;
    @DexIgnore
    public static /* final */ int spinBars; // = 2130969654;
    @DexIgnore
    public static /* final */ int spinnerDropDownItemStyle; // = 2130969655;
    @DexIgnore
    public static /* final */ int spinnerStyle; // = 2130969656;
    @DexIgnore
    public static /* final */ int splitTrack; // = 2130969657;
    @DexIgnore
    public static /* final */ int srcCompat; // = 2130969658;
    @DexIgnore
    public static /* final */ int stackFromEnd; // = 2130969660;
    @DexIgnore
    public static /* final */ int state_above_anchor; // = 2130969662;
    @DexIgnore
    public static /* final */ int state_collapsed; // = 2130969663;
    @DexIgnore
    public static /* final */ int state_collapsible; // = 2130969664;
    @DexIgnore
    public static /* final */ int state_liftable; // = 2130969665;
    @DexIgnore
    public static /* final */ int state_lifted; // = 2130969666;
    @DexIgnore
    public static /* final */ int statusBarBackground; // = 2130969672;
    @DexIgnore
    public static /* final */ int statusBarScrim; // = 2130969673;
    @DexIgnore
    public static /* final */ int strokeColor; // = 2130969675;
    @DexIgnore
    public static /* final */ int strokeWidth; // = 2130969679;
    @DexIgnore
    public static /* final */ int subMenuArrow; // = 2130969682;
    @DexIgnore
    public static /* final */ int submitBackground; // = 2130969684;
    @DexIgnore
    public static /* final */ int subtitle; // = 2130969685;
    @DexIgnore
    public static /* final */ int subtitleTextAppearance; // = 2130969686;
    @DexIgnore
    public static /* final */ int subtitleTextColor; // = 2130969687;
    @DexIgnore
    public static /* final */ int subtitleTextStyle; // = 2130969688;
    @DexIgnore
    public static /* final */ int suggestionRowLayout; // = 2130969689;
    @DexIgnore
    public static /* final */ int switchMinWidth; // = 2130969693;
    @DexIgnore
    public static /* final */ int switchPadding; // = 2130969694;
    @DexIgnore
    public static /* final */ int switchStyle; // = 2130969695;
    @DexIgnore
    public static /* final */ int switchTextAppearance; // = 2130969696;
    @DexIgnore
    public static /* final */ int tabBackground; // = 2130969702;
    @DexIgnore
    public static /* final */ int tabContentStart; // = 2130969703;
    @DexIgnore
    public static /* final */ int tabGravity; // = 2130969704;
    @DexIgnore
    public static /* final */ int tabIconTint; // = 2130969705;
    @DexIgnore
    public static /* final */ int tabIconTintMode; // = 2130969706;
    @DexIgnore
    public static /* final */ int tabIndicator; // = 2130969707;
    @DexIgnore
    public static /* final */ int tabIndicatorAnimationDuration; // = 2130969708;
    @DexIgnore
    public static /* final */ int tabIndicatorColor; // = 2130969709;
    @DexIgnore
    public static /* final */ int tabIndicatorFullWidth; // = 2130969710;
    @DexIgnore
    public static /* final */ int tabIndicatorGravity; // = 2130969711;
    @DexIgnore
    public static /* final */ int tabIndicatorHeight; // = 2130969712;
    @DexIgnore
    public static /* final */ int tabInlineLabel; // = 2130969713;
    @DexIgnore
    public static /* final */ int tabMaxWidth; // = 2130969714;
    @DexIgnore
    public static /* final */ int tabMinWidth; // = 2130969715;
    @DexIgnore
    public static /* final */ int tabMode; // = 2130969716;
    @DexIgnore
    public static /* final */ int tabPadding; // = 2130969717;
    @DexIgnore
    public static /* final */ int tabPaddingBottom; // = 2130969718;
    @DexIgnore
    public static /* final */ int tabPaddingEnd; // = 2130969719;
    @DexIgnore
    public static /* final */ int tabPaddingStart; // = 2130969720;
    @DexIgnore
    public static /* final */ int tabPaddingTop; // = 2130969721;
    @DexIgnore
    public static /* final */ int tabRippleColor; // = 2130969722;
    @DexIgnore
    public static /* final */ int tabSelectedTextColor; // = 2130969723;
    @DexIgnore
    public static /* final */ int tabStyle; // = 2130969724;
    @DexIgnore
    public static /* final */ int tabTextAppearance; // = 2130969725;
    @DexIgnore
    public static /* final */ int tabTextColor; // = 2130969726;
    @DexIgnore
    public static /* final */ int tabUnboundedRipple; // = 2130969727;
    @DexIgnore
    public static /* final */ int textAllCaps; // = 2130969733;
    @DexIgnore
    public static /* final */ int textAppearanceBody1; // = 2130969734;
    @DexIgnore
    public static /* final */ int textAppearanceBody2; // = 2130969735;
    @DexIgnore
    public static /* final */ int textAppearanceButton; // = 2130969736;
    @DexIgnore
    public static /* final */ int textAppearanceCaption; // = 2130969737;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline1; // = 2130969738;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline2; // = 2130969739;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline3; // = 2130969740;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline4; // = 2130969741;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline5; // = 2130969742;
    @DexIgnore
    public static /* final */ int textAppearanceHeadline6; // = 2130969743;
    @DexIgnore
    public static /* final */ int textAppearanceLargePopupMenu; // = 2130969744;
    @DexIgnore
    public static /* final */ int textAppearanceListItem; // = 2130969745;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSecondary; // = 2130969746;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSmall; // = 2130969747;
    @DexIgnore
    public static /* final */ int textAppearanceOverline; // = 2130969748;
    @DexIgnore
    public static /* final */ int textAppearancePopupMenuHeader; // = 2130969749;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultSubtitle; // = 2130969750;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultTitle; // = 2130969751;
    @DexIgnore
    public static /* final */ int textAppearanceSmallPopupMenu; // = 2130969752;
    @DexIgnore
    public static /* final */ int textAppearanceSubtitle1; // = 2130969753;
    @DexIgnore
    public static /* final */ int textAppearanceSubtitle2; // = 2130969754;
    @DexIgnore
    public static /* final */ int textColorAlertDialogListItem; // = 2130969764;
    @DexIgnore
    public static /* final */ int textColorSearchUrl; // = 2130969765;
    @DexIgnore
    public static /* final */ int textEndPadding; // = 2130969767;
    @DexIgnore
    public static /* final */ int textInputStyle; // = 2130969771;
    @DexIgnore
    public static /* final */ int textStartPadding; // = 2130969774;
    @DexIgnore
    public static /* final */ int theme; // = 2130969791;
    @DexIgnore
    public static /* final */ int thickness; // = 2130969792;
    @DexIgnore
    public static /* final */ int thumbTextPadding; // = 2130969793;
    @DexIgnore
    public static /* final */ int thumbTint; // = 2130969794;
    @DexIgnore
    public static /* final */ int thumbTintMode; // = 2130969795;
    @DexIgnore
    public static /* final */ int tickMark; // = 2130969800;
    @DexIgnore
    public static /* final */ int tickMarkTint; // = 2130969801;
    @DexIgnore
    public static /* final */ int tickMarkTintMode; // = 2130969802;
    @DexIgnore
    public static /* final */ int tint; // = 2130969803;
    @DexIgnore
    public static /* final */ int tintMode; // = 2130969804;
    @DexIgnore
    public static /* final */ int title; // = 2130969806;
    @DexIgnore
    public static /* final */ int titleEnabled; // = 2130969807;
    @DexIgnore
    public static /* final */ int titleMargin; // = 2130969808;
    @DexIgnore
    public static /* final */ int titleMarginBottom; // = 2130969809;
    @DexIgnore
    public static /* final */ int titleMarginEnd; // = 2130969810;
    @DexIgnore
    public static /* final */ int titleMarginStart; // = 2130969811;
    @DexIgnore
    public static /* final */ int titleMarginTop; // = 2130969812;
    @DexIgnore
    public static /* final */ int titleMargins; // = 2130969813;
    @DexIgnore
    public static /* final */ int titleTextAppearance; // = 2130969814;
    @DexIgnore
    public static /* final */ int titleTextColor; // = 2130969815;
    @DexIgnore
    public static /* final */ int titleTextStyle; // = 2130969816;
    @DexIgnore
    public static /* final */ int toolbarId; // = 2130969823;
    @DexIgnore
    public static /* final */ int toolbarNavigationButtonStyle; // = 2130969824;
    @DexIgnore
    public static /* final */ int toolbarStyle; // = 2130969825;
    @DexIgnore
    public static /* final */ int tooltipForegroundColor; // = 2130969826;
    @DexIgnore
    public static /* final */ int tooltipFrameBackground; // = 2130969827;
    @DexIgnore
    public static /* final */ int tooltipText; // = 2130969828;
    @DexIgnore
    public static /* final */ int track; // = 2130969830;
    @DexIgnore
    public static /* final */ int trackTint; // = 2130969831;
    @DexIgnore
    public static /* final */ int trackTintMode; // = 2130969832;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969834;
    @DexIgnore
    public static /* final */ int useCompatPadding; // = 2130969851;
    @DexIgnore
    public static /* final */ int viewInflaterClass; // = 2130969853;
    @DexIgnore
    public static /* final */ int voiceIcon; // = 2130969869;
    @DexIgnore
    public static /* final */ int windowActionBar; // = 2130969934;
    @DexIgnore
    public static /* final */ int windowActionBarOverlay; // = 2130969935;
    @DexIgnore
    public static /* final */ int windowActionModeOverlay; // = 2130969936;
    @DexIgnore
    public static /* final */ int windowFixedHeightMajor; // = 2130969937;
    @DexIgnore
    public static /* final */ int windowFixedHeightMinor; // = 2130969938;
    @DexIgnore
    public static /* final */ int windowFixedWidthMajor; // = 2130969939;
    @DexIgnore
    public static /* final */ int windowFixedWidthMinor; // = 2130969940;
    @DexIgnore
    public static /* final */ int windowMinWidthMajor; // = 2130969941;
    @DexIgnore
    public static /* final */ int windowMinWidthMinor; // = 2130969942;
    @DexIgnore
    public static /* final */ int windowNoTitle; // = 2130969943;
}
