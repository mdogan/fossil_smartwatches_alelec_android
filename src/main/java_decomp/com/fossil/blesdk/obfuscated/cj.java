package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cj {
    @DexIgnore
    public static /* final */ String a; // = ej.a("InputMerger");

    @DexIgnore
    public static cj a(String str) {
        try {
            return (cj) Class.forName(str).newInstance();
        } catch (Exception e) {
            ej a2 = ej.a();
            String str2 = a;
            a2.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract bj a(List<bj> list);
}
