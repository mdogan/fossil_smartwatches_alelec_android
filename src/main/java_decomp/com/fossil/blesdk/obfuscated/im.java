package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface im<TTaskResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(jm<TTaskResult> jmVar) throws Exception;
}
