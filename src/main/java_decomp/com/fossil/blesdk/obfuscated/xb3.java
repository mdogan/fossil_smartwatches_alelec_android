package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xb3 implements Factory<GoalTrackingOverviewWeekPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewWeekPresenter a(vb3 vb3, UserRepository userRepository, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewWeekPresenter(vb3, userRepository, fn2, goalTrackingRepository);
    }
}
