package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qi4 extends wi4<ri4> {
    @DexIgnore
    public /* final */ jd4<Throwable, cb4> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qi4(ri4 ri4, jd4<? super Throwable, cb4> jd4) {
        super(ri4);
        wd4.b(ri4, "job");
        wd4.b(jd4, "handler");
        this.i = jd4;
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.invoke(th);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCompletion[" + ph4.a((Object) this) + '@' + ph4.b(this) + ']';
    }
}
