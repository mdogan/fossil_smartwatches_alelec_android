package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.br4;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class er4 implements DialogInterface.OnClickListener {
    @DexIgnore
    public Object e;
    @DexIgnore
    public fr4 f;
    @DexIgnore
    public br4.a g;
    @DexIgnore
    public br4.b h;

    @DexIgnore
    public er4(hr4 hr4, fr4 fr4, br4.a aVar, br4.b bVar) {
        Object obj;
        if (hr4.getParentFragment() != null) {
            obj = hr4.getParentFragment();
        } else {
            obj = hr4.getActivity();
        }
        this.e = obj;
        this.f = fr4;
        this.g = aVar;
        this.h = bVar;
    }

    @DexIgnore
    public final void a() {
        br4.a aVar = this.g;
        if (aVar != null) {
            fr4 fr4 = this.f;
            aVar.a(fr4.d, Arrays.asList(fr4.f));
        }
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        fr4 fr4 = this.f;
        int i2 = fr4.d;
        if (i == -1) {
            String[] strArr = fr4.f;
            br4.b bVar = this.h;
            if (bVar != null) {
                bVar.a(i2);
            }
            Object obj = this.e;
            if (obj instanceof Fragment) {
                mr4.a((Fragment) obj).a(i2, strArr);
            } else if (obj instanceof Activity) {
                mr4.a((Activity) obj).a(i2, strArr);
            } else {
                throw new RuntimeException("Host must be an Activity or Fragment!");
            }
        } else {
            br4.b bVar2 = this.h;
            if (bVar2 != null) {
                bVar2.b(i2);
            }
            a();
        }
    }

    @DexIgnore
    public er4(gr4 gr4, fr4 fr4, br4.a aVar, br4.b bVar) {
        this.e = gr4.getActivity();
        this.f = fr4;
        this.g = aVar;
        this.h = bVar;
    }
}
