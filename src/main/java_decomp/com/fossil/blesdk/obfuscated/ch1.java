package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ch1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ long f;
    @DexIgnore
    public /* final */ /* synthetic */ bg1 g;

    @DexIgnore
    public ch1(bg1 bg1, String str, long j) {
        this.g = bg1;
        this.e = str;
        this.f = j;
    }

    @DexIgnore
    public final void run() {
        this.g.c(this.e, this.f);
    }
}
