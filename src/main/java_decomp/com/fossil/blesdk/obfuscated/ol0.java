package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ol0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ol0> CREATOR; // = new pl0();
    @DexIgnore
    public Bundle e;
    @DexIgnore
    public xd0[] f;

    @DexIgnore
    public ol0(Bundle bundle, xd0[] xd0Arr) {
        this.e = bundle;
        this.f = xd0Arr;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e, false);
        lk0.a(parcel, 2, (T[]) this.f, i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public ol0() {
    }
}
