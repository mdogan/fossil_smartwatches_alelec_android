package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh3 extends as2 implements xs3.g {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ProfileEditViewModel j;
    @DexIgnore
    public ur3<bf2> k;
    @DexIgnore
    public gk2 l;
    @DexIgnore
    public k42 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final nh3 a() {
            return new nh3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public b(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.OTHER);
            nh3.d(this.e).a(Gender.OTHER);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements dc<ProfileEditViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 a;

        @DexIgnore
        public c(nh3 nh3) {
            this.a = nh3;
        }

        @DexIgnore
        public final void a(ProfileEditViewModel.b bVar) {
            MFUser f = bVar.f();
            if (f != null) {
                this.a.c(f);
            }
            Boolean g = bVar.g();
            if (g != null) {
                this.a.O(g.booleanValue());
            }
            Uri a2 = bVar.a();
            if (a2 != null) {
                this.a.a(a2);
            }
            if (bVar.c()) {
                this.a.e();
            } else {
                this.a.d();
            }
            if (bVar.e() != null) {
                this.a.n();
            }
            Pair<Integer, String> d = bVar.d();
            if (d != null) {
                this.a.a(d.getFirst().intValue(), d.getSecond());
            }
            if (bVar.b()) {
                this.a.o();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public d(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public e(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0030  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Editable editable;
            String valueOf;
            ProfileEditViewModel d = nh3.d(this.e);
            bf2 bf2 = (bf2) nh3.c(this.e).a();
            if (bf2 != null) {
                TextInputEditText textInputEditText = bf2.r;
                if (textInputEditText != null) {
                    editable = textInputEditText.getEditableText();
                    valueOf = String.valueOf(editable);
                    if (valueOf == null) {
                        d.b(StringsKt__StringsKt.d(valueOf).toString());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            editable = null;
            valueOf = String.valueOf(editable);
            if (valueOf == null) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public f(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0030  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Editable editable;
            String valueOf;
            ProfileEditViewModel d = nh3.d(this.e);
            bf2 bf2 = (bf2) nh3.c(this.e).a();
            if (bf2 != null) {
                TextInputEditText textInputEditText = bf2.s;
                if (textInputEditText != null) {
                    editable = textInputEditText.getEditableText();
                    valueOf = String.valueOf(editable);
                    if (valueOf == null) {
                        d.c(StringsKt__StringsKt.d(valueOf).toString());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            editable = null;
            valueOf = String.valueOf(editable);
            if (valueOf == null) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public g(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public h(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.doCameraTask();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements hu3 {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 a;
        @DexIgnore
        public /* final */ /* synthetic */ bf2 b;

        @DexIgnore
        public i(nh3 nh3, bf2 bf2) {
            this.a = nh3;
            this.b = bf2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.b.z;
            wd4.a((Object) rulerValuePicker, "binding.rvpHeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                nh3.d(this.a).a(i);
                return;
            }
            nh3.d(this.a).a(Math.round(qk2.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements hu3 {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 a;
        @DexIgnore
        public /* final */ /* synthetic */ bf2 b;

        @DexIgnore
        public j(nh3 nh3, bf2 bf2) {
            this.a = nh3;
            this.b = bf2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.b.A;
            wd4.a((Object) rulerValuePicker, "binding.rvpWeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                nh3.d(this.a).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            nh3.d(this.a).b(Math.round(qk2.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public k(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.MALE);
            nh3.d(this.e).a(Gender.MALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nh3 e;

        @DexIgnore
        public l(nh3 nh3) {
            this.e = nh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.FEMALE);
            nh3.d(this.e).a(Gender.FEMALE);
        }
    }

    /*
    static {
        String simpleName = nh3.class.getSimpleName();
        wd4.a((Object) simpleName, "ProfileEditFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ur3 c(nh3 nh3) {
        ur3<bf2> ur3 = nh3.k;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProfileEditViewModel d(nh3 nh3) {
        ProfileEditViewModel profileEditViewModel = nh3.j;
        if (profileEditViewModel != null) {
            return profileEditViewModel;
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @zq4(1222)
    public final void doCameraTask() {
        if (cn2.a(cn2.d, getActivity(), "EDIT_AVATAR", false, 4, (Object) null)) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Intent b2 = bl2.b(activity);
                if (b2 != null) {
                    startActivityForResult(b2, 1234);
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if ((kotlin.text.StringsKt__StringsKt.d(r6).length() > 0) != false) goto L_0x0057;
     */
    @DexIgnore
    public final void O(boolean z) {
        ur3<bf2> ur3 = this.k;
        if (ur3 != null) {
            bf2 a2 = ur3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                wd4.a((Object) progressButton, "it.save");
                boolean z2 = true;
                if (z) {
                    TextInputEditText textInputEditText = a2.r;
                    wd4.a((Object) textInputEditText, "it.etFirstName");
                    Editable editableText = textInputEditText.getEditableText();
                    wd4.a((Object) editableText, "it.etFirstName.editableText");
                    if (StringsKt__StringsKt.d(editableText).length() > 0) {
                        TextInputEditText textInputEditText2 = a2.s;
                        wd4.a((Object) textInputEditText2, "it.etLastName");
                        Editable editableText2 = textInputEditText2.getEditableText();
                        wd4.a((Object) editableText2, "it.etLastName.editableText");
                    }
                }
                z2 = false;
                progressButton.setEnabled(z2);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        wd4.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            ProfileEditViewModel profileEditViewModel = this.j;
                            if (profileEditViewModel != null) {
                                if (profileEditViewModel.e()) {
                                    es3 es3 = es3.c;
                                    FragmentManager childFragmentManager = getChildFragmentManager();
                                    wd4.a((Object) childFragmentManager, "childFragmentManager");
                                    es3.I(childFragmentManager);
                                } else {
                                    FragmentActivity activity3 = getActivity();
                                    if (activity3 != null) {
                                        activity3.finish();
                                    }
                                }
                                return true;
                            }
                            wd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            } else {
                wd4.a();
                throw null;
            }
        }
        return true;
    }

    @DexIgnore
    public final void T0() {
        ProfileEditViewModel profileEditViewModel = this.j;
        if (profileEditViewModel != null) {
            profileEditViewModel.g();
        } else {
            wd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        wd4.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                wd4.a();
                                throw null;
                            }
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void o() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.k(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            ProfileEditViewModel profileEditViewModel = this.j;
            if (profileEditViewModel != null) {
                profileEditViewModel.a(intent);
            } else {
                wd4.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        bf2 bf2 = (bf2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_profile_edit, (ViewGroup) null, false, O0());
        PortfolioApp.W.c().g().a(new qh3()).a(this);
        k42 k42 = this.m;
        if (k42 != null) {
            jc a2 = mc.a((Fragment) this, (lc.b) k42).a(ProfileEditViewModel.class);
            wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
            this.j = (ProfileEditViewModel) a2;
            bf2.y.setOnClickListener(new d(this));
            bf2.r.addTextChangedListener(new e(this));
            bf2.s.addTextChangedListener(new f(this));
            bf2.B.setOnClickListener(new g(this));
            bf2.q.setOnClickListener(new h(this));
            bf2.z.setValuePickerListener(new i(this, bf2));
            bf2.A.setValuePickerListener(new j(this, bf2));
            bf2.u.setOnClickListener(new k(this));
            bf2.t.setOnClickListener(new l(this));
            bf2.v.setOnClickListener(new b(this));
            ProfileEditViewModel profileEditViewModel = this.j;
            if (profileEditViewModel != null) {
                profileEditViewModel.c().a(getViewLifecycleOwner(), new c(this));
                this.k = new ur3<>(this, bf2);
                wd4.a((Object) bf2, "binding");
                return bf2.d();
            }
            wd4.d("mViewModel");
            throw null;
        }
        wd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        wl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ProfileEditViewModel profileEditViewModel = this.j;
        if (profileEditViewModel != null) {
            profileEditViewModel.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("edit_profile_view");
        gk2 a2 = dk2.a((Fragment) this);
        wd4.a((Object) a2, "GlideApp.with(this)");
        this.l = a2;
    }

    @DexIgnore
    public final void b(int i2, Unit unit) {
        int i3 = oh3.b[unit.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = o;
            local.d(str, "updateData weight=" + i2 + " metric");
            ur3<bf2> ur3 = this.k;
            if (ur3 != null) {
                bf2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.x;
                    wd4.a((Object) flexibleTextView, "it.ftvWeightUnit");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Kg));
                    RulerValuePicker rulerValuePicker = a2.A;
                    wd4.a((Object) rulerValuePicker, "it.rvpWeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.A.setFormatter(new ProfileFormatter(4));
                    a2.A.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = o;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            ur3<bf2> ur32 = this.k;
            if (ur32 != null) {
                bf2 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.x;
                    wd4.a((Object) flexibleTextView2, "it.ftvWeightUnit");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs));
                    float f2 = qk2.f((float) i2);
                    RulerValuePicker rulerValuePicker2 = a3.A;
                    wd4.a((Object) rulerValuePicker2, "it.rvpWeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.A.setFormatter(new ProfileFormatter(4));
                    a3.A.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"SimpleDateFormat"})
    public final void c(MFUser mFUser) {
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            gk2 gk2 = this.l;
            if (gk2 != null) {
                fk2<Drawable> a2 = gk2.a((Object) new ck2("", str)).a((mv<?>) new sv().a((po<Bitmap>) new ok2()));
                ur3<bf2> ur3 = this.k;
                if (ur3 != null) {
                    bf2 a3 = ur3.a();
                    FossilCircleImageView fossilCircleImageView = a3 != null ? a3.q : null;
                    if (fossilCircleImageView != null) {
                        a2.a((ImageView) fossilCircleImageView);
                        ur3<bf2> ur32 = this.k;
                        if (ur32 != null) {
                            bf2 a4 = ur32.a();
                            if (a4 != null) {
                                FossilCircleImageView fossilCircleImageView2 = a4.q;
                                if (fossilCircleImageView2 != null) {
                                    fossilCircleImageView2.setBorderColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.gray));
                                }
                            }
                            ur3<bf2> ur33 = this.k;
                            if (ur33 != null) {
                                bf2 a5 = ur33.a();
                                if (a5 != null) {
                                    FossilCircleImageView fossilCircleImageView3 = a5.q;
                                    if (fossilCircleImageView3 != null) {
                                        fossilCircleImageView3.setBorderWidth(3);
                                    }
                                }
                                ur3<bf2> ur34 = this.k;
                                if (ur34 != null) {
                                    bf2 a6 = ur34.a();
                                    if (a6 != null) {
                                        FossilCircleImageView fossilCircleImageView4 = a6.q;
                                        if (fossilCircleImageView4 != null) {
                                            fossilCircleImageView4.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.oval_solid_light_grey));
                                        }
                                    }
                                } else {
                                    wd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            wd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else {
                wd4.d("mGlideRequests");
                throw null;
            }
        } else {
            ur3<bf2> ur35 = this.k;
            if (ur35 != null) {
                bf2 a7 = ur35.a();
                if (a7 != null) {
                    FossilCircleImageView fossilCircleImageView5 = a7.q;
                    if (fossilCircleImageView5 != null) {
                        gk2 gk22 = this.l;
                        if (gk22 != null) {
                            fossilCircleImageView5.a(gk22, profilePicture, str);
                        } else {
                            wd4.d("mGlideRequests");
                            throw null;
                        }
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
        ur3<bf2> ur36 = this.k;
        if (ur36 != null) {
            bf2 a8 = ur36.a();
            if (a8 != null) {
                TextInputEditText textInputEditText = a8.r;
                if (textInputEditText != null) {
                    textInputEditText.setText(mFUser.getFirstName());
                }
            }
            ur3<bf2> ur37 = this.k;
            if (ur37 != null) {
                bf2 a9 = ur37.a();
                if (a9 != null) {
                    TextInputEditText textInputEditText2 = a9.s;
                    if (textInputEditText2 != null) {
                        textInputEditText2.setText(mFUser.getLastName());
                    }
                }
                ur3<bf2> ur38 = this.k;
                if (ur38 != null) {
                    bf2 a10 = ur38.a();
                    if (a10 != null) {
                        FlexibleTextView flexibleTextView = a10.D;
                        if (flexibleTextView != null) {
                            flexibleTextView.setText(mFUser.getEmail());
                        }
                    }
                    nk2 nk2 = nk2.a;
                    Gender gender = mFUser.getGender();
                    wd4.a((Object) gender, "user.gender");
                    Pair<Integer, Integer> a11 = nk2.a(gender, MFUser.getAge(mFUser.getBirthday()));
                    if (mFUser.getHeightInCentimeters() == 0) {
                        mFUser.setHeightInCentimeters(a11.getFirst().intValue());
                    }
                    if (mFUser.getHeightInCentimeters() > 0) {
                        int heightInCentimeters = mFUser.getHeightInCentimeters();
                        Unit heightUnit = mFUser.getHeightUnit();
                        wd4.a((Object) heightUnit, "user.heightUnit");
                        a(heightInCentimeters, heightUnit);
                    }
                    if (mFUser.getWeightInGrams() == 0) {
                        mFUser.setWeightInGrams(a11.getSecond().intValue() * 1000);
                    }
                    if (mFUser.getWeightInGrams() > 0) {
                        int weightInGrams = mFUser.getWeightInGrams();
                        Unit weightUnit = mFUser.getWeightUnit();
                        wd4.a((Object) weightUnit, "user.weightUnit");
                        b(weightInGrams, weightUnit);
                    }
                    String birthday = mFUser.getBirthday();
                    if (!TextUtils.isEmpty(birthday)) {
                        try {
                            Date e2 = sk2.e(birthday);
                            ur3<bf2> ur39 = this.k;
                            if (ur39 != null) {
                                bf2 a12 = ur39.a();
                                if (a12 != null) {
                                    FlexibleTextView flexibleTextView2 = a12.C;
                                    if (flexibleTextView2 != null) {
                                        flexibleTextView2.setText(sk2.b(e2));
                                    }
                                }
                            } else {
                                wd4.d("mBinding");
                                throw null;
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                    Gender gender2 = mFUser.getGender();
                    wd4.a((Object) gender2, "user.gender");
                    b(gender2);
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void d() {
        ur3<bf2> ur3 = this.k;
        if (ur3 != null) {
            bf2 a2 = ur3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                if (progressButton != null) {
                    progressButton.b();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e() {
        ur3<bf2> ur3 = this.k;
        if (ur3 != null) {
            bf2 a2 = ur3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                if (progressButton != null) {
                    progressButton.c();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(int i2, Unit unit) {
        int i3 = oh3.a[unit.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = o;
            local.d(str, "updateData height=" + i2 + " metric");
            ur3<bf2> ur3 = this.k;
            if (ur3 != null) {
                bf2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    wd4.a((Object) flexibleTextView, "it.ftvHeightUnit");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_PreferredUnits_ChooseUnits_Label__Cm));
                    RulerValuePicker rulerValuePicker = a2.z;
                    wd4.a((Object) rulerValuePicker, "it.rvpHeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.z.setFormatter(new ProfileFormatter(-1));
                    a2.z.a(100, 251, i2);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = o;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            ur3<bf2> ur32 = this.k;
            if (ur32 != null) {
                bf2 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.w;
                    wd4.a((Object) flexibleTextView2, "it.ftvHeightUnit");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_PreferredUnits_ChooseUnits_Label__Ft));
                    Pair<Integer, Integer> b2 = qk2.b((float) i2);
                    RulerValuePicker rulerValuePicker2 = a3.z;
                    wd4.a((Object) rulerValuePicker2, "it.rvpHeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.z.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker3 = a3.z;
                    Integer first = b2.getFirst();
                    wd4.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = qk2.a(first.intValue());
                    Integer second = b2.getSecond();
                    wd4.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker3.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void b(Gender gender) {
        ur3<bf2> ur3 = this.k;
        if (ur3 != null) {
            bf2 a2 = ur3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                int a3 = k6.a((Context) c2, (int) R.color.primaryColor);
                int a4 = k6.a((Context) c2, (int) R.color.surface);
                Drawable c3 = k6.c(c2, R.drawable.bg_border_disable_color_primary);
                Drawable c4 = k6.c(c2, R.drawable.bg_border_active_color_primary);
                a2.t.setTextColor(a3);
                a2.u.setTextColor(a3);
                a2.v.setTextColor(a3);
                FlexibleButton flexibleButton = a2.t;
                wd4.a((Object) flexibleButton, "it.fbFemale");
                flexibleButton.setBackground(c3);
                FlexibleButton flexibleButton2 = a2.u;
                wd4.a((Object) flexibleButton2, "it.fbMale");
                flexibleButton2.setBackground(c3);
                FlexibleButton flexibleButton3 = a2.v;
                wd4.a((Object) flexibleButton3, "it.fbOther");
                flexibleButton3.setBackground(c3);
                int i2 = oh3.c[gender.ordinal()];
                if (i2 == 1) {
                    ur3<bf2> ur32 = this.k;
                    if (ur32 != null) {
                        bf2 a5 = ur32.a();
                        if (a5 != null) {
                            FlexibleButton flexibleButton4 = a5.t;
                            if (flexibleButton4 != null) {
                                flexibleButton4.setTextColor(a4);
                                flexibleButton4.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    ur3<bf2> ur33 = this.k;
                    if (ur33 != null) {
                        bf2 a6 = ur33.a();
                        if (a6 != null) {
                            FlexibleButton flexibleButton5 = a6.v;
                            if (flexibleButton5 != null) {
                                flexibleButton5.setTextColor(a4);
                                flexibleButton5.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                } else {
                    ur3<bf2> ur34 = this.k;
                    if (ur34 != null) {
                        bf2 a7 = ur34.a();
                        if (a7 != null) {
                            FlexibleButton flexibleButton6 = a7.u;
                            if (flexibleButton6 != null) {
                                flexibleButton6.setTextColor(a4);
                                flexibleButton6.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Uri uri) {
        if (isActive()) {
            gk2 gk2 = this.l;
            if (gk2 != null) {
                fk2<Drawable> a2 = gk2.a(uri).a(qp.a).a((mv<?>) new sv().a((po<Bitmap>) new ok2()));
                ur3<bf2> ur3 = this.k;
                if (ur3 != null) {
                    bf2 a3 = ur3.a();
                    FossilCircleImageView fossilCircleImageView = a3 != null ? a3.q : null;
                    if (fossilCircleImageView != null) {
                        a2.a((ImageView) fossilCircleImageView);
                        O(true);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wd4.b(str, "tag");
        wd4.b(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i2, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == R.id.tv_cancel) {
            n();
        } else if (i2 == R.id.tv_ok) {
            T0();
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i2, str, childFragmentManager);
        }
    }
}
