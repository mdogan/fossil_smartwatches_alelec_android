package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gh1 extends vi1 {
    @DexIgnore
    public static /* final */ Pair<String, Long> w; // = new Pair<>("", 0L);
    @DexIgnore
    public SharedPreferences c;
    @DexIgnore
    public kh1 d;
    @DexIgnore
    public /* final */ jh1 e; // = new jh1(this, "last_upload", 0);
    @DexIgnore
    public /* final */ jh1 f; // = new jh1(this, "last_upload_attempt", 0);
    @DexIgnore
    public /* final */ jh1 g; // = new jh1(this, "backoff", 0);
    @DexIgnore
    public /* final */ jh1 h; // = new jh1(this, "last_delete_stale", 0);
    @DexIgnore
    public /* final */ jh1 i; // = new jh1(this, "midnight_offset", 0);
    @DexIgnore
    public /* final */ jh1 j; // = new jh1(this, "first_open_time", 0);
    @DexIgnore
    public /* final */ jh1 k; // = new jh1(this, "app_install_time", 0);
    @DexIgnore
    public /* final */ lh1 l; // = new lh1(this, "app_instance_id", (String) null);
    @DexIgnore
    public String m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public long o;
    @DexIgnore
    public /* final */ jh1 p; // = new jh1(this, "time_before_start", ButtonService.CONNECT_TIMEOUT);
    @DexIgnore
    public /* final */ jh1 q; // = new jh1(this, "session_timeout", 1800000);
    @DexIgnore
    public /* final */ ih1 r; // = new ih1(this, "start_new_session", true);
    @DexIgnore
    public /* final */ jh1 s; // = new jh1(this, "last_pause_time", 0);
    @DexIgnore
    public /* final */ jh1 t; // = new jh1(this, "time_active", 0);
    @DexIgnore
    public boolean u;
    @DexIgnore
    public ih1 v; // = new ih1(this, "app_backgrounded", false);

    @DexIgnore
    public gh1(yh1 yh1) {
        super(yh1);
    }

    @DexIgnore
    public final boolean A() {
        return this.c.contains("deferred_analytics_collection");
    }

    @DexIgnore
    public final Pair<String, Boolean> a(String str) {
        e();
        long c2 = c().c();
        String str2 = this.m;
        if (str2 != null && c2 < this.o) {
            return new Pair<>(str2, Boolean.valueOf(this.n));
        }
        this.o = c2 + l().a(str, kg1.l);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(getContext());
            if (advertisingIdInfo != null) {
                this.m = advertisingIdInfo.getId();
                this.n = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.m == null) {
                this.m = "";
            }
        } catch (Exception e2) {
            d().z().a("Unable to get advertising id", e2);
            this.m = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.m, Boolean.valueOf(this.n));
    }

    @DexIgnore
    public final String b(String str) {
        e();
        String str2 = (String) a(str).first;
        MessageDigest w2 = ol1.w();
        if (w2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, w2.digest(str2.getBytes()))});
    }

    @DexIgnore
    public final void c(String str) {
        e();
        SharedPreferences.Editor edit = s().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final void d(String str) {
        e();
        SharedPreferences.Editor edit = s().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final boolean p() {
        return true;
    }

    @DexIgnore
    public final void q() {
        this.c = getContext().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.u = this.c.getBoolean("has_been_opened", false);
        if (!this.u) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.d = new kh1(this, "health_monitor", Math.max(0, kg1.m.a().longValue()));
    }

    @DexIgnore
    public final SharedPreferences s() {
        e();
        n();
        return this.c;
    }

    @DexIgnore
    public final String t() {
        e();
        return s().getString("gmp_app_id", (String) null);
    }

    @DexIgnore
    public final String u() {
        e();
        return s().getString("admob_app_id", (String) null);
    }

    @DexIgnore
    public final Boolean v() {
        e();
        if (!s().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(s().getBoolean("use_service", false));
    }

    @DexIgnore
    public final void w() {
        e();
        d().A().a("Clearing collection preferences.");
        if (l().a(kg1.n0)) {
            Boolean x = x();
            SharedPreferences.Editor edit = s().edit();
            edit.clear();
            edit.apply();
            if (x != null) {
                a(x.booleanValue());
                return;
            }
            return;
        }
        boolean contains = s().contains("measurement_enabled");
        boolean z = true;
        if (contains) {
            z = c(true);
        }
        SharedPreferences.Editor edit2 = s().edit();
        edit2.clear();
        edit2.apply();
        if (contains) {
            a(z);
        }
    }

    @DexIgnore
    public final Boolean x() {
        e();
        if (s().contains("measurement_enabled")) {
            return Boolean.valueOf(s().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    @DexIgnore
    public final String y() {
        e();
        String string = s().getString("previous_os_version", (String) null);
        h().n();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = s().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    @DexIgnore
    public final boolean z() {
        e();
        return s().getBoolean("deferred_analytics_collection", false);
    }

    @DexIgnore
    public final void b(boolean z) {
        e();
        d().A().a("Setting useService", Boolean.valueOf(z));
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    @DexIgnore
    public final boolean c(boolean z) {
        e();
        return s().getBoolean("measurement_enabled", z);
    }

    @DexIgnore
    public final void d(boolean z) {
        e();
        d().A().a("Updating deferred analytics collection", Boolean.valueOf(z));
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("deferred_analytics_collection", z);
        edit.apply();
    }

    @DexIgnore
    public final void a(boolean z) {
        e();
        d().A().a("Setting measurementEnabled", Boolean.valueOf(z));
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    @DexIgnore
    public final boolean a(long j2) {
        return j2 - this.q.a() > this.s.a();
    }
}
