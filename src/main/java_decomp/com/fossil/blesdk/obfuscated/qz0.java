package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qz0 extends l21<w01> {
    @DexIgnore
    public static /* final */ ee0.g<qz0> E; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0<Object> F; // = new ee0<>("Fitness.GOALS_API", new sz0(), E);

    /*
    static {
        new ee0("Fitness.GOALS_CLIENT", new tz0(), E);
    }
    */

    @DexIgnore
    public qz0(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 125, bVar, cVar, lj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitGoalsApi");
        if (queryLocalInterface instanceof w01) {
            return (w01) queryLocalInterface;
        }
        return new x01(iBinder);
    }

    @DexIgnore
    public final int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitGoalsApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.GoalsApi";
    }
}
