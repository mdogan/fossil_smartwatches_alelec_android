package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.transition.Transition;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vg extends hi {
    @DexIgnore
    public float b; // = 3.0f;

    @DexIgnore
    public long a(ViewGroup viewGroup, Transition transition, qh qhVar, qh qhVar2) {
        int i;
        int i2;
        int i3;
        if (qhVar == null && qhVar2 == null) {
            return 0;
        }
        if (qhVar2 == null || b(qhVar) == 0) {
            i = -1;
        } else {
            qhVar = qhVar2;
            i = 1;
        }
        int c = c(qhVar);
        int d = d(qhVar);
        Rect c2 = transition.c();
        if (c2 != null) {
            i3 = c2.centerX();
            i2 = c2.centerY();
        } else {
            int[] iArr = new int[2];
            viewGroup.getLocationOnScreen(iArr);
            int round = Math.round(((float) (iArr[0] + (viewGroup.getWidth() / 2))) + viewGroup.getTranslationX());
            i2 = Math.round(((float) (iArr[1] + (viewGroup.getHeight() / 2))) + viewGroup.getTranslationY());
            i3 = round;
        }
        float a = a((float) c, (float) d, (float) i3, (float) i2) / a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) viewGroup.getWidth(), (float) viewGroup.getHeight());
        long b2 = transition.b();
        if (b2 < 0) {
            b2 = 300;
        }
        return (long) Math.round((((float) (b2 * ((long) i))) / this.b) * a);
    }

    @DexIgnore
    public static float a(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }
}
