package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sv extends mv<sv> {
    @DexIgnore
    public static sv E;
    @DexIgnore
    public static sv F;

    @DexIgnore
    public static sv b(qp qpVar) {
        return (sv) new sv().a(qpVar);
    }

    @DexIgnore
    public static sv c(boolean z) {
        if (z) {
            if (E == null) {
                E = (sv) ((sv) new sv().a(true)).a();
            }
            return E;
        }
        if (F == null) {
            F = (sv) ((sv) new sv().a(false)).a();
        }
        return F;
    }

    @DexIgnore
    public static sv b(ko koVar) {
        return (sv) new sv().a(koVar);
    }

    @DexIgnore
    public static sv b(Class<?> cls) {
        return (sv) new sv().a(cls);
    }
}
