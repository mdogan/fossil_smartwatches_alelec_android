package com.fossil.blesdk.obfuscated;

import com.google.android.libraries.places.api.net.PlacesClient;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface z23 extends w52<y23> {
    @DexIgnore
    void B(String str);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void j(List<String> list);

    @DexIgnore
    void setTitle(String str);

    @DexIgnore
    void v(String str);
}
