package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ri4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class si4<J extends ri4> extends wi4<J> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si4(J j) {
        super(j);
        wd4.b(j, "job");
    }
}
