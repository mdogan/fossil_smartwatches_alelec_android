package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.jr3;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class on2 extends BroadcastReceiver {
    @DexIgnore
    public fn2 a;
    @DexIgnore
    public jr3 b;
    @DexIgnore
    public AlarmHelper c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<jr3.d, jr3.c> {
        @DexIgnore
        public /* final */ /* synthetic */ on2 a;

        @DexIgnore
        public b(on2 on2) {
            this.a = on2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(jr3.d dVar) {
            wd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            this.a.a().e();
        }

        @DexIgnore
        public void a(jr3.c cVar) {
            wd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", cVar.toString());
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public on2() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.c;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        fn2 fn2 = this.a;
        if (fn2 != null) {
            long g = fn2.g(PortfolioApp.W.c().e());
            if (!TextUtils.isEmpty(action) && wd4.a((Object) action, (Object) "android.intent.action.TIME_TICK") && System.currentTimeMillis() - g <= ScanService.BLE_SCAN_TIMEOUT) {
                FossilNotificationBar.Companion companion = FossilNotificationBar.c;
                if (context != null) {
                    companion.a(context);
                } else {
                    wd4.a();
                    throw null;
                }
            }
            if (wd4.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    wd4.a((Object) instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    wd4.a((Object) instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    wd4.a((Object) instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        jr3 jr3 = this.b;
                        if (jr3 != null) {
                            jr3.a(new jr3.b(), new b(this));
                        } else {
                            wd4.d("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e);
                }
            }
        } else {
            wd4.d("mSharedPreferencesManager");
            throw null;
        }
    }
}
