package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w10 extends GattOperationResult {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId b;
    @DexIgnore
    public /* final */ GattDescriptor.DescriptorId c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w10(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, GattDescriptor.DescriptorId descriptorId, byte[] bArr) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        wd4.b(characteristicId, "characteristicId");
        wd4.b(descriptorId, "descriptorId");
        wd4.b(bArr, "data");
        this.b = characteristicId;
        this.c = descriptorId;
        this.d = bArr;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId b() {
        return this.b;
    }

    @DexIgnore
    public final byte[] c() {
        return this.d;
    }

    @DexIgnore
    public final GattDescriptor.DescriptorId d() {
        return this.c;
    }
}
