package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DecodeJob;
import com.fossil.blesdk.obfuscated.br;
import com.fossil.blesdk.obfuscated.uq;
import com.fossil.blesdk.obfuscated.wp;
import com.fossil.blesdk.obfuscated.ww;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rp implements tp, br.a, wp.a {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);
    @DexIgnore
    public /* final */ yp a;
    @DexIgnore
    public /* final */ vp b;
    @DexIgnore
    public /* final */ br c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ eq e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ a g;
    @DexIgnore
    public /* final */ jp h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ DecodeJob.e a;
        @DexIgnore
        public /* final */ h8<DecodeJob<?>> b; // = ww.a(150, new C0032a());
        @DexIgnore
        public int c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.rp$a$a  reason: collision with other inner class name */
        public class C0032a implements ww.d<DecodeJob<?>> {
            @DexIgnore
            public C0032a() {
            }

            @DexIgnore
            public DecodeJob<?> a() {
                a aVar = a.this;
                return new DecodeJob<>(aVar.a, aVar.b);
            }
        }

        @DexIgnore
        public a(DecodeJob.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public <R> DecodeJob<R> a(un unVar, Object obj, up upVar, ko koVar, int i, int i2, Class<?> cls, Class<R> cls2, Priority priority, qp qpVar, Map<Class<?>, po<?>> map, boolean z, boolean z2, boolean z3, mo moVar, DecodeJob.b<R> bVar) {
            DecodeJob<R> a2 = this.b.a();
            uw.a(a2);
            DecodeJob<R> decodeJob = a2;
            int i3 = this.c;
            int i4 = i3;
            this.c = i3 + 1;
            decodeJob.a(unVar, obj, upVar, koVar, i, i2, cls, cls2, priority, qpVar, map, z, z2, z3, moVar, bVar, i4);
            return decodeJob;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ er a;
        @DexIgnore
        public /* final */ er b;
        @DexIgnore
        public /* final */ er c;
        @DexIgnore
        public /* final */ er d;
        @DexIgnore
        public /* final */ tp e;
        @DexIgnore
        public /* final */ wp.a f;
        @DexIgnore
        public /* final */ h8<sp<?>> g; // = ww.a(150, new a());

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ww.d<sp<?>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public sp<?> a() {
                b bVar = b.this;
                return new sp(bVar.a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        @DexIgnore
        public b(er erVar, er erVar2, er erVar3, er erVar4, tp tpVar, wp.a aVar) {
            this.a = erVar;
            this.b = erVar2;
            this.c = erVar3;
            this.d = erVar4;
            this.e = tpVar;
            this.f = aVar;
        }

        @DexIgnore
        public <R> sp<R> a(ko koVar, boolean z, boolean z2, boolean z3, boolean z4) {
            sp<R> a2 = this.g.a();
            uw.a(a2);
            sp<R> spVar = a2;
            spVar.a(koVar, z, z2, z3, z4);
            return spVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements DecodeJob.e {
        @DexIgnore
        public /* final */ uq.a a;
        @DexIgnore
        public volatile uq b;

        @DexIgnore
        public c(uq.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public uq a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new vq();
                    }
                }
            }
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d {
        @DexIgnore
        public /* final */ sp<?> a;
        @DexIgnore
        public /* final */ tv b;

        @DexIgnore
        public d(tv tvVar, sp<?> spVar) {
            this.b = tvVar;
            this.a = spVar;
        }

        @DexIgnore
        public void a() {
            synchronized (rp.this) {
                this.a.c(this.b);
            }
        }
    }

    @DexIgnore
    public rp(br brVar, uq.a aVar, er erVar, er erVar2, er erVar3, er erVar4, boolean z) {
        this(brVar, aVar, erVar, erVar2, erVar3, erVar4, (yp) null, (vp) null, (jp) null, (b) null, (a) null, (eq) null, z);
    }

    @DexIgnore
    public <R> d a(un unVar, Object obj, ko koVar, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, qp qpVar, Map<Class<?>, po<?>> map, boolean z, boolean z2, mo moVar, boolean z3, boolean z4, boolean z5, boolean z6, tv tvVar, Executor executor) {
        long a2 = i ? qw.a() : 0;
        up a3 = this.b.a(obj, koVar, i2, i3, map, cls, cls2, moVar);
        synchronized (this) {
            wp<?> a4 = a(a3, z3, a2);
            if (a4 == null) {
                d a5 = a(unVar, obj, koVar, i2, i3, cls, cls2, priority, qpVar, map, z, z2, moVar, z3, z4, z5, z6, tvVar, executor, a3, a2);
                return a5;
            }
            tvVar.a(a4, DataSource.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final wp<?> b(ko koVar) {
        wp<?> b2 = this.h.b(koVar);
        if (b2 != null) {
            b2.d();
        }
        return b2;
    }

    @DexIgnore
    public final wp<?> c(ko koVar) {
        wp<?> a2 = a(koVar);
        if (a2 != null) {
            a2.d();
            this.h.a(koVar, a2);
        }
        return a2;
    }

    @DexIgnore
    public rp(br brVar, uq.a aVar, er erVar, er erVar2, er erVar3, er erVar4, yp ypVar, vp vpVar, jp jpVar, b bVar, a aVar2, eq eqVar, boolean z) {
        this.c = brVar;
        uq.a aVar3 = aVar;
        this.f = new c(aVar);
        jp jpVar2 = jpVar == null ? new jp(z) : jpVar;
        this.h = jpVar2;
        jpVar2.a((wp.a) this);
        this.b = vpVar == null ? new vp() : vpVar;
        this.a = ypVar == null ? new yp() : ypVar;
        this.d = bVar == null ? new b(erVar, erVar2, erVar3, erVar4, this, this) : bVar;
        this.g = aVar2 == null ? new a(this.f) : aVar2;
        this.e = eqVar == null ? new eq() : eqVar;
        brVar.a((br.a) this);
    }

    @DexIgnore
    public void b(bq<?> bqVar) {
        if (bqVar instanceof wp) {
            ((wp) bqVar).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> d a(un unVar, Object obj, ko koVar, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, qp qpVar, Map<Class<?>, po<?>> map, boolean z, boolean z2, mo moVar, boolean z3, boolean z4, boolean z5, boolean z6, tv tvVar, Executor executor, up upVar, long j) {
        tv tvVar2 = tvVar;
        Executor executor2 = executor;
        up upVar2 = upVar;
        long j2 = j;
        sp<?> a2 = this.a.a((ko) upVar2, z6);
        if (a2 != null) {
            a2.a(tvVar2, executor2);
            if (i) {
                a("Added to existing load", j2, (ko) upVar2);
            }
            return new d(tvVar2, a2);
        }
        sp a3 = this.d.a(upVar, z3, z4, z5, z6);
        sp spVar = a3;
        up upVar3 = upVar2;
        DecodeJob<R> a4 = this.g.a(unVar, obj, upVar, koVar, i2, i3, cls, cls2, priority, qpVar, map, z, z2, z6, moVar, a3);
        this.a.a((ko) upVar3, (sp<?>) spVar);
        sp spVar2 = spVar;
        up upVar4 = upVar3;
        tv tvVar3 = tvVar;
        spVar2.a(tvVar3, executor);
        spVar2.b(a4);
        if (i) {
            a("Started new load", j, (ko) upVar4);
        }
        return new d(tvVar3, spVar2);
    }

    @DexIgnore
    public final wp<?> a(up upVar, boolean z, long j) {
        if (!z) {
            return null;
        }
        wp<?> b2 = b((ko) upVar);
        if (b2 != null) {
            if (i) {
                a("Loaded resource from active resources", j, (ko) upVar);
            }
            return b2;
        }
        wp<?> c2 = c(upVar);
        if (c2 == null) {
            return null;
        }
        if (i) {
            a("Loaded resource from cache", j, (ko) upVar);
        }
        return c2;
    }

    @DexIgnore
    public static void a(String str, long j, ko koVar) {
        Log.v("Engine", str + " in " + qw.a(j) + "ms, key: " + koVar);
    }

    @DexIgnore
    public final wp<?> a(ko koVar) {
        bq<?> a2 = this.c.a(koVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof wp) {
            return (wp) a2;
        }
        return new wp<>(a2, true, true, koVar, this);
    }

    @DexIgnore
    public synchronized void a(sp<?> spVar, ko koVar, wp<?> wpVar) {
        if (wpVar != null) {
            if (wpVar.f()) {
                this.h.a(koVar, wpVar);
            }
        }
        this.a.b(koVar, spVar);
    }

    @DexIgnore
    public synchronized void a(sp<?> spVar, ko koVar) {
        this.a.b(koVar, spVar);
    }

    @DexIgnore
    public void a(bq<?> bqVar) {
        this.e.a(bqVar);
    }

    @DexIgnore
    public void a(ko koVar, wp<?> wpVar) {
        this.h.a(koVar);
        if (wpVar.f()) {
            this.c.a(koVar, wpVar);
        } else {
            this.e.a(wpVar);
        }
    }
}
