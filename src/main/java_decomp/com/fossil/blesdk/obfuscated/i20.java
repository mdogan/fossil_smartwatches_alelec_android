package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.config.DeviceConfigKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class i20 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[DeviceConfigKey.values().length];

    /*
    static {
        a[DeviceConfigKey.BIOMETRIC_PROFILE.ordinal()] = 1;
        a[DeviceConfigKey.DAILY_STEP.ordinal()] = 2;
        a[DeviceConfigKey.DAILY_STEP_GOAL.ordinal()] = 3;
        a[DeviceConfigKey.DAILY_CALORIE.ordinal()] = 4;
        a[DeviceConfigKey.DAILY_CALORIE_GOAL.ordinal()] = 5;
        a[DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE.ordinal()] = 6;
        a[DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL.ordinal()] = 7;
        a[DeviceConfigKey.DAILY_DISTANCE.ordinal()] = 8;
        a[DeviceConfigKey.INACTIVE_NUDGE.ordinal()] = 9;
        a[DeviceConfigKey.VIBE_STRENGTH.ordinal()] = 10;
        a[DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE.ordinal()] = 11;
        a[DeviceConfigKey.TIME.ordinal()] = 12;
        a[DeviceConfigKey.BATTERY.ordinal()] = 13;
        a[DeviceConfigKey.HEART_RATE_MODE.ordinal()] = 14;
        a[DeviceConfigKey.DAILY_SLEEP.ordinal()] = 15;
        a[DeviceConfigKey.DISPLAY_UNIT.ordinal()] = 16;
        a[DeviceConfigKey.SECOND_TIMEZONE_OFFSET.ordinal()] = 17;
        a[DeviceConfigKey.CURRENT_HEART_RATE.ordinal()] = 18;
    }
    */
}
