package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ep0 {
    @DexIgnore
    public static /* final */ cp0 A; // = cp0.f("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ cp0 B; // = cp0.f("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ cp0 C; // = cp0.f("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ cp0 D; // = cp0.f("cervical_position");
    @DexIgnore
    public static /* final */ cp0 E; // = cp0.f("cervical_dilation");
    @DexIgnore
    public static /* final */ cp0 F; // = cp0.f("cervical_firmness");
    @DexIgnore
    public static /* final */ cp0 G; // = cp0.f("menstrual_flow");
    @DexIgnore
    public static /* final */ cp0 H; // = cp0.f("ovulation_test_result");
    @DexIgnore
    public static /* final */ cp0 a; // = cp0.g("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ cp0 b; // = cp0.g("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ cp0 c; // = cp0.g("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ cp0 d; // = cp0.g("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ cp0 e; // = cp0.g("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ cp0 f; // = cp0.g("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ cp0 g; // = cp0.g("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ cp0 h; // = cp0.g("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ cp0 i; // = cp0.f("body_position");
    @DexIgnore
    public static /* final */ cp0 j; // = cp0.f("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ cp0 k; // = cp0.g("blood_glucose_level");
    @DexIgnore
    public static /* final */ cp0 l; // = cp0.f("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ cp0 m; // = cp0.f("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ cp0 n; // = cp0.f("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ cp0 o; // = cp0.g("oxygen_saturation");
    @DexIgnore
    public static /* final */ cp0 p; // = cp0.g("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ cp0 q; // = cp0.g("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ cp0 r; // = cp0.g("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ cp0 s; // = cp0.g("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ cp0 t; // = cp0.g("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ cp0 u; // = cp0.g("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ cp0 v; // = cp0.g("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ cp0 w; // = cp0.f("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ cp0 x; // = cp0.f("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ cp0 y; // = cp0.f("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ cp0 z; // = cp0.g("body_temperature");
}
