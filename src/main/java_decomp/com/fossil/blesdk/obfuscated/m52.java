package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m52 implements Factory<fn2> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public m52(o42 o42, Provider<Context> provider) {
        this.a = o42;
        this.b = provider;
    }

    @DexIgnore
    public static m52 a(o42 o42, Provider<Context> provider) {
        return new m52(o42, provider);
    }

    @DexIgnore
    public static fn2 b(o42 o42, Provider<Context> provider) {
        return a(o42, provider.get());
    }

    @DexIgnore
    public static fn2 a(o42 o42, Context context) {
        fn2 a2 = o42.a(context);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public fn2 get() {
        return b(this.a, this.b);
    }
}
