package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uo;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vo {
    @DexIgnore
    public static /* final */ uo.a<?> b; // = new a();
    @DexIgnore
    public /* final */ Map<Class<?>, uo.a<?>> a; // = new HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements uo.a<Object> {
        @DexIgnore
        public uo<Object> a(Object obj) {
            return new b(obj);
        }

        @DexIgnore
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements uo<Object> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public Object b() {
            return this.a;
        }
    }

    @DexIgnore
    public synchronized void a(uo.a<?> aVar) {
        this.a.put(aVar.getDataClass(), aVar);
    }

    @DexIgnore
    public synchronized <T> uo<T> a(T t) {
        uo.a<?> aVar;
        uw.a(t);
        aVar = this.a.get(t.getClass());
        if (aVar == null) {
            Iterator<uo.a<?>> it = this.a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                uo.a<?> next = it.next();
                if (next.getDataClass().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = b;
        }
        return aVar.a(t);
    }
}
