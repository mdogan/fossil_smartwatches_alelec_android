package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i61 extends wb1<i61> {
    @DexIgnore
    public static volatile i61[] e;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public long[] d; // = ec1.b;

    @DexIgnore
    public i61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static i61[] e() {
        if (e == null) {
            synchronized (ac1.b) {
                if (e == null) {
                    e = new i61[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        long[] jArr = this.d;
        if (jArr != null && jArr.length > 0) {
            int i = 0;
            while (true) {
                long[] jArr2 = this.d;
                if (i >= jArr2.length) {
                    break;
                }
                vb1.b(2, jArr2[i]);
                i++;
            }
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof i61)) {
            return false;
        }
        i61 i61 = (i61) obj;
        Integer num = this.c;
        if (num == null) {
            if (i61.c != null) {
                return false;
            }
        } else if (!num.equals(i61.c)) {
            return false;
        }
        if (!ac1.a(this.d, i61.d)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(i61.b);
        }
        yb1 yb12 = i61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (i61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (((hashCode + (num == null ? 0 : num.hashCode())) * 31) + ac1.a(this.d)) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        long[] jArr = this.d;
        if (jArr == null || jArr.length <= 0) {
            return a;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            long[] jArr2 = this.d;
            if (i >= jArr2.length) {
                return a + i2 + (jArr2.length * 1);
            }
            i2 += vb1.b(jArr2[i]);
            i++;
        }
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(ub1.e());
            } else if (c2 == 16) {
                int a = ec1.a(ub1, 16);
                long[] jArr = this.d;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.d, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = ub1.f();
                    ub1.c();
                    length++;
                }
                jArr2[length] = ub1.f();
                this.d = jArr2;
            } else if (c2 == 18) {
                int c3 = ub1.c(ub1.e());
                int a2 = ub1.a();
                int i = 0;
                while (ub1.l() > 0) {
                    ub1.f();
                    i++;
                }
                ub1.f(a2);
                long[] jArr3 = this.d;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.d, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = ub1.f();
                    length2++;
                }
                this.d = jArr4;
                ub1.d(c3);
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
